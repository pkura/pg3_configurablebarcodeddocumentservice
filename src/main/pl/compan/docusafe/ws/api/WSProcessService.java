package pl.compan.docusafe.ws.api;

import com.google.common.collect.Lists;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ManualAssignmentDto;
import pl.compan.docusafe.api.user.office.ProcessService;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.DSApiUtils;

import java.util.List;
import java.util.concurrent.Callable;

import javax.jws.WebParam;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class WSProcessService {

    private ProcessService processService;

    public WSProcessService(){
        processService = Docusafe.getBean(ProcessService.class);
    }

    public ListDtos<KeyElement> getProcessActions(final String documentId) throws Exception {
        List<KeyElement> results = null;
            results = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
                @Override
                public List<KeyElement> call() throws Exception {
                    return processService.getProcessActions(documentId);
                }
            });

        return new ListDtos<KeyElement>(results);
    }

    public boolean executeProcessAction(final String documentId, final String processAction) throws Exception {
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                processService.executeProcessAction(documentId, processAction);
                return null;
            }
        });

        return true;
    }

    public boolean assignMultiManualAssignment(@WebParam(name = "assigns") final ManualAssignmentDto[] assigns,@WebParam(name = "documentId") final String documentId) throws Exception {
        DSApiUtils.inWSContext(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                processService.manualMutliAssign(Lists.newArrayList(assigns), documentId);
                return null;
            }
        });
        return true;
    }

    public Long[] getDocumentsForProcess(final String processName) throws Exception {
        List<Long> results = DSApiUtils.inWSContext(new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return processService.getDocumentsForProcess(processName);
            }
        });
        return results.toArray(new Long[results.size()]);
    }

    public KeyElement[] getProcessForDocument(final String documentId) throws Exception{
        List<KeyElement> results = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return processService.getProcessForDocument(documentId);
            }
        });
        return results.toArray(new KeyElement[results.size()]);
    }

    public String getProcessInstanceDiagram(final String processId) throws Exception{
        String s = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return processService.getProcessInstance(processId);
            }
        });
        return s;
    }

    public String getProcessDefinition(final String processName) throws Exception {
        String s = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return processService.getProcessDefinition(processName);
            }
        });
        return s;
    }

    public Boolean loadProcessDefinition (final String base64,final String processName) throws Exception {
        DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return processService.loadProcessDefinition(base64, processName);
            }
        });

        return true;
    }


}
