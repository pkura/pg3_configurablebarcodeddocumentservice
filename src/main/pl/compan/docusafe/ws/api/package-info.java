/**
 * Pakiet zawiera list� klas zwi�zanych z Interfejsem WebService. Dost�pne WebSerwisy: <br/>
 * 
 *  - http://host:port/services/DivisionService?wsdl -us�uga zwi�zana ze struktur� organizacyjna<br/>
 *  - http://host:port/services/UserService?wsdl - us�uga zwiazana z u�ytkownikami<br/>
 *  - http://host:port/services/SubstitutionService?wsdl - us�uga zwi�zana z zast�pstwami<br/>
 *  - http://host:port/services/JasperReportService?wsdl - us�uga zwiazana z raportami jaspera<br/>
 *  - http://host:port/services/OfficeCaseService?wsdl - us�uga zwiazana ze sprawami<br/>
 *  - http://host:port/services/OfficeDocumentService?wsdl - us�uga zwiazana z dokumentami<br/>
 *  - http://host:port/services/WsTaskListService?wsdl - us�uga zwi�zana z lista zadan uzytkownikow<br/>
 *  - http://host:port/services/WSXesService?wsdl - us�uga zwiazana z logami audytowymi XES<br/>
 *  - http://host:port/services/WSGraphMLService?wsdl - us�uga zwiazana z logami audytowymi GraphML<br/>
 *  - http://host:port/services/WSDotService?wsdl - us�uga zwiazana z logami audytowymi DOT<br/>
 *  - http://host:port/services/ConverterService?wsdl - us�uga zwiazana z konwerterem plik�w<br/>
 *
 *  Przyk�ady �ada� do poszczeg�lnych plik�w mo�emy znale�� w katalogu <code><b>soapui-projects</b></code>
 *  s� to pliki projekt�w z aplikacji SoapUI.
 */
package pl.compan.docusafe.ws.api;