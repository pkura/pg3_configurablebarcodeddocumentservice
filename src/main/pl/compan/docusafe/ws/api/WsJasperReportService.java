package pl.compan.docusafe.ws.api;

import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.rest.RestJasperReportService;
import pl.compan.docusafe.spring.user.office.JasperReportServiceImpl;
import pl.compan.docusafe.util.DSApiUtils;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestJasperReportService
 */
public class WsJasperReportService {
    final JasperReportServiceImpl reportService;

    public WsJasperReportService() {
        this.reportService = Docusafe.getBean(JasperReportServiceImpl.class);
    }

    public KeyElement[] getReportLists() throws Exception {
        List<KeyElement> o = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return reportService.getReportsList();
            }
        });

       return o.toArray(new KeyElement[o.size()]);
    }

    
    public KeyElement[] getReportType() throws Exception {
        List<KeyElement> o = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return reportService.getReportsType();
            }
        });

        return o.toArray(new KeyElement[o.size()]);
    }

    /**
     *
     * @param jasperReport
     * @return
     * @throws Exception
     */
    public String addReport(final JasperReportDto jasperReport) throws Exception {
        String response = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return reportService.addReport(jasperReport);
            }
        });

        return response;
    }
}
