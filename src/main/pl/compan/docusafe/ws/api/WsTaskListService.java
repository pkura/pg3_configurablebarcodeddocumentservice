package pl.compan.docusafe.ws.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.rest.RestTaskListService;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.TaskListHelper;

import javax.annotation.Nullable;

import java.util.concurrent.Callable;
import java.util.*;

/**
 * @see pl.compan.docusafe.rest.RestTaskListService
 */
public class WsTaskListService {
    private static final Logger log = LoggerFactory.getLogger(WsTaskListService.class);

    private final static Function<Map<String, String>, FieldView[]> MAP_TO_FIELDVIEW = new Function<Map<String, String>, FieldView[]>() {
        @Override
        public @Nullable FieldView[] apply(@Nullable Map<String, String> ds) {
            FieldView[] fv= new FieldView[ds.size()];
            int i =0;
            for(Map.Entry<String, String> d : ds.entrySet()){
                fv[i] = new FieldView(d.getKey(), d.getValue());
                i++;
            }

            return fv;
        }
    };

    public  FieldView[][] getUserTaskList(final String userName, final Integer offset) throws Exception {
        List<Map<String, String>> r =  DSApiUtils.inWSContext(new Callable<List<Map<String, String>>>() {
            @Override
            public List<Map<String, String>> call() throws Exception {
                try {
                    return TaskListHelper.instance().getTasks(userName, offset);
                } catch (Exception e) {
                    log.error("", e);
                    throw e;
                }
            }
        });

        List<FieldView[]> fvs = Lists.transform(r, MAP_TO_FIELDVIEW);

        return toArray(fvs);
    }


    private FieldView[][] toArray(List<FieldView[]> o) {
        return o.toArray(new FieldView[o.size()][]);
    }
}
