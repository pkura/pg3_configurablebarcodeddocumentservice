package pl.compan.docusafe.ws.api;

import com.google.common.base.Optional;

import pl.compan.docusafe.api.user.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.ResourceNotFoundException;
import pl.compan.docusafe.rest.RestDivisionService;
import pl.compan.docusafe.util.DSApiUtils;

import javax.jws.WebParam;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestDivisionService
 */
public class WsDivisionService {
    private final DivisionService divisionService;
    final UserService userService;

    public WsDivisionService(){
        divisionService = Docusafe.getBean(DivisionService.class);
        userService = Docusafe.getBean(UserService.class);
    }

    private DivisionDto getAndCheck(long id) {
        Optional<DivisionDto> optional = divisionService.getById(id);
        if(!optional.isPresent()){
            throw new IllegalArgumentException("division " + id + " not found");
        }
        return optional.get();
    }

    public DivisionDto[] getAllDivisions() throws IllegalArgumentException{
        List<DivisionDto> ret =  DSApiUtils.inWSContext(new Callable<List<DivisionDto>>() {
            @Override
            public List<DivisionDto> call() throws Exception {
                return divisionService.getAllDivisions();
            }
        });
        return ret.toArray(new DivisionDto[ret.size()]);
    }

    public DivisionDto[] getSubdivisions(@WebParam(name = "divisionId") final long id) throws IllegalArgumentException{
        List<DivisionDto> ret =  DSApiUtils.inWSContext(new Callable<List<DivisionDto>>() {
            @Override
            public List<DivisionDto> call() throws Exception {
                return divisionService.getSubdivisions(getAndCheck(id));
            }
        });
        return ret.toArray(new DivisionDto[ret.size()]);
    }

    public DivisionDto getById(@WebParam(name = "divisionId") final long id) throws IllegalArgumentException{
        return DSApiUtils.inWSContext(new Callable<DivisionDto>() {
            @Override
            public DivisionDto call() throws Exception {
                return getAndCheck(id);
            }
        });
    }

    public DivisionDto getByGuid(@WebParam(name = "divisionGuid") final String guid)  throws IllegalArgumentException{
        return DSApiUtils.inWSContext(new Callable<DivisionDto>() {
            @Override
            public DivisionDto call() throws Exception {
                return divisionService.getByGuid(guid).orNull();
            }
        });
    }

    public UserDto[] getUsers(@WebParam(name = "divisionId") final long divisionId) throws IllegalArgumentException{
        List<UserDto> ret = DSApiUtils.inWSContext(new Callable<List<UserDto>>() {
            @Override
            public List<UserDto> call() throws Exception {
                return divisionService.getUsers(getAndCheck(divisionId));
            }
        });
        return ret.toArray(new UserDto[ret.size()]);
    }

    public long createDivision(@WebParam(name = "division") final DivisionDto divisionDto) throws IllegalArgumentException{
        divisionDto.setType(DivisionType.DIVISION);
        return DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return divisionService.create(divisionDto);
            }
        });
    }

    public long createGroup(@WebParam(name = "division") final DivisionDto divisionDto) throws IllegalArgumentException{
        divisionDto.setType(DivisionType.GROUP);
        return DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return divisionService.create(divisionDto);
            }
        });
    }

    public boolean delete(@WebParam(name = "divisionId") final long id) throws IllegalArgumentException{
        DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                divisionService.delete(getAndCheck(id));
                return null;
            }
        });
        return true;
    }

    public boolean update(@WebParam(name = "division") final DivisionDto divisionDto) throws IllegalArgumentException{
        DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                divisionService.update(divisionDto);
                return null;
            }
        });
        return true;
    }

    /**
     * Dodaje u�ytkownika do wybranego dzia�u
     * @param divisionId id dzia�u
     * @param user dane u�ytkownika znajduj�ce si� w RequestBody
     * @param request obiekt requestu wstrzykiwany w metod�
     * @return zwraca uri do dzia�u
     * @throws Exception
     */
    public boolean addUserToDivision(final long divisionId, final UserDto user) throws Exception {
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws EdmException {
                Optional<DivisionDto> division;
                division = divisionService.getById(divisionId);
                if (division.isPresent()) {
                    divisionService.addUser(division.get(), user);
                    return null;
                } else {
                    throw new ResourceNotFoundException("Division '" + divisionId + "' not found");
                }
            }
        });
        return true;
    }

    /**
     * Usuwa u�ytkownika z danego dzia�u
     * @param divisionId id dzia�u do usuni�cia
     * @param userId
     * @throws Exception
     */
     public boolean removeUserFromDivision(final long divisionId, final String userId) throws Exception {
         DSApiUtils.inWSContextTransaction(new Callable<Object>() {
             @Override
             public Object call() throws EdmException {
                 Optional<DivisionDto> division = divisionService.getById(divisionId);
                 Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
                 if (division.isPresent() && user.isPresent()) {
                     return divisionService.removeUser(division.get(), user.get());
                 } else if (user.isPresent()) {
                     throw new IllegalArgumentException("Division '" + divisionId + "' not found");
                 } else {
                     throw new IllegalArgumentException("User '" + userId + "' not found");
                 }
             }
         });

         return true;
    }
}
