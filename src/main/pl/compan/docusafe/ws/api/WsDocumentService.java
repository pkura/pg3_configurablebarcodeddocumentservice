package pl.compan.docusafe.ws.api;

import com.asprise.util.tiff.al;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.RestDocumentService;
import pl.compan.docusafe.rest.RestRequestHandler;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.spring.user.office.DocumentServiceImpl;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestDocumentService
 */
public class WsDocumentService {
    private static final Logger log = LoggerFactory.getLogger(WsDocumentService.class);
    private final DocumentService documentService;

    public WsDocumentService() {
        this.documentService = Docusafe.getBean(DocumentService.class);
    }

    public Boolean validateBarcode(final String algorithm, final String barcode) throws EdmException{
        return DSApiUtils.inWSContext(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return documentService.validateBarcode(algorithm, barcode);
            }
        });
    }

    public Long[] findByBarcode(final String barcode, final String check) throws EdmException {
        if(StringUtils.isNotEmpty(check)){
            if(!documentService.validateBarcode("PG",barcode)){
                throw new EdmException("Niepoprawny barkod!");
            }
        }
        return toArrayLong(DSApiUtils.inWSContext(new Callable<List<Long>>() {
            @Override
            public List<Long> call() throws Exception {
                return documentService.findByBarcode(barcode);
            }
        }));
    }

    public OfficeDocumentDto[] searchDocuments(final OfficeDocumentDto document) throws EdmException{
        return toArrayOfficeDocumentDto(DSApiUtils.inWSContext(new Callable<List<OfficeDocumentDto>>() {
            @Override
            public List<OfficeDocumentDto> call() throws Exception {
                return documentService.searchDocuments(document);
            }
        }));
    }

    public FieldView[] getDocumentDictionary( final String documentId,
                                             final String dictionary,
                                             final Long id) throws EdmException  {
        return toArrayFieldView(DSApiUtils.inWSContext(new Callable<List<FieldView>>() {
            @Override
            public List<FieldView> call() throws Exception {
                return documentService.getDocumentDictionaryObject(documentId, dictionary, id);
            }
        }));
    }

    public String getDocumentAsXml(final String documentId) throws EdmException {
        return DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return documentService.getDocumentAsXml(documentId);
            }
        });
    }

    public DictionaryDto[] searchDictionary(final DictionaryDto dictionaryDtos) throws EdmException{
        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues()))
            return new DictionaryDto[0];

        List<DictionaryDto> fields = DSApiUtils.inWSContext(new Callable<List<DictionaryDto>>() {
            @Override
            public List<DictionaryDto> call() throws Exception {
                return documentService.searchDictionary(dictionaryDtos);
            }
        });

        return toArrayDictionaryDto(fields);
    }

    public DictionaryDto[] addDictionary(final DictionaryDto dictionaryDtos) throws EdmException{
        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues()))
            return new DictionaryDto[0];

        List<DictionaryDto> fields = DSApiUtils.inWSContextTransaction(new Callable<List<DictionaryDto>>() {
            @Override
            public List<DictionaryDto> call() throws Exception {
                return documentService.add(dictionaryDtos);
            }
        });

        return toArrayDictionaryDto(fields);
    }

    public Boolean removeDictionary(final DictionaryDto dictionaryDtos) throws EdmException{
        boolean b = DSApiUtils.inWSContextTransaction(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return documentService.remove(dictionaryDtos);
            }
        });
        return b;
    }

    public Boolean updateDictionary(final DictionaryDto dictionaryDtos) throws EdmException{
        if(CollectionUtils.isEmpty(dictionaryDtos.getFieldValues()))
            throw new EdmException("Bad request");

        Boolean fields = DSApiUtils.inWSContextTransaction(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return documentService.update(dictionaryDtos);
            }
        });

        return fields;
    }


    public RichDocumentView[] searchFullTextDocument(final FullTextSearchDto text) throws Exception {
        List<RichDocumentView> b = DSApiUtils.inWSContext(new Callable<List<RichDocumentView>>() {
            @Override
            public List<RichDocumentView> call() throws Exception {
                return documentService.fullTextSearchDocument(text);
            }
        });

       return toArrayRichDocumentView(b);
    }


    public WorkHistoryBean[] getAudit(final String documentId) throws Exception {
        List<WorkHistoryBean> results = DSApiUtils.inWSContext(new Callable<List<WorkHistoryBean>>() {
            @Override
            public List<WorkHistoryBean> call() throws Exception {
                return documentService.getAudit(documentId);
            }
        });

        return toArrayWorkHistoryBean(results);
    }


    public boolean watch(final String documentId) throws Exception{
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                documentService.watch(documentId);
                return null;
            }
        });

        return true;
    }

    public boolean unwatch(final String documentId) throws Exception{
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                documentService.unwatch(documentId);
                return null;
            }
        });

        return true;
    }

    private WorkHistoryBean[] toArrayWorkHistoryBean(List<WorkHistoryBean> o) {
        return o.toArray(new WorkHistoryBean[o.size()]);
    }

    private RichDocumentView[] toArrayRichDocumentView(List<RichDocumentView> b) {
        return b.toArray(new RichDocumentView[b.size()]);
    }

    private Long[] toArrayLong(List<Long> o) {
        return o.toArray(new Long[o.size()]);
    }

    private OfficeDocumentDto[] toArrayOfficeDocumentDto(List<OfficeDocumentDto> o){
        return o.toArray(new OfficeDocumentDto[o.size()]);
    }

    private DictionaryDto[] toArrayDictionaryDto(List<DictionaryDto> o){
        return o.toArray(new DictionaryDto[o.size()]);
    }

    private FieldView[] toArrayFieldView(List<FieldView> o){
        return o.toArray(new FieldView[o.size()]);
    }
}
