package pl.compan.docusafe.ws.api;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.rest.RestOfficeCaseService;
import pl.compan.docusafe.rest.RestRequestHandler;
import pl.compan.docusafe.util.DSApiUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestOfficeCaseService
 */
public class WsOfficeCaseService {

  private final OfficeCaseService officeCaseService;

    public WsOfficeCaseService() {
        this.officeCaseService = Docusafe.getBean(OfficeCaseService.class);
    }


    public  OfficeCaseDto[] getUserOfficeCase(final String username)
    {
        List<OfficeCaseDto> o = DSApiUtils.inWSContext(new Callable<List<OfficeCaseDto>>() {
            @Override
            public List<OfficeCaseDto> call() throws Exception {
                return officeCaseService.getUserOfficeCase(username);
            }
        });

        return toArray(o);
    }

    public OfficeCaseDto[] getDivisionOfficeCase(final String guid)
    {
        List<OfficeCaseDto> o = DSApiUtils.inWSContext(new Callable<List<OfficeCaseDto>>() {
            @Override
            public List<OfficeCaseDto> call() throws EdmException {
                return officeCaseService.getDivisionOfficeCase(guid);
            }
        });

        return toArray(o);
    }

    public AssociativeDocumentStore.AssociativeDocumentBean[] getDocumentOfficeCase(final String documentId)
    {
        List<AssociativeDocumentStore.AssociativeDocumentBean> a = DSApiUtils.inWSContext(new Callable<List<AssociativeDocumentStore.AssociativeDocumentBean>>() {
            @Override
            public List<AssociativeDocumentStore.AssociativeDocumentBean> call() throws EdmException {
                return officeCaseService.getDocumentOfficeCase(documentId);
            }
        });

        return a.toArray(new AssociativeDocumentStore.AssociativeDocumentBean[a.size()]);
    }

    public OfficeCaseDto[] getOfficeCaseToOfficeCase( final Long officeCaseId) {
        return toArray(DSApiUtils.inWSContext(new Callable<List<OfficeCaseDto>>() {
            @Override
            public List<OfficeCaseDto> call() throws EdmException {
                return officeCaseService.getOfficeCaseToOfficeCase(officeCaseId);
            }
        }));
    }

    public String createOfficeCase( final OfficeCaseDto officeCaseDto)
    {
        return DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws EdmException {
                boolean ok = officeCaseService.createOfficeCase(officeCaseDto);
                if (!ok)
                    throw new IllegalArgumentException("Nie uda�o si� utworzy� sprawy!");

                return officeCaseDto.getId().toString();
            }
        });
    }


    public String addDocumentToOfficeCase( final String documentId, final Long officeCaseId)
    {
        return DSApiUtils.inWSContextTransaction(new Callable<String>() {
            @Override
            public String call() throws EdmException {
                try{
                    String msg = officeCaseService.addDocumentToOfficeCase(documentId, officeCaseId, true);

                    return msg;
                }catch(Exception e){
                    throw new EdmException(e);
                }
            }
        });
    }


    public  String removeDocumentFromCase( final String documentId, final Long officeCaseId)
    {
        return DSApiUtils.inWSContextTransaction(new Callable<String>() {
            @Override
            public String call() throws EdmException {
                String msg = officeCaseService.removeDocumentFromOfficeCase(documentId, officeCaseId);
                return msg;
            }
        });
    }


    public OfficeCaseDto getOfficeCase(final String officeCaseId)
    {
        return DSApiUtils.inWSContext(new Callable<OfficeCaseDto>() {
            @Override
            public OfficeCaseDto call() throws Exception {
                return officeCaseService.getOfficeCase(officeCaseId);
            }
        });
    }

    public CasePriority[] getCasePriority(){
        return toArray(DSApiUtils.inWSContext(new Callable<List<CasePriority>>() {
            @Override
            public List<CasePriority> call() throws Exception {
                return CasePriority.list();
            }
        }));
    }

    public OfficeFolderDto[] getPortfolio(final String guid) throws Exception {
        List<OfficeFolderDto> folders =  DSApiUtils.inWSContext(new Callable<List<OfficeFolderDto>>() {
            @Override
            public List<OfficeFolderDto> call() throws Exception {
                //@TODO czy potrzebne jest dodanie filtrowania po roku tak jak jest na
                //@see PortfolioBaseAction
                return officeCaseService.getPortfolio(guid);
            }
        });

        return folders.toArray(new OfficeFolderDto[folders.size()]);
    }

    public AssociativeDocumentStore.AssociativeDocumentBean[] getDocuments(final String officeCaseId) throws Exception{
        List<AssociativeDocumentStore.AssociativeDocumentBean> list = DSApiUtils.inWSContext(new Callable<List<AssociativeDocumentStore.AssociativeDocumentBean>>() {
            @Override
            public List<AssociativeDocumentStore.AssociativeDocumentBean> call() throws Exception {
                return officeCaseService.getDocuments(officeCaseId);
            }
        });

        return list.toArray(new AssociativeDocumentStore.AssociativeDocumentBean[list.size()]);
    }


    public boolean addOfficeCaseToOfficeCase(final String officeCaseId,final String officeCaseAddId) throws Exception {
        DSApiUtils.inWSContext(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                officeCaseService.addOfficeCaseToOfficeCase(officeCaseId, officeCaseAddId);
                return null;
            }
        });

        return true;
    }

    public boolean removeOfficeCaseToOfficeCase(final String officeCaseId, final String officeCaseRemoveId) throws Exception {
        DSApiUtils.inWSContext(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                officeCaseService.removeOfficeCaseToOfficeCase(officeCaseId, officeCaseRemoveId);
                return null;
            }
        });

        return true;
    }

    public boolean update(final OfficeCaseDto officeCaseDto) throws Exception {
        Long id = DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws EdmException {
                String msg = officeCaseService.updateOfficeCase(officeCaseDto);
                return officeCaseDto.getId();
            }
        });
        return true;
    }

    public boolean delete(final KeyElement officeCaseDto) throws Exception {
        String msg = DSApiUtils.inWSContextTransaction(new Callable<String>() {
            @Override
            public String call() throws EdmException {
                String msg = officeCaseService.deleteOfficeCase(officeCaseDto.getId(), officeCaseDto.getValue());
                return msg;
            }
        });
        if(StringUtils.isNotBlank(msg)){
            throw new EdmException(msg);
        }
        return true;
    }

    public WorkHistoryBean[] getAudit(final String id) throws Exception {
        List<WorkHistoryBean> results = DSApiUtils.inWSContext(new Callable<List<WorkHistoryBean>>() {
            @Override
            public List<WorkHistoryBean> call() throws Exception {
                return officeCaseService.getCaseAudit(id);
            }
        });

        return toArray(results);
    }

    private WorkHistoryBean[] toArray(List<WorkHistoryBean> o) {
        return o.toArray(new WorkHistoryBean[o.size()]);
    }

    private CasePriority[] toArray(List<CasePriority> o) {
        return o.toArray(new CasePriority[o.size()]);
    }

    private OfficeCaseDto[] toArray(List<OfficeCaseDto> o) {
        return o.toArray(new OfficeCaseDto[o.size()]);
    }

}
