package pl.compan.docusafe.export;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;

public abstract class AbstractExportKind implements ExportKind {
	public static final String EMPTY  = "NULL"; 
	
	public boolean isNumberTheRecords() {
		return true;
	}
	
	public boolean addHeader() {
		return false;
	}
	
	public String getHeader() {
		return "";
	}
	
	public boolean addFooter() {
		return false;
	}
	
	public String getFooter() {
		return "";
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.export.ExportKind#canExport(pl.compan.docusafe.core.dockinds.DocumentKind, java.lang.Long)
	 */
	public boolean canExport(DocumentKind dk, Long id)
		throws EdmException
	{
		return true;
	}
	
	/**
	 * Metoda pobiera warto�� z p�l dockindowych typu Class (s�owniki).
	 * @param f
	 * @param o
	 * @return
	 */
	protected String getValue(Field f, Object o) {
		if (o != null) {
			if (f.getType().equals("class") && o instanceof AbstractDocumentKindDictionary) {
					AbstractDocumentKindDictionary a = (AbstractDocumentKindDictionary)o;
					return a.getDictionaryDescription();
			} else
				return o.toString();
		} else
			return EMPTY;
	}
}
