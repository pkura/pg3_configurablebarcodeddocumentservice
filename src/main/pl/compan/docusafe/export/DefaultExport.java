package pl.compan.docusafe.export;

import java.util.List;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.events.handlers.ExportDocuments;

public class DefaultExport extends AbstractExportKind {

	public String getRecord(DocumentKind dk, Long docId) throws Exception {
		FieldsManager fm = dk.getFieldsManager(docId);
		StringBuffer sb = new StringBuffer();
		List<Field> fieldsList = fm.getFields();
		for (Field f : fieldsList) {
			Object value = fm.getValue(f.getCn());
			sb.append(getValue(f, value));
			sb.append(ExportDocuments.SEPARATOR);
		}
		return sb.toString();
	}


}
