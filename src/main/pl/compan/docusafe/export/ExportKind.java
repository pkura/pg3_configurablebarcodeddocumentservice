package pl.compan.docusafe.export;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;


public interface ExportKind {
	/**
	 * Metoda zwraca odpowiedni rekord (wiersz danych) do exportu.
	 * @param dk - odpowiedni {@link DocumentKind}}
	 * @return
	 * @throws Exception 
	 */
	public String getRecord(DocumentKind dk, Long docId) throws Exception;
	/**
	 * Metoda okre�la czy dodawa� nag��wek do pliku.
	 * @return
	 */
	public boolean addHeader();
	/**
	 * Metoda okre�la czy dodawa� stopk�.
	 * @return
	 */
	public boolean addFooter();
	/**
	 * Metoda zwraca nag��wek.
	 * @return
	 */
	public String getHeader();
	/**
	 * Metoda zwraca stopk�.
	 * @return
	 */
	public String getFooter();
	/**
	 * Metoda okre�la czy dodawa� liczb� porz�dkow� na pocz�tku wiersza (rekordu).
	 * @return
	 */
	public boolean isNumberTheRecords();
	/**
	 * @param dk
	 * @param id
	 * @return
	 */
	public boolean canExport(DocumentKind dk, Long id) throws EdmException;
}