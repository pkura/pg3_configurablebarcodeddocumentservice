package pl.compan.docusafe.web;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.viewserver.ViewServer;

/**
 * @author Mariusz Kilja�czyk
 */
public class TiffyAppletService extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(TiffyAppletService.class);
    private static Map<String,Long> revisions;
    private static Map<String,Integer> pagesCount;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
    	Long revisionId = null;
    	String uri = req.getRequestURI();
    	String key;
    	boolean isXml = false;
    	if(uri.endsWith(".tiffy"))
    	{
    		key = getRevisionFromImgLink(uri);
    		revisionId = TiffyAppletService.revisions.get(key);
    	}
    	else if(uri.endsWith(".xml"))
    	{
    		key = getRevisionFromXmlLink(uri);
    		isXml  = true;
    		revisionId = TiffyAppletService.revisions.get(key);
    	//trzeba sie zastanowic jak to czyscic i kiedy 
    	//	TiffyAppletService.revisions.remove(revisionId);
    	}
    	else
    	{
    		return;
    	}

        try 
        {
			DSApi.openAdmin();
			if(isXml)
			{
				OutputStream out = resp.getOutputStream();
				AttachmentRevision ar = AttachmentRevision.find(revisionId);
				String anno = ar.getAnnotations();
				if(anno == null)
				{
					anno = createEmptyAnno(pagesCount.get(key));
				}
				
	            ByteArrayInputStream str = new ByteArrayInputStream(anno.getBytes());
	            byte[] buf = new byte[1024];
	            int len;
	            while ((len = str.read(buf)) > 0)
	            {
	                out.write(buf, 0, len);
	            }
	            out.close();
	            str.close();
			}
			else
			{
				AttachmentRevision revision = AttachmentRevision.find(revisionId);
				//File file = ViewServer.getFile(revision);
			
		        InputStream is = revision.getAttachmentStream();
		        //is = new FileInputStream(file);
		        OutputStream os = resp.getOutputStream();
	
		        org.apache.commons.io.IOUtils.copy(is, os);		        
		        org.apache.commons.io.IOUtils.closeQuietly(os);
		        org.apache.commons.io.IOUtils.closeQuietly(is);
			}
		}
        catch (EdmException e) 
        {
			log.error("",e);
		}
        finally
        {
        	DSApi._close();
        }
    }
    
    /***
     * Tworzy pusty xml, brak pustego xml powodowal bledy appletu podczas dodawania uwag
     */
    private String createEmptyAnno(Integer i) 
    {
    	if(pagesCount == null)
    		return "<ANNOTATIONS><PAGE><PAGE/></ANNOTATIONS>";
    	StringBuilder sb = new StringBuilder();
    	sb.append("<ANNOTATIONS>");
		for (int j = 0; j < i; j++) 
		{
			sb.append("<PAGE></PAGE>");
		}
		sb.append("</ANNOTATIONS>");
		return sb.toString();
	}

	public String getRevisionFromXmlLink(String path)
    {
    	String[] tmpTab = path.split("/");
        String key = tmpTab[tmpTab.length - 1];
        key = key.substring(0,key.length()-4);
    	return key;
    }
    
    public String getRevisionFromImgLink(String path)
    {
        String[] tmpTab = path.split("/");
        String key = tmpTab[tmpTab.length - 1];
        key = key.substring(0,key.length()-6);
    	return key;
    }

	public static void setRevisions(Map<String,Long> revisions) 
	{
		TiffyAppletService.revisions = revisions;
	}
	
	public static void addRevision(String key,Long id)
	{
		if(TiffyAppletService.revisions == null)
			TiffyAppletService.revisions = new HashMap<String, Long>();
		TiffyAppletService.revisions.put(key, id);
	}
	
	public static void addPageCount(String key,Integer count)
	{
		if(TiffyAppletService.pagesCount == null)
			TiffyAppletService.pagesCount = new HashMap<String, Integer>();
		TiffyAppletService.pagesCount.put(key, count);
	}

	public static Map<String,Long> getRevisions() 
	{
		return revisions;
	}
}