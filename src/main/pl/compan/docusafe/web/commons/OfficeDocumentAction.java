package pl.compan.docusafe.web.commons;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Interfejs ktory jest zwiazany z akcja procesuja dokument
 * @author wkutyla
 *
 */
public interface OfficeDocumentAction 
{
	/**
	 * Pobieramy dokument ID
	 * @return
	 */
	public Long getDocumentId();
	/**
	 * Pobieramy dokument
	 * @return
	 */
	public Document getDocument();
	/**
	 * ustawiamy document
	 * @param document
	 */
	public void setDocument (OfficeDocument document);
}
