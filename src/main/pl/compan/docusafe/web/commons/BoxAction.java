package pl.compan.docusafe.web.commons;

import pl.compan.docusafe.core.base.Document;

/**
 * Interfejs reprezentowany przez akcje ktora:
 * - obsuguje nadawanie pudla archiwalnego
 * @author Mariusz Kilja�czyk
 *
 */
public interface BoxAction
{
    public boolean isBoxActionAvailable();

    public boolean isNeedsNotBox();
    
    public void setNeedsNotBox(Boolean isneed);

    public boolean isBoxNumberReadonly() ;

    public Long getBoxId() ;

    public void setBoxId(Long boxId);

    public String getBoxNumber() ;

    public void setBoxNumber(String boxNumber);

    public Long getCurrentBoxId();
    
    public void setCurrentBoxId(Long boxId);

    public String getCurrentBoxNumber();
    
    public void setCurrentBoxNumber(String currentBoxNumber);
    
    public Document getDocument();
    
    public void addActionError(String arg0);
    
    public void addActionMessage(String arg0);
    
	public void setBoxNumberReadonly(boolean boxNumberReadonly);

	public boolean isPudloModify();

	public void setPudloModify(boolean pudloModify);
}
