package pl.compan.docusafe.web.commons;

import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.web.office.common.RemarksTabAction.RemarkBean;
import java.util.*;
/**
 * Interfejs reprezentowany przez akcje ktora:
 * - jest zwiazana z dokumentem
 * - wyswietla uwagi z nim zwiazane.
 * @author wkutyla
 *
 */
public interface RemarkableAction extends OfficeDocumentAction
{
	/**
	 * Pobieranie uwag
	 * @return
	 */
	public List<RemarkBean> getRemarks();
	/**
	 * Ustawiamy kolekcje z uwagami
	 * @param remarks
	 */
	public void setRemarks(List<RemarkBean> remarks);
		
	/**
	 * Ustawiamy ostatnia uwage
	 * @param remark
	 */
	public void setLastRemark(Remark remark);	
}
