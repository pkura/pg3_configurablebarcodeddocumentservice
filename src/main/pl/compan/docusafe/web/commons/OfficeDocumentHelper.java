package pl.compan.docusafe.web.commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.logic.DocumentAttachmentFilterLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.RemarksTabAction.RemarkBean;

import com.google.common.base.Preconditions;

/**
 * Klasa pomocnicza, ktora wykonuje wspolne operacje zwiazane z dostepem do obiektu officeDocument
 * @author wkutyla
 *
 */
public class OfficeDocumentHelper 
{
	static final Logger log = LoggerFactory.getLogger(OfficeDocumentHelper.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
	
	/**
	 * Metoda pobiera uwagi dla dokumentu, sortuje je 
	 * @param remarkableAction
	 * @throws EdmException
	 */
	public void retrieveRemarks(RemarkableAction remarkableAction) throws EdmException
	{	
		log.trace("retrieveRemarks documentId={}",remarkableAction.getDocumentId());
		Document document = remarkableAction.getDocument();
		if (document == null){
			log.error("document == null");
			return;
		}
		if (remarkableAction.getDocument().getRemarks() != null){
            List<RemarkBean> remarks = new ArrayList<RemarkBean>(document.getRemarks().size());
            List<Remark> remarksCopy = new ArrayList<Remark>(document.getRemarks());
            Collections.reverse(remarksCopy);
            int posn = document.getRemarks().size() - 1;
            for (Remark aRemarksCopy : remarksCopy) {
                remarks.add(new RemarkBean(aRemarksCopy, posn--));
            }
            remarkableAction.setRemarks(remarks);
            //TODO - potrzeba jeszcze ustawic ustatnia uwage
        }
	}
	/**
	 * Metoda inicjude dane pudla
	 * @param boxAction
	 * @throws EdmException
	 */
	public void initBox(BoxAction boxAction) throws EdmException
	{
		if(boxAction.getDocument().getDocumentKind() == null)
			return;
		boxAction.setBoxNumberReadonly(!DSApi.context().isAdmin() && AvailabilityManager.isAvailable("boxnumber.readonly"));
        boxAction.setPudloModify(AvailabilityManager.isAvailable("boxnumber.normal") || DSApi.context().isAdmin());
        
		if(boxAction.getDocument() == null)
			throw new DSException("Brak dokuemtu");
		
        Long boxId = boxAction.getDocument().getDocumentKind().logic().getBoxLine(boxAction.getDocument());
        if (boxId == null)
        {
            /**pud�o z linii domy�lnej*/
            if (GlobalPreferences.isBoxOpen())
            {
                boxId = GlobalPreferences.getBoxId();                       
            }
            else
                boxId = null;
        }
        
        if (boxId != null)
        {
            try
            {
                Box box = Box.find(boxId);
                boxAction.setCurrentBoxNumber(box.getName());
                boxAction.setCurrentBoxId(box.getId());
            }
            catch (EntityNotFoundException e)
            {
            }
        }                
        
        if (boxAction.getDocument().getBox() != null)
        {
        	boxAction.setBoxNumber(boxAction.getDocument().getBox().getName());
        	boxAction.setBoxId(boxAction.getDocument().getBox().getId());
        }
        else if (boxAction.getDocument().hasWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX))
        {
        	boxAction.setNeedsNotBox(true);
        	boxAction.setBoxNumber("x");
        }
	}

	public void updateBox(BoxAction boxAction)
	{
        Preconditions.checkNotNull(boxAction.getDocument(), "document cannot be null");
        if(!boxAction.isBoxActionAvailable()){
            return;
        }
		boxAction.setBoxNumber(TextUtils.trimmedStringOrNull(boxAction.getBoxNumber()));
        try {
        	/**Jesli nie widac pudla to go nie edytujemy*/
            if ("x".equalsIgnoreCase(boxAction.getBoxNumber()))
            {
                boxAction.getDocument().setBox(null);
                boxAction.getDocument().addWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
            } else if (boxAction.getBoxNumber() != null)
            {
                boxAction.getDocument().clearWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                //BoxLine boxline = boxAction.getDocument().getDocumentKind().getBoxLine(boxAction.getDocument());
                //String lineCN = null;
                //if(boxline != null)
                //    lineCN = boxline.getCn();
                
                String lineCN = boxAction.getDocument().getDocumentKind().logic().getBoxLineCn(boxAction.getDocument());
                
                try
                {
                	boxAction.getDocument().setBox(Box.findByName(lineCN,boxAction.getBoxNumber()));
                }catch(EntityNotFoundException e){
                	if(!AvailabilityManager.isAvailable("boxnumber.autocreate"))
                		throw e;
                	else
                	{
                		Box box = new Box(boxAction.getBoxNumber());
                		box.setLine(lineCN);
                		box.create();
                		boxAction.getDocument().setBox(box);
                	}
                }
            }
            else if (boxAction.getBoxId() != null)
            {
                boxAction.getDocument().clearWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                boxAction.getDocument().setBox(Box.find(boxAction.getBoxId()));
            }
            else if (!boxAction.getDocument().getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND))
            {
                boxAction.getDocument().setBox(null);
            }
        }
        catch (EdmException e) {
            boxAction.addActionError(e.getMessage());
            log.error(e.getMessage(), e);
        }
	}

    /**
     * Filtruje za��czniki w/g logiki dokumentu
     * @param document
     * @param attachmentList
     * @return
     * @throws EdmException
     */
    public static List<Attachment> filterAttachments(Document document, List<Attachment> attachmentList) throws EdmException {
        Preconditions.checkNotNull(document);
        DocumentLogic logic = document.getDocumentKind().logic();

        if (logic != null && logic instanceof DocumentAttachmentFilterLogic) {
            attachmentList = ((DocumentAttachmentFilterLogic) document.getDocumentKind().logic()).filterAttachments(document, attachmentList);
        } else {
            attachmentList = document.getAttachments();
        }

        return attachmentList;
    }
}
