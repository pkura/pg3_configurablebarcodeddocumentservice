package pl.compan.docusafe.web.commons;

import java.util.Map;

import pl.compan.docusafe.core.base.Document;
/**
 * Interfejs reprezentowany przez akcje ktora:
 * - obsuguje akcje dockindButton
 * @author Mariusz Kilja�czyk
 *
 */
public interface DockindButtonAction
{
	/**Przekazuje cn wywoanej akcji*/
	public String getDockindEventValue();
	
	public  void setDockindEventValue(String dockindEventValue);
    
    /**Zwraca miejsce w jakim sie znajdujemy (tasklist,documentArchive)*/
    public String getPlace();    
}
