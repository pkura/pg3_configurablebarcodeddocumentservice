package pl.compan.docusafe.web.commons;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.*;

/**
 *
 * @author Micha� Sankowski
 */
public class AcceptanceView extends EventActionSupport{
	private final static Logger LOG = LoggerFactory.getLogger(AcceptanceView.class);

	private Long documentId;
	private FieldsManager fm;

	protected void setup() {
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			Document doc = Document.find(documentId);
			if(!doc.canRead()){
				//zwyk�y u�ytkownik nie powinien si� tu znale��
				log.warn("nieuprawniony dost�p (documentId=" + documentId +")");
				return;
			}
			setFm(doc.getFieldsManager());
			getFm().initialize();
			getFm().initializeAcceptances();
			log.info("acceptance-view : documentId={}, fm={}, fm.acceptanceDefinition={}",documentId, getFm(),getFm().getAcceptancesDefinition());
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public FieldsManager getFm() {
		return fm;
	}

	public void setFm(FieldsManager fm) {
		this.fm = fm;
	}
}
