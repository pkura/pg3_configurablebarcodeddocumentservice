package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.*;
import java.util.Collection;

/**
 * PartView sk�adaj�cy si� z kilku partview'ow
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CompositePartView extends DefaultPartView{

	private final PartView[] views;

	public CompositePartView(PartView... views){
		this.views = views;
	}

	public CompositePartView(Collection<PartView> views){
		this.views = views.toArray(new PartView[views.size()]);
	}

	@Override
	public void beforePart(RenderingContext rc) throws Exception {
		for(PartView view: views){
			view.beforePart(rc);
		}
	}

	@Override
	public void afterPart(RenderingContext rc) throws Exception {
		for(PartView view: views){
			view.afterPart(rc);
		}
	}
}
