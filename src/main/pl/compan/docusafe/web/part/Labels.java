package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.RenderingContext;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class Labels{

	private Labels(){
	}

	public static ContentRenderer unescapedLabel(String label){
		return new StringOut(label);
	}

}

class StringOut implements ContentRenderer {
	private final String out;

	public StringOut(String out) {
		this.out = out;
	}

	public void renderContents(RenderingContext renderingContext) throws Exception {
		renderingContext.getContents().append(out);
	}
}