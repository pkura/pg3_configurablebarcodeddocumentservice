package pl.compan.docusafe.web.part;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class AbstractPartView implements PartView{

	public final boolean isRendered() {
		return true;
	}

}
