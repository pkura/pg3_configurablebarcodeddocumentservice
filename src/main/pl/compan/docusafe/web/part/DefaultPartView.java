package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.*;
import pl.compan.docusafe.web.part.AbstractPartView;

/**
 * Domy�lna implementacja PartView, nie zmienia nic w wy�wietlaniu zawarto�ci
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DefaultPartView extends AbstractPartView{

	public final static DefaultPartView INSTANCE = new DefaultPartView();

	protected DefaultPartView(){}

	public void beforePart(RenderingContext rc) throws Exception {
//		rc.getContents().append("<div id=\"" + rc.getPartName() + "_part\" class=\"part\">");
	}

	public void afterPart(RenderingContext rc) throws Exception {
//		rc.getContents().append("</div>");
	}
}
