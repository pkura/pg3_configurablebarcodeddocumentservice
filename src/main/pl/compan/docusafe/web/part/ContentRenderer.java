package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.*;

/**
 * Obiekt renderuj�cy fragment strony
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface ContentRenderer {

	void renderContents(RenderingContext renderingContext) throws Exception;

}
