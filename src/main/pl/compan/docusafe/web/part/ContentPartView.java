package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.RenderingContext;

/**
 * Adapter ContentRenderer -> PartView
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ContentPartView extends DefaultPartView{
	private final ContentRenderer renderer;

	public ContentPartView(ContentRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public void beforePart(RenderingContext rc) throws Exception {
		renderer.renderContents(rc);
	}

	@Override
	public void afterPart(RenderingContext rc) throws Exception {
	}
}
