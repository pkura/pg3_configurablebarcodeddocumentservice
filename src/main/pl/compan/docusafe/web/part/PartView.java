package pl.compan.docusafe.web.part;

import pl.compan.docusafe.core.cfg.part.*;

/**
 * Widok pewnej cz�ci strony. Implementacje musz� by� thread-safe,
 * najlepiej gdyby by�y niezmienne (immutable).
 * Klasy implementuj�ce powinny rozszerza� klas� AbstractPartView.
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface PartView {

	/**
	 * PartView, kt�ry nie wy�wietla zawarto�ci
	 */
	public final static PartView NULL = new NullPartView();
	/**
	 * PartView, kt�ry nie wp�ywa na wy�wietlan� zawarto�� - w wielu przypadkach domy�lne zachowanie
	 */
	//public final static PartView DEFAULT = DefaultPartView.INSTANCE;

	/**
	 * Czy PartView b�dzie wy�wietlany wraz z zawarto�ci�, je�li ma by� false -> nale�y u�y� PartView.NULL
	 * @return
	 */
	boolean isRendered();

	/**
	 * Metoda wp�ywa na PageContext przed zawarto�ci� PartView,
	 * metoda jest wywo�ywana je�li isRendered == true
	 * @param pc
	 */
	void beforePart(RenderingContext renderingContext) throws Exception;
	void afterPart(RenderingContext renderingContext) throws Exception;
}
class NullPartView implements PartView{

	public boolean isRendered() {
		return false;
	}

	public void beforePart(RenderingContext renderingContext) {
		throw new RuntimeException("NullPartView - metoda ta nie powinna by� wywo�ana");
	}

	public void afterPart(RenderingContext renderingContext) {
		throw new RuntimeException("NullPartView - metoda ta nie powinna by� wywo�ana");
	}
}