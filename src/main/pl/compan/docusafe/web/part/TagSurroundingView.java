package pl.compan.docusafe.web.part;

import com.google.common.base.Function;
import pl.compan.docusafe.core.cfg.part.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * PartView, kt�ry otacza zawarto�� pewnym tagiem z okre�lonymi atrybutami
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class TagSurroundingView extends DefaultPartView{
	public final static TagSurroundingView DIV = new DefaultDivSurroundingView();

	private final String tagName;
	private final Map<String, String> attributes;
	private final Map<String, ? extends Function<RenderingContext, String>> dynamicAttributes;

	public TagSurroundingView(String tagName, Map<String, String> attributes, Map<String, ? extends Function<RenderingContext, String>> dynamicAttributes) {
		this.tagName = tagName;
		this.attributes = Collections.unmodifiableMap(attributes);
		this.dynamicAttributes = Collections.unmodifiableMap(dynamicAttributes);
		Set<String> set = new HashSet<String>(attributes.keySet());
		if(set.removeAll(dynamicAttributes.keySet())){
			throw new IllegalArgumentException("zdublowany atrybut w zbiorach:" + attributes.entrySet() + " " + dynamicAttributes.entrySet());
		}
	}

	public TagSurroundingView(String tagName) {
		this.tagName = tagName;
		this.attributes = Collections.emptyMap();
		this.dynamicAttributes = Collections.emptyMap();
	}

	@Override
	public void beforePart(RenderingContext rc) throws Exception {
		rc.getContents().append("<").append(tagName).append(" ");
		for(Entry<String, ? extends Function<RenderingContext, String>> attribute: dynamicAttributes.entrySet()){
			rc.getContents()
				.append(attribute.getKey())
				.append("=\"")
				.append(attribute.getValue().apply(rc))
				.append("\"");
		}
		for(Entry<String, String> attribute: attributes.entrySet()){
			rc.getContents()
				.append(attribute.getKey())
				.append("=\"")
				.append(attribute.getValue())
				.append("\"");
		}
		rc.getContents().append(">");
	}

	@Override
	public void afterPart(RenderingContext rc) throws Exception {
		rc.getContents().append("</").append(tagName).append(">");
	}
}


class DefaultDivSurroundingView extends TagSurroundingView{

	public DefaultDivSurroundingView() {
		super("div", 
			Collections.singletonMap("class", "part"),
			Collections.singletonMap("id", new DivId()));
	}
}

class DivId implements Function<RenderingContext, String>{
	public String apply(RenderingContext from) {
		return from.getPartName()+"_part";
	}
}
