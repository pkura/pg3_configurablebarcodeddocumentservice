package pl.compan.docusafe.web.wsso;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.users.auth.SsoTokenStore;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * User: Tomasz
 * Date: 17.03.14
 * Time: 16:35
 */
public class WssoAction extends EventActionSupport {

    private static Logger LOG = LoggerFactory.getLogger(WssoAction.class);
    private String link;

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            String baseLink = Docusafe.getAdditionProperty("wsso.baselink");
            if(baseLink != null && baseLink.length() > 0 && baseLink.contains("#{tokenKey}")) {
                String tokenKey = SsoTokenStore.instance().generateTokenForCurrentUser();
                link = baseLink.replace("#{tokenKey}",tokenKey);
            } else {
                link = null;
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
