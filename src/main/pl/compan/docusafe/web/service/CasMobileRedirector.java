package pl.compan.docusafe.web.service;

import com.google.gson.JsonElement;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.util.CookieHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.AjaxSerializeActionListener;
import pl.compan.docusafe.webwork.event.RichEventActionSupport;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;

public class CasMobileRedirector extends RichEventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(CasMobileRedirector.class);

    @Override
    protected void setup() {
        registerListener(DEFAULT_ACTION).append(new Magic());
    }

    private class Magic extends AjaxSerializeActionListener {

        @Override
        public Object serializeAction(ActionEvent event) throws Exception {

            String mobileApplicationPath = AdditionManager.getProperty("app.mobile.url");

            if(mobileApplicationPath == null) {
                throw new Exception("app.mobile.url is not defined in configuration");
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpServletResponse response = ServletActionContext.getResponse();

            String jSessionId = CookieHelper.getCookieValueOrError(request, "JSESSIONID");

            String cookiePath = new URL(mobileApplicationPath).getPath();

            response.setStatus(302);
            response.setHeader("Location", mobileApplicationPath);
            response.setHeader("Set-Cookie", "JSESSIONID="+jSessionId+"; Path="+cookiePath);

            return jSessionId;
        }
    }
}
