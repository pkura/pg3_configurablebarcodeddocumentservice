package pl.compan.docusafe.web.service;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.service.stagingarea.RetainedObjectAttachment;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StreamUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewRetainedAttachmentAction.java,v 1.2 2006/02/20 15:42:49 lk
 *          Exp $
 */
public class ViewRetainedAttachmentAction extends EventActionSupport
{
	private Long id;

	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			File file = null;
			String mime = null;
			String filename = null;
			InputStream stream = null;
			try
			{
				RetainedObjectAttachment object = RetainedObjectAttachment.find(id);
				mime = object.getMime();
				filename = object.getFilename();
				stream = object.getInputStream();
				file = StreamUtils.toTempFile(stream);
				stream.close();
			} catch (Exception e)
			{
				event.getLog().error(e.getMessage(), e);
			}

			if (file != null)
			{
				try
				{
					ServletUtils.streamFile(ServletActionContext.getResponse(), file, mime,
							"Content-Disposition: attachment; filename=\"" + filename + "\"");
				} catch (IOException e)
				{
					event.getLog().error(e.getMessage(), e);
				}
			}
		}
	}

	public void setId(Long id)
	{
		this.id = id;
	}
}
