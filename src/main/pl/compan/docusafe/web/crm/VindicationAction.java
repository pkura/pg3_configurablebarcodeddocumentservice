package pl.compan.docusafe.web.crm;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;
/**
 * 
 * @author Mariusz Kilja�czyk
 *
 */

public class VindicationAction extends EventActionSupport{
    
    private List<InOfficeDocumentKind> kinds;
    private boolean canChooseKind;
    private boolean canReadDictionaries;
    private boolean canEdit;
    private DocumentKind documentKind = null;
    private Integer kindId;
    private boolean goToList;
    private String redirectUrl;
    
    // @EXPORT
    private FieldsManager fm;
    private Map<String,String> documentKinds;

    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();

    // @EXPORT/@IMPORT
    protected String documentKindCn;
        
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);
        
        registerListener("doCreate").
            append( new Create()).
            append(fillForm);

       }
    private FormFile file;

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                canReadDictionaries = true;
                canEdit = true;
                fillForm(DocumentLogic.TYPE_OUT_OFFICE); 
                // inicjuje rodzaj pisma przychodz�cego
                kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                canChooseKind = true;
                if (!canChooseKind)
                {    
                    // nie mo�na wybiera� na formatce - rodzaj pisma ustalony                    
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                            kindId = inKind.getId();
                    }
                }                                    
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
    
    protected void fillForm(int documentType) throws EdmException
    {
        canReadDictionaries = true;
        List<DocumentKind> docKinds = DocumentKind.listForCreate();
        documentKinds = new LinkedHashMap<String,String>();
        
        if (documentKindCn == null)
        {
            documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
            documentKindCn = documentKind.getCn();
        }
        else
            documentKind = DocumentKind.findByCn(documentKindCn);

        fm = documentKind.getFieldsManager(null);
        fm.initialize();
        documentKind.logic().setInitialValues(fm, documentType);

        if (hasActionErrors())
            fm.reloadValues(values);   
    }
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                canEdit = true;
                canReadDictionaries = true;
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));   
                DSApi.context().begin();
                InOfficeDocument doc = new InOfficeDocument();
                DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
                doc.setDocumentKind(documentKind);
                doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
                doc.setDivisionGuid(DSDivision.ROOT_GUID);
                doc.setCurrentAssignmentAccepted(Boolean.FALSE);
                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setSummary("Windykacja");
                doc.setForceArchivePermissions(false);
                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.setClerk(DSApi.context().getPrincipalName());
                doc.setSender(new Sender());
                doc.setSource("crm");
                doc.setOriginal(true);
                
                List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                boolean canChooseKind = (kindName == null);
                if (!canChooseKind)
                {                       
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                        	doc.setKind(InOfficeDocumentKind.find(inKind.getId()));
                    }
                }
                else
                {
                	doc.setKind(InOfficeDocumentKind.find(1));            	
                }
                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());
                doc.setIncomingDate(currentDay.getTime());
                doc.setCtime(new Date());
                
                doc.create();
                Journal journal = Journal.getMainIncoming();
                Long journalId;
                journalId = journal.getId();
                Integer sequenceId = null;
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
                
                doc.bindToJournal(journalId, sequenceId);
                
                Long newDocumentId = doc.getId();
                
                doc.setDocumentKind(documentKind);
                newDocumentId = doc.getId();
                
                documentKind.set(newDocumentId, values);
                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                documentKind.logic().documentPermissions(doc);

                WorkflowFactory.createNewProcess(doc, true, "Pismo utworzone");                                            
                event.addActionMessage("Dodano zadanie na liste zada�");

                DSApi.context().commit();
                
                if(goToList)
                {
                    event.setResult("task-list-in");
                }
                else
                {
                    event.setResult("success");
                }
                    
                
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                return;
            }
            catch (Throwable e)
            {
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError("Wyst�pi� b��d ("+e.getMessage()+")");
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
             
        }        
    }

    public boolean isCanChooseKind()
    {
        return canChooseKind;
    }

    public void setCanChooseKind(boolean canChooseKind)
    {
        this.canChooseKind = canChooseKind;
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds()
    {
        return documentKinds;
    }

    public void setDocumentKinds(Map<String, String> documentKinds)
    {
        this.documentKinds = documentKinds;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public List<InOfficeDocumentKind> getKinds()
    {
        return kinds;
    }

    public void setKinds(List<InOfficeDocumentKind> kinds)
    {
        this.kinds = kinds;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public void setFm(FieldsManager fm)
    {
        this.fm = fm;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public void setCanReadDictionaries(boolean canReadDictionaries)
    {
        this.canReadDictionaries = canReadDictionaries;
    }

    public boolean isCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public boolean isGoToList()
    {
        
        return goToList;
    }

    public void setGoToList(boolean goToList)
    {
        this.goToList = goToList;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

}