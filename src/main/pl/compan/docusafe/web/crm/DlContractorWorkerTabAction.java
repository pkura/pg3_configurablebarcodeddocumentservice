package pl.compan.docusafe.web.crm;


public class DlContractorWorkerTabAction extends NewContractorWorkerAction
{
    public String getBaseLink()
    {
        return "/crm/contractor-worker-tab.action";
    }

    public boolean isCrm()
    {
        return false;
    }
    
    public boolean isContractorTab()
    {
    	return true;
    }
}

