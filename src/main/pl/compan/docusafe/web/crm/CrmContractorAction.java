package pl.compan.docusafe.web.crm;

import pl.compan.docusafe.web.office.dictionaries.ContractorBaseAction;

public class CrmContractorAction extends ContractorBaseAction {

	@Override 
	public String getBaseLink() {
		return "/crm/crmContractor.action";
	}

	@Override
	public boolean isCrm() {
		return true;
	}

	@Override
	public boolean isPopup() {
		return false;
	}
}
