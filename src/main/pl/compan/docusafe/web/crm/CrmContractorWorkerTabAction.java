package pl.compan.docusafe.web.crm;


public class CrmContractorWorkerTabAction extends NewContractorWorkerAction
{
    public String getBaseLink()
    {
        return "/crm/new-contractor-worker-tab.action";
    }

    public boolean isCrm()
    {
        return true;
    }
    
    public boolean isContractorTab()
    {
    	return true;
    }
    
}

