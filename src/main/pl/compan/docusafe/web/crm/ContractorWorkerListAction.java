package pl.compan.docusafe.web.crm;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.crm.ContractorWorker.ContractorWorkerQuery;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
/**
 * Akcja wy�wietlaj�ca liste os�b w module CRM.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class ContractorWorkerListAction extends EventActionSupport
{

    public static final String URL = "/crm/contractor-worker-list.action";
    public static final int LIMIT = 20;
    
    //@IMPORT/EXPORT
    private Long   id;
    private String firstname;
    private String lastname;
    private String phoneNumber;
    private String email;

    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private ContractorWorker inst;
    private Pager pager;
    private int offset;
    private String sortDesc;
    private String sortAsc;
    private Boolean ascending;
    private String sortField;
    private List<? extends ContractorWorker> results;
    private boolean dictionaryAction;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(ContractorWorkerListAction.class.getPackage().getName(),null);
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            try
            {
                if(canRead)
                {
                    ContractorWorkerQuery form = new ContractorWorkerQuery();    
                    form.addAsc("firstname");
                    form.setOffset(0);
                    form.setLimit(LIMIT);

                    SearchResults<? extends ContractorWorker> results = ContractorWorker.search(form);
    
                    if (results == null || results.totalCount() == 0)
                    {
                       
                    }
                    else
                    {
                        ContractorWorkerListAction.this.results = fun.list(results);
                        Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                        {
                            public String getLink(int offset)
                            {
                                return HttpUtils.makeUrl(URL, new Object[] 
                                    {
                                        "doSearch", "true",     
                                        "firstname",firstname,                                           
                                        "lastname",lastname,                       
                                        "phoneNumber",phoneNumber,
                                        "email",email,
                                        "offset", String.valueOf(offset)
                                    }
                                );
                            }
                        };
                        pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                        
                        sortAsc = linkVisitor.getLink(offset) + "&ascending=true"; 
                        sortDesc = linkVisitor.getLink(offset) + "&ascending=false"; 
                    }
                }
                else
                    throw new EdmException(sm.getString("BrakUprawnienDoOgladaniaListyOsob"));
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("BrakUprawnienDoUsuwaniaZeSlownika");

                inst = ContractorWorker.getInstance().find(id);
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getFirstname() +" "+inst.getLastname()));
                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    } 
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            try
            {
                ContractorWorkerQuery form = new ContractorWorkerQuery(); 

                if (firstname!= null) form.setFirstname(firstname);
                if (lastname!= null) form.setLastname(lastname);
                if (phoneNumber!= null) form.setPhoneNumber(phoneNumber);
                if (email!= null) form.setPhoneNumber(email);

                form.setLimit(LIMIT);
                form.setOffset(offset);

                if (ascending != null)
                    if(ascending)
                        form.addAsc(sortField);
                    else
                        form.addDesc(sortField);
                else
                    form.addAsc("firstname");

                SearchResults<? extends ContractorWorker> results = ContractorWorker.search(form);

                if (results == null || results.totalCount() == 0)
                {
                    throw new EdmException(sm.getString("NieZnalezionoOsobPasujacychDoWpisanychDanych"));
                }
                ContractorWorkerListAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                            "doSearch", "true",     
                            "firstname",firstname,                                           
                            "lastname",lastname,                       
                            "phoneNumber",phoneNumber,
                            "email",email,
                            "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);

                sortAsc = linkVisitor.getLink(offset) + "&ascending=true"; 
                sortDesc = linkVisitor.getLink(offset) + "&ascending=false"; 
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }

    public Boolean getAscending()
    {
        return ascending;
    }

    public void setAscending(Boolean ascending)
    {
        this.ascending = ascending;
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public List<? extends ContractorWorker> getResults()
    {
        return results;
    }

    public void setResults(List<? extends ContractorWorker> results)
    {
        this.results = results;
    }

    public String getSortAsc()
    {
        return sortAsc;
    }

    public void setSortAsc(String sortAsc)
    {
        this.sortAsc = sortAsc;
    }

    public String getSortDesc()
    {
        return sortDesc;
    }

    public void setSortDesc(String sortDesc)
    {
        this.sortDesc = sortDesc;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean isDictionaryAction()
    {
        return dictionaryAction;
    }

    public void setDictionaryAction(boolean dictionaryAction)
    {
        this.dictionaryAction = dictionaryAction;
    }
}
