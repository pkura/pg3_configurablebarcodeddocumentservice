/*
 */

package pl.compan.docusafe.web.crm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.ilpol.CrmVindicationLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author Piter Boss
 */
public class ContractorImportAction extends EventActionSupport
{	
	private StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
	protected final Log log = LogFactory.getLog(getClass());
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doImport").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

       }
    private FormFile file;
    private List<AssignmentObjective> objectives;
    private String objectiveSel;
    private String objectiveTxt;

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try 
        	{
        		//DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
				objectives = AssignmentObjective.list();
				 if (objectives.size() > 0)
		                objectiveSel = ((AssignmentObjective) objectives.get(0)).getName();
			}
        	catch (EdmException e) 
        	{
        		log.error(e.getMessage());
			}
        	finally
            {
            	try {DSApi.close();} catch (EdmException e1) {LogFactory.getLog("eprint").debug("", e1);}
            }
           
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
            if(file==null)
            {
                addActionError(sm.getString("BrakPliku"));
                return;
            }
           
        }
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public List getObjectives() 
    {
		return objectives;
	}

	public void setObjectives(List objectives) 
	{
		this.objectives = objectives;
	}

	public String getObjectiveSel() {
		return objectiveSel;
	}

	public void setObjectiveSel(String objectiveSel) {
		this.objectiveSel = objectiveSel;
	}

	public String getObjectiveTxt() {
		return objectiveTxt;
	}

	public void setObjectiveTxt(String objectiveTxt) {
		this.objectiveTxt = objectiveTxt;
	}
}