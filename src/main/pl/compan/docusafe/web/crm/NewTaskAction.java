/*
 */
package pl.compan.docusafe.web.crm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author <a href="mailto:mariusz.kiljanczyk@com-pan.pl">Mariusz Kilja�czyk</a>
 */
public class NewTaskAction extends EventActionSupport
{

    private List<InOfficeDocumentKind> kinds;
    private boolean canChooseKind;
    private boolean canReadDictionaries;
    private boolean canEdit;
    private DocumentKind documentKind = null;
    private Integer kindId;
    private Long contractorId;
    private Long contractorWorkerId;
    private Boolean needApplicant;
    private String contractorName;
    private boolean taskArchived;
	private static Contractor tmpContractor = new Contractor();
    // @EXPORT
    private FieldsManager fm;

    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();

    // @EXPORT/@IMPORT
    protected String documentKindCn;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate")
            .append(OpenHibernateSession.INSTANCE)
            .append(new Create())
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);
    }



    private class FillForm implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                if(contractorId==null && contractorWorkerId==null)
                {
                    needApplicant = true;
                    return;
                }
                if(contractorId!=null)
                {
                    Contractor contractor = tmpContractor.find(contractorId);
                    contractorName = contractor.getName();
                }
                else
                {
                    ContractorWorker worker =  ContractorWorker.getInstance().find(contractorWorkerId);
                    contractorName = worker.getFirstname()+" "+worker.getLastname();
                }

                canReadDictionaries = true;
                canEdit = true;
                documentKindCn = DocumentLogicLoader.CRMTASK_KIND;
                fillForm(DocumentLogic.TYPE_OUT_OFFICE);
                // inicjuje rodzaj pisma przychodz�cego
                kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                canChooseKind = true;
                if (!canChooseKind)
                {
                    // nie mo�na wybiera� na formatce - rodzaj pisma ustalony
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                            kindId = inKind.getId();
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    protected void fillForm(int documentType) throws EdmException
    {
        canReadDictionaries = true;
        if (documentKindCn == null)
        {
            documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRMTASK_KIND);
            documentKindCn = documentKind.getCn();
        }
        else
        {
            documentKind = DocumentKind.findByCn(documentKindCn);
        }
        fm = documentKind.getFieldsManager(null);
        fm.initialize();
        documentKind.logic().setInitialValues(fm, documentType);

        if (hasActionErrors())
            fm.reloadValues(values);
    }

     private class Create implements ActionListener
     {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if(contractorId==null && contractorWorkerId==null)
                    throw new EdmException("Nie podano kontrahenta");

                InOfficeDocument newDocument = new InOfficeDocument();
                DocumentKind dockind = DocumentKind.findByCn(DocumentLogicLoader.CRMTASK_KIND);
                newDocument.setDocumentKind(dockind);

                Sender sender = new Sender();
                sender.setAnonymous(true);
                newDocument.setSender(sender);

                Map address = GlobalPreferences.getAddress();
                Recipient recipient = new Recipient();
                recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                recipient.setCountry("PL");
                newDocument.addRecipient(recipient);
                newDocument.setAssignedDivision(DSDivision.ROOT_GUID);
                newDocument.setCreatingUser(DSApi.context().getPrincipalName());
                newDocument.setIncomingDate(new Date());
                newDocument.setSummary("summary");// i tak zostanie nadpisane

                List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
                String kindName = dockind.logic().getInOfficeDocumentKind();
                for (InOfficeDocumentKind inKind : kinds)
                {
                    if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                        newDocument.setKind(inKind);
                }
                newDocument.setSource("crm");
                newDocument.create();
                dockind.logic().archiveActions(newDocument, DocumentKindsManager.getType(newDocument));
                dockind.logic().validate(values, dockind, null);
                dockind.set(newDocument.getId(), values);

                WorkflowFactory.createNewProcess(newDocument, true);
                newDocument.getDocumentKind().logic().onStartProcess(newDocument);

                dockind.logic().documentPermissions(newDocument);//document permissions musz� zawsze by� na samym ko�cu

                String coordinatorName = dockind.logic().getProcessCoordinator(newDocument).getUsername();
                if(coordinatorName!=null)
                {
                    DSUser user = DSUser.findByUsername(coordinatorName);
                    addActionMessage("Dodano nowe zadanie. Zadanie zosta�o zadekretowane na " + user.getFirstname() + " " + user.getLastname());
                }
                else
                {
                    addActionMessage("Dodano nowe zadanie. Nie ustawiono koordynatora, wi�c nie zosta�o zadekretowane");
                }
                taskArchived = true;

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                return;
            }
            catch (Throwable e)
            {
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError("Wyst�pi� b��d ("+e.getMessage()+")");
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
     }

    public boolean isCanChooseKind()
    {
        return canChooseKind;
    }

    public void setCanChooseKind(boolean canChooseKind)
    {
        this.canChooseKind = canChooseKind;
    }

    public boolean isCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public void setCanReadDictionaries(boolean canReadDictionaries)
    {
        this.canReadDictionaries = canReadDictionaries;
    }

    public Long getContractorId()
    {
        return contractorId;
    }

    public void setContractorId(Long contractorId)
    {
        this.contractorId = contractorId;
    }

    public String getContractorName()
    {
        return contractorName;
    }

    public void setContractorName(String contractorName)
    {
        this.contractorName = contractorName;
    }

    public Long getContractorWorkerId()
    {
        return contractorWorkerId;
    }

    public void setContractorWorkerId(Long contractorWorkerId)
    {
        this.contractorWorkerId = contractorWorkerId;
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public void setFm(FieldsManager fm)
    {
        this.fm = fm;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public List<InOfficeDocumentKind> getKinds()
    {
        return kinds;
    }

    public void setKinds(List<InOfficeDocumentKind> kinds)
    {
        this.kinds = kinds;
    }

    public Boolean getNeedApplicant()
    {
        return needApplicant;
    }

    public void setNeedApplicant(Boolean needApplicant)
    {
        this.needApplicant = needApplicant;
    }

    public boolean isTaskArchived()
    {
        return taskArchived;
    }

    public void setTaskArchived(boolean taskArchived)
    {
        this.taskArchived = taskArchived;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }
}
