package pl.compan.docusafe.web.crm;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
/**
 * Akcja wyswietlająca liste kontrahentów.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class ContractorListAction extends EventActionSupport
{

    public static final String URL = "/crm/contractor-list.action";
    public static final int LIMIT = 20;
    
    //@IMPORT/EXPORT
    private Long   id;
    private String nazwa;
    private String city;
    private String street;
    private String phoneNumber;
    private String fax;
    private String email;
    private String nip;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private Contractor inst;
    private Pager pager;
    private int offset;
    private String sortDesc;
    private String sortAsc;
    private Boolean ascending;
    private String sortField;
	private static Contractor tmpContractor = new Contractor();
    private List<? extends Contractor> results;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(ContractorListAction.class.getPackage().getName(),null);
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            try
            {
                if(canRead)
                {
                    QueryForm form = new QueryForm(offset, LIMIT);    
                    form.addOrderAsc("name");
                    SearchResults<? extends Contractor> results = Contractor.search(form);
    
                    if (results == null || results.totalCount() == 0)
                    {
                        
                    }
                    else
                    {
                        ContractorListAction.this.results = fun.list(results);
                        Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                        {
                            public String getLink(int offset)
                            {
                                return HttpUtils.makeUrl(URL, new Object[] 
                                    {
                                        "doSearch", "true",     
                                        "nazwa",nazwa,                                           
                                        "city",city,                       
                                        "street",street, 
                                        "phoneNumber",phoneNumber,
                                        "fax",fax,
                                        "email",email,
                                        "offset", String.valueOf(offset)
                                    }
                                );
                            }
                        };
                        pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                        
                        sortAsc = linkVisitor.getLink(offset) + "&ascending=true"; 
                        sortDesc = linkVisitor.getLink(offset) + "&ascending=false"; 
                    }
                }
                else
                    throw new EdmException(sm.getString("BrakUprawnienDoOgladaniaKontrahentow"));
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnień do usuwania ze słownika");

                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.KLIENT_CN);
                dockindQuery.field(f,id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                if (searchResults == null || searchResults.totalCount()<1)
                {
                    inst = tmpContractor.find(id);
                    inst.delete();
                    id = null;
                    addActionMessage(sm.getString("ZeSlownikaUsunieto",inst.getName()));
                }
                else
                    throw new EdmException(sm.getString("NieMoznaUsunacTegoKonytachentaPoniewazSaDoNiegoPrzypisaneDokumenty"));
                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    } 
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if (nazwa!= null) form.addProperty("name",nazwa);
                if (city!= null) form.addProperty("city",city);
                if (street!= null) form.addProperty("street",street);
                if (nip != null ) form.addProperty("nip", nip);


                if (ascending != null)
                    if(ascending)
                        form.addOrderAsc(sortField);
                    else
                        form.addOrderDesc(sortField);
                else
                    form.addOrderAsc("name");

                SearchResults<? extends Contractor> results = Contractor.search(form);

                if (results == null || results.totalCount() == 0)
                {
                    throw new EdmException(sm.getString("NieZnalezionoKontrahentowPasujacychDoWpisanychDanych"));
                }
                ContractorListAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true", 
                                "nazwa",nazwa,                                        
                                "city",city,                       
                                "street",street, 
                                "phoneNumber",phoneNumber,
                                "fax",fax,
                                "email",email,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);

                sortAsc = linkVisitor.getLink(offset) + "&ascending=true"; 
                sortDesc = linkVisitor.getLink(offset) + "&ascending=false"; 
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            nazwa = TextUtils.trimmedStringOrNull(nazwa);
            city = TextUtils.trimmedStringOrNull(city);
            street = TextUtils.trimmedStringOrNull(street);
        }
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String nip)
    {
        this.city = nip;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String regon)
    {
        this.street = regon;
    }

    public List<? extends Contractor> getResults()
    {
        return results;
    }

    public void setResults(List<? extends Contractor> results)
    {
        this.results = results;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public Boolean getAscending()
    {
        return ascending;
    }

    public void setAscending(Boolean ascending)
    {
        this.ascending = ascending;
    }

    public String getSortAsc()
    {
        return sortAsc;
    }

    public void setSortAsc(String sortAsc)
    {
        this.sortAsc = sortAsc;
    }

    public String getSortDesc()
    {
        return sortDesc;
    }

    public void setSortDesc(String sortDesc)
    {
        this.sortDesc = sortDesc;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

   
}
