/*
 */
package pl.compan.docusafe.web.crm;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.parametrization.ilpol.CrmAppLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 *
 * @author <a href="mailto:tomasz.lipka@com-pan.pl">Tomasz Lipka</a>
 */
public class NewClientContactAction extends EventActionSupport {

    private List<InOfficeDocumentKind> kinds;
    private Integer kindId;
    private Long contractorId;
    private Long contractorWorkerId;
    private String contractorName;
    private Boolean applicationArchived;
    private DocumentKind documentKind = null;
    private boolean canReadDictionaries;
    private boolean canEdit;
    private boolean canChooseKind;
    protected String documentKindCn;
    private FieldsManager fm;
	private static Contractor tmpContractor = new Contractor();
    
    protected Map<String,Object> values = new HashMap<String,Object>();

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave")
            .append(OpenHibernateSession.INSTANCE)
            .append(new Save())
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {

        public FillForm()
        {
        }

        public void actionPerformed(ActionEvent event) {
            try
            {
                if(contractorId!=null)
                {
                    Contractor contractor = tmpContractor.find(contractorId);
                    contractorName = contractor.getName();
                }
                canReadDictionaries = true;
                canEdit = true;
                documentKindCn = DocumentLogicLoader.CRM_MARKETING_TASK_KIND;
                fillForm(DocumentLogic.TYPE_OUT_OFFICE); 
                kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                canChooseKind = false;
                if (!canChooseKind)
                {    
                    // nie mo�na wybiera� na formatce - rodzaj pisma ustalony                    
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                            kindId = inKind.getId();
                    }
                }
            }
            catch(EdmException edme)
            {
                addActionError(edme.getMessage());
            }
        }
    }
    
    protected void fillForm(int documentType) throws EdmException
    {
        canReadDictionaries = true;
        if (documentKindCn == null)
        {
            documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
            documentKindCn = documentKind.getCn();
        }
        else
        {
            documentKind = DocumentKind.findByCn(documentKindCn);
        }
        fm = documentKind.getFieldsManager(null);
        fm.initialize();
        documentKind.logic().setInitialValues(fm, documentType);

        if (hasActionErrors())
            fm.reloadValues(values);   
    }

     private class Save implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try
            {
            	DSApi.context().begin();
                String kindDate;
                kindDate = (String)values.get(CrmAppLogic.DATA_FIELD_CN);
                Date data;
                if(kindDate==null)
                {
                    data = new Date();
                }
                else
                {
                    data = DateUtils.nullSafeParseJsDate(kindDate);
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(data);
                Date d = new Date();

                DocumentKind dockind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
                Document document = new  Document("zg�oszenie "+d.getHours()+ ":"+d.getMinutes(), "zg�oszenie "+DateUtils.formatCommonTime(data));
                
                Folder folder = Folder.getRootFolder();
                folder = folder.createSubfolderIfNotPresent("Kontakt z klientem - CRM");
                folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
                folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.MONTH)+1));
                folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
                document.setFolder(folder);
                document.setDocumentKind(dockind);
                document.create();
                Long newDocumentId = document.getId();
                
                values.put("NUMER_KONTRAHENTA", contractorId);
                
                //dockind.logic().correctValues(values, dockind);
                //dockind.logic().validateAcceptances(values, dockind, newDocumentId);
                dockind.set(newDocumentId, values);
                //dockind.logic().validate(values, dockind);
                dockind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
                
                
                
                dockind.logic().documentPermissions(document);
                addActionMessage("Dodano zg�oszenie ");
                applicationArchived = true;
                DSApi.context().commit();
                //nowe zadanie naliste koordynatora
            }
            catch(EdmException edme)
            {
            	DSApi.context().setRollbackOnly();
                addActionError(edme.getMessage());
            }
        }

    }

    public Long getContractorId()
    {
        return contractorId;
    }

    public Long getContractorWorkerId()
    {
        return contractorWorkerId;
    }

    public String getContractorName()
    {
        return contractorName;
    }

    public void setContractorId(Long contractorId)
    {
        this.contractorId = contractorId;
    }

    public void setContractorWorkerId(Long contractorWorkerId)
    {
        this.contractorWorkerId = contractorWorkerId;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public void setApplicationArchived(Boolean applicationArchived)
    {
        this.applicationArchived = applicationArchived;
    }

    public void setContractorName(String contractorName)
    {
        this.contractorName = contractorName;
    }

    public Boolean getApplicationArchived()
    {
        return applicationArchived;
    }

    public boolean isCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public void setCanReadDictionaries(boolean canReadDictionaries)
    {
        this.canReadDictionaries = canReadDictionaries;
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public void setFm(FieldsManager fm)
    {
        this.fm = fm;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public boolean isCanChooseKind()
    {
        return canChooseKind;
    }

    public void setCanChooseKind(boolean canChooseKind)
    {
        this.canChooseKind = canChooseKind;
    }

    
}
