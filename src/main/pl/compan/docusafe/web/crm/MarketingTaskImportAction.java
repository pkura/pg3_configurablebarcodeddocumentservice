/*
 */

package pl.compan.docusafe.web.crm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.crm.Machine;
import pl.compan.docusafe.core.crm.Marka;
import pl.compan.docusafe.core.crm.Region;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.ilpol.ServicesUtils;
import pl.compan.docusafe.parametrization.ilpoldwr.MarketingTaskLogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * 
 * @author Mariusz Kilja�czyk
 */
public class MarketingTaskImportAction extends EventActionSupport
{

	private StringManager sm = GlobalPreferences.loadPropertiesFile("", null);
	protected final Log log = LogFactory.getLog(getClass());
	private final Integer USER=1;
	private final Integer GRUPA=2;
	private final Integer KATEGORIA=3;
	private final Integer UWAGA=4;
	private final Integer MARKI_MASZYNY=5;
	private final Integer RODZAJ_KONTRAHENTA=6;
	private final Integer REGION=7;
	private final Integer NAZWA=8;
	private final Integer SIK=9;
	private final Integer NIP=10;
	private final Integer REGON=11;
	private final Integer KRS=12;
	private final Integer MIASTO=13;
	private final Integer KOD=14;
	private final Integer ULICA=15;
	private final Integer TELEFON=16;
	private final Integer FAX=17;
	private final Integer EMAIL=18;
	private final Integer WWW=19;
	private final Integer OBROT=20;

	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);

		//registerListener("doTMP").append(OpenHibernateSession.INSTANCE).append(new TMP()).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doImport").
			append(OpenHibernateSession.INSTANCE)
			.append(new Create())
			.append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

	}

	private FormFile file;
	private List<AssignmentObjective> objectives;
	private String objectiveSel;
	private String objectiveTxt;
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				objectives = AssignmentObjective.list();
				if (objectives.size() > 0)
					objectiveSel = ((AssignmentObjective) objectives.get(0)).getName();
			}
			catch (EdmException e)
			{
				log.error(e.getMessage());
			}
		}
	}

	private class Create implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if (file == null)
			{
				addActionError(sm.getString("BrakPliku"));
				return;
			}
			try
			{
				
				String line;
				Map<String, Object> values;
				String[] dane;
				FileReader fr = new FileReader(file.getFile());
				BufferedReader br = new BufferedReader(fr);
				br.readLine();
				Integer count = 0;
				Integer countLine = 0;
				DocumentKind dk = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
				while ((line = br.readLine()) != null)
				{
					try
					{
						DSApi.context().begin();
						countLine++;
						values = new TreeMap<String, Object>();
						dane = line.split(";");
						if(dane.length < 7)
						{
							values.put("GRUPA",dk.getFieldByCn("GRUPA").getEnumItemByCn("DM").getId());
							values.put("KATEGORIA", dk.getFieldByCn("KATEGORIA").getEnumItemByCn("KLIENT_ILPOL").getId());
							DSUser user = DSUser.findByUsername(dane[0]);
							values.put(MarketingTaskLogic.UZYTKOWNIK_FIELD_CN, user.getId());
							String nip = dane[3];
							nip = nip.trim();
							nip = nip.replaceAll("-", "");
							List<Contractor> conList = Contractor.findByNIP(nip);
							if (conList != null && conList.size() > 0)
							{
								values.put(MarketingTaskLogic.NUMER_KONTRAHENTA_FIELD_CN, conList.get(0).getId());									
							}
							else
							{
								addActionMessage("Nie znalaz� kontrahenta o numerze nip  " + nip);
								DSApi.context().commit();
								continue;
							}
							createTask("Zadanie Handlowe", values, user,dane[2]);		
						}
						else
						{
							String nip = dane[NIP];
							String kategoria = dane[KATEGORIA];
							String grupa = dane[GRUPA];
							System.out.println(grupa+kategoria);
							if(nip != null)
							{
								nip = nip.trim();
								nip = nip.replaceAll("-", "");
							}
							if(StringUtils.isNotEmpty(grupa))
							{
								values.put("GRUPA", dk.getFieldByCn("GRUPA").getEnumItemByTitle(grupa.trim()).getId());
							}
							if(StringUtils.isNotEmpty(kategoria))
							{
								values.put("KATEGORIA", dk.getFieldByCn("KATEGORIA").getEnumItemByTitle(kategoria.trim()).getId());
							}
							Contractor con = null;
							if (nip != null && nip.length() > 0)
							{
								List<Contractor> conList = Contractor.findByNIP(nip);
								if (conList != null && conList.size() > 0)
								{
									addActionMessage("Pomini�to kontrahenta o numerze nip  " + nip);
									DSApi.context().commit();
									//con = conList.get(0);
									continue;
									
								}
							}
							else
							{
								con = getByAttr(dane);
							}
							
							if (con == null)
							{
								con = createContractor(dane, nip);
							}
								
							values.put(MarketingTaskLogic.NUMER_KONTRAHENTA_FIELD_CN, con.getId());
							DSUser user = DSUser.findByUsername(dane[USER]);
							if (user != null)
								values.put(MarketingTaskLogic.UZYTKOWNIK_FIELD_CN, user.getId());
							
							createTask("Zadanie Handlowe", values, user,dane[UWAGA]);
						}
						count++;
						DSApi.context().commit();

						if(countLine%100 == 99)
						{
							log.error("Czyszczenie sesi po "+countLine+" dokumentach");
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
						}
					}
					catch (Exception e)
					{
						log.error(e.getMessage(),e);
						addActionError("B��d importu w lini " + countLine + " : " + e.getMessage());
						DSApi.context().rollback();
					}
				}
				addActionMessage("Zaimportowano " + count + " zada�");
			}
			catch (Exception ioe)
			{
				log.error("B��d importu zada� :" + ioe);
				addActionError("B��d importu " + ioe);
			}
			finally
			{
//				try
//				{
//					DSApi.close();
//				}
//				catch (EdmException e1)
//				{
//					LogFactory.getLog("eprint").error("", e1);
//				}
			}
		}
	}

	private Contractor createContractor(String[] tmp, String nip) throws EdmException
	{
		Contractor con = new Contractor();
		con.setName(StringUtils.left(getTrimString(NAZWA,tmp), 50));
		con.setFullName(getTrimString(NAZWA,tmp));
		con.setNip(nip);
		con.setStreet(getTrimString(ULICA,tmp));
		con.setCity(getTrimString(MIASTO,tmp));
		con.setCode(getTrimString(KOD,tmp));
		con.setContractorNo(getTrimString(SIK,tmp));
		con.setEmail(getTrimString(EMAIL,tmp));
		con.setFax(getTrimString(FAX,tmp));
		Set<Role> role = new HashSet<Role>();
		String roleName = "Nowy Kontakt";
		if(tmp[RODZAJ_KONTRAHENTA] != null && tmp[RODZAJ_KONTRAHENTA].length() > 0)
		{
			roleName = tmp[RODZAJ_KONTRAHENTA].trim();
		}
		role.add(Role.findByName(roleName));
		con.setRole(role);
		if(tmp[MARKI_MASZYNY] != null && tmp[MARKI_MASZYNY].length() > 0)
		{
			if("Dealer".equals(roleName))
			{
				Set<Marka> marki = new HashSet<Marka>();
				String name = tmp[MARKI_MASZYNY].trim();
				String[] markiTab = name.split("\\|");
				for (String marka : markiTab) 
				{
					
					Marka m = Marka.findByName(marka);
					if(m != null)
					{
						marki.add(m);
					}
				}
				con.setMarka(marki);
			}
			if("Dostawca".equals(roleName))
			{
				Set<Machine> machine = new HashSet<Machine>();
				String name = tmp[MARKI_MASZYNY].trim();
				String[] machines = name.split("\\|");
				for (String ma : machines) 
				{
					
					Machine m = Machine.findByName(ma);
					if(m != null)
					{
						machine.add(m);
					}
				}
				con.setMachine(machine);
			}
		}
		con.setPhoneNumber(getTrimString(TELEFON,tmp));
	//	con.setPhoneNumber2(getTrimString(15,tmp));
//		con.setDsUser(tmp[USER]);
		con.addPatron(tmp[USER], "DS");
		con.setWww(getTrimString(WWW,tmp));
		con.setObrot(getDouble(OBROT,tmp));
		con.setRegion(Region.findByName(getTrimString(REGION,tmp)));
		con.create();

		int indexStart = OBROT+1;

		for (;;)
		{
			if (tmp.length > indexStart + 2)
			{
				ContractorWorker conW = new ContractorWorker();
				conW.setFirstname(tmp[indexStart]);
				conW.setLastname(tmp[indexStart + 1]);
				conW.setRemarks(tmp[indexStart + 2]);
				conW.setContractorId(con.getId());
				conW.create();
				indexStart += 3;
			}
			else
				break;
		}
		return con;
	}
	
	private Contractor getByAttr(String[] tmp) throws EdmException
	{
		if(tmp[NAZWA] == null || tmp[NAZWA].length() < 1)
			throw new EdmException("Brak nazwy kontrahenta");
		List<Contractor> list = Contractor.findByNameCity(tmp[NAZWA], tmp[MIASTO]);

		if(list != null && list.size() > 0)
			return list.get(0);
		else
			return null;		
	}

	private String getTrimString(Integer posn, String[] tab)
	{
		if (tab == null)
			return " ";
		else
		{
			try
			{
				return tab[posn].trim();
			}
			catch (Exception e)
			{return " ";}
		}
	}
	
	private Double getDouble(Integer posn, String[] tab)
	{
		if (tab == null)
			return new Double(0);
		else
		{
			try
			{
				return new Double(tab[posn].trim());
			}
			catch (Exception e)
			{return new Double(0);}
		}
	}

	public void setFile(FormFile file)
	{
		this.file = file;
	}

	public List getObjectives()
	{
		return objectives;
	}

	public void setObjectives(List objectives)
	{
		this.objectives = objectives;
	}

	public String getObjectiveSel()
	{
		return objectiveSel;
	}

	public void setObjectiveSel(String objectiveSel)
	{
		this.objectiveSel = objectiveSel;
	}

	public String getObjectiveTxt()
	{
		return objectiveTxt;
	}

	public void setObjectiveTxt(String objectiveTxt)
	{
		this.objectiveTxt = objectiveTxt;
	}

	public void createTask(String summary, Map<String, Object> values, DSUser user, String uwaga) throws EdmException
	{
		InOfficeDocument doc = new InOfficeDocument();
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
		String guid = user.getDivisions() != null && user.getDivisions().length > 0 ? user
				.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID;
		ServicesUtils.createTaskForPersonOrDiv("Zadanie marketingowe", values, user.getName(), guid, "", doc, documentKind, "Zadanie marketingowe",false);
		doc.addRemark(new Remark(uwaga));
//		doc.setDocumentKind(documentKind);
//		doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
//		doc.setDivisionGuid(DSDivision.ROOT_GUID);
//		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
//		doc.setCreatingUser(DSApi.context().getPrincipalName());
//		doc.setSummary(summary);
//		doc.setForceArchivePermissions(false);
//		doc.setAssignedDivision(DSDivision.ROOT_GUID);
//		doc.setClerk(DSApi.context().getPrincipalName());
//		doc.setSender(new Sender());
//		doc.setSource("crm");
//
//		List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
//		String kindName = documentKind.logic().getInOfficeDocumentKind();
//		boolean canChooseKind = (kindName == null);
//		if (!canChooseKind)
//		{
//			for (InOfficeDocumentKind inKind : kinds)
//			{
//				if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
//					doc.setKind(InOfficeDocumentKind.find(inKind.getId()));
//			}
//		}
//		else
//		{
//			doc.setKind(InOfficeDocumentKind.find(1));
//		}
//		Calendar currentDay = Calendar.getInstance();
//		currentDay.setTime(GlobalPreferences.getCurrentDay());
//		doc.setIncomingDate(currentDay.getTime());
//		doc.setOriginal(true);
//		doc.create();
//		doc.addRemark(new Remark(uwaga));
//		Journal journal = Journal.getMainIncoming();
//		Long journalId;
//		journalId = journal.getId();
//		Integer sequenceId = null;
//		sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
//
//		doc.bindToJournal(journalId, sequenceId);
//
//		Long newDocumentId = doc.getId();
//
//		doc.setDocumentKind(documentKind);
//		newDocumentId = doc.getId();
//
//		documentKind.set(newDocumentId, values);
//		documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
//		documentKind.logic().documentPermissions(doc);
//		DSApi.context().session().flush();
//		/**
//		 * to bylo ustawiane jako opis?? procesu - teraz ustawiane jest "start-process"
//		String obj = null;
//		if (objectiveTxt == null)
//			obj = objectiveSel;
//		else
//			obj = objectiveTxt;
//		 */
//        Map<String, Object> map = Maps.newHashMap();
//		map.put(ASSIGN_USER_PARAM, user);
//		map.put(ASSIGN_DIVISION_GUID_PARAM, (user.getDivisions() != null && user.getDivisions().length > 0 ? user
//				.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID));
//		documentKind.getDockindInfo().getProcessesDeclarations().onStart(doc, map);
//		Map<String, Object> map = Maps.newHashMap();
//		if (user == null)
//		{
//			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
//			documentKind.getDockindInfo().getProcessesDeclarations().onStart(doc, map);
//			//WorkflowFactory.createNewProcess(doc, false, obj);
//		}			
//		else
//		{
//			map.put(ASSIGN_USER_PARAM, user.getName());
//			map.put(ASSIGN_DIVISION_GUID_PARAM, (user.getDivisions() != null && user.getDivisions().length > 0 ? user
//					.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID));
//			//WorkflowFactory.createNewProcess(doc, false, obj, user.getName(), (user.getDivisions() != null && user.getDivisions().length > 0 ? user
//					//.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID), user.getName());
//		}
			
	}
}