package pl.compan.docusafe.web.crm;

import java.util.List;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.crm.ContractorWorker.ContractorWorkerQuery;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
/**
 * Akcja dodania/edycji osoby w module CRM.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class NewContractorWorkerAction extends EventActionSupport
{
	private Logger log  = LoggerFactory.getLogger(NewContractorWorkerAction.class);
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long   id;
    private String title;
    private String firstname;
    private String lastname;
    private String street;
    private String zip;
    private String location;
    private String country;
    private String pesel;
    private String nip;
    private String email;
    private String fax;
    private String remarks;
    private String phoneNumber;
    private String phoneNumber2;
    private String contractorName;
    private List<ContractorWorker> workers;
    private Long defaultWorkerId;
    private Long thisWorkerId;
    private boolean dictionaryAction;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private ContractorWorker inst;
    private Pager pager;
    private int offset;
    private List<? extends ContractorWorker> results;
    private String param;
	 private static Contractor tmpContractor = new Contractor();

    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    public boolean isContractorTab()
    {
    	log.error("B��d definicji, ta metoda powinna byc przeci��ona");
    	return false;
    }
    public boolean isCrm()
    {
    	log.error("B��d definicji, ta metoda powinna byc przeci��ona");
    	return false;
    }
    public String getBaseLink()
    {
    	log.error("B��d definicji, ta metoda powinna byc przeci��ona");
        return "/crm/contractor-worker.action";
    }
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
            
       registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
            
        registerListener("doChangeDefault").
            append(OpenHibernateSession.INSTANCE).
            append(new ChangeDefault()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try
        	{
        		
        		if(thisWorkerId == null)
        			throw new EdmException("Nie wybrano pracownika");
        		DSApi.context().begin();
        		ContractorWorker cw = ContractorWorker.getInstance().find(thisWorkerId);
        		String name = cw.getImieNazwisko();
        		cw.delete();
        		thisWorkerId = null;
        		DSApi.context().commit();
        		addActionMessage("Usuni�to "+ name);
        	}
        	catch (Exception e) 
        	{
				log.error("",e);
				addActionError(e.getMessage());
			}
        }
    }
    
    private class ChangeDefault implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try
        	{
        		if(id == null)
        			throw new EdmException("Nie wybrano kontrahenta");
        		DSApi.context().begin();
        		Contractor con = null;
    			con = tmpContractor.find(id);
    			con.setDefaultWorkerId(thisWorkerId);
    			DSApi.context().commit();
        	}
        	catch (Exception e) 
        	{
				log.error("",e);
				addActionError(e.getMessage());
			}
        }
    }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            
	            if (id != null) 
	            {	             
	            	if(dictionaryAction)
	            	{
	            		setThisWorkerId(id);
	            		inst = ContractorWorker.getInstance().find(getThisWorkerId());
	            		id = inst.getContractorId();
	            	}
	            	
	            	Contractor con = null;
        			con = tmpContractor.find(id);
        			if(con == null)
        				return;
        			
        			workers = ContractorWorker.findByContractorId(id);  
        			if(inst == null)
        			{
		            	if(getThisWorkerId() != null)
		            		inst = ContractorWorker.getInstance().find(getThisWorkerId());
	        			else if(con.getDefaultWorkerId() != null)
	                    	inst = ContractorWorker.getInstance().find(con.getDefaultWorkerId());
	                    else if(workers != null && workers.size() > 0)
	                    	inst = workers.get(0);
        			}
            		        			                
                    defaultWorkerId = con.getDefaultWorkerId();     
	            	if (inst != null) 
	                {
	                    thisWorkerId = inst.getId();
						title = inst.getTitle();
						firstname = inst.getFirstname();
						lastname = inst.getLastname();
						street = inst.getStreet();
						zip = inst.getZip();
						location = inst.getLocation();
						country = inst.getCountry();
						pesel = inst.getPesel();
						nip = inst.getNip();
						email = inst.getEmail();
						fax = inst.getFax();
						remarks = inst.getRemarks();
						phoneNumber = inst.getPhoneNumber();
	                    phoneNumber2 = inst.getPhoneNumber2();
	                    id = inst.getContractorId();
                        try
                        {
                            contractorName = tmpContractor.find(id).getName();
                        }
                        catch (EdmException e)
                        {
                        	log.debug("", e);
                        }
	                }
	            }
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
                log.error("",e);
            }
        }
        
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                	 
                inst = new ContractorWorker();
                
                inst.setTitle(title);
                inst.setFirstname(firstname);
                inst.setLastname(lastname);
                inst.setStreet(street);
                inst.setZip(zip);
                inst.setLocation(location);
                inst.setCountry(country);
                inst.setPesel(pesel);
                inst.setNip(nip);
                inst.setEmail(email);
                inst.setFax(fax);
                inst.setRemarks(remarks);
                inst.setPhoneNumber(phoneNumber);
                inst.setPhoneNumber2(phoneNumber2);
                inst.setContractorId(id);
                inst.create();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",firstname +" "+lastname));

            } 
            catch (EdmException e)
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {  
            try
            {
                DSApi.context().begin();
                ContractorWorker inst = ContractorWorker.getInstance().find(thisWorkerId);
                
                inst.setTitle(title);
                inst.setFirstname(firstname);
                inst.setLastname(lastname);
                inst.setStreet(street);
                inst.setZip(zip);
                inst.setLocation(location);
                inst.setCountry(country);
                inst.setPesel(pesel);
                inst.setNip(nip);
                inst.setEmail(email);
                inst.setFax(fax);
                inst.setRemarks(remarks);
                inst.setPhoneNumber(phoneNumber);
                inst.setPhoneNumber2(phoneNumber2);
                inst.setContractorId(id);
                
                DSApi.context().session().save(inst);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").error("", e);
            }
        }
    }
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            try
            {
                ContractorWorkerQuery form = new ContractorWorkerQuery(); 

                if (firstname!= null) form.setFirstname(firstname);
                if (lastname!= null) form.setLastname(lastname);
                if (phoneNumber!= null) form.setPhoneNumber(phoneNumber);
                if (email!= null) form.setPhoneNumber(email);
                if (id!= null) form.setContractorId(id);
                if (country!= null) form.setCountry(country);
                if (fax!= null) form.setFax(fax);
                if (location!= null) form.setLocation(location);
                if (nip!= null) form.setNip(nip);
                if (pesel!= null) form.setPesel(pesel);
                if (phoneNumber!= null) form.setPhoneNumber(phoneNumber);
                if (phoneNumber2!= null) form.setPhoneNumber2(phoneNumber2);
                if (remarks!= null) form.setRemarks(remarks);
                if (street!= null) form.setStreet(street);
                if (title!= null) form.setTitle(title);
                if (zip!= null) form.setZip(zip);

                form.setLimit(LIMIT);
                form.setOffset(offset);
                form.addAsc("firstname");

                SearchResults<? extends ContractorWorker> results = ContractorWorker.search(form);

                if (results == null || results.totalCount() == 0)
                {
                    throw new EdmException(sm.getString("NieZnalezionoOsobPasujacychDoWpisanychDanych"));
                }
                NewContractorWorkerAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(getBaseLink(), new Object[] 
                            {
                            "doSearch", "true",     
                            "firstname",firstname,                                           
                            "lastname",lastname,                       
                            "phoneNumber",phoneNumber,
                            "email",email,
                            "id", id,
                            "country",country,
                            "fax",fax,
                            "location",location,
                            "nip",nip,
                            "pesel",pesel,
                            "phoneNumber",phoneNumber,
                            "phoneNumbre2",phoneNumber2,
                            "remarks",remarks,
                            "street",street,
                            "title",title,
                            "zip",zip,
                            "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getPesel()
    {
        return pesel;
    }

    public void setPesel(String pesel)
    {
        this.pesel = pesel;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public List<? extends ContractorWorker> getResults()
    {
        return results;
    }

    public void setResults(List<? extends ContractorWorker> results)
    {
        this.results = results;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getContractorName()
    {
        return contractorName;
    }

    public void setContractorName(String contractorName)
    {
        this.contractorName = contractorName;
    }

    public String getPhoneNumber2()
    {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2)
    {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }
	public void setWorkers(List<ContractorWorker> workers) {
		this.workers = workers;
	}
	public List<ContractorWorker> getWorkers() {
		return workers;
	}
	public void setDefaultWorkerId(Long defaultWorkerId) {
		this.defaultWorkerId = defaultWorkerId;
	}
	public Long getDefaultWorkerId() {
		return defaultWorkerId;
	}
	public void setThisWorkerId(Long thisWorkerId) {
		this.thisWorkerId = thisWorkerId;
	}
	public Long getThisWorkerId() {
		return thisWorkerId;
	}
	public void setDictionaryAction(boolean dictionaryAction)
	{
		this.dictionaryAction = dictionaryAction;
	}
	public boolean isDictionaryAction()
	{
		return dictionaryAction;
	}


}
