package pl.compan.docusafe.web.crm;


public class DlContractorWorkerAction extends NewContractorWorkerAction
{
    public String getBaseLink()
    {
        return "/crm/contractor-worker.action";
    }

    public boolean isCrm()
    {
        return false;
    }
    
    public boolean isContractorTab()
    {
    	return false;
    }
}

