/*
 */
package pl.compan.docusafe.web.crm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.parametrization.ilpol.CrmVindicationLogic;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 *
 * @author Mariusz Kilja�czyk
 */
public class ClientContactListAction extends EventActionSupport 
{

	private static Log log = LogFactory.getLog(ClientContactListAction.class);
    private Long id;
    private List<ContactListBean> beans;
    private static List<String> cns;
    private static Map<String,String> kinds;
    private String cn;
    
    static
    {
    	cns = new ArrayList<String>();
    	cns.add(DocumentLogicLoader.DL_KIND);
    	cns.add(DocumentLogicLoader.CRM_VINDICATION_KIND);
    	cns.add(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);    	
    }
    
    private void initKinds()
    {
    	kinds = new HashMap<String, String>();
    	try
    	{
	    	for (String cn : cns) 
	    	{
				kinds.put(cn, DocumentKind.findByCn(cn).getName());
			}
	    	kinds.put("all", "Wszystkie");
    	}
    	catch (Exception e)
    	{
    		log.error("",e);
		}
    }
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {

        public FillForm()
        {
        }

        public void actionPerformed(ActionEvent event) 
        {
        	if(id == null)
        	{
        		addActionError("Nie wybrano kontrahenta");
        		return;
        	}
        	
        	try
        	{
        		if(kinds == null || kinds.size() < 1)
            	{
            		initKinds();
            	}
        		if(cn == null)
        			cn = DocumentLogicLoader.CRM_VINDICATION_KIND;
        		beans = new ArrayList<ContactListBean>();
        		if(cn == null || cn.equals("all"))
        		{
	        		for (String cn : cns)
	        		{
	        			log.error("cn = "+cn);
		        		DocumentKind kind = DocumentKind.findByCn(cn);
		        		DockindQuery dockindQuery = new DockindQuery(0,0);
		                dockindQuery.setDocumentKind(kind);
		                dockindQuery.setCheckPermissions(false);
		                Field f = null;
//		                if("crmVindication".equals(cn))
//		                	f= kind.getFieldByCn("NUMER_KONTRAHENTA");
//		                else
		                f= kind.getFieldByCn("KLIENT");
		                
		                dockindQuery.field(f,getId());	                
		                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
		                
		                while(searchResults.hasNext())
		                {
		                	Document tmp = searchResults.next();
		                	tmp.getDocumentKind().getFieldsManager(tmp.getId());
		                	FieldsManager fm = tmp.getDocumentKind().getFieldsManager(tmp.getId());
		                	beans.add(new ContactListBean(tmp.getId(),getOpis(fm,tmp),tmp.getCtime()));
		                }
	        		}
        		}
        		else
        		{
        			log.error("cn = "+cn);
        			DocumentKind kind = DocumentKind.findByCn(cn);
	        		DockindQuery dockindQuery = new DockindQuery(0,0);
	                dockindQuery.setDocumentKind(kind);
	                dockindQuery.setCheckPermissions(false);
	                Field f = null;
	               
//	                if("crmVindication".equals(cn))
//	                	f= kind.getFieldByCn("NUMER_KONTRAHENTA");
//	                else
	                	f= kind.getFieldByCn("KLIENT");
	                
	                dockindQuery.field(f,getId());	                
	                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
	                
	                while(searchResults.hasNext())
	                {
	                	Document tmp = searchResults.next();
	                	tmp.getDocumentKind().getFieldsManager(tmp.getId());
	                	FieldsManager fm = tmp.getDocumentKind().getFieldsManager(tmp.getId());
	                	beans.add(new ContactListBean(tmp.getId(),getOpis(fm,tmp),tmp.getCtime()));
	                }
        		}
        		Collections.sort(beans);
        	}
        	catch (Exception e) 
        	{
				addActionError(e.getMessage());
				log.error("",e);
			}
        }

		private String getOpis(FieldsManager fm,Document doc) throws EdmException 
		{
			String ret = "";
			if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.DL_KIND))
			{
				ret = doc.getTitle();
				List<DlContractDictionary> l = (List<DlContractDictionary>) fm.getValue(DlLogic.NUMER_UMOWY_CN);
				if(l != null && l.size() > 0)
					ret += " [ " +((Map<String,String> )l.get(0)).get("NUMER_UMOWY_NUMERUMOWY")+" ]";//" [ " + l.get(0).getNumerUmowy()+" ]";
			}
			else if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.CRM_VINDICATION_KIND))
			{
				ret += fm.getValue(CrmVindicationLogic.NUMER_WEZWANIA_FIELD_CN)+" kwota = "+
				fm.getValue(CrmVindicationLogic.NALEZNOSC_FIELD_CN);
			}
			else if(fm.getDocumentKind().getCn().equals(DocumentLogicLoader.CRM_MARKETING_TASK_KIND))
			{
				ret += doc.getTitle();//fm.getValue(CrmMarketingTaskLogic.STATUS_FIELD_CN);
			}
			return ret;
		}
    }
    
    private class ContactListBean implements Comparable<ContactListBean>
    {
    	private String opis;
    	private Long id;
    	private Date ctime;
    	
		public ContactListBean(Long id, String opis,Date ctime)
		{
			super();
			this.setId(id);
			this.setOpis(opis);
			this.setCtime(ctime);
		}

		public void setOpis(String opis) {
			this.opis = opis;
		}

		public String getOpis() {
			return opis;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getId() {
			return id;
		}

		public void setCtime(Date ctime) {
			this.ctime = ctime;
		}

		public Date getCtime() {
			return ctime;
		}

		public int compareTo(ContactListBean o)
		{
			return this.ctime.compareTo(o.getCtime());
		}
    }


	public void setBeans(List<ContactListBean> beans) {
		this.beans = beans;
	}

	public List<ContactListBean> getBeans() {
		return this.beans;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getCn() {
		return cn;
	}

	public static void setKinds(Map<String,String> kinds) {
		ClientContactListAction.kinds = kinds;
	}

	public static Map<String,String> getKinds() {
		return kinds;
	}

}
