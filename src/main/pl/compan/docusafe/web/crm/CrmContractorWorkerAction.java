package pl.compan.docusafe.web.crm;


public class CrmContractorWorkerAction extends NewContractorWorkerAction
{
    public String getBaseLink()
    {
        return "/crm/new-contractor-worker.action";
    }

    public boolean isCrm()
    {
        return true;
    }
    
    public boolean isContractorTab()
    {
    	return false;
    }
    
}

