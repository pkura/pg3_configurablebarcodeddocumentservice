package pl.compan.docusafe.web;

import com.opensymphony.xwork.ActionContext;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

/**
 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
 */
public class JsLogAction extends EventActionSupport
{
	private final static Logger jslog = LoggerFactory.getLogger("js");
	
	protected void setup()
	{
		LogAction logAction = new LogAction();
		registerListener(DEFAULT_ACTION).append(logAction);
	}
	
	private class LogAction implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			String json = getRequestParameter("log");
			String level = getRequestParameter("level");
			
			// deserialize json
//			Gson gson = new Gson();
//			Log log = gson.fromJson(json, Log.class);

			if(level == null)
			{
				jslog.error("level is null");
				jslog.error(json);
			}
			else if(level.equals("info"))
			{
				jslog.info(json);
			}
			else if(level.equals("debug"))
			{
				jslog.debug(json);
			}
			else if(level.equals("warn"))
			{
				jslog.warn(json);
			}
			else if(level.equals("error"))
			{
				jslog.error(json);
			}
			else
			{
				jslog.error("Unrecognized level: "+level+", logging as error");
				jslog.error(json);
			}
		}
	}
	
	/**
	 * Gets parameter value POST/GET request
	 * 
	 * e.g. for GET request if p is "abc" then
	 * for url "...?abc=123" method will return "123".
	 * 
	 * @param p
	 * @return request paramater
	 */
	private String getRequestParameter(String p)
	{
    	String[] ps = (String[])ActionContext.getContext().getParameters().get(p);
    	
    	if(ps == null || ps.length == 0)
    	{
    		return null;
    	}
    	else
    	{
    		return ps[0];
    	}

	}
}
