package pl.compan.docusafe.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;
import java.util.prefs.Preferences;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;




/**
 * @author Piotr Komisarski
 */
public class GifService extends HttpServlet
{

    private static final Log log = LogFactory.getLog(GifService.class);

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
    	resp.setHeader("Cache-Control", "public, max-age="+(24*60*60)); // cache 24 godziny
        Preferences prefs = Preferences.systemNodeForPackage(Docusafe.class);
        String path = req.getServletPath();

        String[] tmp = path.split("/");
        //   tmp[tmp.length-1]  jest sam� nazw� pliku np. "dyskietka.gif"
        String fileName = tmp[tmp.length - 1];

        Properties current = null;
        String user = "";
        String layout = "classic";
        if (AuthUtil.getSubject(req) != null)
        {
            
            try
            {
                DSApi.open(AuthUtil.getSubject(req));
                user = DSApi.context().getDSUser().getName();
                layout = prefs.get(user, "classic");
            }
            catch (EdmException ex)
            {
                log.error(ex.getMessage(), ex);
            }
            finally
            {
                DSApi._close();
            }
        }
        current = CssService.layouts.get(layout);
        if (current.containsKey(fileName)) path = current.getProperty(fileName);
        

        InputStream is = req.getSession().getServletContext().getResourceAsStream(path);

        if (is == null) 
        {
        	try
        	{
        		is = new FileInputStream(Docusafe.getHome() + path);      
        	}
        	catch (Exception e) {
				log.warn(e);
			}
        }
        try
        {
	        OutputStream os = resp.getOutputStream();
	        
	        org.apache.commons.io.IOUtils.copy(is, os);        
	        org.apache.commons.io.IOUtils.closeQuietly(os);
	        org.apache.commons.io.IOUtils.closeQuietly(is);
        }
        catch (Exception e) 
        {
			log.debug(e);
		}
    }
}