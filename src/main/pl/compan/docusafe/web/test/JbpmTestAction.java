package pl.compan.docusafe.web.test;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class JbpmTestAction extends EventDrivenAction {

	private static final Log log = LogFactory.getLog(JbpmTestAction.class);

	private static final String[] eventNames = new String[] { "doDefault" };

	public static final String FORWARD = "test/jbpm-test";

	public String[] getEventNames() {
		return eventNames;
	}

	public void testSimplePersistence() throws EdmException {

		deployProcessDefinition();

		//processInstanceIsCreatedWhenUserSubmitsWebappForm();

		//theProcessInstanceContinuesWhenAnAsyncMessageIsReceived();

	}

	/**
	 * Zastapic czym predzej normalnym panelem workflow
	 * @throws EdmException
	 */
	@Deprecated
	public void deployProcessDefinition() throws EdmException {

		
		DSApi.openAdmin();
		JbpmContext jbpmContext = DSjBPM.getContext();
		try 
		{
			ProcessDefinition processDefinition = ProcessDefinition
			.parseXmlResource("procesy/DSProcess/processdefinition.xml");
			LogFactory.getLog("workflow_log").debug("wersja: " + processDefinition.getVersion());
			
			jbpmContext.deployProcessDefinition(processDefinition);
			processDefinition = ProcessDefinition
			.parseXmlResource("procesy/DSProcessCC/processdefinition.xml");
			jbpmContext.deployProcessDefinition(processDefinition);
			processDefinition = ProcessDefinition
			.parseXmlResource("procesy/AkceptacjeIC/processdefinition.xml");
			jbpmContext.deployProcessDefinition(processDefinition);
			log.info("Wczytano now� definicj� procesu - wersja:"+ processDefinition.getVersion());
		} 
		finally 
		{				
			jbpmContext.close();
			DSApi.close();
		}
	}

	public void processInstanceIsCreatedWhenUserSubmitsWebappForm() throws EdmException {

		
		
		DSApi.openAdmin();
		JbpmContext jbpmContext = DSjBPM.getContext();
		
		
		try {
			
			GraphSession graphSession = jbpmContext.getGraphSession();

			ProcessDefinition processDefinition = graphSession
					.findLatestProcessDefinition("hello_world");

			ProcessInstance processInstance = new ProcessInstance(
					processDefinition);

			Token token = processInstance.getRootToken();

			token.signal();

			jbpmContext.save(processInstance);

		} finally 
		{
			jbpmContext.close();
			DSApi.close();
		}
	}

	public void theProcessInstanceContinuesWhenAnAsyncMessageIsReceived() throws EdmException {

		DSApi.openAdmin();
		JbpmContext jbpmContext = DSjBPM.getContext();
		try {

			GraphSession graphSession = jbpmContext.getGraphSession();

			ProcessDefinition processDefinition = graphSession
					.findLatestProcessDefinition("hello_world");

			List processInstances = graphSession
					.findProcessInstances(processDefinition.getId());

			// Because we know that in the context of this unit test, there
			// is
			// only one execution. In real life, the processInstanceId can
			// be
			// extracted from the content of the message that arrived or
			// from
			// the user making a choice.
			ProcessInstance processInstance = (ProcessInstance) processInstances
					.get(0);

			// Now we can continue the execution. Note that the
			// processInstance
			// delegates signals to the main path of execution (=the root
			// token).
			processInstance.signal();

			jbpmContext.save(processInstance);

		} finally 
		{
								
			jbpmContext.close();
			DSApi.close();
		}
	}

	private void testJBPMFlow() {
	}
	
	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {

		try {
			testSimplePersistence();
		} catch (EdmException e) {
			errors.add(e.getMessage());
			log.error("Blad", e);
		} 
		return mapping.findForward("main");
	}

}
