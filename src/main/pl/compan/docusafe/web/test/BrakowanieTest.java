package pl.compan.docusafe.web.test;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.KreatorProtokoluBrakowaniaScheduler;
import pl.compan.docusafe.parametrization.archiwum.ProtokolBrakowaniaLogic.ProtokolBrakowaniaStale;
import pl.compan.docusafe.service.simple.OrganizationUnitsImportService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class BrakowanieTest extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	/** The Constant log. */
	private static final Logger log = LoggerFactory
			.getLogger(BrakowanieTest.class);
	private String docId;

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("test1")
                .append(OpenHibernateSession.INSTANCE)
				.append(new Test1()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("slownik")
                .append(OpenHibernateSession.INSTANCE)
				.append(new SlownikTest()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("wywolajImport")
                .append(OpenHibernateSession.INSTANCE)
				.append(new ImportTest()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				System.out.println("BrakowanieTest.jsp");
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}

	private class Test1 implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				KreatorProtokoluBrakowaniaScheduler scheduler = new KreatorProtokoluBrakowaniaScheduler();
				scheduler.generujProtokolBrakowania();
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}

	private class ImportTest implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
                Runnable r = new Runnable(){

                    @Override
                    public void run() {
                        log.error("tick");
                        OrganizationUnitsImportService service = new OrganizationUnitsImportService();
                        service.importStrukturyOrg();
                        service.importUzytkownikow();
                    }
                };

                Thread th = new Thread(r);
                th.start();
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}

	private class SlownikTest implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				OfficeDocument document = OfficeDocument.find(Long
						.valueOf(docId));
				FieldsManager fm = document.getFieldsManager();
				pl.compan.docusafe.core.dockinds.field.Field field = fm
						.getField(ProtokolBrakowaniaStale.CN_DICT_BRAKOWANIE_POZYCJA);
				DictionaryField dictionaryField = (DictionaryField) fm
						.getDocumentKind()
						.getFieldByCn(
								ProtokolBrakowaniaStale.CN_DICT_BRAKOWANIE_POZYCJA);

				field.getType();
				/*
				 * for (Field f : field.getFields()) { f. }
				 */
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}
}
