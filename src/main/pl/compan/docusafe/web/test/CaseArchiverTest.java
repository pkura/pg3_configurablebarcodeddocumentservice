package pl.compan.docusafe.web.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.drools.util.StringUtils;
import org.jbpm.api.Execution;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.model.Transition;
import org.jbpm.api.task.Task;
import org.jbpm.pvm.internal.model.ActivityImpl;
import org.jbpm.pvm.internal.model.ExecutionImpl;
import org.jbpm.pvm.internal.model.ProcessDefinitionImpl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled.Wykaz;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class CaseArchiverTest extends EventActionSupport
{
	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(CaseArchiverTest.class);

	public static final String ERROR_VARIABLE= "ERROR_DESCRIPTION";

	private String folder_id;
	private String doc_id;
	private JBPMInfo jBPMInfo;
	
	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("test1").
			append(OpenHibernateSession.INSTANCE).
			append(new Test1()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("test2").
			append(OpenHibernateSession.INSTANCE).
			append(new Test2()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("testJbpm").
			append(OpenHibernateSession.INSTANCE).
			append(new TestJbpm()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}
	
	class JBPMInfoTransition{
		String activity;
		List<Transition> transitions;
		public JBPMInfoTransition(String ac, List<Transition> transitions)
		{
			this.activity= ac;
			this.transitions= transitions;
		}
		public String getActivity()
		{
			return activity;
		}
		public List<Transition> getTransitions()
		{
			return transitions;
		}
	}
	class JBPMInfo{
		ProcessInstance processInstance;
		List<Task> tasks;
		List<JBPMInfoTransition> transitions;
		List<String> vars;
		JBPMInfo(ProcessInstance processInstance, List<Task> tasks, List<JBPMInfoTransition> transitionsStruct, List<String> vars){
			this.processInstance= processInstance;
			this.tasks= tasks;
			this.transitions= transitionsStruct;
			this.vars= vars;
		}
		
		public ProcessInstance getProcessInstance()
		{
			return processInstance;
		}
		public List<Task> getTasks()
		{
			return tasks;
		}
		public List<JBPMInfoTransition> getTransitions()
		{
			return transitions;
		}
		public List<String> getVars()
		{
			return vars;
		}
		@Override
		public String toString(){
			return processInstance!=null? processInstance.getId() : "Null";
		}
		public String getProcessInstanceInfo(){
			String info= "";
			if(!StringUtils.isEmpty(processInstance.getId()))
				info+= " id= "+processInstance.getId();
			if(!StringUtils.isEmpty(processInstance.getName()))
				info+= " name= "+processInstance.getName();
			if(!StringUtils.isEmpty(processInstance.getState()))
				info+= " state= "+processInstance.getState();
			if(!StringUtils.isEmpty(processInstance.getKey()))
				info+= " key= "+processInstance.getKey();
			return info;
		}
	}

	private class TestJbpm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				// pobranie procesu po dokument id
				Long docId= Long.parseLong(doc_id.trim());

				Iterable<String> jbpmIds= Jbpm4ProcessLocator.processIds(docId);
				String processInstanceId= jbpmIds.iterator().next();
				ProcessInstance processInstance= Jbpm4Provider.getInstance().getExecutionService().findProcessInstanceById(processInstanceId);
				
				// tasks
				System.out.println("processInstance "+processInstance.getName());
				List<Task> tasks= Jbpm4Provider.getInstance().getTaskService().createTaskQuery().processInstanceId(processInstanceId).list();
				
				// transitions
				String processDefId= ((ExecutionImpl)processInstance.getProcessInstance()).getProcessDefinitionId();
				ProcessDefinitionImpl  pdef= (ProcessDefinitionImpl) Jbpm4Provider.getInstance().getRepositoryService().createProcessDefinitionQuery().processDefinitionId(processDefId).list().get(0);
				Set<String> activities= ((Execution)processInstance).findActiveActivityNames();
				
				List<JBPMInfoTransition> transitionsStruct = new ArrayList<JBPMInfoTransition>(activities.size());
				
				for(String ac : activities){
					ActivityImpl a= pdef.findActivity(ac);
					transitionsStruct.add(new JBPMInfoTransition(ac, (List<Transition>) a.getOutgoingTransitions()));
				}
				
				// zmienne procesu
			//	Object var1= Jbpm4Provider.getInstance().getExecutionService().getVariable(processInstanceId, ERROR_VARIABLE);//ExecutuinService -> getVaraible
			//	List<String> vars = new ArrayList<String>();
			//	if(var1!=null)
			//		vars.add(ERROR_VARIABLE+ " -- "+var1);
				
				//select * from [JBPM4_VARIABLE] where EXECUTION_ = (select EXECUTION_ from [dbo].[JBPM4_VARIABLE] 
				//where key_ = 'doc-id' and LONG_VALUE_ = doc_id)
				
				List<String> vars = new ArrayList<String>();
				Set<String> varnames= Jbpm4Provider.getInstance().getExecutionService().getVariableNames(processInstanceId);
				Map<String, Object> variables= Jbpm4Provider.getInstance().getExecutionService().getVariables(processInstanceId, varnames);
				for( Entry<String, Object> v : variables.entrySet()){
					vars.add( v.getKey() + " -- " +v.getValue() );
				}

				// finalizacja
				jBPMInfo= new JBPMInfo(processInstance, tasks, transitionsStruct, vars);
				
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}

	private class Test1 implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				// ustaw date: rok-2, styczen 01
				Calendar calendar= Calendar.getInstance();
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 2);
				calendar.set(Calendar.MONTH, /* 0-based */0);
				calendar.set(Calendar.DAY_OF_MONTH, /* 1-based */1);
				calendar.set(Calendar.HOUR_OF_DAY, /* 0-based */0);
				calendar.set(Calendar.MINUTE, /* 0-based */0);
				Date finishDate= calendar.getTime();

				NewCaseArchiverScheduled.generujWykazy(finishDate,null);
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}
	
	private class Test2 implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				Long folderId= Long.parseLong(folder_id);

				DSApi.context().begin();
				
				System.out.println("Probuje pobrac teczke o id: "+folderId);
				OfficeFolder folder = (OfficeFolder) DSApi.context().session().get(OfficeFolder.class, folderId);
				System.out.println("Teczka ma status "+folder.getArchiveStatus());
				
				Date archiveDate= Calendar.getInstance().getTime();
				Wykaz wykaz= new Wykaz(archiveDate, folder.getDivisionGuid());
				wykaz.setWykazNumber(NewCaseArchiverScheduled.getMaxWykazNumber()+1);
				Object[] dates= NewCaseArchiverScheduled.getDatesFromTo(folderId);
				wykaz.addFolder(folder, (Date)dates[0], (Date)dates[1]);
				
				///NewCaseArchiverScheduled.archiwizujTeczki(wykaz);
				
				DSUser odbiorca=DSUser.findByUsername("system");
				if( NewCaseArchiverScheduled.createWykazDoc(odbiorca.getName(), wykaz) )
				{
					NewCaseArchiverScheduled.ustawStatusTeczkom(wykaz);
					
					DSApi.context().commit();
				}
				
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
				DSApi.context()._rollback();
			}
		}
	}

	public void setFolder_id(String folder_id)
	{
		this.folder_id = folder_id;
	}

	public String getFolder_id()
	{
		return folder_id;
	}

	public String getDoc_id()
	{
		return doc_id;
	}

	public void setDoc_id(String doc_id)
	{
		this.doc_id= doc_id;
	}

	public void setjBPMInfo(JBPMInfo jBPMInfo)
	{
		this.jBPMInfo = jBPMInfo;
	}

	public JBPMInfo getjBPMInfo()
	{
		return jBPMInfo;
	}
}
