package pl.compan.docusafe.web;


import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.LogFactory;

/**
 * @author Piotr Komisarski
 */
public class MailService extends HttpServlet
{
    public void init()
    {
        MailSender ms = new MailSender();
        ms.setDaemon(true);
        ms.start();
    }
}

class MailSender extends Thread
{
    public void run()
    {
        while(true)
        {
            try
            {
                
                String[] emailList = {"komisarskip@beta.mini.pw.edu.pl"};
                
//                java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
                SendMailUsingAuthentication smtpMailSender = new SendMailUsingAuthentication();
//                try
//                {
//                    smtpMailSender.postMail( emailList, "SUBJECT", "MSG", "piotr.komisarski@com-pan.pl");
//                } catch (MessagingException ex)
//                {
//                }
                
                
                Thread.currentThread().sleep(600000);
            }
            catch (InterruptedException ex)
            {
            	LogFactory.getLog("eprint").debug("", ex);
            }
        }
    }
    
    
    public class SendMailUsingAuthentication
    {
        
        private static final String SMTP_HOST_NAME = "mail.com-pan.pl";
        private static final String SMTP_AUTH_USER = "piotr.komisarski";
        private static final String SMTP_AUTH_PWD  = "qwert6";
        
        private static final String emailMsgTxt      = "Online Order Confirmation Message. Also include the Tracking Number.";
        private static final String emailSubjectTxt  = "Order Confirmation Subject";
        private static final String emailFromAddress = "apy@gmail.com";
        
        // Add List of Email address to who email needs to be sent to
        
        
        
        
        public void postMail( String recipients[ ], String subject,
                String message , String from) throws MessagingException
        {
            
            //Set the host smtp address
            Properties props = new Properties();
            props.put("mail.smtp.host", SMTP_HOST_NAME);
            props.put("mail.smtp.auth", "true");
            
            Authenticator auth = new SMTPAuthenticator();
            Session session = Session.getDefaultInstance(props, auth);
            
            // create a message
            Message msg = new MimeMessage(session);
            
            // set the from and to address
            InternetAddress addressFrom = new InternetAddress(from);
            msg.setFrom(addressFrom);
            
            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++)
            {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);
            
            
            // Setting the Subject and Content Type
            msg.setSubject(subject);
            msg.setContent(message, "text/plain");
            Transport.send(msg);
        }
        
        
        /**
         * SimpleAuthenticator is used to do simple authentication
         * when the SMTP server requires it.
         */
        private class SMTPAuthenticator extends javax.mail.Authenticator
        {
            
            public PasswordAuthentication getPasswordAuthentication()
            {
                String username = SMTP_AUTH_USER;
                String password = SMTP_AUTH_PWD;
                return new PasswordAuthentication(username, password);
            }
        }
        
    }
    
    
}

















