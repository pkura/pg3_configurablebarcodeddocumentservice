package pl.compan.docusafe.web;

import java.io.Serializable;

public class TimePeriod implements Serializable 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private String startDate;
	private String endDate;
	
	public TimePeriod() 
	{
	}

	public TimePeriod(String startDate, String endDate) 
	{
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(String startDate) 
	{
		this.startDate = startDate;
	}

	public String getEndDate() 
	{
		return endDate;
	}

	public void setEndDate(String endDate) 
	{
		this.endDate = endDate;
	}
}
