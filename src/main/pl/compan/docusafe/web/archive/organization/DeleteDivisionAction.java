package pl.compan.docusafe.web.archive.organization;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jackrabbit.JackrabbitGroups;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DeleteDivisionAction.java,v 1.9 2007/05/08 15:30:34 mmanski Exp $
 */
public class DeleteDivisionAction extends EventProcessingAction
{
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        // ustawianie forwardu przed doDelete, poniewa� ten listener
        // przekierowuje do listy dzia��w po pomy�lnym usuni�ciu
        registerListener("doDelete").
            append(new ValidateDeleteListener()).
            append(new SetActionForwardListener("main")).
            append("delete", new Delete()).
            append(fillForm);

        registerListener("doCancel").
            append(new CancelListener());
    }

    /**
     * Usuwanie dzia�u.
     */
    class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final String divisionGuid = (String) ((DynaActionForm) event.getForm()).get("divisionGuid");
            final String substGuid = (String) ((DynaActionForm) event.getForm()).get("substGuid");

            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSApi.context().begin();

                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("Usuwanie dzia�u "+divisionGuid+" z przeniesieniem "
                        +"zawarto�ci do dzia�u "+substGuid);

                UserFactory.getInstance().deleteDivision(divisionGuid, substGuid);

                DSApi.context().commit();

                if(AvailabilityManager.isAvailable("jackrabbit.sync.organization")) {
                    try {
                        // TODO: doda� przeniesienie dokument� do drugiego dzia�u w Jackrabbit
                        JackrabbitGroups.instance().delete(divisionGuid);
                    } catch(Exception ex) {
                        throw new EdmException(EditOrganizationAction.JACKRABBIT_ERROR_STRING, ex);
                    }
                }

                final String returnDivisionGuid = "";//DSDivision.find(divisionGuid).getParent().getGuid();

                event.setForward(new ActionForward(
                    event.getMapping().findForward("organization/edit-organization").getPath()//+
                        //(StringUtils.isEmpty(returnDivisionGuid) ?
                        //"" : "?divisionGuid="+returnDivisionGuid)
                        , true));
            }
            catch (DivisionNotFoundException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            catch (Exception e)
            {
                event.getLog().error(e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class ValidateDeleteListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setForward(event.getMapping().findForward("main"));

            String divisionGuid = (String) ((DynaActionForm) event.getForm()).get("divisionGuid");
            if (StringUtils.isEmpty(divisionGuid))
                event.getErrors().add(sm.getString("deleteDivision.missingDivisionGuid"));

            if (event.getErrors().size() > 0)
                event.skip("delete");
        }
    }

    /**
     * Przycisk "Anuluj".
     */
    class CancelListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String divisionGuid = (String) ((DynaActionForm) event.getForm()).get("divisionGuid");

            event.setForward(new ActionForward(
                event.getMapping().findForward("organization/division").getPath()+
                    (StringUtils.isEmpty(divisionGuid) ?
                    "" : "?divisionGuid="+divisionGuid), true));
        }
    }


    class FillForm implements ActionListener
    {
        /**
         * Umieszczenie w formularzu nazwy usuwanego dzia�u.
         */
        public void actionPerformed(ActionEvent event)
        {
            if (event.getForward() == null)
                event.setForward(event.getMapping().findForward("main"));

            String divisionGuid = (String) ((DynaActionForm) event.getForm()).get("divisionGuid");

            if (StringUtils.isEmpty(divisionGuid))
                return;

            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSDivision division = DSDivision.find(divisionGuid);
                event.getRequest().setAttribute("division", division);
                event.getRequest().setAttribute("canDelete",
                    UserFactory.getInstance().canDelete(division));
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
}
