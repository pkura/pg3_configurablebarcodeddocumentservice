package pl.compan.docusafe.web.archive.organization;

import java.util.Arrays;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.OrganizationTree;
import std.fun;
import std.lambda;

/**
 * Edycja r�l przypisanych do dzia�u.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionRolesAction.java,v 1.12 2007/11/27 12:11:40 mariuszk Exp $
 */
public class DivisionRolesAction extends EventProcessingAction
{
    public static final String FORWARD = "organization/division-roles";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(new SetActionForwardListener("main")).
            append(fillForm);

        registerListener("doUpdate").
            append(new SetActionForwardListener("main")).
            append(new Update()).
            append(fillForm);

        registerListener("doReturn").
            append(new Return());
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long[] roleIds = (Long[]) event.getDynaForm().get("officeRoles");
            if (roleIds == null)
                roleIds = new Long[0];
            Arrays.sort(roleIds);

            final String guid = (String) event.getDynaForm().get("divisionGuid");

            DSContext ctx = null;
            boolean success = false;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                DSDivision division = DSDivision.find(guid);
                Role[] roles = Role.list();

                for (int i=0, n=roles.length; i < n; i++)
                {
                    if (Arrays.binarySearch(roleIds, roles[i].getId()) >= 0)
                    {
                        roles[i].getDivisions().add(division.getGuid());
                    }
                    else
                    {
                        roles[i].getDivisions().remove(division.getGuid());
                    }
                }

                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                cache.invalidate(division);

                ctx.commit();
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }

            if (success)
            {
                event.setForward(new ActionForward(EditOrganizationAction.getLink(
                    event.getRequest(), guid), true));
            }
        }
    }

    /**
     * Powr�t do {@link EditOrganizationAction} w tym samym
     * punkcie struktury.
     */
    private class Return implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");

            event.setForward(new ActionForward(EditOrganizationAction.getLink(
                event.getRequest(), guid), true));
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            if (!Configuration.coreOfficeAvailable())
                return;

            final String guid = (String) event.getDynaForm().get("divisionGuid");

            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DSDivision division = null;
                if (!StringUtils.isEmpty(guid))
                    division = DSDivision.find(guid);
                else
                    division = DSDivision.find(DSDivision.ROOT_GUID);

                UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        return event.getRequest().getContextPath()+
                            EditOrganizationAction.getLink(
                                event.getRequest(), ((DSDivision) element).getGuid());
                    }
                };

                event.getRequest().setAttribute("tree",
                    OrganizationTree.newTree(division, urlVisitor, event.getRequest(),
                        true, true, false).generateTree());

                Collection<DynaBean> roleBeans = fun.map(
                    Role.list(), new lambda<Role, DynaBean>()
                {
                    public DynaBean act(Role role)
                    {
                        DynaBean bean = DynaBeans.bean(DynaBeans.checkbox);
                        bean.set("value", role.getId());
                        bean.set("label", role.getName());
                        bean.set("checked", Boolean.valueOf(role.getDivisions().contains(guid)));
                        return bean;
                    }
                });

                if (division.isPosition() || division.isGroup())
                    event.getRequest().setAttribute("officeRoles", roleBeans);
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(HttpServletRequest request, String divisionGuid)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (divisionGuid != null ? "?divisionGuid="+divisionGuid : "");
    }
}
