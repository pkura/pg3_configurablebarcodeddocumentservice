package pl.compan.docusafe.web.archive.organization;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.OrganizationTree;

import javax.servlet.http.HttpServletRequest;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RenameDivisionAction.java,v 1.10 2007/04/17 14:39:23 lk Exp $
 */
public class RenameDivisionAction extends EventProcessingAction
{
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String FORWARD = "organization/rename-division";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doRename").
            append(new SetActionForwardListener("main")).
            append(new ValidateRename()).
            append("rename", new Rename()).
            append("fillForm", fillForm);

        registerListener("doReturn").
            append(new Return());
    }

    private class ValidateRename implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");
            String code = (String) event.getDynaForm().get("code");
            String name = (String) event.getDynaForm().get("name");

            if (StringUtils.isEmpty(guid))
                event.getErrors().add(sm.getString("renameDivision.missingGuid"));

            if (StringUtils.isEmpty(name))
                event.getErrors().add(sm.getString("renameDivision.missingName"));

            if (!StringUtils.isEmpty(code))
            {
                for (int i=0, n=code.length(); i < n; i++)
                {
                    char c = code.charAt(i);
                    if (Character.isWhitespace(c))
//                    if (!(c >= 'A' && c <= 'Z') && !(c >= 'a' && c <= 'z') &&
//                        !(c >= '0' && c <= '9') && c != '-')
                    {
                        event.getErrors().add(sm.getString("editOrganization.invalidDivisionCode"));
                    }
                }
            }

            if (event.getErrors().size() > 0)
                event.skip("rename");
        }
    }

    private class Rename implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");
            String code = (String) event.getDynaForm().get("code");
            String name = (String) event.getDynaForm().get("name");

            boolean success = false;
            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSApi.context().begin();

                DSDivision division = DSDivision.find(guid);

                if (Configuration.coreOfficeAvailable() &&
                    division.isDivision() && StringUtils.isEmpty(code))
                    throw new EdmException(sm.getString("editOrganization.missingDivisionCode"));

                division.setName(name);
                division.setCode(TextUtils.trimmedStringOrNull(code));

                division.update();

                DSApi.context().commit();

                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }

            if (success)
            {
                event.setForward(new ActionForward(EditOrganizationAction.getLink(
                    event.getRequest(), guid), true));
                event.skip("fillForm");
            }
        }
    }

    /**
     * Powr�t do {@link EditOrganizationAction} w tym samym
     * punkcie struktury.
     */
    private class Return implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");

            event.setForward(new ActionForward(EditOrganizationAction.getLink(
                event.getRequest(), guid), true));
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");

            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DSDivision division = null;
                if (!StringUtils.isEmpty(guid))
                    division = DSDivision.find(guid);
                else
                    division = DSDivision.find(DSDivision.ROOT_GUID);

                event.getDynaForm().set("name", division.getName());
                event.getDynaForm().set("code", division.getCode());
/*
                }
                else
                {
                    SettingsHelper sh = ctx.getSettingsHelper();
                    event.getDynaForm().set("name",
                        sh.getProperty(SettingsHelper.ORGANIZATION_NAME));
                }
*/

                UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        return event.getRequest().getContextPath()+
                            EditOrganizationAction.getLink(
                                event.getRequest(), ((DSDivision) element).getGuid());
                    }
                };

                event.getRequest().setAttribute("tree",
                    OrganizationTree.newTree(division, urlVisitor, event.getRequest(),
                        true, true, true).generateTree());


                event.getRequest().setAttribute("codeNeeded",
                    Boolean.valueOf(division.isDivision()));
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(HttpServletRequest request, String guid)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+"?divisionGuid="+guid;
    }
}
