package pl.compan.docusafe.web.archive.organization;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.UrlVisitor;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.web.tree.OrganizationTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:komisarskip@beta.mini.pw.edu.pl">Piotr Komisarski</a>
 */


public class MoveDivisionAction extends EventActionSupport
{
	static final Logger log = LoggerFactory.getLogger(MoveDivisionAction.class);
	private String guid;
	private String firstDivisionGuid;
	private String treeHtml;
	private String name;
	private boolean moved;


	protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
        registerListener(DEFAULT_ACTION).
        	append(OpenHibernateSession.INSTANCE).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doMove").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Move()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);

    }
    
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSDivision targetDivision;
                if(name==null)
                {
	                try
	                {
	                    if (firstDivisionGuid != null)
	                        targetDivision = DSDivision.find(firstDivisionGuid);
	                    else 
	                        throw new EdmException("firstdivisionguid = null");
	                }
	                catch (DivisionNotFoundException e)
	                {
	                    throw new EdmException("Nieprawidlowy dzial/stanowisko");
	                }
	                String username = " " + targetDivision.getName();
	                String divis = targetDivision.getDivisionType();
	                name = (divis.equals("division")?"dzia�":(divis.equals("position")?"pozycj�":(divis.equals("swimlane")?"swimlane":"grup�"))) + username;
                }
               
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            String url = HttpUtils.makeUrl(
                                ServletActionContext.getRequest().getContextPath()+"/organization/move-division.action",
                                new Object[] {
                                    "firstDivisionGuid", firstDivisionGuid,
                                    "guid", ((DSDivision) element).getGuid()
                                });
                            return url;
                        }
                        catch (Exception e)
                        {
                            return null;
                        }
                    }
                };
                  
                try
                {
                    if (guid != null)
                    {
                        targetDivision = DSDivision.find(guid);
                        if (guid.equals(firstDivisionGuid))
                        	  targetDivision = targetDivision.getParent();
                    }
                    else 
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                        guid = targetDivision.getGuid();
                }
                catch (DivisionNotFoundException e)
                {
                    throw new EdmException("Nieprawidlowy dzial/stanowisko");
                }
                  
                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true, false, false);

                treeHtml = tree.generateTree();
                  
                  
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
	
	private class Move implements ActionListener
	{
		public void actionPerformed(ActionEvent event) {
			try
			{
				PreparedStatement ps = null;
				try
				{
					DSDivision division;
					DSDivision targetDivision;
					division = DSDivision.find(firstDivisionGuid);
					targetDivision = DSDivision.find(guid);
					ps = DSApi.context().prepareStatement("SELECT ID FROM DS_DIVISION WHERE GUID=?");
					ps.setString(1, targetDivision.getGuid());
					ResultSet rs = ps.executeQuery();
					boolean exception = false;
					Long id = null;
					if (rs.next())
					{
						id = rs.getLong(1);						
					}
					else
					{
						exception = true;
					}
					rs.close();
					DSApi.context().closeStatement(ps);
					if (exception)
					{
						throw new EdmException("Nieprawidlowy dzial/stanowisko");
					}
					
					if(DSApi.context().session().createCriteria(DSDivision.class).
						add(Restrictions.eq("parent", targetDivision)).
						add(Restrictions.eq("name", division.getName())).
						uniqueResult()!=null){
						throw new EdmException(StringManager.getManager(Constants.Package).getString("WWybranymDzialeIstniejeJuzDzialONazwie",division.getName()));
					}										
					ps = DSApi.context().prepareStatement("UPDATE DS_DIVISION SET PARENT_ID=? WHERE GUID=?");
					ps.setLong(1, id);
					ps.setString(2, division.getGuid());
					ps.execute();
					DSApi.context().closeStatement(ps);					
					moved = true;
		    	}
		    	catch (DivisionNotFoundException e) 
		    	{
		    		log.error("e",e);
					throw new EdmException(e.getMessage()); 
				}
		    	catch (SQLException e) {
		    		log.error("e",e);
					throw new EdmException(e); 
				}
		    	finally
		    	{
		    		DSApi.context().closeStatement(ps);
		    	}
			}
		  catch (EdmException e) {
			  DSApi.context().setRollbackOnly();
              addActionError(e.getMessage());
			}
		}	
	}
	
	public String getName() {
		return name;
	}

	public String getTreeHtml() {
		return treeHtml;
	}

	public String getFirstDivisionGuid() {
		return firstDivisionGuid;
	}
	
	public void setFirstDivisionGuid(String firstDivisionGuid) {
		this.firstDivisionGuid = firstDivisionGuid;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public boolean getMoved() {
		return moved;
	}

	
	
}
