package pl.compan.docusafe.web.archive.organization;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.dockinds.field.DSDivisionEnumField;
import pl.compan.docusafe.core.jackrabbit.JackrabbitGroups;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;

/**
 * Edycja struktury organizacji.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditOrganizationAction.java,v 1.49 2009/09/23 08:05:59 pecet1 Exp $
 */
public class EditOrganizationAction extends EventProcessingAction
{
    private static final Log log = LogFactory.getLog(EditOrganizationAction.class);
    public static final String JACKRABBIT_ERROR_STRING = "B��d w repozytorium Jackrabbit";

    private static boolean extendedTree = Configuration.additionAvailable("extendedTree");

    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String FORWARD = "organization/edit-organization";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doCreateDivision").
            append(new ValidateCreateDivision()).
            append("createSubdivision", new CreateDivision()).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doAddUsers").
            append(new SetActionForwardListener("main")).
            append("addUsers", new AddUsers()).
            append("fillForm", fillForm);

        registerListener("doAddBackup").
	        append(new SetActionForwardListener("main")).
	        append("addBackup", new AddBackup()).
	        append("fillForm", fillForm);

        registerListener("doRemoveUsers").
            append(new RemoveUsers()).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doDeleteDivision").
            append(new SetActionForwardListener("main")).
            append(new DeleteDivision()).
            append("fillForm", fillForm);
    }

    private class ValidateCreateDivision implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String name = (String) event.getDynaForm().get("name");
            String code = (String) event.getDynaForm().get("code");
            String divisionType = (String) event.getDynaForm().get("divisionType");

            if (StringUtils.isEmpty(name))
                event.getErrors().add(sm.getString("editOrganization.missingDivisionName"));

            if (!DSDivision.TYPE_DIVISION.equals(divisionType) &&
                !DSDivision.TYPE_GROUP.equals(divisionType) &&
                !DSDivision.TYPE_POSITION.equals(divisionType) &&
                !DSDivision.TYPE_SWIMLANE.equals(divisionType))
                event.getErrors().add(sm.getString("editOrganization.missingDivisionType"));

            // kod dzia�u wymagany tylko dla kancelarii ps. nie ma gwiazdki a prawie wszyscy klienci zglaszaja ze im to nie potrzebne i zeby usunac 
//            if (Configuration.coreOfficeAvailable() &&
//                StringUtils.isEmpty(code) &&
//                DSDivision.TYPE_DIVISION.equals(divisionType))
//                event.getErrors().add(sm.getString("editOrganization.missingDivisionCode"));

            if (!StringUtils.isEmpty(code))
            {
                for (int i=0, n=code.length(); i < n; i++)
                {
                    char c = code.charAt(i);
                    if (Character.isWhitespace(c))
//                    if (!(c >= 'A' && c <= 'Z') && !(c >= 'a' && c <= 'z') &&
//                        !(c >= '0' && c <= '9') && c != '-')
                    {
                        event.getErrors().add(sm.getString("editOrganization.invalidDivisionCode"));
                    }
                }
            }

            if (event.getErrors().size() > 0)
                event.skip("createSubdivision");
        }
    }

    private class CreateDivision implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String name = (String) event.getDynaForm().get("name");
            String code = TextUtils.trimmedStringOrNull((String) event.getDynaForm().get("code"));
            String divisionType = (String) event.getDynaForm().get("divisionType");
            final String guid = (String) event.getDynaForm().get("divisionGuid");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                // bie��cy dzia�
                DSDivision division = null;

                DSDivision createdDivision = null;

                // pobieram bie��cy dzia� (o ile istnieje)
                if (!StringUtils.isEmpty(guid))
                {
                    division = DSDivision.find(guid);
                }
                else
                {
                    division = DSDivision.find(DSDivision.ROOT_GUID);
                }

                if (DSDivision.TYPE_DIVISION.equals(divisionType))
                {
                    createdDivision = division.createDivision(name, code);
                }
                else if (DSDivision.TYPE_GROUP.equals(divisionType))
                {
                    createdDivision = division.createGroup(name);
                }
                else if (DSDivision.TYPE_POSITION.equals(divisionType))
                {
                    createdDivision = division.createPosition(name);
                }
                else if (DSDivision.TYPE_SWIMLANE.equals(divisionType))
                {
                    createdDivision = division.createSwimlane(name);
                }

//                Division newDivision = new Division();
//                newDivision.setName(name);
//                newDivision.setParent(division);
//                newDivision.setDivisionType(divisionType);
//
//                if (Division.TYPE_DIVISION.equals(divisionType))
//                    newDivision.setCode(StringUtils.isEmpty(code) ? null : code);
//
//                oh.createSubdivision(newDivision);

                ctx.commit();
                DSDivisionEnumField.reloadForAll();

                if(createdDivision != null && AvailabilityManager.isAvailable("jackrabbit.sync.organization")) {
                    try {
                        JackrabbitGroups.instance().createOrUpdate(createdDivision);
                    } catch(Exception ex) {
                        throw new EdmException(JACKRABBIT_ERROR_STRING, ex);
                    }
                }

                event.getDynaForm().set("name", null);
                event.getDynaForm().set("code", null);
            }
            catch (EdmException e)
            {
                log.error("[CreateDivision] error", e);
                ctx.setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class ValidateAddUser implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String guid = (String) event.getDynaForm().get("divisionGuid");
            String username = (String) event.getDynaForm().get("username");

            // wprawdzie na stronie g��wnej kategorii nie ma przycisku
            // dodawania u�ytkownik�w, ale dodatkowe zabezpieczenie nie
            // zawadzi
            if (StringUtils.isEmpty(guid))
            {
                event.getErrors().add(sm.getString("editOrganization.missingDivisionGuid"));
            }

            if (username == null || !username.endsWith("/user") ||
                username.length() <= "/user".length())
            {
                event.getErrors().add(sm.getString("divisionAction.mustSelectUser"));
            }

            if (event.getErrors().size() > 0)
                event.skip("addUser");
        }
    }

    public class Elsewhere {
        private DSUser user;
        private String[] divisionPaths;

        public Elsewhere(DSUser user, String[] divisionPaths)
        {
            this.user = user;
            this.divisionPaths = divisionPaths;
        }

        public DSUser getUser()
        {
            return user;
        }

        public String[] getDivisionPaths()
        {
            return divisionPaths;
        }
    }

    private class AddUsers implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	Add(event, false);
        }
    }

    private void Add(ActionEvent event, boolean backup){
    	final String guid = (String) event.getDynaForm().get("divisionGuid");
        String[] confirmedUsernames = (String[]) event.getDynaForm().get("confirmedUsernames");
        Boolean confirmAddUsers = (Boolean) event.getDynaForm().get("confirmAddUsers");
        final String[] usernames = (String[]) event.getDynaForm().get("usernames");

        event.getRequest().setAttribute("returnUrl", event.getRequest().getContextPath()+
            getLink(event.getRequest(), guid));

        if (usernames != null && usernames.length > 0 &&
            (confirmAddUsers == null || !confirmAddUsers.booleanValue()))
        {

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                // u�ytkownicy, kt�rzy s� tylko w jednym dziale
                // i z g�ry s� potwierdzeni
                List preconfirmedUsernames = new ArrayList(usernames.length);
                List elsewheres = new ArrayList(usernames.length);

                for (int i=0; i < usernames.length; i++)
                {
                    String username = usernames[i].substring(0, usernames[i].length()-"/user".length());
                    DSUser user = DSUser.findByUsername(username);

                    // tylko dzia�y, w kt�rych u�ytkownik znajduje si� bezpo�rednio
                    final DSDivision[] divisions = user.getOriginalDivisions();

                    // sprawdzam, czy u�ytkownik jest w innych DZIA�ACH
                    List paths = new ArrayList(divisions.length);
                    for (int d=0; d < divisions.length; d++)
                    {
                        if (divisions[d].isDivision() && !divisions[d].getGuid().equals(guid))
                        {
                            paths.add(divisions[d].getPrettyPath());
                        }
                    }

                    // u�ytkownik jest w co najmniej jednym innym dziale
                    if (paths.size() > 0)
                    {
                        elsewheres.add(new Elsewhere(user, (String[]) paths.toArray(new String[paths.size()])));
                    }
                    else
                    {
                        preconfirmedUsernames.add(user.getName()+"/user");
                    }
                }

                // je�eli kt�ry� z u�ytkownik�w jest ju� w innym dziale,
                // operator jest pytany, czy doda� go r�wnie� do bie��cego dzia�u
                if (elsewheres.size() > 0)
                {
                    event.getRequest().setAttribute("confirmedUsernames", preconfirmedUsernames);
                    event.getRequest().setAttribute("usersToConfirm", elsewheres);
                    event.setForward(event.getMapping().findForward("confirmAddUsers"));
                }
                else
                {
                    confirmedUsernames = (String[]) preconfirmedUsernames.toArray(new String[preconfirmedUsernames.size()]);
                    confirmAddUsers = Boolean.TRUE;
                }
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }


        if (confirmAddUsers != null && confirmAddUsers.booleanValue() &&
            confirmedUsernames != null && confirmedUsernames.length > 0)
        {

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();
				DataMartEventBuilder.create();

                final DSDivision division = DSDivision.find(guid);

                List<DSUser> addedUsers = new ArrayList<DSUser>();

                // dodawanie u�ytkownik�w do dzia�u
                for (int i=0; i < confirmedUsernames.length; i++)
                {
                    String username = confirmedUsernames[i].substring(0, confirmedUsernames[i].length()-"/user".length());
                    DSUser user = DSUser.findByUsername(username);
                    if (!division.addUser(user,backup))
                    {
                        event.getMessages().add(sm.getString("UzytkownikZnajdowalSieJuzWtymDziale", user.asFirstnameLastname()));
                    }
                    else
                    {
                        addedUsers.add(user);
                        event.getMessages().add(sm.getString("UzytkownikZostalDodanyDoDzialu", user.asFirstnameLastname()));
                    }
                }

                String bokGuid = GlobalPreferences.getBokDivisionGuid();
                // tworzenie mapowa� w wewn�trznym workflow
                if (Configuration.coreOfficeAvailable() &&
                    (bokGuid == null || !bokGuid.equals(division.getGuid())))
                {
                    // dzia�y, na kt�re u�ytkownik powinien by�
                    // mapowany w workflow
                    List guids = new ArrayList(4);
                    guids.add(division.getGuid());
                    if (division.isPosition())
                        guids.add(division.getParent().getGuid());

                    for (int i=0; i < confirmedUsernames.length; i++)
                    {

                        String username = confirmedUsernames[i].substring(0, confirmedUsernames[i].length()-"/user".length());
                        /*DSUser user = */DSUser.findByUsername(username);

                        //division.addUser(user);
                        for (Iterator iter=guids.iterator(); iter.hasNext(); )
                        {
                            WorkflowFactory.addParticipantToUsernameMapping(null, null,
                                "ds_guid_"+iter.next(), username);
                        }
                    }

                }
				
//				DataMartEventBuilder.get().log();
                ctx.commit();
                DSDivisionEnumField.reloadForAll();

                if(AvailabilityManager.isAvailable("jackrabbit.sync.organization")) {
                    try {
                        addedUsers = filterUsers(addedUsers);
                        JackrabbitGroups.instance().addUsers(guid, addedUsers);
                    } catch(Exception ex) {
                        throw new EdmException(JACKRABBIT_ERROR_STRING, ex);
                    }
                }

                event.getDynaForm().set("usernames", null);
                event.getDynaForm().set("doAddUsers", null);
                event.getDynaForm().set("doAddBackup", null);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
                log.error(e);
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private List<DSUser> filterUsers(List<DSUser> addedUsers) {
        return FluentIterable.from(addedUsers).filter(new Predicate<DSUser>() {
            @Override
            public boolean apply(DSUser user) {
                try {
                    return DSApi.context().hasPermission(user, DSPermission.PISMO_KOMORKA_PODGLAD);
                } catch (EdmException e) {
                    log.error("[filterUsers] cannot determine permission error, user.name = "+user.getName(), e);
                    return false;
                }
            }
        }).toList();
    }

    private class AddBackup implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	Add(event, true);
        }
    }

    private class RemoveUsers implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String divisionGuid = (String) event.getDynaForm().get("divisionGuid");
            String[] usernames = (String[]) event.getDynaForm().get("selectedUsers");

            if (StringUtils.isEmpty(divisionGuid))
            {
                if (log.isDebugEnabled())
                    log.debug("divisionGuid="+divisionGuid);
                return;
            }

            if (usernames == null || usernames.length == 0)
            {
                if (log.isDebugEnabled())
                    log.debug("usernames="+usernames);
                return;
            }

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();
				DataMartEventBuilder.create();

                List<DSUser> removedUsers = new ArrayList<DSUser>();

                DSDivision division = DSDivision.find(divisionGuid);
                for (int i=0, n=usernames.length; i < n; i++)
                {
                    try
                    {
                        DSUser user = DSUser.findByUsername(usernames[i]);
                        division.removeUser(user);
                        removedUsers.add(user);
                    }
                    catch (UserNotFoundException e)
                    {
                        log.warn("Nie znaleziono u�ytkownika "+usernames[i], e);
                        continue;
                    }
                }

                // usuwanie mapowa� w wewn�trznym workflow
                if (Configuration.coreOfficeAvailable())
                {

                    for (int i=0, n=usernames.length; i < n; i++)
                    {
                        WorkflowFactory.removeParticipantToUsernameMapping(null, null,
                            "ds_guid_"+division.getGuid(), usernames[i]);
                        if (division.isPosition())
                            WorkflowFactory.removeParticipantToUsernameMapping(null, null,
                                "ds_guid_"+division.getParent().getGuid(), usernames[i]);
                    }

                }

//				DataMartEventBuilder.get().log();
                ctx.commit();
                DSDivisionEnumField.reloadForAll();

                if(AvailabilityManager.isAvailable("jackrabbit.sync.organization")) {
                    try {
                        JackrabbitGroups.instance().deleteUsers(divisionGuid, removedUsers);
                    } catch(Exception ex) {
                        throw new EdmException(JACKRABBIT_ERROR_STRING, ex);
                    }
                }

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class DeleteDivision implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String divisionGuid = (String) event.getDynaForm().get("divisionGuid");

            if (StringUtils.isEmpty(divisionGuid))
                return;

            DSContext ctx = null;
            String returnGuid = null;
            boolean success = false;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                DSDivision division = DSDivision.find(divisionGuid);

                // szukam identyfikatora dzia�u nadrz�dnego, aby przekierowa�
                // tam u�ytkownika po usuni�ciu dzia�u bie��cego
                DSDivision upwards = division.getParent();
                if (upwards != null)
                    returnGuid = upwards.getGuid();

                // TODO: wywali� to wszystko, usuwanie w DeleteDivisionAction
                UserFactory.getInstance().deleteDivision(divisionGuid, divisionGuid);

                ctx.commit();
                DSDivisionEnumField.reloadForAll();

                success = true;
            }
            catch (EdmException e)
            {
                ctx.setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
                if (success)
                {
                    event.setForward(new ActionForward(EditOrganizationAction.getLink(
                        event.getRequest(), returnGuid), true));
                    event.skip("fillForm");
                }
            }
        }
    }

    private class FillForm implements ActionListener
    {
        class ModelRuntimeException extends RuntimeException
        {
            public ModelRuntimeException(Throwable cause)
            {
                super(cause);
            }
        }

        public void actionPerformed(final ActionEvent event)
        {
            final String guid = (String) event.getDynaForm().get("divisionGuid");
            try {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DSDivision division = null;

                if (!StringUtils.isEmpty(guid)) {
					try {
						division = DSDivision.find(guid);
					} catch (DivisionNotFoundException e) {
					}
				}

                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("division="+division);

                if (division == null) {
					division = DSDivision.find(DSDivision.ROOT_GUID);
				}

                event.getRequest().setAttribute("divisionGuid", division.getGuid());

                if (!extendedTree) {
					UrlVisitor urlVisitor = new UrlVisitor() {
						public String getUrl(Object element) {
							return event.getRequest().getContextPath()
									+ getLink(event.getRequest(),
											((DSDivision) element).getGuid());
						}
					};

					event.getRequest().setAttribute(
							"tree",
							OrganizationTree.newTree(division, urlVisitor,
									event.getRequest(), true, true, true)
									.generateTree());
				} else {
					OrganizationUrlProvider provider = new OrganizationUrlProvider() {
						public String getUrl(DSDivision div, DSUser user) {
							return event.getRequest().getContextPath()
									+ getLink(event.getRequest(), div.getGuid());
						}
					};

					event.getRequest().setAttribute(
							"etree",
							ExtendedOrganizationTree.createAdminTree(
									division, null, provider,
									event.getRequest().getContextPath())
									.generateTree());
				}


                // u�ytkownicy

                if (division != null)
                {
                    DSUser[] users = division.getOriginalUsersWithoutBackup();
                    DSUser[] usersBackup = division.getOriginalBackupUsers();
                    List userBeans = new ArrayList(users.length);
                    List userBackupBeans = new ArrayList(usersBackup.length);
                    for (int i=0, n=users.length; i < n; i++)
                    {
                        if (event.getLog().isDebugEnabled())
                            event.getLog().debug(users[i].getName()+" w dziale "+division.getName());
                        DynaBean bean = DynaBeans.bean(DynaBeans.user);
                        bean.set("name", users[i].getName());
                        bean.set("description", users[i].asFirstnameLastnameName());
                        userBeans.add(bean);


                    }
                    for (int i=0, n=usersBackup.length; i < n; i++)
                    {
                        if (event.getLog().isDebugEnabled())
                            event.getLog().debug(usersBackup[i].getName()+"backup w dziale "+division.getName());
                        DynaBean bean = DynaBeans.bean(DynaBeans.user);
                        bean.set("name", usersBackup[i].getName());
                        bean.set("description", usersBackup[i].asFirstnameLastnameName());
                        userBackupBeans.add(bean);
                    }
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("userBeans="+userBeans);
                    event.getRequest().setAttribute("userBeans", userBeans);
                    event.getRequest().setAttribute("userBackupBeans", userBackupBeans);
                }

                event.getRequest().setAttribute("renameDivisionLink",
                    RenameDivisionAction.getLink(event.getRequest(), division.getGuid()));

                event.getRequest().setAttribute("setRolesLink",
                    DivisionRolesAction.getLink(event.getRequest(), division.getGuid()));

                // mo�na usun�� ka�dy dzia� z wyj�tkiem g��wnego
                event.getRequest().setAttribute("canDeleteDivision",
                    Boolean.valueOf(!division.isRoot()));

                // role mo�na przypisywa� tylko stanowiskom i grupom
                // i tylko, je�eli modu� kancelarii jest dost�pny
                event.getRequest().setAttribute("canSetRoles",
                    Boolean.valueOf(
                        (division.isPosition() || division.isGroup()) &&
                        Configuration.coreOfficeAvailable()));

                // u�ytkownicy mog� znale�� si� w ka�dym dziale z wyj�tkiem g��wnego
                event.getRequest().setAttribute("enableAddingUsers",
                    Boolean.valueOf(!division.isRoot()));
                // dzia�y mog� znale�� si� tylko w innych dzia�ach (w tym w g��wnym)
                event.getRequest().setAttribute("enableAddingDivisions",
                    Boolean.valueOf(division.isDivision()));
                // stanowiska mog� znale�� si� tylko w dzia�ach (nie w g��wnym)
                event.getRequest().setAttribute("enableAddingPositions",
                    Boolean.valueOf(!division.isRoot() && division.isDivision()));
                // grupy tylko w dziale g��wnym
                // lub w dowolnym, je�eli dost�pne jest jedynie Archiwum
                event.getRequest().setAttribute("enableAddingGroups", true);

                // swimlane'y mo�na dodawa� wsz�dzie, ale tylko jak modu� Workflow
                event.getRequest().setAttribute("enableAddingSwimlanes",
                    Boolean.valueOf(Configuration.workflowAvailable()));

                event.getRequest().setAttribute("addUserLink",
                    event.getRequest().getContextPath()+
                    event.getMapping().findForward("security/pick-subject").getPath()+
                    "?constraints=users&backup=false");
                event.getRequest().setAttribute("addBackupLink",
                        event.getRequest().getContextPath()+
                        event.getMapping().findForward("security/pick-subject").getPath()+
                        "?constraints=users&backup=true");

                event.getDynaForm().set("wfParticipantId",
                    "ds_guid_"+division.getGuid());

            } catch (ModelRuntimeException e) {
				event.getErrors().add(e.getMessage());
			} catch (EdmException e) {
				event.getErrors().add(e.getMessage());
			} finally {
				DSApi._close();
			}
        }
    }

    public static String getLink(HttpServletRequest request, String divisionGuid)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (divisionGuid != null ? "?divisionGuid=" + divisionGuid + "#" + divisionGuid : "");
    }
}
