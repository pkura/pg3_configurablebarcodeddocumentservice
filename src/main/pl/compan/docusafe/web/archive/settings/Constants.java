package pl.compan.docusafe.web.archive.settings;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Constants.java,v 1.1 2004/05/16 20:10:44 administrator Exp $
 */
public final class Constants
{
    public static final String Package;
    static
    {
        Package = Constants.class.getPackage().getName();
    }
}
