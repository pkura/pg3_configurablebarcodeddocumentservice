package pl.compan.docusafe.web.archive.settings;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.index.IndexWriter;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PasswordPolicy;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSDivisionFilter;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.FullNamePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.parametrization.prosika.ProsikaArchiveDocument;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.sms.SMSIntelisoDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.imgwwaw.exports.SimpleErpExportFactory;
import std.filter;
import std.fun;
import com.google.gson.Gson;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.field.RtfPageNumber;
import com.lowagie.text.rtf.field.RtfTotalPageNumber;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;
import com.opensymphony.webwork.ServletActionContext;
import java.io.FileNotFoundException;
import java.util.HashMap;
import org.dom4j.DocumentFactory;
import org.smartparam.engine.config.ParamEngineConfig;
import org.smartparam.engine.config.ParamEngineConfigBuilder;
import org.smartparam.engine.config.ParamEngineFactory;
import org.smartparam.engine.core.ParamEngine;
import org.smartparam.repository.fs.FSParamRepository;
import pl.compan.docusafe.core.cache.CacheBuilderConfiguration;
import pl.compan.docusafe.core.cache.CacheRegistry;
import pl.compan.docusafe.core.cache.SitemapCacheBuilder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.parametrization.ic.IcMpkDic;
import pl.compan.docusafe.parametrization.ic.IcPorcessBean;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.util.axis.simple.SimpleManager;
import pl.compan.docusafe.web.CssService;
import pl.compan.docusafe.ws.simple.InvoiceServiceStub;
import pl.compan.docusafe.ws.simple.SupplierServiceStub;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.DictionaryItem;
import com.lowagie.text.DocumentException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ApplicationSettingsAction.java,v 1.59 2010/08/13 11:02:12 mariuszk Exp $
 */
public class ApplicationSettingsAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(ApplicationSettingsAction.class);
    //private static final StringManager sm = StringManager.getManager(Constants.Package);
    //public static final String FORWARD = "admin/application-settings";
 
    // @EXPORT
	private final static StringManager sm = 
		GlobalPreferences.loadPropertiesFile(AcceptancesManager.class.getPackage().getName(),null);
    private List users;
    /**
     * Lista grup.
     */
    private DSDivision[] groups;
    private Map<String,String> docusafeTypes;
    
    private DateFormat rtfDateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm");
    // @EXPORT/@IMPORT
    private String admin;
    private String instantAssignmentGroup;
    private boolean officeNumbersFromAuxJournals;
    private boolean wfProcessForClonedInDocuments;
    private String customer;
    //private boolean fillDefaultOrganization;
    private String barcodePrefix;
    private String barcodeSuffix;
    private boolean useBarcodes;
    private String docusafeInstanceType;
    private boolean emergency;
    private String browsersInfo;

    private int userCount;
    
    public static String getName(){
		 Subject subject = (Subject)
           ((HttpServletRequest) ServletActionContext.getRequest()).getSession(true).
           getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
           String username = null;
           String fullname = null;
           String substituted = null;

           if (subject != null)
           {
           	Set fullnames = subject.getPrincipals(FullNamePrincipal.class);;
           	if(!fullnames.isEmpty())
               {
           		username = ((FullNamePrincipal) fullnames.iterator().next()).getName();
           		return username;
               }
               

           }
           return "";
	}
    public static String getUsername(){
		 Subject subject = (Subject)
          ((HttpServletRequest) ServletActionContext.getRequest()).getSession(true).
          getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
          String username = null;
          String substituted = null;

          if (subject != null)
          {
          	Set usernames= subject.getPrincipals(UserPrincipal.class);;
          	if(!usernames.isEmpty())
              {
          		username = ((UserPrincipal) usernames.iterator().next()).getName();
          		return username;
              }
              

          }
          return "";
	}
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCleanTasklist").
            append(OpenHibernateSession.INSTANCE).
            append(new CleanTasklist()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doInit").
	        append(OpenHibernateSession.INSTANCE).
	        append(new InitAvailable()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doTest").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Test()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
       /* registerListener("doCleanAttachments").
	        append(OpenHibernateSession.INSTANCE).
	        append(new CompressAttachments()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);*/

        registerListener("doSysinfo").
            append(new Sysinfo());
        
        registerListener("doDownload").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Download()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doIndex").
	        append(OpenHibernateSession.INSTANCE).
	        append(new IndexArchiveDocs()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("browserStats").
	        append(OpenHibernateSession.INSTANCE).
	        append(new BrowserStats()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class Test implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		System.out.println("JEDZIE SMS");    
        		((SMSIntelisoDriver) ServiceManager.getService(SMSIntelisoDriver.NAME)).send("600011571", Configuration.getTemplate("sms.txt"), null);
        		System.out.println("WYSLAL SMS");
        		////        		Long docId = Long.parseLong(customer);
//        		OfficeDocument doc = OfficeDocument.find(30786L);
//        		System.out.println("ZNALAZL");
//        		doc.getDocumentKind().logic().buildExportDocument(doc);
//        		try {
//	        			doc = OfficeDocument.find(30786L);
//	        			ExportedDocument exported = null;
//	        			exported = doc.getDocumentKind().logic().buildExportDocument(doc);
//	
//	        			if (exported == null) {
//	        			throw new NullPointerException("Export document not built!");
//	        			}
//	        			ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
//	        			System.out.println(handler.xmlExport(exported));
//        			}
//        			catch (Exception e) {
//        			}
        	}
        	catch (Exception e) {
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
        		
        }
    }
    
    
   
    
    
    
    private class InitAvailable implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            try
            {
            	System.out.println("INIT ADDS");
            	Docusafe.initAdds();
            	AvailabilityManager.initAdds();
                StringManager.initHomeLocalStringsProperties();
                ParamsManager.reloadEngine();
                Docusafe.refreshAppliactionContext();
                addActionMessage("Prze�adowano ustawienia cwaniaku");
                pl.compan.docusafe.core.cache.CacheRegistry.instance().clean(pl.compan.docusafe.core.cache.CacheDefinition.SITE_MAP);
               	 
            }
            catch (Exception e)
            {
            	addActionError(e.getMessage());
				log.error("", e);
            }
        }
    }
    
    private class Download implements ActionListener
    { 
    	public void actionPerformed(ActionEvent event)
        {
    		File f = null;
    		long size = 0;
    		Document document=null;

    		try {
				
	    		BaseFont timesBase= BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
				
	    		Font boldFont3 = new Font(timesBase, 12, Font.BOLD);
	    		Font boldFont2 = new Font(timesBase, 16, Font.BOLD);
	    		Font font = new Font(timesBase,12);

    			f = File.createTempFile("temp", "rft");
    			FileOutputStream fos;
				fos = new FileOutputStream(f);
	            document = new Document();
	         
	            
	            RtfWriter2.getInstance(document, fos);
	            document.open();
	            Paragraph par = new Paragraph("");
	            par.setAlignment(Element.ALIGN_RIGHT);

	            // Add the RtfPageNumber to the Paragraph
	            par.add(new RtfPageNumber());
	            par.add("/");
	            par.add(new RtfTotalPageNumber());
	                        
	            // Create an RtfHeaderFooter with the Paragraph and set it
	            // as a footer for the document
	            RtfHeaderFooter footer = new RtfHeaderFooter(par);
	            document.setFooter(footer);
	           
	            
	            String username = getName();
	            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
	            Date dt = new Date();
	            document.add(new Phrase("\n						DataUtworzenia: "+df.format(dt)+"\n",font));
	            document.add(new Phrase("						Tworz�cy: "+username+"\n\n\n\n\n\n\n",font));

	            document.add(new Phrase("	Parametryzacja systemu Docusafe z dnia "+df.format(dt)+"\n",boldFont2));
	            document.add(new Phrase("	Dla "+GlobalPreferences.getCustomer()+"\n\n\n\n\n\n\n\n\n\n\n\n",boldFont2));
	            
	            document.add(new Phrase("Licencja dla "+Docusafe.getLicenseString()+"\n",font));
	            document.add(new Phrase("Wersja systemu "+Docusafe.getVersion()+"."+Docusafe.getDistNumber()+"\n",font));
	            document.add(new Phrase("Liczba u�ytkownik�w: "+UserFactory.getInstance().getUserCount(),font));
	            document.newPage();
	             
	             
	            int sortMode = DSUser.SORT_LASTNAME_FIRSTNAME;
	            Role[] roles = Role.list();
	            document.add(new Phrase("Zdefiniowane role:",boldFont2));
	            for (Role role : roles) {
				
	            	Table roleTable = createRoleTable(role,boldFont2,boldFont3,font);
	            	document.add(roleTable);
					document.add(Chunk.NEWLINE);
	            }  
	            
	             
	            
	            document.add(new Phrase("Polityka hase�",boldFont2));
				Table passwordPolicyTable = createPassPolicyTable(font);
				document.add(passwordPolicyTable);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
	            
				document.add(new Phrase("Struktura organizacyjna:",boldFont2));
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				DSDivision division = DSDivision.find(DSDivision.ROOT_GUID);
				String tree = createDivisionTree(division, 0);
				document.add(new Phrase(tree,font));
				
				document.add(new Phrase("U�ytkownicy w systemie:",boldFont2));
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				SearchResults results = DSUser.getUsers(0, -1, sortMode, true);
				while (results.hasNext())
	            {
					DSUser user = (DSUser) results.next();
					Table userTable = createUsersTable(user,boldFont3,font);
					document.add(userTable);
                    document.add(Chunk.NEWLINE);
	            }
				
				
				document.add(new Phrase("Rodzaje dokument�w",boldFont2));
	            List documentKinds = DocumentKind.list();
				for (Object docKind : documentKinds) {
					Table docKindTable = createDocKindTable((DocumentKind)docKind, boldFont2, boldFont3, font);
					document.add(docKindTable);
					document.add(Chunk.NEWLINE);
					try{
						if(docKind != null)
						{
							org.dom4j.Document xmlDoc =  ((DocumentKind) docKind).getXML();
							document.add(new Phrase(xmlDoc.asXML(),font));
						}
					}catch (EdmException e) {
						document.add(new Phrase("Nie znalezionon dokumentu!!!!",font));
					}
					document.add(Chunk.NEWLINE);
				}
		        
    		}
            catch (Exception e) 
            {
            	addActionError(e.getMessage());
				log.error("", e);
			}
			finally{
				document.close();
			}
			
			size = f.length();

            HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("application/rtf");
            response.setHeader("Content-Disposition", "attachment; filename=\"" +
                "DefinicjeSystemu"+rtfDateFormat.format(new Date())+".rtf\"");
            response.setContentLength((int) size);
            try
            {
                OutputStream output = response.getOutputStream();
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
                byte[] buf = new byte[2048];
                int count;
                while ((count = in.read(buf)) > 0)
                {
                    output.write(buf, 0, count);
                    output.flush();
                }
                in.close();
                output.close();
            }
            catch (IOException e)
            {
            	addActionError(e.getMessage());
				log.error("", e);
            }
            finally
            {
                f.delete();
            }

        }
    	
    	private String createDivisionTree(DSDivision division,int depth){
    		String tree=null;
    		try{
	    		tree = generateTab(depth)+division.getName()+"\n";
	    		DSDivision[] subDivisions = division.getChildren();
	    		for (DSDivision subDivision : subDivisions) {
					tree+=createDivisionTree(subDivision, depth+1);
				}
    		} catch (EdmException e) {
    			addActionError(e.getMessage());
				log.error("", e);
			}
	    	return tree;
    	}
    	private String generateTab(int numberOfTabs){
    		String ret ="";
    		for (int i = 0; i < numberOfTabs; i++) {
				ret+="	";
			}
    		return ret;
    	} 
    	private Table createDocKindTable(DocumentKind docKind,Font boldFont2,Font boldFont3,Font font){
    		Table table = null;
    		
    		try{
    			table = new Table(3);
				table.setPadding(5);
			 	table.setSpacing(5);
				
				Cell cell = new Cell(new Phrase(sm.getString("NazwaKodowa"),boldFont3));
				table.addCell(cell);
				cell = new Cell(new Phrase(sm.getString("NazwaDlaUzytkownika"),boldFont3));
				table.addCell(cell);
				cell = new Cell(new Phrase(sm.getString("TabelaWBazie"),boldFont3));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(docKind.getCn(),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(docKind.getName(),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(docKind.getTablename(),font));
				table.addCell(cell);
				
				 
    		}catch (BadElementException e) {
    			addActionError(e.getMessage());
				log.error("", e);
    		} 
    		return table;
    	}
    	private Table createRoleTable(Role role,Font boldFont2,Font boldFont3,Font font){
    		Table table = null;
    		try{
    			table = new Table(2);
				table.setPadding(5);
				table.setSpacing(5);
				
				Cell cell = new Cell(new Phrase(sm.getString("NazwaRoli")+": "+role.getName(),boldFont2));
				cell.setColspan(2);
				table.addCell(cell);
				
				
				List categories = fun.filter(DSPermission.categories(),
	                    new filter<DSPermission.Category>()
	                    {
	                        public boolean accept(DSPermission.Category cat)
	                        {
	                            return cat.permissions().size() > 0;
	                        }
	                    }); 
				for (Object category : categories) {
					cell = new Cell(new Phrase(sm.getString("Kategoria")+": "+((DSPermission.Category)category).getName(),boldFont3));
					cell.setColspan(2);
					table.addCell(cell);
					
					List allPermissions = DSPermission.getAvailablePermissions();
					List rolePermissions = role.getPermissionsObjects();
	                List boundPermissions = new ArrayList(allPermissions);
	                boundPermissions.retainAll(rolePermissions);
	                for (Object permission : boundPermissions) {
	                	if(((DSPermission)permission).getCategory().equals(category)){
	                		cell = new Cell(new Phrase(((DSPermission)permission).getDescription(),font));
							cell.setColspan(2);
							table.addCell(cell);
	                	}
	                	
					}
				}
    		}catch (BadElementException e) {
    			addActionError(e.getMessage());
				log.error("", e);
			}
    		return table;
    	}
    	private Table createUsersTable(DSUser user,Font boldFont3,Font font){
			
		
			Table table=null;
			
			try{
				
				table = new Table(2);
				table.setPadding(5);
				table.setSpacing(5);
				
				Cell cell = new Cell(new Phrase(sm.getString("NazwaUzytkownika"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getName(),font));
				table.addCell(cell);
	
				cell = new Cell(new Phrase(sm.getString("Stanowiska"),font));
				table.addCell(cell);
				DSDivision[] positions= user.getDivisions();
				String posNames = "";
				for (DSDivision pos : positions) {
					if(pos.isPosition()){
						posNames+=pos.getName()+" ";
					}
				}
				cell = new Cell(new Phrase(posNames ,font));
				table.addCell(cell);
	
				cell = new Cell(new Phrase(sm.getString("Imie"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getFirstname(),font));
				table.addCell(cell);
				 
				cell = new Cell(new Phrase(sm.getString("Nazwisko"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getLastname(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("Kod u�ytkownika"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getIdentifier(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("Email"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getEmail()));
				table.addCell(cell);
	
				cell = new Cell(new Phrase(sm.getString("TelStac"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getPhoneNum(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("NumWew"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getExtension(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("TelKom"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getMobileNum(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("NumKomunikatora"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.getCommunicatorNum(),font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("ZablokowaneLogowanie"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.isLoginDisabled()?"TAK":"NIE",font));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("LogowanieTylkoCertyfikatem"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase(user.isCertificateLogin()?"TAK":"NIE"));
				table.addCell(cell);
				
				cell = new Cell(new Phrase(sm.getString("RoleArchiwum")+":",boldFont3));
				cell.setColspan(2);
				table.addCell(cell);
	
				Set userRoles = user.getRoles();
				
				
				cell=new Cell(new Phrase(sm.getString("Administrator"),font));
				table.addCell(cell);
				cell = new Cell(new Phrase( userRoles != null && userRoles.contains(DSUser.ADMIN_ROLE)?"TAK":"NIE",font));
				table.addCell(cell);
				
				cell=new Cell(new Phrase(sm.getString("AdministratorDanych"),font));
				table.addCell(cell);
				cell = new Cell( new Phrase(userRoles != null && userRoles.contains(DSUser.DATA_ADMIN_ROLE)?"TAK":"NIE",font));
				table.addCell(cell);
				
				DSDivision[] groups = UserFactory.getInstance().getGroups();
				if(groups!=null){
	    			cell = new Cell(new Phrase(sm.getString("Grupy")+":",boldFont3));
	    			cell.setColspan(2);
	    			table.addCell(cell);
				}
				DSDivision[] divisions = user.getDivisions();
				for (DSDivision group : divisions) {
					if(group.isGroup())
					{
	    				cell=new Cell(new Phrase(group.getName(),font));
		    			table.addCell(cell);

						cell = new Cell(new Phrase("TAK",font));
						table.addCell(cell);
					}
	
				} 
			 	
				Role[] roles = Role.list();
	
				if(roles!=null){
					cell = new Cell(new Phrase(sm.getString("RoleKancelarii")+":",boldFont3));
	    			cell.setColspan(2);
	    			table.addCell(cell);
				}
				for (Role role : roles) {
					
					if((userRoles != null && Role.listUserRoles(user.getName()).contains(role.getId()))){
						
	    				cell=new Cell(role.getName());
		    			table.addCell(cell);
		    			cell = new Cell("TAK");
		    			table.addCell(cell);
					}
				}
			
				boolean empty = true ;
				for (DSDivision division : divisions) {
				
					if(division.isDivision())
					{
						if(empty)
						{
							cell=new Cell(new Phrase(sm.getString("Dzialy")+":",boldFont3));
			    			cell.setColspan(2);
						}
						empty = false; 
	    				cell=new Cell(new Phrase(sm.getString("Dzial"),font));
		    			table.addCell(cell);
						cell = new Cell(new Phrase(division.getName(),font));
						table.addCell(cell);
					}
	
				} 
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error("", e);
			}
			 
			return table;
    	}
    	private Table createPassPolicyTable(Font font){
            Preferences prefs = DSApi.context().systemPreferences().
            node(PasswordPolicy.NODE_PASSWORD_POLICY);
            Table table=null;
			try {
				table = new Table(2);
			} catch (BadElementException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			}
			try{
			table.setBorderColor(new Color(0, 0, 255));
			table.setPadding(5);
			table.setSpacing(5);
			
			Cell cell = new Cell(new Phrase(sm.getString("MinimalnaDlugoscHasla"),font));
			table.addCell(cell);
			cell = new Cell(new Phrase( Integer.toString(prefs.getInt(PasswordPolicy.KEY_MIN_PASSWORD_LENGTH, PasswordPolicy.KEY_MIN_PASSWORD_LENGTH_DEFAULT)),font));
			table.addCell(cell);
			

			cell = new Cell(new Phrase(sm.getString("HasloWygasa"),font));
			table.addCell(cell);
			Phrase phrase1 = new Phrase("Po "+Integer.toString(prefs.getInt(PasswordPolicy.KEY_DAYS_TO_EXPIRE, 0))+" dniach");
			Phrase phrase2 = new Phrase("NIE");
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_PASSWORD_EXPIRES, false)?
					phrase1 : phrase2);
			table.addCell(cell);

			cell = new Cell(new Phrase(sm.getString("WymaganeCyfry"),font));
			table.addCell(cell);
			phrase1 = new Phrase(Integer.toString(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_DIGITS, 0)),font);
			phrase2 = new Phrase("Nie wymagane",font);
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_DIGITS, false)?
					phrase1:phrase2);
			table.addCell(cell);
			
			cell = new Cell(new Phrase(sm.getString("WymaganeDuzeLitery"),font));
			table.addCell(cell);
			phrase1 = new Phrase(Integer.toString(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_LETTERS, 0)),font);
		
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_LETTERS, false)?
					phrase1:phrase2);
			table.addCell(cell);
			 
			cell = new Cell(new Phrase(sm.getString("WymaganeMaleLitery"),font));
			table.addCell(cell);
			phrase1 = new Phrase(Integer.toString(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SMALL_LETTERS, 0)));
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SMALL_LETTERS, false)?
					phrase1:phrase2);
			table.addCell(cell);
			
			cell = new Cell(new Phrase(sm.getString("WymaganeZnakiSpecjalne"),font));
			table.addCell(cell);
			phrase1 = new Phrase(Integer.toString(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SPECIALS, 0)));
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SPECIALS, false)?
					phrase1:phrase2);
			table.addCell(cell);
			 
			
			cell = new Cell(new Phrase(sm.getString("BlokowanieHaselPoNieudanychProbach")));
			table.addCell(cell);
			phrase1=new Phrase("Po "+Integer.toString(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_INCORRECT_LOGINS, 0))+" pr�bach",font);
			cell = new Cell(prefs.getBoolean(PasswordPolicy.KEY_LOCKING_PASSWORDS, false)?
					phrase1:phrase2);
			table.addCell(cell);
			}
			catch (BadElementException e) {
				addActionError(e.getMessage());
				log.error("", e);
			}
			return table;
    	}
        
    }
    private class Sysinfo implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            InputStream is = getClass().getClassLoader().getResourceAsStream("sysinfo.txt");
            HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", "attachment; filename=docusafe-sysinfo.txt");
            //response.setContentLength(size);
            try
            {
                OutputStream output = response.getOutputStream();
                byte[] buf = new byte[8192];
                int count;
                while ((count = is.read(buf)) > 0)
                {
                    output.write(buf, 0, count);
                    output.flush();
                }
                output.close();
                is.close();
            }
            catch (Exception e)
            {
            	addActionError(e.getMessage());
				log.error("", e);
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(admin))
            {
                addActionError("Nie wybrano u�ytkownika administracyjnego");
            }

            if (useBarcodes &&
                (StringUtils.isEmpty(barcodePrefix) || StringUtils.isEmpty(barcodeSuffix)))
                addActionError("Po zaznaczeniu opcji 'Obs�uga kod�w kreskowych' nale�y " +
                    "wybra� prefiks oraz sufiks kodu kreskowego");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                // u�ytkownik administracyjny
                DSUser adminUser = DSUser.findByUsername(admin);

                if (StringUtils.isEmpty(adminUser.getEmail()))
                    throw new EdmException("U�ytkownik administracyjny musi " +
                        "mie� adres email.");

                // u�ytkownik administracyjny musi mie� rol� "admin"
                GlobalPreferences.setAdminUsername(adminUser.getName());
                adminUser.addRole(DSUser.ADMIN_ROLE);

                adminUser.update();

                // grupa u�ytkownik�w do natychmiastowej dekretacji

                GlobalPreferences.setInstantAssignmentGroup(TextUtils.trimmedStringOrNull(instantAssignmentGroup));
                GlobalPreferences.setOfficeNumbersFromAuxJournals(officeNumbersFromAuxJournals);
                GlobalPreferences.setWfProcessForClonedInDocuments(wfProcessForClonedInDocuments);
                GlobalPreferences.setBarcodePrefix(TextUtils.trimmedStringOrNull(barcodePrefix));
                GlobalPreferences.setBarcodeSuffix(TextUtils.trimmedStringOrNull(barcodeSuffix));
                GlobalPreferences.setUseBarcodes(useBarcodes);
                GlobalPreferences.setDocusafeInstanceType(docusafeInstanceType);
                GlobalPreferences.setCustomer(customer);

                DSApi.context().commit();

                //addActionMessage("Zapisano ustawienia");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
				log.error("", e);
            }
        }
    }
    /** Procedura czysci zakonczone zadania workflow, sa one niepotrzebne, a obciazaja baze */
    private class CleanTasklist implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Thread thread = new Thread(new CleanTask());
            thread.start();       	
            
        } 
    }
    
    /* Procedura zmniejsza jakosc duzych obrazow i zapisuje je jako czrnobialy tiff  */
    /*private class CompressAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        { 
            Thread thread2 = new Thread(new CleanAttachments());
            thread2.start();  
        } 
    }*/
    
/*    private class CleanAttachments implements Runnable
    {
    	PreparedStatement ps = null;
        Statement st = null;
        String mime;
        boolean onDisc;
        Long id;
        Long contenetSize;
        Long contentrefId;
        InputStream in;
		public void run() 
		{
			try
			{
				DSApi.openAdmin();
				st = DSApi.context().createStatement();
	            ResultSet rs = st.executeQuery("select id, contentdata, onDisc, mime, CONTENTSIZE,Contentrefid from DS_ATTACHMENT_REVISION where CONTENTSIZE > 1000000");
	            while(rs.next())
	            {
	            	DSApi.context().begin();
	            	id= rs.getLong(1);
	            	onDisc = rs.getBoolean(3);
	            	mime = rs.getString(4);
	            	contenetSize = rs.getLong(5);
	            	contentrefId = rs.getLong(6);
	            	if(mime.contains("image/tiff") && contenetSize > 1000000)
	            	{
	            		if(!onDisc)
	            		{
	            			in = rs.getBinaryStream(2);
			            	File f =File.createTempFile("docusafe_m", "");
			                OutputStream out=new FileOutputStream(f);
			                byte buf[]=new byte[1024];
			                int len;
			                while((len=in.read(buf))>0)
			                out.write(buf,0,len);
			                out.close(); 
			                in.close();
			                TIFFReader tiffReader =  new TIFFReader(f);
			                if( contenetSize/tiffReader.countPages() > 100000)
			                {
			                	File file = ImageKit.CompressioTiff((float)0.1, tiffReader, null, null, TIFFWriter.TIFF_CONVERSION_TO_GRAY, TIFFWriter.TIFF_COMPRESSION_DEFLATE);
			                	ps = DSApi.context().prepareStatement(
				                        "update DS_ATTACHMENT_REVISION set contentdata = ? , CONTENTSIZE = ? where id = ?");
				                    FileInputStream fis = new FileInputStream(file);
				                    ps.setBinaryStream(1, fis, (int) file.length());
				                    ps.setLong(2, file.length());
				                    ps.setLong(3, id);
				                    ps.executeUpdate();
				                    
				                    DSApi.context().closeStatement(ps);
				                    fis.close();   
				                    file.delete();
			                }
			                f.delete();
	            		}
	            		else
	            		{
	            			
	            			try
	            			{
	            				in  = new FileInputStream(new File(Attachment.getPath((id))));  
	            			}
	            			catch (FileNotFoundException e) 
	            			{
								continue;
							}
	            			File f =File.createTempFile("docusafe_m", "");
			                OutputStream out=new FileOutputStream(f);
			                byte buf[]=new byte[1024];
			                int len;
			                while((len=in.read(buf))>0)
			                out.write(buf,0,len);
			                out.close();
			                in.close();
			                TIFFReader tiffReader =  new TIFFReader(f);
			                if( contenetSize/tiffReader.countPages() > 100000)
			                {
			                	File file = ImageKit.CompressioTiff((float)0.2, tiffReader, null, null,TIFFWriter.TIFF_CONVERSION_TO_GRAY, TIFFWriter.TIFF_COMPRESSION_DEFLATE);
			                	ps = DSApi.context().prepareStatement(
				                        "update DS_ATTACHMENT_REVISION set CONTENTSIZE = ? where id = ?");
			                    ps.setLong(1, file.length());
			                    ps.setLong(2, id);
			                    ps.executeUpdate();
			                    DSApi.context().closeStatement(ps); 
			                	Attachment.updateFile(new FileInputStream(file), id);
			                	file.delete();
			                }
			                f.delete();
	            		}
	            	}  
	            	DSApi.context().commit();
	            	
	            	DSApi.context().session().flush();
	            }
	           
	            rs.close();
	            DSApi.context().closeStatement(st);
			}
			catch (HibernateException e)
			{
				LogFactory.getLog("eprint").debug("", e);
			}
			catch (SQLException e) {
				LogFactory.getLog("eprint").debug("", e);
			} catch (EdmException e) 
			{
				LogFactory.getLog("eprint").debug("", e);
			} catch (FileNotFoundException e) 
			{
				LogFactory.getLog("eprint").debug("", e);
			} catch (IOException e)
			{
				LogFactory.getLog("eprint").debug("", e);
			}
			finally
            {
                try
                {
                    DSApi.close();
                }
                catch (EdmException e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
            }
		}
    	
    }
    */
    
    private class CleanTask implements Runnable
    {
        public void run()
        {
            try
            {
                DSApi.openAdmin();
                InternalWorkflowService.cleanClosedTask();
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
				log.error("", e);
            }
            finally
            {
                try
                {
                    DSApi.close();
                }
                catch (EdmException e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
            }
        }
    
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                admin = GlobalPreferences.getAdminUsername();
                instantAssignmentGroup = GlobalPreferences.getInstantAssignmentGroup();
                officeNumbersFromAuxJournals = GlobalPreferences.isOfficeNumbersFromAuxJournals();
                wfProcessForClonedInDocuments = GlobalPreferences.isWfProcessForClonedInDocuments();
                useBarcodes = GlobalPreferences.isUseBarcodes();
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
                docusafeInstanceType = GlobalPreferences.getDocusafeInstanceType();
                docusafeTypes = GlobalPreferences.getDocusafeTypes();
                customer = GlobalPreferences.getCustomer();

                if (UserFactory.getInstance().isLdap())
                {
                    groups = new DSDivision[0];
                    userCount = 0;
                }
                else
                {
                    // TODO: z LDAP ten formularz si� blokuje
                    groups = DSDivision.find(DSDivision.ROOT_GUID).
                        getChildren(new DSDivisionFilter() {
                            public boolean accept(DSDivision division)
                            {
                                return division.isGroup();
                            }
                        });
                    userCount = UserFactory.getInstance().getUserCount();
                }
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
				log.error("", e);
            }
        }
    }
    
    private class IndexArchiveDocs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
//        		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
//	            IndexWriter writer = new IndexWriter(Docusafe.getIndexes(), analyzer, true, MaxFieldLength.UNLIMITED);
//	            indexDocs(writer, Finder.list(ProsikaArchiveDocument.class, "id"));
//	            writer.optimize();
//	            writer.close();
 
        	}
        	catch(Exception e)
        	{
        		addActionError(e.getMessage());
				log.error("", e);
        	}
        }
    }
    
    static class BrowserStat {
		String name;
		String version;
		Integer width;
		Integer height;
		
		public BrowserStat(String name, String version, Integer width, Integer height) {
			this.name = name;
			this.version = version;
			this.width = width;
			this.height = height;
		}
		
		public BrowserStat() {
			
		}
	}
    
    private class BrowserStats implements ActionListener {
    	

		public void actionPerformed(ActionEvent event) {
			browsersInfo = "";
			List<BrowserStat> browserStats = new ArrayList<BrowserStat>();
			try {
				DSApi.context().begin();
				PreparedStatement ps = DSApi.context().prepareStatement("select * from ds_agent_user_stats");
				ResultSet rs = ps.executeQuery();
				Gson gson = new Gson();
				while(rs.next()) {
					browserStats.add(new BrowserStat(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
				}
				
				browsersInfo = gson.toJson(browserStats);
				
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error("", e);
			}
		}
    	
    }
    
    void indexDocs(IndexWriter writer,List<ProsikaArchiveDocument> docs) throws Exception 
    {
    	//Na razie nic nie indeksujemy
    	/*
	      for(ProsikaArchiveDocument pad:docs) 
	      {
	         org.apache.lucene.document.Document d = new org.apache.lucene.document.Document();
	         //System.out.println(pad.getId()+"="+pad.getContent());
	         d.add(new Field("documentid", pad.getId()+"", Field.Store.YES, Field.Index.NO));
	         if(pad.getContent()!=null)
	        	 d.add(new Field("content", pad.getContent(),Field.Store.NO, Field.Index.ANALYZED));
	         if(pad.getAccountCode()!=null)
	         	d.add(new Field("accountCode", pad.getAccountCode(),Field.Store.NO, Field.Index.ANALYZED));
	         if(pad.getCode()!=null)
	        	 d.add(new Field("code", pad.getCode(),Field.Store.NO, Field.Index.ANALYZED));
	         if(pad.getDepartment()!=null)
	        	 d.add(new Field("department", pad.getDepartment(),Field.Store.NO, Field.Index.ANALYZED));
	         writer.addDocument(d);
	      }
	      */
    }
      


    public List getUsers()
    {
        return users;
    }

    public String getAdmin()
    {
        return admin;
    }

    public void setAdmin(String admin)
    {
        this.admin = admin;
    }

    public DSDivision[] getGroups()
    {
        return groups;
    }

    public String getInstantAssignmentGroup()
    {
        return instantAssignmentGroup;
    }

    public void setInstantAssignmentGroup(String instantAssignmentGroup)
    {
        this.instantAssignmentGroup = instantAssignmentGroup;
    }

    public boolean isOfficeNumbersFromAuxJournals()
    {
        return officeNumbersFromAuxJournals;
    }

    public void setOfficeNumbersFromAuxJournals(boolean officeNumbersFromAuxJournals)
    {
        this.officeNumbersFromAuxJournals = officeNumbersFromAuxJournals;
    }

    public boolean isWfProcessForClonedInDocuments()
    {
        return wfProcessForClonedInDocuments;
    }

    public void setWfProcessForClonedInDocuments(boolean wfProcessForClonedInDocuments)
    {
        this.wfProcessForClonedInDocuments = wfProcessForClonedInDocuments;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public void setBarcodePrefix(String barcodePrefix)
    {
        this.barcodePrefix = barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public void setBarcodeSuffix(String barcodeSuffix)
    {
        this.barcodeSuffix = barcodeSuffix;
    }

    public boolean isUseBarcodes()
    {
        return useBarcodes;
    }

    public void setUseBarcodes(boolean useBarcodes)
    {
        this.useBarcodes = useBarcodes;
    }

    public int getUserCount()
    {
        return userCount;
    }

    public String getDocusafeInstanceType()
    {
        return docusafeInstanceType;
    }

    public void setDocusafeInstanceType(String docusafeInstanceType)
    {
        this.docusafeInstanceType = docusafeInstanceType;
    }

    public Map<String, String> getDocusafeTypes()
    {
        return docusafeTypes;
    }

    public boolean isEmergency()
    {
        return GlobalPreferences.EMERGENCY;
    }

    public String getCustomer()
    {
        return customer;
    }

    public void setCustomer(String customer)
    {
        this.customer = customer;
    }
	public void setBrowsersInfo(String browsersInfo) {
		this.browsersInfo = browsersInfo;
	}
	public String getBrowsersInfo() {
		return browsersInfo;
	}
    
}
