package pl.compan.docusafe.web.archive.settings;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.CollectiveAssignmentEntry;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.webwork.event.*;

/**
 * @deprecated Nieużywane
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditCollectiveAssignmentEntryAction.java,v 1.6 2008/08/19 15:35:25 pecet4 Exp $
 */
public class EditCollectiveAssignmentEntryAction extends EventActionSupport
{
    // @EXPORT/@IMPORT
    private Integer id;
    private String processId;
    private String objective;

    // @EXPORT
    private WfProcessMgr[] processManagers;
    private CollectiveAssignmentEntry entry;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new Save()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	// zostaje WfService, bo klasa jest deprecated
                WfService iws = WorkflowServiceFactory.newInternalInstance().newService();
                iws.connectAdmin();

                processManagers = iws.getAllProcessMgrs();

                if (id != null)
                {
                    entry = CollectiveAssignmentEntry.find(id);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(objective))
                addActionError("Nie podano nazwy dekretacji");

            try
            {
                ProcessId.parse(processId);
            }
            catch (WorkflowException e)
            {
                addActionError("Niepoprawny identyfikator procesu: "+processId);
            }

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                    CollectiveAssignmentEntry caEntry = CollectiveAssignmentEntry.find(id);
                    caEntry.setObjective(objective);
                    caEntry.setWfProcess(processId);
                }
                else
                {
                    CollectiveAssignmentEntry caEntry = new CollectiveAssignmentEntry();
                    caEntry.setObjective(objective);
                    caEntry.setWfProcess(processId);
                    caEntry.create();
                }

                DSApi.context().commit();

                event.setResult("collective-assignment");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public WfProcessMgr[] getProcessManagers()
    {
        return processManagers;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public CollectiveAssignmentEntry getEntry()
    {
        return entry;
    }

    public String getObjective()
    {
        return objective;
    }

    public void setObjective(String objective)
    {
        this.objective = objective;
    }
}
