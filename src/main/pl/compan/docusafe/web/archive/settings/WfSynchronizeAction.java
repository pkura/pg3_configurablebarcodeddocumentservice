package pl.compan.docusafe.web.archive.settings;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.workflow.WfProcessMgr;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.office.workflow.xpdl.Participant;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Akcja tworząca w serwerze workflow użytkowników oraz mapowania
 * ról odpowiadających działom i grupom na użytkowników.
 *
 * @deprecated Nieużywane
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WfSynchronizeAction.java,v 1.8 2008/08/19 15:35:25 pecet4 Exp $
 */
public class WfSynchronizeAction extends EventProcessingAction
{
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    private static final String FORWARD = null;

    protected void setup()
    {
        SetActionForwardListener setForward = new SetActionForwardListener("main");
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doSynchronize").
            append(setForward).
            append(new Synchronize()).
            append(fillForm);
    }

    /**
     * Tworzenie mapowań użytkowników na obiekty Participant w definicjach
     * procesów.
     */
    private class Synchronize implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            DynaActionForm form = event.getDynaForm();
            // lista identyfikatorów procesów postaci
            // identyfikator_pakietu . " " . identyfikator_procesu
            // (rozdzielone jedną spacją)
            String[] processes = (String[]) form.get("processes");

            if (processes == null || processes.length == 0)
            {
                event.getErrors().add(sm.getString("wfSynchronize.notProcessesSelected"));
                return;
            }

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                // tworzenie w serwerze workflow brakujących użytkowników
                DSUser[] dsUsers = (DSUser[]) DSUser.list(DSUser.SORT_NONE).toArray(new DSUser[0]);

                for (int i=0, n=dsUsers.length; i < n; i++)
                {
                	event.getLog().info("Tworzenie w serwerze workflow " +
                            "użytkownika "+dsUsers[i].getName());
                	WorkflowFactory.createUser(dsUsers[i].getName(),
                			Configuration.getProperty("workflow.default.password"),
                			"",
                			dsUsers[i].getEmail() != null ? dsUsers[i].getEmail() : "",
                			"");
                	
                }

                // tworzenie mapowań grup użytkowników na użytkowników
                
                
                if (processes != null && processes.length > 0)
                {
                    for (int i=0, n=processes.length; i < n; i++)
                    {
                        String[] p = StringUtils.split(processes[i], " ");
                        if (p == null || p.length != 2)
                        {
                            event.getLog().warn("Nieprawidłowy identyfikator procesu " +
                                "odczytany z formularza: "+processes[i]);
                            continue;
                        }

                        String packageId = p[0];
                        String processDefinitionId = p[1];
                        
                        WorkflowFactory.createUserGroupsToUsersMapping(packageId, processDefinitionId, event.getLog());
                        
                    }
                }

            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            DynaActionForm form = event.getDynaForm();

            event.getRequest().setAttribute("wfHost",
                Configuration.getProperty("shark.host"));
            event.getRequest().setAttribute("wfPort",
                Configuration.getProperty("shark.port"));
            event.getRequest().setAttribute("wfServerName",
                Configuration.getProperty("shark.server.name"));

            pl.compan.docusafe.core.office.workflow.WfService ws = null;
            DSContext ctx = null;
            try
            {
                //ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
            	
            	//k. - zostawiam wfservice - klasa nieużywana
                ws = WorkflowServiceFactory.newExternalInstance().newService();
                ws.connectAdmin();

                List mgrBeans = new LinkedList();
                WfProcessMgr[] mgrs = ws.getAllProcessMgrs();
                if (mgrs != null && mgrs.length > 0)
                {
                    for (int i=0; i < mgrs.length; i++)
                    {
                        DynaBean bean = DynaBeans.bean(DynaBeans.checkbox);

                        bean.set("value",
                            mgrs[i].package_id() + " " +
                            mgrs[i].process_definition_id());
                        bean.set("label", mgrs[i].package_id()+" - "+mgrs[i].name());

                        mgrBeans.add(bean);
                    }
                }

                event.getRequest().setAttribute("processMgrs", mgrBeans);
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                //DSApi._close();
                if (ws != null) ws._disconnect();
            }
        }
    }

    public static String getLink()
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }
}
