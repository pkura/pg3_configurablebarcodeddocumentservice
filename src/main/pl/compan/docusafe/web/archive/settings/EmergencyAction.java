package pl.compan.docusafe.web.archive.settings;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;




public class EmergencyAction extends EventActionSupport
{
    private boolean emergency;
    private Map<String, String> usersAll;
    private Map<String, String> usersSelected;
    private String[] usernames;
    

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        SelectedUser selectedUser = new SelectedUser();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(selectedUser).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(selectedUser).
            append(new Update()).
            append(fillForm).
            append(selectedUser).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doOffEmergency").
            append(OpenHibernateSession.INSTANCE).
            append(new Emergency()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);;
}

private class Emergency implements ActionListener
{

    public void actionPerformed(ActionEvent event)
    {
        GlobalPreferences.clearFreeUser();
        GlobalPreferences.EMERGENCY = false;
    }
    
}

private class SelectedUser implements ActionListener
{

    @SuppressWarnings("unchecked")
    public void actionPerformed(ActionEvent event)
    {
        usersSelected = new LinkedHashMap<String, String>();
        GlobalPreferences.addFreeUser("admin");
        Map<String,String> freeUser = GlobalPreferences.getFreeUser();
        Set userName = freeUser.keySet();
        Iterator<String> it = userName.iterator();
        while(it.hasNext())
        {
            String name = it.next();
            try
            {
                usersSelected.put(name,DSUser.findByUsername(name).asLastnameFirstname());
            }
            catch (UserNotFoundException e)
            {
                // TODO Auto-generated catch block
            	LogFactory.getLog("eprint").debug("", e);
            }
            catch (EdmException e)
            {
                // TODO Auto-generated catch block
            	LogFactory.getLog("eprint").debug("", e);
            }
        }  
    }
    
}
    
    private class FillForm implements ActionListener 
    {

        public void actionPerformed(ActionEvent event)
        {
             try
            {
                usersAll = new LinkedHashMap<String, String>();
       
                
                List<DSUser> usersList = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                Iterator<DSUser> itr = usersList.iterator();
                while (itr.hasNext())
                {
                    DSUser user = itr.next();
                    if(GlobalPreferences.getFreeUser(user.getName())== null)
                    usersAll.put(user.getName(), user.asLastnameFirstname());
                }
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }            
        }
    }
    
    private class Update implements ActionListener 
    {

        public void actionPerformed(ActionEvent event)
        {
           GlobalPreferences.clearFreeUser();
           if(usernames ==null || usernames.length == 0 )
           {
               addActionError("Nie wybrano użykownikow");
           }
           else
           {
               for (int i=0; i < usernames.length; i++)
               { 
                   GlobalPreferences.addFreeUser(usernames[i]);
               }
               GlobalPreferences.EMERGENCY = true;
           }
          }      
        
    }
    public boolean Checked(String name)
    {
        return GlobalPreferences.getFreeUser(name)!=null;
        
    }

    public void setActivity(String[] activity)
    {
        for (int i=0; i < activity.length; i++)
        {
            GlobalPreferences.addFreeUser(activity[i]);
        }        
    }
    
    public boolean isEmergency()
    {
        return GlobalPreferences.EMERGENCY;
    }    
    
    public Map<String, String> getUsersAll()
    {
        return usersAll;
    }

    public Map<String, String> getUsersSelected()
    {
        return usersSelected;
    }

    public String[] getUsernames()
    {
        return usernames;
    }

    public void setUsernames(String[] usernames)
    {
        this.usernames = usernames;
    }
    
}
