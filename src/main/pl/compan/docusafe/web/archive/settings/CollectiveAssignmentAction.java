package pl.compan.docusafe.web.archive.settings;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.CollectiveAssignmentEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;

/**
 * @deprecated Nieu�ywane
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CollectiveAssignmentAction.java,v 1.9 2008/02/29 12:36:14 pecet2 Exp $
 */
public class CollectiveAssignmentAction extends EventActionSupport
{
    // @EXPORT
    private List collectiveAssignments;
    /**
     * Lista grup.
     */
    private DSDivision[] groups;

    // @EXPORT/@IMPORT
    private boolean enable;
    private String collectiveAssignmentGroup;

    // @IMPORT
    private Integer[] ids;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new Save()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                enable = GlobalPreferences.isEnableCollectiveAssignment();
                
                collectiveAssignments = CollectiveAssignmentEntry.list();
                collectiveAssignmentGroup = GlobalPreferences.getCollectiveAssignmentGroup();

                groups = UserFactory.getInstance().getGroups();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (enable && StringUtils.isEmpty(collectiveAssignmentGroup))
            {
                addActionError("Nie wybrano grupy dla zbiorczej dekretacji");
                return;
            }

            try
            {
                DSApi.context().begin();

                GlobalPreferences.setEnableCollectiveAssignment(Boolean.valueOf(String.valueOf(enable)).booleanValue());

                // grupa u�ytkownik�w zbiorczej dekretacji

                GlobalPreferences.setCollectiveAssignmentGroup(collectiveAssignmentGroup);

                DSApi.context().commit();

                addActionMessage("Zapisano ustawienia");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (ids == null || ids.length == 0)
            {
                addActionError("Nie wybrano cel�w dekretacji do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < ids.length; i++)
                    CollectiveAssignmentEntry.find(ids[i]).delete();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    public List getCollectiveAssignments()
    {
        return collectiveAssignments;
    }

    public void setIds(Integer[] ids)
    {
        this.ids = ids;
    }

    public String getCollectiveAssignmentGroup()
    {
        return collectiveAssignmentGroup;
    }

    public void setCollectiveAssignmentGroup(String collectiveAssignmentGroup)
    {
        this.collectiveAssignmentGroup = collectiveAssignmentGroup;
    }

    public DSDivision[] getGroups()
    {
        return groups;
    }
}
