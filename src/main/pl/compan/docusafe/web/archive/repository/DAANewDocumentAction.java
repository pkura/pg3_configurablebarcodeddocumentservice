package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Daa;
import pl.compan.docusafe.web.office.common.DaaAttachmentPattern;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.util.*;
import java.io.File;

/**
 * @author Bartlomiej Spychalski bsp@spychalski.eu
 * @version $Id: DAANewDocumentAction.java,v 1.59 2010/04/01 09:31:39 pecet5 Exp $
 */
public class DAANewDocumentAction extends EventActionSupport {
   
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(DAANewDocumentAction.class.getPackage().getName(),null);
    private List doctypeFieldList;
    private Map doctypeFields = new HashMap();

    private String title;
    private String description;
    private FormFile file;
    private boolean addToBox;

    private Long folderId;
    private String boxName;
    private Long boxId;
    private String NR_SZKODY;
    private Long newDocumentId;

    private String agent_imie;
    private String agent_nazwisko;
    private String agent_numer;
    private String agencja_nazwa;
    private String agencja_nip;
    private String agencja_numer;
    private List rodzaje_sieci;
    private Integer rodzaj_sieci;
    private boolean canReadDAASlowniki;

    /* wz�r umowy */
    private Long attachmentPattern;
    private List<DaaAttachmentPattern> patterns;

    protected void setup() {
        if (!Configuration.hasExtra("daa"))
            throw new Error(sm.getString("AkcjaDostepnaTylkoDlaLicencjiDAA"));

        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
                append(OpenHibernateSession.INSTANCE).
                append(new Create()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            try {


                if(!DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE))
                    addActionError(sm.getString("BrakUprawnieniaDoPrzegladaniaSlownikowAgentaAgencji"));
                canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);
                patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();

                Doctype doctype = Doctype.findByCn("daa");
                doctypeFieldList = Arrays.asList(doctype.getFieldsAsArray());

                // domy�lny status - "przyj�ty"
                doctypeFields.put(doctype.getFieldByCn("STATUS").getId(), doctype.getFieldByCn("STATUS").getEnumItemByCn("ARCHIWALNY").getId());
                doctypeFields.put(doctype.getFieldByCn("DATA_WPLYWU").getId(), new Date());

                rodzaje_sieci = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItems();


                Long _boxId = GlobalPreferences.getBoxId();
                if (_boxId != null && GlobalPreferences.isBoxOpen()) {
                    try {
                        Box box = Box.find(_boxId);
                        boxId = box.getId();
                        boxName = box.getName();
                    }
                    catch (EntityNotFoundException e) {
                    	LogFactory.getLog("eprint").debug("", e);
                    }
                }
            }
            catch (EdmException e) {
                addActionError(e.getMessage());
            }
        }
    }

    private class Create implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            if (hasActionErrors())
                return;

            long totaltime = System.currentTimeMillis();
            try {
                DSApi.context().begin();

                long time = System.currentTimeMillis();
                Doctype doctype = Doctype.findByCn("daa");
                event.getLog().trace("Doctype.findByCn: " + (System.currentTimeMillis() - time));

                time = System.currentTimeMillis();
                if (Daa.getRODZAJ_SIECI(doctype, doctypeFields) == null || (Daa.getRODZAJ_SIECI(doctype, doctypeFields) != null && Daa.getRODZAJ_SIECI(doctype, doctypeFields).getId() == 0))
                    throw new EdmException(sm.getString("NieWybranoRodzajuSieci")+" ");
                event.getLog().trace("getRODZAJ_SIECI: " + (System.currentTimeMillis() - time));


                if (Daa.getTYP_DOKUMENTU(doctype, doctypeFields) == null || (Daa.getTYP_DOKUMENTU(doctype, doctypeFields) != null && Daa.getTYP_DOKUMENTU(doctype, doctypeFields).getId() == 0))
                    throw new EdmException(sm.getString("NieWybranoTypuDokumentu")+" ");
                event.getLog().trace("getTYP_DOKUMENTU: " + (System.currentTimeMillis() - time));


                String agentIds = doctypeFields.get(doctype.getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENT").getId()).toString() : null;
                String agencjaIds = doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()).toString() : null;

                Doctype.EnumItem e = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
                String typ_dokumentu = "";
                String rodzaj_dokumentu = "";
                if(e != null) {
                    typ_dokumentu = e.getTitle(); if(typ_dokumentu != null) typ_dokumentu= typ_dokumentu.trim();
                    rodzaj_dokumentu = e.getArg1(); if(rodzaj_dokumentu != null) rodzaj_dokumentu = rodzaj_dokumentu.trim();
                }

                DaaAgent agent = null;
                DaaAgencja agencja = null;


                try {
                    if (agentIds != null) {
                        agent = new DaaAgent().find(new Long(agentIds.trim()));
                        if (agent != null) {
                            agent_imie = agent.getImie();
                            agent_numer = agent.getNumer();
                            agent_nazwisko = agent.getNazwisko();
                        }
                    }
                    if (agencjaIds != null) {
                        agencja = new DaaAgencja().find(new Long(agencjaIds.trim()));
                        if (agencja != null) {
                            agencja_nazwa = agencja.getNazwa();
                            agencja_numer = agencja.getNumer();
                            agencja_nip = agencja.getNip();
                        }
                    }

                } catch (Exception ex) {
                }


                //tytul i opis
                String klasaId = doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()).toString() : null;
                Doctype.EnumItem klasaRaportuEnum = klasaId != null ? doctype.getFieldByCn("KLASA_RAPORTU").getEnumItem(new Integer(klasaId)) : null;
                rodzaj_sieci = new Integer((String)doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId())).intValue();
                //String siec = (String)doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getName());
                Doctype.EnumItem siecEnum = Daa.getRODZAJ_SIECI(doctype, doctypeFields);
                Document document = new Document(
                        title,
                        description);
                //document.setFolder(Folder.getRootFolder());

                Daa.daaDocument(document, agent,  agencja,  siecEnum, typ_dokumentu, rodzaj_dokumentu, klasaRaportuEnum, true);
                document.create();
                Daa.daaDocumentPermission(document);

                if (document instanceof OfficeDocument) {
                    time = System.currentTimeMillis();
                    ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.create("::nw_archive", DSApi.context().getPrincipalName(),
                                    sm.getString("DokumentZarchiwizowanoWfolderze",document.getFolderPath()),
                                    null, null));
                    try {
                        DSApi.context().session().flush();
                    }
                    catch (HibernateException ex) {
                        throw new EdmHibernateException(ex);
                    }
                }

                event.getLog().trace("Document.create: " + (System.currentTimeMillis() - time));


                newDocumentId = document.getId();

                if (newDocumentId != null) {
                    if (agent != null) {
                        agencja = agent.getAgencja();
                    }


                    if (agencja != null) {
                        rodzaj_sieci = agencja.getRodzaj_sieci();
                        doctype.set(newDocumentId, doctype.getFieldByCn("RODZAJ_SIECI").getId(), rodzaj_sieci);

                        if (agent != null)
                            doctype.set(newDocumentId, doctype.getFieldByCn("AGENT").getId(), agent.getId());
                        doctype.set(newDocumentId, doctype.getFieldByCn("AGENCJA").getId(), agencja.getId());
                    } else {
                        rodzaj_sieci = Daa.getRODZAJ_SIECI(doctype, doctypeFields).getId();
                        doctype.set(newDocumentId, doctype.getFieldByCn("RODZAJ_SIECI").getId(), rodzaj_sieci);

                    }
                }

                time = System.currentTimeMillis();
                String NR_POLISY = null;
                Doctype.Field[] fields = doctype.getFieldsAsArray();
                for (int i = 0; i < fields.length; i++) {
                    String value = HttpUtils.valueOrNull(doctypeFields.get(fields[i].getId()));
                    String cn = HttpUtils.valueOrNull(fields[i].getCn());

                    if (!"AGENT".equals(fields[i].getCn()) && !"AGENCJA".equals(fields[i].getCn()) && !"RODZAJ_SIECI".equals(fields[i].getCn()))
                    {
                        doctype.set(document.getId(), fields[i].getId(), value);
                    }

                }


                event.getLog().trace("for(Doctype.set): " + (System.currentTimeMillis() - time));

                time = System.currentTimeMillis();
                /* wkuty - dodanie numeru pudla */
                if (addToBox && boxId != null)
                {
                    document.setBox(Box.find(boxId));
                }
                /* wkuty mode off */
                DSApi.context().session().flush();
                event.getLog().trace("fileDocument: " + (System.currentTimeMillis() - time));

                folderId = document.getFolderId();


                time = System.currentTimeMillis();
                //Nie tlumaczone poniewaz niewiadomo czy nie szuka po tych konkretnych wartosciach
                if (file != null && file.getFile() != null) {
                    Attachment attachment = new Attachment("Skan");
                    document.createAttachment(attachment);
                    attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                }

                /* dodanie wzoru umowy */
                patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();
                if ((attachmentPattern != null) && (patterns.get(attachmentPattern.intValue() - 1).getFilename() != null))
                {
                    File patternFile = patterns.get(attachmentPattern.intValue() - 1).getFile();
                    if (patternFile != null)
                    {
                        Attachment attachment = new Attachment("Wz�r umowy");
                        document.createAttachment(attachment);
                        attachment.createRevision(patternFile).setOriginalFilename(patternFile.getName());
                    }
                    else
                        throw new EdmException(sm.getString("BladPodczasPobieraniaPlikuZeWzoremUmowy"));
                }

                DSApi.context().session().flush();
                event.getLog().trace("createRevision: " + (System.currentTimeMillis() - time));

                time = System.currentTimeMillis();
                if (addToBox && boxId != null) {
                    document.setBox(Box.find(boxId));
                }
                event.getLog().trace("setBox: " + (System.currentTimeMillis() - time));

                time = System.currentTimeMillis();
                DSApi.context().commit();
                event.getLog().trace("commit: " + (System.currentTimeMillis() - time));

                event.setResult("explore-documents");

            }
            catch (EdmException e) {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            catch (HibernateException e) {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally {
                event.getLog().trace("Create: " + (System.currentTimeMillis() - totaltime));
            }
        }
    }


    public List getDoctypeFieldList() {
        return doctypeFieldList;
    }

    public Map getDoctypeFields() {
        return doctypeFields;
    }

    public void setDoctypeFields(Map doctypeFields) {
        this.doctypeFields = doctypeFields;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setAddToBox(boolean addToBox) {
        this.addToBox = addToBox;
    }

    public Long getBoxId() {
        return boxId;
    }

    public void setBoxId(Long boxId) {
        this.boxId = boxId;
    }

    public String getBoxName() {
        return boxName;
    }

    public String getNR_SZKODY() {
        return NR_SZKODY;
    }

    public void setNR_SZKODY(String NR_SZKODY) {
        this.NR_SZKODY = NR_SZKODY;
    }

    public Long getNewDocumentId() {
        return newDocumentId;
    }

    //-------------------------------

    char alfabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',  'r', 's', 't', 'u', 'w', 'x', 'y', 'z', ' ', ' '};



    public String pobierzPrzedzial(char s, int n) {
        int l = alfabet.length-1;
        int i = 0;
        int chk;
        char ss;
        String str = "a-b";

        while (i < l - n) {
            chk = i + n  ;
            if (chk > l) chk = l ;
            if(chk == 0) chk = 1;
            if (alfabet[i] <= s && alfabet[chk] >= s) {
                return "" + alfabet[i] + "-" + alfabet[chk];
            }
            if(s == '�') return "a-b";
            if(s == '�') return "c-d";
            if(s == '�') return "e-f";
            if(s == '�') return "k-l";
            if(s == '�') return "m-n";
            if(s == '�') return "o-p";
            if(s == '�') return "r-s";
            if(s == '�' || s ==  '�') return "�-�";
            i++;
            i += n;
        }
        return str;
    }

    public String getAgent_imie() {
        return agent_imie;
    }

    public void setAgent_imie(String agent_imie) {
        this.agent_imie = agent_imie;
    }

    public String getAgent_nazwisko() {
        return agent_nazwisko;
    }

    public void setAgent_nazwisko(String agent_nazwisko) {
        this.agent_nazwisko = agent_nazwisko;
    }

    public void setAgent_numer(String agent_numer) {
        this.agent_numer = agent_numer;
    }

    public String getAgent_numer() {
        return agent_numer;
    }

    public String getAgencja_nazwa() {
        return agencja_nazwa;
    }

    public void setAgencja_nazwa(String agencja_nazwa) {
        this.agencja_nazwa = agencja_nazwa;
    }

    public String getAgencja_numer() {
        return agencja_numer;
    }

    public void setAgencja_numer(String agencja_numer) {
        this.agencja_numer = agencja_numer;
    }

    public String getAgencja_nip() {
        return agencja_nip;
    }

    public void setAgencja_nip(String agencja_nip) {
        this.agencja_nip = agencja_nip;
    }

    public char[] getAlfabet() {
        return alfabet;
    }

    public void setAlfabet(char[] alfabet) {
        this.alfabet = alfabet;
    }

    public List getRodzaje_sieci() {
        return rodzaje_sieci;
    }

    public void setRodzaje_sieci(List rodzaje_sieci) {
        this.rodzaje_sieci = rodzaje_sieci;
    }

    public Integer getRodzaj_sieci() {
        return rodzaj_sieci;
    }

    public void setRodzaj_sieci(Integer rodzaj_sieci) {
        this.rodzaj_sieci = rodzaj_sieci;
    }

    public void setAttachmentPattern(Long attachmentPattern)
    {
        this.attachmentPattern = attachmentPattern;
    }

    public Long getAttachmentPattern()
    {
        return attachmentPattern;
    }

    public List<DaaAttachmentPattern> getPatterns()
    {
        return patterns;
    }

    public boolean isCanReadDAASlowniki() {
        return canReadDAASlowniki;
    }

    public void setCanReadDAASlowniki(boolean canReadDAASlowniki) {
        this.canReadDAASlowniki = canReadDAASlowniki;
    }

}
