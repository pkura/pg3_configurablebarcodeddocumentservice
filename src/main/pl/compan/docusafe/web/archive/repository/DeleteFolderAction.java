package pl.compan.docusafe.web.archive.repository;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

/**
 * Usuwanie folderu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DeleteFolderAction.java,v 1.5 2009/10/01 08:57:30 mariuszk Exp $
 */
public class DeleteFolderAction extends EventProcessingAction
{
	private Logger log = LoggerFactory.getLogger(DeleteFolderAction.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(DeleteFolderAction.class.getPackage().getName(),null);

    private static final String FORWARD = "repository/delete-folder";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION, new SetActionForwardListener("main"));

        registerListener("doCancel", new Cancel());

        registerListener("doDelete").
            append(new SetActionForwardListener("main")).
            append("delete", new Delete());
    }

    private class Cancel implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long folderId = (Long) event.getDynaForm().get("folderId");
            event.setForward(new ActionForward(ExploreDocumentsAction.getLink(
                event.getRequest(), folderId), true));
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long folderId = (Long) event.getDynaForm().get("folderId");
            DSContext ctx = null;
            boolean success = false;
            Long parentId = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                DocumentHelper dh = ctx.getDocumentHelper();

                Folder folder = Folder.find(folderId);

                // wyszukanie folderu nadrzędnego, którego zawartość zostanie
                // wyświetlona na stronie po usunięciu obecnego bieżącego folderu
                try
                {
                    Folder parent = folder.parent();
                    if (parent != null)
                        parentId = parent.getId();
                }
                catch (AccessDeniedException e)
                {
                }

                if (parentId == null)
                    parentId = Folder.getRootFolder().getId();

                dh.safeDeleteFolder(folder);

                ctx.commit();

                success = true;
            }
            catch (AccessDeniedException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                //event.getErrors().add(sm.getString("deleteFolder.accessDenied"));
                event.getErrors().add(e.getMessage());
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }

            if (success)
                event.setForward(new ActionForward(ExploreDocumentsAction.getLink(
                    event.getRequest(), parentId), true));
        }
    }

    public static String getLink(HttpServletRequest request, Long folderId,
                                 String prettyPath)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        try
        {
            return fc.getPath()+"?folderId="+folderId +
                "&prettyPath="+URLEncoder.encode(prettyPath, "iso-8859-2");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
