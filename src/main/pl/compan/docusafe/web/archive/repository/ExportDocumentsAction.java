package pl.compan.docusafe.web.archive.repository;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.DcLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ExportDocumentsAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory
			.getLogger(ExportDocumentsAction.class);

	private Map<String, String> documentKinds;
	private Map<Integer, String> docTypes;

	private String documentKindCn;
	private Integer docTypeId;
	private String path;

	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doExport").append(OpenHibernateSession.INSTANCE)
				.append(new Export()).append(fillForm).appendFinally(
						CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				List<DocumentKind> docKinds = DocumentKind.list(true);
				documentKinds = new LinkedHashMap<String, String>();
				docTypes = new LinkedHashMap<Integer, String>();

				for (DocumentKind docKind : docKinds)
				{
					documentKinds.put(docKind.getCn(), docKind.getName());
					if (DocumentLogicLoader.DC_KIND.equals(documentKindCn) && DocumentLogicLoader.DC_KIND.equals(docKind.getCn()))
						for (EnumItem enumItem : docKind.getFieldByCn(
								DcLogic.KATEGORIA_FIELD_CN).getEnumItems())
							docTypes.put(enumItem.getId(), enumItem.getTitle());
				}
			} catch (Exception e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}

	private class Export implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (path == null || path.equals(""))
				{
					addActionError("Musisz poda� �cie�k� dla plik�w na serwerze");
					return;
				}
				
				String eventParams = documentKindCn + ";;" + path;
				if (docTypeId != null)
					eventParams = documentKindCn + ";" + docTypeId + ";" + path;
				
				Long eventId = EventFactory.registerEvent("immediateTrigger",
						"ExportDocuments", eventParams, null,
						new java.sql.Timestamp(new java.util.Date().getTime()));
				log.debug("Zarejestrowano event o id {}", eventId);
				addActionMessage("Rozpocz�to eksport dokument�w w tle.");
			} catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	public void setDocumentKinds(Map<String, String> documentKinds)
	{
		this.documentKinds = documentKinds;
	}

	public Map<String, String> getDocumentKinds()
	{
		return documentKinds;
	}

	public void setDocumentKindCn(String documentKindCn)
	{
		this.documentKindCn = documentKindCn;
	}

	public String getDocumentKindCn()
	{
		return documentKindCn;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public Map<Integer, String> getDocTypes()
	{
		return docTypes;
	}
	
	public Integer getDocTypeId()
	{
		return docTypeId;
	}
	
	public void setDocTypeId(Integer docTypeCn)
	{
		this.docTypeId = docTypeCn;
	}
}
