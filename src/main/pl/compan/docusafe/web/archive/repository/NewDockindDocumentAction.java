package pl.compan.docusafe.web.archive.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.DaaAttachmentPattern;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.web.office.common.DwrDocument;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class NewDockindDocumentAction extends EventActionSupport implements DwrDocument
{
	private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(NewDockindDocumentAction.class.getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(NewDockindDocumentAction.class);
    // @EXPORT
    private FieldsManager fm;
    private Map<String,String> documentKinds;
    private String boxName;
    private Long folderId;
    private Long newDocumentId;
    private boolean canAddToRS;
    private List<DaaAttachmentPattern> patterns;

    // @IMPORT
    private Map<String,Object> values = new HashMap<String,Object>();
    private FormFile file;
    private boolean addToBox;

    // @EXPORT/@IMPORT
    private String documentKindCn;
    private String title;
    private String description;
    private Long boxId;
    private String NR_SZKODY;
    private Long attachmentPattern;  
    private boolean manualFolder;
    private String folderPrettyPath;
    private Map<String, Object> dockindKeys;

    private List<DSUser> usersByDocumentToUpdate;
    private List<Aspect> documentAspects;
    private String documentAspectCn;
    
    private boolean createNext = false;
    private boolean showCreateNext = false;
	private Document document;
	
	protected List<FormFile> multiFiles = new ArrayList<FormFile>();
    protected Map<String,String> loadFiles;
    protected String[] returnLoadFiles;
    protected String filestoken;
    
    private static final DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
    
    public boolean isNormalKind()
    {
        return DocumentKind.NORMAL_KIND.equals(documentKindCn);
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeDockind").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                dockindFillForm();
                dwrDocumentHelper.fillForm(NewDockindDocumentAction.this);
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * akcje zwi�zane z wype�nianiem formularzy na stronie JSP dla rzeczy zale�nych od rodzaju dokumentu
     * @throws EdmException
     */
    protected void dockindFillForm() throws EdmException
    {
        List<DocumentKind> docKinds = DocumentKind.listForCreate();
        documentKinds = new LinkedHashMap<String,String>();
        
        DocumentKind documentKind = null;

        for (DocumentKind docKind : docKinds)
        {
            documentKinds.put(docKind.getCn(), docKind.getName());
        }
        if (documentKindCn == null)
        {
            documentKind = DocumentKind.findByCn(DocumentKind.getDefaultKind(DocumentLogic.TYPE_ARCHIVE));
            documentKindCn = documentKind.getCn();
        }
        else
            documentKind = DocumentKind.findByCn(documentKindCn);

        fm = documentKind.getFieldsManager(null);
        fm.initialize();
        documentKind.logic().setInitialValues(fm, DocumentLogic.TYPE_ARCHIVE);

        showCreateNext = "true".equalsIgnoreCase(documentKind.getProperties().get("createNext"));
        
        documentAspects = documentKind.getAspectsForUser(DSApi.context().getPrincipalName());
        if(documentAspects == null)
        {
        	documentAspectCn = null;
        }
        else if(documentAspects != null && documentAspectCn == null)
        {
        	documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
        }
        // jak byl blad, to powracamy na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
        if (hasActionErrors() || createNext)
            fm.reloadValues(values);
        
        canAddToRS = documentKind.logic().canAddToRS();
        
        if (DocumentLogicLoader.DAA_KIND.equals(documentKindCn))
            patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();
        
        // rzeczy zwi�zane z pud�em archiwalnym
        String boxLine = documentKind.getProperties().get(DocumentKind.BOX_LINE_KEY);
        if (boxLine == null)
        {
            // pud�o z linii domy�lnej
            if (GlobalPreferences.isBoxOpen())
            {
                boxId = GlobalPreferences.getBoxId();                       
            }
            else
                boxId = null;
        }
        else
        {
            boxId = GlobalPreferences.getBoxId(boxLine);
        }
        if (boxId != null)
        {
            try
            {
                Box box = Box.find(boxId);
                boxName = box.getName();
                boxId = box.getId();
                
            }
            catch (EntityNotFoundException e)
            {
                boxId = null;
            }
        }  
    }
    /**
     * Metoda dodaj�ca za��czniki do dokumentu, pobiera zalaczniki z listy multiFiles zawierajacej aktualnie wczytane zalaczniki
     * oraz z tablicy returnLoadFiles zawierajace zalaczniki wczytane przed przeladowanie formularza (jesli takie nastapilo)
     * @param doc
     * @throws AccessDeniedException
     * @throws EdmException
     */
    protected void addFiles(Document doc) throws AccessDeniedException, EdmException
    {
    	
    	doc.setPermissionsOn(false);
    	if(multiFiles != null && multiFiles.size() > 0)
        {
        	for (FormFile formFile : multiFiles)
			{
        		 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			}            	
        }
        if( returnLoadFiles != null && returnLoadFiles.length > 0 )
        {
        	for (String fileKey : returnLoadFiles)
			{
        		String path = fileKey.replace("\\","/");
				File f = new File(path);
				 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(f).setOriginalFilename(f.getName().replace(filestoken+"_", ""));
			}
        }
    }

	public FormFile getMultiFiles() 
	{
		if(this.multiFiles != null && this.multiFiles.size() > 0 )
			return this.multiFiles.get(0);
		else
			return null;
	}
    
	public void setMultiFiles(FormFile file) {
		this.multiFiles.add(file);
	}
    

	public Map<String, String> getLoadFiles() {
		return loadFiles;
	}

	public void setLoadFiles(Map<String, String> loadFiles) {
		this.loadFiles = loadFiles;
	}

	public String[] getReturnLoadFiles() {
		return returnLoadFiles;
	}

	public void setReturnLoadFiles(String[] returnLoadFiles) {
		this.returnLoadFiles = returnLoadFiles;
	}

	public String getFilestoken() {
		return filestoken;
	}

	public void setFilestoken(String filestoken) {
		this.filestoken = filestoken;
	}

	/**Metoda zapoisuje pobrane zalaczniki to katalogu tymczasowego i dodaje wpis do loadFiles
     * Jest to robione na wypadek wystapienia bledu i powrotu jeszcze raz na formularz dodani dokumenu.
     * Zalaczniki sa pamietane i nie trzeba ich jeszcze raz przesylac */
    protected void saveFile()
    {
    	try
    	{
    		if(filestoken == null || filestoken.length() < 1)
    			filestoken = ServletActionContext.getRequest().getRequestedSessionId()+ new Date().getTime();
    		if(loadFiles == null)
    			loadFiles = new HashMap<String, String>();
	    	if(multiFiles != null && multiFiles.size() > 0)
	        {
	        	for (FormFile formFile : multiFiles)
				{   		
					File tmp = new File(Docusafe.getTemp(),filestoken+"_"+formFile.getFile().getName());
					FileOutputStream os = new FileOutputStream(tmp);
					FileInputStream stream = new FileInputStream(formFile.getFile());
					byte[] buf = new byte[2048];
					int count;
					while ((count = stream.read(buf)) > 0)
					{
					    os.write(buf, 0, count);
					}
					os.close();
					stream.close();
					loadFiles.put(tmp.getAbsolutePath(), formFile.getFile().getName());
				}
	        }
	    	if(returnLoadFiles != null && returnLoadFiles.length > 0)
	    	{
	    		for (String path : returnLoadFiles)
				{
	    			String[] tab = path.split("\\\\");
	    			String fileName = tab[tab.length -1].replace(filestoken+"_", "");
	    			loadFiles.put(path,fileName);
				}
	    	}
    	}
    	catch (Exception e) 
    	{
    		log.error("",e);
			addActionError("B��d zapisu plik�w "+e.getMessage());
		}
    }

    /**
     * dodaktowe rzeczy wykonywane podczas tworzenia dokumentu zale�ne od wybranego rodzaju dokumentu
     */
    protected void specificAdditions(OfficeDocument doc) throws EdmException
    {
        if (DocumentLogicLoader.DAA_KIND.equals(documentKindCn))
        {
            /* dodanie wzoru umowy */
            patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();
            if ((attachmentPattern != null) && (patterns.get(attachmentPattern.intValue() - 1).getFilename() != null))
            {
                File patternFile = patterns.get(attachmentPattern.intValue() - 1).getFile();

                if (patternFile != null)
                {
                    Attachment attachment = new Attachment("Wz�r umowy");
                    doc.createAttachment(attachment);
                    attachment.createRevision(patternFile).setOriginalFilename(patternFile.getName());
                }
                else
                    throw new EdmException(sm.getString("BladPodczasPobieraniaPlikuZeWzoremUmowy"));
            }
        }
    }    
    
	public Document getNewDocument()
	{
		return dwrDocumentHelper.getNewDocument(NewDockindDocumentAction.this);
	}
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (hasActionErrors())
                return;
            Long totaltime = null;
            try
            {
            	totaltime = System.currentTimeMillis();
            
                DSApi.context().begin();

                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
                
                documentKind.logic().correctValues(values, documentKind);
                // walidacja nie jest tu raczej potrzebna, gdy� wykonuje si� ona ju� na poziomie Javascriptu
                //documentKind.logic().validate(values, documentKind);

                String summary = TextUtils.trimmedStringOrNull(title);

                if (summary == null)
                    summary = documentKind.getName();

                description = TextUtils.trimmedStringOrNull(description);

                if (description == null)
                    description = summary;

                Document document = new Document(
                    summary,
                    description);

                document.setFolder(Folder.getRootFolder());
                document.setDocumentKind(documentKind);

                document.create();
                DSApi.context().session().flush();
                
                newDocumentId = document.getId();
                //jak tworzy to ma wszytstkie uprawnienia(nie zapisuje sie to w bazie)
                document.setPermissionsOn(false);
                
                //documentKind.set(newDocumentId, values);
                if (AvailabilityManager.isAvailable("dwr"))
                {
                	setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
                	documentKind.logic().setAfterCreateBeforeSaveDokindValues(documentKind ,document.getId(), dockindKeys);
					documentKind.saveDictionaryValues(document, dockindKeys);
                	documentKind.saveMultiDictionaryValues(Document.find(newDocumentId), dockindKeys);
                	documentKind.set(newDocumentId, dockindKeys);   
                }
                else
                {
                	documentKind.set(newDocumentId, values);
                }
                documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
                documentKind.logic().documentPermissions(document);
                
                if (folderId != null && DocumentKind.NORMAL_KIND.equals(documentKind.getCn()))
                    document.setFolder(Folder.find(folderId),true); 
                
                folderId = document.getFolderId();
                //event.getLog().debug("folderId="+folderId);

                if (file != null && file.getFile() != null)
                {
                    Attachment attachment = new Attachment("Skan");
                    document.createAttachment(attachment);

                    attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                }
                DSApi.context().session().flush();

                if (addToBox && boxId != null)
                {
                    document.setBox(Box.find(boxId));
                }

                
                HttpSession session = ServletActionContext.getRequest().getSession();
                session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
                //LoggerFactory.getLogger("tomekl").debug("doc {} next {}",document.getId(), createNext);
                //addActionError("test reload values");
                addFiles(document);
                if(createNext)
                {
                	title = null;
                	description = null;
                	StringBuffer sb = new StringBuffer();
                	sb.append("Utwozono dokument, id");
                	sb.append(' ');
                	sb.append(document.getId());
                	addActionMessage(sb.toString());
                }
                else if (NR_SZKODY != null && DSApi.context().hasPermission(DSPermission.NW_WPIS_RS))
                {
                    event.setResult("nw-drs");
                }
                else 
                {
                    event.setResult("explore-documents");
                }
                if (DSApi.context().inTransaction())
               	 DSApi.context().commit();
            }
            catch (Exception e)
            {
            	saveFile();
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                log.trace("Create: "+(System.currentTimeMillis()-totaltime));
            }
        }
    }
    
	public Long getDocumentId() {
		return newDocumentId;
	}

	public void setDocumentId(Long documentId) {
		newDocumentId = documentId;
		
	}

	public void setDockindCn(String cn) {
		documentKindCn = cn;
		
	}

	public int getDocType() {
		return DocumentLogic.TYPE_ARCHIVE;
	}


    public FieldsManager getFm()
    {
        return fm;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds()
    {
        return documentKinds;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public Long getFolderId()
    {
        return folderId;
    }

    public void setFolderId(Long folderId)
    {
        this.folderId = folderId;
    }

    public void setAddToBox(boolean addToBox)
    {
        this.addToBox = addToBox;
    }

    public Long getBoxId()
    {
        return boxId;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public String getBoxName()
    {
        return boxName;
    }

    public String getNR_SZKODY()
    {
        return NR_SZKODY;
    }

    public void setNR_SZKODY(String NR_SZKODY)
    {
        this.NR_SZKODY = NR_SZKODY;
    }
 
    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public boolean isCanAddToRS()
    {
        return canAddToRS;
    }        
    
    public String getDockindAction()
    {
        return "create";
    }

	public boolean isManualFolder() {
		return manualFolder;
	}

	public void setManualFolder(boolean manualFolder) {
		this.manualFolder = manualFolder;
	}

	public String getFolderPrettyPath() {
		return folderPrettyPath;
	}

	public void setFolderPrettyPath(String folderPrettyPath) {
		this.folderPrettyPath = folderPrettyPath;
	}

	public List <Aspect> getDocumentAspects() {
		return documentAspects;
	}

	public String getDocumentAspectCn() {
		return documentAspectCn;
	}

	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}

	public boolean isNewDocumentAction()
	{
		return true;
	}

	public boolean isCreateNext() {
		return createNext;
	}

	public void setCreateNext(boolean createNext) {
		this.createNext = createNext;
	}

	public boolean isShowCreateNext() {
		return showCreateNext;
	}

	public void setShowCreateNext(boolean showCreateNext) {
		this.showCreateNext = showCreateNext;
	}


	public void setDocumentKinds(Map<String,String> documentKinds) {
		this.documentKinds = documentKinds;
		
	}

	public Map<String, Object> getDockindKeys() {
		return this.dockindKeys;
	}

	public void setDockindKeys(Map<String, Object> dockindKeys) {
		this.dockindKeys = dockindKeys;
		
	}

	public void setDocument(Document document) {
		this.document = document;
		
	}

	public Document getDocument() {
		return this.document;
	}

	public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException {
		dwrDocumentHelper.setBeforeUpdate(NewDockindDocumentAction.this);
		
	}

	public Logger getLog() {
		return log;
	}
        
     /**        ---------- FUNKCJA DO ODFILTROWANIA UZYTKOWNIKOW W RAZIE POTRZEBY
     * potrzebny <available name="archiwizacja.dokument.updatujListeUzytkownika" value="true"/> w xml
     */
        
   /**
     * funkcja potrzebna do modyfikowania listy uzytkownik�w
     * @return
     * @throws EdmException 
     */
    public List<DSUser> getUsersByDocumentToUpdate() throws EdmException
    {
        try
        {
//            List<DSUser> usersByDocumentToUpdate = new ArrayList<DSUser>();
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            
            if(documentKindCn.equals("overtime_app"))
                usersByDocumentToUpdate = DSUser.listAcceptanceSubordinates(DSApi.context().getPrincipalName());
            
        } catch (EdmException e){
            log.error("",e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();

        } finally{
            try{
                DSApi.close();
            } catch (EdmException e){}
        }

        return usersByDocumentToUpdate;
    }
    
    public String getUsersDockindFieldUpdate()
    {
        log.error(documentKindCn);
        
        if(documentKindCn.equals("overtime_app"))
            return "DS_USER";
        
        return "DS_USER";
    }
}

