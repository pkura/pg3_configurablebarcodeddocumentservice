package pl.compan.docusafe.web.archive.repository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.parametrization.ald.ALDInvoiceInfo2;
import pl.compan.docusafe.parametrization.ald.ALDManager;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;


public class ALDInvoiceVerificationAction extends EventActionSupport
{
	static final Logger log = LoggerFactory.getLogger(ALDInvoiceVerificationAction.class);
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ALDInvoiceVerificationAction.class.getPackage().getName(),null);
    // @EXPORT
    private List<Map<String, Object>> invoiceBeans;

    // @IMPORT
    private FormFile file;
    private String sortField;
    private boolean asc;
    private StringManager langG;

    private Long[] documentIds;
    private static String EV_FILL = "fill";
    private boolean streamPdf = false;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                langG = GlobalPreferences.loadPropertiesFile("",null);
                List<ALDInvoiceInfo2> invoiceInfos2 = ALDInvoiceInfo2.find(DSApi.context().getPrincipalName(), sortField, asc);
                invoiceBeans = ALDManager.prepareInvoice2Beans(invoiceInfos2);
                if(streamPdf)
                {
                	 //FIXME
                	 //DSApi.context().begin();
                     if(documentIds==null) 
                         addActionError(sm.getString("NieWybranoDokumentow"));
                     else
                     {
                         //nie zawsze znajdzie dokumenty wiec w tablicy moga byc nulle
                         List<Long> tmpList = new ArrayList<Long>();
                         
                         for(Long l : documentIds)
                             if(l!=null)
                                 tmpList.add(l);
                         
                         if(tmpList.size()==0)
                         {
                             addActionError(sm.getString("NieWybranoDokumentow"));
                             return;
                         }
                         
                         documentIds = new Long[tmpList.size()];
                         for(int i=0; i<tmpList.size(); i++)
                             documentIds[i] = tmpList.get(i);
                         
                         Document[] docsView = new Document[documentIds.length];
                         for(int i=0;i<documentIds.length;i++)
                             docsView[i]= Document.find(documentIds[i]);
                         File tmp = null;
                         OutputStream os = null;
                         try
                         {
                             	tmp = File.createTempFile("docusafe_", ".pdf");
                             	os = new BufferedOutputStream(new FileOutputStream(tmp));

                             	PdfUtils.attachmentsAsPdf(docsView, true, os);
                             	//os.close();
                         }
                         catch (Exception e)
                         {
                             log.error(e.getMessage(), e);
                         }
                         finally
                         {
                        	 org.apache.commons.io.IOUtils.closeQuietly(os);
                             //nie potrzebne DSApi.context().setRollbackOnly();
                         }

                         if (tmp != null)
                         {
                             	event.setResult(NONE);
                             	try
                             	{
                             		ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(tmp),
                                         "application/pdf", (int) tmp.length(),"Content-Disposition: attachment; filename=\"DocusafeReport"  + DateFormat.getDateTimeInstance().format(new Date()) + ".pdf\"");
                             	}
                             	catch (IOException e)
                             	{
                             		log.error(e.getMessage(), e);
                             	}
                             	finally
                             	{
                             		tmp.delete();
                             	}
                         }
                     }
                     //DSApi.context().rollback();
                	
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class LoadFile implements ActionListener
    {    	
        public void actionPerformed(ActionEvent event)
        {
        	log.debug("werfikacja pliku faktur");
            if (file == null || !file.sensible())
            {
                addActionError(sm.getString("NieWybranoPliku"));
                return;
            }
            if(!file.getFile().getName().endsWith(".csv"))
            {
                addActionError(sm.getString("WybranyZalacznikJestWniedopuszczalnymFormacie"));
                return;
            }
            
            event.skip(EV_FILL);
            
            try
            {
                DSApi.context().begin();
                
                // usuwamy wcześniejsze wpisy tego użytkownika
                ALDManager.deleteInvoiceInfos2(DSApi.context().getPrincipalName());
                DSApi.context().commit();
                //skroci transakcje
                DSApi.context().begin();
                // dodajemy nowe z podanego pliku i przetwarzamy wczytane dane
                List<ALDInvoiceInfo2> invoiceInfos2 = ALDManager.parseAndSaveVATRegistryFile(file.getFile());
                invoiceBeans = ALDManager.prepareInvoice2Beans(invoiceInfos2);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    } 


    public String getSortLink(String sortField, boolean asc)
    {
        return "/repository/ald-invoice-verification.action?sortField="+sortField+"&asc="+asc;
    }

    public List<Map<String, Object>> getInvoiceBeans()
    {
        return invoiceBeans;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public void setAsc(boolean asc)
    {
        this.asc = asc;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setDocumentIds(Long[] documentIds) {
        this.documentIds = documentIds;
    }

	public void setStreamPdf(boolean streamPdf) {
		this.streamPdf = streamPdf;
	}

	public boolean isStreamPdf() {
		return streamPdf;
	}

    
}
