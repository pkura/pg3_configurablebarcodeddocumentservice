package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.Tab;
import std.fun;
import std.lambda;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.XmlImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;

import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.HttpServletResponse;

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 * @version $Id$
 */
public class AgenciesDictionaryAction extends EventActionSupport
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(AgenciesDictionaryAction.class.getPackage().getName(),null);
    private String exportFilename;
    private FormFile importFile;

    private List<Tab> tabs;
    
    protected void setup()
    {
        if (!Configuration.hasExtra("business"))
            throw new Error("Akcja dost�pna tylko dla licencji business");

        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doExport").
            append(OpenHibernateSession.INSTANCE).
            append(new ExportDictionary()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!Configuration.hasExtra("business"))
                    throw new Error(sm.getString("AkcjaDostepnaTylkoDlaLicencjiBusiness"));

                tabs = ImportDocumentsTabs.createTabs(ImportDocumentsTabs.TAB_DICTIONARY);
                
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class ExportDictionary implements ActionListener
    {
        private String prepareForCsv(Object val)
        {
            if (val == null)
                return "";
            return val.toString().replace(';', ',');
        }
        
        public void actionPerformed(ActionEvent event)
        {
            PrintStream output = null;
            try
            {
                List<DaaAgencja> daaAgencjaList = DaaAgencja.findAll();
                
                if (exportFilename == null || exportFilename.trim().length() == 0)
                {
                    throw new EdmException(sm.getString("NiePodanoNazwyPliku"));
                }
                
                output = new PrintStream(exportFilename);
                
                for (DaaAgencja agencja : daaAgencjaList)
                {
                    StringBuilder cvsLine = new StringBuilder();
                    cvsLine.append(prepareForCsv(agencja.getId()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getNazwa()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getUlica()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getKod()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getMiejscowosc()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getRodzaj_sieci()) + ";");
                    cvsLine.append(prepareForCsv(agencja.getNumer()));
                    output.println(cvsLine.toString());
                }
                addActionMessage(sm.getString("SlownikAgencjiZostalWyeksportowanyDoPliku")+" " + exportFilename);
            }
            catch (FileNotFoundException e)
            {
                addActionError(sm.getString("NieMoznaUtworzycPliku"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                if (output != null)
                    output.close();
            }
        }
    }

    public List<Tab> getTabs()
    {
        return tabs;
    }

    public void setExportFilename(String exportFilename)
    {
        this.exportFilename = exportFilename;
    }

    public String getExportFilename()
    {
        return exportFilename;
    }

    public void setImportFile(FormFile importFile)
    {
        this.importFile = importFile;
    }

    public FormFile getImportFile()
    {
        return importFile;
    }
}
