package pl.compan.docusafe.web.archive.repository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.dockinds.other.InvoiceInfo;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class InvoiceVerificationAction extends EventActionSupport
{
	static final long serialVersionUID = 1L;
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ALDInvoiceVerificationAction.class.getPackage().getName(),null);
	private Logger log = LoggerFactory.getLogger(InvoiceVerificationAction.class);
    // @EXPORT
    private List<Map<String, Object>> invoiceBeans;

    // @IMPORT
    private FormFile file;
    private String sortField;
    private boolean asc;
    private StringManager langG;
    private Integer invoiceType;
    private Boolean rejestrWybor;

    private Long[] documentIds;
    private static String EV_FILL = "fill";
    private boolean streamPdf = false;

    protected void setup()
    {
        //FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("default").
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetCSV").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetCSV()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doChangeProcessStatus").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ChangeProcessStatus()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	//System.out.println(invoiceType);
            	rejestrWybor = AvailabilityManager.isAvailable("invoice.verification.zakup");
            	if(invoiceType==null) invoiceType = InvoiceInfo.INVOICE_TYPE_KOSZT;
                langG = GlobalPreferences.loadPropertiesFile("",null);
                List<InvoiceInfo> invoiceInfos = InvoiceInfo.find(DSApi.context().getPrincipalName(), sortField, invoiceType, asc);
                invoiceBeans = InvoiceLogic.prepareInvoice2Beans(invoiceInfos);
                if(streamPdf)
                {
                	//FIXME - dlaczego context rollbakuje sie z automatu?
                	 //DSApi.context().begin();
                     if(documentIds==null) 
                         addActionError(sm.getString("NieWybranoDokumentow"));
                     else
                     {
                         //nie zawsze znajdzie dokumenty wiec w tablicy moga byc nulle
                         List<Long> tmpList = new ArrayList<Long>();
                         
                         for(Long l : documentIds)
                             if(l!=null)
                                 tmpList.add(l);
                         
                         if(tmpList.size()==0)
                         {
                             addActionError(sm.getString("NieWybranoDokumentow"));
                             return;
                         }
                         
                         documentIds = new Long[tmpList.size()];
                         for(int i=0; i<tmpList.size(); i++)
                             documentIds[i] = tmpList.get(i);
                         
                         Document[] docsView = new Document[documentIds.length];
                         for(int i=0;i<documentIds.length;i++)
                             docsView[i]= Document.find(documentIds[i]);
                         File tmp = null;
                         OutputStream os = null;
                         try
                         {
                             	tmp = File.createTempFile("docusafe_", ".pdf");
                             	os = new BufferedOutputStream(new FileOutputStream(tmp));

                             	PdfUtils.attachmentsAsPdf(docsView, true, os);
                             	//os.close(); -- zamykane w finally
                         }
                         catch (Exception e)
                         {
                             event.getLog().error(e.getMessage(), e);
                         }
                         finally
                         {
                        	 org.apache.commons.io.IOUtils.closeQuietly(os);
                             //DSApi.context().setRollbackOnly();
                         }

                         if (tmp != null)
                         {
                             	event.setResult(NONE);
                             	try
                             	{
                             		ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(tmp),
                                         "application/pdf", (int) tmp.length(),"Content-Disposition: attachment; filename=\"DocusafeReport"  + DateFormat.getDateTimeInstance().format(new Date()) + ".pdf\"");
                             	}
                             	catch (IOException e)
                             	{
                             		event.getLog().error(e.getMessage(), e);
                             	}
                             	finally
                             	{
                             		tmp.delete();
                             	}
                         }
                     }
                     //FIXME - po co rollbak
                     //DSApi.context().rollback();
                	
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class LoadFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	//System.out.println(invoiceType);
        	
        	rejestrWybor = AvailabilityManager.isAvailable("invoice.verification.zakup");
        	if(invoiceType==null) invoiceType = InvoiceInfo.INVOICE_TYPE_KOSZT;
            if (file == null || !file.sensible())
            {
                addActionError(sm.getString("NieWybranoPliku"));
                return;
            }
            if(!file.getFile().getName().endsWith(".csv"))
            {
                addActionError(sm.getString("WybranyZalacznikJestWniedopuszczalnymFormacie"));
                return;
            }
            
            event.skip(EV_FILL);
            
            try
            {
                // usuwamy wcześniejsze wpisy tego użytkownika
                InvoiceLogic.deleteInvoiceInfos(DSApi.context().getPrincipalName(), invoiceType);
                // dodajemy nowe z podanego pliku i przetwarzamy wczytane dane
                List<InvoiceInfo> invoiceInfos = new ArrayList<InvoiceInfo>();
                if (invoiceType.equals(InvoiceInfo.INVOICE_TYPE_KOSZT)) {
					invoiceInfos = InvoiceICLogic.parseAndSaveFVFile(file.getFile());
				} else if (invoiceType.equals(InvoiceInfo.INVOICE_TYPE_ZAKUP)) {
					invoiceInfos = InvoiceLogic.parseAndSaveFVZakupFile(file.getFile());
				}
                invoiceBeans = InvoiceLogic.prepareInvoice2Beans(invoiceInfos);
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    } 
    private class GetCSV implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	FileWriter fw = null;
        	try
        	{
        		event.setResult(NONE);
	        	List<InvoiceInfo> invoiceInfos = InvoiceInfo.find(DSApi.context().getPrincipalName(), sortField, asc);
	        	File t = File.createTempFile("VatVerification", ".csv");
        		fw = new FileWriter(t);
	        	for(InvoiceInfo ii: invoiceInfos)
	        	{
	        		fw.write(ii.getInvoiceNumber()+";");
	        		fw.write(ii.getNip()+";");
	        		fw.write(ii.getInvDate()+";");
	        		fw.write(ii.getOrigAmountBrutto()+";");
	        		fw.write(ii.getAmountBrutto()+";");
	        		fw.write(ii.getErrorInfo()+";");
	        		fw.write(ii.getStatusDescription()+";");
	        		fw.write("\n");
	        	}
	        	fw.close();
	        	ServletUtils.streamFile(ServletActionContext.getResponse(),t,
                        "text/plain", "Content-Disposition: attachment; filename=\"DocusafeReport"  + DateFormat.getDateTimeInstance().format(new Date()) + ".csv\"");
	        	
        	}
        	catch(Exception e)
        	{
        		addActionError(sm.getString("WystapilBladGenerowaniaPlikuCSV"));
        		event.getLog().debug(e);
        		
        	}
        	finally
        	{
        		org.apache.commons.io.IOUtils.closeQuietly(fw);
        	}
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (documentIds == null || documentIds.length==0)
            {
                addActionError(sm.getString("NieWybranoFaktur"));
                return;
            }
            
            try
            {
                DSApi.context().begin();
                
                // usuwamy wcześniejsze wpisy tego użytkownika
                InvoiceLogic.deleteInvoiceInfos(DSApi.context().getPrincipalName(), documentIds);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
   
    private class  ChangeProcessStatus implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (documentIds == null || documentIds.length==0)
            {
                addActionError(sm.getString("NieWybranoFaktur"));
                return;
            }
            
            try
            {
                DSApi.context().begin();
                
                
                InvoiceLogic.chanegeProcessStatusInvoiceInfos(DSApi.context().getPrincipalName(), documentIds, InvoiceInfo.PROCESS_STATUS_DONE);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    public String getSortLink(String sortField, boolean asc)
    {
        return "/repository/invoice-verification.action?sortField="+sortField+"&asc="+asc;
    }

    public List<Map<String, Object>> getInvoiceBeans()
    {
        return invoiceBeans;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public void setAsc(boolean asc)
    {
        this.asc = asc;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setDocumentIds(Long[] documentIds) {
        this.documentIds = documentIds;
    }

	public void setStreamPdf(boolean streamPdf) {
		this.streamPdf = streamPdf;
	}

	public boolean isStreamPdf() {
		return streamPdf;
	}

	public Long[] getDocumentIds() {
		return documentIds;
	}

	public Integer getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(Integer invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Boolean getRejestrWybor() {
		return rejestrWybor;
	}

	public void setRejestrWybor(Boolean rejestrWybor) {
		this.rejestrWybor = rejestrWybor;
	}

    
}

