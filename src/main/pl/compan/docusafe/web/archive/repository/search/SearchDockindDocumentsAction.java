package pl.compan.docusafe.web.archive.repository.search;

import com.asprise.util.tiff.TIFFReader;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.json.JSONArray;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.*;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.dictionary.LocationForIC;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.DaaLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshotXml;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.nationwide.CompileAttachmentsManager;
import pl.compan.docusafe.parametrization.adm.AdmDwrPermissionFactory;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;


/* User: Administrator, Date: 2007-03-06 12:08:02 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class SearchDockindDocumentsAction extends EventActionSupport
{
    
    private static final Logger log = LoggerFactory.getLogger(SearchDockindDocumentsAction.class);
    static StringManager sm = GlobalPreferences.loadPropertiesFile(SearchDockindDocumentsAction.class.getPackage().getName(), null);
    private static final int LIMIT = 10;
    protected boolean omitLimit;

    /**
     *  to jest tablica numerow id wyszukanych pism
     *  potrzebna jest do akcji archive. Akcja ta polega na tym,
     *  na wyszukanych pismach wywoluje sie archiveaction
     */
    protected Long[] ids;

    // @EXPORT
    private Map<String,String> documentKinds;
    protected DocumentKind kind;
    private List<DSUser> accessedByUsers;
    protected String attachmentsAsPdfUrl;
    protected String attachmentsAsPdfUrlPartially;
    protected List<Map<String, Object>> results;
    protected List<TableColumn> columns;
    protected Pager pager;
    protected boolean canCompileAttachments;
    protected String addDocsForCompilationUrl;
    private boolean adminAccess;//czy uzytkownik ma uprawnienia admina
    private String searchStyle;
    private boolean canGenerateXlsReport;
	private boolean canGenerateCsvReport;
	private boolean canGenerateXmlReport;
	private static  boolean searchByPermisionClass =  AvailabilityManager.isAvailable("search.dokind.document.action.ByPermisiionClass");
    //zamowienia
	private Integer orderKind;
	private Map<Integer,String> orderKindMap;
	private Long location;
	private Integer priority;
	private Map<Integer,String> priorityMap;
	private String remark;
	private Integer deliveryKind;
	private Map<Integer,String> deliveryKindMap;

    // @EXPORT/@IMPORT
    protected String documentKindCn;
    private boolean resultsInPopup;
    protected Map<String,Object> values = new HashMap<String,Object>();
    protected Map<String,Object> invalues = new HashMap<String,Object>();
    protected Map<String,Object> flagiMap;
    protected boolean ascending;
    private String sortField;
    protected String boxName;
    protected String accessedBy;
    private String acceptedBy;
    protected String[] accessedAs;
    private String accessedFrom;
    private String accessedTo;
    private String[] forceSearchEq;
    private Long documentId;
    private String documentIdString;
    protected Long officeNumberYear;
    private String[] opsParts;
    protected Document[] resultDocumentsTable;
    private String documentIdsString;
    //flagi
    protected List<Flags.FlagBean> flags;
    protected List<Flags.FlagBean> userFlags;
    protected Long[] flagIds;
    protected Long[] userFlagIds;
    protected String flagsDateFrom;
    protected String flagsDateTo;
    protected String userFlagsDateFrom;
    protected String userFlagsDateTo;
    protected boolean flagsOn;

    private String ctimeFrom;
    private String ctimeTo;
    
    private String author;

    private boolean czyAktualny;
    
    // @IMPORT
    protected int offset;
    protected int limit;
    /** dokumenty, z kt�rych maj� by� pobrane za��czniki do kompilacji */ 
    //private Map<String,Integer> docs;
    private Long[] documentIds;
    private String newSearchStyle;
    
    private String fullTextSearch;

	/** DsDocument fields */
    /** lista dost�pnych dziennik�w */
    private List<Journal> journals;
    /** wybrany dziennik */
    private Long journalId;
    /** Opis dokumentu */
    private String description;
    private String abstractDescription;
    private String documentDateFrom;
    private String documentDateTo;
    private String postalRegNumber;
    private String officeNumber;
    private String officeNumberFrom;
    private String officeNumberTo;
    protected String documentReferenceId;
	protected String documentOfficeCaseSymbol;
   
    private String viewtimeFrom;
    private String viewtimeTo;
    private String[] viewUsers;
    private List<DSUser> viewedByUsers;
    private boolean notViewed;

    protected SearchCriteria senderSC = new SearchCriteria("dso_person", "SENDER");
    protected SearchCriteria recipientSC = new SearchCriteria("dso_person", "RECIPIENT");

    //centra kosztow
    private List<CentrumKosztow> centra; 
    private List<AccountNumber> konta;
    private List<LocationForIC> locations;
    //private List<Integer> centra_sel;
    //private Integer[] centra_sel;
    private Integer centra_sel;
    private String actionName;
    private String fieldValuesSource;
    /** link dla aktualnego wyszukania (ale bez parematr�w do sortowania) */
    protected String thisSearchUrl;

    private List<Aspect> documentAspects;
    private String documentAspectCn;
    private boolean orderDocs = false;
    private Integer count;
    private boolean canOrder = false;
    
    private boolean canMultichange = false;
    
    protected static final String EV_FILL = "fillForm";

    private String saveSearchName;
    private Set<String> saveSearchNames;

    /** mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy wynik�w (w klasie QueryForm) */
    private static final Map<String, String> sortProperties = new HashMap<String, String>();
    static {
        sortProperties.put("document_id", "id");
        sortProperties.put("document_title", "title");
        sortProperties.put("document_ctime", "ctime");
        sortProperties.put("document_mtime", "mtime");
    }
    
    private String redirectUrl;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClear").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clear()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doChangeDockind").
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm(true)).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeStyle").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGeneratePdfPartially").
	        append(OpenHibernateSession.INSTANCE).
	        //append(new PrepareArchive()).//omit limit
	        //append(new Search()).
	        append(new GeneratePDFPartially()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAction").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new PrepareArchive()).//omit limit
	    	append(new Search()).
	    	append(new SpecifiedAction()).
	    	append(EV_FILL, fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);       

	    
        registerListener("doMultiChangeAttributes").
	        append(OpenHibernateSession.INSTANCE).
	        append(new MultiChangeAttributes()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doArchive").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareArchive()).
            append(new Search()).
            append(new Archive()).
            //append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
		
        registerListener("doAddDocsForCompilation").
            append(OpenHibernateSession.INSTANCE).
            append(new AddDocumentsForCompilation()).
            append(new Search()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doReCheck").
	        append(OpenHibernateSession.INSTANCE).
	        append(new PrepareArchive()).
	        append(new Search()).
	        append(new Recheck()).
	        append(EV_FILL, fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);        
    }

    protected void prepOrder() throws EdmException {}

    public String getAcceptedBy() {
        return acceptedBy;
    }

    public void setAcceptedBy(String acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    private class Clear implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	ServletActionContext.getRequest().getSession().removeAttribute("values");
        	ServletActionContext.getRequest().getSession().removeAttribute("invalues");
        	ServletActionContext.getRequest().getSession().removeAttribute("flagiMap");
        	ServletActionContext.getRequest().getSession().removeAttribute("senderMap");
        	ServletActionContext.getRequest().getSession().removeAttribute("recipientMap");
        	values = new HashMap<String,Object>();
            invalues = new HashMap<String,Object>();
            flagiMap = null; //new HashMap<String,Object>();
        	fieldValuesSource = null;
        	documentId = null;
        	documentIdString=null;
        	setOfficeNumber(null);
        	setOfficeNumberFrom(null);
        	setOfficeNumberTo(null);
        	setOfficeNumberYear(null);
        	setDocumentOfficeCaseSymbol(null);
        	setDocumentReferenceId(null);
            flagsDateFrom = null;
            flagsDateTo = null;
            userFlagsDateFrom = null;
            userFlagsDateTo = null;
            accessedFrom = null;
            accessedTo = null;

            documentDateFrom = null;
            documentDateTo = null;

            recipientSC.clear();
            senderSC.clear();

            description = null;
            postalRegNumber = null;
            saveSearchName = null;
            saveSearchNames = null;
        }
    }
    
    private class FillForm implements ActionListener
    {
        private boolean doChangeDockind;
        
        public FillForm()
        {           
        }
        
        public FillForm(boolean doChangeDockind)
        {
            this.doChangeDockind = doChangeDockind;
        }
        
        public void actionPerformed(ActionEvent event)
        {
            try
            {        
            	prepOrder();

                flagsOn = "true".equals(Configuration.getProperty("flags"));
                
                if (flagsOn)
                {
                    flags = Flags.listFlags(false);
                    userFlags = Flags.listFlags(true);
                } 
                
                adminAccess = DSApi.context().getDSUser().isAdmin();
                List<DocumentKind> docKinds = DocumentKindProvider.get().visible().list();
                documentKinds = new LinkedHashMap<String,String>();
                
                try
                {
                	centra = CentrumKosztow.listAll();
                	konta = AccountNumber.listAll();
                	locations = LocationForIC.list();
                }
                catch (Exception e) 
				{
					log.debug(e.getMessage(), e);
				}

                for (DocumentKind docKind : docKinds)
                {
                    documentKinds.put(docKind.getCn(), docKind.getName());
                }
                
                if(AvailabilityManager.isAvailable("searchDockinds.allDockinds.bfl"))
                	documentKinds.put("allDockinds", "Wszystkie");
                
                
                if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    accessedByUsers = DSUser.listWithDeletedCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA))
                {
                    // tylko z dzia�u
                    accessedByUsers = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    accessedByUsers = new ArrayList<DSUser>(1);
                    accessedByUsers.add(DSApi.context().getDSUser());
                }
                viewedByUsers = Lists.newArrayList(accessedByUsers);

                if("allDockinds".equals(documentKindCn))
                {
                	return;
                }
                if (documentKindCn == null)
                {
                    kind = DocumentKind.findByCn(DocumentKind.getDefaultKind());
                    if (AvailabilityManager.isAvailable("p4.parcelRegistrOnlyForMineralna"))
                    {
                        kind =   DocumentKind.findByCn("normal");
                    }
                    if(kind == null)
                    {
                    	throw new EdmException("W systemie brakuje dokumentu zdefiniowanego w adds.properties jakos domy�lnego dla Docusafe");
                    }
                    documentKindCn = kind.getCn();
                }
                else
                    kind = DocumentKind.findByCn(documentKindCn);
                
                if(AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")){
                	kind = checkIsFromRedirectAction(kind);
                	documentKindCn = kind.getCn();
                }	
                
               
                kind.initialize();
                documentAspects = kind.getAspectsForUser(DSApi.context().getPrincipalName());
                if(documentAspects != null && documentAspectCn == null)
                {
                	documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
                }
                
                
                if(!DSApi.context().userPreferences().node("ops").get("toSave", "brak").equalsIgnoreCase("brak"))
                {
                	opsParts = DSApi.context().userPreferences().node("ops").get("toSave", "brak").split("\\s*;\\s*");
                }

                try {
                    Field djf = kind.getFieldByCn(DocumentJournalField.CN);
                    if(djf instanceof DocumentJournalField){
                    	
                    	if(kind.logic().canSearchAllJournals())
                        journals = Journal.listAllByType(((DocumentJournalField) djf).getJournalType());
                    	else {
                    		journals = kind.logic().getJournalsPermissed(((DocumentJournalField) djf).getJournalType());
                    	}
                    }
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }


                
                if (newSearchStyle != null)
                    searchStyle = newSearchStyle;
                else
                {
                    if (searchStyle == null || doChangeDockind)
                    {
                        searchStyle = kind.getProperties().get(DocumentKind.SEARCH_STYLE);
                        if (searchStyle == null)
                            searchStyle = "normal";
                    }
                }
                ServletActionContext.getRequest().getSession().removeAttribute("recipientMap");
                ServletActionContext.getRequest().getSession().removeAttribute("senderMap");

                String names = DSApi.context().userPreferences().node("search").get("s-names","");
                saveSearchNames = Sets.newHashSet(Splitter.on(';').trimResults().omitEmptyStrings().split(names));
                saveSearchName = null;
            }
            catch (EdmException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private void GeneratePDF(ActionEvent event)
    {
        	try{
       		 
        		DSApi.context().begin();
        		
                File tmp = null;
                try
                {
                    tmp = File.createTempFile("docusafe_", ".pdf");
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                    
                    PdfUtils.attachmentsAsPdf(resultDocumentsTable, true, os);
                    
                    os.close();
                }
                catch (Exception e)
                {
                    log.error(e.getMessage(), e);
                }
                finally
                {
                    DSApi.context().setRollbackOnly();
                }

                if (tmp != null)
                {
                    event.setResult(NONE);
                    event.skip(EV_FILL);
                    try
                    {
                    	HttpServletResponse resp = ServletActionContext.getResponse();
                    	resp.setContentType("application/pdf");
                		resp.setHeader("Content-disposition", "attachment; filename=\"documents.pdf\"");
                        ServletUtils.streamFile(resp, tmp, "application/pdf");
                    }
                    catch (IOException e)
                    {
                        log.error(e.getMessage(), e);
                    }
                    finally
                    {
                        tmp.delete();
                    }
                }

                DSApi.context().rollback();
        	}
        	catch(EdmException e)
            {
                event.addActionError(e.getMessage());
            }        
    }
    /**
     * Sprawdza czy wywolanie formatki nast�pi�o z przekierowania paczek dokument�w
     * jesli tak to ustawia dokinda paczke 
     * 
     * @param kind
     * @return
     * @throws EdmException
     */
    private DocumentKind checkIsFromRedirectAction(DocumentKind kind) throws EdmException
	{
    	DocumentKind newkind = new DocumentKind();
    	if(	ServletActionContext.getRequest().getParameter("package")!=null
    			&& !ServletActionContext.getRequest().getParameter("package").isEmpty()
    			&& ServletActionContext.getRequest().getParameter("package").contains("true")){
    		
    		if (Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
    			newkind = DocumentKind.findByCn(Docusafe.getAdditionProperty("kind-package"));
    			
    		}
    	}else {
    		return kind;
    	}
    		return newkind;	
	}


	private class SpecifiedAction implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if("doGeneratePdf".equals(actionName)) GeneratePDF(event);
        	//else if(("doGeneratePdfPartially".equals(actionName))) GeneratePDFPartially(event);
        	//else if(("doRockwellReport".equals(actionName))) GenerateRockwellXLS(event);
        	else if(("doGenerateXls".equals(actionName))) GenerateXLS(event);
        	else if (("doGenerateCsv".equals(actionName))) GenerateCSV(event);
			else if (("doGenerateXml".equals(actionName))) GenerateXML(event);
        	else if(("doGenerateAldXls".equals(actionName))) GenerateAldXLS(event);
        	else if(("doGenerateXlsFromChecked".equals(actionName))) GenerateXLSFromChecked(event);
        	else if (("doGenerateCsvFromChecked".equals(actionName))) GenerateCSVFromChecked(event);
			else if (("doGenerateXmlFromChecked".equals(actionName))) GenerateXMLFromChecked(event);
        	else if(("moveToArchive".equals(actionName) || "removeDocument".equals(actionName))) globalAction(event);
            else if(("doSaveSearch".equals(actionName))) SaveSearch(event);
        }
    }
    
    private class MultiChangeAttributes implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		if(documentIds == null)
        	{
        		addActionError(sm.getString("NieWybranoDokumentow"));
        		return;
        	}
        	
        	redirectUrl = "/repository/multi-change-attributes.action";
        	
        	List<Long> ids = Arrays.asList(documentIds); /** z zaznaczonych */
        	ServletActionContext.getRequest().getSession().setAttribute("documentIds_multi", ids);
        	
        	event.skip(EV_FILL);
        	event.setResult(REDIRECT);
    	}
    }
    
    private class GeneratePDFPartially implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
        	 try {
        		 
                 DSApi.context().begin();
                 
                 if(documentIds==null) 
                 {
                     addActionError(sm.getString("NieWybranoDokumentow"));
                     omitLimit = false;
                 }
                 else{
                     Document[] docsView = new Document[documentIds.length];
                     for(int i=0;i<documentIds.length;i++)
                     {
                          docsView[i]= Document.find(documentIds[i]);
                     }
                     File tmp = null;
                     try
                     {
                         tmp = File.createTempFile("docusafe_", ".pdf");
                         OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                              
                         PdfUtils.attachmentsAsPdf(docsView, true, os);
                         os.close();
                     }
                     catch (Exception e)
                     {
                         log.error(e.getMessage(), e);
                     }
                     finally
                     {
             		 DSApi.context().setRollbackOnly();
                     }

                     if (tmp != null)
                     {
                         event.setResult(NONE);
                         event.skip(EV_FILL);
                         try
                         {
                        	 HttpServletResponse resp = ServletActionContext.getResponse();
                         	 resp.setContentType("application/pdf");
                     		 resp.setHeader("Content-disposition", "attachment; filename=\"documents.pdf\"");
                             ServletUtils.streamFile(resp, tmp, "application/pdf");
                         }
                         catch (IOException e)
                         {
                             log.error(e.getMessage(), e);
                         }
                         finally
                         {
                             tmp.delete();
                         }
                     }
                 }

                 DSApi.context().rollback();
             }
             catch(EdmException e)
             {
                 event.addActionError(e.getMessage());
             }
    	}
    }
    
    private void specialSearchForBFL()
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		String ftsquery = null;
    		if(fullTextSearch != null && fullTextSearch.length() > 0)
    			ftsquery = Expression.convertQueryToFTS(fullTextSearch);
    		DSDivision[] divisions = DSUser.findByUsername(DSApi.context().getPrincipalName()).getDivisions();    
    		StringBuilder sb = new StringBuilder(" select COUNT( distinct c.document_id) AS doccount from ("+
    				"select document_id  from dsg_bfl_komunikat a1"+(ftsquery != null ? " where CONTAINS (a1.text_data,?) " : "")+ 
    				" union all "+
    				"select document_id from dsg_bfl_legislacyjny a2"+(ftsquery != null ? " where CONTAINS (a2.text_data,?)" : "")+ 
    				 " union all "+
    				"select document_id from dsg_bfl_protokol a3"+(ftsquery != null ? " where CONTAINS (a3.text_data,?)" : "")+ 
    				" union all "+
    				"select document_id from dsg_bfl_wal_zgrom a4"+(ftsquery != null ? " where CONTAINS (a4.text_data,?)" : "")+ 
    				" union all "+
    				"select document_id from dsg_bfl_zarzadzenie a5 "+(ftsquery != null ? " where CONTAINS (a5.text_data,?)" : "")+ 
    				" union all "+
    				"select document_id from DSG_BFL_UCHWALA a6 "+(ftsquery != null ? " where CONTAINS (a6.text_data,?)" : "")+  
    				" union all "+
    				" select document_id from DSG_BFL_WN_ZARZ a7 "+(ftsquery != null ? " where CONTAINS (a7.text_data,?)" : "")+  
    				") as c , ds_document_permission p, ds_document doc where doc.DELETED =0 and doc.id = c.document_id and c.document_id = p.document_id and (");
    		
    	            for (DSDivision division : divisions)
    	            {
    	                if (!division.isGroup())
    	                    continue;                
    	                sb.append("(p.subjecttype = 'group') AND (p.subject = ?) or ");
    	            }
    	            sb.replace(sb.length()-3, sb.length(), "");
    	            sb.append(")");
    		
    	            System.out.println("Mariusz "+sb.toString());
    	    		ps = DSApi.context().prepareStatement(sb.toString());
    	    		int j =1;
    	    		if(ftsquery != null)
    	    		{
	    	    		for (int i = 1; i < 8; i++) 
	    	    			ps.setString(i, ftsquery);
	    	    		j =8;
    	    		}

    	            for (DSDivision division : divisions)
    	            {
    	                if (!division.isGroup())
    	                    continue;                
    	                ps.setString(j, division.getGuid());
    	                j++;
    	            }
    	    		rs = ps.executeQuery();
    	    		rs.next();
    	    		int totalCount = rs.getInt(1);
    	    		
    	    		ps.close();    
    	    		
    		sb = new StringBuilder("select * from (SELECT TOP "+limit+" * FROM ( select distinct top "+(offset+limit)+"   c.document_id from ("+
			"select document_id from dsg_bfl_komunikat a1"+(ftsquery != null ? " where CONTAINS (a1.text_data,?)" : "")+
			" union all "+
			"select document_id from dsg_bfl_legislacyjny a2 "+(ftsquery != null ? " where CONTAINS (a2.text_data,?)" : "")+
			" union all "+
			"select document_id from dsg_bfl_protokol a3 "+(ftsquery != null ? " where CONTAINS (a3.text_data,?)" : "")+ 
			" union all "+
			"select document_id from dsg_bfl_wal_zgrom a4  "+(ftsquery != null ? " where CONTAINS (a4.text_data,?)" : "")+
			" union all "+
			"select document_id from dsg_bfl_zarzadzenie a5  "+(ftsquery != null ? " where CONTAINS (a5.text_data,?)" : "")+ 
			" union all "+
			"select document_id from DSG_BFL_UCHWALA a6 "+(ftsquery != null ? " where CONTAINS (a6.text_data,?)" : "")+
			" union all "+
			" select document_id from DSG_BFL_WN_ZARZ a7 "+(ftsquery != null ? " where CONTAINS (a7.text_data,?)" : "")+ 
			") as c , ds_document_permission p , ds_document doc where doc.DELETED =0 and doc.id = c.document_id and c.document_id = p.document_id and (");

            for (DSDivision division : divisions)
            {
                if (!division.isGroup())
                    continue;                
                sb.append("(p.subjecttype = 'group') AND (p.subject = ?) or ");
            }
            sb.replace(sb.length()-3, sb.length(), "");
            sb.append(") order by document_id asc) as foo order by document_id desc ) as bar ORDER by document_id ASC");

    		ps = DSApi.context().prepareStatement(sb.toString());
    		j =1;
    		if(ftsquery != null)
    		{
	    		for (int i = 1; i < 8; i++) 
	    			ps.setString(i, ftsquery);
	    		j =8;
    		}
            for (DSDivision division : divisions)
            {
                if (!division.isGroup())
                    continue;                
                ps.setString(j, division.getGuid());
                j++;
            }
    		rs = ps.executeQuery();
    		List<Document> resultsBfl = new ArrayList<Document>(limit);
            while (rs.next()) {
            	 resultsBfl.add(DSApi.context().load(Document.class, rs.getLong(1)));
            }
            SearchResults<Document> searchResultsBfl = new SearchResultsAdapter<Document>(resultsBfl, offset, totalCount,Document.class);
            
            
            count = searchResultsBfl.totalCount();
            
            ids = new Long[searchResultsBfl.count()];
            resultDocumentsTable = searchResultsBfl.results();
            thisSearchUrl = getLink("doSearch", offset, limit, null, null, documentKindCn, "session", boxName,
                    accessedBy, accessedAs, accessedFrom, accessedTo, resultsInPopup,documentAspectCn,author,
                    documentDateFrom, documentDateTo,officeNumberFrom,
                    officeNumberTo,officeNumberYear, null, null);
            for (int t=searchResultsBfl.count(), i=t-1; i>=0 ;i--)
                ids[i] = resultDocumentsTable[i].getId();
            if (searchResultsBfl.count() == 0)
            {
                addActionMessage(sm.getString("NieZnalezionoDokumentow"));
            }
            else
            {
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor(){
                    public String getLink(int offset)
                    {
                        return SearchDockindDocumentsAction.getLink("doSearch", offset, limit, sortField, Boolean.valueOf(ascending),
                            documentKindCn, "session", boxName, accessedBy, accessedAs, accessedFrom, accessedTo,
                            resultsInPopup, documentAspectCn,author,documentDateFrom, documentDateTo,officeNumberFrom,
                            officeNumberTo,officeNumberYear, null, null);
                    }
                    
                    
                };
                pager = new Pager(linkVisitor, offset, limit, searchResultsBfl.totalCount(), 10);
            }
            int[] columnAvaiable = new int[0];
            results = bflAddColumns(searchResultsBfl, columnAvaiable);
    	}
    	catch (Exception e) {
			log.error(e.getMessage(), e);
		}
    }
    
    
    private List<Map<String,Object>> bflAddColumns(Iterator<Document> searchResults, int[] columnAvaiable) throws EdmException
    {
    	 
    		String tmp = "\"document_id\",\"document_title\",\"document_ctime\",\"document_author\",\"document_mtime\",\"document_remark\",\"box_name\"";  
    	    columns = getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));
    	    List<String> columnNames = getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));
    	    
    	    // warto�� tej zmiennej zostanie wype�niona na podstawie pierwszego
    	    // znalezionego dokumentu; zak�adam, �e ka�dy dokument jest tego
    	    // samego typu

    	    results = new ArrayList<Map<String,Object>>();

    	    int docCount = 0;
    	    
    	    // jesli po dodaniu wszystkich rekordow indeks jakiejs kolumy bedzie rowna 0
    	    // tzn. ze zaden rekord nie ma wypelnionego tego atrybuty a wiec ta kolumna
    	    // nie bedzie wyswietlana
    	        
    	    while (searchResults.hasNext())
    	    {
    	        Document document = searchResults.next();

    	        if (!document.canReadXXX())
    	            continue;

    	        //dla aegon , nie wyswietlamy dokumentow z kosza
    	        if(document.getFolder().getTitle().equalsIgnoreCase("Kosz")) {
    	        	continue;
    	        }

    	        docCount++;

    	        Map<String, Object> bean = new HashMap<String, Object>();   
    	        bean.put("link", "/repository/edit-document.action?id="+document.getId());
    	        bean.put("document_id", document.getId());
    	        bean.put("document_title", document.getTitle());
    	        bean.put("document_description", document.getDescription());
    	        bean.put("document_ctime", document.getCtime());
    	        bean.put("document_mtime", document.getMtime());
    	        bean.put("document_remark", document.getLastRemark());
    	        
    	        if (document.getBox() != null)
    	        	bean.put("box_name", document.getBox().getName());
    	        else
    	        	bean.put("box_name", "brak");
    	        
    	        if(columnNames.contains("document_flags"))
    	        {
    	            String flags="";
    	            for(Task.FlagBean f : LabelsManager.getReadFlagBeansForDocument(document.getId(),DSApi.context().getPrincipalName(), false))
    	            {
    	            	flags += f.getC() + " ";
    	            }
    	            bean.put("document_flags", flags);
    	        }
    	        if(columnNames.contains("document_user_flags"))
    	        {
    	            String flags="";
    	            for(Task.FlagBean f : LabelsManager.getReadFlagBeansForDocument(document.getId(),DSApi.context().getPrincipalName(), true))
    	            {
    	            	flags += f.getC() + " ";
    	            }
    	            bean.put("document_user_flags", flags);
    	        }
    	        
    	        if(columnNames.contains("current_workflow_location"))
    	        {
    	            bean.put("current_workflow_location", document.getCurrentWorkflowLocation());
    	            
    	            if (document.getType() == DocumentType.INCOMING)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/incoming/assignment-history.action?documentId="+document.getId());
    	            }
    	            else if (document.getType() == DocumentType.OUTGOING)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/outgoing/assignment-history.action?documentId="+document.getId());
    	            }
    	            else if (document.getType() == DocumentType.INTERNAL)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/internal/assignment-history.action?documentId="+document.getId());
    	            }
    	            else
    	            {
    	            	bean.put("current_workflow_location_link", null);
    	            }
    	            
    	        }
    	        
    	        if (columnNames.contains("document_author"))
    	        {
    	            bean.put("document_author", DSUser.safeToFirstnameLastname(document.getAuthor()));
    	        }

    	        bean.put("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));
    	        
    	        bean.put("canReadOrginalAttachments", Boolean.valueOf(document.canReadOrginalAttachments(null)));

    	        if (columnNames.contains("attachment_link1"))
    	        {
    	            List atts = document.listAttachments();
    	            if (atts.size() > 0)
    	            {
    	                // wy�wietlany jest najnowszy za��cznik, ale
    	                // nale�y pomin�� za��czniki specjalnego
    	                // znaczenia (cn != null)
    	                int pos = atts.size() - 1;
    	                Attachment att = ((Attachment) atts.get(pos));
    	                while (att != null && att.getCn() != null && pos > 0)
    	                {
    	                    att = ((Attachment) atts.get(--pos));
    	                }
    	                if (att != null)
    	                {
    	                    AttachmentRevision rev = att.getMostRecentRevision();
    	                    if (rev != null)
    	                    {
    	                        bean.put("attachment_link1", ViewAttachmentRevisionAction.getLink(
                                        ServletActionContext.getRequest(), rev.getId()));
    	                        if (ViewServer.mimeAcceptable(rev.getMime()))
    	                            bean.put("attachment_link_vs", "/viewserver/viewer.action?id="+rev.getId());
    	                    }
    	                }
    	            }
    	        }

    	        results.add(bean);
    	    }
    	    return results;
    }


    /**
     * Sortowanie wynik�w (results) pod warunkiem, �e jeden z OrderBy jest typu OrderPredicateProvider i metoda isManualSorted zwr�ci true
     */
    protected boolean sortResults(FormQuery.OrderBy[] orders) {
        boolean sorted = false;

        if (kind == null || results == null || ids == null || results.size() == 0 || results.size() != ids.length)
            return sorted;

        for (FormQuery.OrderBy order : orders) {
            String attr = order.getAttribute();
            if (attr.startsWith("dockind_")) {
                try {
                    String fieldCn = attr.substring("dockind_".length());
                    Field field = kind.getFieldByCn(fieldCn);

                    if (ManualSortedNonColumnField.isManualSorted(field)) {
                        final boolean asc = order.isAscending();
                        final String key = "dockind_" + field.getCn();

                        Collections.sort(results, new Comparator<Map<String, Object>>() {
                            @Override
                            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                                Object oValue1 = o1.get(key);
                                Object oValue2 = o2.get(key);

                                String value1 = (oValue1 != null && oValue1 instanceof String) ? (String) oValue1 : "";
                                String value2 = (oValue2 != null && oValue2 instanceof String) ? (String) oValue2 : "";

                                if (value1.isEmpty() && value2.isEmpty()) return 0;
                                if (value1.isEmpty()) return asc ? 1 : -1;
                                if (value2.isEmpty()) return asc ? -1 : 1;

                                int comparison = value1.compareToIgnoreCase(value2);
                                if (comparison == 0) return 0;
                                return (asc ? comparison : comparison * (-1)) / Math.abs(comparison);
                            }
                        });

                        sorted = true;
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return sorted;
    }
    protected class InvalidSqlRangeDate extends Exception{
    	
    }
    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (resultsInPopup){
                event.setResult("popup");
            } 
            canGenerateXlsReport = Configuration.isAdditionOn(Configuration.ADDITION_GENERATE_XLS_REPORT_FROM_SEARCH_DOCKIND);
            canGenerateCsvReport = Configuration.isAdditionOn(Configuration.ADDITION_GENERATE_CSV_REPORT_FROM_SEARCH_DOCKIND);
			canGenerateXmlReport = Configuration.isAdditionOn(Configuration.ADDITION_GENERATE_XML_REPORT_FROM_SEARCH_DOCKIND);
            
            if (documentId != null||(documentIdString!=null && !documentIdString.equals(""))) {
                prepareSingleDocumentResult(event);
                return;
            }

            if (documentKindCn == null)
                addActionError(sm.getString("NieWybranoTypuDokumentu"));

            if (hasActionErrors())
                return;

            /**Decyzja o realizacji na ostyatnia chwile nie mam czasu*/
            if("allDockinds".equals(documentKindCn))
            {
            	specialSearchForBFL();
            	return;
            }

            //Map params = ActionContext.getContext().getParameters();

            if (sortField == null)
                sortField = "id";

            boxName = TextUtils.trimmedStringOrNull(boxName);
            accessedBy = TextUtils.trimmedStringOrNull(accessedBy);

            limit = limit > 0 ? limit : LIMIT;

            try{
                SearchDockindDocumentHelper.instance().search(SearchDockindDocumentsAction.this, event);
            }catch(Exception e){
                log.error(e.getMessage());
            }
        }

        private void prepareSingleDocumentResult(ActionEvent event) {
            Document document;
            try {
            	if(AvailabilityManager.isAvailable("wyszukiwarka.IDString"))documentId=Long.parseLong(documentIdString);
            	if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
            		document = Document.find(documentId);
            		if(document != null && document.getCzyAktualny()){
            			event.setResult("edit-document");
            			return;
            		}else{
            			 addActionError(sm.getString("dokumentOPodanymIdJestNieaktywny", document.getId()));
            			return;
            		}

            	}else{
            		document = Document.find(documentId);
            		if(document != null)
            		{
            			event.setResult("edit-document");
            			return;
            		}
            		else
            		{
            			addActionError(sm.getString("NieZnalezionoDokumentuOidentyfikatorze")+": "+ documentId);
            			return;
            		}
            	}
            }
            catch (EdmException e)
            {
            	addActionError(sm.getString("NieZnalezionoDokumentuOidentyfikatorze")+": "+ documentId);
            	return;
            }
            catch(NumberFormatException e){
            	event.addActionError(sm.getString("wyszukiwarka.niepoprawneID"));
            	return;
            }
        }
    }

    private class PrepareArchive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (("doGenerateXlsFromChecked".equals(actionName)) || ("doGenerateAldXls".equals(actionName))) {
        		omitLimit = false;
        	} else {
        		omitLimit = true;
        	}
        }
    }

    private void SaveSearch(ActionEvent event) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);

            oos.writeObject(values);
            oos.close();
            baos.close();
            String valuesStr = Base64.encodeBase64String(baos.toByteArray());

            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);

            oos.writeObject(invalues);
            oos.close();
            baos.close();
            String invaluesStr = Base64.encodeBase64String(baos.toByteArray());

            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);

            Map<String,Object> elseMap = new HashMap<String, Object>();
            elseMap.put("accessedAs",accessedAs);
            elseMap.put("accessedBy",accessedBy);
            elseMap.put("accessedFrom",accessedFrom);
            elseMap.put("accessedTo",accessedTo);
            elseMap.put("boxName",boxName);
            elseMap.put("flagIds",flagIds);
            elseMap.put("flagsDateFrom",flagsDateFrom);
            elseMap.put("flagsDateTo",flagsDateTo);
            elseMap.put("userFlagIds",userFlagIds);
            elseMap.put("userFlagsDateFrom",userFlagsDateFrom);
            elseMap.put("userFlagsDateTo",userFlagsDateTo);
            elseMap.put("ctimeFrom",ctimeFrom);
            elseMap.put("ctimeTo",ctimeTo);
            elseMap.put("author",author);

            oos.writeObject(elseMap);

            oos.close();
            baos.close();
            String elseMapStr = Base64.encodeBase64String(baos.toByteArray());

            DSApi.context().begin();

            if(saveSearchName == null || saveSearchName.length() < 1) {
                throw new EdmException(sm.getString("NiePodanoNazwyWyszukania"));
            }

            String names = DSApi.context().userPreferences().node("search").get("s-names","");
            names = names + saveSearchName + ";";

            DSApi.context().userPreferences().node("search").put("s-names",names);
            DSApi.context().userPreferences().node("search").node(saveSearchName).put("valuesStr",valuesStr);
            DSApi.context().userPreferences().node("search").node(saveSearchName).put("invaluesStr",invaluesStr);
            DSApi.context().userPreferences().node("search").node(saveSearchName).put("elseMapStr", elseMapStr);
            DSApi.context().userPreferences().node("search").node(saveSearchName).put("documentKindCn", documentKindCn);

            DSApi.context().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context()._rollback();
            addActionError(e.getMessage());
        }

    }

    private void GenerateAldXLS(ActionEvent event)
    {
    	PreparedStatement ps = null;
    	ReportEnvironment reportEnvironment = null;
    	Report r = null;
    	Long reportId = null;
    	
    	try
    	{
	    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
	    	 
	    	oos.writeObject(values);
	    	oos.close();
	    	baos.close();
	    	String valuesStr = Base64.encodeBase64String(baos.toByteArray());
	    	
	    	baos = new ByteArrayOutputStream();
	    	oos = new ObjectOutputStream(baos);
	    	 
	    	oos.writeObject(invalues);
	    	oos.close();
	    	baos.close();
	    	String invaluesStr = Base64.encodeBase64String(baos.toByteArray());
	    	
	    	baos = new ByteArrayOutputStream();
	    	oos = new ObjectOutputStream(baos);
	    	 
	    	Map<String,Object> elseMap = new HashMap<String, Object>();
	    	elseMap.put("accessedAs",accessedAs);
	    	elseMap.put("accessedBy",accessedBy);
	    	elseMap.put("accessedFrom",accessedFrom);
	    	elseMap.put("accessedTo",accessedTo);
	    	elseMap.put("boxName",boxName);
	    	elseMap.put("flagIds",flagIds);
	    	elseMap.put("flagsDateFrom",flagsDateFrom);
	    	elseMap.put("flagsDateTo",flagsDateTo);
	    	elseMap.put("userFlagIds",userFlagIds);
	    	elseMap.put("userFlagsDateFrom",userFlagsDateFrom);
	    	elseMap.put("userFlagsDateTo",userFlagsDateTo);
	    	
	    	oos.writeObject(elseMap);
	    	
	    	oos.close();
	    	baos.close();
	    	String elseMapStr = Base64.encodeBase64String(baos.toByteArray());
	    	
	    	reportEnvironment = new ReportEnvironment();
	    	r = reportEnvironment.getReportInstanceByCn("ald-search-result-report");
			r.init(reportEnvironment.getDefaultConfigForCn("ald-search-result-report", r), "ald-search-result-report");
			
			List<ReportParameter> params = r.getParams();
	        for(ReportParameter rp: params)
	        {
	           if(rp.getFieldCn().equalsIgnoreCase("VALUES"))
	           {
	               rp.setValue(TextUtils.parseStringArray(valuesStr));
	           }
	           if(rp.getFieldCn().equalsIgnoreCase("INVALUES"))
	           {
	               rp.setValue(TextUtils.parseStringArray(invaluesStr));
	           }
	           if(rp.getFieldCn().equalsIgnoreCase("ELSEMAP"))
	           {
	               rp.setValue(TextUtils.parseStringArray(elseMapStr));
	           }
	
	        }
	        r.setParams(params);
						
			DSApi.context().begin();
        	Date d = new Date();
        	if(DSApi.isOracleServer())
        		ps = DSApi.context().prepareStatement("insert into ds_new_report (id,report_cn, ctime, username, status, criteria, title, description, process_date, remote_key) values(ds_new_report_id.nextval,?,?,?,?,?,?,?,?,?)");
        	else
        		ps = DSApi.context().prepareStatement("insert into ds_new_report (report_cn, ctime, username, status, criteria, title, description, process_date, remote_key) values(?,?,?,?,?,?,?,?,?)");
        	ps.setString(1, r.getReportCn());
        	ps.setTimestamp(2, new Timestamp(d.getTime()));
        	ps.setString(3, DSApi.context().getPrincipalName());
        	ps.setInt(4, Report.NEW_REPORT_STATUS);
        	
        	File file = File.createTempFile("docusafe_report_", ".tmp");
			OutputStream output = new BufferedOutputStream(new FileOutputStream(file));
			PrintStream pr = new PrintStream(output, true, "UTF-8");
			pr.print(r.getSearchCriteria().asXML());
			pr.close();
			output.close();
			InputStream is = new FileInputStream(file);
			ps.setBinaryStream(5, is, (int) file.length());
			
        	ps.setString(6, sm.getString("raportXLS"));
        	ps.setString(7, sm.getString("raportXLSZWynikowWyszukiwania"));
        	ps.setTimestamp(8, new Timestamp(new Date().getTime()));
        	
        	String remKey = DSApi.context().getPrincipalName() + System.currentTimeMillis();
        	Random rand = new Random(System.currentTimeMillis());
        	while(remKey.length()<30)
        	{
        		remKey += rand.nextInt(9)%10;
        	}
        	ps.setString(9, remKey);
        	
        	if(ps.executeUpdate()>0)
        		addActionMessage("ZarejestrowanoRaport");
        	
        	DSApi.context().closeStatement(ps);
        	ps = DSApi.context().prepareStatement("select id from ds_new_report where ctime=?");
        	ps.setTimestamp(1, new Timestamp(d.getTime()));
        	ResultSet rs = ps.executeQuery();
        	if(rs.next())
        	{
        		reportId = rs.getLong("id");
        	}
        	DSApi.context().commit();
    	}
    	catch (Exception e) 
    	{
    		e.printStackTrace();
    		log.error(e.getMessage(), e);
    		DSApi.context().setRollbackOnly();
			DSApi.context().closeStatement(ps);
    		addActionError(e.getMessage());
		}
    	
    	redirectUrl = "/reports/view-specified-report.action?reportId="+reportId;
    	
    	event.skip(EV_FILL);
    	event.setResult(REDIRECT);
    	
    }
    
    /**
     * Generowanie XLSa ze znalezionych dokumentow
     * @TODO - logika do wywalenia z akcji do jakiegos innego obiektu
     * @param event
     */
    private void GenerateXLSFromChecked(ActionEvent event)
    {
    		if(documentIds==null) {
     			addActionError(sm.getString("NieWybranoDokumentow"));
     			omitLimit = false;
     			return;
     		}
     		else
     		{
     			List<Document> documents = null;
     			
     			try 
     			{
     				documents = Document.find(Arrays.asList(documentIds));
     			} 
     			catch (Exception e) 
     			{
     				log.debug("find many documents", e);
				}
     			
     			List<Map<String,Object>> temp = null;
     			try 
     			{
     				temp = addColumns(documents.iterator(), null);
     			} 
     			catch (Exception e) 
     			{
     				event.addActionError("adding columns to search results error");
     			}
     			
     			if(temp!=null && temp.size()>0)
     				results=temp;
     		}

        HSSFWorkbook workbook = createXlsReport(event);

        if (workbook == null) return;
              
              try
              {
                  File  tmpfile = File.createTempFile("DocuSafe", "tmp");
                  tmpfile.deleteOnExit();
                  FileOutputStream fis = new FileOutputStream(tmpfile);
                  workbook.write(fis);
                  fis.close();
                  ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
              }
              catch(IOException ioe)
              {
                  event.addActionError(ioe.getMessage());
                  log.debug(ioe.getMessage(), ioe);
              }
              event.setResult(NONE);
         
    	
    }

    private HSSFWorkbook createXlsReport(ActionEvent event) {
        Map<String, String> columns = new HashMap<String,String>();

        try
        {
        	String tmp =  DSApi.context().systemPreferences().node("search-documents").get("columns_"+getDocumentKindCn(),DocumentKindsManager.getAllDockindColumnsString(DocumentKind.findByCn(getDocumentKindCn())));
            String cols = DSApi.context().userPreferences().node("search-documents").get("columns_"+documentKindCn, tmp);

            for(Field fd : DocumentKind.findByCn(documentKindCn).getFields())
            {
                if(cols.indexOf(fd.getCn())>0)
                {
                    columns.put("dockind_"+fd.getCn(), fd.getName());
                }
            }
        }
        catch(EdmException ee)
        {
            event.addActionError(ee.getMessage());
        }

        columns.put("document_author", sm.getString("Autor"));
        columns.put("document_title", sm.getString("Tytul"));
        //columns.put("document_description", sm.getString("Opis"));
        columns.put("document_id", "ID");
        columns.put("document_ctime", sm.getString("DataUtworzenia"));
        columns.put("document_mtime", sm.getString("DataModyfikacji"));
        columns.put("document_remark", sm.getString("OstatniaUwaga"));
        columns.put("box_name", "Pud�o Archiwalne");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Docusafe Software");

        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle = workbook.createCellStyle();
        HSSFFont hSSFFont = workbook.createFont();
        hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
        hSSFFont.setFontHeightInPoints((short) 10);
        cellStyle.setFont(hSSFFont);

        if(results.size()==0)
            return null;

        int rowNumber = 1;

        HSSFRow headerRow =  sheet.createRow(0);

        List<String> keys = new ArrayList<String>();
        keys.add("document_id");
        keys.add("document_author");
        keys.add("document_title");
        //keys.add("document_description");
        keys.add("document_ctime");
        keys.add("document_mtime");
        keys.add("document_remark");
        keys.add("box_name");

        for(String key : columns.keySet())
        {
            if(!keys.contains(key))
                keys.add(key);
        }

        short headerNumber = 0;

        HSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setBottomBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setLeftBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setRightBorderColor(HSSFColor.RED.index);

        headerStyle.setTopBorderColor(HSSFColor.RED.index);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);

        for (String key : keys)
        {

            if(key.equals("link") || key.equals("canReadAttachments") || key.equals("attachment_link1") || key.equals("attachment_link_vs"))
                continue;

             HSSFCell headercell = headerRow.createCell(headerNumber++);
             headercell.setCellStyle(headerStyle);
             String title = columns.get(key);
             title = title==null ? key : title;
             headercell.setCellValue(title);
        }

        for (Map<String, Object> values : results)
        {
            HSSFRow row =  sheet.createRow(rowNumber++);
            int cellNumber = 0;
            for (String str : keys)
            {
                if (str.equals("link") || str.equals("canReadAttachments") || str.equals("attachment_link1") || str.equals("attachment_link_vs"))
                    continue;

                HSSFCell cell = row.createCell((short)cellNumber++);

                if (values.get(str)==null) {
                    cell.setCellValue("");

                } else {
                    String possibleNumber = values.get(str).toString().replaceAll(" ", "");
                    if (NumberUtils.isNumber(possibleNumber)) {
                        cell.setCellValue(Double.parseDouble(possibleNumber));
                    } else {
                        cell.setCellValue(values.get(str).toString());
                        if(values.get(str).equals("nie") || values.get(str).equals("tak"))
                        {
                                cell.setCellValue(sm.getString(values.get(str).toString()));
                        }
                    }
                }
                    cell.setCellStyle(cellStyle);

            }
        }
        //for (int i=0;i<30;i++)
        for(int i=0; i < keys.size(); i++)
        {
            try
            {
                if(AvailabilityManager.isAvailable("repository.autoSizeColumn.xls"))
                    sheet.autoSizeColumn((short)i);
            }
            catch (Exception exc)
            {
                log.error("Blad w metodzie sheet.autoSizeColumn", exc);
            }
        }
        return workbook;
    }
    
    private void GenerateCSVFromChecked(ActionEvent event) {
		if (documentIds == null) {
			addActionError(sm.getString("NieWybranoDokumentow"));
			omitLimit = false;
			return;
		} else {
			List<Document> documents = null;

			try {
				documents = Document.find(Arrays.asList(documentIds));
			} catch (Exception e) {
				log.debug("find many documents", e);
			}

			List<Map<String, Object>> temp = null;
			try {
				temp = addColumns(documents.iterator(), null);
			} catch (Exception e) {
				event.addActionError("adding columns to search results error");
			}

			if (temp != null && temp.size() > 0)
				results = temp;
		}

		GenerateCSV(event);

	}

	private void GenerateXMLFromChecked(ActionEvent event) {

		if (documentIds == null) {
			addActionError(sm.getString("NieWybranoDokumentow"));
			omitLimit = false;
			return;
		} else {
			List<Document> documents = null;

			try {
				documents = Document.find(Arrays.asList(documentIds));
			} catch (Exception e) {
				log.debug("find many documents", e);
			}

			List<Map<String, Object>> temp = null;
			try {
				temp = addColumns(documents.iterator(), null);
			} catch (Exception e) {
				event.addActionError("adding columns to search results error");
			}

			if (temp != null && temp.size() > 0)
				results = temp;
		}

		GenerateXML(event);

		
	}

    private void GenerateXLS(ActionEvent event)
    {
        HSSFWorkbook workbook = createXlsReport(event);

         try
         {
             File  tmpfile = File.createTempFile("DocuSafe", "tmp");
             tmpfile.deleteOnExit();
             FileOutputStream fis = new FileOutputStream(tmpfile);
             workbook.write(fis);
             fis.close();
             ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
         }
         catch(IOException ioe)
         {
             event.addActionError(ioe.getMessage());
             log.debug(ioe.getMessage(), ioe);
         }
         event.setResult(NONE);
        
    }
    
	private void GenerateCSV(ActionEvent event) {

		try {
			File tmpcsvfile = File.createTempFile("DocusafeCSV", "tmp");
			CsvDumper dumper = new CsvDumper();

			Map<String, String> columns = new HashMap<String, String>();

			String tmp =  DSApi.context().systemPreferences().node("search-documents").get("columns_"+getDocumentKindCn(),DocumentKindsManager.getAllDockindColumnsString(DocumentKind.findByCn(getDocumentKindCn())));
			String cols = DSApi.context().userPreferences()
					.node("search-documents")
					.get("columns_" + documentKindCn, tmp);

			for (Field fd : DocumentKind.findByCn(documentKindCn).getFields()) {
				if (cols.indexOf(fd.getCn()) > 0) {
					columns.put("dockind_" + fd.getCn(), fd.getName());
				}
			}

			columns.put("document_author", sm.getString("Autor"));
			columns.put("document_title", sm.getString("Tytul"));
			columns.put("document_id", "ID "); // SYLK format dla Excel
			columns.put("document_ctime", sm.getString("DataUtworzenia"));
			columns.put("document_mtime", sm.getString("DataModyfikacji"));
			columns.put("document_remark", sm.getString("OstatniaUwaga"));
			columns.put("box_name", "Pud�o Archiwalne");

			List<String> keys = new ArrayList<String>();
			keys.add("document_id");
			keys.add("document_author");
			keys.add("document_title");
			keys.add("document_ctime");
			keys.add("document_mtime");
			keys.add("document_remark");
			keys.add("box_name");

			for (String key : columns.keySet()) {
				if (!keys.contains(key))
					keys.add(key);
			}

			dumper.openFile(tmpcsvfile);

			dumper.newLine();
			for (String key : keys) {

				String title = columns.get(key);
				title = title == null ? key : title;
				dumper.addText(title);

			}
			dumper.dumpLine();

			for (Map<String, Object> values : results) {

				dumper.newLine();

				for (String str : keys) {

					if (values.get(str) == null) {
						dumper.addText("");
					} else {

						dumper.addText(sm.getString(values.get(str).toString()));
					}
				}
				dumper.dumpLine();
			}

			ServletUtils
					.streamFile(ServletActionContext.getResponse(), tmpcsvfile,
							"text/csv",
							"Content-Disposition: attachment; filename=\"DocusafeReportCSV.csv\"");

		} catch (Exception e) {
			log.debug("", e);
		}

	}

	private void GenerateXML(ActionEvent event) {

		try {
			File tmpcsvfile = File.createTempFile("DocusafeCSV", "tmp");
			CsvDumper dumper = new CsvDumper();

			Map<String, String> columns = new HashMap<String, String>();

			String tmp =  DSApi.context().systemPreferences().node("search-documents").get("columns_"+getDocumentKindCn(),DocumentKindsManager.getAllDockindColumnsString(DocumentKind.findByCn(getDocumentKindCn())));
			String cols = DSApi.context().userPreferences().node("search-documents") .get("columns_" + documentKindCn, tmp);
			
			for (Field fd : DocumentKind.findByCn(documentKindCn).getFields()) {
				if (cols.indexOf(fd.getCn()) > 0) {
					columns.put("dockind_" + fd.getCn(), fd.getName());
				}
			}

			columns.put("document_author", sm.getString("Autor"));
			columns.put("document_title", sm.getString("Tytul"));
			columns.put("document_id", "ID "); // SYLK format dla Excel
			columns.put("document_ctime", sm.getString("DataUtworzenia"));
			columns.put("document_mtime", sm.getString("DataModyfikacji"));
			columns.put("document_remark", sm.getString("OstatniaUwaga"));
			columns.put("box_name", "Pud�o Archiwalne");

			List<String> keys = new ArrayList<String>();
			keys.add("document_id");
			keys.add("document_author");
			keys.add("document_title");
			keys.add("document_ctime");
			keys.add("document_mtime");
			keys.add("document_remark");
			keys.add("box_name");

			for (String key : columns.keySet()) {
				if (!keys.contains(key))
					keys.add(key);
			}

			TaskSnapshotXml tasksXml = new TaskSnapshotXml(results, keys, columns);

			JAXBContext jaxbContext = JAXBContext
					.newInstance(TaskSnapshotXml.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			File tmpXmlFile = File.createTempFile("DocusafeXml", "tmp");
			jaxbMarshaller.marshal(tasksXml, tmpXmlFile);
			ServletUtils
					.streamFile(ServletActionContext.getResponse(), tmpXmlFile,
							"text/xml",
							"Content-Disposition: attachment; filename=\"DocusafeXmlReport.xml\"");

		} catch (Exception e) {
			log.error("Blad przy generowaniu pliku XML", e);
		}

	}

    private class Archive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            ArchiveAction aa = new ArchiveAction(ids);
            aa.setDaemon(true);
            aa.start();
        }
    }
    
    /**
     * Wykonuje akcje na grupie dokument�w
     * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
     *
     */
        public void globalAction(ActionEvent event)
        {        	
        	log.info("Zaczynam wykonywanie {}", actionName);
            if (documentIds == null)
            {
            	event.addActionMessage("Brak pism do ponownego zapisania");
                return;
            }
            log.info("Liczba dokument�w do ponownego zapisania: " + String.valueOf(documentIds.length));
            int errors=0,ok=0;
            
			try {
				boolean contextOpened = false;
				try {
					contextOpened = DSApi.openContextIfNeeded();
					
//					DSApi.openAdmin();
					DSApi.context().begin();
	
					for (int i = 0, t = documentIds.length; i < t; i++) {
						boolean isOk = moveToArchive(documentIds[i]);
	
						if (isOk)
							ok++;
						else
							errors++;
					}
	
					DSApi.context().commit();
					DSApi.close();
					event.addActionMessage("Czynno�� zosta�a wykonana!");
				} finally {
					DSApi.closeContextIfNeeded(contextOpened);
				}
			} catch (Exception ee) {
				log.error(ee.getMessage(), ee);
				DSApi.context().setRollbackOnly();
				DSApi._close();
				event.addActionError(ee.getMessage());
			}  
            log.info("Wykonano MoveToArchive {} - Ilosc bled�w: {}" ,String.valueOf(ok),String.valueOf(errors));
        }
        
        /**
         * Wykonuje akcje przeniesienia do archiwum.
         * @param id
         */
        private boolean moveToArchive(Long id) throws Exception
        {
        	try
            {
                Document doc = Document.find(id);
                if (doc != null && doc.getDocumentKind() != null)
                {
                    String documentTypeName = doc.getStringType();
                    int documentType;
                    
                    if (documentTypeName.equals(DocumentType.INCOMING.getName()))
                        documentType = DocumentLogic.TYPE_IN_OFFICE;
                    
                    else if (documentTypeName.equals(DocumentType.INTERNAL.getName()))                	
                        documentType = DocumentLogic.TYPE_INTERNAL_OFFICE;
                    
                    else if (documentTypeName.equals(DocumentType.OUTGOING.getName()))
                        documentType = DocumentLogic.TYPE_OUT_OFFICE;
                    
                    else documentType = DocumentLogic.TYPE_ARCHIVE;
                    
                    if(doc instanceof OfficeDocument)
                    {
//                    	((OfficeDocument)doc).setArchived(doArchived);
                    	doc.getDocumentKind().logic().globalActionDocument(doc, documentType, actionName);
                    }
                    
                    DocumentKind.resaveDocument(doc, documentType);
                    return true;
                }
                
                return false;
            }
            catch(Exception e)
            {
                
                log.error("Nastapil wyjatek przy dokumencie o id: {}", id.toString());
                log.error(e.getMessage(), e);
                throw e;
            }
        }
    
    private class Recheck implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
            {
	        	List<Document> docs = new ArrayList<Document>();
	        	
	            for(int i=0;i<ids.length;i++)
	            {
	            	try
	            	{
	            		docs.add(Document.find(ids[i]));
	            	}
	            	catch (Exception e) {
	            		log.debug(e.getMessage());
					}
	            }
	            
	            Collections.sort(docs, new Document.DocumentComparatorByDate());
	            Map<DlContractDictionary,ArrayList<Document>> docsList = new HashMap<DlContractDictionary, ArrayList<Document>>();
	            
	            for(Document doc : docs)
	            {
	            	FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
	            	try
	            	{	
	            		fm.initialize();
	            		DlContractDictionary kej = (DlContractDictionary) ((ArrayList)fm.getValue("NUMER_UMOWY")).get(0);
	            		if(kej == null)
	            			continue;
	            		if(docsList.containsKey(kej))
	            		{
	            			docsList.get(kej).add(doc);
	            		}
	            		else
	            		{
	            			ArrayList<Document> tmpList = new ArrayList<Document>();
	            			tmpList.add(doc);
	            			docsList.put(kej, tmpList);            			
	            		}
	            	}
	            	catch (Exception e) {
	            		log.debug(e.getMessage());
					}
	            	
	            }
	            docs.clear();
	            
	            List<DlContractDictionary> o = new ArrayList<DlContractDictionary>(docsList.keySet());
	            Collections.sort(o, new DlContractDictionary.DlContractorComparatorByNumerUmowy());
	            
	            StringBuffer sb = new StringBuffer();
	            
	            for(DlContractDictionary x : o)
	            {
	            	Integer pagesInPackage = 0;
	            	sb.append("Numer umowy;"+x.getNumerUmowy()+"\n");
	            	for(Document doc : docsList.get(x))
	            	{
	            		Integer pagesInDocument = 0;
	            		sb.append(";Dokument;"+doc.getId());
	            		for(Attachment at : doc.getAttachments())
	            		{
	            			if(at.getMostRecentRevision() != null && at.getMostRecentRevision().getMime() != null && at.getMostRecentRevision().getMime().equals("image/tiff"))
	            			{
	            				try
	                			{
	                				InputStream blob = at.getMostRecentRevision().getBinaryStream();
	                    			File file = File.createTempFile("docusafe", "tiff");
	                                file.deleteOnExit();
	                                OutputStream out = new FileOutputStream(file);
	                                byte[] buf = new byte[4096];
	                                int len;
	                                while ((len = blob.read(buf)) > 0)
	                                {
	                                    out.write(buf, 0, len);
	                                }
	                                out.close();
	                                blob.close();
	                                TIFFReader tiffReader = new TIFFReader(file);
	                                pagesInDocument = pagesInDocument + tiffReader.countPages();
	                			}
	                			catch (Exception e) {
	                				log.debug(e.getMessage());
	    						}
	            			}
	            		}
	            		sb.append(";Stron w dokumencie;"+pagesInDocument+"\n");
	            		pagesInPackage = pagesInPackage + pagesInDocument;
	            	}
	            	sb.append(";Stron w paczce;"+pagesInPackage+"\n");
	            }
	            docsList.clear();
	            o.clear();
	            
	            
	            File file1 = new File(Docusafe.getHome()+File.separator+"ImpolRaport.csv");
            	
	            OutputStream os = new FileOutputStream(file1);
	            os.write(sb.toString().getBytes());
	            os.flush();
	            os.close();
	            //ServletUtils.streamFile(ServletActionContext.getResponse(), file1, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"raport.csv\"");
            }
            catch (Exception e) {
            	log.error(e.getMessage(), e);
			}
            //event.setResult(NONE); 
        }
    }
    
    public class ArchiveAction extends Thread
    {
        private Long[] ids;
        //Zarchwizuje dokument
        private boolean doArchived = false;
        
        public ArchiveAction(Long [] arg)
        {
            ids = arg;
        }
        
        public ArchiveAction(Long [] arg, boolean doArchived)
        {
            ids = arg;
            this.doArchived = doArchived;
        }
        
        @Override
        public void run() 
        {
            log.info("Zaczynam wykonywanie archiveAction.");
            if (ids == null)
            {
                log.error("Brak pism do ponownego zapisania");
                return;
            }
            log.info("Liczba dokument�w do ponownego zapisania: " + String.valueOf(ids.length));
            Long id;
            Document doc;
            int errors=0,ok=0;
            
            try
            {
                DSApi.openAdmin();
                DSApi.context().begin();
            
                for (int i=0,t=ids.length; i < t; i++)
                {
                        id = ids[i];
                        try
                        {
                            doc = Document.find(id);
                            if (doc != null && doc.getDocumentKind() != null)
                            {
                                String documentTypeName = doc.getStringType();
                                int documentType;
                                
                                if (documentTypeName.equals(DocumentType.INCOMING.getName()))
                                    documentType = DocumentLogic.TYPE_IN_OFFICE;
                                
                                else if (documentTypeName.equals(DocumentType.INTERNAL.getName()))
                                    documentType = DocumentLogic.TYPE_INTERNAL_OFFICE;
                                
                                else if (documentTypeName.equals(DocumentType.OUTGOING.getName()))
                                    documentType = DocumentLogic.TYPE_OUT_OFFICE;
                                
                                else documentType = DocumentLogic.TYPE_ARCHIVE;
                                
                                //doc.getDocumentKind().logic().archiveActions(doc, documentType);
                                DocumentKind.resaveDocument(doc, documentType);
                                ok++;
                            }
                        }
                        catch(Exception e)
                        {
                            errors++;
                            log.error("Nastapil wyjatek przy dokumencie o id: {}", id.toString());
                            log.error(e.getMessage(), e);
                        }
                }
                DSApi.context().commit();
                DSApi.close();
            }
            catch(EdmException ee)
            {
            	log.error(ee.getMessage(), ee);
                DSApi.context().setRollbackOnly();
                try
                {    
                   DSApi.close();
                }
                catch(Exception ex){}
            }   
            log.info("Wykonano " + String.valueOf(ok) + " - Ilosc bled�w:" + String.valueOf(errors) + "\n\n\n");
        }
    }

    private class AddDocumentsForCompilation implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            //sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            try
            {
                DSApi.context().begin();
    
                // TODO: zmienic na typ wybierany z listy na wynikach wyszukiwania
                //CompileAttachmentsManager.compileAndCreate(docs, NationwideLogic.TYP_DYSPOZYCJA_WYPLATY_ID);
                
                CompileAttachmentsManager.addDocumentsForCompilation(documentIds, DSApi.context().getPrincipalName());
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WybranoNoweZalacznikiDoKompilacji"));
            }
            catch (EdmException e)
            {
            	log.debug(e.getMessage(), e);
                addActionError(e.getMessage());
                try {DSApi.context().rollback(); } catch (EdmException f) {};
            }
        }
    }

    private List<String> getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(SearchDocumentsAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
            	log.debug(e.getMessage(), pe);
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }

    /**
     * Zwraca list� obiekt�w TableColumn.
     * @param columnsString
     */
    protected List<TableColumn> getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.documents.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(property, desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null));
            }
        }
        catch (ParseException e)
        {
        	log.debug(e.getMessage(), e);
            try
            {
                JSONArray array = new JSONArray(SearchDocumentsAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }


    public String getSortLink(String sortField, boolean ascending)
    {
        return thisSearchUrl +
            (thisSearchUrl.indexOf('?') > 0 ? "&" : "?") +
            "sortField="+sortField+
            "&ascending="+ascending;
    }

    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            return DateUtils.formatJsDateTime((Date) object);
        }
        else
        {
        	if(object.toString().equals("tak"))
        		return sm.getString("tak");
        	else if(object.toString().equals("nie"))
        		return sm.getString("nie");
            return object.toString();
        }
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds()
    {
        return documentKinds;
    }

    public DocumentKind getKind()
    {
        return kind;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public List getResults()
    {
        return results;
    }

    public List getColumns()
    {
        return columns;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public void setBoxName(String boxName)
    {
        this.boxName = boxName;
    }

    public void setAccessedBy(String accessedBy)
    {
        this.accessedBy = accessedBy;
    }

    public void setAccessedAs(String[] accessedAs)
    {
        this.accessedAs = accessedAs;
    }

    public List getAccessedByUsers()
    {
        return accessedByUsers;
    }

    

    public String getAttachmentsAsPdfUrl()
    {
        return attachmentsAsPdfUrl;
    }

    public String getAccessedFrom()
    {
        return accessedFrom;
    }

    public void setAccessedFrom(String accessedFrom)
    {
        this.accessedFrom = accessedFrom;
    }

    public String getAccessedTo()
    {
        return accessedTo;
    }

    public void setAccessedTo(String accessedTo)
    {
        this.accessedTo = accessedTo;
    }
    
    
    protected List<Map<String,Object>> addColumns(Iterator<Document> searchResults, int[] columnAvaiable) throws EdmException
    {
    	 
    		String tmp = DSApi.context().systemPreferences().node("search-documents").get("columns",SearchDocumentsAction.DEFAULT_COLUMNS);
    	    columns = getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));
    	    List<String> columnNames = getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));
    	    List<String> userDockindColumns = DocumentKindsManager.getUserDockindColumns(kind);

    	    if (columnAvaiable == null) {
    	    	columnAvaiable = new int[kind.getFields().size()];
    	    }
    	    
    	    // warto�� tej zmiennej zostanie wype�niona na podstawie pierwszego
    	    // znalezionego dokumentu; zak�adam, �e ka�dy dokument jest tego
    	    // samego typu

    	    results = new LinkedList<Map<String,Object>>();

    	    int docCount = 0;
    	    
    	    // jesli po dodaniu wszystkich rekordow indeks jakiejs kolumy bedzie rowna 0
    	    // tzn. ze zaden rekord nie ma wypelnionego tego atrybuty a wiec ta kolumna
    	    // nie bedzie wyswietlana
    	        
    	    while (searchResults.hasNext())
    	    {
    	    	
			Document document = searchResults.next();

			if (AvailabilityManager.isAvailable("search.withoutPermissionTable")){
			 
				if (AvailabilityManager.isAvailable("AdmDwrPermissionFactory") && !AdmDwrPermissionFactory.canReadXXX(document))
					continue;
			}
			else if(searchByPermisionClass ){
					if(!checkInPermissionClass(document))
					continue;	
			}
			 else if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")
					&& document.getDocumentKind().getCn().equals(Docusafe.getAdditionProperty("kind-package"))){

			} else if (!document.canReadXXX())
				continue;

			//dla aegon , nie wyswietlamy dokumentow z kosza
			if (document.getFolder().getTitle().equalsIgnoreCase("Kosz"))
			{
				continue;
			}
    	        
    	        
    	        docCount++;

    	        Map<String, Object> bean = new LinkedHashMap<String, Object>();
    	        bean.put("link", "/repository/edit-document.action?id="+document.getId());
    	        bean.put("document_id", document.getId());
    	        bean.put("document_title", document.getTitle());
    	        bean.put("document_description", document.getDescription());
    	        bean.put("document_ctime", document.getCtime());
    	        bean.put("document_mtime", document.getMtime());
    	        bean.put("document_remark", document.getLastRemark());
    	        
                if(document instanceof OfficeDocument)
                {
                	bean.put("officeNumber", ((OfficeDocument)document).getFormattedOfficeNumber());
                }
                
    	        if (document.getBox() != null)
    	        	bean.put("box_name", document.getBox().getName());
    	        else
    	        	bean.put("box_name", "brak");
    	        
    	        if(columnNames.contains("document_flags"))
    	        {
    	            String flags="";
    	            for(Task.FlagBean f : LabelsManager.getReadFlagBeansForDocument(document.getId(),DSApi.context().getPrincipalName(), false))
    	            {
    	            	flags += f.getC() + " ";
    	            }
    	            bean.put("document_flags", flags);
    	        }
    	        if(columnNames.contains("document_user_flags"))
    	        {
    	            String flags="";
    	            for(Task.FlagBean f : LabelsManager.getReadFlagBeansForDocument(document.getId(),DSApi.context().getPrincipalName(), true))
    	            {
    	            	flags += f.getC() + " ";
    	            }
    	            bean.put("document_user_flags", flags);
    	        }
    	        
    	        if(columnNames.contains("current_workflow_location"))
    	        {
    	            bean.put("current_workflow_location", document.getCurrentWorkflowLocation());
    	            
    	            if (document.getType() == DocumentType.INCOMING)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/incoming/assignment-history.action?documentId="+document.getId());
    	            }
    	            else if (document.getType() == DocumentType.OUTGOING)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/outgoing/assignment-history.action?documentId="+document.getId());
    	            }
    	            else if (document.getType() == DocumentType.INTERNAL)
    	            {
    	            	bean.put("current_workflow_location_link", "/office/internal/assignment-history.action?documentId="+document.getId());
    	            }
    	            else
    	            {
    	            	bean.put("current_workflow_location_link", null);
    	            }
    	            
    	        }
    	        
    	        if (columnNames.contains("document_author"))
    	        {
    	            bean.put("document_author", DSUser.safeToFirstnameLastname(document.getAuthor()));
    	        }

    	        bean.put("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));
    	        
    	        bean.put("canReadOrginalAttachments", Boolean.valueOf(document.canReadOrginalAttachments(null)));

    	        if (columnNames.contains("attachment_link1"))
    	        {
    	            List atts = document.listAttachments();
    	            if (atts.size() > 0)
    	            {
    	                // wy�wietlany jest najnowszy za��cznik, ale
    	                // nale�y pomin�� za��czniki specjalnego
    	                // znaczenia (cn != null)
    	                int pos = atts.size() - 1;
    	                Attachment att = ((Attachment) atts.get(pos));
    	                while (att != null && att.getCn() != null && pos > 0)
    	                {
    	                    att = ((Attachment) atts.get(--pos));
    	                }
    	                if (att != null)
    	                {
    	                    AttachmentRevision rev = att.getMostRecentRevision();
    	                    if (rev != null)
    	                    {
    	                        bean.put("attachment_link1", ViewAttachmentRevisionAction.getLink(
    	                            ServletActionContext.getRequest(), rev.getId()));
    	                        if (ViewServer.mimeAcceptable(rev.getMime()))
    	                            bean.put("attachment_link_vs", "/viewserver/viewer.action?id="+rev.getId());
    	                    }
    	                }
    	            }
    	        }

    	        FieldsManager fm = kind.getFieldsManager(document.getId());
    	        int i = 0;

    	        for(String colName:userDockindColumns)
    	        {
    	        	if(colName.startsWith("#complex#"))
    	        	{
    	        		String res = "";
    	        		String def = DSApi.context().systemPreferences().node("complex-columns"+documentKindCn).get(colName.substring("#complex#".length()), "");
    	        		String colsdef = def;//DSApi.context().userPreferences().node("complex-columns"+documentKindCn).get(colName.substring("#complex#".length()), def);
    	        		if(colsdef.length()>0)
    	        		{
    	        			try{
    	            			JSONArray array = new JSONArray(colsdef);
    	            			for (int j=0; j < array.length(); j++)
    	    	                {
    	            				Field f = kind.getFieldByCn(array.getString(j));
    	    	                    if(fm.getDescription(f.getCn())!=null){
    	    	                    	res+= (j==0?"":"|") + fm.getDescription(f.getCn());
    	    	                    }
    	    	                }
    	                		bean.put("dockind_"+colName, res);
    	                		columnAvaiable[i]=1;
    	        			}
    	            		catch(ParseException f)
    	            		{
    	            			columnAvaiable[i]=0;
    	            		}
    	        		}
    	        		else
    	        		{
    	        			bean.put("dockind_"+colName, res);
    	        		}
    	        	}
    	        	else
    	        	{
    	        		Field f = kind.getFieldByCn(colName);
    	        		//LoggerFactory.getLogger("kamilj").debug("-> "+document.getId()+" "+f.getCn()+" "+fm.getDescription(f.getCn()));    
    	        		if(f != null && (!(f.isHidden() || f.isSearchHidden()) || f.isSearchShow())) //&& fm.getDescription(f.getCn())!=null)
    	        		{
    	                    bean.put("dockind_"+f.getCn(), fm.getDescription(f.getCn()));
    	                    columnAvaiable[i]=1;
    	        		}
    	        	}
    	        	i++;
    	   
    	        }

    	        results.add(bean);
    	    }
    	    return results;
    }

    private boolean checkInPermissionClass(Document document)
	{
    	try
		{
			return PermissionManager.getInstance().getSearchPermisionFactory()!=null? 
					PermissionManager.getInstance().getSearchPermisionFactory().canReadXXX(document): true;
		} catch (EdmException e)
		{
			log.error("", e);
			return true;
		}
	}

	public static String getLink(String action,int offset, int limit, String sortField, Boolean ascending,
                                 String documentKindCn, String fieldValuesSource, String boxName,
                                 String accessedBy, String[] accessedAs, String accessedFrom, String accessedTo,
                                  boolean popup, String documentAspectCn, String author,String documentDateFrom,
                                  String documentDateTo, String officeNumberFrom, String officeNumberTo, Long officeNumberYear,
                                  String ctimeFrom, String ctimeTo)
    {
        StringBuilder link = new StringBuilder("/repository/search-dockind-documents.action" +
        		(action!=null?("?"+action+"=true&"):"?")+
        		"offset="+offset+
            (boxName != null ? "&boxName="+boxName : "") +
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField : "") +
            (ascending != null ? "&ascending="+ascending : "") +
            (documentKindCn != null ? "&documentKindCn="+documentKindCn : "") +
            (accessedFrom != null ? "&accessedFrom="+accessedFrom : "") +
            (accessedTo != null ? "&accessedTo="+accessedTo : "") +
            (accessedBy != null ? "&accessedBy="+accessedBy : "") +
            (author != null ? "&author="+author : "") +
            (popup ? "&resultsInPopup=true" : "")+
            (documentDateFrom !=null? "&documentDateFrom="+documentDateFrom: "" )+
            (documentDateTo !=null? "&documentDateTo="+documentDateTo: "" )+
            (officeNumberFrom !=null? "&officeNumberFrom="+officeNumberFrom: "" )+
            (officeNumberTo !=null? "&officeNumberTo="+officeNumberTo: "" )+
            (officeNumberYear !=null? "&officeNumberYear="+officeNumberYear: "" )+
            (fieldValuesSource!=null?"&fieldValuesSource="+fieldValuesSource : "") +
            (ctimeFrom!=null?"&ctimeFrom="+ctimeFrom : "") +
            (ctimeTo!=null?"&ctimeTo="+ctimeTo : ""));

        if (accessedAs != null)
        {
            for (int i=0; i < accessedAs.length; i++)
            {
                link.append("&accessedAs=");
                link.append(accessedAs[i]);
            }
        }
        
        if(documentAspectCn != null)
        {
        	link.append("&documentAspectCn=");
        	link.append(documentAspectCn);
        }
        
        return link.toString();
    }

    public void setResultsInPopup(boolean resultsInPopup)
    {
        this.resultsInPopup = resultsInPopup;
    }

    public boolean getResultsInPopup()
    {
        return resultsInPopup;
    }

    public String[] getForceSearchEq()
    {
        return forceSearchEq;
    }

    public void setForceSearchEq(String[] forceSearchEq)
    {
        this.forceSearchEq = forceSearchEq;
    }

    public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }

    public String getAddDocsForCompilationUrl()
    {
        return addDocsForCompilationUrl;
    }

    public boolean isCanCompileAttachments()
    {
        return canCompileAttachments;
    }          
    
    public String getSearchStyle()
    {
        return searchStyle;
    }

    public void setSearchStyle(String searchStyle)
    {
        this.searchStyle = searchStyle;
    }

    public void setNewSearchStyle(String newSearchStyle)
    {
        this.newSearchStyle = newSearchStyle;
    }

    public boolean isAdminAccess()
    {
        return adminAccess;
    }
    
    public boolean isOmitLimit()
    {
        return omitLimit;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
    
    public boolean isCanGenerateXlsReport()
    {
        return canGenerateXlsReport;
    }
    
    public void setCanGenerateXlsReport(boolean arg)
    {
        canGenerateXlsReport = arg;
    }
    
    public boolean isCanGenerateCsvReport() {
		return canGenerateCsvReport;
	}

	public void setCanGenerateCsvReport(boolean arg) {
		canGenerateCsvReport = arg;
	}

	public boolean isCanGenerateXmlReport() {
		return canGenerateXmlReport;
	}

	public void setCanGenerateXmlReport(boolean arg) {
		canGenerateXmlReport = arg;
	}

    
    public String getAttachmentsAsPdfUrlPartially() {
        return attachmentsAsPdfUrlPartially;
    }

	public List<CentrumKosztow> getCentra() {
		return centra;
	}

	public void setCentra(List<CentrumKosztow> centra) {
		this.centra = centra;
	}

	public Integer getCentra_sel() {
		return centra_sel;
	}

	public void setCentra_sel(Integer centra_sel) {
		this.centra_sel = centra_sel;
	}

	public String[] getOpsParts() {
		return opsParts;
	}

	public void setOpsParts(String[] opsParts) {
		this.opsParts = opsParts;
	}

	public String getSortField() {
		return sortField;
	}


	public Map<String, Object> getInvalues() {
		return invalues;
	}

	public void setInvalues(Map<String, Object> invalues) {
		this.invalues = invalues;
	}

	public String getDocumentIdsString() {
		return documentIdsString;
	}

	public void setDocumentIdsString(String documentIdsString) {
		this.documentIdsString = documentIdsString;
	}

	public void setAdminAccess(boolean adminAccess) {
		this.adminAccess = adminAccess;
	}

	public List<AccountNumber> getKonta() {
		return konta;
	}

	public void setKonta(List<AccountNumber> konta) {
		this.konta = konta;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getFieldValuesSource() {
		return fieldValuesSource;
	}

	public void setFieldValuesSource(String fieldValuesSource) {
		this.fieldValuesSource = fieldValuesSource;
	}

	public List<Flags.FlagBean> getFlags() {
		return flags;
	}

	public void setFlags(List<Flags.FlagBean> flags) {
		this.flags = flags;
	}

	public List<Flags.FlagBean> getUserFlags() {
		return userFlags;
	}

	public void setUserFlags(List<Flags.FlagBean> userFlags) {
		this.userFlags = userFlags;
	}

	public Long[] getFlagIds() {
		return flagIds;
	}

	public void setFlagIds(Long[] flagIds) {
		this.flagIds = flagIds;
	}

	public Long[] getUserFlagIds() {
		return userFlagIds;
	}

	public void setUserFlagIds(Long[] userFlagIds) {
		this.userFlagIds = userFlagIds;
	}

	public String getFlagsDateFrom() {
		return flagsDateFrom;
	}

	public void setFlagsDateFrom(String flagsDateFrom) {
		this.flagsDateFrom = flagsDateFrom;
	}

	public String getFlagsDateTo() {
		return flagsDateTo;
	}

	public void setFlagsDateTo(String flagsDateTo) {
		this.flagsDateTo = flagsDateTo;
	}

	public String getUserFlagsDateFrom() {
		return userFlagsDateFrom;
	}

	public void setUserFlagsDateFrom(String userFlagsDateFrom) {
		this.userFlagsDateFrom = userFlagsDateFrom;
	}

	public String getUserFlagsDateTo() {
		return userFlagsDateTo;
	}

	public void setUserFlagsDateTo(String userFlagsDateTo) {
		this.userFlagsDateTo = userFlagsDateTo;
	}

	public boolean isFlagsOn() {
		return flagsOn;
	}

	public void setFlagsOn(boolean flagsOn) {
		this.flagsOn = flagsOn;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}
	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}
	public String getDocumentAspectCn() {
		return documentAspectCn;
	}
	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}
	public void setOrderDocs(boolean orderDocs) {
		this.orderDocs = orderDocs;
	}
	public boolean isOrderDocs() {
		return orderDocs;
	}
	public Integer getOrderKind() {
		return orderKind;
	}
	public void setOrderKind(Integer orderKind) {
		this.orderKind = orderKind;
	}
	public Map<Integer, String> getOrderKindMap() {
		return orderKindMap;
	}
	public void setOrderKindMap(Map<Integer, String> orderKindMap) {
		this.orderKindMap = orderKindMap;
	}
	public Long getLocation() {
		return location;
	}
	public void setLocation(Long location) {
		this.location = location;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Map<Integer, String> getPriorityMap() {
		return priorityMap;
	}
	public void setPriorityMap(Map<Integer, String> priorityMap) {
		this.priorityMap = priorityMap;
	}
	public String getRemark() {
		return remark;
	}
	public Integer getDeliveryKind() {
		return deliveryKind;
	}

	public void setDeliveryKind(Integer deliveryKind) {
		this.deliveryKind = deliveryKind;
	}

	public Map<Integer, String> getDeliveryKindMap() {
		return deliveryKindMap;
	}

	public void setDeliveryKindMap(Map<Integer, String> deliveryKindMap) {
		this.deliveryKindMap = deliveryKindMap;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setCanOrder(boolean canOrder) {
		this.canOrder = canOrder;
	}

	public boolean isCanOrder() {
		return canOrder;
	}
	
	public boolean isCanMultichange()
	{
		return canMultichange;		
	}

	public Map<String, Object> getFlagiMap() {
		return flagiMap;
	}

	public void setFlagiMap(Map<String, Object> flagiMap) {
		this.flagiMap = flagiMap;
	}

	public List<LocationForIC> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationForIC> locations) {
		this.locations = locations;
	}

    public String getCtimeTo() {
        return ctimeTo;
    }

    public void setCtimeTo(String ctimeTo) {
        this.ctimeTo = ctimeTo;
    }

    public String getCtimeFrom() {
        return ctimeFrom;
    }

    public void setCtimeFrom(String ctimeFrom) {
        this.ctimeFrom = ctimeFrom;
    }

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentDateTo() {
        return documentDateTo;
    }

    public void setDocumentDateTo(String documentDateTo) {
        this.documentDateTo = documentDateTo;
    }

    public String getDocumentDateFrom() {
        return documentDateFrom;
    }

    public void setDocumentDateFrom(String documentDateFrom) {
        this.documentDateFrom = documentDateFrom;
    }

    public String getPostalRegNumber() {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber) {
        this.postalRegNumber = postalRegNumber;
    }

    public String getRecipientFirstname() {
        return this.recipientSC.getValue("firstname");
    }

    public void setRecipientFirstname(String recipientFirstame) {
        this.recipientSC.put("firstname",recipientFirstame);
    }

    public String getRecipientLastname() {
        return this.recipientSC.getValue("lastname");
    }

    public void setRecipientLastname(String recipientLastname) {
        this.recipientSC.put("lastname",recipientLastname);
    }

    public String getRecipientOrganization() {
        return this.recipientSC.getValue("organization");
    }

    public void setRecipientOrganization(String recipientOrganization) {
        this.recipientSC.put("organization",recipientOrganization);
    }

    public String getRecipientOrganizationDivision() {
        return this.recipientSC.getValue("organizationDivision");
    }

    public void setRecipientOrganizationDivision(String recipientOrganizationDivision) {
        this.recipientSC.put("organizationDivision",recipientOrganizationDivision);
    }

    public String getSenderFirstame() {
        return this.senderSC.getValue("firstname");
    }

    public void setSenderFirstame(String senderFirstame) {
        this.senderSC.put("firstname",senderFirstame);
    }

    public String getSenderLastname() {
        return this.senderSC.getValue("lastname");
    }

    public void setSenderLastname(String senderLastname) {
        this.senderSC.put("lastname",senderLastname);
    }

    public String getSenderOrganization() {
        return this.senderSC.getValue("organization");
    }

    public void setSenderOrganization(String senderOrganization) {
        this.senderSC.put("organization",senderOrganization);
    }

    public String getSenderOrganizationDivision() {
        return this.senderSC.getValue("organizationDivision");
    }

    public void setSenderOrganizationDivision(String senderOrganizationDivision) {
        this.senderSC.put("organizationDivision",senderOrganizationDivision);
    }

    public Long getJournalId() {
        return journalId;
    }

    public void setJournalId(Long journalId) {
        this.journalId = journalId;
    }

    public List<Journal> getJournals() {
        return journals;
    }

    public void setJournals(List<Journal> journals) {
        this.journals = journals;
    }
    public String getFullTextSearch() {
		return fullTextSearch;
	}

	public void setFullTextSearch(String fullTextSearch) {
		this.fullTextSearch = fullTextSearch;
	}

	public String getSenderZip()
	{
        return this.senderSC.getValue("zip");
	}

	public void setSenderZip(String senderZip)
	{
        this.senderSC.put("zip",senderZip);
	}

	public String getSenderNip()
	{
        return this.senderSC.getValue("nip");
	}

	public void setSenderNip(String senderNip)
	{
        this.senderSC.put("nip",senderNip);
	}

	public String getSenderLocation()
	{
        return this.senderSC.getValue("location");
	}

	public void setSenderLocation(String senderLocation)
	{
        this.senderSC.put("location",senderLocation);
	}

	public String getRecipientZip()
	{
        return this.recipientSC.getValue("zip");
	}

	public void setRecipientZip(String recipientZip)
	{
        this.recipientSC.put("zip",recipientZip);
	}
	
	public String getRecipientStreet()
	{
        return this.recipientSC.getValue("street");
	}

	public void setRecipientStreet(String recipientStreet)
	{
        this.recipientSC.put("street",recipientStreet);
	}

	public String getRecipientNip()
	{
        return this.recipientSC.getValue("nip");
	}

	public void setRecipientNip(String recipientNip)
	{
        this.recipientSC.put("nip",recipientNip);
	}

	public String getRecipientLocation()
	{
        return this.recipientSC.getValue("location");
	}

	public void setRecipientLocation(String recipientLocation)
	{
        this.recipientSC.put("location",recipientLocation);
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public void setOfficeNumberYear(Long officeNumberYear)
	{
		this.officeNumberYear = officeNumberYear;
	}

	public Long getOfficeNumberYear()
	{
		return officeNumberYear;
	}
	public String getOfficeNumberFrom()
	{
		return officeNumberFrom;
	}

	public void setOfficeNumberFrom(String officeNumberFrom)
	{
		this.officeNumberFrom = officeNumberFrom;
	}

	public String getOfficeNumberTo()
	{
		return officeNumberTo;
	}

	public void setOfficeNumberTo(String officeNumberTo)
	{
		this.officeNumberTo = officeNumberTo;
	}
	
	public boolean getCzyAktualny()
	{
		return czyAktualny;
	}
	
	public void setCzyAktualny(boolean czyAktualny)
	{
		this.czyAktualny = czyAktualny;
	}

    public String getViewtimeFrom() {
        return viewtimeFrom;
    }

    public void setViewtimeFrom(String viewtimeFrom) {
        this.viewtimeFrom = viewtimeFrom;
    }

    public String getViewtimeTo() {
        return viewtimeTo;
    }

    public void setViewtimeTo(String viewtimeTo) {
        this.viewtimeTo = viewtimeTo;
    }

    public String[] getViewUsers() {
        return viewUsers;
    }

    public void setViewUsers(String[] viewUsers) {
        this.viewUsers = viewUsers;
    }

    public List<DSUser> getViewedByUsers() {
        return viewedByUsers;
    }

    public void setViewedByUsers(List<DSUser> viewedByUsers) {
        this.viewedByUsers = viewedByUsers;
    }

    public boolean isNotViewed() {
        return notViewed;
    }

    public void setNotViewed(boolean notViewed) {
        this.notViewed = notViewed;
    }
	public String getDocumentReferenceId()
	{
		return documentReferenceId;
	}

	public void setDocumentReferenceId(String documentReferenceId)
	{
		this.documentReferenceId = documentReferenceId;
	}

	public String getDocumentOfficeCaseSymbol()
	{
		return documentOfficeCaseSymbol;
	}
	
	public void setDocumentOfficeCaseSymbol(String documentOfficeCaseSymbol)
	{
		this.documentOfficeCaseSymbol = documentOfficeCaseSymbol;
	}

	public String getDocumentIdString() {
		return documentIdString;
	}

	public void setDocumentIdString(String documentIdString) {
		this.documentIdString = documentIdString;
	}

    public Set<String> getSaveSearchNames() {
        return saveSearchNames;
    }

    public void setSaveSearchNames(Set<String> saveSearchNames) {
        this.saveSearchNames = saveSearchNames;
    }

    public String getSaveSearchName() {
        return saveSearchName;
    }

    public void setSaveSearchName(String saveSearchName) {
        this.saveSearchName = saveSearchName;
    }

	
	public String getAbstractDescription()
	{
		return abstractDescription;
	}

	
	public void setAbstractDescription(String abstractDescription)
	{
		this.abstractDescription = abstractDescription;
	}
}