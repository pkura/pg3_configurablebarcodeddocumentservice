package pl.compan.docusafe.web.archive.repository;


import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.parametrization.ald.ALDLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Cell;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Row;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

public class CorrectingNoteAction extends EventActionSupport
{
	  private StringManager sm =
	        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	
	private String invoiceNo;
	private String nazwaDostawcy;
	private String nipDostawcy;
	private String nazwaKlienta; 
	private String numerKsiegowania;
	private String bookCode;
	private String vinNo;
	private String dataWplyniecia;
	private Long documentId;
	private Document document;
	private DocumentKind documentKind;
	private String documentKindCn;
	private FieldsManager fm;
	private String dataWystawienia;
	private String dataSprzedazy;
	private String trescKorygowana;
	private String trescPoprawna;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddCorrectingNote").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Pdf()).
        	appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
        public FillForm()
        {
        }

        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		document = Document.find(documentId);
        		if (documentKindCn == null)
                {
                    documentKind = document.getDocumentKind() != null ? document.getDocumentKind() :
                        DocumentKind.findByCn(DocumentKind.getDefaultKind());
                    documentKindCn = documentKind.getCn();
                }
                else
                    documentKind = DocumentKind.findByCn(documentKindCn);       
                
                // pobranie warto�ci dla danego dokumentu
                fm = documentKind.getFieldsManager(document.getId());
                fm.initialize();
                fm.initializeAcceptances();
                nipDostawcy = TextUtils.toString(fm.getValue(ALDLogic.NIP_DOSTAWCY_FIELD_CN));
                invoiceNo = TextUtils.toString(fm.getValue(ALDLogic.NR_FAKTURY_FIELD_CN));
                nazwaKlienta = TextUtils.toString(fm.getValue(ALDLogic.NAZWA_KLIENTA_FIELD_CN));
                vinNo = TextUtils.toString(fm.getValue(ALDLogic.NR_VIN_FIELD_CN));
                //bookCode = fm.getValue(ALDLogic.BOOK_CODE_FIELD_CN).toString();
                //numerKsiegowania = fm.getValue(ALDLogic.NR_KSIEGOWANIA_FIELD_CN).toString();
                //bookCode = fm.getValue(ALDLogic.BOOK_CODE_FIELD_CN).toString();
                //numerKsiegowania = fm.getValue(ALDLogic.NR_KSIEGOWANIA_FIELD_CN).toString();
                
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        
        }
    }
	private class Pdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	File temp = null;

            try
            {
            	Long num = DSApi.context().systemPreferences().getLong("correctingNoteNumber", 1);
            	num++;
            	DSApi.context().systemPreferences().putLong("correctingNoteNumber", num);
            	document = Document.find(documentId);
            	if (documentKindCn == null)
                {
                    documentKind = document.getDocumentKind() != null ? document.getDocumentKind() :
                        DocumentKind.findByCn(DocumentKind.getDefaultKind());
                    documentKindCn = documentKind.getCn();
                }
                else
                    documentKind = DocumentKind.findByCn(documentKindCn);       
                
                // pobranie warto�ci dla danego dokumentu
                fm = documentKind.getFieldsManager(document.getId());
                fm.initialize();
                fm.initializeAcceptances();
                if(fm.getValue(ALDLogic.NAZWA_DOSTAWCY_FIELD_CN)!=null)
                	nazwaDostawcy = fm.getValue(ALDLogic.NAZWA_DOSTAWCY_FIELD_CN).toString();
                nipDostawcy = fm.getValue(ALDLogic.NIP_DOSTAWCY_FIELD_CN).toString();
                invoiceNo = fm.getValue(ALDLogic.NR_FAKTURY_FIELD_CN).toString();
                nazwaKlienta = fm.getValue(ALDLogic.NAZWA_KLIENTA_FIELD_CN).toString();
                vinNo = fm.getValue(ALDLogic.NR_VIN_FIELD_CN).toString();
                //bookCode = fm.getValue(ALDLogic.BOOK_CODE_FIELD_CN).toString();
                //numerKsiegowania = fm.getValue(ALDLogic.NR_KSIEGOWANIA_FIELD_CN).toString();
                
            	File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 8);
                
            	 temp = File.createTempFile("docusafe_", ".pdf");
                 com.lowagie.text.Document pdfDoc =
                     new com.lowagie.text.Document(PageSize.A4,30,60,30,30);
                 PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                 
                 
                 HeaderFooter footer =
                	 new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                             DateUtils.formatCommonDateTime(new Date())), font), new Phrase("."));
                 footer.setAlignment(Element.ALIGN_CENTER);
                 pdfDoc.setFooter(footer);
                 
                 
                 Map address = GlobalPreferences.getAddress();
					
                 address.get(GlobalPreferences.ORGANIZATION);
		         address.get(GlobalPreferences.STREET);
		         address.get(GlobalPreferences.ZIP);
		         address.get(GlobalPreferences.LOCATION);
		            
                 pdfDoc.open();
                 Cell cell;
                 Row row;
                 Rectangle rect;
                 Table table = new Table(11);
                 table.setCellsFitPage(true);
                 table.setAutoFillEmptyCells(true);
                 //table.setCellpadding(1);
                 //table.setCellspacing(2);
                 table.setSpaceInsideCell(1);
                 table.setTableFitsPage(true);
                 rect = new Rectangle(0,0);
                 
                 cell = new Cell();
                 cell.setColspan(7);
                 cell.setRowspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Miejscowo��:", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.LOCATION), font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Data:", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase(DateUtils.formatJsDate(new Date()), font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Nota koryguj�ca numer: ", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase("1111", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("Nr ID: ", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("#"+num, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.add(new Phrase("orygina�", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Wystawca noty", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Odbiorca noty", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.ORGANIZATION), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.setRowspan(3);
                 cell.add(new Phrase(nazwaDostawcy, font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.STREET), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 /*
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("-------", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 */
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.ZIP), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.LOCATION), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 /*cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);*/
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("NIP: ", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("NIP: ", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase(nipDostawcy, font));
                 cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase("Nota dotyczy faktury VAT nr:", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase(invoiceNo, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("z dnia", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase(dataWystawienia, new Font(baseFont, 6)));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("z data sprzeda�y", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase(dataSprzedazy, new Font(baseFont, 6)));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("wystawionej przez", font));
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase(nazwaDostawcy, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(5);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("NIP: "+nipDostawcy, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(5);
                 cell.setBorder(0);
                 table.addCell(cell);
                 

                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Tre�� korygowana", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(9);
                 cell.add(new Phrase(trescKorygowana, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Tre�� poprawna", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(9);
                 cell.add(new Phrase(trescPoprawna, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 pdfDoc.add(table);
                 pdfDoc.newPage();
                 
                 table = new Table(11);
                 table.setCellsFitPage(true);
                 table.setAutoFillEmptyCells(true);
                 //table.setCellpadding(1);
                 //table.setCellspacing(2);
                 table.setSpaceInsideCell(1);
                 table.setTableFitsPage(true);
                 rect = new Rectangle(0,0);
                 
                 cell = new Cell();
                 cell.setColspan(7);
                 cell.setRowspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Miejscowo��:", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.LOCATION), font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Data:", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase(DateUtils.formatJsDate(new Date()), font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Nota koryguj�ca numer: ", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase("1111", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("Nr ID: ", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("#"+num, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.add(new Phrase("kopia", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Wystawca noty", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("Odbiorca noty", font));
                 cell.setBackgroundColor(Color.LIGHT_GRAY);
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.ORGANIZATION), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.setRowspan(3);
                 cell.add(new Phrase(nazwaDostawcy, font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.STREET), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 /*
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("-------", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 */
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.ZIP), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase((String)address.get(GlobalPreferences.LOCATION), font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 /*cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                 table.addCell(cell);*/
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("NIP: ", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("-----", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("NIP: ", font));
                 cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase(nipDostawcy, font));
                 cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase("Nota dotyczy faktury VAT nr:", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(3);
                 cell.add(new Phrase(invoiceNo, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase("z dnia", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase(dataWystawienia, new Font(baseFont, 6)));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("z data sprzeda�y", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.add(new Phrase(dataSprzedazy, new Font(baseFont, 6)));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("wystawionej przez", font));
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase(nazwaDostawcy, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(5);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.setBorder(0);
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(4);
                 cell.add(new Phrase("NIP: "+nipDostawcy, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(5);
                 cell.setBorder(0);
                 table.addCell(cell);
                 

                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Tre�� korygowana", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(9);
                 cell.add(new Phrase(trescKorygowana, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(2);
                 cell.add(new Phrase("Tre�� poprawna", font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(9);
                 cell.add(new Phrase(trescPoprawna, font));
                 table.addCell(cell);
                 
                 cell = new Cell();
                 cell.setColspan(11);
                 cell.setBorder(0);
                 cell.add(new Phrase(" ",font));
                 table.addCell(cell);
                 
                 pdfDoc.add(table);
                 pdfDoc.close();
                 
                 DSApi.context().begin();
                 Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("NotaKorygujaca", 254));
                 document.createAttachment(attachment);
                 if(document instanceof InOfficeDocument){
                 	((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
                 }
                 
                 attachment.createRevision(temp).setOriginalFilename("NotaKorygujaca");
                 DSApi.context().commit();
                 addActionMessage(sm.getString("PomyslnieUtworzonoNote"));
                 event.setResult("edit-dockind-document");
            }
            catch (EdmException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            catch (DocumentException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            catch (IOException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            
        }
    }

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getNazwaDostawcy() {
		return nazwaDostawcy;
	}

	public void setNazwaDostawcy(String nazwaDostawcy) {
		this.nazwaDostawcy = nazwaDostawcy;
	}

	public String getNipDostawcy() {
		return nipDostawcy;
	}

	public void setNipDostawcy(String nipDostawcy) {
		this.nipDostawcy = nipDostawcy;
	}

	public String getNazwaKlienta() {
		return nazwaKlienta;
	}

	public void setNazwaKlienta(String nazwaKlienta) {
		this.nazwaKlienta = nazwaKlienta;
	}

	public String getNumerKsiegowania() {
		return numerKsiegowania;
	}

	public void setNumerKsiegowania(String numerKsiegowania) {
		this.numerKsiegowania = numerKsiegowania;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getVinNo() {
		return vinNo;
	}

	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}

	public String getDataWplyniecia() {
		return dataWplyniecia;
	}

	public void setDataWplyniecia(String dataWplyniecia) {
		this.dataWplyniecia = dataWplyniecia;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long id) {
		this.documentId = id;
	}

	public String getDataWystawienia() {
		return dataWystawienia;
	}

	public void setDataWystawienia(String dataWystawienia) {
		this.dataWystawienia = dataWystawienia;
	}

	public String getDataSprzedazy() {
		return dataSprzedazy;
	}

	public void setDataSprzedazy(String dataSprzedazy) {
		this.dataSprzedazy = dataSprzedazy;
	}

	public String getTrescKorygowana() {
		return trescKorygowana;
	}

	public void setTrescKorygowana(String trescKorygowana) {
		this.trescKorygowana = trescKorygowana;
	}

	public String getTrescPoprawna() {
		return trescPoprawna;
	}

	public void setTrescPoprawna(String trescPoprawna) {
		this.trescPoprawna = trescPoprawna;
	}

}
