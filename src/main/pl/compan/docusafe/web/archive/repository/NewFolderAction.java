package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * Tworzenie nowego folderu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewFolderAction.java,v 1.7 2006/02/20 15:42:36 lk Exp $
 */
public class NewFolderAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(NewFolderAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doCreate", "doCancelled"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    public static final String FORWARD = "repository/new-folder";

    private static StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        Long folderId = (Long) form.get("folderId");

        request.setAttribute("returnLink",
            ExploreDocumentsAction.getLink(request, folderId));

        if (folderId != null)
        {
            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(request));
                Folder folder = Folder.find(folderId);
                request.setAttribute("folder", folder);
                request.setAttribute("folderPrettyPath", folder.getPrettyPath());
            }
            catch (EdmException e)
            {
                log.error("", e);
                errors.add(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
            finally
            {
                DSApi._close();
            }
        }

        return mapping.findForward("main");
    }

    /**
     * Rezygnacja z tworzenia dokumentu.
     */
    public ActionForward doCancelled(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        Long folderId = (Long) form.get("folderId");
        return new ActionForward(ExploreDocumentsAction.getLink(request, folderId), true);
    }

    public ActionForward doCreate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String title = (String) form.get("title");

        // sprawdzenie obecno�ci wymaganych atrybut�w
        if (StringUtils.isEmpty(title))
            errors.add(sm.getString("folder.missingTitle"));

        if (errors.size() > 0)
            return mapping.findForward("main");

        Long folderId = (Long) form.get("folderId");
        boolean success = false;

        request.setAttribute("returnLink",
            ExploreDocumentsAction.getLink(request, folderId));

        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(request));

            ctx.begin();

            //SecurityHelper sh = ctx.getSecurityHelper();

            Folder folder = null;

            if (folderId != null)
            {
                folder = Folder.find(folderId);
            }
            else
            {
                folder = Folder.getRootFolder();
            }

            request.setAttribute("folder", folder);
            request.setAttribute("folderPrettyPath", folder.getPrettyPath());

            Folder newFolder = folder.createSubfolder(title);

            ctx.commit();
            success = true;
        }
        catch (EdmException e)
        {
            log.error("", e);
            errors.add(e.getMessage());
            if (ctx != null) DSApi.context().setRollbackOnly();
        }
        finally
        {
            DSApi._close();
        }

        // je�eli utworzenie dokumentu si� powiod�o, przej�cie do
        // przegl�dania dokument�w (w tym samym folderze, w kt�rym
        // utworzono nowy folder)
        if (success)
        {
            return new ActionForward(ExploreDocumentsAction.getLink(request, folderId), true);
        }
        else
        {
            return mapping.findForward("main");
        }
    }

    public String[] getEventNames()
    {
        return eventNames;
    }

    public static String getLink(HttpServletRequest request, Long folderId)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+"?folderId="+folderId;
    }
}
