package pl.compan.docusafe.web.archive.repository.search;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.document.DocumentFinder;
import pl.compan.docusafe.core.base.document.DocumentFinderProvider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.*;

public class SearchDocumentsProcessor
{
    private static final Logger log = LoggerFactory.getLogger(SearchDocumentsProcessor.class);

    public static void process(final SearchDocumentsRequest searchRequest, ActionEvent event, SearchDocumentsAction context){
    	SearchDocumentsProcessor.process(searchRequest, event, context, false);
    }
    
    public static void process(final SearchDocumentsRequest searchRequest, ActionEvent event, SearchDocumentsAction context, boolean searchByDatabase)
    {
        // link bez sortField i ascending
        context.setThisSearchUrl(context.getLink(searchRequest.offset, searchRequest.limit, null, null, searchRequest.title, searchRequest.description, searchRequest.startCtime, searchRequest.endCtime,
                searchRequest.startMtime, searchRequest.endMtime, searchRequest.author, searchRequest.documentKindIds, searchRequest.boxId, searchRequest.accessedBy, searchRequest.accessedAs,searchRequest.lastRemark, searchRequest.documentBarcode));

        final String title = TextUtils.trimmedStringOrNull(searchRequest.title);
        final String description = TextUtils.trimmedStringOrNull(searchRequest.description);
        String attribute = TextUtils.trimmedStringOrNull(searchRequest.attribute);
        final String author = TextUtils.trimmedStringOrNull(searchRequest.author);
        String attachmentBarcode = TextUtils.trimmedStringOrNull(searchRequest.attachmentBarcode);
        final String startCtime = TextUtils.trimmedStringOrNull(searchRequest.startCtime);
        final String endCtime = TextUtils.trimmedStringOrNull(searchRequest.endCtime);
        final String startMtime = TextUtils.trimmedStringOrNull(searchRequest.startMtime);
        final String endMtime = TextUtils.trimmedStringOrNull(searchRequest.endMtime);
        final String lastRemark = TextUtils.trimmedStringOrNull(searchRequest.lastRemark);
        final String accessedBy = TextUtils.trimmedStringOrNull(searchRequest.accessedBy);
        final String documentBarcode = TextUtils.trimmedStringOrNull(searchRequest.documentBarcode);

        final int limit = searchRequest.limit > 0 ? searchRequest.limit : context.LIMIT;

        Date ctime1 = null, ctime2 = null, mtime1 = null, mtime2 = null;
        synchronized (context.dateFormat)
        {
            try { if (startCtime != null) ctime1 = context.dateFormat.parse(startCtime); } catch (Exception e) { }
            try { if (endCtime != null) ctime2 = DateUtils.endOfDay(context.dateFormat.parse(endCtime)); } catch (Exception e) { }
            try { if (startMtime != null) mtime1 = context.dateFormat.parse(startMtime); } catch (Exception e) { }
            try { if (endMtime != null) mtime2 = DateUtils.endOfDay(context.dateFormat.parse(endMtime)); } catch (Exception e) { }
        }

        ctime1 = DateUtils.nullSafeMidnight(ctime1, 0);
        ctime2 = DateUtils.nullSafeMidnight(ctime2, 1);
        mtime1 = DateUtils.nullSafeMidnight(mtime1, 0);
        mtime2 = DateUtils.nullSafeMidnight(mtime2, 1);

        QueryForm queryForm = new QueryForm(searchRequest.offset, limit);

        queryForm.setEncoding(searchRequest.encoding);

        if (title != null)
            queryForm.addProperty("title", title);
        if (description != null)
            queryForm.addProperty("description", description);
        if (attribute != null)
            queryForm.addProperty("attribute", attribute);
        if (ctime1 != null)
            queryForm.addProperty("startCtime", ctime1);
        if (ctime2 != null)
            queryForm.addProperty("endCtime", ctime2);
        //log.trace("--> mkulbaka endCtime = "+ ctime2.toString());//mkulbaka TODO debug

        if (mtime1 != null)
            queryForm.addProperty("startMtime", mtime1);
        if (mtime2 != null)
            queryForm.addProperty("endMtime", mtime2);
        if (author != null)
            queryForm.addProperty("author", author);
        if (attachmentBarcode != null)
            queryForm.addProperty("barcode", attachmentBarcode);
        if (searchRequest.doctypeIds != null && searchRequest.doctypeIds.length > 0)
            queryForm.addProperty("doctypeIds", searchRequest.doctypeIds);
        if (searchRequest.documentKindIds != null && searchRequest.documentKindIds.length > 0)
            queryForm.addProperty("documentKindIds", searchRequest.documentKindIds);
        if (searchRequest.boxId != null)
            queryForm.addProperty("boxId", searchRequest.boxId);
        if (accessedBy != null)
            queryForm.addProperty("accessedBy", accessedBy);
        if (searchRequest.accessedAs != null && searchRequest.accessedAs.length > 0)
            queryForm.addProperty("accessedAs", searchRequest.accessedAs);

        if(lastRemark!=null){
            queryForm.addProperty("lastRemark", lastRemark);
        }
        if (documentBarcode != null){
            queryForm.addProperty("barcode", documentBarcode);
        }

        DocumentHelper dh = DSApi.context().getDocumentHelper();
        if (searchRequest.sortField != null)
        {
            if (searchRequest.ascending) queryForm.addOrderAsc(searchRequest.sortField);
            else queryForm.addOrderDesc(searchRequest.sortField);
        }
        else
        {
            queryForm.addOrderAsc("title");
        }

        try
        {
            if (searchRequest.documentId != null || (searchRequest.documentIdString!=null&&!searchRequest.documentIdString.equals("")))
            {
                if(AvailabilityManager.isAvailable("wyszukiwarka.IDString"))searchRequest.documentId=Long.parseLong(searchRequest.documentIdString);
                try
                {
                    if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
                        Document document = Document.find(searchRequest.documentId);
                        if(document.getCzyAktualny()){
                            event.setResult("edit-document");
                            return;
                        }else{
                            throw new EdmException(context.sm.getString("dokumentOPodanymIdJestNieaktywny", document.getId()));
                        }

                    }else{
                        Document document = Document.find(searchRequest.documentId);
                    		/*if (document.getDocumentKind() != null)
                            event.setResult("edit-dockind-document");
                        else*/
                        event.setResult("edit-document");
                        return;
                    }
                }
                catch (DocumentLockedException e)
                {
                }
            }

            context.columns = context.getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", context.DEFAULT_COLUMNS));
            List columnNames = context.getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", context.DEFAULT_COLUMNS));

            DocumentFinder finder;
            SearchResults searchResults;
            try {
            	finder = DocumentFinderProvider.instance().provide(queryForm, searchByDatabase);
                searchResults = finder.find();
            } catch (Exception e) {
                throw new EdmException("Nieznany b��d", e);
            }

            List<Map<String,Object>> results = null;
            if (searchResults.totalCount() == 0)
            {
                //langG.getString("NieZnalezionoDokumentow")
                event.addActionMessage(context.sm.getString("NieZnalezionoDokumentow"));
            }
            else
            {
                results = new ArrayList<Map<String, Object>>(searchResults.totalCount());

                while (searchResults.hasNext())
                {
                    Document document = (Document) searchResults.next();
                    if (document instanceof OfficeDocument) {
                        OfficeDocument officeDocument = (OfficeDocument) document;
                        Map<String, Object> bean = new HashMap<String, Object>();
                            /*if (document.getDocumentKind() != null)
                                bean.put("link", "/repository/edit-dockind-document.action?id="+document.getId());
                            else*/
                        bean.put("link", "/repository/edit-document.action?id="+officeDocument.getId());
                        bean.put("document_id", officeDocument.getId());
                        bean.put("document_title", officeDocument.getTitle());
                        bean.put("officeNumber", officeDocument.getOfficeNumber());
                        bean.put("document_description", officeDocument.getDescription());
                        bean.put("document_ctime", officeDocument.getCtime());

                        bean.put("document_mtime", officeDocument.getMtime());

                        if (columnNames.contains("document_author"))
                        {
                            bean.put("document_author", DSUser.safeToFirstnameLastname(officeDocument.getAuthor()));
                        }
                        bean.put("document_remark",document.getLastRemark());
                        bean.put("canReadAttachments", Boolean.valueOf(officeDocument.canReadAttachments()));

                        if (columnNames.contains("attachment_link1") &&
                                officeDocument.canReadAttachments())
                        {
                            List atts = officeDocument.listAttachments();
                            if (atts.size() > 0)
                            {
                                AttachmentRevision rev = ((Attachment) atts.get(atts.size()-1)).getMostRecentRevision();
                                if (rev != null)
                                {
                                    bean.put("attachment_link1", ViewAttachmentRevisionAction.getLink(
                                            ServletActionContext.getRequest(), rev.getId()));
                                    bean.put("attachment_rev1", rev.getRevision());
                                }
                            }
                        }

                        if (columnNames.contains("officeNumber"))
                            bean.put("officeNumber",  officeDocument.getOfficeNumber());

                        results.add(bean);
                    }
                }

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return SearchDocumentsAction.getLink(offset, limit, searchRequest.sortField,
                                Boolean.valueOf(searchRequest.ascending), title, description, startCtime, endCtime,
                                startMtime, endMtime, author, searchRequest.documentKindIds, searchRequest.boxId,
                                accessedBy, searchRequest.accessedAs, lastRemark, documentBarcode);
                    }
                };
                context.pager = new Pager(linkVisitor, searchRequest.offset, limit, searchResults.totalCount(), 10);
            }

            context.results = results;
        }
        catch (DocumentNotFoundException e)
        {
            //event.addActionMessage(langG.getString("NieZnalezionoDokumentow"));
            log.warn("[process] Documents not found");
            event.addActionMessage(context.sm.getString("NieZnalezionoDokumentow"));
        }
        catch (EdmException e)
        {
            log.error("[process] error", e);
            event.addActionError(e.getMessage());
        }
        catch (NumberFormatException e)
        {
            log.error("[process] number format error", e);
            event.addActionError(context.sm.getString("wyszukiwarka.niepoprawneID"));
            event.skip(context.EV_SEARCH);
        }
    }
}
