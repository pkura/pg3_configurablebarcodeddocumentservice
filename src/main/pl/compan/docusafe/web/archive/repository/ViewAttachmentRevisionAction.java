package pl.compan.docusafe.web.archive.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.DocumentException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.client.HttpServerErrorException;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.AttachmentRevisionNotFoundException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.integration.DocumentConverter;
import pl.compan.docusafe.nationwide.LayeredPdf;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

/**
 * Klasa sprawdza uprawnienia do za��cznika i wypisuje do strumienia,
 * lub przekierowuje u�ytkownika do tre�ci za��cznika,
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewAttachmentRevisionAction.java,v 1.28 2009/09/18 10:22:11 kubaw Exp $
 */
public class ViewAttachmentRevisionAction extends EventDrivenAction {
    private static final Logger log = LoggerFactory.getLogger(ViewAttachmentRevisionAction.class);
    private static final String[] eventNames = new String[] { "doDefault" };
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(ViewAttachmentRevisionAction.class.getPackage().getName(),null);

    public static final String FORWARD = "repository/view-attachment-revision";

    public String[] getEventNames() {
        return eventNames;
    }

    public static boolean createResponse(Long id, Long binderId, Long attachmentId, String targetExt, HttpServletRequest request, HttpServletResponse response, Messages errors) {
        if (id == null && attachmentId == null)
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            if (log.isDebugEnabled()) {
                log.debug("id=null, status 404");
            }

        }

        InputStream content = null;
        String mime = null;
        String originalFilename = null;

        boolean finish = false;

        try {
            final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
            ctx.begin();

            AttachmentRevision revision = getAttachmentRevisionFromParameters(id, attachmentId);

            if (insufficientPermissions(binderId, revision)) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return false;
            }

            mime = revision.getMime();
            originalFilename = revision.getOriginalFilename();
            if(targetExt != null) {

                if(AvailabilityManager.isAvailable("viewAttachmentAsPdf.originalFilename")) {
                    originalFilename = FilenameUtils.removeExtension(revision.getOriginalFilename())+"."+targetExt;
                } else {
                    originalFilename = revision.getId().toString()+"."+targetExt;
                }

                if(AvailabilityManager.isAvailable("app.documentconverter")) {
                    if(AdditionManager.getPropertyAsList("app.documentconverter.extensions").indexOf(targetExt) == -1) {
                        throw new EdmException("Nie wspierany format konwersji: "+targetExt);
                    }

                    mime = DocumentConverter.getMimeType(targetExt);
                    content = revision.getAttachmentAs(targetExt);
                } else if(targetExt.equals("pdf")) {
                    content = prepareAttachmentContentAsPdf(revision);
                } else {
                    log.error("Cannot convert revision, id = {}, targetExt = {}", revision.getId(), targetExt);
                    throw new EdmException("Nie mo�na skonwertowa� za��cznika");
                }

            } else if (revision instanceof JackrabbitAttachmentRevision) {
                response.sendRedirect(((JackrabbitAttachmentRevision) revision).getDownloadLink());
            } else {
                content = revision.getAttachmentStream();
            }

            if(AvailabilityManager.isAvailable("addDownloadAttachmentHistory")) {
                Document doc = revision.getAttachment().getDocument();
                DataMartManager.addHistoryEntry(doc.getId(), Audit.createWithPriority("downloadAttachment", DSApi.context().getPrincipalName()
                        , sm.getString("UzytkownikPobralZalacznik"), ""+revision.getId(), Audit.LOWEST_PRIORITY));
            }
            if(AvailabilityManager.isAvailable("ukw.ogolne.changeStatus.attachment")) {
                Long docID = revision.getAttachment().getDocument().getId();
                DSUser user = DSApi.context().getDSUser();
                OfficeDocument document = (OfficeDocument) OfficeDocument.find(docID, false);
                if ("ogolne".equalsIgnoreCase(document.getDocumentKind().getCn())) {
                    List<Long> mpkIDS = (List<Long>) document.getFieldsManager().getKey("MPK");
                    for (Long mpkID : mpkIDS) {
                        CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkID);
                        if (mpk != null && mpk.getCentrumId() == user.getId().intValue()) {
                            mpk.setConnectedTypeId(3);
                        }
                    }
                }
            }
            ctx.commit();
        } catch (AccessDeniedException e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            finish = true;
        } catch (AttachmentRevisionNotFoundException e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
            try {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
            } catch (IOException e1) {
                log.error(e1.getMessage(), e1);
            }
            return false;
        } catch(HttpServerErrorException ex) {
            log.error(ex.getMessage(), ex);
            DSApi.context().setRollbackOnly();
            attachmentErrorRedirect(request, response, ex.getResponseBodyAsString());
            if(errors != null) {
                errors.add(sm.getString("editAttachment.updateException", ex.getStatusText()));
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
            attachmentErrorRedirect(request, response);

            if(errors != null) {
                errors.add(sm.getString("editAttachment.updateException", e.getMessage()));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
            attachmentErrorRedirect(request, response);
            if(errors != null) {
                errors.add(sm.getString("editAttachment.updateException", e.getMessage()));
            }
        } finally {
            DSApi._close();
        }
        if(content != null && !finish){
            returnContentToResponse(response, content, mime, originalFilename);
        }
        return true;
    }

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        Long id = (Long) form.get("id");
        Long binderId = (Long) form.get("binderId");
        Long attachmentId = (Long) form.get("attachmentId");
        boolean asPdf = "true".equals((String) form.get("asPdf"));

        String targetExt = asPdf ? "pdf" : (String)form.get("targetExt");
        if(StringUtils.isBlank(targetExt)) {
            targetExt = null;
        }

        boolean found = createResponse(id, binderId, attachmentId, targetExt, request, response, errors);
        if(! found) {
            return mapping.findForward("not-found");
        } else {
            return null;
        }
    }

    /**
     * @param revision
     * @return
     * @throws EdmException
     * @throws IOException
     * @throws DocumentException
     * @deprecated use {@link pl.compan.docusafe.integration.DocumentConverter} instead.
     */
    private static InputStream prepareAttachmentContentAsPdf(AttachmentRevision revision) throws EdmException, IOException, DocumentException {
        List<InputStream> files = new ArrayList<InputStream>();
        files.add(revision.getImageAsPDF());

        File file = File.createTempFile("attachment_as_pdf", ".pdf");
        OutputStream os =  new FileOutputStream(file);
        String layer = null;
        if(AvailabilityManager.isAvailable("viewAttaAsPdf.addInformationAboutUser"))
        {
            layer = sm.getString("ZalacznikPobranyPrzeUzytkownikaDnia",
                    DSApi.context().getDSUser().asFirstnameLastname(), DateUtils.formatCommonDateTime(new Date()));
        }
        LayeredPdf.streamsAsPdf(files, null, null, false, os, null, null, null, layer, revision.getAttachment().getDocument());
        return new FileInputStream(file);
    }

    private static void returnContentToResponse(HttpServletResponse response, InputStream content, String mime, String originalFilename) {
        OutputStream output = null;
        try {
            response.setContentType(mime);
            response.setHeader("Content-Disposition", "attachment; filename=\""+
                    HttpUtils.contentDispositionFilename(originalFilename)+"\"");
            output = response.getOutputStream();
            IOUtils.copy(content, output);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(content);
        }
    }

    private static void attachmentErrorRedirect(HttpServletRequest request, HttpServletResponse response, String message) {
        try {
            response.sendRedirect(request.getContextPath()+"/attachment-error.jsp" + (message == null ? "" : "?message="+ URLEncoder.encode(message, "ISO-8859-2")));
        } catch (IOException e1) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    private static void attachmentErrorRedirect(HttpServletRequest request, HttpServletResponse response) {
        attachmentErrorRedirect(request, response, null);
    }

    private static boolean insufficientPermissions(Long binderId, AttachmentRevision revision) throws EdmException {
        return revision.getAttachment().isDocumentType() && !revision.getAttachment().getDocument().canReadAttachments(binderId);
    }

    private static AttachmentRevision getAttachmentRevisionFromParameters(Long id, Long attachmentId) throws EdmException {
        AttachmentRevision revision = null;
        if(attachmentId != null) {
            Attachment attachment = Attachment.find(attachmentId);
            revision = attachment.getMostRecentRevision();
        }
        if(revision == null) {
            revision = AttachmentRevision.find(id);
        }
        return revision;
    }
    private static AttachmentRevision getAttachmentRevisionFromParameters(Long attachmentId) throws EdmException {
        AttachmentRevision revision = null;
        if(attachmentId != null) {
            Attachment attachment = Attachment.find(attachmentId);
            revision = attachment.getMostRecentRevision();
        } else {
            throw new EdmException("Nie znaleziono za��cznika o podanym id");
        }
        return revision;
    }

    /**
     * @param attachmentId
     * @return za��cznik pdf w base64
     * @throws EdmException
     * @throws DocumentException
     * @throws IOException
     */
    public static String convertAttachmentToPdfBase64(Long attachmentId) throws EdmException, DocumentException, IOException {
        AttachmentRevision revision = getAttachmentRevisionFromParameters(attachmentId);
        InputStream content = prepareAttachmentContentAsPdf(revision);
        File tempFile = File.createTempFile(revision.getFileName(), ".tmp");
        try {
            FileOutputStream out = new FileOutputStream(tempFile);
            IOUtils.copy(content, out);

        } catch (Exception e) {
            log.error("", e);
        }
        return Base64.encodeBase64String(FileUtils.readFileToByteArray(tempFile));
    }


    public static String getLink(HttpServletRequest request, Long id) {
        return "/repository/view-attachment-revision.do?id="+id;
    }
}
