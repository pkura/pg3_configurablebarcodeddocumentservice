package pl.compan.docusafe.web.archive.repository.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.archive.DocumentOrderManager;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ApplicationSettingsAction.java,v 1.58 2010/07/14 13:35:15 mariuszk Exp $
 */
public class SearchBoxAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(SearchBoxAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(SearchBoxAction.class.getPackage().getName(),null);
	
	private List<Box> boxes;
	private Long boxId;
	private Integer priorityKind;
	private Map<Integer,String> priorityMap;
	private  String description;
	private List<BoxBean> boxBeans = new ArrayList<BoxBean>();
	private static DocumentOrderManager orderManager = new DocumentOrderManager();
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReturn").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Return()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doOrder").
            append(OpenHibernateSession.INSTANCE).
            append(new Order()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class Return implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
            try
            {
            	DSApi.context().begin();
            	if(!DSApi.context().hasPermission(DSPermission.ZAMAWIANIE_ZARZADZANIE))
            		throw new EdmException("Brak uprawnie�");

            	DSApi.context().commit();
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());      
            }
        }
    }

    private class Order implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
            try
            {
            	Box box = Box.find(boxId);

	           	String mailto = Docusafe.getAdditionProperty("eservice.order.email");
	           	Map<String, Object> context = new HashMap<String, Object>();
	           	context.put("BARCODE", box.getName());
	           	context.put("orderKind", orderManager.getOrderPrioritets().get(priorityKind));
	           	context.put("orderer", DSApi.context().getDSUser().asFirstnameLastname());
	           	for (String key : context.keySet()) 
	           	{
	           	//	System.out.println(key + " - "+ context.get(key));
				}
	            ((Mailer) ServiceManager.getService(Mailer.NAME)).
	            send(mailto, null, null, Configuration.getMail(Mail.ORDER_BOX), context);
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());      
            }
        }
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	//DocumentOrderManager orderManager =  new DocumentOrderManager();
            	priorityMap = orderManager.getOrderPrioritets();
            	System.out.println(priorityMap);
            	if(description != null)
            		boxes = Box.findByDescription(description);
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
    }
    
    private class BoxBean
    {
    	private Box box;
    	private String status;
    	private String user;
    	private String orderDate;
    	
    	public BoxBean(Box box,DocumentOrderManager orderManager) throws  EdmException {
			super();
			this.box = box;
		}

		public BoxBean()
    	{
    		
    	}

		public BoxBean(Box box2)
		{
			this.box = box2;
		}

		public Box getBox() {
			return box;
		}

		public void setBox(Box box) {
			this.box = box;
		}

		public String getOrderDate() {
			return orderDate;
		}

		public void setOrderDate(String orderDate) {
			this.orderDate = orderDate;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}
    }

	public List<Box> getBoxes() {
		return boxes;
	}

	public void setBoxes(List<Box> boxes) {
		this.boxes = boxes;
	}

	public Long getBoxId() {
		return boxId;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;
	}

	public Integer getPriorityKind() {
		return priorityKind;
	}

	public void setPriorityKind(Integer priorityKind) {
		this.priorityKind = priorityKind;
	}

	public Map<Integer, String> getPriorityMap() {
		return priorityMap;
	}

	public void setPriorityMap(Map<Integer, String> priorityMap) {
		this.priorityMap = priorityMap;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<BoxBean> getBoxBeans() {
		return boxBeans;
	}

	public void setBoxBeans(List<BoxBean> boxBeans) {
		this.boxBeans = boxBeans;
	}
}

