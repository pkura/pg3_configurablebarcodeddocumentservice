package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.archive.repository.search.SearchFilter;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class ExploreFilter extends EventActionSupport
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ExploreFilter.class.getPackage().getName(),null);
 

    private List searchFilter;
    private Long[] deleteKeys;
    private String BUsunZaznaczone;
@Override
protected void finalize() throws Throwable {
	// TODO Auto-generated method stub
	super.finalize();
}
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			try {
				List<SearchFilter> filtrWysz = SearchFilter.list();
				int keys = filtrWysz.size();
				searchFilter = new ArrayList(keys);

				for (int i = 0; i < keys; i++) {
					Map bean = new HashMap();
						
						bean.put("key", filtrWysz.get(i).getId());
						bean.put("filterName", filtrWysz.get(i).getFilterName());
						bean.put("link",  "/repository/search-documents.action?id="+filtrWysz.get(i).getId());

						searchFilter.add(bean);
					}

			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

   
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteKeys == null || deleteKeys.length == 0)
            {
                event.addActionError("NieWybranoElementowDoUsuniecia");
                return;
            }
            

            try
            {
            	  DSApi.context().begin();

                  for (int i=0; i < deleteKeys.length; i++)
                  {
                	  SearchFilter k = SearchFilter.find(deleteKeys[i]);
                	  addActionMessage("Usuni�to '"+k.getTitle()+"'");
                	  k.delete();
                  }

                  DSApi.context().commit();
                }

            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
        }
    }

    public List getSearchFilter()
    {
        return searchFilter;
    }

    public void setDeleteKeys(Long[] deleteKeys)
    {
        this.deleteKeys = deleteKeys;
    }

    public String getBUsunZaznaczone()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("BUsunZaznaczone");
    }

    public void setBUsunZaznaczone(String usunZaznaczone)
    {
        BUsunZaznaczone = usunZaznaczone;
    }
}
