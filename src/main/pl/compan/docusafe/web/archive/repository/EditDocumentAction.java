package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentLock;
import pl.compan.docusafe.core.base.DocumentLockMode;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentSignature;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.DocumentWatch;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Daa;
import pl.compan.docusafe.web.DrsInfo;
import pl.compan.docusafe.web.archive.security.SetPermissionsAction;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.office.CloneDocumentManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.SetResultListener;
import std.fun;
import std.lambda;

import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2005-05-10 13:09:06 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditDocumentAction.java,v 1.167 2010/08/04 06:59:28 mariuszk Exp $
 */
public class EditDocumentAction extends EventActionSupport
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(EditDocumentAction.class.getPackage().getName(),null);
    final static Logger log = LoggerFactory.getLogger(EditDocumentAction.class);
    private Long id;

    private Long doctypeId;
    private String title;
    private String description;
    private Long[] ids;
    private Long folderId;
    private String unlockReason;
    private String email;
    private String boxNumber;
    private Long boxId;
    private String lockExpiration;
    private Map<Integer, Object> doctypeFields = new HashMap<Integer, Object>();
    private boolean canUndelete;
    private boolean canClone;
    private boolean canModifyAttachments;
    private boolean canUnarchive;
    private boolean zamawianie;

    private String officeLink;

    private String currentBoxNumber;
    private Long currentBoxId;

    private String folderPrettyPath;
    private String queryLink;
    private String addAttachmentLink;
    private Map<Long, String> doctypes;
    private String documentAuthor;
    private String folderLink;
    private String permissionsLink;
    private String sendLinkLink;
    private String lockDocumentLink;
    private boolean canSetPermissions;
    private boolean canLock;
    private boolean canUnlock;
    private boolean canDelete;
    private boolean canUpdate;
    private boolean canReadAttachments;
    private Date lockedThru;
    private String lockedBy;
    private Document document;
    private List<Map<String, Object>> attachments;
    private List<Doctype.Field> doctypeFieldList;
    private boolean boxNumberReadonly;

    private boolean nwEnableDRS;
    private String drsSourceLink;
    private String drsOfficeSourceLink;
    private Integer drsSourceKO;
    private Long drsSourceID;
    private boolean drsSourceSigned;
    // u�ywane dla dokument�w "nationwide", dla kt�rych wygenerowano DRS
    private List<DrsInfo> drsDocs;

    /** czy dokument jest zablowany podpisem */
    private Boolean blocked;
    private boolean useSignature;
    private boolean canSign;
    private Collection<Map> signatureBeans;

    private String agent_imie, agent_nazwisko;
    private String agent_numer;
    private String agencja_nazwa;
    private String agencja_numer;
    private String agencja_nip;
    private Integer rodzaj_sieci;
    private boolean canReadDAASlowniki;
    private boolean canChangeDoctype;

    private Doctype currDoctype;
    private boolean daa;

    /** true <=> przypisany do agenta lub agencji (tylko dla dokumentow DAA) */
    private boolean daaAssigned;
    private List<Rodzaj_dokumentu> rodzaje_dokumentow = new ArrayList<Rodzaj_dokumentu>();
    private String rodzaj_dokumentu;
    /**Pozwala na prze��czenie si� do zak�adki historia zam�wie� dokumentu*/
    public boolean hist;
    /**Blokada przycisku Zam�w w dokumencie*/
    private boolean blokada;
    public static final String EV_FILL = "fillForm";
    /**uprawnienie ustawiane w Administacji->Role->U�ytkownik->Lista rozwijana(na dole)*/
    private boolean obsluga;
    

    public boolean isObsluga()
    {
        return obsluga;
    }

    public void setObsluga(boolean obsluga)
    {
        this.obsluga = obsluga;
    }

    public static String getLink(Long id)
    {
        return "/repository/edit-document.action?id="+id;
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdateBox").
            append(OpenHibernateSession.INSTANCE).
            append(new UpdateBox()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);


        registerListener("doDeleteAttachments").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteAttachments()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteAttachmentsPermanently").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteAttachmentsPermanently()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUndelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Undelete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("explore-documents"));

        registerListener("doSendUnlockRequest").
            append(OpenHibernateSession.INSTANCE).
            append(new UnlockRequest()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUnlock").
            append(OpenHibernateSession.INSTANCE).
            append(new Unlock()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddToFavourites").
            append(OpenHibernateSession.INSTANCE).
            append(new ToFavourites()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddToBookmarks").
            append(OpenHibernateSession.INSTANCE).
            append(new ToBookmarks()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeDoctype").
            append(OpenHibernateSession.INSTANCE).
            append(new ChangeDoctype()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPermanentlyDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new PermanentlyDelete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUnarchive").
            append(OpenHibernateSession.INSTANCE).
            append(new Unarchive()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClone").
            append(OpenHibernateSession.INSTANCE).
            append(new CloneDocument()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }


    private class Unarchive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                document = Document.find(id);

                Doctype doctype = Doctype.findByCn("nationwide");

                if (document.getDoctype() != null)
                {
                    document.setDoctype(null);
                    document.setDoctype(doctype);

                    if (document instanceof OfficeDocument)
                    {
                        
                        document.changelog(DocumentChangelog.UNARCHIVE_DOCUMENT);
                        ((OfficeDocument) document).addWorkHistoryEntry(Audit.create("::nw_archive", DSApi.context().getPrincipalName(),
                                sm.getString("CofnietoArchiwizacjeDokumentu")));
                    }

                }

                document.setFolder(Folder.getRootFolder());

                if (document instanceof OfficeDocument)
                {
                    TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }



    private class ChangeDoctype implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (rodzaj_dokumentu == null)
                return;

            try
            {
                DSApi.context().begin();

                document = Document.find(id);
                Doctype doctype = Doctype.findByCn(rodzaj_dokumentu);

                boolean wasDoctype = (document.getDoctype() != null);
               /* if (document.getDoctype() != null)
                {*/
                    if ((document.getDoctype() == null) || !document.getDoctype().equals(doctype))
                    {
                        document.setDoctype(null);
                        document.setDoctype(doctype);

                        doctype.set(document.getId(),doctype.getFieldByCn("STATUS").getId(), doctype.getFieldByCn("STATUS").getEnumItemByCn("PRZYJETY").getId());
                        if (document.getType() == DocumentType.INCOMING)
                            doctype.set(document.getId(),doctype.getFieldByCn("DATA_WPLYWU").getId(), ((InOfficeDocument) document).getIncomingDate());

                        if ("daa".equals(rodzaj_dokumentu))
                        {
                            doctype.set(document.getId(),doctype.getFieldByCn("TYP_DOKUMENTU").getId(), new Integer(90)/* typ "Inne" z dokument�w OWCA */);
                            doctype.set(document.getId(),doctype.getFieldByCn("RODZAJ_SIECI").getId(), doctype.getFieldByCn("RODZAJ_SIECI").getEnumItemByCn("SIEC_WLASNA").getId());

                            Doctype.EnumItem typEnum = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(90);
                            Doctype.EnumItem siecEnum = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItem(doctype.getFieldByCn("RODZAJ_SIECI").getEnumItemByCn("SIEC_WLASNA").getId());
                            Daa.daaDocument(document, null,  null,  siecEnum, typEnum.getTitle(), typEnum.getArg1(), null, false);
                            if (document instanceof OfficeDocument) {
                                ((OfficeDocument) document).changelog(DocumentChangelog.CHANGE_DOCKIND);
                                ((OfficeDocument) document).setSummaryOnly(document.getDescription());
                                String tmp = null;
                                if (wasDoctype)
                                    tmp = sm.getString("ZmienionoRodzajDokumentuZbiznesowegoNaArchiwumAgenta");
                                else
                                    tmp = sm.getString("ZmienionoRodzajDokumentuNaArchiwumAgenta");
                                ((OfficeDocument) document).addWorkHistoryEntry(
                                       Audit.create("changeDoctype",
                                            DSApi.context().getPrincipalName(),
                                            tmp));
                            }
                            Daa.daaDocumentPermission(document);
                        }

                    }
                /*}*/

                if (document instanceof OfficeDocument)
                {
                    TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            ids = null;
            if(!hist)
            {
                sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                
            	blokada = false;

                addAttachmentLink = NewAttachmentAction.getLink(id);
    
                try
                {
                    document = Document.find(id);
    
                    useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
                    canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);
                    
                    if(document != null && document.getDoctype() != null) {
                        rodzaj_dokumentu = document.getDoctype().getCn();
                        if(!"daa".equals(rodzaj_dokumentu)) rodzaj_dokumentu = "nationwide";
                    }
    
    
    
                    if (!DSApi.context().hasObjectPermission(document, ObjectPermission.READ))
                        throw new AccessDeniedException(sm.getString("BrakUprawnienDoOdczytaniaDokumentu"));
    
                    if (Docusafe.hasExtra("daa"))
                    {
    	                canChangeDoctype = ((document.getDoctype() != null) && (("daa".equals(document.getDoctype().getCn())) || ("nationwide".equals(document.getDoctype().getCn()))));
    	                if (document.getDoctype() == null)
    	                	canChangeDoctype = true;
                    }
                    else
                    	canChangeDoctype = false;
                    
                    canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);
    
                    title = document.getTitle();
                    description = document.getDescription();
                    folderId = document.getFolderId();
                    folderPrettyPath = document.getFolderPath(" / ");
                    
                    //zakotwiczenie
                    folderPrettyPath+="#"+Folder.findDestAnchor(Folder.find(folderId)).getId();
                    
                    
                    canUndelete = document.isDeleted() && DSApi.context().isAdmin();
                    canDelete = !document.isDeleted() && document.canDelete();
                    canUpdate = document.canModify();

                    canModifyAttachments = document.canModifyAttachments();
                    canReadAttachments = document.canReadAttachments();
                    //canClone = !(document instanceof OutOfficeDocument);
                    //canClone = !(document instanceof );//true;
                    if(document.getDoctype() != null)
                        canClone = "nationwide_drs".equals(document.getDoctype().getCn()) ? false : true;
                    else
                        canClone = true;
                    //canClone = document.getDoctype() != null ? true: false;
    
    
                    canUnarchive = document.getDoctype() != null;
                    boxNumberReadonly = !DSApi.context().isAdmin();
    
                    if (document.getBox() != null)
                    {
                        boxNumber = document.getBox().getName();
                        boxId = document.getBox().getId();
                    }
    
                    if (/*boxNumber == null && */GlobalPreferences.isBoxOpen())
                    {
                        Long boxId = GlobalPreferences.getBoxId();
                        if (boxId != null)
                        {
                            try
                            {
                                Box box = Box.find(boxId);
                                currentBoxNumber = box.getName();
                                currentBoxId = box.getId();
                            }
                            catch (EntityNotFoundException e)
                            {
                            }
                        }
                    }
    
                    if(document.getDoctype() != null) doctypeId = document.getDoctype().getId();
    
    
                    daa = ((document.getDoctype() != null) && ("daa".equals(document.getDoctype().getCn())));
    
                    /* gdy dokument DAA to sprawdzam czy dokument juz przypisany - jesli tak, to nie mozna zrobic go "nieprzypisanym" */
                    if (Docusafe.hasExtra("daa") && (document.getDoctype() != null) && ("daa".equals(document.getDoctype().getCn())))
                    {
                        try {
                            doctypeFields = document.getDoctype().getValueMap(document.getId());
                            String agentIds = doctypeFields.get(document.getDoctype().getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(document.getDoctype().getFieldByCn("AGENT").getId()).toString() : null;
                            String agencjaIds = doctypeFields.get(document.getDoctype().getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(document.getDoctype().getFieldByCn("AGENCJA").getId()).toString() : null;
    
                            Doctype.EnumItem typEnum = document.getDoctype().getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(document.getDoctype().getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
                            if (agentIds != null || agencjaIds != null || "RODZAJ_RAPORT".equals(typEnum.getArg1()))
                                daaAssigned = true;
                        }
                        catch(NullPointerException e) {
    
                        }
                    }
    
                    // umieszczam na li�cie tylko jeden typ dokumentu, bo
                    // lista i tak jest nieaktywna
                    doctypes = new LinkedHashMap<Long, String>();
                    List allDoctypes = Doctype.list(true);
    
                    for (Iterator iter=allDoctypes.iterator(); iter.hasNext(); )
                    {
                        Doctype d = (Doctype) iter.next();
                        doctypes.put(d.getId(), d.getName());
                    }
    
                    if (doctypeId != null )
                    {
                        Doctype doctype = Doctype.find(doctypeId);
                        doctypeFieldList = Arrays.asList(doctype.getFieldsAsArray());
    
                        if (document.getDoctype() != null)
                        {
                            doctypeFields = new HashMap<Integer, Object>();
                            Map<Integer, Object> values = document.getDoctype().getValueMap(document.getId());
                            // pola nowego typu, do kt�rego chcemy skonwertowa� istniej�cy typ
                            Doctype.Field[] foreignFields = doctype.getFieldsAsArray();
                            for (int i=0; i < foreignFields.length; i++)
                            {
                                try
                                {
                                    Doctype.Field docField = document.getDoctype().
                                        getFieldByCn(foreignFields[i].getCn());
                                    doctypeFields.put(foreignFields[i].getId(),
                                        foreignFields[i].coerce(values.get(docField.getId())));
    //                                    document.getDoctype().coerce(
    //                                        foreignFields[i], values.get(docField.getId())));
                                }
                                catch (EntityNotFoundException e)
                                {
                                }
                                catch(NullPointerException e) {}
                            }
                        }
                    }
                    else if (document.getDoctype() != null )
                    {
                    // je�eli doctypeId != null, to znaczy �e typ w formularzu
                    // zosta� zmieniony i w ChangeDoctype wygenerowano now� list�
                    // p�l dla wybranego typu
                        if (!allDoctypes.contains(document.getDoctype()))
                            doctypes.put(document.getDoctype().getId(), document.getDoctype().getName());
    
    
    
                        doctypeId = document.getDoctype().getId();
                        doctypeFieldList = Arrays.asList(document.getDoctype().getFieldsAsArray());
    
                        doctypeFields = document.getDoctype().getValueMap(document.getId());
    
                        String agentIds = null;
                        String agencjaIds = null;
                        String rodzaj_sieciIds = null;
    
                        if(document.getDoctype().getFieldByCn("AGENT") != null && document.getDoctype().getFieldByCn("AGENCJA") != null && document.getDoctype().getFieldByCn("RODZAJ_SIECI") != null ) {
                            agentIds = doctypeFields.get(document.getDoctype().getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(document.getDoctype().getFieldByCn("AGENT").getId()).toString() : null;
    
                            agencjaIds = doctypeFields.get(document.getDoctype().getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(document.getDoctype().getFieldByCn("AGENCJA").getId()).toString() : null;
    
                            rodzaj_sieciIds = doctypeFields.get(document.getDoctype().getFieldByCn("RODZAJ_SIECI").getId()) != null ? doctypeFields.get(document.getDoctype().getFieldByCn("RODZAJ_SIECI").getId()).toString() : null;
    
                            DaaAgent agent = null;
                            DaaAgencja agencja = null;
    
    
                            try {
                                if(agentIds != null) {
                                    agent = new DaaAgent().find(new Long(agentIds.trim()));
                                    if(agent != null) {
                                        agent_imie = agent.getImie();
                                        agent_numer = agent.getNumer();
                                        agent_nazwisko = agent.getNazwisko();
                                    }
                                }
                                if(agencjaIds != null) {
                                    agencja = new DaaAgencja().find(new Long(agencjaIds.trim()));
                                    if(agencja != null) {
                                        agencja_nazwa = agencja.getNazwa();
                                        agencja_numer = agencja.getNumer();
                                        agencja_nip = agencja.getNip();
                                    }
                          
                                }
                                if(rodzaj_sieciIds != null) {
                                    rodzaj_sieci = new Integer(rodzaj_sieciIds);
                                }
    
                            }   catch(Exception ex) {
                       
                            }
    
               
    
                        }
    
                    }
    
                    documentAuthor = DSUser.safeToFirstnameLastnameName(document.getAuthor());
                    folderLink = ExploreDocumentsAction.getLink(document.getFolderId());
                    permissionsLink = SetPermissionsAction.getLink(
                        ServletActionContext.getRequest(), "document", document.getId());
                    sendLinkLink = SendLinkAction.getLink(document.getId());
                    lockDocumentLink = LockDocumentAction.getLink(document.getId());
    
    
                    /* podpisy */
                    if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                    {
                        blocked = (document.getBlocked() != null && document.getBlocked());
                        List<DocumentSignature> signatures = DocumentSignature.findByDocumentId(document.getId()/*, false*/);
    
                        class mapper implements lambda<DocumentSignature, Map>
                        {
                            public Map act(DocumentSignature signature)
                            {
                                BeanBackedMap result = new BeanBackedMap(signature,
                                    "id", "ctime", "correctlyVerified");
                                try
                                {
                                    result.put("author", DSUser.findByUsername(signature.getAuthor()));
                                }
                                catch (EdmException e)
                                {
                                }
    
                                return result;
                            }
                        }
    
                        final mapper mapper = new mapper();
                        signatureBeans = fun.map(signatures, mapper);
                    }
    
                    List attachmentList = document.listAttachments();
                    attachments = new ArrayList<Map<String, Object>>(attachmentList.size());
                    for (Iterator iter=attachmentList.iterator(); iter.hasNext(); )
                    {
                        Attachment attachment = (Attachment) iter.next();
                        // obiekt attachment mo�e by� r�wny null np. w�wczas, gdy
                        // w numeracji p�l DS_ATTACHMENT.POSN znajdzie si�
                        // przerwa
                        if (attachment == null)
                        {
                            event.getLog().warn("attachment=null, document="+document);
                            continue;
                        }
    
                        Map<String, Object> bean = new HashMap<String, Object>();
                        bean.put("id", attachment.getId());
                        bean.put("title", attachment.getTitle());
                        bean.put("cn", attachment.getCn());
                        bean.put("editLink", EditAttachmentAction.getLink(attachment.getId()));
    
                        Object[] revs = attachment.getRevisions().toArray();
                        if (revs != null && revs.length > 0)
                        {
                            AttachmentRevision rev = (AttachmentRevision) revs[revs.length-1];
                            bean.put("recentContentLink",
                                ViewAttachmentRevisionAction.getLink(
                                    ServletActionContext.getRequest(), rev.getId()));
                            bean.put("ctime", rev.getCtime());
                            bean.put("size", rev.getSize());
                            bean.put("revision", rev.getRevision());
                            bean.put("mime", rev.getMime());
                            bean.put("icon", MimetypesFileTypeMap.getInstance().getIcon(
                                MimetypesFileTypeMap.getInstance().getContentType(
                                    rev.getOriginalFilename())));
                            bean.put("author", DSUser.safeToFirstnameLastnameName(rev.getAuthor()));
    
                            bean.put("showRecentContentLink", "/viewserver/viewer.action?id="+rev.getId());
                            bean.put("revisionId", rev.getId());
                        }
                        else
                        {
                            bean.put("noContent", Boolean.TRUE);
                        }
    
                        // ponizsze na potrzeby "zalacznikow-maili"
                        if ("mail".equals(attachment.getCn()))
                        {
                            bean.put("ctime", attachment.getCtime());
                            bean.put("author", DSUser.safeToFirstnameLastnameName(attachment.getAuthor()));
                            bean.put("wparam", attachment.getWparam());
                        }
    
                        attachments.add(bean);
                    }
    
                    canSetPermissions = DSApi.context().getSecurityHelper().canSetPermissions(document);
    
                    if (document.canLock(DocumentLockMode.WRITE))
                    {
                        canLock = true;
                        Calendar cal = new GregorianCalendar();
                        cal.add(Calendar.DATE, 1);
                        lockExpiration = DateUtils.formatJsDateTime(cal.getTime());
                    }
    
                    DocumentLock lock = document.getWriteLock();
    
                    if (lock != null)
                    {
                        lockedThru = lock.getExpiration();
                        lockedBy = DSUser.safeToFirstnameLastname(lock.getUsername());
                        canUnlock = lock.getUsername().equals(DSApi.context().getPrincipalName());
                    }
                }
                catch (AccessDeniedException e)
                {
                    event.setResult("access-denied");
                }
                catch (DocumentLockedException e)
                {
                    event.setResult("locked");
                    try
                    {
                        lockedBy = DSUser.safeToFirstnameLastname(e.getUsername());
                    }
                    catch (EdmException e1)
                    {
                    	log.error("",e1);
                        addActionError(e.getMessage());
                    }
                }
                catch (EdmException e)
                {
                	log.error("",e);
                    addActionError(e.getMessage());
                    log.error("",e);
                }
            }
        }
    }


    private class UpdateBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            if (hasActionErrors())
                return;

            boxNumber = TextUtils.trimmedStringOrNull(boxNumber);

            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (boxNumber != null && DSApi.context().isAdmin())
                {
                    document.setBox(Box.findByName(null,boxNumber));
                }
                else if (boxId != null)
                {
                    document.setBox(Box.find(boxId));
                }
                else
                {
                    document.setBox(null);
                }

                DSApi.context().commit();

                addActionMessage(sm.getString("ZapisanoNumerPudlaArchiwalnego"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            title = TextUtils.trimmedStringOrNull(title);
            description = TextUtils.trimmedStringOrNull(description);
            boxNumber = TextUtils.trimmedStringOrNull(boxNumber);

            if (title == null)
                addActionError(sm.getString("NiePodanoTytuluDokumentu"));
            else if (title.length() > 254)
                addActionError(sm.getString("MaksymalnaDlugoscTytuluDokumentuTo254znaki"));

            if (description == null)
                addActionError(sm.getString("NiePodanoOpisuDokumentu"));
            else if (description.length() > 510)
                addActionError(sm.getString("MaksymalnaDlugoscOpisuDokumentuTo510znakow"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (!document.isDeleted() && folderId != null &&
                    !folderId.equals(document.getFolderId()))
                {
                    // FolderNotFoundException
                    document.setFolder(Folder.find(folderId));
                }

                // dotychczasowe warto�ci atrybut�w
                Map valueMap = Collections.EMPTY_MAP;

                if(document.getDoctype() != null)
                    valueMap = document.getDoctype().getValueMap(document.getId());

                document.setTitle(title);
                document.setDescription(description);


                Doctype doctype;

                if (boxNumber != null && DSApi.context().isAdmin())
                {
                    document.setBox(Box.findByName(null,boxNumber));
                }
                else if (boxId != null)
                {
                    document.setBox(Box.find(boxId));
                }
                else
                {
                    document.setBox(null);
                }

                //DateFormat dateTypeFormat = new SimpleDateFormat("dd-MM-yyyy");

                // je�eli Document.doctype == null, nadanie nowego typu, bez nadawania warto�ci
                // je�eli Document.doctype == doctypeId, nadanie warto�ci,
                // je�eli Document.doctype != doctypeId, nadanie nowego typu, bez warto�ci


                if (doctypeId != null && document != null)
                {
                    Map doctypeValues;
                    Map doctypeValuesByCn;
                    // XXX: uwaga - nie ka�de pole ma cn
                    // zbi�r zawieraj�cy cn p�l, kt�rych warto�ci uleg�y zmianie
                    Set<String> doctypeDirtyValuesByCn = new HashSet<String>();

                    // doctypeFields - warto�ci p�l



                  /*  if(daa) {
                        doctype = Doctype.findByCn("daa");
                       // document.setDoctype(null);

                    }
                    else */if (document.getDoctype() == null ||
                        !document.getDoctype().getId().equals(doctypeId))
                    {
                        doctype = Doctype.find(doctypeId);
                        document.setDoctype(null);
                        document.setDoctype(doctype);
                    }
                    else
                    {
                        doctype = document.getDoctype();
                    }


                    doctypeValues = doctype.getValueMap(document.getId());
                    doctypeValuesByCn = doctype.getValueMapByCn(document.getId());

                    boolean daaChanged = false;
                    Doctype.Field[] dtFields = doctype.getFieldsAsArray();
                    for (int i=0; i < dtFields.length; i++)
                    {
                        if (dtFields[i].isHidden() || (!DSApi.context().isAdmin() && dtFields[i].isReadOnly()))
                            continue;

                        String value = HttpUtils.valueOrNull(doctypeFields.get(dtFields[i].getId()));

                        if (dtFields[i].getCn() != null &&
                            ((value != null && doctypeValues.get(dtFields[i].getId()) == null) ||
                             (doctypeValues.get(dtFields[i].getId()) != null &&
                              !doctypeValues.get(dtFields[i].getId()).equals(dtFields[i].coerce(value)))))
                        {
                            doctypeDirtyValuesByCn.add(dtFields[i].getCn());
                        }

                        if ("AGENT".equals(dtFields[i].getCn()) || "AGENCJA".equals(dtFields[i].getCn()) || "RODZAJ_SIECI".equals(dtFields[i].getCn()))
                        {
                            String oldValue = null;
                            if (doctype.getFieldByCn(dtFields[i].getCn()).getId() != null)
                                 oldValue = (valueMap.get(doctype.getFieldByCn(dtFields[i].getCn()).getId()) != null) ? valueMap.get(doctype.getFieldByCn(dtFields[i].getCn()).getId()).toString().trim() : null;
                            if (oldValue == null || value == null)
                            {
                                if (oldValue != value)
                                    daaChanged = true;
                            }
                            else
                                if (!oldValue.equals(value.trim()))
                                    daaChanged = true;

                            doctype.set(document.getId(), dtFields[i].getId(), value);
                            
                        }
                        else
                        {
                            doctype.set(document.getId(), dtFields[i].getId(), value);
                            
                        }
                    }

                    if (daaChanged && (document instanceof OfficeDocument))
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.create("daa", DSApi.context().getPrincipalName(),
                                sm.getString("DokumentZarchiwizowanoWfolderze",document.getFolderPath()),
                                null, null));

                    if(Configuration.hasExtra("daa") && "daa".equals(doctype.getCn())) {

                      
                        String agentIds = doctypeFields.get(doctype.getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENT").getId()).toString() : null;
                        String agencjaIds = doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()).toString() : null;

                        DaaAgent agent = null;
                        DaaAgencja agencja = null;
                        if (agentIds != null) {
                            Long agentId =  new Long(agentIds.trim());
                            agent = new DaaAgent().find(agentId);
                        }
                        if (agencjaIds != null) {
                            Long agencjaId =  new Long(agencjaIds.trim());
                            agencja = new DaaAgencja().find(agencjaId);
                        }

                        Doctype.EnumItem e = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
                        String typ_dokumentu = "";
                        String rodzaj_dokumentu = "";
                        if(e != null) {
                            typ_dokumentu = e.getTitle(); if(typ_dokumentu != null) typ_dokumentu= typ_dokumentu.trim();
                            rodzaj_dokumentu = e.getArg1(); if(rodzaj_dokumentu != null) rodzaj_dokumentu = rodzaj_dokumentu.trim();
                        }
                        String klasaId = doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()).toString() : null;
                        Doctype.EnumItem klasaRaportuEnum = klasaId != null ? doctype.getFieldByCn("KLASA_RAPORTU").getEnumItem(new Integer(klasaId)) : null;
                        rodzaj_sieci = new Integer((String)doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId())).intValue();
                        Doctype.EnumItem siecEnum = Daa.getRODZAJ_SIECI(doctype, doctypeFields);

                        Daa.daaDocument(document, agent,  agencja,  siecEnum, typ_dokumentu, rodzaj_dokumentu, klasaRaportuEnum, false);
                        Daa.daaDocumentPermission(document);
                        if (document instanceof OfficeDocument)
                        	((OfficeDocument) document).setSummaryOnly(document.getDescription());

/*                        if (document.getType() == DocumentType.INCOMING)
                        {
                            for (Iterator iter=InOfficeDocumentKind.list().iterator(); iter.hasNext(); )
                            {
                                InOfficeDocumentKind kind = (InOfficeDocumentKind) iter.next();
                                if (kind.getName() != null && kind.getName().indexOf("iznesowy") > 0)
                                {
                                    ((InOfficeDocument) document).setKind(kind);
                                    break;
                                }
                            }
                        }*/
                    }
                }

                if (document instanceof OfficeDocument)
                {
                    TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                }

                DSApi.context().commit();

                addActionMessage("Zapisano zmiany");
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
           /*catch(NullPointerException e) {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            */
        }
    }


    char alfabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',  'r', 's', 't', 'u', 'w', 'x', 'y', 'z', ' ', ' '};



    public String pobierzPrzedzial(char s, int n) {
        int l = alfabet.length-1;
        int i = 0;
        int chk;
        char ss;
        String str = "a-b";

        while (i < l - n) {
            chk = i + n  ;
            if (chk > l) chk = l ;
            if(chk == 0) chk = 1;
            if (alfabet[i] <= s && alfabet[chk] >= s) {
                return "" + alfabet[i] + "-" + alfabet[chk];
            }
            if(s == '�') return "a-b";
            if(s == '�') return "c-d";
            if(s == '�') return "e-f";
            if(s == '�') return "k-l";
            if(s == '�') return "m-n";
            if(s == '�') return "o-p";
            if(s == '�') return "r-s";
            if(s == '�' || s ==  '�') return "�-�";
            i++;
            
            i += n;
        }
        return str;
    }





    private class DeleteAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                // XXX: nie mo�na zamieni� na document.canModifyAttachments(),
                // bo ta funkcja pozwala na wszystko dokumentom kancelaryjnym
                if (!DSApi.context().hasObjectPermission(document, ObjectPermission.MODIFY_ATTACHMENTS))
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZalacznika"));

                for (int i=0; ids != null && i < ids.length; i++)
                {
                    Attachment attachment = document.getAttachment(ids[i]);
                    if (attachment != null)
                    {
                        attachment.delete();
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteAttachmentsPermanently implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                // XXX: nie mo�na zamieni� na document.canModifyAttachments(),
                // bo ta funkcja pozwala na wszystko dokumentom kancelaryjnym
                if (!DSApi.context().hasObjectPermission(document, ObjectPermission.MODIFY_ATTACHMENTS))
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZalacznika"));

                for (int i=0; ids != null && i < ids.length; i++)
                {
                    document.scrubAttachment(ids[i]);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (document instanceof OfficeDocument)
                    throw new EdmException(sm.getString("NieMoznaUsunacTegoDokumentuPoniewazJest")+" " +
                        sm.getString("OnDokumentemKancelaryjnym"));
                document.delete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("explore-documents"); // oczekuje parametru folderId
                event.skip(EV_FILL);
            }
        }
    }

    private class PermanentlyDelete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                document.scrubAttachments();
                document.delete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {            	
                event.setResult("explore-documents"); // oczekuje parametru folderId
            }
        }
    }

    private class Undelete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                document.undelete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("explore-documents"); // oczekuje parametru folderId
            }
        }
    }

    private class Unlock implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document.find(id).unlock();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class UnlockRequest implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            unlockReason = TextUtils.trimmedStringOrNull(unlockReason);
            if (unlockReason == null)
                addActionError(sm.getString("NiePodanoPrzyczynyProsbyOodblokowanieDokumentu"));

            if (hasActionErrors())
                return;

            String email = null;
            String senderName = null;
            try
            {
                senderName = DSApi.context().getDSUser().asFirstnameLastname();
                Document.find(id);

                // metoda find rzuca wyj�tek DocumentLockedException,
                // z kt�rego mo�na odczyta� nazw� u�ytkownika blokuj�cego dokument

                // je�eli wykonywanie dosz�o tutaj, oznacza to, �e dokument nie
                // jest ju� zablokowany
                return;
            }
            catch (DocumentLockedException e)
            {
                // odczytanie nazwy i adresu u�ytkownika blokuj�cego dokument
                try
                {
                    DSUser user = DSUser.findByUsername(e.getUsername());

                    if (StringUtils.isEmpty(user.getEmail()))
                        throw new UserNotFoundException(user.getName());

                    email = user.getEmail();
                }
                catch (UserNotFoundException unfe)
                {
                    // je�eli nie znaleziono u�ytkownika zak�adaj�cego blokad�,
                    // pro�ba wysy�ana jest do admina
                    try
                    {

                        DSUser admin = DSUser.findByUsername(GlobalPreferences.getAdminUsername());

                        if (!StringUtils.isEmpty(admin.getEmail()))
                        {
                            email = admin.getEmail();
                            addActionMessage(sm.getString("WyslanoProsbeOodblokowanieDoAdministratora"));
                        }
                        else
                        {
                            addActionError(sm.getString("NieMoznaWyslacProsbyOodblokowanieAdministratorNie")+" " +
                                sm.getString("MaAdresuEmail"));
                        }
                    }
                    catch (EdmException ee)
                    {
                    	log.error("",ee);
                        addActionError(e.getMessage());
                    }
                }
                catch (EdmException ee)
                {
                    addActionError(e.getMessage());
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }

            if (email != null)
            {
                Map<String, Object> context = new HashMap<String, Object>();

                context.put("documentUrl", Configuration.getBaseUrl() +
                    "/repository/edit-document.action?id="+id);
                context.put("sender", senderName);
                context.put("reason", unlockReason);

                try
                {
                    ((Mailer) ServiceManager.getService(Mailer.NAME)).
                        send(email, null, null, Configuration.getMail(Mail.DOCUMENT_UNLOCK_REQUEST), context);
                }
                catch (Exception e)
                {
                    addActionError(e.getMessage());
                }

                addActionMessage(sm.getString("WyslanoProsbeOodblokowanieDokumentu"));
                unlockReason = null;
            }
        }
    }

    private class ToFavourites implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (!document.hasWatch(DocumentWatch.Type.DOCUMENT_MODIFIED))
                    document.addWatch(DocumentWatch.Type.DOCUMENT_MODIFIED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_DELETED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_DELETED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_ADDED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_ADDED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED);

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("favourites");
            }
        }
    }

    private class ToBookmarks implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                BookmarksAction.addDocument(document);

                DSApi.context().commit();

                event.addActionMessage(sm.getString("DodanoDokumentDoUlubionych"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
        }
    }

    private class CloneDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                CloneDocumentManager.clone(id);

                DSApi.context().commit();

                addActionMessage(sm.getString("DokumentZostalSklonowany"));

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

        }
    }
    
    public String getFolderPrettyPath()
    {
        return folderPrettyPath;
    }

    public String getQueryLink()
    {
        return queryLink;
    }

    public void setQueryLink(String queryLink)
    {
        this.queryLink = queryLink;
    }

    public String getAddAttachmentLink()
    {
        return addAttachmentLink;
    }

    public Map getDoctypes()
    {
        return doctypes;
    }

    public String getDocumentAuthor()
    {
        return documentAuthor;
    }

    public String getFolderLink()
    {
        return folderLink;
    }

    public String getPermissionsLink()
    {
        return permissionsLink;
    }

    public String getSendLinkLink()
    {
        return sendLinkLink;
    }

    public String getLockDocumentLink()
    {
        return lockDocumentLink;
    }

    public boolean isCanSetPermissions()
    {
        return canSetPermissions;
    }

    public boolean isCanLock()
    {
        return canLock;
    }

    public boolean isCanUnlock()
    {
        return canUnlock;
    }

    public Date getLockedThru()
    {
        return lockedThru;
    }

    public String getLockedBy()
    {
        return lockedBy;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getDoctypeId()
    {
        return doctypeId;
    }

    public void setDoctypeId(Long doctypeId)
    {
        this.doctypeId = doctypeId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long[] getIds()
    {
        return ids;
    }

    public void setIds(Long[] ids)
    {
        this.ids = ids;
    }

    public Long getFolderId()
    {
        return folderId;
    }

    public void setFolderId(Long folderId)
    {
        this.folderId = folderId;
    }

    public String getUnlockReason()
    {
        return unlockReason;
    }

    public void setUnlockReason(String unlockReason)
    {
        this.unlockReason = unlockReason;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getLockExpiration()
    {
        return lockExpiration;
    }

    public void setLockExpiration(String lockExpiration)
    {
        this.lockExpiration = lockExpiration;
    }

    public Document getDocument()
    {
        return document;
    }

    public List getAttachments()
    {
        return attachments;
    }

    public List getDoctypeFieldList()
    {
        return doctypeFieldList;
    }

    public Map getDoctypeFields()
    {
        return doctypeFields;
    }

    public void setDoctypeFields(Map<Integer, Object> doctypeFields)
    {
        this.doctypeFields = doctypeFields;
    }

    public boolean isCanUndelete()
    {
        return canUndelete;
    }

    public String getOfficeLink()
    {
        return officeLink;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanUpdate()
    {
        return canUpdate;
    }

    public boolean isCanClone()
    {
        return canClone;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }

    public String getCurrentBoxNumber()
    {
        return currentBoxNumber;
    }

    public Long getBoxId()
    {
        return boxId;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public Long getCurrentBoxId()
    {
        return currentBoxId;
    }

    public boolean isCanReadAttachments()
    {
        return canReadAttachments;
    }

    public boolean isCanModifyAttachments()
    {
        return canModifyAttachments;
    }

    public boolean isCanUnarchive()
    {
        return canUnarchive;
    }

    public boolean isBoxNumberReadonly()
    {
        return boxNumberReadonly;
    }

    public boolean isNwEnableDRS()
    {
        return nwEnableDRS;
    }

    public String getDrsSourceLink()
    {
        return drsSourceLink;
    }

    public String getDrsOfficeSourceLink()
    {
        return drsOfficeSourceLink;
    }

    public List<DrsInfo> getDrsDocs()
    {
        return drsDocs;
    }

    public Integer getDrsSourceKO()
    {
        return drsSourceKO;
    }

    public Long getDrsSourceID()
    {
        return drsSourceID;
    }

    public boolean getDrsSourceSigned()
    {
        return drsSourceSigned;
    }

    public Boolean getBlocked()
    {
        return blocked;
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public boolean isCanSign()
    {
    	return canSign;
    }
    
    public Collection<Map> getSignatureBeans()
    {
        return signatureBeans;
    }

    public String getAgent_imie() {
        return agent_imie;
    }

    public void setAgent_imie(String agent_imie) {
        this.agent_imie = agent_imie;
    }

    public String getAgent_nazwisko() {
        return agent_nazwisko;
    }

    public void setAgent_nazwisko(String agent_nazwisko) {
        this.agent_nazwisko = agent_nazwisko;
    }

    public String getAgent_numer() {
        return agent_numer;
    }

    public void setAgent_numer(String agent_numer) {
        this.agent_numer = agent_numer;
    }

    public String getAgencja_nazwa() {
        return agencja_nazwa;
    }

    public void setAgencja_nazwa(String agencja_nazwa) {
        this.agencja_nazwa = agencja_nazwa;
    }

     public String getAgencja_numer() {
        return agencja_numer;
    }

    public void setAgencja_numer(String agencja_numer) {
        this.agencja_numer = agencja_numer;
    }

     public String getAgencja_nip() {
        return agencja_nip;
    }

    public void setAgencja_nip(String agencja_nip) {
        this.agencja_nip = agencja_nip;
    }

    public Integer getRodzaj_sieci() {
        return rodzaj_sieci;
    }

    public void setRodzaj_sieci(Integer rodzaj_sieci) {
        this.rodzaj_sieci = rodzaj_sieci;
    }

    public Doctype getCurrDoctype() {
        return currDoctype;
    }

    public void setCurrDoctype(Doctype currDoctype) {
        this.currDoctype = currDoctype;
    }

    public boolean isDaa() {
        return daa;
    }

    public void setDaa(boolean daa) {
        this.daa = daa;
    }

    public boolean isDaaAssigned()
    {
        return daaAssigned;
    }

    public boolean isCanReadDAASlowniki() {
        return canReadDAASlowniki;
    }

    public void setCanReadDAASlowniki(boolean canReadDAASlowniki) {
        this.canReadDAASlowniki = canReadDAASlowniki;
    }

    public List<Rodzaj_dokumentu> getRodzaje_dokumentow() {
        return rodzaje_dokumentow;
    }

    public void setRodzaje_dokumentow(List<Rodzaj_dokumentu> rodzaje_dokumentow) {
        this.rodzaje_dokumentow = rodzaje_dokumentow;
    }

    public String getRodzaj_dokumentu() {
        return rodzaj_dokumentu;
    }

    public void setRodzaj_dokumentu(String rodzaj_dokumentu) {
        this.rodzaj_dokumentu = rodzaj_dokumentu;
    }

    public boolean isCanChangeDoctype()
    {
        return canChangeDoctype;
    }


    //klasa pomocnicza
    class Rodzaj_dokumentu {
        private String cn;
        private String nazwa;

        public Rodzaj_dokumentu(String cn, String nazwa) {
            this.cn = cn;
            this.nazwa = nazwa;
        }

        public String getCn() {
            return cn;
        }

        public void setCn(String cn) {
            this.cn = cn;
        }

        public String getNazwa() {
            return nazwa;
        }

        public void setNazwa(String nazwa) {
            this.nazwa = nazwa;
        }
    }

    public boolean isZamawianie()
    {
        return zamawianie;
    }

    public void setZamawianie(boolean zamawianie)
    {
        this.zamawianie = zamawianie;
    }

    public boolean isHist()
    {
        return hist;
    }

    public void setHist(boolean hist)
    {
        this.hist = hist;
    }

    public boolean isBlokada()
    {
        return blokada;
    }

    public void setBlokada(boolean blokada)
    {
        this.blokada = blokada;
    }
}


