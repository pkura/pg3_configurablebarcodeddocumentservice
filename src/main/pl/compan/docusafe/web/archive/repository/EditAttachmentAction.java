package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.beanutils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.upload.FormFile;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.AttachmentRevisionSignature;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Edycja za��cznika.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditAttachmentAction.java,v 1.17 2009/05/13 13:04:25 mariuszk Exp $
 */
public class EditAttachmentAction extends EventDrivenAction
{
	private StringManager smL =
        GlobalPreferences.loadPropertiesFile(EditAttachmentAction.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(EditAttachmentAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doCancelled", "doUpdate"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    public static final String FORWARD = "repository/edit-attachment";
    /**
     * Ile wersji za��cznika jest pokazywanych domy�lnie.
     */
    public static final int SHOW_REVISIONS = 5;

    private static StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        fillForm(request, form, errors);
        return mapping.findForward("main");
    }

    public ActionForward doUpdate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String title = (String) form.get("title");
        String description = (String) form.get("description");

        if (StringUtils.isEmpty(title))
            errors.add(sm.getString("document.missingTitle"));
        if (StringUtils.isEmpty(description))
            errors.add(sm.getString("document.missingDescription"));

        InputStream stream = null;
        FormFile file = (FormFile) form.get("file");

        // plik interesuje mnie tylko wtedy, je�eli jakikolwiek zostanie
        // przes�any, wtedy sprawdzam czy ma przypadkiem d�ugo�� zero
        // i wypisuj� odpowiedni komunikat b��du; je�eli �aden plik
        // nie jest przesy�any, ignoruj� ten parametr

        // o fakcie przes�ania pliku dowiaduj� si� z w�asno�ci
        // FormFile.getFileName() - je�eli ma d�ugo�� zero, nie wysy�ano pliku
        if (file != null && file.getFileName() != null &&
            file.getFileName().length() > 0 && file.getFileSize() > 0)
        {
            try
            {
                stream = file.getInputStream();
            }
            catch (IOException e)
            {
                errors.add(sm.getString("attachment.cannotOpenFile"));
            }
        }
        else if (file != null && file.getFileName() != null &&
            file.getFileName().length() > 0 && file.getFileSize() == 0)
        {
            errors.add(sm.getString("attachment.missingFileContent"));
        }

        if (errors.size() > 0)
        {
            fillForm(request, form, errors);
            return mapping.findForward("main");
        }

        Long id = (Long) form.get("id");
        Long binderId = (Long) form.get("binderId");

        if (id == null)
            throw new RuntimeException(sm.getString("editAttachment.missingId"));

        boolean success = false;
        try
        {
            DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
            ctx.begin();

            //DocumentHelper dh = ctx.getDocumentHelper();

            Attachment attachment = Attachment.find(id,true,binderId);

            String newTitle = TextUtils.trimmedStringOrNull((String) form.get("title"), 254);

            if (attachment.getTitle() != null && !attachment.getTitle().equals(newTitle))
                messages.add(smL.getString("ZmienionoTytulZalacznika"));

            attachment.setTitle(newTitle);

            String newDescription = TextUtils.trimmedStringOrNull((String) form.get("description"), 510);
            if (attachment.getDescription() != null && !attachment.getDescription().equals(newDescription))
                messages.add(smL.getString("ZmienionoOpisZalacznika"));

            attachment.setDescription(newDescription);

            // je�eli u�ytkownik przes�a� plik, tworz� now� rewizj�
            if (stream != null)
            {
                attachment.createRevision(stream, file.getFileSize(), file.getFileName());
            }

            ctx.commit();
            success = true;
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            log.error("", e);
            errors.add(e.getMessage());
        }
        finally
        {
            DSApi._close();
            fillForm(request, form, errors);
        }

        if (success && stream != null)
            messages.add(sm.getString("editAttachment.createdNewRevision"));

        return mapping.findForward("main");
    }

    public String[] getEventNames()
    {
        return eventNames;
    }

    private void fillForm(HttpServletRequest request, DynaActionForm form, Messages errors)
    {
        Long id = (Long) form.get("id");
        Long binderId = (Long) form.get("binderId");
        if (id == null)
            throw new RuntimeException(sm.getString("editAttachment.missingId"));

        boolean showAll = form.get("showAllRevisions") != null &&
            ((Boolean) form.get("showAllRevisions")).booleanValue();

        try
        {
            DSContext ctx = DSApi.open(AuthUtil.getSubject(request));

            Attachment attachment = Attachment.find(id,true,binderId);

            BeanUtils.copyProperties(form, attachment);

            request.setAttribute("attachment", attachment);

            if (attachment.getDocument() != null)
                request.setAttribute("documentLink",
                    EditDocumentAction.getLink(attachment.getDocument().getId()));

            //request.setAttribute("documentId", attachment.getDocument().getId());

            if (attachment.isDocumentType())
            {
                request.setAttribute("canReadAttachments", Boolean.valueOf(attachment.getDocument().canReadAttachments(binderId)));
            }

            List revisions = new ArrayList(attachment.getRevisions());
            Collections.reverse(revisions);

            DynaClass revisionClass = new BasicDynaClass("attachmentRevision", null,
                new DynaProperty[] {
                    new DynaProperty("id", Long.class),
                    new DynaProperty("revision", Integer.class),
                    new DynaProperty("ctime", java.util.Date.class),
                    new DynaProperty("size", Integer.class),
                    new DynaProperty("originalFilename", String.class),
                    new DynaProperty("author", String.class),
                    new DynaProperty("icon", String.class),
                    new DynaProperty("signatures", List.class)
                });

            DynaBean[] revisionBeans = new DynaBean[showAll ?
                revisions.size() : Math.min(SHOW_REVISIONS, revisions.size())];

            for (int i=0, n=revisions.size(); i < n && (showAll || i < SHOW_REVISIONS); i++)
            {
                AttachmentRevision revision = (AttachmentRevision) revisions.get(i);
                DynaBean bean = revisionClass.newInstance();
                bean.set("id", revision.getId());
                bean.set("revision", revision.getRevision());
                bean.set("ctime", revision.getCtime());
                bean.set("size", revision.getSize());
                bean.set("originalFilename", revision.getOriginalFilename());
                bean.set("icon", MimetypesFileTypeMap.getInstance().getIcon(
                        MimetypesFileTypeMap.getInstance().getContentType(
                            revision.getOriginalFilename())));
                bean.set("author", DSUser.safeToFirstnameLastnameName(revision.getAuthor()));
                List<AttachmentRevisionSignature> signatures = AttachmentRevisionSignature.findByAttachmentRevisionId(revision.getId());
                bean.set("signatures", signatures);
                //bean.set("signaturesSize", signatures.size());
                revisionBeans[i] = bean;
            }

            request.setAttribute("revisionBeans", revisionBeans);
            if(binderId != null)
            	request.setAttribute("binderId", binderId);
            if (!showAll && revisions.size() > SHOW_REVISIONS)
            {
                request.setAttribute("linkShowAll", request.getContextPath()+getLink(id, true));
            }
        }
        catch (EdmException e)
        {
            log.error("", e);
            errors.add(e.getMessage());
        }
        catch (Exception e)
        {
            log.error("", e);
            throw new RuntimeException(e.getMessage(), e);
        }
        finally
        {
            DSApi._close();
        }
    }

    public static String getLink(Long id)
    {
        return getLink(id, false);
    }

    public static String getLink(Long id, boolean showAll)
    {
        return "/repository/edit-attachment.do" +
            "?id="+id +
            (showAll ? "&showAllRevisions="+showAll : "");
    }
}
