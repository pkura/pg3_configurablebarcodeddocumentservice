package pl.compan.docusafe.web.archive.repository;

import java.security.AccessControlException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentHelper;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;


public class AttachmentSearchDocumentsAction extends EventActionSupport
{
    public static final StringManager sm = StringManager.getManager(Constants.Package);
    private static final Logger log = LoggerFactory.getLogger(AttachmentSearchDocumentsAction.class);  

    private StringManager langG ;
    
    private static final int LIMIT = 10;
    public static final String DEFAULT_COLUMNS_ATTACHMENT;
   
    static
    {
        DEFAULT_COLUMNS_ATTACHMENT = new JSONArray(Arrays.asList(new String[] {
            "document_id", "document_title", "document_ctime","officeNumber",
            "document_author", "document_mtime","document_remark","box_name","attachment_desc_filename"
        })).toString();
    }

    public static final String[] ALL_COLUMNS = { "document_id", "document_title",
                                                 "document_ctime", "document_mtime", "document_author",
                                                 "attachment_link1","document_remark", "document_description", 
                                                 "document_flags","document_user_flags", "current_workflow_location",
                                                 "box_name","attachment_desc_filename"};

    // mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy
    // wynik�w (w klasie QueryForm)
    private static final Map<String, String> sortProperties = new HashMap<String, String>();
    static
    {
        sortProperties.put("document_id", "id");
        sortProperties.put("document_title", "title");
        sortProperties.put("document_ctime", "ctime");
        sortProperties.put("document_mtime", "mtime");
    }
    
    private String klucz;
    private int offset;
    private int limit;
    private String sortField;
    private boolean ascending;

    // @EXPORT
    private List<Doctype> doctypes;
    private List<Map<String, Object>> results;
    private List columns;
    private Pager pager;
   

    private String thisSearchUrl;
  


    public static String getColumnDescription(String column)
    {
        return sm.getString("search.documents.column."+column);
    }
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
          
        }
    }
    
    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        			try{
        				  thisSearchUrl = getLink(offset, limit, null, null, klucz);
        				columns = getColumns(DSApi.context().userPreferences().node("attachment-search-documents").get("columns", DEFAULT_COLUMNS_ATTACHMENT));
                        List columnNames = getColumnNames(DSApi.context().userPreferences().node("attachment-search-documents").get("columns", DEFAULT_COLUMNS_ATTACHMENT));
                  
                   AttachmentHelper ah = DSApi.context().getAttachmentHelper();
                   QueryForm queryForm = new QueryForm(offset, limit);
                   
                   if (klucz != null)
                   {
                	   queryForm.addProperty("title", klucz);
                	   queryForm.addProperty("originalFilename", klucz);
                	   
                   }
                   
                   if (sortField != null)
                   {
                       if (ascending) queryForm.addOrderAsc(sortField);
                       else queryForm.addOrderDesc(sortField);
                   }
                   
                   else
                   {
                       queryForm.addOrderDesc("id");
                   }
                   
                   SearchResults searchResults = ah.getAttachment(queryForm);
                   if (searchResults.totalCount() == 0)
                   {
                      
                       event.addActionMessage(sm.getString("NieZnalezionoDokumentow"));
                   }else{
                   results = new ArrayList<Map<String, Object>>();
                   limit = limit > 0 ? limit : LIMIT;
        		if(klucz != null){
               
        			 while (searchResults.hasNext())
                     {
        				 
        				 AttachmentRevision attRe = (AttachmentRevision) searchResults.next();
                       	 Attachment att = attRe.getAttachment();
                       	 Document document = Document.find(att.getDocument().getId()); 
                       	 
                         Map<String, Object> bean = new HashMap<String, Object>();

                         bean.put("link", "/repository/edit-document.action?id="+document.getId());
                         bean.put("document_id", document.getId());
                         bean.put("document_title", document.getTitle());
                         bean.put("document_description", document.getDescription());
                         bean.put("document_ctime", document.getCtime());
                         
                         bean.put("document_mtime", document.getMtime());
                         bean.put("attachment_desc_filename", att.getDescription() + "  " + attRe.getOriginalFilename());
                         if (columnNames.contains("document_author"))
                         {
                             bean.put("document_author", DSUser.safeToFirstnameLastname(document.getAuthor()));
                         }
                         bean.put("document_remark",document.getLastRemark());
                         bean.put("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));
                         
                         if (columnNames.contains("attachment_link1") &&
                             document.canReadAttachments())
                         {
                             List atts = document.listAttachments();
                             if (atts.size() > 0)
                             {
                                 AttachmentRevision rev = ((Attachment) atts.get(atts.size()-1)).getMostRecentRevision();
                                 if (rev != null)
                                 {
                                     bean.put("attachment_link1", ViewAttachmentRevisionAction.getLink(
                                         ServletActionContext.getRequest(), rev.getId()));
                                     bean.put("attachment_rev1", rev.getRevision());
                                 }
                             }
                         
                         }
                         results.add(bean);
                      	  
                         
                     }
        			 
        			 Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                     {
                         public String getLink(int offset)
                         {
                        	 return AttachmentSearchDocumentsAction.getLink(offset, limit, sortField,
                                     Boolean.valueOf(ascending), klucz);
                         }
                     };
                     pager = new Pager(linkVisitor, offset, limit, searchResults.totalCount(), 10);
                }else{
                	addActionMessage(sm.getString("niepodanoslowkluczowych"));
                }
                   }
        	}catch(Exception e){
            	addActionMessage(e.toString());
        	}	
        
        }
        }
    

    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            String date = DateUtils.formatJsDateTime((Date) object);
            
            if (date.substring(date.indexOf(' ')+1).equals("00:00"))
                return DateUtils.formatJsDate((Date) object);
            else
                return date;
        }
        else
        {
            return object.toString();
        }
    }

    public String getSortLink(String sortField, boolean ascending)
    {
        return thisSearchUrl +
            (thisSearchUrl.indexOf('?') > 0 ? "&" : "?") +
            "sortField="+sortField+
            "&ascending="+ascending;
    }

    private List getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS_ATTACHMENT);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }

    private List getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.documents.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(array.getString(i), desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null,""));
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS_ATTACHMENT);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null,""));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public List getResults()
    {
        return results;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public List getColumns()
    {
        return columns;
    }

    public Pager getPager()
    {
        return pager;
    }

    public static String getLink(int offset, int limit, String sortField, Boolean ascending,
                          String klucz)
    {
        StringBuilder link = new StringBuilder("/repository/attachment-search-documents.action" +
            "?doSearch=true" +
            "&offset="+offset+
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField : "") +
            (ascending != null ? "&ascending="+ascending : "") +
        	(klucz!=null ? "&klucz="+klucz:""));
     
        return link.toString();
    }

    public String getThisSearchUrl() {
        return thisSearchUrl;
    }

    public void setThisSearchUrl(String thisSearchUrl) {
        this.thisSearchUrl = thisSearchUrl;
    }
	
	public String getKlucz() {
		return klucz;
	}
	
	public void setKlucz(String klucz) {
		this.klucz=klucz;
	}
	
}

