package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockMode;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LockDocumentAction.java,v 1.7 2008/04/03 16:26:30 pecet4 Exp $
 */
public final class LockDocumentAction extends EventProcessingAction
{
	private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(LockDocumentAction.class.getPackage().getName(),null);

    private static final String FORWARD = "repository/lock-document";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doLock").
            append(setForward).
            append(new Lock()).
            append("fillForm", fillForm);
    }

    /**
     * Blokowanie dokumentu.
     */
    private static class Lock implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            DynaActionForm form = event.getDynaForm();

            Long documentId = (Long) form.get("documentId");
            String sDate = (String) form.get("date");
            String sTime = (String) form.get("time");

            Date expiration;
            if (!StringUtils.isEmpty(sDate) && !StringUtils.isEmpty(sTime))
            {
                expiration = DateUtils.parseJsDateTime(sDate+" "+sTime);
            }
            else if (!StringUtils.isEmpty(sDate))
            {
                expiration = DateUtils.parseJsDateTime(sDate+" 23:59");
            }
            else
            {
                event.getErrors().add(sm.getString("NiePodanoDatyWygasnieciaBlokady"));
                return;
            }

            if (expiration.getTime() < System.currentTimeMillis())
            {
                event.getErrors().add(sm.getString("editDocument.invalidLockExpirationDate"));
                return;
            }

            try
            {
                DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                Document document = Document.find(documentId);

                document.lock(DocumentLockMode.WRITE, expiration);

                ctx.commit();

                event.getRequest().setAttribute("close", Boolean.TRUE);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            final DynaActionForm form = event.getDynaForm();
            if (form.get("time") == null || ((String) form.get("time")).length() == 0)
                form.set("time", "23:59");

            event.getRequest().setAttribute("documentLink", EditDocumentAction.getLink((Long) form.get("documentId")));
//
//            try
//            {
//                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
//            }
//            catch (EdmException e)
//            {
//                event.getErrors().add(e.getMessage());
//            }
//            finally
//            {
//                DSApi._close();
//            }
        }
    }

    public static String getLink(Long documentId)
    {
        final ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() + "?documentId="+documentId;
    }
}
