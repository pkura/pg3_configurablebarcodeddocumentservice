package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.field.DockindButtonField;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.logic.DaaLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.nationwide.CompileAttachmentsManager;
import pl.compan.docusafe.nationwide.CompileAttachmentsManager.CompilationStatus;
import pl.compan.docusafe.parametrization.pg.PgPackageLogic;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.DrsInfo;
import pl.compan.docusafe.web.archive.security.SetPermissionsAction;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.commons.BoxAction;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.commons.OfficeDocumentHelper;
import pl.compan.docusafe.web.office.common.DwrDocument;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.*;
import std.fun;
import std.lambda;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.HttpServletResponse;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class EditDockindDocumentAction extends EventActionSupport implements BoxAction,DockindButtonAction, DwrDocument
{
	private static final Logger log = LoggerFactory.getLogger(EditDockindDocumentAction.class);
    // @EXPORT
    private FieldsManager fm;
    private Map<String,String> documentKinds;
    /** info o mo�liwo�ci wykonania operacji "kompiluj pliki sprawy" */
    private CompilationStatus compilationStatus;
    private boolean canGenerateDocumentView;
    
    // @IMPORT
    private Map<String,Object> values = new HashMap<String,Object>();
    private boolean manualFolder; 
    private String acceptanceCn;
    private Map<Integer,String> centrumAcceptanceCn = new LinkedHashMap<Integer, String>();
    /** identyfikator obiektu, dla kt�rego wycofa� akceptacj� */
    private Long objectId;
    
    // @EXPORT/@IMPORT
    private String documentKindCn;
    private String remark;
    
    private Long id;

    private String title;
    private String description;
    private Long[] ids;
    private Long folderId;
    private String unlockReason;
    private String email;
    private String boxNumber;
    private Long boxId;
    private String lockExpiration;
    private boolean canUndelete;
    private boolean canClone;
    private boolean canModifyAttachments;
    private boolean canUnarchive;
    private boolean canDeleteAttachments;
    private boolean canModifyAttachmentsNfos;
    private boolean officeLinksEnabled;

    private List<DSUser> usersByDocumentToUpdate;
    private String officeLink;
    private String przywrocLink;
    private List<Task> officeTask;

    private String currentBoxNumber;
    private Long currentBoxId;

    private String folderPrettyPath;
    private String queryLink;
    private String addAttachmentLink;
    private String documentAuthor;
    private String folderLink;
    private String permissionsLink;
    private String sendLinkLink;
    private String lockDocumentLink;
    private boolean canSetPermissions;
    private boolean canLock;
    private boolean canUnlock;
    private boolean canDelete;
    private boolean canUpdate;
    private boolean canReadAttachments;
    private boolean canReadOrginalAttachments;
    private boolean canRead;
    private boolean popup;
    private Date lockedThru;
    private String lockedBy;
    private Document document;
    private List<Map<String, Object>> attachments;
    private boolean boxNumberReadonly;
    
    //lang    
    private static StringManager smL ;
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(EditDockindDocumentAction.class.getPackage().getName(),null);
    private boolean canAddToRS;
    // u�ywane dla dokument�w "nationwide", dla kt�rych wygenerowano DRS
    private List<DrsInfo> drsDocs;

    /** czy dokument jest zablowany podpisem */
    private Boolean blocked = Boolean.FALSE;
    private boolean useSignature;
    private boolean canSign;
    private Collection<Map> signatureBeans;

    private boolean pudloModify;
    private boolean usunZaznaczoneZalaczniki;
    private boolean trwaleUsunZaznaczoneZalaczniki;
    private boolean canReadDictionaries;
    private boolean canChangeDockind;
    /** true <=> przypisany do agenta lub agencji (tylko dla dokumentow DAA) */
    private boolean daaAssigned;
    /** zmienne do zamawiania */
    private Long idU;
    private Long idBox;
    private Date dateZam;
    private String status;
    /**Pozwala na prze��czenie si� do zak�adki historia zam�wie� dokumentu*/
    public boolean hist;

    /**Blokada przycisku Zam�w w dokumencie*/
    private boolean blokada;
    /**uprawnienie ustawiane w Administacji->Role->U�ytkownik->Lista rozwijana(na dole)*/
    private boolean zamawianie;
    /**uprawnienie ustawiane w Administacji->Role->U�ytkownik->Lista rozwijana(na dole)*/
    private boolean obsluga;
    /**czy mo�na dodawac noty koryguj�ce*/
    private boolean canAddCorrectingNotes;
    /**Id dokumentu teczki, potrzebny przy sprawdzaniu uprawnien po przejsciu do dokumentu z teczki*/
    private Long binderId;
    
    private Boolean createRemark = false;
    private boolean disabledAllButtons = false;
    private boolean takePackageConfirmation = false;
    private boolean packageKind;
    private boolean importedDocument;
    private List<Aspect> documentAspects;
    private String documentAspectCn;
	private Collection<RenderBean> processRenderBeans;
	private Map<String, Object> dockindKeys;

    private String processName;
	private String processId;
	private String processAction;
	
	private List<Flags.Flag> globalFlags;
	private List<Flags.Flag> userFlags;
	private boolean flagsPresent;
	private Long[] globalFlag;
    private Long[] userFlag;
	private Boolean needsNotBox;
	private String dockindEventValue;
	protected static OfficeDocumentHelper officeDocumentHelper = new OfficeDocumentHelper();

    public static final String EV_FILL = "fillForm";
    public static final String EV_UPDATE_FLAGS = "updateFlags";
    private static final DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
    protected static final String EV_UPDATE = "update";

    private Boolean forbidd;

    public Boolean getForbidd() {
        return forbidd;
    }

    public void setForbidd(Boolean forbidd) {
        this.forbidd = forbidd;
    }

    private Map<String, String> templates;

    public String getLink(Long id)
    {
    	if(!popup)
    		return "/repository/edit-document.action?id="+id;
    	else
    		return "/repository/edit-dockind-document-popup.action?id="+id;
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("jsonView").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(fillForm).
            append(new JsonView()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doChangeDockind").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm(true)).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUpdate").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Update()).
            append(EV_UPDATE_FLAGS, new UpdateFlags()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUpdateBox").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new UpdateBox()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doDeleteAttachments").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new DeleteAttachments()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doDeleteAttachmentsPermanently").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new DeleteAttachmentsPermanently()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doDelete").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Delete()).
            append(EV_FILL, fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doProcessAction")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new ProcessAction())
				.append(EV_FILL,new FillForm(false))
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUndelete").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Undelete()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doCancel").
            append(new SetResultListener("explore-documents"));

        registerListener("doSendUnlockRequest").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new UnlockRequest()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUnlock").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Unlock()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doAddToFavourites").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new ToFavourites()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doAddToBookmarks").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new ToBookmarks()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doPermanentlyDelete").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new PermanentlyDelete()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUnarchive").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Unarchive()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doCompileAttachments").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new CompileAttachments()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doGiveAcceptance").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Update()).
            append(new GiveAcceptance()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doWithdrawAcceptance").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Update()).
            append(new WithdrawAcceptance()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doGenerateDocumentView").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new GenerateDocumentView()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doDockindEvent").
	        append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	        append(new ValidateCreate()).
	        append(new DockindEvent(this)).
	        append(EV_FILL,new FillForm(false)).
	        appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        
        registerListener("doUpdateDWR").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new ValidateCreate()).
	    	append(EV_UPDATE, new UpdateDWR()).
	    	append(new UpdateFlags()).
	    	append(new FillForm(false)).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCreateTaskAndAssign").
	        append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	        append(new DoCreateTaskAndAssign()).
	        appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doConfirmTakePackage").
        	append(OPEN_HIBERNATE_AND_JBPM_SESSION).
        	append(new ConfirmTakePackage()).
        	append(EV_FILL,new FillForm(false)).
        	appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
       
       /* registerListener("doSelfAssignImportedDocument").
        	append(OPEN_HIBERNATE_AND_JBPM_SESSION).
        	append(new SelfAssignImportedDocument()).
        	append(EV_FILL,new FillForm(false)).
        	appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);*/
    }

    private final static Logger _PA_LOG = LoggerFactory
			.getLogger(ProcessAction.class);

    private class ProcessAction extends LoggedActionListener {
		// TODO uog�lni� na wszystkie akcje
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
//			if (log.isTraceEnabled()) {
				log.info("processId = " + processId);
				log.info("processName = " + processName);
				log.info("processAction = " + processAction);
//			}
			Document doc = Document.find(getDocumentId());
			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().getProcess(processName);
            assert pdef != null;
			ProcessInstance pi = pdef.getLocator().lookupForId(processId);
            //log.info("begin context");
			DSApi.context().begin();
            pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
			pdef.getLogic().process(
					pi,
					ProcessActionContext
                            .action(processAction)
                            .document((OfficeDocument)doc));
			DSApi.context().commit();
            //log.info("commited");

//            if(pa.isReassigned()){
//			    event.setResult(getTaskListResult());
//			    event.skip(EV_FILL);
//            }
		}

		@Override
		public void handleException(Exception e, ActionEvent event) {
			super.handleException(e, event);
//			event.skip(EV_ARCHIVE);
		}

		@Override
		public Logger getLogger() {
			return _PA_LOG;
		}
	 }

	private class UpdateDWR extends TransactionalActionListener{
		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception 
		{
			dwrDocumentHelper.update(EditDockindDocumentAction.this); 
		}
		
		@Override
		public Logger getLogger() {
			return log;
		}
	}
	
	private class ValidateCreate extends LoggedActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception 
        {
        	log.info("loading dwr fields");
            setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
        }

        @Override
        public Logger getLogger() 
        {
            return log;
        }
    }
	/**
	 * @return the processRenderBeans
	 */
	public Collection<RenderBean> getProcessRenderBeans() {
		return processRenderBeans;
	}

	/**
	 * @param processRenderBeans the processRenderBeans to set
	 */
	public void setProcessRenderBeans(Collection<RenderBean> processRenderBeans) {
		this.processRenderBeans = processRenderBeans;
	}

	private class DockindEvent implements ActionListener
	{
	 	private DockindButtonAction eventActionSupport;
	 	
	 	public DockindEvent(DockindButtonAction eventActionSupport)
	 	{
	 		this.eventActionSupport = eventActionSupport;
	 	}
	
		public void actionPerformed(ActionEvent event)
		{
			String eventName = "";
			try
			{
				document = Document.find(id);
				document.getDocumentKind().logic().doDockindEvent(eventActionSupport,event,document, null, getValues(),getDockindKeys());
			}
			catch (Exception e) 
			{
				addActionError("B��d wykonywania akcji "+e.getMessage());
				log.error("B��d wykonywania akcji "+eventName,e);
				
			}
		}
	}
	
	private class DoCreateTaskAndAssign implements ActionListener
	{
	 	private DockindButtonAction eventActionSupport;
	 	
	
		public void actionPerformed(ActionEvent event)
		{
			String eventName = "";
			try
			{
				document = Document.find(id);
				//WorkflowFactory.createNewProcess((OfficeDocument) document, false, sm.getString("przekazano"), document.getAuthor(), "rootdivision", DSApi.context().getPrincipalName());
			}
			catch (Exception e) 
			{
				addActionError("B��d wykonywania akcji "+e.getMessage());
				log.error("B��d wykonywania akcji "+eventName,e);
				
			}
		}
	}
    

    private class FillForm implements ActionListener
    {
        private boolean changeDockind = false;

        public FillForm(boolean changeDockind)
        {
            this.changeDockind = changeDockind;
        }

        public FillForm()
        {
        }

        public void actionPerformed(ActionEvent event)
        {
			if(getId() == null)
    		{
    			addActionMessage("Nie wybrano dokumentu");
    			return;
    		}
            
			smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            
            addAttachmentLink = NewAttachmentAction.getLink(id);

            try
            {
                try {
                    document = Document.find(id,binderId);
                } catch (pl.compan.docusafe.core.security.AccessDeniedException e) {
                    if(forbidd != null && forbidd) {
                        ServletActionContext.getResponse().setStatus(HttpServletResponse.SC_FORBIDDEN);
                        ServletActionContext.getResponse().setContentType("text/html; charset=iso-8859-2");
                        ServletActionContext.getResponse().setCharacterEncoding("iso-8859-2");

                        OutputStream out = ServletActionContext.getResponse().getOutputStream();

                        String html = "<html><head><title>" + HttpServletResponse.SC_FORBIDDEN + "</title></head><body><h1>"+HttpServletResponse.SC_FORBIDDEN+"</h1></body></html>";
                        PrintWriter writer = new PrintWriter(out);

                        writer.write(html);
                        writer.flush();
                        writer.close();
                        return;
                    } else {
                        throw e;
                    }
                }

                //document = Document.find(id,binderId);
                dwrDocumentHelper.fillForm(EditDockindDocumentAction.this);
                DocumentLogic logic = document.getDocumentKind().logic();

                useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
                canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);

                //wydaje sie bez sensu podczas szukania dokumentu sprawdzane sa uprawnienia
                
                canRead = document.canRead(binderId);
				if(!canRead) {
					addActionError(sm.getString("NieMaszUprawnienDoPodgladuTegoPisma"));
				}

                canChangeDockind = true;
                if(document != null && document.getDocumentKind() != null)
                {
                	canChangeDockind = document.getDocumentKind().logic().canChangeDockind(document);                
                }
                
                title = document.getTitle();
                description = document.getDescription();
                folderId = document.getFolderId();
                folderPrettyPath = document.getFolderPath(" / ");
                canUndelete = document.isDeleted() && DSApi.context().isAdmin();
                canDelete = !document.isDeleted() && document.canDelete();
                canUpdate = document.canModify(binderId);
                canModifyAttachments = document.canModifyAttachments(binderId);
                canReadAttachments = document.canReadAttachments(binderId);
                canReadOrginalAttachments = document.canReadOrginalAttachments(binderId);
                canDeleteAttachments = logic.canDeleteAttachments(document);
                officeLinksEnabled = true;

                //sprawdzenie czy wy��czona jest modyfikacja dokumentu z poziomu innej ni� lista zada�
                if(AvailabilityManager.isAvailable("document.edit.onlyFromTaskList") &&
                    !DSApi.context().hasPermission(DSPermission.EDYCJA_PISMA_SPOZA_LISTY_ZADAN)){
                    canDelete = false;
                    canUpdate = false;
                    canModifyAttachments = false;
                    canDeleteAttachments = false;
                }

                if(AvailabilityManager.isAvailable("browsedocument.officedocument.visible.onlyAdmin")){
                    officeLinksEnabled = DSApi.context().isAdmin();
                }

                //sprawdzenie czy mo�na doda� za��cznik dla NFOS
//				canModifyAttachmentsNfos = Minimal_Logic.getInstance().canModifyAttachments(document.getId());
                
                canClone = true;//document.getDoctype() != null ? true: false;

                // sprawdzanie czy mozliwa kompilacja plikow sprawy dla tego dokumentu
                compilationStatus = CompileAttachmentsManager.checkCompilationStatus(document);
                
                // czy mo�liwe jest wygenerowanie obrazu w poostaci PDF dla tego dokumentu
                canGenerateDocumentView = document.getDocumentKind().logic().canGenerateDocumentView();
                
                canUnarchive = document.getDocumentKind().logic().canUnarchive(document);
                canUnarchive &= canChangeDockind;
                
                
                trwaleUsunZaznaczoneZalaczniki = AvailabilityManager.isAvailable("trwaleUsunZaznaczoneZalaczniki") || DSApi.context().isAdmin();
                usunZaznaczoneZalaczniki = AvailabilityManager.isAvailable("usunZaznaczoneZalaczniki") || DSApi.context().isAdmin();
               
                if(AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow")&& Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
                	if (documentKindCn.equals(Docusafe.getAdditionProperty("kind-package"))){
                		packageKind = true;
                		disabledAllButtons();
                		canDeleteAttachments=true;
                		canModifyAttachments =true;
                		blocked = false;
                		trwaleUsunZaznaczoneZalaczniki=false;
                		
                	}
                }else{
                	packageKind =false;
                }
                
                dockindFillForm(changeDockind);
                officeDocumentHelper.initBox(EditDockindDocumentAction.this);
                
                if(AvailabilityManager.isAvailable("editDockindDocumentAction.SelfAssignImportedDocument") && checkDocIsImportedDocument()){
                	importedDocument = true;
                	disabledAllButtons();
                }

                if (Configuration.hasExtra("business") )
                {
                	DocumentKind dockind = DocumentKind.findByCn(documentKindCn);
                	String openIn = dockind.getProperties().get(DocumentKind.OPEN_TAB_IN_OFFICE);
                    if (document.getType() == DocumentType.INCOMING)
                    {
                    	
                    	if( "document-archive".equals(openIn))
                    	{
                    		officeLink = "/office/incoming/document-archive.action?documentId="+id;
                    	}
                    	else
                    	{
                    		officeLink = "/office/incoming/summary.action?documentId="+id;
                    	}
                    }
                    else if (document.getType() == DocumentType.OUTGOING)
                    {
                    	if( "document-archive".equals(openIn))
                    	{
                    		officeLink = "/office/outgoing/document-archive.action?documentId="+id;
                    	}
                    	else
                    	{
                    		officeLink = "/office/outgoing/summary.action?documentId="+id;
                    	}
                    }
                    else if (document.getType() == DocumentType.INTERNAL)
                    {
                    	if( "document-archive".equals(openIn))
                    	{
                    		officeLink = "/office/internal/document-archive.action?documentId="+id;
                    	}
                    	else
                    	{
                    		officeLink = "/office/internal/summary.action?documentId="+id;
                    	}
                    }
                }
                
                if(officeLink != null && officeLink.length() > 0)
                {
                	przywrocLink = officeLink+"&doReopenWf=true";
                }
                
                setProcessRenderBeans(ProcessUtils.renderAllProcesses(document, ProcessActionContext.ARCHIVE_VIEW));

                TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
                
                try
                {
                	officeTask = new ArrayList<Task>(taskList.getTasks(DSApi.context().getDSUser(),id));
                }
                catch(EdmException e)
                {
                	
                }
                documentAuthor = DSUser.safeToFirstnameLastnameName(document.getAuthor());
                folderLink = ExploreDocumentsAction.getLink(document.getFolderId());
                folderLink+="#"+Folder.findDestAnchor(Folder.find(folderId)).getId() ;
                permissionsLink = SetPermissionsAction.getLink(
                    ServletActionContext.getRequest(), "document", document.getId());
                sendLinkLink = SendLinkAction.getLink(document.getId());
                lockDocumentLink = LockDocumentAction.getLink(document.getId());
                
                String _documentKind = document.getDocumentKind().getCn();
//                    if (_documentKind.equals(DocumentKind.ALD_KIND) || _documentKind.equals(DocumentKind.ALD_UMOWY_KIND))
                		canAddCorrectingNotes = false;
//                    else
//                    	canAddCorrectingNotes = true;

                blocked = (document.getBlocked() != null && document.getBlocked());
                /* podpisy */
                if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                {
                    List<DocumentSignature> signatures = DocumentSignature.findByDocumentId(document.getId()/*, false*/);

                    class mapper implements lambda<DocumentSignature, Map>
                    {
                        public Map act(DocumentSignature signature)
                        {
                            BeanBackedMap result = new BeanBackedMap(signature,
                                "id", "ctime", "correctlyVerified");
                            try
                            {
                                result.put("author", DSUser.findByUsername(signature.getAuthor()));
                            }
                            catch (EdmException e)
                            {
                            }

                            return result;
                        }
                    }

                    final mapper mapper = new mapper();
                    signatureBeans = fun.map(signatures, mapper);
                }
                
                /* flagi */
                String flagsOn = Configuration.getProperty("flags");
                if ("true".equals(flagsOn) && AvailabilityManager.isAvailable("document.archive.flags"))
                {
                    globalFlags = document.getFlags().getDocumentFlags(false);
                    userFlags = document.getFlags().getDocumentFlags(true);
                    flagsPresent = globalFlags.size() > 0 || userFlags.size() > 0;
                }
                else
                    flagsPresent = false;
                

                /* za��czniki */
                List<Attachment> attachmentList = OfficeDocumentHelper.filterAttachments(document, document.listAttachments());

                attachments = new ArrayList<Map<String, Object>>(attachmentList.size());
                for (Iterator iter=attachmentList.iterator(); iter.hasNext(); )
                {
                    Attachment attachment = (Attachment) iter.next();
                    // obiekt attachment mo�e by� r�wny null np. w�wczas, gdy
                    // w numeracji p�l DS_ATTACHMENT.POSN znajdzie si�
                    // przerwa
                    if (attachment == null)
                    {
                        event.getLog().warn("attachment=null, document="+document);
                        continue;
                    }

                    Map<String, Object> bean = new HashMap<String, Object>();
                    bean.put("id", attachment.getId());
                    bean.put("title", attachment.getTitle());
                    bean.put("cn", attachment.getCn());
	                bean.put("editable", attachment.isEditable());
                    bean.put("editLink", EditAttachmentAction.getLink(attachment.getId())+"&binderId="+binderId);

                    Object[] revs = attachment.getRevisions().toArray();
                    if (revs != null && revs.length > 0)
                    {
                        AttachmentRevision rev = (AttachmentRevision) revs[revs.length-1];
                        bean.put("recentContentLink",
                            ViewAttachmentRevisionAction.getLink(
                                ServletActionContext.getRequest(), rev.getId())+"&binderId="+binderId);
                        bean.put("ctime", rev.getCtime());
                        bean.put("size", rev.getSize());
                        bean.put("revision", rev.getRevision());
                        bean.put("mime", rev.getMime());
                        bean.put("icon", MimetypesFileTypeMap.getInstance().getIcon(
                            MimetypesFileTypeMap.getInstance().getContentType(
                                rev.getOriginalFilename())));
                        bean.put("author", DSUser.safeToFirstnameLastnameName(rev.getAuthor()));

                        bean.put("showRecentContentLink", "/viewserver/viewer.action?id="+rev.getId());
                        bean.put("revisionId", rev.getId());
                    }
                    else
                    {
                        bean.put("noContent", Boolean.TRUE);
                    }

                    // ponizsze na potrzeby "zalacznikow-maili"
                    if ("mail".equals(attachment.getCn()))
                    {
                        bean.put("ctime", attachment.getCtime());
                        bean.put("author", DSUser.safeToFirstnameLastnameName(attachment.getAuthor()));
                        bean.put("wparam", attachment.getWparam());
                    }

                    attachments.add(bean);
                }

                canSetPermissions = DSApi.context().getSecurityHelper().canSetPermissions(document);

                if (document.canLock(DocumentLockMode.WRITE))
                {
                    canLock = true;
                    Calendar cal = new GregorianCalendar();
                    cal.add(Calendar.DATE, 1);
                    lockExpiration = DateUtils.formatJsDateTime(cal.getTime());
                }

                DocumentLock lock = document.getWriteLock();

                if (lock != null)
                {
                    lockedThru = lock.getExpiration();
                    lockedBy = DSUser.safeToFirstnameLastname(lock.getUsername());
                    canUnlock = lock.getUsername().equals(DSApi.context().getPrincipalName());
                }

                document.eventDocumentView();

                if (document.getFieldsManager().getField("STATUS") != null) {
                    status = (String) document.getFieldsManager().getEnumItemCn("STATUS");
                    if (StringUtils.isNotBlank(status) && status.equals("accepted")) {
                        templates = document.getDocumentKind().getDockindInfo().getTemplates();
                    }
                }
            }
            catch (Exception e) {
				log.error(e.getMessage(), e);
			}
        }
    }

    private class JsonView extends AjaxSerializeActionListener {

        @Override
        public Object serializeAction(ActionEvent event) throws Exception {
            Map<String, Object> ret = new HashMap<String, Object>();

            ret.put("document", new DocumentView(document));

            return ret;
        }
    }


	private void disabledAllButtons()
	{
		canLock = false;
		canUnlock = false;
		canUnarchive = false;
		canGenerateDocumentView = false;
		canChangeDockind = false;
		canClone = false;
		canUndelete = false;
		canDelete = false;
		canUpdate = false;
		canModifyAttachments = false;
		canReadAttachments = false;
		canReadOrginalAttachments = false;
		canDeleteAttachments = false;
		canSetPermissions = false;
		canUndelete = false;
		disabledAllButtons =true;
		officeLink=null;
	}
    
	

	
	

	protected void dockindFillForm(boolean changeDockind) throws EdmException
    {
        List<DocumentKind> docKinds = DocumentKind.listForEdit();
        documentKinds = new LinkedHashMap<String,String>();
        DocumentKind documentKind = null;

        for (DocumentKind docKind : docKinds)
        {
            documentKinds.put(docKind.getCn(), docKind.getName());
        }
        if (documentKindCn == null)
        {
            documentKind = document.getDocumentKind() != null ? document.getDocumentKind() :
                DocumentKind.findByCn(DocumentKind.getDefaultKind());
            documentKindCn = documentKind.getCn();
        }
        else
            documentKind = DocumentKind.findByCn(documentKindCn);       
        
        canReadDictionaries = documentKind.logic().canReadDocumentDictionaries(); 
        // pobranie warto�ci dla danego dokumentu
        fm = documentKind.getFieldsManager(document.getId());
        fm.initialize();
        fm.initializeAcceptances();

        
        documentAspects = documentKind.getAspectsForUser(DSApi.context().getPrincipalName());
        if(documentAspects == null)
        {
        	documentAspectCn = null;
        }
        else if(documentAspects != null && documentAspectCn == null)
        {
        	documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
        }
        
        //sprawdzanie czy mo�na czyta� s�owniki na stronie JSP
        canReadDictionaries = documentKind.logic().canReadDocumentDictionaries();       
        
        // rzeczy zwi�zane z rodzajem dokumentu 'daa'
        if (DocumentLogicLoader.DAA_KIND.equals(documentKind.getCn()))
        {                        
            // sprawdzam czy dokument juz przypisany - jesli to tak, to nie bedzie mozna zrobic go "nieprzypisanym" 
            if ((fm.getValue(DaaLogic.AGENCJA_FIELD_CN) != null) || (fm.getValue(DaaLogic.AGENT_FIELD_CN) != null) 
                    || DaaLogic.RAPORTY_CN.equals(fm.getEnumItemCn(DaaLogic.KATEGORIA_FIELD_CN)))
                daaAssigned = true;
        }
        
        // gdy zmieniamy typ (i dokument nie jest w�a�nie tego typu), to ustalam warto�ci pocz�tkowe
        
        if (changeDockind && (document.getDocumentKind() == null || !documentKindCn.equals(document.getDocumentKind().getCn())))
            documentKind.logic().setInitialValues(fm, DocumentLogic.TYPE_ARCHIVE);
        
        // jak byl blad, to powracamy na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
        if (hasActionErrors())
            fm.reloadValues(values);
    }

    private class Unarchive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            try
            {
                DSApi.context().begin();
                document = Document.find(id);

                if (document.getDocumentKind() != null && !DocumentKind.NORMAL_KIND.equals(document.getDocumentKind().getCn()))
                {
                    documentKindCn = DocumentKind.NORMAL_KIND;
                    document.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));

                    document.getDocumentKind().logic().archiveActions(document, DocumentKindsManager.getType(document));
                    document.getDocumentKind().logic().documentPermissions(document);

                    if (document instanceof OfficeDocument)
                    {
                        document.changelog(DocumentChangelog.UNARCHIVE_DOCUMENT);
                        ((OfficeDocument) document).addWorkHistoryEntry(Audit.create("::nw_archive", DSApi.context().getPrincipalName(),
                                smL.getString("CofnietoArchiwizacjeDokumentu")));

                         TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
        }
    }

    private class UpdateBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            if (id == null)
                return;

            if (hasActionErrors())
                return;
            try
            {
	            DSApi.context().begin();
	            officeDocumentHelper.updateBox(EditDockindDocumentAction.this);
	            DSApi.context().commit();
	            addActionMessage(sm.getString("ZapisanoNumerPudlaArchiwalnego"));
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
			}
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            if (id == null)
                return;
            
            description = TextUtils.trimmedStringOrNull(description);

            if (title == null)
                addActionError(smL.getString("NiePodanoTytuluDokumentu"));
            else if (title.length() > 254)
                addActionError(smL.getString("MaksymalnaDlugoscTytuluDokumentuTo254Znaki"));

            if (description == null)
                addActionError(smL.getString("NiePodanoOpisuDokumentu"));
            else if (description.length() > 510)
                addActionError(smL.getString("MaksymalnaDlugoscOpisuDokumentuTo510Znakow"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
                documentKind.initialize();
                
                documentKind.logic().correctValues(values, documentKind);
                documentKind.logic().validate(values, documentKind, document.getId());
                documentKind.logic().validateAcceptances(values, documentKind, id);
                
                document.setTitle(title);
                document.setDescription(description);                                

                boolean setFolderManually = DocumentKind.NORMAL_KIND.equals(documentKind.getCn()) && (manualFolder || documentKind.getCn().equals(document.getDocumentKind().getCn()))
                 && !document.isDeleted() && folderId != null;
                
                boolean dockindChanged = !document.getDocumentKind().getCn().equals(documentKindCn);
                document.setDocumentKind(documentKind);

                documentKind.logic().checkRemarkField(document, values, remark);
                
                if (dockindChanged)
                    documentKind.setWithHistory(document.getId(), values, /*changeAll*/ true);
                else
                    documentKind.setWithHistory(document.getId(), values, /*changeAll*/ false);

                documentKind.logic().archiveActions(document, DocumentKindsManager.getType(document));
                documentKind.logic().documentPermissions(document);   

                // je�li r�cznie wybrano folder to nadpisuje folder ustalony przez archiwizacj�
                if (setFolderManually && !folderId.equals(document.getFolderId()))
                {
                    // FolderNotFoundException
                    document.setFolder(Folder.find(folderId));
                }

                AcceptancesManager.checkFinalAcceptance(document);
                setDocument(document);
                officeDocumentHelper.updateBox(EditDockindDocumentAction.this);
                
                if (document instanceof OfficeDocument)
                {
                    TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                }

                String flagsOn = Configuration.getProperty("flags");
                if (!"true".equals(flagsOn) || !AvailabilityManager.isAvailable("document.archive.flags"))
                {
                	event.skip(EV_UPDATE_FLAGS);
                }
                
                DSApi.context().commit();

                addActionMessage(smL.getString("ZapisanoZmiany"));
                
                // jak akcja sko�czy�a si� pomy�lnie to na pewno domy�lnie nie mo�e pojawi� si� pole z uwag�
                remark = null;
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                
                
                // XXX: nie mo�na zamieni� na document.canModifyAttachments(),
                // bo ta funkcja pozwala na wszystko dokumentom kancelaryjnym
                if (!DSApi.context().hasObjectPermission(document, ObjectPermission.MODIFY_ATTACHMENTS))
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZalacznika"));
                
                String remark = "Za��cznik zosta� usuni�ty.";

                for (int i=0; ids != null && i < ids.length; i++)
                {
                    Attachment attachment = document.getAttachment(ids[i]);
                    if (attachment != null)
                    {
                    	if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym"))document.getAttachments().remove(attachment);
                        attachment.delete();
//                        remark += attachment.getTitle() + " (" + attachment.getId() + ")";
//                        if (i + 1 < ids.length)
//                        	remark += ", ";
                    }
                }
                
                if (ids != null && ids.length > 0 && createRemark)
                	document.addRemark(new Remark(remark, DSApi.context().getPrincipalName()));
                
                if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym")&&document instanceof InOfficeDocument){
                    ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
                }

                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(document.getId());
                }

                DSApi.context().commit();
                DSApi.context().session().clear();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteAttachmentsPermanently implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                // XXX: nie mo�na zamieni� na document.canModifyAttachments(),
                // bo ta funkcja pozwala na wszystko dokumentom kancelaryjnym
                if (!DSApi.context().hasObjectPermission(document, ObjectPermission.MODIFY_ATTACHMENTS))
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZalacznika"));

                for (int i=0; ids != null && i < ids.length; i++)
                {
                	if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym"))document.getAttachments().remove(Attachment.find(ids[i]));
                    document.scrubAttachment(ids[i]);
                }
                if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym")&&document instanceof InOfficeDocument){
                    ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
                }

                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(document.getId());
                }

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (document instanceof OfficeDocument && !(DSApi.context().hasPermission(DSPermission.DOKUMENT_USUN_GLOBALNA)))
                    throw new EdmException(sm.getString("NieMoznaUsunacTegoDokumentuPoniewazJest")+" " +
                        sm.getString("OnDokumentemKancelaryjnym"));
                document.delete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("explore-documents"); // oczekuje parametru folderId
                event.skip(EV_FILL);
            }
        }
    }

    private class PermanentlyDelete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                document.scrubAttachments();
                document.delete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("explore-documents"); // oczekuje parametru folderId
            }
        }
    }

    private class Undelete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                document.undelete();

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("explore-documents"); // oczekuje parametru folderId
            }
        }
    }

    private class Unlock implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document.find(id).unlock();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class UnlockRequest implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            unlockReason = TextUtils.trimmedStringOrNull(unlockReason);
            if (unlockReason == null)
                addActionError(sm.getString("NiePodanoPrzyczynyProsbyOodblokowanieDokumentu"));

            if (hasActionErrors())
                return;

            String email = null;
            String senderName = null;
            try
            {
                senderName = DSApi.context().getDSUser().asFirstnameLastname();
                Document.find(id);

                // metoda find rzuca wyj�tek DocumentLockedException,
                // z kt�rego mo�na odczyta� nazw� u�ytkownika blokuj�cego dokument

                // je�eli wykonywanie dosz�o tutaj, oznacza to, �e dokument nie
                // jest ju� zablokowany
                return;
            }
            catch (DocumentLockedException e)
            {
                // odczytanie nazwy i adresu u�ytkownika blokuj�cego dokument
                try
                {
                    DSUser user = DSUser.findByUsername(e.getUsername());

                    if (StringUtils.isEmpty(user.getEmail()))
                        throw new UserNotFoundException(user.getName());

                    email = user.getEmail();
                }
                catch (UserNotFoundException unfe)
                {
                    // je�eli nie znaleziono u�ytkownika zak�adaj�cego blokad�,
                    // pro�ba wysy�ana jest do admina
                    try
                    {

                        DSUser admin = DSUser.findByUsername(GlobalPreferences.getAdminUsername());

                        if (!StringUtils.isEmpty(admin.getEmail()))
                        {
                            email = admin.getEmail();
                            addActionMessage(sm.getString("WyslanoProsbeOodblokowanieDoAdministratora"));
                        }
                        else
                        {
                            addActionError(sm.getString("NieMoznaWyslacProsbyOodblokowanieAdministratorNie")+" " +
                                sm.getString("MaAdresuEmail"));
                        }
                    }
                    catch (EdmException ee)
                    {
                        addActionError(e.getMessage());
                    }
                }
                catch (EdmException ee)
                {
                    addActionError(e.getMessage());
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }

            if (email != null)
            {
                Map<String, Object> context = new HashMap<String, Object>();

                context.put("documentUrl", Configuration.getBaseUrl() +
                    "/repository/edit-document.action?id="+id);
                context.put("sender", senderName);
                context.put("reason", unlockReason);

                try
                {
                    ((Mailer) ServiceManager.getService(Mailer.NAME)).
                        send(email, null, null, Configuration.getMail(Mail.DOCUMENT_UNLOCK_REQUEST), context);
                }
                catch (Exception e)
                {
                    addActionError(e.getMessage());
                }

                addActionMessage(sm.getString("WyslanoProsbeOodblokowanieDokumentu"));
                unlockReason = null;
            }
        }
    }

    private class ToFavourites implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean success = false;
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                if (!document.hasWatch(DocumentWatch.Type.DOCUMENT_MODIFIED))
                    document.addWatch(DocumentWatch.Type.DOCUMENT_MODIFIED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_DELETED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_DELETED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_ADDED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_ADDED);
                if (!document.hasWatch(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED))
                    document.addWatch(DocumentWatch.Type.ATTACHMENT_REVISION_ADDED);

                DSApi.context().commit();
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

            if (success)
            {
                event.setResult("favourites");
            }
        }
    }

    private class ToBookmarks implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                BookmarksAction.addDocument(document);

                DSApi.context().commit();

                event.addActionMessage(smL.getString("DodanoDokumentDoUlubionych"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
                
            }
        }
    }
    
	/*public class SelfAssignImportedDocument implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				getValues();
				getDocument().getDocumentKind();
				getDocument().getId();

				OfficeDocument doc = OfficeDocument.findOfficeDocument(getDocument().getId(), true);
				if (doc instanceof InOfficeDocument && ((InOfficeDocument) doc).getDelivery().getName().equals(InOfficeDocumentDelivery.IMPORTED_DOCUMENT))
				{
					((InOfficeDocument) doc).setDelivery(InOfficeDocumentDelivery.findByName("Poczta"));
										Map<String, Object> params = Maps.newHashMap();
					params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do Realizacji zeskanowanego dokumentu");
					params.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.MANUAL_PROCESS_NAME);
					for (String aid : Jbpm4ProcessLocator.taskIds(doc.getId()))
					{
						TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(aid.replaceAll("[^,]*,", ""));
						if (taskSnapshot != null && taskSnapshot.getProcess().equals("jbpm4"))
							Jbpm4WorkflowFactory.reassign(aid, doc.getId(), doc.getAuthor(), DSApi.context().getDSUser().getName(), params);
					}
					
					TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());
					takePackageConfirmation = true;
					event.addActionMessage("Przyjeto Dokument - dodano do listy zada� u�ytkownika " + DSApi.context().getDSUser().asFirstnameLastname());
					DSApi.context().commit();

				}
			} catch (EdmException e)
			{
				DSApi.context().setRollbackOnly();
				event.addActionError(e.getMessage());
				log.error("", e);
			}
		}

	}*/
	private class ConfirmTakePackage implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();

				getValues();
				getDocument().getDocumentKind();
				getDocument().getId();
				document.getDocumentKind().logic().correctValues(getValues(), getDocument().getDocumentKind(), getDocument().getId());
				DSApi.context().commit();
				DSUser userOdb = null;
				for(Recipient rec : ((OfficeDocument)document).getRecipients()){
					userOdb = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
				}
				fm = document.getFieldsManager();
				if (userOdb!=null)
				{ 
					
					if (DSApi.context().getDSUser().getId().equals(userOdb.getId()))
					{
						event.addActionMessage("Przyjeto przesy�k� - dodano do listy zada� u�ytkownika " + userOdb.asFirstnameLastname());
					} else
					{
						event.addActionMessage("Przyjeto przesy�k� do dostarczenia");
					}
				} else if (fm.getFieldValues() != null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) != null
						&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) == true))
				{
					DSDivision div = DSDivision.find(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
					if (DSApi.context().getDSUser().inDivisionByGuid(div.getGuid()))
					{
						event.addActionMessage("Przyjeto przesy�k� - dodano do listy zada� u�ytkownika " + DSApi.context().getDSUser().asFirstnameLastname());
					} else
					{
						event.addActionMessage("Przyjeto przesy�k� do dostarczenia");
					}

				}
				takePackageConfirmation = true;


			} catch (EdmException e)
			{
				DSApi.context().setRollbackOnly();
				event.addActionError(e.getMessage());
			}
		}
	}

    
	
	private class CompileAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                document = Document.find(id);
                CompileAttachmentsManager.compile(document);
                
                DSApi.context().commit();

                addActionMessage(sm.getString("SkompilowanoWszystkiePlikiSprawyDoZalacznika"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                try {DSApi.context().rollback(); } catch (EdmException f) {};
            }
        }
    }
    
    public class GiveAcceptance implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {            
            if (hasActionErrors())
                return;
            
            try
            {
                DSApi.context().begin();
                
                if (acceptanceCn != null)
                    AcceptancesManager.giveAcceptance(acceptanceCn, id);
                else
                    AcceptancesManager.giveCentrumAcceptance(centrumAcceptanceCn, id);
                
                DSApi.context().commit();
                addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    public class WithdrawAcceptance implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {            
            if (hasActionErrors())
                return;
            
            try
            {
                DSApi.context().begin();
                
                if (objectId != null)
                    AcceptancesManager.withdrawCentrumAcceptance(id, objectId);
                else
                    AcceptancesManager.withdrawAcceptance(id, acceptanceCn);
                
                DSApi.context().commit();
                addActionMessage(sm.getString("WycofanoAkceptacjeDokumentu"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }    

    private class GenerateDocumentView implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {    
            File pdf = null;
            try
            {
                Document document = Document.find(id);
                pdf = document.getDocumentKind().logic().generateDocumentView(document);
                
                if (pdf != null && pdf.exists())
                {
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("rozmiar pliku (obrazu dokumentu) "+pdf+": "+pdf.length());
                    try
                    {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdf),
                            "application/pdf", (int) pdf.length());
                    }
                    catch (IOException e)
                    {
                        event.getLog().error("", e);
                    }
                    finally
                    {
                        pdf.delete();
                    }
                }
                event.setResult(NONE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class UpdateFlags implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if(hasActionErrors()){
                return;
            }
            try
            {
                DSApi.context().begin();
                Long documentId = id;
                Document document = Document.find(id);
                List<Long> ids;
                if(globalFlag!=null)
                {
		            for (Long id : globalFlag)
		            {
		                if(!LabelsManager.existsDocumentToLabelBean(documentId, id)) LabelsManager.addLabel(documentId, id, DSApi.context().getPrincipalName());
		            }
		            globalFlags = document.getFlags().getDocumentFlags(false);
		            ids = Arrays.asList(globalFlag);
		            for(Flags.Flag f: globalFlags)
		            {
		            	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(documentId, f.getId()) && f.isCanClear()) LabelsManager.removeLabel(documentId, f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(false))
		            {
		            	if(LabelsManager.existsDocumentToLabelBean(documentId, f.getId()) && f.isCanClear()) LabelsManager.removeLabel(documentId, f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                if(userFlag!=null)
                {
	                for (Long id :userFlag)
	                {
	                    if(!LabelsManager.existsDocumentToLabelBean(documentId, id)) LabelsManager.addLabel(documentId, id, DSApi.context().getPrincipalName());
	                }
	                userFlags = document.getFlags().getDocumentFlags(true);
	                ids = Arrays.asList(userFlag);
	                for(Flags.Flag f: userFlags)
	                {
	                	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(documentId, f.getId())) LabelsManager.removeLabel(documentId, f.getId(),DSApi.context().getPrincipalName());
	                }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(true))
 	                {
 	                	if( LabelsManager.existsDocumentToLabelBean(documentId, f.getId())) LabelsManager.removeLabel(documentId, f.getId(),DSApi.context().getPrincipalName());
 	                }
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    
    public FieldsManager getFm()
    {
        return fm;
    }

	public boolean checkDocIsImportedDocument()
	{
		OfficeDocument doc;
		try
		{
			doc = OfficeDocument.findOfficeDocument(getDocument().getId(), true);
			importedDocument = (doc != null && doc instanceof InOfficeDocument && ((InOfficeDocument) doc).getDelivery() != null && ((InOfficeDocument) doc)
					.getDelivery().getName().equals(InOfficeDocumentDelivery.IMPORTED_DOCUMENT));
			return importedDocument;
		} catch (DocumentNotFoundException e)
		{
			log.error("blad przy sprawdzaniu rodzaju przesylkii ", e);
			return false;


		} catch (EdmException e)
		{
			log.equals("");
			return false;

		}
	}


	public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds()
    {
        return documentKinds;
    }

    public String getFolderPrettyPath()
    {
        return folderPrettyPath;
    }

    public String getQueryLink()
    {
        return queryLink;
    }

    public void setQueryLink(String queryLink)
    {
        this.queryLink = queryLink;
    }

    public String getAddAttachmentLink()
    {
        return addAttachmentLink;
    }

    public String getDocumentAuthor()
    {
        return documentAuthor;
    }

    public String getFolderLink()
    {
        return folderLink;
    }

    public String getPermissionsLink()
    {
        return permissionsLink;
    }

    public String getSendLinkLink()
    {
        return sendLinkLink;
    }

    public String getLockDocumentLink()
    {
        return lockDocumentLink;
    }

    public boolean isCanSetPermissions()
    {
        return canSetPermissions;
    }

    public boolean isCanLock()
    {
        return canLock;
    }

    public boolean isCanUnlock()
    {
        return canUnlock;
    }

    public Date getLockedThru()
    {
        return lockedThru;
    }

    public String getLockedBy()
    {
        return lockedBy;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long[] getIds()
    {
        return ids;
    }

    public void setIds(Long[] ids)
    {
        this.ids = ids;
    }

    public Long getFolderId()
    {
        return folderId;
    }

    public void setFolderId(Long folderId)
    {
        this.folderId = folderId;
    }

    public String getUnlockReason()
    {
        return unlockReason;
    }

    public void setUnlockReason(String unlockReason)
    {
        this.unlockReason = unlockReason;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getLockExpiration()
    {
        return lockExpiration;
    }

    public void setLockExpiration(String lockExpiration)
    {
        this.lockExpiration = lockExpiration;
    }

	public Document getDocument() {
		try{
			if(document == null) document = Document.find(getDocumentId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return document;
	}

	public List getAttachments()
    {
        return attachments;
    }

    public boolean isCanUndelete()
    {
        return canUndelete;
    }

    public String getOfficeLink()
    {
        return officeLink;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanUpdate()
    {
        return canUpdate;
    }

    public boolean isCanClone()
    {
        return canClone;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }

    public String getCurrentBoxNumber()
    {
        return currentBoxNumber;
    }

    public Long getBoxId()
    {
        return boxId;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public Long getCurrentBoxId()
    {
        return currentBoxId;
    }

    public boolean isCanReadAttachments()
    {
        return canReadAttachments;
    }

    public boolean isCanModifyAttachments()
    {
        return canModifyAttachments;
    }

    public boolean isCanUnarchive()
    {
        return canUnarchive;
    }

    public boolean isBoxNumberReadonly()
    {
        return boxNumberReadonly;
    }
    
	public void setBoxNumberReadonly(boolean boxNumberReadonly) {
		this.boxNumberReadonly = boxNumberReadonly;
	}  

    public boolean isCanAddToRS()
    {
        return canAddToRS;
    }

    public List<DrsInfo> getDrsDocs()
    {
        return drsDocs;
    }    

    public Boolean getBlocked()
    {
        return blocked;
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public boolean isCanSign()
    {
    	return canSign;
    }

    public Collection<Map> getSignatureBeans()
    {
        return signatureBeans;
    }

    public CompilationStatus getCompilationStatus()
    {
        return compilationStatus;
    }

    public boolean isDaaAssigned()
    {
        return daaAssigned;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public boolean isCanChangeDockind()
    {
        return canChangeDockind;
    }

    public boolean isCanRead()
    {
        return canRead;
    }

    public void setManualFolder(boolean manualFolder)
    {
        this.manualFolder = manualFolder;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public boolean isCanGenerateDocumentView()
    {
        return canGenerateDocumentView;
    }

    public void setAcceptanceCn(String acceptanceCn)
    {
        this.acceptanceCn = acceptanceCn;
    }
    
    public Map<Integer, String> getCentrumAcceptanceCn()
    {
        return centrumAcceptanceCn;
    }

    public void setCentrumAcceptanceCn(Map<Integer, String> centrumAcceptanceCn)
    {
        this.centrumAcceptanceCn = centrumAcceptanceCn;
    }

    public void setObjectId(Long objectId)
    {
        this.objectId = objectId;
    }  
    
    public String getDockindAction()
    {
        return "edit";
    }

    public boolean isAcceptancesPage()
    {
        return true;
    }

    public Date getDateZam()
    {
        return dateZam;
    }

    public void setDateZam(Date dateZam)
    {
        this.dateZam = dateZam;
    }

    public Long getIdBox()
    {
        return idBox;
    }

    public void setIdBox(Long idBox)
    {
        this.idBox = idBox;
    }

    public Long getIdU()
    {
        return idU;
    }

    public void setIdU(Long idU)
    {
        this.idU = idU;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public boolean isHist()
    {
        return hist;
    }

    public void setHist(boolean hist)
    {
        this.hist = hist;
    }

    public boolean isBlokada()
    {
        return blokada;
    }

    public void setBlokada(boolean blokada)
    {
        this.blokada = blokada;
    }

    public boolean isObsluga()
    {
        return obsluga;
    }

    public void setObsluga(boolean obsluga)
    {
        this.obsluga = obsluga;
    }

    public boolean isZamawianie()
    {
        return zamawianie;
    }

    public void setZamawianie(boolean zamawianie)
    {
        this.zamawianie = zamawianie;
    }

	public boolean isCanAddCorrectingNotes() {
		return canAddCorrectingNotes;
	}

	public List<Task> getOfficeTask() {
		return officeTask;
	}

	public void setOfficeTask(List<Task> officeTask) {
		this.officeTask = officeTask;
	}

	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}
	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}

	public String getDocumentAspectCn() {
		return documentAspectCn;
	}

	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}

	public void setBinderId(Long binderId) {
		this.binderId = binderId;
	}

	public Long getBinderId() {
		return binderId;
	}

	public boolean isPudloModify() {
		return pudloModify;
	}

	public void setPudloModify(boolean pudloModify) {
		this.pudloModify = pudloModify;
	}

	public boolean isUsunZaznaczoneZalaczniki() {
		return usunZaznaczoneZalaczniki;
	}

	public void setUsunZaznaczoneZalaczniki(boolean usunZaznaczoneZalaczniki) {
		this.usunZaznaczoneZalaczniki = usunZaznaczoneZalaczniki;
	}

	public boolean isTrwaleUsunZaznaczoneZalaczniki() {
		return trwaleUsunZaznaczoneZalaczniki;
	}

	public void setTrwaleUsunZaznaczoneZalaczniki(
			boolean trwaleUsunZaznaczoneZalaczniki) {
		this.trwaleUsunZaznaczoneZalaczniki = trwaleUsunZaznaczoneZalaczniki;
	}

	public String getPrzywrocLink()
	{
		return przywrocLink;
	}

	public void setPrzywrocLink(String przywrocLink)
	{
		this.przywrocLink = przywrocLink;
	}

	public boolean isPopup() {
		return popup;
	}

	public void setPopup(boolean popup) {
		this.popup = popup;
	}

	public List<Flags.Flag> getGlobalFlags()
	{
		return globalFlags;
	}

	public void setGlobalFlags(List<Flags.Flag> globalFlags)
	{
		this.globalFlags = globalFlags;
	}

	public List<Flags.Flag> getUserFlags()
	{
		return userFlags;
	}

	public void setUserFlags(List<Flags.Flag> userFlags)
	{
		this.userFlags = userFlags;
	}

	public boolean isFlagsPresent()
	{
		return flagsPresent;
	}

	public void setFlagsPresent(boolean flagsPresent)
	{
		this.flagsPresent = flagsPresent;
	}

	public Long[] getGlobalFlag()
	{
		return globalFlag;
	}

	public void setGlobalFlag(Long[] globalFlag)
	{
		this.globalFlag = globalFlag;
	}

	public Long[] getUserFlag()
	{
		return userFlag;
	}

	public void setUserFlag(Long[] userFlag)
	{
		this.userFlag = userFlag;
	}

    public boolean isBoxActionAvailable() {
        return AvailabilityManager.isAvailable("repository.pudlo", getDocumentKindCn());
    }

    public boolean isNeedsNotBox()
	{
		return Boolean.TRUE.equals(needsNotBox);
	}

	public void setCurrentBoxId(Long currentBoxId) 
	{
		this.currentBoxId = currentBoxId;
	}

	public void setCurrentBoxNumber(String currentBoxNumber) 
	{
		this.currentBoxNumber = currentBoxNumber;
	}

	public void setNeedsNotBox(Boolean isneed)
	{
		this.needsNotBox = isneed;
		
	}
	
	public String getDockindEventValue() 
	{
		return dockindEventValue;
	}

	public String getPlace()
	{
		return DockindButtonField.EDIT_DOCKIND_DOCUMENT;
	}

	public void setDockindEventValue(String dockindEventValue) 
	{
		this.dockindEventValue = dockindEventValue;
	} 
	
	public boolean isCanDeleteAttachments()
	{
		return canDeleteAttachments;
	}

    public boolean isOfficeLinksEnabled() {
        return officeLinksEnabled;
    }

    public void setDocumentId(Long documentId) {
		this.id = documentId;
		
	}

	public int getDocType() {
		return DocumentLogic.TYPE_ARCHIVE;
	}

	public void setDocumentKinds(Map<String, String> documentKinds) {
		this.documentKinds = documentKinds;
		
	}

	public Map<String, Object> getDockindKeys() {
		return dockindKeys;
	}

	public void setDockindKeys(Map<String, Object> dockindKeys) {
		this.dockindKeys = dockindKeys;		
	}

	public void setDocument(Document document) {
		this.document = document;
		
	}

	public void setBeforeUpdate(Map<String, Object> dockindKeys) 
	{
		// TODO Auto-generated method stub
		
	}

	public Logger getLog() {
		return log;
	}

	public Document getNewDocument() {
		return null;
	}

	public Long getDocumentId() {
		return this.id;
	}


    public String getProcessAction() {
        return processAction;
    }

    public void setProcessAction(String processAction) {
        this.processAction = processAction;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

	
	public void setCreateRemark(Boolean createRemark)
	{
		this.createRemark = createRemark;
	}
	
	public void setTemplates(Map<String, String> templates) {
		this.templates = templates;
	}
    
	public Map<String, String> getTemplates() {
		return templates;
	}
        /**        ---------- FUNKCJA DO ODFILTROWANIA UZYTKOWNIKOW W RAZIE POTRZEBY
     * potrzebny <available name="archiwizacja.dokument.updatujListeUzytkownika" value="true"/> w xml
     */
        
   /**
     * funkcja potrzebna do modyfikowania listy uzytkownik�w
     * @return
     * @throws EdmException 
     */
    public List<DSUser> getUsersByDocumentToUpdate() throws EdmException
    {
        try
        {
//            List<DSUser> usersByDocumentToUpdate = new ArrayList<DSUser>();
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            
            if(documentKindCn.equals("overtime_app"))
                usersByDocumentToUpdate = DSUser.listAcceptanceSubordinates(document.getAuthor());
            
        } catch (EdmException e){
            log.error("",e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();

        } finally{
            try{
                DSApi.close();
            } catch (EdmException e){}
        }

        return usersByDocumentToUpdate;
    }
    
    public String getUsersDockindFieldUpdate()
    {
        log.error(documentKindCn);
        
        if(documentKindCn.equals("overtime_app"))
            return "DS_USER";
        
        return "DS_USER";
    }

	public boolean isCanReadOrginalAttachments() {
		return canReadOrginalAttachments;
	}

	public void setCanReadOrginalAttachments(boolean canReadOrginalAttachments) {
		this.canReadOrginalAttachments = canReadOrginalAttachments;
	}
	public boolean isTakePackageConfirmation()
	{
		return takePackageConfirmation;
	}

	
	public void setTakePackageConfirmation(boolean takePackageConfirmation)
	{
		this.takePackageConfirmation = takePackageConfirmation;
	}
	public boolean isDisabledAllButtons()
	{
		return disabledAllButtons;
	}
	public void setDisabledAllButtons(boolean disabledAllButtons)
	{
		this.disabledAllButtons = disabledAllButtons;
	}

	
	public boolean isImportedDocument()
	{
		return importedDocument;
	}

	
	public void setImportedDocument(boolean importedDocument)
	{
		this.importedDocument = importedDocument;
	}

	
	public boolean isPackageKind()
	{
		return packageKind;
	}

	
	public void setPackageKind(boolean packageKind)
	{
		this.packageKind = packageKind;
	}

	
	

	
}
