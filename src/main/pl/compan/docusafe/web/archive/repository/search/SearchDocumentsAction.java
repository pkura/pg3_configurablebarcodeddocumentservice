package pl.compan.docusafe.web.archive.repository.search;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.security.AccessControlException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.gson.Gson;

import net.sourceforge.barbecue.Module;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.json.JSONArray;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserView;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.archive.repository.Constants;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.webwork.event.*;

import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2005-05-30 15:32:52 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchDocumentsAction.java,v 1.45 2010/02/23 07:32:14 mariuszk Exp $
 */
public class SearchDocumentsAction extends RichEventActionSupport
{
    public static final StringManager sm = StringManager.getManager(Constants.Package);
    private static final Logger log = LoggerFactory.getLogger(SearchDocumentsAction.class);  

    private StringManager langG ;
    
    static final int LIMIT = 10;
    public static final String DEFAULT_COLUMNS; // = "[\"officeNumber\", \"ctime\", \"sender\", \"description\", \"status\"]";
    static final String EV_SEARCH = "search";
    static
    {
        DEFAULT_COLUMNS = new JSONArray(Arrays.asList(new String[] {
            "document_id", "document_title", "document_ctime","officeNumber",
            "document_author", "document_mtime","document_remark","box_name"
        })).toString();
    }

    public static final String[] ALL_COLUMNS = { "document_id", "document_title",
                                                 "document_ctime", "document_mtime", "document_author",
                                                 "attachment_link1","document_remark", "document_description", 
                                                 "document_flags","document_user_flags", "current_workflow_location",
                                                 "box_name"};

    // mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy
    // wynik�w (w klasie QueryForm)
    private static final Map<String, String> sortProperties = new HashMap<String, String>();
    static
    {
        sortProperties.put("document_id", "id");
        sortProperties.put("document_title", "title");
        sortProperties.put("document_ctime", "ctime");
        sortProperties.put("document_mtime", "mtime");
        sortProperties.put("officeNumber", "officeNumber");
    }
    private String filterName;
    private Long id;
    private Long documentId;
	private String documentBarcode;
    private String documentIdString;
    private int offset;
    private int limit;
    private String sortField;
    private boolean ascending;
    private String title;
    private String description;
    private String attribute;
    private String startCtime;
    private String endCtime;
    private String startMtime;
    private String endMtime;
    private String attachmentBarcode;
    private String author;
    private Long[] doctypeIds;
    private Long[] documentKindIds;
    private Long boxId;
    private String accessedBy;
    private String[] accessedAs;
    private String lastRemark ;
    private String fieldValue; // u�ywane w AttrSearch

    // @EXPORT
    private List<DSUser> users;
    private List<Doctype> doctypes;
    private List<DocumentKind> documentKinds;
    private boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    List<Map<String, Object>> results;
    List columns;
    Pager pager;
    private List<Box> boxList;
    private List<DSUser> accessedByUsers;

    private String thisSearchUrl;
    private boolean canView;

//    private SearchDocumentsRequest searchRequest;

	/**
     * Klasa u�ywana do formatowania dat podanych przez u�ytkownika.
     * Daty wybierane s� z kontrolki kalendarza i maj� ustalony format
     * (dd-MM-yyyy). U�ywana jest domy�lna strefa czasowa, kt�r� mo�na
     * zmieni� przy pomocy w�asno�ci "user.timezone" w linii polece� JRE.
     */
    public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private DocumentChangelog[] documentChangeLogAll;
    private String dateFrom;
    private String dateTo;
    private Long authorUserId;

    public static String getColumnDescription(String column)
    {
        return sm.getString("search.documents.column."+column);
    }
    
    /*public static String getLink(Long id)
    {
        return "/repository/search-documents.action?id="+id;
    }*/
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("jsonView").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new JsonView()).
            appendFinally(CloseHibernateSession.INSTANCE);

/*
        registerListener("doAttrSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new AttrSearch()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
*/
        registerListener("doSave").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Save()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(EV_SEARCH,new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

//        registerListener("doSearchJson").
//            append(OpenHibernateSession.INSTANCE).
//            append(EV_SEARCH,new Search()).
//            append(new JsonSearchResult()).
//            appendFinally(CloseHibernateSession.INSTANCE);

        hibernateListener(new JsonSearch());
    }

    private class FillForm implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
                documentChangeLogAll = DocumentChangelog.all();

    			if(id != null){
    				SearchFilter filtr = SearchFilter.find(id);
    				filterName = filtr.getFilterName();
    				documentId = filtr.getDocument_id();
    				title = filtr.getTitle();
    				description = filtr.getDescription();
    				attribute = filtr.getAttribute();
    				author = filtr.getAuthor();
    				attachmentBarcode = filtr.getAttachmentBarcode();
    				SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
    				if(filtr.getStartCtime() != null)
    					startCtime = fmt.format(filtr.getStartCtime());
    				if(filtr.getEndCtime() != null)
    					endCtime = fmt.format(filtr.getEndCtime());
    				if(filtr.getStartMtime() != null)
    					startMtime = fmt.format(filtr.getStartMtime());
    				if(filtr.getEndMtime() != null)
    					endMtime = fmt.format(filtr.getEndMtime());
    				lastRemark = filtr.getLastRemark();
    				accessedBy = filtr.getAccessedBy();
    				String[] kindId =  StringUtils.split(filtr.getDocumentKindIds(), ',');
    				if(kindId.length != 0){
    					documentKindIds = new Long[kindId.length];
    					for(int i=0; i<kindId.length; i++){
    						documentKindIds[i] = Long.valueOf(kindId[i]);
    					}
    				}

    			}
    			if(DSApi.context().hasPermission(DSApi.context().getDSUser(), DSPermission.ARCHIWUM_DOSTEP))            	  
    			{           
    				setCanView(true);
    			}
    			else if(Docusafe.moduleAvailable("coreoffice"))
    			{            		
    				throw new AccessControlException("Nie masz uprawnie� dost�pu do modu�u Dokumenty");
    			}

                langG = GlobalPreferences.loadPropertiesFile("",null);                                               
                
                users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                if (Configuration.hasExtra("business")/* && !Configuration.hasExtra("nationwide")*/)       
                    documentKinds = DocumentKindProvider.get().visible().list();
                else
                    doctypes = Doctype.list(true);
                
                useBarcodes = GlobalPreferences.isUseBarcodes();
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
                boxList = Box.list(); 

                if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    accessedByUsers = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA))
                {
                    // tylko z dzia�u
                    accessedByUsers = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    accessedByUsers = new ArrayList<DSUser>(1);
                    accessedByUsers.add(DSApi.context().getDSUser());
                }
            }
            catch (EdmException e)
            {
                event.addActionError(e.getMessage());
            }
        }
    }

    private class JsonView extends AjaxSerializeActionListener {

        @Override
        public Object serializeAction(ActionEvent event) throws Exception {
            Map<String, Object> ret = new HashMap<String, Object>();

            ret.put("columns", columns);
            ret.put("documentKinds", documentKinds);
            ret.put("documentChangeLogAll", FluentIterable.from(Arrays.asList(documentChangeLogAll)).transform(new Function<DocumentChangelog, DocumentChangelogView>() {
                @Override
                public DocumentChangelogView apply(DocumentChangelog input) {
                    return new DocumentChangelogView(input);
                }
            }).toList());
            ret.put("users", FluentIterable.from(users).transform(new Function<DSUser, UserView>() {
                @Override
                public UserView apply(DSUser user) {
                    return new UserView(user);
                }
            }).toList());

            ret.put("accessedByUsers", FluentIterable.from(accessedByUsers).transform(new Function<DSUser, UserView>() {
                @Override
                public UserView apply(DSUser user) {
                    return new UserView(user);
                }
            }).toList());

            return ret;
        }
    }

    private class JsonSearch extends AjaxSerializeActionListener {

        @Override
        public Object serializeAction(ActionEvent event) throws Exception {

            SearchDocumentsRequest searchRequest = new Gson().fromJson(rawInput, SearchDocumentsRequest.class);
            searchRequest.encoding = "UTF-8";

            new SearchDocumentsProcessor().process(searchRequest, event, SearchDocumentsAction.this);

//            BeanUtils.copyProperties(this, searchRequest);
//            new Search().actionPerformed(event);
//            Map<String, Object> ret = new HashMap<String, Object>();

            if(results == null) {
                results = new ArrayList<Map<String, Object>>();
            }

            for(Map<String, Object> result : results) {
               log.debug("[JsonSearch] result");

                for(Map.Entry<String, Object> entry: result.entrySet()) {
                    log.debug("[JsonSearch]     {} -> {}", entry.getKey(), entry.getValue());
                }
            }

            // prettyPrint
            for(Map<String, Object> result: results) {
                for(Map.Entry<String, Object> entry: result.entrySet()) {
                    result.put(entry.getKey(), prettyPrint(entry.getValue()));
                }
            }


            Map<String, Object> ret = new HashMap<String, Object>();
            ret.put("columns", columns);
            ret.put("results", results);


            // Je�li result jest "edit-documents" oznacza to,
            // �e znale�li�my dokument i by�o przekierowanie
            // na strone dokumentu.
            if(results.size() == 0 && "edit-document".equals(event.getResult())) {
                ret.put("redirect", true);
            }

            return ret;
        }
    }
    
    private class Save implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		 try {
    			 if(StringUtils.isEmpty(filterName))
    				 addActionError(sm.getString("niePodanoNazwy"));
    			 
    			 if(StringUtils.isEmpty(title) && StringUtils.isEmpty(description) && StringUtils.isEmpty(author) && StringUtils.isEmpty(attribute)
    					 && StringUtils.isEmpty(attachmentBarcode) && StringUtils.isEmpty(lastRemark) && StringUtils.isEmpty(accessedBy) && startCtime == null
    					 && endCtime == null && startMtime == null && endMtime == null && documentId == null && documentKindIds == null)
    				 addActionError(sm.getString("niePodanoParametrow"));
    				 
    			 if (hasActionErrors())
    	    			return;
    			 
				DSApi.context().begin();
			List<SearchFilter> find = SearchFilter.findByCn(getFilterName());
			if(find.size() != 0){
				for(SearchFilter f:find){
					 f.SetFilterName(filterName);
		    		 f.setDocument_id(documentId);
		    		 f.setTitle(title);
		    		 f.setDescription(description);
		    		 f.setAttribute(attribute);
		    		 f.setAuthor(author);
		    		 f.setAttachmentBarcode(attachmentBarcode);
		    		 
		    		 if(startCtime != null)
		    			 f.setStartCtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(startCtime).getTime()));
		    		 if(endCtime != null)
		    			 f.setEndCtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(endCtime).getTime()));
		    		 if(startMtime != null)
		    			 f.setStartMtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(startMtime).getTime()));
		    		 if(endMtime != null)
		    			 f.setEndMtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(endMtime).getTime()));
		    		 
		    		 f.setLastRemark(lastRemark);
		    		 f.setAccessedBy(accessedBy);
		    		 String kindId = "";
		    		 if(documentKindIds != null){
		    			 for(int i=0; i<documentKindIds.length; i++){
		    				 kindId += documentKindIds[i] + ",";
		    			 }
                     }
                     else
                     {
                         kindId = "11,";
                     }

		    		 f.setDocumentKindIds(kindId);
		    		 f.save();
		    		 addActionMessage("zapisano");
				}
			} else {
    		 SearchFilter filtrWysz = new SearchFilter();
    		 filtrWysz.SetFilterName(filterName);
    		 if(documentIdString!=null && documentIdString.equals("")&&AvailabilityManager.isAvailable("wyszukiwarka.IDString"))
    			 filtrWysz.setDocument_id(Long.parseLong(documentIdString));
    		 else filtrWysz.setDocument_id(documentId);
    		 filtrWysz.setTitle(title);
    		 filtrWysz.setDescription(description);
    		 filtrWysz.setAttribute(attribute);
    		 filtrWysz.setAuthor(author);
    		 filtrWysz.setAttachmentBarcode(attachmentBarcode);
    		 
    		 if(startCtime != null)
    			 filtrWysz.setStartCtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(startCtime).getTime()));
    		 if(endCtime != null)
    			 filtrWysz.setEndCtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(endCtime).getTime()));
    		 if(startMtime != null)
    			 filtrWysz.setStartMtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(startMtime).getTime()));
    		 if(endMtime != null)
    			 filtrWysz.setEndMtime(new java.sql.Date(DateUtils.nullSafeParseJsDate(endMtime).getTime()));

    		 filtrWysz.setLastRemark(lastRemark);
    		 filtrWysz.setAccessedBy(accessedBy);
    		 String kindId = "";
    		 if(documentKindIds != null){
    			 for(int i=0; i<documentKindIds.length; i++){
    				 kindId += documentKindIds[i] + ",";
    			 }
    		 }
    		 filtrWysz.setDocumentKindIds(kindId);
    		 filtrWysz.save();


    		 addActionMessage("zapisano");
			}
			 DSApi.context().commit();
    		} catch (EdmException e) {
    			event.addActionError(e.getMessage());
    		}
    		 catch (NumberFormatException e)
             {
                 event.addActionError(sm.getString("wyszukiwarka.niepoprawneID"));
             }
    	}
    }

    private class Search extends RichActionListener
    {
        public void action(ActionEvent event) throws Exception {
            SearchDocumentsRequest searchRequest = new SearchDocumentsRequest();
            searchRequest.copyProperties(SearchDocumentsAction.this);
            searchRequest.encoding = "ISO-8859-2";
            
            boolean searchByDatabase = AvailabilityManager.isAvailable("searchDocuments.searchByDatabaseAnyway");
            
            new SearchDocumentsProcessor().process(searchRequest, event, SearchDocumentsAction.this, searchByDatabase);
        }
    }

    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            String date = DateUtils.formatJsDateTime((Date) object);
            
            if (date.substring(date.indexOf(' ')+1).equals("00:00"))
                return DateUtils.formatJsDate((Date) object);
            else
                return date;
        }
        else
        {
            return object.toString();
        }
    }

    public String getSortLink(String sortField, boolean ascending)
    {
        return thisSearchUrl +
            (thisSearchUrl.indexOf('?') > 0 ? "&" : "?") +
            "sortField="+sortField+
            "&ascending="+ascending;
    }

    List getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }

    List getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.documents.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(array.getString(i), desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null,""));
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null,""));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getDocumentIdString() {
		return documentIdString;
	}

	public void setDocumentIdString(String documentIdString) {
		this.documentIdString = documentIdString;
	}

    public String getDocumentBarcode() {
		return documentBarcode;
	}

	public void setDocumentBarcode(String documentBarcode) {
		this.documentBarcode = documentBarcode;
	}
	
	public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAttribute()
    {
        return attribute;
    }

    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    public String getStartCtime()
    {
        return startCtime;
    }

    public void setStartCtime(String startCtime)
    {
        this.startCtime = startCtime;
    }

    public String getEndCtime()
    {
        return endCtime;
    }

    public void setEndCtime(String endCtime)
    {
        this.endCtime = endCtime;
    }

    public String getStartMtime()
    {
        return startMtime;
    }

    public void setStartMtime(String startMtime)
    {
        this.startMtime = startMtime;
    }

    public String getEndMtime()
    {
        return endMtime;
    }

    public void setEndMtime(String endMtime)
    {
        this.endMtime = endMtime;
    }

    public String getAttachmentBarcode()
    {
        return attachmentBarcode;
    }

    public void setAttachmentBarcode(String attachmentBarcode)
    {
        this.attachmentBarcode = attachmentBarcode;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Long[] getDoctypeIds()
    {
        return doctypeIds;
    }

    public void setDoctypeIds(Long[] doctypeIds)
    {
        this.doctypeIds = doctypeIds;
    }

    public Long[] getDocumentKindIds()
    {
        return documentKindIds;
    }

    public void setDocumentKindIds(Long[] documentKindIds)
    {
        this.documentKindIds = documentKindIds;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public List<Doctype> getDoctypes()
    {
        return doctypes;
    }

    public List<DocumentKind> getDocumentKinds()
    {
        return documentKinds;
    }

    public boolean isUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public List getResults()
    {
        return results;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public List getColumns()
    {
        return columns;
    }

    public Pager getPager()
    {
        return pager;
    }

    public String getFieldValue()
    {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue)
    {
        this.fieldValue = fieldValue;
    }


    public List getBoxList()
    {
        return boxList;
    }

    public Long getBoxId()
    {
        return boxId;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public void setAccessedBy(String accessedBy)
    {
        this.accessedBy = accessedBy;
    }

    public void setAccessedAs(String[] accessedAs)
    {
        this.accessedAs = accessedAs;
    }

    public List getAccessedByUsers()
    {
        return accessedByUsers;
    }       
    
    public boolean isCanView() 
    {
		return canView;
	}

	public void setCanView(boolean canView) 
	{
		this.canView = canView;
	}

    public static String getLink(int offset, int limit, String sortField, Boolean ascending,
                          String title, String description,
                          String startCtime, String endCtime,
                          String startMtime, String endMtime,
                          String author, Long[] documentKindIds, 
                          Long boxId, String accessedBy, String[] accessedAs, String lRemark, String documentBarcode)
    {
        StringBuilder link = new StringBuilder("/repository/search-documents.action" +
            "?doSearch=true" +
            "&offset="+offset+
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField : "") +
            (ascending != null ? "&ascending="+ascending : "") +
            (title != null ? "&title="+title : "") +
            (description != null ? "&description="+description : "") +
            (boxId != null ? "&boxId="+boxId : "") +
            (startCtime != null ? "&startCtime="+startCtime : "") +
            (endCtime != null ? "&endCtime="+endCtime : "") +
            (startMtime != null ? "&startMtime="+startMtime : "") +
            (endMtime != null ? "&endMtime="+endMtime : "") +
            (author != null ? "&author="+author : "") +
            (accessedBy != null ? "&accessedBy="+accessedBy : "")+
            (lRemark!=null ? "&lastRemark="+lRemark:"")+
            (documentBarcode!=null ? "&documentBarcode="+documentBarcode:""));
        if (accessedAs != null)
        {
            for (int i=0; i < accessedAs.length; i++)
            {
                link.append("&accessedAs=");
                link.append(accessedAs[i]);
            }
        }
        if (documentKindIds != null && documentKindIds.length > 0)
        {
            for (int i=0; i < documentKindIds.length; i++)
            {
                link.append("&documentKindIds=");
                link.append(documentKindIds[i]);
            }
        }
        return link.toString();
    }


    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public Long getAuthorUserId() {
        return authorUserId;
    }

    public String getAccessedBy() {
        return accessedBy;
    }

    public String[] getAccessedAs() {
        return accessedAs;
    }

    public boolean getAscending() {
        return ascending;
    }

    public String getThisSearchUrl() {
        return thisSearchUrl;
    }

    public void setThisSearchUrl(String thisSearchUrl) {
        this.thisSearchUrl = thisSearchUrl;
    }

	public String getLastRemark() {
		return lastRemark;
	}

	public void setLastRemark(String lastRemark) {
		this.lastRemark = lastRemark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFilterName() {
		return filterName;
	}
	
	public void setFilterName(String filterName) {
		this.filterName=filterName;
	}

    public DocumentChangelog[] getDocumentChangeLogAll() {
        return documentChangeLogAll;
    }

    public void setDocumentChangeLogAll(DocumentChangelog[] documentChangeLogAll) {
        this.documentChangeLogAll = documentChangeLogAll;
    }
}

