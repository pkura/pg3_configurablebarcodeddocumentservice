package pl.compan.docusafe.web.archive.repository.search;

import pl.compan.docusafe.core.dockinds.Database;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/***/
public class SearchCriteria implements Cloneable {
    private final String tableName;
    private final Object tableValue;
    private Map<Database.Column, String> values = new HashMap<Database.Column, String>();

    public SearchCriteria(String tableName, Object tableValue) {
        this.tableName = tableName;
        this.tableValue = tableValue;
    }

    public void clear() {
        values.clear();
    }

    public boolean isAnyValue() {
        return !values.isEmpty();
    }

    public String getTableName() {
        return tableName;
    }

    public Object getTableValue() {
        return tableValue;
    }

    public String getValue(String criteriaName) {
        return values.get(criteriaName);
    }

    @Override
    public SearchCriteria clone() {
        SearchCriteria clone = new SearchCriteria(tableName, tableValue);
        Set<Map.Entry<Database.Column, String>> entries = getColumnValue();
        for (Map.Entry<Database.Column, String> entry : entries)
            clone.put(entry.getKey().getName(), entry.getValue());
        return clone;
    }

    public Set<Map.Entry<Database.Column, String>> getColumnValue() {
        return values.entrySet();
    }

    public SearchCriteria put(String columnName, String value) {
        if (value == null) return this;
        values.put(new Database.Column(columnName), value);
        return this;
    }
}
