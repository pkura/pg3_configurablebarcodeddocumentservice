package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.parametrization.prosika.ProsikaArchiveDocument;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import java.sql.*;
/**
 * Zamawianie dokumentow archiwalnych 
 * Na razie dla COKu
 * @author Mariusz Kilja�czyk
 */
@SuppressWarnings("serial")
public class OrderArchiveDocumentAction extends EventActionSupport
{
	static final Logger log = LoggerFactory.getLogger(OrderArchiveDocumentAction.class);
	/**
	 * Departament
	 */
	private String department;
	/**
	 * Wydzial
	 */
	private String wydzial;
	/**
	 * Numer Oracla
	 */
	private String oracleNo;
	/**
	 * Numer FKX (Region, kod kreskowy
	 */
	private String fkxNo;
	/**
	 * Nazwa dokumentu
	 */
	private String name;
	/**
	 * Znak akt
	 */	
	private String znakAkt;
	/**
	 * Tytul
	 */
	private String title;
	
	/**
	 * Rok
	 */
	private String rok;
	
	private String rokOd;
	private String rokDo;
	/**
	 * Numer segregatora, archiboxu
	 */
	private String numerSegregatora;
	
	/**
	 * Numer pudla IM
	 */
	private String boxNumber;
	
	private String doReload;
	

	/**
	 * Opis
	 */
	private String description;
	
	private List<ProsikaArchiveDocument> archiveDocuments;
	private Long archiveDocumentId;
	private List<OrderBean> orders;	
	
	
	//Informacje pomocnicze 
	 static String[] departments = null;
	
	 String[] wydzialy = null;
	static String[] lata = null;
	static Map<String,String[]> wydzialyMapa = new HashMap<String,String[]>();
	
 	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doOrder").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
    }
 
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  
        	try
        	{   
        		setUpLists();
        		doReload = "false";
	        	if(archiveDocumentId != null)
	        	{
	        		ProsikaArchiveDocument pas = ProsikaArchiveDocument.find(archiveDocumentId);

	        		orders = new ArrayList<OrderBean>();
	        		department = pas.getDepartment();
	        		wydzial = pas.getWydzial();
	        		oracleNo = pas.getOracleNo();
	        		fkxNo = pas.getFkxNo();
	        		name = pas.getName();
	        		znakAkt = pas.getZnakAkt();
	        		title = pas.getTitle();
	        		rok = pas.getRok();
	        		numerSegregatora = pas.getNumerSegregatora();
	        		boxNumber = pas.getBoxNumber();
	        		description = pas.getDescription();
	        	}
        	}
        	catch (Exception e)
        	{
        		addActionError(e.getMessage());
				log.error("", e);
			}
        }
        
        /**
         * Ustawiamy listy niezbedne do obslugi formatek
         * @throws Exception
         */
        protected synchronized void setUpLists()
        {
        	List<String> pom = null;
        	java.sql.PreparedStatement ps = null;
        	try
        	{
	        	if (departments==null)
	        	{
	        		pom = new ArrayList<String>();
	        		ps = DSApi.context().prepareStatement("select distinct department from dsg_prosika_archive_document order by department");
	        		ResultSet rs = ps.executeQuery();
	        		while (rs.next())
	        		{
	        			String dep = rs.getString("department"); 
	        			pom.add(dep);
	        			log.trace(dep);
	        		}
	        		rs.close();
	        		departments = pom.toArray(new String[pom.size()]);
	        		DSApi.context().closeStatement(ps);
	        	}
	        	String depKey = department;
	        	if (depKey == null || depKey.trim().length()==0)
	        	{
	        		depKey = "ALL";
	        	}
	        	log.trace("lista dla depKey={}",depKey);
	        	//Lista zalezy od wybranego departamentu, ale 
	        	if (wydzialyMapa.get(depKey)==null)
	        	{
	        		pom = new ArrayList<String>();
	        		if (depKey.equals("ALL"))
	        		{
	        			ps = DSApi.context().prepareStatement("select distinct wydzial from dsg_prosika_archive_document order by wydzial");
	        		}
	        		else
	        		{
	        			ps = DSApi.context().prepareStatement("select distinct wydzial from dsg_prosika_archive_document where department = ? order by wydzial");
	        			ps.setString(1, depKey);
	        		}
	        		ResultSet rs = ps.executeQuery();
	        		while (rs.next())
	        		{
	        			String wydzial = rs.getString("wydzial");
	        			log.trace(wydzial);
	        			pom.add(wydzial);
	        		}
	        		rs.close();
	        		DSApi.context().closeStatement(ps);
	        		wydzialy = pom.toArray(new String[pom.size()]);
	        		wydzialyMapa.put(depKey, wydzialy);
	        	}
	        	else
	        	{
	        		wydzialy = wydzialyMapa.get(depKey);
	        	}
	        	//weryfikujemy czy wydziala znajduje sie na liscie wydzialowa
	        	log.trace("weryfikujemy czy {} znajduje sie na liscie dla dep = {}",wydzial,depKey);
	        	boolean jest = false;
	        	for (String wydz : wydzialy)
	        	{
	        		if (wydz.equalsIgnoreCase(wydzial))
	        		{
	        			jest = true;
	        			break;
	        		}
	        	}
	        	if (!jest)
	        	{
	        		log.trace("wywalamy go z listy");
	        		wydzial = null;
	        	}
	        	pom = new ArrayList<String>();
	        	for (int rok = 1981; rok <= 2009;rok++)
	        	{
	        		pom.add(new Integer(rok).toString());
	        	}
	        	lata = pom.toArray(new String[pom.size()]);
        	}
        	catch (Exception e)
        	{
        		log.error("",e);
        	}
        	finally
        	{
        		DSApi.context().closeStatement(ps);
        	}
        }
    }
    
    private class Search implements ActionListener
    {
    	
        @SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent event)
        {
        	if ("true".equals(doReload))
        		return;
        	log.debug("doSearch");
        	try
        	{
        		Criteria criteria = DSApi.context().session().createCriteria(ProsikaArchiveDocument.class,"id");  
        		addCriteria(criteria,department,"department");
        		addCriteria(criteria,wydzial,"wydzial");
        		addCriteria(criteria,oracleNo,"oracleNo");
        		addCriteria(criteria,fkxNo,"fkxNo");
        		addCriteria(criteria,znakAkt,"znakAkt");
        		addCriteria(criteria,title,"title");
        		addCriteria(criteria,rok,"rok");
        		addCriteria(criteria,numerSegregatora,"numerSegregatora");
        		addCriteria(criteria,boxNumber,"boxNumber");
        		addCriteria(criteria,description,"description",true); 
	            if (rokOd != null && rokOd.trim().length()>0)
	            	criteria.add(Restrictions.ge("rok", rokOd));
	            if (rokDo != null && rokDo.trim().length()>0)
	            	criteria.add(Restrictions.le("rok", rokDo));
        		archiveDocuments = criteria.list();
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
				log.error("", e);
			}
        }
        
        /**
         * Dodaje do kryteriow wartosc o ile jest niepusta
         * @param criteria
         * @param value
         * @param fieldName
         * @throws Exception
         */
        private void addCriteria (Criteria criteria, String value, String fieldName) throws Exception
        {
        	addCriteria(criteria,value,fieldName,false);
        }
        
        private void addCriteria (Criteria criteria, String value, String fieldName,boolean context) throws Exception
        {
        	if (value != null && value.trim().length()>0)
        	{
        		log.trace("{}={}",fieldName,value);
        		if (context)
        		{
        			criteria.add(Restrictions.like(fieldName, "%"+value+"%"));
        		}
        		else
        		{
        			criteria.add(Restrictions.like(fieldName, value));
        		}
        	}
        }
    }
    /*
    private class SearchByLucene implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  
        	try
        	{
        		String query = "";
        		boolean wasadded = false;
        		if(content!=null)
        		{
        			query+="content:("+content+") ";
        			wasadded = true;
        		}
        		if(accountCode!=null)
        		{
        			if(wasadded) query+="AND ";
        			query+="accountCode:("+accountCode+") ";
        			wasadded = true;
        		}
        		if(department!=null)
        		{
        			if(wasadded) query+="AND ";
        			query+="department:("+department+") ";
        			wasadded = true;
        		}
        		if(code!=null)
        		{
        			if(wasadded) query+="AND ";
        			query+="code:("+code+")";
        			wasadded = true;
        		}
        		if(wasadded)
        		{
	        		StandardAnalyzer analyzer = new StandardAnalyzer();
		            Searcher searcher = new IndexSearcher(IndexReader.open(Docusafe.getIndexes()));
		            org.apache.lucene.search.Query q = new org.apache.lucene.queryParser.QueryParser("content", analyzer).parse(query);
		            TopDocCollector hc = new TopDocCollector(100);
		            searcher.search(q, hc);
		            ScoreDoc[] hits = hc.topDocs().scoreDocs;
		            archiveDocuments = new ArrayList<ProsikaArchiveDocument>();
		            for(int i = 0; i<hits.length;i++)
		            {
		            	//System.out.println(searcher.doc(hits[i].doc).get("documentid"));
		            	archiveDocuments.add(ProsikaArchiveDocument.find(Long.parseLong(searcher.doc(hits[i].doc).get("documentid"))));
		            }
        		}
        		else
        		{
        			archiveDocuments = Finder.list(ProsikaArchiveDocument.class, "id");
        		}
	            
        		//archiveDocuments = ProsikaArchiveDocument.searchDocument(content, accountCode,department,code);
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
				log.error("", e);
			}
        }
    }
    */
    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  
        	try
        	{
        		
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
				log.error("", e);
			}
        }
    }
    
    private class OrderBean
    {
    	String orderName;
    	List<DocumentBean> documents;

		public String getOrderName() {
			return orderName;
		}

		public void setOrderName(String orderName) {
			this.orderName = orderName;
		}

		public List<DocumentBean> getDocuments() {
			return documents;
		}

		public void setDocuments(List<DocumentBean> documents) {
			this.documents = documents;
		}
		
		private List<DocumentBean> getBeans(List<Document> docs) throws EdmException
		{
			List<DocumentBean> ret = new ArrayList<DocumentBean>();
			if(docs == null)
				return ret;
			for (Document document : docs) 
			{
				ret.add(new DocumentBean(document));
			}
			return ret;
			
		}
		
		private class DocumentBean
		{
			private String description;
			private String mime;
			private Long revisionId;
			private Long id;
			
			public DocumentBean(Document doc) throws EdmException
			{
				this.description = doc.getDescription();
				if(doc.getAttachments() != null && doc.getAttachments().size() > 0 
						&& doc.getAttachments().get(doc.getAttachments().size()-1).getMostRecentRevision() != null)
				{
					this.mime = doc.getAttachments().get(doc.getAttachments().size()-1).getMostRecentRevision().getMime();
					this.revisionId = doc.getAttachments().get(doc.getAttachments().size()-1).getMostRecentRevision().getId();
				}
				this.id = doc.getId();
			}
			
			public String getDescription() {
				return description;
			}
			public void setDescription(String description) {
				this.description = description;
			}
			public String getMime() {
				return mime;
			}
			public void setMime(String mime) {
				this.mime = mime;
			}
			public Long getRevisionId() {
				return revisionId;
			}
			public void setRevisionId(Long revisionId) {
				this.revisionId = revisionId;
			}

			public void setId(Long id) {
				this.id = id;
			}

			public Long getId() {
				return id;
			}
		}
    }

	public List<ProsikaArchiveDocument> getArchiveDocuments() {
		return archiveDocuments;
	}

	public void setArchiveDocuments(List<ProsikaArchiveDocument> archiveDocuments) {
		this.archiveDocuments = archiveDocuments;
	}

	public Long getArchiveDocumentId() {
		return archiveDocumentId;
	}

	public void setArchiveDocumentId(Long archiveDocumentId) {
		this.archiveDocumentId = archiveDocumentId;
	}

	public List<OrderBean> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderBean> orders) {
		this.orders = orders;
	}

	
	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDepartment() {
		return department;
	}

	public String getWydzial() {
		return wydzial;
	}

	public void setWydzial(String wydzial) {
		this.wydzial = wydzial;
	}

	public String getOracleNo() {
		return oracleNo;
	}

	public void setOracleNo(String oracleNo) {
		this.oracleNo = oracleNo;
	}

	public String getFkxNo() {
		return fkxNo;
	}

	public void setFkxNo(String fkxNo) {
		this.fkxNo = fkxNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String documentName) {
		this.name = documentName;
	}

	public String getZnakAkt() {
		return znakAkt;
	}

	public void setZnakAkt(String znakAkt) {
		this.znakAkt = znakAkt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRok() {
		return rok;
	}

	public void setRok(String rok) {
		this.rok = rok;
	}

	public String getNumerSegregatora() {
		return numerSegregatora;
	}

	public void setNumerSegregatora(String numerSegregatora) {
		this.numerSegregatora = numerSegregatora;
	}

	public String getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public static String[] getDepartments() {
		return departments;
	}

	public static void setDepartments(String[] departments) {
		OrderArchiveDocumentAction.departments = departments;
	}
	
	public static String[] getLata() {
		return lata;
	}

	public static void setLata(String[] lata) {
		OrderArchiveDocumentAction.lata = lata;
	}

	public String getRokOd() {
		return rokOd;
	}

	public void setRokOd(String rokOd) {
		this.rokOd = rokOd;
	}

	public String getRokDo() {
		return rokDo;
	}

	public void setRokDo(String rokDo) {
		this.rokDo = rokDo;
	}

	public String[] getWydzialy() {
		return wydzialy;
	}

	public void setWydzialy(String[] wydzialy) {
		this.wydzialy = wydzialy;
	}

	public String getDoReload() {
		return doReload;
	}

	public void setDoReload(String doReload) {
		this.doReload = doReload;
	}
	
}
