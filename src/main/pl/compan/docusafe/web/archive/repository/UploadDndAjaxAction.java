package pl.compan.docusafe.web.archive.repository;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.encoders.Base64;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.general.encryption.AESEncryptor;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class UploadDndAjaxAction extends EventActionSupport{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(UploadDndAjaxAction.class);
	public static final StringManager sm = GlobalPreferences.loadPropertiesFile(UploadDndAjaxAction.class.getPackage().getName(),null);
		
	private FormFile file;
	private Long folderId;
	private String fileDescription;
	private String fileDate;
	private Long id;
	private String publicKey;
	private boolean encryption;
	
	@Override
	protected void setup() {
		
		 registerListener(DEFAULT_ACTION).
         	append(OpenHibernateSession.INSTANCE).
         	append(new FillForm()).
         	appendFinally(CloseHibernateSession.INSTANCE);
		
	}
	
	public class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {

		File attachment = null;
		try {
		
			Map<String, Object> props = new HashMap<String, Object>();
			attachment = file.getFile();	
			
			if((StringUtils.isNotBlank(publicKey)) && encryption) {
				AESEncryptor encryption = new AESEncryptor();
				encryption.setUpKey(publicKey);
				InputStream is = new FileInputStream(attachment.getAbsolutePath()); // sciezka poprawna
				File enctemp = File.createTempFile(attachment.getName().replaceAll("\\.", "_"), ".kryp");
				OutputStream os = new FileOutputStream(enctemp.getAbsolutePath()); // katalog temp - odszyfrowuje si� 
				encryption.encrypt(is, os);	
				attachment = enctemp;
			}
			
			props.put("DOC_DATE", fileDate);
			props.put("DOC_DESCRIPTION", fileDescription);
			props.put("FOLDER", folderId);

			DSIBean bean = new DSIBean();
			bean.setDocumentKind(getDragDropDockind());
			bean.setImageArray(FileUtils.readFileToByteArray(attachment));
			bean.setImageName(attachment.getName());
			
			id = DSIManager.persistDocument(getDragDropDockind(), props, DocumentType.INCOMING.getDSiImportKindCode(), bean);

			DSApi.context().begin();
			Document doc = OfficeDocument.find(id, false);	
			doc.setFolderId(folderId);
			
			DSApi.context().grant(doc, new PermissionBean(ObjectPermission.READ, doc.getAuthor(), "user","read"));
			DSApi.context().session().save(doc);
			DSApi.context().session().flush();			
			DSApi.context().commit();
			
			log.debug("DODANO: {} {}  {}  {}", file, folderId, fileDescription, fileDate);
			} catch (Exception e) {
				log.error("{} {}  {}  {}", file, folderId, fileDescription, fileDate);
			}finally{
				DSApi._close();
				file.getFile().deleteOnExit();
				attachment.deleteOnExit();
			}
			
		}

		private String getDragDropDockind() {
			return AdditionManager.getPropertyOrDefault("dokumenty.przegladaj.draganddrop.dockind","normal_dragdrop");
		}

	}

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}	

	public String getFileDescription() {
		return fileDescription;
	}

	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	public String getFileDate() {
		return fileDate;
	}

	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}
	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public boolean getEncryption() {
		return encryption;
	}

	public void setEncryption(boolean encryption) {
		this.encryption = encryption;
	}

}