package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.webwork.NewOfficeDocumentRedirectAction;

/**
 * Przekierowuje do w�a�ciwej akcji tworzenia nowego dokumentu,
 * zale�nie od licencji.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewAction.java,v 1.4 2009/10/29 12:05:42 mariuszk Exp $
 */
public class NewAction extends NewOfficeDocumentRedirectAction
{
    private Long folderId;

    public Long getFolderId()
    {
        return folderId;
    }

    public void setFolderId(Long folderId)
    {
        this.folderId = folderId;
    }
}
