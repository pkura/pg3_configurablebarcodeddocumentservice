package pl.compan.docusafe.web.archive.repository;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.parametrization.ilpoldwr.NormalLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import javax.servlet.http.HttpSession;
import java.util.*;

public class SplitTiffAction extends EventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(SplitTiffAction.class);
    private final static StringManager sm = StringManager.getManager(SplitTiffAction.class.getPackage().getName());

    private Integer showPage;
    private List<FormFile> multiFiles = new ArrayList<FormFile>();
    private Map<String, String> files = new TreeMap<String, String>();
    private List<String> selectedFiles = new ArrayList<String>();

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSplit").append(OpenHibernateSession.INSTANCE).append(new SplitTiff()).appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSave").append(OpenHibernateSession.INSTANCE).append(new Save()).appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            showPage = 1;
            HttpSession session = ServletActionContext.getRequest().getSession(false);
            session.removeAttribute(NormalLogic.ILPOL_SPLITTED_TIFF);
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }

    }
    private class SplitTiff extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            showPage = 2;
            if (multiFiles != null && multiFiles.size() > 0)
            {
                files = ImageKit.splitAttachments(multiFiles, Docusafe.getPngTempFile());
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }

    }

    private class Save extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(selectedFiles != null && !selectedFiles.isEmpty()) {
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(NormalLogic.ILPOL_SPLITTED_TIFF, selectedFiles);
                addActionMessage(sm.getString("UtworzonoZalacznikZliczbaStron", selectedFiles.size()));
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public void setSelectedFiles(String[] selectedFiles) {
        this.selectedFiles = new ArrayList<String>(Arrays.asList(selectedFiles));
    }

    public Integer getShowPage() {
        return showPage;
    }

    public FormFile getMultiFiles()
    {
        if (this.multiFiles != null && this.multiFiles.size() > 0)
            return this.multiFiles.get(0);
        else
            return null;
    }

    public void setMultiFiles(FormFile file)
    {
        this.multiFiles.add(file);
    }

    public Map<String, String> getFiles()
    {
        return files;
    }
}
