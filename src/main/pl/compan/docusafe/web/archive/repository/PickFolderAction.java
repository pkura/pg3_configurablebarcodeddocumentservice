package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.FolderNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.FoldersTree;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PickFolderAction.java,v 1.5 2006/02/20 15:42:36 lk Exp $
 */
public class PickFolderAction extends EventProcessingAction
{
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(new SetActionForwardListener("main")).
            append(new FillForm());
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            Long folderId = (Long) event.getDynaForm().get("folderId");
            final String lparam = (String) event.getDynaForm().get("lparam");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                final DocumentHelper dh = ctx.getDocumentHelper();

                Folder folder = null;

                if (folderId != null)
                {
                    try
                    {
                        folder = Folder.find(folderId);
                    }
                    catch (FolderNotFoundException e)
                    {
                        folder = Folder.getRootFolder();
                    }
                }
                else
                {
                    folder = Folder.getRootFolder();
                }

                UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            return "javascript:void(pickFolder("+((Folder) element).getId()+
                                ", \""+((Folder) element).getPrettyPath()+
                                "\", \""+(lparam != null ? lparam : "")+"\")";
                        }
                        catch (EdmException e)
                        {
                            return "#";
                        }
                    }
                };

                HtmlTree tree = FoldersTree.newTree(ctx, folder, urlVisitor,
                    event.getRequest().getContextPath());

                event.getRequest().setAttribute("tree", tree.generateTree());
            }
            catch (EdmException e)
            {
                //
            }
            finally
            {
                DSApi._close();
            }
        }
    }
}
