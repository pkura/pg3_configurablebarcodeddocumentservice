package pl.compan.docusafe.web.archive.repository;

import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.encryption.EncryptedAttachmentRevision;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.imports.ImportManager;
import pl.compan.docusafe.core.imports.ImportedDocumentInfo;
import pl.compan.docusafe.core.imports.ImportedFileInfo;
import pl.compan.docusafe.core.imports.XmlImportManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2007-01-29 15:33:31 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */ 
public class ImportDocumentsAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(ImportDocumentsAction.class);
	private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(ImportDocumentsAction.class.getPackage().getName(),null);
    // @EXPORT
    private Collection<Map> importedFiles;
    private boolean canImport;
    private Map<Integer,String> importTypes;
    private int iterator = 0;

    // @IMPORT
    private FormFile file;
    private Long importedFileId;
    private Integer importType;

    // @EXPORT/@IMPORT
    private String tiffDirectory;
    private StringManager smL;
    private StringManager smG;
    
    private List<Tab> tabs;

    protected void setup()
    {
        if (!Configuration.hasExtra("business"))
            throw new Error(sm.getString("AkcjaDostepnaTylkoDlaLicencjiBusiness"));

        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAddFile").
            append(OpenHibernateSession.INSTANCE).
            append(new AddFile()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doStartImport").
            append(OpenHibernateSession.INSTANCE).
            append(new StartImport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doGenerateReport").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateReport()).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			smG = GlobalPreferences.loadPropertiesFile("", null);
			try
			{
				if (!DSApi.context().isAdmin())
				{
					canImport = false;
					throw new EdmException(sm.getString("BrakUprawnienDoImportowaniaDokumentow"));
				}
				else
					canImport = true;

				tabs = ImportDocumentsTabs.createTabs(ImportDocumentsTabs.TAB_DOCUMENTS);

				importTypes = new LinkedHashMap<Integer, String>();
				importTypes.put(ImportManager.TYPE_OGOLNY_XML, smG.getString("Ogolny"));

				if (DocumentKind.findByCn(DocumentKind.NORMAL_KIND) != null && !AvailabilityManager.isAvailable("nationwide.new.import"))
					importTypes.put(ImportManager.TYPE_AEGON_BIZ_SIMPLE, smG.getString("DokumentBiznesowy"));

				if (DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND) != null)
					importTypes.put(ImportManager.TYPE_ALD_FAKTURY, smG.getString("FakturaALD"));

				if (DocumentKind.findByCn(DocumentLogicLoader.DF_KIND) != null)
					importTypes.put(ImportManager.TYPE_AEGON_FINANSOWY, smG.getString("DokumentFinansowy"));

				if (DocumentKind.findByCn(DocumentLogicLoader.DP_KIND) != null)
					importTypes.put(ImportManager.TYPE_AEGON_PRAWNY, smG.getString("DokumentPrawny"));

				if (DocumentKind.findByCn(DocumentLogicLoader.DAA_KIND) != null)
					importTypes.put(ImportManager.TYPE_DAA, smG.getString("DokumentArchiwumAgenta"));

				if (DocumentKind.findByCn(DocumentLogicLoader.DC_KIND) != null)
					importTypes.put(ImportManager.TYPE_AEGON_PTE, smG.getString("DokumentCzlonkowski"));

				if (DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND) != null)
					importTypes.put(ImportManager.TYPE_EMPLOYEE, DocumentKind.findByCn(DocumentLogicLoader.EMPLOYEE_KIND).getName());
				
				if(DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND) != null && AvailabilityManager.isAvailable("nationwide.new.import"))
				{
					importTypes.put(ImportManager.TYPE_NATIONWIDE, DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND).getName()+" (in)");
					importTypes.put(ImportManager.TYPE_NATIONWIDE_OUTGOING, DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND).getName()+" (out)");
				}				

				tiffDirectory = Docusafe.getAdditionProperty("import.documents.tiff_directory");

				prepareImportedFiles();
			}
			catch (EdmException e)
			{
				addActionError(e.getMessage());
			}
		}
	}

	private void prepareImportedFiles() throws EdmException
	{

		importedFiles = fun.map(ImportedFileInfo.list(), new lambda<ImportedFileInfo, Map>()
		{
			public Map act(ImportedFileInfo importedFileInfo)
			{
				BeanBackedMap map = new BeanBackedMap(importedFileInfo, "id", "fileName", "ctime", "endTime");
				try
				{
					Integer documentsNo =  ImportedDocumentInfo.countDocuments(importedFileInfo);
					Integer processedDocumentsNo = ImportManager.STATUS_WAITING.equals(importedFileInfo.getStatus()) ? 0 : ImportedDocumentInfo.countProcessedDocuments(importedFileInfo);
					Integer badDocumentsNo =  ImportedDocumentInfo.countBadDocuments(importedFileInfo);
					String status = "";
					boolean canGenerateReport = false;
					if(importedFileInfo.getImportType() > 7 && ImportManager.STATUS_PARSE.equals(importedFileInfo.getStatus()))
					{
						status = ImportManager.getStatusString(importedFileInfo.getStatus());
						canGenerateReport = ImportManager.STATUS_DONE.equals(importedFileInfo.getStatus());
					}
					else if(importedFileInfo.getImportType() > 7 && importedFileInfo.getStatus() > ImportManager.STATUS_WAITING &&
						((processedDocumentsNo+badDocumentsNo) >= documentsNo))
					{
						status = ImportManager.getStatusString(ImportManager.STATUS_DONE);
						canGenerateReport = true;
					}
					else
					{
						status = ImportManager.getStatusString(importedFileInfo.getStatus());
						canGenerateReport = ImportManager.STATUS_DONE.equals(importedFileInfo.getStatus());
					}					
					map.put("creatingUser", DSUser.safeToFirstnameLastname(importedFileInfo.getCreatingUser()));					
					map.put("documentsNo",documentsNo);
					map.put("processedDocumentsNo", processedDocumentsNo);
					map.put("badDocumentsNo",badDocumentsNo);					
					map.put("canStartImport", ImportManager.STATUS_WAITING.equals(importedFileInfo.getStatus()));
					map.put("status", status);
					map.put("canGenerateReport",canGenerateReport );
					map.put("iteratorValue", iterator++);
				}
				catch (EdmException e)
				{
				}
				return map;
			}
		});
	}

	private class AddFile implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			smL = GlobalPreferences.loadPropertiesFile(ImportDocumentsTabs.class.getPackage().getName(), null);
			try
			{
				synchronized (ImportedDocumentInfo.class)
				{
					if (tiffDirectory == null)
						tiffDirectory = Docusafe.getAdditionProperty("import.documents.tiff_directory");

					File dir = new File(tiffDirectory);
					if (!dir.exists() || !dir.isDirectory())
						throw new EdmException(smL.getString("NieIstniejeKatalogOpodanejNazwie"));

					if (file == null || !file.sensible())
						throw new EdmException(smL.getString("NiePodanoPlikuZdanymiDoImportu"));

					/*if (!ImportManager.isTiffDirectoryAllowed(tiffDirectory, importType))
						throw new EdmException(smL.getString("BrakUprawnienDoImportowaniaDokumentowZkatalogu") + tiffDirectory + "'");*/

					if (ImportManager.TYPE_OGOLNY_XML.equals(importType))
					{
						XmlImportManager.parseAndAddImportedFile(file.getFile(), tiffDirectory, importType);
					}
					/**
					 * ostatnim importem w starej strukturze to TYPE_AEGON_PTE
					 * wszystkie nast�pne musz� by� prze DSI w przyszlosci przerobic stare importy na DSI
					 */
					else if (importType > 7)
					{
						ImportManager.addImportedFileInfo(file.getFile(), tiffDirectory, importType);
					}
					else
					{
						ImportManager.parseAndAddImportedFile(file.getFile(), tiffDirectory, importType);
					}
				}
			}
			catch (EdmException e)
			{
				log.error("",e);
				addActionError(e.getMessage());
			}
		}
	}

	private class StartImport implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			smL = GlobalPreferences.loadPropertiesFile(ImportDocumentsTabs.class.getPackage().getName(), null);
			try
			{
				synchronized (ImportedDocumentInfo.class)
				{
					DSApi.context().begin();

					ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);
					
					DocumentKind dk = DocumentKind.findByCn(ImportManager.getDockindCn(importedFileInfo.getImportType()));
					if(dk.logic().createAttachmentRevision() instanceof EncryptedAttachmentRevision)
					{
						if(DSApi.context().credentials().getKey(ImportManager.getDockindCn(importedFileInfo.getImportType()))==null)
								throw new EdmException(sm.getString("MusiszBycAutoryzowanyDlaRodzajuDokumentuDlaKotregoZlecaszImport"));
						DSIImportHandler.addToKeysMap(ImportManager.getDockindCn(importedFileInfo.getImportType()),
								DSApi.context().credentials().getKey(ImportManager.getDockindCn(importedFileInfo.getImportType())));
					}
					if(importedFileInfo.getImportType() > 7)
					{
						ImportManager.startDSIImport(importedFileId);
					}
					else if (ImportManager.isCsvImport(importedFileInfo.getImportType()))
						ImportManager.startImport(importedFileId);

					else if (importedFileInfo.getImportType().equals(ImportManager.TYPE_OGOLNY_XML))
						XmlImportManager.startImport(importedFileId);
					else
						throw new EdmException(smL.getString("NieznanyTypImportu") + ": " + importedFileInfo.getImportType());

					DSApi.context().commit();
				}
			}
			catch (Exception e)
			{
				log.error("",e);
				addActionError(e.getMessage());
				DSApi.context().setRollbackOnly();
			}
		}
	}

	private class GenerateReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();

				String filename = "raport.csv";

				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
				// response.setHeader("Content-Disposition", "inline");
				// response.setContentLength((int) content.length());

				ImportedFileInfo importedFileInfo = ImportedFileInfo.find(importedFileId);
				Integer importType = importedFileInfo.getImportType();
				if(importType > 7)
				{
					ImportManager.generateDSIReport(importedFileId, response.getOutputStream());
				}					
				else if (ImportManager.isCsvImport(importType))
					ImportManager.generateReport(importedFileId, response.getOutputStream());
				else if (ImportManager.TYPE_OGOLNY_XML.equals(importType))
					XmlImportManager.generateReport(importedFileId, response.getOutputStream());

				DSApi.context().commit();
			}
			catch (Exception e)
			{
				log.error("",e);
				addActionError(e.getMessage());
				DSApi.context().setRollbackOnly();
			}
		}
	}

	public Collection<Map> getImportedFiles()
	{
		return importedFiles;
	}

	public boolean isCanImport()
	{
		return canImport;
	}

	public void setFile(FormFile file)
	{
		this.file = file;
	}

	public void setImportedFileId(Long importedFileId)
	{
		this.importedFileId = importedFileId;
	}

	public String getTiffDirectory()
	{
		return tiffDirectory;
	}

	public void setTiffDirectory(String tiffDirectory)
	{
		this.tiffDirectory = tiffDirectory;
	}

	public Map<Integer, String> getImportTypes()
	{
		return importTypes;
	}

	public void setImportType(Integer importType)
	{
		this.importType = importType;
	}

	public List<Tab> getTabs()
	{
		return tabs;
	}

}
