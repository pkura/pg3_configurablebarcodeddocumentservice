package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class ImpulsLeasingCustomerAction extends EventActionSupport
{
    
    private Integer klient;
    private List<Map<String, Object>> documents;
    private List<? extends DlContractDictionary> results;


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            appendFinally(CloseHibernateSession.INSTANCE);        
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        }
    }
    
    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                //dokumenty
                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.KLIENT_CN);
                dockindQuery.field(f,klient);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                documents = new ArrayList<Map<String,Object>>(searchResults.totalCount());
                
                while (searchResults.hasNext())
                {
                    Document document = searchResults.next();
                    Map<String, Object> bean = new HashMap<String, Object>(); 
                    FieldsManager fm = kind.getFieldsManager(document.getId());
                    bean.put("id",document.getId());
                    bean.put("rodzajDokumentu",fm.getValue(DlLogic.RODZAJ_DOKUMENTU_CN));
                    bean.put("typDokumentu",fm.getValue(DlLogic.TYP_DOKUMENTU_CN));
                    bean.put("data",fm.getValue(DlLogic.DATA_DOKUMENTU_CN));
                //    if(fm.getValue(DlLogic.NUMER_UMOWY_CN)!=null) 
                //        bean.put("numerUmowy",((DlContractDictionary)fm.getValue(DlLogic.NUMER_UMOWY_CN)).getNumerUmowy());
                    bean.put("link","/repository/edit-dockind-document.action?id="+document.getId());
                    documents.add(bean);
                }
                // umowy
                QueryForm form = new QueryForm(0,0);
                
                if(klient != null)
                    form.addProperty("idKlienta", klient);
                
                form.addOrderAsc("numerUmowy");
                SearchResults<? extends DlContractDictionary> results = DlContractDictionary.search(form);
                if(results != null)
                	ImpulsLeasingCustomerAction.this.results = fun.list(results);          
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            	LogFactory.getLog("eprint").debug("", e);
            }
        }
    }

    public List<Map<String, Object>> getDocuments()
    {
        return documents;
    }

    public void setDocuments(List<Map<String, Object>> documents)
    {
        this.documents = documents;
    }

    public Integer getKlient()
    {
        return klient;
    }

    public void setKlient(Integer klient)
    {
        this.klient = klient;
    }

    public List<? extends DlContractDictionary> getResults()
    {
        return results;
    }

    public void setResults(List<? extends DlContractDictionary> results)
    {
        this.results = results;
    }

}
