package pl.compan.docusafe.web.archive.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.upload.FormFile;
import org.bouncycastle.util.encoders.Base64;
import org.jbpm.pvm.internal.cmd.GetResourceAsStreamCmd;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.general.encryption.AESEncryptor;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;



/**
 * Tworzenie nowego za��cznika do dokumentu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewAttachmentAction.java,v 1.13 2008/04/03 16:26:30 pecet4 Exp $
 */
public class NewAttachmentAction extends EventDrivenAction
{

    private static final Log log = LogFactory.getLog(NewAttachmentAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doCreate", "doCancel"
    };
    static
    {
        Arrays.sort(eventNames);
    }

    public static final String FORWARD = "repository/new-attachment";

    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(NewAttachmentAction.class.getPackage().getName(),null);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {

        request.setAttribute("documentLink",
            EditDocumentAction.getLink((Long) form.get("documentId")));

        return mapping.findForward("main");
    }

    /**
     * Rezygnacja z tworzenia dokumentu.
     */
    public ActionForward doCancel(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        Long id = (Long) form.get("documentId");
        if (id != null)
        {
            return new ActionForward("/repository/edit-document.action"+
                "?id="+id, true);
        }
        else
        {
            return new ActionForward(mapping.findForward("repository").getPath(), true);
        }
    }

    /** 
     * Tworzenie zalacznika 
     * @param mapping
     * @param form
     * @param eventParameter
     * @param request
     * @param response
     * @param errors
     * @param messages
     * @return
     */
    
    public ActionForward doCreate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String title = (String) form.get("title");
        String description = (String) form.get("description");
        String  publicKey = (String) form.get("publicKey");
    	Boolean encryption = (Boolean) form.get("encryption");

        request.setAttribute("documentLink",
            EditDocumentAction.getLink((Long) form.get("documentId")));

        if (StringUtils.isEmpty(title))
            errors.add(sm.getString("attachment.missingTitle"));
        if (StringUtils.isEmpty(description))
            errors.add(sm.getString("attachment.missingDescription"));

        InputStream stream = null;
        final FormFile file = (FormFile) form.get("file");
        if (file == null)
        {
            errors.add(sm.getString("attachment.missingFile"));
        }
        else if (file.getFileSize() == 0)
        {
            errors.add(sm.getString("attachment.missingFileContent"));
        }
        else if (AvailabilityManager.isAvailable("attachment.validate.size"))
        {
            int maxFileSize = AdditionManager.getIntProperty("attachment.size.limit");
            if(file.getFileSize() > maxFileSize)
            errors.add(sm.getString("MaxSizeFileUpload"));
        }
        else
        {
            try
            {
                stream = file.getInputStream();
            }
            catch (IOException e)
            {
                errors.add(sm.getString("attachment.cannotOpenFile"));
            }
        }

        //String mime = MimetypesFileTypeMap.getInstance().getContentType(file.getFileName().toLowerCase());

        if (errors.size() > 0)
            return mapping.findForward("main");

        Long documentId = (Long) form.get("documentId");

        if (documentId == null)
            throw new RuntimeException(sm.getString("newAttachment.missingDocumentId"));

        Long attachmentId = null;
/*
        Context ctx = (Context) request.getSession(true).getAttribute(Context.KEY);
        if (ctx == null)
            throw new RuntimeException(sm.getString("missingContext"));
*/

        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(request));
            ctx.begin();

            Document document = Document.find(documentId);

            if (!document.canModifyAttachments())
                throw new AccessDeniedException(sm.getString("BrakUprawnienDoTworzeniaZalacznikowWtymDokumencie"));
            
          
            Attachment attachment = new Attachment(title.trim(), description.trim());

            document.createAttachment(attachment);
			
            if((StringUtils.isNotBlank(publicKey)) && encryption) {
               	AESEncryptor encryptFile = new AESEncryptor();
            	encryptFile.setUpKey(publicKey);
            	InputStream is = file.getInputStream();
            	File enctemp = File.createTempFile(file.getFileName().replaceAll("\\.", "_"), ".kryp");
            	OutputStream os = new FileOutputStream(enctemp.getAbsolutePath()); 
            	encryptFile.encrypt(is, os);   
            	attachment.createRevision(enctemp);
            	
            } 
            else {
            	attachment.createRevision(file.getInputStream(), file.getFileSize(), file.getFileName());
            }

//            dh.createRevision(attachment, stream, file.getFileSize(),
//                file.getFileName());

            attachmentId = attachment.getId();
            
            if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym")&&document instanceof InOfficeDocument){
                ((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
            }

            if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                JackrabbitXmlSynchronizer.createEvent(document.getId());
            }

            ctx.commit();

        }
        catch (Exception e)
        {
            log.error("", e);
            errors.add(e.getMessage());
            if (ctx != null) DSApi.context().setRollbackOnly();
        }
        finally
        {
            DSApi._close();
        }

        if (attachmentId != null && ctx != null ) //&& !ctx._wasRolledBack())
        {
            return new ActionForward("/repository/edit-document.action"+
                "?id="+documentId, true);
        }
        else
        {
            return mapping.findForward("main");
        }
    }



    public String[] getEventNames()
    {
        return eventNames;
    }


    public static String getLink(Long documentId)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+"?documentId="+documentId;
    }
}
