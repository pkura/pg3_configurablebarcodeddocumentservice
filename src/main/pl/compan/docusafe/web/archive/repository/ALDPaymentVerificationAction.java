package pl.compan.docusafe.web.archive.repository;

import com.opensymphony.webwork.ServletActionContext;
import java.util.ArrayList;
import java.util.Set;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;

import java.util.Collections;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ald.ALDInvoiceInfo;
import pl.compan.docusafe.parametrization.ald.ALDManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfGenerator;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;

import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ALDPaymentVerificationAction extends EventActionSupport
{
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ALDPaymentVerificationAction.class.getPackage().getName(),null);
    // @EXPORT
    private List<Map<String, Object>> invoiceBeans;
    private String pdfUrl;
    
    
    // @IMPORT
    private FormFile file;
    private Map<String,Boolean> payments = new HashMap<String, Boolean>();
    private String sortField;
    private boolean asc;
    
    private String streamPdf;
    private String date;
    
 
    
    private static String EV_FILL = "fill";
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doExecutePayments").
            append(OpenHibernateSession.INSTANCE).
            append(new ExecutePayments()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if(streamPdf!=null && streamPdf.equals("true"))
                {
                    if(pdfUrl==null)   
                        throw new EdmException(sm.getString("WystapilNieznanyBladPrzyTworzeniuRaportu."));
                    
                    File streamFile = new File(Docusafe.getTemp() + File.separator + pdfUrl);
                    try
                    {
                        if(streamFile.exists())
                            ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(streamFile),
                                "application/pdf", (int) streamFile.length(),"Content-Disposition: attachment; filename=\"DocusafeReport"  + DateFormat.getDateTimeInstance().format(new Date()) + ".pdf\"");
                        else
                            throw new EdmException(sm.getString("WystapilNieznanyBladPrzyTworzeniuRaportu."));
                    }
                    catch(IOException ioe)
                    {
                        addActionError(sm.getString("WystapilBladPrzyPobieraniuPlikuPDF"));
                        return;
                    }
                }
                if(date==null)
                    date = DateUtils.formatJsDate(new Date());
                List<ALDInvoiceInfo> invoiceInfos = ALDInvoiceInfo.find(DSApi.context().getPrincipalName(), sortField, asc);
                invoiceBeans = ALDManager.prepareBeans(invoiceInfos);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class LoadFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (file == null || !file.sensible())
            {
                addActionError(sm.getString("NieWybranoPliku"));
                return;
            }
            
            event.skip(EV_FILL);
            
            try
            {
                DSApi.context().begin();
                
                // usuwamy wcześniejsze wpisy tego użytkownika
                ALDManager.deleteInvoiceInfos(DSApi.context().getPrincipalName());
                // dodajemy nowe z podanego pliku i przetwarzamy wczytane dane
                List<ALDInvoiceInfo> invoiceInfos = ALDManager.parseAndSavePaymentFile(file.getFile());
                if (invoiceInfos != null)
                	invoiceBeans = ALDManager.prepareBeans(invoiceInfos);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    private class ExecutePayments implements ActionListener
    {
        @SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent event)
        {
            try
            {
               
                DSApi.context().begin();
                Date paymentDate = DateUtils.nullSafeParseJsDate(date);
                
                if(paymentDate== null)
                    throw new EdmException(sm.getString("NieprawidlowaData"));
                if (payments.size() > 0) {
                	ALDManager.executePayments(payments, paymentDate);
                	 pdfUrl = createPDFReport(payments);
                } else {
                	addActionMessage(sm.get("BrakFaktur"));
                }
                
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    private String createPDFReport(Map<String,Boolean> payments)
    {
        try
        {
                List<Long> ids = new ArrayList<Long>();
                Set<String> stringIds = payments.keySet();
                for(String str : stringIds)
                {
                    ids.add(Long.parseLong(str.split("_")[1]));
                }
                
                List<String> columns = new ArrayList<String>();
                int[] widths = new int[7];
                int pos = 0;
                Float sumAmount=0f;
                List<List<Object>> content = new ArrayList<List<Object>>();
                ALDInvoiceInfo aldInvoice;
                List<Object> row;
                
                columns.add("Supplier");
                widths[pos++]=35;
             
                columns.add("Book Code");
                widths[pos++]=20;
                
                columns.add("Document Nr.");
                widths[pos++]=35;
    
                columns.add("Reference Nr.");
                widths[pos++]=50;

                columns.add("Document Id");
                widths[pos++]=40;
                
                columns.add("Supplier Amount");
                widths[pos++]=30;
                
                columns.add("Amount");
                widths[pos++]=30;
                
                for(Long l : ids)
                {
                    row = new ArrayList<Object>();
                    aldInvoice = ALDInvoiceInfo.find(l);
                    row.add(aldInvoice.getSupplierNumber());
                    row.add(aldInvoice.getBookCode());
                    row.add(aldInvoice.getNrKsiegowania());
                    row.add(aldInvoice.getInvoiceNumber());
                    
                    Long documentId = aldInvoice.getDocumentId();
                    
                    Document doc = Document.find(documentId);
                    
                    if(doc instanceof OfficeDocument)
                      ((OfficeDocument) doc).addWorkHistoryEntry(
                       Audit.create("ald", DSApi.context().getPrincipalName(),
                            sm.getString("DokumentOznaczonoJakoZaplacony")));//troche niewlasciwie ze zmieniam 
                    //historie pisma w funkcji ktora generuje pdf, ale trzeba byloby duplikowac sporo kodu.
                    
                    row.add(documentId);
                    
                    row.add("");
                    row.add(doubleRepresentationForPdf((double)(aldInvoice.getAmount())));
                    sumAmount+=aldInvoice.getAmount();
                    content.add(row);
                }
                
                class DocumentNrComparator implements java.util.Comparator
                {
                    public int compare(Object o1, Object o2) {
                        return (((List<Object>)o1).get(2)).toString().compareTo((((List<Object>)o2).get(2)).toString());
                    }   
                }
                
                class SupplierComparator implements java.util.Comparator
                {
                    public int compare(Object o1, Object o2) {
                        return (((List<Object>)o1).get(0)).toString().compareTo((((List<Object>)o2).get(0)).toString());
                    }   
                }
                
                Collections.sort(content, new DocumentNrComparator());
                Collections.sort(content, new SupplierComparator());
                
                List<List<Object>> contentWithPartSum = new ArrayList<List<Object>>();
                
                List<Object> tmpList2=null;
                String supplierNumber = content.get(0).get(0).toString();
                String tmp;
                Double partSum = 0d;
                for(int i=0; i<content.size(); i++)
                {
                    List<Object> tmplist = content.get(i);
                    
                    //pobieramy supplier
                    String tmpSupplier = tmplist.get(0).toString();
                    
                    String parse = tmplist.get(6).toString().replace("\"", "").replace(",", ".").replace(" ", "");
                    Double tmpD = Double.parseDouble(parse);
                    
                    boolean supplierEquals = tmpSupplier.equals(supplierNumber);
                    
                    //caly czas jest ten sam supplierNumber
                    if(supplierEquals)
                    {
                        partSum += tmpD;
                    }
                    else
                    {
                        tmpList2 = new ArrayList<Object>();
                        tmpList2.add("");
                        tmpList2.add("");
                        tmpList2.add("");
                        tmpList2.add("");
                        tmpList2.add("");
                        
                        tmpList2.add(doubleRepresentationForPdf(((double)(Math.round(partSum*100)))/100));
                        tmpList2.add("");
                        contentWithPartSum.add(tmpList2);
                        partSum = tmpD;
                        supplierNumber = tmpSupplier;
                    }
                    
                    contentWithPartSum.add(tmplist);
                }
                
                tmpList2 = new ArrayList<Object>();
                tmpList2.add("");
                tmpList2.add("");
                tmpList2.add("");
                tmpList2.add("");
                tmpList2.add("");
                tmpList2.add(doubleRepresentationForPdf(((double)(Math.round(partSum*100)))/100));
                tmpList2.add("");
                contentWithPartSum.add(tmpList2);
                
                row = new ArrayList<Object>();
                row.add("");
                row.add("");
                row.add("");
                row.add("");
                row.add("");
                row.add("Sum:");
                row.add(doubleRepresentationForPdf(((double)(Math.round(sumAmount*100)))/100));
                contentWithPartSum.add(row);
                
                
                StringBuffer title = new StringBuffer("\n"+sm.getString("RaportUtworzonyPrzez")+" ");
                title.append(DSApi.context().getPrincipalName());
                title.append(".        "+sm.getString("Data")+": ");
                title.append(DateFormat.getDateTimeInstance().format(new Date()));
                
                File pdffile = PdfGenerator.generate(Docusafe.getTemp(), sm.getString("RaportWeryfikacjiFaktur"), title.toString(), widths, columns, contentWithPartSum, PdfGenerator.VERTICAL);
                                
                return pdffile.getName();
        }
        catch(EdmException ee)
        {
        }
        return null;
    }
    
    private static String doubleRepresentationForPdf(double arg)
    {
         
        DecimalFormat df = new DecimalFormat("#0.00");
        String frm = df.format(arg);
        int len = frm.length();
        if(len>6)
            frm = frm.substring(0, len-6) + " " + frm.substring(len-6, len);
        return frm;
    }
    
    public String getSortLink(String sortField, boolean asc)
    {
        return "/repository/ald-payment-verification.action?sortField="+sortField+"&asc="+asc;
    }

    public List<Map<String, Object>> getInvoiceBeans()
    {
        return invoiceBeans;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }
    
    public Map<String, Boolean> getPayments()
    {
        return payments;
    }

    public void setPayments(Map<String, Boolean> payments)
    {
        this.payments = payments;
    }

    public void setAsc(boolean asc)
    {
        this.asc = asc;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }                
    
    public String getDate()
    {
        return date;
    }
    
    public void setDate(String date)
    {
        this.date = date;
    }
    
    public String getPdfUrl()
    {
        return pdfUrl;
    }
    
    public void setPdfUrl(String arg)
    {
        this.pdfUrl = arg;
    }
    
    public void setStreamPdf(String arg)
    {
        streamPdf = arg;
    }

    public String getZaladuj()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("Zaladuj");
    }

}

