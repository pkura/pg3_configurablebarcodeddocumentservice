package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import javax.servlet.http.HttpServletRequest;

/**
 * Edycja atrybut�w folderu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RenameFolderAction.java,v 1.6 2006/02/20 15:42:36 lk Exp $
 */
public class RenameFolderAction extends EventProcessingAction
{
    private static final Log log = LogFactory.getLog(RenameFolderAction.class);

    private static final String FORWARD = "repository/rename-folder";

    private static StringManager sm =
        StringManager.getManager(Constants.Package);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doCancel", new Cancel());

        registerListener("doUpdate").
            append(new SetActionForwardListener("main")).
            append(new ValidateUpdate()).
            append("update", new Update()).
            append("fillForm", fillForm);
    }

    private class Cancel implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long folderId = (Long) event.getDynaForm().get("id");
            event.setForward(new ActionForward(ExploreDocumentsAction.getLink(
                event.getRequest(), folderId), true));
        }
    }

    private class ValidateUpdate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String title = (String) event.getDynaForm().get("title");

            if (StringUtils.isEmpty(title))
                event.getErrors().add(sm.getString("folder.missingTitle"));

            if (event.getErrors().size() > 0)
                event.skip("update");
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String title = ((String) event.getDynaForm().get("title")).trim();
            Long folderId = (Long) event.getDynaForm().get("id");

            boolean success = false;
            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                Folder folder = Folder.find(folderId);

                folder.setTitle(title);

                ctx.commit();

                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }

            if (success)
            {
                event.skip("fillForm");
                event.setForward(new ActionForward(ExploreDocumentsAction.getLink(
                    event.getRequest(), folderId), true));
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long id = (Long) event.getDynaForm().get("id");
            if (id == null)
                return;

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                Folder folder = Folder.find(id);

                event.getDynaForm().set("title", folder.getTitle());
                event.getRequest().setAttribute("folder", folder);
            }
            catch (EdmException e)
            {
                log.error("", e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(HttpServletRequest request, Long folderId)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+"?id="+folderId;
    }
}
