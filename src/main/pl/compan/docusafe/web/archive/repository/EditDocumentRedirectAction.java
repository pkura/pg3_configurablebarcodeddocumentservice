package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Przekierowuj do odpowiedniej akcji edytuj�cej dokument w zale�no�ci od licencji.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 *
 */
public class EditDocumentRedirectAction extends EventActionSupport
{
    private Long id;
    private Long binderId;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String result = null;
            try
            {               
                Document document = Document.find(id,binderId);
                if (document.getDocumentKind() != null)
                {
                    result = "dockind";
                }
            }
            catch (EdmException e)
            {
                LOG.error("B��d wyszukania dokumentu podczas okre�lania formtki edycji dokumentu");
            }
            
            if (result != null)
                event.setResult(result);
        }
    }    
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}

	public Long getBinderId()
	{
		return binderId;
	}    
}
