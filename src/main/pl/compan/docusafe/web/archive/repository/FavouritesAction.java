package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.DocumentWatch;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.SortUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.SortingLinks;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import std.fun;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FavouritesAction.java,v 1.21 2009/01/27 12:27:13 pecet1 Exp $
 */
public final class FavouritesAction extends EventProcessingAction
{
    private static final Log log = LogFactory.getLog(FavouritesAction.class);
    private static  StringManager sm ;
    private static StringManager smL =
            GlobalPreferences.loadPropertiesFile(FavouritesAction.class.getPackage().getName(),null);

    private static final String FORWARD = "repository/favourites";

    public static final String TAB_DOCMOD = "docmod";
    public static final String TAB_ATTADD = "attadd";
    public static final String TAB_ATTDEL = "attdel";
    public static final String TAB_ATTREVADD = "attrevadd";

    public static final String SORT_DOCID = "documentId";
    public static final String SORT_DOCTITLE = "documentTitle";
    public static final String SORT_DOCTIME = "documentCtime";
    public static final String SORT_MTIME = "documentMtime";
    public static final String SORT_LASTNAME = "lastname";
    public static final String SORT_FOLTITLE = "folderTitle";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(setForward).
                append(fillForm);

        registerListener("doRemove").
                append(setForward).
                append(new Remove()).
                append("fillForm", fillForm);
    }

    private final class Remove implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {


            final DynaActionForm form = event.getDynaForm();
            final Long[] watchIds = (Long[]) form.get("watchIds");
            event.getRequest().setAttribute("BUsunZObserwowanych",sm.getString("BUsunZObserwowanych"));

            if (watchIds == null)
                return;

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                for (int i=0; i < watchIds.length; i++)
                {
                    DocumentWatch.deleteWatch(watchIds[i]);
                }

                ctx.commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
                log.error("[Remove] error", e);
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private final class FillForm implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            try {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                sm = GlobalPreferences.loadPropertiesFile("", null);
                DSApi.close();//Constants.Package
            } catch (EdmException e1) {
                log.debug("", e1);
            }

            event.getRequest().setAttribute("BUsunZObserwowanych",sm.getString("BUsunZObserwowanych"));
            final DynaActionForm form = event.getDynaForm();

            String tab = (String) form.get("tab");
            final String sortField = (String) form.get("sortField");

            List tabs = new ArrayList(4);

            // tworzenie listy zak�adek
            String[] allTabs = new String[] { TAB_DOCMOD, TAB_ATTADD, TAB_ATTDEL, TAB_ATTREVADD };
            String[] sortedTabs = (String[]) allTabs.clone();
            boolean ascending = form.get("ascending") != null &&
                    ((Boolean) form.get("ascending")).booleanValue();
            Arrays.sort(sortedTabs);
            if (Arrays.binarySearch(sortedTabs, tab) < 0){
                tab = TAB_DOCMOD;
            }
            for (int i = 0; i < allTabs.length; i++) {
                DynaBean thisTab = DynaBeans.newHtmlLink();
                thisTab.set("title", sm.getString("favourites.tab." + allTabs[i]));
                thisTab.set("link", FavouritesAction.getLink(allTabs[i], sortField, ascending));
                thisTab.set("selected", Boolean.valueOf(allTabs[i].equals(tab)));
                tabs.add(thisTab);
            }

            event.getRequest().setAttribute("tabs", tabs);

            // rodzaj zak�adki na podstawie bie��cej zak�adki
            // (u�ywany przy tworzeniu DynaBeanS)
            final DocumentWatch.Type type;

            if (TAB_DOCMOD.equals(tab)) {
                type = DocumentWatch.Type.DOCUMENT_MODIFIED;
            } else if (TAB_ATTADD.equals(tab)) {
                type = DocumentWatch.Type.ATTACHMENT_ADDED;
            } else if (TAB_ATTDEL.equals(tab)) {
                type = DocumentWatch.Type.ATTACHMENT_DELETED;
            } else if (TAB_ATTREVADD.equals(tab)) {
                type = DocumentWatch.Type.ATTACHMENT_REVISION_ADDED;
            } else {
                type = DocumentWatch.Type.DOCUMENT_MODIFIED;
            }

            try {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DocumentWatch[] watches = DocumentWatch.getWatches();

                if (watches.length > 0) {
                    List<DynaBean> beans = new ArrayList<DynaBean>(fun.map(
                            watches, new lambda<DocumentWatch, DynaBean>()
                    {
                        public DynaBean act(DocumentWatch watch)
                        {
                            if (type != watch.getType())
                                throw new NoSuchElementException();

                            DynaBean bean = DynaBeans.bean(DynaBeans.documentWatch);

                            bean.set("id", watch.getId());
                            bean.set("link", EditDocumentAction.getLink(watch.getDocumentId()));

                            Document document;

                            try
                            {
                                document = Document.find(watch.getDocumentId());

                                bean.set("documentId", document.getId());
                                bean.set("folderTitle", document.getFolderPath());
                                bean.set("documentTitle", document.getTitle());
                                bean.set("documentCtime", document.getCtime());
                                bean.set("documentMtime", document.getMtime());

                                try
                                {
                                    DSUser user = DSUser.findByUsername(document.getAuthor());
                                    bean.set("user", user.asFirstnameLastname());
                                    bean.set("firstname", user.getFirstname());
                                    bean.set("lastname", user.getLastname());
                                }
                                catch (UserNotFoundException e)
                                {
                                    bean.set("user", document.getAuthor());
                                }
                            }
                            catch (DocumentNotFoundException e)
                            {
                                throw new NoSuchElementException();
                            }
                            catch (AccessDeniedException e)
                            {
                                bean.set("documentInaccessible", Boolean.TRUE);
                                bean.set("documentTitle", smL.getString("Dokument")+" "+watch.getDocumentId()+
                                        " ("+smL.getString("BrakDostepu")+")");
                            }
                            catch (DocumentLockedException e)
                            {
                                bean.set("documentInaccessible", Boolean.TRUE);
                                bean.set("documentTitle", smL.getString("Dokument")+" "+watch.getDocumentId()+
                                        " ("+smL.getString("Zablokowany")+")");
                            }
                            catch (EdmException e)
                            {
                                throw new NoSuchElementException();
                            }

                            return bean;
                        }
                    }));

                    // sortowanie
                    final int sortDirection = ascending ? 1 : -1;
                    Collections.sort(beans, new Comparator()
                    {
                        public int compare(Object o1, Object o2)
                        {
                            DynaBean a = (DynaBean) o1;
                            DynaBean b = (DynaBean) o2;

                            int result;

                            if (SORT_DOCTIME.equals(sortField)) {
                                result = SortUtils.compare((Date) a.get("documentCtime"), (Date) b.get("documentCtime"));
                            } else if (SORT_LASTNAME.equals(sortField)) {
                                result = SortUtils.compare((String) a.get("lastname"), (String) b.get("lastname"));
                            } else if (SORT_FOLTITLE.equals(sortField)) {
                                result = SortUtils.compare((String) a.get("folderTitle"), (String) b.get("folderTitle"));
                            } else if (SORT_DOCID.equals(sortField)) {
                                result = SortUtils.compare((Long) a.get("documentId"), (Long) b.get("documentId"));
                            } else if (SORT_MTIME.equals(sortField)) {
                                result = SortUtils.compare((Date) a.get(SORT_MTIME), (Date) b.get(SORT_MTIME));
                            } else {
                                result = 0;
                            }

                            // domy�lnie sortowanie po tytule, r�wnie� wtedy,
                            // gdy por�wnywane pola s� identyczne
                            if (result == 0)
                            {
                                result = SortUtils.compare((String) a.get("documentTitle"), (String) b.get("documentTitle"));
                            }

                            return result * sortDirection;
                        }
                    });

                    event.getRequest().setAttribute("watches", beans);
                    event.getRequest().setAttribute("sorting", new SortingLinksImpl(tab));
                }
            }
            catch (EdmException e) {
                event.getErrors().add(e.getMessage());
            } finally {
                DSApi._close();
            }
        }
    }

    static class SortingLinksImpl extends SortingLinks
    {
        private String tab;

        public SortingLinksImpl(String tab)
        {
            this.tab = tab;
        }

        public Object get(Object key)
        {
            if ("documentTitle_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCTITLE, true);
            }
            else if ("documentTitle_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCTITLE, false);
            }
            else if ("documentCtime_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCTIME, true);
            }
            else if ("documentCtime_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCTIME, false);
            }
            else if ("documentMtime_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_MTIME, true);
            }
            else if ("documentMtime_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_MTIME, false);
            }
            else if ("lastname_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_LASTNAME, true);
            }
            else if ("lastname_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_LASTNAME, false);
            }
            else if ("folderTitle_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_FOLTITLE, true);
            }
            else if ("folderTitle_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_FOLTITLE, false);
            }
            else if ("documentId_asc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCID, true);
            }
            else if ("documentId_desc".equals(key))
            {
                return FavouritesAction.getLink(tab, SORT_DOCID, false);
            }
            else
            {
                throw new IllegalArgumentException(smL.getString("NieznanePoleSortowania")+": "+key);
            }
        }
    }

    public static String getLink()
    {
        final ModuleConfig config = (ModuleConfig)
                Docusafe.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }

    public static String getLink(String tab, String sortField, boolean ascending)
    {
        final ModuleConfig config = (ModuleConfig)
                Docusafe.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
                "?tab="+tab +
                (sortField != null ? "&sortField="+sortField : "") +
                "&ascending="+ascending;
    }
}