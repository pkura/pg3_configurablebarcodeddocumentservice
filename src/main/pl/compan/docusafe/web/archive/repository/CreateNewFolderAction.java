package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

/**
 *  Klasa służy do tworzenia nowego folderu
 *
 *    Date: 4.07.2013
 *    Time: 11:30
 *
 *    @author Magorzata Wierzgała
 *
 */
public class CreateNewFolderAction extends EventActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(CreateNewFolderAction.class);

    private static final StringManager sm = StringManager.getManager(pl.compan.docusafe.web.archive.users.Constants.Package);

    /**
     * Nazwa nowego folderu
     */
    private String newFolderName;

    /**
     * ID folderu
     */
    private Long folderId;

    /**
     * Katalog, w ktorym znajduje sie plik
     */
    private String folderPath;


    @Override
    protected void setup() {

        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
                append(OpenHibernateSession.INSTANCE).
                append(new DoCreate()).
                appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event) {

            Folder parentFolder = null;
            log.error(" Folder: " + folderId);

            if(folderId!=null){
                try {
                    parentFolder = Folder.find(folderId);
                } catch (EdmException e) {
                    addActionError(sm.getString("NieMaTakiegoFolderu"));
                }
                try {
                    folderPath = parentFolder.getPrettyPath();
                } catch (EdmException e1) {
                    addActionError(sm.getString("NieMoznaOdczytacSciezki"));
                }
            }else {
                addActionMessage(sm.getString("BrakId"));
            }
        }
    }

    private class DoCreate implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

            Folder folder = null;

            try {
                if (folderId != null) {
                    folder = Folder.find(folderId);
                }
                else
                {
                    folder = Folder.getRootFolder();
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                addActionError(sm.getString("NieZnalezionoFolderu"));
            }

            try {
                if (StringUtils.isEmpty(newFolderName)) {
                    log.info("pusty tytul");
                    addActionError(sm.getString("FolderMusiMiecTytul"));
                }else{
                    Folder newFolder = folder.createSubfolder(newFolderName);
                    DSApi.context().session().save(newFolder);
                    // flush, aby w tabeli pojawil sie wiersz z nowym id
                    DSApi.context().session().flush();
                    addActionMessage(sm.getString("StworzonoFolder"));
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                addActionError(sm.getString("BrakMozliwosciStworzeniaFolderu"));
                addActionError(sm.getString("TakiFolderJuzIstnieje"));
            }


        }
    }

    public String getNewFolderName() {
        return newFolderName;
    }

    public void setNewFolderName(String newFolderName) {
        this.newFolderName = newFolderName;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
}
