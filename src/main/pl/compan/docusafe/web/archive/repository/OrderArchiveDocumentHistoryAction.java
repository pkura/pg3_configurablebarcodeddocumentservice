package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.archive.DocumentOrderManager;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.ProsikaArchiveDocument;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author Mariusz Kilja�czyk
 * @version $Id: OrderArchiveDocumentHistoryAction.java,v 1.9 2010/08/04 06:59:29 mariuszk Exp $
 */
public class OrderArchiveDocumentHistoryAction extends EventActionSupport
{
	final static Logger log = LoggerFactory.getLogger(OrderArchiveDocumentHistoryAction.class);
	private Long id;
	private Long orderId;
	private List<OrderHistoryBean> orderHistory;
	private List<Document> documents;
	private Boolean canAddDocument;
	private String remark;
	private String boxNumber;
	
 	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doOrder").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Show()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  

        }
    }
	
    public class OrderHistoryBean
    {
        private Date date;
        private String author;
        private String content;


        public Date getDate()
        {
            return date;
        }

        public String getAuthor()
        {
            return author;
        }

        public String getContent()
        {
            return content;
        }
    }

    private class Show implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  
        }
    }    	
    
    public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getOrderId() {
		return orderId;
	}


	public void setOrderHistory(List<OrderHistoryBean> orderHistory) {
		this.orderHistory = orderHistory;
	}

	public List<OrderHistoryBean> getOrderHistory() {
		return orderHistory;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setCanAddDocument(Boolean canAddDocument) {
		this.canAddDocument = canAddDocument;
	}

	public Boolean getCanAddDocument() {
		return canAddDocument;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public String getBoxNumber() {
		return boxNumber;
	}
}
