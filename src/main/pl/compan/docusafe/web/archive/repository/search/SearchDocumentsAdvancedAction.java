package pl.compan.docusafe.web.archive.repository.search;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.event.*;

import java.io.*;
import java.text.ParseException;
import java.util.*;

/* User: Administrator, Date: 2005-06-02 13:24:53 */

/**
 * Zaawansowane wyszukiwanie dokument�w na podstawie typu (Doctype).
 * <p>
 * Uwaga - wyszukiwarka powinna szuka� zawsze dokument�w jednego typu, przed
 * rozszerzeniem tej funkcjonalno�ci nale�y zmodyfikowa� kod tworz�cy dodatkowe
 * kolumny w wynikach wyszukiwania.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchDocumentsAdvancedAction.java,v 1.55 2009/08/11 07:17:18 wkuty Exp $
 * @deprecated - powinna byc stosowana SearchDockindDocumentsAction
 */
@Deprecated
public class SearchDocumentsAdvancedAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(SearchDocumentsAdvancedAction.class);
	private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(SearchDocumentsAdvancedAction.class.getPackage().getName(),null);

    private static final int LIMIT = 10;

    private boolean attachmentsAsPdf;
    private String attachmentsAsPdfUrl;

    private boolean resultsInPopup;

    private Long doctypeId;
    private int offset;
    private boolean ascending;
    private String sortField;
    private int limit;
    private String boxName;
    private String accessedBy;
    private String[] accessedAs;
    private String accessedFrom;
    private String accessedTo;

    // @EXPORT
    private List doctypes;
    private List doctypeFields;
    private List<Map<String, Object>> results;
    private List<TableColumn> columns;
    private Pager pager;
    private List<DSUser> accessedByUsers;

    private String thisSearchUrl;
    private String agencja_nazwa, agencja_numer, agencja_nip, agent_imie, agent_nazwisko, agent_numer;

    private boolean canReadDAASlowniki;
    private Long documentId; 

    private static final String EV_FILL = "fillForm";

    // mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy
    // wynik�w (w klasie QueryForm)
    private static final Map<String, String> sortProperties = new HashMap<String, String>();
    static
    {
        sortProperties.put("document_id", "id");
        sortProperties.put("document_title", "title");
        sortProperties.put("document_ctime", "ctime");
        sortProperties.put("document_mtime", "mtime");
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeDoctype").
            append(OpenHibernateSession.INSTANCE).
            append(new ChangeDoctype()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                doctypes = Doctype.list(true);

                if (doctypeId != null)
                {
                    Doctype doctype = Doctype.find(doctypeId);
                    doctypeFields = Arrays.asList(doctype.getFieldsAsArray());
                }

                canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);

                if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    accessedByUsers = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA))
                {
                    // tylko z dzia�u
                    accessedByUsers = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    accessedByUsers = new ArrayList<DSUser>(1);
                    accessedByUsers.add(DSApi.context().getDSUser());
                }
            }
            catch (EdmException e)
            {
                event.addActionError(e.getMessage());
            }
        }
    }

    private class ChangeDoctype implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
/*
            if (doctypeId == null)
                return;

            try
            {
                Doctype doctype = Doctype.find(doctypeId);
                doctypeFields = Arrays.asList(doctype.getFieldsAsArray());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
*/
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
            if (resultsInPopup)
                event.setResult("popup");

            if (doctypeId == null)
                addActionError(sm.getString("NieWybranoTypuDokumentu"));

            if (hasActionErrors())
                return;

            Map params = ActionContext.getContext().getParameters();


            Map<Long, DaaAgencja> agencje = new HashMap<Long, DaaAgencja>();
            Map<Long, DaaAgent> agenci = new HashMap<Long, DaaAgent>();


            try {
                List<DaaAgent> lista_agenci = null;
                List<DaaAgencja> lista_agencje = null;
                lista_agenci = DaaAgent.findAll();
                lista_agencje = DaaAgencja.findAll();
                for(DaaAgent a : lista_agenci) {
                    agenci.put(a.getId(), a);
                }
                for(DaaAgencja a : lista_agencje) {
                    agencje.put(a.getId(), a);
                }

            } catch (EdmException e) {
                event.addActionError(e.getMessage());
            }


            if (sortField == null)
                sortField = "id";

            boxName = TextUtils.trimmedStringOrNull(boxName);
            accessedBy = TextUtils.trimmedStringOrNull(accessedBy);

            limit = limit > 0 ? limit : LIMIT;

            try
            {
                Document.DoctypeQuery doctypeQuery = new Document.DoctypeQuery(offset, limit);

                if (accessedBy != null || accessedAs != null)
                {
                    if (accessedBy != null)
                    {
                        doctypeQuery.accessedBy(accessedBy);
                    }
                
	                if (accessedAs != null)
	                {
	                    doctypeQuery.enumAccessedAs(accessedAs);
	                }
	
	                /* jak wybrany tylko zakres dolny, to zakresem gornym tez bedzie zakres dolny */
	                Date accFrom = DateUtils.nullSafeParseJsDate(accessedFrom);
	                Date accTo = DateUtils.nullSafeParseJsDate(accessedTo);
	
	                doctypeQuery.accessedFromTo(
	                    DateUtils.nullSafeMidnight(accFrom, 0),
	                    DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
                }

                // pud�o archiwalne
                if (boxName != null)
                {
                    Box box = Box.findByName(null,boxName);
                    doctypeQuery.boxId(box.getId());
                }

                Doctype doctype = Doctype.find(doctypeId);

                // warto�ci p�l formularza do odno�nika wyszukiwania
                final Map fieldValues = new HashMap();

                boolean checkPermissions = true;

                doctypeQuery.doctype(doctypeId);
                Doctype.Field[] dfields = doctype.getFieldsAsArray();
                for (int i=0; i < dfields.length; i++)
                {
                    String[] values = (String[]) params.get("field_"+dfields[i].getId());
                    if ("AGENT".equals(dfields[i].getCn()))
                        values = (String[]) params.get("agent_id");
                    if ("AGENCJA".equals(dfields[i].getCn()))
                         values = (String[]) params.get("agencja_id");
                    if ("NR_POLISY".equals(dfields[i].getCn()))
                    {
                        if (values != null && values.length > 0 && values[0] != null)
                        {
                            checkPermissions = doctypeQuery.checkPermissions(values[0]);
                            if (values[0].length()>=10)
                            {
                            	log.trace("Polisa ma {} znakow wymuszamy szukanie equal",values[0].length());
                            	doctypeQuery.addForceSearchEq("NR_POLISY");
                            }
                        }
                        
                    }


                    if (values != null && values.length > 0)
                    {
                        if (dfields[i].getType().equals(Doctype.Field.ENUM))
                        {
                            Integer[] enums = new Integer[values.length];
                            for (int e=0; e < values.length; e++)  {
                                try {enums[e] = new Integer(values[e]);} catch(NumberFormatException ex) {}
                            }
                            doctypeQuery.enumField(dfields[i].getId(), enums);
                            fieldValues.put(dfields[i].getId(), enums);
                        }
                        else
                        {
                            String value = TextUtils.trimmedStringOrNull(values[0]);
                            if (value != null)
                            {
                                doctypeQuery.field(dfields[i].getId(), value);
                                fieldValues.put(dfields[i].getId(), value);

                             /*   if ("AGENCJA".equals(dfields[i].getCn()))
                                {
                                    doctypeQuery.agencjaId(value);
                                } */
                            }
                        }
                        continue;
                    }

                    values = (String[]) params.get("field_"+dfields[i].getId()+":from");
                    if (values != null && values.length > 0)
                    {
                        String value = TextUtils.trimmedStringOrNull(values[0]);
                        if (value != null)
                        {
                            doctypeQuery.fieldFrom(dfields[i].getId(), value);
                            fieldValues.put(dfields[i].getId()+":from", value);
                        }
                    }

                    values = (String[]) params.get("field_"+dfields[i].getId()+":to");
                    if (values != null && values.length > 0)
                    {
                        String value = TextUtils.trimmedStringOrNull(values[0]);
                        if (value != null)
                        {
                            doctypeQuery.fieldTo(dfields[i].getId(), value);
                            fieldValues.put(dfields[i].getId()+":to", value);
                        }
                    }


                }
                if(agent_imie != null)
                    doctypeQuery.agentImie(agent_imie);
                if(agent_nazwisko != null)
                    doctypeQuery.agentNazwisko(agent_nazwisko);
                if(agencja_nazwa != null)
                    doctypeQuery.agencjaNazwa(agencja_nazwa);
                if(agencja_numer != null)
                    doctypeQuery.agencjaNumer(agencja_numer);
                if(agencja_nip != null)
                    doctypeQuery.agencjaNip(agencja_nip);
                if(agent_numer != null)
                    doctypeQuery.agentNumer(agent_numer);

                if (ascending)
                    doctypeQuery.orderAsc(sortField);
                else
                    doctypeQuery.orderDesc(sortField);
                doctypeQuery.checkSortField(sortField);

                // sprawdzanie czy wybrano "wymuszenie" szukania "Eq" dla niektorych pol
                String[] forceSearchEq = (String[]) params.get("forceSearchEq");
                if (forceSearchEq != null && forceSearchEq.length > 0)
                {
                    for (String fieldCn : forceSearchEq)
                        doctypeQuery.addForceSearchEq(fieldCn);
                }

                // url celowo bez sortField, bo ten parametr b�dzie dodany p�niej
                // w getSortLink()
                thisSearchUrl = getLink(offset, limit, null, null, doctypeId, fieldValues, boxName,
                    accessedBy, accessedAs, accessedFrom, accessedTo,
                    agencja_nazwa, agencja_nip, agencja_numer, agent_nazwisko, agent_imie, agent_numer, resultsInPopup);
                attachmentsAsPdfUrl = thisSearchUrl + "&attachmentsAsPdf=true";

                SearchResults searchResults = Document.searchByDoctype(doctypeQuery);

                if (attachmentsAsPdf)
                {
                    DSApi.context().begin();

                    File tmp = null;
                    try
                    {
                        Document[] documents = (Document[]) searchResults.results();
                        tmp = File.createTempFile("docusafe_", ".pdf");
                        OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                        PdfUtils.attachmentsAsPdf(documents, false, os);
                        os.close();
                    }
                    catch (Exception e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                    finally
                    {
                        DSApi.context().setRollbackOnly();
                    }

                    if (tmp != null)
                    {
                        event.setResult(NONE);
                        event.skip(EV_FILL);
                        try
                        {
                            ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/pdf");
                        }
                        catch (IOException e)
                        {
                            event.getLog().error(e.getMessage(), e);
                        }
                        finally
                        {
                            tmp.delete();
                        }
                    }

                    DSApi.context().rollback();

                    return;
                }

                if (searchResults.count() == 0)
                {
                    addActionMessage(sm.getString("NieZnalezionoDokumentow"));
                }
                else
                {

                    columns = getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", SearchDocumentsAction.DEFAULT_COLUMNS));
                    List<String> columnNames = getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", SearchDocumentsAction.DEFAULT_COLUMNS));


                    // warto�� tej zmiennej zostanie wype�niona na podstawie pierwszego
                    // znalezionego dokumentu; zak�adam, �e ka�dy dokument jest tego
                    // samego typu
                    Doctype commonDoctype = null;
                    Doctype.Field[] commonDoctypeFields = null;

                    results = new ArrayList(searchResults.totalCount());

                    int docCount = 0;

                    while (searchResults.hasNext())
                    {
                        Document document = (Document) searchResults.next();

                        if (!checkPermissions && !document.canReadXXX())
                            continue;

                        docCount++;

                        if (commonDoctype == null)
                        {
                            commonDoctype = document.getDoctype();
                            if (commonDoctype != null)
                            {
                                commonDoctypeFields = commonDoctype.getFieldsAsArray();
                            }
                        }

                        Map<String, Object> bean = new HashMap<String, Object>();
                        bean.put("link", "/repository/edit-document.action?id="+document.getId());
                        bean.put("document_id", document.getId());
                        bean.put("document_title", document.getTitle());
                        bean.put("document_description", document.getDescription());
                        bean.put("document_ctime", document.getCtime());
                        bean.put("document_mtime", document.getMtime());
                        if (columnNames.contains("document_author"))
                        {
                            bean.put("document_author", DSUser.safeToFirstnameLastname(document.getAuthor()));
                        }

                        bean.put("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));

                        if (columnNames.contains("attachment_link1"))
                        {
                            List atts = document.listAttachments();
                            if (atts.size() > 0)
                            {
                                // wy�wietlany jest najnowszy za��cznik, ale
                                // nale�y pomin�� za��czniki specjalnego
                                // znaczenia (cn != null)
                                int pos = atts.size() - 1;
                                Attachment att = ((Attachment) atts.get(pos));
                                while (att != null && att.getCn() != null && pos > 0)
                                {
                                    att = ((Attachment) atts.get(--pos));
                                }
                                if (att != null)
                                {
                                    AttachmentRevision rev = att.getMostRecentRevision();
                                    if (rev != null)
                                    {
                                        bean.put("attachment_link1", ViewAttachmentRevisionAction.getLink(
                                                ServletActionContext.getRequest(), rev.getId()));
                                        if (ViewServer.mimeAcceptable(rev.getMime()))
                                            bean.put("attachment_link_vs", "/viewserver/viewer.action?id="+rev.getId());
                                    }
                                }
                            }
                        }

                        results.add(bean);
                    }

                    if (docCount > 0)
                    {
                        event.skip(EV_FILL);

                        Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                        {
                            public String getLink(int offset)
                            {
                                return SearchDocumentsAdvancedAction.getLink(offset, limit, sortField, Boolean.valueOf(ascending),
                                    doctypeId, fieldValues, boxName, accessedBy, accessedAs, accessedFrom, accessedTo,
                                    agencja_nazwa, agencja_nip, agencja_numer, agent_nazwisko, agent_imie, agent_numer, resultsInPopup);
                            }
                        };
                        pager = new Pager(linkVisitor, offset, limit, searchResults.totalCount(), 10);
                    }
                    else
                    {
                        addActionMessage(sm.getString("NieZnalezionoDokumentow"));
                    }

                }
            }
            catch (EdmException e)
            {
                event.addActionError(e.getMessage());
            }
        }
    }

/*
    private String getFirstValue(Map params, Integer fieldId)
    {
        String[] values = (String[]) params.get("field_"+fieldId);
        if (values != null && values.length > 0)
        {
            return values[0];
        }

        values = (String[]) params.get("field_"+fieldId+":from");
    }
*/

    private List<String> getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(SearchDocumentsAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }

    /**
     * Zwraca list� obiekt�w TableColumn.
     * @param columnsString
     */
    private List<TableColumn> getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.documents.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(property, desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null));
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(SearchDocumentsAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }

    public String getSortLink(String sortField, boolean ascending)
    {
        return thisSearchUrl +
            (thisSearchUrl.indexOf('?') > 0 ? "&" : "?") +
            "sortField="+sortField+
            "&ascending="+ascending;
    }

    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            return DateUtils.formatJsDateTime((Date) object);
        }
        else
        {
            return object.toString();
        }
    }

    public List getDoctypes()
    {
        return doctypes;
    }

    public Long getDoctypeId()
    {
        return doctypeId;
    }

    public void setDoctypeId(Long doctypeId)
    {
        this.doctypeId = doctypeId;
    }

    public List getDoctypeFields()
    {
        return doctypeFields;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public List getResults()
    {
        return results;
    }

    public List getColumns()
    {
        return columns;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public void setBoxName(String boxName)
    {
        this.boxName = boxName;
    }

    public void setAccessedBy(String accessedBy)
    {
        this.accessedBy = accessedBy;
    }

    public void setAccessedAs(String[] accessedAs)
    {
        this.accessedAs = accessedAs;
    }

    public List getAccessedByUsers()
    {
        return accessedByUsers;
    }

    public void setAttachmentsAsPdf(boolean attachmentsAsPdf)
    {
        this.attachmentsAsPdf = attachmentsAsPdf;
    }

    public String getAttachmentsAsPdfUrl()
    {
        return attachmentsAsPdfUrl;
    }

    public String getAccessedFrom()
    {
        return accessedFrom;
    }

    public void setAccessedFrom(String accessedFrom)
    {
        this.accessedFrom = accessedFrom;
    }

    public String getAccessedTo()
    {
        return accessedTo;
    }

    public void setAccessedTo(String accessedTo)
    {
        this.accessedTo = accessedTo;
    }

    public String getAgencja_nazwa() {
        return agencja_nazwa;
    }

    public void setAgencja_nazwa(String agencja_nazwa) {
        this.agencja_nazwa = agencja_nazwa;
    }

    public String getAgencja_numer() {
        return agencja_numer;
    }

    public void setAgencja_numer(String agencja_numer) {
        this.agencja_numer = agencja_numer;
    }

    public String getAgencja_nip() {
        return agencja_nip;
    }

    public void setAgencja_nip(String agencja_nip) {
        this.agencja_nip = agencja_nip;
    }

    public String getAgent_imie() {
        return agent_imie;
    }

    public void setAgent_imie(String agent_imie) {
        this.agent_imie = agent_imie;
    }

    public String getAgent_nazwisko() {
        return agent_nazwisko;
    }

    public void setAgent_nazwisko(String agent_nazwisko) {
        this.agent_nazwisko = agent_nazwisko;
    }

    public String getAgent_numer() {
        return agent_numer;
    }

    public void setAgent_numer(String agent_numer) {
        this.agent_numer = agent_numer;
    }

    public static String getLink(int offset, int limit, String sortField, Boolean ascending,
                                 Long doctypeId, Map fields, String boxName,
                                 String accessedBy, String[] accessedAs, String accessedFrom, String accessedTo,
                                 String agencja_nazwa, String agencja_nip, String agencja_numer, String agent_nazwisko, String agent_imie, String agent_numer,
                                 boolean popup)
    {
        StringBuilder link = new StringBuilder("/repository/search-documents-advanced.action" +
            "?doSearch=true" +
            (boxName != null ? "&boxName="+boxName : "") +
            "&offset="+offset+
            "&limit="+limit+
            (agencja_nazwa != null ? "&agencja_nazwa="+agencja_nazwa : "") +
            (agencja_numer != null ? "&agencja_numer="+agencja_numer : "") +
            (agencja_nip != null ? "&agencja_nip="+agencja_nip : "") +
            (agent_imie != null ? "&agent_imie="+agent_imie : "") +
            (agent_nazwisko != null ? "&agent_nazwisko="+agent_nazwisko : "") +
            (agent_numer != null ? "&agent_numer="+agent_numer : "") +
            (sortField != null ? "&sortField="+sortField : "") +
            (ascending != null ? "&ascending="+ascending : "") +
            (doctypeId != null ? "&doctypeId="+doctypeId : "") +
            (accessedFrom != null ? "&accessedFrom="+accessedFrom : "") +
            (accessedTo != null ? "&accessedTo="+accessedTo : "") +
            (accessedBy != null ? "&accessedBy="+accessedBy : "") +
            (popup ? "&resultsInPopup=true" : ""));
        if (accessedAs != null)
        {
            for (int i=0; i < accessedAs.length; i++)
            {
                link.append("&accessedAs=");
                link.append(accessedAs[i]);
            }
        }
        if (fields != null)
        {
            for (Iterator iter=fields.entrySet().iterator(); iter.hasNext(); )
            {
                Map.Entry e = (Map.Entry) iter.next();
                if (e.getValue() instanceof Object[])
                {
                    Object[] o = (Object[]) e.getValue();
                    for (int i=0; i < o.length; i++)
                    {
                        link.append("&field_");
                        link.append(e.getKey());
                        link.append("=");
                        link.append(o[i]);
                    }
                }
                else
                {
                    link.append("&field_");
                    link.append(e.getKey());
                    link.append("=");
                    link.append(e.getValue());
                }
            }
        }
        return link.toString();
    }

    public boolean isCanReadDAASlowniki() {
        return canReadDAASlowniki;
    }

    public void setCanReadDAASlowniki(boolean canReadDAASlowniki) {
        this.canReadDAASlowniki = canReadDAASlowniki;
    }

    public void setResultsInPopup(boolean resultsInPopup)
    {
        this.resultsInPopup = resultsInPopup;
    }

    public boolean getResultsInPopup()
    {
        return resultsInPopup;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
    
    
}
