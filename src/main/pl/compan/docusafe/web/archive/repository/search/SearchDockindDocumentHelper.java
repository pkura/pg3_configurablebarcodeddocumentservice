package pl.compan.docusafe.web.archive.repository.search;

import com.google.common.primitives.Ints;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.DaaLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;

import javax.servlet.http.HttpSession;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.util.*;

/**
 * Klasa pomocnicza do obs�ugi klasy @see SearchDockindDocumentsAction
 *
 *
 */
public class SearchDockindDocumentHelper {

    private static final Logger log = LoggerFactory.getLogger(SearchDockindDocumentHelper.class);
    static StringManager sm = GlobalPreferences.loadPropertiesFile(SearchDockindDocumentHelper.class.getPackage().getName(), null);

    public static SearchDockindDocumentHelper instance(){
        return new SearchDockindDocumentHelper();
    }

    public void search(final SearchDockindDocumentsAction action, ActionEvent event) throws EdmException {
        try
        {
            action.prepOrder();

            DockindQuery dockindQuery;

            if(AvailabilityManager.isAvailable("saveSearch") && action.getSaveSearchName() != null && action.getSaveSearchName().length() > 0) {
                String documentKindCnStr = DSApi.context().userPreferences().node("search").node(action.getSaveSearchName()).get("documentKindCn",action.documentKindCn);
                if(StringUtils.isNotBlank(action.getDocumentKindCn()))
                    action.documentKindCn = documentKindCnStr;
            }

            action.kind = DocumentKind.findByCn(action.documentKindCn);

            if(action.omitLimit) {
                dockindQuery = new DockindQuery(action.offset, 0);
            } else {
                dockindQuery = new DockindQuery(action.offset, action.limit);
            }

            if(!StringUtils.isBlank(action.getDescription())){
                if(AvailabilityManager.isAvailable("seachDockindDocument.LikeDescription")) {
                    dockindQuery.likeDocumentDescription("%"+action.getDescription().trim()+"%");
                    log.info("documentDescription");
                } else {
                    dockindQuery.documentDescription(action.getDescription().trim());
                    log.info("documentDescription");
                }
            }
            if(!StringUtils.isBlank(action.getAbstractDescription())){
            	 dockindQuery.likeDocumentAbstractDescription("%"+action.getAbstractDescription().trim()+"%");
            }
            Long parsedOfficeNumber;
            if(AvailabilityManager.isAvailable("wyszukiwarka.IDString") && StringUtils.isNotBlank(action.getOfficeNumber()))
                Long.parseLong(action.getOfficeNumber());
            if (action.getOfficeNumber() != null && (parsedOfficeNumber = Long.parseLong(action.getOfficeNumber())) != null) {
                dockindQuery.officeNumber(parsedOfficeNumber,action.kind.getDockindInfo().isForType(DocumentType.INCOMING));
            }


            if (AvailabilityManager.isAvailable("searchDockindDocument.officeNumberFromTo") ){
                Integer parsedOfficeNumberFrom = null;
                Integer parsedOfficeNumberTo = null;
                if (action.getOfficeNumberFrom()!=null && Ints.tryParse(action.getOfficeNumberFrom()) != null)
                    parsedOfficeNumberFrom = Ints.tryParse(action.getOfficeNumberFrom());
                if (action.getOfficeNumberTo()!=null && Ints.tryParse(action.getOfficeNumberTo()) != null)
                    parsedOfficeNumberTo = Ints.tryParse(action.getOfficeNumberTo());

                dockindQuery.officeNumberFromTo(parsedOfficeNumberFrom, parsedOfficeNumberTo ,action.kind.getDockindInfo().isForType(DocumentType.INCOMING));
            }

            if (action.getOfficeNumberYear() != null ){
                dockindQuery.officeNumberYear(action.officeNumberYear ,action.kind.getDockindInfo().isForType(DocumentType.INCOMING));
            }else if(AvailabilityManager.isAvailable("searchDockindDocument.currentYearDefault")){
                action.officeNumberYear = (long) GlobalPreferences.getCurrentYear();
                dockindQuery.officeNumberYear(action.officeNumberYear ,action.kind.getDockindInfo().isForType(DocumentType.INCOMING));
            }
            if (action.getDocumentOfficeCaseSymbol() != null ){
             
                if (action.kind.getDockindInfo().isForType(DocumentType.INCOMING))
                    dockindQuery.likeOfficeCaseSymbol("in",removePercentExpresion(action.getDocumentOfficeCaseSymbol()));
                else dockindQuery.likeOfficeCaseSymbol("out",removePercentExpresion(action.getDocumentOfficeCaseSymbol()));
            }

            if (action.getDocumentReferenceId() != null ){

                if (action.kind.getDockindInfo().isForType(DocumentType.INCOMING))
                    dockindQuery.likedocumentReferenceId("in", removePercentExpresion(action.getDocumentOfficeCaseSymbol()));
                else dockindQuery.likedocumentReferenceId("out", removePercentExpresion(action.getDocumentOfficeCaseSymbol()));

            }

            if(!StringUtils.isBlank(action.getPostalRegNumber())){
                if(action.kind.getDockindInfo().isForType(DocumentType.INCOMING)){
                    dockindQuery.inOfficePostalRegNumber(action.getPostalRegNumber().trim());
                } else {
                    dockindQuery.outOfficePostalRegNumber(action.getPostalRegNumber().trim());
                }
                log.info("postalRegNumber = {}", action.getPostalRegNumber());
            }

            if(action.getJournalId() != null) {
                dockindQuery.journalId(action.getJournalId());
            }

            if (action.recipientSC.isAnyValue()){
                dockindQuery.personAttributes(action.recipientSC.getTableName(),action.recipientSC.getTableValue(),action.recipientSC.getColumnValue() ,action.recipientSC);
                HttpSession session = ServletActionContext.getRequest().getSession();
                session.setAttribute("recipientMap", action.recipientSC.clone());
                Object obj = session.getAttribute("recipientMap");
            }
            if (action.senderSC.isAnyValue()){
                dockindQuery.personAttributes(action.senderSC.getTableName(),action.senderSC.getTableValue(),action.senderSC.getColumnValue(),action.senderSC);
                HttpSession session = ServletActionContext.getRequest().getSession();
                session.setAttribute("recipientMap", action.senderSC.clone());
                Object obj = session.getAttribute("recipientMap");
            }

            if(action.getDocumentDateFrom() != null || action.getDocumentDateTo() != null){
                if(action.getDocumentDateFrom()!=null&&AvailabilityManager.isAvailable("wyszukiwarka.walidacjaDocumentDate")){
                    if(DateUtils.parseDateAnyFormat(action.getDocumentDateFrom()).before(DateUtils.parseDateAnyFormat("1900-1-1"))||DateUtils.parseDateAnyFormat(action.getDocumentDateFrom()).after(DateUtils.parseDateAnyFormat("9999-1-1")))
                        throw action.new InvalidSqlRangeDate();
                }
                if(action.getDocumentDateTo()!=null&&AvailabilityManager.isAvailable("wyszukiwarka.walidacjaDocumentDate")){
                    if(DateUtils.parseDateAnyFormat(action.getDocumentDateTo()).before(DateUtils.parseDateAnyFormat("1900-1-1"))||DateUtils.parseDateAnyFormat(action.getDocumentDateTo()).after(DateUtils.parseDateAnyFormat("9999-1-1")))
                        throw action.new InvalidSqlRangeDate();
                }
                if(action.kind.getDockindInfo().isForType(DocumentType.INCOMING)){
                    log.info("inOfficeDocumentDate {} {}",action.getDocumentDateFrom(), action.getDocumentDateTo());
                    dockindQuery.inOfficeDocumentDate(
                            DateUtils.nullSafeParseJsDate(action.getDocumentDateFrom()),
                            DateUtils.nullSafeParseJsDate(action.getDocumentDateTo()));
                } else {
                    log.info("outOfficeDocumentDate {} {}",action.getDocumentDateFrom(), action.getDocumentDateTo());
                    dockindQuery.outOfficeDocumentDate(
                            DateUtils.nullSafeParseJsDate(action.getDocumentDateFrom()),
                            DateUtils.nullSafeParseJsDate(action.getDocumentDateTo()));
                }
            }

            if (action.accessedBy != null || action.accessedAs != null)
            {
                if (action.accessedBy != null)
                {
                    dockindQuery.accessedBy(action.accessedBy);
                }

                if (action.accessedAs != null)
                {
                    dockindQuery.enumAccessedAs(action.accessedAs);
                }

                // jak wybrany tylko zakres dolny, to zakresem gornym tez bedzie zakres dolny
                Date accFrom = DateUtils.nullSafeParseJsDate(action.getAccessedFrom());
                Date accTo = DateUtils.nullSafeParseJsDate(action.getAccessedTo());

                dockindQuery.accessedFromTo(
                        DateUtils.nullSafeMidnight(accFrom, 0),
                        DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
            }
            if(action.getAcceptedBy() != null){
                dockindQuery.acceptedBy(action.getAcceptedBy());
            }
            action.kind = DocumentKind.findByCn(action.documentKindCn);
            action.kind.initialize();

            if(action.getCtimeFrom() != null || action.getCtimeFrom() != null) {
                Date ctimeFromDate = DateUtils.nullSafeParseJsDate(action.getCtimeFrom());
                Date ctimeToDate = DateUtils.nullSafeParseJsDate(action.getCtimeTo());
                dockindQuery.ctime(ctimeFromDate, ctimeToDate);
            }

            if (action.isNotViewed() || action.getViewUsers() != null || action.getViewtimeFrom() != null || action.getViewtimeTo() != null) {
                Date viewFrom = DateUtils.nullSafeParseJsDate(action.getViewtimeFrom());
                Date viewTo = DateUtils.nullSafeParseJsDate(action.getViewtimeTo());

                dockindQuery.viewed(action.isNotViewed(), action.getViewUsers(), viewFrom, viewTo);
            }

            if (action.boxName != null) {
                Box box = Box.findByName(action.kind.getProperties().get(DocumentKind.BOX_LINE_KEY), action.boxName);
                dockindQuery.boxId(box.getId());
            }

            if(action.getAuthor() != null)
            {
                dockindQuery.author(action.getAuthor());
            }

            if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
                dockindQuery.czyAktualny();
            }

            boolean readSearchCriteriaFromSession = false;

            if("session".equals(action.getFieldValuesSource()))
            {
                action.values = (Map<String,Object>)ServletActionContext.getRequest().getSession().getAttribute("values");
                action.invalues = (Map<String,Object>)ServletActionContext.getRequest().getSession().getAttribute("invalues");
                action.flagiMap = (Map<String,Object>)ServletActionContext.getRequest().getSession().getAttribute("flagiMap");
                setFlags(action, action.flagiMap);

                Object sc = ServletActionContext.getRequest().getSession().getAttribute("recipientMap");
                if (sc != null)
                    action.recipientSC = (SearchCriteria) sc;
                sc = ServletActionContext.getRequest().getSession().getAttribute("senderMap");
                if (sc != null)
                    action.senderSC = (SearchCriteria) sc;
                readSearchCriteriaFromSession = true;
            }

            if(AvailabilityManager.isAvailable("saveSearch") && StringUtils.isNotEmpty(action.getSaveSearchName())) {
                String valuesStr = DSApi.context().userPreferences().node("search").node(action.getSaveSearchName()).get("valuesStr","");
                String invaluesStr = DSApi.context().userPreferences().node("search").node(action.getSaveSearchName()).get("invaluesStr","");
                String elseMapStr = DSApi.context().userPreferences().node("search").node(action.getSaveSearchName()).get("elseMapStr","");

                try {

                    if(valuesStr != null && valuesStr.length() > 0) {
                        ByteArrayInputStream b = null;
                        ObjectInputStream o = null;

                        b = new ByteArrayInputStream(Base64.decodeBase64(valuesStr.getBytes()));
                        o = new ObjectInputStream(b);
                        action.values = (Map<String,Object>) o.readObject();

                        o.close();
                        b.close();
                    }

                    if(StringUtils.isNotBlank(invaluesStr)) {
                        ByteArrayInputStream b = null;
                        ObjectInputStream o = null;

                        b = new ByteArrayInputStream(Base64.decodeBase64(invaluesStr.getBytes()));
                        o = new ObjectInputStream(b);
                        action.invalues = (Map<String,Object>) o.readObject();

                        o.close();
                        b.close();
                    }

                    if(StringUtils.isNotBlank(elseMapStr)) {
                        ByteArrayInputStream b = null;
                        ObjectInputStream o = null;

                        b = new ByteArrayInputStream(Base64.decodeBase64(elseMapStr.getBytes()));
                        o = new ObjectInputStream(b);
                        Map<String,Object> elseMap = (Map<String,Object>) o.readObject();
                        action.flagiMap = elseMap;
                        setFlags(action, action.flagiMap);

                        String bn = (String) elseMap.get("boxName");
                        if(bn != null && bn.length() > 0) {
                            Box box = Box.findByName(action.kind.getProperties().get(DocumentKind.BOX_LINE_KEY), bn);
                            dockindQuery.boxId(box.getId());
                        }

                        String ctf = (String) elseMap.get("ctimeFrom");
                        String ctt = (String) elseMap.get("ctimeTo");

                        if(ctf != null  || ctt != null) {
                            Date ctimeFromDate = DateUtils.nullSafeParseJsDate(ctf);
                            Date ctimeToDate = DateUtils.nullSafeParseJsDate(ctt);
                            dockindQuery.ctime(ctimeFromDate, ctimeToDate);
                        }

                        String authorStr = (String) elseMap.get("author");
                        if(authorStr != null && authorStr.length() > 0)
                            dockindQuery.author(authorStr);

                        if (elseMap.get("accessedBy") != null || elseMap.get("accessedAs") != null) {
                            if (elseMap.get("accessedBy") != null) {
                                dockindQuery.accessedBy((String) elseMap.get("accessedBy"));
                            }

                            if (elseMap.get("accessedAs") != null) {
                                dockindQuery.enumAccessedAs((String[]) elseMap.get("accessedAs"));
                            }

                            Date accFrom = DateUtils.nullSafeParseJsDate((String) elseMap.get("accessedFrom"));
                            Date accTo = DateUtils.nullSafeParseJsDate((String) elseMap.get("accessedTo"));

                            dockindQuery.accessedFromTo(DateUtils.nullSafeMidnight(accFrom, 0),DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
                        }

                        o.close();
                        b.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    log.debug(e.getMessage(), e);
                }
            }

            action.flagsOn = "true".equals(Configuration.getProperty("flags"));

            if (action.flagsOn)
            {
                dockindQuery.setAllFlags(action.flagIds,
                        DateUtils.nullSafeParseJsDate(action.flagsDateFrom),
                        DateUtils.nullSafeParseJsDate(action.flagsDateTo),
                        action.userFlagIds,
                        DateUtils.nullSafeParseJsDate(action.userFlagsDateFrom),
                        DateUtils.nullSafeParseJsDate(action.userFlagsDateTo)
                );
            }
            // warto�ci p�l formularza do odno�nika wyszukiwania
            Map<String,Object> fieldValues = new HashMap<String,Object>();

            dockindQuery.setDocumentKind(action.kind);
            boolean checkPermissions;
            // je�eli admin to pomijamy tabele permsision
            if (DSApi.context().isAdmin()){
                checkPermissions = false ;
            }else
                checkPermissions/*=true;*/ = action.kind.logic().searchCheckPermissions(action.values);

            dockindQuery.setCheckPermissions(checkPermissions);


            // Sprawdzanie czy wybrano "wymuszenie" szukania "Eq" dla niekt�rych p�l z dockind-a.
            // Ta czynno�� MUSI by� wykonana przed dodawaniem poszczeg�lnych kryetri�w dla p�l z dockind-a.
                /*if (forceSearchEq != null && forceSearchEq.length > 0)
                {
                    for (String fieldCn : forceSearchEq)
                        dockindQuery.addForceSearchEq(fieldCn);
                }*/
            for (String fieldCn : action.kind.logic().forceEqualsSearchList(action.values))
            {
                dockindQuery.addForceSearchEq(fieldCn);
            }

            // dodawanie do zapytania poszczeg�lnych kryteri�w dla p�l z typu dokumentu (dockind)
            boolean centra_active = false;

            //TODO przenie�� w jaki� spos�b do odpowiednich podklas Field
            for (Field field : action.kind.getFields()) {
                if (field instanceof SearchPredicateProvider) {
                    log.info("field with search provider : {}", field.getCn());
                    ((SearchPredicateProvider) field).addSearchPredicate(dockindQuery, action.values, fieldValues);
                } else if (field instanceof DateField || field.isSearchByRange()  ) {

                    if (action.invalues.get(field.getCn()) == null) {
                        String from = (String) action.values.get(field.getCn() + "_from");
                        String to = (String) action.values.get(field.getCn() + "_to");
                            /*Date dateFrom = (Date)*/
                        Object valueFrom = field.simpleCoerce(from);
	                        /*Date dateTo = (Date)*/
                        Object valueTo = field.simpleCoerce(to);
                        dockindQuery.rangeField(field, valueFrom, valueTo);
                        fieldValues.put(field.getCn() + "_from", from);
                        fieldValues.put(field.getCn() + "_to", to);
                    } else {
                        String value = (String) action.invalues.get(field.getCn());
                        String[] tmp = value.split("\n");
                        for (int i = 0; i < tmp.length; i++) {
                            tmp[i] = tmp[i].trim();
                        }
                        dockindQuery.InStringField(field, tmp);
                        fieldValues.put(field.getCn(), tmp);
                    }
                } else if (field instanceof DocumentInStampDateField){
                	prepereForInStamDatefield(field ,action,dockindQuery);
               	
               }else if (field instanceof StringField) {
                    if (action.invalues.get(field.getCn()) != null) {
                        String value = (String) action.invalues.get(field.getCn());
                        String[] tmp = value.split("\n");
                        for (int i = 0; i < tmp.length; i++) {
                            tmp[i] = tmp[i].trim();
                        }
                        dockindQuery.InField(field, tmp);
                        fieldValues.put(field.getCn(), tmp);
                    } else {
                        String value = (String) field.simpleCoerce(action.values.get(field.getCn()));
                        dockindQuery.stringField(field, value);
                        fieldValues.put(field.getCn(), value);
                    }

                } else if (field instanceof ListValueDbEnumField) {
                    if (action.invalues.get(field.getCn()) != null) {
                        String value = (String) action.invalues.get(field.getCn());
                        String[] tmp = value.split(",");
                        for (int i = 0; i < tmp.length; i++) {
                            tmp[i] = tmp[i].trim();

                        }
                        dockindQuery.InField(field, tmp);
                        fieldValues.put(field.getCn(), tmp);
                    } else {
                        String value = (String) field.simpleCoerce(action.values.get(field.getCn()));
                        dockindQuery.stringField(field, value);
                        fieldValues.put(field.getCn(), value);
                    }
                } else if (Field.BOOL.equals(field.getType())) {
                    String value = (String) action.values.get(field.getCn());
                    Boolean bool = (Boolean) field.simpleCoerce(value);
                    dockindQuery.boolField(field, bool);
                    fieldValues.put(field.getCn(), bool);
                } else if (field instanceof FloatField) {
                    String value = (String) action.values.get(field.getCn());
                    Float fl = (Float) field.simpleCoerce(value);
                    dockindQuery.field(field, fl);
                    fieldValues.put(field.getCn(), fl);
                }
//	                    else if (Field.DOUBLE.equals(field.getType()))
                else if (field instanceof DoubleField) {
                    String value = (String) action.values.get(field.getCn());
                    Double fl = (Double) field.simpleCoerce(value);
                    dockindQuery.field(field, fl);
                    fieldValues.put(field.getCn(), fl);
                } else if (Field.CLASS.equals(field.getType())) {
                    String value = null;
                    String[] multiClass = null;
                    if (action.invalues.get(field.getCn()) != null) {
                        value = (String) action.invalues.get(field.getCn());
                        String[] tmp = value.split("\n");
                        for (int i = 0; i < tmp.length; i++) {
                            tmp[i] = tmp[i].trim();
                        }
                        dockindQuery.classField(field, tmp, action.values.get(field.getCn() + "_attrName").toString());
                    } else {
                        try {
                            multiClass = (String[]) action.values.get(field.getCn());
                        } catch (ClassCastException e) {
                            value = (String) action.values.get(field.getCn());
                        }
                        if (multiClass != null) {
                            dockindQuery.InField(field, multiClass);
                        } else if (value != null && (!action.documentKindCn.equals(DocumentLogicLoader.DAA_KIND) ||
                                (action.documentKindCn.equals(DocumentLogicLoader.DAA_KIND) && !field.getCn().equals(DaaLogic.AGENT_FIELD_CN)))) {
                            dockindQuery.field(field, value);
                        } else {
                            for (String property : field.getDictionaryAttributes().keySet()) {
                                value = (String) action.values.get(field.getCn() + "_" + property);

                                if ((property.equals("nip") || property.equals("prettyNip"))) {
                                    value = DicInvoice.cleanNip(value);
                                    property = "nip";
                                }

                                // KOMPLET DOKUMENT�W DLA ARCHIWUM AGENTA AEGON
                                if (property.equals("setOfDocuments")) {
                                    String col = dockindQuery.getDocumentKind().getFieldByCn(DaaLogic.TYP_DOKUMENTU_FIELD_CN).getColumn();
                                    // komplet dokument�w dla agenta archiwum (AEGON)
                                    if (value != null && value.equals("on"))
                                        dockindQuery.addIn(dockindQuery.getDocumentKind().getTablename() +
                                                DockindQuery.ATTRIBUTE_SEPARATOR + col, DaaLogic.SET_OF_DOCUMENTS);
                                    else
                                        dockindQuery.addNotIn(dockindQuery.getDocumentKind().getTablename() +
                                                DockindQuery.ATTRIBUTE_SEPARATOR + col, DaaLogic.SET_OF_DOCUMENTS);
                                } else {
                                    dockindQuery.otherClassField(field, property, value);
                                    fieldValues.put(field.getCn() + "_" + property, value);
                                }
                            }
                        }
                    }
                } else if (Field.CENTRUM_KOSZT.equals(field.getType())) {
                    String value = (String) action.values.get(field.getCn());
                    if (value != null) {
                        Integer integ = Integer.parseInt(value);
                        //dockindQuery.field(field, integ);
                        dockindQuery.centrumKosztowField(field, integ);
                        fieldValues.put(field.getCn(), integ);
                    }
                } else if (field.HTML.equals(field.getType())) {

	                   /* else if (field.getType().equals("dsdivision")&& AvailabilityManager.isAvailable("SearchDockindDocument.dsdivision.byNameOrCode"))
	                    {
						values.get("DSDIVISION"); // zwraca wpisana wartosc
						if (values.get("DSDIVISION") instanceof String)
						List<DSDivision> divisions;
						try {
							divisions = DSDivision.findByLikeName((String) values.get("DSDIVISION"));
							if (divisions.isEmpty() || divisions.get(0).isPosition())
								divisions = DSDivision.findByLikeCode((String) values.get("DSDIVISION"));
							if (divisions != null && !divisions.isEmpty()){
							for (DSDivision div : divisions) {
								Object value = div.getId();
								dockindQuery.field(field, value);
								fieldValues.put(field.getCn(), value);
							}
							}else {
								addActionError("Nie znaleziono dokument�w pod zadanym kryterium wyszukiwania");
								throw new Exception("");
							}
						} catch (Exception e) {
							log.error("B�ad przy wyszukiwaniu Division po nazwie",e);
							}
						}
						*/
                } else {

                    Object value = field.simpleCoerce(action.values.get(field.getCn()));
                    if (value instanceof Object[]) {
                        Object[] array = (Object[]) value;
                        if (array.length == 0)
                            value = null;
                        else
                            value = array[0];
                    }
                    if (value != null) {
                        dockindQuery.field(field, value);
                        fieldValues.put(field.getCn(), value);
                    }
                }

            }
            if(action.values.get("KONTA_KOSZTOWE") != null)
            {
                String value = (String) action.values.get("KONTA_KOSZTOWE");
                if(value != null)
                {
                    dockindQuery.kontoKosztoweField(value);
                    fieldValues.put("KONTA_KOSZTOWE", value);
                }
            }
            if(action.values.get("CENTRUM_LOKALIZACJA") != null)
            {
                String value = (String) action.values.get("CENTRUM_LOKALIZACJA");
                if(value != null)
                {
                    dockindQuery.lokalizacjaField(value);
//                		fieldValues.put("LOKALIZACJA", value);
                }
            }

            if (readSearchCriteriaFromSession ){

                if (action.recipientSC.isAnyValue())
                    dockindQuery.personAttributes(action.recipientSC.getTableName(),action.recipientSC.getTableValue(),action.recipientSC.getColumnValue(),action.recipientSC);
                if (action.senderSC.isAnyValue())
                    dockindQuery.personAttributes(action.senderSC.getTableName(),action.senderSC.getTableValue(),action.senderSC.getColumnValue(),action.recipientSC);
            }

            if (action.ascending)
                dockindQuery.orderAsc(action.getSortField());
            else
                dockindQuery.orderDesc(action.getSortField());
            //   dockindQuery.checkSortField(sortField);
            setSessionAttribiute("values", action.values);
            setSessionAttribiute("invalues", action.invalues);

            if(action.flagiMap == null)
            {
                action.flagiMap = new HashMap<String, Object>();
                action.flagiMap.put("flagIds",action.flagIds);
                action.flagiMap.put("flagsDateFrom",action.flagsDateFrom);
                action.flagiMap.put("flagsDateTo",action.flagsDateTo);
                action.flagiMap.put("userFlagIds",action.userFlagIds);
                action.flagiMap.put("userFlagsDateFrom",action.userFlagsDateFrom);
                action.flagiMap.put("userFlagsDateTo",action.userFlagsDateTo);
                setSessionAttribiute("flagiMap", action.flagiMap);
            }

            //ystem.out.println("po");
            // url celowo bez sortField, bo ten parametr b�dzie dodany p�niej
            // w getSortLink()
            action.thisSearchUrl = action.getLink("doSearch", action.offset, action.limit, null, null, action.documentKindCn, "session", action.boxName,
                    action.accessedBy, action.accessedAs, action.getAccessedFrom(), action.getAccessedTo(), action.getResultsInPopup(), action.getDocumentAspectCn(),
                    action.getAuthor(),
                    action.getDocumentDateFrom(), action.getDocumentDateTo(),action.getOfficeNumberFrom(),action.getOfficeNumberTo(),
                    action.officeNumberYear, action.getCtimeFrom(), action.getCtimeTo());

            action.attachmentsAsPdfUrlPartially = action.thisSearchUrl + "&sortField="+action.getSortField()+"&attachmentsAsPdfPartially=true";
            action.attachmentsAsPdfUrl = action.thisSearchUrl + "&sortField="+action.getSortField() + "&ascending="+action.ascending+"&attachmentsAsPdf=true";

            action.addDocsForCompilationUrl = action.getLink(null, action.offset, action.limit, null, null, action.documentKindCn, "session", action.boxName,
                    action.accessedBy, action.accessedAs, action.getAccessedFrom(), action.getAccessedTo(),
                    action.getResultsInPopup(), action.getDocumentAspectCn(), action.getAuthor(),
                    action.getDocumentDateFrom(), action.getDocumentDateTo(), action.getOfficeNumberFrom(),action.getOfficeNumberTo(),
                    action.officeNumberYear, action.getCtimeFrom(), action.getCtimeTo())
                    + (action.getSortField() != null ? "&sortField="+action.getSortField()+"&ascending="+action.ascending : "");

            action.canCompileAttachments = DocumentLogicLoader.NATIONWIDE_KIND.equals(action.documentKindCn) && !action.getResultsInPopup()
                    && DSApi.context().hasPermission(DSPermission.NW_KOMPILACJA_ZALACZNIKOW_Z_WYSZUKIWARKI);

            SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);

            action.setCount(searchResults.totalCount());

            action.ids = new Long[searchResults.count()];
            action.resultDocumentsTable = searchResults.results();

            for (int t=searchResults.count(), i=t-1; i>=0 ;i--)
                action.ids[i] = action.resultDocumentsTable[i].getId();
            if (searchResults.count() == 0) {
                action.addActionMessage(sm.getString("NieZnalezionoDokumentow"));
            }
            else
            {

                String tmp = DSApi.context().systemPreferences().node("search-documents").get("columns", SearchDocumentsAction.DEFAULT_COLUMNS);
                action.columns = action.getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));

                //List<String> columnNames = getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", tmp));
                List<String> userDockindColumns = new ArrayList<String>(); //DocumentKindsManager.getUserDockindColumns(kind);

                if(action.getDocumentAspectCn() != null)
                {
                    for(String str : DocumentKindsManager.getUserDockindColumns(action.kind))
                    {
                        if(action.kind.getDockindInfo().getAspectsMap().get(action.getDocumentAspectCn()).getFiListCns().contains(str))
                        {
                            userDockindColumns.add(str);
                        }
                    }
                }
                else
                {
                    userDockindColumns = DocumentKindsManager.getUserDockindColumns(action.kind);
                }

                //for(String str : userDockindColumns)
                int[] columnAvaiable = new int[action.kind.getFields().size()];
                action.results = action.addColumns(searchResults, columnAvaiable);

                boolean sorted = action.sortResults(dockindQuery.getOrder());
                if (sorted){
                    if (dockindQuery.getLimit() > 0 && dockindQuery.getLimit()<action.getResults().size()){
                        int fromIdx =dockindQuery.getOffset();
                        int toIdx = dockindQuery.getOffset()
                                + (dockindQuery.getLimit() < action.results.size() - dockindQuery.getOffset()
                                ? dockindQuery.getLimit()
                                : action.results.size() - dockindQuery.getOffset());

                        action.results = action.results.subList(fromIdx,toIdx);
                        action.ids = new Long[action.results.size()];
                    }
                    for (int i = 0; i < action.results.size(); ++i)
                        action.ids[i] = (Long)action.results.get(i).get("document_id");
                }

                int docCount = action.results.size();

                if (docCount > 0)
                {
                    if(event != null)
                        event.skip(action.EV_FILL);

                    int i = 0;

                    for(String colName:userDockindColumns)
                    {
                        if (columnAvaiable[i] == 1)
                        {
                            if(colName.startsWith("#complex#"))
                            {
                                action.columns.add(new TableColumn(
                                        "dockind_"+colName,
                                        colName.substring("#complex#".length()),
                                        // dla atrybut�w wielowarto�ciowych nie ma obecnie sortowania
                                        null,
                                        null));
                            }
                            else
                            {
                                Field f = action.kind.getFieldByCn(colName);
                                if((!(f.isHidden() || f.isSearchHidden())) || f.isSearchShow())
                                {
                                    action.columns.add(new TableColumn(
                                            "dockind_"+f.getCn(),
                                            f.getName(),
                                            // dla atrybut�w wielowarto�ciowych nie ma obecnie sortowania
                                            f.isMultiple() ? null : action.getSortLink("dockind_" + f.getCn(), false),
                                            f.isMultiple() ? null : action.getSortLink("dockind_" + f.getCn(), true)));
                                }
                            }
                        }
                        i++;
                    }

                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
                        public String getLink(int offset) {
                            return SearchDockindDocumentsAction.getLink("doSearch", offset, action.limit, action.getSortField(), Boolean.valueOf(action.ascending),
                                    action.documentKindCn, "session", action.boxName, action.accessedBy, action.accessedAs, action.getAccessedFrom(), action.getAccessedTo(),
                                    action.getResultsInPopup(), action.getDocumentAspectCn(), action.getAuthor(),
                                    action.getDocumentDateFrom(), action.getDocumentDateTo(), action.getOfficeNumberFrom(),
                                    action.getOfficeNumberTo(), action.officeNumberYear,action.getCtimeFrom(), action.getCtimeTo());
                        }
                    };
                    action.pager = new Pager(linkVisitor, action.offset, action.limit, searchResults.totalCount(), 10);
                } else {
                    if(event != null)
                        event.addActionMessage(sm.getString("NieZnalezionoDokumentow"));
                    else
                        throw new EdmException(sm.getString("NieZnalezionoDokumentow"));
                }
            }
        } catch (EdmException e) {
            log.error(e.getLocalizedMessage(), e);
            if(event != null)
                event.addActionError(e.getMessage());
            else
                throw new EdmException(e.getMessage());
        } catch (ClassNotFoundException e) {
            log.error(e.getLocalizedMessage(), e);
            if(event != null)
                event.addActionError(e.getMessage());
            else
                throw new EdmException(e.getMessage());
        }
        catch(NumberFormatException r){
            if(event != null)
                event.addActionError(sm.getString("NieZnalezionoDokumentow"));
            else
                throw new EdmException(sm.getString("NieZnalezionoDokumentow"));
        } catch (ParseException e) {
            if(event != null && action.getDocumentDateFrom()!=null)
                event.addActionError(sm.getString("NieMoznaSkonwertowacWartosci")+": "+action.getDocumentDateFrom()+" "+sm.getString("DoDaty"));
            else
                 throw new EdmException(sm.getString("NieMoznaSkonwertowacWartosci")+": "+action.getDocumentDateFrom()+" "+sm.getString("DoDaty"));

            if(event != null && action.getDocumentDateTo()!=null)
                event.addActionError(sm.getString("NieMoznaSkonwertowacWartosci")+": "+action.getDocumentDateTo()+" "+sm.getString("DoDaty"));
            else
                throw new EdmException(sm.getString("NieMoznaSkonwertowacWartosci")+": "+action.getDocumentDateTo()+" "+sm.getString("DoDaty"));
            log.error(e.getLocalizedMessage(), e);
        } catch (SearchDockindDocumentsAction.InvalidSqlRangeDate e) {
            if(event != null && action.getDocumentDateFrom()!=null)
                event.addActionError(sm.getString("PrzekazanaDataNieZnajdujeSie")+sm.getString("WzakresieWymaganymPrzezBazeDanych")+": "+action.getDocumentDateFrom());
            else
                throw new EdmException(sm.getString("PrzekazanaDataNieZnajdujeSie")+sm.getString("WzakresieWymaganymPrzezBazeDanych")+": "+action.getDocumentDateFrom());
            if(event != null && action.getDocumentDateTo()!=null)
                event.addActionError(sm.getString("PrzekazanaDataNieZnajdujeSie")+sm.getString("WzakresieWymaganymPrzezBazeDanych")+": "+action.getDocumentDateTo());
            else
                throw new EdmException(sm.getString("PrzekazanaDataNieZnajdujeSie")+sm.getString("WzakresieWymaganymPrzezBazeDanych")+": "+action.getDocumentDateTo());
            log.error(e.getLocalizedMessage(), e);
        }
        action.setAdminAccess(DSApi.context().isAdmin());
    }

	private void prepereForInStamDatefield(Field field,SearchDockindDocumentsAction action, DockindQuery dockindQuery) {
		String from = (String) action.values.get(field.getCn() + "_from");
		String to = (String) action.values.get(field.getCn() + "_to");

		Date stampFromDate = DateUtils.nullSafeParseJsDate(from);
		Date stampToDate = DateUtils.nullSafeParseJsDate(to);
		dockindQuery.stampDateIn(stampFromDate, stampToDate);
	}

	public String removePercentExpresion(String stringToClear){
    	
    	stringToClear.replaceAll("%", "");
        	
    return stringToClear;
        
   }
    private void setSessionAttribiute(String key, Object obj) {
        if(ServletActionContext.getRequest() != null){
            ServletActionContext.getRequest().getSession().setAttribute(key, obj);
        }

    }

    private void setFlags(SearchDockindDocumentsAction action, Map<String, Object> flagiMap)
    {
        if(flagiMap != null && flagiMap.get("flagIds") != null)
            action.flagIds = (Long[]) flagiMap.get("flagIds");
        if(flagiMap != null &&  flagiMap.get("flagsDateFrom") != null)
            action.flagsDateFrom = (String) flagiMap.get("flagsDateFrom");
        if(flagiMap != null &&  flagiMap.get("flagsDateTo") != null)
            action.flagsDateTo = (String) flagiMap.get("flagsDateTo");
        if(flagiMap != null &&  flagiMap.get("userFlagIds") != null)
            action.userFlagIds = (Long[]) flagiMap.get("userFlagIds");
        if(flagiMap != null &&  flagiMap.get("userFlagsDateFrom") != null)
            action.userFlagsDateFrom = (String) action.flagiMap.get("userFlagsDateFrom");
        if(flagiMap != null &&  flagiMap.get("userFlagsDateTo") != null)
            action.userFlagsDateTo = (String) flagiMap.get("userFlagsDateTo");
    }
}
