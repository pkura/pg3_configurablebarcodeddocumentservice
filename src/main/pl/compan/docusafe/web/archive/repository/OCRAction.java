package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

public class OCRAction  extends EventActionSupport
{
    private boolean generated;
    private Long documentId;

    private final static Logger LOG = LoggerFactory.getLogger(OCRAction.class);
    private final StringManager sm =
            GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

    @Override
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSendToOcr").
                append(OpenHibernateSession.INSTANCE).
                append(new Send()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public FillForm()
        {

        }

        public void actionPerformed(ActionEvent event)
        {

        }
    }

    private class Send implements ActionListener {

        public void actionPerformed(ActionEvent event)
        {
           try
           {
               Document doc = Document.find(documentId);
               if(doc.getAttachments().size()==0)
               {
                   addActionError(sm.getString("DokumentNieMaZalacznikow"));
                   generated = false;
                   return;
               }
               for (Attachment att : doc.getAttachments())
               {
                   DSApi.context().begin();
                   doc.getDocumentKind().logic().getOcrSupport().putToOcr(att.getMostRecentRevision());
                   DSApi.context().commit();
               }
               generated = true;
               addActionMessage(sm.getString("PomyslniePrzeslanoZalocznikDoOcr"));
           }
           catch (Exception e)
           {
              LOG.error(e.getMessage(), e);
               generated = false;
           }

        }
    }

    public boolean isGenerated()
    {
        return generated;
    }

    public void setGenerated(boolean generated)
    {
        this.generated = generated;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
}
