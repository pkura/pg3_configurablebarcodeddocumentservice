package pl.compan.docusafe.web.archive.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.RockwellVendor;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class RockwellVendorsImportAction extends EventActionSupport
{

	 protected void setup()
	    {
	        FillForm fillForm = new FillForm();

	        registerListener(DEFAULT_ACTION).
	            append(OpenHibernateSession.INSTANCE).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);

	        registerListener("doImport").
	            append(OpenHibernateSession.INSTANCE).
	            append(new Create()).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);

	       }
	    private FormFile file;

	    private class FillForm implements ActionListener
	    {
	        public void actionPerformed(ActionEvent event) 
	        {
	        }
	    }

	    private class Create implements ActionListener
	    {
	        public void actionPerformed(ActionEvent event) 
	        {
	            if(file==null)
	            {
	                addActionError("Missing file");
	                return;
	            }
	            if(!file.getFile().getName().substring(file.getFile().getName().length()-3, file.getFile().getName().length()).equalsIgnoreCase("csv") )
	            {
	            	addActionError("Wrong file type");
	            	return;
	            }
	            	
	            int count = 0;
	            int countImport = 0;
	            try
	            {
	                String line;
	                FileReader fr = new FileReader(file.getFile());
	                BufferedReader br = new BufferedReader(fr);
	             
	               
	                try
	                {
		                while((line = br.readLine())!=null)
		                {
		                	if(line.startsWith("VENDOR"))
		                		continue;
		                	
		                	try
		                    {
		                		String vendorId;
		                		String setId;
		                		count++;

			                    try
			                    {
			                    	vendorId = line.substring(0, 10);
			                    	setId = TextUtils.trimmedStringOrNull(line.substring(11, 16),10);
			                    }
			                    catch (NumberFormatException e) 
								{
			                    	event.addActionMessage("Import error on line  "+count+" invalid vendor id");
			                    	continue;
								}
			                    
			                    if(findVendor(vendorId, setId))
			                    {
			                    	event.addActionMessage("Import error on line  "+count+" vendor("+vendorId+","+setId+") already exists");
			                    	continue;			                    	
			                    }
			                    DSApi.context().begin();
			                    RockwellVendor vendor = new RockwellVendor();
			                    vendor.setVendorId(vendorId);
			                    vendor.setSetId(setId);
			                    vendor.setShortName(TextUtils.trimmedStringOrNull(line.substring(17, 31),100));
			                    vendor.setName(TextUtils.trimmedStringOrNull(line.substring(32, 72),100));
			                    vendor.setLocation(TextUtils.trimmedStringOrNull(line.substring(73,83),20));
			                    vendor.setBankAccountNumber(TextUtils.trimmedStringOrNull(line.substring(84, 101),50));
			                    vendor.setBankId(TextUtils.trimmedStringOrNull(line.substring(102, 113),50));
			                    vendor.setSwiftId(TextUtils.trimmedStringOrNull(line.substring(114, 127),50));
			                    vendor.setIban(TextUtils.trimmedStringOrNull(line.substring(128, 156),50));
			                    vendor.setAddress(TextUtils.trimmedStringOrNull(line.substring(157,192),50));
			                    vendor.setCity(TextUtils.trimmedStringOrNull(line.substring(193,224),20));
			                     
			                    countImport ++;
		                    
			                    vendor.create();
			                    DSApi.context().commit();
		                    }
		                    catch (EdmException e2) 
							{
		                    	event.addActionMessage("Import error on line  "+count);
		                    	//count --;
								try {
									DSApi.context().rollback();
								} catch (EdmException e1) {
									LogFactory.getLog("eprint").debug("", e1);
								}
							}
		                }
	                }
	                catch (Exception e) 
					{
	                	try {
							DSApi.context().rollback();
						} catch (EdmException e1) {
							LogFactory.getLog("eprint").debug("", e1);
						}
					}                   
	                addActionMessage("Imported " +String.valueOf(countImport) + " vendors.");
	            }
	            catch (IOException e3) 
				{
	            	event.addActionMessage("File error");
				}
	        }
	    }

	    public void setFile(FormFile file)
	    {
	        this.file = file;
	    }
	    
	    private boolean findVendor(String vendorId, String setId) throws EdmException
	    {
	    	Criteria c = DSApi.context().session().createCriteria(RockwellVendor.class);            
            c.add(Expression.eq("vendorId",vendorId));   
            c.add(Expression.eq("setId",setId)); 
            List<RockwellVendor> list = (List<RockwellVendor>) c.list();
            if(list.size() < 1)
            	return false;
            else
            	return true;
	    }
}
