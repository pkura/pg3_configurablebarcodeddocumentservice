package pl.compan.docusafe.web.archive.repository;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.google.common.base.Functions;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import org.apache.lucene.queryparser.flexible.standard.parser.ParseException;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.lucene.DocumentSearcher;
import pl.compan.docusafe.lucene.ExtensionDocumentHandler;
import pl.compan.docusafe.lucene.LuceneResult;
import pl.compan.docusafe.lucene.LuceneUtils;
import pl.compan.docusafe.lucene.searcher.DefaultSearcher;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * 
 * @author przemek
 */
public class FulltextSearchDocumentsAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(FulltextSearchDocumentsAction.class);
	private static final long serialVersionUID = 1L;
	public static final StringManager sm = GlobalPreferences.loadPropertiesFile(FulltextSearchDocumentsAction.class.getPackage().getName(),null);

    private Map<String,String> extensionsList;
    private String extension;
    private String searchText;
    private int searchLimit;
    private List<LuceneResult> luceneResult;


	public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

	private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            extensionsList =  Maps.newLinkedHashMap(Maps.uniqueIndex(LuceneUtils.getSupportedExtensions(), Functions.<String>identity()));
            extensionsList.put("","Dowolne");
            extension = "";
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
	        	log.info("fulltextSearch, szukana fraza {}, limit wynik�w {}, rozszerzenie pliku {}",searchText,searchLimit,extension);

	        	if (searchText == null) {
        			addActionMessage(sm.getString("WymaganeZapytanie"));
        			return;
        		}
	        	
	        	if (searchText.startsWith("*") || searchText.startsWith("?")) {
	        		addActionError(sm.getString("LuceneForbiddenCharacters"));
	        		return;
	        	}
	        	
	        	DocumentSearcher ds = new DefaultSearcher();
                String filenameSubsearch = StringUtils.isBlank(extension) ? ""
                        : " AND " + ExtensionDocumentHandler.FILENAME_EXTENSION_KEY +":" + extension;
                String searchQuery = searchText + filenameSubsearch;
                log.info("search query : '{}'", searchQuery);
	        	luceneResult = ds.search(searchQuery, searchLimit);

                if(AvailabilityManager.isAvailable("fulltextSearch.datamart")){
                    DSApi.context().begin();
                    DataMartEventProcessors.DataMartEventBuilder.get()
                        .event(DataMartEventProcessors.EventType.FULL_TEXT_SEARCH)
                        .objectId(searchText);
                    DSApi.context().commit();
                }
        	}
        	catch(ParseException e)
        	{
        		addActionError("Niedozwolone zapytanie");
        		log.error("",e);
        	}
        	catch (FileNotFoundException e) 
        	{
        		addActionError("Nie zaindeksowano �adnych plik�w");
        		log.error("",e);
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
        		log.error("",e);
        	}
        }
    }

    
    public void setSearchText(String searchText) {
    	this.searchText = searchText;
    }
    
    public String getSearchText() {
    	return searchText;
    }

	public int getSearchLimit() {
		return searchLimit;
	}

	public void setSearchLimit(int searchLimit) {
		this.searchLimit = searchLimit;
	}
	
    public List<LuceneResult> getLuceneResult() {
		return luceneResult;
	}

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Map<String, String> getExtensionsList() {
        return extensionsList;
    }
}