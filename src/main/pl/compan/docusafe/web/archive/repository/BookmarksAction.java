package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Ulubione.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: BookmarksAction.java,v 1.6 2008/04/03 16:26:29 pecet4 Exp $
 */
public class BookmarksAction extends EventActionSupport
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(BookmarksAction.class.getPackage().getName(),null);
    public static final String NODE = "favourites";

    private List bookmarks;
    private String[] deleteKeys;
    private String BUsunZaznaczone;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Preferences prefs = DSApi.context().userPreferences().node(NODE);
                String[] keys = prefs.keys();
                bookmarks = new ArrayList(keys.length);

                for (int i=0; i < keys.length; i++)
                {
                    if (keys[i].startsWith("document;"))
                    {
                        Map bean = new HashMap();
                        Long documentId = new Long(keys[i].substring("document;".length()));

                        // wartością jest opis bookmarka, obecnie nieużywany
                        //String title = DSApi.context().userPreferences().node(NODE).get(keys[i], "Dokument "+documentId);

                        try
                        {
                            Document document = Document.find(documentId);

                            bean.put("key", keys[i]);
                            bean.put("documentId", documentId);
                            bean.put("title", document.getTitle());
                            bean.put("link", "/repository/edit-document.action?id="+documentId);

                            bookmarks.add(bean);
                        }
                        catch (EdmException e)
                        {
                            event.getLog().info("Usuwanie bookmarka "+keys[i]);
                            prefs.remove(keys[i]);
                        }
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
            catch (BackingStoreException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteKeys == null || deleteKeys.length == 0)
            {
                event.addActionError(sm.getString("NieWybranoElementowDoUsuniecia"));
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < deleteKeys.length; i++)
                {
                    DSApi.context().userPreferences().node(NODE).remove(deleteKeys[i]);
                }

                DSApi.context().commit();
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
            }
        }
    }

    public static void addDocument(Document document) throws EdmException
    {
        DSApi.context().userPreferences().node(NODE).put("document;"+document.getId(), document.getTitle());
    }

    public List getBookmarks()
    {
        return bookmarks;
    }

    public void setDeleteKeys(String[] deleteKeys)
    {
        this.deleteKeys = deleteKeys;
    }

    public String getBUsunZaznaczone()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("BUsunZaznaczone");
    }

    public void setBUsunZaznaczone(String usunZaznaczone)
    {
        BUsunZaznaczone = usunZaznaczone;
    }
}
