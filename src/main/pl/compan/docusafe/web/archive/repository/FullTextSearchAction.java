package pl.compan.docusafe.web.archive.repository;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.document.DocumentFinder;
import pl.compan.docusafe.core.base.document.DocumentFinderProvider;
import pl.compan.docusafe.core.base.document.DocumentSolrFinder;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.web.admin.FullTextManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.RichEventActionSupport;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class FullTextSearchAction extends RichEventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(FullTextSearchAction.class);

    @Override
    protected void setup() {
        hibernateListener(new Search());
    }

    private class Search extends AjaxActionListener {
        private int maxResults = 0;

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            maxResults = getIntParameterOrDefault("maxResults", 1000);
            
        	QueryForm queryForm = new QueryForm(0, maxResults);
    		queryForm.addProperty("disableSolrHighlight", true);
    		
        	queryForm.addProperty("barcode", getParameterOrError("query"));
        	
        	DocumentFinder documentFinderProvider = DocumentFinderProvider.instance().provide(queryForm);
        	
        	Iterator<Document> searchResults = documentFinderProvider.find();
        	
        	if(!searchResults.hasNext()){
        		queryForm.getProperties().clear();
        		queryForm.addProperty("full_text", getParameterOrError("query"));
        	
        		queryForm.addProperty("fullTextSearchFields", DocumentSolrFinder.DEFAULT_FULL_TEXT_SEARCH_FIELDS);
        	
        		documentFinderProvider = DocumentFinderProvider.instance().provide(queryForm);
        	
        		searchResults = documentFinderProvider.find();
        	}
        	
        	JsonElement el = null;
            try{
                el = FullTextManager.getDocumentsJsonView(IteratorUtils.toList(searchResults));
            }catch(Exception e){
                addActionError(event, e);
                log.error("", e);
            }

            return el;
        }

        /**
         * <p>
         *     Je�li w query nie ma znaku `:` to query zamieniane jest
         *     na `content:{query} OR title:{query}`. W przeciwnym razie
         *     zamieniane jest na `"{query}"`. `{query}` to wklejona
         *     do stringa zawarto�� zmiennej `query`.
         * </p>
         * @param query
         * @return
         */
        private String modifyQuery(String query) {

            if(query.indexOf(":") == -1) {
                return "content:\""+query+"\" OR title:\""+query+"\"";
            } else {
                return "\""+query+"\""  ;
            }
        }

    }

}
