package pl.compan.docusafe.web.archive.repository;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.CompositeWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.parametrization.utp.NormalLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.webwork.event.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.lowagie.text.Row;
import com.opensymphony.webwork.ServletActionContext;


/* User: Administrator, Date: 2007-03-12 09:23:59 */

/**
 * Akcja klonuj�ca dokumenty posiadaj�ca pewien rodzaj dokumentu
 * {@link pl.compan.docusafe.core.dockinds.DocumentKind}.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class CloneDockindDocumentAction extends EventActionSupport {
	
	/*
	 * 21-08-2013 Tomasz Nowak
	 * odnosnie pol slownikowych
	 * nie sa one wyswietlane na formatce ak jak powinny, 
	 * dlatego najlepiej ich nie wyswietlac( ukryc je w scripcie w jsp)
	 * jesli beda wyswietlone, nadal wszystko bedzie dizalac, jednakze, nie bedzie to przejrzyste
	 * wrecz bedzie zle wyswietlane, choc wywalac nie powinno
	 * 
	 * */
	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			CloneDockindDocumentAction.class.getPackage().getName(), null);
	// @EXPORT
	private FieldsManager fm;
	private List attachments;
	private boolean canModify;
	/**
	 * czy dokument jest zablokowany podpisem
	 */
	private boolean blocked;
	private String title;
	private String description;
	/**
	 * Dla przekierowania do explore-documents.
	 */
	private Long folderId;
	private boolean canReadDictionaries;

	// @EXPORT/@IMPORT
	private Long documentId;

	public Document getDocument() {
		return document;
	}

	public Map<String, String> getDocumentKinds() {
		return documentKinds;
	}

	private Document document;
	private String documentKindCn;
	private int docType;
	private Map copyAttachments = new HashMap();
	private Map<String, String> documentKinds;

	// @IMPORT
	private Map<String, Object> values = new HashMap<String, Object>();

	// wersja
	boolean nowa_wersja;
	int staraWersja;
	long stareGuid;
	String nowy_opis;
	boolean wyslano;

	public boolean isnowa_wersja() {
		return nowa_wersja;
	}

	public void setnowa_wersja(boolean nowa_wersja) {
		this.nowa_wersja = nowa_wersja;
	}

	public void setnowy_opis(String nowy_opis) {
		this.nowy_opis = nowy_opis;
	}

	public String getnowy_opis() {
		return nowy_opis;
	}

	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doClone").append(OpenHibernateSession.INSTANCE)
				.append(new Clone()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (documentId == null)
				return;

			try {
				document = Document.find(documentId);

				fm = document.getDocumentKind().getFieldsManager(documentId);
				fm.initialize();
				
				// zaznaczenie pola daty jako nie obowiazkowe, jednakze bedzie
				// wywalac blad w przypadku, gdy jest puste i pole
				// dokumentbezdaty jest falszem
				if (document instanceof InOfficeDocument)
					fm.getField("DOC_DATE").setRequired(false);

				if (AvailabilityManager.isAvailable("p4CloneDocument")) {
					if (document instanceof InOfficeDocument)
						docType = DocumentLogic.TYPE_IN_OFFICE;
					else if (document instanceof OutOfficeDocument) {
						if (((OutOfficeDocument) document).isInternal())
							docType = DocumentLogic.TYPE_INTERNAL_OFFICE;
						else
							docType = DocumentLogic.TYPE_OUT_OFFICE;
					}

					List<DocumentKind> list = DocumentKindProvider.get()
							.visible().forManualCreate()
							.type(getDocumentTypeAsType(docType)).list();
					documentKinds = new HashMap<String, String>();
					for (DocumentKind documentKind : list) {
						documentKinds.put(documentKind.getCn(),
								documentKind.getName());
					}
					DocumentKind kind = document.getDocumentKind();
					if (kind != null)
						documentKindCn = kind.getCn();
				}

				// sprawdzanie czy mo�na czyta� s�owniki na stronie JSP
				canReadDictionaries = document.getDocumentKind().logic()
						.canReadDocumentDictionaries();
				;

				canModify = document.canModify();
				title = document.getTitle();
				description = document.getDescription();
				attachments = document.getAttachments();

				if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) {
					blocked = (document.getBlocked() != null && document
							.getBlocked());
				} else
					blocked = false;

				if(AvailabilityManager.isAvailable("UTP.wlacz.automatyczneTworzenieNowejWersji")){
					OfficeDocument doc = OfficeDocument.find(document.getId());
					FieldsManager fm = doc.getFieldsManager();
					if(doc instanceof OutOfficeDocument){
						OutOfficeDocument docOut = OutOfficeDocument.findOutOfficeDocument(doc.getId());
						if(!docOut.isInternal()){
							if(doc.getCzyCzystopis() && doc.getOfficeNumber() != null){
								setWyslano(true);
							} else {
								setWyslano(false);
							}
						} 
					}
				}
			} catch (EdmException e) {
				addActionError(e.getMessage());
			}

		}
	}

	private DocumentType getDocumentTypeAsType(int docType) {
		if (DocumentLogic.TYPE_IN_OFFICE == docType)
			return DocumentType.INCOMING;
		if (DocumentLogic.TYPE_OUT_OFFICE == docType)
			return DocumentType.OUTGOING;
		if (DocumentLogic.TYPE_INTERNAL_OFFICE == docType)
			return DocumentType.INTERNAL;
		if (DocumentLogic.TYPE_ARCHIVE == docType)
			return DocumentType.ARCHIVE;
		return DocumentType.INCOMING;
	}


	private class Clone implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				Document document = Document.find(documentId);
				HttpServletRequest request = ServletActionContext.getRequest();
				Sender tmpSender = new Sender();
				long tmpSenderId = 0;
				List<Audit> listaAudit = new ArrayList<Audit>();
				List<AssignmentHistoryEntry> listaAHE=new ArrayList<AssignmentHistoryEntry>(); 
				if (document instanceof OfficeDocument) {
					if (((OfficeDocument) document).getSenderId() != null) {
						tmpSenderId = ((OfficeDocument) document).getSenderId();
						Person person = Person.find(tmpSenderId);
						tmpSender.fromMap(person.toMap());
						listaAudit=document.getWorkHistory();
						listaAHE=((OfficeDocument) document).getAssignmentHistory();
					}
				}
				stareGuid = document.getVersionGuid();
				staraWersja = document.getVersion();

				String tmp = request.getParameter("nowa_wersja");
//				Map<String, String> rob = request.getParameterMap();
				nowa_wersja = Boolean.parseBoolean(tmp);
				String name = DSApi.context().getDSUser().getName();

				DocumentKind documentKind = document.getDocumentKind();
				if (AvailabilityManager.isAvailable("p4CloneDocument"))
					values = RequestLoader.loadKeysFromWebWorkRequest();

				// sprawdzenie czy pole data jest puste, a nie jest
				// zaznaczona
				// opcja dokumentu bez daty w przypadku dokumentu
				// przychodzacego
				boolean jestData = true;
				if (document instanceof InOfficeDocument) {
					if (Boolean.parseBoolean((String) values
							.get("dokument_bez_daty"))) {
					} else {
						if (values.get("DOC_DATE") == null)
							jestData = false;

						else {
							if (values.get("DOC_DATE").equals(""))
								jestData = false;

						}
					}
				}
				//pole data zwrotki w wypadku dokumentu wychodzacego
				if(document instanceof OutOfficeDocument){
					if (!Boolean.parseBoolean((String)values.get("zwrotka"))) {
					} else {
						if (values.get("datazwrotki")==null)
							jestData = false;

						else {
							if (values.get("datazwrotki").equals(""))
								jestData = false;
						}
					}
				}
				// niewpisana data przy niezaznaczonym checkobxie dokument
				// bez daty
				if (jestData == false) {
					if(document instanceof InOfficeDocument)addActionError("Pole data mo�e by� puste tylko w przypadku dokumentu bez daty.");
					else addActionError("Pola data zwrotki mo�e by� puste tylko w wypadku dokumentu bez zwrotki.");
				} else {
					// wyzerowanie daty w przypadku dokumentu bez daty
					if (Boolean.parseBoolean((String) values
							.get("dokument_bez_daty"))&& document instanceof InOfficeDocument) {
						values.remove("DOC_DATE");
						values.put("DOC_DATE", null);
					}
					if (Boolean.parseBoolean((String) values
							.get("datazwrotki")) && document instanceof OutOfficeDocument) {
						values.remove("datazwrotki");
						values.put("datazwrotki", null);
					}
					// chce utworzyc nowa wersje a nie ma uprawnien
					if (nowa_wersja
							&& !DSApi
									.context()
									.hasPermission(
											DSPermission.WERSJONOWANIE_UTWORZ_NOWA_WERSJE)) {
						addActionError("Brak uprawnie� do tworzenia nowej wersji");
						return;
					} else {

						documentKind.logic()
								.correctValues(values, documentKind);
						// walidacja nie jest tu raczej potrzebna, gdy� wykonuje
						// si�
						// ona
						// ju� na poziomie Javascriptu
						if(AvailabilityManager.isAvailable("utp.wlacz.unikalneBarcody")){
							OfficeDocument doc = OfficeDocument.find(documentId);
							if(values.get("BARCODE") != null){
								if(!doc.getBarcode().equals(values.get("BARCODE"))){
									String bar = (String) values.get("BARCODE");
									List<OfficeDocument> barcode = OfficeDocument.findAllByBarcode(bar);
									List list = OfficeFolder.findByBarcode(bar);
									if (list != null && list.size()>0){
										throw new EdmException("Wprowadzony kod kreskowy = " + bar + " ju� istnieje w systemie !");
									}
									if(barcode.size() > 0){
										throw new EdmException("Wprowadzony kod kreskowy = " + bar + " ju� istnieje w systemie !");
									}
								}
							}
						}else{
							documentKind.logic().validate(values, documentKind,
									null);
						}
						List<Long> attachmentsToClone = new ArrayList<Long>(
								copyAttachments.size());
						List<Long> attachmentsToDelAfter = new ArrayList<Long>();
						for (Iterator iter = copyAttachments.entrySet()
								.iterator(); iter.hasNext();) {
							Map.Entry entry = (Map.Entry) iter.next();
							Long attachmentId = new Long(entry.getKey()
									.toString());
							String copyOrMove = HttpUtils.valueOrNull(entry
									.getValue());
							if ("copy".equals(copyOrMove)
									|| "move".equals(copyOrMove))
								attachmentsToClone.add(attachmentId);
							if ("move".equals(copyOrMove))
								attachmentsToDelAfter.add(attachmentId);
						}
						
						Document newDocument = document
								.cloneObject((Long[]) attachmentsToClone
										.toArray(new Long[attachmentsToClone
												.size()]));
						newDocument.setUnsafeCtime(new Date());


						// w przypadku pism wychodzacych nie tworzony jest nowy
						// sender, w przypadku pism wychodzacy/przychodzacych
						// uzupelniane jest pole sendera w bazie pisma starego
						//odbiorcy wprzypadku pisma przychodzacego maja zle id dokumentu(poprawiane jest tu)
						if (document instanceof OfficeDocument
								&& ((OfficeDocument) document).getSenderId() != null) {

							tmpSender.setDocumentId(document.getId());
							tmpSender.setDictionaryGuid("rootdivision");
							tmpSender.setDictionaryType("sender");

							// ustawienie pol sendera dla in
							if (newDocument instanceof InOfficeDocument) {

								Person p = Person
										.find((long) ((OfficeDocument) newDocument)
												.getSenderId());
								p.setDocumentId(newDocument.getId());
								p.setDictionaryGuid("rootdivision");
								p.setDictionaryType("sender");
								//ustawienie dla recipient�w document_id
								List<Recipient> lista = ((OfficeDocument)newDocument).getRecipients();
								Iterator it = lista.iterator();
								while (it.hasNext()) {
									Recipient r = (Recipient) it.next();
									r.setDocumentId(newDocument.getId());
									r.setDocument(newDocument);
								}

							}
							// swtorzenie nowego dla out
							if (document instanceof OutOfficeDocument) {
								Person p = Person
										.find((long) ((OfficeDocument) document)
												.getSenderId());
								p.setDocumentId(newDocument.getId());
								p.setDictionaryType("sender");

								Sender tmpSender2 = new Sender();

								if (document instanceof OfficeDocument) {
									tmpSenderId = ((OfficeDocument) document)
											.getSenderId();
									Person person = Person.find(tmpSenderId);
									tmpSender2.fromMap(person.toMap());

								}
								tmpSender2.setDictionaryGuid(null);
								tmpSender2.setDocument(newDocument);
								tmpSender2.create();
								((OfficeDocument) newDocument)
										.setSender(tmpSender2);
								((OfficeDocument) newDocument)
										.setSenderId(tmpSender2.getId());
							}

						}

						

						// wyrzucenie przeniesionych zalacznikow
						for (Long id : attachmentsToDelAfter)
							document.scrubAttachment(id);
						//w przypadku gdy nie pobralo nadawcy(nie ma go) to ustawia sendera, aby sie nie wywalalo w documentuserdivision
						if(values.containsKey("SENDER_HERE")){
							if(!(values.get("SENDER_HERE")!=null))
								values.put("SENDER_HERE", "");
						}

						if(AvailabilityManager.isAvailable("archiwizacja.klonowanie.oldOfficeNumbers")){
							values.remove("JOURNAL");
						}
						/* zapisywanie danych biznesowych */
						documentKind.set(newDocument.getId(), values);

						documentKind.logic().archiveActions(newDocument,
								DocumentKindsManager.getType(newDocument));

						documentKind.logic().documentPermissions(newDocument);

						/*
						 * na wypadek gdyby oryginalny dokument by� podpisany
						 * podpisem elektronicznym
						 */
						newDocument.setBlocked(false);
						
						// ustawianie wersji
						if (nowa_wersja) {
							OfficeDocument doc = OfficeDocument.find(documentId);
							List<OfficeDocument> docActive = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
							for (OfficeDocument od : docActive) {
								od.setCzyAktualny(false);
							}
							newDocument.setCzyAktualny(true);
							newDocument.setCzyCzystopis(false);
							String sql = "select MAX(version)as LastVersion from ds_document where version_guid="
									+ stareGuid;
							PreparedStatement ps = DSApi.context()
									.prepareStatement(sql);
							ResultSet rs = ps.executeQuery();
							rs.next();

							newDocument
									.setVersion(rs.getInt("LastVersion") + 1);
							newDocument.setVersionGuid(stareGuid);
							newDocument.setVersionDesc(nowy_opis);
							
							if (document instanceof OfficeDocument) {
								listaAudit.add(Audit.create(
												"newVersion",
												DSApi.context()
														.getPrincipalName(),
												sm.getString("UtworzonoNowaWersje")));
								listaAudit
								.add(Audit.create(
										"Active",
										DSApi.context().getPrincipalName(),
										sm.getString("WersjaNieAktywna")
										+ " "
										+ document
												.getVersion()));
								listaAudit
								.add(Audit.create("Active",
										DSApi.context().getPrincipalName(),
										sm.getString("WersjaAktywna") + " "
												+ newDocument.getVersion()));

							}

						} else {
							if (document instanceof OfficeDocument) {
								listaAudit.add(Audit.create(
												"::nw_clone",
												DSApi.context()
														.getPrincipalName(),
												sm.getString("DokumentZostalSklonowany")));

							}

							newDocument.setCzyAktualny(true);
							newDocument.setCzyCzystopis(false);
							newDocument.setVersion(1);
							newDocument.setVersionGuid((int) (long) newDocument
									.getId());

						
						}
						if (newDocument instanceof InOfficeDocument)
							((InOfficeDocument) newDocument)
									.setSummary(newDocument
											.getDescription());
						if (newDocument instanceof OutOfficeDocument)
							((OutOfficeDocument) newDocument)
									.setSummary(newDocument
											.getDescription());
						
						//w set ja czysci wiec znow uzupelniam(wczesniej zapisana)
						document.setWorkHistory(listaAudit);
						folderId = newDocument.getFolderId();

						// klonowanie historii pisma
						if(nowa_wersja){
/*							if (AvailabilityManager.isAvailable("dokument.usuwaStareWpisyZHistoriiZanimUstawiaNowa"))
							{
								try
								{
									PreparedStatement ps = DSApi.context().prepareStatement("delete from dso_document_audit where document_id=" + document);
									ps.executeUpdate();
								} catch (SQLException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (EdmException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}*/
						newDocument.setWorkHistory(document.getWorkHistory());
						//int i=0;
						for (Audit audit : document.getWorkHistory()) {
							if (audit.getProperty() != null){
								((OfficeDocument) newDocument)
										.addWorkHistoryEntry(audit);
							}
							
						}
						
						}
						else{
							for (Audit audit : document.getWorkHistory()) {
								if (audit.getProperty() != null){
									((OfficeDocument) document)
											.addWorkHistoryEntry(audit);
								}
								
							}
						
						
						
					/*	for (AssignmentHistoryEntry ahe : ((OfficeDocument) document)
								.getAssignmentHistory()) {
							((OfficeDocument) newDocument)
									.addAssignmentHistoryEntry(ahe);
							
						}
						
						((OfficeDocument) newDocument)
								.setAssignmentHistory(((OfficeDocument) document)
										.getAssignmentHistory());*/
						}
						if (newDocument instanceof OfficeDocument) {
							// p4
							if (AvailabilityManager
									.isAvailable("p4CloneDocument")) {
								// documentKind.saveDictionaryValues(newDocument,
								// values);
								// documentKind.saveMultiDictionaryValues(newDocument,
								// values);
								Date entryDate = GlobalPreferences
										.getCurrentDay();
								Journal journal = Journal.getMainIncoming();
								Long journalId = journal.getId();
								DSApi.context().session().flush();
								Integer sequenceId = Journal.TX_newEntry2(
										journalId, newDocument.getId(),
										entryDate);
								((InOfficeDocument) newDocument).bindToJournal(
										journalId, sequenceId, event);

								newDocument
										.getDocumentKind()
										.logic()
										.onStartProcess(
												(OfficeDocument) newDocument,
												event);
							} else{
								if(nowa_wersja){
									//zamkniecie procesu dla starego dokumentu
									OfficeDocument doc = OfficeDocument.find(documentId);
									List<OfficeDocument> guid = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
									for(OfficeDocument od:guid){
										if(JBPMTaskSnapshot.findByDocumentId(od.getId()) != null){
											List<JBPMTaskSnapshot> taskId = JBPMTaskSnapshot.findByDocumentId(od.getId());
											if(taskId.size() != 0){
												if(taskId.size()>1){
													for(int i=0;i<taskId.size()-1;i++){
														WorkflowFactory wf = WorkflowFactory.getInstance();
														Set<Long> docIds = Sets.newLinkedHashSet();

														docIds.add(taskId.get(i).getDocumentId());

														if(wf instanceof InternalWorkflowFactory){
															for (Long docId : docIds) {
																String activityId = wf.findManualTask(docId);
																wf.manualFinish(activityId, false);
															}
														} else if(wf instanceof Jbpm4WorkflowFactory) {
															for (Long docId : docIds) {
																if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
																	String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
																	wf.manualFinish(firstActivityId, false);
																}
															}
														} else {
															throw new IllegalStateException("nieznany WorkflowFactory");
														}
													}
												} else {
													for(int i=0;i<taskId.size();i++){
														WorkflowFactory wf = WorkflowFactory.getInstance();
														Set<Long> docIds = Sets.newLinkedHashSet();

														docIds.add(taskId.get(i).getDocumentId());

														if(wf instanceof InternalWorkflowFactory){
															for (Long docId : docIds) {
																String activityId = wf.findManualTask(docId);
																wf.manualFinish(activityId, false);
															}
														} else if(wf instanceof Jbpm4WorkflowFactory) {
															for (Long docId : docIds) {
																if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
																	String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
																	wf.manualFinish(firstActivityId, false);
																}
															}
														} else if(wf instanceof CompositeWorkflowFactory) {
															// TODO zakonczenie pracy z klonem dokumentu (activiti)
														} else {
															throw new IllegalStateException("nieznany WorkflowFactory");
														}
													}
												}
											}
										}
									}
								}
							}
							//utworzenie procesu dla nowego dokumentu
							newDocument.getDocumentKind().logic().onStartProcess(
									(OfficeDocument) newDocument,event);
						}
						if (nowa_wersja) {
							String sql = "delete from dso_document_asgn_history where document_id="
									+ document.getId();
							PreparedStatement ps = DSApi.context()
									.prepareStatement(sql);
							ps.executeUpdate();
						} else {
							/*for (int i = 0; i < listaAHE.size(); i++) {
								((OfficeDocument) document)
										.addAssignmentHistoryEntry(listaAHE
												.get(i));

							}*/

							((OfficeDocument) document)
									.setAssignmentHistory(listaAHE);

						}

						DSApi.context().commit();
						PreparedStatement ps = null;
						ResultSet rs = null;
						// sklonowanie uprawnie� dla nowego dokumentu
						// uprawnienia dla grupy
						newDocument.getDocumentKind().logic()
								.documentPermissions(newDocument);
						DSApi.context().session().save(newDocument);
						// uprawniania dla autora
						java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

						DSUser author = DSApi.context().getDSUser();
						String user = author.getName();
						String fullName = author.getLastname() + " "
								+ author.getFirstname();

						perms.add(new PermissionBean(ObjectPermission.READ,
								user, ObjectPermission.USER, fullName + " ("
										+ user + ")"));
						perms.add(new PermissionBean(
								ObjectPermission.READ_ATTACHMENTS, user,
								ObjectPermission.USER, fullName + " (" + user
										+ ")"));
						perms.add(new PermissionBean(ObjectPermission.MODIFY,
								user, ObjectPermission.USER, fullName + " ("
										+ user + ")"));
						perms.add(new PermissionBean(
								ObjectPermission.MODIFY_ATTACHMENTS, user,
								ObjectPermission.USER, fullName + " (" + user
										+ ")"));
						perms.add(new PermissionBean(ObjectPermission.DELETE,
								user, ObjectPermission.USER, fullName + " ("
										+ user + ")"));

						Set<PermissionBean> documentPermissions = DSApi
								.context().getDocumentPermissions(newDocument);
						perms.addAll(documentPermissions);
						((AbstractDocumentLogic) newDocument.getDocumentKind()
								.logic()).setUpPermission(newDocument, perms);

						if (newDocument instanceof OfficeDocument) {
							// flush i tak robi sie w
							// updateDSApi.context().session().flush();
							DSApi.context().begin();
							if(!nowa_wersja && !AvailabilityManager.isAvailable("archiwizacja.klonowanie.oldOfficeNumbers")){
								ps = null;
								rs = null;
								if(newDocument instanceof InOfficeDocument) {
									ps = DSApi
											.context()
											.prepareStatement(
													"SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=1");
									ps.setLong(1, newDocument.getId());
									rs = ps.executeQuery();
									if(rs.next()){
										((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
									}
								} else if( newDocument instanceof OutOfficeDocument) {
									if(newDocument.getType() == DocumentType.INTERNAL){
										Date entryDate = GlobalPreferences.getCurrentDay();
										Long journalId = Journal.getMainInternal().getId();
										Integer sequenceId = Journal.TX_newEntry2(
												journalId, newDocument.getId(), entryDate);
										OutOfficeDocument doc = OutOfficeDocument
												.findOutOfficeDocument(newDocument.getId());
										doc.bindToJournal(journalId, sequenceId);
										ps = DSApi
												.context()
												.prepareStatement(
														"SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=3");
										ps.setLong(1, newDocument.getId());
										rs = ps.executeQuery();
										if(rs.next()){
											((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
										}

									} else {
										OfficeDocument oldDoc = OfficeDocument.find(documentId);
										if(oldDoc.getOfficeNumber() != null){
											Date entryDate = GlobalPreferences.getCurrentDay();
											Long journalId = Journal.getMainOutgoing().getId();
											Integer sequenceId = Journal.TX_newEntry2(
													journalId, newDocument.getId(), entryDate);
											OutOfficeDocument doc = OutOfficeDocument
													.findOutOfficeDocument(newDocument.getId());
											doc.bindToJournal(journalId, sequenceId);
											ps = DSApi
													.context()
													.prepareStatement(
															"SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=2");
											ps.setLong(1, newDocument.getId());
											rs = ps.executeQuery();
											if(rs.next()){
												((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
											}
										}
									}
								}
							}
							if(AvailabilityManager.isAvailable("archiwizacja.klonowanie.oldOfficeNumbers")){
								List<JournalEntry> journalEntries = JournalEntry.findByDocumentId(documentId);
								
								for(JournalEntry journalEntry: journalEntries){
									try{
										PreparedStatement preparedStatement = DSApi.context().prepareStatement("INSERT INTO DSO_JOURNAL_ENTRY(DOCUMENTID,SEQUENCEID,CTIME,entrydate,JOURNAL_ID,dailySequenceId,author) VALUES(?,?,?,?,?,?,?)");
							            
										preparedStatement.setLong(1, newDocument.getId());
										preparedStatement.setInt(2, journalEntry.getSequenceId());
										preparedStatement.setTimestamp(3, new java.sql.Timestamp(journalEntry.getCtime().getTime()));
							            preparedStatement.setDate(4, new java.sql.Date(journalEntry.getEntryDate().getTime()));
							           	preparedStatement.setLong(5, journalEntry.getJournal().getId());
							           	preparedStatement.setLong(6, journalEntry.getDailySequenceId());
							           	preparedStatement.setString(7, journalEntry.getAuthor());
							           	
							            preparedStatement.executeUpdate();
									}
									catch(Exception e){	}
								}
							}
						TaskSnapshot.updateByDocumentId(
									newDocument.getId(),
									((OfficeDocument) newDocument)
											.getStringType());
						
						Map<Long,String> label = LabelsManager.getLabelBeanByDocumentId(documentId, "label");
						if(!label.isEmpty()){
							for(Map.Entry<Long, String> l:label.entrySet()){
								LabelsManager.addLabel(newDocument.getId(), l.getKey(), DSApi.context().getPrincipalName());
							}
						}

							/*
							 * JBPMTaskSnapshot.updateAllTasksByDocumentId(
							 * newDocument.getId(),((OfficeDocument)
							 * newDocument) .getStringType() );
							 */
							DSApi.context().commit();
						}
						
						if (nowa_wersja){
							uaktualnijSprawy(documentId, newDocument.getId());
							
							OfficeDocument doc = OfficeDocument.find(newDocument.getId());
							OfficeDocument oldDoc = OfficeDocument.find(documentId);
							doc.setOfficeNumber(oldDoc.getOfficeNumber());
							List<DSUser> dsUsers = doc.getDocumentKind().logic().getUserWithAccessToDocument(newDocument.getId());
							if(dsUsers != null){
								for (DSUser dsUser : dsUsers) {
									DocumentMailHandler.createEvent(newDocument.getId(), NormalLogic.MAILNEWVERSION, dsUser.getEmail());
								}
							}
						}

						if (AvailabilityManager.isAvailable("p4CloneDocument"))
							event.setResult("task-list");
						else
							event.setResult("task-list");

					}
				}
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			} catch (SQLException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
		

		// uaktualnienie dokumentow przypisanych do spraw
		//(dla utp)uaktualnienie bazy powiazanych dokumentow(usuniecie wszystkich dokumentow powiazanych dla dokumentu starego)
		private void uaktualnijSprawy(Long stareId, Long noweId) {
			try {
				DSApi.context().begin();
				Long caseId = null;
				String caseDocumentId = null;
				String sql = null;
				ResultSet rs = null;
				PreparedStatement ps = null;
				if (Document.find(stareId) instanceof InOfficeDocument) {
					sql = "select case_id,casedocumentid from dso_in_document where id = "
							+ stareId.toString();
					ps = DSApi.context().prepareStatement(sql);
					rs = ps.executeQuery();
					if (rs.next()) {
						caseId = rs.getLong("case_id");
						caseDocumentId = rs.getString("casedocumentid");
					}

					// tabela in

					sql = "update dso_in_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
							+ stareId.toString();
					ps = DSApi.context().prepareStatement(sql);
					ps.executeUpdate();

					// jesli dokument jest przypisany do sprawy
					if (caseId != 0) {
						sql = "update dso_in_document set case_id = "
								+ caseId.toString() + ", casedocumentid='"
								+ caseDocumentId + "' where id = "
								+ noweId.toString();
					} else {
						sql = "update dso_in_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
								+ noweId.toString();
					}
					ps = DSApi.context().prepareStatement(sql);
					ps.executeUpdate();

				} else if (Document.find(stareId) instanceof OutOfficeDocument) {

					sql = "select case_id,casedocumentid from dso_out_document where id = "
							+ stareId.toString();
					ps = DSApi.context().prepareStatement(sql);
					rs = ps.executeQuery();
					if (rs.next()) {
						caseId = rs.getLong("case_id");
						caseDocumentId = rs.getString("casedocumentid");
					}

					// tabela out

					sql = "update dso_out_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
							+ stareId.toString();
					ps = DSApi.context().prepareStatement(sql);

					ps.executeUpdate();
					// jesli dokument jest przypisany do sprawy
					if (caseId != 0) {
						sql = "update dso_out_document set case_id = "
								+ caseId.toString() + ", casedocumentid='"
								+ caseDocumentId + "' where id = "
								+ noweId.toString();
					} else {

						sql = "update dso_out_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
								+ noweId.toString();
					}
					ps = DSApi.context().prepareStatement(sql);
					ps.executeUpdate();

				}
				
//				  sql = "update DSW_JBPM_TASKLIST set  document_id= " +
//				  noweId.toString() + " where document_id=" +
//				  stareId.toString(); ps =
//				  DSApi.context().prepareStatement(sql); 
//				  ps.executeUpdate();
				String tabela_multi = Document.find(stareId).getDocumentKind().getMultipleTableName();
				 if(AvailabilityManager.isAvailable("utp.usuwajDokumentyPowiazanePrzyZmianieWersji")){
					  sql = "delete from " + tabela_multi + " where (document_id="+stareId.toString()+" or field_val="+stareId.toString()+") and field_cn='LINKS'";
							  ps=DSApi.context().prepareStatement(sql); 
							  ps.executeUpdate();
					  }

				ps.close();
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}

			catch (SQLException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}

		}
	}

	public FieldsManager getFm() {
		return fm;
	}

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public List getAttachments() {
		return attachments;
	}

	public boolean isCanModify() {
		return canModify;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public Long getFolderId() {
		return folderId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Map getCopyAttachments() {
		return copyAttachments;
	}

	public void setCopyAttachments(Map copyAttachments) {
		this.copyAttachments = copyAttachments;
	}

	public boolean isCanReadDictionaries() {
		return canReadDictionaries;
	}

	public String getDockindAction() {
		return "clone";
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public int getDocType() {

		return docType;
	}

	public boolean isCanChangeDockind() {
		try {
			return getDocument().getDocumentKind().logic()
					.canChangeDockind(getDocument());
		} catch (NullPointerException e) {

		} catch (EdmException e) {

		}

		return true;
	}
	
	public boolean getWyslano() {
		return wyslano;
	}
	
	public void setWyslano(boolean wyslano) {
		this.wyslano=wyslano;
	}
}
