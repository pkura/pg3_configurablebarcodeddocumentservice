package pl.compan.docusafe.web.archive.repository.search;

import pl.compan.docusafe.core.dockinds.DocumentKind;

import java.util.List;

public class SearchDocumentsRequest {
    public Long id;
    public String title;
    public List<DocumentKind> documentKinds;
    public String dateFrom;
    public String dateTo;
    public Long authorUserId;
    public String accessedBy;
    public String lastRemark;
    public String endMtime;
    public String endCtime;
    public String startMtime;
    public String startCtime;
    public String attachmentBarcode;
    public String author;
    public String attribute;
    public String description;
    public int limit;
    public Long[] doctypeIds;
    public Long[] documentKindIds;
    public Long boxId;
    public String[] accessedAs;
    public String sortField;
    public boolean ascending;
    public Long documentId;
    public String documentBarcode;
    public String documentIdString;
    public int offset;
    public String encoding;

    public void copyProperties(SearchDocumentsAction action) {
        this.id = action.getId();
        this.title = action.getTitle();
        this.documentKinds = action.getDocumentKinds();
        this.dateFrom = action.getDateFrom();
        this.dateTo = action.getDateTo();
        this.authorUserId = action.getAuthorUserId();
        this.accessedBy = action.getAccessedBy();
        this.lastRemark = action.getLastRemark();
        this.endMtime = action.getEndMtime();
        this.endCtime = action.getEndCtime();
        this.startMtime = action.getStartMtime();
        this.startCtime = action.getStartCtime();
        this.attachmentBarcode = action.getAttachmentBarcode();
        this.author = action.getAuthor();
        this.attribute = action.getAttribute();
        this.description = action.getDescription();
        this.limit = action.getLimit();
        this.doctypeIds = action.getDoctypeIds();
        this.documentKindIds = action.getDocumentKindIds();
        this.boxId = action.getBoxId();
        this.accessedAs = action.getAccessedAs();
        this.sortField = action.getSortField();
        this.ascending = action.getAscending();
        this.documentId = action.getDocumentId();
        this.documentIdString = action.getDocumentIdString();
        this.documentBarcode = action.getDocumentBarcode();
        this.offset = action.getOffset();
    }
}
