
package pl.compan.docusafe.web.archive.repository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.FolderNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.UrlVisitorWithAnchors;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.security.SetPermissionsAction;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.FoldersTree;
import pl.compan.docusafe.web.viewserver.ViewServer;

/**
 * Akcja pozwalaj�ca na "chodzenie" po folderach i ogl�danie znajduj�cych
 * si� w nich dokument�w.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExploreDocumentsAction.java,v 1.36 2009/12/08 11:24:17 pecet3 Exp $
 */
public class ExploreDocumentsAction extends EventProcessingAction
{
    
	public static final String FORWARD = "repository/explore-documents";

    public static final String SORT_TITLE = "title";

	public static final String SORT_CTIME = "ctime";
    public static final String SORT_MTIME = "mtime";
    public static final String SORT_AUTHOR = "author";
  
    private static final String EVENT_MAIN = "ev_main";
    private static final String EVENT_POPUP = "ev_popup";
    private   boolean deleteDocument = false;
    public static final String EXPLORE_DOCUMENTS_DEFAULT_SORT_MODE = "explore.documents.default.sortMode";
    public static final String EXPLORE_DOCUMENTS_DEFAULT_ASCENDING = "explore.documents.default.ascending";
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(ExploreDocumentsAction.class.getPackage().getName(),null);

	private static final Logger log = LoggerFactory.getLogger(ExploreDocumentsAction.class);

    public static Long idReq;

    public static final String FOLDERURL = "/repository/create-new-folder.action?folderId=";
    
    protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
				append(fillForm).
				append(EVENT_MAIN, new SetActionForwardListener("main")).
				append(EVENT_POPUP, new SetActionForwardListener("popup"));
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            DynaActionForm form = event.getDynaForm();
            HttpServletRequest request = event.getRequest();
            if (form.get("doDeleteDocument")!=null)
             deleteDocument = form.get("doDeleteDocument").equals("true");
            if (deleteDocument){
            	Delete d = new Delete();
            	d.actionPerformed(event);
            }
            	
            Long folderId = (Long) form.get("folderId");

            final String param = ""+ form.get("param");
            request.setAttribute("param", ""+param);
            
            Long myId = (Long) form.get("myId");

            final int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;

            String defaultSortMode = StringUtils.defaultString(Docusafe.getAdditionProperty(EXPLORE_DOCUMENTS_DEFAULT_SORT_MODE), SORT_TITLE);
            final String sortMode = StringUtils.isEmpty((String) form.get("sortMode")) ? defaultSortMode : (String) form.get("sortMode");

            boolean defaultAscending = AvailabilityManager.isAvailable(EXPLORE_DOCUMENTS_DEFAULT_ASCENDING);
            final Boolean ascending = form.get("ascending") != null ? (Boolean) form.get("ascending") : defaultAscending;
            
            final Boolean pdf = form.get("printToPdf") != null ? (Boolean) form.get("printToPdf") : Boolean.FALSE;

            
            final Boolean popup = form.get("popupwin") != null ? (Boolean) form.get("popupwin") : Boolean.FALSE; 
            //form.set("popupwin", popup);
            //request.setAttribute("popupwin", popup);
            
            
            
            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(request));

                final DocumentHelper dh = ctx.getDocumentHelper();

                Folder _folder;

                if (folderId != null)
                {
                    try
                    {
                        _folder = Folder.find(folderId);
                    }
                    catch (FolderNotFoundException e)
                    {
                        _folder = Folder.getRootFolder();
                    }
                }
                else
                {
                    _folder = Folder.getRootFolder();
                }

                // potrzebuj� zmienn� finaln� dla interfejsu Pager.LinkVisitor
                final Folder folder = _folder;

                UrlVisitorWithAnchors urlVisitor = new UrlVisitorWithAnchors()
                {
                    public String getUrl(Object element)
                    {
                        return event.getRequest().getContextPath() +
                            getLink(event.getRequest(), ((Folder) element).getId(), popup.booleanValue(),param);
                    }
                };

                HtmlTree tree = FoldersTree.newTree(ctx, folder, urlVisitor,
                    request.getContextPath());

                setReqId( folder.getId() );

                if (tree != null)
                {
                    request.setAttribute("treeHtml", tree.generateTree());
                }


                final int LIMIT = 20;

//                Document[] documents = null;

                // TODO: zrobi� SearchResult
//                documents = dh.getDocuments(folder, offset, LIMIT+1,  
//                    sortMode, ascending.booleanValue());

                boolean showDeleted = folder.isSystem(Folder.SN_TRASH);//.equals(folder.getSystemName());

                SearchResults results = dh.searchDocuments(offset, LIMIT, folder, 
                    showDeleted, sortMode, ascending.booleanValue());

                // u�ytkownik mo�e usun�� folder, je�eli w folderze nie ma
                // dokument�w oraz u�ytkownik posiada odpowiednie uprawnienia
                boolean canDeleteFolder =
                    !folder.isRoot() &&
                    results.count() == 0 &&
                    !folder.isUndeletable() &&
                    folder.canDelete();

                boolean canCreate = folder.canCreate();

                List documentBeans = new ArrayList(results.count());

                DynaProperty[] props = new DynaProperty[] {
                    new DynaProperty("id", Long.class),
                    new DynaProperty("title", String.class),
                    new DynaProperty("ctime", Date.class),
                    new DynaProperty("mtime", Date.class),
                    new DynaProperty("author", String.class),
                    new DynaProperty("editLink", String.class),
                    new DynaProperty("canDelete", Boolean.class),
                    new DynaProperty("canReadAttachments", Boolean.class),
                    /*new DynaProperty("revisionLink", String.class),
                    new DynaProperty("viewerLink", String.class),*/
                    new DynaProperty("attachments", ArrayList.class),
                    new DynaProperty("isDeleted", Boolean.class)
                };
                DynaClass documentClass = new BasicDynaClass("document", null, props);

                //for (int i=0, n=documents.length; i < n && i < LIMIT; i++)

                // lista dokumentow z zaznaczonego folderu
                List <Long> dockid = new LinkedList <Long> ();
                String dockindCn = null;
                for ( ; results.hasNext(); )
                {
                    Document document = (Document) results.next();
                    dockid.add(document.getId());
                    if(document.getDocumentKind() != null)
                    {
                    	dockindCn = document.getDocumentKind().getCn();
                    }
                    DynaBean bean = new BasicDynaBean(documentClass);
                    bean.set("id", document.getId());
                    bean.set("title", document.getTitle());
                    bean.set("ctime", document.getCtime());
                    bean.set("mtime", document.getMtime());
                    bean.set("isDeleted", Boolean.valueOf(document.isDeleted()));
                    bean.set("author", DSUser.safeToFirstnameLastnameName(document.getAuthor()));
                    
                    if(popup)
                    {
                    	bean.set("editLink", request.getContextPath()+"/repository/edit-dockind-document-popup.action?popup=true"+"&id="+document.getId());
                    }
                    else
                    {
                    	bean.set("editLink", request.getContextPath()+"/repository/edit-document.action"+"?id="+document.getId());                        
                    }
                    
                    bean.set("canDelete", Boolean.valueOf(document.canDelete()));
                    bean.set("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));

                                        
                    /* za��czniki */
                    List attachmentList = document.listAttachments();
                    ArrayList<Map<String, Object>> attachments = new ArrayList<Map<String, Object>>(attachmentList.size());
                    
                    for(Iterator iter = attachmentList.iterator(); iter.hasNext(); ) {
                    	Attachment attachment = (Attachment) iter.next();
                    	
                    	if(attachment == null)
                    		continue;
                    	
                    	Map<String, Object> attBean = new HashMap<String, Object>();
                    	
                    	AttachmentRevision rev = attachment.getMostRecentRevision();
                    	
                    	if(rev != null) {
                    		attBean.put("revisionLink", request.getContextPath() + ViewAttachmentRevisionAction.getLink(request, rev.getId()));
                    		
                    		if(ViewServer.mimeAcceptable(rev.getMime()))
                    			attBean.put("viewerLink", 
                    					"javascript:openToolWindow('"+request.getContextPath()+"/viewserver/viewer.action?id="+rev.getId()+"&width=1000&heighte=750','vs',1000,750);");
                    		else
                    			attBean.put("viewerLink", "");
                    	}
                    	
                    	
                    	attBean.put("id", attachment.getId());
                    	attBean.put("title", attachment.getTitle());
                    	attBean.put("cn", attachment.getCn());
                    	
                    	attachments.add(attBean);
                    }
                    bean.set("attachments", attachments);
                    
                    documentBeans.add(bean);
                }
                boolean showCtime = AvailabilityManager.isAvailable("exploreDocument.ctime", dockindCn);
                request.setAttribute("showCtime", showCtime);
                
                request.setAttribute("documentBeans", documentBeans);
                

                // klasa tworz�ca odno�niki do kolejnych stron wynik�w
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return getExploreLink(offset, LIMIT, folder.getId(), sortMode,
                            ascending.booleanValue(),popup.booleanValue());
                    }
                };

                Pager pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                request.setAttribute("pager", pager);
                

                if (canCreate)
                {
                    //request.setAttribute("newDocumentLink", "/repository/new.action?folderId="+folder.getId()); 
                	request.setAttribute("newDocumentLink", "/repository/new-dockind-document.action?folderId="+folder.getId());
                    request.setAttribute("newFolderLink", NewFolderAction.getLink(request, folder.getId()));
                }

                if (folder.canModify())
                    request.setAttribute("renameFolderLink", RenameFolderAction.getLink(request, folder.getId()));

                if (ctx.getSecurityHelper().canSetPermissions(folder))
                    request.setAttribute("editFolderPermissionsLink", SetPermissionsAction.getLink(
                        request, "folder", folder.getId()));

                if (canDeleteFolder)
                    request.setAttribute("deleteFolderLink", DeleteFolderAction.getLink(
                        request, folder.getId(), folder.getPrettyPath()));

                if (DocumentKind.isAvailable(DocumentLogicLoader.NATIONWIDE_KIND) || DocumentKind.isAvailable(DocumentLogicLoader.DP_PTE_KIND))
                    request.setAttribute("printToPdf", "/repository/explore-documents.do?printToPdf=true&folderId="+folder.getId());

                //request.setAttribute("canCreateDocumentsAndFolders", Boolean.valueOf(sh.hasPermission(folder, Permission.CREATE)));

                request.setAttribute("folder", folder);

                // odno�niki sortowania
                request.setAttribute("title_up", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_TITLE, true,popup.booleanValue()));
                request.setAttribute("title_down", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_TITLE, false,popup.booleanValue()));

                request.setAttribute("ctime_up", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_CTIME, true,popup.booleanValue()));
                request.setAttribute("ctime_down", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_CTIME, false,popup.booleanValue()));

                request.setAttribute("mtime_up", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_MTIME, true,popup.booleanValue()));
                request.setAttribute("mtime_down", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_MTIME, false,popup.booleanValue()));

                request.setAttribute("author_up", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_AUTHOR, true,popup.booleanValue()));
                request.setAttribute("author_down", request.getContextPath() +
                    getLink(request, folder.getId(), SORT_AUTHOR, false,popup.booleanValue()));
                if(param != null && param.length() > 0)
                	request.setAttribute("dicParam", param);
                if (dockid.size() > 0 && pdf) {
                	generatePdf(dockid, event);
                }
            }
            catch (EdmException e) {
				event.getErrors().add(e.getMessage());
				log.error(e.getMessage(), e);
			} catch (Exception e) {
				event.getErrors().add(e.getLocalizedMessage());
				log.error(e.getMessage(), e);
			} finally {
				DSApi._close();
			}
			
			if(popup)
			{
				event.skip(EVENT_MAIN);
			}
			else
			{
				event.skip(EVENT_POPUP);
			}
        }
    }

    
    private void generatePdf(List <Long> documentIds, ActionEvent event) {
		 try{

            DSApi.context().begin();

            Document[] docsView = new Document[documentIds.size()];
            
            for(int i=0;i<documentIds.size();i++)
            {
               docsView[i]= Document.find(documentIds.get(i));
            }
            File tmp = null;
            try
            {
               tmp = File.createTempFile("docusafe_", ".pdf");
               OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));

               PdfUtils.attachmentsAsPdf(docsView, true, os);
               os.close();
            }
            catch (Exception e)
            {
               event.getErrors().add(e.getMessage());
               log.error(e.getMessage(), e);
            }
            finally
            {
           	 DSApi.context().setRollbackOnly();
            }

            if (tmp != null)
            {
                try
                {
                 	 HttpServletResponse resp = event.getResponse();
                  	 resp.setContentType("application/pdf");
              		 resp.setHeader("Content-disposition", "attachment; filename=\"folder.pdf\"");
                     ServletUtils.streamFile(resp, tmp, "application/pdf");
                }
                catch (IOException e)                 
                {
               	 	event.getErrors().add("Nie uda�o si� pobrac dokument�w. ".concat(e.getMessage()));
                    log.error(e.getMessage(), e);
                }
                finally
                {
                    tmp.delete();
                }
            }
            DSApi.context().rollback();
        }
        catch(EdmException e)
        {        	
            event.getErrors().add(e.getMessage());
            log.error(e.getMessage(), e);
        }
	}

    /**
     * Usuwanie permanentnie  dokumentu i za�acznik�w z bazy .
     */
    private class Delete implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{	
			
			
			Map<Integer, String> listSQls = new HashMap<Integer, String>();
			Long documentId = (Long) event.getDynaForm().get("documentId");
			List<Journal> journals;
			listSQls.put(1, "Delete from dso_person where document_id =? ");
			listSQls.put(2, "delete from DSO_DOCUMENT_ASGN_HISTORY where document_id =?");
			listSQls.put(3, "delete from DSO_DOCUMENT_AUDIT where document_id =?");
			listSQls.put(4, "delete from DS_DOCUMENT_CHANGELOG  where   document_id = ?");
			listSQls.put(6, "delete from dso_in_document  where id = ?");
			listSQls.put(7, "delete from dso_out_document  where id = ?");
			listSQls.put(8, "delete from ds_document  where id = ?");
			listSQls.put(9, "delete from DSW_JBPM_TASKLIST where DOCUMENT_ID = ?");
			String deleteFromJournalEntry = "Delete from dso_journal_entry where documentid =? and journal_id=?";
			String deleteFromAttachmentRevs = "Delete from DS_ATTACHMENT_REVISION where attachment_id =? ";
			String deleteFromDSAttachment = "Delete from DS_ATTACHMENT where id =? ";

			DSContext ctx = null;
			try
			{
				
				ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
				ctx.begin();
				if(!DSApi.context().isAdmin() && !DSApi.context().hasPermission(DSPermission.DOKUMENT_PERMANENTLY_DELETE)){
					throw new EdmException("Nie posiadasz uprawnien do usuniecia dokumentu z bazy danych");
				}

				Document doc = ctx.getDocumentHelper().getDocument(documentId);
				listSQls.put(5, "Delete from " + doc.getDocumentKind().getTablename() + " where document_id=?"); 
				PrintWriter pw = new PrintWriter(prepareFileToDeleteDocumentLog());
				pw.append("Usuniecie Dokumentu Info:\n");
				pw.append("Osoba usuwajca = " +DSApi.context().getDSUser().asFirstnameLastname()+"\n");
				fillDeletedDocumentInfo(pw ,doc);
	
				//konczenie wszystkic hproces�w zwi�zanyc hz usuwanym dokumentem 
				for (String aid : Jbpm4ProcessLocator.taskIds(documentId))
				{
					TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(aid.replaceAll("[^,]*,", ""));
					if (taskSnapshot != null)
						WorkflowFactory.getInstance().manualFinish(aid, false);
				}
				//usuwanie za�acznikow 
				pw.append("Usunieto za�aczniki : " +"\n");
				List<Attachment> listAttachments = doc.getAttachments();
				List<Long> attachmentsIDs = new ArrayList<Long>();
				for (Attachment att : listAttachments)
				{
					attachmentsIDs.add(att.getId());
					PreparedStatement ps = DSApi.context().prepareStatement(deleteFromAttachmentRevs);
					ps.setLong(1, att.getId());
					ps.execute();
					ps.close();
					pw.append("Tytu� : "+att.getTitle() +"\n");

				}
				for (Long attID : attachmentsIDs)
				{
					PreparedStatement ps = DSApi.context().prepareStatement(deleteFromDSAttachment);
					ps.setLong(1, attID);
					ps.execute();
					ps.close();
				}
				if (doc instanceof InOfficeDocument)
				{
					pw.append("Usuniento z dziennik�w : \n");
					journals = Journal.findByDocumentId(documentId);

					if (((InOfficeDocument) doc).getJournal() != null)
					{
						for (Journal j : journals)
						{
							PreparedStatement ps = DSApi.context().prepareStatement(deleteFromJournalEntry);
							ps.setLong(1, documentId);
							ps.setLong(2, j.getId());
							ps.execute();
							ps.close();
							pw.append("Dziennik: "+j.getDescription() +" \n");
						}


					}
				} else if (doc instanceof OutOfficeDocument)
				{
					pw.append("Usuniento z dziennik�w :\n");
					journals = Journal.findByDocumentId(documentId);

					if (((OutOfficeDocument) doc).getJournal() != null)
					{
						for (Journal j : journals)
						{
							PreparedStatement ps = DSApi.context().prepareStatement(deleteFromJournalEntry);
							ps.setLong(1, documentId);
							ps.setLong(2, j.getId());
							ps.execute();
							ps.close();
							pw.append("Dziennik: "+j.getDescription() +" z roku :"+j.getCyear() +"Opis dziennika :" +j.getSummary()+" \n");
						}
					}
				}
				for (int key : listSQls.keySet())
				{
					PreparedStatement ps = DSApi.context().prepareStatement(listSQls.get(key));
					ps.setLong(1, documentId);
					ps.execute();
					ps.close();
					
				}
				pw.append("Usunieto Historie dokumentu i historie dekretacji:" +"\n");
				pw.close();
				ctx.commit();

			} catch (EdmException e)
			{
				if (ctx != null)
					DSApi.context().setRollbackOnly();
				event.getErrors().add(e.getMessage());
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				log.error("", e);
			} catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				log.error("", e);
			} finally
			{
				DSApi._close();
			}
		}
		private void fillDeletedDocumentInfo(PrintWriter pw, Document doc)
		{
			pw.append("Data usuniecia dokumentu  : "+new Date().toLocaleString()+"\n");
			pw.append("ID Dokumentu : " +doc.getId()+"\n");
			pw.append("Opis dokumentu : " +doc.getDescription()+"\n");
			pw.append("Autor dokumentu : " +doc.getAuthor()+"\n");
			pw.append("Data utworzenia dokumentu  : " +doc.getCtime()+"\n");
			if(doc instanceof OfficeDocument)
			{
				pw.append("Numer KO  : " +((OfficeDocument)doc).getOfficeNumber()+"\n");
				pw.append("Kod kreskowy  : " +((OfficeDocument)doc).getBarcode()+"\n");
				pw.append("Dokument w sprawie  : " +((OfficeDocument)doc).getCaseDocumentId()+"\n");
				
				pw.append("Historia Pisma :\n");
				for ( Audit aud:	((OfficeDocument)doc).getWorkHistory()){
				pw.append("Data wykonania :"+aud.getCtime()+", wykonujacy : "+aud.getUsername() +", czynnos� :" +aud.getDescription()+"\n");
				}
				pw.append("Historia Dekretacji :\n");
				for ( AssignmentHistoryEntry ase:	((OfficeDocument)doc).getAssignmentHistory()){
					pw.append("Data wykonania :"+ase.getCtime()+", wykonujacy : "+ase.getSourceUser()  +", cel :" +ase.getObjective()+"\n");
				}
				
				
			}	
			
			
		}
		private String prepareFileToDeleteDocumentLog()
		
		{ 
			StringBuilder	path = new StringBuilder();
			 path.append(Docusafe.getHome().getAbsolutePath()+"/UsunieteDokumenty/");
			 new File(path.toString()).mkdirs();
			 path.append("usuni�cieDokumentu"+ new DateUtils().formatCommonDateTimeWithoutWhiteSpaces(new Date()));
			return path.toString();
			
			
			
		}
	}

    /**
     * Zwraca odno�nik do strony przedstawiaj�cej zawarto�� folderu
     * o przekazanym ID. ID mo�e by� r�wne null.
     * @deprecated
     */
    public static String getLink(HttpServletRequest request, Long folderId)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (folderId != null ? "?folderId="+folderId : "");
    }
    
    
    public static String getLink(HttpServletRequest request, Long folderId, boolean popup,String param)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (folderId != null ? "?folderId="+folderId : "?folderId=")+
            (param != null ? "&param="+param : "")+
            (popup ? "&popupwin=true" : "");
    }

    public static String getLink()
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }

    public static String getLink(Long folderId)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (folderId != null ? "?folderId="+folderId : "");            
    }
    
    public static String getLink(Long folderId, boolean popup)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            (folderId != null ? "?folderId="+folderId : "?folderId=")+
            (popup ? "&popupwin=true" : "");
    }

    public static String getLink(HttpServletRequest request, Long folderId,
                                 String sortMode, boolean ascending,boolean popup)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            "?sortMode="+sortMode +
            "&ascending="+ascending +
            (folderId != null ? "&folderId="+folderId : "")+
            (popup ? "&popupwin=true" : "");
    }

    public static String getLink(HttpServletRequest request, Long folderId,
            String sortMode, boolean ascending, String param)
	{
			ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
			ForwardConfig fc = config.findForwardConfig(FORWARD);
			return fc.getPath()+
			"?sortMode="+sortMode +
			"&ascending="+ascending +
			(folderId != null ? "&folderId="+folderId : "");			
	}
    
    private static String getExploreLink(int offset, int limit, Long folderId,
                                  String sortMode, boolean ascending, boolean popup)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+
            "?sortMode="+sortMode +
            "&ascending="+ascending +
            (folderId != null ? "&folderId="+folderId : "") +
            (offset > 0 ? "&offset="+offset : "") +
            (limit > 0 ? "&limit="+limit : "")+
            (popup ? "&popupwin=true" : "");
    }

    private static String getExploreLink(int offset, int limit, Long folderId,
            String sortMode, boolean ascending)
	{
		ModuleConfig config = (ModuleConfig)
		Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
		ForwardConfig fc = config.findForwardConfig(FORWARD);
		return fc.getPath()+
		"?sortMode="+sortMode +
		"&ascending="+ascending +
		(folderId != null ? "&folderId="+folderId : "") +
		(offset > 0 ? "&offset="+offset : "") +
		(limit > 0 ? "&limit="+limit : "");
	}
    
    
    public static void setReqId(Long id)
    {
    	idReq = id;
    }
    public static Long getReqId()
    {
    	return idReq;
    }
}


