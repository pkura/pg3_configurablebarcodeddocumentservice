package pl.compan.docusafe.web.archive.repository;

import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;
import java.util.List;

public class AssignmentHistoryAction extends AssignmentHistoryTabAction
{
    private Long id;
    private Long binderId;

    protected List prepareTabs()
    {
        this.id = getDocumentId();
        return null;
    }

    public String getDocumentType()
    {
        return null;
    }

    public void setId(Long id) {
        this.id = id;
        setDocumentId(this.id);
    }

    public Long getId() {
        return id;
    }

    public void setBinderId(Long binderId)
    {
        this.binderId = binderId;
    }

    public Long getBinderId()
    {
        return binderId;
    }
}
