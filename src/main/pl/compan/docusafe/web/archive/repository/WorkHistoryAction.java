package pl.compan.docusafe.web.archive.repository;

import java.util.List;

import pl.compan.docusafe.web.office.common.WorkHistoryTabAction;

public class WorkHistoryAction extends WorkHistoryTabAction
{
	private Long id;
	private Long binderId;
	
    protected List prepareTabs()
    {
    	this.id = getDocumentId();
        return null;
    }

    public String getBaseLink()
    {
        return "";
    }

    public String getDocumentType()
    {
        return null;
    }

	public void setId(Long id) {
		this.id = id;
                setDocumentId(this.id);
	}

	public Long getId() {
		return id;
	}

	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}

	public Long getBinderId()
	{
		return binderId;
	}
}
