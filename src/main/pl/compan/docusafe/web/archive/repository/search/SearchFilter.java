package pl.compan.docusafe.web.archive.repository.search;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

@Entity
@Table(name = "utp_search_filter")
public class SearchFilter {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "document_id", nullable = true)
	private Long document_id;

	@Column(name = "title", nullable = true)
	private String title;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "attribute", nullable = true)
	private String attribute;

	@Column(name = "author", nullable = true)
	private String author;

	@Column(name = "attachmentBarcode", nullable = true)
	private String attachmentBarcode;

	@Column(name = "startCtime", nullable = true)
	private Date startCtime;

	@Column(name = "endCtime", nullable = true)
	private Date endCtime;

	@Column(name = "startMtime", nullable = true)
	private Date startMtime;

	@Column(name = "endMtime", nullable = true)
	private Date endMtime;

	@Column(name = "lastRemark", nullable = true)
	private String lastRemark;

	@Column(name = "accessedBy", nullable = true)
	private String accessedBy;
	
	@Column(name = "filterName", nullable = false)
	private String filterName;
	
	@Column(name = "documentKindIds", nullable = true)
	private String documentKindIds;
	
	public SearchFilter() {
	}

	public static List<SearchFilter> list() throws EdmException {

		return DSApi.context().session().createCriteria(SearchFilter.class).addOrder(Order.asc("id")).list();
	}
	
	public static List<SearchFilter> findByCn(String _cn) throws EdmException
	{
		return DSApi.context().session().createCriteria(SearchFilter.class).add(Restrictions.eq("filterName", _cn)).list();
	}
	
	public static SearchFilter find(Long id) throws EdmException {
		
		return Finder.find(SearchFilter.class, id);
	}

	public boolean save() throws EdmException {

		try {
			DSApi.context().session().saveOrUpdate(this);
			return true;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public boolean delete() throws EdmException {
		
		try {
			DSApi.context().session().delete(this);
			return true;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long document_id) {
		this.document_id = document_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAttachmentBarcode() {
		return attachmentBarcode;
	}

	public void setAttachmentBarcode(String attachmentBarcode) {
		this.attachmentBarcode = attachmentBarcode;
	}

	public Date getStartCtime() {
		return startCtime;
	}

	public void setStartCtime(Date startCtime) {
		this.startCtime = startCtime;
	}

	public Date getEndCtime() {
		return endCtime;
	}

	public void setEndCtime(Date endCtime) {
		this.endCtime = endCtime;
	}

	public Date getStartMtime() {
		return startMtime;
	}

	public void setStartMtime(Date startMtime) {
		this.startMtime = startMtime;
	}

	public Date getEndMtime() {
		return endMtime;
	}

	public void setEndMtime(Date endMtime) {
		this.endMtime = endMtime;
	}

	public String getLastRemark() {
		return lastRemark;
	}

	public void setLastRemark(String lastRemark) {
		this.lastRemark = lastRemark;
	}

	public String getAccessedBy() {
		return accessedBy;
	}

	public void setAccessedBy(String accessedBy) {
		this.accessedBy = accessedBy;
	}
	
	public String getFilterName() {
		return filterName;
	}
	
	public void SetFilterName(String filterName) {
		this.filterName=filterName;
	}

	public String getDocumentKindIds() {
		return documentKindIds;
	}

	public void setDocumentKindIds(String documentKindIds) {
		this.documentKindIds = documentKindIds;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

}
