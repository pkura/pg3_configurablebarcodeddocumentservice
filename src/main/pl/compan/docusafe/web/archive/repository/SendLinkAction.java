package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SendLinkAction.java,v 1.16 2009/06/15 12:49:23 mariuszk Exp $
 */
public final class SendLinkAction extends EventProcessingAction
{
        
	private static Logger log = LoggerFactory.getLogger(SendLinkAction.class);
    private static final String FORWARD = "repository/send-link";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doSendLink").
            append(setForward).
            append(new SendLink()).
            append("fillForm", fillForm);
    }

    private static class SendLink implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            StringManager sm =GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
            DynaActionForm form = event.getDynaForm();

            Long id = (Long) form.get("documentId");
            String email = (String) form.get("email");
            String username = (String) form.get("username");

            if (id == null)
                return;
            
            if ((StringUtils.isEmpty(email) && StringUtils.isEmpty(username)))
            {
                event.getErrors().add(sm.getString("NalezyWybracZlistyUzytkownikaSystemowegoLubWpisacAdresEmail"));
                return;
            }

            Map<String, Object> context = new HashMap<String, Object>();
            context.put("documentUrl", Configuration.getBaseUrl() +
                EditDocumentAction.getLink(id));
            context.put("documentId", id);

            String mailto;
            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                Document document = Document.find(id);

                context.put("documentTitle", document.getTitle());
                context.put("sender", DSApi.context().getDSUser().asFirstnameLastname());

                if (!StringUtils.isEmpty(username))
                {
                    DSUser user = DSUser.findByUsername(username);
                    if (StringUtils.isEmpty(user.getEmail()))
                        throw new EdmException(sm.getString("UzytkownikNieMaAdresuEmail",username));
                    mailto = user.getEmail();
                }
                else
                {
                    mailto = email;
                }
            
                ((Mailer) ServiceManager.getService(Mailer.NAME)).
                    send(mailto, null, null, Configuration.getMail(Mail.SEND_DOCUMENT_LINK), context);

                event.getMessages().add(sm.getString("editDocument.linkSentTo", email));
                event.getRequest().setAttribute("close", Boolean.TRUE);
            }
            catch (Exception e)
            {
            	log.error("",e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DSUser[] users = (DSUser[]) DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME).toArray(new DSUser[0]);

                List userBeans = new ArrayList(users.length);

                for (int i=0, n=users.length; i < n; i++)
                {
                    if (StringUtils.isEmpty(users[i].getEmail()))
                        continue;

                    DynaBean bean = DynaBeans.newHtmlOption();
                    bean.set("label", users[i].asLastnameFirstname());
                    bean.set("value", users[i].getName());

                    userBeans.add(bean);
                }

                event.getRequest().setAttribute("users", userBeans);
            }
            catch (EdmException e)
            {
            	log.error("",e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(Long documentId)
    {
        final ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() + "?documentId="+documentId;
    }
}
