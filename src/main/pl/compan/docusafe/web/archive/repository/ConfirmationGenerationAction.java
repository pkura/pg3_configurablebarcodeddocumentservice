package pl.compan.docusafe.web.archive.repository;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ConfirmationGenerationAction extends EventActionSupport
{

	private final static Logger log = LoggerFactory.getLogger(ConfirmationGenerationAction.class);

	private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private Long documentId;
	private String link;
	private Document document;
	private DocumentKind documentKind;
	private String documentKindCn;
	private FieldsManager fm;
	private boolean generated = false;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddConfirmation").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Pdf()).
        	appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	
	private class FillForm implements ActionListener
    {
        public FillForm()
        {
        }

        public void actionPerformed(ActionEvent event)
        {
        	
        }
    }
	
	private class Pdf implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
        	try{
        		document = Document.find(documentId);
        		if (documentKindCn == null)
                {
                    documentKind = document.getDocumentKind() != null ? document.getDocumentKind() :
                        DocumentKind.findByCn(DocumentKind.getDefaultKind());
                    documentKindCn = documentKind.getCn();
                }
                else
                    documentKind = DocumentKind.findByCn(documentKindCn);
        		if(document.getAttachments()==null) throw new EdmException("Brak załącznika");
        		fm = documentKind.getFieldsManager(document.getId());
                fm.initialize();
                fm.initializeAcceptances();
                
                String dataWplywu = DateUtils.formatJsDate((Date)fm.getValue(InvoiceLogic.DATA_WPLYWU_CN));
                String dataFaktury = DateUtils.formatJsDate((Date)fm.getValue(InvoiceLogic.DATA_WYSTAWIENIA_FIELD_CN));
                String numer = fm.getValue(InvoiceLogic.NUMER_FAKTURY_FIELD_CN).toString();
                
                if(document.getAttachments().size()==0)
                {
                	addActionError(sm.getString("DokumentNieMaZalacznikow"));
                	generated = false;
                	return;
                }
        		AttachmentRevision arr = document.getAttachments().get(0).getRevisions().iterator().next();
        		boolean isImage = true;
        		File img = arr.saveToTempFile();
        		if(!img.getName().endsWith("tiff")
        				&&!img.getName().endsWith("tif")
        				&&!img.getName().endsWith("jpg")
        				&&!img.getName().endsWith("gif")
        				&&!img.getName().endsWith("png"))
        			isImage = false;
        		
	        	File fontDir = new File(Configuration.getHome(), "fonts");
	            File arial = new File(fontDir, "arial.ttf");
	            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
	                BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
	
	            Font font = new Font(baseFont, 10);
	           
	           
	        	 File temp = File.createTempFile("docusafe_", ".pdf");
	             com.lowagie.text.Document pdfDoc =
	                 new com.lowagie.text.Document(PageSize.A4,30,60,30,30);
	             PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
	             pdfDoc.setPageCount(1);
	             
	             HeaderFooter footer =
	            	 new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzez",DSApi.context().getPrincipalName(),
	                         DateUtils.formatCommonDateTime(new Date())), font), false);
	             footer.setPageNumber(1);
	             footer.setAlignment(Element.ALIGN_CENTER);
	             pdfDoc.setFooter(footer);

	             pdfDoc.open();
	             Paragraph p;
	             p = new Paragraph(new Phrase(sm.getString("PotwierdzenieWplynieciaFakturyNumer", numer),new Font(baseFont, 12,Font.BOLD)));
	             p.setAlignment(Element.ALIGN_CENTER);
	             pdfDoc.add(p);
	             
	             p = new Paragraph(new Phrase(sm.getString("ZDataWystwienia",dataFaktury),new Font(baseFont, 12,Font.BOLD)));
	             p.setAlignment(Element.ALIGN_CENTER);
	             pdfDoc.add(p);
	             
	             if(isImage){
		             Image image = Image.getInstance(img.getAbsolutePath());
		             image.scaleToFit(500, 600);
		             pdfDoc.add(image);
	             }
	             else
	             {
	            	 p = new Paragraph(new Phrase(sm.getString("ZalacznikNieJestObrazem"),new Font(baseFont, 12, Font.BOLD, Color.RED)));
	            	 addActionError(sm.getString("ZalacznikNieJestObrazem"));
	            	 pdfDoc.add(p);
	             }
	             p = new Paragraph(new Phrase(sm.getString("FakturaWplynelaDnia", dataWplywu),font));
	             pdfDoc.add(p);
	             
	             p = new Paragraph(new Phrase(".......................",font));
	             p.setAlignment(Element.ALIGN_RIGHT);
	             pdfDoc.add(p);
	             p = new Paragraph(new Phrase("Podpis         ",new Font(baseFont, 8, Font.ITALIC)));
	             p.setAlignment(Element.ALIGN_RIGHT);
	             pdfDoc.add(p);
	             pdfDoc.close();
                 
	             if(!DSApi.context().isTransactionOpen())
	            	 DSApi.context().begin();
                 Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(sm.getString("Potwierdzenie"), 254));
                 document.createAttachment(attachment);
                 if(document instanceof InOfficeDocument){
                 	((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
                 }
                 
                 
                 AttachmentRevision ar = attachment.createRevision(temp);
                 ar.setOriginalFilename(sm.getString("Potwierdzenie"));
                 ar.getId();
                 DSApi.context().commit();
                 generated = true;
                 addActionMessage(sm.getString("PomyslnieUtworzonoPotwierdzenieWplyniecia"));
                 link = "/viewserver/viewer.action?id=" + ar.getId();
	             
                
	             
        	}
        	catch(FileNotFoundException e){
        		addActionError(sm.getString("NieZnalezionoPliku"));
        	}
        	catch(EdmException e)
            {
				 log.error(e.getMessage(), e);
	           	 DSApi.context().setRollbackOnly();
	           	 generated = false;
	           	 addActionError(sm.getString("NieUdaloSieWygenerowacPotwierdzenia"));
            }
        	catch(Exception e)
        	{
				log.error(e.getMessage(), e);
        		addActionError(e.getMessage());
        	}
        }
    }

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document){ 
		this.document = document;
	}

	public DocumentKind getDocumentKind() {
		return documentKind;
	}

	public void setDocumentKind(DocumentKind documentKind) {
		this.documentKind = documentKind;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public boolean isGenerated() {
		return generated;
	}

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}


}
