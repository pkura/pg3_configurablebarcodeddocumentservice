package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.parametrization.ilpoldwr.LeasingLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Akcja "Nowa teczka " dodaje nowe dokumenty leasingowe z podzielonego
 * multitiffa
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class NewPortfoliosAction extends EventActionSupport
{

	private static final Logger log = LoggerFactory.getLogger(NewPortfoliosAction.class);
	private static final String URL = "/repository/new-portfolios.action";
	private static final int LIMIT = 20;
	private StringManager sm = GlobalPreferences.loadPropertiesFile(NewPortfoliosAction.class.getPackage().getName(), null);

	// @IMPORT/EXPORT
	private Long id;
	private Long dostawcaId;
	private Long binderId;
	private String nazwa;
	private String city; 
	private String street;
	private String phoneNumber;
	private String fax;
	private String email;
	private static Contractor tmpContractor = new Contractor();

	private Boolean canDelete;
	private Boolean canAdd;
	private Boolean canRead;
	private Boolean canEdit;
	private Integer showPage;

	private Pager pager;
	private int offset;
	private String sortDesc;
	private String sortAsc;
	private Boolean ascending;
	private String sortField;
	private List<? extends Contractor> results;
	private String sesId;
	private static String EV_FILL = "fill";

	private String dictionaryContractAction;
	private String dictionaryApplicationAction;
	private String customerName;
	private Long applicationId;
	private Long contractId;
	private Integer contractorKind;
	private List<EnumItem> contractorkinds;
	private List<Long> docsId;

	private Contractor contractor;
	private String applicationName;
	private String contractName;
	private List<FormFile> multiFiles = new ArrayList<FormFile>();
	private Map<String, String> files = new TreeMap<String, String>();
	private List<EnumItem> types;
	public ArrayList<Long> contractIds;
	private Map<Long,String> contractsIdsMap ;
	private String[] idUmowy;
	private String[] idUmowyPrzekazane;
	private String idUmowyPrzekazane2;



	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").append(OpenHibernateSession.INSTANCE).append(new Clean()).append(new Search()).append(EV_FILL, fillForm).append(
				CloseHibernateSession.INSTANCE);

		registerListener("doNew").append(OpenHibernateSession.INSTANCE).append(new New()).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSave").append(OpenHibernateSession.INSTANCE).append(new Save()).appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
				canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
				canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
				canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
			}
			try
			{
				dictionaryApplicationAction = DlApplicationDictionary.getInstance().dictionaryAction();
				dictionaryContractAction = DlContractDictionary.getInstance().dictionaryAction();

				if (canRead)
				{
					QueryForm form = new QueryForm(offset, LIMIT);
					form.addOrderAsc("name");
					SearchResults<Contractor> results = Contractor.search(form);

					if (results == null || results.totalCount() == 0)
					{

					}
					else
					{
						NewPortfoliosAction.this.results = fun.list(results);
						Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
						{
							public String getLink(int offset)
							{
								return HttpUtils.makeUrl(URL, new Object[] { "doSearch", "true", "nazwa", nazwa, "city", city, "street", street,
										"phoneNumber", phoneNumber, "fax", fax, "email", email, "offset", String.valueOf(offset) });
							}
						};
						pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
						sortAsc = linkVisitor.getLink(offset) + "&ascending=true";
						sortDesc = linkVisitor.getLink(offset) + "&ascending=false";
					}
				}
				else
					throw new EdmException(sm.getString("BrakUprawnienDoOgladaniaKontrahentow"));
			}
			catch (EdmException e)
			{
				addActionError(e.getMessage());
			}
		}
	}

	private class New implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			idUmowyPrzekazane = idUmowy;
			showPage = 2;
			results = null;
			dictionaryApplicationAction = DlApplicationDictionary.getInstance().dictionaryAction();
			dictionaryContractAction = DlContractDictionary.getInstance().dictionaryAction();

			try
			{
				if (id == null && dostawcaId == null)
					throw new EdmException("Nie wybrano kontrahenta");
				contractor = tmpContractor.find(id);
				DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
				setContractorkinds(kind.getFieldByCn("RODZAJ_KONTRAHENTA").getEnumItems());
				if (applicationId != null)
				{
					applicationName = DlApplicationDictionary.getInstance().find(applicationId).getDictionaryDescription();
					contractIds = DlApplicationDictionary.getInstance().getContracts(applicationId);
					if(contractIds != null)
                	{
						contractsIdsMap = new HashMap<Long,String>();
						for(Long cid : contractIds)
	                    {
	                    	if(cid > 0)
	                    	{
		                    	DlContractDictionary dlc = DlContractDictionary.getInstance().find(cid);
		                    	contractsIdsMap.put(cid, dlc.getNumerUmowy());
	                    	}
	                    }
                	}
				}
				else if (contractId != null)
				{
					contractName = DlContractDictionary.getInstance().find(contractId).getDictionaryDescription();
				}

				customerName = contractor.getName();
				if (multiFiles != null && multiFiles.size() > 0)
				{
					files = ImageKit.splitAttachments(multiFiles, Docusafe.getPngTempFile());
					types = ((LeasingLogic) kind.logic()).getEnumItms(kind);
					Collections.sort(types, new EnumItem.EnumItemComparator());
					showPage = 3;
				}
			}
			catch (EdmException e)
			{
				log.error("", e);
				event.addActionError(e.getMessage());
			}
			catch (Exception e)
			{
				log.error("", e);
				event.addActionError("Bł±d ładowania plików");
			}
		}
	}

	private class Save implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				log.debug("Start zapisu nowych dokument�w");
				docsId = new ArrayList<Long>();
				HttpServletRequest r = ServletActionContext.getRequest();
				if (log.isTraceEnabled())
				{
					Set<String> set = r.getParameterMap().keySet();
					for (String paramName : set)
					{
						log.trace("Param name " + paramName + " value " + r.getParameter(paramName));
					}
					log.trace("OSTATNI PARAMETR");
				}
				List<Long> idUmowoLong = new ArrayList<Long>();
				if(idUmowyPrzekazane2 != null && idUmowyPrzekazane2.length() > 0)
				{
					for (String id : idUmowyPrzekazane2.split(","))
					{
							idUmowoLong.add(Long.valueOf(id.trim()));
						
					}
				}
				DocumentKind kind;
				kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
				Field f = kind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN);
				types = f.getEnumItems();
				Object obj;
				for (Iterator<EnumItem> iterator2 = types.iterator(); iterator2.hasNext();)
				{
					log.debug("Iteruje po rodzajach");
					EnumItem enumItm = (EnumItem) iterator2.next();
					String tmp = r.getParameter(enumItm.getCn());

					if (tmp == null || tmp.length() < 2)
					{
						log.debug("NIe ma dokumentu o rodzaju "+enumItm.getCn());
						continue;
					}
					log.debug("file_" + enumItm.getCn() + " " + tmp);
					Integer discriminator = Integer.parseInt(enumItm.getRefValue());
					if (discriminator == null)
					{
						continue;
					}
					String[] pages = tmp.split(",");
					log.debug("Tworzy dokument");
					obj = createDoc(enumItm.getId(), discriminator, idUmowoLong, applicationId, null, null);
					log.debug("Uruchamia zadanie dodania za��cznika");
					ImageKit.addAttachments(pages, obj,DlLogic.getImportKind(enumItm.getId()));
					if (obj instanceof Document)
					{
						addActionMessage("Dodano document " + ((Document) obj).getTitle());
						docsId.add(((Document) obj).getId());
						log.debug("Dodał dokument " + ((Document) obj).getTitle());
					}
					else
					{
						addActionMessage("Dodano now± wersje zał±cznika " + ((Attachment) obj).getId() +" oraz zmieniono status dokumentu");
						docsId.add(((Attachment) obj).getDocument().getId());
						log.debug("Dodal wersje zalacznika " + ((Attachment) obj).getId());
					}
				}
				log.debug("Wywo�anie handler�w");
				EventFactory.startEventByHandler("AddAttachmentsToDocument");
				EventFactory.startEventByHandler("AddRevisionToAttachment");
 
				addActionMessage("Dodano dokumenty");
				showPage = 4;
			}
			catch (Exception e)
			{
				addActionError("Nie dodano dokumentów : " + e.getMessage());
				DSApi.context().setRollbackOnly();
				log.error("", e);

			}
			finally
			{
				try
				{
					DSApi.close();
				}
				catch (EdmException e)
				{
					log.error("", e);
				}
			}
		}
	}

	private boolean rodzaje(String cn, DocumentKind kind,List<Long> idUmowoLong) throws Exception
	{
		HttpServletRequest r = ServletActionContext.getRequest();
		Object obj = null;
		Field f = kind.getFieldByCn(cn);
		types = f.getEnumItems();
		for (Iterator<EnumItem> iterator2 = types.iterator(); iterator2.hasNext();)
		{
			EnumItem enumItm = (EnumItem) iterator2.next();
			String tmp = r.getParameter(enumItm.getCn());

			if (tmp == null || tmp.length() < 2)
			{
				continue;
			}
			log.debug("Rodzaje cn " + cn + " file_" + enumItm.getCn() + " " + tmp);
			Integer availableWhen = Integer.parseInt(f.getAvailableWhen().get(0).getFieldValue().toString());
			Field f2 = kind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN);
			Integer discriminator = (Integer) ((EnumRefField) f2).getEnumItemDiscriminator(availableWhen);
			if (discriminator == null)
			{
				continue;
			}
			String[] pages = tmp.split(",");
			try
			{
				obj = createDoc(availableWhen, discriminator, idUmowoLong, applicationId, enumItm.getId(), cn);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				continue;
			}			
			ImageKit.addAttachments(pages, obj,DlLogic.getImportKind(availableWhen));
			if (obj instanceof Document)
			{
				addActionMessage("Dodano document " + ((Document) obj).getTitle());
				log.debug("Dodał dokument " + ((Document) obj).getTitle());
			}
			else
			{
				addActionMessage("Dodano now± wersje zał±cznika "+ ((Attachment) obj).getId());
				docsId.add(((Attachment) obj).getDocument().getId());
				log.debug("Dodal wersje zalacznika " + ((Attachment) obj).getId());
			}
		}
		return true;
	}

	public Object createDoc(Integer type, Integer parentField, List<Long> contracts, Long application, Integer rodzaj, String cn) throws Exception
	{
		// sprawdzenie czy juz taki istnieje
		Map<String, Object> values = new HashMap<String, Object>();
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
		DockindQuery dockindQuery = new DockindQuery(0, 100);
		dockindQuery.setCheckPermissions(false);
		dockindQuery.setDocumentKind(documentKind); 
		dockindQuery.enumField(documentKind.getFieldByCn("RODZAJ_KONTRAHENTA"), contractorKind);
		dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_CN), parentField);
		dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN), type);
		if (applicationId != null && applicationId > 0)
		{
			dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_WNIOSKU_CN), applicationId);
		}
		if (contracts != null && contracts.size() > 0)
		{
			dockindQuery.InField(documentKind.getFieldByCn(DlLogic.NUMER_UMOWY_CN), contracts.toArray());
		}
		if (cn != null && rodzaj != null)
		{
			dockindQuery.enumField(documentKind.getFieldByCn(cn), rodzaj);
		}
		dockindQuery.setCheckPermissions(false);
		log.debug("Szukam czy dokument ju� istnieje");
		SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
		Document existDoc = null;
		
		if (searchResults.count() > 0)
		{
			log.debug("Znalaz� taki dokument ");
			for (Document candidateDocument : searchResults.results())
			{
				List<Long> contractsIds = (List<Long>) candidateDocument.getDocumentKind().getFieldsManager(candidateDocument.getId()).getKey(DlLogic.NUMER_UMOWY_CN);
				if( contractsIds != null && contracts != null && contractsIds.size() == contracts.size())
				{
					contractsIds.removeAll(contracts);
					if(contractsIds.size() < 1)
					{
						existDoc = candidateDocument;
					}
					else
					{
						Map<String, Object> valuesCanDoc = new HashMap<String, Object>();
						valuesCanDoc.put(DlLogic.NUMER_UMOWY_CN, contractsIds);
						candidateDocument.getDocumentKind().setWithHistory(candidateDocument.getId(), valuesCanDoc,false);
					}
				}
				else if(contractsIds != null)
				{
					contractsIds.removeAll(contracts);
					Map<String, Object> valuesCanDoc = new HashMap<String, Object>();
					valuesCanDoc.put(DlLogic.NUMER_UMOWY_CN, contractsIds);
					candidateDocument.getDocumentKind().setWithHistory(candidateDocument.getId(), valuesCanDoc,false);
				}
				else if((contracts == null || contracts.size() < 1 ))
				{
					System.out.println("BEZ UMOWY");
					existDoc = candidateDocument;
				}
			}
		}
		if (existDoc != null)
		{
			values.put(DlLogic.STATUS_DOKUMENTU_CN, 5);
			//Document doc = searchResults.results()[0];
			if(!existDoc.canModifyAttachments(binderId))
    			throw new EdmException("Nie masz uprawnień do dodania załącznika");
			DSApi.context().begin();
			existDoc.getDocumentKind().setWithHistory(existDoc.getId(), values, false,Label.SYSTEM_LABEL_OWNER);
			DSApi.context().commit();
			List<Attachment> attas = existDoc.getAttachments();
			if (attas != null && attas.size() > 0)
			{
				return attas.get(attas.size() - 1);
			}
			else
			{
				return existDoc;
			}
		}
		log.trace("Integer type, Integer parentField, Long contract, Long application, Integer rodzaj, String cn "+ type+" "+parentField+" "+" "+application+" "+rodzaj+" "+cn);
		DSApi.context().begin();
		String summary = "Dodany przez Nowa Teczka";
		Document document = null;
		document = new Document(summary, summary);
		document.setDocumentKind(documentKind);
		document.setForceArchivePermissions(false);
		document.setCtime(new Date());
		document.setFolder(Folder.getRootFolder());
		document.create();
		Long newDocumentId = document.getId();
		values.put(DlLogic.RODZAJ_DOKUMENTU_CN, parentField);
		values.put(DlLogic.TYP_DOKUMENTU_CN, type);
		if (cn != null && rodzaj != null)
			values.put(cn, rodzaj);
		if(contractorKind != null && dostawcaId != null && contractorKind.equals(20))
		{
			values.put(DlLogic.KLIENT_CN, dostawcaId);
		}
		else
		{
			values.put(DlLogic.KLIENT_CN, id);
		}
		
		if (application != null)
			values.put(DlLogic.NUMER_WNIOSKU_CN, application);
		if (contracts != null)
		{

			values.put(DlLogic.NUMER_UMOWY_CN, contracts);
		}
		
		values.put("RODZAJ_KONTRAHENTA", contractorKind);
		values.put(DlLogic.STATUS_DOKUMENTU_CN, 5);
		documentKind.setWithHistory(newDocumentId, values,false);
		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
		documentKind.logic().documentPermissions(document);
		DSApi.context().commit();
		log.trace("Dodaje dokument " + document.getId());
		return document;
	}

	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
				canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
				canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
				canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
			}
			try
			{
				QueryForm form = new QueryForm(offset, LIMIT);

				if (nazwa != null)
					form.addProperty("name", nazwa);
				if (city != null)
					form.addProperty("city", city);
				if (street != null)
					form.addProperty("street", street);

				if (ascending != null)
					if (ascending)
						form.addOrderAsc(sortField);
					else
						form.addOrderDesc(sortField);
				else
					form.addOrderAsc("name");

				SearchResults<? extends Contractor> results = Contractor.search(form);

				if (results == null || results.totalCount() == 0)
				{
					results = null;
					showPage = 1;
					throw new EdmException(sm.getString("NieZnalezionoKontrahentowPasujacychDoWpisanychDanych"));
				}
				event.skip(EV_FILL);
				NewPortfoliosAction.this.results = fun.list(results);
				Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
				{
					public String getLink(int offset)
					{
						return HttpUtils.makeUrl(URL, new Object[] { "doSearch", "true", "nazwa", nazwa, "city", city, "street", street,
								"phoneNumber", phoneNumber, "fax", fax, "email", email, "offset", String.valueOf(offset) });
					}
				};
				pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);

				sortAsc = linkVisitor.getLink(offset) + "&ascending=true";
				sortDesc = linkVisitor.getLink(offset) + "&ascending=false";
			}
			catch (EdmException e)
			{
				addActionError(e.getMessage());
			}
		}
	}

	private class Clean implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			nazwa = TextUtils.trimmedStringOrNull(nazwa);
			city = TextUtils.trimmedStringOrNull(city);
			street = TextUtils.trimmedStringOrNull(street);
		}
	}

	public Boolean getCanAdd()
	{
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd)
	{
		this.canAdd = canAdd;
	}

	public Boolean getCanDelete()
	{
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete)
	{
		this.canDelete = canDelete;
	}

	public Boolean getCanRead()
	{
		return canRead;
	}

	public void setCanRead(Boolean canRead)
	{
		this.canRead = canRead;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getNazwa()
	{
		return nazwa;
	}

	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String nip)
	{
		this.city = nip;
	}

	public int getOffset()
	{
		return offset;
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public Pager getPager()
	{
		return pager;
	}

	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String regon)
	{
		this.street = regon;
	}

	public List<? extends Contractor> getResults()
	{
		return results;
	}

	public void setResults(List<? extends Contractor> results)
	{
		this.results = results;
	}

	public Boolean getCanEdit()
	{
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit)
	{
		this.canEdit = canEdit;
	}

	public Boolean getAscending()
	{
		return ascending;
	}

	public void setAscending(Boolean ascending)
	{
		this.ascending = ascending;
	}

	public String getSortAsc()
	{
		return sortAsc;
	}

	public void setSortAsc(String sortAsc)
	{
		this.sortAsc = sortAsc;
	}

	public String getSortDesc()
	{
		return sortDesc;
	}

	public void setSortDesc(String sortDesc)
	{
		this.sortDesc = sortDesc;
	}

	public String getSortField()
	{
		return sortField;
	}

	public void setSortField(String sortField)
	{
		this.sortField = sortField;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public Contractor getContractor()
	{
		return contractor;
	}

	public void setContractor(Contractor contractor)
	{
		this.contractor = contractor;
	}

	public FormFile getMultiFiles()
	{
		if (this.multiFiles != null && this.multiFiles.size() > 0)
			return this.multiFiles.get(0);
		else
			return null;
	}

	public void setMultiFiles(FormFile file)
	{
		this.multiFiles.add(file);
	}

	public Map<String, String> getFiles()
	{
		return files;
	}

	public void setFiles(Map<String, String> files)
	{
		this.files = files;
	}

	public List<EnumItem> getTypes()
	{
		return types;
	}

	public void setTypes(List<EnumItem> types)
	{
		this.types = types;
	}

	public String getSesId()
	{
		return sesId;
	}

	public void setSesId(String sesId)
	{
		this.sesId = sesId;
	}

	public Integer getShowPage()
	{
		return showPage;
	}

	public void setShowPage(Integer showPage)
	{
		this.showPage = showPage;
	}

	public String getDictionaryContractAction()
	{
		return dictionaryContractAction;
	}

	public void setDictionaryContractAction(String dictionaryContractAction)
	{
		this.dictionaryContractAction = dictionaryContractAction;
	}

	public String getDictionaryApplicationAction()
	{
		return dictionaryApplicationAction;
	}

	public void setDictionaryApplicationAction(String dictionaryApplicationAction)
	{
		this.dictionaryApplicationAction = dictionaryApplicationAction;
	}

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public Long getApplicationId()
	{
		return applicationId;
	}

	public void setApplicationId(Long applicationId)
	{
		this.applicationId = applicationId;
	}

	public Long getContractId()
	{
		return contractId;
	}

	public void setContractId(Long contractId)
	{
		this.contractId = contractId;
	}

	public String getApplicationName()
	{
		return applicationName;
	}

	public void setApplicationName(String applicationName)
	{
		this.applicationName = applicationName;
	}

	public String getContractName()
	{
		return contractName;
	}

	public void setContractName(String contractName)
	{
		this.contractName = contractName;
	}

	public void setContractorKind(Integer contractorKind)
	{
		this.contractorKind = contractorKind;
	}

	public Integer getContractorKind()
	{
		return contractorKind;
	}

	public void setContractorkinds(List<EnumItem> contractorkinds)
	{
		this.contractorkinds = contractorkinds;
	}

	public List<EnumItem> getContractorkinds()
	{
		return contractorkinds;
	}

	public void setDostawcaId(Long dostawcaId)
	{
		this.dostawcaId = dostawcaId;
	}

	public Long getDostawcaId()
	{
		return dostawcaId;
	}

	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}

	public Long getBinderId()
	{ 
		return binderId;
	}

	public List<Long> getDocsId() {
		return docsId;
	}

	public void setDocsId(List<Long> docsId) {
		this.docsId = docsId;
	}


	public Map<Long, String> getContractsIdsMap() {
		return contractsIdsMap;
	}

	public void setContractsIdsMap(Map<Long, String> contractsIdsMap) {
		this.contractsIdsMap = contractsIdsMap;
	}

	public String[] getIdUmowy() {
		return idUmowy;
	}

	public void setIdUmowy(String[] idUmowy) {
		this.idUmowy = idUmowy;
	}
	
	public ArrayList<Long> getContractIds() {
		return contractIds;
	}

	public void setContractIds(ArrayList<Long> contractIds) {
		this.contractIds = contractIds;
	}

	public String[] getIdUmowyPrzekazane() {
		return idUmowyPrzekazane;
	}

	public void setIdUmowyPrzekazane(String[] idUmowyPrzekazane) {
		this.idUmowyPrzekazane = idUmowyPrzekazane;
	}

	public String getIdUmowyPrzekazane2() {
		return idUmowyPrzekazane2;
	}

	public void setIdUmowyPrzekazane2(String idUmowyPrzekazane2) {
		this.idUmowyPrzekazane2 = idUmowyPrzekazane2;
	}

}
