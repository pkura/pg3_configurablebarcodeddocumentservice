package pl.compan.docusafe.web.archive.repository;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeHandler;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.java4less.vision.Barcode1DReader;
/**
 * 
 * @author Mariusz Kilja�czyk
 *
 */

public class AddOrderArchiveDocumentAction extends EventActionSupport
{    
	private StringManager sm = StringManager.getManager(AddOrderArchiveDocumentAction.class.getPackage().getName());
	private Logger log = LoggerFactory.getLogger(AddOrderArchiveDocumentAction.class);
	
    private List<InOfficeDocumentKind> kinds;
    private boolean canChooseKind;
    private boolean canReadDictionaries;
    private boolean canEdit;
    private DocumentKind documentKind = null;
    private Integer kindId;
    private String redirectUrl;
    private Long docId;
    private Long orderId;
    
    // @EXPORT
    private FieldsManager fm;
    private Map<String,String> documentKinds;

    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();

    // @EXPORT/@IMPORT
    protected String documentKindCn;
        
    protected void setup()
    {

        registerListener(DEFAULT_ACTION).
        	append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new CreateDocument()).
	    	append(new FillForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doArchive").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Archive()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE); 

       }
    private FormFile file;

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canReadDictionaries = true;
                canEdit = true;
                fillForm(DocumentLogic.TYPE_OUT_OFFICE); 
                // inicjuje rodzaj pisma przychodz�cego
                kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                canChooseKind = true;
                if (!canChooseKind)
                {    
                    // nie mo�na wybiera� na formatce - rodzaj pisma ustalony                    
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                            kindId = inKind.getId();
                    }
                }                                    
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    protected void fillForm(int documentType) throws EdmException
    {
        documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
        documentKindCn = documentKind.getCn();
        documentKind.initialize();
        fm = documentKind.getFieldsManager(docId);
        fm.initialize();
        documentKind.logic().setInitialValues(fm, documentType);

        if (hasActionErrors())
            fm.reloadValues(values);   
    }
    

    private class Archive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try        
            {                
                DSApi.context().begin();
                OfficeDocument document = OfficeDocument.findOfficeDocument(docId);                 
                DocumentKind documentKind = null;
                documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
                documentKind.initialize();             
                documentKind.logic().correctValues(values, documentKind);                
                boolean dockindChanged = !document.getDocumentKind().getCn().equals(documentKindCn);
                document.setDocumentKind(documentKind);
                if (dockindChanged)
                {
                	documentKind.setWithHistory(document.getId(), values, /*changeAll*/ true);
                }
                else
                {
                	documentKind.setWithHistory(document.getId(), values, /*changeAll*/ false);
                }
               /* if (document.getType() == DocumentType.INCOMING && !DocumentKind.NORMAL_KIND.equals(documentKind.getCn()))
                {
                    for (Iterator iter=InOfficeDocumentKind.list().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKind kind = (InOfficeDocumentKind) iter.next();
                        if (kind.getName().indexOf("iznesowy") > 0)
                        {
                            ((InOfficeDocument) document).setKind(kind);
                            break;
                        }
                    }
                }      */     
                /**
                 * @TODO - dlaczego tutaj musimy wolac te 3 metody na raz??? - nie lepiej zrobic jedna metode?
                 */
                documentKind.logic().archiveActions(document, DocumentKindsManager.getType(document));
                documentKind.logic().documentPermissions(document);
                TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                DSApi.context().commit();
                addActionMessage(sm.getString("ZarchiwizowanoDokument"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }
    
    private class CreateDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {           
            if (file == null )
            {
                addActionError("Nie podano pliku");
                return;
            }
            InOfficeDocument doc = new InOfficeDocument();
            try
            {
            	 
            	BarcodeHandler bh = new BarcodeHandler();
            	bh.setFileName(file.getName());
            	bh.setFilePath(file.getFile().getParent());
            	bh.setX(0);
            	bh.setY(0);
            	String bar = bh.scanImage(Barcode1DReader.INTERLEAVED25);
            	if(BarcodeHandler.DEFAULT_BARCODE.equals(bar))
            	{
            		addActionError(sm.getString("NieUdaloSieZnalezcBarkodu"));
            		return;
            	}
            	//przenioslem transakcje tutaj - po co ma byc otwarta przez czas szukania
            	//barcodow
            	DSApi.context().begin();
              	doc.setOriginal(false);
        	      
        	    Sender sender = new Sender();
        		sender.setAnonymous(true);
        		doc.setSender(sender);
        	    doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        	    doc.setDivisionGuid(DSDivision.ROOT_GUID);
        	    doc.setCurrentAssignmentAccepted(Boolean.FALSE);

                doc.setDocumentDate(new Date());

                DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);                
                doc.setSummary(documentKind.getName());

                InOfficeDocumentKind kind = InOfficeDocumentKind.list().get(0);
                doc.setKind(kind);

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setIncomingDate(currentDay.getTime());

                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                  // referent nie jest na razie przydzielany
                  //doc.setClerk(DSApi.context().getDSUser().getName());

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                doc.create();    
                
                doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));     
                                          
                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));
                  
                doc.setDocumentKind(documentKind);

                Long newDocumentId = doc.getId();

                Map<String,Object> values = new HashMap<String,Object>();
                values.put(Prosikalogic.BARCODE_FIELD_CN, bar);
                values.put(Prosikalogic.SPOSOB_PRZYJECIA_CN, 50);
               
                documentKind.setOnly(newDocumentId, values);
                  
                //documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                //documentKind.logic().documentPermissions(doc);  
                DSApi.context().session().save(doc);
                DSApi.context().session().flush();
                DSApi.context().session().refresh(doc);
                  
                 
                 
                	 
                Attachment attachment = new Attachment(bar);
                doc.createAttachment(attachment);
                AttachmentRevision ar = attachment.createRevision(file.getFile());
                ar.setOriginalFilename(bar);
                if(Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
                	ar.setOnDisc(AttachmentRevision.FILE_ON_DISC);
                else
                	ar.setOnDisc(AttachmentRevision.FILE_IN_DATABASE);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
            catch (Exception e)
            {
            	log.error("",e);
            	addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
            if(doc.getId()==null)
            {
            	addActionError(sm.getString("NieUdaloSiePrzyjacDokumentu"));
            	return;
            }

            Integer sequenceId;
            Long journalId;
            try
            {
            	Journal journal = Journal.getMainIncoming();
                journalId = journal.getId();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date());
            }
            catch (EdmException e)
            {
            	addActionError(sm.getString("NieMoznaDodacPismaDoDziennika"));
                log.debug("",e);
                return;
            }
            try
            {
            	if(true)
            		throw new EdmException("Funkcja wy��czona");
    	        DSApi.context().begin();
    	        DSApi.context().session().refresh(doc);
    	        doc.bindToJournal(journalId, sequenceId);
    	        WorkflowFactory.createNewProcess(doc, false);
    	        doc.getDocumentKind().logic().onStartProcess(doc);
    	        docId = doc.getId();
    	        
    	        DSApi.context().commit(); 
    	        
            }
            catch (Exception e)
            {            	
    	    	DSApi.context().setRollbackOnly();
    	    	log.error("",e);
            }
        }
    }

    public boolean isCanChooseKind()
    {
        return canChooseKind;
    }

    public void setCanChooseKind(boolean canChooseKind)
    {
        this.canChooseKind = canChooseKind;
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds()
    {
        return documentKinds;
    }

    public void setDocumentKinds(Map<String, String> documentKinds)
    {
        this.documentKinds = documentKinds;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public List<InOfficeDocumentKind> getKinds()
    {
        return kinds;
    }

    public void setKinds(List<InOfficeDocumentKind> kinds)
    {
        this.kinds = kinds;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public void setFm(FieldsManager fm)
    {
        this.fm = fm;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public void setCanReadDictionaries(boolean canReadDictionaries)
    {
        this.canReadDictionaries = canReadDictionaries;
    }

    public boolean isCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getOrderId() {
		return orderId;
	}

}
