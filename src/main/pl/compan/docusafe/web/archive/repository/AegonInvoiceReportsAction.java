/*
 * AegonInvoiceReportsAction.java
 *
 */

package pl.compan.docusafe.web.archive.repository;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.db.criteria.NativeProjection.AggregateProjection;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
/**
 *
 * @author Piotr Komisarski
 */

public class AegonInvoiceReportsAction extends EventActionSupport
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(AegonInvoiceReportsAction.class.getPackage().getName(),null);
	final static String FILE_ENCODING = "windows-1250";
	private static final String ZestawienieTable = "DSG_INVOICE_ZESTAWIENIA";
	private static final String ZestawienieIdColumn = "ZESTAWIENIE_ID";
	private static final String ZestawienieDataColumn = "ZESTAWIENIE_DATA";
	private static final String ZestawienieUserColumn = "ZESTAWIENIE_USER";
    
    private Integer unreportedInvoices;
    private Integer maxReportNumber;//maksymalny wystepujacy nr zestawienia
    private Integer download;//ktore zestawienie generowac

    private Boolean listUnpaidInvoices;//definiuje widok. Czy widac raporty czy liste faktur niezaplaconych
    private Boolean canListUnpaidInvoices;//definiuje czy ktos moze sie przelaczyc na ten widok

    private Integer offset;
    private Integer limit;
    
    private String date;
    private Long[] Ids;

    private List<InvoiceReport> reports;

    private Pager pager;
    
    private final static String INVOICE_ACCOUNTING = "INVOICE_ACCOUNTING";
    public final static String URL = "/repository/aegon-invoice-reports.action";
    
    static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#######0.00");
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append( new GenerateFile()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGenerate").
            append(OpenHibernateSession.INSTANCE).
            append(new Generate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPay").
            append(OpenHibernateSession.INSTANCE).
            append(new Pay()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Map<Integer, InvoiceReport> reportsMap = new HashMap<Integer, InvoiceReport>();
            reports = new ArrayList<AegonInvoiceReportsAction.InvoiceReport>();
            if(download!=null)
                return;
            canListUnpaidInvoices = canListUnpaidInvoices();
            listUnpaidInvoices = (listUnpaidInvoices==null)?false:listUnpaidInvoices && canListUnpaidInvoices;

            try
            {
                DocumentKind invoice = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_KIND);
                String tableName = invoice.getTablename();
                if(listUnpaidInvoices)
                {
                    String sql = "select document_id, nr_zestawienia, kwota_brutto, dostawca, data_platnosci from " + tableName + " where nr_zestawienia is not null and zaplacono is null";
                    PreparedStatement ps = DSApi.context().prepareStatement(sql);
                    ResultSet rs = ps.executeQuery();
                    reports = new ArrayList<AegonInvoiceReportsAction.InvoiceReport>();
                    while(rs.next())
                    {
                        InvoiceReport ir = new InvoiceReport();
                        ir.setListingNo(rs.getInt("nr_zestawienia"));
                        ir.setGrossAmount(rs.getFloat("kwota_brutto"));
                        Long id = rs.getLong("dostawca");
                        //DfInst inst  = DfInst.find(id);
                        DicInvoice inst = DicInvoice.getInstance().find(id);
                        ir.setSupplyer(inst.getName());
                        ir.setSupplyerNumber(inst.getNumerKontrahenta());
                        ir.setPaymentDate(DateUtils.formatCommonDate(rs.getDate("data_platnosci")));
                        ir.setId(rs.getInt("document_id"));
                        reports.add(ir);
                    }
                    rs.close();
                    DSApi.context().closeStatement(ps);                    
                }
                else
                {
                    String nrZestawieniaColumn = invoice.getFieldByCn(InvoiceLogic.NUMER_ZESTAWIENIA_FIELD_CN).getColumn();
                    String akceptacjaFinalnaColumn = invoice.getFieldByCn(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN).getColumn();
                    String query = "select DOCUMENT_ID from " + tableName + " where "+nrZestawieniaColumn+" is null and "+akceptacjaFinalnaColumn+"=?";
                    PreparedStatement ps = DSApi.context().prepareStatement(query);
                    ps.setInt(1, 1);
                    ResultSet rs = ps.executeQuery();
                    unreportedInvoices = 0;
                    while(rs.next())
                    {
                        Integer x = rs.getInt("DOCUMENT_ID");
                        unreportedInvoices++;
                    }
                    rs.close();
                    DSApi.context().closeStatement(ps);

                    maxReportNumber = 0;

                    //query = "select " + nrZestawieniaColumn + " from " + tableName + " where " + nrZestawieniaColumn + " is not null";
                    //query = "select ZESTAWIENIE_ID, ZESTAWIENIE_DATA from DSG_INVOICE_ZESTAWIENIA";
                    
                    query = "select inv.nr_zestawienia as nr, zest.zestawienie_data as data from dsg_invoice inv join DSG_INVOICE_ZESTAWIENIA zest on nr_zestawienia = zestawienie_id where nr_zestawienia is not null";
                    
                    ps = DSApi.context().prepareStatement(query);

                    rs = ps.executeQuery();
                    while(rs.next())
                    {
                        //Integer x = rs.getInt(nrZestawieniaColumn);
                    	Integer x = rs.getInt("nr");
                        Date dt = rs.getDate("data");
                        maxReportNumber = (maxReportNumber>x)?maxReportNumber:x;
                        if(!reportsMap.containsKey(x))
                            reportsMap.put(x, new InvoiceReport(x, 1, dt));
                        else
                            reportsMap.get(x).incrementCount();
                    }
                    rs.close();
                    DSApi.context().closeStatement(ps);                    

                    reports = new ArrayList<AegonInvoiceReportsAction.InvoiceReport>();
                    for(InvoiceReport r : reportsMap.values())
                        reports.add(r);

                    Collections.sort(reports, new Comparator() {
                        public int compare(Object o1, Object o2) {
                        return  ((InvoiceReport)o2).getId() - ((InvoiceReport)o1).getId();
                    }
                    });
                    
                    //pager
                    
                    offset = offset != null ? offset : 0;
                    limit = limit != null ? limit : 10;
                    
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return HttpUtils.makeUrl(URL, new Object[] {"offset", String.valueOf(offset),"limit",String.valueOf(limit)});
                        }
                    };

                    pager = new Pager(linkVisitor, offset, limit, reports.size(), 10);
                    reports = reports.subList(offset, offset+limit <= reports.size() ? offset+limit : reports.size());
                    
                    //pger : end
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                LogFactory.getLog("eprint").debug("", e);
            }
            catch(SQLException sqle)
            {
                addActionError(sqle.getMessage());
                LogFactory.getLog("eprint").debug("", sqle);
            }
        }
    }

    private class Pay implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DocumentKind invoice = DocumentKind.findByCn("invoice");
                String nrZestawieniaColumn = invoice.getFieldByCn(InvoiceLogic.NUMER_ZESTAWIENIA_FIELD_CN).getColumn();
                String tableName = invoice.getTablename();
                String query = "update " + tableName + " set zaplacono = ?, data_zaplaty = ? where document_id = ?";
                
                java.sql.Date d;
                Map<String, Object> values = new HashMap<String, Object>();
                EnumItem stat = invoice.getFieldByCn("STATUS").getEnumItemByCn("ZAKSIEGOWANA");
                values.put("STATUS", stat.getId());
                if(date==null || date.length()==0)
                    throw new EdmException(sm.getString("NieWybranoDaty"));
                d = new java.sql.Date(DateUtils.parseJsDate(date).getTime());
                for(Long l : Ids)
                {
                	PreparedStatement ps = DSApi.context().prepareStatement(query);
                    invoice.setOnly(l, values);
                    ps.setInt(1, 1);
                    ps.setDate(2, d);
                    ps.setLong(3, l);
                    ps.execute();
                    DSApi.context().closeStatement(ps);                    
                }
                if(Ids.length>0)
                    addActionMessage(sm.getString("Zaplacono"));                
            }
            catch(SQLException sqle)
            {
                addActionError(sqle.getMessage());
            }
            catch(EdmException edme)
            {
                addActionError(edme.getMessage());
            }
        }
     }

    private class Generate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DocumentKind invoice = null;
            	if(DocumentKind.isAvailable(DocumentLogicLoader.INVOICE_PTE))
            	{
            		invoice = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_PTE);
            	}
            	invoice = DocumentKind.findByCn("invoice");
            	
            	// numer zestawienia
            	maxReportNumber = getLastReportNumber() + 1;
            	
            	String nrZestawieniaColumn = invoice.getFieldByCn(InvoiceLogic.NUMER_ZESTAWIENIA_FIELD_CN).getColumn();
                String akceptacjaFinalnaColumn = invoice.getFieldByCn(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN).getColumn();
                String tableName = invoice.getTablename();
            	
                // sprawdzenie czy sa faktury dostepne dla utworzenia nowego zestawienia
            	if (!hasInvoicesToUpdate(tableName, akceptacjaFinalnaColumn, nrZestawieniaColumn))
            		return;
                
                String query = "update " + tableName + " set " + nrZestawieniaColumn + " = ? where " + akceptacjaFinalnaColumn + " = 1 and " + nrZestawieniaColumn + " is null";
                PreparedStatement ps = DSApi.context().prepareStatement(query);
                ps.setInt(1, maxReportNumber);
                ps.execute();
                ps.close();
                
                query = "insert into " + 
                	ZestawienieTable + "(" + 
                	ZestawienieIdColumn  + "," + 
                	ZestawienieDataColumn + "," + 
                	ZestawienieUserColumn + ") values ( ? , ? , ? )";
                
                ps = DSApi.context().prepareStatement(query);
                ps.setInt(1, maxReportNumber);
                ps.setTimestamp(2, new Timestamp(new Date().getTime()));
                ps.setString(3, DSApi.context().getPrincipalName());
                ps.execute();
                DSApi.context().closeStatement(ps);
            }
            catch(SQLException sqle)
            {
                addActionError(sqle.getMessage());
            }
            catch(EdmException edme)
            {
                addActionError(edme.getMessage());
            }            
        }
     }

    private class GenerateFile implements ActionListener
    {

        public void actionPerformed(ActionEvent event) {
            
            try
            {
                if (download==null)
                    return;
                DocumentKind invoice = DocumentKind.findByCn("invoice");
                String tableName = invoice.getTablename();
                String query = "select DOCUMENT_ID from " + tableName + "  where nr_zestawienia =?";
                PreparedStatement ps = DSApi.context().prepareStatement(query);
                ps.setInt(1, download);
                ResultSet rs = ps.executeQuery();
            
                List<Integer> ids = new ArrayList<Integer>();
                while(rs.next())
                    ids.add(rs.getInt("DOCUMENT_ID"));
                rs.close();
                DSApi.context().closeStatement(ps);
                
                StringBuffer sb;
                FieldsManager fm;
                File file = File.createTempFile("docusafe", "invoice");
                file.deleteOnExit();
                
                OutputStream os = new FileOutputStream(file);
                sb = new StringBuffer();
                sb.append("Numer faktury;");
                sb.append("Dostawca;");
                sb.append("Numer kontrahenta;");
                sb.append("Data wystawienia;");
                sb.append("Data platnosci;");
                sb.append("MPK;");
                sb.append("Konto kosztowe;");
                sb.append("Kwota brutto;");
                sb.append("Kwota faktury;");
                sb.append("Akceptacja;");
                sb.append("Opis;");
                sb.append("Numer rachunku bankowego;");               
                sb.append("\r\n");
                
                os.write(sb.toString().getBytes(FILE_ENCODING));
                
                StringBuffer pre, mid, post;
                
                for(Integer i : ids)
                {
                    pre = new StringBuffer();
                    post = new StringBuffer();
                    
                    //fm = invoice.getFieldsManager(new Long(i));
                    Document doc = Document.find(Long.valueOf(i));
                    fm = doc.getDocumentKind().getFieldsManager(doc.getId());
                    
                    pre.append(fm.getValue(InvoiceLogic.NUMER_FAKTURY_FIELD_CN));
                    pre.append(';');
                    
                    if(doc.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND))
                    {
                    	DicInvoice inst = (DicInvoice) fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN);
                    	try { pre.append(inst.getName()); } catch (Exception e) { }
                    	pre.append(';');
                        try { pre.append(inst.getNumerKontrahenta()); } catch (Exception e) { }                    
                    }
                    else if(doc.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_PTE))
                    {
                    	DpInst inst = (DpInst) fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN);
                    	try { pre.append(inst.getName()); } catch (Exception e) { }
                    	pre.append(';');
                        try { pre.append(inst.getNumerKontrahenta()); } catch (Exception e) { }                    
                    }
                    pre.append(';');
                    Date data = (Date)fm.getValue(InvoiceLogic.DATA_WYSTAWIENIA_FIELD_CN);
                    try { pre.append(DateUtils.formatCommonDate(data)); } catch (Exception e) { }
                    pre.append(';');
                    
                    data = (Date)fm.getValue(InvoiceLogic.DATA_PLATNOSCI_FIELD_CN);
                    if (data != null)
                    	pre.append(DateUtils.formatCommonDate(data));
                    else
                    	pre.append("brak daty");
                    pre.append(';');
                    
                    post.append(';');                   
                    post.append(fm.getDescription(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN));
                    post.append(';');
                    
                    String opisTowaru = (String)fm.getValue(InvoiceLogic.OPIS_TOWARU_FIELD_CN);
                    opisTowaru = TextUtils.clearEntersOrNull(opisTowaru) != null ?  TextUtils.clearEntersOrNull(opisTowaru) : opisTowaru;
                    opisTowaru = (opisTowaru==null || opisTowaru.length() < 1)?"---":opisTowaru;
                    post.append(opisTowaru);
                    //post.append(';');
                    //to musze doklejac formatowanie bylo tekstowe                                        
                    
                    post.append(';');
                    try
                    {
                    	String numerKonta = (String)fm.getValue(InvoiceLogic.NUMER_RACHUNKU_BANKOWEGO_CN);
                    	try
                    	{
                    		//Jesli zaczyna sie od liczby to doklejamy PL
                    		Integer x = new Integer(numerKonta.substring(0,1));
                    		post.append("PL");
                    	}
                    	catch (Exception e1) { };
                    	post.append(numerKonta);
                    }
                    catch (Exception e)
                    {
                    	
                    }
                    post.append("\r\n");
                    
                    @SuppressWarnings("unchecked")
                    List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
                    for(int j=0;j<list.size(); j++)
                    {
                        mid = new StringBuffer();
                        mid.append(list.get(j).getCentrumCode());
                        mid.append(';');                        
                        String str = list.get(j).getAccountNumber();
                        str = (str==null)?"":str;                        
                        mid.append(str);
                        mid.append(';');
                        mid.append(NUMBER_FORMAT.format(list.get(j).getAmount().doubleValue()));
                        mid.append(';');
                        if(j == 0)
                        {
                        	mid.append(fm.getValue(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN));                            
                        }
                        os.write(pre.toString().getBytes(FILE_ENCODING));
                        os.write(mid.toString().getBytes(FILE_ENCODING));
                        os.write(post.toString().getBytes(FILE_ENCODING));
                    }
                    
                }
                os.close();
                
                ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"Zestawienie nr."+download+".csv\"");
                
                event.setResult(NONE);
            }
            catch(EdmException edme)
            {
                addActionError(edme.getMessage());
            }
            catch(SQLException sqle)
            {
                addActionError(sqle.getMessage());
            }
            catch(IOException ioe)
            {
                addActionError(ioe.getMessage());
            }
        }
    }

    private boolean canListUnpaidInvoices()
    {
        try
        {
            //INVOICE_ACCOUNTING
            DSUser user = DSApi.context().getDSUser();
            if(user.isAdmin())
                return true;
            DSDivision[] divisions = user.getDivisions();
            for(int i=0;i<divisions.length;i++)
                if(divisions[i].getGuid().equals(INVOICE_ACCOUNTING))
                    return true;
        }
        catch(EdmException edme)
        {
            return false;
        }
        return false;
    }
    
    /**
     * Zwraca ostatni numer zestawienia
     * @return
     * @throws EdmException
     */
    private Integer getLastReportNumber() throws EdmException
    {
    	CriteriaResult cr = DSApi.context().createNativeCriteria(ZestawienieTable, "zt")
    		.setProjection(NativeExps.projection()
    				.addProjection("zt." + ZestawienieIdColumn))
    		.addOrder(NativeExps.order().add("zt." + ZestawienieIdColumn, OrderType.DESC))
    		.setLimit(1)
    		.criteriaResult();
    	
    	if (cr.next())
    		return cr.getInteger(0, 0);
    		
    	return 0;
    }
    
    /**
     * Sprawdza czy sa faktury dost�pne do utworzenia zestawienia
     * @param tableName
     * @param akceptacjaFinalnaColumn
     * @param nrZestawieniaColumn
     * @return
     * @throws EdmException
     */
    private Boolean hasInvoicesToUpdate(String tableName, String akceptacjaFinalnaColumn, 
    		String nrZestawieniaColumn) throws EdmException
    {
    	CriteriaResult cr = DSApi.context().createNativeCriteria(tableName, "inv")
    		.setProjection(NativeExps.projection()
    				.addAggregateProjection("inv.DOCUMENT_ID", "docCount", AggregateProjection.COUNT))
    		.add(NativeExps.eq("inv." + akceptacjaFinalnaColumn, 1))
    		.add(NativeExps.isNull("inv." + nrZestawieniaColumn))
			.criteriaResult();
    	
    	if (cr.next())
    		return cr.getInteger("docCount", 0) > 0;
		
		return false;
    }

    public Integer getMaxReportNumber() {
        return maxReportNumber;
    }

    public void setMaxReportNumber(Integer maxReportNumber) {
        this.maxReportNumber = maxReportNumber;
    }

    public Integer getUnreportedInvoices() {
        return unreportedInvoices;
    }

    public void setUnreportedInvoices(Integer unreportedInvoices) {
        this.unreportedInvoices = unreportedInvoices;
    }

    public List<AegonInvoiceReportsAction.InvoiceReport> getReports() {
        return reports;
    }

    public void setReports(List<AegonInvoiceReportsAction.InvoiceReport> reports) {
        this.reports = reports;
    }

    public Integer getDownload() {
        return download;
    }

    public void setDownload(Integer download) {
        this.download = download;
    }

    public Boolean getListUnpaidInvoices()
    {
        return listUnpaidInvoices;
    }

    public void setListUnpaidInvoices(Boolean listUnpaidInvoices)
    {
        this.listUnpaidInvoices = listUnpaidInvoices;
    }

    public Boolean getCanListUnpaidInvoices()
    {
        return canListUnpaidInvoices;
    }



    private class InvoiceReport
    {
        private Integer id;
        private Integer count;
        private Float grossAmount;
        private Integer listingNo;
        private String paymentDate;
        private String supplyer;
        private String supplyerNumber;
        private Date when;
        private Integer countFromSQL;
        
        
        public InvoiceReport() 
        {
        	this.count = new Integer(0);
        	
        	this.countFromSQL = _getCount();
        }

        public InvoiceReport(Integer id, Integer count) {
            this.id = id;
            this.count = count;
            
            this.countFromSQL = _getCount();
        }

        public InvoiceReport(Integer id, Integer count, Date when) {
            this.id = id;
            this.count = count;
            this.when = when;
            
            this.countFromSQL = _getCount();
        }
        
        public void incrementCount()
        {
            count++;
        }
        
        public Integer getCount()
        {
        	//return this.count;
        	return this.countFromSQL;
        }

        public Integer _getCount()
        {
        	Integer retCount = 0;
    		PreparedStatement ps = null;
    		try
    		{
    			ps = DSApi.context().prepareStatement("SELECT COUNT(document_id) as count FROM dsg_invoice where nr_zestawienia = ?");
    			ps.setInt(1, this.id);
    		
    			ResultSet rs = ps.executeQuery();
    			while(rs.next())
    			{
    				retCount = rs.getInt("count");
    			}
    			
    			DSApi.context().closeStatement(ps);
    			ps = null;
    		}
    		catch (Exception e) 
    		{
    			e.printStackTrace();
    			retCount = this.count;
			}
    		finally
    		{
    			DSApi.context().closeStatement(ps);
    		}
        	return retCount;
        }

        public void setCount(Integer count)
        {
            this.count = count;
        }

        public Integer getId() 
        {
            return id;
        }

        public void setId(Integer id)
        {
            this.id = id;
        }

        public Float getGrossAmount()
        {
            return grossAmount;
        }

        public void setGrossAmount(Float kwota_brutto)
        {
            this.grossAmount = kwota_brutto;
        }

        public Integer getListingNo()
        {
            return listingNo;
        }

        public void setListingNo(Integer nrZestawienia)
        {
            this.listingNo = nrZestawienia;
        }

        public String getPaymentDate()
        {
            return paymentDate;
        }

        public void setPaymentDate(String paymentDate)
        {
            this.paymentDate = paymentDate;
        }

        public String getSupplyer()
        {
            return supplyer;
        }

        public void setSupplyer(String supplyer)
        {
            this.supplyer = supplyer;
        }

		public String getSupplyerNumber() {
			return supplyerNumber;
		}

		public void setSupplyerNumber(String supplyerNumber) {
			this.supplyerNumber = supplyerNumber;
		}

		public Date getWhen() {
			return when;
		}

		public void setWhen(Date when) {
			this.when = when;
		}
		
		public String getStrWhen()
		{
			return DateUtils.formatCommonDate(this.when);
		}

    }

    public void setIds(Long[] arg)
    {
        for(Long l : arg)

        this.Ids = arg;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

}
