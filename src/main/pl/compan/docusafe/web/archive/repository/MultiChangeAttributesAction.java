package pl.compan.docusafe.web.archive.repository;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class MultiChangeAttributesAction extends EventActionSupport {

	/**
	 * 
	 */
	
	Logger log = LoggerFactory.getLogger(MultiChangeAttributesAction.class);
	
	private static final long serialVersionUID = 1L;
	static StringManager sm = GlobalPreferences.loadPropertiesFile(MultiChangeAttributesAction.class.getPackage().getName(), null);
	protected Map<String,Object> values = new HashMap<String,Object>();
	protected String documentKindCn;
	private DocumentKind documentKind;
	private FieldsManager fm;
	private String documentKindName;
	private Boolean specialBool = true;
	
	private List<Aspect> documentAspects;
    private String documentAspectCn;
	private Integer count;
	
	private Boolean canReadDictionaries;	
	private String redirectUrl;
	
	protected void setup() 
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
		
        registerListener("doChangeAttributes").
            append(OpenHibernateSession.INSTANCE).
            append(new ChangeAttributes()).
            append("fillForm",fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
        	if(ServletActionContext.getRequest().getSession().getAttribute("documentIds_multi") == null)
        		return;
        	
        	try
        	{
        		canReadDictionaries = true;
        		count = ((List<Long>)ServletActionContext.getRequest().getSession().getAttribute("documentIds_multi")).size();
        		documentKind = Document.find(((List<Long>)ServletActionContext.getRequest().getSession().getAttribute("documentIds_multi")).get(0)).getDocumentKind();
        		documentKindCn = documentKind.getCn();
        		documentKindName = documentKind.getName();
        		fm = documentKind.getFieldsManager(null);
        		fm.initialize();
        		documentAspects = documentKind.getAspectsForUser(DSApi.context().getPrincipalName());
                if(documentAspects == null)
                {
                	documentAspectCn = null;
                }
                else if(documentAspects != null && documentAspectCn == null)
                {
                	documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
                }
        	}
        	catch (Exception e) 
        	{
				log.error("MultiChangeAttributesAction",e);
			}
        }
    }

	private class ChangeAttributes implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Map<String,Object> wartosci = new HashMap<String,Object>();
			for(String key : values.keySet())
			{
				if(values.get(key) != null)
				{
					wartosci.put(key,values.get(key));
				}
			}
			
			boolean clean = true;
			
			Document doc = null;
			DataMartEvent dme = new DataMartEvent(true, null, null, DataMartDefs.DOCUMENT_FIELD_MULTI_CHANGE, documentKindCn, null, null, null);
			Long multichangeId = dme.getEventId();
			for(Long id : (List<Long>)ServletActionContext.getRequest().getSession().getAttribute("documentIds_multi"))
			{
				try
				{
					doc = Document.find(id);
					DSApi.context().begin();
					
					doc.getDocumentKind().setWithHistory(doc.getId(), wartosci, false, multichangeId != null ? ""+multichangeId.intValue() : null);
					doc.getDocumentKind().logic().archiveActions(doc, DocumentKindsManager.getType(doc));
					doc.getDocumentKind().logic().documentPermissions(doc);
					addActionMessage(sm.getString("ZmienionoAtrybutyDlaDokumentuOId")+" : "+id);
					
					DSApi.context().commit();
										
				}
				catch (Exception e) 
				{
					clean = false;
					//e.printStackTrace();
					addActionError(sm.getString("NieUdaloSieZmienicAtrybutowDlaDokumentuOId")+" : "+id+"  "+e.getMessage());
					DSApi.context()._rollback();
				}				
			}
			
			doc = null;
			ServletActionContext.getRequest().getSession().setAttribute("documentIds_multi", null);
			
			redirectUrl = "/reports/office/documents-multichange.action?doGenerate=true&changeId="+multichangeId;
			if(clean)
			{
				event.skip("fillForm");
				event.setResult("redirect");
			}
		}
	}
	
	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public DocumentKind getDocumentKind() {
		return documentKind;
	}

	public void setDocumentKind(DocumentKind documentKind) {
		this.documentKind = documentKind;
	}

	public FieldsManager getFm() {
		return fm;
	}

	public void setFm(FieldsManager fm) {
		this.fm = fm;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public String getDocumentKindName() {
		return documentKindName;
	}

	public void setDocumentKindName(String documentKindName) {
		this.documentKindName = documentKindName;
	}

	public static StringManager getSm() {
		return sm;
	}

	public static void setSm(StringManager sm) {
		MultiChangeAttributesAction.sm = sm;
	}

	public Boolean getSpecialBool() {
		return specialBool;
	}

	public void setSpecialBool(Boolean specialBool) {
		this.specialBool = specialBool;
	}

	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}

	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}

	public String getDocumentAspectCn() {
		return documentAspectCn;
	}

	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}

	public Integer getCount() {
		return count;
	}

	public Boolean getCanReadDictionaries() {
		return canReadDictionaries;
	}

	public void setCanReadDictionaries(Boolean canReadDictionaries) {
		this.canReadDictionaries = canReadDictionaries;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

}
