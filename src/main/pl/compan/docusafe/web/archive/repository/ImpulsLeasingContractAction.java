package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ImpulsLeasingContractAction extends EventActionSupport
{
    
    private Long id;
    private DlContractDictionary dlC;
    private List<Map<String, Object>> documents;
    private static StringManager smL =
        GlobalPreferences.loadPropertiesFile(ImpulsLeasingContractAction.class.getPackage().getName(),null);


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);   
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                dlC = DlContractDictionary.getInstance().find(id);
                if(dlC==null)
                {
                   event.addActionMessage(smL.getString("NieZnalezionoUmowy"));
                   return;
                }   

                //dokumenty
                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.NUMER_UMOWY_CN);
                dockindQuery.field(f,dlC.getId());
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                documents = new ArrayList<Map<String,Object>>(searchResults.totalCount());
                
                while (searchResults.hasNext())
                {
                    Document document = searchResults.next();
                    Map<String, Object> bean = new HashMap<String, Object>(); 
                    FieldsManager fm = kind.getFieldsManager(document.getId());
                    bean.put("id",document.getId());
                    bean.put("rodzajDokumentu",fm.getValue(DlLogic.RODZAJ_DOKUMENTU_CN));
                    bean.put("typDokumentu",fm.getValue(DlLogic.TYP_DOKUMENTU_CN));
                    bean.put("data",fm.getValue(DlLogic.DATA_DOKUMENTU_CN));
                //    if(fm.getValue(DlLogic.NUMER_UMOWY_CN)!=null) 
                 //       bean.put("numerUmowy",((DlContractDictionary)fm.getValue(DlLogic.NUMER_UMOWY_CN)).getNumerUmowy());
                    bean.put("link","/repository/edit-dockind-document.action?id="+document.getId());
                    documents.add(bean);
                }
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }
            
        }
    }

    public DlContractDictionary getDlC()
    {
        return dlC;
    }

    public void setDlC(DlContractDictionary dlC)
    {
        this.dlC = dlC;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
    
    public List<Map<String, Object>> getDocuments()
    {
        return documents;
    }

    public void setDocuments(List<Map<String, Object>> documents)
    {
        this.documents = documents;
    }

}
