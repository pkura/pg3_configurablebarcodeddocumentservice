package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;

import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import pl.compan.docusafe.core.office.ContractManagment;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

public abstract class ContractManagmentTabAction extends EventActionSupport {

    protected abstract List<Tab> prepareTabs();
    private static final Logger log = LoggerFactory.getLogger(ContractManagmentTabAction.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(ContractManagmentTabAction.class.getPackage().getName(), null);
    private Long documentId;
    private ContractManagment contractManagment;
    private List<DSUser> users;
    private Long id;
    private Long contractId;
    private List<ContractManagment> contracts;
    private Long contractsCount;
    private Pager pager;
    private int LIMIT = 20;
    protected Integer offset;
    protected String sortField;
    protected Boolean ascending;
    private List<TableColumn> columns;
    private String[] activityIds;
    private String contractDate;
    private String returnDate;
    
    private Date searchContractDate;
    private Date searchReturnDate;
    private String searchNote;
    private Long searchUserId;
    
    public static final String EV_SAVE_OR_CREATE = "save_or_create";
    public static final String EV_REMOVE_BATCH = "remove_batch";
    public static final String EV_DELETE = "delete";
    public static final String EV_SEARCH = "search";
    public static final String CONTRACT = "10";
    public static final String CONTRACT_TP = "20";

    @Override
    protected void setup() {
        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new CheckActionRequest()).
                append(new FillForm(true)).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
                append(OPEN_HIBERNATE_AND_JBPM_SESSION).
                append(new CheckActionRequest()).
                append(new ValidateContractManagmentRequest()).
                append(EV_SAVE_OR_CREATE, new SaveContractManagment()).
                append(new FillForm(false)).
                appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doRemove").
                append(OpenHibernateSession.INSTANCE).
                append(new CheckActionRequest()).
                append(new DeleteContractManagment()).
                append(new FillForm(true)).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRemoveBatch").
                append(OPEN_HIBERNATE_AND_JBPM_SESSION).
                append(new CheckActionRequest()).
                append(EV_REMOVE_BATCH, new RemoveBatchAction()).
                append(new FillForm(true)).
                appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doSearch").
                append(OPEN_HIBERNATE_AND_JBPM_SESSION).
                append(new CheckActionRequest()).
                //append(new ValidateContractManagmentRequest()).
                append(EV_SEARCH, new Search()).
                append(new FillForm(true)).
                appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        //setActualColumns();
    }

    private class CheckActionRequest implements ActionListener {

        public static final String _TASK_LIST = "explore-documents";
        public static final String _SIMPLEOFFICE_CREATED = "simpleoffice-created";

        public void actionPerformed(ActionEvent event) {

            if (isDocumentIdNull()) {
                if (event.getResult() == null) {
                    event.setResult(getRedirectResult());
                    event.cancel();
                }
                return;
            }

            if (!isDocumentExist()) {
                if (event.getResult() == null) {
                    event.setResult(getRedirectResult());
                    event.cancel();
                }
                return;
            }
            id = documentId;
            createUsersList();
        }

        public String getRedirectResult() {
//            if (Configuration.officeAvailable() || Configuration.faxAvailable()) {
                return _TASK_LIST;
//            } else {
//                return _SIMPLEOFFICE_CREATED;
//            }
        }

        public boolean isDocumentExist() {
            try {
                return ContractManagment.isDocumentInDP(getDocumentId());
            } catch (Exception e) {
                logException(e);
            }

            return false;
        }
    }

    /**CheckActionRequest:END_CLASS*/
    public class FillForm implements ActionListener {

        private boolean needNewForm = false;

        FillForm() {
        }

        FillForm(boolean needNewForm) {
            this.needNewForm = needNewForm;
        }

        public void actionPerformed(ActionEvent event) {
            createFormAndListData(needNewForm);
            setActualColumns();
            this.setPagerInstance();
        }

        public void setPagerInstance() {
            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {

                public String getLink(int offset) {
                    String link = getBaseLink().substring(0, getBaseLink().indexOf("?"));
                    Object[] params = new Object[]{
                                "sortField", sortField,
                                "documentId", documentId,
                                "ascending", String.valueOf(ascending),
                                "offset", String.valueOf(offset),
                    "searchUserId", null,
                    "searchContractDate", null,
                    "searchReturnDate", null,
                    "searchNote", null
                    };
                    
                    if(searchUserId != null)
//                        params[8] = "searchUserId"; 
                        params[9] = searchUserId;
                    if(searchContractDate != null)
//                        params[10] = "searchContractDate"; 
                        params[11] = searchContractDate;
                    if(searchReturnDate != null)
//                        params[12] = "searchReturnDate"; 
                        params[13] = searchReturnDate;
                    if(searchNote != null)
//                        params[14] = "searchNote"; 
                        params[15] = searchNote;
                    
                    return HttpUtils.makeUrl(link,params);
                };
                
                public String getBaseLink() {
                    return "/repository/contract-managment.action?";
                }
            };

            pager = new Pager(linkVisitor, offset == null ? 0 : offset, LIMIT, contractsCount.intValue(), 10);
        }
    }

    public class ValidateContractManagmentRequest implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            checkDocumentId();
            checkUserId();
            checkContractDate();
            checkReturnDate();

            if (hasActionErrors()) {
                event.setAttribute("error", Boolean.TRUE);
                if (event.hasListener(EV_SAVE_OR_CREATE)) {
                    event.skip(EV_SAVE_OR_CREATE);
                }
                if (event.hasListener(EV_SEARCH)) {
                    event.skip(EV_SEARCH);
                }
            }
        }

        private void checkDocumentId() {
            contractManagment.setDocumentId(getDocumentId());
        }

        private void checkUserId() {
            try {

                if (contractManagment.getUserId() == null || !isUserExist()) {
                    throw new Exception();
                }
            } catch (Exception e) {
                addActionError(sm.getString("TakiUzytkownikNieIstnieje"));
            }

        }

        private void checkContractDate() {
            if (DateUtils.isEmptyDateString(contractDate)) {
                addActionError(sm.getString("NiepoprawnaDataZamowienia"));
            }
            contractManagment.setContractDate(DateUtils.nullSafeParseJsDate(contractDate));
        }

        private void checkReturnDate() {

        if (!DateUtils.isEmptyDateString(returnDate))
            if (!DateUtils.isGreaterDate(returnDate, contractDate)) {
                addActionError(sm.getString("DataZwrotuMusiBycWieksza"));
            }
            contractManagment.setReturnDate(DateUtils.nullSafeParseJsDate(returnDate));

        }
    }

    public class Search implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            contractManagment.setContractDate(DateUtils.nullSafeParseJsDate(contractDate));
            contractManagment.setReturnDate(DateUtils.nullSafeParseJsDate(returnDate));
            
            searchContractDate = contractManagment.getContractDate();
            searchReturnDate = contractManagment.getReturnDate();
            searchNote = contractManagment.getNote();
            searchUserId = contractManagment.getUserId();
        }
    }
    
    private boolean isUserExist() {
        Iterator userIterator = getUsers().iterator();

        while (userIterator.hasNext()) {
            DSUser user = (DSUser) userIterator.next();
            if ((user.getId()).equals(contractManagment.getUserId())) {
                return true;
            }
        }
        return false;
    }

    /** ValidateContractManagmentRequest:END_CLASS*/
    /**
     * Pobiera wszystkie potrzebne dane
     */
    public void createFormAndListData(boolean getNewFormData) {
        try {
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
          
            contractsCount = ContractManagment.countContractsManagments(documentId,
                    searchContractDate,searchReturnDate,searchNote, searchUserId);

            if (contractsCount > 0) {
                contracts = ContractManagment.findByDocumentId(
                        documentId,
                        sortField == null ? "id" : sortField,
                        ascending == null ? true : ascending,
                        offset == null ? 0 : offset,
                        LIMIT,searchContractDate,searchReturnDate,searchNote, searchUserId);
            } else {
                contracts = new ArrayList<ContractManagment>();
            }


            setContractsUserName();

            if (getNewFormData && getContractId() != null) {
                contractManagment = ContractManagment.getContractManagmentFromList(contracts, getContractId());
            }

            if (contractManagment == null) {
                contractManagment = new ContractManagment(getDocumentId());
            }

        } catch (EdmException e) {
            logException(e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();

        } finally {
            try {
                DSApi.close();
            } catch (EdmException e) {
            }
        }
    }

    public void setContractsUserName() {
        contracts = ContractManagment.setContractsUsersName(contracts, users);
    }

    public class SaveContractManagment implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                contractManagment.save();                
        
                DSApi.context().commit();
                addActionMessage(sm.getString("Zapisano"));

                try {
                    DSApi.context().session().flush();
                } catch (HibernateException e) {
                }

            } catch (EdmException e) {
                logException(e);

                addActionError(e.getMessage());
                try {
                    DSApi.context().setRollbackOnly();
                } catch (Exception e2) {
                }

            } finally {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                }
            }
        }
    }

    public class DeleteContractManagment implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            if (contractManagment.getId() == null) {
                return;
            }

            try {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                contractManagment.delete();

                addActionMessage(sm.getString("Usunieto"));

                DSApi.context().commit();

                try {
                    DSApi.context().session().flush();
                } catch (HibernateException e) {
                }

            } catch (EdmException e) {
                logException(e);
                addActionError(e.getMessage());
                try {
                    DSApi.context().setRollbackOnly();
                } catch (Exception e2) {
                }

            } finally {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                }
            }
        }
    }

    public class RemoveBatchAction implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            if (activityIds == null) {
                return;
            }

            try {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                ContractManagment.deleteAll(activityIds);

                DSApi.context().commit();
                addActionMessage(sm.getString("UsunieteObiekty"));

                try {
                    DSApi.context().session().flush();
                } catch (HibernateException e) {
                }

            } catch (EdmException e) {
                logException(e);

                addActionError(e.getMessage());
                try {
                    DSApi.context().setRollbackOnly();
                } catch (Exception e2) {
                }

            } finally {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                }
            }
        }
    }

    private void createUsersList() {
        try {
            //DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            //setTabs(prepareTabs());
            this.users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

        } catch (EdmException e) {
            logException(e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();

        } finally {
            try {
                DSApi.close();
            } catch (EdmException e) {
            }
        }
    }

    public ContractManagment getContractManagment() {
        return contractManagment;
    }

    public void setContractManagment(ContractManagment contractManagment) {
        this.contractManagment = contractManagment;
    }

    public void logException(Exception e) {
        log.error("", e);
    }

    public void logError(String errorString) {
        log.error(errorString);
    }

    private boolean isDocumentIdNull() {
        return getDocumentId() == null;
    }

    public List<DSUser> getUsers() {
        return users;
    }

    public void setUsers(List users) {
        this.users = users;
    }

    public List<ContractManagment> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractManagment> contracts) {
        this.contracts = contracts;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long id) {
        this.contractId = id;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    private void setActualColumns() {
        List<TableColumn> actualColumns = new ArrayList<TableColumn>(5);
        String[] properties = {"id", "userId", "contractDate", "returnDate", "note"};
        String[] titles = {"Id", "Zamawiajacy", "DataZamowienia", "DataZwrotu", "Uwagi"};
        
        String searchParams = getToLinkSearchParams();
        
        for (int i = 0; i < properties.length; i++) {
            String tmp = getLink() + searchParams + "&sortField="+
                    ("shortReceiveDate".equals(properties[i]) ? "receiveDate" : properties[i]) +
                    "&ascending=";
            
            actualColumns.add(new TableColumn(properties[i], sm.getString(titles[i]), tmp + "false", tmp + "true", ""));
        }

        this.columns = actualColumns;
    }

    private String getToLinkSearchParams()
    {
        StringBuilder sb = new StringBuilder();
//        if(contractManagment == null)
//        {
//            contractManagment = new ContractManagment(getDocumentId());
//        }
        if(searchUserId != null)
        {
            sb.append("&searchUserId=").append(searchUserId.toString());
            contractManagment.setUserId(searchUserId);
        }
        if(searchContractDate != null)
        {
            sb.append("&searchContractDate=").append(searchContractDate);
            contractManagment.setContractDate(searchContractDate);
        }
        if(searchReturnDate != null)
        {
            sb.append("&searchReturnDate=").append(searchReturnDate);
            contractManagment.setReturnDate(searchReturnDate);
        }
        if(searchNote != null)
        {
            sb.append("&searchNote=").append(searchNote);
            contractManagment.setNote(searchNote);
        }
        return sb.toString();
    }
    
    public List<TableColumn> getColumns() {
        return this.columns;
    }

    public String getLink() {
        return "/repository/contract-managment.action?documentId=" + getDocumentId();
    }

    public Pager getPager() {
        return pager;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        if (offset == null) {
            this.offset = 0;
        } else {
            this.offset = offset;
        }
    }

    public String[] getActivityIds() {
        return activityIds;
    }

    public void setActivityIds(String[] activityIds) {
        this.activityIds = activityIds;
    }

    public String getContractDate() {
        return contractDate;
    }

    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public Date getSearchContractDate()
    {
        return searchContractDate;
    }

    public void setSearchContractDate(Date searchContractDate)
    {
        this.searchContractDate = searchContractDate;
    }

    public String getSearchNote()
    {
        return searchNote;
    }

    public void setSearchNote(String searchNote)
    {
        this.searchNote = searchNote;
    }

    public Date getSearchReturnDate()
    {
        return searchReturnDate;
    }

    public void setSearchReturnDate(Date searchReturnDate)
    {
        this.searchReturnDate = searchReturnDate;
    }

    public Long getSearchUserId()
    {
        return searchUserId;
    }

    public void setSearchUserId(Long seatchUserId)
    {
        this.searchUserId = seatchUserId;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    
    
}