package pl.compan.docusafe.web.archive.repository;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Tworzenie nowego dokumentu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewDocumentAction.java,v 1.15 2006/02/20 15:42:36 lk Exp $
 */
public class NewDocumentAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(NewDocumentAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doCreate", "doCancelled"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    public static final String FORWARD = "repository/new-document";

    private static StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        Long folderId = (Long) form.get("folderId");

        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(request));

            fillForm(request, form);

            if (folderId != null)
            {
                Folder folder = Folder.find(folderId);

                if (folder != null)
                {
                    form.set("folderId", folderId);
                    request.setAttribute("folderPrettyPath", folder.getPrettyPath());
                }
            }

        }
        catch (EdmException e)
        {
            log.error("", e);
            errors.add(e.getMessage());
        }
        finally
        {
            DSApi._close();
        }

        return mapping.findForward("main");
    }

    /**
     * Rezygnacja z tworzenia dokumentu.
     */
    public ActionForward doCancelled(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        return new ActionForward(mapping.findForward("repository").getPath(), true);
    }

    public ActionForward doCreate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String title = (String) form.get("title");
        String description = (String) form.get("description");

        // sprawdzenie obecno�ci wymaganych atrybut�w
        if (StringUtils.isEmpty(title))
            errors.add(sm.getString("document.missingTitle"));
        if (StringUtils.isEmpty(description))
            errors.add(sm.getString("document.missingDescription"));

        if (title != null && title.length() > 254)
            errors.add(sm.getString("document.titleTooLong", "254"));
        if (description != null && description.length() > 510)
            errors.add(sm.getString("document.descriptionTooLong", "510"));

        if (errors.size() > 0)
            return mapping.findForward("main");

        Long doctypeId = (Long) form.get("doctypeId");
        Long folderId = (Long) form.get("folderId");
        Long documentId = null;

        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(request));

            ctx.begin();

            fillForm(request, form);

            Folder folder;

            if (folderId != null)
            {
                folder = Folder.find(folderId); // FolderNotFoundException
            }
            else
            {
                folder = Folder.getRootFolder();
            }

            Document document = new Document(
                TextUtils.trimmedStringOrNull((String) form.get("title")),
                TextUtils.trimmedStringOrNull((String) form.get("description")));
            document.setFolder(folder);

            if (doctypeId != null)
                document.setDoctype(Doctype.find(doctypeId));

            document.create();

            ctx.commit();

            documentId = document.getId();
        }
        catch (Exception e)
        {
            errors.add(e.getMessage());
            if (ctx != null) DSApi.context().setRollbackOnly();
        }
        finally
        {
            DSApi._close();
        }

        // je�eli utworzenie dokumentu si� powiod�o, przej�cie do
        // formularza edycji dokumentu
        if (documentId != null)
        {
            messages.add(sm.getString("newDocument.createdDocument"));
            return new ActionForward("/repository/edit-document.action" +
                "?id="+documentId);
        }
        else
        {
            return mapping.findForward("main");
        }
    }

    private void fillForm(HttpServletRequest request, DynaActionForm form) throws EdmException
    {
        List doctypes = Doctype.list(true);
        List beans = new ArrayList(doctypes.size());

        for (Iterator iter=doctypes.iterator(); iter.hasNext(); )
        {
            Doctype doctype = (Doctype) iter.next();
            DynaBean bean = DynaBeans.newHtmlOption();
            bean.set("label", doctype.getName());
            bean.set("value", doctype.getId().toString());
            beans.add(bean);
        }

        request.setAttribute("doctypes", beans);
    }

    public String[] getEventNames()
    {
        return eventNames;
    }

    public static String getLink(HttpServletRequest request, Long folderId)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath()+"?folderId="+folderId;
    }
}
