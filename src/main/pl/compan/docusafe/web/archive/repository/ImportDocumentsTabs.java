package pl.compan.docusafe.web.archive.repository;

import java.util.ArrayList;
import java.util.List;

import bsh.This;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;

public class ImportDocumentsTabs
{
    public static String TAB_DOCUMENTS = "documents";
    public static String TAB_DICTIONARY = "dictionary";
    private static StringManager smL;
    
    public static List<Tab> createTabs(String tab) throws EdmException
    {
        smL = GlobalPreferences.loadPropertiesFile(ImportDocumentsTabs.class.getPackage().getName(),null);
        List<Tab> tabs = new ArrayList<Tab>();
        tabs.add(new Tab(smL.getString("Dokumenty"), smL.getString("ImportDokumentow"), "/repository/import-documents.action", TAB_DOCUMENTS.equals(tab)));
        
        if (DocumentKind.findByCn(DocumentLogicLoader.DAA_KIND) != null)
        {
            tabs.add(new Tab(smL.getString("SlownikAgencji"), smL.getString("SlownikAgencji"), "/repository/agencies-dictionary.action", TAB_DICTIONARY.equals(tab)));
        }
        
        if (tabs.size() == 1)
            tabs.clear();
        return tabs;
    }
}
