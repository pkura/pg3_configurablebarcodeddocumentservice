package pl.compan.docusafe.web.archive.repository.search;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.archive.DocumentOrderManager;
import pl.compan.docusafe.core.base.ExampleDoc;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class SearchExampleAction extends EventActionSupport {

	private static final Logger log = LoggerFactory.getLogger(SearchExampleAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(SearchExampleAction.class.getPackage().getName(),null);
	
	private List<ExampleDoc> docs;
	public List<ExampleDoc> getDocs() {
		return docs;
	}


	public void setDocs(List<ExampleDoc> docs) {
		this.docs = docs;
	}


	private Long id;
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getCreation_date() {
		return creation_date;
	}


	public void setCreation_date(Date creationDate) {
		creation_date = creationDate;
	}


	private String name;
	private Date creation_date;
	private static DocumentOrderManager orderManager = new DocumentOrderManager();
	
    protected void setup()
    {
    	
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
      
    }
  
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	//DocumentOrderManager orderManager =  new DocumentOrderManager();
            	if(name != null)
            	{
            		docs = ExampleDoc.findByName(name);
            		log.error("SearchExampleAction: wykonano metod� .findByName("+name+"), liczba wynikow: "+docs.size());
            	}
            	else
            	{
            		log.error("SearchExampleAction: nie wykonano metody .findByName, parametr name=null");
            		
            	}
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
    }
    
   
}

