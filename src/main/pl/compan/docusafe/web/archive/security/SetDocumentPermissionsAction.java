package pl.compan.docusafe.web.archive.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SetDocumentPermissionsAction.java,v 1.3 2008/07/28 14:29:18 pecet4 Exp $
 */
public class SetDocumentPermissionsAction extends EventActionSupport
{
	String objectType;
	Long objectId;
	List subjects;
	List subjectUsers;
	List permissions;
	List subjectKeys;
	Map subjectPermissions;
	List subjectPerms;
	List availablePermissions;
	private static final Log log = LogFactory.getLog(SetPermissionsAction.class);
	private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(SetDocumentPermissionsAction.class.getPackage().getName(),null);
    protected void setup()
    {
    	FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (objectType == null || objectId == null)
                addActionError(sm.getString("BrakParametruObjectTypeLubObjectId"));

            Class clazz;

            if ("document".equals(objectType))
            {
                clazz = Document.class;
            }
            else if ("folder".equals(objectType))
            {
                clazz = Folder.class;
            }
            else
            {
            	addActionError(sm.getString("NieprawidlowaWartoscParametruObjectType"));
                return;
            }
            
            DSContext ctx = null;
            try
            {
                ctx = DSApi.context();

                permissions = ObjectPermission.find(clazz, objectId);


                // istniej�ce uprawnienia dla bie��cego obiektu
                subjectKeys = new LinkedList();
                subjectPermissions = new HashMap();

                //for (int i=0, n=perms.length; i < n; i++)
                for (Iterator iter=permissions.iterator(); iter.hasNext(); )
                {
                    ObjectPermission permission = (ObjectPermission) iter.next();

                    SubjectKey subjectKey = new SubjectKey(permission.getSubject(), permission.getSubjectType());

                    subjectPerms = (List) subjectPermissions.get(subjectKey.toString());
                    if (subjectPerms == null)
                    {
                        subjectPerms = new ArrayList(4);
                        subjectPermissions.put(subjectKey.toString(), subjectPerms);
                    }
                    subjectPerms.add(permission.getName());

                    if (!subjectKeys.contains(subjectKey))
                        subjectKeys.add(subjectKey);
                }

                // nazwy u�ytkownik�w, kt�rzy maj� jakie� uprawnienia
                DSApi.context().setAttribute("subjectPermissions", subjectPermissions);

    /*
                request.setAttribute("users", uh.getUsers(UserHelper.SORT_LASTNAME_FIRSTNAME));
    */
                DynaClass subjectClass = new BasicDynaClass("subject", null,
                    new DynaProperty[] {
                        new DynaProperty("value", String.class),
                        new DynaProperty("text", String.class)
                    });

                subjects = new ArrayList(subjectKeys.size());
                subjectUsers = new ArrayList(subjectKeys.size());

                // u�ytkownik�w nie dodaj� do subjectBeans, bo najpierw trzeba
                // ich posortowa�

                try
                {
                    for (Iterator iter=subjectKeys.iterator(); iter.hasNext(); )
                    {
                        SubjectKey subjectKey = (SubjectKey) iter.next();
                        DynaBean subjectBean = subjectClass.newInstance();
                        subjectBean.set("value", subjectKey.toString());

                        if (subjectKey.getType().equals(ObjectPermission.USER))
                        {
                            try
                            {
                                // nie �api� wyj�tku bazowego - EdmException
                                DSUser user = DSUser.findByUsername(subjectKey.getName());
                                subjectUsers.add(user);
                                //subjectBean.set("text", user.asFirstnameLastnameName());
                            }
                            catch (UserNotFoundException e)
                            {
                                log.warn("", e);
                                // zabezpieczenie przed umieszczaniem tego u�ytkownika
                                // na li�cie - powoduje b��dy javascriptu
                                subjectPermissions.remove(subjectKey.toString());
                                continue;
                            }
                        }
                        else if (subjectKey.getType().equals(ObjectPermission.GROUP))
                        {
                            try
                            {
                                DSDivision division = DSDivision.find(subjectKey.getName());
                                subjectBean.set("text", division.getName());
                            }
                            catch (DivisionNotFoundException e)
                            {
                                log.warn("", e);
                                subjectPermissions.remove(subjectKey.toString());
                                continue;
                            }

                            subjects.add(subjectBean);
                        }
                        else if (subjectKey.getType().equals(ObjectPermission.ANY))
                        {
                            subjectBean.set("text", sm.getString("Wszyscy"));

                            subjects.add(subjectBean);
                        }
                        else
                        {
                            throw new IllegalArgumentException(sm.getString("TypAktoraNieJestObslugiwany",subjectKey.getType()));
                        }
                    }

                    if (subjectUsers.size() > 0)
                    {
                        DSUser[] _users = (DSUser[]) subjectUsers.toArray(new DSUser[subjectUsers.size()]);
                        UserFactory.sortUsers(_users, DSUser.SORT_LASTNAME_FIRSTNAME, true);

                        //for (Iterator iter=subjectUsers.iterator(); iter.hasNext(); )
                        for (int i=0; i < _users.length; i++)
                        {
                            DSUser user = _users[i];
                            DynaBean subjectBean = subjectClass.newInstance();
                            subjectBean.set("value", user.getName()+"/user");
                            subjectBean.set("text", user.asFirstnameLastnameName());
                            subjects.add(subjectBean);
                        }
                    }
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException(e.getMessage(), e);
                }
                catch (InstantiationException e)
                {
                    throw new RuntimeException(e.getMessage(), e);
                }

                DSApi.context().setAttribute("subjects", subjects);

                // tworz� tablic� dost�pnych uprawnie� (beany zawieraj�ce
                // atrybuty name i description)
                DynaClass permissionClass = new BasicDynaClass("permission", null,
                    new DynaProperty[] {
                        new DynaProperty("name", String.class),
                        new DynaProperty("description", String.class)
                    });

                //DynaBean[] availablePermissions = new DynaBean[4];
                availablePermissions = new LinkedList();
                DynaBean bean;

                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.READ);
                bean.set("description", sm.getString("Odczyt"));
                availablePermissions.add(bean);

                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.MODIFY);
                bean.set("description", sm.getString("Modyfikacja"));
                availablePermissions.add(bean);

                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.DELETE);
                bean.set("description", sm.getString("Usuwanie"));
                availablePermissions.add(bean);

                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.READ_ATTACHMENTS);
                bean.set("description", sm.getString("OdczytZalacznikow"));
                availablePermissions.add(bean);

                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.MODIFY_ATTACHMENTS);
                bean.set("description", sm.getString("ModyfikacjaZalacznikow"));
                availablePermissions.add(bean);

                if (clazz == Folder.class)
                {
                    bean = new BasicDynaBean(permissionClass);
                    bean.set("name", ObjectPermission.CREATE);
                    bean.set("description", sm.getString("Tworzenie"));
                    availablePermissions.add(bean);
                }

                //DSApi.context().setAttribute("availablePermissions", availablePermissions);

                //DSApi.context().setAttribute("pickSubjectLink",
                    //mapping.findForward("security/pick-subject").getPath());
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
    
    private class SubjectKey
    {
        private String name;
        private String type;

        public SubjectKey(String name, String type)
        {
            this.name = name;
            this.type = type;
        }

        public String getName()
        {
            return name;
        }

        public String getType()
        {
            return type;
        }

        public String toString()
        {
            return name+"/"+type;
        }

        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof SubjectKey)) return false;

            final SubjectKey subjectKey = (SubjectKey) o;

            if (name != null ? !name.equals(subjectKey.name) : subjectKey.name != null) return false;
            if (type != null ? !type.equals(subjectKey.type) : subjectKey.type != null) return false;

            return true;
        }

        public int hashCode()
        {
            int result;
            result = (name != null ? name.hashCode() : 0);
            result = 29 * result + (type != null ? type.hashCode() : 0);
            return result;
        }
    }

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public List getSubjects() {
		return subjects;
	}

	public void setSubjects(List subjects) {
		this.subjects = subjects;
	}

	public List getSubjectUsers() {
		return subjectUsers;
	}

	public void setSubjectUsers(List subjectUsers) {
		this.subjectUsers = subjectUsers;
	}

	public List getPermissions() {
		return permissions;
	}

	public void setPermissions(List permissions) {
		this.permissions = permissions;
	}

	public List getSubjectKeys() {
		return subjectKeys;
	}

	public void setSubjectKeys(List subjectKeys) {
		this.subjectKeys = subjectKeys;
	}

	public Map getSubjectPermissions() {
		return subjectPermissions;
	}

	public void setSubjectPermissions(Map subjectPermissions) {
		this.subjectPermissions = subjectPermissions;
	}

	public List getSubjectPerms() {
		return subjectPerms;
	}

	public void setSubjectPerms(List subjectPerms) {
		this.subjectPerms = subjectPerms;
	}

	public List getAvailablePermissions() {
		return availablePermissions;
	}

	public void setAvailablePermissions(List availablePermissions) {
		this.availablePermissions = availablePermissions;
	}
}
