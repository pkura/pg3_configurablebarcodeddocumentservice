package pl.compan.docusafe.web.archive.security;

import org.apache.commons.beanutils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.security.SecurityHelper;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SetPermissionsAction.java,v 1.13 2007/09/13 11:37:37 mariuszk Exp $
 */
public class SetPermissionsAction extends EventDrivenAction {
    private final static StringManager sm =
            GlobalPreferences.loadPropertiesFile(SetPermissionsAction.class.getPackage().getName(), null);

    private static final Logger log = LoggerFactory.getLogger(SetPermissionsAction.class);

    private static final String[] eventNames = new String[]{
            "doDefault", "doUpdate"
    };

    static {
        Arrays.sort(eventNames);
    }

    public String[] getEventNames() {
        return eventNames;
    }

    public static final String FORWARD = "security/set-permissions";

    public ActionForward doDefault(
            ActionMapping mapping,
            DynaActionForm form,
            String eventParameter,
            HttpServletRequest request,
            HttpServletResponse response,
            Messages errors,
            Messages messages) {
        fillForm(request, form, mapping, errors);
        return mapping.findForward("main");
    }

    public ActionForward doUpdate(
            ActionMapping mapping,
            DynaActionForm form,
            String eventParameter,
            HttpServletRequest request,
            HttpServletResponse response,
            Messages errors,
            Messages messages) throws EdmException {
        String objectType = (String) form.get("objectType");
        Long objectId = (Long) form.get("objectId");
        String permissionsString = (String) form.get("permissionsString");

        Class clazz = getObjectTypeClass(errors, objectType);

        if (errors.size() > 0 || clazz == null)
            return mapping.findForward("main");

        try
        {
            DSApi.open(AuthUtil.getSubject(request));
            DSApi.context().begin();

            SecurityHelper.applyObjectPermissions(objectId, permissionsString, clazz);

            DSApi.context().commit();
        }catch(Exception e){
            log.error("", e);
            errors.add(e.getMessage());
            DSApi.context().setRollbackOnly();
        } finally{
            DSApi._close();
        }

        fillForm(request, form, mapping, errors);

        request.setAttribute("updatedPermissions", Boolean.TRUE);
        return mapping.findForward("main");
    }

    private Class getObjectTypeClass(Messages errors, String objectType) {
        Class clazz;
        if ("document".equals(objectType)) {
            clazz = Document.class;
        } else if ("folder".equals(objectType)) {
            clazz = Folder.class;
        } else {
            errors.add(sm.getString("NieprawidlowaWartoscParametruObjectType"));
            clazz = null;
        }
        return clazz;
    }


    private class SubjectKey {
        private String name;
        private String type;

        public SubjectKey(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String toString() {
            return name + "/" + type;
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SubjectKey)) return false;

            final SubjectKey subjectKey = (SubjectKey) o;

            if (name != null ? !name.equals(subjectKey.name) : subjectKey.name != null) return false;
            if (type != null ? !type.equals(subjectKey.type) : subjectKey.type != null) return false;

            return true;
        }

        public int hashCode() {
            int result;
            result = (name != null ? name.hashCode() : 0);
            result = 29 * result + (type != null ? type.hashCode() : 0);
            return result;
        }
    }

    private void fillForm(HttpServletRequest request, DynaActionForm form,
                          ActionMapping mapping, Messages errors) {
        String objectType = (String) form.get("objectType");
        Long objectId = (Long) form.get("objectId");

        if (objectType == null || objectId == null)
            errors.add(sm.getString("BrakParametruObjectTypeLubObjectId"));

        Class clazz;

        if ("document".equals(objectType)) {
            clazz = Document.class;
        } else if ("folder".equals(objectType)) {
            clazz = Folder.class;
        } else {
            errors.add(sm.getString("NieprawidlowaWartoscParametruObjectType"));
            return;
        }

        if (errors.size() > 0)
            return;

        DSContext ctx = null;
        try {
            ctx = DSApi.open(AuthUtil.getSubject(request));


            List permissions = ObjectPermission.find(clazz, objectId);


            // istniej�ce uprawnienia dla bie��cego obiektu
            List subjectKeys = new LinkedList();
            Map subjectPermissions = new HashMap();

            //for (int i=0, n=perms.length; i < n; i++)
            for (Iterator iter = permissions.iterator(); iter.hasNext(); ) {
                ObjectPermission permission = (ObjectPermission) iter.next();

                SubjectKey subjectKey = new SubjectKey(permission.getSubject(), permission.getSubjectType());

                List subjectPerms = (List) subjectPermissions.get(subjectKey.toString());
                if (subjectPerms == null) {
                    subjectPerms = new ArrayList(4);
                    subjectPermissions.put(subjectKey.toString(), subjectPerms);
                }
                subjectPerms.add(permission.getName());

                if (!subjectKeys.contains(subjectKey))
                    subjectKeys.add(subjectKey);
            }

            // nazwy u�ytkownik�w, kt�rzy maj� jakie� uprawnienia
            request.setAttribute("subjectPermissions", subjectPermissions);

/*
            request.setAttribute("users", uh.getUsers(UserHelper.SORT_LASTNAME_FIRSTNAME));
*/
            DynaClass subjectClass = new BasicDynaClass("subject", null,
                    new DynaProperty[]{
                            new DynaProperty("value", String.class),
                            new DynaProperty("text", String.class)
                    });

            List subjects = new ArrayList(subjectKeys.size());
            List subjectUsers = new ArrayList(subjectKeys.size());

            // u�ytkownik�w nie dodaj� do subjectBeans, bo najpierw trzeba
            // ich posortowa�

            try {
                for (Iterator iter = subjectKeys.iterator(); iter.hasNext(); ) {
                    SubjectKey subjectKey = (SubjectKey) iter.next();
                    DynaBean subjectBean = subjectClass.newInstance();
                    subjectBean.set("value", subjectKey.toString());

                    if (subjectKey.getType().equals(ObjectPermission.USER)) {
                        try {
                            // nie �api� wyj�tku bazowego - EdmException
                            DSUser user = DSUser.findByUsername(subjectKey.getName());
                            subjectUsers.add(user);
                            //subjectBean.set("text", user.asFirstnameLastnameName());
                        } catch (UserNotFoundException e) {
                            log.warn("", e);
                            // zabezpieczenie przed umieszczaniem tego u�ytkownika
                            // na li�cie - powoduje b��dy javascriptu
                            subjectPermissions.remove(subjectKey.toString());
                            continue;
                        }
                    } else if (subjectKey.getType().equals(ObjectPermission.GROUP)) {
                        try {
                            DSDivision division = DSDivision.find(subjectKey.getName());
                            subjectBean.set("text", division.getName());
                        } catch (DivisionNotFoundException e) {
                            log.warn("", e);
                            subjectPermissions.remove(subjectKey.toString());
                            continue;
                        }

                        subjects.add(subjectBean);
                    } else if (subjectKey.getType().equals(ObjectPermission.ANY)) {
                        subjectBean.set("text", sm.getString("Wszyscy"));

                        subjects.add(subjectBean);
                    } else {
                        throw new IllegalArgumentException(sm.getString("TypAktoraNieJestObslugiwany", subjectKey.getType()));
                    }
                }

                if (subjectUsers.size() > 0) {
                    DSUser[] _users = (DSUser[]) subjectUsers.toArray(new DSUser[subjectUsers.size()]);
                    UserFactory.sortUsers(_users, DSUser.SORT_LASTNAME_FIRSTNAME, true);

                    //for (Iterator iter=subjectUsers.iterator(); iter.hasNext(); )
                    for (int i = 0; i < _users.length; i++) {
                        DSUser user = _users[i];
                        DynaBean subjectBean = subjectClass.newInstance();
                        subjectBean.set("value", user.getName() + "/user");
                        subjectBean.set("text", user.asFirstnameLastnameName());
                        subjects.add(subjectBean);
                    }
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getMessage(), e);
            } catch (InstantiationException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            request.setAttribute("subjects", subjects);

            // tworz� tablic� dost�pnych uprawnie� (beany zawieraj�ce
            // atrybuty name i description)
            DynaClass permissionClass = new BasicDynaClass("permission", null,
                    new DynaProperty[]{
                            new DynaProperty("name", String.class),
                            new DynaProperty("description", String.class)
                    });

            //DynaBean[] availablePermissions = new DynaBean[4];
            List availablePermissions = new LinkedList();
            DynaBean bean;

            bean = new BasicDynaBean(permissionClass);
            bean.set("name", ObjectPermission.READ);
            bean.set("description", sm.getString("Odczyt"));
            availablePermissions.add(bean);

            bean = new BasicDynaBean(permissionClass);
            bean.set("name", ObjectPermission.MODIFY);
            bean.set("description", sm.getString("Modyfikacja"));
            availablePermissions.add(bean);

            bean = new BasicDynaBean(permissionClass);
            bean.set("name", ObjectPermission.DELETE);
            bean.set("description", sm.getString("Usuwanie"));
            availablePermissions.add(bean);

            bean = new BasicDynaBean(permissionClass);
            bean.set("name", ObjectPermission.READ_ATTACHMENTS);
            bean.set("description", sm.getString("OdczytZalacznikow"));
            availablePermissions.add(bean);

            bean = new BasicDynaBean(permissionClass);
            bean.set("name", ObjectPermission.MODIFY_ATTACHMENTS);
            bean.set("description", sm.getString("ModyfikacjaZalacznikow"));
            availablePermissions.add(bean);

            if (AvailabilityManager.isAvailable("perimission.READ_ORGINAL_ATTACHMENTS")) {
                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.READ_ORGINAL_ATTACHMENTS);
                bean.set("description", sm.getString("OdczytOrginaluZalacznika"));
                availablePermissions.add(bean);
            }

            if (clazz == Folder.class) {
                bean = new BasicDynaBean(permissionClass);
                bean.set("name", ObjectPermission.CREATE);
                bean.set("description", sm.getString("Tworzenie"));
                availablePermissions.add(bean);
            }

            request.setAttribute("availablePermissions", availablePermissions);

            request.setAttribute("pickSubjectLink",
                    mapping.findForward("security/pick-subject").getPath());
        } catch (EdmException e) {
            errors.add(e.getMessage());
        } finally {
            DSApi._close();
        }
    }

    public static String getLink(HttpServletRequest request, String objectType, Long objectId) {
/*
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
*/
        return "/security/set-permissions.do?objectType=" + objectType + "&objectId=" + objectId;
    }
}
