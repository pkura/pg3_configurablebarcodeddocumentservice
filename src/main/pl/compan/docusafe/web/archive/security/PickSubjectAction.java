package pl.compan.docusafe.web.archive.security;

import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * Wyświetla okienko z listą dostępnych użytkowników i grup.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PickSubjectAction.java,v 1.12 2008/10/06 11:01:10 pecet4 Exp $
 */
public class PickSubjectAction extends pl.compan.docusafe.web.common.EventDrivenAction
{
	private static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(PickSubjectAction.class.getPackage().getName(),null);
        
    private static final Log log = LogFactory.getLog(PickSubjectAction.class);
    private Boolean backup ;
    private static final String[] eventNames = new String[] {
        "doDefault"
    };
    static {
        Arrays.sort(eventNames);
    }
    public String[] getEventNames()
    {
        return eventNames;
    }

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        pl.compan.docusafe.web.common.Messages errors,
        Messages messages)
    {
        String[] constraints = (String[]) form.get("constraints");

        boolean hasConstraints = constraints != null && constraints.length > 0;
        backup = (Boolean) form.get("backup");
        request.setAttribute("backup", backup);
        if (hasConstraints)
            Arrays.sort(constraints);

        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(request));

            DynaClass subjectClass = new BasicDynaClass("subject", null,
                new DynaProperty[] {
                    new DynaProperty("value", String.class),
                    new DynaProperty("text", String.class)
                });

            List subjects = new LinkedList();

            try
            {
                DynaBean allSubjects = subjectClass.newInstance();

                // specjalny aktor: Wszyscy
                if (!hasConstraints ||
                    (hasConstraints && Arrays.binarySearch(constraints, "special_groups") >= 0))
                {
                    allSubjects.set("value", "*/"+ObjectPermission.ANY);
                    allSubjects.set("text", sm.getString("Wszyscy"));
                    subjects.add(allSubjects);
                }

                // użytkownicy
                if (!hasConstraints ||
                    (hasConstraints && Arrays.binarySearch(constraints, "users") >= 0))
                {
                    DSUser[] users = (DSUser[]) DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME).toArray(new DSUser[0]);

                    for (int i=0, n=users.length; i < n; i++)
                    {
                        DynaBean userBean = subjectClass.newInstance();
                        userBean.set("value", users[i].getName()+"/"+ObjectPermission.USER);
                        userBean.set("text", users[i].asLastnameFirstnameName());

                        subjects.add(userBean);
                    }
                }

                // grupy
                if (!hasConstraints ||
                    (hasConstraints && Arrays.binarySearch(constraints, "groups") >= 0))
                {
                    DSDivision[] groups = UserFactory.getInstance().getGroups();

                    for (int i=0, n=groups.length; i < n; i++)
                    {
                        DynaBean groupBean = subjectClass.newInstance();
                        groupBean.set("value", groups[i].getGuid()+"/"+ObjectPermission.GROUP);
                        groupBean.set("text", groups[i].getName());

                        subjects.add(groupBean);
                    }
                }

                request.setAttribute("subjects", subjects);
            }
            catch (IllegalAccessException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
            catch (InstantiationException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }


        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
        }
        finally
        {
            DSApi._close();
        }

        return mapping.findForward("main");
    }

	public Boolean getBackup() {
		return backup;
	}

	public void setBackup(Boolean backup) {
		this.backup = backup;
	}

}
