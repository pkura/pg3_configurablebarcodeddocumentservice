package pl.compan.docusafe.web.archive.init;

import ognl.OgnlContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.ExtendedProperties;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.upload.FormFile;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.common.license.License;
import pl.compan.docusafe.common.license.LicenseVerificationException;
import pl.compan.docusafe.common.sql.DDL;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.OgnlExpression;
import pl.compan.docusafe.util.ShUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Pocz�tkowa konfiguracja aplikacji. Akcja tworzy katalog
 * domowy, a w nim pliki konfiguracyjne aplikacji.
 * <p>
 * Akcja nie powinna umo�liwia� usuwania jakichkolwiek danych:
 * je�eli w podanej bazie danych s� juz jakie� tabele, nie
 * powinny by� usuwane, je�eli w podanym katalogu domowym s�
 * ju� pliki - nie powinny by� nadpisywane etc.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InitialConfigurationAction.java,v 1.60 2010/05/06 14:23:58 mariuszk Exp $
 */
public class InitialConfigurationAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(InitialConfigurationAction.class);
    private String version = "352";
    private boolean getVersion = true;

    /**
     * Has�o "admin" zakodowane przy pomocy SHA i base64.
     * @see UserFactory#hashPassword(String) 
     */
    public static final String HASHED_ADMIN = "{SHA}0DPiKuNIrrVmD8IUCuw1hQxNqZc=";

    private static final String[] eventNames = new String[] {
        "doDefault", "doStart", "doUseExistingHome", "doSetDatabase",
        "doUseExistingDb", "doChooseAnotherDb", "doSetLdap",
        "doUseExistingLdap", "doChooseAnotherLdap", "doSave","doUseExistingAndUpadateDb"
    };
    static
    {
        Arrays.sort(eventNames);
    }

    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public String[] getEventNames()
    {
        return eventNames;
    }

    /**
     * Ekran pocz�tkowy.
     */
    public ActionForward doDefault(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        return mapping.findForward("page1");
    }

    /**
     * Wyb�r katalogu domowego. Katalog podany przez u�ytkownika
     * mo�e ju� istnie�, b�d� nie.
     */
    public ActionForward doStart(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        FormFile licenseFile = (FormFile) form.get("licenseFile");
        InputStream is = null;

        if (licenseFile != null && licenseFile.getFileSize() > 0)
        {
            try
            {
                is = licenseFile.getInputStream();
            }
            catch (IOException e)
            {
                errors.add("Nie mo�na otworzy� pliku z licencj�");
            }
        }
        else
        {
            errors.add("Nie wybrano pliku z licencj� lub plik ma zerow� d�ugo��");
        }

        if (errors.size() > 0)
            return mapping.findForward("page1");

        try
        {
            License license = License.load(is);
            form.set("licenseBytes", new String(Base64.encodeBase64(license.getLicenseBytes())));
        }
        catch (LicenseVerificationException e)
        {
            errors.add("Nieprawid�owa licencja ("+e.getMessage()+")");
            log.error(e.getMessage(), e);
            return mapping.findForward("page1");
        }

        return mapping.findForward("page2");
    }

    /**
     * Potwierdzenie ch�ci u�ycia istniej�cego katalogu domowego.
     */
/*
    public ActionForward doUseExistingHome(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String home = (String) form.get("home");

        // je�eli nie podano nazwy katalogu, komunikat b��du
        if (home == null || home.trim().length() == 0)
        {
            errors.add(sm.getString("initialize.missingHome"));
            return mapping.findForward("page1");
        }

        // pr�ba skonfigurowania aplikacji przy pomocy plik�w z przekazanego
        // katalogu
        File homeDirectory = new File(home);
        try
        {
            Configuration.configure();

            //Configuration.saveConfiguration(homeDirectory);
            //getServlet().getServletContext().setAttribute(Configuration.CONFIGURED_KEY, Boolean.TRUE);

            return new ActionForward(mapping.findForward("home").getPath(), true);
        }
        catch (EdmException e)
        {
            errors.add(sm.getString("initialize.invalidExistingHome", homeDirectory.getAbsolutePath(),
                e.getMessage()));
            return mapping.findForward("page1");
        }
    }
*/

    /**
     * Konfiguracja bazy danych.
     */
    public ActionForward doSetDatabase(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String dbBrand = (String) form.get("dbBrand");
        String dbUrl = (String) form.get("dbUrl");
        String dbUsername = (String) form.get("dbUsername");
        String dbPassword = (String) form.get("dbPassword");

        if (StringUtils.isEmpty(dbUrl))
            errors.add("Nie podano warto�ci urla JDBC.");
        if (StringUtils.isEmpty(dbUsername))
            errors.add("Nie podano nazwy u�ytkownika.");
        if (Docusafe.getHibernateDialectClass(dbBrand) == null)
            errors.add("Wybrano nieznany rodzaj bazy danych: "+dbBrand);
        if (errors.size() > 0)
        {
            return mapping.findForward("page2");
        }

        BasicDataSource ds = null;
        Connection conn = null;
        try
        {
/*
            Context initCtx = new InitialContext();
            Context ctx = (Context) initCtx.lookup("java:comp/env");
            Object _ds = ctx.lookup(dataSourceName);
            if (_ds == null)
                throw new EdmException(sm.getString("initialize.noDataSource"));
            if (!(_ds instanceof DataSource))
                throw new EdmException(sm.getString("initialize.notDataSourceInstance"));
            DataSource ds = (DataSource) _ds;
*/

/*
            NamingEnumeration enum = ctx.list("jdbc");
            while (enum.hasMore())
            {
                NameClassPair pair = (NameClassPair) enum.next();
            }
*/
/*
            ds = new BasicDataSource();
            ds.setDriverClassName(Configuration.getDriverClass(dbBrand));
            ds.setUsername(dbUsername);
            ds.setPassword(dbPassword != null ? dbPassword : "");
            ds.setUrl(dbUrl);
*/
            //ds = Configuration.createDataSource(dbBrand, dbUrl, dbUsername, dbPassword);
            ds = new BasicDataSource();
            ds.setDriverClassName(Docusafe.getDefaultDriver(dbBrand));
            ds.setUrl(dbUrl);
            ds.setUsername(dbUsername);
            ds.setPassword(dbPassword);

            //ds.setUrl("jdbc:firebirdsql:"+dbHost+":"+dbFile+"?lc_ctype=UNICODE_FSS");

            // testowanie bazy danych (czy s� w niej ju� tabele)
            conn = ds.getConnection();
            List tableNames = new LinkedList();
            ResultSet rs = conn.getMetaData().getTables(null, null, null, null);
            while (rs.next())
            {
                String tableType = rs.getString("TABLE_TYPE");
                String tableSchem = rs.getString("TABLE_SCHEM");

                // nie bior� pod uwag� tabel systemowych, synonim�w Oracle
                // i tabel w schematach systemowych Oracle
                if ("SYSTEM TABLE".equals(tableType) ||
                    (Configuration.ORACLE_9.equals(dbBrand) &&
                     ("SYNONYM".equals(tableType) ||
                      "CTXSYS".equals(tableSchem) ||
                      "MDSYS".equals(tableSchem) ||
                      "SYS".equals(tableSchem) ||
                      "SYSTEM".equals(tableSchem) ||
                      "WMSYS".equals(tableSchem) ||
                      "XDB".equals(tableSchem))))
                    continue;

                StringBuilder name = new StringBuilder();
                if (rs.getString("TABLE_CAT") != null)
                    name.append(rs.getString("TABLE_CAT")+".");
                if (tableSchem != null)
                    name.append(tableSchem+".");
                name.append(rs.getString("TABLE_NAME"));

                if (!"TABLE".equals(tableType) && !"VIEW".equals(tableType))
                {
                    name.append(" ("+tableType+")");
                }

                tableNames.add(name);
            }

            // je�eli w bazie s� ju� tabele, upewniam si� czy
            // u�y� tej bazy
            if (tableNames.size() > 0)
            {
                request.setAttribute("tableNames", tableNames);
                return mapping.findForward("existing-db");
            }
            else
            {
                form.set("createTables", Boolean.TRUE);
            }

        }
/*
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            return mapping.findForward("page2");
        }
*/
        catch (SQLException e)
        {
            errors.add(sm.getString("init.dbAccessError", e.getMessage()));
            return mapping.findForward("page2");
        }
        finally
        {
            if (conn != null) try { conn.close(); } catch (Exception e) { }
            if (ds != null) try { ds.close(); } catch (Exception e) { }
        }
/*
        catch (NamingException e)
        {
            errors.add(sm.getString("initialize.getDataSourceFailed", e.getMessage()));
            return mapping.findForward("page2");
        }
*/

        if ("ldap".equals((String) form.get("userFactory")))
        {
            return mapping.findForward("page3");
        }
        else
        {
            return mapping.findForward("save");
        }
    }

    /**
     * Wyb�r istniej�cej bazy danych.
     */
    public ActionForward doUseExistingDb(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        if ("ldap".equals((String) form.get("userFactory")))
        {
            return mapping.findForward("page3");
        }
        else
        {
            return mapping.findForward("save");
        }
    }

    /**
     * Wyb�r istniej�cej bazy danych i updataowanie jej
     */
   public ActionForward doUseExistingAndUpadateDb(
       ActionMapping mapping,
       DynaActionForm form,
       String eventParameter,
       HttpServletRequest request,
       HttpServletResponse response,
       Messages errors,
       Messages messages)
   {
       if ("ldap".equals((String) form.get("userFactory")))
       {
           return mapping.findForward("page3");
       }
       else
       {
           form.set("createTables", Boolean.TRUE);
           return mapping.findForward("save");
       }
   }

    
    /**
     * Powr�t do wyboru bazy danych.
     */
    public ActionForward doChooseAnotherDb(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        return mapping.findForward("page2");
    }



    public ActionForward doUseExistingLdap(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        form.set("useExistingLdap", Boolean.TRUE);
        // kolejna strona konfiguracji
        return mapping.findForward("save");
    }

    /**
     * Powr�t do wyboru bazy danych.
     */
    public ActionForward doChooseAnotherLdap(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        return mapping.findForward("page3");
    }

    /**
     * Zapisanie konfiguracji, utworzenie bazy danych i struktury LDAP.
     */
    public ActionForward doSave(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String dbBrand = (String) form.get("dbBrand");

        //if (Configuration.getDriverClass(dbBrand) == null)
        if (Docusafe.getHibernateDialectClass(dbBrand) == null)
        {
            errors.add("Nieznany typ bazy danych: "+dbBrand);
            return mapping.findForward("save");
        }

        String rwaType = (String) form.get("rwa");

        // pr�ba utworzenia katalogu domowego i niezb�dnych podkatalog�w
        File homeDirectory = Docusafe.getHome();
        if (!homeDirectory.exists() && !homeDirectory.mkdirs())
        {
            errors.add(sm.getString("init.cannotMkdirs"));
            return mapping.findForward("save");
        }

/*
        File logsDirectory = new File(homeDirectory, Configuration.SUBDIR_LOGS);
        if (!logsDirectory.exists() && !logsDirectory.mkdirs())
        {
            errors.add(sm.getString("init.cannotMkdirs"));
            return mapping.findForward("save");
        }
*/

        File mailDirectory = new File(homeDirectory, Configuration.SUBDIR_MAIL);
        if (!mailDirectory.exists() && !mailDirectory.mkdirs())
        {
            errors.add(sm.getString("init.cannotMkdirs"));
            return mapping.findForward("save");
        }

        if (errors.size() > 0)
            return mapping.findForward("save");

        String userFactory = (String) form.get("userFactory");

        // tworzenie pliku z licencj�

        File licenseFile = new File(homeDirectory, "license.class");
        OutputStream os = null;
        try
        {
            os = new FileOutputStream(licenseFile);
            byte[] licenseBytes = Base64.decodeBase64(((String) form.get("licenseBytes")).getBytes("iso-8859-1"));
            os.write(licenseBytes);
        }
        catch (IOException e)
        {
            errors.add(e.getMessage());
        }
        finally
        {
            if (os != null) try { os.close(); } catch (Exception e) { }
        }

        // fonty

        File fontsDir = new File(homeDirectory, "fonts");
        File[] fonts = new File[] {
            new File(fontsDir, "arial.ttf"),
            new File(fontsDir, "arialbd.ttf"),
            new File(fontsDir, "arialbi.ttf"),
            new File(fontsDir, "ariali.ttf"),
            new File(fontsDir, "arial.xml"),
            new File(fontsDir, "arialbd.xml"),
            new File(fontsDir, "arialbi.xml"),
            new File(fontsDir, "ariali.xml"),
        };

        try
        {
            fontsDir.mkdir();
            for (int i=0; i < fonts.length; i++)
            {
                ShUtils.createFile(getServlet().getServletContext().
                    getResourceAsStream("/WEB-INF/fonts/"+fonts[i].getName()),
                    fonts[i]);
                messages.add(sm.getString("init.createdFile", fonts[i].getName()));
            }
        }
        catch (IOException e)
        {
            errors.add(e.getMessage());
        }


        // docusafe.config

        File docusafeConfig = null;
        try
        {
            docusafeConfig = new File(homeDirectory, "docusafe.config");
            PrintWriter pw = new PrintWriter(new FileWriter(docusafeConfig));
            pw.println("# "+sm.getString("init.file.autoGeneratedFileComment"));
            pw.println("# "+new Date().toString());

            StringBuilder myUrl = new StringBuilder(50);
            myUrl.append("http");
            if (request.isSecure()) myUrl.append("s");
            myUrl.append("://");
            myUrl.append(request.getServerName());
            if ((!request.isSecure() && request.getServerPort() != 80) ||
                (request.isSecure() && request.getServerPort() != 443))
                myUrl.append(":"+request.getServerPort());
            myUrl.append(request.getContextPath());

            pw.println("# url bazowy aplikacji bez ko�cowego znaku '/' (np. http://com-pan.pl:1000)");
            pw.println("base.url = "+myUrl.toString());
            pw.println("user.factory = "+userFactory);

//            if (Configuration.coreOfficeAvailable())
//            {
//                pw.println("password.policy = "+GiodoPasswordPolicy.class.getName());
//            }
//            else
//            {
//                pw.println("password.policy = "+BasicPasswordPolicy.class.getName());
//            }

            pw.close();
        }
        catch (IOException e)
        {
            errors.add(sm.getString("init.unwritableFile",
                 docusafeConfig != null ? docusafeConfig.getAbsolutePath() : "<nieznany>"));
        }

        // TODO: ten plik jest niepotrzebny; zbada�, czy jaki� kod si� go spodziewa, poprawi� i wyrzuci�
        // mailing - plik konfiguracyjny

        File mailConfig = null;
        try
        {
            mailConfig = new File(homeDirectory, "mail.config");
            PrintWriter pw = new PrintWriter(new FileWriter(mailConfig));
            pw.println("# "+sm.getString("init.file.autoGeneratedFileComment"));
            pw.println("# "+new Date().toString());
            pw.println("mail.smtp.host = localhost");
            pw.println("mail.smtp.auth = false");
            pw.println("# mail.smtp.username = ");
            pw.println("# mail.smtp.password = ");
            pw.println("mail.docusafe.from.email = docusafe@localhost");
            pw.println("mail.docusafe.from.personal = Docusafe");
            pw.close();

            messages.add(sm.getString("init.createdFile", mailConfig.getName()));
        }
        catch (IOException e)
        {
            errors.add(sm.getString("init.unwritableFile",
                 mailConfig != null ? mailConfig.getAbsolutePath() : "<nieznany>"));
        }

        // szablony list�w

        try
        {
            for (int i=0; i < Mail.mails.length; i++)
            {
                File file = new File(mailDirectory, Mail.mails[i].getName());
                ShUtils.createFile(getServlet().getServletContext().
                    getResourceAsStream("/WEB-INF/mail/"+Mail.mails[i].getName()),
                    file);
                messages.add(sm.getString("init.createdFile", file.getName()));
            }
        }
        catch (IOException e)
        {
            errors.add(e.getMessage());
        }

        // LDAP - plik konfiguracyjny

        if ("ldap".equals(userFactory))
        {
            File ldapConfig = null;
            try
            {
                String url = "ldap://" + form.get("ldapServer");
                String name = (String) form.get("ldapName");
                String password = (String) form.get("ldapPassword");
                String root = (String) form.get("ldapRoot");

                ldapConfig = new File(homeDirectory, "ldap.config");
                PrintWriter pw = new PrintWriter(new FileWriter(ldapConfig));
                pw.println("# "+sm.getString("init.file.autoGeneratedFileComment"));
                pw.println("# "+new Date().toString());
                pw.println("url="+url);
                pw.println("name="+name);
                pw.println("password="+password);
                pw.println("root="+root);
                pw.close();

                messages.add(sm.getString("init.createdFile", ldapConfig.getName()));
            }
            catch (IOException e)
            {
                errors.add(sm.getString("init.unwritableFile",
                    ldapConfig != null ? ldapConfig.getAbsolutePath() : "<nieznany>"));
            }
        }

        // workflow - plik konfiguracyjny

        File wfConfig = null;
        try
        {
            wfConfig = new File(homeDirectory, "workflow.config");
            PrintWriter pw = new PrintWriter(new FileWriter(wfConfig));
            pw.println("# "+sm.getString("init.file.autoGeneratedFileComment"));
            pw.println("# "+new Date().toString());
            pw.println("shark.host=localhost");
            pw.println("shark.port=10123");
            pw.println("shark.server.name=SharkServer");
            pw.println("shark.admin.username=admin");
            pw.println("shark.admin.password=enhydra");
            pw.close();

            messages.add(sm.getString("init.createdFile", wfConfig.getName()));
        }
        catch (IOException e)
        {
            errors.add(sm.getString("init.unwritableFile",
                wfConfig != null ? wfConfig.getAbsolutePath() : "<nieznany>"));
        }

        // JAAS

        File loginConfig = null;
        try
        {
            loginConfig = new File(homeDirectory, "login.config");
            ShUtils.createFile(getServlet().getServletContext()
                .getResourceAsStream("/WEB-INF/configuration/login.config"),
                loginConfig);

            messages.add(sm.getString("init.createdFile", loginConfig.getName()));
        }
        catch (IOException e)
        {
            errors.add(sm.getString("init.unwritableFile", loginConfig.getAbsolutePath()));
        }

/*
        File edmPolicy = null;
        try
        {
            edmPolicy = new File(homeDirectory, "docusafe.policy");
            ShUtils.createFile(getServlet().getServletContext()
                .getResourceAsStream("/WEB-INF/configuration/docusafe.policy"),
                edmPolicy);

            messages.add(sm.getString("initialize.createdFile", edmPolicy.getName()));
        }
        catch (IOException e)
        {
            errors.add(sm.getString("initialize.unwritableFile", edmPolicy.getAbsolutePath()));
        }
*/

        // Log4j

        try
        {
            ShUtils.createFile(getServlet().getServletContext()
                .getResourceAsStream("/WEB-INF/configuration/log4j.properties.template"),
                new File(homeDirectory, "log4j.properties"));
        }
        catch (IOException e)
        {
            errors.add(sm.getString("init.unreadableTemplate", "log4j.properties.template",
                e.getMessage()));
        }

        //tu byl LDAP

        // tworzenie pliku konfiguracyjnego bazy danych

        String dbUrl = (String) form.get("dbUrl");
        String dbUsername = (String) form.get("dbUsername");
        String dbPassword = (String) form.get("dbPassword");
        try
        {

            File datasourceConfig = new File(homeDirectory, "datasource.config");
            PrintWriter pw = new PrintWriter(new FileWriter(datasourceConfig));
            pw.println("# "+sm.getString("init.file.autoGeneratedFileComment"));
            pw.println("# Domy�lny sterownik bazy danych");
            pw.println("# driver.class="+Docusafe.getDefaultDriver(dbBrand));
            pw.println("database.type="+dbBrand);
            pw.println("username="+dbUsername);
            pw.println("password="+dbPassword);
            pw.println("url="+dbUrl);
            pw.close();

            messages.add(sm.getString("init.createdFile", datasourceConfig.getName()));
        }
        catch (IOException e)
        {
            log.error("", e);
            errors.add(sm.getString("init.resourceReadError", e.getMessage()));
        }

        // tworzenie bazy danych

        if (form.get("createTables") != null && ((Boolean) form.get("createTables")).booleanValue())
        {

            Statement st = null;
            PreparedStatement ps = null;
            InputStream init;
            InputStream rwa;
            InputStream edg;
            InputStream edgClass;
            Connection conn = null;
            BasicDataSource ds = null;
            try
            {
                VelocityEngine ve = Docusafe.getVelocityEngine();
                VelocityContext vc = new VelocityContext();
                vc.put("ddl", DDL.getDDL(Docusafe.getHibernateDialectClass(dbBrand)));

                String dbSuffix;
                if (Docusafe.DB_FIREBIRD_1_5.equals(dbBrand) )
                    dbSuffix = "firebird";
                else if (Configuration.DB_SQLSERVER2000.equals(dbBrand))
                    dbSuffix = "mssql";
                else if(Docusafe.ORACLE_9.equals(dbBrand))
                    dbSuffix = "oracle";
                else
                    throw new EdmException("Nieznany rodzaj bazy danych: "+dbBrand);

                StringWriter tablesSw = new StringWriter(10000);
                Template tablesTemplate = ve.getTemplate("tables.vsql", "iso-8859-2");
                tablesTemplate.merge(vc, tablesSw);

                StringWriter officeSw = new StringWriter(25000);
                Template officeTemplate = ve.getTemplate("office.vsql", "iso-8859-2");
                officeTemplate.merge(vc, officeSw);

                init = getClass().getClassLoader().getResourceAsStream("init."+dbSuffix+".sql");
                if (init == null)
                    throw new EdmException(sm.getString("init.resourceNotFound", "init."+dbSuffix+".sql"));

                if ("um".equals(rwaType) || "sp".equals(rwaType))
                {

                    rwa = getClass().getClassLoader().getResourceAsStream("rwa-"+rwaType+"."+dbSuffix+".sql");
                    if (rwa == null)
                        throw new EdmException(sm.getString("init.resourceNotFound", "rwa."+dbSuffix+".sql"));
                }
                else
                {
                    rwa = null;
                }
                ds = new BasicDataSource();
                ds.setDriverClassName(Docusafe.getDefaultDriver(dbBrand));
                ds.setUrl(dbUrl);
                ds.setUsername(dbUsername);
                ds.setPassword(dbPassword);
                
                 // tworzenie bazy danych
                conn = ds.getConnection();

                executeSqlFile(new BufferedReader(new StringReader(tablesSw.toString())), conn);
                
                getVersion = false;
                
                executeSqlFile(new BufferedReader(new StringReader(officeSw.toString())), conn);

                if(init!=null)
                {
                    BufferedReader rinit = new BufferedReader(
                        new InputStreamReader(init, Charset.forName("ISO-8859-2")));
                    executeSqlFile(rinit,conn);
                    rinit.close();
                }
                if (rwa!=null)
                {
                    BufferedReader rrwa = new BufferedReader(
                        new InputStreamReader(rwa, Charset.forName("ISO-8859-2")));
                    executeSqlFile(rrwa,conn);
                    rrwa.close();
                }
                    executeSqlFile(new BufferedReader(
                        new InputStreamReader(
                            getClass().getClassLoader().getResourceAsStream("certificate."+dbSuffix+".sql"),
                            Charset.forName("ISO-8859-2"))),
                        conn);

 
                st = conn.createStatement();
                st.executeUpdate("INSERT INTO DS_DOCUSAFE (VAR_NAME, VAR_VALUE) "+
                    "VALUES ('"+ Upgrade.APP_VERSION+"', '" + version + "')");
                st.close();
                
                CreateNormalDocKind(conn,dbBrand);
                messages.add(sm.getString("init.createdDatabase"));
            }
            catch (IOException e)
            {
            	e.printStackTrace();
                log.error("", e);
                errors.add(sm.getString("init.resourceReadError", e.getMessage()));
            }
            catch (SQLException e)
            {
            	e.printStackTrace();
                log.error("", e);
                errors.add(sm.getString("init.dbConnectionError", e.getMessage()));
            }
            catch (EdmException e)
            {
            	e.printStackTrace();
                log.error("", e);
                errors.add(e.getMessage());
            }
            catch (Exception e)
            {
            	e.printStackTrace();
                log.error("", e);
                errors.add("B��d podczas tworzenia tabel w bazie danych !("+e.getMessage()+"))");
            }
            catch (Throwable t) {
            	t.printStackTrace();
				// TODO: handle exception
			}
            finally
            {
                if (st != null) try { st.close(); } catch (Throwable t) { }
                if (ps != null) try { ps.close(); } catch (Throwable t) { }
                if (conn != null) try { conn.close(); } catch (Throwable t) { }
                if (ds != null) try { ds.close(); } catch (Throwable t) { }
            }
        }

        if (errors.size() > 0)
        {
            log.error("Wyst�pi�y b��dy podczas inicjalizacji aplikacji");
            return mapping.findForward("save");
        }


        // konfiguracja aplikacji

/*
        try
        {
            Configuration.homeInit();
        }
        catch (EdmException e)
        {
            log.error("", e);
            errors.add(e.getMessage());
            return mapping.findForward("save");
        }
*/

        Docusafe.resumeBoot();

        return mapping.findForward("done");
    }

    private void executeSqlFile(BufferedReader reader, Connection conn)
        throws IOException, SQLException
    {
        //Zapisanie skryptow sql do plikow
        
        File sql = new File(Docusafe.getHome(), "SqlCreateDataBase.txt");
        PrintWriter pw2 = new PrintWriter(new FileWriter(sql,true),true);
        Statement st = null;
        try
        {
            st = conn.createStatement();
            
            SqlFileReader initSql = new SqlFileReader(reader);
            
            if(getVersion){
            version = initSql.peekVsqlVersion();
            }

            while (initSql.hasNextStatement())
            {
            	
                String stmt = initSql.nextStatement();
                pw2.println(stmt+";"); 
                try
                {
                	st.executeUpdate(stmt);
                }
                catch (SQLException e)
                {
                	e.printStackTrace();
                	throw e;
                }
                pw2.println("---");
            }
            
            st.close();
            pw2.close();
        }
        finally
        {
            if (st != null) try { st.close(); } catch (Exception e) { }
        }
    }

    /**
     * Klasa dostarczaj�ca szablony Velocity. Przed jej u�yciem
     * nale�y zainicjalizowa� statyczne pole {@link #servletContext}.
     */
    public static class SimpleServletContextResourceLoader extends ResourceLoader
    {
        static ServletContext servletContext;

        public void init(ExtendedProperties extendedProperties)
        {
        }

        public InputStream getResourceStream(String s) throws ResourceNotFoundException
        {
            if (s == null || s.length() == 0)
                throw new ResourceNotFoundException("empty name");
            InputStream is = servletContext.getResourceAsStream(s);
            if (is == null)
                throw new ResourceNotFoundException(s);
            return is;
        }

        public boolean isSourceModified(Resource resource)
        {
            return false;
        }

        public long getLastModified(Resource resource)
        {
            return 0;
        }
    }

    /**
     * Klasa odczytuj�ca plik zawieraj�cy polecenia SQL rozdzielone
     * �rednikami. Linie rozpoczynaj�ce si� od '--' s� ignorowane.
     */
    public static class SqlFileReader
    {
        private BufferedReader reader;
        private String nextStatement;

        public SqlFileReader(Reader reader)
        {
            this.reader = new BufferedReader(reader);
        }

        /**
         * Zwraca true, je�eli na odczytanie oczekuje kolejne polecenie SQL.
         */
        public boolean hasNextStatement() throws IOException
        {
            return peekNextStatement() != null;
        }

        /**
         * Zwraca kolejne polecenie SQL lub null, je�eli funkcja hasNextStatement()
         * zwraca false.
         */
        public String nextStatement() throws IOException
        {
            return clearCurrentStatement();
        }

        /**
         * Zwraca oczekuj�ce polecenie SQL i nadaje przechowuj�cej je zmiennej
         * warto�� null, aby kolejne wywo�anie odczyta�o kolejne polecenie SQL.
         */
        private String clearCurrentStatement() throws IOException
        {
            peekNextStatement();
            String st = nextStatement;
            nextStatement = null;
            return substituteVariables(st);
        }

        /**
         * Odczytuje kolejne polecenie SQL z obiektu Reader. Je�eli w zmiennej
         * nextStatement znajduje si� ju� odczytane uprzednio polecenie, jest
         * ono zwracane bez odczytywania danych z readera.
         */
        private String peekNextStatement() throws IOException
        {
            if (nextStatement != null)
                return nextStatement;

            String line;
            StringBuilder sql = new StringBuilder(300);

            while ((line = reader.readLine()) != null)
            {
                line = line.trim();

                if (line.length() == 0 || line.startsWith("--"))
                    continue;

                sql.append(" ");
                if (line.endsWith("\\;"))
                {
                    sql.append(line.substring(0, line.length()-2));
                    sql.append(";");
                }
                else if (line.endsWith(";"))
                {
                    sql.append(line.substring(0, line.length()-1));
                    break;
                }
                else
                {
                    sql.append(line);
                }
            }

            if (sql.toString().trim().length() > 0)
            {
                nextStatement = sql.toString();
                return nextStatement;
            }

            return null;
        }
        
        /*
         * Metoda ma za zadanie pobrac pierwsza linie reprezentujaca wersje skryptu vsql
         * tworzaca baze danych
         * linia ta powinna miec postac:
         * -- 350
         */
        
        public String peekVsqlVersion() throws IOException{
            
            String line = null;
            try{
            line = reader.readLine();
            line = line.trim();
            
            line = line.substring(3, line.length());
            
            } catch(Exception e){};
            return line;
        }

        /**
         * Podmienia zmienne w przekazanym ci�gu. Zmienna ma posta�
         * <code>${wyra�enie OGNL)</code> lub <code>{resource:zas�b}</code>,
         * gdzie <code>zas�b</code> jest pobierany z obiektu StringManager
         * zwi�zanego z klas� InitialConfigurationAction.
         *
         * @param str
         */
        private String substituteVariables(String str)
        {
            StringBuilder sb = new StringBuilder(str);

            Pattern p = Pattern.compile("[^\\\\](\\$\\{[^}]+\\})|^(\\$\\{[^}]+\\})");
            Matcher m = p.matcher(sb.toString());

            while (m.find())
            {
                String expression = m.group(1);
                expression = expression.substring(2, expression.length()-1);
                int start = m.start(1);
                int end = m.end(1);

                Object value;

                if (expression.startsWith("resource:"))
                {
                    value = sm.getString(expression.substring("resource:".length()));
                }
                else
                {
                    try
                    {
                        OgnlExpression expr = new OgnlExpression(expression);
                        OgnlContext ctx = new OgnlContext();
                        value = expr.getValue(ctx, new Object());
                    }
                    catch (EdmException e)
                    {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                }

                sb.replace(start, end, String.valueOf(value));

                m = p.matcher(sb.toString());
            }

            return sb.toString();
        }
    }
    
    /**
     * Dodawanie do bazy domy�lnego rodzaju dokumentu: 'Zwyk�y dokument'.
     * 
     * @param conn
     * @param dbBrand
     * @throws Exception
     */
    private void CreateNormalDocKind(Connection conn, String dbBrand) throws Exception
    {
        PreparedStatement ps = null;
        try
        {
            if (dbBrand.equals(Docusafe.DB_FIREBIRD_1_5))
            {
                
                ps = conn.prepareStatement("insert into ds_document_kind (ID,HVERSION" +
                        ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                        ("gen_id(ds_document_kind_id, 1),") +
                        "0,'Zwyk�y dokument','dsg_normal_dockind',1,?," +
                        "'normal');");
                byte[] buf = xml.getBytes("utf-8");
               ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
               ps.executeUpdate();
            }
            else if (dbBrand.equals(Docusafe.ORACLE_9))
            {                
                /** ps = conn.prepareStatement("insert into ds_document_kind (ID,HVERSION" +
                        ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                        "DS_DOCUMENT_KIND_ID.NEXTVAL," +
                        "0,'Zwyk�y dokument','dsg_normal_dockind',1,?,'normal');");
                byte[] buf = xml.getBytes("utf-8");
               ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
               ps.executeUpdate(); */ 
            }
            else if (dbBrand.equals(Docusafe.DB_SQLSERVER2000))
            {                
                ps = conn.prepareStatement("insert into ds_document_kind (HVERSION" +
                        ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                        "0,'Zwyk�y dokument','dsg_normal_dockind',1,?," +
                        "'normal');");
                byte[] buf = xml.getBytes("utf-8");
               ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
               ps.executeUpdate();
            }

        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
    }
    
    private static final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
        "<doctype>" +
        "<fields>" +
        "</fields>" +
        "</doctype>";
    
    
    

}
