package pl.compan.docusafe.web.archive.init;

import pl.compan.docusafe.core.cfg.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filtr nie pozwalaj�cy u�ytkownikowi na otwarcie �adnej strony
 * poza serwletem konfiguracyjnym w sytuacji, gdy aplikacja nie
 * zosta�a prawid�owo skonfigurowana.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InitRedirectFilter.java,v 1.6 2006/02/20 15:42:34 lk Exp $
 */
public class InitRedirectFilter implements Filter
{
    private ServletContext servletContext;

    public void init(FilterConfig filterConfig) throws ServletException
    {
        servletContext = filterConfig.getServletContext();
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        // aplikacja jeszcze nie skonfigurowana - u�ytkownik mo�e otworzy�
        // jedynie serwlet konfiguracyjny i niekt�re pliki pomocnicze
        if (!Configuration.isConfigured())
        {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;

            String relativeRequestURI = request.getRequestURI().substring(request.getContextPath().length());
            if (!relativeRequestURI.startsWith("/init/") &&
                !relativeRequestURI.startsWith("/functions.js"))
            {
                response.sendRedirect(request.getContextPath()+"/init/configuration.do");
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy()
    {
    }
}
