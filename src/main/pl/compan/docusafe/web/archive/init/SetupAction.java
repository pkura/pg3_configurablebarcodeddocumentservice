package pl.compan.docusafe.web.archive.init;

import pl.compan.docusafe.webwork.event.*;

/**
 * Kroki konfiguracji:
 * 1. Potwierdzenie po�o�enia katalogu domowego
 * 2. Licencja
 * 3. U�ytkownicy w LDAP lub bazie danych, rodzaj RWA
 * 4. Wyb�r bazy danych
 * 5. Wyb�r LDAP (je�eli konieczne)
 * 6. Potwierdzenie 
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SetupAction.java,v 1.2 2006/02/20 15:42:34 lk Exp $
 */
public class SetupAction extends EventActionSupport
{
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }
}
