package pl.compan.docusafe.web.archive.users;



import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.CurrentYearAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.upload.FormFile;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;

import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.HttpUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UserCertificateAction extends EventActionSupport
{

	private String username;
	private Map[] certificates;
	private String[] deleteCerts;
	private Short level;
	private HashMap<Short, String> authLevels = new HashMap<Short, String>()
	{
	    {
	        put((short)0, "Logowanie bez certyfikatu" );
	        put((short)1, "Logowanie przy pomocy certfikatu");
	        put((short)2, "Logowanie: certyfikat + podanie loginu");
		    put((short)3, "Logowanie: certyfikat + podanie loginu i has�a");
		}
	};
	
	private static final Log log = LogFactory.getLog(UserCertificateAction.class);
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(CurrentYearAction.class.getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
        	append(OpenHibernateSession.INSTANCE).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSaveCert").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DoSaveCert()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDeleteCert").
    		append(OpenHibernateSession.INSTANCE).
    		append(new DoDeleteCert()).
    		append(fillForm).
    		appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new DoUpdate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	final DSUser user = DSUser.findByUsername(username);
            	
            	level = user.getAuthlevel();
            	
            	X509Certificate[] certs = user.getCertificates();
            	certificates = new Map[certs.length];
    			if (certs.length > 0)
    			{
    				for (int i = 0; i < certs.length; i++)
    				{
    					Map<String, String> bean = new HashMap<String, String>();
    					bean.put("dn", certs[i].getSubjectDN().getName());
    					//System.out.println(certs[i].getSubjectDN().getName());
    					bean.put("issuerDn", certs[i].getIssuerDN().getName());
    					try
    					{
    						// String sig = new
    						// String(Base64.encodeBase64(certs[i].getSignature()));
    						// sygnatur� nale�y dodatkowo zakodowa�, bo jest
    						// przekazywana
    						// jako GET (encoded64 jako post), za� w ci�gu base64
    						// mog� znale�� si� znaki + interpretowane jako spacje
    						bean.put("signature64", HttpUtils.urlEncode(new String(
    								Base64.encodeBase64(certs[i].getSignature()))));
    						//System.out.println( new String(new String(Base64.encodeBase64(certs[i].getSignature())))  );
    						bean.put("encoded64", new String(Base64
    								.encodeBase64(certs[i].getEncoded())));
    					}
    					catch (CertificateEncodingException e) 
    					{
    						bean.put("encoded64", "");
    						bean.put("signature64", "");
    						log.warn(e.getMessage(), e);
    					}
    					certificates[i] = bean;
    				}
//                    event.setResult("success");
    			}
    			else
    			{
    				certificates = null;
    			}
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    
    private class DoSaveCert implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	File file = null;
            	MultiPartRequestWrapper multiWrapper = 
            		(MultiPartRequestWrapper) ServletActionContext.getRequest();
            	
            	if (multiWrapper.hasErrors()) {
            		  Collection errors = multiWrapper.getErrors();
            		  Iterator i = errors.iterator();
            		  while (i.hasNext()) {
            		    addActionError((String) i.next());
            		  }
            		}
            	
            	Enumeration e = multiWrapper.getFileNames();

            	while (e.hasMoreElements()) 
            	{
            	   // get the value of this input tag
            	   String inputValue = (String) e.nextElement();
            	   
            	   // get the content type
            	   //String contentType = multiWrapper.getContentType(inputValue);

            	   // get the name of the file from the input tag
            	  // String fileName = multiWrapper.getFilesystemName(inputValue);

            	   // Get a File object for the uploaded File
            	   file = multiWrapper.getFile(inputValue);
            	   // If it's null the upload failed
            	   if (file == null) 
            	   {
            	      addActionError("Error uploading: " + multiWrapper.getFilesystemName(inputValue));
            	   }            	   

            	   // Do additional processing/logging...
            	 }
            	 FileInputStream fins = new FileInputStream(file);
                 X509Certificate cert = null;
                 try
                 {
                      cert = (X509Certificate) CertificateFactory.getInstance("X509").
                          generateCertificate(fins);
                 }
                 catch (CertificateException ce)
                 {
                	 addActionError("Niepoprawny certyfikat (nie w formacie X.509)");
                 }
                 catch (Exception ie)
                 {
                	 addActionError("B��d odczytu pliku z certyfikatem");
                 }

                 try
                 {
                     DSApi.context().begin();

                     try
                     {
                         DSUser user = DSUser.findByCertificate(cert);
                         addActionError("Inny u�ytkownik ("+user.getName()+") posiada ju� ten certyfikat");
                     }
                     catch (UserNotFoundException unfe)
                     {
                     }

                     DSUser user = DSUser.findByUsername(username);
                     if (!user.addCertificate(cert))
                    	 addActionError("U�ytkownik posiada� ju� ten certyfikat");

                     user.update();

                     DSApi.context().commit();
                 }
                 catch (EdmException ee)
                 {
                     DSApi.context().setRollbackOnly();
                     addActionError(ee.getMessage());
                 }

            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    
    private class DoDeleteCert implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	try
                {
                    //DSApi.open(AuthUtil.getSubject(request));
                    DSApi.context().begin();

                    // kontrola przes�anych danych wykonywana jest tutaj, a nie
                    // przed try-catch, aby wywo�ana zosta�a funkcja fillForm()
                    //String[] deleteCerts = (String[]) form.get("deleteCerts");
                    if (deleteCerts == null || deleteCerts.length == 0)
                    	addActionError("Nie wybrano certyfikat�w do usuni�cia");

                    DSUser user = DSUser.findByUsername(username);

                    for (int i=0; i < deleteCerts.length; i++)
                    {
                        try
                        {
                            user.removeCertificate(Base64.decodeBase64(deleteCerts[i].getBytes()));
                        }
                        catch (EdmException e)
                        {
                            continue;
                        }
                    }

                    user.update();

                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class DoUpdate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	try
                {
                    DSApi.context().begin();

                    DSUser user = DSUser.findByUsername(username);
                    user.setAuthlevel(level);

                    user.update();

                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Map[] getCertificates() {
		return certificates;
	}

	public void setCertificates(Map[] certificates) {
		this.certificates = certificates;
	}

	public String[] getDeleteCerts() {
		return deleteCerts;
	}

	public void setDeleteCerts(String[] deleteCerts) {
		this.deleteCerts = deleteCerts;
	}

	public HashMap<Short, String> getAuthLevels() {
		return authLevels;
	}

	public void setAuthLevels(HashMap<Short, String> authLevels) {
		this.authLevels = authLevels;
	}

	public Short getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}
    
    
    
}


