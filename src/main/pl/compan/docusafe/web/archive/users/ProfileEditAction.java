package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class ProfileEditAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(ProfileEditAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doUpdate"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    //public static final String FORWARD = "users/user-edit";

    public String[] getEventNames()
    {
        return eventNames;
    }
    
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        fillForm(null, request, form, mapping, errors);
        return mapping.findForward("main");
    }
    
    public ActionForward doUpdate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {

        try
        {
           final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
           ctx.begin();

           Profile profile = Finder.find(Profile.class, (Long)form.get("profileId"));//Profile.findByProfilename((String) form.get("profilename"));
           

           String[] newRoles = (String[])form.get("roles");
           Long[] newOfficeRoles = (Long[]) form.get("officeRoles");            
           String[] newGroups = (String[]) form.get("groups");
          
           profile.update(newRoles, newOfficeRoles, newGroups);
           ctx.commit();
           request.setAttribute("profilename", profile.getName());
           request.setAttribute("profileId", profile.getId());
           fillForm(DSApi.context(), request, form, mapping, errors);//messages.add(sm.getString("userEdit.saveData"));
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            
        }

        return mapping.findForward("main");
    }


    private void fillForm(DSContext ctx, HttpServletRequest request, DynaActionForm form,
                          ActionMapping mapping, Messages errors)
    {
        try
        {
            if (ctx == null)
                ctx = DSApi.open(AuthUtil.getSubject(request));
            
            Long currentProfile = (Long)form.get("profileId");
            
            Profile profile = Finder.find(Profile.class, currentProfile);
            if (profile != null) {
            	request.setAttribute("profilename", profile.getName());
		        request.setAttribute("profileId", currentProfile);
            	form.set("profileId", profile.getId());
            	form.set("profilename", profile.getName());
		       
		        // role archiwum
		        form.set("roles", profile.getArchiveRoles().toArray(new String[profile.getArchiveRoles().size()]));
		
		        DSDivision[] groups = UserFactory.getInstance().getGroups();
		        Arrays.sort(groups, DSDivision.NAME_COMPARTOR);
		        request.setAttribute("groups", groups);
		        form.set("groups", profile.getGuids());
		
		        // role kancelarii
		
		        if (AvailabilityManager.isAvailable("officeRoles"))
		        {
		            // wszystkie role
		            Role[] roles = Role.list();
		            DynaBean[] beans = new DynaBean[roles.length];
		            
		            for (int i = 0; i < roles.length; i++) {
		            	beans[i] = DynaBeans.bean(DynaBeans.checkbox);
		            	beans[i].set("value", roles[i].getId());
		            	beans[i].set("label", roles[i].getName());
		            }
		            
		            request.setAttribute("officeRoles", beans);
		
		            // role, w kt�rych u�ytkownik si� znajduje
		            
		            Role[] profileRoles = profile.getOfficeRoles().toArray(new Role[profile.getOfficeRoles().size()]);
		            Long[] roleIds = new Long[profileRoles.length];
		            for (int i = 0; i < profileRoles.length; i++) {
		            	roleIds[i] = profileRoles[i].getId();
		            }
		            form.set("officeRoles", roleIds);
		        }
            }
        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            DSApi._close();
        }
    }


    public static String getLink(String username, int offset, int limit, String sortField, boolean ascending)
    {
    	//TODO
    	return null;
//        ModuleConfig config = (ModuleConfig)
//            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
//        ForwardConfig fc = config.findForwardConfig(FORWARD);
//        return fc.getPath() + "?username="+username +
//            "&offset="+offset+
//            "&limit="+limit+
//            "&sortField="+sortField+
//            "&ascending="+ascending;
    }
}
