package pl.compan.docusafe.web.archive.users;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.UserAdditionalData;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import std.fun;
import std.lambda;

public class ViewUserAction extends EventDrivenAction {
	private static final Log log = LogFactory.getLog(UserEditAction.class);
	private static final String[] eventNames = new String[] { "doDefault" };
	static {
		Arrays.sort(eventNames);
	}
	public static final String FORWARD = "users/view-user";

	public String[] getEventNames() {
		return eventNames;
	}

	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {
		try {
			if (ctx == null)
				ctx = DSApi.open(AuthUtil.getSubject(request));

			final DSUser user = DSUser.findByUsername((String) form
					.get("username"));

			//potrzebne do zakladek
			request.setAttribute("username", user.getName());
			
			form.set("username", user.getName());
			form.set("externalName", user.getExternalName());
			form.set("firstname", user.getFirstname());
			form.set("lastname", user.getLastname());
			form.set("identifier", user.getIdentifier());
			form.set("email", user.getEmail());
			if (user.getSubstituteUser() != null)
				form.set("substitutedBy", user.getSubstituteUser().getName());
			form.set("loginDisabled", new Boolean(user.isLoginDisabled()));
			form.set("certificateLogin", new Boolean(user.isCertificateLogin()));
			form.set("phoneNum", user.getPhoneNum());
			form.set("mobileNum", user.getMobileNum());
			form.set("extension", user.getExtension());
			form.set("communicatorNum", user.getCommunicatorNum());
			form.set("creationTime", user.getCtime() != null ? user.getCtime()
					.toLocaleString() : "");
			form.set("remarks", user.getRemarks());
			if (user.getValidityDate() != null) {
				form.set("validityDate", DateUtils.formatJsDate(user.getValidityDate()));
			} else {
				form.set("validityDate",null);
			}
			if(AvailabilityManager.isAvailable("mail.footer")){
	            UserAdditionalData userAddData = UserAdditionalData.findByUserId(user.getId());
	            if(userAddData != null){
	            	form.set("mailFooter", userAddData.getMailFooter());
	            }else{
	            	form.set("mailFooter", "");
	            }
            }
			// dzia�y, w kt�rych znajduje si� u�ytkownik (z wyj�tkiem grup)
			final String[] divisions = user.getOrdinaryDivisions();
			form.set("divisions", divisions);

			X509Certificate[] certs = user.getCertificates();
			Map[] certBeans = new Map[certs.length];
			if (certs.length > 0) {
				for (int i = 0; i < certs.length; i++) {
					Map<String, String> bean = new HashMap<String, String>();
					bean.put("dn", certs[i].getSubjectDN().getName());
					bean.put("issuerDn", certs[i].getIssuerDN().getName());
					try {
						// String sig = new
						// String(Base64.encodeBase64(certs[i].getSignature()));
						// sygnatur� nale�y dodatkowo zakodowa�, bo jest
						// przekazywana
						// jako GET (encoded64 jako post), za� w ci�gu base64
						// mog� znale�� si� znaki + interpretowane jako spacje
						bean.put("signature64", HttpUtils.urlEncode(new String(
								Base64.encodeBase64(certs[i].getSignature()))));
						bean.put("encoded64", new String(Base64
								.encodeBase64(certs[i].getEncoded())));
					} catch (CertificateEncodingException e) {
						bean.put("encoded64", "");
						bean.put("signature64", "");
						log.warn(e.getMessage(), e);
					}
					certBeans[i] = bean;
				}
			}
			request.setAttribute("certificates", certBeans);

			// role archiwum

			form.set("roles", user.getRoles().toArray(
					new String[user.getRoles().size()]));

			// grupy, w kt�rych u�ytkownik si� znajduje
			String[] groupGuids = user.getGroups();
			Arrays.sort(groupGuids, String.CASE_INSENSITIVE_ORDER);
			form.set("groups", groupGuids);

			// role kancelarii

			if (Configuration.coreOfficeAvailable()) {
				form.set("officeRoles", user.getOfficeRoles());
			}
		} catch (EdmException e) {
			errors.add(e.getMessage());
			log.error(e.getMessage(), e);
		} finally {
			DSApi._close();
		}
	}
}
