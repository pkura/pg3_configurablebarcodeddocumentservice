package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.users.Constants;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class ProfileListAction extends EventDrivenAction {
	private static final Log log = LogFactory.getLog(ProfileEditAction.class);
	private static final String[] eventNames = new String[] { "doDefault", "doDelete", "doCreate" };
	static {
		Arrays.sort(eventNames);
	}

	public String[] getEventNames() {
		return eventNames;
	}

	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}
	
	public ActionForward doDelete(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		
		String[] profileIds = (String[]) form.get("delProfileNames");

        DSContext ctx = null;
        try
        {
        	if (ctx == null) {
				ctx = DSApi.open(AuthUtil.getSubject(request));
        		ctx.begin();
        	}
        		
            for (String profileId : profileIds) {
                Profile profile = Finder.find(Profile.class, Long.parseLong(profileId));
                messages.add(sm.getString("UsunietoProfil",profile.getName()));
                profile.remove();
            }

            ctx.commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
        }
        finally
        {
            DSApi._close();
        }
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	public ActionForward doCreate (ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		
		try {
			final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
			ctx.begin();
			String newProfileName = (String) form.get("newProfileName");
			Profile newProfile = Profile.create(newProfileName);
			if (newProfile == null) {
				//TODO
				throw new EdmException("Profile could not be created");
			}
			ctx.commit();
		} catch (EdmException e) {
			LogFactory.getLog("eprint").debug("", e);
			// TODO: handle exception
		} finally {
			fillForm(DSApi.context(), request, form, mapping, errors);
		}
		return mapping.findForward("main");
	}
	
	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {
		try {
			if (ctx == null)
				ctx = DSApi.open(AuthUtil.getSubject(request));

			Profile[] profiles = Profile.getProfiles();
			request.setAttribute("profiles", profiles);
			

		} catch (EdmException e) {
			errors.add(e.getMessage());
			log.error("", e);
		} finally {
			DSApi._close();
		}
	}
}
