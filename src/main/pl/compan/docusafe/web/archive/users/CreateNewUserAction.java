package pl.compan.docusafe.web.archive.users;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Klasa służy do tworzenia nowego użytkownika w systemie
 *
 * Date: 24.06.13
 * Time: 12:40
 */
public class CreateNewUserAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(CreateNewUserAction.class);

	private static final StringManager sm = StringManager.getManager(Constants.Package);

	/**
	 * Login nowego użytkownika
	 */
	private String newUsername;

	/**
	 * Imię nowego użytkownika
	 */
	private String newFirstname;

	/**
	 * Nazwisko nowego użytkownika
	 */
	private String newLastname;


	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
				append(OpenHibernateSession.INSTANCE).
				append(fillForm).
				appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doCreate").
				append(OpenHibernateSession.INSTANCE).
				append(new DoCreate()).
				appendFinally(CloseHibernateSession.INSTANCE);
	}


	/**
	 * FillForm - zbędny, ale niech będzie
	 */
	private class FillForm implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			//nic nie rób, a bo po co :D?
		}
	}


	/**
	 * Akcja do tworzenia użytkownika
	 */
	private class DoCreate implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (StringUtils.isEmpty(newFirstname) || StringUtils.isEmpty(newLastname) || StringUtils.isEmpty(newUsername)) {
					addActionError(sm.getString("WszystkiePolaSaObowiazkowe"));
					throw new EdmException(sm.getString("WszystkiePolaSaObowiazkowe"));
				}

				if (!newUsername.matches("[a-zA-Z][a-zA-Z0-9]+"))
				{
					addActionError(sm.getString("newUser.invalidCharactersInUsername"));
					throw new EdmException(sm.getString("newUser.invalidCharactersInUsername"));
				}

					if (!DSUser.isUserExists(newUsername))
					{
						DSUser user = UserFactory.getInstance().createUser(newUsername, newFirstname, newLastname);
						user.setCtime(new Timestamp(new Date().getTime()));

						log.error("uzytkownik utworzony {} przez {}", user.asLastnameFirstnameName(), DSApi.context().getPrincipalName());

						if(AvailabilityManager.isAvailable("newUser.addEmployeeCard") || AvailabilityManager.isAvailable("newUser.addEmployeeCardAndAbsences"))
						{
							AbsenceFactory.createCard(user);
						}

						DSUserEnumField.reloadForAll();
						DataBaseEnumField.reloadForTable("ds_user");
						addActionMessage(sm.getString("Utworzona"));
					}
					else
					{
						addActionError(sm.getString("newUser.existingUser", newUsername));
						throw new EdmException(sm.getString("newUser.existingUser", newUsername));
					}
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			} catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	public String getNewUsername()
	{
		return newUsername;
	}

	public void setNewUsername(String newUsername)
	{
		this.newUsername = newUsername;
	}

	public String getNewFirstname()
	{
		return newFirstname;
	}

	public void setNewFirstname(String newFirstname)
	{
		this.newFirstname = newFirstname;
	}

	public String getNewLastname()
	{
		return newLastname;
	}

	public void setNewLastname(String newLastname)
	{
		this.newLastname = newLastname;
	}
}
