package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import pl.compan.docusafe.web.common.form.UpdateUser;


/**
 * akcja z atrybutami sluzbowymi konta uzytkownika (prosika)
 * @author kuba�
 *
 */
public class UserJobAttributesAction extends EventDrivenAction {

	private static final Log log = LogFactory.getLog(UserPermissionsAction.class);

	private static final String[] eventNames = new String[] { "doDefault",
			"doUpdate" };

	static {
		Arrays.sort(eventNames);
	}

	public static final String FORWARD = "users/user-profiles";

	public String[] getEventNames() {
		return eventNames;
	}

	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	
	public ActionForward doUpdate(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) throws EdmException {
		
		DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
		ctx.begin();
		final DSUser user = DSUser.findByUsername((String) form.get("username"));
		
		final String kpx = (String) form.get("kpx");
        final String superior = (String) form.get("superior");
        final String validityDate = (String) form.get("validityDate");
        final Long id = (Long)form.get("location");
        
        //location
       
        
        
        final String remarks = (String) form.get("remarks");
        
        try {
        	
            if(!StringUtils.isEmpty(kpx))
            	user.setKpx(kpx);
            else
            	user.setKpx(null);
            
            if(!StringUtils.isEmpty(superior))
            	try {
            		DSUser supervisor = DSUser.findByUsername(superior);
            		user.setSupervisor(supervisor);
            	} catch (UserNotFoundException e) {
            		user.setSupervisor(null);
            	}
            else
            	user.setSupervisor(null);
            
            if(!StringUtils.isEmpty(validityDate)) {
            	Date validityDateDate = DateUtils.nullSafeParseJsDate(validityDate);
            	if (validityDateDate != null) {
            		Date now = new Date();
            		if (now.after(validityDateDate)) {
            			throw new IllegalArgumentException("Data wa�nosci wczesniejsza ni� data dzisiejsza");
            		}
            		user.setValidityDate(validityDateDate);
            	} else {
            		throw new IllegalArgumentException("Nieprawidlowy format daty");
            	}
            }
            	
            if(!StringUtils.isEmpty(remarks))
            	user.setRemarks(remarks);
            else
            	user.setRemarks(null);
		
            ctx.commit();
        } catch (IllegalArgumentException e) {
        	DSApi.context().setRollbackOnly();
        	errors.add(e.getMessage());
        } catch (EdmException e) {
        	DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
            log.error("", e);
        } finally {
        	fillForm(DSApi.context(), request, form, mapping, errors);
        }
        return mapping.findForward("main");
	}
	
	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {


	}
}