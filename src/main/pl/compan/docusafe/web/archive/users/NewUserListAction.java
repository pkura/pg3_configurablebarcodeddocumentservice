package pl.compan.docusafe.web.archive.users;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Nowa formatka do przeglądania listy użytkowników
 *
 * Żeby włączyć formatkę należy wpisać w available.properties:
 * menu.left.newWebWorkUserList = true
 *
 * Date: 25.06.13
 * Time: 12:14
 */
public class NewUserListAction extends EventActionSupport
{

	private static final Logger log = LoggerFactory.getLogger(NewUserListAction.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);

	/**
	 * Nazwa wyszukiwanego użytkownika
	 */
	private String name;

	/**
	 * Ograniczenie liczby dni aktywności użytkowników
	 */
	private Integer days;

	/**
	 * Stronnicowanie wyników
	 */
	private Pager pager;

	/**
	 * Id użytkownika do usunięcia
	 */
	private Long userIdToDelete;

	/**
	 * Offset dla pagera
	 */
	private int offset;

	/**
	 * Limit wyników na stronie
	 */
	private static final int LIMIT = 10;

	/**
	 * Wyniki wyszukiwania
	 */
	private SearchResults<DSUser> results;

	/**
	 * Pole po którym będą sortowane wyniki
	 */
	private String sortField;

	/**
	 * Czy będzie sortowanie rosnące po sortField
	 */
	private boolean ascending;

	/**
	 * Link do formatki
	 */
	public static final String URL = "/users/new-user-list.action";


	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}


	/**
	 * FillForm - domyślne dane do formatki
	 */
	private class FillForm implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			DSUserQueryForm query =	new DSUserQueryForm(offset, LIMIT);

			try
			{
				if (!StringUtils.isEmpty(sortField))
					if (ascending)
						query.orderAsc(sortField);
					else
						query.orderDesc(sortField);


				if (!StringUtils.isEmpty(name))
					query.name(name);

				if (days != null)
					query.days(days);

				results = search(query);
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}

			if (results.count() == 0)
				addActionMessage(sm.getString("NieZnalezionoUzytkownikow"));

			pager = new Pager(new Pager.LinkVisitor()
			{
				public String getLink(int offset)
				{
					return URL + "?name=" + (name == null ? "" : name) +
							"&days=" + (days == null ? "" : days) +
							"&offset=" + offset +
							"&sortField=" + sortField +
							"&ascending="+ascending;
				}
			}, offset, LIMIT, results.totalCount(), 10);
		}

	}


	/**
	 * FormQuery służące do przygotowania kryteriów wyszukiwania
	 */
	private static class DSUserQueryForm extends FormQuery
	{
		/**
		 * Atrybut po loginie
		 */
		public static final String NAME = "name";

		/**
		 * Atrybut po imieniu
		 */
		public static final String FIRSTNAME = "firstname";

		/**
		 * Atrybut po nazwisku
		 */
		public static final String LASTNAME = "lastname";

		/**
		 * Wartości przekazywane do sortowania
		 */
		private static final String[] SORT_ATTRIBUTES =
				new String[] { "name", "firstname",
						"lastname" };

		public DSUserQueryForm(int offset, int limit)
		{
			super(offset, limit);
		}

		/**
		 * Zwraca tablę Stringów z atrybutami do sortowania
		 *
		 * @return attributes[] - atrybuty sorotowania
		 */
		protected String[] sortAttributes()
		{
			return SORT_ATTRIBUTES;
		}

		/**
		 * Dodanie warunku do zapytania ograniczajaca liczbe użytkowników z częścią podanego loginu (name)
		 *
		 * @param name - login lub nazwiko (część, lub całość)
		 */
		public void name(String name)
		{
			FormQuery userDisjunctionQuery = new FormQuery();
			userDisjunctionQuery.addLike("name", "%" + name + "%");
			userDisjunctionQuery.addLike("lastname", "%" + name + "%");
			addDisjunction(userDisjunctionQuery);

			addEq("deleted", Boolean.FALSE);
		}

		/**
		 * Dodanie warunku do zapytania ograniczajaca liczbe użytkowników którzy zostali zalogowani powyżej
		 * podanej ilości dni.
		 *
		 * @param days ilość dni w których nie został zalogowany użytkownik
		 */
		public void days (int days)
		{
			Calendar now = Calendar.getInstance();
			now.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) - days);

			FormQuery daysDisjunctionQuery = new FormQuery();
			daysDisjunctionQuery.addLe("lastSuccessfulLogin", now.getTime());
			daysDisjunctionQuery.addEq("lastSuccessfulLogin", null);
			addDisjunction(daysDisjunctionQuery);
		}
	}

	/**
	 * Wyszukiwanie użytkowników z uwzględnieniem limity i offsetu
	 *
	 * @param query - zapytanie
	 * @return wyniki zapytania
	 * @throws EdmException
	 */
	public static SearchResults search(DSUserQueryForm query) throws EdmException
	{
		FromClause from = new FromClause();
		final TableAlias certTable = from.createTable(DSApi.getTableName(UserImpl.class));
		final WhereClause where = new WhereClause();


		query.visitQuery(new FormQuery.QueryVisitorAdapter()
		{
			Junction daysDisjunction = Expression.disjunction();
			Junction userDisjunction = Expression.disjunction();

			public void visitLike(FormQuery.Like expr) throws EdmException
			{
				if (expr.getAttribute().equals("name"))
				{
					userDisjunction.add(Expression.like(certTable.attribute("name"), expr.getValue()));
				}
				if (expr.getAttribute().equals("lastname"))
				{
					userDisjunction.add(Expression.like(certTable.attribute("lastname"), expr.getValue()));
				}
			}

			public void visitLe(FormQuery.Le expr) throws  EdmException
			{
				if (expr.getAttribute().equals("lastSuccessfulLogin"))
				{
					daysDisjunction.add(Expression.le(certTable.attribute("lastSuccessfulLogin"), expr.getValue()));
				}
			}

			@Override
			public void visitEq(FormQuery.Eq expr) throws EdmException
			{
				if (expr.getAttribute().equals("lastSuccessfulLogin"))
				{
					daysDisjunction.add(Expression.eq(certTable.attribute("lastSuccessfulLogin"), expr.getValue()));
				}
				if (expr.getAttribute().equals("deleted"))
				{
					where.add(Expression.eq(certTable.attribute("deleted"), expr.getValue()));
				}
			}

			public void endDisjunction() throws EdmException
			{
				if (!userDisjunction.isEmpty())
				{
					where.add(userDisjunction);
				}

				if (!daysDisjunction.isEmpty())
				{
					where.add(daysDisjunction);
				}

			}
		}
		);
		SelectClause selectId = new SelectClause(true);
		SelectColumn idCol = selectId.add(certTable, "id");

		OrderClause order = new OrderClause();
		FormQuery.OrderBy[] orderBy = query.getOrder();
		for (int i=0; i < orderBy.length; i++)
		{
			order.add(certTable.attribute(orderBy[i].getAttribute()), Boolean.valueOf(orderBy[i].isAscending()));
			selectId.add(certTable, orderBy[i].getAttribute());
		}

		SelectClause selectCount = new SelectClause();
		SelectColumn countCol = selectCount.addSql("count(distinct "+certTable.getAlias()+".id)");

		int totalCount;
		SelectQuery selectQuery;

		try
		{
			selectQuery = new SelectQuery(selectCount, from, where, null);
			ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
			if (!rs.next())
				throw new EdmException("Nie mozna pobrac liczby uzytkownikow");
			totalCount = rs.getInt(countCol.getPosition());
			rs.close();

			selectQuery = new SelectQuery(selectId, from, where, order);
			selectQuery.setMaxResults(query.getLimit());
			selectQuery.setFirstResult(query.getOffset());
			rs = selectQuery.resultSet(DSApi.context().session().connection());
			List<UserImpl> results = new ArrayList<UserImpl>(query.getLimit());
			while (rs.next())
			{
				Long id = new Long(rs.getLong(idCol.getPosition()));
				results.add(DSApi.context().load(UserImpl.class, id));
			}
			rs.close();
			return new SearchResultsAdapter<UserImpl>(results, query.getOffset(), totalCount,
					UserImpl.class);
		}
		catch (SQLException e)
		{
			throw new EdmSQLException(e);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	/**
	 * Sortowanie na potrzeby formatki
	 * Metoda wywoływana jest po kliknięcie strzałek (sortowanie) w nagłówku tabelki
	 *
	 * @param sortField - pole po którym będzie sortowanie
	 * @param ascending - czy rosnące
	 * @return
	 */
	public String getSortLink(String sortField, boolean ascending)
	{
			return  URL + "?name=" + (name == null ? "" : name) +
					"&days=" + (days == null ? "" : days) +
					"&offset=" + offset +
					"&sortField=" + sortField +
					"&ascending=" + ascending;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Pager getPager()
	{
		return pager;
	}

	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	public Long getUserIdToDelete()
	{
		return userIdToDelete;
	}

	public void setUserIdToDelete(Long userIdToDelete)
	{
		this.userIdToDelete = userIdToDelete;
	}

	public int getOffset()
	{
		return offset;
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public static int getLimit()
	{
		return LIMIT;
	}

	public SearchResults<DSUser> getResults()
	{
		return results;
	}

	public void setResults(SearchResults<DSUser> results)
	{
		this.results = results;
	}

	public String getSortField()
	{
		return sortField;
	}

	public void setSortField(String sortField)
	{
		this.sortField = sortField;
	}

	public boolean isAscending()
	{
		return ascending;
	}

	public void setAscending(boolean ascending)
	{
		this.ascending = ascending;
	}

	public Integer getDays()
	{
		return days;
	}

	public void setDays(Integer days)
	{
		this.days = days;
	}
}
