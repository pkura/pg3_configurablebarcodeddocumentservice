package pl.compan.docusafe.web.archive.users;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import pl.compan.docusafe.web.common.form.UpdateUser;
import std.pair;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class UserPermissionsAction extends EventDrivenAction {

    private static final Logger log = LoggerFactory.getLogger(UserPermissionsAction.class);

	private static final String[] eventNames = new String[] { "doDefault",
			"doUpdate" };

	static {
		Arrays.sort(eventNames);
	}

	public static final String FORWARD = "users/edit-permissions";

	public String[] getEventNames() {
		return eventNames;
	}
	
	private String[] selectedArchiveRoles;
	private Long[] selectedOfficeRoles;
	private String[] selectedGroups;

	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	
	public ActionForward doUpdate(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) throws EdmException {
		
		DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
		ctx.begin();
		final DSUser user = DSUser.findByUsername((String) form.get("username"));
		
		selectedArchiveRoles = (String[])form.get("selectedArchiveRoles");
		selectedOfficeRoles = (Long[])form.get("selectedOfficeRoles");
		selectedGroups = (String[])form.get("selectedGroups");
		
		UpdateUser.updateArchiveRoles(selectedArchiveRoles, user);
		UpdateUser.updateOfficeRoles(selectedOfficeRoles, user);
		UpdateUser.updateGroups(selectedGroups, user);
		user.update();
		
		ctx.session().flush();
		ctx.session().clear();
		ctx.commit();
		//System.out.println("jesten");
		
		PermissionCache permCache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
		permCache.invalidate(user);
        permCache.update(user.getName());
        
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}
	
	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {
		try {
			if (ctx == null)
				ctx = DSApi.open(AuthUtil.getSubject(request));

			final DSUser user = DSUser.findByUsername((String) form.get("username"));

			request.setAttribute("username", user.getName());
			
			String[] allRoles= Role.archiveRoles;
			Map<String, String[]> userRolesSources = user.getRolesSources();
			Map<String, String[]> rolesSources = new HashMap<String, String[]>();
			for (String role : allRoles) {
				if (userRolesSources.containsKey(role)) {
					rolesSources.put(role, userRolesSources.get(role));
				} else {
					rolesSources.put(role, null);
				}
			}
			Set<String> selectedArchiveRolesSet = userRolesSources.keySet();
			selectedArchiveRoles = selectedArchiveRolesSet.toArray(new String[selectedArchiveRolesSet.size()]);
			form.set("selectedArchiveRoles", selectedArchiveRoles);
			request.setAttribute("rolesSources", rolesSources);
			
			
			DSDivision[] groups = UserFactory.getInstance().getGroups();
			Arrays.sort(groups, DSDivision.NAME_COMPARTOR);
			ArrayList<pair<DSDivision, String[]>> groupsSources = new ArrayList<pair<DSDivision, String[]>>();
			String[] groupSources;
			Set<String> selectedGroupsSet = new HashSet<String>();
			for (DSDivision divis : groups) {
				groupSources = divis.getUserSources(user);
				if (groupSources != null) {
					selectedGroupsSet.add(divis.getGuid());
				}
				groupsSources.add(new pair(divis,groupSources));
			}
			request.setAttribute("groupsSources", groupsSources);
		
			selectedGroups = selectedGroupsSet.toArray(new String[selectedGroupsSet.size()]);
			form.set("selectedGroups", selectedGroups);
			
			
			
			Role[] officeRoles = Role.list();
			ArrayList<pair<Role, String[]>> officeRolesSources = new ArrayList<pair<Role, String[]>>();
			String[] officeRoleSources;
			Set<Long> selectedOfficeRolesSet = new HashSet<Long>();
			for (Role role : officeRoles) {
				officeRoleSources = role.getUserSources(user);
				if (officeRoleSources != null) {
					selectedOfficeRolesSet.add(role.getId());
				}
				officeRolesSources.add(new pair(role, officeRoleSources));
			}
			request.setAttribute("officeRolesSources", officeRolesSources);
		
			selectedOfficeRoles = selectedOfficeRolesSet.toArray(new Long[selectedOfficeRolesSet.size()]);
			form.set("selectedOfficeRoles", selectedOfficeRoles);
			
		} catch (EdmException e) {
			errors.add(e.getMessage());
            log.error("Blad", e);
		} finally {
			DSApi._close();
		}
	}

	public String[] getSelectedArchiveRoles() {
		return selectedArchiveRoles;
	}

}
