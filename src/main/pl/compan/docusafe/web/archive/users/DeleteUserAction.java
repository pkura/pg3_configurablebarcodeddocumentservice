package pl.compan.docusafe.web.archive.users;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import std.fun;
import std.lambda;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

/**
 * Usuwanie u�ytkownika.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DeleteUserAction.java,v 1.18 2009/10/09 12:57:53 mariuszk Exp $
 */
public class DeleteUserAction extends EventDrivenAction
{
	private static final Logger log = LoggerFactory.getLogger(DeleteUserAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doDelete", "doCancel"
    };
    static {
        Arrays.sort(eventNames);
    }

    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String FORWARD = "users/delete-user";

    public String[] getEventNames()
    {
        return eventNames;
    }

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        String username = (String) form.get("username");

        if (StringUtils.isEmpty(username))
            errors.add(sm.getString("deleteUser.missingUsername"));

        if (errors.size() > 0)
            return mapping.findForward("main");

        try
        {
            DSApi.open(AuthUtil.getSubject(request));

            DSUser user = DSUser.findByUsername((String) form.get("username"));

            request.setAttribute("userName", user.asFirstnameLastnameName());
            Collection users =
                fun.map(DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true).results(),
                    new lambda()
                    {
                        public Object act(Object o)
                        {
                            DSUser u = (DSUser) o;
                            DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
                            bean.set("label", u.asLastnameFirstname());
                            bean.set("value", u.getName());
                            return bean;
                        }
                    });
            request.setAttribute("users", users);
        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
        }
        finally
        {
            DSApi._close();
        }

        return mapping.findForward("main");
    }

    public ActionForward doCancel(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        final int limit = form.get("limit") != null ? ((Integer) form.get("limit")).intValue() : UserListAction.LIMIT;
        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
        if (offset < 0) offset = 0;
        final String sortField = (String) form.get("sortField");

        return new ActionForward(UserListAction.getLink(offset, limit, sortField, true), true);
    }

    public ActionForward doDelete(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {

	    	final int limit = form.get("limit") != null ? ((Integer) form.get("limit")).intValue() : UserListAction.LIMIT;
	        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
	        if (offset < 0) offset = 0;
	        final String sortField = (String) form.get("sortField");
	
	        final String username = StringUtils.isEmpty((String) form.get("username")) ?
	            null : ((String) form.get("username")).toLowerCase();
	        final String toUsername = StringUtils.isEmpty((String) form.get("toUsername")) ?
	                null : ((String) form.get("toUsername")).toLowerCase();
	 
	        if (username == null)
	        {
	            errors.add(sm.getString("deleteUser.missingUsername"));
	            return mapping.findForward("main");
	        }
	
	        boolean success;
	        success = deleteUser(request, errors, username,toUsername);
	        refreshAdmin(request, errors);

	        if (success)
	        {
                log.info("Uzytkownik {} usuniety", username);
	            messages.add(sm.getString("deleteUser.userDeleted", username));
	            return new ActionForward(UserListAction.getLink(offset, limit, sortField, true));
	        }
	        else
	        {
	            return mapping.findForward("main");
	        }
    }

    /**
     * Metoda od�wie�a list� zada� - prawdopodobnie by pisma z listy
     * zada� poprzedniego u�ytkownika mog�y do niego przej��
     * @param request
     * @param errors
     */
    private void refreshAdmin(HttpServletRequest request, Messages errors) {
        try
        {
        	DSApi.open(AuthUtil.getSubject(request));
        	DSApi.context().begin();
            String admin = GlobalPreferences.getAdminUsername();
            if(admin == null)
            {
            	admin = "admin";
            }
            TasklistSynchro.Synchro(admin);
            DSApi.context().commit();
        }
        catch (Exception e)
		{
        	DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
		}
        finally
        {
        	 DSApi._close();
        }
    }

    private boolean deleteUser(HttpServletRequest request, Messages errors, String username, String toUser) {
        boolean success = false;
        try {
            DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
            Collection users =
	            fun.map(DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true).results(),
	                new lambda()
	                {
	                    public Object act(Object o)
	                    {
	                        DSUser u = (DSUser) o;
	                        DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
	                        bean.set("label", u.asLastnameFirstname());
	                        bean.set("value", u.getName());
	                        return bean;
	                    }
	                });
	        request.setAttribute("users", users);
	        
            if(toUser == null || toUser.length() < 1)
            	toUser = "admin";
            if(toUser.equals(username))
            	throw new EdmException("Nie mo�na przekaza� zada� usuwanemu u�ytkownikowi");
            DSApi._close();
            AbstractParametrization.getInstance().setBeforeUserDelete(username,toUser,ctx,AuthUtil.getSubject(request));
            ctx = DSApi.open(AuthUtil.getSubject(request));
            ctx.begin();
            if (Configuration.coreOfficeAvailable())
            {
            	TasklistSynchro.Synchro(username);
                WorkflowFactory.removeUser(username,toUser);
            }

            DSLog log = new DSLog();
            log.setUsername(DSApi.context().getPrincipalName());
            log.setCode(DSLog.DELETE_USER);
            log.setCtime(new Date());
            log.setParam(username);
            log.create();

            UserFactory.getInstance().deleteUser(username);
            DSUserEnumField.reloadForAll();
            DataMartEvent userDeleted = new DataMartEvent(true, null, null, DataMartDefs.USER_DELETE, null, username, null, null);
            ctx.commit();
            success = true;
	        
        } catch (EdmException e) {
        	errors.add(e.getMessage());
        	log.error(e.getMessage(),e);
            DSApi.context().setRollbackOnly();
        } finally {
            DSApi._close();
        }
        return success;
    }

    public static String getLink(String username, int offset, int limit, String sortField, boolean ascending)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() + "?username="+username +
            "&offset="+offset+
            "&limit="+limit+
            "&sortField="+sortField+
            "&ascending="+ascending;
    }
}
