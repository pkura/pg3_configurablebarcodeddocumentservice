package pl.compan.docusafe.web.archive.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class UserProfilesAction extends EventDrivenAction {

	private static final Logger log = LoggerFactory.getLogger(UserProfilesAction.class);

	private static final String[] eventNames = new String[] { "doDefault",
			"doUpdate" };

	static {
		Arrays.sort(eventNames);
	}

	public static final String FORWARD = "users/user-profiles";

	public String[] getEventNames() {
		return eventNames;
	}
	
	private String[] selectedProfiles;
	private TreeMap<String,String> allProfilesWithKeys;
	private String[] keys;

	

	//private List<UserProfile> allProfilesWithKeys;
	
	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	//FIXME - zle lapane wyjaktki - wydaje mi sie ze jesli tu bedzie blad to nie zamknie sie
	//polaczenie
	public ActionForward doUpdate(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) throws EdmException 
			{
		
		/*if (log.isTraceEnabled())
		{
			log.trace("update profili dla uzytkownika {}",form.get("username"));
		}*/
		
		DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
		
		try
		{
		
			ctx.begin();
			final DSUser user = DSUser.findByUsername((String) form.get("username"));				
			selectedProfiles = (String[])form.get("selectedProfiles");
			keys = (String[])form.get("keys");
			String[] userProfiles = user.getProfileNames();
		
			/*if (log.isTraceEnabled())
			{
				log.trace("Profile uzytkownika");		
				for (String str : userProfiles) {
					log.trace(str);
				}
				log.trace("zaznaczone profile");
				for (String str : selectedProfiles) 
				{
					log.trace(str);
				}
			}*/
			
			
			List<String> selProfAL = Arrays.asList(selectedProfiles); 
			List<String> usrProfAL = Arrays.asList(userProfiles);
			List<String> allProfiles = Arrays.asList(Profile.getProfilesNames());
			List<String> keysAL = Arrays.asList(keys);		

			if(keysAL.size()>0){
				addUsers(ctx, allProfiles, selProfAL, usrProfAL, user, keysAL);
			}else {
				addUsers(ctx, allProfiles, selProfAL, usrProfAL,  user);
			}
			
			ctx.commit();
		}
		catch (Exception e)
		{
			ctx._rollback();			
			errors.add(e.getMessage());
            log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi._close();
		}
		
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}
	
	private void addUsers(DSContext ctx, List<String> allProfiles, List<String> selProfAL, List<String> usrProfAL, DSUser user, List<String> keysAL)throws EdmException{
		int i = 0;
		for (String profile : allProfiles) {
			if (selProfAL.contains(profile)) {
				if (!usrProfAL.contains(profile)) 
				{
					log.debug("dodajemy uzytkownika {} do profilu {}",user.getName(),profile);
					Profile.findByProfilename(profile).addUser(user, keysAL.get(i));
					ctx.session().flush();
				}else if(!keysAL.get(i).equals(null)){
					Profile.findByProfilename(profile).updateKey(user, keysAL.get(i));
				} 
			} else {
				if (usrProfAL.contains(profile)) 
				{
					log.debug("usuwamy uzytkownika {} z profilu {}",user.getName(),profile);
					Profile.findByProfilename(profile).removeUser(user);
					ctx.session().flush();
				}
			}
			i++;
		}		
	}
	private void addUsers(DSContext ctx, List<String> allProfiles, List<String> selProfAL, List<String> usrProfAL,  DSUser user) throws EdmException{
		for (String profile : allProfiles) {
			if (selProfAL.contains(profile)) {
				if (!usrProfAL.contains(profile)) 
				{
					log.debug("dodajemy uzytkownika {} do profilu {}",user.getName(),profile);
					Profile.findByProfilename(profile).addUser(user);
					ctx.session().flush();
				}
			} else {
				if (usrProfAL.contains(profile)) 
				{
					log.debug("usuwamy uzytkownika {} z profilu {}",user.getName(),profile);
					Profile.findByProfilename(profile).removeUser(user);
					ctx.session().flush();
				}
			}
		}
		
	}
	public String[] getSelectedProfiles() {
		return selectedProfiles;
	}

	public void setSelectedProfiles(String[] selectedProfiles) {
		this.selectedProfiles = selectedProfiles;
	}

	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {
		try {
			if (ctx == null)
				ctx = DSApi.open(AuthUtil.getSubject(request));

			final DSUser user = DSUser.findByUsername((String) form.get("username"));
			String[] allProfiles = Profile.getProfilesNames();
			request.setAttribute("username", user.getName());
			
			Map<String, String> userProfilesWithKeys = user.getProfilesWithKeys();
			allProfilesWithKeys = new TreeMap<String, String>();
			if(allProfiles!=null)
			for(String profile : allProfiles){
				allProfilesWithKeys.put(profile,userProfilesWithKeys.get(profile));
			}
			
				
			request.setAttribute("allProfiles",allProfiles);
			request.setAttribute("allProfilesWithKeys",allProfilesWithKeys);
			form.set("selectedProfiles", user.getProfileNames());
			form.set("divisions",divisionsToMap(user.getAllDivisions()));
			
			
		} catch (EdmException e) {
			errors.add(e.getMessage());
            log.error("Blad", e);
		} finally {			
			DSApi._close();
		}
	}

	private Map<String,String> divisionsToMap(DSDivision[] divisions){
		Map<String, String> divisionsMap = new HashMap<String, String>();
		for(DSDivision division : divisions){
			divisionsMap.put(division.getGuid(), division.getName());
		}
		return divisionsMap;
	}
	
	
	

}
