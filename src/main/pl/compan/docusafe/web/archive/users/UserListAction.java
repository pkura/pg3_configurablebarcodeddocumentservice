package pl.compan.docusafe.web.archive.users;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserSearchBean;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignature;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.office.CaseToCaseAction.CaseToCaseValue;

/**
 * Wy�wietla list� u�ytkownik�w w systemie.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserListAction.java,v 1.25 2010/03/22 15:09:49 mariuszk Exp $
 */
public class UserListAction extends EventDrivenAction
{
    static final int LIMIT = 10;
    private static final Log log = LogFactory.getLog(UserListAction.class);
    private boolean generateSignature = false;
   private List<usersListOption> usersList;
    Integer days ;
    
    private static final String[] eventNames = new String[] {
        "doDefault", "doSearch"
    };
    public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public String[] getEventNames()
    {
        return eventNames;
    }
    public static final String FORWARD = "users/user-list";

    
    private final boolean old = AvailabilityManager.isAvailable("menu.left.olduserlist");
    
    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        final int limit = (form.get("limit") != null && ((Integer) form.get("limit")).intValue()!=0) ? ((Integer) form.get("limit")).intValue() : LIMIT;
        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
        if (offset < 0) offset = 0;
        final String sortField = (String) form.get("sortField");
        final boolean ascending = form.get("ascending") != null ?
            ((Boolean) form.get("ascending")).booleanValue() : true;
            
        if(days==null || days==-1||form.get("days") != null )
        	days = form.get("days") != null ? ((Integer) form.get("days")).intValue() : -1;
        //if(days==-1 && request.getSession().getAttribute("days")!=null)
        	//days = (Integer)request.getSession().getAttribute("days");
        //if(days==-1)
        //	days = (request.getAttribute("days")==null)?-1:(Integer)(request.getAttribute("days"));
        //if(true) throw new Exception(String.valueOf(days));
	    request.setAttribute("days", new Integer(days));
        request.setAttribute("offset", new Integer(offset));
        request.setAttribute("limit", new Integer(limit));
        request.setAttribute("newUserLink", mapping.findForward(UserNewAction.FORWARD).getPath());
        request.setAttribute("showAdv", form.get("showAdv"));
        
     
        try
        {
            DSApi.open(AuthUtil.getSubject(request));
         
            int sortMode;
            if ("name".equals(sortField))
                sortMode = DSUser.SORT_NAME;
            else if ("firstname".equals(sortField))
                sortMode = DSUser.SORT_FIRSTNAME_LASTNAME;
            else if ("lastname".equals(sortField))
                sortMode = DSUser.SORT_LASTNAME_FIRSTNAME;
            else
                sortMode = DSUser.SORT_LASTNAME_FIRSTNAME;

            //SearchResults results = DSUser.getUsers(offset, limit, sortMode, ascending);
            
            SearchResults results = DSUser.getUsers(offset, -1, sortMode, ascending);
            List beans = new ArrayList(results.count());
            
            int nieobecniCounter = 0;
            
            try {
            	if (request.getParameter("generateInternalSignature")!=null)
            		generateSignature = Boolean.parseBoolean(request.getParameter("generateInternalSignature"));
			} catch (Exception e) { }
            usersList = prepareUsersList(request); 
           
            while (results.hasNext())
            {
            	
                DynaBean bean = DynaBeans.bean(DynaBeans.user);
                DSUser user = (DSUser) results.next();
                
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.podpisWewnetrzny") && generateSignature){
                	// generowanie sygnatur uzytkownikow
                	InternalSignature signature = InternalSignature.getInstance();
                	signature.generateSignature(user.getId());
                	// END generowanie sygnatur uzytkownikow
                }
                if(days>0)
                {
                	Date date = new Date();
                	date =  DateUtils.plusDays(date, days*-1);
                	Date tmp = user.getLastSuccessfulLogin();
                	if (tmp==null)
                	    tmp = user.getLastPasswordChange();
                    if (tmp==null || DateUtils.substract(tmp,date)>0) {
                    	nieobecniCounter++;
                    	continue;
                    }
                } 
                
                
                bean.set("name", user.getName());
                bean.set("firstname", user.getFirstname());
                bean.set("lastname", user.getLastname());
                if (!old) {
                	bean.set("editLink", "/users/view-user.do?username="+user.getName());
                } else {
                	bean.set("editLink", UserEditAction.getLink(user.getName(), offset, limit, sortField, true));
                }
                bean.set("deleteLink", DeleteUserAction.getLink(user.getName(), offset, limit, sortField, true));
//                if(AvailabilityManager.isAvailable("kancelaria.wlacz.podpisWewnetrzny"))
//                	bean.set("signature", InternalSignatureManager.findUserSignature(user.getId()).getSignature());

                
                beans.add(bean);


            }
            generateSignature = false;
            List tmpList = new ArrayList();
            for(int j=0;j<beans.size();j++)
            {
            	if((j >= offset && j < offset+limit) || (offset == 0 && limit == 0)) 
            	{
            		tmpList.add(beans.get(j));
            	}
            }
            
            // klasa tworz�ca odno�niki do kolejnych stron wynik�w
            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
            {
                public String getLink(int offset)
                {
                    return UserListAction.getLink(offset, limit, sortField, ascending);
                }
            };

            //Integer test = new Integer(tmpList.size());
            
            Pager pager = new Pager(
                linkVisitor, offset, LIMIT, results.totalCount()-nieobecniCounter, 10);            	
            	//linkVisitor, offset, LIMIT, tmpList.size(), 10);

            request.setAttribute("pager", pager);

            //request.setAttribute("userBeans", beans);
            request.setAttribute("userBeans", tmpList);
            
            

        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            DSApi._close();
        }

        return mapping.findForward("main");
    }
   
	private List prepareUsersList(HttpServletRequest request) throws EdmException {
		// TODO Auto-generated method stub
		List<usersListOption> tmp  = new ArrayList<usersListOption>();
		List<DSUser> dsUsers =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
		for (DSUser dsUser : dsUsers) {
			tmp.add( new usersListOption(dsUser.getLastnameFirstnameWithOptionalIdentifier(),dsUser.getName(),dsUser.getId()));
		}
		createBeanUsersList(tmp, request);
		return tmp;
		
	}
	public void createBeanUsersList(List<usersListOption> list, HttpServletRequest request)
	{
    
        List userBean = new ArrayList(list.size());
        for (int i=0; i < list.size(); i++)
        {
             DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
            bean.set("label", list.get(i).getValue());
            bean.set("value",  list.get(i).getKey());
            userBean.add(bean);
        }
        request.setAttribute("usersList", userBean);
   	
		
	}
	public class usersListOption {
		private String value;
		private String key;
		private long userId;
		 
		public usersListOption() {
			value = "";
			key = "";
			userId= 0;
		}
		public usersListOption(String value, String key ,Long userId) {
			this.value = value;
			this.key = key;
			this.userId = userId;
		}
		public void setValue(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
		public void setKey(String key){
			this.key = key;
		}
		public String getKey(){
			return key;
		}
		public void setcontainerId(long containerId){
			this.userId = containerId;
		}
		public long getcontainerId(){
			return userId;
		}
	}
	
    public ActionForward doSearch(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        final int limit = form.get("limit") != null ? ((Integer) form.get("limit")).intValue() : LIMIT;
        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
        if (offset < 0) offset = 0;
        final String sortField = (String) form.get("sortField");
        final boolean ascending = form.get("ascending") != null ?
            ((Boolean) form.get("ascending")).booleanValue() : true;
        final String query = (String) form.get("query");

        final UserSearchBean queryBean = new UserSearchBean(form);
        if ( form.get("selectedUser")!=null && !((String) form.get("selectedUser")).isEmpty()){
        	try {
				response.sendRedirect("/docusafe/users/view-user.do?username="+(String)form.get("selectedUser"));
			} catch (IOException e) {
				log.error("", e);
			}
        }
        else  if (StringUtils.isEmpty(query) && queryBean.isEmpty())
            return doDefault(mapping, form, eventParameter, request, response, errors, messages);

        int days = form.get("days") != null ? ((Integer) form.get("days")).intValue() : -1;
	    request.setAttribute("days", days);      
	    request.setAttribute("showAdv", form.get("showAdv")); 
        request.setAttribute("offset", new Integer(offset));
        request.setAttribute("limit", new Integer(limit));

        if (old) {
        	request.setAttribute("newUserLink", mapping.findForward(UserNewAction.FORWARD).getPath());
        } 
       
        String selectedUser = "";
		if(form.get("selectetUser")!=null && !((String) form.get("selectetUser")).isEmpty())
			selectedUser  = (String) form.get("selectedUser");
		String a = "asd";

        
        
        try
        {
            DSApi.open(AuthUtil.getSubject(request));

            int sortMode;
            if ("name".equals(sortField))
                sortMode = DSUser.SORT_NAME;
            else if ("firstname".equals(sortField))
                sortMode = DSUser.SORT_FIRSTNAME_LASTNAME;
            else if ("lastname".equals(sortField))
                sortMode = DSUser.SORT_LASTNAME_FIRSTNAME;
            else
                sortMode = DSUser.SORT_LASTNAME_FIRSTNAME;

            SearchResults<DSUser> results = UserFactory.getInstance().searchUsers(offset, limit, sortMode, ascending,
                query, queryBean,false);
            
            List beans = new ArrayList(results.count());

            while (results.hasNext())
            {
                DynaBean bean = DynaBeans.bean(DynaBeans.user);
                DSUser user = (DSUser) results.next();

                if(days>0)
                {
                	Date date = new Date();
                	date =  DateUtils.plusDays(date, days*-1);
                	Date tmp = user.getLastSuccessfulLogin();
                	if(tmp==null)
                		tmp = user.getLastPasswordChange();
                	if(DateUtils.substract(tmp,date)>0)
                		continue;
                }
                
                bean.set("name", user.getName());
                bean.set("firstname", user.getFirstname());
                bean.set("lastname", user.getLastname());
                if (old) {
                	bean.set("editLink", UserEditAction.getLink(user.getName(), offset, limit, sortField, true));
                } else {
                	bean.set("editLink", "/users/view-user.do?username="+user.getName());
                }
                bean.set("deleteLink", DeleteUserAction.getLink(user.getName(), offset, limit, sortField, true));

                beans.add(bean);
            }

            // klasa tworz�ca odno�niki do kolejnych stron wynik�w
            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
            {
                public String getLink(int offset)
                {
                    return UserListAction.getSearchLink(offset, limit, sortField, ascending, query,queryBean);
                }
            };

            Pager pager = new Pager(
                linkVisitor, offset, limit, results.totalCount(), 10);

            request.setAttribute("pager", pager);

            request.setAttribute("userBeans", beans);

        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            DSApi._close();
        }

        return mapping.findForward("main");
    }

    public ActionForward doGenerate(
            ActionMapping mapping,
            DynaActionForm form,
            String eventParameter,
            HttpServletRequest request,
            HttpServletResponse response,
            Messages errors,
            Messages messages) {
    	
				return null;
    	
    }
    
    
    public static String getLink()
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }

    public static String getLink(int offset, int limit, String sortField, boolean ascending)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
            "?offset="+offset+
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField+"&ascending="+ascending: "");
    }
    																		
    public static String getSearchLink(int offset, int limit, String sortField, boolean ascending, String query, UserSearchBean bean)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
            "?doSearch=true"+
            "&query="+HttpUtils.urlEncode(query, "iso-8859-2")+
            bean.getLinkParameters()+
            "&offset="+offset+
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField+"&ascending="+ascending: "");
    }
    									
    public static String getSearchLink(int offset, int limit, String sortField, boolean ascending, String query)
    		    {
    		        ModuleConfig config = (ModuleConfig)
    		            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
    		        ForwardConfig fc = config.findForwardConfig(FORWARD);
    		        return fc.getPath() +
    		            "?doSearch=true"+
    		            "&query="+HttpUtils.urlEncode(query, "iso-8859-2")+
    		            "&offset="+offset+
    		            "&limit="+limit+
    		            (sortField != null ? "&sortField="+sortField+"&ascending="+ascending: "");
    		    }

	public List<usersListOption> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<usersListOption> usersList) {
		this.usersList = usersList;
	}

	 									
}
