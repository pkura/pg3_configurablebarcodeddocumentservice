package pl.compan.docusafe.web.archive.users;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PasswordPolicy;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import std.fun;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserNewAction.java,v 1.41 2010/07/30 11:06:43 kamilj Exp $
 */
public final class UserNewAction extends EventProcessingAction
{
	private final Logger log = LoggerFactory.getLogger(UserNewAction.class);
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String FORWARD = "users/user-new";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doCancel").
            append(new SetActionForwardListener(UserListAction.FORWARD));

        registerListener("doCreate").
            append(setForward).
            append(new Create()).
            append("fillForm", fillForm);
    }

    private final class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final DynaActionForm form = event.getDynaForm();

            String username = (String) form.get("username");
            if (username!=null)
            	username = username.trim();
            
            String firstname = (String) form.get("firstname");
            if (firstname != null)
            	firstname = firstname.trim();
            
            String lastname = (String) form.get("lastname");
            if (lastname != null)
            	lastname = lastname.trim();
            
            String password = (String) form.get("password");
            String password2 = (String) form.get("password2");
            
            String email = (String) form.get("email");
            if (email != null)
            	email = email.trim();
            
            String phoneNum = (String) form.get("phoneNum");
            String mobileNum = (String) form.get("mobileNum");
            String extension = (String) form.get("extension");
            String communicatorNum = (String) form.get("communicatorNum");
            Boolean adUser = (form.get("activeDirectory") == null ? false : (Boolean)form.get("activeDirectory") );
            
            String identifier = (String) form.get("identifier");

            if (StringUtils.isEmpty(username))
                event.getErrors().add(sm.getString("newUser.missingUsername"));
            if (StringUtils.isEmpty(firstname))
                event.getErrors().add(sm.getString("newUser.missingFirstname"));
            if (StringUtils.isEmpty(lastname))
                event.getErrors().add(sm.getString("newUser.missingLastname"));
            if (StringUtils.isEmpty(password) && !adUser)
                event.getErrors().add(sm.getString("newUser.missingPassword"));
            if (password != null && password2 != null && !password.equals(password2))
                event.getErrors().add(sm.getString("newUser.passwordsMismatch"));
            if (!StringUtils.isEmpty(username) && !username.matches("^[a-zA-Z][a-zA-Z0-9]+$"))
                event.getErrors().add(sm.getString("newUser.invalidCharactersInUsername"));
            // w nazwach domen nie mo�e by� podkre�le�, ale dopuszczam je tutaj,
            // bo czasem si� trafiaj�
            if (!StringUtils.isEmpty(email) && !email.matches("^[-a-zA-Z0-9.+,!/_]+@[-a-zA-Z0-9_][-a-zA-Z0-9._]*$"))
                event.getErrors().add(sm.getString("newUser.invalidEmail"));

            if (event.getErrors().size() > 0)
            {
                return;
            }

            DSContext ctx = null;
            boolean success = false;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                // Pozwalamy by 2 u�ytkownikow mia�o ten sam e-mail
                
               /* if (!StringUtils.isEmpty(email))
                {
                    try
                    {
                        // metoda getUserByEmail() rzuca wyj�tek UserNotFoundException, je�eli
                        // u�ytkownik nie zostanie znaleziony
                        if (!DSUser.findByEmail(email).isDeleted())
                            throw new EdmException(sm.getString("newUser.existingEmail", email));
                    }
                    catch (UserNotFoundException e)
                    {
                    }
                }*/

                DSUser user = UserFactory.getInstance().createUser(username, firstname, lastname);

                user.setEmail(email);
                user.setIdentifier(identifier);
                user.setExtension(extension);
                user.setMobileNum(mobileNum);
                user.setPhoneNum(phoneNum);
                user.setCommunicatorNum(communicatorNum);
                user.setCtime(new Timestamp(new Date().getTime()));    
                user.setAdUser(adUser);

                // role w archiwum
                if (form.get("roles") instanceof String[])
                {
                    String[] roles = (String[]) form.get("roles");
                    for (int i=0, n=roles.length; i < n; i++)
                    {
                        user.addRole(roles[i]);
                    }
                }

                AccessLog.logUserCreation(DSApi.context().getPrincipalName(), user.getName());

                // grupy

                if (form.get("groups") instanceof String[])
                {
                    String[] groups = (String[]) form.get("groups");
                    for (int i=0; i < groups.length; i++)
                    {
                        DSDivision group = DSDivision.find(groups[i]);
                        if (group.isGroup())
                            group.addUser(user);
                    }
                    
                }

                // role kancelarii

                if (Configuration.coreOfficeAvailable())
                {
                    if (form.get("officeRoles") instanceof Long[])
                    {
                        Role[] roles = Role.list();
                        Long[] roleIds = (Long[]) form.get("officeRoles");
                        Arrays.sort(roleIds);
                        for (int i=0; i < roles.length; i++)
                        {
                            if (Arrays.binarySearch(roleIds, roles[i].getId()) >= 0)
                            {
                                roles[i].addUser(user.getName());
                            	//roles[i].getUsernames().add(user.getName());
                            }
                            else
                            {
                            	roles[i].removeUser(user.getName());
                            	//roles[i].getUsernames().remove(user.getName());
                            }
                        }
                    }
                }

                // tworzenie obiektu WfResource w wewn�trznym systemie workflow
/*
                if (Configuration.coreOfficeAvailable())
                {
                    pl.compan.docusafe.core.office.workflow.WfService ws =
                        WorkflowServiceFactory.newInternalInstance().newService();
                    ws.connectAdmin();
                    ws.create_resource(username.toLowerCase(), "enhydra", "", email, "");
                    ws.addParticipantToUsernameMapping(
                        null, null, username.toLowerCase(), username.toLowerCase());

                    ws.disconnect();

                    // tworzenie obiektu WfResource w zewn�trznym systemie workflow
                    if (Configuration.workflowAvailable())
                    {
                        pl.compan.docusafe.core.office.workflow.WfService ews =
                            WorkflowServiceFactory.newExternalInstance().newService();
                        ews.connectAdmin();

                        ews.create_resource(user.getName(), "enhydra",
                            "", // bez imienia/nazwiska - polskie znaki powoduj� b��d
                            email != null ? email : "",
                            "");
                        ews.disconnect();
                    }
                }
*/

                user.update();

                user.setPassword(password, true);
                Properties props = (Properties) Docusafe.getServletContext().getAttribute(PasswordPolicy.PROPERTIES_KEY);
                synchronized (Docusafe.getServletContext())
                {
                    if (props == null)
                    {
                        props = PasswordPolicy.getProperties(
                            DSApi.context().systemPreferences().node(PasswordPolicy.NODE_PASSWORD_POLICY));
                        Docusafe.getServletContext().setAttribute(PasswordPolicy.PROPERTIES_KEY, props);
                    }
                }
                try {
                	user.setChangePasswordAtFirstLogon((Boolean) props.get(PasswordPolicy.KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON));
                } 
                catch(Exception e) {
                	log.error(e.getMessage(), e);
                	 event.getErrors().add(e.getMessage());
                }
                DSLog log = new DSLog();
                log.setUsername(DSApi.context().getPrincipalName());
                log.setCode(DSLog.CREATE_USER);
                log.setCtime(new Date());
                log.setParam(user.getName());
               // log.setLparam(user.);
                log.create();
                DocumentKind.resetAllDockindInfo();
                
                ctx.commit();
                success = true;
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
               
            }
            finally
            {
                DSApi._close();
            }

            // wysy�anie listu powitalnego do nowego u�ytkownika
            if (success && !StringUtils.isEmpty(email))
            {
                try
                {
                    Map<String, Object> context = new HashMap<String, Object>();
                    context.put("username", username);
                    context.put("password", password);
                    context.put("loginUrl", Configuration.getBaseUrl());
                    ((Mailer) ServiceManager.getService(Mailer.NAME)).
                        send(email, null, null, Configuration.getMail(Mail.NEW_USER), context);
                }
                catch (Exception e)
                {
                	log.error(e.getMessage(), e);
                    event.getErrors().add("Nie mo�na wys�a� listu do u�ytkownika: "+e.getMessage());
                }
            }

            if (success)
            {
                event.setForward(new ActionForward(event.getMapping().findForward(UserListAction.FORWARD).getPath(), true));
            }
        }
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final DynaActionForm form = event.getDynaForm();

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                // grupy, do kt�rych mo�na doda� u�ytkownika - tylko te
                // znajduj�ce si� na najwy�szym poziomie drzewa organizacji
                DSDivision[] groups = UserFactory.getInstance().getGroups();

                Arrays.sort(groups, DSDivision.NAME_COMPARTOR);
                event.getRequest().setAttribute("groups", groups);


                if (Configuration.coreOfficeAvailable())
                {
                    Collection roleBeans = fun.map(
                        Role.list(), new lambda()
                    {
                        public Object act(Object o)
                        {
                            Role role = (Role) o;
                            DynaBean bean = DynaBeans.bean(DynaBeans.checkbox);
                            bean.set("value", role.getId());
                            bean.set("label", role.getName());
                            //bean.set("checked", Boolean.valueOf(role.getUsernames().contains(user.getName())));
                            return bean;
                        }
                    });

                    event.getRequest().setAttribute("officeRoles", roleBeans);
                }

            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink()
    {
        final ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }
}
