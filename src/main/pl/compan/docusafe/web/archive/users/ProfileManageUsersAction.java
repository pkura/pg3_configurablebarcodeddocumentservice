package pl.compan.docusafe.web.archive.users;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.users.Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class ProfileManageUsersAction
	extends EventDrivenAction
	{ 
	    private static final Log log = LogFactory.getLog(ProfileEditAction.class);
	    private static final String[] eventNames = new String[] {
	        "doDefault", "doUpdate", "doRefreshLeft", "doRefreshRight",
	    };
	    static
	    {
	        Arrays.sort(eventNames);
	    }
	    //public static final String FORWARD = "users/user-edit";
	    private String profilename;
	    private Long profileId;
	    private ArrayList<DSUser> userss;
	    private ArrayList<DSUser> profileUsers;
	    private String[] selectedUsersRight;
	    private String[] selectedUsersLeft;
	    
	    public String[] getEventNames()
	    {
	        return eventNames;
	    }
	    
	    private static final StringManager sm =
	        StringManager.getManager(Constants.Package);

	    public ActionForward doDefault(
	        ActionMapping mapping,
	        DynaActionForm form,
	        String eventParameter,
	        HttpServletRequest request,
	        HttpServletResponse response,
	        Messages errors,
	        Messages messages)
	    {
	        fillForm(null, request, form, mapping, errors);
	        return mapping.findForward("main");
	    }
	
	    public ActionForward doUpdate(
	        ActionMapping mapping,
	        DynaActionForm form,
	        String eventParameter,
	        HttpServletRequest request,
	        HttpServletResponse response,
	        Messages errors,
	        Messages messages)
	    {

	        try
	        {
	           final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
	           ctx.begin();
	           selectedUsersRight = (String[]) form.get("selectedUsersRight");
	           ArrayList<DSUser> selectedUsersRightAL = new ArrayList<DSUser>();
	           for(String username:selectedUsersRight) {
           		selectedUsersRightAL.add(UserFactory.getInstance().findByUsername(username));
           	   }
	      
	           //userss.get(0).asLastnameFirstname()
	           profileId = (Long) form.get("profileId");
	           Profile profile = Finder.find(Profile.class, profileId);         
	           profile.usersOfCollection(selectedUsersRightAL);
	           ctx.commit();
	           messages.add(sm.getString("userEdit.saveData"));
	        }
	        catch (EdmException e)
	        {
	            DSApi.context().setRollbackOnly();
	            errors.add(e.getMessage());
	            log.error("", e);
	        }
	        finally
	        {
	            fillForm(DSApi.context(), request, form, mapping, errors);
	        }

	        return mapping.findForward("main");
	    }

	    private void fillForm(DSContext ctx, HttpServletRequest request, DynaActionForm form,
	                          ActionMapping mapping, Messages errors)
	    {
	        try
	        {
	            if (ctx == null)
	                ctx = DSApi.open(AuthUtil.getSubject(request));
	            profileId = (Long) form.get("profileId");
		        Profile profile = Finder.find(Profile.class, profileId);  
	            profilename = profile.getName();
	            selectedUsersLeft = (String[]) form.get("selectedUsersLeft");
	            selectedUsersRight = (String[]) form.get("selectedUsersRight");
	            //rzuca profilenotfound
	            
            
	            DSUser[] profileUsersAr = profile.getUsers().toArray(new DSUser[profile.getUsers().size()]);
	            DSUser[] usersAr = UserFactory.getInstance().searchUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true, null,false).results();
	            profileUsers = new ArrayList<DSUser>();
	            profileUsers.addAll(Arrays.asList(profileUsersAr));
	            userss = new ArrayList<DSUser>();
	            userss.addAll(Arrays.asList(usersAr));
	            userss.removeAll(profileUsers);
	            
	            request.setAttribute("userss", userss);
	            request.setAttribute("profileUsers",profileUsers);
	            request.setAttribute("profilename", profilename);
	            request.setAttribute("profileId", profile.getId());

	        }
	        catch (EdmException e)
	        {
	            errors.add(e.getMessage());
	            log.error("", e);
	        }
	        finally
	        {
	            DSApi._close();
	        }
	    }


	    public static String getLink(String username, int offset, int limit, String sortField, boolean ascending)
	    {
	    	//TODO
	    	return null;
//	        ModuleConfig config = (ModuleConfig)
//	            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
//	        ForwardConfig fc = config.findForwardConfig(FORWARD);
//	        return fc.getPath() + "?username="+username +
//	            "&offset="+offset+
//	            "&limit="+limit+
//	            "&sortField="+sortField+
//	            "&ascending="+ascending;
	    }

		public ArrayList<DSUser> getUserss() {
			return userss;
		}

		public ArrayList<DSUser> getProfileUsers() {
			return profileUsers;
		}

		public void setSelectedUsersLeft(String[] selectedUsersLeft) {
			this.selectedUsersLeft = selectedUsersLeft;
		}

		public void setSelectedUsersRight(String[] selectedUsersRight) {
			this.selectedUsersRight = selectedUsersRight;
		}

		public String[] getSelectedUsersLeft() {
			return selectedUsersLeft;
		}

		public String[] getSelectedUsersRight() {
			return selectedUsersRight;
		}

		public void setUserss(ArrayList<DSUser> userss) {
			this.userss = userss;
		}

		public void setProfileUsers(ArrayList<DSUser> profileUsers) {
			this.profileUsers = profileUsers;
		}

		public String getProfilename() {
			return profilename;
		}

		public void setProfilename(String profilename) {
			this.profilename = profilename;
		}
}
