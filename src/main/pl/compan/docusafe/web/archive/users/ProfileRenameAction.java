package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class ProfileRenameAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(ProfileEditAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doUpdate"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    //public static final String FORWARD = "users/user-edit";

    public String[] getEventNames()
    {
        return eventNames;
    }
    
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        fillForm(null, request, form, mapping, errors);
        return mapping.findForward("main");
    }
    
    public ActionForward doUpdate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {

        try
        {
           final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
           ctx.begin();

           Profile profile = Finder.find(Profile.class, (Long)form.get("profileId"));//Profile.findByProfilename((String) form.get("profilename"));
           String newName = (String) form.get("newName");
           profile.setName(newName);
           
           ctx.commit();
           request.setAttribute("profilename", profile.getName());
           request.setAttribute("profileId", profile.getId());
           fillForm(DSApi.context(), request, form, mapping, errors);//messages.add(sm.getString("userEdit.saveData"));
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            
        }

        return mapping.findForward("main");
    }


    private void fillForm(DSContext ctx, HttpServletRequest request, DynaActionForm form,
                          ActionMapping mapping, Messages errors)
    {
        try
        {
            if (ctx == null)
                ctx = DSApi.open(AuthUtil.getSubject(request));
            
            Long currentProfile = (Long)form.get("profileId");
            
            Profile profile = Finder.find(Profile.class, currentProfile);
            if (profile != null) {
            	request.setAttribute("profilename", profile.getName());
		        request.setAttribute("profileId", currentProfile);
            	//form.set("profileId", profile.getId());
            	//form.set("profilename", profile.getName());
            }
        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            DSApi._close();
        }
    }
}

