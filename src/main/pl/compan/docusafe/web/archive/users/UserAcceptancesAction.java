package pl.compan.docusafe.web.archive.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

public class UserAcceptancesAction extends EventDrivenAction{

	private final static Logger log = LoggerFactory.getLogger(UserAcceptancesAction.class);
	private static final String[] eventNames = new String[] { "doDefault",
		"doUpdate" };

	
	static {
		Arrays.sort(eventNames);
	}	
	
	public String[] getEventNames() 
	{
		log.trace("UserAcceptancesAction.java:getEventNames()"+eventNames[0]+eventNames[1]);
		return eventNames;
	}
	
	public static final String FORWARD = "users/user-acceptances";	
	
	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages)  {
		log.debug("UserAcceptancesAction.java:doDefault");
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}
	
	private void fillForm(DSContext ctx, HttpServletRequest request,
			DynaActionForm form, ActionMapping mapping, Messages errors) {
		
		try {
			if (ctx == null)
				ctx = DSApi.open(AuthUtil.getSubject(request));
			
			final DSUser user = DSUser.findByUsername((String) form.get("username"));

			request.setAttribute("username", user.getName());
			
			List<AcceptanceCondition> acceptances = AcceptanceCondition.fullUserAcceptances(user);
			ArrayList<AcceptanceConditionBean> acceptancesBeans = new ArrayList<AcceptanceConditionBean>(acceptances.size());
			for(AcceptanceCondition a: acceptances){
				acceptancesBeans.add(new AcceptanceConditionBean(a));
			}
			
			log.debug("UserAcceptancesAction.java:fill form entered");
			request.setAttribute("acceptances",acceptancesBeans);
		} catch (EdmException e) {
			log.error("",e);
		} finally {
			DSApi._close();
		}
	}
}




