package pl.compan.docusafe.web.archive.users;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import java.io.IOException;
import java.util.prefs.Preferences;

import org.apache.struts.action.ActionForward;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SelfChangePasswordAction.java,v 1.19 2010/06/14 17:28:19 mariuszk Exp $
 */
public class SelfChangePasswordAction extends EventProcessingAction
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private static final String EV_CHANGEPASSWORD = "changePassword";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doChangePassword").
            append(new ValidateChangePassword()).
            append(EV_CHANGEPASSWORD, new ChangePassword()).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doCancel", new SetActionForwardListener("home"));
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event) throws IOException
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
            
                AvailabilityManager.initAdds();
                if (AvailabilityManager.isAvailable("uwlodzki"))
                	    	event.getResponse().sendRedirect("https://logowanie.uni.lodz.pl/changepw/");
                      // Uniwersytet ��dzki chcia� mie� zablokowane zmienianie has�a przez u�ytkownik�w z poziomu programu
           
                Preferences prefs = DSApi.context().systemPreferences().
                    node(PasswordPolicy.NODE_PASSWORD_POLICY);

                event.getRequest().setAttribute("minPasswordLength",
                    new Integer(prefs.getInt(PasswordPolicy.KEY_MIN_PASSWORD_LENGTH, PasswordPolicy.KEY_MIN_PASSWORD_LENGTH_DEFAULT)));

                if (prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_DIGITS, false))
                {
                    event.getRequest().setAttribute("minDigits",
                        new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_DIGITS, 0)));
                }
                
                if (prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_LETTERS, false))
                {
                    event.getRequest().setAttribute("minLetters",
                        new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_LETTERS, 0)));
                }
                if (prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SPECIALS, false))
                {
                    event.getRequest().setAttribute("minSpecials",
                        new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SPECIALS, 0)));
                }
                if (prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SMALL_LETTERS, false))
                {
                    event.getRequest().setAttribute("minSmallLetters",
                        new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SMALL_LETTERS, 0)));
                }
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class ValidateChangePassword implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	String oldPassword = (String) event.getDynaForm().get("oldPassword");
            String password = (String) event.getDynaForm().get("password");
            String password2 = (String) event.getDynaForm().get("password2");

            if(oldPassword == null)
            {
            	event.getErrors().add(sm.getString("selfChangePassword.missingOldPassword"));
                event.skip(EV_CHANGEPASSWORD);
                return;
            }
            	
            if (password == null)
            {
                event.getErrors().add(sm.getString("selfChangePassword.missingPassword"));
                event.skip(EV_CHANGEPASSWORD);
                return;
            }

            if (!password.equals(password2))
            {
                event.getErrors().add(sm.getString("selfChangePassword.passwordsMismatch"));
                event.skip(EV_CHANGEPASSWORD);
                return;
            }

/*
            try
            {
                PasswordPolicy.getInstance().validatePassword(
                    AuthUtil.getUserPrincipal(event.getRequest()).getName(), password
                );
            }
            catch (IllegalPasswordException e)
            {
                event.getErrors().add(e.getMessage());
                event.getErrors().addAll(e.getMessages());
                event.skip(EV_CHANGEPASSWORD);
            }
*/
        }
    }

    private class ChangePassword implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String password = (String) event.getDynaForm().get("password");
            String oldPassword = (String) event.getDynaForm().get("oldPassword");
            
            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSApi.context().begin();

                // walidacja has�a tutaj ze wzgl�du na konieczno�� odczytania
                // Preferences 
                PasswordPolicy.validate(
                    AuthUtil.getUserPrincipal(event.getRequest()).getName(),
                    password,
                    DSApi.context().systemPreferences().node(PasswordPolicy.NODE_PASSWORD_POLICY));


                if (!DSApi.context().getDSUser().verifyPassword(oldPassword))
                    throw new IllegalPasswordException("Nieprawid�owe stare has�o");
                
                
                if (DSApi.context().getDSUser().verifyPassword(password))
                    throw new IllegalPasswordException("Nowe has�o musi si� r�ni� od starego");

                DSApi.context().getDSUser().setPassword(password);
//                ctx.getOrganizationHelper().
//                    updateUserPassword(ctx.getUser().getName(), password);

                event.getDynaForm().set("password", null);
                event.getDynaForm().set("password2", null);
                event.getDynaForm().set("username",DSApi.context().getDSUser().getName() );

//                if (ActionContext.getContext().getSession() != null)
//                    ActionContext.getContext().getSession().remove(GlobalConstants.PASSWORD_EXPIRED);


                DSApi.context().commit();
                

                event.getMessages().add(sm.getString("selfChangePassword.passwordChanged"));

                if (event.getRequest().getSession() != null)
                    event.getRequest().getSession().removeAttribute(GlobalConstants.PASSWORD_EXPIRED);

                event.getRequest().setAttribute("passwordChanged", Boolean.TRUE);
               // event.getRequest().setAttribute("username", DSApi.context().getDSUser().getName());
            }
            catch (IllegalPasswordException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
                event.getErrors().addAll(e.getMessages());
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
}
