package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;
import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.impl.AttributeImpl.UseImpl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ApplicationSettingsAction.java,v 1.59 2010/08/13 11:02:12 mariuszk Exp $
 */
public class DeleteUserListAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(DeleteUserListAction.class);
	private  List<DSUser> users;
    private String username;
    public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doRevert").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Revert()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

    }
    
    private class Revert implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.context().begin();
            	UserFactory.getInstance().revertUser(username);
            	DSUserEnumField.reloadForAll();
            	DSApi.context().commit();
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	users = Arrays.asList(UserFactory.getInstance().searchUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true, null, null, true, true,null).results());
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
