package pl.compan.docusafe.web.archive.users;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

/**
 * Used in UserAcceptancesAction
 * @author MSankowski
 */
public class AcceptanceConditionBean{
	
	private String divisionName;
	private String cn;
	private String fieldValue;
	
	
	public AcceptanceConditionBean(AcceptanceCondition base) throws DivisionNotFoundException, EdmException{
		setCn(base.getCn());
		setFieldValue(base.getFieldValue());
		if(base.getDivisionGuid() != null){
			setDivisionName(DSDivision.find(base.getDivisionGuid()).getName());
		}
	}


	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}


	public String getDivisionName() {
		return divisionName;
	}


	public void setCn(String cn) {
		this.cn = cn;
	}


	public String getCn() {
		return cn;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getFieldValue() {
		return fieldValue;
	}
	
}