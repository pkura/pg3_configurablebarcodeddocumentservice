package pl.compan.docusafe.web.archive.users;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

public class NewUserAction extends EventDrivenAction {
    private static final Logger log = LoggerFactory.getLogger(UserPermissionsAction.class);
	private static final String[] eventNames = new String[] { "doDefault",
			"doCreate" };

	static {
		Arrays.sort(eventNames);
	}

	public static final String FORWARD = "main";

	public String[] getEventNames() {
		return eventNames;
	}

	private static final StringManager sm = StringManager
			.getManager(Constants.Package);

	public ActionForward doCreate(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages)
			throws EdmException {
		
		try {
			DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
			ctx.begin();
			
			String newUsername = (String) form.get("newUsername");
			String newFirstname = (String) form.get("newFirstname");
			String newLastname = (String) form.get("newLastname");
			
			if (StringUtils.isEmpty(newFirstname) || StringUtils.isEmpty(newLastname) || StringUtils.isEmpty(newUsername)) {
				throw new EdmException(sm.getString("WszystkiePolaSaObowiazkowe"));
			}
			
			//if (!newUsername.matches("[^[a-zA-Z][a-zA-Z0-9]*"))
			if (!newUsername.matches("[a-zA-Z][a-zA-Z0-9]+"))
				throw new EdmException(sm.getString("newUser.invalidCharactersInUsername"));
			
			DSUser user = UserFactory.getInstance().createUser(newUsername, newFirstname, newLastname);
			user.setCtime(new Timestamp(new Date().getTime()));
			
			ctx.commit();

            log.info("użytkownik utworzony {} przez {}", user.asLastnameFirstnameName(), DSApi.context().getPrincipalName());

			if(AvailabilityManager.isAvailable("newUser.addEmployeeCard") || AvailabilityManager.isAvailable("newUser.addEmployeeCardAndAbsences"))
			{
				AbsenceFactory.createCard(user);				
			}
			ActionForward actionForward = mapping.findForward("success");
			ActionForward newActionForward =  new ActionForward(actionForward.getName(), actionForward.getPath()+"?username="+newUsername, true);
			DSUserEnumField.reloadForAll();
			DataBaseEnumField.reloadForTable("ds_user");
			return newActionForward;
			
		} catch (Exception e)
		{
			errors.add(e.getMessage());
			log.error("Blad", e);
			return mapping.findForward("main");
		} finally {
			DSApi._close();
		}
	}

	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		return mapping.findForward("main");
	}
}