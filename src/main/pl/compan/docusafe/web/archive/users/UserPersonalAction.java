package pl.compan.docusafe.web.archive.users;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.UserAdditionalData;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import pl.compan.docusafe.web.common.form.UpdateUser;
import com.google.common.collect.Lists;

public class UserPersonalAction extends EventDrivenAction {

	private static final Logger log = LoggerFactory.getLogger(UserPermissionsAction.class);

	private static final String[] eventNames = new String[] { "doDefault", "doUpdate" };

	static {
		Arrays.sort(eventNames);
	}

	public static final String FORWARD = "users/user-profiles";

    public String[] getEventNames() {
		return eventNames;
	}

	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	
	public ActionForward doUpdate(ActionMapping mapping, DynaActionForm form, String eventParameter, HttpServletRequest request, HttpServletResponse response, Messages errors, Messages messages) throws EdmException {
		
		DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
		ctx.begin();
		final DSUser user = DSUser.findByUsername((String) form.get("username"));
		
		final String password = (String) form.get("password");
        final String password2 = (String) form.get("password2");
        final String email = (String) form.get("email");
        final String externalName = (String) form.get("externalName");
        final String firstname = (String) form.get("firstname");
        final String lastname = (String) form.get("lastname");
        final String identifier = (String) form.get("identifier");
        final Boolean loginDisabled = (Boolean) form.get("loginDisabled");
        final String phoneNum = (String) form.get("phoneNum");
        final String mobileNum = (String) form.get("mobileNum");
        final String extension = (String) form.get("extension");
        final String communicatorNum = (String) form.get("communicatorNum");
        final String pesel = (String) form.get("pesel");
        final String remarks = (String) form.get("remarks");
        final String roomNum = (String) form.get("roomNum");
        final String supervisorName = (String) form.get("supervisor");
        final Boolean adUser = (form.get("activeDirectory") == null ? false : (Boolean)form.get("activeDirectory") );
        final String adUrl = (String) form.get("adUrl");
        final String mailFooter = (String) form.get("mailFooter");
        
        try {

        	if(StringUtils.isEmpty(lastname) || StringUtils.isEmpty(firstname)) {
        		throw new EdmException(sm.getString("ImieINazwiskoSaobowiazkowe"));
        	}
        	
			DataMartEventBuilder builder = DataMartEventBuilder.create().defaultObjectId(user.getName());

            if(!StringUtils.isEmpty(phoneNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "phoneNum",user.getPhoneNum(), phoneNum);
            	user.setPhoneNum(phoneNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "phoneNum",user.getPhoneNum(), null);
            	user.setPhoneNum(null);
			}
            
            if(!StringUtils.isEmpty(pesel))
            	user.setPesel(pesel);
            else
            	user.setPesel(null);
            
            
            if(!StringUtils.isEmpty(mobileNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "mobileNum",user.getMobileNum(), mobileNum);
            	user.setMobileNum(mobileNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "mobileNum",user.getMobileNum(), null);
            	user.setMobileNum(null);
			}
            
            if(!StringUtils.isEmpty(extension)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "extension",user.getExtension(), extension);
            	user.setExtension(extension);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "extension",user.getExtension(), null);
            	user.setExtension(null);
			}
            	
            if(!StringUtils.isEmpty(communicatorNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "communicatorNum",user.getCommunicatorNum(), communicatorNum);
            	user.setCommunicatorNum(communicatorNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "communicatorNum",user.getCommunicatorNum(), null);
            	user.setCommunicatorNum(null);
			}
            
            if(!StringUtils.isEmpty(remarks))
            	user.setRemarks(remarks);
            else
            	user.setRemarks(null);
            
            if(!StringUtils.isEmpty(roomNum))
            	user.setRoomNum(roomNum);
            else
            	user.setRoomNum(roomNum);

            user.setAdUrl(adUrl);
		
            if(!StringUtils.isEmpty(mailFooter)){
            	UserAdditionalData userAddData = UserAdditionalData.findByUserId(user.getId());
            	if(userAddData != null){
            		userAddData.setMailFooter(mailFooter);
            	}else{
            		userAddData = new UserAdditionalData();
            		userAddData.setUserId(user.getId());
            		userAddData.setMailFooter(mailFooter);
            		userAddData.create();
            	}
            }
            
            UpdateUser.updateUser(
                    (String) form.get("username"),
                    !StringUtils.isEmpty(externalName) ? externalName : null,
                    !StringUtils.isEmpty(firstname) ? firstname : null,
                    !StringUtils.isEmpty(lastname) ? lastname : null,
                    identifier,
                    !StringUtils.isEmpty(email) ? email : null,
                    !StringUtils.isEmpty(password) ? password : null,
                    !StringUtils.isEmpty(password2) ? password2 : null,
                    loginDisabled != null ? loginDisabled.booleanValue() : false,
                    supervisorName, // supervisorUsername
                    user.getSubstituteUser() == null ? null : user.getSubstituteUser().getName() , // substitutedByUsername
                    user.getSubstitutedFrom(), // substitutedFrom
                    user.getSubstitutedThrough(), // substitutedThrough
                    null, //archiveRoles
                    null, //groups
                    null, //officeRoles
                    null,
                    adUser//certLogin
					);
        
		builder.log();
		ctx.commit();
        } catch (EdmException e) {
        	DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
            log.error("", e);
        } finally {
        	fillForm(DSApi.context(), request, form, mapping, errors);
        }
        return mapping.findForward("main");
	}
	
	public ActionForward doDefault(ActionMapping mapping, DynaActionForm form,
			String eventParameter, HttpServletRequest request,
			HttpServletResponse response, Messages errors, Messages messages) {
		fillForm(null, request, form, mapping, errors);
		return mapping.findForward("main");
	}

	private void fillForm(DSContext ctx, HttpServletRequest request, DynaActionForm form, ActionMapping mapping, Messages errors) {

        try
        {
            if (ctx == null)
                ctx = DSApi.open(AuthUtil.getSubject(request));

            String username = (String)form.get("username");
            if (username == null || username.length() == 0) {
            	// from /users/new-user
            	username = (String)request.getAttribute("username");
            }
            final DSUser user = DSUser.findByUsername(username);
            
            request.setAttribute("username", user.getName());
            form.set("username", user.getName());
            form.set("externalName", user.getExternalName());
            form.set("firstname", user.getFirstname());
            form.set("lastname", user.getLastname());
            form.set("identifier", user.getIdentifier());
            form.set("pesel",user.getPesel());
            form.set("email", user.getEmail());
            form.set("password", null);
            form.set("password2", null);
            form.set("loginDisabled", new Boolean(user.isLoginDisabled()));
            form.set("phoneNum", user.getPhoneNum());
            form.set("mobileNum", user.getMobileNum());
            form.set("extension", user.getExtension());
            form.set("communicatorNum", user.getCommunicatorNum());
            form.set("creationTime", user.getCtime()!=null?user.getCtime().toLocaleString():"");	
            form.set("remarks", user.getRemarks());
            form.set("roomNum", user.getRoomNum());
            
            if(AvailabilityManager.isAvailable("mail.footer")){
	            UserAdditionalData userAddData = UserAdditionalData.findByUserId(user.getId());
	            if(userAddData != null){
	            	form.set("mailFooter", userAddData.getMailFooter());
	            }else{
	            	form.set("mailFooter", "");
	            }
            }
            
            boolean isADuser = false;
            if(user.getAdUser() != null){
            	isADuser = user.getAdUser();
            }
            if(isADuser){
                form.set("adUrl", user.getAdUrl());
            } else {
                form.set("adUrl", ActiveDirectoryManager.getDefaultAdUrl());
            }
            if(AvailabilityManager.isAvailable("user.supervisor")){
//                form.set("supervisors", getSupervisors());
                request.setAttribute("supervisors", getSupervisors());
                form.set("supervisor", user.getSupervisor() == null ? "": user.getSupervisor().getName());
            }
            form.set("adUser",Boolean.valueOf(isADuser));
            form.set("activeDirectory", Boolean.valueOf(isADuser));            
            request.setAttribute("activeDirectory",Boolean.valueOf(isADuser));
		} catch (EdmException e) {
			errors.add(e.getMessage());
            log.error("Blad", e);
		} finally {
			DSApi._close();
		}
	}


    public List<DynaBean> getSupervisors() throws EdmException{
        List<DynaBean> ret = Lists.newArrayList();
        for(DSUser user: DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME)){
            DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
            bean.set("value", user.getName());
            bean.set("label", user.asLastnameFirstname());
            ret.add(bean);
        }
        log.info("returning supervisors : {}", ret);
        return ret;
    }
}