package pl.compan.docusafe.web.archive.users;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.upload.FormFile;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.jackrabbit.JackrabbitGroups;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignatureEntity;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignatureManager;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.archive.organization.EditOrganizationAction;
import pl.compan.docusafe.web.common.EventDrivenAction;
import pl.compan.docusafe.web.common.Messages;
import pl.compan.docusafe.web.common.form.UpdateUser;
import static pl.compan.docusafe.util.LoggerFactory.getLogger;

/**
 * Edycja u�ytkownika.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserEditAction.java,v 1.59 2010/02/15 11:06:27 mariuszk Exp $
 */
public class UserEditAction extends EventDrivenAction
{
    private static final Logger log = getLogger(UserEditAction.class);
    private static final String[] eventNames = new String[] {
        "doDefault", "doUpdate", "doCancel", "doSaveCert", "doDeleteCert"
    };
    static
    {
        Arrays.sort(eventNames);
    }
    public static final String FORWARD = "users/user-edit";

    public String[] getEventNames()
    {
        return eventNames;
    }
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public ActionForward doDefault(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        fillForm(null, request, form, mapping, errors);
        return mapping.findForward("main");
    }

	public ActionForward doCancel(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        final int limit = form.get("limit") != null ? ((Integer) form.get("limit")).intValue() : UserListAction.LIMIT;
        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
        if (offset < 0) offset = 0;
        final String sortField = (String) form.get("sortField");

        return new ActionForward(UserListAction.getLink(offset, limit, sortField, true), true);
    }

    public ActionForward doUpdate(
        ActionMapping mapping,
        DynaActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        final int limit = form.get("limit") != null ? ((Integer) form.get("limit")).intValue() : UserListAction.LIMIT;
        int offset = form.get("offset") != null ? ((Integer) form.get("offset")).intValue() : 0;
        if (offset < 0) offset = 0;
        final String sortField = (String) form.get("sortField");

        final String password = (String) form.get("password");
        final String password2 = (String) form.get("password2");
        final String email = (String) form.get("email");
        final String externalName = (String) form.get("externalName");
        final String firstname = (String) form.get("firstname");
        final String lastname = (String) form.get("lastname");
        final String supervisorUsername = (String) form.get("supervisor");
        final String substitutedByUsername = (String) form.get("substitutedBy");
        final String identifier = (String) form.get("identifier");
        final Boolean loginDisabled = (Boolean) form.get("loginDisabled");
        final Boolean certificateLogin = (Boolean) form.get("certificateLogin");
        final String phoneNum = (String) form.get("phoneNum");
        final String mobileNum = (String) form.get("mobileNum");
        final String extension = (String) form.get("extension");
        final String communicatorNum = (String) form.get("communicatorNum");
        final Boolean adUser = (form.get("activeDirectory") == null ? false : (Boolean)form.get("activeDirectory") );
        //System.out.print(phoneNum+"up");

        boolean success = false;
        try
        {
            final DSContext ctx = DSApi.open(AuthUtil.getSubject(request));
            ctx.begin();

            DSUser user = DSUser.findByUsername((String) form.get("username"));

			DataMartEventBuilder builder
					= DataMartEventBuilder.create()
						.defaultObjectId(user.getName());

            if (user.getCertificates().length == 0 &&
                certificateLogin != null && certificateLogin.booleanValue())
                throw new EdmException("Nie mo�na w��czy� opcji logowania przy "+
                    "pomocy certyfikatu, gdy u�ytkownik nie ma certyfikat�w");
            
            if(!StringUtils.isEmpty(phoneNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "phoneNum",user.getPhoneNum(), phoneNum);
            	user.setPhoneNum(phoneNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "phoneNum",user.getPhoneNum(), null);
            	user.setPhoneNum(null);
			}
            
            if(!StringUtils.isEmpty(mobileNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "mobileNum",user.getMobileNum(), mobileNum);
            	user.setMobileNum(mobileNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "mobileNum",user.getMobileNum(), null);
            	user.setMobileNum(null);
			}
            
            if(!StringUtils.isEmpty(extension)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "extension",user.getExtension(), extension);
            	user.setExtension(extension);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "extension",user.getExtension(), null);
            	user.setExtension(null);
			}
            	
            if(!StringUtils.isEmpty(communicatorNum)) {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "communicatorNum",user.getCommunicatorNum(), communicatorNum);
            	user.setCommunicatorNum(communicatorNum);
			} else {
				builder.checkValueChangeEvent(EventType.USER_CHANGE, "communicatorNum",user.getCommunicatorNum(), null);
            	user.setCommunicatorNum(null);
			}
            
            //Sprawdzenie zmian w rolach
            List<String> delRoles = new ArrayList<String>();
            List<String> addRoles = new ArrayList<String>();
            List<Long> delOfficeRoles = new ArrayList<Long>();
            List<Long> addOfficeRoles = new ArrayList<Long>();
            
            List<Long> oldOfficeRoles = Role.listUserRoles((String)form.get("username"));
            Set oldRoles  = user.getRoles();
            String[] newRoles = (String[])form.get("roles");
            Long[] newOfficeRoles = (Long[]) form.get("officeRoles");            
            
           for (int i = 0; i < newRoles.length; i++) 
           {
        	   if(!oldRoles.contains(newRoles[i]))
        	   {
        		   addRoles.add(newRoles[i]);
        	   }
           }
           boolean b = false;
           for (Iterator iter = oldRoles.iterator(); iter.hasNext();) 
           {
        	   String r = (String) iter.next();
        	   for (int i = 0; i < newRoles.length; i++)
				{
					if(newRoles[i].equals(r))
					{
						b=true;
						break;
					}
				}
				if(!b)
				{
					delRoles.add(r);
				}
				b=false;
           }

		   logDeletedRoles(delRoles, form, builder);
		   logAddedRoles(addRoles, form, builder);
   
           for (int i = 0; i < newOfficeRoles.length; i++) 
           {
        	   if(!oldOfficeRoles.contains(newOfficeRoles[i]))
        	   {
        		   addOfficeRoles.add(newOfficeRoles[i]);
        	   }
           }
           b = false;
           if(oldOfficeRoles == null)
           {
        	   oldOfficeRoles = new ArrayList<Long>();
           }
           for (Long r : oldOfficeRoles) 
           {
        	   for (int i = 0; i < newOfficeRoles.length; i++)
				{
					if(newOfficeRoles[i].equals(r))
					{
						b=true;
						break;
					}
				}
				if(!b)
				{
					delOfficeRoles.add(r);
				}
				b=false;
           }
		   
		   logDeletedOfficeRoles(delOfficeRoles, builder, form);
		   logAddedOfficeRoles(addOfficeRoles, builder, form);

		   if(!StringUtils.isEmpty(password) && password.equals(password2)){
			  builder.checkValueChangeEvent(EventType.USER_CHANGE, "password",
				  "?", "??");
		   }

           UpdateUser.updateUser(
                (String) form.get("username"),
                !StringUtils.isEmpty(externalName) ? externalName : null,
                !StringUtils.isEmpty(firstname) ? firstname : null,
                !StringUtils.isEmpty(lastname) ? lastname : null,
                identifier,
                !StringUtils.isEmpty(email) ? email : null,
                !StringUtils.isEmpty(password) ? password : null,
                !StringUtils.isEmpty(password2) ? password2 : null,
                loginDisabled != null ? loginDisabled.booleanValue() : false,
                null, // supervisorUsername
                user.getSubstituteUser() == null ? null : user.getSubstituteUser().getName(), // substitutedByUsername
                user.getSubstitutedFrom(), // substitutedFrom
                user.getSubstitutedThrough(), // substitutedThrough
                (String[])form.get("roles"),
                (String[]) form.get("groups"),
                (Long[]) form.get("officeRoles"),
                certificateLogin != null ? certificateLogin : Boolean.FALSE,adUser
				);

//            if (!StringUtils.isEmpty(password))
//                request.getSession(true).removeAttribute(GlobalConstants.PASSWORD_EXPIRED);

            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
            
            if(AvailabilityManager.isAvailable("jackrabbit.sync.organization")) {
	    		for (Long ltmp : addOfficeRoles) {
	    			Role role = Role.find(ltmp);
	    			for (Iterator iterator = role.getPermissions().iterator(); iterator
							.hasNext();) {
						String strRole = (String) iterator.next();
		                try {

		                    if(strRole.equals(DSPermission.PISMO_WSZYSTKIE_PODGLAD.getName())) {
		                        JackrabbitGroups.instance().addUsers("READ_ALL_DOCS", Arrays.asList(user));
		                    } else {
		                        JackrabbitGroups.instance().deleteUsers("READ_ALL_DOCS", Arrays.asList(user));
		                    }
		                } catch(Exception ex) {
		                    throw new EdmException(EditOrganizationAction.JACKRABBIT_ERROR_STRING, ex);
		                }				
					}
	    		}
            }
            builder.log();

            ctx.commit();

            success = true;
            messages.add(sm.getString("userEdit.saveData"));
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            fillForm(DSApi.context(), request, form, mapping, errors);
        }

//            return mapping.findForward("main");
        if (success)
        {
            messages.add(sm.getString("userEdit.savedUser"));
            return new ActionForward(UserListAction.getLink(offset, limit, sortField, true));
        }
        else
        {
            return mapping.findForward("main");
        }
    }

    public ActionForward doSaveCert(ActionMapping mapping, DynaActionForm form, String eventParameter, HttpServletRequest request, HttpServletResponse response, Messages errors, Messages messages)
    {
        FormFile certFile = (FormFile) form.get("certFile");

        if (certFile == null || certFile.getFileSize() == 0)
        {
            errors.add(sm.getString("userEdit.pustyPlik"));
            return mapping.findForward("main");
        }

        X509Certificate cert;
        try
        {
             cert = (X509Certificate) CertificateFactory.getInstance("X509").
                 generateCertificate(certFile.getInputStream());
        }
        catch (CertificateException e)
        {
            errors.add("Niepoprawny certyfikat (nie w formacie X.509)");
            return mapping.findForward("main");
        }
        catch (IOException e)
        {
            errors.add("B��d odczytu pliku z certyfikatem");
            return mapping.findForward("main");
        }

        try
        {
            DSApi.open(AuthUtil.getSubject(request));
            DSApi.context().begin();

            try
            {
                DSUser user = DSUser.findByCertificate(cert);
                throw new EdmException("Inny u�ytkownik ("+user.getName()+") posiada ju� ten certyfikat");
            }
            catch (UserNotFoundException e)
            {
            }

            DSUser user = DSUser.findByUsername((String) form.get("username"));
            if (!user.addCertificate(cert))
                throw new EdmException("U�ytkownik posiada� ju� ten certyfikat");

            user.update();

            DSApi.context().commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
        }
        finally
        {
            fillForm(DSApi.context(), request, form, mapping, errors);
        }

        return mapping.findForward("main");
    }

    public ActionForward doDeleteCert(ActionMapping mapping, DynaActionForm form,
                                      String eventParameter, HttpServletRequest request,
                                      HttpServletResponse response, Messages errors, Messages messages)
    {
        try
        {
            DSApi.open(AuthUtil.getSubject(request));
            DSApi.context().begin();

            // kontrola przes�anych danych wykonywana jest tutaj, a nie
            // przed try-catch, aby wywo�ana zosta�a funkcja fillForm()
            String[] deleteCerts = (String[]) form.get("deleteCerts");
            if (deleteCerts == null || deleteCerts.length == 0)
                throw new EdmException("Nie wybrano certyfikat�w do usuni�cia");

            DSUser user = DSUser.findByUsername((String) form.get("username"));

            for (int i=0; i < deleteCerts.length; i++)
            {
                try
                {
                    user.removeCertificate(Base64.decodeBase64(deleteCerts[i].getBytes()));
                }
                catch (EdmException e)
                {
                    continue;
                }
            }

            user.update();

            DSApi.context().commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            errors.add(e.getMessage());
        }
        finally
        {
            fillForm(DSApi.context(), request, form, mapping, errors);
        }

        return mapping.findForward("main");
    }

	public void logAddedOfficeRoles(List<Long> addOfficeRoles, DataMartEventBuilder builder, DynaActionForm form) throws EdmException {
		for (Long ltmp : addOfficeRoles) {
			String role = Role.find(ltmp).getName();
			builder.event(EventType.USER_OFFICE_ROLE_ADD).valueChange(null, null, role);
			DSLog l = new DSLog();
			l.setCode(DSLog.ADD_ROLE_KAN);
			l.setCtime(new Date());
			l.setParam((String) form.get("username"));
			l.setLparam(ltmp);
			l.setReason(role);
			l.setUsername(DSApi.context().getPrincipalName());
			l.create();
		}
	}

	public void logAddedRoles(List<String> addRoles, DynaActionForm form, DataMartEventBuilder builder) throws EdmException {
		for (String string : addRoles) {
			builder.event(EventType.USER_ARCHIVE_ROLE_ADD)
				.valueChange(null, null, string);

			DSLog l = new DSLog();
			l.setCode(DSLog.ADD_ROLE_ARCH);
			l.setCtime(new Date());
			l.setParam((String) form.get("username"));
			l.setReason(string);
			//l.setLparam(Long.parseLong(string));
			l.setUsername(DSApi.context().getPrincipalName());
			l.create();
		}
	}

	public void logDeletedOfficeRoles(List<Long> delOfficeRoles, DataMartEventBuilder builder, DynaActionForm form) throws EdmException {
		for (Long ltmp : delOfficeRoles) {
			String role = Role.find(ltmp).getName();
			builder.event(EventType.USER_OFFICE_ROLE_REMOVE).valueChange(null, role, null);
			DSLog l = new DSLog();
			l.setCode(DSLog.DEL_ROLE_KAN);
			l.setCtime(new Date());
			l.setParam((String) form.get("username"));
			l.setLparam(ltmp);
			l.setReason(role);
			l.setUsername(DSApi.context().getPrincipalName());
			l.create();
		}
	}

	public void logDeletedRoles(List<String> delRoles, DynaActionForm form, DataMartEventBuilder builder) throws EdmException {
		for (String string : delRoles) {

			builder.event(EventType.USER_ARCHIVE_ROLE_REMOVE)
				.valueChange(null, string, null);

			DSLog l = new DSLog();
			l.setCode(DSLog.DEL_ROLE_ARCH);
			l.setCtime(new Date());
			l.setParam((String) form.get("username"));
			l.setReason(string);
			//l.setLparam(Long.parseLong(string));
			l.setUsername(DSApi.context().getPrincipalName());
			l.create();
		}
	}

    private void fillForm(DSContext ctx, HttpServletRequest request, DynaActionForm form,
                          ActionMapping mapping, Messages errors)
    {
        try
        {
            if (ctx == null)
                ctx = DSApi.open(AuthUtil.getSubject(request));

            final DSUser user = DSUser.findByUsername((String) form.get("username"));

            form.set("username", user.getName());
            form.set("externalName", user.getExternalName());
            form.set("firstname", user.getFirstname());
            form.set("lastname", user.getLastname());
            form.set("identifier", user.getIdentifier());
            form.set("email", user.getEmail());
            form.set("password", "");
            form.set("password2", null);
            form.set("supervisor", user.getSupervisor() != null ? user.getSupervisor().getName() : null);
            if (user.getSubstituteUser() != null)
                form.set("substitutedBy", user.getSubstituteUser().getName());
            form.set("loginDisabled", Boolean.valueOf(user.isLoginDisabled()));
            form.set("certificateLogin", Boolean.valueOf(user.isCertificateLogin()));
            form.set("phoneNum", user.getPhoneNum());
            form.set("mobileNum", user.getMobileNum());
            form.set("extension", user.getExtension());
            form.set("communicatorNum", user.getCommunicatorNum());
            form.set("creationTime", user.getCtime()!=null?user.getCtime().toLocaleString():"");
            
            InternalSignatureEntity sygnature = InternalSignatureManager.findUserSignature(user.getId());
            form.set("internalSignature", sygnature!=null?sygnature.getSignature():"");
            
            Boolean isADuser = false;
            if(user.getAdUser() != null)
            {
            	isADuser = user.getAdUser();
            }
            form.set("adUser", isADuser);
            form.set("activeDirectory", isADuser);
            request.setAttribute("activeDirectory", isADuser);

//            if (user.getLastPasswordChange() != null &&
//                PasswordPolicy.getInstance().passwordExpires())
//            {
//                request.setAttribute("passwordValidDays", new Integer(PasswordPolicy.getInstance().daysToExpire(user.getLastPasswordChange())));
////                Date validThru = DateUtils.plusDays(user.getLastPasswordChange(), PasswordPolicy.getInstance().passwordValidityDays());
////                request.setAttribute("passwordValidThru", validThru);
////                if (validThru.before(new Date()))
////                    request.setAttribute("passwordValidityWarning", Boolean.TRUE);
//            }

            // dzia�y, w kt�rych znajduje si� u�ytkownik (z wyj�tkiem grup)
            final DSDivision[] divisions = user.getOriginalDivisions();
            final Iterable<DynaBean> divisionBeans = Iterables.transform(
                Iterables.filter(Arrays.asList(divisions), new Predicate<DSDivision>(){
                    public boolean apply(DSDivision dsDivision) {
                        return !dsDivision.isGroup();
                    }
                }),
                new Function<DSDivision, DynaBean>() {
                    public DynaBean apply(DSDivision division) {
                        DynaBean bean = DynaBeans.bean(DynaBeans.division);
                        try {
                            bean.set("guid", division.getGuid());
                            bean.set("prettyPath", division.getPrettyPath());
                            bean.set("position", division.isPosition());
                            return bean;
                        } catch (EdmException e) {
                            throw new NoSuchElementException();
                        }
                    }
            });

            request.setAttribute("divisions", Lists.newArrayList(divisionBeans));

            X509Certificate[] certs = user.getCertificates();
            Map[] certBeans = new Map[certs.length];
            if (certs.length > 0)
            {
                for (int i=0; i < certs.length; i++)
                {
                    Map<String, String> bean = new HashMap<String, String>();
                    bean.put("dn", certs[i].getSubjectDN().getName());
                    bean.put("issuerDn", certs[i].getIssuerDN().getName());
                    try
                    {
                        //String sig = new String(Base64.encodeBase64(certs[i].getSignature()));
                        // sygnatur� nale�y dodatkowo zakodowa�, bo jest przekazywana
                        // jako GET (encoded64 jako post), za� w ci�gu base64
                        // mog� znale�� si� znaki + interpretowane jako spacje
                        bean.put("signature64", HttpUtils.urlEncode(new String(Base64.encodeBase64(certs[i].getSignature()))));
                        bean.put("encoded64", new String(Base64.encodeBase64(certs[i].getEncoded())));
                    }
                    catch (CertificateEncodingException e)
                    {
                        bean.put("encoded64", "");
                        bean.put("signature64", "");
                        log.warn(e.getMessage(), e);
                    }
                    certBeans[i] = bean;
                }
            }
            request.setAttribute("certificates", certBeans);

            // role archiwum

            form.set("roles", user.getRoles().toArray(new String[user.getRoles().size()]));

            // grupy, do kt�rych mo�na doda� u�ytkownika - tylko te
            // znajduj�ce si� na najwy�szym poziomie drzewa organizacji


            DSDivision[] groups = UserFactory.getInstance().getGroups();
            
            Arrays.sort(groups, DSDivision.NAME_COMPARTOR);
            
            request.setAttribute("groups", groups);

            // grupy, w kt�rych u�ytkownik si� znajduje
            String[] groupGuids = Iterables.toArray(Iterables.transform(
                Iterables.filter(Arrays.asList(divisions), new Predicate<DSDivision>(){
                    public boolean apply(DSDivision dsDivision) {
                        return dsDivision.isGroup();
                    }
                }),
                new Function<DSDivision, String>(){
                    public String apply(DSDivision division){
                        return division.getGuid();
                    }
                }), String.class);

            form.set("groups", groupGuids);

            // role kancelarii

            if (AvailabilityManager.isAvailable("officeRoles"))
            {
                // wszystkie role
                Role[] roles = Role.list();
                Collection roleBeans = Lists.transform(
                    Arrays.asList(roles), new Function(){
                    public Object apply(Object o)
                    {
                        Role role = (Role) o;
                        DynaBean bean = DynaBeans.bean(DynaBeans.checkbox);
                        bean.set("value", role.getId());
                        bean.set("label", role.getName());
                        //bean.set("checked", Boolean.valueOf(role.getUsernames().contains(user.getName())));
                        return bean;
                    }
                });

                request.setAttribute("officeRoles", roleBeans);

                // role, w kt�rych u�ytkownik si� znajduje

                Long[] roleIds = Iterables.toArray(Role.listUserRoles(user.getName()), Long.class);

                form.set("officeRoles", roleIds);
            }
        }
        catch (EdmException e)
        {
            errors.add(e.getMessage());
            log.error("", e);
        }
        finally
        {
            DSApi._close();
        }
    }

    /**
     * Zwraca odno�nik do strony edycji danych u�ytkownika.
     * Odno�nik nie posiada �cie�ki kontekstowej.
     */
    public static String getLink(String username, int offset, int limit, String sortField, boolean ascending)
    {
    	
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() + "?username="+username +
            "&offset="+offset+
            "&limit="+limit+
            "&sortField="+sortField+
            "&ascending="+ascending;
    }
}
