package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.util.UrlVisitorWithAnchors;

import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RwaTree.java,v 1.5 2008/07/30 13:17:42 pecet4 Exp $
 */
public class RwaTree
{
    /**
     *
     * @param urlVisitor
     * @param target Wybrane (pod�wietlone) RWA, mo�e by� null.
     * @param contextPath
     * @return
     */
    public static HtmlTree newTree(final UrlVisitor urlVisitor,
                                   final RwaElementVisitor rwaElementVisitor,
                                   final Rwa target,
                                   String contextPath,
                                   final boolean showActive)
    {

        final Object root = new Object();

        pl.compan.docusafe.web.tree.HtmlTreeModel model = new pl.compan.docusafe.web.tree.HtmlTreeModel()
        {
            public Object getRootElement()
            {
                return root;
            }
            
            public String getUrl(Object element)
            {
            	if (urlVisitor instanceof UrlVisitorWithAnchors && element instanceof Rwa) {
            		return urlVisitor.getUrl(element)+"#"+getAnchorName(element);
            	} else {
            		return urlVisitor.getUrl(element);
            	}
                
            }
            
            public String getAnchorName(Object element) {
            	if (element instanceof Rwa) {
            		return String.valueOf(((Rwa)element).getId());
            	} else {
            		return "";
            	}
            }
            
            public List getChildElements(Object element)
            {
                try
                {
                    if (element == root)
                    {
                    	return Rwa.listRoots(showActive);
                    }
                    else
                        return ((Rwa) element).getChildren(showActive);
                }
                catch (EdmException e)
                {
                    return Collections.EMPTY_LIST;
                }
            }

            public boolean isExpanded(Object element)
            {
                return element == root ||
                    (target != null && ((Rwa) element).isAncestor(target));
            }
        };

        HtmlTreeView view = new HtmlTreeView()
        {
            public String getHtml(Object element)
            {
                if (element instanceof Rwa)                	
                {
                		return "<a "+
                        (element == target ? "style='color: red'" : "") +
                        "href='"+rwaElementVisitor.getLink((Rwa) element)+"'>"+
                        rwaElementVisitor.getDescription((Rwa) element)+"</a>";
                }
                else
                {
                    return "<a "+(element == root ? "style='color: red;' " : "") +
                        "href='"+rwaElementVisitor.getLink(null)+"'>"+
                        "Rzeczowy Wykaz Akt</a>";                	
                }
            }
        };

        return new HtmlTree(model, view, contextPath);
    }
}
