package pl.compan.docusafe.web.tree;

import java.util.List;

/**
 * Model drzewa generowanego przez HtmlTree.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeModel.java,v 1.2 2006/02/20 15:42:51 lk Exp $
 */
public interface HtmlTreeModel
{
    /**
     * Zwraca g��wny element drzewa. Null, je�eli g��wny element
     * nie ma by� wy�wietlany.
     * @return G��wny element drzewa lub null.
     */
    Object getRootElement();
    /**
     * Zwraca list� element�w podrz�dnych w stosunku do przekazanego.
     * Zwracana lista mo�e mie� warto�� null, je�eli przekazany element
     * nie ma element�w podrz�dnych.
     * @param element Element, kt�rego dzieci powinny by� zwr�cone przez
     *  t� metod�. Je�eli ma warto�� null, oznacza to, �e metoda getRootElement()
     *  zwr�ci�a warto�� null i metoda powinna zwr�ci� list� dzieci elementu
     *  g��wnego.
     * @return Lista element�w podrz�dnych lub null.
     */
    List getChildElements(Object element);
    /**
     * Zwraca true, je�eli przekazany element powinien by� pokazany
     * jako rozwini�ty. Aby drzewo wygl�da�o dobrze, metoda powinna
     * zwraca� true dla ka�dego elementu znajduj�cego si� na �cie�ce
     * do elementu docelowego.
     */
    boolean isExpanded(Object element);
    /**
     * Odno�nik pokazuj�cy element drzewa zwi�zany z elementem. Ten
     * odno�nik nie jest u�ywany w tytule elementu, tylko w ikonkach
     * rozwijaj�cych i zwijaj�cych fragmenty drzewa.
     */
    String getUrl(Object element);
}
