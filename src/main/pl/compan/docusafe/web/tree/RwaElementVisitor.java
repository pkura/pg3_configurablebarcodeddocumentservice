package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.core.office.Rwa;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RwaElementVisitor.java,v 1.1 2004/08/31 15:11:18 lk Exp $
 */
public interface RwaElementVisitor
{
    String getDescription(Rwa rwa);
    String getLink(Rwa rwa);
}
