package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSDivisionFilter;
import pl.compan.docusafe.util.HtmlImage;
import pl.compan.docusafe.util.HtmlTreeImageVisitor;
import pl.compan.docusafe.util.HtmlTreeStyleVisitor;
import pl.compan.docusafe.util.HtmlTreeTheme;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.util.UrlVisitorWithAnchors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Klasa generuje drzewo odpowiadaj�ce strukturze organizacji
 * korzystaj�c z klasy {@link pl.compan.docusafe.util.HtmlTree}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OrganizationTree.java,v 1.16 2009/02/18 15:11:54 pecet1 Exp $
 */
public class OrganizationTree
{
	
	/**
     * Zwraca drzewo reprezentuj�ce struktur� organizacji.
     * <p>
     * Uwaga: metoda HtmlTree.generateTree() mo�e rzuci� wyj�tek
     * RuntimeException, je�eli wyst�pi b��d podczas pobierania
     * listy podrz�dnych dzia��w.
     * @param target
     * @param request
     * @param showPositions
     * @param showGroups
     * @return
     * @throws EdmException
     */
	public static pl.compan.docusafe.util.HtmlTree newTree(final DSDivision target,
            final UrlVisitor urlVisitor,
            final HttpServletRequest request,
            final boolean showPositions,
            final boolean showGroups,
            final boolean showSwimlanes) throws EdmException
            {
		 		return newTree(target, null, urlVisitor, request, showPositions, showGroups, showSwimlanes);
            }
	
	
    public static pl.compan.docusafe.util.HtmlTree newTree(final DSDivision target, final List<DSDivision> rootChild,
                                   final UrlVisitor urlVisitor,
                                   final HttpServletRequest request,
                                   final boolean showPositions,
                                   final boolean showGroups,
                                   final boolean showSwimlanes)
        throws EdmException
    {
    	
    	final DSDivision root = DSDivision.find(DSDivision.ROOT_GUID);
        final List rootDivisions = Arrays.asList(root.getChildren(new DSDivisionFilter()
        {
            public boolean accept(DSDivision division)
            {
            	
                boolean result =  division.isDivision() ||
                    (showPositions && division.isPosition()) ||
                    (showGroups && division.isGroup()) ||
                    (showSwimlanes && division.isSwimlane());
                if (rootChild != null) {
                	result = result && rootChild.contains(division);
                }
                return result;
            }
        }));

        // lista rozwini�tych dzia��w
        final List expanded = new LinkedList();
        if (target != null) {
			DSDivision _tmp = target;
			do {
				if (_tmp.isHidden()) {//ochrona przed p�tl� niesko�czon�
					expanded.clear();
					expanded.add(DSDivision.find(DSDivision.ROOT_GUID));
					break;
				}
				expanded.add(_tmp);
				_tmp = _tmp.getParent();

			} while (_tmp != null);
		}

        HtmlTreeStyleVisitor styleVisitor = new HtmlTreeStyleVisitor()
        {
            public String getStyleClass(Object element)
            {
                if (element != null && element.equals(target))
                    return "tree_target";
                else
                    return null;
            }
        };

        HtmlTreeImageVisitor imageVisitor = new HtmlTreeImageVisitor()
        {
            public HtmlImage getIconImage(Object element, boolean expanded)
            {
                if (element != null)
                {
                    DSDivision division = (DSDivision) element;
                    if (division.isPosition())
                        return new HtmlImage("/img/folder-position.gif", 17, 17);
                    else if (division.isGroup())
                        return new HtmlImage("/img/folder-group.gif", 17, 17);
                    else if (division.isSwimlane())
                        return new HtmlImage("/img/folder-swimlane.gif", 17, 17);
                }

                return null;
            }
        };

        pl.compan.docusafe.util.HtmlTreeModel treeModel =
            new pl.compan.docusafe.util.HtmlTreeModel()
        {
        	public String getAnchorName(Object element) {
            	return getDivision(element);
            }
        	
        	public String getDivision(Object element)
        	{
        		return ((DSDivision) element).getGuid();
        	}
        	
            public Object getRootElement()
            {
                return root;
            }

            public String getUrl(Object element)
            {
            	if (urlVisitor instanceof UrlVisitorWithAnchors) {
            		return urlVisitor.getUrl(element)+"#"+getAnchorName(element);
            	} else {
            		return urlVisitor.getUrl(element);
            	}
            }

            public String getTitle(Object element)
            {
                DSDivision division = (DSDivision) element;
                return division.getName() +
                    (division.getCode() != null ? " ("+division.getCode()+")" : "");
            }

            public boolean hasChildElements(Object element)
            {
                List children = getChildElements(element);
                return children != null && children.size() > 0;
            }

            public List getChildElements(Object element)
            {
                if (element == root)
                {
                    return rootDivisions;
                }
                else
                {
                    try
                    {
                        DSDivision division = (DSDivision) element;
                        List subdivisionList = new LinkedList();

                        // zwyk�e dzia�y
                        DSDivision[] subdivisions = division.getChildren(new DSDivisionFilter()
                        {
                            public boolean accept(DSDivision division)
                            {
                                return division.isDivision();
                            }
                        });

                        subdivisionList.addAll(Arrays.asList(subdivisions));

                        if (showPositions)
                        {
                            DSDivision[] positions = division.getChildren(new DSDivisionFilter()
                                {
                                    public boolean accept(DSDivision division)
                                    {
                                        return division.isPosition();
                                    }
                                });
                            subdivisionList.addAll(Arrays.asList(positions));
                        }

                        if (showGroups)
                        {
                            DSDivision[] groups = division.getChildren(new DSDivisionFilter()
                                {
                                    public boolean accept(DSDivision division)
                                    {
                                        return division.isGroup();
                                    }
                                });
                            subdivisionList.addAll(Arrays.asList(groups));
                        }

                        if (showSwimlanes)
                        {
                            DSDivision[] swimlanes = division.getChildren(new DSDivisionFilter()
                                {
                                    public boolean accept(DSDivision division)
                                    {
                                        return division.isSwimlane();
                                    }
                                });
                            subdivisionList.addAll(Arrays.asList(swimlanes));
                        }
                        
                        return subdivisionList;
                    }
                    catch (EdmException e)
                    {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                }
            }

            public boolean isExpanded(Object element)
            {
                return expanded.contains(element);
            }

            public HtmlTreeTheme getTheme()
            {
                return HtmlTreeTheme.STANDARD;
            }

			public boolean isSelected(Object element) {
				// TODO Auto-generated method stub
				return false;
			}
			
			public Long getIdRequest()
			{ return new Long(-1); }
        };

        pl.compan.docusafe.util.HtmlTree htmlTree =
            new pl.compan.docusafe.util.HtmlTree(treeModel, request.getContextPath());
        if (target != null)
            htmlTree.setStyleVisitor(styleVisitor);
        htmlTree.setImageVisitor(imageVisitor);

        return htmlTree;
    }

    private OrganizationTree()
    {
    }
}
