package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.core.office.FinishOrAssignState;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.HtmlUtils;

public class AssignmentTreeView extends OrganizationTreeView{
	
	private final boolean hasExecutePlanned;
	
	private final FinishOrAssignState finishOrAssignState;

	public AssignmentTreeView(OrganizationUrlProvider urlProvider,
			String contextPath, DSDivision selectedDivision, DSUser selectedUser, boolean hasExecutePlanned, FinishOrAssignState finishOrAssignState) {
		super(urlProvider, contextPath, selectedDivision, selectedUser);
		this.hasExecutePlanned = hasExecutePlanned;
		this.finishOrAssignState = finishOrAssignState;
	}
	
	//href="javascript:void(openAssignmentWindow('<ww:property value="targetDivision.guid"/>', null, <ww:property value="hasExecutePlanned"/>, <ww:property value="finishOrAssignState.canAssign"/>))"
	
	@Override
	public String getDivisionHtml(OrganizationTreeNode node) {
		String assignmentLink =
				HtmlUtils.toHtmlAnchor("javascript:void(openAssignmentWindow('" 
					+ node.getDivision().getGuid()
					+ "', null, " + hasExecutePlanned
					+ ","
					+ (finishOrAssignState == null || finishOrAssignState.isCanAssign())
					+ "))",
				' ' + HtmlUtils.toImg(contextPath + "/img/document-move.gif", 
						sm.getString("dekretuj"),
						sm.getString("dekretuj")));
		return super.getDivisionHtml(node) + assignmentLink;
	}
	
	
	@Override
	public String getUserHtml(OrganizationTreeNode node) {
		String assignmentLink = HtmlUtils.toHtmlAnchor("javascript:void(openAssignmentWindow('"
				+ node.getDivision().getGuid()
				+ "','"
				+ node.getUser().getName()
				+ "', "
				+ hasExecutePlanned
				+ ","
				+ (finishOrAssignState == null || finishOrAssignState.isCanAssign())
				+ "))",
				' ' + HtmlUtils.toImg(contextPath + "/img/document-move.gif", sm.getString("dekretuj"), sm.getString("dekretuj")));//" [" + sm.getString("dekretuj") + "]");
		return super.getUserHtml(node) + assignmentLink;
	}
}
