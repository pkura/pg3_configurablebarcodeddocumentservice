package pl.compan.docusafe.web.tree.organization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.DivisionImpl;

public class OrganizationTreeJBPMEditorXMLGenerator {
	
	private final String ROOT_CONTAINER_CELL_ID = "ROOT_CONTAINER_CELL_ID";
	private final String ROOT_PARENT_CELL_ID = "ROOT_PARENT_CELL_ID";
	private final String TAB_ID = "com";
	private final String TAB_STYLE = "organizationName";
	private String TAB_VALUE;
	private final String USER_SN = "Organizational Person";
	private final String USER_STYLE = "organizationalPerson";
	private final String GEOMETRY_AS = "geometry";
	private final String DIVISION = "organizationalUnit";
	private final String GROUP = "groupOfNames";
	private final String POSITION = "organizationalRole";
	private final String TOP = "top";
	private final String EDGE = "organizationFlow";
	private final String ROOT_DIVISION = "organization";
	
	private EntryElement entry;
	private EntryElement.ObjectClass objectClass;
	private MXCellElement mxCell;
	private MXGeometryElement mxGeometry;
	private MXGeometryElement.MXPointElement mxPointSource;
	private MXGeometryElement.MXPointElement mxPointTarget;
	private String entryType;
	private List<MXCellElement> entriesConnectionsList = new ArrayList<MXCellElement>();
	private int elementsNumberOfMostPopulatedTreeLevel;
	private int verticalShift = 0;
		
	private static final int MXCELL_ENTRY_NODE = 1;
	private static final int MXCELL_CONNECTION_NODE = 2;
	private static final int MXCELL_PARENT_NODE = 3;
	private static final int MXCELL_CONNECTABLE_NODE = 4;
	private static final int MXCELL_CONTAINER_NODE = 5;
	
	private final int TRUE = 1;
	private final int FALSE = 0;
	
	private Document xmlDocument;
	private Map<Integer, Integer> structureDepthToBreadth = new HashMap<Integer, Integer>();

	public String generateXML(DSDivision root) throws IOException, EdmException{
		if(root != null){
			prepareOrganizationStructure(root, 2, 0);
			generateXMLDocument();
			return addStandaloneAttributeToDocument(xmlDocument);
		}else
			throw new EdmException("ERROR: nie odnaleziono roota struktury organizacyjnej");
	}
	
	private Element createDocumentCore(){
		for(int childrenNumber : structureDepthToBreadth.values()){
			if(elementsNumberOfMostPopulatedTreeLevel == 0)
				elementsNumberOfMostPopulatedTreeLevel = childrenNumber;
			else if(elementsNumberOfMostPopulatedTreeLevel < childrenNumber){
				elementsNumberOfMostPopulatedTreeLevel = childrenNumber;
			}
		}
		
		xmlDocument = DocumentHelper.createDocument(); 
		OrganizationGraphModelElement organizationGraphModelElement = new OrganizationGraphModelElement();
		OrganizationGraphModelElement.PageElement pageElement = organizationGraphModelElement.new PageElement();
		OrganizationGraphModelElement.RootElement rootElement = organizationGraphModelElement.new RootElement();
		pageElement.setBackground("#FFFFFF");
		pageElement.setHeight(verticalShift + structureDepthToBreadth.size() * MXGeometryElement.iconVerticalSize);
		pageElement.setWidth(elementsNumberOfMostPopulatedTreeLevel * MXGeometryElement.iconHorizontalSize);
		pageElement.setCount(""+1);
		pageElement.setHorizontalCount(""+1);
		pageElement.setOrientation(""+0);
		MXCellElement containerMXCell = new MXCellElement();
		containerMXCell.setId(ROOT_CONTAINER_CELL_ID);
		MXCellElement parentMXCell = new MXCellElement();
		parentMXCell.setId(ROOT_PARENT_CELL_ID);
		parentMXCell.setParent(ROOT_CONTAINER_CELL_ID);
		MXCellElement connectableMXCell = new MXCellElement();
		connectableMXCell.setId(TAB_ID);
		connectableMXCell.setConnectable(FALSE);
		connectableMXCell.setParent(ROOT_PARENT_CELL_ID);
		connectableMXCell.setStyle(TAB_STYLE);
		connectableMXCell.setValue(TAB_VALUE);
		connectableMXCell.setVertex(TRUE);
		MXGeometryElement mxGeometry = new MXGeometryElement();
		mxGeometry.setAs(GEOMETRY_AS);
		mxGeometry.setX("28.0");
		mxGeometry.setY("28.0");
		mxGeometry.setHeight(35);
		mxGeometry.setWidth(400);
		connectableMXCell.setMxGeometry(mxGeometry);
		rootElement.setConnectableMXCell(connectableMXCell);
		rootElement.setContainerMXCell(containerMXCell);
		rootElement.setParentMXCell(parentMXCell);
		organizationGraphModelElement.setPageElement(pageElement);
		organizationGraphModelElement.setRootElement(rootElement);
		organizationGraphModelElement.addRootNodeToXMLDocument(xmlDocument);
		return rootElement.getRootXMLNode();
	}
	
	private void generateXMLDocument() throws IOException{
		Element rootElement = createDocumentCore();
		generateEntriesElements(rootElement);
	}
	
//	#TODO do ewentualnego poprawienia, sprawdzic czy istnieje mozliwosc ustawienia atrybutu standalone przy pomocy api dom4j
	private String addStandaloneAttributeToDocument(Document document){
//		<?xml version="1.0" encoding="WINDOWS-1250" standalone="yes"?>
//		jbpm editor requires following pairs of attr="value": encoding="WINDOWS-1250" standalone="yes"
		document.setXMLEncoding("WINDOWS-1250");
		String xmlContent = document.asXML();
		String contentToModify = xmlContent.substring(xmlContent.indexOf("<?"), xmlContent.indexOf("?>")) + " standalone=\"yes\"?>";
		String properContent = xmlContent.substring(xmlContent.indexOf("?>")+2); 
		return contentToModify + properContent;
	}
	
	private void prepareOrganizationStructure(DSDivision division, int treeLevel, int numberOfCountedChildrenOnCurrentTreeLevel) throws EdmException, IOException{
		int currentElementNumber = numberOfCountedChildrenOnCurrentTreeLevel;
		int subDivisionChildren;
		int allCurrentAndPreviousChildren = 0;
		EntryElement parent = null;
		EntryElement child = null;

		if(division.isRoot()){
			TAB_VALUE = division.getName();
			parent = createDivisionEntry(division, 1, 1);
			structureDepthToBreadth.put(1, 1);
		}
		else
			parent = entriesConnectionsList.get(entriesConnectionsList.size()-1).getTarget();
		
		
		for(DSUser user : division.getUsers()){
			currentElementNumber += 1;
			child = createUserEntry(user, division.getName(), currentElementNumber, treeLevel);
			connectEntries(parent, child);
		}
		
		if(division.getChildren().length == 0){ //nie ma dzieci (subdywizji)
			if(structureDepthToBreadth.size() < treeLevel) // nie rozpoczeto zliczania elementow na tym poziomie drzewa
				structureDepthToBreadth.put(treeLevel, division.getUsers().length);
			else //rozpoczeto zliczanie elementow na tym poziomie drzewa
				structureDepthToBreadth.put(treeLevel, structureDepthToBreadth.get(treeLevel) + division.getUsers().length);
		}else{
			if(structureDepthToBreadth.size() < treeLevel)
				structureDepthToBreadth.put(treeLevel, (division.getUsers().length + division.getChildren().length));
			else
				structureDepthToBreadth.put(treeLevel, structureDepthToBreadth.get(treeLevel) + (division.getUsers().length + division.getChildren().length));
			for(DSDivision subDivision: division.getChildren()){
				if(!subDivision.isHidden()){
					currentElementNumber += 1;
					child = createDivisionEntry(subDivision, currentElementNumber, treeLevel);
					connectEntries(parent, child);
					prepareOrganizationStructure(subDivision, (treeLevel+1), allCurrentAndPreviousChildren);
					subDivisionChildren = subDivision.getUsers().length + subDivision.getChildren().length;
					allCurrentAndPreviousChildren += subDivisionChildren;
				}
			}
		}
	}
		
	private void generateEntriesElements(Element rootElement) throws IOException{
		List<String> visitedEntriesIds = new ArrayList<String>();
		for(MXCellElement mxCell : entriesConnectionsList){
			EntryElement parent = mxCell.getSource();
			EntryElement child = mxCell.getTarget();
			
			if(!visitedEntriesIds.contains(parent.getId())){
				setupEntryCoordinates(parent).addEntryToXMLNode(rootElement);
				visitedEntriesIds.add(parent.getId());
			}if(!visitedEntriesIds.contains(child.getId())){
				setupEntryCoordinates(child).addEntryToXMLNode(rootElement);
				visitedEntriesIds.add(child.getId());
			}
			mxCell.addMXCellToXMLNode(rootElement, MXCELL_CONNECTION_NODE);
		}
	}
//	#TODO do ewentualnego poprawienia, aktualnie wspolrzedne nie sa w pelni dobrze ustawiane
	private EntryElement setupEntryCoordinates(EntryElement entry) throws IOException{
		int elementTreeLevel;
		int elementsNumberForGivenTreeLevel;
		int elementPositionNumberInItsTreeLevel;
		double firstElementPositionForGivenTreeLevel;
		// element's top left corner position
		double positionX;
		double positionY;
		elementTreeLevel = entry.getMxCell().getMxGeometry().getLogicalPositionY();
		elementsNumberForGivenTreeLevel = structureDepthToBreadth.get(elementTreeLevel);
		elementPositionNumberInItsTreeLevel = entry.getMxCell().getMxGeometry().getLogicalPositionX();
		firstElementPositionForGivenTreeLevel = ((elementsNumberOfMostPopulatedTreeLevel - elementsNumberForGivenTreeLevel) * MXGeometryElement.iconHorizontalSize) / 2;
		positionX = firstElementPositionForGivenTreeLevel + (MXGeometryElement.iconHorizontalSize * elementPositionNumberInItsTreeLevel) + MXGeometryElement.ENTRY_DEFAULT_PADDING_X;
		positionY = verticalShift + (elementTreeLevel-1) * MXGeometryElement.iconVerticalSize + MXGeometryElement.ENTRY_DEFAULT_PADDING_Y;
		entry.getMxCell().getMxGeometry().setX(""+positionX);
		entry.getMxCell().getMxGeometry().setY(""+positionY);
		return entry;
	}

	private EntryElement createUserEntry(DSUser child, String divisionName, int childNumber, int structureDepth) throws IOException{
//		user / organizational person / pracownik
//		creating organizational person
		entry = new EntryElement();
		entry.setId(child.getName() + divisionName);
		entry.setCn(child.getFirstname() + " " + child.getLastname());
		entry.setSn(USER_SN);
		entry.setEntryType(USER_STYLE);
		entry.setDocusafeId(child.getName());
		objectClass = entry.new ObjectClass();
		objectClass.setName(USER_SN);
		entry.addObjectClass(objectClass);
		objectClass = entry.new ObjectClass();
		objectClass.setName("person");
		entry.addObjectClass(objectClass);
		objectClass = entry.new ObjectClass();
		objectClass.setName("inetOrgPerson");
		entry.addObjectClass(objectClass);
		objectClass = entry.new ObjectClass();
		objectClass.setName("top");
		entry.addObjectClass(objectClass);
		
		mxCell = new MXCellElement();
		mxCell.setParent(ROOT_PARENT_CELL_ID);
		mxCell.setStyle(USER_STYLE);
		mxCell.setVertex(TRUE);
		mxGeometry = new MXGeometryElement();
		mxGeometry.setAs(GEOMETRY_AS);
		mxGeometry.setHeight(MXGeometryElement.ENTRY_DEFAULT_HIGHT);
		mxGeometry.setWidth(MXGeometryElement.ENTRY_DEFAULT_WIDTH);
//		XY
		mxGeometry.setLogicalPositionX(childNumber);
		mxGeometry.setLogicalPositionY(structureDepth);
		mxCell.setMxGeometry(mxGeometry);
		
		entry.setMxCell(mxCell);
		
//		end of organizational person creation
		return entry;
	}
	
	private EntryElement createDivisionEntry(DSDivision division, int childNumber, int structureDepth) throws IOException{
//	 	division types:
//		division / organizational unit  / dzia� (zawiera dzia�y oraz pracownik�w)
//		group    / organizational group / grupa (zawiera wy��cznie pracownik�w)
//	 	position / organizational role  / stanowisko (zawiera pracownik�w) czym r�ni si� stanowisko od grupy (mo�e jest unikatowe dla danego pracownika?)	
//		creating organizational unit/group/role
		entry = new EntryElement();
		
		if(division.isRoot()){
			entryType = ROOT_DIVISION;
			entry.setDc(division.getName());
			entry.setO(division.getName());
		}
		else if(division.getDivisionType().equals("division")){
			entryType = DIVISION;
			entry.setOu(division.getName());
		}
		else if(division.getDivisionType().equals("group")){
			entryType = GROUP;
			entry.setCn(division.getName());
		}
		else if(division.getDivisionType().equals("position")){
			entryType = POSITION;
			entry.setCn(division.getName());
		}
		
		entry.setId(division.getName());
		entry.setDocusafeId(division.getGuid());
		entry.setEntryType(entryType);
		objectClass = entry.new ObjectClass();
		objectClass.setName(entryType);
		entry.addObjectClass(objectClass);
		if(division.isRoot()){
			objectClass = entry.new ObjectClass();
			objectClass.setName("dcObject");
		}else{
			objectClass = entry.new ObjectClass();
			objectClass.setName(TOP);
		}
		entry.addObjectClass(objectClass);
		mxCell = new MXCellElement();
		mxCell.setParent(ROOT_PARENT_CELL_ID);
		if(division.isRoot())
			entryType +="Root";
		mxCell.setStyle(entryType);
		mxCell.setVertex(TRUE);
		mxGeometry = new MXGeometryElement();
		mxGeometry.setAs(GEOMETRY_AS);
		mxGeometry.setHeight(MXGeometryElement.ENTRY_DEFAULT_HIGHT);
		mxGeometry.setWidth(MXGeometryElement.ENTRY_DEFAULT_WIDTH);
		mxCell.setMxGeometry(mxGeometry);
		mxGeometry.setLogicalPositionX(childNumber);
		mxGeometry.setLogicalPositionY(structureDepth);
		entry.setMxCell(mxCell);
		return entry;
	}
	
	private void connectEntries(EntryElement parent, EntryElement child){
//		creating edge between division and assosiated user
		mxCell = new MXCellElement();
		mxCell.setId(parent.getId() + " TO " + child.getId());
		mxCell.setEdge(TRUE);
		mxCell.setStyle(EDGE);
		mxCell.setParent(ROOT_PARENT_CELL_ID);
		mxCell.setSource(parent);
		mxCell.setTarget(child);
		mxGeometry = new MXGeometryElement();
		mxGeometry.setRelative(TRUE);
		mxPointSource = mxGeometry.new MXPointElement();
		mxPointSource.setAs("sourcePoint");
		mxPointTarget = mxGeometry.new MXPointElement();
		mxPointTarget.setAs("targetPoint");
		mxGeometry.setSourcePoint(mxPointSource);
		mxGeometry.setTargetPoint(mxPointTarget);
		mxCell.setMxGeometry(mxGeometry);
		mxCell.setValue("");
//		end of edge creation
		entriesConnectionsList.add(mxCell);
	}
	
	private class OrganizationGraphModelElement{
		
		private final String ORGANIZATION_GRAPH_MODEL_ELEMENT_NAME = "OrganizationGraphModel"; 
		private PageElement pageElement;
		private RootElement rootElement;
		
		public void addRootNodeToXMLDocument(Document document){
			Element organizationGraphModel = document.addElement(ORGANIZATION_GRAPH_MODEL_ELEMENT_NAME);
			getPageElement().addNodeToXMLElement(organizationGraphModel);		
			getRootElement().addNodeToXMLElement(organizationGraphModel);	
		}
		
		public PageElement getPageElement() {
			return pageElement;
		}

		public void setPageElement(PageElement pageElement) {
			this.pageElement = pageElement;
		}

		public RootElement getRootElement() {
			return rootElement;
		}

		public void setRootElement(RootElement rootElement) {
			this.rootElement = rootElement;
		}

		private class PageElement{
			private String background;
			private String count;
			private String horizontalCount;
			private String orientation;
			private double height;
			private double width;
			
			
			public PageElement() {
				super();
				// TODO Auto-generated constructor stub
			}

			public void addNodeToXMLElement(Element element){
				element.addElement("page")
					.addAttribute("background", getBackground())
					.addAttribute("height", ""+getHeight())
					.addAttribute("width", ""+getWidth())
					.addAttribute("count", ""+getCount())
					.addAttribute("horizontalcount", ""+getHorizontalCount())
					.addAttribute("orientation", ""+getOrientation());
			}
			
			public String getBackground() {
				return background;
			}
			public void setBackground(String background) {
				this.background = background;
			}
			public String getCount() {
				return count;
			}
			public void setCount(String count) {
				this.count = count;
			}
			public String getHorizontalCount() {
				return horizontalCount;
			}
			public void setHorizontalCount(String horizontalCount) {
				this.horizontalCount = horizontalCount;
			}
			public String getOrientation() {
				return orientation;
			}
			public void setOrientation(String orientation) {
				this.orientation = orientation;
			}
			public double getHeight() {
				return height;
			}
			public void setHeight(double height) {
				this.height = height;
			}
			public double getWidth() {
				return width;
			}
			public void setWidth(double width) {
				this.width = width;
			}
		}
		
		private class RootElement{
			private MXCellElement containerMXCell;
			private MXCellElement parentMXCell;
			private MXCellElement connectableMXCell;
			
			private Element rootElement;
			public void addNodeToXMLElement(Element element){
				rootElement = element.addElement("root");
				getContainerMXCell().addMXCellToXMLNode(rootElement, MXCELL_CONTAINER_NODE);
				getParentMXCell().addMXCellToXMLNode(rootElement, MXCELL_PARENT_NODE);
				getConnectableMXCell().addMXCellToXMLNode(rootElement, MXCELL_CONNECTABLE_NODE);
			}
			
			public MXCellElement getContainerMXCell() {
				return containerMXCell;
			}
			public void setContainerMXCell(MXCellElement containerMXCell) {
				this.containerMXCell = containerMXCell;
			}
			public MXCellElement getParentMXCell() {
				return parentMXCell;
			}
			public void setParentMXCell(MXCellElement parentMXCell) {
				this.parentMXCell = parentMXCell;
			}
			public MXCellElement getConnectableMXCell() {
				return connectableMXCell;
			}
			public void setConnectableMXCell(MXCellElement connectableMXCell) {
				this.connectableMXCell = connectableMXCell;
			}
			
			public Element getRootXMLNode(){
				return rootElement;
			}
		}
	}

	private class MXCellElement{
		/*
		Cells are the elements of the graph model. They represent the state
		of the groups, vertices and edges in a graph.
		*/
		private String mxCellElem = "mxCell";
		
//		attributes of mxCell element
		/**
		 * Holds the Id. Default is null.
		 */
		private String id;
		/**
		 * Holds the user object. Default is null.
		 */
		private String value = "value";
		/**
		 * Reference to the parent cell and source and target terminals for edges.
		 */
		private String parent;
		private EntryElement source;
		private EntryElement target;
		/**
		 * Holds the child cells and connected edges.
		 */
		private String children = "children";
		private String edges = "edges";
		/**
		 * Holds the style as a string of the form
		 * stylename[;key=value]. Default is null.
		 */
		private String style = "style";
		
		/**
		 * Holds the geometry. Default is null.
		 */
		private MXGeometryElement mxGeometry;
		
		
		/**
		 * Specifies whether the cell is a vertex or edge and whether it is
		 * connectable, visible and collapsed. Default values are false, false,
		 * true, true and false respectively.
		 */
		private int vertex;
		private int edge;
		private int connectable;
		private int visible;
		private int collapsed;
		

		public void addMXCellToXMLNode(Element node, int nodeType){
			Element mxCell = node.addElement("mxCell");
//			if(nodeType == MXCELL_ENTRY_NODE){
//				mxCell 
//					.addAttribute("parent", ROOT_PARENT_CELL_ID)
//					.addAttribute("style", getStyle())
//					.addAttribute("vertex", ""+getVertex());
//				mxGeometry.addMXGeometryToXMLNode(mxCell, nodeType);
//			}else if (nodeType == MXCELL_CONNECTION_NODE){
//				mxCell
//					.addAttribute("edge", ""+getEdge())
//					.addAttribute("id", getId())
//					.addAttribute("parent", ROOT_PARENT_CELL_ID)
//					.addAttribute("source", getSource().getId())
//					.addAttribute("style", getStyle())
//					.addAttribute("target", getTarget().getId())
//					.addAttribute("value", getValue());
//				mxGeometry.addMXGeometryToXMLNode(mxCell, nodeType);
//			}
			switch(nodeType){
				case MXCELL_ENTRY_NODE: 
					mxCell 
						.addAttribute("parent", ROOT_PARENT_CELL_ID)
						.addAttribute("style", getStyle())
						.addAttribute("vertex", ""+getVertex());
					mxGeometry.addMXGeometryToXMLNode(mxCell, nodeType);
					break;
				case MXCELL_CONNECTION_NODE:
					mxCell
					.addAttribute("edge", ""+getEdge())
						.addAttribute("id", getId())
						.addAttribute("parent", ROOT_PARENT_CELL_ID)
						.addAttribute("source", getSource().getId())
						.addAttribute("style", getStyle())
						.addAttribute("target", getTarget().getId())
						.addAttribute("value", getValue());
					mxGeometry.addMXGeometryToXMLNode(mxCell, nodeType);
					break;
				case MXCELL_CONTAINER_NODE:
					mxCell
						.addAttribute("id", ROOT_CONTAINER_CELL_ID);
					break;
				case MXCELL_PARENT_NODE:
					mxCell
						.addAttribute("id", ROOT_PARENT_CELL_ID)
						.addAttribute("parent", ROOT_CONTAINER_CELL_ID);
					break;
				case MXCELL_CONNECTABLE_NODE:
					mxCell
						.addAttribute("id", TAB_ID)
						.addAttribute("parent", ROOT_PARENT_CELL_ID)
						.addAttribute("connectable", ""+FALSE)
						.addAttribute("style", TAB_STYLE)
						.addAttribute("value", TAB_VALUE)
						.addAttribute("vertex", ""+TRUE);
					getMxGeometry().addMXGeometryToXMLNode(mxCell, MXCELL_CONNECTABLE_NODE);
					break;
			}
		}
		
		public String getMxCellElem() {
			return mxCellElem;
		}
		public void setMxCellElem(String mxCellElem) {
			this.mxCellElem = mxCellElem;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getParent() {
			return parent;
		}
		public void setParent(String parent) {
			this.parent = parent;
		}
		public EntryElement getSource() {
			return source;
		}
		public void setSource(EntryElement source) {
			this.source = source;
		}
		public EntryElement getTarget() {
			return target;
		}
		public void setTarget(EntryElement target) {
			this.target = target;
		}
		public String getChildren() {
			return children;
		}
		public void setChildren(String children) {
			this.children = children;
		}
		public String getEdges() {
			return edges;
		}
		public void setEdges(String edges) {
			this.edges = edges;
		}
		public String getStyle() {
			return style;
		}
		public void setStyle(String style) {
			this.style = style;
		}
		public MXGeometryElement getMxGeometry() {
			return mxGeometry;
		}
		public void setMxGeometry(MXGeometryElement mxGeometry) {
			this.mxGeometry = mxGeometry;
		}
		public int getVertex() {
			return vertex;
		}
		public void setVertex(int vertex) {
			this.vertex = vertex;
		}
		public int getEdge() {
			return edge;
		}
		public void setEdge(int edge) {
			this.edge = edge;
		}
		public int getConnectable() {
			return connectable;
		}
		public void setConnectable(int connectable) {
			this.connectable = connectable;
		}
		public int getVisible() {
			return visible;
		}
		public void setVisible(int visible) {
			this.visible = visible;
		}
		public int getCollapsed() {
			return collapsed;
		}
		public void setCollapsed(int collapsed) {
			this.collapsed = collapsed;
		}
	}
	
  	private class MXGeometryElement{

  		public final static int ENTRY_DEFAULT_HIGHT = 40;
		public final static int ENTRY_DEFAULT_WIDTH = 40;
		public final static int ENTRY_DEFAULT_PADDING_Y = 120;
		public final static int ENTRY_DEFAULT_PADDING_X = 60;
		public final static int iconHorizontalSize = MXGeometryElement.ENTRY_DEFAULT_WIDTH + 2 * MXGeometryElement.ENTRY_DEFAULT_PADDING_X;
		public final static int iconVerticalSize = MXGeometryElement.ENTRY_DEFAULT_HIGHT + 2 * MXGeometryElement.ENTRY_DEFAULT_PADDING_Y;
		
		private double height = ENTRY_DEFAULT_HIGHT;
		private double width = ENTRY_DEFAULT_WIDTH;
		private String as = "geometry";
		private String x = "x";
		private String y = "y";
		private MXPointElement sourcePoint;
		private MXPointElement targetPoint;
		private int relative;
		
		private int logicalPositionX;
		private int logicalPositionY;
		
		
		public void addMXGeometryToXMLNode(Element element, int nodeType){
			Element mxGeometry = element.addElement("mxGeometry");
			
			switch(nodeType){
				case MXCELL_ENTRY_NODE:
					mxGeometry
						.addAttribute("as", getAs())
						.addAttribute("height", ""+MXGeometryElement.ENTRY_DEFAULT_HIGHT)
						.addAttribute("width", ""+MXGeometryElement.ENTRY_DEFAULT_WIDTH)
						.addAttribute("x", getX())
						.addAttribute("y", getY());
					break;
				case MXCELL_CONNECTABLE_NODE:
					mxGeometry
						.addAttribute("as", getAs())
						.addAttribute("height", ""+MXGeometryElement.ENTRY_DEFAULT_HIGHT)
						.addAttribute("width", ""+MXGeometryElement.ENTRY_DEFAULT_WIDTH)
						.addAttribute("x", getX())
						.addAttribute("y", getY());
					break;
				case MXCELL_CONNECTION_NODE: 
					mxGeometry
						.addAttribute("as", getAs())
						.addAttribute("relative", ""+getRelative());
					getSourcePoint().addMXPointToXMLNode(mxGeometry);
					getTargetPoint().addMXPointToXMLNode(mxGeometry);
					break;
			}
		}
		
		public int getLogicalPositionX() {
			return logicalPositionX;
		}

		public void setLogicalPositionX(int logicalPositionX) {
			this.logicalPositionX = logicalPositionX;
		}
		
		public int getLogicalPositionY() {
			return logicalPositionY;
		}


		public void setLogicalPositionY(int logicalPositionY) {
			this.logicalPositionY = logicalPositionY;
		}
		
		public String getAs() {
			return as;
		}

		public void setAs(String as) {
			this.as = as;
		}

		public double getHeight() {
			return height;
		}

		public void setHeight(double height) {
			this.height = height;
		}

		public double getWidth() {
			return width;
		}

		public void setWidth(double width) {
			this.width = width;
		}

		public String getX() {
			return x;
		}

		public void setX(String x) {
			this.x = x;
		}

		public String getY() {
			return y;
		}

		public void setY(String y) {
			this.y = y;
		}

		public MXPointElement getSourcePoint() {
			return sourcePoint;
		}

		public void setSourcePoint(MXPointElement sourcePoint) {
			this.sourcePoint = sourcePoint;
		}

		public MXPointElement getTargetPoint() {
			return targetPoint;
		}

		public void setTargetPoint(MXPointElement targetPoint) {
			this.targetPoint = targetPoint;
		}

		
		public int getRelative() {
			return relative;
		}

		public void setRelative(int relative) {
			this.relative = relative;
		}


		private class MXPointElement{
			private String as;
			private String x;
			private String y;
			
			private int logicalPositionX;
			private int logicalPositionY;
			
			
			public void addMXPointToXMLNode(Element element){
				Element mxPoint = element.addElement("mxPoint")
					.addAttribute("as", getAs())	
					.addAttribute("x", getX())
					.addAttribute("y", getY());
			}
			
			public int getLogicalPositionX() {
				return logicalPositionX;
			}
			public void setLogicalPositionX(int logicalPositionX) {
				this.logicalPositionX = logicalPositionX;
			}
			public int getLogicalPositionY() {
				return logicalPositionY;
			}
			public void setLogicalPositionY(int logicalPositionY) {
				this.logicalPositionY = logicalPositionY;
			}
			public String getAs() {
				return as;
			}
			public void setAs(String as) {
				this.as = as;
			}
			public String getX() {
				return x;
			}
			public void setX(String x) {
				this.x = x;
			}
			public String getY() {
				return y;
			}
			public void setY(String y) {
				this.y = y;
			}
		}
  	}

  	private class EntryElement{
		private String cn;
		private String id;
		private String sn;
		private String ou;
		private String o;
		private String dc;
		private List<ObjectClass> objectClassList;
		private MXCellElement mxCell;
		private MXGeometryElement mxGeometry;
		private String entryType;
		private String docusafeId;
		
		public void addEntryToXMLNode(Element node){
			Element entryNode = node.addElement("entry");
			entryNode.addAttribute("docusafe_id", getDocusafeId());
//			if entry is type of division
			if((getO() != null && !getO().equals("")) || (getOu() != null && !getOu().equals(""))){
				if(getEntryType().equals(ROOT_DIVISION)){
					entryNode
						.addAttribute("dc",	getDc())
						.addAttribute("o",	getO());
				}
				else if(getEntryType().equals(DIVISION)){
					entryNode
						.addAttribute("ou",	getOu());
				}else{
					entryNode
						.addAttribute("cn",	getCn());
				}
				
				for(ObjectClass objectClass : getObjectClassList()){
					objectClass.addObjectClassToXMLNode(entryNode);
				}
				entryNode.addAttribute("id", getId());
				getMxCell().addMXCellToXMLNode(entryNode, MXCELL_ENTRY_NODE);
			}else{
				 entryNode
					 .addAttribute("cn", getCn())
					 .addAttribute("id", getId())
					 .addAttribute("sn", getSn());
				 for(ObjectClass objectClass : getObjectClassList()){
						objectClass.addObjectClassToXMLNode(entryNode);
				 }
				 getMxCell().addMXCellToXMLNode(entryNode, MXCELL_ENTRY_NODE);
			}
		}
		
		
		public String getDocusafeId() {
			return docusafeId;
		}


		public void setDocusafeId(String docusafeId) {
			this.docusafeId = docusafeId;
		}


		public String getO() {
			return o;
		}


		public void setO(String o) {
			this.o = o;
		}


		public String getDc() {
			return dc;
		}


		public void setDc(String dc) {
			this.dc = dc;
		}


		public String getEntryType() {
			return entryType;
		}


		public void setEntryType(String entryType) {
			this.entryType = entryType;
		}


		public String getOu() {
			return ou;
		}


		public void setOu(String ou) {
			this.ou = ou;
		}


		public String getCn() {
			return cn;
		}


		public void setCn(String cn) {
			this.cn = cn;
		}


		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getSn() {
			return sn;
		}


		public void setSn(String sn) {
			this.sn = sn;
		}


		public List<ObjectClass> getObjectClassList() {
			return objectClassList;
		}


		public void setObjectClassList(List<ObjectClass> objectClassList) {
			this.objectClassList = objectClassList;
		}


		public void addObjectClass(ObjectClass o){
			if(objectClassList != null)
				objectClassList.add(o);
			else{
				objectClassList = new ArrayList<ObjectClass>();
				objectClassList.add(o);
			}
		}
		
		public void removeObjectClass(ObjectClass o){
			if(objectClassList != null)
				objectClassList.remove(o);
		}
		
		public MXCellElement getMxCell() {
			return mxCell;
		}


		public void setMxCell(MXCellElement mxCell) {
			this.mxCell = mxCell;
		}


		public MXGeometryElement getMxGeometry() {
			return mxGeometry;
		}


		public void setMxGeometry(MXGeometryElement mxGeometry) {
			this.mxGeometry = mxGeometry;
		}


		private class ObjectClass{
			private String name;

			
			public void addObjectClassToXMLNode(Element element){
				element.addElement("objectClass").addAttribute("name", getName());
			}
			
			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}
			
			@Override
			public int hashCode(){
				return name.hashCode();
			}
			
			@Override
			public boolean equals(Object o){
				if(o == null)                return false;
			    if(!(o instanceof ObjectClass)) return false;

			    ObjectClass other = (ObjectClass) o;
			    if(this.name != other.name)      return false;
			    
			    return true;
			}
	
		}
	}
}




