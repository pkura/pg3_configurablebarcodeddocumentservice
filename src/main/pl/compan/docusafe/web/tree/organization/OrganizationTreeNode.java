package pl.compan.docusafe.web.tree.organization;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;



public interface OrganizationTreeNode {
	
	List getChildren() throws EdmException;		

	/**
	 * Zwraca rozwiniety dzial, ktory jest najnizej w hierarchii
	 */
	DSDivision getDivision();
	
	/**
	 * Zwraca usera ktory reprezentuje ten node, moze zwrocic null
	 * @return
	 */
	DSUser getUser();
	
	/**
	 * Zwraca informację czy dany user w danym dziale jest backupem
	 * @return
	 */
	boolean isBackup();
}
