package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.HtmlTheme;
import pl.compan.docusafe.web.common.HtmlUtils;

/**
 * Widok dla drzewa w strukturze organizacujnej (zak�adka organizacja)
 * @author MSankowski
 *
 */
public class AdminTreeView extends OrganizationTreeView{

	static final Logger log = LoggerFactory.getLogger(AdminTreeView.class);
	public AdminTreeView(OrganizationUrlProvider urlProvider,
			String contextPath, DSDivision selectedDivision, DSUser selectedUser) {
		super(urlProvider, contextPath, selectedDivision, selectedUser);		
	}
	
	public String getUserHtml(OrganizationTreeNode node){
		//StringBuilder out = new StringBuilder();	
		//String link = urlProvider.getUrl(node.getDivision(), node.getUser());	
		
		String userAnchor = HtmlUtils.toHtmlAnchor(contextPath + "/users/view-user.do?username="+ node.getUser().getName(), 
								HtmlUtils.toImgWithClass(contextPath+ HtmlTheme.gotoUserIcon().getSrc(),
										sm.getString("przejdzDoUzytkownika"), 
										sm.getString("przejdzDoUzytkownika"), "zoomImgTree"));
		String substituted;
		try {
			substituted =  node.getUser().getSubstituteUser()!= null ?
						" ("
							+ sm.getString("zastepowanyPrzez")
							+ " "
							+ node.getUser().getSubstituteUser().asFirstnameLastname()
							+ ")"
						:"";
		} catch(Exception e){
			log.error(e.getMessage(),e);
			substituted = "";
		}				
		
		String checkbox = "";		
		//DSDivision.isInDivisionAsBackup(division, user)
		if(node.getDivision().equals(selectedDivision)){
			checkbox = HtmlUtils.toCheckbox("selectedUsers", node.getUser().getName()) ;
		}
		
		if(!node.isBackup()){
			return checkbox + 
				HtmlUtils.toSpan(node.getUser().asFirstnameLastname())		
				+ " " + userAnchor
				+ HtmlUtils.toSpan(substituted, "italic");
		} else {
			return checkbox + 
				HtmlUtils.toSpan(node.getUser().asFirstnameLastname(),"backup")
				+ " " + userAnchor
				+ HtmlUtils.toSpan(substituted, "italic");
		}
	}

}
