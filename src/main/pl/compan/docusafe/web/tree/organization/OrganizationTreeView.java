package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.HtmlImage;
import pl.compan.docusafe.util.HtmlTreeTheme;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.HtmlTheme;
import pl.compan.docusafe.web.common.HtmlUtils;
import pl.compan.docusafe.web.tree.ExtendedHtmlTreeView;

/**
 * Prosta implementacja ExtendedTreeView. Wy�wietla dzia�y (nazwa jest linkiem rozszerzaj�cym drzewo) i u�ytkownik�w (jako zwyk�y tekst). 
 * @author MSankowski
 *
 */
public class OrganizationTreeView implements ExtendedHtmlTreeView{
	static final Logger log = LoggerFactory.getLogger(OrganizationTreeView.class);
	protected static StringManager sm = GlobalPreferences.loadPropertiesFile(OrganizationTreeView.class.getPackage().getName(),null);

	protected OrganizationUrlProvider urlProvider;

	protected DSDivision selectedDivision;

	protected final String contextPath;

	public OrganizationTreeView(OrganizationUrlProvider urlProvider, String contextPath, DSDivision selectedDivision, DSUser selectedUser){
		this.urlProvider = urlProvider;
		this.selectedDivision = selectedDivision;
		this.contextPath = contextPath;
	}

	final public String getHtml(Object element) {
		OrganizationTreeNode node = (OrganizationTreeNode) element;				

		if(node.getUser() != null){
			return getUserHtml(node);
		} else {	
			return getDivisionHtml(node);
		}
	}

	public String getDivisionHtml(OrganizationTreeNode node){
		String string =node.getDivision().getName()
				+ (node.getDivision().getCode() != null? " [" + node.getDivision().getCode() + "]":"") ;
		if(node.getDivision().equals(selectedDivision)){
			return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision(), node.getUser()), string , "active-link");
		} else {
			return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision(), node.getUser()), string);
		}
	}

	/**
	 * Zwraca html usera 
	 * @param node we�e� z userem 
	 * @return wygenerowany html
	 */
	public String getUserHtml(OrganizationTreeNode node){
		//StringBuilder out = new StringBuilder();	
		//String link = urlProvider.getUrl(node.getDivision(), node.getUser());				

		String substituted;
		try {
			substituted =  node.getUser().getSubstituteUser()!= null ?
						" ("
							+ sm.getString("zastepowanyPrzez")
							+ " "
							+ node.getUser().getSubstituteUser().asLastnameFirstname()
							+ ")"
						:"";
		} catch(Exception e){
			log.error(e.getMessage(),e);
			substituted = "";
		}

		if(!node.isBackup()){
			return HtmlUtils.toSpan(node.getUser().asLastnameFirstname())
				+ substituted;
		} else {
			return HtmlUtils.toSpan(node.getUser().asLastnameFirstname(),"backup")	
				+ substituted;
		}
	}

	public HtmlImage getIcon(OrganizationTreeNode node, boolean expanded){
		if (node.getUser() != null) {
			return HtmlTheme.userIcon();
		} else {
			if (node.getDivision().isGroup()) {
				return HtmlTheme.groupIcon();
			} else if(node.getDivision().isPosition()){
				return HtmlTheme.positionIcon();
			} else {
				if (expanded) {
					return HtmlTreeTheme.STANDARD.getExpandedFolderImage();
				} else {
					return HtmlTreeTheme.STANDARD.getCollapsedFolderImage();
				}
			}
		}
	}

	public final HtmlImage getIcon(Object element, boolean expanded) {
		OrganizationTreeNode node = (OrganizationTreeNode) element;
		return getIcon(node, expanded);
	}
}
