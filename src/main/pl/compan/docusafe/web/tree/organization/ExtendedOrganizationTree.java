package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.FinishOrAssignState;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.web.tree.HtmlTree;
import pl.compan.docusafe.web.tree.HtmlTreeModel;
import pl.compan.docusafe.web.tree.HtmlTreeView;
import pl.compan.docusafe.web.tree.acceptance.AcceptanceOrganizationTreeView;
import pl.compan.docusafe.web.tree.acceptance.AcceptanceOrganizationUrlProvider;
import pl.compan.docusafe.web.tree.acceptance.AcceptancesOrganizationTreeModel;



/**
 * Rozszerzona struktura organizacji - zawiera tak�e u�ytkownik�w
 * @author MSankowski 
 */
public class ExtendedOrganizationTree extends HtmlTree{

	private ExtendedOrganizationTree(HtmlTreeModel model, HtmlTreeView view,
			String contextPath) {
		super(model, view, contextPath);
	}

    public static HtmlTree  createOrganizationTree(DSDivision selectedDivision, DSUser selectedUser, OrganizationUrlProvider provider, OrganizationTreeView view, String contextPath) throws EdmException{
		return new ExtendedOrganizationTree(
                new OrganizationTreeModel(provider, selectedDivision, selectedUser),
				view,
                contextPath);
	}

    public static HtmlTree  createOrganizationTree(OrganizationUrlProvider provider, OrganizationTreeView view, String contextPath) throws EdmException{
		return new ExtendedOrganizationTree(
                new OrganizationTreeModel(provider, null, null),
				view,
                contextPath);
	}

	public static HtmlTree createOrganizationTree(OrganizationUrlProvider provider, String contextPath) throws EdmException{
		return new ExtendedOrganizationTree(
                new OrganizationTreeModel(provider, null, null),
				new OrganizationTreeView(provider, contextPath, null, null),
                contextPath);
	}

	public static HtmlTree createOrganizationTree(DSDivision selectedDivision, DSUser selectedUser, OrganizationUrlProvider provider, String contextPath) throws EdmException{
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, selectedUser),
				new OrganizationTreeView(provider, contextPath, selectedDivision, selectedUser), contextPath);
	}
	
	public static HtmlTree createAdminTree(DSDivision selectedDivision, DSUser selectedUser, OrganizationUrlProvider provider, String contextPath) throws EdmException{		
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, selectedUser), 
				new AdminTreeView(provider, contextPath, selectedDivision, selectedUser), contextPath);
	}
	
	public static HtmlTree createOrganizationTreeViewToContacts(DSDivision selectedDivision, DSUser selectedUser, OrganizationUrlProvider provider, String contextPath) throws EdmException{		
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, selectedUser), 
				new OrganizationTreeViewToContacts(provider, contextPath, selectedDivision, selectedUser), contextPath);
	}
	
	public static HtmlTree createAssignmentTree(DSDivision selectedDivision, DSUser selectedUser, boolean hasExecutePlanned, FinishOrAssignState finishOrAssignState, OrganizationUrlProvider provider, String contextPath) throws EdmException{		
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, selectedUser), 
				new AssignmentTreeView(provider, contextPath, selectedDivision, selectedUser, hasExecutePlanned, finishOrAssignState), contextPath);
	}

	public static HtmlTree createAssignmentTree(DSDivision selectedDivision, DSUser selectedUser, boolean hasExecutePlanned, OrganizationUrlProvider provider, String contextPath) throws EdmException{
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, selectedUser),
				new AssignmentTreeView(provider, contextPath, selectedDivision, selectedUser, hasExecutePlanned, null), contextPath);
	}
	
	public static HtmlTree createSelectSupervisorOrganizationTree(DSDivision selectedDivision, DSUser currentUser, OrganizationUrlProvider provider, String contextPath) throws EdmException {
		return new ExtendedOrganizationTree(new OrganizationTreeModel(provider, selectedDivision, currentUser), 
				new SelectSupervisorOrganizationTreeView(provider, contextPath, selectedDivision, currentUser), 
				contextPath);
	}
	
	public static HtmlTree createAcceptancesOrganizationTree(AcceptanceDivisionImpl selectedDivision, AcceptanceOrganizationUrlProvider urlProvider, String contextPath) throws EdmException
	{
		return new ExtendedOrganizationTree(
				new AcceptancesOrganizationTreeModel(urlProvider, selectedDivision),
				new AcceptanceOrganizationTreeView( urlProvider,  contextPath,  selectedDivision),
				contextPath
		);
	}
}
