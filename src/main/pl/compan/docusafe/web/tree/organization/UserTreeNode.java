package pl.compan.docusafe.web.tree.organization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public class UserTreeNode implements OrganizationTreeNode{
	
	private final DSDivision division;
	private final DSUser base;
	private final boolean backup;
	
	public UserTreeNode(DSUser user, DSDivision division){
		this.division = division;
		this.base = user;		
		this.backup = false;
	}
	
	public UserTreeNode(DSUser user, DSDivision division, boolean backup){
		this.division = division;
		this.base = user;		
		this.backup = backup;
	}

	@SuppressWarnings("unchecked")
	public List getChildren() throws EdmException {
		return Collections.EMPTY_LIST;
	}

	public DSDivision getDivision() {		
		return division;
	}

	public String getName() {
		return base.asFirstnameLastname();
	}

	public DSUser getUser() {
		return base;
	}
	
	public static List fromArray(DSUser[] users, DSDivision div){
		Arrays.sort(users, DSUser.LASTNAME_COMPARATOR);
		List ret = new ArrayList(users.length);
		for(DSUser user: users){
			ret.add(new UserTreeNode(user, div));
		}
		return ret;
	}
	
	public static List fromArray(DSUser[] users, DSDivision div, boolean backup){
		Arrays.sort(users, DSUser.LASTNAME_COMPARATOR);
		List ret = new ArrayList(users.length);
		for(DSUser user: users){
			ret.add(new UserTreeNode(user, div, backup));
		}
		return ret;
	}

	public boolean isBackup() {
		return backup;
	}
}
