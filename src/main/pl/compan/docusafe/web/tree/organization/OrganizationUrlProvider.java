package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public interface OrganizationUrlProvider {

	/**
	 * zwraca url do strony, kt�ra ma wyswietlic drzewo ktore bedzie mialo zaznaczony odpowiedni node
	 * @param div najni�szy rozwini�ty dzial (= wybrany dzial gdy user == null)
	 * @param user zaznaczony uzytkownik (moze byc null)
	 * @return
	 */
	String getUrl(DSDivision div, DSUser user);
		
}
