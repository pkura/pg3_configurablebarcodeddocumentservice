package pl.compan.docusafe.web.tree.organization;

import java.util.Set;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.HtmlUtils;

public class SelectSupervisorOrganizationTreeView extends OrganizationTreeView 
{
	private DSUser currentUser;

	public SelectSupervisorOrganizationTreeView(OrganizationUrlProvider urlProvider,
			String contextPath, DSDivision selectedDivision, DSUser currentUser) 
	{
		super(urlProvider, contextPath, selectedDivision, currentUser);
		this.currentUser = currentUser;
	}

	/**
	 * Zwraca html usera 
	 * @param node we�e� z userem 
	 * @return wygenerowany html
	 */
	@Override
	public String getUserHtml(OrganizationTreeNode node)
	{
		//StringBuilder out = new StringBuilder();	
		//String link = urlProvider.getUrl(node.getDivision(), node.getUser());		

		String substituted;
		try {
			substituted =  node.getUser().getSubstituteUser()!= null ?
						" ("
							+ sm.getString("zastepowanyPrzez")
							+ " "
							+ node.getUser().getSubstituteUser().asLastnameFirstname()
							+ ")"
						:"";
		} catch(Exception e){
			log.error(e.getMessage(),e);
			substituted = "";
		}
		
		boolean isCheckboxChecked = false;
		boolean isCheckboxDisabled = false;
		if (node.getUser().equals(currentUser))
			isCheckboxDisabled = true;

		if(!node.isBackup()){
			return  HtmlUtils.toSpan(node.getUser().asLastnameFirstname() 
							+ HtmlUtils.toCheckbox("supervisors", node.getUser().getName(), isCheckboxChecked, isCheckboxDisabled))
				    + substituted;
		} else {
			return HtmlUtils.toSpan(node.getUser().asLastnameFirstname() 
					+ HtmlUtils.toCheckbox("supervisors", node.getUser().getName(), isCheckboxChecked, isCheckboxDisabled),"backup")	
				+ substituted;
		}
	}
}
