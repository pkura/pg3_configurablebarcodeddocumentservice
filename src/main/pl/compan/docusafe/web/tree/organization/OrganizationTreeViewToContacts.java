package pl.compan.docusafe.web.tree.organization;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.HtmlTheme;
import pl.compan.docusafe.web.common.HtmlUtils;

/**
 * Widok dla drzewa w strukturze organigramu (zak�adka organigram) link z uzytkownika do kontaktu
 * @author MSankowski
 *
 */
public class OrganizationTreeViewToContacts extends OrganizationTreeView{

	static final Logger log = LoggerFactory.getLogger(OrganizationTreeViewToContacts.class);
	public OrganizationTreeViewToContacts(OrganizationUrlProvider urlProvider,
			String contextPath, DSDivision selectedDivision, DSUser selectedUser) {
		super(urlProvider, contextPath, selectedDivision, selectedUser);		
	}
	
	public String getUserHtml(OrganizationTreeNode node){

		String userAnchor = HtmlUtils.toHtmlAnchor(contextPath + "/help/user-info.action?tid=0&userImpl.name="+ node.getUser().getName(), 
								HtmlUtils.toImgWithClass(contextPath+ HtmlTheme.gotoUserIcon().getSrc(),
										sm.getString("przejdzDoUzytkownika"), 
										sm.getString("przejdzDoUzytkownika"), "zoomImgTree"));
		String substituted;
		try {
			substituted =  node.getUser().getSubstituteUser()!= null ?
						" ("
							+ sm.getString("zastepowanyPrzez")
							+ " "
							+ node.getUser().getSubstituteUser().asFirstnameLastname() //getSubstitute().asFirstnameLastname()
							+ ")"
						:"";
		} catch(Exception e){
			log.error(e.getMessage(),e);
			substituted = "";
		}				
		
		String checkbox = "";
		//DSDivision.isInDivisionAsBackup(division, user)
		if(node.getDivision().equals(selectedDivision)){
			checkbox = HtmlUtils.toCheckbox("selectedUsers", node.getUser().getName()) ;
		}
		
		if(!node.isBackup()){
			return checkbox + 
				HtmlUtils.toSpan(node.getUser().asFirstnameLastname())		
				+ " " + userAnchor
				+ HtmlUtils.toSpan(substituted, "italic");
		} else {
			return checkbox + 
				HtmlUtils.toSpan(node.getUser().asFirstnameLastname(),"backup")
				+ " " + userAnchor
				+ HtmlUtils.toSpan(substituted, "italic");
		}
	}
	
	public String getDivisionHtml(OrganizationTreeNode node){
		String string =node.getDivision().getName()
				+ (node.getDivision().getCode() != null? " [" + node.getDivision().getCode() + "]":"") ;
		if(DSDivision.TYPE_GROUP.equals(node.getDivision().getDivisionType()))
		{
			return null;
		}
		else if(node.getDivision().equals(selectedDivision)){
			return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision(), node.getUser()), string , "active-link");
		} else {
			return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision(), node.getUser()), string);
		}
	}

}
