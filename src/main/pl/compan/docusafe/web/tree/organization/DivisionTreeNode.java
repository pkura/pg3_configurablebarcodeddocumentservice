package pl.compan.docusafe.web.tree.organization;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

class DivisionTreeNode implements OrganizationTreeNode{

	final private DSDivision base;
	
	/**
	 * Tworzy nierozwinięty OrganizationTreeNode
	 * @param base
	 */
	public DivisionTreeNode(DSDivision base){
		this.base = base;
	}
	

	public List getChildren() throws EdmException {
		List ret = fromArray(base.getChildren());
		ret.addAll(UserTreeNode.fromArray(base.getOriginalUsersWithoutBackup(), base));
		ret.addAll(UserTreeNode.fromArray(base.getOriginalBackupUsers(), base, true));
		return ret;
	}

	
	@SuppressWarnings("unchecked")
	static List fromArray(DSDivision[] divs){
		List ret = new ArrayList(divs.length);
		
		for(DSDivision div: divs){
			ret.add(new DivisionTreeNode(div));
		}
		
		return ret;
	}

	public String getName() {
		return base.getName();
	}

	public DSDivision getDivision() {
		return base;
	}

	public DSUser getUser() {
		return null;
	}

	public boolean isBackup() {
		return false;
	}
}
