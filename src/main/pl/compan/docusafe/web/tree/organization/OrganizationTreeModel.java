package pl.compan.docusafe.web.tree.organization;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.web.tree.HtmlTreeModel;

class OrganizationTreeModel implements HtmlTreeModel{
	
	static final Logger log = LoggerFactory.getLogger(OrganizationTreeModel.class);
	private Logger debugLogger;
	private OrganizationTreeNode root;
	private OrganizationUrlProvider urlProvider;
	
	private DSDivision selectedDivision;
	private DSUser selectedUser;
	
	private Set<DSDivision> expandedDivisions;
	
	private OrganizationTreeModel() throws EdmException {
		try {
			root = new DivisionTreeNode(DSDivision.find(DSDivision.ROOT_GUID));
		} catch (DivisionNotFoundException e) {
			log.error(e.getMessage(),e);
		} 
	}
	
	/**
	 * Tworzy model z rozwini�tymi ga��ziami
	 * @param urlProvider do tworzenia url'i
	 * @param selectedDivision najni�szy rozwini�ty dzia� (== zaznaczony dzia� gdy user == null), mo�e by� null
	 * @param selectedUser zaznaczony user (mo�e by� null)
	 * @throws EdmException
	 */
	public OrganizationTreeModel(OrganizationUrlProvider urlProvider, DSDivision selectedDivision, DSUser selectedUser) throws EdmException{
		this();
		this.urlProvider = urlProvider;
		this.selectedDivision = selectedDivision;
		this.selectedUser = selectedUser;	
		initExpanded();
	}
	
	private void initExpanded() throws DivisionNotFoundException, EdmException {
		if (selectedDivision == null) {
			expandedDivisions = Collections.emptySet();
		} else {
			expandedDivisions = new HashSet<DSDivision>();
			for (DSDivision temp = selectedDivision; temp != null; temp = temp
					.getParent()) {
				expandedDivisions.add(temp);
			}
		}
	}

	public List getChildElements(Object element) {
		try {
			return ((OrganizationTreeNode) element).getChildren();
		} catch (EdmException e) {
			debugLogger.error(e.getMessage(),e);
		}
		return null;
	}

	public Object getRootElement() {
		return root;
	}

	public String getUrl(Object element) {
		OrganizationTreeNode node = ((OrganizationTreeNode)element);
		return urlProvider.getUrl(node.getDivision(), node.getUser());
	}

	public boolean isExpanded(Object element) {
		if(expandedDivisions.contains(((OrganizationTreeNode)element).getDivision())){
			return true;
		} else {
			return false;
		}
	}
}
