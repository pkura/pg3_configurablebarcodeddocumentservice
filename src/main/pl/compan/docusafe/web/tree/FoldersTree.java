package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.HtmlTreeStyleVisitor;
import pl.compan.docusafe.util.HtmlTreeTheme;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.util.UrlVisitorWithAnchors;
import pl.compan.docusafe.web.archive.repository.ExploreDocumentsAction;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.LogFactory;

/**
 * Generuje HTML drzewa folder�w korzystaj�c z klas
 * {@link pl.compan.docusafe.util.HtmlTree} oraz
 * {@link pl.compan.docusafe.util.HtmlTreeModel}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FoldersTree.java,v 1.17 2009/09/03 12:52:53 rafalp Exp $
 */
public class FoldersTree
{
    /**
     * Zwraca obiekt HtmlTree reprezentuj�cy drzewo folder�w. W przypadku,
     * gdy u�ytkownik nie ma prawa dost�pu do g��wnego folderu, zwracana
     * jest warto�� null.
     */

    public static pl.compan.docusafe.util.HtmlTree newTree(final DSContext context, final Folder target,
                                   final UrlVisitor urlVisitor,
                                   final String contextPath)
        throws EdmException
    {
        // folder g��wny
        Folder _root = null;
        try
        {
            _root = Folder.getRootFolder();
        }
        catch (AccessDeniedException e)
        {
            return null;
        }

        final Folder root = _root;
        // lista folder�w prowadz�cych do folderu docelowego
        final List <Folder> expanded = new LinkedList <Folder> ();
        if (target != null)
        {
            try
            {
                Folder f = target;
                do
                {
                	f.setAnchor(true);
                    expanded.add(f);
                    f = f.parent();
                }
                while (f != null);
            }
            catch (AccessDeniedException e)
            {
                // metoda f.parent() mo�e rzuci� ten wyj�tek
            }
        }

        HtmlTreeStyleVisitor styleVisitor = new HtmlTreeStyleVisitor()
        {
            public String getStyleClass(Object element)
            {
                if (element == target)
                    return "tree_target";
                else
                    return null;
            }
        };

        pl.compan.docusafe.util.HtmlTreeModel model = new pl.compan.docusafe.util.HtmlTreeModel()
        {
            public Object getRootElement()
            {
                return root;
            }

            public String getAnchorName(Object element) {
            	return String.valueOf(((Folder) element).getId());
            }

            public String getUrl(Object element)
            {
            	if (urlVisitor instanceof UrlVisitorWithAnchors) {
//            		Folder destAnchor = null;
//            		try {
//            			destAnchor = Folder.findDestAnchor((Folder)element);
//            		} catch (EdmException e) {
//            			LogFactory.getLog("eprint").debug("", e);
//            		}
            		return urlVisitor.getUrl(element)+"#"+getAnchorName(element);
            	} else {
            		return urlVisitor.getUrl(element);
            	}
            }

            public String getTitle(Object element)
            {
                return ((Folder) element).getTitle();
            }

            public boolean hasChildElements(Object element)
            {
                try
                {
                    if (element == target)
                    {
                        return ((Folder) element).hasChildFolders();
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (EdmException e)
                {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }

            public List getChildElements(Object element)
            {
                try
                {
                    return ((Folder) element).getChildFolders();
                }
                catch (EdmException e)
                {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }

            public boolean isExpanded(Object element)
            {
                return expanded.contains(element);
            }

            public HtmlTreeTheme getTheme()
            {
                return HtmlTreeTheme.STANDARD;
            }
            public boolean isSelected (Object element)
            { 
            	ExploreDocumentsAction explore =  new ExploreDocumentsAction();
            	if (((Folder) element).getId().equals(explore.idReq))
            	{
            			return true;
            	}
            	return false;
            }

            public Long getIdRequest()
            {
            	return new ExploreDocumentsAction().idReq;
            }

			public String getDivision(Object element)
			{
				return null;
			}
        };

        pl.compan.docusafe.util.HtmlTree htmlTree =
            new pl.compan.docusafe.util.HtmlTree(model, contextPath);
        if (target != null)
            htmlTree.setStyleVisitor(styleVisitor);

        return htmlTree;
    }
    //added
    public Long getIdRequest()
    {
    	return new ExploreDocumentsAction().idReq;
    }

    private FoldersTree()
    {
    }
}