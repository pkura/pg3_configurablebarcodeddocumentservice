package pl.compan.docusafe.web.tree;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.chemistry.opencmis.client.api.*;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.util.*;
import java.util.List;
import java.util.Set;

/**
 * Geenruje Drzewo HTML z folder�w alfresco
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class AlfrescoFolderTree {
    private final static Logger log = LoggerFactory.getLogger(AlfrescoFolderTree.class);

    /**
    * Zwraca obiekt HtmlTree reprezentuj�cy drzewo folder�w.
    */
    public static pl.compan.docusafe.util.HtmlTree newTree(String target, final String contextPath, ChooseType chooseType) throws Exception
    {
        AlfrescoApi alfrescoApi = new AlfrescoApi();
        alfrescoApi.connect();

        Set<String> expandedUuids = getExpandedPath(alfrescoApi, target);
        pl.compan.docusafe.util.HtmlTreeModel model = new AlfrescoTreeModel(alfrescoApi, target, expandedUuids, chooseType);
        pl.compan.docusafe.util.HtmlTree htmlTree = new pl.compan.docusafe.util.HtmlTree(model, contextPath);

        if (target != null)
            htmlTree.setStyleVisitor(getHtmlTreeStyleVisitor(target));

        htmlTree.setImageVisitor(new AlfrescoTreeImageVisitor());
        htmlTree.setAdditionalButtonVisitor(new AlfrescoAdditionButtonVisitor(chooseType));
        alfrescoApi.disconnect();

        return htmlTree;
    }

    /**
     * Zwraca Liste uuid folder�w w kt�ych zawiera si� wybrany element
     * @param alfrescoApi
     * @return
     */
    private static Set<String> getExpandedPath(AlfrescoApi alfrescoApi, String target) throws Exception {
        Set<String> expandedUuids = Sets.newHashSet();

        if(target != null){
            CmisObject object = alfrescoApi.getObject(target);
            expandedUuids.add(target);
            //mo�emy trafi� na zaznaczony dokument
            Folder f = ((FileableCmisObject) object).getParents().iterator().next();

            do{
                expandedUuids.add(f.getId());
                f = f.getFolderParent();
            }while(f != null);
        }

        log.info("expanded uuids {}", expandedUuids);
        return expandedUuids;
    }

    /**
     * Klasa budujaca model drzewa katalog�w alfresco
     */
    private static class AlfrescoTreeModel implements pl.compan.docusafe.util.HtmlTreeModel
    {
        AlfrescoApi alfrescoApi = null;
        String target = null;
        Set<String> expandedUuids = Sets.newHashSet();
        ChooseType chooseType = ChooseType.DOCUMENT;

        private AlfrescoTreeModel(AlfrescoApi alfrescoApi, String target, Set<String> expandedUuids, ChooseType chooseType) {
            this.alfrescoApi = alfrescoApi;
            this.target = target;
            this.expandedUuids = expandedUuids;
            this.chooseType = chooseType;
        }

        @Override
        public Object getRootElement() {
            return alfrescoApi.getRootFolder();
        }

        @Override
        public String getUrl(Object element) {
            UrlVisitor urlVisitor = new UrlVisitor()
            {
                public String getUrl(Object element)
                {
                    String url = ServletActionContext.getRequest().getContextPath();

                    switch(chooseType){
                        case DOCUMENT :
                        case FOLDER :
                            url += "/office/alfresco/pick-alfresco.action?chooseType="+chooseType+"&uuid="+((CmisObject) element).getId();
                            break;
                        default:
                            url +=  "/office/alfresco/pick-alfresco.action";
                    }

                    return url;
                }
            };

            return urlVisitor.getUrl(element)+"#"+getAnchorName(element);
        }

        @Override
        public String getAnchorName(Object element) {
            if(element instanceof CmisObject)
                return ((CmisObject) element).getId();
            log.error("niespodziewany inny obiekt {}", element);
            return "";
        }

        @Override
        public String getTitle(Object element) {
            if(element instanceof CmisObject)
                return ((CmisObject) element).getName();
            log.error("niespodziewany inny obiekt {}", element);
            return "";
        }

        @Override
        public boolean hasChildElements(Object element) {
            boolean hasChildren = false;

            if((element instanceof Folder))
                hasChildren = ((Folder) element).getChildren().getTotalNumItems() > 0L;

            return hasChildren;
        }

        @Override
        public List getChildElements(Object element) {
            List<CmisObject> childs = Lists.newArrayList();

            if((element instanceof Folder)){
                ItemIterable<CmisObject> children = ((Folder) element).getChildren();
                childs = Lists.newArrayList(children);
            }

            return childs;
        }

        @Override
        public boolean isExpanded(Object element) {
            if(element instanceof Folder && expandedUuids.contains(((CmisObject)element).getId()))
                return true;
            // jest dokumentem nie mo�e byc rozszerzalny
            return false;
        }

        @Override
        public Long getIdRequest() {
            return 0L;
        }

        @Override
        public boolean isSelected(Object element) {
            if((element instanceof CmisObject) && ((CmisObject)element).getId().equals(target))
                return true;
            else
                return false;
        }

        @Override
        public String getDivision(Object element) {
            return null;
        }

        @Override
        public HtmlTreeTheme getTheme() {
            return HtmlTreeTheme.STANDARD;
        }
    }

    /**
     * Dodatkwoe style dla poszczegolnych element�w
     * @param target
     * @return
     */
    private static HtmlTreeStyleVisitor getHtmlTreeStyleVisitor(final String target){
        HtmlTreeStyleVisitor styleVisitor = new HtmlTreeStyleVisitor()
        {
         public String getStyleClass(Object element)
            {
             if ((element instanceof CmisObject) && ((CmisObject) element).getId().equals(target))
                    return "tree_target";
             else
                 return null;
          }
         };

        return styleVisitor;
    }


    private static class AlfrescoTreeImageVisitor implements HtmlTreeImageVisitor
    {
        public HtmlImage getIconImage(Object element, boolean expanded)
        {
            if (element != null && element instanceof Document)
            {
                    return new HtmlImage("/img/document.gif", 17, 17);
            }

            return null;
        }
    }

    private static class AlfrescoAdditionButtonVisitor implements HtmlTreeAdditionalButtonVisitor{

        ChooseType chooseType;

        private AlfrescoAdditionButtonVisitor(ChooseType chooseType) {
            this.chooseType = chooseType;
        }

        @Override
        public boolean hasAdditionalAction(Object element) {
            boolean has = false;

            switch(chooseType){
                case DOCUMENT : if(element instanceof Document) has = true; break;
                case FOLDER : if(element instanceof Folder) has = true; break;
            }

            return has;
        }

        @Override
        public void getAdditionalAction(Object element, StringBuilder out) {

            String imgSrc = ServletActionContext.getRequest().getContextPath() + "/img/list-plus.gif";
            switch(chooseType){
                case DOCUMENT :
                    if(element instanceof Document){
                        out.append("<a href=\"javascript:void(pick('").append(((CmisObject)element).getId()).append("','DOCUMENT'));javascript:void(window.close());\">");
                        out.append("<img src='").append(imgSrc).append("'/>");
                        out.append("</a>");
                    }
                    break;
                case FOLDER :
                    if(element instanceof Folder){
                        out.append("<a href=\"javascript:void(pick('").append(((CmisObject)element).getId()).append("','FOLDER'));javascript:void(window.close());\">");
                        out.append("<img src='").append(imgSrc).append("'/>");
                        out.append("</a>");
                    }
                    break;
            }
        }
    }

    /**
     * Rodzaj elementu jaki ma zosta� ostatecznie wybrany
     * Do importowania do DS b�dzie to dokument
     * Do exportowania do Alfresco b�dzie to folder
     */
    public enum ChooseType{
        DOCUMENT,
        FOLDER;
    }
}
