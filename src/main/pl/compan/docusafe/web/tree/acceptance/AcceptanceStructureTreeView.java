package pl.compan.docusafe.web.tree.acceptance;

import java.util.Arrays;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.HtmlTreeView;

class AcceptanceStructureTreeView implements HtmlTreeView{
	
	private final UrlVisitor visitor;
	private AcceptanceTreeNodeAdapter selected;
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

	public AcceptanceStructureTreeView(UrlVisitor visitor){
		this.visitor = visitor;
	}		
	
	public String getHtml(Object element) {
		if(element instanceof AcceptancesDefinition.FinalAcceptance){			
			return "Akceptacja finalna" +  Arrays.toString(((AcceptancesDefinition.FinalAcceptance)element).getRequiredAcceptances());
		} else if(element instanceof AcceptancesDefinition.Acceptance){
			AcceptancesDefinition.Acceptance acceptance = ((AcceptancesDefinition.Acceptance)element);
			return acceptance.getCn();
		} else if(element instanceof AcceptanceCondition){
			AcceptanceCondition acceptance = (AcceptanceCondition) element;
			return (acceptance.getDivisionGuid() != null)?acceptance.getDivisionGuid():acceptance.getUsername();
		} else if(element instanceof AcceptanceTreeNodeAdapter){
			AcceptanceTreeNodeAdapter el = (AcceptanceTreeNodeAdapter) element;
			
			String divisionLink = (el.getDivisionGuid() != null) && (!el.getDivision().isHidden())
					? "<a href=\"../organization/edit-organization.do?divisionGuid="
					+ el.getDivisionGuid()
					+ "\"> <img src=\"../img/folder-move-small.gif\" alt=\"go to division\" title=\""
					+ sm.getString("PrzejdzDoStrukturyOrganizacji")
					+ "\"></a>"
					: "";
					
			StringBuilder errors = new StringBuilder("");
			if(el.isInvalid()){ 
				for(String s: el.getErrorDescriptions()){					
					errors.append(" <img src=\"../img/error.gif\" alt=\"error in channel\" title=\"" 
						+ s + "\">");					
				}
			}
			
			return errors + " <a " + getClassString(el) + "href=\"" + visitor.getUrl(element)+ "\">" + el + "</a>" + divisionLink;
		}
		return "??";
	}
	
	private String getClassString(AcceptanceTreeNodeAdapter el){		
		if(el.equals(selected)){
			return "class=\"active-link\" ";
		} else {
			if(el.getDivision() != null){
				if(el.getDivision().isHidden()){
					return "class=\"deleted\"";
				}
				
				if(el.isDivisionEmpty()){ 
					return "class=\"empty\"";
				}
			}
		}		
		return "";
	}

	public AcceptanceStructureTreeView setSelected(AcceptanceTreeNodeAdapter selected) {
		this.selected = selected;
		return this;
	}

	public AcceptanceTreeNodeAdapter getSelected() {
		return selected;
	}
}