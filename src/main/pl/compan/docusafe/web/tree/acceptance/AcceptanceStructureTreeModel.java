package pl.compan.docusafe.web.tree.acceptance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.HtmlTreeModel;

class AcceptanceStructureTreeModel implements HtmlTreeModel
{
	static final Logger log = LoggerFactory.getLogger(AcceptanceStructureTreeModel.class);
	private final UrlVisitor visitor;
	private final AcceptancesDefinition flow;
	private final AcceptanceTreeNodeAdapter expanded;
	private final int maxLevel;
	//private final Map<Integer,AcceptancesDefinition.Acceptance> levelMap = new HashMap<Integer,AcceptancesDefinition.Acceptance> ();
	private final List<String> cns;
	
	
	public AcceptanceStructureTreeModel(UrlVisitor visitor, DocumentKind kind, AcceptanceTreeNodeAdapter expanded) throws DivisionNotFoundException, EdmException{
		this.visitor = visitor;				
		kind.initialize();
		this.flow = kind.getAcceptancesDefinition();		
		
		this.expanded = expanded;
		
		{
			Collection<AcceptancesDefinition.Acceptance> acceptances = flow
					.getAcceptances().values();
			cns = new ArrayList(Collections.nCopies(acceptances.size(), null));
			
			
			int max = Integer.MIN_VALUE;
			for (AcceptancesDefinition.Acceptance acceptance : acceptances) {
				max = Math.max(max, acceptance.getLevel());
				cns.set(acceptance.getLevel() - 1, acceptance.getCn());
				//levelMap.put(acceptance.getLevel(), acceptance);
			}
			
			this.maxLevel = max;
		}
		if(expanded != null){
			this.expanded.setCnList(cns);
		}
	}

	public List getChildElements(Object element){
		if(element instanceof AcceptancesDefinition.FinalAcceptance){
			ArrayList list = new ArrayList(1);
			list.add(flow.getAcceptances().get(cns.get(maxLevel - 1)));
			return list;
		} else  if(element instanceof AcceptancesDefinition.Acceptance){
			AcceptancesDefinition.Acceptance acceptance = ((AcceptancesDefinition.Acceptance)element);
			if(acceptance.getLevel()>1){
				int level = acceptance.getLevel() - 2;
				if(cns.get(level) != null){
					try {
						List r = AcceptanceCondition.find(cns.get(level));
						return AcceptanceTreeNodeAdapter.fromList(r, cns); 						
					} catch (EdmException e) {
						log.error(
								"AcceptanceStructureTree.java:58 " +e, e);
						return null;
					}
				} else {
					return null;
				}
			}
		} else if(element instanceof AcceptanceTreeNodeAdapter){
			try {
				return ((AcceptanceTreeNodeAdapter)element).getChildren();
			} catch (EdmException e) {
				log.error("AcceptanceStructureTree.java:69 " +e, e);
				return null;
			}
		}
		return null;
	}				

	public Object getRootElement() {
		return flow.getFinalAcceptance();
	}

	public String getUrl(Object element) {
		return visitor.getUrl(element);
	}

	public boolean isExpanded(Object element) {
		if (!(element instanceof AcceptanceTreeNodeAdapter)) {
			return true;
		} else if (expanded != null) {
			/*Logger.getLogger("MICHAEL").debug(
					"AcceptanceStructureTreeModel.java: expanded.isDescendantOf(element) = "+ expanded.isDescendantOf(element)
					+'\n' + "expanded = " + expanded
					+'\n' + "element = " + element
					+'\n' + "expanded.equals(element) = " + expanded.equals(element));*/
			return ((AcceptanceTreeNodeAdapter)element).getFieldValue() == null || expanded.equals(element) || expanded.isDescendantOf(element);
		} else {
			return false;
		}
	}
}





