package pl.compan.docusafe.web.tree.acceptance;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;

class AcceptanceDivisionTreeNode {

	final private AcceptanceDivisionImpl base;
	
	public AcceptanceDivisionTreeNode(AcceptanceDivisionImpl base)
	{
		this.base = base;
	}
	

	public List<AcceptanceDivisionTreeNode> getChildren() 
	{
		List<AcceptanceDivisionTreeNode> ret = fromArray(base.getChildren());		
		return ret;
	}

	
	@SuppressWarnings("unchecked")
	static List<AcceptanceDivisionTreeNode> fromArray(Set<AcceptanceDivisionImpl> divs)
	{
		List ret = new ArrayList(divs.size());
		
		for(AcceptanceDivisionImpl div: divs){
			ret.add(new AcceptanceDivisionTreeNode(div));
		}
		
		return ret;
	}

	public String getName() 
	{
		return base.getName();
	}

	public AcceptanceDivisionImpl getDivision() 
	{
		return base;
	}
	
}
