package pl.compan.docusafe.web.tree.acceptance;

import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;

public interface AcceptanceOrganizationUrlProvider {

	String getUrl(AcceptanceDivisionImpl div);
		
}
