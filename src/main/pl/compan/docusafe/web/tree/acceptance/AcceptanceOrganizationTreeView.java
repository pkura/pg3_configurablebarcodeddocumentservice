package pl.compan.docusafe.web.tree.acceptance;

import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.HtmlImage;
import pl.compan.docusafe.util.HtmlTreeTheme;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.HtmlTheme;
import pl.compan.docusafe.web.common.HtmlUtils;
import pl.compan.docusafe.web.tree.ExtendedHtmlTreeView;

public class AcceptanceOrganizationTreeView implements ExtendedHtmlTreeView 
{

	static final Logger log = LoggerFactory.getLogger("acceptance_division");
	
	private AcceptanceDivisionImpl selectedAcceptanceDivision;
	private AcceptanceOrganizationUrlProvider urlProvider;
	
	protected final String contextPath;
	
	public AcceptanceOrganizationTreeView(AcceptanceOrganizationUrlProvider urlProvider, String contextPath, AcceptanceDivisionImpl selectedAcceptanceDivision)
	{
		this.urlProvider = urlProvider;
		this.selectedAcceptanceDivision = selectedAcceptanceDivision;
		this.contextPath = contextPath;
	}
	
	public String getHtml(Object element) 
	{
		AcceptanceDivisionTreeNode node = (AcceptanceDivisionTreeNode) element;
		String title = node.getName();
		boolean selected = node.getDivision().equals(selectedAcceptanceDivision);
		
		String addString = HtmlUtils.toHtmlAnchor("javascript:add("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/plus2.gif", "add", "add"));
		
		if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE) || node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE))
		{
			return getUserHtml(node, selected);
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE))
		{
			return getAcceptanceHtml(node, selected);
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE)
				|| node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_CENTRUM_KOSZTOW_TYPE))
		{
			return getDsDivisionHtml(node, selected);
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DIVISION_DIVISION_TYPE))
		{
			return getAcceptanceDivisionHtml(node, selected);
		}
		else
		{
			return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision()), title , selected ? "active-link" : null) + addString;
		}
		
	}
	
	private String getUserHtml(AcceptanceDivisionTreeNode node, boolean selected)
	{
		String removeString = HtmlUtils.toHtmlAnchor("javascript:remove("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/minus2.gif", "remove", "remove"));
		return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), "javascript:void(0);", node.getName() , selected ? "active-link" : null) + removeString;
	}
	
	private String getDsDivisionHtml(AcceptanceDivisionTreeNode node, boolean selected)
	{
		String removeString = HtmlUtils.toHtmlAnchor("javascript:remove("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/minus2.gif", "remove", "remove"));
		String addString = HtmlUtils.toHtmlAnchor("javascript:add("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/plus2.gif", "add", "add"));
		//return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), "javascript:void(0);", node.getName() , selected ? "active-link" : null) + removeString;
		return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision()), node.getName() + " (Typ: " + node.getDivision().getPreetyDivisionType() + ")", selected ? "active-link" : null) + addString + removeString;
	}
	
	private String getAcceptanceHtml(AcceptanceDivisionTreeNode node, boolean selected)
	{
		String addString = HtmlUtils.toHtmlAnchor("javascript:add("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/plus2.gif", "add", "add"));
		String removeString = HtmlUtils.toHtmlAnchor("javascript:remove("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/minus2.gif", "remove", "remove"));
		String ogolna = node.getDivision().getOgolna().equals(1) ?  " (ogolna)" : "";
		return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision()), node.getName() + ogolna + " (Typ: " + node.getDivision().getPreetyDivisionType() + ", Kod: " + node.getDivision().getCode() + ")", selected ? "active-link" : null) + addString + removeString;
	}
	
	private String getAcceptanceDivisionHtml(AcceptanceDivisionTreeNode node, boolean selected)
	{
		String addString = HtmlUtils.toHtmlAnchor("javascript:add("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/plus2.gif", "add", "add"));
		String removeString = HtmlUtils.toHtmlAnchor("javascript:remove("+node.getDivision().getId()+");",HtmlUtils.toImg(contextPath + "/img/minus2.gif", "remove", "remove"));
		
		if(node.getDivision().getCode() != null && node.getDivision().getCode().equals("root"))
			removeString = "";
		
		return HtmlUtils.toHtmlAnchorWithName(node.getDivision().getGuid(), urlProvider.getUrl(node.getDivision()), node.getName() + " (Typ: " + node.getDivision().getPreetyDivisionType() + ", Kod: " + node.getDivision().getCode() + ")", selected ? "active-link" : null) + addString + removeString;
	}
	
	public HtmlImage getIcon(Object element, boolean expanded) 
	{
		AcceptanceDivisionTreeNode node = (AcceptanceDivisionTreeNode) element;

		if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE))
		{
			return HtmlTheme.gotoUserIcon();
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE))
		{
			return HtmlTheme.userIcon();
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE))
		{
			return HtmlTheme.positionIcon();
		}
		else if(node.getDivision().getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE))
		{
			return HtmlTheme.groupIcon();
		}
		
		if (expanded) 
		{
			return HtmlTreeTheme.STANDARD.getExpandedFolderImage();
		} 
		else 
		{
			return HtmlTreeTheme.STANDARD.getCollapsedFolderImage();
		}
	}
}
