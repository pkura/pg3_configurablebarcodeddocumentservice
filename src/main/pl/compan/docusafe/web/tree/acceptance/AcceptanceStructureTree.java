package pl.compan.docusafe.web.tree.acceptance;


import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.HtmlTree;

public class AcceptanceStructureTree extends HtmlTree {

	public AcceptanceStructureTree(UrlVisitor visitor, DocumentKind kind, AcceptanceTreeNodeAdapter expanded, String contextPath) throws DivisionNotFoundException, EdmException {
		super(new AcceptanceStructureTreeModel(visitor, kind, expanded), new AcceptanceStructureTreeView(visitor).setSelected(expanded), contextPath);		

	}		

	public static AcceptanceStructureTree newTree(UrlVisitor urlVisitor, DocumentKind kind, Object expanded, String contextPath) throws DivisionNotFoundException, EdmException{
		//Logger.getLogger("MICHAEL")
		//		.debug("AcceptanceStructureTree.java:expanded = "+expanded);
		if(expanded instanceof AcceptanceCondition){
			expanded = new AcceptanceTreeNodeAdapter((AcceptanceCondition) expanded,(String[]) null);
			//Logger.getLogger("MICHAEL")
			//		.debug("AcceptanceStructureTree.java:created " + expanded);
		} else {
			//Logger.getLogger("MICHAEL").debug("AcceptanceStructureTree.java:expanded null");
			expanded = null;
		}
		return new AcceptanceStructureTree(urlVisitor, kind, (AcceptanceTreeNodeAdapter)expanded, contextPath);
	}		
}









