package pl.compan.docusafe.web.tree.acceptance;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.HtmlTreeModel;

public class AcceptancesOrganizationTreeModel implements HtmlTreeModel 
{

	static final Logger log = LoggerFactory.getLogger("acceptance_division");
	private AcceptanceDivisionTreeNode root;
	private AcceptanceOrganizationUrlProvider urlProvider;
	
	private AcceptanceDivisionImpl selectedAcceptanceDivision;
	private Set<AcceptanceDivisionImpl> expandedAcceptanceDivisions;
	
	private AcceptancesOrganizationTreeModel()
	{
		root = new AcceptanceDivisionTreeNode(AcceptanceDivisionImpl.findByCode("root"));
	}
	
	public AcceptancesOrganizationTreeModel(AcceptanceOrganizationUrlProvider urlProvider, AcceptanceDivisionImpl selectedDivision)
	{
		this();
		this.urlProvider = urlProvider;
		this.selectedAcceptanceDivision = selectedDivision;
		initExpanded();
	}
	
	private void initExpanded()
	{
		if (selectedAcceptanceDivision == null) 
		{
			expandedAcceptanceDivisions = Collections.emptySet();
		} 
		else 
		{
			expandedAcceptanceDivisions = new HashSet<AcceptanceDivisionImpl>();
			for (AcceptanceDivisionImpl temp = selectedAcceptanceDivision; temp != null; temp = temp.getParent()) 
			{
				expandedAcceptanceDivisions.add(temp);
			}
		}
	}
	
	public List<AcceptanceDivisionTreeNode> getChildElements(Object element) 
	{
		return ((AcceptanceDivisionTreeNode) element).getChildren();		
	}

	public Object getRootElement() 
	{
		return root;
	}

	public String getUrl(Object element) 
	{
		AcceptanceDivisionTreeNode node = ((AcceptanceDivisionTreeNode)element);
		return urlProvider.getUrl(node.getDivision());
	}

	public boolean isExpanded(Object element) 
	{
		if(expandedAcceptanceDivisions.contains(((AcceptanceDivisionTreeNode)element).getDivision()))
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

}
