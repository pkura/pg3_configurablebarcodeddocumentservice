package pl.compan.docusafe.web.tree.acceptance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.StringManager;



public class AcceptanceTreeNodeAdapter {		

	protected StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(AcceptanceTreeNodeAdapter.class);
	
	
	private AcceptanceCondition acType;	
	private DSDivision division;
	boolean empty = false;
	//wsp�lna (dla wszystkich nod�w danego drzewa)lista wszystkich kod�w akceptacji
	private List<String> cnList;
	
	public String getCn(){
		return acType.getCn();
	}
	
	public String getFieldValue(){
		return acType.getFieldValue();
	}
	
	public boolean isDescendantOf(Object element){
		if(element instanceof AcceptanceTreeNodeAdapter){
			AcceptanceTreeNodeAdapter atna = (AcceptanceTreeNodeAdapter) element;				
			try {
				if(atna.getFieldValue() == null) {
					return true;
				} else if(atna.getAcceptanceLevel() > this.getAcceptanceLevel()){				
					for(AcceptanceCondition ac: AcceptanceCondition.find(atna.getCn(), this.getFieldValue())){
						if(ac.getDivisionGuid().equals(atna.getDivisionGuid())){
							return true;
						}
					}
				} else {
					return false;
				}
			} catch (EdmException e) {				
				log.error(
						"AcceptanceTreeNodeAdapter.java:" +e,e);
				return false;
			}
		} else { 
			//elementy, kt�re nie s� AcceptanceTreeNodeAdapter wyst�puj� tylko raz i s� korzeniem - wi�c wszystkie nody s� ich potomkami
			return true;
		}
		
		return false;
	}		
	
	public String getDivisionGuid() {
		return acType.getDivisionGuid();
	}
	
	public boolean isDivisionEmpty(){
		return empty;		
	}
	
	/**
	 * Okre�la czy dany w�ze� jest niepoprawny np nie zawiera potomk�w, jest pustym dzia�em etc.
	 * @return true je�li jest niepoprawny, dodatkowo ustawia odpowiednio property errorDescription
	 */
	public boolean isInvalid(){
		
		boolean ret = false;
		if(!cnList.get(0).equals(acType.getCn())){ //je�li nie jest pierwszym etapem akceptacji
			try {				
				if(getChildren().isEmpty()) {
					errorDescription.add(sm.getString("brakWczesniejszychWarunkowAkceptacji"));
					ret = true;				
				}				
				if(this.isDivisionEmpty()){
					errorDescription.add(sm.getString("dzialJestPusty"));
					ret = true;	
				}
				if(division != null && division.isHidden()){
					errorDescription.add(sm.getString("dzialJuzNieIstnieje"));
					ret = true;
				}
			} catch (EdmException e) {
				log.error("AcceptanceTreeNodeAdapter.java:isInvalid() ",e);
				errorDescription.add(e.getMessage());
				ret = true;
			}
		}
		
		return ret;
	}
	
	LinkedList<String> errorDescription = new LinkedList<String>();
	/**
	 * Je�li metoda isInvalid() zwr�ci�a true to ta metoda zwraca przyczyny b��d�w
	 */
	public LinkedList<String> getErrorDescriptions(){
		return errorDescription;
	}

	public AcceptanceTreeNodeAdapter(AcceptanceCondition ac, String[] cnList) throws DivisionNotFoundException, EdmException{
		
		this.acType = ac;
		if(this.acType.getDivisionGuid() != null){
			division = (DSDivision.find(acType.getDivisionGuid()));
			empty = getDivision().getUsers().length == 0;
		}
		if(cnList != null){
			this.cnList = new LinkedList<String>(Arrays.asList(cnList));
		} else {
			this.cnList = null;
		}
	}
	
	public Integer getAcceptanceLevel(){
		return cnList.indexOf(this.getCn()) + 1;
	}
	
	public AcceptanceTreeNodeAdapter(AcceptanceCondition ac, List<String> cnList) throws DivisionNotFoundException, EdmException{
		if(ac == null) throw new NullPointerException("ac cannot be null");
		
		this.acType = ac;
		if(this.acType.getDivisionGuid() != null){
			division = DSDivision.find(acType.getDivisionGuid());
			empty = division.getUsers().length == 0;
		}
		this.cnList = cnList;
	}
	
	
	String cacheToString = null;
	private String createToString(){
		try {
			if(getDivision() != null){
				return getDivision().getName()+" ["+acType.getCn()+"]";
			} else {
				return DSUser.findByUsername(acType.getUsername()).asFirstnameLastname() +" ["+acType.getCn()+"]";
			}
		} catch (EdmException e) {
			log.error("AcceptanceTreeNodeAdapter.java:",e);
		}		
		
		return acType.getCn() + "["+acType.getFieldValue()+"]";	
	}
	
	public String toString(){
		if(cacheToString == null){
			cacheToString = createToString();
		}
		return cacheToString;
	}
	
	List<AcceptanceTreeNodeAdapter> children = null;		
	public List<AcceptanceTreeNodeAdapter> getChildren() throws EdmException{
		
		if(children == null){
			children = findChildren();
		}
		
		return children;
	}
	
	List<AcceptanceTreeNodeAdapter> findChildren() throws DivisionNotFoundException, EdmException{				
		/*Logger.getLogger("MICHAEL")
				.debug("AcceptanceTreeNodeAdapter.java:getChildren()\n"
					+"		acType.getCn() = " + acType.getCn() + '\n'
					+"		acType.getFieldValue() = " + acType.getFieldValue() +'\n'
					+"		this.getPrevCn() = " + this.getPrevCn());*/
		
		if(cnList == null){
			log.debug("cnList = null");
			return null;
		} 
		

		
		if(acType.getFieldValue() == null){
			return AcceptanceTreeNodeAdapter.fromList(AcceptanceCondition.find(getPrevCn()), cnList);
		}				

		List<AcceptanceCondition> thisConditions = AcceptanceCondition.findByGuid(acType
				.getDivisionGuid());
		
		LinkedList<AcceptanceCondition> ret = new LinkedList<AcceptanceCondition>();
		
		mainLoop:
		for (AcceptanceCondition ac : thisConditions) {

			for(AcceptanceCondition acond: AcceptanceCondition.find(getPrevCn(), ac.getFieldValue())){
				ret.add(acond);			
				continue mainLoop;
			}
			
			/***********************************************************************
			 * TODO hack dla intercarsa - usun�� w przysz�o�ci
			 **********************************************************************/			
			//Logger.getLogger("MICHAEL")
			//		.debug("AcceptanceTreeNodeAdapter.java:"+acType+acType.getCn());
			
			if (acType.getCn()!=null && acType.getCn().equals("szefa_pionu")
					&& DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND) != null
					/*&& (temp = CentrumKosztow.find(Integer.parseInt(acType.getFieldValue()))) != null 
					&& temp.getDefaultSimpleAcceptance() - obecnie nie dzia�a*/) {
				//je�li ten kana� ma w��czon� domy�lnie uproszczon� akceptacj� to dodaj tak�e koordynator�w(akceptacja "zwykla")
				for(AcceptanceCondition acond: AcceptanceCondition.find("zwykla", ac.getFieldValue())){
					ret.add(acond);			
				}				
				continue;
			}
			/***********************************************************************
			 * KONIEC hacka
			 **********************************************************************/
		}
		return fromList(ret, cnList);	
	}
	
	String getPrevCn(){
		String prevCN = null;		
		{
			int index = cnList.indexOf(acType.getCn()) - 1;
			if(index < 0) {
				return null;
			}
			prevCN = cnList.get(index);
		}
		return prevCN;
	}
	
	String getNextCn(){
		String nextCN = null;		
		{
			int index = cnList.indexOf(acType.getCn()) + 1;
			if(cnList.size()<= index) {
				return null;
			}
			nextCN = cnList.get(index);
		}
		return nextCN;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof AcceptanceTreeNodeAdapter) &&
			((AcceptanceTreeNodeAdapter)obj).acType.isSameType(this.acType);
	}
	
	@Override
	public int hashCode() {		
		return acType.getCn().hashCode() + (acType.getDivisionGuid()!=null ? acType.getDivisionGuid().hashCode():0);//(acType.getFieldValue() !=null? acType.getFieldValue().hashCode(): 0 );
	}	
	
	public static List<AcceptanceTreeNodeAdapter> fromList(List<AcceptanceCondition> cond, String[] nextCN) throws DivisionNotFoundException, EdmException{
		HashSet<AcceptanceTreeNodeAdapter> ret = new HashSet<AcceptanceTreeNodeAdapter>(); 
		for(AcceptanceCondition c: cond){			
			ret.add(new AcceptanceTreeNodeAdapter(c, nextCN));
		}
		return new ArrayList<AcceptanceTreeNodeAdapter>(ret);
	}
	
	public static List<AcceptanceTreeNodeAdapter> fromList(List<AcceptanceCondition> cond, List<String>  nextCN) throws DivisionNotFoundException, EdmException{
		//Logger.getLogger("MICHAEL")
		//		.debug("fromList:"+cond);
		HashSet<AcceptanceTreeNodeAdapter> ret = new HashSet<AcceptanceTreeNodeAdapter>(); 
		for (AcceptanceCondition c : cond) {			
			ret.add(new AcceptanceTreeNodeAdapter(c, nextCN));			
		}
		return new ArrayList<AcceptanceTreeNodeAdapter>(ret);
	}

	void setCnList(List<String> cnList) {
		this.cnList = cnList;
	}

	public DSDivision getDivision() {
		return division;
	}
}
