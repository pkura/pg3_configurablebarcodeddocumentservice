package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.parametrization.ifpan.ZapotrzebowanieLogic;
import pl.compan.docusafe.util.HtmlImage;
import pl.compan.docusafe.util.HtmlTreeTheme;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * Klasa generuj�ca HTML przedstawiaj�cy drzewo dowolnych obiekt�w
 * na podstawie modelu reprezentowanego przez {@link pl.compan.docusafe.util.HtmlTreeModel}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTree.java,v 1.4 2008/10/27 10:53:33 pecet1 Exp $
 */
public class HtmlTree
{
    private HtmlTreeModel model;
    // private HtmlTreeView view;
    private ExtendedHtmlTreeView view;

    private int[] columnInfo = new int[1];

    private static final int LAST_SIBLING = 1;
    private static final int MIDDLE_SIBLING = 2;

    private HtmlTreeTheme theme = HtmlTreeTheme.STANDARD;
    private String contextPath;

	private static Logger log = LoggerFactory.getLogger(HtmlTreeTheme.class);
    
    public HtmlTree(HtmlTreeModel model, HtmlTreeView view, String contextPath)
    {
        this.model = model;
        if(view instanceof ExtendedHtmlTreeView){
        	this.view = (ExtendedHtmlTreeView) view;
        } else {
        	this.view = new HtmlTreeViewAdapter(view);
        }
        this.contextPath = contextPath != null ? contextPath : "";
    }
    
    /**
     * Tworzy drzewo z rozszerzonym widokiem
     */
    public HtmlTree(HtmlTreeModel model, ExtendedHtmlTreeView view, String contextPath)
    {
    	this(model, (HtmlTreeView)view, contextPath);
    }

    public String generateTree()
    {
        StringBuilder out = new StringBuilder();
        
        out.append("<div class=\"tree\">");

        Object rootElement = model.getRootElement();

        // numer kolejnej kolumny drzewa; root dostaje 0, je�eli
        // istnieje
        int nextColumn = 0;

        out.append("<nobr>");

        // ga��� g��wna
        if (rootElement != null)
        {
            HtmlImage image = view.getIcon(rootElement, true);//getIconImage(rootElement, true);
            //out.append("<nobr>");
            out.append("<img src='"+contextPath + image.getSrc()+"' " +
                "width='"+image.getWidth()+"' " +
                "height='"+image.getHeight()+"' " +
                "border='0' " +
                "class='tree'>");
            out.append("&nbsp;");

            out.append(view.getHtml(rootElement));
            out.append("<br>");

            setColumnInfo(0, LAST_SIBLING);

            nextColumn++;
        }

        walkTree(out, rootElement, model.getChildElements(rootElement), nextColumn);

        out.append("</nobr>");
        out.append("</div>");

        return out.toString();
    }

    /**
     * Rekurencyjnie generuje reszt� drzewa rozwijaj�c tylko te
     * ga��zie, dla kt�rych metoda {@link pl.compan.docusafe.util.HtmlTreeModel#isExpanded}
     * zwraca true.
     * @param out
     * @param parent Element nadrz�dny dla listy element�w childElements.
     *  U�ywany do generowania odno�nika do elementu nadrz�dnego (ikonka
     *  z minusem s�u��ca do "zwijania" fragmentu drzewa prowadzi do folderu
     *  nadrz�dnego).
     * @param childElements Lista element�w, kt�re maj� by� pokazane na
     *  aktualnym poziomie. Przekazuj� list�, zamiast pozwoli� metodzie
     *  na samodzielne pobranie tej listy z elementu parent, aby unikn��
     *  dwukrotnego pobierania element�w podrz�dnych z tego samego elementu
     *  nadrz�dnego.
     */
    private void walkTree(StringBuilder out, Object parent, List childElements,
                          int column)
    {
        if (childElements == null || childElements.size() == 0)
            return;

        for (int i=0, n=childElements.size(); i < n; i++)
        {
            Object child = childElements.get(i);
            List children = model.getChildElements(child);

            setColumnInfo(column, i < n-1 ? MIDDLE_SIBLING : LAST_SIBLING);

            boolean isLastSibling = i == n-1;
            boolean isExpanded = model.isExpanded(child);
            boolean hasChildren = children != null && children.size() > 0;
            String link = model.getUrl(child);
            link = link.replace("\"", "'");

            // wype�niacz (pionowe linie, gdzie s� potrzebne, w pozosta�ych
            // miejscach pusty obrazek)
            for (int col=1; col < column; col++)
            {
                if (getColumnInfo(col) == MIDDLE_SIBLING)
                {
                    out.append("<img " +
                        "src='"+contextPath+theme.getVerticalLineImage().getSrc()+"' " +
                        "width='"+theme.getVerticalLineImage().getWidth()+"' " +
                        "height='"+theme.getVerticalLineImage().getHeight()+"' " +
                        "border='0' class='tree'>");
                }
                else //if (getColumnInfo(col) == LAST_SIBLING)
                {
                    out.append("<img " +
                        "src='"+contextPath+theme.getBlankImage().getSrc()+"' " +
                        "width='"+theme.getBlankImage().getWidth()+"' " +
                        "height='"+theme.getBlankImage().getHeight()+"' " +
                        "border='0' class='tree'>");
                }
            }

            // odpowiedni znaczek (plus/minus) przy ikonce folderu
            if (isExpanded)
            {
                String parentLink = parent != null ? model.getUrl(parent) : null;
                if (isLastSibling)
                {
                    // mimo oznaczenia elementu jako rozwini�tego (expanded),
                    // nie mo�na wykluczy�, �e model zwr�ci takie oznaczenie
                    // dla folderu nie maj�cego folder�w podrz�dnych
                    if (hasChildren)
                    {
                        out.append(
                            (parentLink != null ? "<a class='"+theme.getControlLinkClass()+"' href='"+parentLink+"'>" : "") +
                            "<img " +
                                "src='"+contextPath+theme.getLastExpandedControlImage().getSrc()+"' " +
                                "width='"+theme.getLastExpandedControlImage().getWidth()+"' " +
                                "height='"+theme.getLastExpandedControlImage().getHeight()+"' " +
                                "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : ""));
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
                else
                {
                    if (hasChildren)
                    {
                        out.append(
                            (parentLink != null ? "<a class='"+theme.getControlLinkClass()+"' href=\""+parentLink+"\">" : "") +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleExpandedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleExpandedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleExpandedControlImage().getHeight()+"' " +
                            "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : ""));
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
            }
            else
            {
                if (isLastSibling)
                {
                    if (hasChildren)
                    {
                        out.append("<a class='"+theme.getControlLinkClass()+"' href=\""+link+"\">" +
                            "<img " +
                            "src='"+contextPath+theme.getLastCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getLastCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getLastCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a>");
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
                else
                {
                    if (hasChildren)
                    {
                        out.append("<a class='"+theme.getControlLinkClass()+"' href=\""+link+"\">" +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a>");
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
            }

            HtmlImage image = view.getIcon(child, isExpanded);

            out.append("<img src='"+contextPath+image.getSrc()+"' " +
                "width='"+image.getWidth()+"' " +
                "height='"+image.getHeight()+"' " +
                "border='0' " +
                "class='tree'>");

            // obrazek folderu (otwarty lub zamkni�ty)
/*
            if (isExpanded)
            {
                out.append("<img src='"+contextPath+theme.getExpandedFolderImage().getSrc()+"' " +
                    "width='"+theme.getExpandedFolderImage().getWidth()+"' " +
                    "height='"+theme.getExpandedFolderImage().getHeight()+"' " +
                    "border='0' " +
                    "class='tree'>");
            }
            else
            {
                out.append("<img src='"+contextPath+theme.getCollapsedFolderImage().getSrc()+"' " +
                    "width='"+theme.getCollapsedFolderImage().getWidth()+"' " +
                    "height='"+theme.getCollapsedFolderImage().getHeight()+"' " +
                    "border='0' " +
                    "class='tree'>");
            }
*/

            out.append(view.getHtml(child));
            out.append("<br>");

            if (isExpanded && children != null && children.size() > 0)
            {
                walkTree(out, child, children, column+1);
            }
        }
    }
    


    private void setColumnInfo(int column, int info)
    {
        if (columnInfo.length < column+1)
        {
            int[] tmp = new int[column+1];
            System.arraycopy(columnInfo, 0, tmp, 0, columnInfo.length);
            columnInfo = tmp;
        }

        columnInfo[column] = info;
    }

    private int getColumnInfo(int column)
    {
        if (columnInfo.length < column+1)
            throw new IllegalArgumentException("columnInfo.length="+columnInfo.length+
                ", column="+column);
        return columnInfo[column];
    }
    
    class HtmlTreeViewAdapter implements ExtendedHtmlTreeView{

    	public HtmlTreeView base;
    	
    	public HtmlTreeViewAdapter(HtmlTreeView base){
    		this.base = base;
    	}    	
    	
        private HtmlImage getIconImage(Object element, boolean expanded)
        {
            HtmlImage image = null; //view.getIconImage(element, expanded);

            if (image == null)
            {
                if (expanded)
                    image = theme.getExpandedFolderImage();
                else
                    image = theme.getCollapsedFolderImage();
            }
            return image;
        }

		public String getHtml(Object element) {
			return base.getHtml(element);
		}
		
		public HtmlImage getIcon(Object element, boolean expanded) {			
			return getIconImage(element, expanded);
		}    	
    }
}
