package pl.compan.docusafe.web.tree;

import pl.compan.docusafe.util.HtmlImage;

/**
 * Widok, kt�ry umo�liwa dodatkowo podawanie ikon
 * @author MSankowski
 *
 */
public interface ExtendedHtmlTreeView extends HtmlTreeView{
	HtmlImage getIcon(Object element, boolean expanded);
}
