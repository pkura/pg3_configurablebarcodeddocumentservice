package pl.compan.docusafe.web.tree;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeView.java,v 1.1 2004/08/30 16:03:40 lk Exp $
 */
public interface HtmlTreeView
{
    /**
     * Zwraca kod html, kt�ry zostanie u�yty jako tytu� elementu drzewa.
     * @return
     */
    String getHtml(Object element);
    //String getIconImage(Object element, boolean expanded);
}
