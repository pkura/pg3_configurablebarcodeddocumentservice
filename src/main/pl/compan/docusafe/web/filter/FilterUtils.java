package pl.compan.docusafe.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Klasa pomocnicza do filtr�w
 */
public class FilterUtils  {

    private static final Logger log = LoggerFactory.getLogger(FilterUtils.class);

    private static final Set<String> ANONYMOUS_ACCESS_URLS = new HashSet<String>();
    private static final Set<String> ANONYMOUS_ACCESS_PATTERS = new HashSet<String>();

    /**
     * uzupe�nia colekcj� anonimowych dost�p�w do DS
     * @param accesses
     */
    public static void fillAccesses(String[] accesses){
        for (String anon : accesses)
        {
            if (anon.endsWith("/*"))
                ANONYMOUS_ACCESS_PATTERS.add(anon.substring(0, anon.length()-1));
            else
                ANONYMOUS_ACCESS_URLS.add(anon);
        }
    }

    public static boolean checkAnonymousAccess(String uri)
    {
        log.info("acces_url size={}, access_patterns size={}", ANONYMOUS_ACCESS_URLS.size(), ANONYMOUS_ACCESS_PATTERS.size());
        boolean allowed = false;

        if (ANONYMOUS_ACCESS_URLS.contains(uri))
        {
            allowed = true;
        }
        else
        {
            // tworz� na podstawie bie��cego urla �cie�k�, do kt�rej do��czam
            // kolejne elementy �cie�ki urla (zawsze z '/' na ko�cu).
            // po ka�dym do��czeniu elementu �cie�ki sprawdzam, czy znajduje si�
            // ona w anonymousAccessPatterns
            String[] parts = uri.split("/");
            StringBuilder path = new StringBuilder("/");
            for (int i=0; i < parts.length; i++)
            {
                if ("".equals(parts[i])) continue;
                path.append(parts[i]);
                path.append('/');
                if (ANONYMOUS_ACCESS_PATTERS.contains(path.toString()))
                {
                    allowed = true;
                    break;
                }
            }
        }

        return allowed;
    }
}
