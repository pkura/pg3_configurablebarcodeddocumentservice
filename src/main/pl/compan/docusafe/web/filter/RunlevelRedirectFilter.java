package pl.compan.docusafe.web.filter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalConstants;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/* User: Administrator, Date: 2005-09-09 13:49:09 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RunlevelRedirectFilter.java,v 1.3 2006/02/20 15:42:39 lk Exp $
 */
public class RunlevelRedirectFilter extends DocusafeHttpFilter
{
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
        String uri = (String) request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI);

        if (Docusafe.getRunlevel() == Docusafe.RL_NEWINSTANCE_INIT)
        {
            if (!uri.startsWith("/init/"))
            {
                response.sendRedirect(request.getContextPath()+"/init/configuration.do");
                return;
            }
        }
        else if (Docusafe.getRunlevel() < Docusafe.RL_RUNNING)
        {
            if (!uri.startsWith("/boot.jsp"))
            {
                response.sendRedirect(request.getContextPath()+"/boot.jsp");
                response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
                return;
            }
        }
        else if (uri.startsWith("/boot.jsp"))
        {
            response.sendRedirect(request.getContextPath());
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            return;
        }

        filterChain.doFilter(request, response);
    }

    public int getRequiredRunlevel()
    {
        return Docusafe.RL_NONE;
    }
}
