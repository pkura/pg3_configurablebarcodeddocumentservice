package pl.compan.docusafe.web.filter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalConstants;
import pl.compan.docusafe.util.ContextLocale;
import pl.compan.docusafe.util.LocaleUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
/* User: Administrator, Date: 2005-09-09 12:54:28 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrepareEnvironmentFilter.java,v 1.3 2006/02/20 15:42:39 lk Exp $
 */
public class PrepareEnvironmentFilter extends DocusafeHttpFilter
{
    private final static Logger LOG = LoggerFactory.getLogger(PrepareEnvironmentFilter.class);
    private String requestCharacterEncoding;

    public void init(FilterConfig filterConfig) throws ServletException
    {
        requestCharacterEncoding = filterConfig.getInitParameter("request-character-encoding");
        if (requestCharacterEncoding == null)
            throw new NullPointerException("requestCharacterEncoding");

        // sprawdzenie, czy wybrana nazwa kodowania jest poprawna
        try
        {
            "_".getBytes(requestCharacterEncoding);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ServletException("Podano nieznan� nazw� kodowania znak�w " +
                "dla HttpServletRequest.setCharacterEncoding()", e);
        }
    }

    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
//        LOG.info("filter - {} ", );

        Locale locale = LocaleUtil.getLocale();
        ContextLocale.setContextLocale(locale);

        // atrybuty u�ywane przez tagi JSTL
        request.setAttribute(Config.FMT_LOCALE, locale);
        request.setAttribute(Config.FMT_FALLBACK_LOCALE, LocaleUtil.getLocale());

        // domy�lne kodowanie znak�w
        if("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
            request.setCharacterEncoding("UTF-8");
        } else {
            request.setCharacterEncoding(requestCharacterEncoding);
        }

        // oryginalny adres u�yty do wywo�ania strony jest zapami�tywany
        // jako atrybut requestu
        String requestURI = request.getRequestURI();
        String relativeUri = requestURI.substring(request.getContextPath().length(), requestURI.length());
        request.setAttribute(GlobalConstants.ORIGINAL_REQUEST_URI, relativeUri);

        filterChain.doFilter(request, response);
    }

    public int getRequiredRunlevel()
    {
        return Docusafe.RL_NONE;
    }
}
