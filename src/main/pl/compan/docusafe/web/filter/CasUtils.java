package pl.compan.docusafe.web.filter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.Map;
import javax.servlet.Filter;
import javax.naming.NamingException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpException;
import org.apache.http.client.methods.HttpGet;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.util.XmlUtils;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.alfresco.AlfrescoUtils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.ServletUtils;

/**
 * klasa pomocnicza dla us�ug zwi�zanych z Cas
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class CasUtils {

    private static Logger log = LoggerFactory.getLogger(CasUtils.class);

    private static final Map<String , Assertion> assertions = Maps.newHashMap();

    public static final String ALFRESCO_TICKET = "alf_ticket";

    public static void putAssertions(String key, Assertion ticket){
        log.debug("key={} v={}",key, ticket);
        assertions.put(key, ticket);
    }

    public static Assertion getAssertions(String key){
            return assertions.get(key);
    }

    /**
     * Pobiera ticket alfresco
     * @param user
     * @return
     */
    public static String getProxyTicket(DSUser user){
        String ticket = null;
        try{
            String username = null;
            if(assertions.containsKey(user.getName())){
               username = user.getName();
            } else if(assertions.containsKey(user.getExternalName())) {
                username = user.getExternalName();
            }

            ticket = getProxyTicket(username);
        }catch(Exception e){
            log.error("", e);
        }

        return ticket;
    }
    public static String getProxyTicket(String username) {
        String ticket = null;

        try {
            ticket = getAssertions(username).getPrincipal().getProxyTicketFor(AdditionManager.getProperty("cas.proxyServer.urlFor"));
        }catch (Exception e) {
            log.error("", e);
        }

        log.debug("alfresco ticket: {}" , ticket);
        return ticket;
    }

    /**
     * Czy cas jest w��czony.
     * @return
     */
    public static boolean isCasEnabled(){
        return AdditionManager.getBooleanProperty("cas.enabled");
    }

    public static String getCasServerLoginUrl(){
        return AdditionManager.getProperty("cas.server.login_url");
    }

    public static String getCasServerLogoutUrl(){
        return AdditionManager.getProperty("cas.server.logout_url");
    }


    public static String getCasServerUrlPrefix(){
        return AdditionManager.getProperty("cas.server.url_prefix");
    }

    /**
     * Adresy uri jakie maj� byc nieblokowane przez CAS
     * @TODO do poprawy i u�ycia anonymous-access z filtra AuthFilter
     * @param uri
     * @return
     */
    public static boolean isNotCasable(String uri){
        return FilterUtils.checkAnonymousAccess(uri);
    }

    public static String getTicket() {
        HttpServletRequest request = ServletActionContext.getRequest();

        HttpClient client = new HttpClient();
        String ticket = null;

        try {
            ticket = getAlfrescoTicket(request, client);
        } catch (NamingException e) {
            log.error("Need to set JNDI variable alfrescoApp if using Alfresco", e);
        } catch (Exception e) {
            log.error("", e);
        }

        log.debug("alfresco ticket:" + ticket);

        return ticket;
    }

    private static String getAlfrescoTicket(HttpServletRequest request, HttpClient client)
            throws UnsupportedEncodingException, IOException, HttpException,
            ServletException, NamingException, EdmException {

        String username = DSApi.context().getDSUser().getExternalName();

        // Read out the ticket id
        String ticket = null;

        final String proxyTicket = getProxyTicket(DSApi.context().getDSUser());
        log.debug("proxyTicket {}", proxyTicket);
        StringBuilder sb = getAlfrescoLoginCasUri()
                .append("?u=").append(username)
                .append("&t=").append(proxyTicket)
                ;


        log.info("url={}", sb.toString());
       // String response = CommonUtils.getResponseFromServer(sb.toString(), "UTF-8");

        String response = getRequestUrl(client, username, proxyTicket, sb);
        log.debug("resp {}", response);
        ticket = XmlUtils.getTextForElement(response, "ticket");
        log.debug("ticket {}", ticket);
        return ticket;
    }

    private static StringBuilder getAlfrescoLoginCasUri() {
        return new StringBuilder(AdditionManager.getProperty("cas.proxyServer.urlFor"))
                .append(Docusafe.getAdditionPropertyOrDefault("alfresco.logincas.prefix", "/api/logincas"));
    }

    private static String getRequestUrl(HttpClient client, String username, String proxyticket, StringBuilder sb) throws IOException {
        String ticket = null;
        URL url = new URL(sb.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        try
        {
            conn.setRequestMethod("GET");
            //conn.setRequestProperty("SOAPAction", "GET");
            int statusCode = conn.getResponseCode();
            // Read back the ticket
            if (statusCode == 200) {
                ticket = IOUtils.toString(conn.getInputStream());
            } else {
                log.error("Authentication failed, received response code: {}", statusCode);
                log.error("{}", IOUtils.toString(conn.getInputStream()));
            }
            log.debug("ticket response : {} ", ticket);
        }catch(Exception e){
            log.error("", e);
        }finally{
            conn.disconnect();
        }
        return ticket;
    }

    /**
     * Validuje ticket proxy w casie.
     * @param ticket
     * @return Zwraca username u�ytkownika
     * @throws UnsupportedEncodingException
     */
    public static String checkProxyTicket(String ticket) throws UnsupportedEncodingException, EdmException {
        String url = CasUtils.getCasServerUrlPrefix() + "/proxyValidate?ticket=" + ticket + "&service=" + URLEncoder.encode(Docusafe.getBaseUrl(), "UTF-8");
        log.info("url {}", url);
        String response = CommonUtils.getResponseFromServer(url, "UTF-8");
        log.info("cas-proxy response {} ", response);
        String userName = XmlUtils.getTextForElement(response, "user");

        DSUser user = getUserByNameOrExternalName(userName);

        if(user != null)
            return user.getName();
        else
            return null;
    }

    /**
     * Wyszukuje u�ytkownika po name lub external name
     * @param userName
     * @return
     */
    private static DSUser getUserByNameOrExternalName(String userName) throws EdmException {

        boolean ctx = DSApi.openContextIfNeeded();
        DSUser ret = null;
        try {
            ret = DSUser.findByUsername(userName);
        } catch (UserNotFoundException e) {
            try {
                ret = DSUser.findByExternalName(userName);
            } catch (EdmException e1) {
            }
        } catch (EdmException e) {
            throw new RuntimeException(e);
        } finally{
            DSApi.closeContextIfNeeded(ctx);
        }

        return ret;
    }
}