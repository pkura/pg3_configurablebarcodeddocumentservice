package pl.compan.docusafe.web.filter;

import org.jasig.cas.client.authentication.AttributePrincipalImpl;
import org.jasig.cas.client.jaas.AssertionPrincipal;
import org.jasig.cas.client.util.XmlUtils;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class CasDSProxyValidator extends Cas20ProxyTicketValidator {

    private static Logger log = LoggerFactory.getLogger(CasDSProxyValidator.class);
    public CasDSProxyValidator(final String casServerUrlPrefix) {
        super(casServerUrlPrefix);
    }

    public Assertion validate(final String ticket, final String service) throws TicketValidationException {

        final String validationUrl = constructValidationUrl(ticket, service);
        log.debug("Constructing validation url: " + validationUrl);

        try {
            log.debug("Retrieving response from server.");
            final String serverResponse = retrieveResponseFromServer(new URL(validationUrl), ticket);

            if (serverResponse == null) {
                throw new TicketValidationException("The CAS server returned no response.");
            }

            log.debug("Server response: " + serverResponse);

            return parseResponseFromServer(serverResponse);
        } catch (final MalformedURLException e) {
            log.error("", e);
            throw new TicketValidationException(e);
        }
    }

    @Override
    protected void customParseResponse(String serverResponse, Assertion assertion) throws TicketValidationException {
        final String principal = XmlUtils.getTextForElement(serverResponse, "user");
        CasUtils.putAssertions(principal, assertion);
    }
}
