package pl.compan.docusafe.web.filter;

import java.io.IOException;
import java.util.*;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jasig.cas.client.proxy.Cas20ProxyRetriever;
import org.jasig.cas.client.proxy.CleanUpTimerTask;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.AbstractTicketValidationFilter;
import org.jasig.cas.client.validation.Cas10TicketValidationFilter;
import org.jasig.cas.client.validation.Cas10TicketValidator;
import org.jasig.cas.client.validation.Cas20ProxyReceivingTicketValidationFilter;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.jasig.cas.client.validation.TicketValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.GlobalConstants;

/**
 * klasa wczytuj�ca ustawienia z HOME i w zale�no�ci od tego, czy CAS jest w��czony, uruchamia odpowiedni filtr lub go
 * zupe�nie pomija
 *
 * Stara klasa
 * 
 * @author bkaliszewski
 */
@Deprecated
public class CasTicketValidationFilter extends
AbstractTicketValidationFilter
//Cas20ProxyReceivingTicketValidationFilter
{

    private static Logger log = LoggerFactory.getLogger(CasTicketValidationFilter.class);
	/** nalezy wykonac init OnDemand, jednak nie moze on by� na r�wni z init(), poniewa� nie jest wtedy wczytany jeszcze HOME */
	private boolean initiatedFromHomeProperties= false;

    private static final int DEFAULT_MILLIS_BETWEEN_CLEANUPS = 60 * 1000;
	/** zmienna wczytywana z HOME. Je�li false, to filtr w og�le nie jest w��czany */
	private boolean casEnabled;
    /**
     * The URL to send to the CAS server as the URL that will process proxying requests on the CAS client.
     */
    private String proxyReceptorUrl;

    /**
     * Storage location of ProxyGrantingTickets and Proxy Ticket IOUs.
     */
    private ProxyGrantingTicketStorage proxyGrantingTicketStorage = new ProxyGrantingTicketStorageImpl();

	public CasTicketValidationFilter()
	{
		this.ignoreInitConfiguration= true;
	}

    public void init() {
        // podstawowa inicjacja, do poprawy pozniej
        this.serverName= "x";
        setTicketValidator(new Cas20ProxyTicketValidator("x"));
        //this.millisBetweenCleanUps= DEFAULT_MILLIS_BETWEEN_CLEANUPS;
        this.redirectAfterValidation= true;

        super.init();
    }


	@Override
	public final void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException
	{
        if (!initiatedFromHomeProperties)
            initFromHomeProperties();

        boolean isBackDoor = Boolean.valueOf(((HttpServletRequest) servletRequest).getHeader("X-DOCUSAFE-CAS-BACKDOOR"));
        
		if (casEnabled && !isBackDoor) {
//            try {
//                CommonUtils.readAndRespondToProxyReceptorRequest((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, this.proxyGrantingTicketStorage);
//            } catch (final RuntimeException e) {
//                log.error(e.getMessage(), e);
//            }

            String uri= ((javax.servlet.http.HttpServletRequest) servletRequest).getRequestURI();
			if (uri.startsWith("/docusafe"))
				uri= uri.substring(("/docusafe").length()); // drobny fix

			if (!CasUtils.isNotCasable(uri)) {
				super.doFilter(servletRequest, servletResponse, filterChain);
				return;
			}
		}
		
		filterChain.doFilter(servletRequest, servletResponse);
	}



	private void initFromHomeProperties()
	{
		casEnabled= Boolean.parseBoolean(Docusafe.getAdditionProperty("cas.enabled"));
        String sName = Docusafe.getAdditionProperty("cas.server.name");
        serverName = sName != null && sName.length() > 0 ? sName : Docusafe.getBaseContextUrl();
        setProxyReceptorUrl(Docusafe.getAdditionPropertyOrDefault("cas.proxyReceptorUrl", null));
		String casServerUrlPrefix= Docusafe.getAdditionProperty("cas.server.url_prefix");

		if (casEnabled)
			setTicketValidator(getTicketValidator(casServerUrlPrefix, serverName));

		initiatedFromHomeProperties= true;
	}

    public void destroy() {
        super.destroy();
    }

	protected final TicketValidator getTicketValidator(String casServerUrlPrefix, String serverName) {
        final String allowAnyProxy = Docusafe.getAdditionPropertyOrDefault("cas.acceptAnyProxy", null);
        final String allowedProxyChains = Docusafe.getAdditionPropertyOrDefault("cas.allowedProxyChains", null);
        final Cas20ServiceTicketValidator validator;

        if (CommonUtils.isNotBlank(allowAnyProxy) || CommonUtils.isNotBlank(allowedProxyChains)) {
            final Cas20ProxyTicketValidator v = new CasDSProxyValidator(casServerUrlPrefix);
            v.setAcceptAnyProxy(parseBoolean(allowAnyProxy));
            v.setAllowedProxyChains(CommonUtils.createProxyList(allowedProxyChains));
            validator = v;
        } else {
            validator = new Cas20ServiceTicketValidator(casServerUrlPrefix);
        }

        validator.setProxyCallbackUrl(Docusafe.getAdditionPropertyOrDefault("cas.proxyCallbackUrl", null));
        validator.setProxyGrantingTicketStorage(this.proxyGrantingTicketStorage);
        validator.setProxyRetriever(new Cas20ProxyRetriever(casServerUrlPrefix, Docusafe.getAdditionPropertyOrDefault("cas.validation.encoding", "UTF-8")));
        validator.setRenew(false);
        validator.setHostnameVerifier(null);
        validator.setEncoding(null);

        return validator;
    }

    public final void setProxyReceptorUrl(final String proxyReceptorUrl) {
        this.proxyReceptorUrl = proxyReceptorUrl;
    }

    public void setProxyGrantingTicketStorage(final ProxyGrantingTicketStorage storage) {
        this.proxyGrantingTicketStorage = storage;
    }
}
