package pl.compan.docusafe.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jasig.cas.client.authentication.AuthenticationFilter;

import org.jasig.cas.client.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;

/**
 * klasa wczytuj�ca ustawienia z HOME i w zale�no�ci od tego, czy CAS jest w��czony, uruchamia odpowiedni filtr lub go
 * zupe�nie pomija
 * 
 * @author bkaliszewski
 */
public class CasAuthenticationFilter extends AuthenticationFilter
{
    private static Logger log = LoggerFactory.getLogger(CasAuthenticationFilter.class);
	/** nalezy wykonac init OnDemand, jednak nie moze on by� na r�wni z init(),
         poniewa� nie jest wtedy wczytany jeszcze HOME */
	private boolean initiatedFromHomeProperties= false;

	/** zmienna wczytywana z HOME. Je�li false, to filtr w og�le nie jest w��czany */
	private boolean casEnabled;

	public CasAuthenticationFilter()
	{
		this.ignoreInitConfiguration= true;
	}

	@Override
	public void init()
	{
		// podstawowa inicjacja, do poprawy pozniej
		this.casServerLoginUrl= "x";
		this.serverName= "x";
		//this.
		//renew=true;
		super.init();
	}

	@Override
	public final void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (!initiatedFromHomeProperties)
			initFromHomeProperties();

		boolean isBackDoor = Boolean.valueOf(((HttpServletRequest) servletRequest).getHeader("X-DOCUSAFE-CAS-BACKDOOR"));
		
		if (casEnabled && !isBackDoor) {
            final HttpServletRequest request = (HttpServletRequest) servletRequest;
            final String ticket = CommonUtils.safeGetParameter(request, getArtifactParameterName());
            if(StringUtils.isNotBlank(ticket)){
                log.error("tt {}", ticket);
            }
			String uri= ((javax.servlet.http.HttpServletRequest) servletRequest).getRequestURI();
			if (uri.startsWith("/docusafe"))
				uri= uri.substring(("/docusafe").length()); // drobny fix
			
			if (!CasUtils.isNotCasable(uri)) {
				super.doFilter(servletRequest, servletResponse, filterChain);
				return;
			}
		}
		
		filterChain.doFilter(servletRequest, servletResponse);
	}

	private void initFromHomeProperties()
	{
        casEnabled= CasUtils.isCasEnabled();
		if(casEnabled)
		{
            String sName = Docusafe.getAdditionProperty("cas.server.name");
			serverName = sName != null && sName.length() > 0 ? sName : Docusafe.getBaseContextUrl();
			casServerLoginUrl= Docusafe.getAdditionProperty("cas.server.login_url");

			System.out.println("CAS enabled: " + casEnabled + ", serverName: " + serverName+", renew: "+renew);
		}
		initiatedFromHomeProperties= true;
	}
}
