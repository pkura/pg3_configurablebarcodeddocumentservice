package pl.compan.docusafe.web.filter;

import com.google.common.collect.Lists;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.certificates.auth.PublicKeyManager;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Filtr s�u��cy do weryfikacji host�w za pomoc� certyfikatu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@WebFilter(filterName = "HostVerificationFilter", urlPatterns = {"/*"})
public class HostVerificationFilter extends DocusafeHttpFilter {

    List<String> apiUrls = Lists.newArrayList("/api", "/services/");

    public static final String CERTIFICATE_SSL = "Certificate-ssl";

    private static final Logger log = LoggerFactory.getLogger(HostVerificationFilter.class);

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        log.trace(" doFilter ");

        if(!isRequestFromMobile(request)){
	        if (AvailabilityManager.isAvailable("api.host.ssl.enable")) {
	            //wymaga posiadania certyfiaktu w headerze
	            log.trace("dost�p do API {}", request.getRemoteAddr());
	            boolean isApiAccess = checkUri(fixUri(request.getRequestURI()));
	            if (isApiAccess && !validateCert(request)) {
	                log.error("Nie udane logowanie certyfikatem ssl dla hosta {} -> {}", request.getRemoteHost(), request.getRemoteAddr());
	                // response.sendRedirect(request.getContextPath() + "/ssl-login-error.jsp");
	                response.setHeader("Reason", "Nieudane logowanie certyfikatem ssl dla danego hosta " + request.getRemoteHost());
	                response.setStatus(Response.SC_BAD_REQUEST);
	                return;
	            }
	            //else {
	             //   filterChain.doFilter(request, response);
	           // }
	        }
        }
        filterChain.doFilter(request, response);
    }

    private boolean isRequestFromMobile(HttpServletRequest request) {
    	//you're in mobile land
    	if (request.getHeader("User-Agent")!=null && !request.getHeader("User-Agent").isEmpty()){
    		return request.getHeader("User-Agent").indexOf("Mobile") != -1;
    	}
    	else 
    		return false;
		
	}

	/**
     * Waliduje przes�any certyfiakt
     *
     * @param request
     * @return
     */
    private boolean validateCert(HttpServletRequest request) {
        String certBase64 = request.getHeader(CERTIFICATE_SSL);
        log.trace("cert {}", certBase64);
        boolean isOk = false, openCotext = false;

        try {
            openCotext = DSApi.openContextIfNeeded();
            isOk = PublicKeyManager.validateByHostName(request.getRemoteAddr(), fixUri(request.getRequestURI()), certBase64);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            DSApi.closeContextIfNeeded(openCotext);
        }

        return isOk;
    }

    private boolean checkUri(String uri){

        for(String u : apiUrls){
            if(uri.startsWith(u))
                return true;
        }
        return false;
    }
    private String fixUri(String uri) {
        if (uri.startsWith("/docusafe"))
            uri = uri.substring(("/docusafe").length()); // drobny fix

        return uri;
    }

    /**
     * @return Minimalny poziom uruchomienia aplikacji, na kt�rym filtr
     * mo�e dzia�a�.
     */
    @Override
    public int getRequiredRunlevel() {
        return Docusafe.RL_HIBERNATE_INIT;
    }
}
