package pl.compan.docusafe.web.filter;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.security.auth.Subject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalConstants;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PasswordPolicy;
import pl.compan.docusafe.core.sso.Iis;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.CASLogin;
import pl.compan.docusafe.core.users.auth.CertificatePrincipal;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.core.users.auth.IISLogin;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.web.common.Sitemap;
/* User: Administrator, Date: 2005-09-09 13:12:33 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AuthFilter.java,v 1.23 2009/07/22 08:11:04 tomekl Exp $
 */
public class AuthFilter extends DocusafeHttpFilter
{
	/**
	 * bugfix dla b��dnej nazwy Certyfikatu (cn) dla strony opzz-dev.demo.eo.pl
	 * bez tej podmiany rzuca b��dem javy dla SSL (dotyczy tylko Logowania CAS)
	 */
	static {
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){
 
	        public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("*.demo.eo.pl")) {
	                return true;
	            }
	            
	            System.out.println("Nie mo�na zweryfikowa� Https: "+hostname);
	            return false;
	        }
	    });
	}
	
    private static final Logger log = LoggerFactory.getLogger(AuthFilter.class);

    private static final String LAST_PASSWORD_CHECK_KEY = AuthFilter.class.getName()+".LAST_CHECK_KEY";
    public final static String SUBJECT_KEY = AuthFilter.class.getName()+"SUBJECT_KEY";
    public final static String LANGUAGE_KEY = AuthFilter.class.getName()+"LANGUAGE_KEY";
    
    private int PASSWORD_VALID_OK = 0;
    private int PASSWORD_VALID_EXPIRED = 1;
    private int PASSWORD_VALID_NOT_BEEN_CHANGED = 2;
    /**
     * Cz�stotliwo�� sprawdzania wa�no�ci has�a (co godzin�).
     */
    private static final long PASSWORD_CHECK_INTERVAL = 1000 * 60 * 60;

    private String loginModuleName;
    private String loginPage;
    private String insufficientPermissionsPage;
    private String passwordRecoveryPage;
    private Boolean casEnabled;
    private Boolean iisEnabled;
    private String casLogout;

	private boolean initiatedFromHomeProperties;

    public void init(FilterConfig filterConfig) throws ServletException
    {
        loginModuleName = filterConfig.getServletContext().getInitParameter("loginModuleName");
        loginPage = filterConfig.getInitParameter("login-page");
        insufficientPermissionsPage = filterConfig.getInitParameter("insufficient-permissions-page");
        passwordRecoveryPage = filterConfig.getInitParameter("password-recovery-page");

        String[] anon = filterConfig.getInitParameter("anonymous-access").trim().split("\\s*,\\s*");
        FilterUtils.fillAccesses(anon);
    }
    
    @Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
    	if( !initiatedFromHomeProperties )
			initFromHomeProperties();
    	
    	String uri = (String) request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI);
    	if( uri.startsWith("/docusafe"))
    		uri= uri.substring(("/docusafe").length());		//drobny fix
    	
    	boolean allowed;
    	Subject subject = (Subject) request.getSession(true).getAttribute(SUBJECT_KEY);
    	// 34e99612-78b2-4e6b-883d-a5752fef9138    	
    	
    	// Sprawdzenie, czy w adresie URL nie zosta� przes�any GUID
    	
    	if(iisEnabled!=null && iisEnabled && uri.startsWith("/logout.do"))
		{
    		request.getSession(true).invalidate();
			
			 String queryString = request.getQueryString();
			 
			//response.sendRedirect(casLogout);
			 response.sendRedirect(request.getContextPath()+loginPage+"?url="+
                       URLEncoder.encode(uri+
                       (queryString != null ? "?"+queryString : ""), "utf-8"));
			return;
		}
    	else    	
    	// logowanie IIS
    	if (iisEnabled!=null && iisEnabled && subject==null && !uri.startsWith(loginPage))
    	{   
    		String iis_url = Docusafe.getAdditionProperty("iis.url");
    		String iis_back_url = Docusafe.getAdditionProperty("iis.back.url");
    		String iis_domain = Docusafe.getAdditionProperty("iis.domain");
    		
    		if (request.getParameter("SSO_GUID")!=null)
    		{
    			String sso_guid = request.getParameter("SSO_GUID");
    			
    			// Wczytanie z bazy danych infromacji o requescie
    			
    			Iis _sso;
				try {
					if (!DSApi.isContextOpen())
					{
						DSApi.openAdmin();			
					}
					
					try
					{
					_sso = Iis.findByGuidSso(sso_guid);
					}
					catch (EdmException edmExc)
					{
						// Nie znaleziono takiego u�ytkownika
						// Brak mo�liwo�ci zalogowania automatycznego, wy�wietlamy stron� z logowaniem dla u�ytkownika
						
						response.sendRedirect("http://localhost:8080/wssk/login.jsp");
						return;
					}
						
					DSApi.close();
					
					if (_sso!=null){
	    				// Utworzenie kontekstu na podstawie loginu u�ytkownika
						
						
			    		/*// wylogowujemy
			    		if( uri.startsWith("/logout.do"))
			    		{
			    			request.getSession(true).invalidate();
			    			response.sendRedirect(casLogout);
			    			return;
			    		}*/
			    		
						/*// nie pokazujemy strony logowania Docusafe
						if (uri.startsWith("/login")) {
							response.sendRedirect(request.getContextPath() + "/office/tasklist/user-task-list.action");
							return;
						}*/
			    		
			    		// nie potwierdzone logowanie...
			    			
			    		// potwierdzone logowanie, logujemy uzytkownika
			    		if (subject == null && _sso.getLogin_ad()!= null )
			    		{
			    			// Pobranie u�ytkownika z ca�ej nazwy domenowej
			    			String _pelna_nazwa = _sso.getLogin_ad();
			    			
			    			// Nie zalogujemy u�ytkownika je�eli nie zgadza si� domena			    			
			    			if (!_pelna_nazwa.toUpperCase().startsWith(iis_domain.toUpperCase()))
			    				return;
			    			
			    			int index = _pelna_nazwa.lastIndexOf('\\');
			    			String _login_uzytkownika = _pelna_nazwa.substring(index+1);
			    			
			    			try
			    			{
			    			IISLogin.login(request, response, _login_uzytkownika );
			    			}
			    			catch (LoginException le)
			    			{
			    				// Login nie wyst�puje w bazie danych
			    				// Wy�wietlamy okono do logowania
			    				 if (subject == null)
			    			        {
			    			        	allowed= FilterUtils.checkAnonymousAccess(uri);
			    			        }
			    			        else
			    			        {
			    			        	allowed= checkRoles(subject);
			    			        }
			    		    	
			    			        // niezalogowany u�ytkownik przekierowywany jest do strony logowania
			    			        // zalogowany bez uprawnie� przekierowany do insufficient-permissions.jsp
			    			        if (!allowed)
			    			        {
			    			            if (subject == null)
			    			            {
			    			                String queryString = request.getQueryString();
			    			
			    			                if (!uri.startsWith(loginPage))
			    			                {
			    			                    response.sendRedirect(request.getContextPath()+loginPage+"?url="+
			    			                        URLEncoder.encode(uri+
			    			                        (queryString != null ? "?"+queryString : ""), "utf-8"));
			    			                }
			    			            }
			    			            else
			    			            {
			    			                response.sendRedirect(request.getContextPath()+insufficientPermissionsPage);
			    			            }
			    			
			    			            return;
			    			        }
			    			}
			    			return;
			    		}
			    		
						return;
					}
					
				} catch (EdmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		else
    		{
    			// Logowanie IIS jest wymagane
    			
    			 UUID uuid = UUID.randomUUID();        
    			 String randomUUIDString = uuid.toString();
    			
    			response.sendRedirect(iis_url+"?GUID_SOURCE="+randomUUIDString+"&BACK_URL="+iis_back_url);
    			
    			return;
    		}
    	}
    	
    	// only logowanie CAS
    	boolean isBackDoor = Boolean.valueOf(((HttpServletRequest) request).getHeader("X-DOCUSAFE-CAS-BACKDOOR"));
    	if( (casEnabled!=null && casEnabled) && !isBackDoor )
    	{
    		Assertion assertion;
    		assertion= (Assertion) request.getSession(true).getAttribute("_const_cas_assertion_");
    		
    		// wylogowujemy
    		if( uri.startsWith("/logout.do"))
    		{
    			request.getSession(true).invalidate();
    			response.sendRedirect(casLogout);
    			return;
    		}
    		
			// nie pokazujemy strony logowania Docusafe
			if (uri.startsWith("/login")) {
				response.sendRedirect(request.getContextPath() + "/office/tasklist/user-task-list.action");
				return;
			}
    		
    		// nie potwierdzone logowanie...
    		if( assertion==null)
    		{
    			if( !FilterUtils.checkAnonymousAccess(uri) )
    			{
    				// brak zalogowania, brak dost�pu anonimowego - przekieruj na stron� brak uprawnie�
    				response.sendRedirect(request.getContextPath()+insufficientPermissionsPage);
    				return;
    			}
    			// else: dost�p anonimowy, kontynuuj
    		}
    			
    		// potwierdzone logowanie, logujemy uzytkownika
    		if (subject == null && assertion!= null )
    		{
    			CASLogin.loginCAS(request, response, assertion );
    			return;
    		}
    		if(AvailabilityManager.isAvailable("AuthFilter.CasEnabled.checkPerrmisionsOnAccesToAdministrationPanel")&& uri.contains("admin") && !checkRoles(subject)){
    			response.sendRedirect(request.getContextPath()+insufficientPermissionsPage);
				return;
    		}
    	}
    	// without cas
    	else
    	{
			certificateLogin(request, response);
	
	        if (subject == null)
	        {
	        	allowed= FilterUtils.checkAnonymousAccess(uri);
	        }
	        else
	        {
	        	allowed= checkRoles(subject);
	        }
    	
	        // niezalogowany u�ytkownik przekierowywany jest do strony logowania
	        // zalogowany bez uprawnie� przekierowany do insufficient-permissions.jsp
	        if (!allowed)
	        {
	            if (subject == null)
	            {
	                String queryString = request.getQueryString();
	
	                if (!uri.startsWith(loginPage))
	                {
	                    response.sendRedirect(request.getContextPath()+loginPage+"?url="+
	                        URLEncoder.encode(uri+
	                        (queryString != null ? "?"+queryString : ""), "utf-8"));
	                }
	            }
	            else
	            {
	                response.sendRedirect(request.getContextPath()+insufficientPermissionsPage);
	            }
	
	            return;
	        }
    	}

        try
        {
//            if (!checkPasswordValid(request, response))
//                return;
            HttpSession session = request.getSession(true);

            // trzeba przepu�ci� strony powiadomienia o konieczno�ci
            // zmiany has�a, stron� zmiany has�a oraz pliki pomocnicze
            // (style, obrazki, funkcje JS)
            Integer passwordValidResult = null;
            if (!(uri.startsWith("/self-change-password.do") ||
                  uri.startsWith("/password-validity-check.action") ||
                  uri.startsWith("/functions.js") ||
                  uri.startsWith("/json.js") ||
                  uri.startsWith("/calendar096/") ||
                  uri.startsWith("/main.css") ||
                  uri.startsWith("/img/") ||
                  uri.startsWith("/logout.do")) &&
                (session.getAttribute(GlobalConstants.PASSWORD_EXPIRED) == Boolean.TRUE ||
                		(passwordValidResult = isPasswordValid(request, response)) > 0))
            {
                session.setAttribute(GlobalConstants.PASSWORD_EXPIRED, Boolean.TRUE);
                if (passwordValidResult != null)
                	response.sendRedirect(
                			request.getContextPath().concat("/password-validity-check.action?what=").concat(passwordValidResult.toString()));
                else
                	response.sendRedirect(request.getContextPath().concat("/self-change-password.do"));
                return;
            }
        }
        catch (EdmException e)
        {
            throw new ServletException(e.getMessage(), e);
        }

        filterChain.doFilter(request, response);
    }

    /**
     * wczytuje ustawienia z HOME. musi byc uruchamiane w p�nejszym momencie ni� init(), inaczej nie zadzia�a
     * dlatego jest uruchamiane raz "on demand" przy doFilter
     */
	private void initFromHomeProperties()
	{
		casEnabled= Boolean.parseBoolean(Docusafe.getAdditionProperty("cas.enabled"));
		casLogout= Docusafe.getAdditionProperty("cas.server.logout_url");
		initiatedFromHomeProperties= true;
		if (Docusafe.getAdditionProperty("iis.enabled")!=null)
		{
			iisEnabled =Boolean.parseBoolean(Docusafe.getAdditionProperty("iis.enabled"));
		}
	}

	


	private boolean checkRoles(Subject subject)
	{
		boolean allowed;
		String[] requiredRoles = Sitemap.getRequiredRoles();
		Collection<String> forbiddenRoles = Arrays.asList(Sitemap
				.getForbiddenRoles());
		Set<RolePrincipal> rolePrincipals = subject
				.getPrincipals(RolePrincipal.class);

		if (requiredRoles != null) {
			/*
			 * Arrays.sort(requiredRoles); // zmienna, w kt�rej b�d� ustawia�
			 * bity odpowiadaj�ce znalezionym // rolom z tablicy requiredRoles
			 * int fill = 0;
			 * 
			 * 
			 * if (rolePrincipals != null && rolePrincipals.size() > 0) { for
			 * (Iterator iter=rolePrincipals.iterator(); iter.hasNext(); ) {
			 * String role = ((RolePrincipal) iter.next()).getName(); int pos =
			 * Arrays.binarySearch(requiredRoles, role); if (pos >= 0) { fill |=
			 * (1 << pos); } } }
			 * 
			 * // allowed = wszystkie bity ustawione allowed = fill == (1 <<
			 * requiredRoles.length)-1;
			 */

			ArrayList<String> roleNames = new ArrayList<String>();
			for (RolePrincipal role : rolePrincipals) {
				roleNames.add(role.getName());
			}

			allowed = true;
			for (String str : Arrays.asList(requiredRoles)) {
				if (!roleNames.contains(str)) {
					allowed = false;
				}
			}
		} else // brak wymaga� co do r�l
		{
			allowed = true;
		}
		if (forbiddenRoles != null) {
			for (RolePrincipal role : rolePrincipals) {
				if (forbiddenRoles.contains(role.getName())) {
					allowed = false;
				}
			}
		}
		return allowed;
	}

	/**
     * Zwraca true je�eli has�o u�ytkownika nie uleg�o przeterminowaniu.
     * Dla niezalogowanych u�ytkownik�w zawsze zwraca true.
     * Dla logowania z pliku zawsze zwraca true
     * TODO: trzyma� PasswordPolicy w dystrybuowanym cache, nie w ServletContext
     */
    private int isPasswordValid(HttpServletRequest request, HttpServletResponse response)
        throws EdmException, IOException
    {
        HttpSession session = request.getSession(true);

        Subject subject = (Subject) session.getAttribute(SUBJECT_KEY);

        if (subject == null)
            return PASSWORD_VALID_OK;
        
        DSApi.open(subject/*AuthUtil.getSubject(request)*/);
        try
        {
            String tmp = DSApi.context().getDSUser().getName();

        // Sprawdzenie czy u�ytkownik loguje sie z pliku
        // Je�li tak zwracamy true
            if (GlobalPreferences.getLoginFile(tmp) != null)
            {
                return PASSWORD_VALID_OK;
            }

            Long lastCheck = (Long) session.getAttribute(LAST_PASSWORD_CHECK_KEY);
    
            if (lastCheck == null ||
                (System.currentTimeMillis() - lastCheck.longValue()) > PASSWORD_CHECK_INTERVAL)
            {
               /* try
                {
                    DSApi.open(subject);*/
    
                    log.info("lastCheck "+lastCheck+" ("+DSApi.context().getPrincipalName()+")");
    
                    session.setAttribute(LAST_PASSWORD_CHECK_KEY, new Long(System.currentTimeMillis()));
    
                    Properties props = (Properties) Docusafe.getServletContext().
                        getAttribute(PasswordPolicy.PROPERTIES_KEY);
    
                    synchronized (Docusafe.getServletContext())
                    {
                        if (props == null)
                        {
                            props = PasswordPolicy.getProperties(
                                DSApi.context().systemPreferences().node(PasswordPolicy.NODE_PASSWORD_POLICY));
                            Docusafe.getServletContext().setAttribute(PasswordPolicy.PROPERTIES_KEY, props);
                        }
                    }
    
                    Boolean passwordExpires = (Boolean) props.get(PasswordPolicy.KEY_PASSWORD_EXPIRES);
                    Integer days = (Integer) props.get(PasswordPolicy.KEY_DAYS_TO_EXPIRE);
    
                    log.info("isPasswordValid days="+days+" expires="+passwordExpires.booleanValue()+
                        " isAdmin="+DSApi.context().isAdmin()+" ("+DSApi.context().getPrincipalName()+")");
    
                    DSUser user = DSApi.context().getDSUser();
                    Boolean activeDerectoryUser = false;
                    activeDerectoryUser = (user.getAdUser() == null ? false : user.getAdUser());
                    
                    if (((Boolean) props.get(PasswordPolicy.KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON)) == true)
                    	if (user.isChangePasswordAtFirstLogon())
                    		return PASSWORD_VALID_NOT_BEEN_CHANGED;
                    
                    if (passwordExpires.booleanValue() && days.intValue() > 0)
                    {
                        
                        Date lastChange = user.getLastPasswordChange();                        
                        	
                        if (lastChange == null)
                            lastChange = new Date(0);
    
                        int daysInto = DateUtils.difference(new Date(), lastChange);
                        log.info("isPasswordValid daysInto="+daysInto+", days="+days+
                            " days-daysInto="+(days.intValue() - daysInto));
    
                        // je�eli czas u�ywania bie��cego has�a przekroczy� dozwolony czas,
                        // zwracanie false (has�o niewa�ne)
                        if (!activeDerectoryUser && daysInto >= days.intValue())
                        {
                            log.trace("return false");
                            return PASSWORD_VALID_EXPIRED;
                        }
                        // trzy dni przed up�ywem wa�no�ci has�a ostrze�enie
                        else if (!activeDerectoryUser && days.intValue() - daysInto < 3)
                        {
                            log.info("days left="+(days.intValue() - daysInto));
                            Docusafe.getServletContext().setAttribute(
                                PasswordPolicy.DAYS_LEFT_KEY,
                                new Integer(days.intValue() - daysInto));
                        }
                        else
                        {
                            log.trace("remove days left");
                            Docusafe.getServletContext().removeAttribute(PasswordPolicy.DAYS_LEFT_KEY);
                        }
                    }
            /*    }
                finally
                {
                    DSApi._close();
                }*/
            }

        }
        finally
        {
            DSApi._close();
        }    
            
        return PASSWORD_VALID_OK;
    }

    private void certificateLogin(HttpServletRequest request, HttpServletResponse response)
        throws IOException
    {
        if (!request.isSecure())
            return;

        HttpSession session = request.getSession(true);

        // zalogowany u�ytkownik umieszczony w sesji
        Subject subject = (Subject) session.getAttribute(SUBJECT_KEY);

        /*
        Je�eli u�ytkownik nie jest zalogowany (subject == null), sprawdzanie
        certyfikatu i ewentualne zalogowanie u�ytkownika.
        Je�eli u�ytkownik jest zalogowany, sprawdzam czy certyfikat zgadza si�
        z zapami�tanym w Subject (CertificatePrincipal).
        Je�eli nie - wylogowuj�, je�eli tak, puszczam dalej.
        */
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (certs != null && certs.length > 0)
        {
            // je�eli zalogowano si� certyfikatem
            Set certPrincipals;
            if (subject != null &&
                (certPrincipals = subject.getPrincipals(CertificatePrincipal.class)) != null &&
                certPrincipals.size() > 0)
            {
                CertificatePrincipal certPrincipal = (CertificatePrincipal) certPrincipals.iterator().next();
                boolean valid = false;
                for (int i=0; i < certs.length; i++)
                {
                    if (certs[i].equals(certPrincipal.getCertificate()))
                    {
                        valid = true;
                        break;
                    }
                }

                // wylogowanie u�ytkownika nie pasuj�cego do certyfikatu
                if (!valid)
                {
                    session.invalidate();
                    response.sendRedirect(request.getContextPath());
                    return;
                }
            }
            // brak zalogowanego u�ytkownika, ale jest certyfikat
            else if (subject == null)
            {
                for (int i=0; i < certs.length; i++)
                {
                    try
                    {
                        LoginContext lc = new LoginContext(
                            loginModuleName,
                            new FormCallbackHandler(username, password, certs[i],
                                NetUtil.getClientAddr(request).getIp()));

                        lc.login();
                        subject = lc.getSubject();
                        session.setAttribute(AuthFilter.SUBJECT_KEY, subject);
                    }
                    catch (LoginException e)
                    {                        
                        log.info("Nie powiod�a si� pr�ba zalogowania u�ytkownika " +
                                "certyfikatem "+certs[i].getSubjectDN()+" ("+
                                certs[i].getIssuerDN()+"): "+e.toString());
                        
                    }
                }
            }
            // TODO: else - u�ytkownik zalogowany bez certyfikatu, teraz przekazuje certyfikat
        }
    }

    public int getRequiredRunlevel()
    {
        return Docusafe.RL_RUNNING;
    }
}
