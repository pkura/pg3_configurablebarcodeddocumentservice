package pl.compan.docusafe.web.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.boot.Docusafe;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/* User: Administrator, Date: 2005-09-09 12:44:54 */

/**
 * Klasa bazowa filtr�w Docusafe.  Wsp�pracuje z kernelem sprawdzaj�c,
 * czy aplikacja osi�gn�a odpowiedni poziom uruchomienia, aby filtr
 * m�g� dzia�a�.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocusafeHttpFilter.java,v 1.4 2007/12/23 15:27:40 wkuty Exp $
 */
public abstract class DocusafeHttpFilter implements Filter
{
    private static final Log log = LogFactory.getLog(DocusafeHttpFilter.class);

    public final void doFilter(ServletRequest servletRequest,
                               ServletResponse servletResponse,
                               FilterChain filterChain)
        throws IOException, ServletException
    {
        int runlevel = Docusafe.getRunlevel();

        // XXX: w�skie gard�o - Docusafe.getRunlevel() jest synchronizowana
        if (runlevel < getRequiredRunlevel())
        {
            log.debug("Pomijanie filtra "+getClass().getName()+", poniewa� " +
                "wymaga on poziomu "+getRequiredRunlevel()+", a bie��cym " +
                "poziomem jest "+runlevel);
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else
        {
            doFilter((HttpServletRequest) servletRequest,
                (HttpServletResponse) servletResponse, filterChain);
        }
    }

    public abstract void doFilter(HttpServletRequest request,
                                  HttpServletResponse response,
                                  FilterChain filterChain)
        throws IOException, ServletException;

    /**
     * @return Minimalny poziom uruchomienia aplikacji, na kt�rym filtr
     * mo�e dzia�a�.
     */
    public abstract int getRequiredRunlevel();

    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void destroy()
    {
    }
}
