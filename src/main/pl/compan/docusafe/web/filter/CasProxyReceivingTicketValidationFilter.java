package pl.compan.docusafe.web.filter;

import org.drools.util.StringUtils;
import org.jasig.cas.client.proxy.Cas20ProxyRetriever;
import org.jasig.cas.client.proxy.CleanUpTimerTask;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import std.filter;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Validator, s�u��cy do obs�ugi cas -proxy.
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class CasProxyReceivingTicketValidationFilter  extends AbstractTicketValidationFilter {

    private static final String[] RESERVED_INIT_PARAMS = new String[] {"proxyGrantingTicketStorageClass", "proxyReceptorUrl", "acceptAnyProxy", "allowedProxyChains", "casServerUrlPrefix", "proxyCallbackUrl", "renew", "exceptionOnValidationFailure", "redirectAfterValidation", "useSession", "serverName", "service", "artifactParameterName", "serviceParameterName", "encodeServiceUrl", "millisBetweenCleanUps"};

    private static final int DEFAULT_MILLIS_BETWEEN_CLEANUPS = 60 * 1000;

    /** @var nalezy wykonac init OnDemand, jednak nie moze on by� na r�wni z init(), poniewa� nie jest wtedy wczytany jeszcze HOME */
    private boolean initiatedFromHomeProperties= false;

    /** @var zmienna wczytywana z HOME. Je�li false, to filtr w og�le nie jest w��czany */
    private boolean casEnabled = false;

    private String casServerLoginUrl = "x";
    private String proxyCallBackUrl = null;

    /** @var musi by� inne ni� null bo si� filtr nie zainicjuje */
    private String casServerUrlPrefix = "x";
    /**
     * The URL to send to the CAS server as the URL that will process proxying requests on the CAS client.
     */
    private String proxyReceptorUrl = "/service/pgtCallback.action";

    private Timer timer;

    private TimerTask timerTask =null;

    private int millisBetweenCleanUps;

    /**
     * Storage location of ProxyGrantingTickets and Proxy Ticket IOUs.
     */
    private ProxyGrantingTicketStorage proxyGrantingTicketStorage = new ProxyGrantingTicketStorageImpl();
    private boolean acceptAnyProxy = false;

    protected void initInternal(final FilterConfig filterConfig) throws ServletException {
        setProxyReceptorUrl(proxyReceptorUrl);

        final String proxyGrantingTicketStorageClass = getPropertyFromInitParams(filterConfig, "proxyGrantingTicketStorageClass", null);

        if (proxyGrantingTicketStorageClass != null) {
            try {
                final Class storageClass = Class.forName(proxyGrantingTicketStorageClass);
                this.proxyGrantingTicketStorage = (ProxyGrantingTicketStorage) storageClass.newInstance();
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        }

        log.trace("Setting proxyReceptorUrl parameter: " + this.proxyReceptorUrl);
        this.millisBetweenCleanUps = Integer.parseInt(getPropertyFromInitParams(filterConfig, "millisBetweenCleanUps", Integer.toString(DEFAULT_MILLIS_BETWEEN_CLEANUPS)));
        super.initInternal(filterConfig);
    }

    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.proxyGrantingTicketStorage, "proxyGrantingTicketStorage cannot be null.");

        if (this.timer == null) {
            this.timer = new Timer(true);
        }

        if (this.timerTask == null) {
            this.timerTask = new CleanUpTimerTask(this.proxyGrantingTicketStorage);
        }
        this.timer.schedule(this.timerTask, this.millisBetweenCleanUps, this.millisBetweenCleanUps);
    }

    /**
     * Constructs a Cas20ServiceTicketValidator or a Cas20ProxyTicketValidator based on supplied parameters.
     *
     * @param filterConfig the Filter Configuration object.
     * @return a fully constructed TicketValidator.
     */
    protected final TicketValidator getTicketValidator(final FilterConfig filterConfig) {
        final String allowedProxyChains = getPropertyFromInitParams(filterConfig, "allowedProxyChains", null);
        Cas20ServiceTicketValidator validator = null;
        //obej�cie.
        String casServerPrefix = casServerUrlPrefix.equals("x") ? CasUtils.getCasServerUrlPrefix() : casServerUrlPrefix;
        try{
            System.out.println("acceptAny " + initAcceptAnyProxy());
            if (initAcceptAnyProxy() || CommonUtils.isNotBlank(allowedProxyChains)) {
                final Cas20ProxyTicketValidator v = new CasDSProxyValidator(casServerPrefix);
                v.setAcceptAnyProxy(acceptAnyProxy);
                v.setAllowedProxyChains(CommonUtils.createProxyList(allowedProxyChains));
                validator = v;
            } else {
                validator = new Cas20ServiceTicketValidator(casServerPrefix);
            }
            validator.setProxyCallbackUrl(initProxyCallBackUrl());
            validator.setProxyGrantingTicketStorage(this.proxyGrantingTicketStorage);
            validator.setProxyRetriever(new Cas20ProxyRetriever(casServerPrefix, getPropertyFromInitParams(filterConfig, "encoding", null)));
            validator.setRenew(parseBoolean(getPropertyFromInitParams(filterConfig, "renew", "false")));
            validator.setEncoding(getPropertyFromInitParams(filterConfig, "encoding", null));

            final Map additionalParameters = new HashMap();
            final List params = Arrays.asList(RESERVED_INIT_PARAMS);

            for (final Enumeration e = filterConfig.getInitParameterNames(); e.hasMoreElements();) {
                final String s = (String) e.nextElement();

                if (!params.contains(s)) {
                    additionalParameters.put(s, filterConfig.getInitParameter(s));
                }
            }

            validator.setCustomParameters(additionalParameters);
            validator.setHostnameVerifier(getHostnameVerifier(filterConfig));
        }catch(Exception e){
            e.printStackTrace();
        }
        return validator;
    }

    public void destroy() {
        super.destroy();
        this.timer.cancel();
    }

    /**
     * This processes the ProxyReceptor request before the ticket validation code executes.
     */
    protected final boolean preFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        if (!initiatedFromHomeProperties){
            initFromHomeProperties();
        }
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;
        final String requestUri = request.getRequestURI();

        if(!casEnabled){
            filterChain.doFilter(request, response);
            return false;
         }

        if (CommonUtils.isEmpty(this.proxyReceptorUrl) || !requestUri.endsWith(this.proxyReceptorUrl)) {
            return true;
        }

        try {
            CommonUtils.readAndRespondToProxyReceptorRequest(request, response, this.proxyGrantingTicketStorage);
        } catch (final RuntimeException e) {
            log.error(e.getMessage(), e);
            throw e;
        }

        return false;
    }

    private void initFromHomeProperties() {
        try{
            casEnabled = CasUtils.isCasEnabled();
            if(casEnabled){
                String sName = Docusafe.getAdditionProperty("cas.server.name");
                serverName = sName != null && sName.length() > 0 ? sName : Docusafe.getBaseContextUrl();
                casServerLoginUrl = CasUtils.getCasServerLoginUrl();
                initProxyCallBackUrl();
                initAcceptAnyProxy();
                casServerUrlPrefix = CasUtils.getCasServerUrlPrefix();
                System.out.printf("init complete casEnable=%s casServLoginURl=%s proxyCallback=%s proxyReceptor=%s acceptAny=%s", casEnabled, casServerLoginUrl, proxyCallBackUrl,
                proxyReceptorUrl, acceptAnyProxy);
            }
            initiatedFromHomeProperties = true;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String initProxyCallBackUrl(){
        try
        {
            if(StringUtils.isEmpty(proxyCallBackUrl)){
                proxyCallBackUrl = Docusafe.getBaseUrl() + proxyReceptorUrl;
            }
        }catch(Exception e){
            System.out.println("B��d ustawiania proxyCallBackUrl w filtrze");
        }

        return proxyCallBackUrl;
    }

    public boolean initAcceptAnyProxy(){
        try
        {
            if(!acceptAnyProxy){
                acceptAnyProxy = AdditionManager.getBooleanPropertyOrDefault("cas.acceptAnyProxy", false);
            }
        }catch(Exception e){
            System.out.println("B��d ustawiania proxyCallBackUrl w filtrze");
        }

        return acceptAnyProxy;
    }



    public final void setProxyReceptorUrl(final String proxyReceptorUrl) {
        this.proxyReceptorUrl = proxyReceptorUrl;
    }

    public void setProxyGrantingTicketStorage(final ProxyGrantingTicketStorage storage) {
        this.proxyGrantingTicketStorage = storage;
    }

    public void setTimer(final Timer timer) {
        this.timer = timer;
    }

    public void setTimerTask(final TimerTask timerTask) {
        this.timerTask = timerTask;
    }

    public void setMillisBetweenCleanUps(final int millisBetweenCleanUps) {
        this.millisBetweenCleanUps = millisBetweenCleanUps;
    }
}
