package pl.compan.docusafe.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.jasig.cas.client.util.AssertionThreadLocalFilter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalConstants;

/**
 * klasa wczytuj�ca ustawienia z HOME i w zale�no�ci od tego, czy CAS jest w��czony, uruchamia odpowiedni filtr lub go
 * zupe�nie pomija
 * 
 * @author bkaliszewski
 */
public class CasAssertionThreadLocalFilter extends AssertionThreadLocalFilter
{
	// nalezy wykonac init OnDemand, jednak nie moze on by� na r�wni z init(), poniewa� nie jest wtedy wczytany jeszcze
	// HOME
	private boolean initiatedFromHomeProperties= false;

	// zmienna wczytywana z HOME. Je�li false, to filtr w og�le nie jest w��czany
	private boolean casEnabled;

	public CasAssertionThreadLocalFilter()
	{
	}

	@Override
	public final void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (!initiatedFromHomeProperties)
			initFromHomeProperties();

		boolean isBackDoor = Boolean.valueOf(((HttpServletRequest) servletRequest).getHeader("X-DOCUSAFE-CAS-BACKDOOR"));
		
		if (casEnabled && !isBackDoor) {
			String uri= ((javax.servlet.http.HttpServletRequest) servletRequest).getRequestURI();
			if (uri.startsWith("/docusafe"))
				uri= uri.substring(("/docusafe").length()); // drobny fix

			if (!CasUtils.isNotCasable(uri)) {
				super.doFilter(servletRequest, servletResponse, filterChain);
				return;
			}
		}
		
		filterChain.doFilter(servletRequest, servletResponse);
	}

	private void initFromHomeProperties()
	{
		casEnabled= Boolean.parseBoolean(Docusafe.getAdditionProperty("cas.enabled"));
		initiatedFromHomeProperties= true;
	}
}
