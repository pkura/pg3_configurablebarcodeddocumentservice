package pl.compan.docusafe.web.filter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalConstants;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.web.common.Sitemap;

import javax.security.auth.Subject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
/* User: Administrator, Date: 2005-09-09 13:07:22 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrepareRequestFilter.java,v 1.6 2008/10/06 11:01:13 pecet4 Exp $
 */
public class PrepareRequestFilter extends DocusafeHttpFilter
{
    private static int filterLatency=-1;
    
    private static void loadFilterLatency()
    {

        String str = Docusafe.getAdditionProperty(Configuration.FILTER_LATENCY);
        filterLatency = 0;
        try
        {
            if(str!=null)
            {
                filterLatency = Integer.parseInt(str);
            }
        }
        catch(NumberFormatException nfe){}
        
    }
    
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
        String uri = (String) request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI);
        
        if(filterLatency<0)
            loadFilterLatency();
        else if(filterLatency>0)
        {
            double d = Math.random();
            if(d>0.5d)
            {
                //polowa ��da� b�dzie "przeprocesowana" - o szczegoly pytac Wojtka
                d = Math.random() * filterLatency;
                try
                {
                    Thread.sleep(1000*(long)d);
                }
                catch(InterruptedException ie){}
            }
        }
        
        
        // Sitemap odpowiada za wy�wietlanie menu; powinno dosta�
        // adres wywo�ania, aby pod�wietla� w�a�ciw� opcj�
        Sitemap.bind(uri, request.getQueryString());

        // aktualizacja uprawnie� zalogowanego u�ytkownika
        Subject subject = AuthUtil.getSubject(request);
        if (subject != null)
        {
            try
            {
                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                Principal userPrincipal = AuthUtil.getUserPrincipal(subject);
                // odwo�uj� si� bezpo�rednio do us�ugi, bo odwo�anie przez
                // DSContext wymaga utworzenia DSContext, czego chc� unikn��,
                // je�eli nie jest naprawd� potrzebne
                if (cache.updateNeeded(userPrincipal.getName()))
                {
                    try
                    {
                        DSApi.open(subject);

                        cache.update(userPrincipal.getName());
                    }
                    catch (EdmException e)
                    {
                        // ten b��d raczej ju� nie wyst�pi, bo filtr zadzia�a dopiero
                        // po pe�nym uruchomieniu aplikacji
                        throw new ServletException("B��d aktualizacji uprawnie�.  Je�eli aplikacja " +
                            "nie zosta�a jeszcze skonfigurowana, mo�liwe, �e Tomcat korzysta " +
                            "z zserializowanej sesji.", e);
                    }
                    finally
                    {
                        DSApi._close();
                    }
                }
            }
            catch (ServiceException e)
            {
                throw new ServletException(e.getMessage(), e);
            }
        }

        filterChain.doFilter(request, response);
    }

    public int getRequiredRunlevel()
    {
        return Docusafe.RL_RUNNING;
    }
}
