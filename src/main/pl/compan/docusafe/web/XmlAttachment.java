package pl.compan.docusafe.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.jfree.util.Log;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.nfos.XSLManager;
import pl.compan.docusafe.parametrization.nfos.XSLWzory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import com.google.common.base.Preconditions;

/**
 * Klasa wy�wietlaj�ca xmle
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class XmlAttachment extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(XmlAttachment.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("servlet path = " + req.getRequestURL());
        StringBuffer url = req.getRequestURL();
        long id = Long.parseLong(url.substring(url.lastIndexOf("/")+1));
        LOG.info("id = " + id);
        try{
            DSApi.open(AuthUtil.getSubject(req));
            resp.setContentType("text/html");
            provideXml(id, resp);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        } finally {
            DSApi._close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String xml = TextUtils.isoToUtf(req.getParameter("xml"));
        LOG.info("xml-attachment do post \nxml = {}", xml);
        try {
            transformXml(resp,
                    IOUtils.toInputStream(xml,"UTF-8"),
                    getStyleSheet(
                            XMLInputFactory.newInstance().createXMLStreamReader(
                                    IOUtils.toInputStream(xml,"UTF-8"))));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    private void provideXml(long id, HttpServletResponse resp) throws Exception{
        AttachmentRevision ar = AttachmentRevision.find(id);
        InputStream as = null;
        try {
            as = ar.getAttachmentStream();
            XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(as);
            String xmlStyleSheet;
            xmlStyleSheet = getStyleSheet(reader);
            if (AvailabilityManager.isAvailable("setXmlEncodingFromXmlOnViewSerwer")
            		&& !resp.getCharacterEncoding().equals(reader.getEncoding()))
            	resp.setCharacterEncoding(reader.getEncoding());
            reader.close();
            IOUtils.closeQuietly(as);
            as = ar.getAttachmentStream();
            transformXml(resp, as, xmlStyleSheet);
        } finally {
            IOUtils.closeQuietly(as);
        }
    }

    private final static X509TrustManager NULL_TRUST_MANAGER = new X509TrustManager(){
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };

    /**
     * 
     * @param resp
     * @param as - za��cznik
     * @param xmlStyleSheet - link do transformaty xsl
     * @throws TransformerException
     * @throws IOException
     */
    private void transformXml(HttpServletResponse resp, InputStream as, String xmlStyleSheet) throws TransformerException, IOException {
        Preconditions.checkNotNull(xmlStyleSheet);
        LOG.info("style sheet = {}", xmlStyleSheet);
        TransformerFactory tFactory = TransformerFactory.newInstance();

        URLConnection urlCon;
        try {
            URL url = new URL(xmlStyleSheet);
            urlCon = url.openConnection();
            if(AvailabilityManager.isAvailable("ssl.acceptNotTrusted.xml.stylesheet")){
                if(urlCon instanceof HttpsURLConnection){
                    LOG.info("HttpsURLConnection");
                    SSLContext context = SSLContext.getInstance("SSL");
                    context.init(null, new X509TrustManager[]{NULL_TRUST_MANAGER}, null);
                    ((HttpsURLConnection) urlCon).setSSLSocketFactory(context.getSocketFactory());
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return;
        }
        
        InputStream inputStream = null;
        boolean netStatus = true;
        try {			
        	inputStream = urlCon.getInputStream();
        } catch (Exception e) {
			Log.debug("Problem z hostem lub po��czeniem internetowym");
			netStatus = false;
		}
        
        Transformer transformer = null;
        XSLManager xslManager = XSLManager.getInstance();
		                
        /*
         * Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource());
         */
		if (netStatus) {
			try {
				URL url = new URL(xmlStyleSheet);
				urlCon = url.openConnection();
				InputStream is = urlCon.getInputStream();
				transformer = tFactory.newTransformer(new StreamSource(is));
				is.close();
				
				transformer.transform(
						new javax.xml.transform.stream.StreamSource(as),
						new javax.xml.transform.stream.StreamResult(resp.getOutputStream()));

				zapiszTransformateDoPlikuIDB(xmlStyleSheet, xslManager, url);
				
			} catch (Exception e) {
				LOG.error("Problem z hostem lub po��czeniem internetowym", e);
			}
		} else {
			// nie ma internetu
			try {
				DSApi.openAdmin();
				if (AvailabilityManager.isAvailable("transformata.offline")
						&& !netStatus) {
					Source source = poberzTransformate(xmlStyleSheet, xslManager);
					transformer = tFactory.newTransformer(source);
					transformer.transform(
							new javax.xml.transform.stream.StreamSource(as),
							new javax.xml.transform.stream.StreamResult(resp.getOutputStream()));
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			} finally {
				DSApi._close();
			}
		}
    }
    
	private Source poberzTransformate(String xmlStyleSheet,
			XSLManager xslManager) {
		Source source = null;
		File xslFile = null;
		XSLWzory xslWzor = XSLManager.getByUri(xmlStyleSheet);
		if (xslWzor != null) {
			String path = xslManager.getFullPathName() + "\\";
			xslFile = new File(path + "style_" + xslWzor.getId() + ".xsl");
			source = new StreamSource(xslFile);
			StringReader strReader = new StringReader(
					xslWzor.getXslFile());
			source = new StreamSource(strReader);
		}
		return source;
	}

	/**
	 * Tabela w bazie: DS_XSL_WZORY
	 * 
	 * @param xmlStyleSheet 
	 * @param xslManager
	 * @param url
	 * @throws EdmException
	 * @throws IOException
	 */
	private void zapiszTransformateDoPlikuIDB(String xmlStyleSheet,
			XSLManager xslManager, URL url) throws EdmException, IOException {
		try {
			// Zapisywanie wzoru transformaty do pliku i DB
			//FIXME
			if (!DSApi.isContextOpen())
				DSApi.openAdmin();
			DSApi.context().begin();
			XSLWzory xslwzory = XSLManager.getByUri(xmlStyleSheet);
			if (xslwzory == null) {
				xslwzory = new XSLWzory();
			}
			xslwzory.setXslUri(xmlStyleSheet);

			String xslFile = IOUtils.toString(url.openStream(), "utf-8");
			//dodanie tytu�u strony o ograniczony zaufaniu do transformaty
			xslFile = modifyXslFile(xslFile);
			xslwzory.setXslFile(xslFile);

			DSApi.context().session().saveOrUpdate(xslwzory);

			String path = xslManager.getFullPathName() + "\\";
			xslManager.SaveFile(path + "style_" + xslwzory.getId() + ".xsl",
					url.openStream());

			DSApi.context().commit();
			DSApi._close();
		} catch (HibernateException e) {
			DSApi.context()._rollback();
			LOG.error("", e);
		}
	}
    
	private String getStyleSheet(XMLStreamReader reader) throws XMLStreamException {
        String xmlStyleSheet = null;
        while(reader.hasNext()){
            int type = reader.next();
            if(type == XMLStreamReader.PROCESSING_INSTRUCTION
                    && reader.getPITarget().equals("xml-stylesheet")){
                String[] data = reader.getPIData().split("href\\s*=\\s*\"");
                if(data.length != 2){
                    throw new IllegalArgumentException("B��dna instrukcja xml-stylesheet");
                }
                xmlStyleSheet = data[1].substring(0,data[1].indexOf('"'));
                break;
            }
        }
        return xmlStyleSheet;
    }
	
	private String modifyXslFile(String xslFile){
		int li = xslFile.lastIndexOf("<head>")+6;
		String pre = xslFile.substring(0, li);
		String suf = xslFile.substring(li,xslFile.length());
		
		pre = pre.concat("<title>PRACA W TRYBIE OFFLINE!!! - transformata XSL mo�e by� nieaktualna.</title>");
		xslFile = pre.concat(suf);
		return xslFile;
	}
}
