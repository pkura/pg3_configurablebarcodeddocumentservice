package pl.compan.docusafe.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.OgnlUtil;
import pl.compan.docusafe.webwork.event.*;

import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.*;

/* User: Administrator, Date: 2005-10-04 11:30:55 */

/**
 * Pobiera list� zmian z pliku HOME/changelog.<numer wersji>.xml
 * i wy�wietla j� u�ytkownikowi.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ChangelogAction.java,v 1.9 2008/10/06 09:34:26 pecet4 Exp $
 */
public class ChangelogAction extends EventActionSupport
{
    private static final Log log = LogFactory.getLog(Changelog.class);
    private List<Changelog> entries;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Integer version = Docusafe.getDistNumber();

            entries = new ArrayList<Changelog>();

            for (int i=version; i >= version-15; i--)
            {            	
              /*  File clog = new File(Docusafe.getHome(),
                    "changelog/changelog."+i+".xml");*/

                InputStream input = Docusafe.class.getClassLoader().getResourceAsStream("changelog/changelog." + i + Docusafe.getCurrentLanguage() + ".xml");
                //if (!clog.exists())
                if (input == null)
                    continue;

                final Changelog changelog = new Changelog(i);

                DefaultHandler handler = new DefaultHandler()
                {
                    private boolean inTitle;
                    private boolean inDescription;
                    private String title;
                    private String description;
                    private boolean ignore;
                    
                    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
                    {                    	
                        if ("entry".equals(qName))
                        {
                            try
                            {
                                if (attributes.getValue("dockinds") != null)
                                    if (!OgnlUtil.checkOgnlValue(attributes.getValue("dockinds"), DocumentKind.listCns()))
                                        ignore = true;     
                                if (attributes.getValue("modules") != null)
                                    if (!OgnlUtil.checkOgnlValue(attributes.getValue("modules"), Docusafe.getAvailableModules()))
                                        ignore = true; 
                                if (attributes.getValue("extras") != null)
                                    if (!OgnlUtil.checkOgnlValue(attributes.getValue("extras"), Docusafe.getAvailableExtras()))
                                        ignore = true; 
                            }
                            catch (EdmException e)
                            {
                                throw new SAXException(e);
                            }
                        }
                        if ("title".equals(qName))
                            inTitle = true;
                        if ("description".equals(qName))
                            inDescription = true;
                    }

                    public void endElement(String uri, String localName, String qName) throws SAXException
                    {                    	
                        if ("entry".equals(qName))
                        {                        	
                            if (!ignore && title != null && title.length() > 0)
                            {
                                changelog.addChange(title, description);
                            }
                            title = null;
                            description = null;
                            ignore = false;
                        }
                        if ("title".equals(qName))
                        {
                            inTitle = false;
                            title = title.trim();
                        }
                        if ("description".equals(qName))
                        {
                            inDescription = false;
                            description = description.trim();
                        }
                    }

                    public void characters(char ch[], int start, int length) throws SAXException
                    {
                        if (inTitle)
                        {
                            if (title != null)
                                title += new String(ch, start, length);
                            else
                                title = new String(ch, start, length);
                        }
                        if (inDescription)
                        {    
                            if (description != null)
                                description += new String(ch, start, length);
                            else
                                description = new String(ch, start, length);                            
                        }
                    }
                };

                try
                {
                    SAXParserFactory.newInstance().newSAXParser().
                        parse(new InputSource(input/*clog.getAbsolutePath()*/), handler);
                }
                catch (Exception e)
                {
                    //addActionError("B��d ");
                    event.getLog().error(e.getMessage(), e);
                }

                entries.add(changelog);
            }

        }
    }        

    public class Changelog
    {
        private Integer version;
        private Map<String,String> changes;

        public Changelog(Integer version)
        {
            this.version = version;
            changes = new LinkedHashMap<String,String>();
        }

        public Integer getVersion()
        {
            return version;
        }

        public void setVersion(Integer version)
        {
            this.version = version;
        }

        public Map<String, String> getChanges()
        {
            return changes;
        }

        public void addChange(String title, String description)
        {
            changes.put(title, description);
        }
    }

    public List<Changelog> getEntries()
    {
        return entries;
    }
}
