package pl.compan.docusafe.web.viewserver;

import com.asprise.util.tiff.TIFFReader;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lowagie.text.DocumentException;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.axis.components.logger.LogFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceUtils;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceManager;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.naming.StringResource;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.nationwide.LayeredPdf;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.parametrization.ic.SadLogic;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.stagingarea.RetainedObjectAttachment;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TiffyAppletService;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.commons.BoxAction;
import pl.compan.docusafe.web.commons.OfficeDocumentAction;
import pl.compan.docusafe.web.commons.OfficeDocumentHelper;
import pl.compan.docusafe.web.commons.RemarkableAction;
import pl.compan.docusafe.web.office.common.DwrDocument;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.RemarksTabAction.RemarkBean;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import javax.servlet.http.HttpServletRequest;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.List;

import static java.lang.String.format;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

/**
 * Klasa posiadaj�ca g��wne okno viewservera.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewerAction.java,v 1.153 2010/07/29 13:28:31 pecet1 Exp $
 */
public class ViewerAction extends EventActionSupport implements OfficeDocumentAction, RemarkableAction, BoxAction, DwrDocument
{
	static final Logger log = LoggerFactory.getLogger(ViewerAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(ViewerAction.class.getPackage().getName(),null);
    /**
     * domy�lna szeroko�� kolumny z danymi (archiwizacj�, uwagami, itp)
     */
    public static int DEFAULT_DATA_SIZE = new Integer(Docusafe.getAdditionProperty("viewserverDataSize"));
    
    private Long id;
    private int width = 640;
    private int height = 480;
    private int page;
    private boolean fax = false; 
    private String barCode = "";

	//do wyrzucenia gdy bedzie klasa barCod
	private int barCodeX = 0;
	private int barCodeY = 0;
	private int barCodePage = 0;

	// @EXPORT
    /** url do za��cznika, kt�ry mo�na wy�wietli� w przegl�darce */
    private String src;
 //   private String pdfsrc;
    private int contentWidth;
    private int contentHeight;
    private int pageCount = 1;
    private Pager pager;
    private boolean viewserverWithData = false;
    private Integer viewserverDataSize;
    private boolean viewserverWholePage = false;
    private boolean viewserverDataSide = true;
    private FieldsManager fm;
    private boolean canReadDictionaries;
    private boolean needsNotBox;    
    private String boxNumber;
    private Long currentBoxId;
    private String currentBoxNumber;   
    /** czy dokument jest zablokowany podpisem */
    private boolean blocked;
    private boolean boxNumberReadonly;
    private boolean canUpdate;
    private boolean canReadAttachments;
    private List<RemarkBean> remarks;

    /** IC - pokazywanie zbudowanego konta kosztowego */
    private boolean showFullAccount;

	private Integer officeNumber;
    private List<Flags.Flag> globalFlags;
    private List<Flags.Flag> userFlags;
    private boolean flagsPresent;
    // tablice s� d�u�sze od MAX_FLAGS o 1, poniewa� flagi indeksowane
    // s� od 1 i te indeksy u�yte s� w tre�ci strony JSP
    private Long[] globalFlag;
    private Long[] userFlag;
    private boolean enableCollectiveAssignments = false;
    private FinishOrAssignState finishOrAssignState;
    private Map<String,String> substituted;
    private Map<String, String> assignmentsMap;
    private Map<String, String> wfProcesses;
    private List<Adnotation> adnotations;
    private String tiffyID;
    private boolean tiffyApplet;
    private String annotations;

    private Document document;
    /**
     * je�li true to znaczy, �� zadekretowali�my dokument i zamkniemy okno viewservera, prze��dowuj�c
     * g��wn� stron� na list� zada�
     */
    private boolean documentReassigned;
    private AttachmentRevision revision;
	/**
	 * Uproszczona przyspieszona dekretacja
	 */
	private boolean fastAssignment;
    /**
     * akcja, na kt�r� prze�adowa� g��wne okno po wykonaniu dekretacji
     */
    private String reloadLink;
    private Boolean isReloadOpener = false;
    
    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();
    private String content;
    /**
     * Lista zbiorczej dekretacji; ka�dy element tablicy ma posta�
     * nazwa_u�ytkownika "," CollectiveAssignmentEntry.id
     */
    private String[] assignments;
    private boolean doChangePage;
    private String msg;
	private Boolean preventRefresh;
    
    // @IMPORT/@EXPORT
    private Long boxId;
    private Long documentId;
    private String activity;
    private boolean next;
    private boolean showSendToEva;
    private boolean showReject;
    private boolean externalWorkflow;
    private String evaMessage;
    private Boolean acceptancesPage;
    private String documentKindCn;
    private Integer docType;
    
    //zmienne przechowujace aktualna pozycje obrazka
    private Integer rotate;
    private Integer imgWidth;
    private Integer imgHeight;
    
    private Long centrumToAcceptanceId;
    private Map<Long,Map<String,String>> userToNextAcceptance;
    private Map<String,String> acceptancesUserList;
	private Map<String, String> acceptancesCnList;
	private String discardUserAcceptance;
	private String discardCnAcceptance;
    private Boolean acceptanceToUser;
    private Boolean generalAcceptances;
    private Map<String,String> userToGeneralAcceptance;
    private Boolean finishOn;

    private Map<String,String> userToAcceptance;
    private String userAcceptance;
    private boolean jbpmAcceptances;
   
	private List<Aspect> documentAspects;
	private String documentAspectCn;
    
    private static final String EV_FILL = "fill";
    private static final String EV_ARCH = "archive";

	private CentrumKosztow centrum;
	private LinkedList<RenderBean> processRenderBeans;

	private boolean manualFinishPossible;
	private boolean finishAndGoNextPossible;

	private String processName;
	private String processId;
	private String processAction;
	
	private Long binderId;
	
	private String jcrDownloadLink;
	
	private Boolean editAsWebOdf = false;
	
	/**
	 * rozszerzenia plik�w kt�re moga byc otwierane w web odf
	 */
	private static List<String> mimeOdfList = Lists.newArrayList("application/vnd.oasis.opendocument.text",
	                		  "application/vnd.oasis.opendocument.spreadsheet", 
	                		  "application/vnd.oasis.opendocument.presentation");
	//INTERCARS
	private Map<String, String> acceptanceNames;
	private Set<String> cnsNeeded;
	private boolean acceptanceButtonsForCurrentUserVisible = true;
	
	protected static OfficeDocumentHelper officeDocumentHelper = new OfficeDocumentHelper();
	
	private ViewerAction thisAction = this;
	
	/** 
	 * <p>Parametry wyswietlanego obrazka na stronie.<br />
	 * Format x1,y1,z1,x2,y2,z2..  </p>gdzie:
	 * <li><b>x</b> rozmiar (1 - domyslny rozmiar, 0 - zmiesc na ekranie)
	 * <li><b>y</b> stopien obrotu (co 45 st)
	 * <li><b>z</b> ujemne - stopien pomniejszenia, dodatnie - stopien powiekszenia
	 */
	private String pagesParam;
	private boolean pudloModify;


    private Long objectId;
    private String acceptanceCn;
    private boolean rejectObjectAcceptanceAvailable;

    // dwr
    private Map<String,String> documentKinds;
    private Map<String,Object> dockindKeys;
    private static final DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();

    /**
     * <p>
     *     Opcja powi�zana z parametrem available <code>viewserver.rich-document-as-pdf</code>.
     * </p>
     *
     * <p>Zmienna jest inicjalizowana w <code>setup()</code> jako: <code>viewserver.rich-document-as-pdf && !pdf.onviewer</code>.</p>
     *
     * <p>
     * Parametr <code>viewserver.rich-document-as-pdf</code>
     * uruchamia mo�liwo�� podgl�du za��cznik�w rtf, doc, docx, itd. jako pdf.
     *
     * <p>
     *     Aby funkcjonalno�� zadzia�a�a musi by� uruchomiony OpenOffice w service mode.
     *     OpenOffice konwertuje pliki. Service mode uruchamiamy poprzez komend�
     *     <code>soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard</code>
     * </p>
     *
     * Funkcjonalno�� jest uzale�niona od parametru available <code>pdf.onviewer</code>
     * w nast�puj�cy spos�b:
     * <ul>
     *     <li>
     *         W przypadku podgl�du za��cznik�w viewerem (klikni�cie w lupk�)
     *         opcja dzia�a tylko przy ustawieniu <code>pdf.onviewer=false</code>,
     *         tj. streamujemy wygenerowany pdf bezpo�rednio do przegl�darki
     *         (de facto pomijaj�c viewera).
     *     </li>
     *     <li>
     *         W przypadku pobierania za��cznika z widoku dokumentu (<code>asPdf</code>, czyli klikni�cie
     *         w ikonk� pdf) opcja dzia�a niezale�nie od <code>pdf.onviewer</code>.
     *     </li>
     * </ul>
     */
    private boolean richTextAsPdf = false;

    protected void setup()
    {
        boolean richDocumentAsPdf = AvailabilityManager.isAvailable("viewserver.rich-document-as-pdf");
        boolean pdfOnViewer = AvailabilityManager.isAvailable("pdf.onviewer");

        richTextAsPdf = richDocumentAsPdf && !pdfOnViewer;

        FillForm fillForm = new FillForm();
		Archive archive = new Archive();
		
        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);        

		registerListener("doProcessAction").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(EV_ARCH,archive).
			append(new ProcessAction()).
            append(EV_FILL,fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doArchive").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(archive).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
    
        registerListener("doUpdateBox").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new UpdateBox()).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doAssignments").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(archive).
            append(new Assignments()).
            append(EV_FILL, new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("sendToEva").
	        append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	        append(new SendToEva()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doReject").
	       append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	       append(new Reject()).
	       append(EV_FILL, new FillForm()).
	       appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		//pe�na akceptacja intercarsa
		registerListener("doFullAcceptance").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
         	append(new Archive()).
         	append(new FullAcceptance()).
			append(EV_FILL, new FillForm()).
         	appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doManualFinish").
	        append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	        append(new ManualFinish()).
			append(EV_FILL, new FillForm()).
	        appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doDiscard").
	   		append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	   		append(new Discard()).
	   		append(new ManualFinish()).
	   		append(EV_FILL,new FillForm()).
	        appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

	   registerListener("doDiscardTo").
	  		append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	  		append(new DiscardTo()).
	  		append(EV_FILL,new FillForm()).
	  		appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);



	   registerListener("doDiscardToIM").
	  		append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	  		append(new DiscardToIM()).
	  		append(EV_FILL,new FillForm()).
	  		appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

	   registerListener("doNextDecretation"). 
	    	append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	    	append(archive).
	    	append(new nextDecretation()).
	    	append(EV_FILL,new FillForm()).
	    	appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doDiscardObjectAcceptance")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(new Archive())
                .append(new DiscardObjectAcceptance())
                .append(EV_FILL, new FillForm())
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

	   registerListener("doTmp").
	       append(OPEN_HIBERNATE_AND_JBPM_SESSION).
	       append(new FillForm()).
	       appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

	   registerListener("doSaveAnnotation").
	       append(OpenHibernateSession.INSTANCE).
	       append(new SaveAnnotation()).
	       append(new FillForm()).
	       appendFinally(CloseHibernateSession.INSTANCE);

	   registerListener("doWithdraw")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
//				.append(new Archive())
				.append(new WithdrawDocument())
				.append(EV_FILL, new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUpdateDwr")
                .append(OpenHibernateSession.INSTANCE)
                .append(new ValidateCreate())
                .append(new UpdateDwr())
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);
	   
        
      /*  registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new AddRemark()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);*/
    }
    public static String getDecompressedPath()
    {
    	 String decompressedPath = Docusafe.getAdditionProperty("addition.pathToDecompressedTiffs");
         if(decompressedPath.equals(".")){
         	decompressedPath=Docusafe.getHome()+"/viewserver";
         }
         
         return decompressedPath;
    }

	//------------
    private class Coordinates {  // #todo do wywalenia po zmianach
    	private int x;
    	private int y;
		private int pageNr;
    	private String barCodeString;
    	
    	public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public int getPageNr() {
			return pageNr;
		}

		public void setPageNr(int pageNr) {
			this.pageNr = pageNr;
		}

		public String getBarCodeString() {
			return barCodeString;
		}

		public void setBarCodeString(String barCode) {
			this.barCodeString = barCode;
		}
    	
    	public Coordinates(int x, int y, int pageNr, String barCode) {
    		this.x = x;
    		this.y = y;
    		this.pageNr = pageNr;
    		this.barCodeString = barCode;
    	}
    	
    	public Coordinates() {
    		this.x =0;
    		this.y =0;
    		this.pageNr = 0;
    		this.barCodeString = "";
    	}
    }

	

	private class FullAcceptance extends TransactionalActionListener{
        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(hasActionErrors()){
                throw new EdmException("Wyst�pi�y b��dy - akceptacja nie mo�e zosta� wykonana");
            }
        }

        @Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			if(userAcceptance == null){
			    AcceptanceUtils.fullAccept(getDocumentId(), getActivity());
            } else {
                AcceptanceUtils.fullAcceptWithNextUser(getDocumentId(), getActivity(), userAcceptance);
            }
		}

        @Override
        public void handleException(Exception e, ActionEvent event) {
            try {
                throw e;
            } catch (IntercarsAcceptanceManager.SafoExportException ex) {
                event.setResult("error");
                addActionError(ex.getMessage());
                try {
                    DSApi.context().rollback();
                    CLOSE_HIBERNATE_AND_JBPM_SESSION.actionPerformed(event);
                    LOG.error("Zapisywanie komunikatu '" + e.getMessage() +"'");
                    OPEN_HIBERNATE_AND_JBPM_SESSION.actionPerformed(event);
                    DSApi.context().begin();
                    DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).setOnly(getDocumentId(),
                            Collections.singletonMap(InvoiceICLogic.SAFO_LAST_MESSAGE_FIELD_CN, e.getMessage()));
                    DSApi.context().commit();
                } catch (Exception exc) {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                    LOG.error(exc);
                }
            } catch (Exception ex) {
                super.handleException(ex, event);
            }
        }

        @Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
                openNextTask(DocumentType.INCOMING , false);
            }			

		@Override
		public Logger getLogger() {
			return log;
		}
	}

	private class WithdrawDocument extends TransactionalActionListener{

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			WorkflowFactory.manualFinish(getDocumentId(), false);
            Map<String, Object> toSet = Maps.newHashMap();
            toSet.put(InvoiceICLogic.STATUS_FIELD_CN, 666);
            toSet.put(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN, 666);
            getDocument().getDocumentKind().setOnly(getDocumentId(),toSet);
		}

		@Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			DSApi.context().session().flush();
			documentReassigned = true;
            reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
			event.setResult("task-list");
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

	private class ProcessAction extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			OfficeDocument doc = OfficeDocument.find(getDocumentId());
			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName);
			ProcessInstance pi = pdef.getLocator().lookupForId(processId);
			DSApi.context().begin();
			pdef.getLogic().process(pi,
				ProcessActionContext.action(processAction)
									.document(doc)
									.activityId(getActivity()));
			DSApi.context().commit();
			openNextTask(DocumentType.INCOMING , false);
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	OfficeDocument officeDoc = null;

			fastAssignment = AvailabilityManager.isAvailable("archiwizacja.szybkaDekretacja");

            if (!fax) {
                // Uwaga: z tej metody jest streamowany output do przegl�darki
                officeDoc = standardAttachment(event, officeDoc);
            } else {
                retainedObjectAttachment(event);
            }

            if(document != null){
                initializeDocumentKind(document);
            }

            boolean dwr = AvailabilityManager.isAvailable("dwr.viewserver");


            if(officeDoc != null) {
                try {
                    processRenderBeans = new LinkedList<RenderBean>();
					log.info("Initializing process definitions");
					
					if (officeDoc.getDocumentKind() != null)
						for(ProcessDefinition pd: officeDoc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions()){
							log.info("Process definition:" + pd.getName());
							getProcessRenderBeans().addAll(pd.getView().render(pd.getLocator().lookupForDocument(officeDoc)));
						}

	            	canUpdate = officeDoc.canModify();

					if(activity == null){
						archiveProcessingCheck(officeDoc);
					}

                    if(!WorkflowFactory.jbpm){
                        WorkflowActivity activity =
                                (getActivity() != null)
                                    ? WorkflowFactory.getWfActivity(getActivity())
                                    : null;

                        externalWorkflow = WorkflowUtils.isExternalWorkflow(activity);
                        if(externalWorkflow)
                        {
                            finishOrAssignState = WorkflowUtils.getFinishOrAssignState(officeDoc, activity);
                        }
                    }
		            
		            showSendToEva = false;
		            showReject = false;
		            if (fm != null && officeDoc.getDocumentKind().getCn().equals(DocumentLogicLoader.ROCKWELL_KIND))
		            {
		            	showReject = true;
		            	Integer tmp = null;
						tmp = (Integer)fm.getKey(RockwellLogic.EVA_STATUS_FIELD_CN);
						
		            	if(tmp ==0)
		            	{
		            		evaMessage = null;
		            		showSendToEva = true;
		            	}
		            	else
		            	{
		            		evaMessage = "Document was sent to Eva";
		            	}
		            }
		            if (AvailabilityManager.isAvailable("viewer.acceptances") && fm != null) {
						acceptancesPage = true;
						documentKindCn = fm.getDocumentKind().getCn();

						if (documentKindCn.equals(DocumentLogicLoader.INVOICE_IC_KIND) || documentKindCn.equals(DocumentLogicLoader.SAD_KIND)) {
							prepareForIc();
						}
					}
		            
		            try 
		            {
						officeDoc.getDocumentKind().logic().checkCanFinishProcess(officeDoc);
						finishOn = true;
					} 
		            catch (Exception e) 
					{
						finishOn = false;
					}
		            
		            manualFinishPossible = (activity != null && (finishOn == Boolean.TRUE));
		            
        	        if ((hasActionErrors() || doChangePage) && fm != null)
        	        {
        	            fm.reloadValues(values);
        	        }

                    if(dwr) {
                        documentKindCn = officeDoc.getDocumentKind().getCn();
                        docType = DwrDocumentHelper.getDocumentTypeAsDocumentLogicType(officeDoc.getType());
                        dwrDocumentHelper.fillForm(ViewerAction.this);
                    }

	            } catch (EdmException e) {
					addActionError(e.getMessage());
					log.error(e.getMessage(), e);
				}
	            finally 
				{
					finishAndGoNextPossible = manualFinishPossible && finishAndGoNextPossible;
				}
            } 
            else
            {
                if (dwr) {
                    try {
                        documentKindCn = document.getDocumentKind().getCn();
                        Set<DocumentType> documentTypes = document.getDocumentKind().getDockindInfo().getDocumentTypes();
                        DocumentType documentType = findProperDocumentType(documentTypes);

                        docType = DwrDocumentHelper.getDocumentTypeAsDocumentLogicType(documentType);
                        dwrDocumentHelper.fillForm(ViewerAction.this);
                    } catch (EdmException e) {
                        addActionError(e.getMessage());
                        log.error(e.getMessage(), e);
                    }
                }
                manualFinishPossible = false;
				finishAndGoNextPossible = false;
			}

            // lista dockind�w
            if(dwr) {
                try {
                    List<DocumentKind> documentKindList = DocumentKind.listForEdit();
                    documentKinds = Maps.newLinkedHashMap();
                    for(DocumentKind dockind : documentKindList) {
                        documentKinds.put(dockind.getCn(), dockind.getName());
                    }
                } catch (EdmException e) {
                    addActionError(e.getLocalizedMessage());
                    log.error(e.getMessage(), e);
                }
            }
        }

        private OfficeDocument standardAttachment(ActionEvent event, OfficeDocument officeDoc) {
            viewserverWithData = DSApi.context().userPreferences().node("other").getBoolean("viewserverWithData", false);
            viewserverWholePage = DSApi.context().userPreferences().node("other").getBoolean("viewserverWholePage", true);
            viewserverDataSide = DSApi.context().userPreferences().node("other").getBoolean("viewserverDataSide", true);
            try
            {
                adnotations = Adnotation.list();
                if(id == null && documentId != null){

                    List<Attachment> attachments = null;
                    if (AvailabilityManager.isAvailable("viewerAction.disabled.CheckPermissions"))
                        attachments = Documents.document(documentId).getDocument(false).getAttachments();
                    else
                         attachments = Documents.document(documentId).getDocument().getAttachments();
                    if(attachments.size() == 1){
                        id = attachments.get(0).getMostRecentRevision().getId();
                    } else {
                        for(Attachment att : attachments) {
                            AttachmentRevision mostRecentRevision = att.getMostRecentRevision();
                            if(ViewServer.mimeAcceptable(mostRecentRevision.getMime())){
                                id = mostRecentRevision.getId();
                                break;
                            }
                        }
                    }
                }
                revision = AttachmentRevision.find(id);
                if(!hasActionErrors()){
                    DSApi.context().begin();
                    revision.verifyIntegrity();
                    DSApi.context().commit();
                }
                document = revision.getAttachment().getDocument();
                canUpdate = document.canModify();

                canReadAttachments = document.canReadAttachments();
                
                if(canReadAttachments){
	                if(document instanceof OfficeDocument) {
	                    officeDoc = (OfficeDocument)document;
	                }
	
	                if (revision.getMime().startsWith("image/") || 
	                		(revision.getMime().equals("application/pdf") && AvailabilityManager.isAvailable("pdf.onviewer")))
	                {
                        displayFromViewServer(event, revision);
	                }
	                else if (revision.getMime().equals("application/pdf") || (richTextAsPdf && !editAsWebOdf))
	                {
                        displayFromStream(event, revision);
	                }
	                else if (editAsWebOdf && mimeOdfList.contains(revision.getMime()))
	                {
                        displayFromStreamToEditOdf(event, revision);
	                }
	                else
	                {
	                    event.setResult("no-viewer");
	                }
                }
                if(AvailabilityManager.isAvailable("addDownloadAttachmentHistory"))
                {
                    DSApi.context().begin();

                    DataMartManager.addHistoryEntry(document.getId(), Audit.createWithPriority("downloadAttachment", DSApi.context().getPrincipalName()
                            , sm.getString("UzytkownikWyswietlilZalacznik"), "" + revision.getId(), Audit.LOWEST_PRIORITY));
                    DSApi.context().commit();
                }

                if(AvailabilityManager.isAvailable("ukw.ogolne.changeStatus.attachment"))
                {
                    Long docID = revision.getAttachment().getDocument().getId();
                    DSUser user = DSApi.context().getDSUser();
                    OfficeDocument document = (OfficeDocument) OfficeDocument.find(docID, false);
                    if ("ogolne".equalsIgnoreCase(document.getDocumentKind().getCn())) {
                        List<Long> mpkIDS = (List<Long>) document.getFieldsManager().getKey("MPK");
                        for (Long mpkID : mpkIDS) {
                            CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkID);
                            if (mpk != null && mpk.getCentrumId() == user.getId().intValue()) {
                                DSApi.context().begin();
                                mpk.setConnectedTypeId(3);
                                DSApi.context().commit();
                            }
                        }
                    }
                }

                src =  revision.getPageLinkForBinder(page, binderId);

                pager = new Pager(new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return "javascript:changePage("+offset+")";
                    }
                }, page, 1, pageCount, 10, true);

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                addActionError(e.getMessage());
            }
            return officeDoc;
        }

        /**
         * Streamuje bezpo�rednio do przegl�darki
         *
         * @param event
         * @param revision
         * @throws EdmException
         * @throws DocumentException
         */
        private void displayFromStream(ActionEvent event, AttachmentRevision revision) throws EdmException, DocumentException {
            try
            {
                List<InputStream> files = new ArrayList<InputStream>();
                files.add(revision.getAttachmentStream());

                File file = File.createTempFile("attachment_as_pdf", ".pdf");

                // normalne zachowanie (dostajemy plik pdf)
                if(revision.getMime().equals("application/pdf") || !richTextAsPdf) {
                    OutputStream os =  new FileOutputStream(file);
                    String layer = null;
                    if(AvailabilityManager.isAvailable("viewAttaAsPdf.addInformationAboutUser"))
                    {
                        layer = sm.getString("ZalacznikPobranyPrzeUzytkownikaDnia",
                                DSApi.context().getDSUser().asFirstnameLastname(), DateUtils.formatCommonDateTime(new Date()));
                    }
                    LayeredPdf.streamsAsPdf(files, null, null, false, os, null, null, null, layer);
                }
                // konwersja do pdf (dostajemy dowolny plik)
                else {
                    final File input = revision.getAsTempFile();
                    log.debug("revision.getAsTempFile = {}", input.getAbsolutePath());
                    PdfUtils.documentToPdf(input, file);
                    file.deleteOnExit();
                }

                FileInputStream stream = new FileInputStream(file);

                ServletUtils.streamResponse(ServletActionContext.getResponse(), stream,
                        "application/pdf", stream.available());
            }
            catch (IOException e)
            {
                event.getLog().error("", e);
            }
            event.setResult(NONE);
        }
        
        /**
         * Streamuje bezpo�rednio do przegl�darki .odt na potrzeby ich edycji
         *
         * @param event
         * @param revision
         * @throws EdmException
         * @throws DocumentException
         */
        private void displayFromStreamToEditOdf(ActionEvent event, AttachmentRevision revision) throws EdmException, DocumentException {

			event.setResult("odf-viewer"); 
			jcrDownloadLink = ((JackrabbitAttachmentRevision) revision).getDownloadLink();
			log.debug(jcrDownloadLink);
			

		}

        private void displayFromViewServer(ActionEvent event, AttachmentRevision revision) throws EdmException, IOException {
            event.setResult("image");
            String decompressedPath = getDecompressedPath();
            File imagePropertiesFile = new File(decompressedPath+"//"+
                    revision.getFileName()+".properties");

            Properties imageProps = new Properties();
            Point size = null;
            if(imagePropertiesFile.exists())
            {
                imageProps.load(new FileInputStream(imagePropertiesFile));
                size = new Point();
                size.x = Integer.parseInt(imageProps.getProperty("width"));
                size.y = Integer.parseInt(imageProps.getProperty("height"));
                pageCount = Integer.parseInt(imageProps.getProperty("pageCount"));
            }
            else
            {
                if (revision.getMime().startsWith("image/tiff"))
                {
                    try
                    {

                        pageCount = revision.getPageCount();
                        if (page >= pageCount)
                            page = pageCount-1;
                        RenderedImage imPage = revision.getPage(page);
                        size = new Point(imPage.getWidth(), imPage.getHeight());
                    }
                    catch(Exception e)
                    {
                        msg = sm.getString("BladOdczytuDanegoPlikuTIFFObrazFizycznieZleSformatowany");
                        addActionError(msg);
                    }
                }
                else if(revision.getMime().equals("application/pdf"))
                {
                    PDDocument document = PDDocument.load(revision.getBinaryStream());
                    List<PDPage> pDPages = document.getDocumentCatalog().getAllPages();
                    pageCount = pDPages.size();
                    PDPage pDPage = pDPages.get(page);
                        int rotation = pDPage.findRotation();
////	                                 Map<String, PDXObjectImage> images = pDPage.getResources().getImages();
//	                                 if (images.size() > 0) {
////	                                     for (String imageKey : images.keySet()) {
//	                                         BufferedImage image = pDPage.convertToImage(BufferedImage.TYPE_BYTE_GRAY, 300);;
//	                                         if (image != null && rotation != 0) {
//	                                             image = ImageUtils.rotateImage(image, rotation);
//	                                         }
//	                                         if(image != null){
//	                                        	 size = new Point(image.getWidth(), image.getHeight());
//	                                         }
////	                                   }
//	                                 } else {
                            BufferedImage image = pDPage.convertToImage(BufferedImage.TYPE_BYTE_GRAY, 300);
                            if (image != null && rotation != 0) {
                                image = ImageUtils.rotateImage(image, rotation);
                            }
                            if(image != null){
                                size = new Point(image.getWidth(), image.getHeight());
                            }
//	                                 }

                }
                else
                {
                    pageCount = 1;
                    size = ImageUtils.getImageSize(revision.getAttachmentStream(), revision.getMime());
                }
                //System.out.println(size);
                imageProps.setProperty("pageCount", Integer.toString(pageCount));
                imageProps.setProperty("width", Integer.toString(size.x));
                imageProps.setProperty("height", Integer.toString(size.y));
                File viewserverFolder =new File( imagePropertiesFile.getParent());
                viewserverFolder.mkdirs();
                FileOutputStream out = new FileOutputStream(imagePropertiesFile,true);
                imageProps.save(out,"");
                out.close();
            }
            contentWidth = size.x;
            contentHeight = size.y;
            imgWidth = size.x;
            imgHeight = size.y;
            tiffyApplet = "true".equals(Docusafe.getAdditionProperty("tiffyApplet")) &&
                                DSApi.context().userPreferences().node("other").getBoolean("viewserverTiffyApplet", true) &&
                                revision.getMime().startsWith("image/tiff");
            if(tiffyApplet)
            {
                HttpServletRequest r = ServletActionContext.getRequest();
                tiffyID = r.getRequestedSessionId()+ new Date().getTime();
                TiffyAppletService.addRevision(tiffyID, revision.getId());
                TiffyAppletService.addPageCount(tiffyID, pageCount);
            }
            if (viewserverWithData && revision.getAttachment().isDocumentType())
            {
                 viewserverDataSize = DSApi.context().userPreferences().node("other").getInt("viewserverDataSize", ViewerAction.DEFAULT_DATA_SIZE);
                 prepareData(revision.getAttachment().getDocument());
            }
        }

        private void retainedObjectAttachment(ActionEvent event) {
            try {
                RetainedObjectAttachment revision = RetainedObjectAttachment.find(id);
                if (revision == null) {
                    new EdmException("NieZnalezionoZalacznika");
                }
                if (revision.getMime().startsWith("image/")) {
                    event.setResult("image");

                    File file = ViewServer.getFile(revision);

                    Point size = null;

                    if (revision.getMime().startsWith("image/tiff")) {
                        try {
                            TIFFReader reader = new TIFFReader(file);
                            pageCount = reader.countPages();
                            if (page >= pageCount)
                                page = pageCount - 1;
                            RenderedImage imPage = reader.getPage(page);
                            size = new Point(imPage.getWidth(), imPage.getHeight());
                        } catch (Exception e) {
                            addActionError(sm.getString("BladOdczytuDanegoPlikuTIFFObrazFizycznieZleSformatowany"));
                        }
                    } else {
                        pageCount = 1;
                        size = ImageUtils.getImageSize(new FileInputStream(file), revision.getMime());
                    }

                    contentWidth = size.x;
                    contentHeight = size.y;
                } else if (revision.getMime().equals("application/pdf")) {
                    File temp = revision.saveToTempFile();

                    try {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                                "application/pdf", (int) temp.length());
                    } catch (IOException e) {
                        log.error("", e);
                    } finally {
                        temp.delete();
                    }

                    event.setResult(NONE);
                } else {
                    event.setResult("no-viewer");
                }

                src = "/viewserver/content.action?id=" + id +
                        "&width=" + contentWidth +
                        "&height=" + contentHeight +
                        "&page=" + page +
                        (binderId != null ? "&binderId=" + binderId : "") +
                        "&fax=true";

                pager = new Pager(new Pager.LinkVisitor() {
                    public String getLink(int offset) {
                        return "/viewserver/viewer.action?id=" + id +
                                "&page=" + offset +
                                "&width=" + width +
                                "&height=" + height +
                                "&fax=true";
                    }
                }, page, 1, pageCount, 10);
            } catch (Exception e) {
                if (!getActionErrors().contains(msg)) addActionError(e.getMessage());
                log.error(e.getLocalizedMessage(), e);
            }
        }

        private void initializeDocumentKind(Document document) {
            try {
                if(document.getDocumentKind() != null && document.getDocumentKind().getCn() != null) {
                    if (AvailabilityManager.isAvailable("viewerAction.disabled.CheckPermissions"))
                    {
                        fm = Documents.document(document.getId()).getDocument(false).getFieldsManager();
                    }
                    else
                    	fm = Documents.document(document.getId()).getFieldsManager();
                    fm.initialize();
                    fm.initializeAcceptances();
                    documentAspects = document.getDocumentKind().getAspectsForUser(DSApi.context().getPrincipalName());
                    if (documentAspects == null) {
                        documentAspectCn = null;
                    } else if (documentAspects != null && documentAspectCn == null) {
                        documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
                    }
                }
            } catch (EdmException ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

		private void archiveProcessingCheck(OfficeDocument officeDoc) throws EdmException {
			boolean process = false;
			
			log.debug("Procesowanie w archiwum: sprawdzanie");

//			if (AvailabilityManager.isAvailable("archiwum.procesowanie.grupa")) {
//				process = DSDivision.isInDivision(GroupPermissions.ARCHIVE_PROCESSING_GUID);
//			} else if (AvailabilityManager.isAvailable("archiwum.procesowanie")) {
//				process = true;
//			}

			process = GroupPermissions.canProcessThroughArchive();

			if (process) {
				log.debug("Procesowanie w archiwum");
				try {
					activity = WorkflowFactory.findManualTask(officeDoc.getId());
					finishAndGoNextPossible = false;
				} catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}
		}

		private void icProcessCentra(List<CentrumKosztowDlaFaktury> ckf, Boolean simpleAcceptance) throws EdmException {
            rejectObjectAcceptanceAvailable = getActivity() != null && DSApi.context().hasPermission(DSPermission.INVOICE_REJECT_OBJECT_ACCEPTANCE);
			for (CentrumKosztowDlaFaktury centrumDlaFaktury : ckf) {
				centrumDlaFaktury.initAcceptancesModes();
				if (centrumDlaFaktury.getAcceptanceMode() != null) {
					cnsNeeded.addAll(centrumDlaFaktury.getAcceptanceCnsNeeded());
				}
                if(showFullAccount){
                    centrumDlaFaktury.getFullAccountNumber();
                }
				Map<String, String> tmpUser = fm.getAcceptancesState().userToNextAcceptance(centrumDlaFaktury, simpleAcceptance);
				userToNextAcceptance.put(centrumDlaFaktury.getId(), tmpUser);
				//AcceptanceCondition accc = null;
				String nextAcc = fm.getAcceptancesState().getNextAcceptation(centrumDlaFaktury, simpleAcceptance);
				//if (nextAcc != null) {
				//	accc = AcceptanceCondition.find(nextAcc).get(0);
				//}
//				if (accc != null && accc.getFieldValue() != null) {
//					//generalAcceptances = false;
//				} else {
                if(nextAcc != null){
					userToGeneralAcceptance = tmpUser;
                } else {
                    continue;
                }
//				}
				if (centrum == null || !centrum.isIntercarsPowered()) {
					for (IntercarsAcceptanceMode ic : centrumDlaFaktury.getPossibleAcceptanceModes()) {
						String name = StringResource.get("acceptanceModeName." + centrumDlaFaktury.getCentrumId() + "." + ic.getId());
						if (name != null) {
							acceptanceNames.put(centrumDlaFaktury.getCentrumId() + "." + ic.getId(), name);
						} else {
							acceptanceNames.put(centrumDlaFaktury.getCentrumId() + "." + ic.getId(), ic.getSimpleName());
						}
					}
				} else {
					for (IntercarsAcceptanceMode ic : centrum.getAvailableAcceptanceModes()) {
						String name = StringResource.get("acceptanceModeName." + centrum.getId() + "." + ic.getId());
						if (name != null) {
							acceptanceNames.put(centrum.getId() + "." + ic.getId(), name);
						} else {
							acceptanceNames.put(centrum.getId() + "." + ic.getId(), ic.getSimpleName());
						}
					}
				}
			}
		}

		private void prepareForIc() throws EdmException {
			List<CentrumKosztowDlaFaktury> ckf = (List<CentrumKosztowDlaFaktury>) fm.getValue("CENTRUM_KOSZTOWE");
			userToNextAcceptance = new HashMap<Long, Map<String, String>>();
            showFullAccount = fm.getEnumItemCn(InvoiceICLogic.STATUS_FIELD_CN).equals("DYREKTORA_FIN"); //pokazuj pe�ne konto tylko ksi�gowym
			generalAcceptances = true;
			acceptanceNames = new HashMap<String, String>();
			cnsNeeded = new HashSet<String>();
			jbpmAcceptances = true;
			fastAssignment = InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(
					fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN));
			
			Integer dzial = (Integer) fm.getKey(InvoiceICLogic.DZIAL_FIELD_CN);
			if (dzial != null) {
				centrum = (CentrumKosztow.find(fm.getField(InvoiceICLogic.DZIAL_FIELD_CN).getEnumItem(dzial).getCentrum()));
			}
			icProcessCentra(ckf, false);
			acceptancesUserList = fm.getAcceptancesState().getAcceptancesUserList();
			setAcceptancesCnList(new LinkedHashMap<String, String>(IntercarsAcceptanceMode.getAllAcceptanceCodes().size() / 2));
			acceptanceButtonsForCurrentUserVisible =
				document.getDocumentKind().logic() instanceof SadLogic //oni widz� zawsze
				|| ((InvoiceICLogic)document.getDocumentKind().logic())
					.isAcceptanceButtonsForCurrentUserVisible(document,fm);
			for(DocumentAcceptance da: fm.getAcceptancesState().getFieldAcceptances()){
				getAcceptancesCnList().put(da.getAcceptanceCn(), da.getAcceptanceCn());
			}
			for(DocumentAcceptance da: fm.getAcceptancesState().getGeneralAcceptances()){
				getAcceptancesCnList().put(da.getAcceptanceCn(), da.getAcceptanceCn());
			}
		}
    }

    private DocumentType findProperDocumentType(Set<DocumentType> documentTypes) {
        return documentTypes.isEmpty() ?
                                        DocumentType.ARCHIVE :
                                        documentTypes.contains(DocumentType.ARCHIVE) ?
                                                                                        DocumentType.ARCHIVE :
                                                                                        documentTypes.iterator().next();
    }

    private class SaveAnnotation implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        { 
        	AttachmentRevision revision;
			try 
			{
				revision = AttachmentRevision.find(id);
				if (!revision.getAttachment().isDocumentType())
	             {
	                 throw new EdmException(sm.getString("ZalacznikNieJestZalacznikiemDoDokumentu"));
	             }
				revision.updateAnnotations(getAnnotations());
			}
			catch (EdmException e) 
			{
				addActionError(e.getMessage());
				LogFactory.getLog("eprint").debug("", e);
			} catch (IOException e) 
			{
				addActionError(e.getMessage());
				LogFactory.getLog("eprint").debug("", e);
			}
             
        }
    }
    
    public class SendToEva implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			Map<String,Object> statusMap = new HashMap<String,Object>();
                statusMap.put(RockwellLogic.EVA_STATUS_FIELD_CN,1);
                statusMap.put(RockwellLogic.SUBMITTED_BY_CN,DSApi.context().getPrincipalName());
                statusMap.put(RockwellLogic.SUBMIT_DATE_FIELD_CN,new Date());
                statusMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,RockwellLogic.STAT_PROCESSED_EVA);
                
                AttachmentRevision revision = AttachmentRevision.find(id);
                Document doc =  revision.getAttachment().getDocument();
               //  = Document.find(getDocumentId());
                if(doc != null)
                {
                	doc.getDocumentKind().setWithHistory(doc.getId(),statusMap,false);
                    
                	DSApi.context().begin();

                    OfficeDocument document = (OfficeDocument)doc;
                    DocumentType docType = document.getType();
                    WorkflowFactory.getInstance().manualFinish(getActivity(), true);
                   	DSApi.context().commit();
                    //powiadamiany EVA
                   	pl.compan.docusafe.parametrization.ra.EvaNotifier.notifyEva();

                    openNextTask(docType,next);
                    event.setResult("image");
                }
    		}                
    		catch(Exception e)
    		{
    			LogFactory.getLog("eprint").error("", e);
    			addActionError(e.getMessage());
    		}
    	}	
    }
    
    private void openNextTask(DocumentType docType,boolean next) throws EdmException, Exception
    {
    		documentReassigned = true;
			if(preventRefresh == Boolean.TRUE) 
			{				
				reloadLink = null;
				return;
			}
         	TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
         	DSUser user = DSApi.context().getDSUser();
         	if(docType.equals(DocumentType.INCOMING))
         	{
             	Map map = taskList.getInDocumentTasks(null, user, 0, 10, null, "documentOfficeNumber", true,null,null);
             	List<Task> tasks = (List<Task>) map.get("tasks");
             	if(tasks.size()> 0 && next)
             	{
             		if(tasks.get(0).isAccepted())
             			reloadLink = "/office/incoming/document-archive.action?documentId="+tasks.get(0).getDocumentId()
             				+"&activity=internal,"+tasks.get(0).getActivityKey()+"&openViewer=true";
             		else
             			reloadLink = "/office/accept-wf-assignment.action?doAccept=true&activity=internal,"
             				+tasks.get(0).getActivityKey()+"&target=archive&openViewer=true";
             	}
             	else
             	{
             		reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
             	}
             	
         	}
         	else if(docType.equals(DocumentType.OUTGOING))
         	{
         		 Map map = taskList.getDocumentTasks(null, user, OutOfficeDocument.TYPE, null, "documentOfficeNumber", true, 0, 100,null,null);
         		 List<Task> tasks = (List<Task>) map.get("tasks");
         		if(tasks.size()> 0 && next)
              	{
         			if(tasks.get(0).isAccepted())
         				reloadLink = "/office/outgoing/document-archive.action?documentId="+tasks.get(0).getDocumentId()
         					+"&activity=internal,"+tasks.get(0).getActivityKey();
         			else
         				reloadLink = "/office/accept-wf-assignment.action?doAccept=true&activity=internal,"
         					+tasks.get(0).getActivityKey()+"&target=archive";
              	}
              	else
              	{
              		reloadLink = "/office/tasklist/current-user-task-list.action?tab=out";
              	}
         	}
         	else if(docType.equals(DocumentType.INTERNAL))
         	{
         		Map map = taskList.getDocumentTasks(null, user, OutOfficeDocument.INTERNAL_TYPE, null, "documentOfficeNumber", true, 0, 100,null,null);
         		List<Task> tasks = (List<Task>) map.get("tasks");
         		if(tasks.size()> 0 && next)
             	{
         			if(tasks.get(0).isAccepted())
         				reloadLink = "/office/internal/document-archive.action?documentId="+tasks.get(0).getDocumentId()
         					+"&activity=internal,"+tasks.get(0).getActivityKey();
         			else
         				reloadLink = "/office/accept-wf-assignment.action?doAccept=true&activity=internal,"+tasks.get(0).getActivityKey()+"&target=archive";
             	}
             	else
             	{
             		reloadLink = "/office/tasklist/current-user-task-list.action?tab=internal";
             	}
         	}
    }
    
    /** FIXME stary spos�b dekretacji! do przerobienia*/
    public class Reject implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			Map<String,Object> valueMap = new HashMap<String,Object>();
                valueMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,RockwellLogic.STAT_REJECT_AP);
                Document doc = Document.find(getDocumentId());
                if(doc != null)
                {
                	doc.getDocumentKind().setOnly(doc.getId(),valueMap);
                }  

                WorkflowFactory.simpleDivisionAssignment(
                		getActivity(),
                		RockwellLogic.GUID_TO_REJECT,
                		null,
                		WorkflowFactory.wfManualName(),
                		null, 
                		"zwrot",
                		event);
                DSApi.context().commit();
                openNextTask(((OfficeDocument)doc).getType(),next);
                event.setResult("image");
    		}       
    		catch(Exception e)
    		{
    			log.error("",e);
    			addActionError(e.getMessage());
    			DSApi.context()._rollback();
    			
    		}
    	}
    }
    
    private class ManualFinish implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
				WorkflowFactory.getInstance().manualFinish(getActivity(), true);
				DSApi.context().commit();
				openNextTask(document.getType(), next);
				event.skip(EV_FILL);
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
			}
            DSApi._close();
			event.setResult("image");
		}
	}
    
    private void prepareData(Document document) throws EdmException
    {        
        if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
        {
            blocked = (document.getBlocked() != null && document.getBlocked());
        }
        else
            blocked = false;
        
        DocumentKind documentKind = document.getDocumentKind();
        fm = documentKind.getFieldsManager(document.getId());
        fm.initialize();
        
        // sprawdzanie czy mo�na czyta� s�owniki na stronie JSP
        canReadDictionaries = documentKind.logic().canReadDocumentDictionaries();
        officeDocumentHelper.initBox(ViewerAction.this);        
        documentId = document.getId();
        /**Zaladowanie uwag*/
        officeDocumentHelper.retrieveRemarks(thisAction);
        
        if (document instanceof OfficeDocument)
        {
            // UWAGI
        	OfficeDocument officeDocument = (OfficeDocument) document;
            officeNumber = officeDocument.getOfficeNumber();
            // DEKRETACJA
            if(!WorkflowFactory.jbpm){
                WorkflowActivity wfActivity = getActivity() != null ?
                    WorkflowFactory.getWfActivity(getActivity()) :
                    null;
                Map<String,Map<String,String>> results = WorkflowFactory.getInstance().prepareCollectiveAssignments(getActivity());
                if (results != null)
                {
                    enableCollectiveAssignments = true;
                    assignmentsMap = results.get("assignmentsMap");
                    substituted = results.get("substituted");
                    wfProcesses = results.get("wfProcesses");

                    //tu tez powinno byc przez helper
                    finishOrAssignState = WorkflowUtils.getFinishOrAssignState(officeDocument, wfActivity);
                }
            }
            
            // FLAGI
            String flagsOn = Configuration.getProperty("flags");
            // narazie nie ma flag dla wychodz�cych
            if ("true".equals(flagsOn) && (document.getType() == DocumentType.INCOMING || document.getType() == DocumentType.INTERNAL))
            {
                globalFlags = document.getFlags().getDocumentFlags(false);                           
                userFlags = document.getFlags().getDocumentFlags(true);
                flagsPresent = globalFlags.size() > 0;
            }
            else
                flagsPresent = false;
        }          
    }
    
    
    
    private class UpdateBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			try 
			{
				DSApi.context().begin();
				officeDocumentHelper.updateBox(ViewerAction.this);
				DSApi.context().commit();
	            addActionMessage(sm.getString("ZapisanoNumerPudlaArchiwalnego"));
			}
			catch (EdmException e) {
				log.error("", e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
        }
    }

    private class Archive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {            	            	
                DSApi.context().begin();
                if(AvailabilityManager.isAvailable("dwr.viewserver")) {
                    setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
                    dwrDocumentHelper.update(ViewerAction.this);
                }
                
                AttachmentRevision revision = AttachmentRevision.find(id);
                if (!revision.getAttachment().isDocumentType())
                {
                    throw new EdmException(sm.getString("ZalacznikNieJestZalacznikiemDoDokumentu"));
                }
                tiffyApplet = "true".equals(Docusafe.getAdditionProperty("tiffyApplet")) && DSApi.context().userPreferences().node("other").getBoolean("viewserverTiffyApplet", true); 
                if(tiffyApplet && getAnnotations() != null)
                {
                	revision.updateAnnotations(getAnnotations());
                }
                
                Document document = revision.getAttachment().getDocument();
                
                checkIntercars(document);

                DocumentKind documentKind = document.getDocumentKind();

                documentKind.logic().correctValues(values, documentKind);
                documentKind.logic().validate(values, documentKind, document.getId());

                documentKind.setWithHistory(document.getId(), values,true);

                documentKind.logic().archiveActions(document, DocumentKindsManager.getType(document));
                documentKind.logic().documentPermissions(document);                

                officeDocumentHelper.updateBox(ViewerAction.this);

                /*if (document.getType() == DocumentType.INCOMING && !DocumentKind.NORMAL_KIND.equals(documentKind.getCn()))
                {
                    for (Iterator iter=InOfficeDocumentKind.list().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKind kind = (InOfficeDocumentKind) iter.next();
                        if (kind.getName().indexOf("iznesowy") > 0)
                        {
                            ((InOfficeDocument) document).setKind(kind);
                            break;
                        }
                    }
                }*/

                // dodanie uwagi
                content = TextUtils.trimmedStringOrNull(content);                
                if ((content != null && !content.equals(sm.getString("WpiszTrescUwagi"))))
                    document.addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
                
                // flagi
                String flagsOn = Configuration.getProperty("flags");
                // narazie nie ma flag dla wychodz�cych
                if ("true".equals(flagsOn) && (document.getType() == DocumentType.INCOMING || document.getType() == DocumentType.INTERNAL))
                {
                	List<Long> ids;
                    if(globalFlag!=null)
                    {
    		            for (Long id : globalFlag)
    		            {
    		                if(!LabelsManager.existsDocumentToLabelBean(document.getId(), id)) LabelsManager.addLabel(document.getId(), id, DSApi.context().getPrincipalName());
    		            }
    		            globalFlags = document.getFlags().getDocumentFlags(false);
    		            ids = Arrays.asList(globalFlag);
    		            for(Flags.Flag f: globalFlags)
    		            {
    		            	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(document.getId(), f.getId())&&f.isCanClear()) LabelsManager.removeLabel(document.getId(), f.getId(),DSApi.context().getPrincipalName());
    		            }
                    }
                    else
                    {
                    	for(Flags.Flag f: document.getFlags().getDocumentFlags(false))
    		            {
    		            	if(LabelsManager.existsDocumentToLabelBean(document.getId(), f.getId())&&f.isCanClear()) LabelsManager.removeLabel(document.getId(), f.getId(), DSApi.context().getPrincipalName());
    		            }
                    }
                    
                    if(userFlag!=null)
                    {
    	                for (Long id :userFlag)
    	                {
    	                    if(!LabelsManager.existsDocumentToLabelBean(document.getId(), id)) LabelsManager.addLabel(document.getId(), id, DSApi.context().getPrincipalName());
    	                }
    	                userFlags = document.getFlags().getDocumentFlags(true);
    	                ids = Arrays.asList(userFlag);
    	                for(Flags.Flag f: userFlags)
    	                {
    	                	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(document.getId(), f.getId())) LabelsManager.removeLabel(document.getId(), f.getId());
    	                }
                    }
                    else
                    {
                    	for(Flags.Flag f: document.getFlags().getDocumentFlags(true))
     	                {
     	                	if( LabelsManager.existsDocumentToLabelBean(document.getId(), f.getId())) LabelsManager.removeLabel(document.getId(), f.getId());
     	                }
                    }
                }
                
                isReloadOpener = true;
                TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());

                DSApi.context().commit();

                addActionMessage(sm.getString("ZarchiwizowanoDokument"));
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                log.error(e.getMessage(), e);
                event.addActionError(e.getMessage());
            }
        }

		private void checkIntercars(Document document) throws EdmException {
			if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND)
					 || document.getDocumentKind().getCn().equals(DocumentLogicLoader.SAD_KIND)) {

				List<CentrumKosztowDlaFaktury> ckdfs = CentrumKosztowDlaFaktury
						.findByDocumentId(getDocumentId());
				boolean refresh = false;
				for (CentrumKosztowDlaFaktury ckdf : ckdfs) {
					String value;
					if ((value = ServletActionContext.getRequest()
							.getParameter("centrum" + ckdf.getId())) != null) {												
						Integer integer = new Integer(value);
						if(!integer.equals(ckdf.getAcceptanceMode())){
							log.debug(format("zmiana acceptance mode [id=%d] na %s",ckdf.getId(),value));
							ckdf.setAcceptanceMode(integer);
							refresh = true;
						}
					}
				}
				
				if(refresh){
					document.getDocumentKind().logic().getAcceptanceManager().refresh(document, document.getFieldsManager(), null);
				}					
			}
		}
    }
    
   /* private class AddRemark implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (content == null)
            {
                addActionError("Nie wpisano tre�ci uwagi");
                return;
            }

            try
            {
                DSApi.context().begin();

                AttachmentRevision revision = AttachmentRevision.find(id);
                Document document = revision.getAttachment().getDocument();
                if (document instanceof OfficeDocument)
                    ((OfficeDocument) document).addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }    */
    

    private class Assignments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (assignments == null || assignments.length == 0)
            {
                addActionError(sm.getString("NieWybranoUzytkownikowDoDekretacji"));
                return;
            }

			if(hasActionErrors()){
				return;
			}

            event.setResult("image");
            try
            {
                DSApi.context().begin();
 
                String[] activityIds = new String[] { getActivity() };
           	 	Set<String> paSet = new HashSet<String>();
           	 	for (int i=0; i < assignments.length; i++)
                {
                    String processDesc =  ServletActionContext.getRequest().getParameter("assignmentProcess_"+assignments[i]);
					String tmp;
					if(processDesc != null){
						String[] splitted = processDesc.split("::");
						if(splitted.length > 1){
							tmp = splitted[1];
						} else {
							tmp = WorkflowFactory.wfManualName();
						}
					} else {
						tmp = WorkflowFactory.wfManualName();
					}
          		 
           		 	String pa = WorkflowFactory.getPreparedAssignment(assignments[i], tmp, "");
           		 	paSet.add(pa);
                }
           	 	String[] paArray = paSet.toArray(new String[paSet.size()]);
           	 
           	 	WorkflowFactory.multiAssignment(
           	 			activityIds, 
           	 			DSApi.context().getPrincipalName(), 
           	 			paArray, 
           	 			null, 
           	 			event);
       		 
                Document document = Document.find(documentId);
           	 	TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());

                //boolean mainDocumentReassigned = WorkflowFactory.collectiveAssignment(assignments, wfActivity, documentId, event);                                            

                DSApi.context().commit();
            
                if (document.getType() == DocumentType.OUTGOING)
                {
                    reloadLink =  "/office/tasklist/current-user-task-list.action?tab=out";
                }
                else if (document.getType() == DocumentType.INTERNAL)
                {
                    reloadLink =  "/office/tasklist/current-user-task-list.action?tab=internal";
                }
                else
                {
                    reloadLink =  "/office/tasklist/current-user-task-list.action?tab=in";
                }
                event.skip(EV_FILL);
                documentReassigned = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
				log.error(e.getMessage(), e);
                addActionError(e.getMessage());
            }
        }
    }
    

    private Point adjustSize(Point imageSize, Point pageSize)
    {
        if (imageSize.x < pageSize.x && imageSize.y < pageSize.y)
            return new Point(imageSize);
        double fx = (double) pageSize.x / imageSize.x;
        double fy = (double) pageSize.y / imageSize.y;
        double f = Math.min(fx, fy);
        return new Point((int) (f * imageSize.x), (int) (f * imageSize.y));
    }
    
    private static double round(double value, int decimalPlace)
    {
      double power_of_ten = 1;
      while (decimalPlace-- > 0)
         power_of_ten *= 10.0;
      return Math.round(value * power_of_ten) / power_of_ten;
    }

    private class nextDecretation implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
            if(hasActionErrors()){
                addActionError("Wyst�pi�y b��dy - akceptacja nie mo�e zosta� wykonana");
                return;
            }
    		try
            {
    			if(!DSApi.context().isTransactionOpen())
    				DSApi.context().begin();
    			AcceptanceCondition thisAcceptance = null;
      	       	String nextAcceptance = null;


      	       	Document document = Document.find(getDocumentId());
      	       	DocumentKind kind = document.getDocumentKind();
      	       	Map<String, Object> valuesTmp = new TreeMap<String, Object>();
      	       	FieldsManager fm;
      	       	kind.initialize();
      	       	fm = kind.getFieldsManager(getDocumentId());
      	       	fm.initialize();
      	       	fm.initializeAcceptances();
      	       	Long centrumId = null;
      	       	CentrumKosztowDlaFaktury ckf = null;

      	       	if (document.getDocumentKind().logic().accept(document, centrumToAcceptanceId)) {

					log.debug("document accepted");

					if (document.getDocumentKind().logic()
							.getAcceptanceManager() != null) {

						log.debug("!JBPM acceptance!");

						DSApi.context().session().flush();
						if(acceptanceToUser == Boolean.TRUE){
							log.debug("Acceptance to user:" + acceptanceToUser);
							document.getDocumentKind().logic()
								.getAcceptanceManager().refresh(document,fm,
									null);
							sendToAfterAssignment(event);
						} else {
							document.getDocumentKind().logic()
								.getAcceptanceManager().refresh(document,fm,
									getActivity());
						}
					}
				} else {

					if (centrumToAcceptanceId == null) {
						ckf = CentrumKosztowDlaFaktury.findByDocumentId(
								getDocumentId()).get(0);
					} else {
						ckf = CentrumKosztowDlaFaktury.getInstance()
								.find(centrumToAcceptanceId);
					}

					Boolean simpleAcceptance = (IntercarsAcceptanceMode
							.from(fm) == IntercarsAcceptanceMode.SIMPLE2);

					thisAcceptance = fm.getAcceptancesState()
							.getThisAcceptation(ckf, simpleAcceptance);
					nextAcceptance = fm.getAcceptancesState()
							.getNextAcceptation(null, simpleAcceptance);

					centrumId = ckf.getId();

					if (thisAcceptance == null) {
						addActionError("Wszystkie akceptacje zosta�y wykonane dla dokumentu ");
					} else {
						this.accept(thisAcceptance, document, valuesTmp, fm,
								centrumId, ckf);
					}

					this.sendAfterAsignment(event, thisAcceptance,
								nextAcceptance, fm, ckf);
				}
				DSApi.context().session().flush();
				fm = kind.getFieldsManager(getDocumentId());
				if (!fm.getBoolean(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN)) {
					documentReassigned = true;
					reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
					event.setResult("task-list");
				}
    			TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
    			document.getDocumentKind().setOnly(document.getId(), valuesTmp);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	//event.setResult("error");
                addActionError(e.getMessage());
                log.error("DocumentArchiveTabAction.java:", e);
                try{
                    DSApi.context().rollback();
                } catch (Exception ex){
                    addActionMessage(e.getMessage());
                    log.error(ex.getMessage(), ex);
                }
            }
    	}

		private void sendAfterAsignment(ActionEvent event,
				AcceptanceCondition thisAcceptance, String nextAcceptance,
				FieldsManager fm, CentrumKosztowDlaFaktury ckf) throws EdmException,
				DivisionNotFoundException, UserNotFoundException {

			DSUser user = null;
  	       	DSDivision division = null;
			
			

			if(!acceptanceToUser){
			    if(nextAcceptance != null)
			    {
			    	Integer minTask = Integer.MAX_VALUE;
			    	DSUser userTmp = null;
			    	AcceptanceCondition acTmp  = null;

			    	List<CentrumKosztowDlaFaktury> centraDoAkceptacji = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
			    	for (CentrumKosztowDlaFaktury ckf2 : centraDoAkceptacji)
			    	{
			    		if(!(nextAcceptance.equals(thisAcceptance.getCn()) && ckf2.getAcceptingCentrumId().equals(ckf.getAcceptingCentrumId()) && centrumToAcceptanceId != null))
			    		{
			        		List<AcceptanceCondition> accList = AcceptanceCondition.find(nextAcceptance, ckf2.getAcceptingCentrumId().toString());
			        		if(accList != null && accList.size() > 0)
			        		{
			        			acTmp = accList.get(0);

			        		}
			    		}
					}
			    	if(acTmp == null && fm.getAcceptancesDefinition().getGeneralAcceptances().containsKey(nextAcceptance))
			    	{
			    		List<AcceptanceCondition> accList = AcceptanceCondition.find(nextAcceptance);
			    		if(accList != null && accList.size() > 0)
			    		{
			    			acTmp = accList.get(0);
			    		}
			    	}

			    	if(acTmp != null)
			    	{

			        	if(acTmp.getDivisionGuid() != null)
			        	{	    	        	
			        		division = DSDivision.find(acTmp.getDivisionGuid());
			        	}
			        	else if(acTmp.getUsername() != null)
			        	{
			        		userTmp = DSUser.findByUsername(acTmp.getUsername());
			        		Integer taskSize = 2;
			        		if(taskSize < minTask)
			        		{
			        			minTask = taskSize;
			        			user = userTmp;
			        		}
			        	}
			    	}

			        if(user != null || division != null)
			        {
			        	String[] documentActivity = new String[1];
			        	documentActivity[0] = getActivity();
			        	String plannedAssignment = "";

			        	if(user != null)
			        	{
			        		DSDivision[] divisions = user.getDivisions();
			        		plannedAssignment = (divisions != null && divisions.length > 0 ? divisions[0].getGuid() : DSDivision.ROOT_GUID)+"/"+user.getName()+"/"+WorkflowFactory.wfManualName()+"//"+"realizacja";
			        	}
			        	if(division != null)
			        	{
			        		plannedAssignment = division.getGuid()+"//"+WorkflowFactory.wfManualName()+"/*/realizacja";
			        	}

			            String curUser = DSApi.context().getPrincipalName();
			            WorkflowFactory.multiAssignment(documentActivity,curUser, new String[] {plannedAssignment}, plannedAssignment, event);
			            event.setResult("task-list");

			        }
			        else
			        {
			        	event.addActionError(sm.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
			        }
			    }
			    else
			    {
			    	event.addActionError(sm.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
			    }
			}
			else
			{
				sendToAfterAssignment(event);
			}
		}

		private void accept(AcceptanceCondition thisAcceptance,
				Document document, Map<String, Object> valuesTmp,
				FieldsManager fm, Long centrumId, CentrumKosztowDlaFaktury ckf)
				throws EdmException, DSException {



			if(fm.getAcceptancesDefinition().getAcceptances().get(thisAcceptance.getCn()).getFieldCn() == null)
			{
				AcceptancesManager.giveAcceptance(thisAcceptance.getCn(), getDocumentId());
				valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm.getAcceptancesDefinition().getAcceptances().get(thisAcceptance.getCn()).getLevel());
			}
			else
			{
				Map<Integer,String> centrumAcceptanceCn = new TreeMap<Integer, String>();
				centrumAcceptanceCn.put(new Integer(centrumId.toString()), thisAcceptance.getCn());
				AcceptancesManager.giveCentrumAcceptance(centrumAcceptanceCn, getDocumentId());

//				Logger.getLogger("MICHAEL").debug(
//						"DocumentArchiveTabAction.java:centrumAcceptanceCn = " + centrumAcceptanceCn.toString());

				if (thisAcceptance.getCn().equals("zwykla")) {
					valuesTmp.put("AKCEPTACJA_ZWYKLA", 1);

					//tylko podczas akceptacji zwyk�ej mo�na wybra� tryb akceptcji

					log.trace("DocumentArchiveTabAction.java:acceptanceMode");

				}
				valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm.getAcceptancesDefinition().getAcceptances().get(thisAcceptance.getCn()).getLevel());
			}
			addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu")+" : "+ thisAcceptance.getCn());
		}

		private void sendToAfterAssignment(ActionEvent event) throws EdmException {
			DSUser user = null;
			DSDivision division = null;
			userAcceptance = userAcceptance.replace(",", "");
			try {
				user = DSUser.findByUsername(userAcceptance);
			} catch (UserNotFoundException e) {
				try {
					division = DSDivision.find(userAcceptance);
				} catch (DivisionNotFoundException e2) {
					addActionError(sm.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
				}
			}
			if (user != null || division != null) {
				String[] documentActivity = new String[1];
				documentActivity[0] = getActivity();
				String plannedAssignment = "";
				if (user != null) {
					DSDivision[] divisions = user.getDivisions();
					plannedAssignment = (divisions != null && divisions.length > 0 ? divisions[0].getGuid() : DSDivision.ROOT_GUID) + "/" + user.getName() + "/" + WorkflowFactory.wfManualName() + "//" + "realizacja";
				}
				if (division != null) {
					plannedAssignment = division.getGuid() + "//" + WorkflowFactory.wfManualName() + "/*/realizacja";
				}
				String curUser = DSApi.context().getPrincipalName();
				WorkflowFactory.multiAssignment(documentActivity, curUser, new String[]{plannedAssignment}, plannedAssignment, event);
				event.setResult("task-list");
			}
		}
    }


    
    private class Discard implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{
			try
    		{
    			DSApi.context().begin();
    			
    			//newNote = TextUtils.trimmedStringOrNull(newNote);    
        		content = TextUtils.trimmedStringOrNull(content);  
        		OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
        		if (content != null)
        		{
	        		if(content != null)
	        		{
	        			((OfficeDocument) document).addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
	        		}
					addActionMessage(sm.getString("DodanoUwage"));
					
        		}
    			
        		InvoiceLogic.getInstance().discard(document, getActivity());
        		
    			
    			 if (document.getType() == DocumentType.OUTGOING)
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=out";
                 }
                 else if (document.getType() == DocumentType.INTERNAL)
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=internal";
                 }
                 else
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=in";
                 }
                 documentReassigned = true;
                 
                 DSApi.context().commit();
    		}
    		catch (EdmException e)
			{
        		addActionError(e.getMessage());
        		LogFactory.getLog("eprint").debug("", e);
				addActionError(e.getMessage());
			}
		}
    }
    
    private class DiscardTo implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
				if (discardUserAcceptance != null) {
					processDiscardToUser(document, event);
				} else if(discardCnAcceptance != null) {
					processDiscardToCn(document, event);
				} else {
					event.addActionError("Nie wybrano osoby");
				}


				if (document.getType() == DocumentType.OUTGOING) {
					reloadLink = "/office/tasklist/current-user-task-list.action?tab=out";
				} else if (document.getType() == DocumentType.INTERNAL) {
					reloadLink = "/office/tasklist/current-user-task-list.action?tab=internal";
				} else {
					reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
				}


				DSApi.context().commit();


			} catch (EdmException e) {
				addActionError(e.getMessage());
				LogFactory.getLog("eprint").debug("", e);
				addActionError(e.getMessage());
			}
		}

		private void addRemark(OfficeDocument document) throws EdmException {
			if (content != null) {
				if (content != null) {
					((OfficeDocument) document).addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
				}
				addActionMessage(sm.getString("DodanoUwage"));
			}
		}

		private void processDiscardToUser(OfficeDocument document, ActionEvent event) throws EdmException {
			discardUserAcceptance = discardUserAcceptance.replace(",", "");
			//newNote = TextUtils.trimmedStringOrNull(newNote);
			content = TextUtils.trimmedStringOrNull(content);
			addRemark(document);
			String[] documentActivity = new String[1];
			documentActivity[0] = getActivity();
			String plannedAssignment = "";
			DSUser user = DSUser.findByUsername(discardUserAcceptance);
			if (user != null) {
				DSDivision[] divisions = user.getDivisions();
				plannedAssignment = (divisions != null && divisions.length > 0 ? divisions[0].getGuid() : DSDivision.ROOT_GUID) + "/" + user.getName() + "/" + WorkflowUtils.iwfManualName() + "//" + "realizacja";
			} else {
				event.addActionError("Nie znaleziono osoby");
			}
			String curUser = DSApi.context().getDSUser().getName();
			if (user != null && user.getName().equals(curUser)) {
				documentReassigned = false;
			} else {
				WorkflowFactory.multiAssignment(documentActivity, curUser, new String[]{plannedAssignment}, plannedAssignment, event);
				//event.skip(EV_FILL);
				documentReassigned = true;
			}
			AcceptanceUtils.discardToUser(document, getActivity(), event, user);
		}

		private void processDiscardToCn(OfficeDocument document, ActionEvent event) throws EdmException {
			if(document == null) document = OfficeDocument.find(getDocumentId());
			AcceptanceUtils.discardToCn(document, getActivity(), event, discardCnAcceptance, null);
			addRemark(document);
			event.setResult("task-list");
		}
    }




    private class DiscardObjectAcceptance extends TransactionalActionListener {

        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(hasActionErrors()){
                throw new EdmException("Wyst�pi�y b��dy, odrzucanie akceptacji nie mo�e zosta� wykonane.");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            //log.error("transaction|{}|{}", objectId, discardCnAcceptance);
            AcceptanceUtils.discardToCn((OfficeDocument) document, getActivity(), event, acceptanceCn, objectId);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            openNextTask(DocumentType.INCOMING, false);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }
    
    private class DiscardToIM implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{
			try
    		{
    			DSApi.context().begin();
    			
    			//newNote = TextUtils.trimmedStringOrNull(newNote);    
        		content = TextUtils.trimmedStringOrNull(content);  
        		OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
        		if (content != null)
        		{
	        		if(content != null)
	        		{
	        			((OfficeDocument) document).addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
	        		}
					addActionMessage(sm.getString("DodanoUwage"));
					
        		}
    			
        		InvoiceLogic.getInstance().returnToIM(document, getActivity(),event);

        		 if (document.getType() == DocumentType.OUTGOING)
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=out";
                 }
                 else if (document.getType() == DocumentType.INTERNAL)
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=internal";
                 }
                 else
                 {
                     reloadLink =  "/office/tasklist/current-user-task-list.action?tab=in";
                 }
                 documentReassigned = true;
        		
    			DSApi.context().commit();

    		}
    		catch (EdmException e)
			{
        		addActionError(e.getMessage());
        		LogFactory.getLog("eprint").debug("", e);
				addActionError(e.getMessage());
			}
		}
    }

    private class ValidateCreate extends LoggedActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception
        {
            log.info("loading dwr fields");
            setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
        }

        @Override
        public Logger getLogger()
        {
            return log;
        }
    }

    private class UpdateDwr extends TransactionalActionListener
    {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception
        {
            dwrDocumentHelper.update(ViewerAction.this);
            isReloadOpener = true;
            addActionMessage(sm.getString("ZarchiwizowanoDokument"));
        }

        @Override
        public Logger getLogger()
        {
            return log;
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSrc()
    {
        return src;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getContentWidth()
    {
        return contentWidth;
    }

    public int getContentHeight()
    {
        return contentHeight;
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPageCount()
    {
        return pageCount;
    }

    public Pager getPager()
    {
        return pager;
    }
    
    public void setFax(boolean t)
    {
    	fax=t;
    }

    public boolean isFax()
    {
        return fax;
    }

    public boolean isCanReadDictionaries()
    {
        return canReadDictionaries;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public boolean isViewserverWithData()
    {
        return viewserverWithData;
    }    
    
    public Integer getViewserverDataSize()
    {
        return viewserverDataSize;
    }

    public boolean isViewserverWholePage()
    {
        return viewserverWholePage;
    }

    public Long getCurrentBoxId()
    {
        return currentBoxId;
    }

    public String getCurrentBoxNumber()
    {
        return currentBoxNumber;
    }

    public Long getBoxId()
    {
        return boxId;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }

    public boolean isNeedsNotBox()
    {
        return needsNotBox;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public boolean isBoxNumberReadonly()
    {
        return boxNumberReadonly;
    }

    public boolean isCanUpdate()
    {
        return canUpdate;
    }

    public List<RemarkBean> getRemarks()
    {
        return remarks;
    }

    public void setRemarks(List<RemarkBean> remarks) 
    {
		this.remarks = remarks;
	}
    
    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    @Override
    public Long getDocumentId()
    {
        return documentId;
    }

    @Override
    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public final String getActivity()
    {
        return activity;
    }

    public final void setActivity(String activity)
    {
        this.activity = activity;
    }    
    
    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public boolean isEnableCollectiveAssignments()
    {
        return enableCollectiveAssignments;
    }

    public FinishOrAssignState getFinishOrAssignState()
    {
        return finishOrAssignState;
    }

    public Map<String, String> getSubstituted()
    {
        return substituted;
    }

    public Map<String, String> getWfProcesses()
    {
        return wfProcesses;
    }

    public Map<String, String> getAssignmentsMap()
    {
        return assignmentsMap;
    }

    public void setAssignments(String[] assignments)
    {
        this.assignments = assignments;
    }

    public boolean isDocumentReassigned()
    {
        return documentReassigned;
    }

    public String getReloadLink()
    {
        return reloadLink;
    }

    public void setDoChangePage(boolean doChangePage)
    {
        this.doChangePage = doChangePage;
    }        
    
    public List getGlobalFlags()
    {
        return globalFlags;
    }

    public boolean isFlagsPresent()
    {
        return flagsPresent;
    }
    
    public Long[] getGlobalFlag() {
		return globalFlag;
	}
	public void setGlobalFlag(Long[] globalFlag) {
		this.globalFlag = globalFlag;
	}
	public List<Adnotation> getAdnotations()
    {
        return adnotations;
    }
	public Boolean getNext() {
		return next;
	}
	public void setNext(Boolean next) {
		this.next = next;
	}
	public Boolean getShowSendToEva() {
		return showSendToEva;
	}
	public void setShowSendToEva(Boolean showSendToEva) {
		this.showSendToEva = showSendToEva;
	}
	public Boolean getShowReject() {
		return showReject;
	}
	public void setShowReject(Boolean showReject) {
		this.showReject = showReject;
	}
	public Boolean getExternalWorkflow() {
		return externalWorkflow;
	}
	public void setExternalWorkflow(Boolean externalWorkflow) {
		this.externalWorkflow = externalWorkflow;
	}
	public String getEvaMessage() {
		return evaMessage;
	}
	public void setEvaMessage(String evaMessage) {
		this.evaMessage = evaMessage;
	}
	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}
	public Boolean getAcceptancesPage() {
		return acceptancesPage;
	}
	public void setAcceptancesPage(Boolean acceptancesPage) {
		this.acceptancesPage = acceptancesPage;
	}
	public String getDocumentKindCn() {
		return documentKindCn;
	}

    @Override
    public int getDocType() {
        return docType;
    }

    public void setDocType(int docType) {
        this.docType = docType;
    }

    public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

    @Override
    public Map<String, String> getDocumentKinds() {
        return documentKinds;
    }

    @Override
    public void setDocumentKinds(Map<String, String> documentKinds) {
        this.documentKinds = documentKinds;
    }

    @Override
    public Map<String, Object> getDockindKeys() {
        return this.dockindKeys;
    }

    @Override
    public void setDockindKeys(Map<String, Object> dockindKeys) {
        this.dockindKeys = dockindKeys;
    }

    @Override
    public void setDocument(Document find) {
        this.document = find;
    }

    public String getDockindAction()
    {
        return "edit";
    }
	public Long getCentrumToAcceptanceId() {
		return centrumToAcceptanceId;
	}
	public void setCentrumToAcceptanceId(Long centrumToAcceptanceId) {
		this.centrumToAcceptanceId = centrumToAcceptanceId;
	}
	public Map<Long, Map<String, String>> getUserToNextAcceptance() {
		return userToNextAcceptance;
	}
	public void setUserToNextAcceptance(
			Map<Long, Map<String, String>> userToNextAcceptance) {
		this.userToNextAcceptance = userToNextAcceptance;
	}
	public Map<String, String> getAcceptancesUserList() {
		return acceptancesUserList;
	}
	public void setAcceptancesUserList(Map<String, String> acceptancesUserList) {
		this.acceptancesUserList = acceptancesUserList;
	}
	public Boolean getAcceptanceToUser() {
		return acceptanceToUser;
	}
	public void setAcceptanceToUser(Boolean acceptanceToUser) {
		this.acceptanceToUser = acceptanceToUser;
	}
	public Boolean getGeneralAcceptances() {
		return generalAcceptances;
	}
	public void setGeneralAcceptances(Boolean generalAcceptances) {
		this.generalAcceptances = generalAcceptances;
	}
	public Map<String, String> getUserToGeneralAcceptance() {
		return userToGeneralAcceptance;
	}
	public void setUserToGeneralAcceptance(
			Map<String, String> userToGeneralAcceptance) {
		this.userToGeneralAcceptance = userToGeneralAcceptance;
	}
	public Boolean getFinishOn() {
		return finishOn;
	}
	public void setFinishOn(Boolean finishOn) {
		this.finishOn = finishOn;
	}
	public Map<String, String> getUserToAcceptance() {
		return userToAcceptance;
	}
	public void setUserToAcceptance(Map<String, String> userToAcceptance) {
		this.userToAcceptance = userToAcceptance;
	}
	public String getUserAcceptance() {
		return userAcceptance;
	}
	public void setUserAcceptance(String userAcceptance) {
		this.userAcceptance = userAcceptance;
	}
	public boolean isViewserverDataSide() {
		return viewserverDataSide;
	}
	public void setRotate(Integer rotate) {
		this.rotate = rotate;
	}
	public Integer getRotate() {
		return rotate;
	}
	public void setImgWidth(Integer imgWidth) {
		this.imgWidth = imgWidth;
	}
	public Integer getImgWidth() {
		return imgWidth;
	}
	public void setImgHeight(Integer imgHeight) {
		this.imgHeight = imgHeight;
	}
	public Integer getImgHeight() {
		return imgHeight;
	}
	public void setTiffyID(String tiffyID) {
		this.tiffyID = tiffyID;
	}
	public String getTiffyID() {
		return tiffyID;
	}
	public void setTiffyApplet(boolean tiffyApplet) {
		this.tiffyApplet = tiffyApplet;
	}
	public boolean isTiffyApplet() {
		return tiffyApplet;
	}
	public void setAnnotations(String annotations) {
		this.annotations = annotations;
	}
	public String getAnnotations() {
		return annotations;
	}
	public List<Flags.Flag> getUserFlags() {
		return userFlags;
	}
	public void setUserFlags(List<Flags.Flag> userFlags) {
		this.userFlags = userFlags;
	}
	public Long[] getUserFlag() {
		return userFlag;
	}
	public void setUserFlag(Long[] userFlag) {
		this.userFlag = userFlag;
	}
	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}
	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}
	public String getDocumentAspectCn() {
		return documentAspectCn;
	}
	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}
	public void setIsReloadOpener(Boolean isReloadOpener) {
		this.isReloadOpener = isReloadOpener;
	}
	public Boolean getIsReloadOpener() {
		return isReloadOpener;
	}

	public LinkedList<RenderBean> getProcessRenderBeans() {
		return processRenderBeans;
	}

	public boolean isManualFinishPossible() {
		return manualFinishPossible;
	}

	public void setManualFinishPossible(boolean manualFinishPossible) {
		this.manualFinishPossible = manualFinishPossible;
	}

	public boolean isJbpmAcceptances() {
		return jbpmAcceptances;
	}

	public void setJbpmAcceptances(boolean jbpmAcceptances) {
		this.jbpmAcceptances = jbpmAcceptances;
	}

	public CentrumKosztow getCentrum() {
		return centrum;
	}

	public void setCentrum(CentrumKosztow centrum) {
		this.centrum = centrum;
	}

	public Map<String, String> getAcceptanceNames() {
		return acceptanceNames;
	}

	public void setAcceptanceNames(Map<String, String> acceptanceNames) {
		this.acceptanceNames = acceptanceNames;
	}

	public Set<String> getCnsNeeded() {
		return cnsNeeded;
	}

	public void setCnsNeeded(Set<String> cnsNeeded) {
		this.cnsNeeded = cnsNeeded;
	}

	public boolean getPreventRefresh() {
		//return preventRefresh.equals(Boolean.TRUE);
		return Boolean.TRUE.equals(preventRefresh);
	}

	public void setPreventRefresh(boolean preventRefresh) {
		this.preventRefresh = preventRefresh;
	}

	public boolean isFinishAndGoNextPossible() {
		return finishAndGoNextPossible;
	}

	public void setFinishAndGoNextPossible(boolean finishAndGoNextPossible) {
		this.finishAndGoNextPossible = finishAndGoNextPossible;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessAction() {
		return processAction;
	}

	public void setProcessAction(String processAction) {
		this.processAction = processAction;
	}
	
	public void setLastRemark(Remark remark)
	{
		
	}

    @Override
	public Document getDocument()
	{
		try{
			if(document == null) document = Document.find(documentId);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return this.document;
	}

    @Override
    public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException {
        dwrDocumentHelper.setBeforeUpdate(this);
    }

    @Override
    public Logger getLog() {
        return log;
    }

    @Override
    public Document getNewDocument() {
        return dwrDocumentHelper.getNewDocument(this);
    }

    public void setDocument (OfficeDocument document)
	{
		document = document;
	}
	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}
	public Long getBinderId()
	{
		return binderId;
	}
	public void setPagesParam(String str) {
		this.pagesParam = str;
	}	
	public String getPagesParam() {
		return pagesParam;	
	}

	public Map<String, String> getAcceptancesCnList() {
		return acceptancesCnList;
	}

	public void setAcceptancesCnList(Map<String, String> acceptancesCnList) {
		this.acceptancesCnList = acceptancesCnList;
	}

	public String getDiscardUserAcceptance() {
		return discardUserAcceptance;
	}

	public void setDiscardUserAcceptance(String discardUserAcceptance) {
		this.discardUserAcceptance = discardUserAcceptance;
	}

	public String getDiscardCnAcceptance() {
		return discardCnAcceptance;
	}

	public void setDiscardCnAcceptance(String discardCnAcceptance) {
		this.discardCnAcceptance = discardCnAcceptance;
	}

	public boolean isAcceptanceButtonsForCurrentUserVisible() {
		return acceptanceButtonsForCurrentUserVisible;
	}

	public void setAcceptanceButtonsForCurrentUserVisible(boolean acceptanceButtonsForCurrentUserVisible) {
		this.acceptanceButtonsForCurrentUserVisible = acceptanceButtonsForCurrentUserVisible;
	}

	public boolean isFastAssignment() {
		return fastAssignment;
	}

	public void setFastAssignment(boolean fastAssignment) {
		this.fastAssignment = fastAssignment;
	}
	public void setBoxNumberReadonly(boolean boxNumberReadonly) {
		this.boxNumberReadonly = boxNumberReadonly;		
	}

	public void setCurrentBoxId(Long boxId) {
		this.currentBoxId = currentBoxId;		
	}

	public void setCurrentBoxNumber(String currentBoxNumber) {
		this.currentBoxNumber = currentBoxNumber;		
	}

	public void setNeedsNotBox(Boolean isneed) {
		this.needsNotBox = isneed;		
	}
	public boolean isPudloModify() {
		return pudloModify;
	}
	public void setPudloModify(boolean pudloModify) {
		this.pudloModify = pudloModify;
	}

    public boolean isBoxActionAvailable() {
        return AvailabilityManager.isAvailable("viewer.pudlo");
    }

    public int getBarCodeX() {
        return barCodeX;
    }

    public void setBarCodeX(int barCodeX) {
        this.barCodeX = barCodeX;
    }

    public int getBarCodeY() {
        return barCodeY;
    }

    public void setBarCodeY(int barCodeY) {
        this.barCodeY = barCodeY;
    }

    public int getBarCodePage() {
        return barCodePage;
    }

    public void setBarCodePage(int barCodePage) {
        this.barCodePage = barCodePage;
    }

  // czesc do zmiany, razem z sam� klasa Coordinates
	private Coordinates barCodes[] = {
    		new Coordinates(250, 250, 0, "bar1bla"),
    		new Coordinates(1000, 750, 0, "bar1bla"),
    		new Coordinates(250, 1250, 0, "bar1blll"),
    		new Coordinates(250, 1750, 0, "bar1weqew"),
    		new Coordinates(750, 2250, 0, "bar1ffdf"),
    		new Coordinates(250, 3000, 0, "bar1tytyt"),
    		new Coordinates(250, 250, 1, "barCode12321"),
    		new Coordinates(1000, 750, 1, "barCode343343"),
    		new Coordinates(250, 1250, 1, "barCode76234"),
    		new Coordinates(250, 1750, 1, "barCode888657"),
    		new Coordinates(750, 2250, 1, "barCode235643"),
    		new Coordinates(250, 3000, 1, "barCode333333")
    };//#todo

	public Coordinates[] getBarCodes() {
		return barCodes;
	}
	public void setBarCodes(Coordinates[] barCodes) {
		this.barCodes = barCodes;
  	}

    public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
  	}

    public boolean isShowFullAccount() {
        return showFullAccount;
    }
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getAcceptanceCn() {
        return acceptanceCn;
    }

    public void setAcceptanceCn(String acceptanceCn) {
        this.acceptanceCn = acceptanceCn;
    }

    public boolean isRejectObjectAcceptanceAvailable() {
        return rejectObjectAcceptanceAvailable;
    }
	public boolean isEditAsWebOdf() {
		return editAsWebOdf;
	}
	public void setEditAsWebOdf(boolean editAsWebOdf) {
		this.editAsWebOdf = editAsWebOdf;
	}
	
	public String getJcrDownloadLink() throws EdmException{
		return jcrDownloadLink;
	}
	
	public AttachmentRevision getRevision() {
		return revision;
	}
	
}
