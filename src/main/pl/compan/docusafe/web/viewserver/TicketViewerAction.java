package pl.compan.docusafe.web.viewserver;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.archive.AnonymousAccessTicket;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class TicketViewerAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	
	Logger log = LoggerFactory.getLogger(TicketViewerAction.class);
	
	private String code;

	protected void setup() 
	{
        registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        
        	
        	AnonymousAccessTicket ticket;
        	AttachmentRevision revision;
        	event.setResult(NONE);

            try
            {
            	DSApi.openAdmin();
            	ticket = AnonymousAccessTicket.findByCode(code);
            	
            	if(ticket.getValidTill()!=null && ticket.getValidTill().before(new Date()))
                {
                    throw new EdmException("Ticket zuzyty");
                }
            	if(ticket.getAccessCount() >= ticket.getMaxAccess())
            	{
            		throw new EdmException("Ticket zuzyty");
            	}
            	
            	if(ticket.getAttachmentRevisionId()!=null)
            	{
            		revision = AttachmentRevision.find(ticket.getAttachmentRevisionId());
            	}
            	else if(ticket.getAttachmentId()!=null)
            	{
            		revision = Attachment.find(ticket.getAttachmentId()).getMostRecentRevision();
            	}
            	else if(ticket.getDocumentId()!=null)
            	{
            		Date d = new Date();
            		Attachment oldest = null;
            		for(Attachment a:Document.find(ticket.getDocumentId()).listAttachments())
            		{
            			if(a.getCtime().before(d)) oldest = a;
            		}
            		revision = oldest.getMostRecentRevision();
            	}
            	else
            	{
            		addActionError("ERROR");
                    event.setResult(ERROR);
                    return;
            	}
            	DSApi.context().begin();
            	ticket.setAccessCount(ticket.getAccessCount()+1);
            	DSApi.context().session().save(ticket);
            	DSApi.context().commit();
            	ServletUtils.streamFile(ServletActionContext.getResponse(), revision.saveToTempFile(), revision.getMime(), "Content-Disposition: "+ticket.getReturnMethod()+"; filename=\"document_"+revision.getAttachment().getDocument().getId()+"\"");
            	DSApi.close();
            }
            catch(Exception e)
            {
            	DSApi.context().setRollbackOnly();
            	log.error("", e);
            	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                try
                {
                	ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                }
                catch(IOException f)
                {
                	log.error(f.getMessage(),f);
                }
                event.setResult(ERROR);
            }
        }
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	

}
