package pl.compan.docusafe.web.viewserver;

import java.io.*;

import org.slf4j.Logger;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.service.stagingarea.RetainedObjectAttachment;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
/**
 * Akcja zwraca zawarto�� za��cznika sprawdzaj�c wcze�niej uprawnienia. Je�eli uprawnienia
 * s� niewystarczaj�ce zwracana jest zawarto�c pliku insufficientPermissions.png z HOME'a.
 *
 * Je�eli jako parametr zostanie przekazany insufficientPermissions=true to zawsze zostanie zwr�cony powy�szy
 * obrazek.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ContentAction.java,v 1.27 2009/07/03 10:58:21 mariuszk Exp $
 */
public class ContentAction extends EventActionSupport
{
	Logger log = org.slf4j.LoggerFactory.getLogger(ContentAction.class);
    private Long id;
    private int page;
    private boolean fax = false;
    private Long binderId;

    private String format;
    /** flaga przekazywana gdy akcja ma za zadanie wy�wietli� obrazek o braku uprawnie� */
    private boolean insufficientPermissions;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	AttachmentRevision revision;
            format = AttachmentRevision.PNG;
            String mime;

            InputStream resp;
            try {
                //w miejscu tworzenia url'a sprawdzono uprawnienia - nie trzeba ich ponownie sprawdza�
                if(insufficientPermissions) {
                    mime = "image/png";
                    resp = getInsufficientPermissionsFileAsStream();
                } else if (fax == false){
            		revision = AttachmentRevision.find(id);
                    if (revision.getAttachment().isDocumentType() && !revision.getAttachment().getDocument().canReadAttachments(binderId))
                    {
                        mime = "image/png";
                        resp = getInsufficientPermissionsFileAsStream();
                    }
                    else
                    {
                        mime = revision.getMime();
                        resp = revision.getStreamByPage(page, format, fax);
                    }
            	} else {
            		RetainedObjectAttachment rev = RetainedObjectAttachment.find(id);
                	mime = rev.getMime();
                	resp = new FileInputStream(ViewServer.getFile(rev));
            	}
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(), e);
                return;
            }

            try
            {
                ServletUtils.streamResponse(ServletActionContext.getResponse(), resp, mime, resp.available(), new String[0]);
            }
            catch (IOException e)
            {
                log.warn(e.getMessage(), e);
            }
            event.setResult(NONE);
        }
    }

    private FileInputStream getInsufficientPermissionsFileAsStream() throws FileNotFoundException {
        return new FileInputStream(getInsufficientPermissionsFile());
    }

    //zwraca plik, ktory jest wyswietlany, gdy nie ma praw do podgladu zalacznika
    private File getInsufficientPermissionsFile()
    {
        File file = new File(Docusafe.getHome().getPath() +  "/insufficientPermissions.png");

        if(file.exists())
            return file;

        try{
            InputStream is=ServletActionContext.getServletContext().getResourceAsStream("/img/insufficientPermissions.png");
            file.createNewFile();
            OutputStream os = new FileOutputStream(file);
            org.apache.commons.io.IOUtils.copy(is,os);
            org.apache.commons.io.IOUtils.closeQuietly(is);
            org.apache.commons.io.IOUtils.closeQuietly(os);
        }
        catch(IOException ioe){return null;}

        return file;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setPage(int page)
    {
        this.page = page;
    }
    public void setFax(boolean t){
    	fax=t;
    }
    
    public boolean getFax(){
    	return fax;
    }

	public void setBinderId(Long binderId) {
		this.binderId = binderId;
	}

	public Long getBinderId() {
		return binderId;
	}

    public boolean isInsufficientPermissions() {
        return insufficientPermissions;
    }

    public void setInsufficientPermissions(boolean insufficientPermissions) {
        this.insufficientPermissions = insufficientPermissions;
    }
}
