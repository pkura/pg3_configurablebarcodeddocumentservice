/*
 * AnonymousViewServer.java
 */
package pl.compan.docusafe.web.viewserver;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.util.AttachmentUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.lowagie.text.DocumentException;
import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * Viewer za��cznik�w. Dla za��cznik�w generowany jest jednorazowy adres URL.
 * Pod tym linkiem mo�na zobaczy� za��cznik nie b�d�c zalogowanym
 * (tylko plik, a nie strone viewserwera).
 *
 * Na razie tylko dla dokument�w z dockinda rockwell
 *
 * @author Piotr Komisarski, Oskar Jarczyk
 */
public class AnonymousViewerAction extends EventActionSupport
{

	public final static Logger log = LoggerFactory.getLogger(AnonymousViewerAction.class);
    //swego rodzaju hash, na podstawie kt�rego jest podejmowana decyzja czy pokaza� plik czy nie.
    //pierwsze 32 znaki to hash, istniej�cy w kolumnie "url" tabeli dsg_rockwell
    private String param;
    

    protected void setup()
    {
        registerListener(DEFAULT_ACTION)
        	.appendFinally(new FillForm());
        
        registerListener("getAttachment")
        	.appendFinally(new AttachmentForm());
    }

    private class FillForm implements ActionListener
    {
    	final static String sql = "select document_id from dsg_rockwell where url = ?";
    	
        public void actionPerformed(ActionEvent event)
        {
            if (param == null)
            {
                setParam(((String)event.getAttribute("param")).substring(0, 32));
            }
            event.setResult(NONE);
            PreparedStatement ps = null;
            try
            {
            	String attachmentName = "attachment.pdf";
                DSApi.openAdmin();
                ps = DSApi.context().prepareStatement(sql);
                ps.setString(1, param);
                ResultSet rs = ps.executeQuery();
                
                if (!rs.next())
                {
                    String komunikat = "<h2>Rockwell document not found</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return;
                }

                Long id = rs.getLong(1);
                attachmentName = "invoice_"+id+".pdf"; 
                rs.close();
                ps.close();
                rs = null;
                ps = null;
                Long attachmentId = getAttachmentId(id);
                StringBuffer sb = new StringBuffer();
                File file = null;
                
                try
                {
                	/**
                	 * @TODO - jest problem z pobieraniem zalacznikow Landscape
                	 * Zrobic to ta metoda jak podglad PDFa 
                	 */
                	file = PdfUtils.getAttachmentAsFile(attachmentId, sb);
                }
                catch (IOException e)
                {
                	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return; 
                }
                catch (EdmException e) 
				{
                	AnonymousViewerAction.log.debug("", e);
                	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return; 
				}
                catch (DocumentException e) 
				{
                	AnonymousViewerAction.log.debug("", e);
				}
                if(file == null)
                {
                	String komunikat = "<h2>Wrong type of attachment</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return;
                }
                else
                {            
                	ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/pdf", "Content-Disposition: inline; filename=\""+attachmentName+"\"");                
                }

            }
            //ta akcja jest potencjalnie nara�ona na ataki, wi�c nie
            //mo�na pr�bowa� stara� si� uodparnia� na wyj�tki - je�li
            //zajdzie byle jaki to od razu ustawiamy result na NONE
            catch (SQLException sqle)
            {
            	AnonymousViewerAction.log.error("", sqle);
                event.setResult(ERROR);
            }
            catch (EdmException edme)
            {
            	AnonymousViewerAction.log.error("", edme);
                event.setResult(ERROR);
            }
            catch(FileNotFoundException fnfe)
            {
            	AnonymousViewerAction.log.error("", fnfe);
                event.setResult(ERROR);
            }
            catch(IOException ioe)
            {
            	AnonymousViewerAction.log.error("", ioe);
                event.setResult(ERROR);
            }
            finally
            {   
            	DSApi.context().closeStatement(ps);
            	DSApi._close();
            }
        }
    }
    
    
    /**
     * Umo�liwia udost�pnienie za��cznika na zaewn�trz dla niezalogowanych u�ytkownik�w
     * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
     *
     */
    private class AttachmentForm implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
        {
            if (param == null)
            {
                setParam(((String)event.getAttribute("param")));
            }
            event.setResult(NONE);
            
            try
            {
            	String attachmentName = "attachment.pdf";
            	String mime = "application/pdf";
                DSApi.openAdmin();
                Long attachmentId = AttachmentUtils.decodeAnonymousParam(param);
                
                if (attachmentId == null)
                {
                    String komunikat = "<h2>Document not found</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return;
                }

                File file = null;
                
                try
                {
                	//@FIXME do modyfikacji
//                	file = PdfUtils.getAttachmentAsFile(attachmentId, sb);
                	Attachment attachment = Attachment.find(attachmentId);
                	AttachmentRevision revision = attachment.getMostRecentRevision();
                	if(revision == null)
                		throw new Exception("Za��cznik nie znaleziony");
                	file = revision.saveToTempFile();
                	attachmentName = revision.getOriginalFilename();
                	mime = revision.getMime();
                }
                catch (IOException e)
                {
                	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return; 
                }
                catch (EdmException e) 
				{
                	AnonymousViewerAction.log.debug("", e);
                	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return; 
				}
                catch (Exception e) 
				{
                	AnonymousViewerAction.log.debug("", e);
                	String komunikat = "<h2>"+e.getMessage()+"</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return; 
				}
                if(file == null)
                {
                	String komunikat = "<h2>Wrong type of attachment</h2>";
                    ByteArrayInputStream str = new ByteArrayInputStream(komunikat.getBytes());
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), str, "text/html", str.available(), "Content-Disposition: inline;");
                    return;
                }
                else
                {            
                	ServletUtils.streamFile(ServletActionContext.getResponse(), file, mime, "Content-Disposition: inline; filename=\""+attachmentName+"\"");                
                }

            }
            //ta akcja jest potencjalnie nara�ona na ataki, wi�c nie
            //mo�na pr�bowa� stara� si� uodparnia� na wyj�tki - je�li
            //zajdzie byle jaki to od razu ustawiamy result na NONE
           
            catch (EdmException edme)
            {
            	AnonymousViewerAction.log.error("", edme);
                event.setResult(ERROR);
            }
            catch(FileNotFoundException fnfe)
            {
            	AnonymousViewerAction.log.error("", fnfe);
                event.setResult(ERROR);
            }
            catch(IOException ioe)
            {
            	AnonymousViewerAction.log.error("", ioe);
                event.setResult(ERROR);
            }
            catch (Exception sqle)
            {
            	AnonymousViewerAction.log.error("", sqle);
                event.setResult(ERROR);
            }
            finally
            {   
            	DSApi._close();
            }
        }
    }
    
    /**
     *  Funkcja, kt�ra pobiera id dokumentu i zwraca id za��cznika.
     *  Je�li jest wi�cej ni� jeden za��cznik to zwraca najnowszy
     *
     */
    private long getAttachmentId(long document_id) throws EdmException, SQLException
    {
        PreparedStatement ps = null; 
        try
        { 
        	//wedlug zgloszenia rockwell nie powinnismy brac najnowszego zalacznika tylko najstarszy
        	//----
        	ps = DSApi.context().prepareStatement("select id from ds_attachment where document_id = ? order by id");      
        	ps.setLong(1, document_id);
        	ResultSet rs = ps.executeQuery();

        	if (!rs.next())
        		throw new EdmException("Attachment not found");
        	long ret = rs.getLong("ID");
        	rs.close();
        	return ret;
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
        
    }

    public void setParam(String param)
    {
        this.param = param;
    }
}
