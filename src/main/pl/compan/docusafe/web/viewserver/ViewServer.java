package pl.compan.docusafe.web.viewserver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.util.ShUtils;
import pl.compan.docusafe.service.stagingarea.RetainedObjectAttachment;

import java.io.File;
import java.io.IOException;
import pl.compan.docusafe.core.base.Attachment;
/* User: Administrator, Date: 2005-12-27 15:38:52 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewServer.java,v 1.19 2010/07/29 13:28:31 pecet1 Exp $
 */
public class ViewServer
{
    private static final Log log = LogFactory.getLog(ViewServer.class);

    private static File temp;

    /**
     * Zwraca informacje czy dany mime okre�la plik xml (jest kilka mo�liwo�ci)
     * @param mime
     * @return
     */
    public static boolean mimeXml(String mime){
        log.info("mimeXml = " + mime);
        return mime.equals("application/xml");
    }

    public static boolean mimePicture(String mime){
        log.info("mimePicture = " + mime);
        return mime.startsWith("image/tiff") ||
                mime.startsWith("image/png") ||
                mime.startsWith("image/jpeg");
    }

    public static boolean mimeWebOdf(String mime){
        log.info("mimeWebOdf = " + mime);
        return mime.startsWith("application/vnd.oasis.opendocument.text");
    }

    public static boolean mimeAcceptable(String mime)
    {
            return
            mime.startsWith("application/pdf") ||
            mime.startsWith("application/msword") ||
            mime.startsWith("application/vnd.ms-excel") ||
            mime.startsWith("application/vnd.openxmlformats-officedocument.wordprocessingml.document") ||
            mime.startsWith("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
            mime.startsWith("application/rtf") ||
            mime.startsWith("application/vnd.oasis.opendocument.text") ||
            mime.startsWith("application/vnd.oasis.opendocument.spreadsheet") ||
            mime.startsWith("text/plain");
    }

    public static boolean mimeAcceptableWithoutOnViewer(String mime) {
        boolean richTextAsPdf = AvailabilityManager.isAvailable("viewserver.rich-document-as-pdf");
        boolean pdfOnViewer = AvailabilityManager.isAvailable("pdf.onviewer");

        return mime.startsWith("image/tiff") ||
                mime.startsWith("image/png") ||
                mime.startsWith("image/jpeg") ||
                mime.startsWith("application/pdf") ||
                (richTextAsPdf && !pdfOnViewer);
    }
    
    public static boolean mimeImageAcceptable(String mime)
    {
        return mime.startsWith("image/tiff") ||
            mime.startsWith("image/png") ||
            mime.startsWith("image/jpeg");
    }
    
    public static File getFile(RetainedObjectAttachment revision) throws EdmException, IOException
    {
        File file = new File(Attachment.getPath(revision.getId()));
        if(!Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM || !file.exists())
        {          
            if (!file.exists() || !file.isFile() || file.length() != revision.getSize().intValue())
            {
                log.debug("Tworzenie pliku1 "+file);
                ShUtils.delete(file);
                revision.saveToFile(file);
            }
            else
            {
                log.debug("Znaleziono plik1 "+file);
            }
        }
        return file;
    }

    
   /* public static File getFile(AttachmentRevision revision) throws EdmException, IOException
    {
        File file = new File(Attachment.getPath(revision.getId()));
        if(!Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM || !file.exists())
        {
            if (!file.exists() || !file.isFile() || file.length() != revision.getSize().intValue())
            {            	
                ShUtils.delete(file);
                revision.saveToFile(file);
            }
 
        }
        //jesli zalaczniki ogolnie sa trzymane na dysku 
        //oraz akurat ten egzemplarz jest na dysku 
        //to wystarczy go zwrocic
        return file;
    }*/
    
    private synchronized static File getCache() throws EdmException, IOException
    {
        if (ViewServer.temp == null)
        {
            if (!Docusafe.getTemp().canWrite())
                throw new EdmException("Brak praw do zapisu w katalogu "+
                    Docusafe.getTemp());

            int serial = 1;
            File cache;
            do
            {
                cache = new File(Docusafe.getHome(),"viewserver");

                // znaleziono ju� istniej�ce cache
                if (cache.isDirectory() && new File(cache, "viewserver").isFile())
                    break;

                if (!cache.exists() && !cache.mkdirs())
                {
                    throw new EdmException("Nie mo�na utworzy� katalogu "+cache);
                }
                else
                {
                    new File(cache, "viewserver").createNewFile();
                    break;
                }
            }
            while (true);

            ViewServer.temp = cache;
        }

        return temp;
    }
}
