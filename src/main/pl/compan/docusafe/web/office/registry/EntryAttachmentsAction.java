package pl.compan.docusafe.web.office.registry;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.AttachmentRevisionSignature;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */
public class EntryAttachmentsAction extends EventActionSupport
{
    private StringManager sm=
        GlobalPreferences.loadPropertiesFile(EntryAttachmentsAction.class.getPackage().getName(),null);
    // @EXPORT
    private List attachments = new LinkedList();
    private Attachment attachment;
    private boolean useScanner;
    private boolean useSystemJTwain;
    private boolean useSignature;
    private boolean canSign;
    private boolean editorOn;       
    private boolean showBtnMarkAttachments;
    private Collection<Map> revisionsReversed;
    private Collection<Map> attachmentBeans;
    private RegistriesMenuInfo menuInfo;
    private boolean canRead;
    private boolean canUpdate;
    private List<Tab> tabs;
    private String registryName;
    
    // @IMPORT
    private Long[] attachmentIds;
    private String title;
    private String barcode;
    private String content;
    private Long attachmentId;
    private Long revisionId;
    private Map markAttachment = new HashMap();

    
    private FormFile file;
    private Long entryId;
    private RegistryEntry entry;

    

    private static final String EV_ADD = "add";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new Add()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doInitEdit").
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            append(new InitEdit()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doEdit").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new Edit()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doInitAddWithEditor").
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            append(new InitAddWithEditor()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddWithEditor").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new AddWithEditor()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddRevision").
            append(OpenHibernateSession.INSTANCE).
            append(EV_ADD, new AddRevision()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doMarkAttachment").
            append(OpenHibernateSession.INSTANCE).
            append(new MarkAttachment()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                tabs = EntryTabs.createTabs(EntryTabs.TAB_ATTACHMENTS, getEntryId());

                if (entryId == null)
                    throw new EdmException("nie podano identyfikatora wpisu"); 
                
                entry = RegistryEntry.find(entryId);
                Registry registry = entry.getRegistry();
                registryName = registry.getName();
                
                // przygotowanie informacji dla menu rejestr�w
                menuInfo = Registry.getRegistriesMenuInfo(registry.getId());
                
                // uprawnienia dla rejestr�w
                canRead = registry.canRead();
                canUpdate = registry.canWrite();
                
                if (!canRead)                    
                    throw new EdmException("Nie posiadasz uprawnie� do czytania wpis�w w tym rejestrze");
                
                useScanner = DSApi.context().userPreferences().node("scanner").getBoolean("use", false);
                useSystemJTwain = DSApi.context().userPreferences().node("scanner").getBoolean("useSystemJTwain", false);
                useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
                canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);
                //blocked = (document.getBlocked() != null && document.getBlocked());
                editorOn = Configuration.additionAvailable(Configuration.ADDITION_EDITOR);
                
                // wybrano do podgl�du konkretny za��cznik
                if (attachmentId != null)
                {
                    attachment = entry.getAttachment(attachmentId);

                    final int count = 10;

                    revisionsReversed = fun.map(
                        fun.slice(fun.reverse(attachment.getRevisions()), 0, count),
                        new lambda<AttachmentRevision, Map>()
                        {
                            public Map act(AttachmentRevision attachmentRevision)
                            {
                                BeanBackedMap result = new BeanBackedMap(attachmentRevision,
                                    "id", "size", "ctime", "revision");
                                try
                                {
                                    result.put("author", DSUser.findByUsername(attachmentRevision.getAuthor()));
                                    if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                                        result.put("signatures", AttachmentRevisionSignature.findByAttachmentRevisionId(attachmentRevision.getId()));
                                        
                                }
                                catch (EdmException e)
                                {
                                }
                                return result;
                            }
                        });
                }
                else
                {
                    class mapper implements lambda<Attachment, Map>
                    {
                        public boolean anyNullAttachments = false;

                        public Map act(Attachment attachment)
                        {
                            BeanBackedMap result = new BeanBackedMap(attachment,
                                "id", "mostRecentRevision", "registryEntry", "title", "barcode", "lparam", "cn", "wparam");
                            try
                            {
                                result.put("author", DSUser.findByUsername(attachment.getAuthor()));
                                result.put("attCtime", attachment.getCtime());
                            }
                            catch (EdmException e)
                            {
                            }

                            if (attachment.getRevisions().isEmpty())
                                anyNullAttachments = true;

                            return result;
                        }

                        public boolean isAnyNullAttachments()
                        {
                            return anyNullAttachments;
                        }
                    }

                    final mapper mapper = new mapper();
                    attachmentBeans = fun.map(entry.getAttachments(), mapper);
                    showBtnMarkAttachments = mapper.anyNullAttachments;
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
                addActionError("Nie podano tytu�u za��cznika");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                RegistryEntry entry = RegistryEntry.find(getEntryId());

                Attachment attachment = entry.getAttachment(attachmentId);

                String newTitle = TextUtils.trimmedStringOrNull(title, 254);
                if (attachment.getTitle() != null && !attachment.getTitle().equals(newTitle))
                    addActionMessage("Zmieniono tytu� za��cznika");

                attachment.setTitle(newTitle);

                String newBarcode = TextUtils.trimmedStringOrNull(barcode, 25);
                if (attachment.getBarcode() != null && !attachment.getBarcode().equals(newBarcode))
                    addActionMessage("Zmieniono kod kreskowy");

                attachment.setBarcode(newBarcode);

                DSApi.context().commit();
                addActionMessage("Zapisano zmiany");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateAdd implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
            {
                addActionError("Nie podano tytu�u za��cznika");
            }

            if (!StringUtils.isEmpty(barcode) && barcode.trim().length() > 25)
            {
                addActionError("Zbyt d�ugi kod kreskowy (maksymalnie 25 znak�w)");
            }

            if (hasActionErrors())
            {
                //event.skip(EV_HANDLE_UPLOAD);
                event.skip(EV_ADD);
                return;
            }
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            try
            {
                DSApi.context().begin();

                RegistryEntry entry = RegistryEntry.find(getEntryId());

                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                entry.createAttachment(attachment);

                attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                
                entry.getWorkHistory().add(Audit.create("attachments", DSApi.context().getPrincipalName(),
                    sm.getString("DodanoZalacznik",attachment.getTitle())));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                file.getFile().delete();
            }
        }
    }

    private class InitAddWithEditor implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("editor");
        }
    }


    private class AddWithEditor implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            try
            {
                DSApi.context().begin();

                RegistryEntry entry = RegistryEntry.find(getEntryId());

                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                entry.createAttachment(attachment);

                byte[] buf = content.getBytes();
                AttachmentRevision revision = attachment.createRevision(new ByteArrayInputStream(buf), content.length(), title+".html");//.setOriginalFilename(title+".html");
                revision.setEditable(Boolean.TRUE);

                entry.getWorkHistory().add(Audit.create("attachments", DSApi.context().getPrincipalName(),
                    sm.getString("DodanoZalacznik",attachment.getTitle())));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class InitEdit implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            InputStream stream = null;
            try
            {
            	//@TODO - do poprawy, usuniecie ifa/calosci moze powodowac bledy w r�nych systemach
            	if(!DSApi.context().isTransactionOpen()){
            		DSApi.context().begin();
            	}
            	
                AttachmentRevision revision = AttachmentRevision.find(revisionId);

                title = revision.getAttachment().getTitle();
                barcode = revision.getAttachment().getBarcode();

                stream = revision.getBinaryStream();
                if (stream != null)
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }

                    content = os.toString();

                    os.close();
                    stream.close();
                    stream = null;
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            catch (IOException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }

            event.setResult("editor");
        }
    }


    private class Edit implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            try
            {
                DSApi.context().begin();

                AttachmentRevision revision = AttachmentRevision.find(revisionId);


                Attachment attachment = revision.getAttachment();
                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                attachment.setTitle(TextUtils.trimmedStringOrNull(title, 254));

                byte[] buf = content.getBytes();
                if (DSApi.context().getPrincipalName().equals(revision.getAuthor()))
                {
                    revision.updateBinaryStream(new ByteArrayInputStream(buf), content.length());
                    revision.setSize(content.length());
                    addActionMessage("Zmiany zapisano w najnowszej istniej�cej wersji za��cznika");
                }
                else
                {
                    AttachmentRevision newRevision = attachment.createRevision(new ByteArrayInputStream(buf), content.length(), title+".html");//.setOriginalFilename(title+".html");
                    newRevision.setEditable(Boolean.TRUE);
                    addActionMessage("Utworzono now� wersj� za��cznika");
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class AddRevision implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                RegistryEntry entry = RegistryEntry.find(getEntryId());

                entry.getAttachment(attachmentId).createRevision(file.getFile());

                DSApi.context().commit();
                addActionMessage("Zaktualizowano tre�� za��cznika");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                file.getFile().delete();
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (attachmentIds == null || attachmentIds.length == 0)
                addActionError("Nie zaznaczono za��cznik�w do usuni�cia");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                RegistryEntry entry = RegistryEntry.find(getEntryId());

                for (Long id : attachmentIds)
                {
                    Attachment attachment = entry.getAttachment(id); 
                    attachment.delete();
                    entry.getWorkHistory().add(Audit.create("attachments", DSApi.context().getPrincipalName(),
                        sm.getString("UsunietoZalacznik",attachment.getTitle())));
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class MarkAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // markAttachment: String -> String[]
            // klucze to napisowe reprezentacji id za��cznik�w,
            // warto�ci to pozycje wybrane z listy

            // sprawdzam, przy ilu za��cznikach wybrano jak�� warto��
            // z listy rozwijanej
            int selectedCount = 0;
            for (Iterator iter=markAttachment.entrySet().iterator(); iter.hasNext(); )
            {
                // kluczami tablicy s� identyfikatory za��cznik�w (jako napisy),
                // warto�ciami jednoelementowe tablice napis�w odpowiadaj�ce
                // warto�ciom wybranym z rozwijanej listy przy danym za��czniku
              
                Map.Entry entry = (Map.Entry) iter.next();
                if (!(entry.getKey() instanceof String) ||
                    !(entry.getValue() instanceof String || entry.getValue() instanceof String[]))
                    continue;
                try
                {
                    
                    Long.valueOf((String) entry.getKey());
                    String[] values;
                    if (entry.getValue() instanceof String) {
                        values = new String[1];
                        values[0] = (String) entry.getValue();
                    }
                    else
                        values = (String[]) entry.getValue();
                    
                    if (values.length > 0 && values[0].trim().length() > 0)
                        selectedCount++;
                }
                catch (NumberFormatException e)
                {
                    continue;
                }
            }

            if (selectedCount == 0)
                addActionError("Nie wybrano za��cznik�w do oznaczenia");

            if (hasActionErrors())
                return;

        }
    }

    public List getAttachments()
    {
        return attachments;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setAttachmentIds(Long[] attachmentIds)
    {
        this.attachmentIds = attachmentIds;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Long getAttachmentId()
    {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId)
    {
        this.attachmentId = attachmentId;
    }

    public Long getRevisionId()
    {
        return revisionId;
    }

    public void setRevisionId(Long revisionId)
    {
        this.revisionId = revisionId;
    }

    public Attachment getAttachment()
    {
        return attachment;
    }

    public Map getMarkAttachment()
    {
        return markAttachment;
    }

    public boolean isUseScanner()
    {
        return useScanner;
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public boolean isCanSign()
    {
    	return canSign;
    }   

    public boolean isEditorOn()
    {
        return editorOn;
    }

    public boolean isShowBtnMarkAttachments()
    {
        return showBtnMarkAttachments;
    }

    public Collection<Map> getRevisionsReversed()
    {
        return revisionsReversed;
    }

    public Collection<Map> getAttachmentBeans()
    {
        return attachmentBeans;
    }

    public boolean isUseSystemJTwain()
    {
        return useSystemJTwain;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setRevisionFile(FormFile file)
    {
        this.file = file;
    }

    public FormFile getRevisionFile()
    {
        return file;
    }

    public List getTabs()
    {
        return tabs;
    }

    public RegistryEntry getEntry()
    {
        return entry;
    }

    public void setEntryId(Long entryId)
    {
        this.entryId = entryId;
    }

    public Long getEntryId()
    {
        return entryId;
    }

    public boolean isCanRead()
    {
        return canRead;
    }  
    
    public boolean isCanUpdate()
    {
        return canUpdate;
    }
    
    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }

    public String getRegistryName()
    {
        return registryName;
    }

    public String getBaseLink()
    {
        return "/office/registry/entry-attachments.action";
    }
}
