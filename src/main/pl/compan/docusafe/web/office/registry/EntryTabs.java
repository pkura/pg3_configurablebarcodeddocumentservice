package pl.compan.docusafe.web.office.registry;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;

public class EntryTabs
{
	private final static StringManager sm=
        GlobalPreferences.loadPropertiesFile(EntryTabs.class.getPackage().getName(),null);
    public static String TAB_MAIN = "main";
    public static String TAB_ATTACHMENTS = "attachments";
    public static String TAB_HISTORY = "history";
    
    public static List<Tab> createTabs(String tab, Long entryId)
    {
        List<Tab> tabs = new ArrayList<Tab>();
        
        tabs.add(new Tab(sm.getString("Ogolne"), sm.getString("OgolneInformacje"), 
                "/office/registry/entry-main.action?entryId="+entryId, TAB_MAIN.equals(tab)));
        tabs.add(new Tab(sm.getString("Zalaczniki"), sm.getString("Zalaczniki"), 
                "/office/registry/entry-attachments.action?entryId="+entryId, TAB_ATTACHMENTS.equals(tab)));       
        tabs.add(new Tab(sm.getString("Historia"), sm.getString("Historia"), 
                "/office/registry/entry-history.action?entryId="+entryId, TAB_HISTORY.equals(tab)));
        
        return tabs;
    }
}
