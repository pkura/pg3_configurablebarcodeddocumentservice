package pl.compan.docusafe.web.office.registry;

import java.util.*;

import org.hibernate.Hibernate;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.regtype.RegistriesManager;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.regtype.RegistryEntryManager;
import pl.compan.docusafe.core.office.*;

import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl>Marcin Wlizlo</a>
 */
public class EntryMainAction extends EventActionSupport
{    
    // @IMPORT/@EXPORT
    private Long entryId;
    private Map<String, Object> values = new HashMap<String, Object>(); 
    
    // @EXPORT
    private List<Tab> tabs;
    private Long registryId;
    private Date ctime;
    private String creator;
    private Registry registry;
    private Map<Long,String> cases;
    private InOfficeDocument inDocument;
    private RegistryEntryManager rm;        
    /** numer wpisu w formacie numer_kolejny/rok */
    private String entryNumberPerYear;  
    private RegistriesMenuInfo menuInfo;
    private boolean canRead;
    private boolean canWrite;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    
    }
     
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            tabs = EntryTabs.createTabs(EntryTabs.TAB_MAIN, entryId);
            
            try
            {
                if (entryId != null)
                {
                    RegistryEntry entry = RegistryEntry.find(entryId);
                    // przygotowanie informacji dla menu rejestr�w
                    menuInfo = Registry.getRegistriesMenuInfo(entry.getRegistry().getId());
                    registry = entry.getRegistry();
                    
                    canRead = registry.canRead();
                    canWrite = registry.canWrite();
                    
                    if (!canRead)                    
                        throw new EdmException("Nie posiadasz uprawnie� do czytania wpis�w w tym rejestrze");                    
                    
                    Integer year = entry.getYear();
                    Integer entryNumber = entry.getSequenceNumber();
                    entryNumberPerYear = "" + entryNumber + "/" + year;                                                            
                    creator = entry.getCreator() != null ? DSUser.findByUsername(entry.getCreator()).asFirstnameLastname() : null;
                    ctime = entry.getCtime();
                    cases = new LinkedHashMap<Long, String>();
                    for (OfficeCase oc : entry.getCases())
                    {
                        cases.put(oc.getId(), oc.getOfficeId());
                    }
                    if (entry.getInDocument() != null)
                    {
                        Hibernate.initialize(entry.getInDocument());
                        inDocument = entry.getInDocument();
                        Hibernate.initialize(inDocument.getSender());
                    }
                    rm = registry.getRegistryEntryManager(entry.getId());
                    rm.initialize();
                    
                    // jak byl blad, to powracamy na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
                    if (hasActionErrors())
                        rm.reloadValues(values);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }


    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                if (entryId != null)
                {
                    RegistryEntry entry = RegistryEntry.find(entryId);
                    RegistriesManager.updateRegistryEntry(entry, values);
                }
                
                DSApi.context().commit();                
            }
            catch(EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public void setEntryNumberPerYear(String entryNumberPerYear)
    {
        this.entryNumberPerYear = entryNumberPerYear;
    }

    public String getEntryNumberPerYear()
    {
        return entryNumberPerYear;
    }

    public void setRegistryId(Long registryId)
    {
        this.registryId = registryId;
    }

    public Long getRegistryId()
    {
        return registryId;
    }

    public Registry getRegistry()
    {
        return registry;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public String getCreator()
    {
        return creator;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public Map<Long, String> getCases()
    {
        return cases;
    }

    public RegistryEntryManager getRm()
    {
        return rm;
    }

    public Long getEntryId()
    {
        return entryId;
    }

    public void setEntryId(Long entryId)
    {
        this.entryId = entryId;
    }

    public List<Tab> getTabs()
    {
        return tabs;
    }

    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }    
    
    public boolean isCanRead()
    {
        return canRead;
    }   
    
    public boolean isCanWrite()
    {
        return canWrite;
    }

    public InOfficeDocument getInDocument()
    {
        return inDocument;
    }
}

