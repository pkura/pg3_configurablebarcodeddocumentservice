package pl.compan.docusafe.web.office.registry;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class IndexAction extends EventActionSupport
{
    // @EXPORT
    private RegistriesMenuInfo menuInfo;
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                // przygotowanie informacji dla menu rejestr�w
                menuInfo = Registry.getRegistriesMenuInfo(null);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }
    
    
}
