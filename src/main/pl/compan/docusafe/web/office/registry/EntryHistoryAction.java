package pl.compan.docusafe.web.office.registry;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */
public class EntryHistoryAction extends EventActionSupport
{
    // @EXPORT
    private List<Tab> tabs;  
    private Collection<WorkHistoryBean> workHistory;
    private String registryName;
    private RegistriesMenuInfo menuInfo;
    private boolean canRead;

    // @IMPORT/@EXPORT
    private Long entryId;  
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            tabs = EntryTabs.createTabs(EntryTabs.TAB_HISTORY, entryId);
            
            try
            {
                RegistryEntry entry = RegistryEntry.find(entryId);
                // przygotowanie informacji dla menu rejestr�w
                menuInfo = Registry.getRegistriesMenuInfo(entry.getRegistry().getId());                                
                registryName = entry.getRegistry().getName();
                
                canRead = entry.getRegistry().canRead();
                if (!canRead)                    
                    throw new EdmException("Nie posiadasz uprawnie� do czytania wpis�w w tym rejestrze");                    
                
                workHistory = fun.reverse(fun.map(entry.getWorkHistory(),
                    new lambda<Audit, WorkHistoryBean>()
                    {
                        public WorkHistoryBean act(Audit audit)
                        {
                            try
                            {
                                return new WorkHistoryBean(audit);
                            }
                            catch (EdmException e)
                            {
                                throw new NoSuchElementException();
                            }
                        }
                    }));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public static class WorkHistoryBean
    {
        private Date date;
        private String author;
        private String content;

        public WorkHistoryBean(Audit entry) throws EdmException
        {
            this.date = entry.getCtime();
            this.author = DSUser.safeToFirstnameLastname(entry.getUsername());
            this.content = entry.getDescription();
        }

        public Date getDate()
        {
            return date;
        }

        public String getAuthor()
        {
            return author;
        }

        public String getContent()
        {
            return content;
        }
    }

    public Collection<WorkHistoryBean> getWorkHistory()
    {
        return workHistory;
    }

    public void setEntryId(Long entryId)
    {
        this.entryId = entryId;
    }

    public Long getEntryId()
    {
        return entryId;
    }
    
    public List<Tab> getTabs()
    {
        return tabs;
    }

    public String getRegistryName()
    {
        return registryName;
    }

    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }

    public boolean isCanRead()
    {
        return canRead;
    }   
}
