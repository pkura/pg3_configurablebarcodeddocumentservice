package pl.compan.docusafe.web.office.registry;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.regtype.RegistriesManager;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.regtype.RegistryEntryManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NewEntryAction extends EventActionSupport
{
    // @IMPORT/@EXPORT
    /** id rejestru do kt�rego zostanie wpisana sprawa */    
    private Long registryId;
    /** id sprawy, z kt�r� powi�za� nowy wpis w rejestrze */
    private Long caseId;
    /** id pisma przychodz�cego, z kt�rym powi�za� nowy wpis w rejestrze */
    private Long documentId;
    /** warto�ci atrybut�w specyficznych dla danego rejestru */
    private Map<String, Object> values = new HashMap<String, Object>();
    
    // @EXPORT
    private RegistryEntryManager rm;
    private Registry registry;
    /** 
     * id utworzonego wpisu - uzywane tylko na koniec po pomyslnym utworzeniu wpisu do 
     * przekierowania do odpowiedniej strny 
     */
    private Long entryId;
    private RegistriesMenuInfo menuInfo;
    private boolean canCreate;
    private String caseOfficeId;
    private InOfficeDocument inDocument;
    
    public static final String EV_FILL = "fill";
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).            
            append(new Create()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    
    private boolean canCreate(Registry registry, boolean addErrors) throws EdmException
    {
        if (!registry.canWrite())
        {
            if (addErrors)
                addActionError("Nie masz uprawnie� do tworzenia wpis�w w tym rejestrze");
            return false;
        }
           
        if (Registry.INDOCUMENT.equals(registry.getRegType()) && documentId == null)
        {
            if (addErrors)
                addActionError("Nie mo�na utworzy� wpisu w tym rejestrze bez okre�lenia pisma przychodz�cego");
            return false;
        }
        
        if (Registry.CASE.equals(registry.getRegType()) && caseId == null)
        {
            if (addErrors)
                addActionError("Nie mo�na utworzy� wpisu w tym rejestrze bez okre�lenia sprawy");
            return false;
        }
        
        if (registry.getRequiredRwa() != null && caseId != null && !OfficeCase.find(caseId).getRwa().getRwa().equals(registry.getRequiredRwa()))
        {
            if (addErrors)
                addActionError("Nie mo�na utworzy� wpisu w tym rejestrze dla sprawy, kt�ra nie znajduje si� w teczce "+registry.getRequiredRwa());
            return false;
        }
        
        return true;
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                registry = Registry.find(registryId);
                
                if (menuInfo == null)
                    menuInfo = Registry.getRegistriesMenuInfo(registry.getId()); 
                
                canCreate = canCreate(registry, true);
                           
                if (caseId != null)
                {
                    caseOfficeId = OfficeCase.find(caseId).getOfficeId();
                }
                if (documentId != null)
                {
                    inDocument = InOfficeDocument.findInOfficeDocument(documentId);
					values.put("PRZEDMIOT_SKARGI", inDocument.getTitle());
                }
                rm = registry.getRegistryEntryManager(registryId);
                rm.initialize();
                rm.reloadValues(values);
                // jak byl blad, to powracamy na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
                if (hasActionErrors())
                    rm.reloadValues(values);

            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                Registry registry = Registry.find(registryId);
                if (!canCreate(registry, false))
                    return;
                
                RegistryEntry entry = RegistriesManager.createRegistryEntry(registryId, caseId, documentId, values);
                entryId = entry.getId();
                
                DSApi.context().commit();
                
                event.skip(EV_FILL);
                event.setResult("entry");
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public Long getCaseId()
    {
        return caseId;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public Long getRegistryId()
    {
        return registryId;
    }

    public void setRegistryId(Long registryId)
    {
        this.registryId = registryId;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public Registry getRegistry()
    {
        return registry;
    }

    public RegistryEntryManager getRm()
    {
        return rm;
    }

    public Long getEntryId()
    {
        return entryId;
    }

    public boolean isCanCreate()
    {
        return canCreate;
    }

    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }


    public String getCaseOfficeId()
    {
        return caseOfficeId;
    }


    public InOfficeDocument getInDocument()
    {
        return inDocument;
    }              
}
