package pl.compan.docusafe.web.office.registry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.regtype.Field;
import pl.compan.docusafe.core.regtype.RegistriesManager;
import pl.compan.docusafe.core.regtype.RegistriesMenuInfo;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.regtype.RegistryEntryManager;
import pl.compan.docusafe.core.regtype.RegistryQuery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class SearchEntriesAction extends EventActionSupport
{
    private static final int LIMIT = 10;
    /** link dla aktualnego wyszukania (ale bez parematr�w do sortowania) */
    private String thisSearchUrl;
    
    // @IMPORT/@EXPORT
    private Registry registry;    
    private String ctimeFrom;
    private String ctimeTo;    
    private Integer sequenceNumberFrom;
    private Integer sequenceNumberTo;
    private String[] creatingUser;
    private String caseOfficeId;
    private String sender;

    // @EXPORT
    private List<Map<String, Object>> results;
    private List<DSUser> users;
    private List<TableColumn> columns;
    private Pager pager;
    private String printUrl;
    private RegistriesMenuInfo menuInfo;
    private boolean canRead;
    
    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();
    private Long registryId;
    private String sortField;
    private boolean ascending;
    private int offset;    
    private int limit;
    private String printout;
    
    public static final String EV_FILL = "fill";
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).            
            append(new Search()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {    
            if (registryId == null)
                return;
            
            try
            {               
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                registry = Registry.find(registryId);
                // przygotowanie informacji dla menu rejestr�w
                if (menuInfo == null)
                    menuInfo = Registry.getRegistriesMenuInfo(registry.getId()); 
                
                registry.initialize();
                
                canRead = registry.canRead();
                if (!canRead)
                    addActionError("Nie posiadasz uprawnie� do przegl�dania tego rejestru");
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                registry = Registry.find(registryId);
                // przygotowanie informacji dla menu rejestr�w
                menuInfo = Registry.getRegistriesMenuInfo(registry.getId()); 
                
                if (!registry.canRead())
                {
                    event.skip(EV_FILL);
                    throw new EdmException("Nie posiadasz uprawnie� do przegl�dania tego rejestru");
                }
                
                if (sortField == null)                
                    sortField = "sequenceNumber";                                    
                
                limit = limit > 0 ? limit : LIMIT;
                RegistryQuery registryQuery = new RegistryQuery(offset, limit);
                registryQuery.setRegistry(registry);
                
                Date ctFrom = DateUtils.nullSafeParseJsDate(ctimeFrom);
                Date ctTo = DateUtils.nullSafeParseJsDate(ctimeTo);
                
                registryQuery.ctime(
                        DateUtils.nullSafeMidnight(ctFrom, 0),
                        DateUtils.nullSafeMidnight(ctTo, 1));
                registryQuery.sequenceNumber(sequenceNumberFrom, sequenceNumberTo);
                
                registryQuery.creatingUser(creatingUser);
                
                // warto�ci p�l formularza do odno�nika wyszukiwania
                final Map<String,Object> fieldValues = new HashMap<String,Object>();
                
                for (Field field : registry.getFields())
                {                                                                                   
                    if (Field.ENUM.equals(field.getType()))
                    {   
                        if (values.get(field.getCn()) == null)
                            continue;
                        String[] enumStr;
                        if (values.get(field.getCn()) instanceof String)
                        {
                            enumStr = new String[] {(String) values.get(field.getCn())};
                        }
                        else
                        {
                            enumStr = (String[]) values.get(field.getCn());
                        }                           
                        Integer[] enumIds = new Integer[enumStr.length];
                        for (int e=0; e < enumStr.length; e++)  {
                            try {enumIds[e] = new Integer(enumStr[e]);} catch(NumberFormatException ex) {}
                        }
                        registryQuery.enumField(field, enumIds);
                        fieldValues.put(field.getCn(), enumIds);
                    }
                    else if (Field.DATE.equals(field.getType()) || field.isSearchByRange())
                    {
                        String from = (String) values.get(field.getCn()+"_from");
                        String to = (String) values.get(field.getCn()+"_to");
                        Object valueFrom = field.simpleCoerce(from);
                        Object valueTo = field.simpleCoerce(to);
                        registryQuery.rangeField(field, valueFrom, valueTo);
                        fieldValues.put(field.getCn()+"_from", from);
                        fieldValues.put(field.getCn()+"_to", to);
                    }
                    else if (Field.STRING.equals(field.getType()))
                    {
                        String value = (String) field.simpleCoerce(values.get(field.getCn()));
                        registryQuery.stringField(field, value);
                        fieldValues.put(field.getCn(), value);
                    }
                    else if (Field.BOOL.equals(field.getType()))
                    {
                        String value = (String) values.get(field.getCn());
                        Boolean bool = (Boolean) field.simpleCoerce(value);
                        registryQuery.boolField(field, bool);
                        fieldValues.put(field.getCn(), bool);
                    }
                    else if (Field.FLOAT.equals(field.getType()))
                    {
                        String value = (String) values.get(field.getCn());
                        Float fl = (Float) field.simpleCoerce(value);
                        registryQuery.field(field, fl);
                        fieldValues.put(field.getCn(), fl);
                    }
                    else if (Field.CLASS.equals(field.getType()))
                    {
                        String value = (String) values.get(field.getCn());                          
                        registryQuery.field(field, value);
                        for (String property : field.getDictionaryAttributes().keySet())
                        {
                            value = (String) values.get(field.getCn()+"_"+property);
                            registryQuery.otherClassField(field, property, value);
                            fieldValues.put(field.getCn()+"_"+property, value);
                        }
                    }
                    else
                    {
                        Object value = field.simpleCoerce(values.get(field.getCn()));
                        if (value instanceof Object[])
                        {
                            Object[] array = (Object[]) value;
                            if (array.length == 0)
                                value = null;
                            else
                                value = array[0];
                        }
                        if (value != null)
                        {
                            registryQuery.field(field, values.get(field.getCn()));
                            fieldValues.put(field.getCn(), values.get(field.getCn()));
                        }
                    }
                }
                
                if (ascending)
                    registryQuery.orderAsc(sortField);
                else
                    registryQuery.orderDesc(sortField);
                
                // url celowo bez sortField, bo ten parametr b�dzie dodany p�niej w getSortLink()
                thisSearchUrl = getLink("doSearch", offset, limit, null, null, registryId, fieldValues, creatingUser, sequenceNumberFrom, sequenceNumberTo, ctimeFrom, ctimeTo);
                printUrl = getLink("doSearch", offset, limit, sortField, ascending, registryId, fieldValues, creatingUser, sequenceNumberFrom, sequenceNumberTo, ctimeFrom, ctimeTo); 
                
                SearchResults<RegistryEntry> searchResults = RegistriesManager.search(registryQuery);
                
                if (printout != null)
                {
                    try
                    {
                        // wykonujemy wydruk
                        File tmp = RegistriesManager.generatePrintout(registry, printout, searchResults.results());
                            
                        ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/pdf");
                        
                        event.skip(EV_FILL);
                        return;
                    }
                    catch (IOException e)
                    {
                        throw new EdmException("B��d podczas generowania wydruku", e);
                    }
                }               
                
                if (searchResults.count() == 0)
                {
                    addActionMessage("Nie znaleziono wpis�w do rejestru");
                }
                else
                {   
                    event.skip(EV_FILL);
                    
                    columns = createColumns();
                    results = new ArrayList<Map<String,Object>>(searchResults.count());
                    
                    while (searchResults.hasNext())
                    {
                        RegistryEntry entry = searchResults.next();

                        Map<String, Object> bean = new HashMap<String, Object>();   
                        bean.put("link", "/office/registry/entry-main.action?entryId="+entry.getId());                       
                        bean.put("entry_id", entry.getId());
                        bean.put("sequenceNumber", entry.getSequenceNumber());
                        bean.put("ctime", entry.getCtime());
                        bean.put("creator", DSUser.safeToFirstnameLastname(entry.getCreator()));
                        if (Registry.CASE.equals(registry.getRegType()))
                            bean.put("case", entry.getCasesDescription());
                        else if (Registry.INDOCUMENT.equals(registry.getRegType()) && entry.getInDocument() != null && entry.getInDocument().getSender() != null)
                            bean.put("sender", entry.getInDocument().getSender().getShortSummary());
                        RegistryEntryManager rm = registry.getRegistryEntryManager(entry.getId());
                        
                        for (Field field :  registry.getFields())
                        {
                            if (field.isHidden() || field.isSearchHidden())
                                continue;
                            bean.put("registry_"+field.getCn(), rm.getDescription(field.getCn()));
                        }

                        results.add(bean);
                    }                    

                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return SearchEntriesAction.getLink("doSearch", offset, limit, sortField, ascending, 
                                    registryId, fieldValues, creatingUser, sequenceNumberFrom, sequenceNumberTo, 
                                    ctimeFrom, ctimeTo);
                        }
                    };
                    pager = new Pager(linkVisitor, offset, limit, searchResults.totalCount(), 10);                    
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     * Tworzy informacje o kolumnach wy�wietlanych na wynikach wyszukiwania.
     * 
     * @return
     * @throws EdmException
     */
    public List<TableColumn> createColumns() throws EdmException
    {
        List<TableColumn> columns = new ArrayList<TableColumn>(10);
        columns.add(new TableColumn("sequenceNumber", "Nr", getSortLink("sequenceNumber", false), getSortLink("sequenceNumber", true)));
        columns.add(new TableColumn("ctime", "Data wpisu", getSortLink("ctime", false), getSortLink("ctime", true)));                    
        columns.add(new TableColumn("creator", "U�ytkownik wpisuj�cy", null/*getSortLink("creator", false)*/, null/*getSortLink("creator", true)*/));
        if (Registry.CASE.equals(registry.getRegType()))
            columns.add(new TableColumn("case", "Sprawa", null, null));
        else if (Registry.INDOCUMENT.equals(registry.getRegType()))
            columns.add(new TableColumn("sender", "Nadawca", null, null));
        
        for (Field field : registry.getFields())
        {
            if (field.isHidden() || field.isSearchHidden())
                continue;
            columns.add(new TableColumn(
                "registry_"+field.getCn(),
                field.getName(),
                // dla atrybut�w wielowarto�ciowych nie ma obecnie sortowania
                field.isMultiple() ? null : getSortLink("registry_"+field.getCn(), false),
                field.isMultiple() ? null : getSortLink("registry_"+field.getCn(), true)));                            
        }
        
        return columns;
    }
    
    /**
     * Formatuje odpowiednio dany obiekt, aby m�g� zosta� poprawnie wy�wietlony (wo�ane ze strony JSP).
     */
    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            return DateUtils.formatJsDateTime((Date) object);
        }
        else
        {
            return object.toString();
        }
    }
    
    /**
     * Tworzy link na podstawie atrybut�w. 
     */
    public static String getLink(String action,int offset, int limit, String sortField, Boolean ascending,
            Long registryId, Map<String,Object> fieldValues, String[] creatingUser,
            Integer sequenceNumberFrom, Integer sequenceNumberTo, String ctimeFrom, String ctimeTo)
    {
        StringBuilder link = new StringBuilder("/office/registry/search-entries.action" +
            "?"+action+"=true" +
            "&offset="+offset+
            "&limit="+limit+
            (sortField != null ? "&sortField="+sortField : "") +
            (ascending != null ? "&ascending="+ascending : "") +
            (registryId != null ? "&registryId="+registryId : "") +
            (ctimeTo != null ? "&ctimeTo="+ctimeTo : "") +
            (ctimeFrom != null ? "&ctimeFrom="+ctimeFrom : "") +
            (sequenceNumberTo != null ? "&sequenceNumberTo="+sequenceNumberTo : "") +
            (sequenceNumberFrom != null ? "&sequenceNumberFrom="+sequenceNumberFrom : ""));
        
        if (creatingUser != null)
        {
            for (int i=0; i < creatingUser.length; i++)
            {
                link.append("&creatingUser=");
                link.append(creatingUser[i]);
            }
        }
        
        if (fieldValues != null)
        {
            for (String key : fieldValues.keySet())
            {
                if (fieldValues.get(key) == null)
                    continue;
                
                if (fieldValues.get(key) instanceof Object[])
                {
                    Object[] o = (Object[]) fieldValues.get(key);
                    for (int i=0; i < o.length; i++)
                    {
                       link.append("&values.");
                       link.append(key);
                       link.append("=");
                       link.append(o[i]);
                    }
                }
                else
                {
                    link.append("&values.");
                    link.append(key);
                    link.append("=");
                    link.append(fieldValues.get(key));
                }
            }
        }
        return link.toString();
    }

    /** 
     * Przekazuje link do sortowania dla danego kryterium sortuj�cego.
     */
    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }
    
    public String getCtimeFrom()
    {
        return ctimeFrom;
    }

    public void setCtimeFrom(String ctimeFrom)
    {
        this.ctimeFrom = ctimeFrom;
    }

    public String getCtimeTo()
    {
        return ctimeTo;
    }

    public void setCtimeTo(String ctimeTo)
    {
        this.ctimeTo = ctimeTo;
    }

    public void setRegistryId(Long registryId)
    {
        this.registryId = registryId;
    }

    public List<Map<String, Object>> getResults()
    {
        return results;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public Registry getRegistry()
    {
        return registry;
    }

    public Integer getSequenceNumberFrom()
    {
        return sequenceNumberFrom;
    }

    public void setSequenceNumberFrom(Integer sequenceNumberFrom)
    {
        this.sequenceNumberFrom = sequenceNumberFrom;
    }

    public Integer getSequenceNumberTo()
    {
        return sequenceNumberTo;
    }

    public void setSequenceNumberTo(Integer sequenceNumberTo)
    {
        this.sequenceNumberTo = sequenceNumberTo;
    }

    public String[] getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String[] creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public String getCaseOfficeId()
    {
        return caseOfficeId;
    }

    public void setCaseOfficeId(String caseOfficeId)
    {
        this.caseOfficeId = caseOfficeId;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPrintout(String printout)
    {
        this.printout = printout;
    }

    public List<TableColumn> getColumns()
    {
        return columns;
    }

    public String getPrintUrl()
    {
        return printUrl;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public RegistriesMenuInfo getMenuInfo()
    {
        return menuInfo;
    }

    public boolean isCanRead()
    {
        return canRead;
    }     
}
