package pl.compan.docusafe.web.office;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.struts.Globals;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import java.io.*;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintEnvelopesAction.java,v 1.11 2009/02/28 08:03:58 wkuty Exp $
 */
public class PrintEnvelopesAction extends EventProcessingAction
{
    private static final StringManager sm =
        StringManager.getManager(pl.compan.docusafe.web.office.Constants.Package);

    private static final String FORWARD = "office/print-envelopes";

    protected void setup()
    {
        SetActionForwardListener setForward = new SetActionForwardListener("main");
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long journalId = (Long) event.getDynaForm().get("journalId");
            Integer journalStartingNumber = (Integer) event.getDynaForm().get("journalStartingNumber");
            Integer journalEndingNumber = (Integer) event.getDynaForm().get("journalEndingNumber");

            DSContext ctx = null;
            File temp = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                DocumentHelper dh = ctx.getDocumentHelper();

                Journal journal = null;

                journal = Journal.find(journalId);

                if (Journal.OUTGOING.equals(journal.getJournalType()))
                {
                    List entries = journal.findEntries(
                        journalStartingNumber.intValue(), journalEndingNumber.intValue());

                    temp = File.createTempFile("docusafe_", "_tmp");
                    com.lowagie.text.Document pdfDoc =
                        new com.lowagie.text.Document(PageSize.A6.rotate());
                    PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                    pdfDoc.open();

                    File fontDir = new File(Configuration.getHome(), "fonts");
                    File arial = new File(fontDir, "arial.ttf");
                    BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                        BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                    Font font = new Font(baseFont, 12);
//                    BaseFont helvetica = BaseFont.createFont("Helvetica", BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
//                    Font font = new Font(helvetica, 12, Font.NORMAL);

                    for (int i=0; i < entries.size(); i++)
                    {
                        final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(((JournalEntry) entries.get(i)).getDocumentId());

                        if (document.getRecipients().size() > 0)
                        {
                            pdfDoc.add(new Phrase(((Recipient) document.getRecipients().get(0)).getSummary(), font));
                        }

                        pdfDoc.newPage();
                    }

                    pdfDoc.close();

                    event.getResponse().setContentType("application/pdf");
                    event.getResponse().setContentLength((int) temp.length());

                    InputStream is = new FileInputStream(temp);
                    OutputStream os = event.getResponse().getOutputStream();
                    org.apache.commons.io.IOUtils.copy(is,os);
                    org.apache.commons.io.IOUtils.closeQuietly(is);
                    org.apache.commons.io.IOUtils.closeQuietly(os);
                }

            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            catch (IOException e)
            {
                // b��dy w okienku (alert)
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
                if (temp != null) temp.delete();
            }
        }
    }

    public static String getLink()
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }
}
