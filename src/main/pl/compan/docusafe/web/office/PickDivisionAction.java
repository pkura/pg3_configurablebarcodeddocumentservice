package pl.compan.docusafe.web.office;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.*;
import std.pair;

/* User: Administrator, Date: 2005-07-11 11:32:33 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PickDivisionAction.java,v 1.5 2008/06/26 14:41:15 kubaw Exp $
 */
public class PickDivisionAction extends EventActionSupport
{
    private String guid;
    private String treeHtml;
    private String prettyPath;
    private String reference;
    private String constraints;

    private boolean canPick;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            try
            {
                DSDivision targetDivision;

                try
                {
                    if (guid != null)
                        targetDivision = DSDivision.find(guid);
                    else
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }
                catch (DivisionNotFoundException e)
                {
                    targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }

                canPick = constraints == null || constraints.indexOf(targetDivision.getDivisionType()) >= 0;

                prettyPath = targetDivision.getPrettyPath();

                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            String url = HttpUtils.makeUrl(
                                ServletActionContext.getRequest().getContextPath()+"/office/pick-division.action",
                                new Object[] {
                                    "guid", ((DSDivision) element).getGuid(),
                                    "reference", reference,
                                    "constraints", constraints
                                });
/*
                            String url =
                                    ServletActionContext.getRequest().getContextPath()+
                                     +
                                    "?guid="+((DSDivision) element).getGuid()+
                                    "&reference="+reference+
                                    "&constraints="+constraints;
*/

                            return url;
                        }
                        catch (Exception e)
                        {
                            event.getLog().error(e.getMessage(), e);
                            return null;
                        }
                    }
                };

                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true, true, false);

                treeHtml = tree.generateTree();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public String getPrettyPath()
    {
        return prettyPath;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getConstraints()
    {
        return constraints;
    }

    public void setConstraints(String constraints)
    {
        this.constraints = constraints;
    }

    public boolean isCanPick()
    {
        return canPick;
    }
}
