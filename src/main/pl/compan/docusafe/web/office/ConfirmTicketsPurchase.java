package pl.compan.docusafe.web.office;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.ProcessInstanceQuery;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ConfirmTicketsPurchase extends EventActionSupport {
	
	private static final long serialVersionUID = 1L;
	public static Logger log = LoggerFactory.getLogger(ConfirmTicketsPurchase.class);

	private List<Ticket> bilety;
	private Integer nrZestawienia;
	private boolean showGenerujButton;
	
	public class FillForm implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			try 
			{
				bilety = Ticket.list(nrZestawienia);
				if (bilety != null && bilety.size() > 0)
					showGenerujButton = true;
			} catch (SQLException e) {
				log.error(e.getMessage(),e);
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			} 
		}
	}

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		 registerListener(DEFAULT_ACTION)
		 	.append(OpenHibernateSession.INSTANCE)
		 	.appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doSearch")
		 	.append(OpenHibernateSession.INSTANCE)
		 	.append("FILL_FORM", fillForm)
		 	.appendFinally(CloseHibernateSession.INSTANCE);

		 registerListener("doConfirm")
		 	.append(OpenHibernateSession.INSTANCE)
		 	.append("FILL_FORM", fillForm)
		 	.append(null, new ConfirmZestawienie())
		 	.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	public class ConfirmZestawienie implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			log.info(" --- TicketsListAction.ConfirmZestawienie --- ");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				log.info("Lista bilet�w: " + bilety);
				DSApi.context().begin();
				for (Ticket wniosekDoPotwierdzenia : bilety) {
					log.info("wniosekDoPotwierdzenia.getDocumentId() = " + wniosekDoPotwierdzenia.getDocumentId());
					Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(wniosekDoPotwierdzenia.getDocumentId());
					for (String taskId : taskIds) {
						log.info("taskId: {}", taskId);
						String procInstance = Jbpm4Provider.getInstance().getExecutionService()
								.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
								.getProcessInstance().getId();
						log.info("procInstance: {}", procInstance);
						if (procInstance.startsWith("Bilety")) {
							Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(procInstance, "to accepted-author");
							JBPMTaskSnapshot.updateByDocumentId(wniosekDoPotwierdzenia.getDocumentId(), "anyType");
						}
					}
				}
				DSApi.context().commit();
				bilety = new ArrayList<Ticket>();
				nrZestawienia = null;
				showGenerujButton = false;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				DSApi.context().closeStatement(ps);
				DbUtils.closeQuietly(rs);
			}

		}

	}

	public List<Ticket> getBilety() {
		return bilety;
	}

	public void setBilety(List<Ticket> bilety) {
		this.bilety = bilety;
	}

	public Integer getNrZestawienia() {
		return nrZestawienia;
	}

	public void setNrZestawienia(Integer nrZestawienia) {
		this.nrZestawienia = nrZestawienia;
	}

	public boolean isShowGenerujButton()
	{
		return showGenerujButton;
	}

	public void setShowGenerujButton(boolean showGenerujButton)
	{
		this.showGenerujButton = showGenerujButton;
	}

}
