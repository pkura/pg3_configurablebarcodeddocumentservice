package pl.compan.docusafe.web.office;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.CaseNotFoundException;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeCaseFinderProvider;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.swing.text.StyledEditorKit.BoldAction;

import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FindCasesAction.java,v 1.42 2009/05/27 06:37:21 pecet3 Exp $
 */
public class FindCasesAction extends EventActionSupport
{
    private static final int LIMIT = 20;
    private int offset;
    StringManager sm = GlobalPreferences.loadPropertiesFile(FindCasesAction.class.getPackage().getName(), null);
   

	private String thisSearchUrl;

    // @EXPORT/@IMPORT
    private boolean popup;
    private boolean add;

    // @EXPORT
    private List<DSUser> users;
    private List<CaseStatus> statuses;
    private SearchResults<Map> results;
    private Pager pager;
    private Long id;
    private List<Tab> tabs = new ArrayList<Tab>(4);
    private Map<Integer, String> records = new LinkedHashMap<Integer, String>();

    // @IMPORT
    private String officeId;
    private String[] assignedUser;
    private String[] person;
    private Integer seqNum;
    private Integer year;
    private String cdateFrom;
    private String cdateTo;
    private String finishDateFrom;
    private String finishDateTo;
    private Integer[] statusId;
    private String sortField;
    private boolean ascending;
    private Integer[] record;
    private Integer rwaId;
    private String prettyRwaCategory;
    private int mode;
    private String title;

    private int przypisane = 0;
    private static final String EV_SEARCH = "search";
    private static final String EV_PRZYP = "showPrzypisane";

    protected void setup()
    {
        class SetResult implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                event.setResult(popup ? "popup" : SUCCESS);
            }
        }

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new Tabs()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new Tabs()).
            append(new ValidateSearch()).
            append(EV_SEARCH, new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        
        registerListener("doShowPzypisane").
        append(OpenHibernateSession.INSTANCE).
        append(new SetResult()).
        append(new Tabs()).
        append(new ValidateSearch()).
        append(EV_PRZYP, new showPrzypisane()).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
  

    private class Tabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(AvailabilityManager.isAvailable("wyszukiwarka.pismoPrzychodzace"))
        	{
        		tabs.add(new Tab("Pisma przychodz�ce", "Pisma przychodz�ce",
        				HttpUtils.makeUrl("/office/find-office-documents.action",
        						new String[] { "tab", FindOfficeDocumentsAction.TAB_IN }),
        						false));
        	}
            if (Configuration.officeAvailable() || Configuration.simpleOfficeAvailable())
            {
            	if(AvailabilityManager.isAvailable("wyszukiwarka.PismaWewnetrzne"))
            	{
            		tabs.add(new Tab("Pisma wewn�trzne", "Pisma wewn�trzne",
            				HttpUtils.makeUrl("/office/find-office-documents.action",
            						new String[] { "tab", FindOfficeDocumentsAction.TAB_INT }),
            						false));
            		if(AvailabilityManager.isAvailable("wyszukiwarka.PismaWychodzace"))
            		{	
            			tabs.add(new Tab("Pisma wychodz�ce", "Pisma wychodz�ce",
            					HttpUtils.makeUrl("/office/find-office-documents.action",
            							new String[] { "tab", FindOfficeDocumentsAction.TAB_OUT }),
            							false));
            		}
            	}
            }

            if (!Configuration.hasExtra("business") || AvailabilityManager.isAvailable("wyszukiwarka.sprawy"))
            {
                tabs.add(new Tab("Sprawy", "Sprawy",
                    HttpUtils.makeUrl("/office/find-cases.action", null),
                    true));
            }
            if (AvailabilityManager.isAvailable("wyszukiwarka.sprawy"))
            {
                tabs.add(new Tab("Teczki", "Teczki",
                    HttpUtils.makeUrl("/office/find-folders.action", null),
                    false));
            }
            if (!Configuration.hasExtra("business") && Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
            tabs.add(new Tab("Polecenia", "Polecenia",
                HttpUtils.makeUrl("/office/find-orders.action",null),
                false));

        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean userAll = false;
            try
            {
            	
                if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA) 
                		||DSApi.context().hasPermission(DSPermission.DODAWANIE_PISM_DO_SPRAW_W_DZIALE))
                {
                    // tylko z dzia�u
                    users = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    users = new ArrayList<DSUser>(1);
                    users.add(DSApi.context().getDSUser());
                }

               DSUser user = DSApi.context().getDSUser();
                 
                statuses = CaseStatus.list();
                records.put(new Integer(Records.GRANTS), "Stypendia");
                records.put(new Integer(Records.CONSTRUCTION), "Zezwolenia na budow�");
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateSearch implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date dCdateFrom = DateUtils.nullSafeParseJsDate(cdateFrom);
            Date dCdateTo = DateUtils.nullSafeParseJsDate(cdateTo);
            if (dCdateFrom != null && dCdateTo != null &&
                dCdateFrom.after(dCdateTo))
                addActionError(sm.getString("WYSZ_SPRAWY_DATA_KONC_TW_WCZ_NIZ_POCZAT"));

            if (hasActionErrors())
                event.skip(EV_SEARCH);
            

            dCdateFrom = DateUtils.nullSafeParseJsDate(finishDateFrom);
            dCdateTo = DateUtils.nullSafeParseJsDate(finishDateTo);
            if (dCdateFrom != null && dCdateTo != null &&
                    dCdateFrom.after(dCdateTo))
                    addActionError(sm.getString("WYSZ_SPRAWY_DATA_KONC_ZAK_SPR_WCZ_NIZ_POCZAT"));

                if (hasActionErrors())
                    event.skip(EV_SEARCH);
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	
        	przypisane = 0;
            // url bez sortowania
			// dodalem dalem person[0] zamiast person, 
			// teraz to dziala, ale pewnie nie do konca
			// tak jak trzeba. swiatly
            thisSearchUrl = "/office/find-cases.action?doSearch=true" +
                "&offset="+offset+
                (officeId != null ? "&officeId="+officeId : "") +
                (person != null ? "&person="+person[0] : "") +
                (seqNum != null ? "&seqNum="+seqNum : "") +
                (year != null ? "&year="+year : "") +
                (cdateFrom != null ? "&cdateFrom="+cdateFrom : "") +
                (cdateTo != null ? "&cdateTo="+cdateTo : "") +
                (finishDateFrom != null ? "&finishDateFrom="+finishDateFrom : "") +
                (finishDateTo != null ? "&finishDateTo="+finishDateTo : "") +
                (assignedUser != null ? "&assignedUser="+assignedUser : "") +
                (popup ? "&popup=true" : "") +
                (add ? "&add=true" : "") +
                (mode == 1 ? "&mode=1" : "") +
                (rwaId != null ? "&rwaId="+rwaId : "") +
                (statusId != null ? "&status="+statusId : "") +
                (record != null ? "&record="+record : "") + 
                (title != null ? "&title="+title: "");

            if (!StringUtils.isEmpty(officeId) && !popup && !AvailabilityManager.isAvailable("wyszukiwarkaSpraw.suzkaniez%albo_zwracaListeSprawANiePojedynczaSprawe"))
            {
            	try
            	{
            		OfficeCase c = OfficeCase.findByOfficeIdLike(officeId);
            		id = c.getId();
            		event.setResult("case");     	
            	}
            	catch (CaseNotFoundException e)
            	{
            		addActionError("Nie znaleziono sprawy o numerze "+officeId);
            	}
            	catch (EdmException e)
            	{
            		addActionError(e.getMessage());
            	}
            }
            else
            {
                try
                {
                    OfficeCase.Query query = OfficeCase.query(offset, LIMIT);
                    query.setArchiveStatusIsNotArchived();
                    query.setClerk(assignedUser);
                    query.setCdateFrom(DateUtils.nullSafeParseJsDate(cdateFrom));
                    query.setCdateTo(DateUtils.nullSafeParseJsDate(cdateTo));
                    query.setFinishDateFrom(DateUtils.nullSafeParseJsDate(finishDateFrom));
                    query.setFinishDateTo(DateUtils.nullSafeParseJsDate(finishDateTo));
                    query.setAuthor(person);
                    query.setSeqNum(seqNum);
                    query.setStatusId(statusId);
                    query.setYear(year);
                    query.setRecord(record);
                    query.setOfficeId(TextUtils.trimmedStringOrNull(officeId));
                    query.setRwaId(rwaId);
                    query.setTitle(title);
                    //sprawy nie bedace w archiwum
                    query.setArchiveStatus(0);
                    query.safeSortBy(sortField, "officeId", ascending);

                    lambda<OfficeCase, Map> mapper = new lambda<OfficeCase, Map>()
                    {
                        public Map act(OfficeCase officeCase)
                        {
                            BeanBackedMap result = new BeanBackedMap(officeCase,
                                "id", "finishDate", "openDate", "officeId", "status", "daysToFinish",
                                "description", "closed", "title");
                            try
                            {
                                DSApi.initializeProxy(officeCase.getStatus());
                                result.put("clerk", DSUser.safeToFirstnameLastname(officeCase.getClerk()));
                            }
                            catch (EdmException e)
                            {
                                event.getLog().warn(e.getMessage(), e);
                            }
                            return result;
                        }
                    };

                    ContainerFinder<OfficeCase, Map> provide;
                    try {
                        provide = OfficeCaseFinderProvider.instance().provide(query, mapper, Map.class);
                    } catch (Exception e) {
                        throw new EdmException("Wyst�pi� nieoczekiwany b��d, skontaktuj si� z administratorem", e);
                    }
                    results = provide.find();

                    if (results.totalCount() == 0)
                    	addActionError("Nie znaleziono �adnych spraw");
                    
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return HttpUtils.makeUrl("/office/find-cases.action",
                                new Object[] {
                            	"mode", Integer.toString(mode),
                                "doSearch", "true",
                                "officeId", officeId,
                                "assignedUser", assignedUser,
                                "person", person,
                                "seqNum", seqNum,
                                "year", year,
                                "statusId", statusId,
                                "cdateFrom", cdateFrom,
                                "cdateTo", cdateTo,
                                "finishDateFrom", finishDateFrom,
                                "finishDateTo", finishDateTo,
                                "sortField", sortField,
                                "rwaId",rwaId,
                                "title",title,
                                "ascending", String.valueOf(ascending),
                                "popup", popup ? "true" : "false",
                                "add", add ? "true" : "false",
                                "offset", String.valueOf(offset)});
                        }
                    };

                    pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    
    private class showPrzypisane implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	
            // url bez sortowania
			// dodalem dalem person[0] zamiast person, 
			// teraz to dziala, ale pewnie nie do konca
			// tak jak trzeba. swiatly
            thisSearchUrl = "/office/find-cases.action?doSearch=true" +
                "&offset="+offset+
                (officeId != null ? "&officeId="+officeId : "") +
                (person != null ? "&person="+person[0] : "") +
                (seqNum != null ? "&seqNum="+seqNum : "") +
                (year != null ? "&year="+year : "") +
                (cdateFrom != null ? "&cdateFrom="+cdateFrom : "") +
                (cdateTo != null ? "&cdateTo="+cdateTo : "") +
                (finishDateFrom != null ? "&finishDateFrom="+finishDateFrom : "") +
                (finishDateTo != null ? "&finishDateTo="+finishDateTo : "") +
                (assignedUser != null ? "&assignedUser="+assignedUser : "") +
                (popup ? "&popup=true" : "") +
                (add ? "&add=true" : "") +
                (mode == 1 ? "&mode=1" : "") +
                (rwaId != null ? "&rwaId="+rwaId : "") +
                (statusId != null ? "&status="+statusId : "") +
                (record != null ? "&record="+record : "")
                + przypisane
                ;

            //powodowa�o b��d 5997 - przy wyszukiwaniu spraw w wyszukiwarne wy�wietla si� pierwsze wyszukanie zamiast listy
//            if (!StringUtils.isEmpty(officeId) && !popup)
//            { 
//                try
//                {
//                    OfficeCase c = OfficeCase.findByOfficeIdLike(officeId);
//                    id = c.getId();
//                    event.setResult("case");
//                }
//                catch (CaseNotFoundException e)
//                {
//                    addActionError("Nie znaleziono sprawy o numerze "+officeId);
//                }
//                catch (EdmException e)
//                {
//                    addActionError(e.getMessage());
//                }
//            }
//            else
//            {
                try
                {
                	przypisane = 1;
                    OfficeCase.Query query2 = OfficeCase.query(offset, LIMIT);
                    query2.setClerk(assignedUser);
                    query2.setCdateFrom(DateUtils.nullSafeParseJsDate(cdateFrom));
                    query2.setCdateTo(DateUtils.nullSafeParseJsDate(cdateTo));
                    query2.setFinishDateFrom(DateUtils.nullSafeParseJsDate(finishDateFrom));
                    query2.setFinishDateTo(DateUtils.nullSafeParseJsDate(finishDateTo));
                    query2.setAuthor(person);
                    query2.setSeqNum(seqNum);
                    query2.setStatusId(statusId);
                    query2.setYear(year);
                    query2.setRecord(record);
                    if (officeId != null)
                    	officeId = officeId.trim()+"%";
                    query2.setOfficeId(TextUtils.trimmedStringOrNull(officeId));
                    query2.setRwaId(rwaId);
                    
                    query2.safeSortBy(sortField, "officeId", ascending);

                    lambda<OfficeCase, Map> mapper = new lambda<OfficeCase, Map>()
                    {
                        public Map act(OfficeCase officeCase)
                        {
                            BeanBackedMap result = new BeanBackedMap(officeCase,
                                "id", "finishDate", "openDate", "officeId", "status", "daysToFinish",
                                "description", "closed");
                            try
                            {
                                DSApi.initializeProxy(officeCase.getStatus());
                                result.put("clerk", DSUser.safeToFirstnameLastname(officeCase.getClerk()));
                            }
                            catch (EdmException e)
                            {
                                event.getLog().warn(e.getMessage(), e);
                            }
                            return result;
                        }
                    };
                    
                    results = OfficeCase.search2( query2, mapper, Map.class);       

                  //  results = OfficeCase.search(query, mapper, Map.class);

                  //  if (results.totalCount() == 0)
                 //   	addActionError("Nie znaleziono �adnych spraw");
                    
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return HttpUtils.makeUrl("/office/find-cases.action",
                                new Object[] {
                            	"mode", Integer.toString(mode),
                                "doSearch", "true",
                                "officeId", officeId,
                                "assignedUser", assignedUser,
                                "person", person,
                                "seqNum", seqNum,
                                "year", year,
                                "statusId", statusId,
                                "cdateFrom", cdateFrom,
                                "cdateTo", cdateTo,
                                "finishDateFrom", finishDateFrom,
                                "finishDateTo", finishDateTo,
                                "sortField", sortField,
                                "rwaId",rwaId,
                                "ascending", String.valueOf(ascending),
                                "popup", popup ? "true" : "false",
                                "add", add ? "true" : "false",
                                "offset", String.valueOf(offset)});
                        }
                    };

                    pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
//            }
        }
    }

    /**
     * Funkcja wywo�ywana ze strony JSP podczas wypisywania wynik�w wyszukiwania.
     * Funkcja jest wywo�ywana na tej samej instancji ActionSupport, kt�ra
     * wykonywa�a wyszukiwanie.
     */
    public String getCaseLink(Map caseBean)
    {   
        return EditCaseAction.getLink((Long) caseBean.get("id"), null, null, thisSearchUrl);
    }

    /**
     * Funkcja wywo�ywana ze strony JSP.
     */
    public String getSortLink(String field, boolean asc)
    {
        return thisSearchUrl + "&sortField="+field + "&ascending="+asc;
    }



    public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public List<CaseStatus> getStatuses()
    {
        return statuses;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public void setAssignedUser(String[] assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public void setPerson(String[] person)
    {
        this.person = person;
    }

    public void setSeqNum(Integer seqNum)
    {
        this.seqNum = seqNum;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public void setStatusId(Integer[] statusId)
    {
        this.statusId = statusId;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public boolean isPopup()
    {
        return popup;
    }

    public void setPopup(boolean popup)
    {
        this.popup = popup;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setCdateFrom(String cdateFrom)
    {
        this.cdateFrom = cdateFrom;
    }

    public void setCdateTo(String cdateTo)
    {
        this.cdateTo = cdateTo;
    }

    public void setOfficeId(String officeId)
    {
        this.officeId = officeId;
    }

    public Long getId()
    {
        return id;
    }

    public void setFinishDateFrom(String finishDateFrom)
    {
        this.finishDateFrom = finishDateFrom;
    }

    public void setFinishDateTo(String finishDateTo)
    {
        this.finishDateTo = finishDateTo;
    }

    public List getTabs()
    {
        return tabs;
    }

    public Map<Integer, String> getRecords()
    {
        return records;
    }

    public Integer[] getRecord()
    {
        return record;
    }

    public void setRecord(Integer[] record)
    {
        this.record = record;
    }
    
    public Integer getRwaId()
    {
        return rwaId;
    }
    
    public void setRwaId(Integer rwaId)
    {
        this.rwaId = rwaId;
    }
    
    public String getPrettyRwaCategory()
    {
        return prettyRwaCategory;
    }
    
    public void setPrettyRwaCategory(String prettyRwaCategory)
    {
        this.prettyRwaCategory = prettyRwaCategory;
    }

	public String getSortField() {
		return sortField;
	}

	public boolean isAscending() {
		return ascending;
	}
	
    public int getMode() {
		return mode;
	}

	public boolean isAdd() {;
		return add;
	}

	public void setAdd(boolean add) {
		if (add)
			mode = 1;
		else
			mode = 0;
		this.add = add;
	}

	public void setMode(int mode) {		
		this.mode = mode;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	 public int isPrzypisane() {
			return przypisane;
		}

		public void setPrzypisane(int przypisane) {
			this.przypisane = przypisane;
		}
}
