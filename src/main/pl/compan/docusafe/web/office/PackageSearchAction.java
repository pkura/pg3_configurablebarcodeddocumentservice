package pl.compan.docusafe.web.office;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.Package;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Prosta wyszukiwarka paczek.
 * @author r.powroznik
 */
public class PackageSearchAction  extends PackageBaseAction {

	private static final Log log = LogFactory.getLog(PackageSearchAction.class);
	
	/**
	 *  Lista użytkowników.
	 */
	private List<DSUser> users;
	
	/**
	 * Numer paczki do wyszukania.
	 */
	private String packageNb;
	
	/**
	 * Data wyszukiwania OD
	 */
	private String packageDateFrom;
	
	/**
	 * Data wyszukiwania DO
	 */
	private String packageDateTo;
	
	/**
	 * Uzytkownik tworzacy paczke.
	 */
	private String creatingUser;
	
	/**
	 * Lista paczek spelniajacych kryteria wyszukania.
	 */
	private List<Package> packages;

	protected void setup()
	{		
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try	{        		
        		packages = Package.find(packageNb, creatingUser, packageDateFrom, packageDateTo);        		
        		if (packages == null || packages.size() == 0)
        			addActionMessage("Nie znaleziono paczek spełniających podane kryteria.");
        	} catch (Exception ex) {
        		addActionError(ex.getMessage());
                log.error("", ex);
			}
        }        
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
        		users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
        	} catch (Exception e) {
        		addActionError(e.getMessage());
                log.error("", e);
			}
        }        
    }

	public String getPackageNb() {
		return packageNb;
	}

	public void setPackageNb(String packageNb) {
		this.packageNb = packageNb;
	}

	public String getPackageDateFrom() {
		return packageDateFrom;
	}

	public void setPackageDateFrom(String packageDateFrom) {
		this.packageDateFrom = packageDateFrom;
	}

	public String getPackageDateTo() {
		return packageDateTo;
	}

	public void setPackageDateTo(String packageDateTo) {
		this.packageDateTo = packageDateTo;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public String getCreatingUser() {
		return creatingUser;
	}

	public void setCreatingUser(String creatingUser) {
		this.creatingUser = creatingUser;
	}

	public List<Package> getPackages() {
		return packages;
	}	

}
