package pl.compan.docusafe.web.office;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
//import java.net.URLDecoder;
//import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FindOfficeDocumentsAction.java,v 1.104 2010/08/04 06:59:42 mariuszk Exp $
 */
public class FindOfficeDocumentsAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(FindOfficeDocumentsAction.class);
    private static final int LIMIT = 10;

    public static final String TAB_IN = "in";
    public static final String TAB_OUT = "out";
    public static final String TAB_INT = "int";
    public static final String TAB_CASES = "cases";
    public static final String TAB_FOLDERS = "folders";
    public static final String TAB_ORDERS= "orders";
    private String thisSearchUrl;

    // @EXPORT
    private List<InOfficeDocumentKind> kinds;
    private List<OutOfficeDocumentDelivery> outDeliveries;
    private List<InOfficeDocumentDelivery> inDeliveries;
    private List<DSUser> users;
    private SearchResults<Map> results;
    private List<Tab> tabs = new ArrayList<Tab>(7);
    private Pager pager;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Map<Long, String> journals;
    private List<InOfficeDocumentStatus> incomingStatuses;
    private List<DSUser> accessedByUsers;
    private Map<String, String> sources;
    private String attachmentsAsPdfUrl;
    private List<Flags.FlagBean> flags;
    private List<Flags.FlagBean> userFlags;

    private Long[] flagIds;
    private Long[] userFlagIds;

    // @IMPORT/@EXPORT
    private String tab;
    private boolean internal;
    private Map <Integer, String> yearsList;
    private Integer selectedYear;

    // @IMPORT
    private Integer officeNumber;
    private Integer officeNumberYear;
    private String documentDateFrom;
    private String documentDateTo;
    private String zpoDateFrom;
    private String zpoDateTo;
    private String postalRegNumber;
    private String stampDateFrom;
    private String stampDateTo;
    private String incomingDateFrom;
    private String incomingDateTo;
    private String recipient;
    private String sender;
    private Integer kindId;
    private String creatingUser;
    private String assignedUser;
    private String barcode;
    private String referenceId;
    private String sortField;
    private boolean ascending;
    private Integer deliveryId;
    private Integer statusId;
    private String summary;
    private Integer fromOfficeNumber;
    private Integer toOfficeNumber;
    //private Integer officeNumberYear2;
    private Long journalId;
    private String accessedBy;
    private String[] accessedAs;
    private String accessedFrom;
    private String accessedTo;
    private String source;
    private boolean attachmentsAsPdf;
    private String flagsDateFrom;
    private String flagsDateTo;
    private String userFlagsDateFrom;
    private String userFlagsDateTo;
    private String caseDocumentId;
    private Integer limit;
    private boolean generateXls;
    private boolean flagsOn;
    
    /** Kopia czy oryginal dokumentu. */
    private Boolean originalDocument;

    private int offset;

    private static final String EV_SEARCH_OUT = "searchout";
    private static final String EV_SEARCH_IN = "searchin";
    private static final String EV_FILL = "fill";

    private StringManager smL;


    protected void setup()
    {
        registerSearchParameters(new String[] {

        });

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Tabs()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearchIn").
            append(OpenHibernateSession.INSTANCE).
            append(new Tabs()).
            append(new ValidateSearchIn()).
            append(EV_SEARCH_IN, new SearchIn()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearchOut").
            append(OpenHibernateSession.INSTANCE).
            append(new Tabs()).
            append(new ValidateSearchOut()).
            append(EV_SEARCH_OUT, new SearchOut()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doClearFields").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Tabs()).
	        append(new ClearFields()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }//setup

    private class ClearFields implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	officeNumber = null;
        	officeNumberYear = null;
        	fromOfficeNumber = null;
        	toOfficeNumber = null;
        	stampDateFrom = null;
        	stampDateTo = null;
        	documentDateFrom = null;
        	documentDateTo = null;
        	incomingDateFrom = null;
        	incomingDateTo = null;
        	summary = null;
        	referenceId = null;
        	caseDocumentId = null;
        	sender = null;
        	recipient = null;
        	postalRegNumber = null;
        	selectedYear = null;
        	journalId = null;
        	statusId = null;
        	deliveryId = null;
        	kindId = null;
        	creatingUser = null;
        	assignedUser = null;
        	source = null;
        }//actionPerformed
    }//class ClearFields
    
    
    private class Tabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

            if (!TAB_IN.equals(tab) && !TAB_OUT.equals(tab) && !TAB_INT.equals(tab)&& !TAB_ORDERS.equals(tab) && !TAB_FOLDERS.equals(tab)  && !TAB_CASES.equals(tab)
            		&&AvailabilityManager.isAvailable("wyszukiwarka.PismaWewnetrzne") && AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
            	tab = TAB_IN;
            if(AvailabilityManager.isAvailable("wyszukiwarka.sprawy")&&!TAB_IN.equals(tab)&& !TAB_OUT.equals(tab) && !TAB_INT.equals(tab)&& !TAB_ORDERS.equals(tab) && !TAB_FOLDERS.equals(tab)){
            	tab=TAB_CASES;
            }
            if(AvailabilityManager.isAvailable("wyszukiwarka.pismoPrzychodzace"))
            {
            	tabs.add(new Tab(smL.getString("PismaPrzychodzace"), smL.getString("PismaPrzychodzace"),
            			HttpUtils.makeUrl("/office/find-office-documents.action",
            					new String[] { "tab", TAB_IN }),
            					TAB_IN.equals(tab)));
            }
            if (Configuration.officeAvailable() || Configuration.simpleOfficeAvailable())
            {
            	if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne") && AvailabilityManager.isAvailable("wyszukiwarka.PismaWewnetrzne"))
            	{
	                tabs.add(new Tab(smL.getString("PismaWewnetrzne"), smL.getString("PismaWewnetrzne"),
	                    HttpUtils.makeUrl("/office/find-office-documents.action",
	                        new String[] { "tab", TAB_INT }),
	                    TAB_INT.equals(tab)));
            	}
            	if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace") && AvailabilityManager.isAvailable("wyszukiwarka.PismaWychodzace"))
            	{
	                tabs.add(new Tab(smL.getString("PismaWychodzace"), smL.getString("PismaWychodzace"),
	                    HttpUtils.makeUrl("/office/find-office-documents.action",
	                        new String[] { "tab", TAB_OUT }),
	                    TAB_OUT.equals(tab)));
            	}
            }

            if (!Configuration.hasExtra("business") || AvailabilityManager.isAvailable("wyszukiwarka.sprawy"))
            {
                tabs.add(new Tab(smL.getString("Sprawy"), smL.getString("Sprawy"),
                    HttpUtils.makeUrl("/office/find-office-documents.action", new String[] { "tab", TAB_CASES }),
                    TAB_CASES.equals(tab)));
            }
            if (!Configuration.hasExtra("business") && Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
            tabs.add(new Tab(smL.getString("Polecenia"), smL.getString("Polecenia"),
                HttpUtils.makeUrl("/office/find-office-documents.action", new String[] { "tab", TAB_ORDERS }),
                TAB_ORDERS.equals(tab)));
            if(AvailabilityManager.isAvailable("wyszukiwarka.sprawy")){
            	tabs.add(new Tab(smL.getString("Teczki"), smL.getString("Teczki"),
            			HttpUtils.makeUrl("/office/find-office-documents.action", new String[] { "tab", TAB_FOLDERS }),
            			TAB_FOLDERS.equals(tab)));
            	
            }


            event.setResult(tab);
        }//actionPerformed
    }//class Tabs

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                kinds = InOfficeDocumentKind.list();
                outDeliveries = OutOfficeDocumentDelivery.list();
                inDeliveries = InOfficeDocumentDelivery.list();
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                useBarcodes = Boolean.valueOf(GlobalPreferences.isUseBarcodes());
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
                incomingStatuses = InOfficeDocumentStatus.list();
                internal = TAB_INT.equals(tab);

                if (!Configuration.faxAvailable() && StringUtils.isEmpty(thisSearchUrl))
                	originalDocument = true;

                List<Journal> docJournals = new ArrayList<Journal>();
            	String journalType = "";
            	
                if(tab.equals(TAB_INT))
        			journalType = Journal.INTERNAL;
        		if(tab.equals(TAB_OUT))
        			journalType = Journal.OUTGOING;
        		if(tab.equals(TAB_IN))
        			journalType = Journal.INCOMING;
        		
            	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
                {
            		docJournals = Journal.findByType(journalType);
            		docJournals.addAll(Journal.findByType(journalType, true));
                }
            	else
            	{
                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                		docJournals = Journal.findByDivisionGuid(division.getGuid(),journalType);
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),journalType,true));
                    }
                }
                if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
                	if(tab.equals(TAB_INT))
                		docJournals.add(Journal.getMainInternal());
            		if(tab.equals(TAB_OUT))
            			docJournals.add(Journal.getMainOutgoing());
            		if(tab.equals(TAB_IN))
            			docJournals.add(Journal.getMainIncoming());
                	 
                }
                journals = new LinkedHashMap<Long, String>(docJournals.size());
                
                yearsList = new TreeMap<Integer, String> ();                
                yearsList.put(-1, "-- wszystkie --");
                
                if (selectedYear == null)
                    selectedYear = -1;                
                
                int year;
                
                for (Journal journal:docJournals )
                {
                    try
                    {   year = journal.getCyear();
                        yearsList.put(year, Integer.toString(year));
                        if (selectedYear != -1 & journal.getCyear() != selectedYear) {
                            continue;
                        }
                        journals.put(journal.getId(),
                            (journal.isClosed() ? "("+year+") " : "") +
                            (journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
                            journal.getDescription());
                    }
                    catch (DivisionNotFoundException e)
                    {
                        journals.put(journal.getId(), "[" + smL.getString("nieznanyDzial") + "] - "+journal.getDescription());
                    }
                }

                if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    accessedByUsers = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA))
                {
                    // tylko z działu
                    accessedByUsers = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    accessedByUsers = new ArrayList<DSUser>(1);
                    accessedByUsers.add(DSApi.context().getDSUser());
                }

                sources = new LinkedHashMap<String, String>(1);
                sources.put("fax", smL.getString("Faks"));

                flagsOn = "true".equals(Configuration.getProperty("flags"));

                if (flagsOn)
                {
                    flags = Flags.listFlags(false);
                    userFlags = Flags.listFlags(true);
                }
                
                if(generateXls)
                    GenerateXLS(event);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }//actionPerformed
    }//class FillForm

    private void GenerateXLS(ActionEvent event)
    {
        Map[] values = results.results();

        smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Docusafe Software");

        if(results.count()==0)
            return;

        int rowNumber = 1;

        HSSFRow headerRow =  sheet.createRow(0);

        short headerNumber = 0;

        HSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setBottomBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setLeftBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setRightBorderColor(HSSFColor.RED.index);

        headerStyle.setTopBorderColor(HSSFColor.RED.index);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);


        HSSFCell headercell;

        headercell= headerRow.createCell(headerNumber++);
        headercell.setCellStyle(headerStyle);
        headercell.setCellValue("Id");

        headercell= headerRow.createCell(headerNumber++);
        headercell.setCellStyle(headerStyle);
        headercell.setCellValue(smL.getString("NumerKO"));

        headercell= headerRow.createCell(headerNumber++);
        headercell.setCellStyle(headerStyle);
        if(tab.equals(TAB_IN))
            headercell.setCellValue(smL.getString("Data"));
        else
            headercell.setCellValue(smL.getString("DataWysylki"));

        headercell= headerRow.createCell(headerNumber++);
        headercell.setCellStyle(headerStyle);
        headercell.setCellValue(smL.getString("Nadawca"));

        headercell= headerRow.createCell(headerNumber++);
        headercell.setCellStyle(headerStyle);
        headercell.setCellValue(smL.getString("Tytul/stanowisko"));



        String [] setIn = {"id","officeNumber","incomingDate", "sender", "title"};
        String [] setOut = {"id","officeNumber","documentDate", "sender", "title"};

        String[] set;

        if(tab.equals(TAB_IN))
        {
            set = setIn;
        }
        else
        {
            set = setOut;
        }


        String content;

        for (int i=0;i<values.length;i++)
        {
            HSSFRow row =  sheet.createRow(rowNumber++);

            int cellNumber = 0;
            for (int j=0;j<set.length;j++)
            {
                HSSFCell cell = row.createCell((short)cellNumber++);
                if (values[i].get(set[j])!=null)
                {
                    if(j!=3)
                        content = values[i].get(set[j]).toString();
                    else
                        content = ((Sender)values[i].get(set[j])).getShortSummary();
                }
                else content = "";
                cell.setCellValue(content==null?"":content);
                HSSFCellStyle style = workbook.createCellStyle();
                cell.setCellStyle(style);
            }
        }
        for (int i=0;i<30;i++)
        {
            try
            {
                sheet.autoSizeColumn((short)i);
            }
            catch (Exception exc) 
            {
                event.getLog().error("Blad w metodzie sheet.autoSizeColumn", exc);
            }
        }

        try
        {
            File  tmpfile = File.createTempFile("DocuSafe", "tmp");
            tmpfile.deleteOnExit();
            FileOutputStream fis = new FileOutputStream(tmpfile);
            workbook.write(fis);
            fis.close();
            ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
        }
        catch(IOException ioe)
        {
            event.addActionError(ioe.getMessage());
            LogFactory.getLog("eprint").debug("", ioe);
        }
        event.setResult(NONE);
    }//generateXLS

    private class ValidateSearchIn implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date docDateFrom = DateUtils.nullSafeParseJsDate(documentDateFrom);
            Date docDateTo = DateUtils.nullSafeParseJsDate(documentDateTo);
            Date stampFrom = DateUtils.nullSafeParseJsDate(stampDateFrom);
            Date stampTo = DateUtils.nullSafeParseJsDate(stampDateTo);
            Date incomingFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            Date incomingTo = DateUtils.nullSafeParseJsDate(incomingDateTo);

            if (docDateFrom != null && docDateTo != null &&
                docDateFrom.after(docDateTo))
                addActionError(smL.getString("PoczatkowaDataPismaJestPozniejszaOdDatyKońcowej"));

            if (stampFrom != null && stampTo != null &&
                stampFrom.after(stampTo))
                addActionError(smL.getString("PoczatkowaDataNadaniaJestPozniejszaOdDatyKoncowej"));

            if (incomingFrom != null && incomingTo != null &&
                incomingFrom.after(incomingTo))
                addActionError(smL.getString("PoczatkowaDataPrzyjeciaJestPozniejszaOdDatyKoncowej"));

            if ((fromOfficeNumber == null || toOfficeNumber == null) &&
                !(fromOfficeNumber == null && toOfficeNumber == null))
                addActionError(smL.getString("PodanoTylkoJedenZZakresuNumerowKO"));

            if (hasActionErrors())
                event.skip(EV_SEARCH_IN);
        }
    }//class ValidateSearchIn

    private class SearchIn implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            //tab=TAB_IN;
            if(generateXls)
                limit =  50000;//to trzeba zmienic
        	if(limit == null || limit < 1)
        		limit = LIMIT;
            InOfficeDocument.Query query;
            query = InOfficeDocument.query(offset, limit);

            if (officeNumberYear != null)
            {
                query.setOfficeNumberYear(officeNumberYear);
            }
            else
            {
                try
                {
                    if (officeNumber != null)
                        query.setOfficeNumberYear(new Integer(GlobalPreferences.getCurrentYear()));
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
            }

            if (officeNumber != null)
            {
                query.setOfficeNumber(officeNumber);
            }
            else if (fromOfficeNumber != null || toOfficeNumber != null)
            {
                query.setFromOfficeNumber(fromOfficeNumber);
                query.setToOfficeNumber(toOfficeNumber);
            }
            
            query.setOriginalDocument(originalDocument == null ? false : true);
            query.setClerk(TextUtils.trimmedStringOrNull(assignedUser));
            query.setStampDateFrom(DateUtils.nullSafeParseJsDate(stampDateFrom));
            query.setStampDateTo(DateUtils.nullSafeParseJsDate(stampDateTo));
            query.setDocumentDateFrom(DateUtils.nullSafeParseJsDate(documentDateFrom));
            query.setDocumentDateTo(DateUtils.nullSafeParseJsDate(documentDateTo));
            query.setIncomingDateFrom(DateUtils.nullSafeParseJsDate(incomingDateFrom));
            query.setIncomingDateTo(DateUtils.nullSafeParseJsDate(incomingDateTo));
            query.setKindId(kindId);
            query.setDeliveryId(deliveryId);
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            
            if(recipient != null)
            {
//            	wykomentowane dekodowania i kodowania
//            	solution: dodać do tomcat connector: URIEncoding="ISO-8859-2"
//            	recipient = URLDecoder.decode(recipient);
            	query.setRecipient(TextUtils.trimmedStringOrNull(recipient));
//            	recipient = URLEncoder.encode(recipient);
            }
            
            if(sender != null)
            {
//            	sender = URLDecoder.decode(sender);
            	query.setSender(TextUtils.trimmedStringOrNull(sender));
//            	sender = URLEncoder.encode(sender);
            }

            query.setBarcode(TextUtils.trimmedStringOrNull(barcode));
            query.setReferenceId(TextUtils.trimmedStringOrNull(referenceId));
            query.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
            
            if(summary != null)
            {
//            	summary = URLDecoder.decode(summary);
            	query.setSummary(TextUtils.trimmedStringOrNull(summary));
//            	summary = URLEncoder.encode(summary);
            }
            query.setJournalId(journalId);
            query.setStatusId(statusId);
            query.setAccessedBy(TextUtils.trimmedStringOrNull(accessedBy));
            query.setAccessedAs(accessedAs);
            /* jak wybrany tylko zakres dolny, to zakresem gornym tez bedzie zakres dolny */
            Date accFrom = DateUtils.nullSafeParseJsDate(accessedFrom);
            Date accTo = DateUtils.nullSafeParseJsDate(accessedTo);
            query.setAccessedFrom(DateUtils.nullSafeMidnight(accFrom, 0));
            query.setAccessedTo(DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
            query.setSource(TextUtils.trimmedStringOrNull(source));
            query.setCaseDocumentId(caseDocumentId);

            flagsOn = "true".equals(Configuration.getProperty("flags"));

            if (flagsOn)
            {
                List<Long> flags = new ArrayList<Long>();
                /*for (int i=1; i < flagIds.length; i++)
                {
                    if (Boolean.valueOf(flagIds[i]).booleanValue())
                        flags.add(new Integer(i));
                }*/
                if (flagIds!=null)
                    query.setFlags(Arrays.asList(flagIds),
                        DateUtils.nullSafeParseJsDate(flagsDateFrom),
                        DateUtils.nullSafeParseJsDate(flagsDateTo));

                List<Long> userFlags = new ArrayList<Long>();
                /*for (int i=1; i < userFlagIds.length; i++)
                {
                    if (Boolean.valueOf(userFlagIds[i]).booleanValue())
                        userFlags.add(new Integer(i));
                }*/
                if (userFlagIds!=null)
                    query.setUserFlags(Arrays.asList(userFlagIds),
                        DateUtils.nullSafeParseJsDate(userFlagsDateFrom),
                        DateUtils.nullSafeParseJsDate(userFlagsDateTo));
            }
            query.safeSortBy(sortField, "officeNumber", ascending);

            // generowanie treści załączników jako PDF
            if (attachmentsAsPdf)
            {
                File tmp = null;
                try
                {
                    DSApi.context().begin();

                    SearchResults<InOfficeDocument> pdfResults = InOfficeDocument.search(query);

                    InOfficeDocument[] documents = pdfResults.results();
                    tmp = File.createTempFile("docusafe_", ".pdf");
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                    PdfUtils.attachmentsAsPdf(documents, false, os);
                    os.close();
                }
                catch (Exception e)
                {
                    event.getLog().error(e.getMessage(), e);
                }
                finally
                {
                    DSApi.context().setRollbackOnly();
                }

                if (tmp != null)
                {
                    event.setResult(NONE);
                    event.skip(EV_FILL);
                    try
                    {
                        ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/pdf");
                    }
                    catch (IOException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                    finally
                    {
                        tmp.delete();
                    }
                }

                return;
            }

            try
            {
                lambda<InOfficeDocument, Map> mapper =
                    new lambda<InOfficeDocument, Map>()
                    {
                        public Map act(InOfficeDocument doc)
                        {
                            BeanBackedMap result = new BeanBackedMap(doc,
                                "id", "referenceId",
                                "incomingDate", "sender", "recipients", "officeNumber",
                                "formattedOfficeNumber", "title", "doctype");
                            try
                            {
                                DSApi.initializeProxy(doc.getRecipients());
                                if (flagsOn)
                                {
                                    result.put("setFlags", LabelsManager.getReadFlagBeansForDocument(doc.getId(), DSApi.context().getPrincipalName(), false));
                                    result.put("setUserFlags", LabelsManager.getReadFlagBeansForDocument(doc.getId(), DSApi.context().getPrincipalName(), true));
                                }
                            }
                            catch (EdmException e)
                            {
                            }
                            return result;
                        }
                    };

                results = InOfficeDocument.search(query, mapper, Map.class);
             
/*
                // stworzenie tablic dodatkowych własności zwracanych wyników;
                // każdy obiekt InOfficeDocument, jaki zostanie wyświetlony
                // na stronie wyników, zostanie "udekorowany" przy pomocy klasy
                // BeanEnhancer dodatkowymi własnościami: setFlags i setUserFlags

                // trzeba to robić tutaj, ponieważ pobieranie tych własności
                // wymaga istniejącej sesji Hibernate, a podczas wyświetlania JSP
                // sesja już nie istnieje
                final Map mappings = new HashMap();

                Object[] objects = results.results();
                for (int i=0; i < objects.length; i++)
                {
                    Document doc = (Document) objects[i];
                    Map extras = new HashMap();
                    extras.put("setFlags", doc.getFlags().getSetFlags());
                    extras.put("setUserFlags", doc.getUserFlags().getSetFlags());
                    mappings.put("#"+doc.getId(), extras);
                }

                results.setMapper(new SearchResults.Mapper()
                {
                    public Object map(Object o)
                    {
                        Map extras = (Map) mappings.get("#"+((Document) o).getId());
                        BeanEnhancer be = new BeanEnhancer(o, extras);
                        return be.create();
                    }
                });

                for (int i=0; i < objects.length; i++)
                {
                    DSApi.initializeProxy(((InOfficeDocument) objects[i]).getRecipients());
                }
*/

                if (results.count() == 0)
                    addActionMessage(smL.getString("NieZnalezionoPism"));
                
                thisSearchUrl =
                    HttpUtils.makeUrl("/office/find-office-documents.action",
                    new Object[] {
                    "doSearchIn", "true",
                    "officeNumber", officeNumber,
                    "officeNumberYear", officeNumberYear,
                    "fromOfficeNumber", fromOfficeNumber,
                    "toOfficeNumber", toOfficeNumber,
                    //"officeNumberYear2", officeNumberYear2,
                    "assignedUser", assignedUser,
                    "documentDateFrom", documentDateFrom,
                    "documentDateTo", documentDateTo,
                    "incomingDateFrom", incomingDateFrom,
                    "incomingDateTo", incomingDateTo,
                    "kindId", kindId,
                    "summary", summary,
                    "creatingUser", creatingUser,
                    "recipient", recipient,
                    "sender", sender,
                    "stampDateFrom", stampDateFrom,
                    "stampDateTo", stampDateTo,
                    "barcode", barcode,
                    "referenceId", referenceId,
                    "fromOfficeNumber", fromOfficeNumber,
                    "toOfficeNumber", toOfficeNumber,
                    "journalId", journalId,
                    "statusId", statusId,
                    "accessedBy", accessedBy,
                    "accessedAs", accessedAs,
                    "accessedFrom", accessedFrom,
                    "accessedTo", accessedTo,
                    "flagsDateTo", flagsDateTo,
                    "flagsDateFrom", flagsDateFrom,
                    "userFlagsDateTo", userFlagsDateTo,
                    "userFlagsDateFrom", userFlagsDateFrom,
                    "caseDocumentId",caseDocumentId,
                    "originalDocument",originalDocument,
                    "flagIds",flagIds,
                    "userFlagIds",userFlagIds,
                    "limit",String.valueOf(limit),
                    "offset", String.valueOf(offset)});
                String tmp = "";
                if (flagsOn)
                {
                	if(flagIds!=null){
	                	for (int i=1; i < flagIds.length; i++)
	                    {
	                        tmp = tmp+"&flagIds="+flagIds[i];
	                    }
                	}
                	if(userFlagIds!=null)
                	{
	                    for (int i=1; i < userFlagIds.length; i++)
	                    {
	                    	tmp = tmp+"&userFlagIds="+userFlagIds[i];
	                    }
                	}
                }
                thisSearchUrl = thisSearchUrl + tmp;
                final String tmp2 = tmp;

                attachmentsAsPdfUrl = thisSearchUrl + "&attachmentsAsPdf=true";

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        String url = HttpUtils.makeUrl("/office/find-office-documents.action",
                            new Object[] {
                            "doSearchIn", "true",
                            "officeNumber", officeNumber,
                            "officeNumberYear", officeNumberYear,
                            "fromOfficeNumber", fromOfficeNumber,
                            "toOfficeNumber", toOfficeNumber,
                            //"officeNumberYear2", officeNumberYear2,
                            "assignedUser", assignedUser,
                            "documentDateFrom", documentDateFrom,
                            "documentDateTo", documentDateTo,
                            "incomingDateFrom", incomingDateFrom,
                            "incomingDateTo", incomingDateTo,
                            "kindId", kindId,
                            "summary", summary,
                            "creatingUser", creatingUser,
                            "recipient", recipient,
                            "sender", sender,
                            "stampDateFrom", stampDateFrom,
                            "stampDateTo", stampDateTo,
                            "barcode", barcode,
                            "referenceId", referenceId,
                            "journalId", journalId,
                            "statusId", statusId,
                            "accessedBy", accessedBy,
                            "accessedAs", accessedAs,
                            "accessedFrom", accessedFrom,
                            "accessedTo", accessedTo,
                            "flagsDateTo", flagsDateTo,
                            "flagsDateFrom", flagsDateFrom,
                            "userFlagsDateTo", userFlagsDateTo,
                            "userFlagsDateFrom", userFlagsDateFrom,
                            "caseDocumentId",caseDocumentId,
                            "originalDocument",originalDocument,
                            "flagIds",flagIds,
                            "userFlagIds",userFlagIds,
                            "ascending", String.valueOf(ascending),
                            "offset", String.valueOf(offset),
                            "limit",String.valueOf(limit)}); 
                        return url + tmp2;
                    }
                };
                pager = new Pager(linkVisitor, offset, limit, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
            	
                addActionError(e.getMessage());
            }
        }//actionPerformed
    }//class SearchIn

    private class ValidateSearchOut implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date docDateFrom = DateUtils.nullSafeParseJsDate(documentDateFrom);
            Date docDateTo = DateUtils.nullSafeParseJsDate(documentDateTo);
            Date zpoFrom = DateUtils.nullSafeParseJsDate(zpoDateFrom);
            Date zpoTo = DateUtils.nullSafeParseJsDate(zpoDateTo);

            if (docDateFrom != null && docDateTo != null &&
                docDateFrom.after(docDateTo))
                addActionError(smL.getString("PoczatkowaDataPismaJestPozniejszaOdDatyKońcowej"));

            if (!internal)
            {
                if (zpoFrom != null && zpoTo != null &&
                    zpoFrom.after(zpoTo))
                    addActionError(smL.getString("PoczatkowaDataZPOJestPozniejszaOdDatyKoncowej"));
            }

            if (hasActionErrors())
                event.skip(EV_SEARCH_OUT);
        }//actionPerformed
    }//class ValidateSearchOut

    private class SearchOut implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            OutOfficeDocument.Query query;
            if(limit == null || limit < 1)
        		limit = LIMIT;
            if(generateXls)
                query = OutOfficeDocument.query(offset, 50000);//to trzeba zmienic
            else
                query = OutOfficeDocument.query(offset, limit);
            
            if (officeNumberYear != null)
            {
                query.setOfficeNumberYear(officeNumberYear);
            }
            else
            {
                try
                {
                    if (officeNumber != null)
                        query.setOfficeNumberYear(new Integer(GlobalPreferences.getCurrentYear()));
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
            }

            if (officeNumber != null)
            {
                query.setOfficeNumber(officeNumber);
            }
            else
            {
                query.setFromOfficeNumber(fromOfficeNumber);
                query.setToOfficeNumber(toOfficeNumber);
            }
            query.setClerk(TextUtils.trimmedStringOrNull(assignedUser));
            query.setDocumentDateFrom(DateUtils.nullSafeParseJsDate(documentDateFrom));
            query.setDocumentDateTo(DateUtils.nullSafeParseJsDate(documentDateTo));
            query.setZpoDateFrom(DateUtils.nullSafeParseJsDate(zpoDateFrom));
            query.setZpoDateTo(DateUtils.nullSafeParseJsDate(zpoDateTo));
            query.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
            query.setDeliveryId(deliveryId);
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            query.setSender(TextUtils.trimmedStringOrNull(sender));
            query.setRecipient(TextUtils.trimmedStringOrNull(recipient));
            query.setBarcode(TextUtils.trimmedStringOrNull(barcode));
            query.setJournalId(journalId);
            query.setSummary(TextUtils.trimmedStringOrNull(summary));
            query.setAccessedBy(TextUtils.trimmedStringOrNull(accessedBy));
            query.setAccessedAs(accessedAs);
            query.setReferenceId(TextUtils.trimmedStringOrNull(referenceId));

            /* jak wybrany tylko zakres dolny, to zakresem gornym tez bedzie zakres dolny */
            Date accFrom = DateUtils.nullSafeParseJsDate(accessedFrom);
            Date accTo = DateUtils.nullSafeParseJsDate(accessedTo);
            query.setAccessedFrom(DateUtils.nullSafeMidnight(accFrom, 0));
            query.setAccessedTo(DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));

            flagsOn = "true".equals(Configuration.getProperty("flags"));

            if (internal && flagsOn)
            {
                List<Long> flags = new ArrayList<Long>();
                /*for (int i=1; i < flagIds.length; i++)
                {
                        flags.add(flagIds[i]);
                }*/
                if (flagIds!=null)
                    query.setFlags(Arrays.asList(flagIds),
                        DateUtils.nullSafeParseJsDate(flagsDateFrom),
                        DateUtils.nullSafeParseJsDate(flagsDateTo));

                List<Long> userFlags = new ArrayList<Long>();
                /*for (int i=1; i < userFlagIds.length; i++)
                {
                    userFlags.add(userFlagIds[i]);
                }*/
                if (userFlagIds!=null)
                    query.setUserFlags(Arrays.asList(userFlagIds),
                        DateUtils.nullSafeParseJsDate(userFlagsDateFrom),
                        DateUtils.nullSafeParseJsDate(userFlagsDateTo));
            }

            query.setInternal(internal);
            query.safeSortBy(sortField, "officeNumber", ascending);
            if (attachmentsAsPdf)
            {
                File tmp = null;
                try
                {
                    DSApi.context().begin();

                    SearchResults<OutOfficeDocument> pdfResults = OutOfficeDocument.search(query);

                    OutOfficeDocument[] documents = pdfResults.results();
                    tmp = File.createTempFile("docusafe_", ".pdf");
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
                    PdfUtils.attachmentsAsPdf(documents, false, os);
                    os.close();
                }
                catch (Exception e)
                {
                    event.getLog().error(e.getMessage(), e);
                }
                finally
                {
                    DSApi.context().setRollbackOnly();
                }

                if (tmp != null)
                {
                    event.setResult(NONE);
                    event.skip(EV_FILL);
                    try
                    {
                        ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/pdf");
                    }
                    catch (IOException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                    finally
                    {
                        tmp.delete();
                    }
                }

                return;
            }

            try
            {
                lambda<OutOfficeDocument, Map> mapper =
                    new lambda<OutOfficeDocument, Map>()
                    {
                        public Map act(OutOfficeDocument doc) 
                        {
                            BeanBackedMap result = new BeanBackedMap(doc,
                                "id", "referenceId",
                                "documentDate", "sender", "recipients", "officeNumber",
                                "formattedOfficeNumber", "title", "doctype");
                            try
                            {
                                DSApi.initializeProxy(doc.getRecipients());
                                result.put("setFlags", doc.getFlags().getDocumentFlags(false));
                                result.put("setUserFlags", doc.getFlags().getDocumentFlags(true));
                            }
                            catch (EdmException e)
                            {
                            }
                            return result;
                        }
                    };

                results = OutOfficeDocument.search(query, mapper, Map.class);

                if (results.count() == 0)
                    addActionMessage(smL.getString("NieZnalezionoPism"));

                thisSearchUrl =
                    HttpUtils.makeUrl("/office/find-office-documents.action",
                    new Object[] {
                    "doSearchOut", "true",
                    "referenceId", referenceId,
                    "officeNumber", officeNumber,
                    "officeNumberYear", officeNumberYear,
                    "fromOfficeNumber", fromOfficeNumber,
                    "toOfficeNumber", toOfficeNumber,
                    //"officeNumberYear2", officeNumberYear2,
                    "assignedUser", assignedUser,
                    "creatingUser", creatingUser,
                    "documentDateFrom", documentDateFrom,
                    "documentDateTo", documentDateTo,
                    "barcode", barcode,
                    "summary", summary,
//                            "incomingDate", incomingDate,
//                            "kindId", kindId,
                    "recipient", recipient,
                    "sender", sender,
                    "journalId", journalId,
                    "internal", String.valueOf(internal),
//                            "stampDate", stampDate,
                    "accessedBy", accessedBy,
                    "accessedAs", accessedAs,
                    "accessedFrom", accessedFrom,
                    "accessedTo", accessedTo,
                    "flagsDateTo", flagsDateTo,
                    "flagsDateFrom", flagsDateFrom,
                    "userFlagsDateTo", userFlagsDateTo,
                    "userFlagsDateFrom", userFlagsDateFrom,
                    "flagIds",flagIds,
                    "userFlagIds",userFlagIds,
                    /*"flagIds", flagIds,
                    "userFlagIds", userFlagIds,          */
                    "limit", String.valueOf(limit),
                    "tab", tab,
                    "offset", String.valueOf(offset)});

                String tmp = "";
                if (internal && flagsOn)
                {
                	if(flagIds!=null)
                	{
	                    for (int i=1; i < flagIds.length; i++)
	                    {
	                        tmp = tmp+"&flagIds="+flagIds[i];
	                    }
                	}
                	if(userFlagIds!=null)
                	{
	                    for (int i=1; i < userFlagIds.length; i++)
	                    {
	                    	tmp = tmp+"&userFlagIds="+userFlagIds[i];
	                    }
                	}
                }
                thisSearchUrl = thisSearchUrl + tmp;
                final String tmp2 = tmp;
                attachmentsAsPdfUrl = thisSearchUrl + "&attachmentsAsPdf=true";

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        String url = HttpUtils.makeUrl("/office/find-office-documents.action",
                            new Object[] {
                            "doSearchOut", "true",
                            "referenceId", referenceId,
                            "officeNumber", officeNumber,
                            "officeNumberYear", officeNumberYear,
                            "fromOfficeNumber", fromOfficeNumber,
                            "toOfficeNumber", toOfficeNumber,
                            //"officeNumberYear2", officeNumberYear2,
                            "assignedUser", assignedUser,
                            "creatingUser", creatingUser,
                            "documentDateFrom", documentDateFrom,
                            "documentDateTo", documentDateTo,
                            "barcode", barcode,
                            "summary", summary,
//                            "incomingDate", incomingDate,
//                            "kindId", kindId,
                            "recipient", recipient,
                            "sender", sender,
                            "journalId", journalId,
                            "internal", String.valueOf(internal),
//                            "stampDate", stampDate,
                            "accessedBy", accessedBy,
                            "accessedAs", accessedAs,
                            "accessedFrom", accessedFrom,
                            "accessedTo", accessedTo,
                            "flagsDateTo", flagsDateTo,
                            "flagsDateFrom", flagsDateFrom,
                            "userFlagsDateTo", userFlagsDateTo,
                            "userFlagsDateFrom", userFlagsDateFrom,
                  /*          "flagIds", flagIds,
                            "userFlagIds", userFlagIds,*/
                            "tab", tab,
                            "ascending", String.valueOf(ascending),
                            "flagIds",flagIds,
                            "userFlagIds",userFlagIds,
                            "offset", String.valueOf(offset),
                            "limit",String.valueOf(limit)});
                        return url + tmp2;
                    }
                };
                pager = new Pager(linkVisitor, offset, limit, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }//actionPerformed
    }//class SearchOut

    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }

    public Pager getPager()
    {
        return pager;
    }

    public Object getResults()
    {
        return results;
    }

    public List getKinds()
    {
        return kinds;
    }

    public List getUsers()
    {
        return users;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public void setOfficeNumber(Integer officeNumber)
    {
        this.officeNumber = officeNumber;
    }

    public void setOfficeNumberYear(Integer officeNumberYear)
    {
        this.officeNumberYear = officeNumberYear;
    }

    public void setDocumentDateFrom(String documentDateFrom)
    {
        this.documentDateFrom = documentDateFrom;
    }

    public void setDocumentDateTo(String documentDateTo)
    {
        this.documentDateTo = documentDateTo;
    }

    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public List getTabs()
    {
        return tabs;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public void setStampDateFrom(String stampDateFrom)
    {
        this.stampDateFrom = stampDateFrom;
    }

    public void setStampDateTo(String stampDateTo)
    {
        this.stampDateTo = stampDateTo;
    }

    public void setIncomingDateFrom(String incomingDateFrom)
    {
        this.incomingDateFrom = incomingDateFrom;
    }

    public void setIncomingDateTo(String incomingDateTo)
    {
        this.incomingDateTo = incomingDateTo;
    }

    public List getOutDeliveries()
    {
        return outDeliveries;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public void setZpoDateFrom(String zpoDateFrom)
    {
        this.zpoDateFrom = zpoDateFrom;
    }

    public void setZpoDateTo(String zpoDateTo)
    {
        this.zpoDateTo = zpoDateTo;
    }

    public void setPostalRegNumber(String postalRegNumber)
    {
        this.postalRegNumber = postalRegNumber;
    }

    public List getInDeliveries()
    {
        return inDeliveries;
    }

    public Boolean getUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public void setFromOfficeNumber(Integer fromOfficeNumber)
    {
        this.fromOfficeNumber = fromOfficeNumber;
    }

    public void setToOfficeNumber(Integer toOfficeNumber)
    {
        this.toOfficeNumber = toOfficeNumber;
    }

/*
    public void setOfficeNumberYear2(Integer officeNumberYear2)
    {
        this.officeNumberYear2 = officeNumberYear2;
    }
*/

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public Map getJournals()
    {
        return journals;
    }

    public void setStatusId(Integer statusId)
    {
        this.statusId = statusId;
    }

    public List getIncomingStatuses()
    {
        return incomingStatuses;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public void setInternal(boolean internal)
    {
        this.internal = internal;
    }

    public void setAccessedBy(String accessedBy)
    {
        this.accessedBy = accessedBy;
    }

    public void setAccessedAs(String[] accessedAs)
    {
        this.accessedAs = accessedAs;
    }

    public List getAccessedByUsers()
    {
        return accessedByUsers;
    }

    public Map getSources()
    {
        return sources;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public void setAttachmentsAsPdf(boolean attachmentsAsPdf)
    {
        this.attachmentsAsPdf = attachmentsAsPdf;
    }

    public String getAttachmentsAsPdfUrl()
    {
        return attachmentsAsPdfUrl;
    }

    public List getFlags()
    {
        return flags;
    }

    public List getUserFlags()
    {
        return userFlags;
    }

    

    public Long[] getFlagIds() {
		return flagIds;
	}

	public void setFlagIds(Long[] flagIds) {
		this.flagIds = flagIds;
	}

	public Long[] getUserFlagIds() {
		return userFlagIds;
	}

	public void setUserFlagIds(Long[] userFlagIds) {
		this.userFlagIds = userFlagIds;
	}

	public void setFlagsDateFrom(String flagsDateFrom)
    {
        this.flagsDateFrom = flagsDateFrom;
    }

    public void setFlagsDateTo(String flagsDateTo)
    {
        this.flagsDateTo = flagsDateTo;
    }

    public void setUserFlagsDateFrom(String userFlagsDateFrom)
    {
        this.userFlagsDateFrom = userFlagsDateFrom;
    }

    public void setUserFlagsDateTo(String userFlagsDateTo)
    {
        this.userFlagsDateTo = userFlagsDateTo;
    }

    public String getAccessedFrom()
    {
        return accessedFrom;
    }

    public void setAccessedFrom(String accessedFrom)
    {
        this.accessedFrom = accessedFrom;
    }

    public String getAccessedTo()
    {
        return accessedTo;
    }

    public void setAccessedTo(String accessedTo)
    {
        this.accessedTo = accessedTo;
    }

    public boolean isFlagsOn()
    {
        return flagsOn;
    }

    /**
     * @return Returns the caseDocumentId.
     */
    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    /**
     * @param caseDocumentId The caseDocumentId to set.
     */
    public void setCaseDocumentId(String caseDocumentId)
    {
        this.caseDocumentId = caseDocumentId;
    }

    public void setGenerateXls(boolean arg)
    {
        generateXls=arg;
    }

	public boolean isAscending() {
		return ascending;
	}

	public String getSortField() {
		return sortField;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}   
    
    
    public void setYearsList( Map <Integer, String> arg) {
        yearsList = arg;
    }
    
    public  Map <Integer, String> getYearsList(){
        return this.yearsList;
    }
    
    public void setSelectedYear(Integer arg) {
        selectedYear = arg;
    }
	
    public Integer getSelectedYear() {
        return this.selectedYear;
    }

	public boolean getOriginalDocument() {
		return originalDocument;
	}

	public void setOriginalDocument(boolean originalDocument) {
		this.originalDocument = originalDocument;
	}

	public Integer getOfficeNumber() {
		return officeNumber;
	}

	public Integer getOfficeNumberYear() {
		return officeNumberYear;
	}

	public String getDocumentDateFrom() {
		return documentDateFrom;
	}

	public String getDocumentDateTo() {
		return documentDateTo;
	}

	public String getPostalRegNumber() {
		return postalRegNumber;
	}

	public String getIncomingDateFrom() {
		return incomingDateFrom;
	}

	public String getIncomingDateTo() {
		return incomingDateTo;
	}

	public String getRecipient() {
		return recipient;
	}

	public String getSender() {
		return sender;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public String getSummary() {
		return summary;
	}

	public Integer getFromOfficeNumber() {
		return fromOfficeNumber;
	}

	public Integer getToOfficeNumber() {
		return toOfficeNumber;
	}

	public String getFlagsDateFrom() {
		return flagsDateFrom;
	}

	public String getFlagsDateTo() {
		return flagsDateTo;
	}

	public String getUserFlagsDateFrom() {
		return userFlagsDateFrom;
	}

	public String getUserFlagsDateTo() {
		return userFlagsDateTo;
	}

	public void setUseBarcodes(Boolean useBarcodes) {
		this.useBarcodes = useBarcodes;
	}
}


