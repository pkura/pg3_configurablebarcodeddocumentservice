package pl.compan.docusafe.web.office;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class BarcodesOrderAction extends EventActionSupport
{

	private Integer number;
	private Long rangeId;
	private StringManager sm = StringManager.getManager(BarcodesOrderAction.class.getPackage().getName());
	private Long locationId;
	private String remark;
	private String transportServiceName;

	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doOrder").
        	append(OpenHibernateSession.INSTANCE).
        	append(new OrderBarcodes()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doConfirm").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Confirm()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUsed").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Used()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCancel").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Cancel()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdvertise").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Advertised()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class Cancel implements ActionListener
	{	
		 public void actionPerformed(ActionEvent event)
	        {
   	
	        }
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        }
    }
	
	private class OrderBarcodes implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

        }
	}

	private class Advertised implements ActionListener
	{	
		 public void actionPerformed(ActionEvent event)
	        {
        	
	        }
	}
	
	private class Used implements ActionListener
	{
	
		 public void actionPerformed(ActionEvent event)
	        {

	        	
	        }
	}
	
	private class Confirm implements ActionListener
	{
	
		 public void actionPerformed(ActionEvent event)
	        {

	        	
	        }
	}

	
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getRangeId() {
		return rangeId;
	}

	public void setRangeId(Long rangeId) {
		this.rangeId = rangeId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}



	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTransportServiceName() {
		return transportServiceName;
	}

	public void setTransportServiceName(String transportServiceName) {
		this.transportServiceName = transportServiceName;
	}

	
	
	
	

}
