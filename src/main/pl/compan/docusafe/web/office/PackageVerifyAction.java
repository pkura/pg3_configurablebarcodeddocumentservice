package pl.compan.docusafe.web.office;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.Package;
import pl.compan.docusafe.parametrization.prosika.PackageElement;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class PackageVerifyAction extends PackageBaseAction
{
	
	Logger log = LoggerFactory.getLogger(PackageVerifyAction.class);
	
	private Long packageId;
	private String numer;
	private String packageStatus;
	private Integer status;
	private Package pac;
	private String [] statusy;
	private String [] elementIds;
	private Map<Integer, String> statusMap = Package.statusElementMap;
	
	protected void setup() 
	{		
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
		
        registerListener("doSave").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Save()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doFinish").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new FinishVerification()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
		
	}
	
	
	
	private class FinishVerification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
	        	if(packageId != null) {
					pac = Package.find(packageId );
				}
	    		DSUser user = DSUser.findByUsername(pac.getUser());
	    		
	    		((Mailer) ServiceManager.getService(Mailer.NAME)).send(
	    				user.getEmail(),
	    				((Mailer) ServiceManager.getService(Mailer.NAME)).getProperty("fromEmail").getValue().toString(),
	    				"docusafe",
	    				"Raport z paczki numer ".concat(pac.getNumer()),
	    				"Raport z paczki",
	    				Package.pdfReportForElements(packageId, true, event)
	    		);
	    		addActionMessage("Wys�ano informacj� o zako�czeniu weryfikacji paczki na adres ".concat(user.getEmail()));
        	} catch (NullPointerException ex) {
        		addActionError("Nale�y skonfigurowa� poczt� wychodz�c�.");
        		log.error("", ex);
        	} catch (EdmException ex) {
        		addActionError(ex.getMessage());
                log.error("", ex);
        	}        	
        }
    }	
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
				fillForm();
				if(packageId != null)
				{
					pac = Package.find(packageId );
				}
					
				if(pac != null)
				{
					rodzajPrzesylki = pac.getRodzajPrzesylki();
					lokalizacja = pac.getLokalizacja()!=null?Long.parseLong(pac.getLokalizacja().toString()):null;
					typDokumentu = pac.getTypDokumentu();
					opis = pac.getOpis();
					numer = pac.getNumer();	
					
					transportServiceName = pac.getTransportServiceName();
					
					packageStatus = Package.statusMap.get(pac.getStatus());
					pac.getPackageElements();
				}
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
                log.error("",e);
        	}
			
        }
    }

	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{        		
        		pac = Package.find(packageId);
        		DSApi.context().begin();
        		pac.setLokalizacja(lokalizacja.intValue());
				pac.setOpis(opis);
				pac.setRodzajPrzesylki(rodzajPrzesylki);
				pac.setTypDokumentu(typDokumentu);
				
				pac.setTransportServiceName(transportServiceName);
				
				
				for(int i=0; i<elementIds.length;i++)
				{
					PackageElement pe = PackageElement.find(new Long(elementIds[i]));
					pe.setStatus(new Integer(statusy[i]));
					DSApi.context().session().save(pe);
				}
				
				DSApi.context().session().flush();
				
				pac.setStatusByElements();
				
				DSApi.context().commit();
				DSApi.context().session().refresh(pac);
        	}
        	catch (Exception e) 
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(e.getMessage());
                log.error("",e);
			}
        }
    }
	
	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getNumer() {
		return numer;
	}

	public void setNumer(String numer) {
		this.numer = numer;
	}

	public String getPackageStatus() {
		return packageStatus;
	}

	public void setPackageStatus(String packageStatus) {
		this.packageStatus = packageStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Package getPac() {
		return pac;
	}

	public void setPac(Package pac) {
		this.pac = pac;
	}

	public String[] getStatusy() {
		return statusy;
	}

	public void setStatusy(String[] statusy) {
		this.statusy = statusy;
	}

	public Map<Integer, String> getStatusMap() {
		return statusMap;
	}

	public void setStatusMap(Map<Integer, String> statusMap) {
		this.statusMap = statusMap;
	}

	public String[] getElementIds() {
		return elementIds;
	}

	public void setElementIds(String[] elementIds) {
		this.elementIds = elementIds;
	}
	
	

}
