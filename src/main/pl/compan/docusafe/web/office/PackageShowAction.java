package pl.compan.docusafe.web.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;


public class PackageShowAction extends PackageBaseAction
{

	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(PackageShowAction.class);
	private Long id;
	private List<Package> packages;
    private String numer;
	private String packageStatus;
	private Integer status;
	private Package pac;
	
	protected void setup() 
	{		
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSend").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Send()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("generatePdf").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GeneratePdfForPackages()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("generatePdfElem").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GeneratePdfForElements()).
	        appendFinally(CloseHibernateSession.INSTANCE);

	}
	
	//TODO: dodac transport
	private class GeneratePdfForPackages implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	File pdfFile = null;
        	try
        	{
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                int fontsize = 10;
                Font font = new Font(baseFont, (float)fontsize);

                pdfFile = File.createTempFile("docusafe_", "_tmp");
                Document pdfDoc =
                    new Document(PageSize.A4.rotate(),30,60,30,30);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));
                
                HeaderFooter footer =
               	 new HeaderFooter(new Phrase("Sporządzone przez "+DSApi.context().getDSUser().asFirstnameLastname()+ " " +
                            DateUtils.formatCommonDateTime(new Date()) + " Strona nr ", font), new Phrase("."));
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);
                
                pdfDoc.open();

                PdfUtils.addHeaderImageToPdf(pdfDoc, "Wprowadzone paczki", baseFont);

                int[] widths = new int[] { 3, 4 , 2, 5};

                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(3);

                Cell cell_nb = new Cell(new Phrase("Numer paczki", font));
                cell_nb.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_nb);

                Cell cell_status = new Cell(new Phrase("Status", font));
                cell_status.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_status);
                table.endHeaders();
                
                Cell cell_document_count = new Cell(new Phrase("Liczba dokumentów", font));
                cell_document_count.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_document_count);
                table.endHeaders();
                
                Cell cell_user = new Cell(new Phrase("Użytkownik", font));
                cell_user.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_user);
                table.endHeaders();
                
                        
                
                pdfDoc.add(table);
                pdfDoc.close();
        	}
        	catch (Exception ex) 
        	{
        		addActionError(ex.getMessage());
        		log.error("", ex);
			}
        	
            if (pdfFile != null && pdfFile.exists())
            {
                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdfFile),
                        "application/pdf", (int) pdfFile.length(),
                        "Content-Disposition: attachment; filename=\"packages.pdf\"");
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {                	
                    log.error("", e);
                }
                finally
                {
                	pdfFile.delete();
                }
            }
        }
        
    }
	
	private class GeneratePdfForElements implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        }
    }

	
	private class Send implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        }
        
    }
	
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
        }
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {        	
           
        }
    }
	
	
}