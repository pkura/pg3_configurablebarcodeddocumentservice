package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.crm.ContactKind;
import pl.compan.docusafe.core.crm.ContactStatus;
import pl.compan.docusafe.core.crm.LeafletKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja s�ownika kontaktow zadania handlowego.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class CrmContactDictionaryAction extends EventActionSupport
{
	private Log log = LogFactory.getLog(CrmContactDictionaryAction.class);
    public static final String URL = "/office/common/crmContractDictionary.action";
    
    //@IMPORT/EXPORT
    private Long id;

    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
  
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    private String rodzajKontaktu;
    private String rodzajNastepnegoKontaktu;
    private List<ContactKind> rodzajeKontaktu;
    private List<ContactStatus> statusykontaktu;
    private Long statusKontaktu;
    private String dataKontaktu;
    private String opis;
    private Set<LeafletKind> availableLeafle;
    private Set<LeafletKind> taskLeafle;
    private String[] selectedLeafle;
    private boolean documentReassigned;
	private String dataNastepnegoKontaktu;
	private String celKontaktu;
	private String createUser;
	private boolean goToTasklist;
    private String reloadLink;
    private Long documentId;
    private String godzina;
	
	
	private static Contact tmpContact = new Contact();
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }

	public void setParam(String param) {
		this.param = param;
	}

	private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
				availableLeafle  = new HashSet<LeafletKind>(LeafletKind.leafletKindlist());				
				rodzajeKontaktu = ContactKind.contactKindList();
				statusykontaktu = ContactStatus.statusKindList();
				if(id != null)
				{
					Contact c = tmpContact.find(id);
					opis = c.getOpis();
					if(c.getRodzajkontaktu() != null)
						rodzajKontaktu = c.getRodzajkontaktu().getCn();
					if(c.getRodzajNastepnegoKontaktu() != null)
						rodzajNastepnegoKontaktu = c.getRodzajNastepnegoKontaktu().getCn();
					if( c.getDataKontaktu() != null)
						dataKontaktu = DateUtils.formatJsDate(c.getDataKontaktu());
					taskLeafle = new HashSet<LeafletKind>();
					taskLeafle = c.getLeaflet();
					if(  c.getDataNastepnegoKontaktu() != null)
					{
						dataNastepnegoKontaktu = DateUtils.formatJsDate( c.getDataNastepnegoKontaktu());
						godzina = DateUtils.formatJsTime( c.getDataNastepnegoKontaktu());
					}
					celKontaktu = c.getCelKontaktu(); 
					if(c.getCreateUser() != null)
					{
						createUser = DSUser.findByUsername(c.getCreateUser()).asFirstnameLastname();
					}
					else
					{
						createUser = DSApi.context().getDSUser().asFirstnameLastname();						
					}
					if(taskLeafle != null)
					{
						for (LeafletKind lk : taskLeafle) 
						{
							availableLeafle.remove(lk);
						}
					}
					if(c.getStatus() != null)
						statusKontaktu = c.getStatus().getId();
				}
				else
				{
					//rodzajKontaktu = "TEL";
					dataKontaktu = DateUtils.formatJsDate(new Date());
				}
			} 
            catch (EdmException e) 
            {	
            	log.error(e.getMessage(), e);
            	addActionError(e.getMessage());
			}
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();
                Contact contact = new Contact();
                setValues(contact);
                contact.create();
                id = contact.getId();
                updateDocument(true, contact); 
                documentReassigned = true;
                if(goToTasklist || statusKontaktu == 115)
                	reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
            	log.error(e.getMessage(), e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener 
    {
		public void actionPerformed(ActionEvent event) 
        {  
            try
            {
            	DSApi.context().begin();
                Contact contact = tmpContact.find(id);
                setValues(contact);
                DSApi.context().session().flush();
                id = contact.getId();
                updateDocument(false, contact);
                documentReassigned = true;
                if(goToTasklist)
                	reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(), e);
            	DSApi.context()._rollback();
            	addActionError(e.getMessage());
            	
            }
        }
    }
    
    private void setValues(Contact contact) throws EdmException
    {
        if(dataKontaktu != null && dataKontaktu.length() > 0 )
        	contact.setDataKontaktu(DateUtils.parseJsDate(dataKontaktu));
        if(dataNastepnegoKontaktu != null && dataNastepnegoKontaktu.length() > 0 )
        {
        	if(StringUtils.isNotEmpty(godzina))
        	{
        		contact.setDataNastepnegoKontaktu(DateUtils.parseJsDateTime((dataNastepnegoKontaktu+" "+DateUtils.correctTime(godzina))));
        	}
        	else
        	{
        		contact.setDataNastepnegoKontaktu(DateUtils.parseJsDate(dataNastepnegoKontaktu));
        	}
        }
        contact.setCelKontaktu(celKontaktu);
        contact.setOpis(opis);
        contact.setRodzajNastepnegoKontaktu(ContactKind.findByCn(rodzajNastepnegoKontaktu));
        contact.setRodzajkontaktu(ContactKind.findByCn(rodzajKontaktu));   
        Set<LeafletKind> leafletSet = new HashSet<LeafletKind>();
        if(selectedLeafle != null && selectedLeafle.length > 0)
        {
        	for (int i = 0; i < selectedLeafle.length; i++) 
        	{
        		leafletSet.add(LeafletKind.findByCn(selectedLeafle[i])); 
			}
        	contact.setLeaflet(leafletSet);
        }
        else
        {
        	contact.setLeaflet(null);
        }             
        if(statusKontaktu != null)
        	contact.setStatus(ContactStatus.find(statusKontaktu));
        else
        	contact.setStatus(null);
    }
    
    private void updateDocument(boolean isNew,Contact contactThis) throws EdmException
    {
    	if(documentId == null)
    		throw new EdmException("Nie znaleziono dokumentu");
    	Document document = Document.find(documentId);
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	List<Contact> l = new ArrayList<Contact>();
    	if(isNew)
    	{
    		l = new ArrayList<Contact>((List<Contact>) fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN));
    		l.add(contactThis);
    		Map<String, Object> values = new HashMap<String, Object>();
    		List<Long> newList = new ArrayList<Long>(l.size());
    		for (Contact con : l)
    		{
				newList.add(con.getId());
			}
    		values.put(CrmMarketingTaskLogic.KONTAKT_FIELD_CN,newList);
    		document.getDocumentKind().setOnly(document.getId(), values);
    	}
         if(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) != null && !(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) instanceof Contact))
         {
         	l.addAll((List<Contact>) fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN));
         }
        
     	if(l != null && l.size() > 0 )
     	{
     		
     		Contact contact = l.get(l.size()-1);
 			
     		if(contact.getStatus().getId() == 115)
 			{
 				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
     			String[] accs = WorkflowFactory.findDocumentTasks(document.getId(), DSApi.context().getPrincipalName());
     			if(accs == null || accs.length < 1)
     			{
     				log.info("Brak zadania na li�cie zada�");
     			}
     			else
     			{
     				WorkflowFactory.getInstance().manualFinish(accs[0], false);
     				documentReassigned = true;
                    goToTasklist = true;
                    reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
     			}
     			log.info("Zakonczono prace z dokumentem o ID: " + document.getId());
 			}
     		else if(contact.getStatus().getId() == 20)
 			{
 				int noAnswerCount = 0;
 				for(Contact c : l)
 				{
 					if(c.getStatus().getId() == 20)
 					{
 						++noAnswerCount;
 					}
 					else
 					{
 						noAnswerCount=0;
 					}
 				}
 				log.info("Ilosc kontaktow bez odbioru: " + noAnswerCount);
 				if(noAnswerCount < 5)
 				{
     				LabelsManager.removeLabelsByName(document.getId(),CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
 					LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
 				}
 				else
 				{
 	    			String[] accs = WorkflowFactory.findDocumentTasks(document.getId(), DSApi.context().getPrincipalName());
 	    			if(accs == null || accs.length < 1)
 	    			{
 	    				throw new EdmException("Brak zadania na li�cie zada�");
 	    			}
 	    			
 		    		WorkflowFactory.getInstance().manualFinish(accs[0], false);
     				documentReassigned = true;
                    goToTasklist = true;
                    reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
 		    		log.info("Zakonczono prace z 5-cio krotnie ustawionym statusem 'nie dobiera'. ID dokumentu: " + document.getId());
 				}
 			}			
     		else
 	    	{
 	    		
 	    		Date d = contact.getDataNastepnegoKontaktu();
 	    		if( d != null)
 	    		{
 	    			Integer i = pl.compan.docusafe.util.DateUtils.substract(d, new Date());
 	    			log.info("XXXXXXXXXXXILOSC dni do kontaktu: " + i);
 	    			if(i > 5)
 	    			{
 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
 	    				LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ODLEGLE, DSApi.context().getPrincipalName(),true);
 	    				Date doDate = org.apache.commons.lang.time.DateUtils.addDays(new Date(), i-5);
 	    				doDate = org.apache.commons.lang.time.DateUtils.setHours(doDate, 1);
 	    				EventFactory.registerEvent("forOneDay", "crmMarketingTaskSetLabelEventHandler"
 	    						,""+i+"|"+LabelsManager.findLabelByName(CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI).getId()+"|"+document.getId(),null, doDate);
 	    			}
 	    			else
 	    			{
 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
 		    			switch (i)
 		    			{
 							case 0:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_DISIAJ, DSApi.context().getPrincipalName(),true);
 								break;
 							case 1:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 2:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_2_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 3:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_3_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 4:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_4_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 5:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI, DSApi.context().getPrincipalName(),true);
 								break;		
 							default:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_PRZETERMINOWANE, DSApi.context().getPrincipalName(),true);
 								break;
 						}
 	    			}
 	    		}
 	    	}
     	}
    	document.getDocumentKind().logic().archiveActions(document,CrmMarketingTaskLogic.UPDATE_DOCUMENT );
		TaskSnapshot.updateByDocument(document);
		
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getParam() {
		return param;
	}

	public String getRodzajKontaktu() {
		return rodzajKontaktu;
	}

	public void setRodzajKontaktu(String rodzajKontaktu) {
		this.rodzajKontaktu = rodzajKontaktu;
	}

	public List<ContactKind> getRodzajeKontaktu() {
		return rodzajeKontaktu;
	}

	public void setRodzajeKontaktu(List<ContactKind> rodzajeKontaktu) {
		this.rodzajeKontaktu = rodzajeKontaktu;
	}

	public String getDataKontaktu() {
		return dataKontaktu;
	}

	public void setDataKontaktu(String dataKontaktu) {
		this.dataKontaktu = dataKontaktu;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Set<LeafletKind> getAvailableLeafle() {
		return availableLeafle;
	}

	public void setAvailableLeafle(Set<LeafletKind> availableLeafle) {
		this.availableLeafle = availableLeafle;
	}

	public Set<LeafletKind> getTaskLeafle() {
		return taskLeafle;
	}

	public void setTaskLeafle(Set<LeafletKind> taskLeafle) {
		this.taskLeafle = taskLeafle;
	}

	public String[] getSelectedLeafle() {
		return selectedLeafle;
	}

	public void setSelectedLeafle(String[] selectedLeafle) {
		this.selectedLeafle = selectedLeafle;
	}

	public boolean isDocumentReassigned() {
		return documentReassigned;
	}

	public void setDocumentReassigned(boolean documentReassigned) {
		this.documentReassigned = documentReassigned;
	}

	public void setDataNastepnegoKontaktu(String dataNastepnegoKontaktu) {
		this.dataNastepnegoKontaktu = dataNastepnegoKontaktu;
	}

	public String getDataNastepnegoKontaktu() {
		return dataNastepnegoKontaktu;
	}

	public void setCelKontaktu(String celKontaktu) {
		this.celKontaktu = celKontaktu;
	}

	public String getCelKontaktu() {
		return celKontaktu;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public Long getStatusKontaktu() {
		return statusKontaktu;
	}

	public void setStatusKontaktu(Long statusKontaktu) {
		this.statusKontaktu = statusKontaktu;
	}

	public List<ContactStatus> getStatusykontaktu() {
		return statusykontaktu;
	}

	public void setStatusykontaktu(List<ContactStatus> statusykontaktu) {
		this.statusykontaktu = statusykontaktu;
	}

	public boolean isGoToTasklist() {
		return goToTasklist;
	}

	public void setGoToTasklist(boolean goToTasklist) {
		this.goToTasklist = goToTasklist;
	}

	public String getReloadLink() {
		return reloadLink;
	}

	public void setReloadLink(String reloadLink) {
		this.reloadLink = reloadLink;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getRodzajNastepnegoKontaktu() {
		return rodzajNastepnegoKontaktu;
	}

	public void setRodzajNastepnegoKontaktu(String rodzajNastepnegoKontaktu) {
		this.rodzajNastepnegoKontaktu = rodzajNastepnegoKontaktu;
	}

	public String getGodzina() {
		return godzina;
	}

	public void setGodzina(String godzina) {
		this.godzina = godzina;
	}
}
