package pl.compan.docusafe.web.office.dictionaries;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.DCMember;
import pl.compan.docusafe.core.dockinds.logic.DcLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

import pl.compan.docusafe.web.Pager;


import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class DcDictAction extends EventActionSupport{

	public static final int LIMIT = 15;
	private Long id;
	private String name ;
	private String surname;
	private String pesel;
	private String accountNumber;
	
	public static final String URL = "/office/common/dcdict.action";
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private DCMember inst;
	private int offset;
	
	private Long searchId ;
	private String searchName ;
	private String searchSurname ;
	private String searchAccountNumber;
	private String searchPesel;
	 private List<? extends DCMember> results;
	 private String param;
	 private Pager pager;
	 
	 private Boolean canDelete;
	 public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	public Boolean getCanAdd() {
		return canAdd;
	}
	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}
	public Boolean getCanRead() {
		return canRead;
	}
	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}
	private Boolean canAdd;
	 private Boolean canRead;
	 
	 
	public boolean isCanAdd() {
		return canAdd;
	}
	public void setCanAdd(boolean canAdd) {
		this.canAdd = canAdd;
	}
	@Override
	protected void setup()  {
		// TODO Auto-generated method stub
		FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	 registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
	    	try{
	    		DSApi.context().begin();
	    		
	    		canRead = DSApi.context().hasPermission(DSPermission.DC_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DC_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DC_SLOWNIK_USUWANIE);
	    		
	            if(id != null)
	            {
	            	inst = new DCMember().find(id);
	
	            	if(inst != null)
	            	{
	            		name = inst.getName();
	            		surname = inst.getSurname();
	            		pesel = inst.getPesel();
	            		Long accountNumberLong = inst.getAccountNumber();
	            		if(accountNumberLong!=null){
	            			accountNumber = accountNumberLong.toString();
	            		}
	            		
	            	
	            	}
	            	//else senderCountry = "PL";
	            	
	            }
	            else
	            {
	            	name = null;
	            	surname = null ;
	            	accountNumber = null; 
	            	pesel = null;
	            }
	            DSApi.context().commit();
			}
			catch(EdmException e)
			{
			
				LogFactory.getLog("eprint").debug("", e);
	            addActionError(e.getMessage());
	            try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
			}
    	}
    }
	private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(name))
            lista.add(sm.getString("NiePodanoImienia"));
        if (StringUtils.isEmpty(pesel))
            lista.add(sm.getString("NiePodanoPESEL"));
        if (StringUtils.isEmpty(accountNumber))
            lista.add(sm.getString("NiePodanoNumeruKonta"));
        if (StringUtils.isEmpty(surname))
            lista.add(sm.getString("NiePodanoNazwiska"));
        return lista;
    }
	private class Delete implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canDelete = DSApi.context().hasPermission(DSPermission.DC_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = new DCMember().find(id);
    			
                /* wyszukuje w dokumentach czy kontrahent zostal gdzies uzyty 
                 * uwaga, tylko w dokumentach typu invoice bo slownik ten bedzie
                 * wykozystywany tylko tam
                 * */
                if(inst==null){
                	addActionError(sm.getString("NieZnalezionoCzlonkaDoUsuniecia"));
                }
                else{
                	DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DC_KIND);
                    DockindQuery dockindQuery = new DockindQuery(0,0);
                    dockindQuery.setDocumentKind(kind);
                    
                    Field f = kind.getFieldByCn(DcLogic.CZLONEK_CN);
                    dockindQuery.field(f,id);
                    SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                    if (searchResults == null || searchResults.totalCount()<1)
                    {
                    	inst.delete();
                    	id = null;
                    	addActionMessage(sm.getString("ZeSlownikaUsunietoCzlonka",inst.getName() + " "+inst.getSurname()));
                    
                    }
                    else
                        throw new EdmException("NieMoznaUsunacTegoCzlonkaPoniewazSaDoNiegoPrzypisaneDokumenty");
                    	
                    
                	
                	DSApi.context().commit();
                }
    		}
    
			catch (EdmException e) 
	        {
	            try 
	            {
	                DSApi.context().rollback();
	            } 
	            catch (EdmException e1) 
	            {
	                addActionError(e1.getMessage());
	            }
	            addActionError(e.getMessage());
	        }
    	}
    }
	private class Update implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		List<String> lista = Validate();
    		try{
	            if(lista.size()>0)
	            {
	                for(int i=0; i<lista.size()-1;i++)
	                    addActionError(lista.get(i));
	                throw new EdmException(lista.get(lista.size()-1));
	            }
    		}
            catch(EdmException e)
    		{
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
                return;
    		}
    			
    		
    		try
    		{
    			DSApi.context().begin();
    			DCMember inst = new DCMember().find(id);
                
                inst.setName(name);
                Long accNumber = Long.parseLong(accountNumber);
                inst.setAccountNumber(accNumber);

                inst.setSurname(surname);
                inst.setPesel(pesel);

                DSApi.context().session().save(inst);
    			DSApi.context().commit();
    			
    		}
    		
    		catch(NumberFormatException e){
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    		
    	}
    }
	
	private class Add implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canAdd = DSApi.context().hasPermission(DSPermission.DC_SLOWNIK_DODAWANIE);
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
    			
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DCMember();
                if(accountNumber!=null)
                {
                	Long accNumber = Long.parseLong(accountNumber);
                	inst.setAccountNumber(accNumber);
                }
                
                inst.setName(name);
                inst.setPesel(pesel);
                inst.setSurname(surname);
                inst.create();
                
                id = inst.getId();
                DSApi.context().commit();
    			addActionMessage(sm.getString("WslownikuUtworzono",inst.getName()));
    		}
    		catch(NumberFormatException e){
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    
    	}
    	
    }
	private class Search implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		
    		//senderCountry = null;
    		
    		try
    		{
    			QueryForm form = new QueryForm(offset, LIMIT);
    			
    			if(name != null) form.addProperty("name", name);
    			if(surname != null) form.addProperty("surname", surname);
    			if(pesel != null) form.addProperty("pesel", pesel);
    			if(accountNumber!=null){
    				Long accNumber = Long.parseLong(accountNumber);
                    form.addProperty("accountNumber", accNumber);
    			}
    			

    			
                form.addOrderAsc("surname");
                
                SearchResults<? extends DCMember> results = DCMember.search(form);
                
                if (results.totalCount() == 0)  {
                	id=null;
                	throw new EdmException(sm.getString("NieZnalezionoCzlonkowPasujacychDoWpisanychDanych"));
                }
                else
                {
                	
                }
                DcDictAction.this.results = fun.list(results);
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "name", name,
                                "surname", surname,
                                "pesel", pesel,
                                "accountNumber", accountNumber,
                                "name", name,
                                "searchName", searchName,
                                "searchSurname", searchSurname,
                                "searchPesel",searchPesel,
                                "searchAccountNumber",searchAccountNumber,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
    		}
    		catch(NumberFormatException e){
    			LogFactory.getLog("eprint").debug("", e);
    			addActionError(e.getMessage());
    			 
    		}
    		catch (EdmException e) 
            {
    			LogFactory.getLog("eprint").debug("", e);
    			addActionError(e.getMessage());
    			 
            }
    		
    	}
    }
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public Long getSearchId() {
		return searchId;
	}
	public void setSearchId(Long searchId) {
		this.searchId = searchId;
	}
	public String getSearchName() {
		return searchName;
	}
	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
	public String getSearchSurname() {
		return searchSurname;
	}
	public void setSearchSurname(String searchSurname) {
		this.searchSurname = searchSurname;
	}
	public String getSearchAccountNumber() {
		return searchAccountNumber;
	}
	public void setSearchAccountNumber(String searchAccountNumber) {
		this.searchAccountNumber = searchAccountNumber;
	}
	public String getSearchPesel() {
		return searchPesel;
	}
	public void setSearchPesel(String searchPesel) {
		this.searchPesel = searchPesel;
	}
	public List<? extends DCMember> getResults() {
		return results;
	}
	public void setResults(List<? extends DCMember> results) {
		this.results = results;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
