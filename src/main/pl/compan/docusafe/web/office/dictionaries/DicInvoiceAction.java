package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.presale.ZamowienieLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/**
 * Akcja s�ownikowa dla s�ownika Instytucji/kontrahent�w (dla dokument�w invoice).
 * 
 * @author Tomasz Lipka
 */
public class DicInvoiceAction extends EventActionSupport
{
	private final static Logger LOG = LoggerFactory.getLogger(DicInvoiceAction.class);
	public static final String URL = "/office/common/dicinvoice.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String numerKontrahenta;
	private String numerKontaBankowego;
    private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String regon;
    private String nip;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String senderCountry;
    private String numerDomu;
    
    private Long searchid;
    private String serchNumerKontrahenta;
	private String serchNumerKontaBankowego;
    private String searchimie;
    private String searchnazwisko;
    private String searchname;
    private String searcholdname;
    private String searchregon;
    private String searchnip;
    private String searchulica;
    private String searchkod;
    private String searchmiejscowosc;
    private String searchtelefon;
    private String searchfaks;
    private String searchemail;
    private String searchsenderCountry;
    private String searchnumerDomu;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canModify;
    
    private DicInvoice inst;
    private Pager pager;
    private int offset;
    private List<? extends DicInvoice> results;
    //tylko dla obi
    private Double maxKwota;
    
    
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
	
    protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    }
    
    private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			canRead = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                canModify = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_MODYFIKACJA);
                
                if(id != null)
                {
                	inst = DicInvoice.getInstance().find(id);
                	if(inst != null)
                	{
                		numerKontrahenta = inst.getNumerKontrahenta();
                		numerKontaBankowego = inst.getNumerKontaBankowego();
                		imie = inst.getImie();
                        nazwisko = inst.getNazwisko();
                        name = inst.getName();
                        oldname = inst.getOldname();
                        nip = inst.getPrettyNip();
                        regon = inst.getRegon();
                        ulica = inst.getUlica();
                        kod = inst.getKod();
                        miejscowosc = inst.getMiejscowosc();
                        telefon = inst.getTelefon();
                        faks = inst.getFaks();
                        email = inst.getEmail();
                        senderCountry = inst.getsenderCountry();
                        numerDomu = inst.getNumerDomu();
                        //na prezentacje 
                        if(AvailabilityManager.isAvailable("dicinvoice.maxKwota"))
                        {
                        	maxKwota = ZamowienieLogic.getMaxKwotaDic(inst.getId()).doubleValue();
                        }
                	}
             //   	else senderCountry = "PL";
                	
                }
             //   else senderCountry = "PL";
    		}
    		catch(EdmException e)
    		{
    			LOG.error(e.getMessage(),e);
                addActionError(e.getMessage());
    		}
    	}
    }
    
    private class Add implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			canAdd = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                inst = new DicInvoice();
                inst.setNumerKontrahenta(numerKontrahenta);
                inst.setNumerKontaBankowego(numerKontaBankowego);
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setRegon(regon);
                inst.setNip(nip);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setsenderCountry(senderCountry);
                inst.setFaks(faks);
                inst.setNumerDomu(numerDomu);
                inst.setTelefon(telefon);
                inst.create();
                id = inst.getId();
                if(AvailabilityManager.isAvailable("dicinvoice.maxKwota") && maxKwota != null)
                {
                	ZamowienieLogic.setMaxKwotaDic(maxKwota, id);
                }
    			
    			DSApi.context().commit();
    			addActionMessage(sm.getString("WslownikuUtworzono",inst.getName()));
    		}
    		catch(EdmException e)
    		{
    			LOG.error(e.getMessage(),e);
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    	}
    }
    
    /**
     * uwagi klasie
     * */
    
    private class Delete implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canDelete = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = DicInvoice.getInstance().find(id);
    			
                /* wyszukuje w dokumentach czy kontrahent zostal gdzies uzyty 
                 * uwaga, tylko w dokumentach typu invoice bo slownik ten bedzie
                 * wykozystywany tylko tam
                 * */
                
                DockindQuery dockindQuery = new DockindQuery(0, 10);
                DocumentKind kind  = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_KIND);
                if(kind == null)
                {
                	kind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
                }
                dockindQuery = new DockindQuery(0, 10);
                dockindQuery.setDocumentKind(kind);
                Field field = kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN);
                dockindQuery.field(field, id);
                SearchResults searchResults = DocumentKindsManager.search(dockindQuery);
                int x = searchResults.count();
                
                if (x>0)
                    throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
                
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getName()));
                
    			DSApi.context().commit();
    		}
    		catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Update implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{

				if (StringUtils.isEmpty(numerKontrahenta) ){
					addActionError(sm.getString("NiePodanoNumeruKontrahenta"));
					return;
				}

				// dodalem spradzanie tego uprawnienia bo wydaje mi sie ze edycja i dodawanie to ten sam poziom dostepu
				canAdd = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_MODYFIKACJA);
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoModyfikowaniaRekordow"));


                
    			DSApi.context().begin();
    			DicInvoice inst = DicInvoice.getInstance().find(id);

				LOG.info("old = '{}'",inst.getNumerKontrahenta());
				LOG.info("new = '{}'", numerKontrahenta);
				if(!inst.getNumerKontrahenta().equals(numerKontrahenta)){
					DicInvoice.checkNumberExists(numerKontrahenta);
				}
    			
    			//szukanie wszystkich dokument�w z tym kontrahentem, tak zeby je zapisac ponownie
                //gdy czasami tytul czuy opis dokumentu zalezy od kontrahenta i trzeba to zapdejtowa�
                List<Long> ids = new ArrayList<Long>();
                try
                {
	                DockindQuery dockindQuery = new DockindQuery(0, 10);
	
	                DocumentKind kind  = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_KIND);
	                if(kind == null)
	                {
	                	kind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
	                }
	                dockindQuery.setDocumentKind(kind);
	                Field field = kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN);
	                dockindQuery.field(field, id);
	                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
	                for (Document doc : searchResults.results())
	                    ids.add(doc.getId());
	    			
	                SearchDockindDocumentsAction sdda = new SearchDockindDocumentsAction();
	                
	                SearchDockindDocumentsAction.ArchiveAction arch =  sdda.new ArchiveAction(ids.toArray(new Long[]{}));
	                arch.setDaemon(true);
	                arch.start();
                }
                catch (Exception e) 
                {
					e.printStackTrace();
				}
                
                inst.setNumerKontrahenta(numerKontrahenta);
                inst.setNumerKontaBankowego(numerKontaBankowego);
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setRegon(regon);
                inst.setNip(nip);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setsenderCountry(senderCountry);
                inst.setFaks(faks);
                inst.setNumerDomu(numerDomu);
                inst.setTelefon(telefon);

                if(AvailabilityManager.isAvailable("dicinvoice.maxKwota") && maxKwota != null)
                {
                	ZamowienieLogic.setMaxKwotaDic(maxKwota, id);
                }
                
    			DSApi.context().session().save(inst);
    			DSApi.context().commit();
    		}
    		catch(EdmException e)
            {
				addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
    	}
    }
    
    private class Search implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		//senderCountry = null;
    		
    		try
    		{
    			QueryForm form = new QueryForm(offset, LIMIT);
    			
    			if(numerKontrahenta != null) form.addProperty("numerKontrahenta", numerKontrahenta);
    			if(numerKontaBankowego != null) form.addProperty("numerKontaBankowego", numerKontaBankowego);
    			if(imie != null) form.addProperty("imie", imie);
                if(nazwisko != null) form.addProperty("nazwisko", nazwisko);
                if(name != null) form.addProperty("name", name);
                if(oldname != null) form.addProperty("oldname", oldname);
                if(regon != null) form.addProperty("regon", regon);
                if(nip != null) form.addProperty("nip", nip);
                if(ulica != null) form.addProperty("ulica", ulica);
                if(kod != null) form.addProperty("kod", kod);
                if(miejscowosc != null) form.addProperty("miejscowosc", miejscowosc);
                if(telefon != null) form.addProperty("telefon", telefon);
                if(faks != null) form.addProperty("faks", faks);
                if(email != null) form.addProperty("email", email);
                if(senderCountry != null) form.addProperty("senderCountry", senderCountry);
                if(numerDomu != null) form.addProperty("numerDomu", numerDomu);
    			
                form.addOrderAsc("numerKontrahenta");
                
                SearchResults<? extends DicInvoice> results = DicInvoice.search(form);
                
                if (results.totalCount() == 0)  
                	throw new EdmException(sm.getString("NieZnalezionoInstytucjiKontrahentowPasujacychDoWpisanychDanych"));
                else
                {
                	serchNumerKontrahenta = numerKontrahenta;
                	serchNumerKontaBankowego = numerKontaBankowego;
                	searchimie = imie;
                    searchnazwisko = nazwisko;
                    searchname = name;
                    searcholdname = oldname;
                    searchregon = regon;
                    searchnip = nip;
                    searchulica = ulica;
                    searchkod= kod;
                    searchmiejscowosc= miejscowosc;
                    searchtelefon= telefon;
                    searchfaks = faks;
                    searchemail = email;
                    searchsenderCountry = senderCountry;
                    searchnumerDomu = numerDomu;
                }
                DicInvoiceAction.this.results = fun.list(results);
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "numerKontrahenta", numerKontrahenta,
                                "numerKontaBankowego", numerKontaBankowego,
                                "imie", imie,
                                "nazwisko", nazwisko,
                                "name", name,
                                "oldname", oldname,
                                "regon", regon,
                                "nip", nip,
                                "ulica", ulica,
                                "kod", kod,
                                "miejscowosc", miejscowosc,
                                "telefon", telefon,
                                "faks", faks,
                                "email", email,
                                "param",param,
                                "senderCountry", senderCountry,
                                "searchimie", searchimie,
                                "searchnazwisko", searchnazwisko,
                                "searchname", searchname,
                                "searcholdname", searcholdname,
                                "searchregon", searchregon,
                                "searchnip", searchnip,
                                "searchulica", searchulica,
                                "searchkod", searchkod,
                                "searchmiejscowosc", searchmiejscowosc,
                                "searchtelefon", searchtelefon,
                                "searchfaks", searchfaks,
                                "searchemail", searchemail,
                                "searchsenderCountry", searchsenderCountry,
                                "searchnumerDomu",searchnumerDomu,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
    		}
    		catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Clean implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		numerKontrahenta = TextUtils.trimmedStringOrNull(numerKontrahenta);
            numerKontaBankowego = TextUtils.trimmedStringOrNull(numerKontaBankowego);
    		imie = TextUtils.trimmedStringOrNull(imie);
            nazwisko = TextUtils.trimmedStringOrNull(nazwisko);
            name = TextUtils.trimmedStringOrNull(name);
            oldname = TextUtils.trimmedStringOrNull(oldname);
            regon = TextUtils.trimmedStringOrNull(regon);
            nip = TextUtils.trimmedStringOrNull(nip);
            ulica = TextUtils.trimmedStringOrNull(ulica);
            kod = TextUtils.trimmedStringOrNull(kod);
            miejscowosc = TextUtils.trimmedStringOrNull(miejscowosc);
            telefon = TextUtils.trimmedStringOrNull(telefon);
            faks = TextUtils.trimmedStringOrNull(faks);
            email = TextUtils.trimmedStringOrNull(email);
            senderCountry  = TextUtils.trimmedStringOrNull(senderCountry);
            numerDomu = TextUtils.trimmedStringOrNull(numerDomu);
    	}
    }
    
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(name))
            lista.add(sm.getString("NiePodanoNazwy"));
        /*
        if (StringUtils.isEmpty(nip))
            lista.add(sm.getString("NiePodanoNIP"));
        */
        if (StringUtils.isEmpty(numerKontrahenta))
            lista.add(sm.getString("NiePodanoNumeruKontrahenta"));
        if (AvailabilityManager.isAvailable("dicinvoice.verify.numerKonta")
				&& StringUtils.isEmpty(numerKontaBankowego) )
            lista.add(sm.getString("NiePodanoNumeruKontaBankowego"));
        
        /*
        try
        {
        	Integer.parseInt(nip);
        	Integer.parseInt(regon);
        }
        catch(Exception e)
        {
        	lista.add(sm.getString("NipIRegonMuszaBycNumerami"));
        }
        */
        return lista;
    }
    
    
    /* Getters & Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumerKontrahenta() {
		return numerKontrahenta;
	}

	public void setNumerKontrahenta(String numerKontrahenta) {
		this.numerKontrahenta = numerKontrahenta;
	}

	public String getNumerKontaBankowego() {
		return numerKontaBankowego;
	}

	public void setNumerKontaBankowego(String numerKontaBankowego) {
		this.numerKontaBankowego = numerKontaBankowego;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOldname() {
		return oldname;
	}

	public void setOldname(String oldname) {
		this.oldname = oldname;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getFaks() {
		return faks;
	}

	public void setFaks(String faks) {
		this.faks = faks;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getsenderCountry() {
		return senderCountry;
	}

	public void setsenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public Long getSearchid() {
		return searchid;
	}

	public void setSearchid(Long searchid) {
		this.searchid = searchid;
	}

	public String getSerchNumerKontrahenta() {
		return serchNumerKontrahenta;
	}

	public void setSerchNumerKontrahenta(String serchNumerKontrahenta) {
		this.serchNumerKontrahenta = serchNumerKontrahenta;
	}

	public String getSerchNumerKontaBankowego() {
		return serchNumerKontaBankowego;
	}

	public void setSerchNumerKontaBankowego(String serchNumerKontaBankowego) {
		this.serchNumerKontaBankowego = serchNumerKontaBankowego;
	}

	public String getSearchimie() {
		return searchimie;
	}

	public void setSearchimie(String searchimie) {
		this.searchimie = searchimie;
	}

	public String getSearchnazwisko() {
		return searchnazwisko;
	}

	public void setSearchnazwisko(String searchnazwisko) {
		this.searchnazwisko = searchnazwisko;
	}

	public String getSearchname() {
		return searchname;
	}

	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}

	public String getSearcholdname() {
		return searcholdname;
	}

	public void setSearcholdname(String searcholdname) {
		this.searcholdname = searcholdname;
	}

	public String getSearchregon() {
		return searchregon;
	}

	public void setSearchregon(String searchregon) {
		this.searchregon = searchregon;
	}

	public String getSearchnip() {
		return searchnip;
	}

	public void setSearchnip(String searchnip) {
		this.searchnip = searchnip;
	}

	public String getSearchulica() {
		return searchulica;
	}

	public void setSearchulica(String searchulica) {
		this.searchulica = searchulica;
	}

	public String getSearchkod() {
		return searchkod;
	}

	public void setSearchkod(String searchkod) {
		this.searchkod = searchkod;
	}

	public String getSearchmiejscowosc() {
		return searchmiejscowosc;
	}

	public void setSearchmiejscowosc(String searchmiejscowosc) {
		this.searchmiejscowosc = searchmiejscowosc;
	}

	public String getSearchtelefon() {
		return searchtelefon;
	}

	public void setSearchtelefon(String searchtelefon) {
		this.searchtelefon = searchtelefon;
	}

	public String getSearchfaks() {
		return searchfaks;
	}

	public void setSearchfaks(String searchfaks) {
		this.searchfaks = searchfaks;
	}

	public String getSearchemail() {
		return searchemail;
	}

	public void setSearchemail(String searchemail) {
		this.searchemail = searchemail;
	}

	public String getSearchsenderCountry() {
		return searchsenderCountry;
	}

	public void setSearchsenderCountry(String searchsenderCountry) {
		this.searchsenderCountry = searchsenderCountry;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public DicInvoice getInst() {
		return inst;
	}

	public void setInst(DicInvoice inst) {
		this.inst = inst;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<? extends DicInvoice> getResults() {
		return results;
	}

	public void setResults(List<? extends DicInvoice> results) {
		this.results = results;
	}

	public StringManager getSm() {
		return sm;
	}

	public void setSm(StringManager sm) {
		this.sm = sm;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}


	public Boolean getCanModify() {
		return canModify;
	}


	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}


	public void setMaxKwota(Double maxKwota)
	{
		this.maxKwota = maxKwota;
	}


	public Double getMaxKwota()
	{
		return maxKwota;
	}


	public String getNumerDomu() {
		return numerDomu;
	}


	public void setNumerDomu(String numerDomu) {
		this.numerDomu = numerDomu;
	}


	public String getSearchnumerDomu() {
		return searchnumerDomu;
	}


	public void setSearchnumerDomu(String searchnumerDomu) {
		this.searchnumerDomu = searchnumerDomu;
	}
	
}
