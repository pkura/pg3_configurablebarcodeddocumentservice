package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.DicLocation;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class DicLocationAction extends EventActionSupport
{
	

	public static final String URL = "/office/common/diclocation.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String name;
	private String street;
	private String zip;
	private String city;
	private String description;
    
    private Long searchid;    
    private String searchname;
	private String searchstreet;
	private String searchcity;
	private String searchzip;

    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    
    private DicLocation inst;
    private Pager pager;
    private int offset;
    private List<? extends DicLocation> results;
    
    
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
	
    protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    }
    
    private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			canRead = true;   //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_ODCZYTYWANIE);
                canAdd = true;    //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                canDelete = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                
                if(id != null)
                {
                	inst = DicLocation.getInstance().find(id);
                	if(inst != null)
                	{
                		setName(inst.getName()); 
                        setStreet(inst.getStreet());
                        setZip(inst.getZip());
                        setCity(inst.getCity());
                        setDescription(inst.getDictionaryDescription());
                	}
                }
                DSApi.context().commit();
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    	}
    }
    
    private class Add implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canAdd = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DicLocation();
                
                inst.setName(getName());
                inst.setStreet(getStreet());
                inst.setZip(getZip());
                inst.setCity(getCity());
                
                
                inst.create();
                
                id = inst.getId();
    			
    			DSApi.context().commit();
    			
    			addActionMessage(sm.getString("WslownikuUtworzono",inst.getName()));
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                	e.printStackTrace();
                    addActionError(e1.getMessage());
                }
                e.printStackTrace();
                addActionError(e.getMessage());
    		}
    	}
    }
    
    /**
     * uwagi klasie
     * */
    
    private class Delete implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canDelete = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = DicLocation.getInstance().find(id);
    			
                
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst,getName()));
                
    			DSApi.context().commit();
    		}
    		catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Update implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			DicLocation inst = DicLocation.getInstance().find(id);
    			
    			inst.setName(getName());
                inst.setStreet(getStreet());
                inst.setZip(getZip());
                inst.setCity(getCity());
                
    			DSApi.context().session().save(inst);
    			DSApi.context().commit();
    		}
    		catch(EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
    	}
    }
    
    private class Search implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			QueryForm form = new QueryForm(offset, LIMIT);
    			
    			if(getName() != null) form.addProperty("name", getName());
    			if(getStreet() != null) form.addProperty("street", getStreet());
    			if(getZip() != null) form.addProperty("zip", getZip());
                if(getCity() != null) form.addProperty("city", getCity());
               
                form.addOrderAsc("name");
                
                SearchResults<? extends DicLocation> results = DicLocation.search(form);
                
                if (results.totalCount() == 0)  
                	throw new EdmException(sm.getString("NieZnalezionoRekorduPasujacychDoWpisanychDanych"));
                else
                {
                	searchname = name;
                	searchstreet = getStreet(); 
                	searchzip = getZip();
                	searchcity = getCity();
                    
                }
                DicLocationAction.this.results = fun.list(results);
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "name", getName(),
                                "street", getStreet(),
                                "zip", getZip(),
                                "city", getCity(),
                                "searchname", searchname,
                                "searchstreet", searchstreet,
                                "searchzip", searchzip,
                                "searchcity", searchcity,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
    		}
    		catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Clean implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		setName(TextUtils.trimmedStringOrNull(getName()));
    		setStreet(TextUtils.trimmedStringOrNull(getStreet()));
    		setZip(TextUtils.trimmedStringOrNull(getZip()));
    		setCity(TextUtils.trimmedStringOrNull(getCity()));    		
    	}
    }
    
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(getName()))
            lista.add(sm.getString("NiePodanoName"));
	        
	        return lista;
	    }


		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}


		public void setStreet(String street) {
			this.street = street;
		}


		public String getStreet() {
			return street;
		}


		public void setZip(String zip) {
			this.zip = zip;
		}


		public String getZip() {
			return zip;
		}


		public void setCity(String city) {
			this.city = city;
		}


		public String getCity() {
			return city;
		}

		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public Long getSearchid() {
			return searchid;
		}


		public void setSearchid(Long searchid) {
			this.searchid = searchid;
		}


		public String getSearchname() {
			return searchname;
		}


		public void setSearchname(String searchname) {
			this.searchname = searchname;
		}


		public String getSearchstreet() {
			return searchstreet;
		}


		public void setSearchstreet(String searchstreet) {
			this.searchstreet = searchstreet;
		}


		public String getSearchcity() {
			return searchcity;
		}


		public void setSearchcity(String searchcity) {
			this.searchcity = searchcity;
		}


		public String getSearchzip() {
			return searchzip;
		}


		public void setSearchzip(String searchzip) {
			this.searchzip = searchzip;
		}


		public Boolean getCanDelete() {
			return canDelete;
		}


		public void setCanDelete(Boolean canDelete) {
			this.canDelete = canDelete;
		}


		public Boolean getCanAdd() {
			return canAdd;
		}


		public void setCanAdd(Boolean canAdd) {
			this.canAdd = canAdd;
		}


		public Boolean getCanRead() {
			return canRead;
		}


		public void setCanRead(Boolean canRead) {
			this.canRead = canRead;
		}


		public DicLocation getInst() {
			return inst;
		}


		public void setInst(DicLocation inst) {
			this.inst = inst;
		}


		public Pager getPager() {
			return pager;
		}


		public void setPager(Pager pager) {
			this.pager = pager;
		}


		public int getOffset() {
			return offset;
		}


		public void setOffset(int offset) {
			this.offset = offset;
		}


		public List<? extends DicLocation> getResults() {
			return results;
		}


		public void setResults(List<? extends DicLocation> results) {
			this.results = results;
		}


		public String getParam() {
			return param;
		}


		public void setParam(String param) {
			this.param = param;
		}


		public void setDescription(String description) {
			this.description = description;
		}


		public String getDescription() {
			return description;
		}

	}
