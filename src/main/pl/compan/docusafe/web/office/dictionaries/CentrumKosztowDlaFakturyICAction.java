package pl.compan.docusafe.web.office.dictionaries;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.*;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.*;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.imports.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ic.IcUtils;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.*;
import static pl.compan.docusafe.parametrization.ic.InvoiceICLogic.*;

/**
 * Akcja s�ownikowa dla centrum kosztowych wybieranych dla faktury kosztowej.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CentrumKosztowDlaFakturyICAction extends EventActionSupport 
{

    // @IMPORT/@EXPORT
    private Long id;
    private String code;
    private Integer centrumId;
    private String amount;
    private String accountNumber;
    private Long documentId;
    private String descriptionAmount;
    private Integer centrum;
    private Boolean simpleAcceptance;

    /** wczytywane w query stringu -> okre�la czy formatka ma zawiera� tylko pole na wczytywanie pliku*/
    private boolean fileOnly;

    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    // @EXPORT
    /** gdy r�ne od null, oznacza po wykonaniu kt�rej akcji powr�cili�my na stron� JSP */
    private String afterAction;
    private String description;
    
    //private List<CentrumKosztow> centra;
    private Map<Integer, String> centra;
    private List<AccountNumber> accountNumbers;
	
    /** oznacza, �e to centrum zosta�o "zaakceptowane" - z�o�ono akceptacj� zale�n� od tego centrum */
    private boolean accepted;
	CentrumKosztowDlaFaktury centrumDlaFaktury;

    /** czy mo�na edytowa� kwot� przypisan� do danego centrum kosztowego */
    private boolean canChangeAmount;

    private boolean canChangeAmountWithoutLimit;

	//�rodki trwa�e - wyb�r tego typu faktury powoduje inne pola w formularzu
	private boolean fixedAssets;

	//dokument sad
	private boolean sad;

	private boolean centrumHidden = false;
    private boolean vatFieldsAvailable = false;

	private List<FixedAssetsSubgroup> subgroups;
	private List<FixedAssetsGroup> groups;
	private List<CustomsAgency> agencies;
	private List<LocationForIC> locations;
	private Integer groupId;
	private Integer subgroupId;
	private Integer customsAgencyId;
	private String storagePlace;
	private Integer locationId;

    private BigDecimal amountBigDecimal;
    private String maxAmount = "0";

    private static final String EV_FILL = "fill";
    private static final String EV_DO = "do";
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private static final Logger LOG = LoggerFactory.getLogger(CentrumKosztowDlaFakturyICAction.class);
    
    private FormFile file;
    
    private List<Map<String,Object>> addedFromFile;

    public boolean localizationAvailable;
    
    private Boolean showDescriptionAmount;

    private Integer remarkId;

    /** Ukrywa (ale nie usuwa z formatki) poni�sze pola */
    private boolean hideVatFields;
    private Integer vatTypeId;
    private Integer classTypeId;
    private Integer connectedTypeId;
    private Integer vatTaxTypeId;
    private boolean analyticsVisible;
    private Integer analyticsId;
    private boolean acceptanceVal = false;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doAdd").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Validate()).
            append(EV_DO, new Add()).
            append(EV_FILL, fillForm).            
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doUpdate").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Validate()).
            append(EV_DO, new Update()).
            append(EV_FILL, fillForm).            
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doDelete").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new Delete()).
            append(EV_FILL, fillForm).            
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
        registerListener("doLoadFile").
        	append(OPEN_HIBERNATE_AND_JBPM_SESSION).
        	append(EV_DO, new LoadFile()).
        	append(EV_FILL, fillForm).
        	appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
    }



    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if(fileOnly) {
                return; //nic nie musisz robi� po prostu wy�wietl formatk�
            }

			FieldsManager fm = null;
            try
            {
                accountNumbers = AccountNumber.list();
                locations = LocationForIC.list();
                if(locationId == null){
                    locationId = LocationForIC.findByCn("CEN").getId();
                }
       
                Document document = null;
                
				if (documentId != null)
                {
                    document = Document.find(documentId);
					fm = document.getFieldsManager();
				}
			
                if (id != null) { //edycja istniej�cego ckdf
                    centrumDlaFaktury = CentrumKosztowDlaFaktury.getInstance().find(id);
                    code = centrumDlaFaktury.getCentrumCode();
                    amount = centrumDlaFaktury.getRealAmount().toString();
                    accountNumber = centrumDlaFaktury.getAccountNumber();
                    locationId = centrumDlaFaktury.getLocationId();
                    if(locationId != null
                        && LocationForIC.LOCATION_CNS_WITH_ANALYTICS.contains(LocationForIC.find(locationId).getCn())){
                        analyticsVisible = true;
                    }

                    documentId = centrumDlaFaktury.getDocumentId();
                    accepted = DocumentAcceptance.find(documentId, null, null, id).size() > 0;
                    //u�ytkownik mo�e edytowa� kwot� je�li dla centrum nie by�o jeszcze akceptacji lub ma odpowiednie uprawnienia (B#4442)
                    canChangeAmount = !accepted || DSApi.context().hasPermission(DSPermission.INVOICE_CHANGE_COST_CENTER_AMOUNT);
                    canChangeAmountWithoutLimit = DSApi.context().hasPermission(DSPermission.INVOICE_CHANGE_COST_CENTER_AMOUNT);
                    document = Document.find(documentId);
					fm = document.getFieldsManager();
                    descriptionAmount = centrumDlaFaktury.getDescriptionAmount();
					storagePlace = centrumDlaFaktury.getStoragePlace();

                    remarkId = centrumDlaFaktury.getRemarkId();
                    connectedTypeId = centrumDlaFaktury.getConnectedTypeId();
                    vatTypeId = centrumDlaFaktury.getVatTypeId();
                    classTypeId = centrumDlaFaktury.getClassTypeId();
                    vatTaxTypeId = centrumDlaFaktury.getVatTaxTypeId();
                    analyticsId = centrumDlaFaktury.getAnalyticsId();
                } else { // dodawanie centrum
					findCentra(fm);
                    canChangeAmount = true;
                    canChangeAmountWithoutLimit = DSApi.context().hasPermission(DSPermission.INVOICE_CHANGE_COST_CENTER_AMOUNT);
                    amount = TextUtils.trimmedStringOrNull(amount);
                    vatTypeId = 20; //100% - domy�lna warto��
                    connectedTypeId = 10; //Niepowi�zane
                    classTypeId = 10; //KUP (S)
                    vatTaxTypeId = 10; //bez podatku VAT
                }

                //ukryj przed akceptacj� ksi�gow�
                hideVatFields = !IntercarsAcceptanceMode.allCnsAfter("dyrektora_fin")
                        .contains(fm.getEnumItemCn(STATUS_FIELD_CN).toLowerCase());

                if (document != null){
                
                	DocumentKind dk = document.getDocumentKind();
                    dk.initialize();

					if(dk.getCn().equals(DocumentLogicLoader.SAD_KIND)){
						processSAD();
                        localizationAvailable = fm.getBoolean(CREDIT_NOTY_FIELD_CN);
					} else {
                        boolean specialInvoice = IcUtils.isSpecialInvoice(fm);
                        if(specialInvoice && locationId == null) {
                            locationId = LocationForIC.findByCn("INTER").getId();
                        }
                        localizationAvailable = specialInvoice
                            || !FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS
                                .contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN));
						if (fm.getField(InvoiceICLogic.RODZAJ_FAKTURY_CN) != null) {
							String invoiceKindCn = fm.getEnumItem(RODZAJ_FAKTURY_CN).getCn();
							if (invoiceKindCn.equals(RODZAJ_SRODKI_TRWALE_CN)) {
								processFixedAssets(fm);
							} else if (FAKTURY_ZAKUPOWE_CNS.contains(invoiceKindCn)) {
								processZakupy();
							}
						}
					}

                    vatFieldsAvailable = fm.getBoolean(FAKTURA_SPECJALNA_CN)
                            || fm.getBoolean(CREDIT_NOTY_FIELD_CN)
                            || fm.getBoolean(KOREKTY_FIELD_CN)
                            || !centrumHidden;

                    LOG.info("ckdf.localizationAvailable = {}", localizationAvailable);

					if (accountNumber != null) {
						AccountNumber number = AccountNumber.findByNumber(accountNumber);
						if (!number.isVisibleInSystem()) {//je�li konto jest ukryte to dodaj je do listy by si� wy�wietli�o
							accountNumbers.add(number);
						}
					}

                    showDescriptionAmount = false;
                    
                    if ("true".equals(dk.getProperties().get(DocumentKind.SHOW_DESCRIPTION_AMOUNT_KEY))) {
						showDescriptionAmount = true;
					}
                    
                    if (centrum != null) {
						fm.initialize();
						fm.initializeAcceptances();
						DataBaseEnumField dbEnum = (DataBaseEnumField) fm.getField(InvoiceLogic.DZIAL_FIELD_CN);
						centrumId = dbEnum.getEnumItem(centrum).getCentrum();
					}
                    
                    String exactAmountFieldCn = document.getDocumentKind().getAcceptancesDefinition().getFinalAcceptance().getExactAmountFieldCn();
                    if (exactAmountFieldCn != null && (fm.getKey(exactAmountFieldCn) instanceof BigDecimal))
                    {
                        // obliczam kwote calkowita
                        BigDecimal fullAmount = (BigDecimal) fm.getKey(exactAmountFieldCn);
                        // obliczam sume aktualnych kwot czastkowych
                        List<CentrumKosztowDlaFaktury> list = CentrumKosztowDlaFaktury.findByDocumentId(document.getId());
                        BigDecimal tmpAmount = BigDecimal.ZERO;
                        for (CentrumKosztowDlaFaktury centrum : list)
                            tmpAmount = tmpAmount.add(centrum.getRealAmount() == null ? BigDecimal.ZERO : centrum.getRealAmount());
                        if (id != null)
                            tmpAmount = tmpAmount.subtract(new BigDecimal(amount == null ? "0" : amount));
                        
                        // okre�lam maksymaln� kwot�, kt�r� mo�na wybra�
                        maxAmount = (fullAmount).subtract(tmpAmount).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                        
                        if (id == null && amount == null)
                        {
                            // ustalam domy�lnie kwot� na brakuj�c� reszte do pe�nej kwoty
                            amount = maxAmount;
                        }                                                        
                    }  
                }
            }
            catch (Exception e) {
				addActionError(e.getMessage());
				LOG.error(e.getMessage(), e);
			}
        }

		private void findCentra(FieldsManager fm) throws SQLException, HibernateException, NumberFormatException, EdmException {
			Integer dzialKey = (Integer) fm.getKey(DZIAL_FIELD_CN);//mo�e by� null dla niekt�rych dokument�w
			if (dzialKey != null && CentrumKosztow.find(fm.getField(DZIAL_FIELD_CN).getEnumItem(dzialKey).getCentrum()).isIntercarsPowered()) {
                List<CentrumKosztow> centraList = CentrumKosztow.list();
                centra = Maps.newLinkedHashMap();
				for (CentrumKosztow ck : centraList) {
                    centra.put(ck.getId(), ck.getSymbol() + " (" + ck.getName() + ")");
				}
			} else {
                fillUserCentra();
			}
		}

		private void processSAD() throws EdmException {
			sad = true; // :(
			centrumHidden = true;
			accountNumbers = AccountNumber.icList();
			setAgencies(CustomsAgency.list());
		}

		private void processFixedAssets(FieldsManager fm) throws EdmException {
			fixedAssets = true;
			accountNumbers = AccountNumber.list();  // prosz� o podpi�cie s�ownik�w dla pola ?numer (opis) konta? takich samych jak dla koszt�w

			groups = FixedAssetsGroup.getAll();

			if(centrumDlaFaktury != null) {
				if(subgroupId == null){
					subgroupId = centrumDlaFaktury.getSubgroupId();
                    if(subgroupId != null){
                        //mo�e si� zdarzy�, �e faktura mia�a zmieniony rodzaj,
                        // wtedy przypisane centra nie maj� wymaganych atrybut�w
                        groupId = FixedAssetsSubgroup.find(subgroupId).getGroup().getId();
                    }
				} else if(groupId == null){// <=> groupId == null && subgroupId != null - w niekt�rych przypadkach mog� by� z�e dane
					groupId = FixedAssetsSubgroup.find(subgroupId).getGroup().getId();
				}
			}

			if(groupId!=null){
				subgroups = FixedAssetsGroup.find(groupId).getSubgroups();
			} else {
				groupId = null;
				subgroups = Collections.emptyList();
				subgroupId = null;
			}
			//init list
			subgroups.size();
		}

		private void processZakupy() throws EdmException {
            if(accountNumber == null){
			    accountNumber = "300-TH";
            }
			accountNumbers = AccountNumber.icList();
			centrumHidden = true;
		}
    }

    private void fillUserCentra() throws SQLException, EdmException {
        String username = DSApi.context().getPrincipalName();
        centra = AcceptanceCondition.getCentraSelectForUser(username);
    }

    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            amount = TextUtils.trimmedStringOrNull(amount);
            accountNumber = TextUtils.trimmedStringOrNull(accountNumber);           
            
            descriptionAmount = TextUtils.trimmedStringOrNull(descriptionAmount, 600);            
            if (amount == null) {
                addActionError(sm.getString("KwotaCzastkowaJestPolemObowiazkowym"));
                event.skip(EV_DO);
                return;
            }
            try {
                String s = amount.replace(",", ".").replace(" ", "");
                amountBigDecimal = new BigDecimal(s).setScale(2, RoundingMode.HALF_UP);
            } catch (NumberFormatException e) {
                addActionError(sm.getString("PodanoNiepoprawnaKwote"));
                event.skip(EV_DO);
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.context().begin();

                DocFacade doc = Documents.document(documentId);
                Document document = doc.getDocument();
				FieldsManager fm = doc.getFieldsManager();

                boolean specialInvoice = IcUtils.isSpecialInvoice(fm);

                if(!specialInvoice //te faktury si� nie licz�
                        && InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN))
                        && !CentrumKosztowDlaFaktury.findByDocumentId(documentId).isEmpty()){
                    throw new EdmException("Faktury zakupowe mog� mie� przypisane tylko jedno centrum.");
                }

                String invoiceKind = fm.getEnumItemCn(RODZAJ_FAKTURY_CN);
                if(remarkId == null
                        && !AccountNumberComment.findForAccount(accountNumber).isEmpty()
                        && (InvoiceICLogic.RODZAJ_KOSZTY_CN.equals(invoiceKind)
                        || InvoiceICLogic.RODZAJ_SRODKI_TRWALE_CN.equals(invoiceKind))){
                    throw new EdmException("Pole \"Komentarz do pozycji\" jest obowi�zkowe dla danego konta");
                }

                if(InvoiceICLogic.RODZAJ_SRODKI_TRWALE_CN.equals(invoiceKind)
                        && subgroupId == null
                        && (accountNumber.startsWith("010") || accountNumber.startsWith("020"))){
                    throw new EdmException("Dla kont zespo�u 010 oraz 020 pole podgrupa jest obowi�zkowe");
                }
                
                CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
                ckdf.setAccountNumber(accountNumber);
                ckdf.setAmount(amountBigDecimal);
                ckdf.setConnectedTypeId(connectedTypeId);
                ckdf.setClassTypeId(classTypeId);
                ckdf.setVatTypeId(vatTypeId);
                ckdf.setRemarkId(remarkId);
                ckdf.setVatTaxTypeId(vatTaxTypeId);

                if(locationId != null
                        && LocationForIC.LOCATION_CNS_WITH_ANALYTICS.contains(LocationForIC.find(locationId).getCn())){
                    ckdf.setAnalyticsId(1);
                }

				CentrumKosztow c;
				if(centrumId != null){
					c = CentrumKosztow.find(centrumId);
				} else {
					String symbol = null;
					if((fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN) || fm.getBoolean(KOREKTY_FIELD_CN))
							&& InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN))) {
						symbol = CentrumKosztow.SPECIAL_INVOICE_CODE;
						c = findCentrum(symbol);
					} else if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.SAD_KIND)) {
						symbol = DocumentLogicLoader.SAD_KIND;
						c = findCentrum(symbol);
					} else {
						symbol = fm.getEnumItemCn(RODZAJ_FAKTURY_CN).toString();
						if (InvoiceICLogic.FAKTURY_ZAKUPOWE_CNS.contains(symbol)){
							c = findCentrum(symbol);
						} else {
							throw new EdmException("Nie wybrano centrum");
						}
					}
				}

				ckdf.setCentrumCode(c.getCn());
				ckdf.setCentrumId(centrumId);

                ckdf.setDocumentId(documentId);
                ckdf.setDescriptionAmount(descriptionAmount);

				ckdf.setSubgroupId(subgroupId);
				ckdf.setStoragePlace(storagePlace);
				ckdf.setCustomsAgencyId(customsAgencyId);
				ckdf.setLocationId(locationId);
                ckdf.setAnalyticsId(analyticsId);

				//nie ka�de pismo ma dzia�
				Integer key = (Integer) fm.getKey(DZIAL_FIELD_CN);
				if (key != null) {
					EnumItem enumitem = fm.getField(DZIAL_FIELD_CN).getEnumItem(key);

					CentrumKosztow documentCentrum = CentrumKosztow.find(enumitem.getCentrum());

					if (documentCentrum.isIntercarsPowered()) {
						ckdf.setAcceptingCentrumId(documentCentrum.getId());
					}
				}
                
                Persister.create(ckdf);
                //simpleAcceptance = c.getDefaultSimpleAcceptance();
                
                description = ckdf.getDescription();
                id = ckdf.getId();
                
                if (document instanceof OfficeDocument) {
					((OfficeDocument) document).addWorkHistoryEntry(
							Audit.createWithPriority("addCentrum",
                                    DSApi.context().getPrincipalName(),
							        sm.getString("PrzypisanoDoDokumentuCentrumKosztowe",ckdf.getCentrumCode()),
							        ckdf.getCentrumCode(), Audit.LOWEST_PRIORITY));
				}
                
                // modyfikacja uprawnie�
                document.getDocumentKind().logic().documentPermissions(document);

                ckdf.initAcceptancesModes();
                try{
					if (!acceptanceVal) {
						if (fm.getBoolean(CREDIT_NOTY_FIELD_CN)
								|| fm.getBoolean(KOREKTY_FIELD_CN)) {
							Iterator<IntercarsAcceptanceMode> it = ckdf
									.getPossibleAcceptanceModes().iterator();
							IntercarsAcceptanceMode mode = it.next();
							if (it.hasNext()) {
								mode = it.next();
							}
							ckdf.setAcceptanceMode(mode.getId());
						} else {
							ckdf.setAcceptanceMode(ckdf
									.getPossibleAcceptanceModes().iterator()
									.next().getId());
						}
                        document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(InvoiceICLogic.AKCEPTACJA_STALA_CN, false));
					} else {
						ckdf.setAcceptanceMode(IntercarsAcceptanceMode.SIMPLEST.getId());
                        document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(InvoiceICLogic.AKCEPTACJA_STALA_CN, true));
					}
                } catch (NoSuchElementException e){
                    LOG.debug(e.getMessage(), e);
                    throw new EdmException("Brak okre�lonych �cie�ek akceptacji dla centrum:" + ckdf.getCentrumCode());
                }

                DSApi.context().session().flush();
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(document);
                document.getDocumentKind().logic().getAcceptanceManager().refresh(document,fm, null);

                DSApi.context().commit();                 
                                
                afterAction = "add";
                event.skip(EV_FILL);

            } catch (EdmException e) {
				addActionError(e.getMessage());
				LOG.error(e.getMessage(), e);
				DSApi.context()._rollback();

			} catch (Exception e) {
				addActionError(e.getMessage());
				LOG.error(e.getMessage(), e);
				DSApi.context()._rollback();
			}
        }

		private CentrumKosztow findCentrum(String symbol) throws EdmException {
			CentrumKosztow c = CentrumKosztow.findBySymbol(symbol);
			if (c == null) {
				throw new EdmException("Brak zdefiniowanego centrum o symbolu " + symbol);
			}
			centrumId = c.getId();
			return c;
		}
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.context().begin();
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(id);
                
            	String oldAccountNumber =  centrum.getAccountNumber();
            	BigDecimal oldAmountFloat = centrum.getRealAmount();
            	String oldDescriptionAmount = centrum.getDescriptionAmount();            	
                
                centrum.setAccountNumber(accountNumber);
                centrum.setAmount(amountBigDecimal);
                centrum.setDescriptionAmount(descriptionAmount);

				centrum.setSubgroupId(subgroupId);
				centrum.setStoragePlace(storagePlace);
				centrum.setCustomsAgencyId(customsAgencyId);
				centrum.setLocationId(locationId);

                centrum.setConnectedTypeId(connectedTypeId);
                centrum.setClassTypeId(classTypeId);
                centrum.setVatTypeId(vatTypeId);
                centrum.setRemarkId(remarkId);
                centrum.setVatTaxTypeId(vatTaxTypeId);
                centrum.setAnalyticsId(analyticsId);

                if(locationId != null ) {
                    if(LocationForIC.LOCATION_CNS_WITH_ANALYTICS.contains(LocationForIC.find(locationId).getCn())){
                        if(analyticsId == null){
                            centrum.setAnalyticsId(1);
                        }
                    } else {
                        centrum.setAnalyticsId(null);
                    }
                } 

                Document document = Document.find(documentId);
                if(oldAccountNumber != null && !oldAccountNumber.equals(accountNumber))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoKontoZna",oldAccountNumber,accountNumber,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }
                if(oldAmountFloat != null && !oldAmountFloat.equals(amountBigDecimal))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoKwoteZna",oldAmountFloat,amountBigDecimal,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }
                if(oldDescriptionAmount != null && !oldDescriptionAmount.equals(descriptionAmount))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoOpisZna",oldDescriptionAmount,descriptionAmount,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }
                
                
                description = centrum.getDescription();
                
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(document);
                
                DSApi.context().commit();
                
                afterAction = "update";                
                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    private final static Function<String[], String[]> headerFilter = HeaderFilters.buildFilter()
            .possibleKeys("CENTRUM_CODE", "ACCOUNT_CODE", "AMOUNT", "DESCRIPTION", "SUBGROUP_NUMBER", "LOCALIZATION", "COMMENT","NULL")
            .requiredKeys("CENTRUM_CODE", "ACCOUNT_CODE", "AMOUNT")
            .alias("NULL","")
            .alias("CENTRUM_CODE", "Kod centrum")
            .alias("ACCOUNT_CODE", "Numer konta")
            .alias("COMMENT", "Komentarz")
            .alias("AMOUNT", "Kwota")
            .alias("DESCRIPTION", "Opis")
            .alias("SUBGROUP_NUMBER", "Numer podgrupy")
            .alias("LOCALIZATION", "Lokalizacja")
            .create();

    /**
     * Klasa odpowiadaj�ca za dodanie centr�w kosztowych do pewnego dokumentu na podstawie importu key value
     * przerywa import przy pierwszym b��dzie
     */
    private class AddCentrumConsumer implements Function<KeyValueEntity, ImportEntryStatus> {

        private final long documentId;
        private final Set<Integer> permittedCentrumIds;
        private final boolean fixedAssets;
        private final boolean icPowered;
        private final Integer centrum;

        static final int vatTypeId = 20; //100% - domy�lna warto��
        static final int connectedTypeId = 10; //Niepowi�zane
        static final int classTypeId = 10; //KUP (S)
        static final int vatTaxTypeId = 10; //bez podatku VAT

        private AddCentrumConsumer(long documentId, FieldsManager fm) throws SQLException, EdmException {
            centrum = fm.getEnumItem(DZIAL_FIELD_CN).getCentrum();

            icPowered = centrum != null && CentrumKosztow.find(centrum).isIntercarsPowered();
            
            if(icPowered){
                this.permittedCentrumIds = null; //wszystkie dost�pne
            } else {
                this.permittedCentrumIds = new HashSet<Integer>(AcceptanceCondition.findByUserGuids());
            }
            this.documentId = documentId;
            String invoiceKind = fm.getEnumItem(RODZAJ_FAKTURY_CN).getCn();
            if(invoiceKind.equals(RODZAJ_SRODKI_TRWALE_CN)){
                fixedAssets = true;
            } else if (invoiceKind.equals(RODZAJ_KOSZTY_CN)){
                fixedAssets = false;
            } else {
                throw new EdmException("Import centr�w z pliku jest mo�liwy tylko dla faktur kosztowych i �rodk�w trwa�ych");
            }
        }

        BigDecimal sum = new BigDecimal("0");

        public ImportEntryStatus apply(KeyValueEntity keyValueEntity) {
            try {
//                LOG.info(" -- apply -- ");
                CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
                ckdf.setDocumentId(documentId);
                String centrumCode = keyValueEntity.string("CENTRUM_CODE");
                if(StringUtils.isBlank(centrumCode)){
                    return ImportEntryStatus.IGNORED;
                } 
                String amount = keyValueEntity.string("AMOUNT");
                if(StringUtils.isBlank(amount)){
                    throw new IllegalArgumentException("Kwota nie mo�e by� pusta");
                }
                ckdf.setAmount(new BigDecimal(amount).setScale(2, RoundingMode.HALF_EVEN));
                centrumCode = centrumCode.replaceAll("\\(.*\\)","").trim();
                CentrumKosztow ck =CentrumKosztow.findBySymbol(centrumCode);
                if(!ck.isAvailable()){
                    throw new IllegalArgumentException("Dodawanie centrum " + ck.getSymbol() + " zosta�o zablokowane");
                }
                ckdf.setCentrumCode(ck.getSymbol());
                ckdf.setCentrumId(ck.getId());

                //DOMYSLNE WARTOSCI DLA IC
                ckdf.setClassTypeId(classTypeId);
                ckdf.setConnectedTypeId(connectedTypeId);
                ckdf.setVatTypeId(vatTypeId);
                ckdf.setVatTaxTypeId(vatTaxTypeId);                

                String localizationCn = keyValueEntity.string("LOCALIZATION");
                if(!StringUtils.isBlank(localizationCn)){
                    localizationCn = localizationCn.replaceAll("\\(.*\\)","").trim();
                    LocationForIC lfi = LocationForIC.findByCn(localizationCn);
                    if(lfi == null){
                        throw new IllegalStateException("Lokalizacja o kodzie " + localizationCn + " nie istnieje");
                    } else {
                        ckdf.setLocationId(lfi.getId());
                    }
                }

                if(icPowered){
                    ckdf.setAcceptingCentrumId(centrum);
                } else {
                    if(!permittedCentrumIds.contains(ckdf.getCentrumId())){
                        throw new IllegalStateException("Nie masz uprawnie� do nadania centrum o kodzie " + ckdf.getCentrumCode());
                    }
                }

                String accountNumber = keyValueEntity.string("ACCOUNT_CODE").replaceAll("\\(.*\\)","").trim();      //zast�p tekst w nawiasach
                //excel/poi nie rozr�nia liczb ca�kowitych od zmiennoprzecinkowych czyli je�li kto� wpisze konto 012 to poi zwr�ci 12.0
                //TODO zastanowi� si� jak to przenie�� do providera
                accountNumber = accountNumber.replaceAll("\\.0+\\z","");
                if(null == AccountNumber.findByNumber(accountNumber)){
                    throw new IllegalArgumentException("Konto " + accountNumber + " nie istnieje");
                }
                ckdf.setAccountNumber(accountNumber);

                String commentText = keyValueEntity.string("COMMENT");
                Integer commentId = null;
                Collection<AccountNumberComment> accountNumberComments = AccountNumberComment.findForAccount(accountNumber);
                for(AccountNumberComment anc : accountNumberComments){
                    if(anc.getName().equalsIgnoreCase(commentText)){
                        commentId = anc.getId();
                        break;
                    }
                }
                if(commentText != null && commentId == null){
                    throw new IllegalArgumentException("Nie znaleziono komentarza '" + commentText + "' dla konta '" + accountNumber + "'");
                } else if (commentId == null && !accountNumberComments.isEmpty()){
                    throw new IllegalArgumentException("Dla konta '" + accountNumber + "' konieczne jest podanie komentarza z listy : " + Joiner.on(", ").join(accountNumberComments));
                }
                ckdf.setRemarkId(commentId);
                ckdf.setDescriptionAmount(keyValueEntity.string("DESCRIPTION"));
                if(fixedAssets){
                    String subgroup = keyValueEntity.string("SUBGROUP_NUMBER");
                    if(StringUtils.isBlank(subgroup)){
                        throw new IllegalArgumentException("Brak numeru podgrupy w co najmniej jednym z wpis�w");
                    }
//                    LOG.info("subgroup = " + subgroup);
                    if(subgroup != null){
                        ckdf.setSubgroupId(new BigDecimal(subgroup).intValue());
                    }
                } else {
                    String subgroup = keyValueEntity.string("SUBGROUP_NUMBER");
                    if(!StringUtils.isBlank(subgroup)){
                        throw new IllegalArgumentException("Numer podgrupy jest dost�pny tylko w �rodkach trwa�ych");
                    }
                }
                ckdf.initAcceptancesModes();
                if(!acceptanceVal) {
                	ckdf.setAcceptanceMode(ckdf.getPossibleAcceptanceModes().iterator().next().getId());
                } else {
                	ckdf.setAcceptanceMode(IntercarsAcceptanceMode.SIMPLEST.getId());
                }
//                LOG.info("centrum " + ckdf.getCentrumCode() + " kwota " + ckdf.getRealAmount()
//                        + " suma = " + (sum = sum.add(ckdf.getRealAmount())));

                DSApi.context().session().save(ckdf);
                return ImportEntryStatus.ADDED;
            } catch (EdmException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }

    private class LoadFile extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(file.getFile().getName().endsWith(".csv")){ //stare zachowanie, prawdopodobnie z ALD
                new LoadCsvFile().actionPerformed(event);
            } else {
                DSApi.context().begin();
                XlsWithHeaderFileProvider provider = new XlsWithHeaderFileProvider(file.getFile());
//                LOG.info(" -- provider -- ");
                provider.setHeaderFilter(headerFilter);
                Document document = Document.find(documentId);
                FieldsManager fm = document.getFieldsManager();
                if(InvoiceICLogic.FAKTURY_ZAKUPOWE_CNS.contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN))){
                    throw new EdmException("Import z pliku xls mo�liwy tylko dla faktur Koszty i �rodki trwa�e");
                }

                document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(InvoiceICLogic.AKCEPTACJA_STALA_CN, acceptanceVal));

                KeyValueImport importTask = new KeyValueImport(provider,
                        Predicates.<KeyValueEntity>alwaysTrue(),
                        new AddCentrumConsumer(documentId, fm),
                        KeyValueImport.EXTERNAL_TRANSACTION);

//                LOG.info(" -- before call -- ");
                importTask.call();
//                LOG.info(" -- after call -- ");
                DSApi.context().session().flush();
                BigDecimal sum = CentrumKosztowDlaFaktury.amountSumForDocument(documentId).setScale(2);
//                LOG.info("sum = " + sum);
                BigDecimal amount = (BigDecimal) fm.getKey(KWOTA_NETTO_FIELD_CN);
                //je�li scale nie s� r�wne to equals zawodzi, setScale zwraca nowego BigDecimala
                if(!sum.equals(amount.setScale(2))){
                    throw new EdmException("Suma (" + sum + ") nie zgadza si� z kwot� na fakturze " + amount);
                }


				document.getDocumentKind().logic().getAcceptanceManager().refresh(document,fm, null);

                DSApi.context().commit();
                afterAction = "fileImport";
                event.skip(EV_FILL);
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }
    
    private class LoadCsvFile implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{	
    		//przepisuje linie z pliku do listy
    		List<String> lista = new ArrayList<String>();
    		//lista dla iteratora z pliku centrum-kosztow.jsp
    		addedFromFile = new LinkedList<Map<String,Object>>();
    		try {
    		
    			try {
    				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getFile()), "cp1250"));
    				String line = "";
    				int lineCount = 0;
    				while ((line = reader.readLine()) != null)
    				{
    					lineCount++;
    					line = line.trim();
    					//ignorujemy 1 linie pliku [jdenak nie ignorujemy bo Paluch nie kce]
    					if(line.startsWith("#")) continue;
    					lista.add(line);
    				}
    			} catch(Exception e) {
    				throw new EdmException(sm.getString("BrakPliku"));
    			}
    			//dzili linie i zapisuje
    			DSApi.context().begin();
    			for(int i=0;i<lista.size();i++) 
    			{
     				String[] parts = lista.get(i).split("\\s*;\\s*");
     				
     				if(parts.length < 3)
     					throw new EdmException("Z�e formatowanie pliku csv.");
     					
    				CentrumKosztowDlaFaktury centrum = new CentrumKosztowDlaFaktury();
    				
    				if(parts[1].length() > 0) 
    				{
    					try {
                        		centrum.setAccountNumber(AccountNumber.findByNumber(parts[1]).getNumber());
                        } catch(Exception e) {
                        	throw new EdmException(sm.getString("BrakKontaONumerze"));
                        }
    				} else centrum.setAccountNumber(null);
    				
                    try {
						BigDecimal amount = new BigDecimal(parts[2]);
						amount.setScale(2);
                    	centrum.setAmount(amount);
                    } catch(Exception e) {
                    	throw new EdmException(sm.getString("KwotaNieJestLiczba"));
                    }
                    
                    CentrumKosztow c = CentrumKosztow.findBySymbol(parts[0]);
                    if(c == null)
                    	throw new EdmException(sm.getString("BrakCentrumONumerze"));
                    centrum.setCentrumCode(c.getCn());
                    centrum.setCentrumId(c.getId());
                    centrum.setDocumentId(documentId);
                    Persister.create(centrum);
                    description = centrum.getDescription();
                    id = centrum.getId();
                    simpleAcceptance = c.getDefaultSimpleAcceptance();
                    //dodajemy wpis do historii
                    Document document = Document.find(documentId);
                    if (document instanceof OfficeDocument)
                    {
                       ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("PrzypisanoDoDokumentuCentrumKosztowe",centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                    //modyfikacja uprawnie�
                    document.getDocumentKind().logic().documentPermissions(document);
                    //sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                    checkFinalAcceptance(document);
                    //wypelnia liste dla iteratora w centrum-kosztow.jsp
    				Map<String,Object> bean = new LinkedHashMap<String, Object>();
    				bean.put("ff_id", centrum.getDocumentId());
    				bean.put("ff_description", centrum.getDescription());
    				bean.put("ff_amount",centrum.getAmount());
    				addedFromFile.add(bean);
    			}
    			afterAction = "LoadCsvFile";
                event.skip(EV_FILL);
    			DSApi.context().commit();    			
    		} 
    		catch (EdmException e) {
    			addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
    		} 
    	}
    }
    
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.context().begin();
                
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(id);
                Persister.delete(centrum);
                
                // dodajemy wpis do historii
                Document document = Document.find(centrum.getDocumentId());
                if (document instanceof OfficeDocument)
                {
                    ((OfficeDocument) document).addWorkHistoryEntry(
                        Audit.createWithPriority("removeCentrum", DSApi.context().getPrincipalName(),
                            sm.getString("UsunietoZdokumentuCentrumKosztowe",centrum.getCentrumCode()),
                            centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                }
                
                // modyfikacja uprawnie�
                document.getDocumentKind().logic().documentPermissions(document);
                
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(document);

                DSApi.context().session().flush();
                document.getDocumentKind().logic().getAcceptanceManager().refresh(document, document.getFieldsManager(), null);
                
                DSApi.context().commit();    
                
                afterAction = "delete";
                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }   
    
    private void checkFinalAcceptance(Document document) throws EdmException
    {
        document.getDocumentKind().initialize();
        AcceptancesManager.checkFinalAcceptance(document);
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getCentrumId()
    {
        return centrumId;
    }

    public void setCentrumId(Integer centrumId)
    {
        this.centrumId = centrumId;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public String getAfterAction()
    {
        return afterAction;
    }

    public Map<Integer, String> getCentra()
    {
        return centra;
    }

    public List<AccountNumber> getAccountNumbers()
    {
        return accountNumbers;
    }

    public String getMaxAmount()
    {
        return maxAmount;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isAccepted()
    {
        return accepted;
    }    
    
    public FormFile getFile() {
    	return this.file;
    }
    
    public void setFile(FormFile file) 
    {
    	this.file = file;
    }
    
    public List<Map<String, Object>> getAddedFromFile() {
	 	return addedFromFile;
	 }

	public String getDescriptionAmount() {
		return descriptionAmount;
	}

	public void setDescriptionAmount(String descriptionAmount) {
		this.descriptionAmount = descriptionAmount;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public Boolean getShowDescriptionAmount() {
		return showDescriptionAmount;
	}

	public void setShowDescriptionAmount(Boolean showDescriptionAmount) {
		this.showDescriptionAmount = showDescriptionAmount;
	}

	public Boolean getSimpleAcceptance() {
		return simpleAcceptance;
	}

	public void setSimpleAcceptance(Boolean simpleAcceptance) {
		this.simpleAcceptance = simpleAcceptance;
	}

	 private static double round(double value, int decimalPlace)
    {
      double power_of_ten = 1;
      while (decimalPlace-- > 0)
         power_of_ten *= 10.0;
      return Math.round(value * power_of_ten) / power_of_ten;
    }

	public boolean isFixedAssets() {
		return fixedAssets;
	}

	public void setFixedAssets(boolean fixedAssets) {
		this.fixedAssets = fixedAssets;
	}

	public List<FixedAssetsSubgroup> getSubgroups() {
		return subgroups;
	}

	public void setSubgroups(List<FixedAssetsSubgroup> subgroups) {
		this.subgroups = subgroups;
	}

	public List<FixedAssetsGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<FixedAssetsGroup> groups) {
		this.groups = groups;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getSubgroupId() {
		return subgroupId;
	}

	public void setSubgroupId(Integer subgroupId) {
		this.subgroupId = subgroupId;
	}

	public String getStoragePlace() {
		return storagePlace;
	}

	public void setStoragePlace(String storagePlace) {
		this.storagePlace = storagePlace;
	}

	public boolean isSad() {
		return sad;
	}

	public void setSad(boolean sad) {
		this.sad = sad;
	}

	public List<CustomsAgency> getAgencies() {
		return agencies;
	}

	public void setAgencies(List<CustomsAgency> agencies) {
		this.agencies = agencies;
	}

	public boolean isCentrumHidden() {
		return centrumHidden;
	}

	public void setCentrumHidden(boolean centrumHidden) {
		this.centrumHidden = centrumHidden;
	}

    public boolean isFileOnly() {
        return fileOnly;
    }

    public void setFileOnly(boolean fileOnly) {
        this.fileOnly = fileOnly;
    }

	/**
	 * @return the locations
	 */
	public List<LocationForIC> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<LocationForIC> locations) {
		this.locations = locations;
	}

	/**
	 * @return the locationId
	 */
	public Integer getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

    public boolean isLocalizationAvailable() {
        return localizationAvailable;
    }

    public void setLocalizationAvailable(boolean localizationAvailable) {
        this.localizationAvailable = localizationAvailable;
    }

    public Integer getRemarkId() {
        return remarkId;
    }

    public void setRemarkId(Integer remarkId) {
        this.remarkId = remarkId;
    }

    public Integer getClassTypeId() {
        return classTypeId;
    }

    public void setClassTypeId(Integer classTypeId) {
        this.classTypeId = classTypeId;
    }

    public Integer getConnectedTypeId() {
        return connectedTypeId;
    }

    public void setConnectedTypeId(Integer connectedTypeId) {
        this.connectedTypeId = connectedTypeId;
    }

    public Integer getVatTypeId() {
        return vatTypeId;
    }

    public void setVatTypeId(Integer vatTypeId) {
        this.vatTypeId = vatTypeId;
    }

    public Map<Integer, String> getConnectedTypes(){
        return CentrumKosztowDlaFaktury.getConnectedTypes();
    }

    public Map<Integer, String> getClassTypes(){
        return CentrumKosztowDlaFaktury.getClassTypes();
    }

    public Map<Integer, String> getVatTypes(){
        return CentrumKosztowDlaFaktury.getVatTypes();
    }

    public Map<Integer, String> getVatTaxTypes(){
        return CentrumKosztowDlaFaktury.getVatTaxTypes();
    }
    
    public Map<Integer, String> getAnalytics(){
        return CentrumKosztowDlaFaktury.getAnalytics();
    }

    public boolean isHideVatFields() {
        return hideVatFields;
    }

    public void setHideVatFields(boolean hideVatFields) {
        this.hideVatFields = hideVatFields;
    }

    public Integer getVatTaxTypeId() {
        return vatTaxTypeId;
    }

    public void setVatTaxTypeId(Integer vatTaxTypeId) {
        this.vatTaxTypeId = vatTaxTypeId;
    }

    public boolean isVatFieldsAvailable() {
        return vatFieldsAvailable;
    }

    public void setVatFieldsAvailable(boolean vatFieldsAvailable) {
        this.vatFieldsAvailable = vatFieldsAvailable;
    }

    public Integer getAnalyticsId() {
        return analyticsId;
    }

    public void setAnalyticsId(Integer analyticsId) {
        this.analyticsId = analyticsId;
    }

    public boolean isAnalyticsVisible() {
        return analyticsVisible;
    }

	public boolean isAcceptanceVal() {
		return acceptanceVal;
	}

	public void setAcceptanceVal(boolean acceptanceVal) {
		this.acceptanceVal = acceptanceVal;
	}

    public boolean isCanChangeAmount() {
        return canChangeAmount;
    }

    public boolean isCanChangeAmountWithoutLimit() {
        return canChangeAmountWithoutLimit;
    }
}
