package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.DfInst;
import pl.compan.docusafe.core.dockinds.logic.DfLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * Akcja s�ownikowa dla s�ownika Instytucji/kontrahent�w (dla dokument�w finansowych).
 * 
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 */

public class DfInstDictionaryAction extends EventActionSupport 
{    
	private static final Logger log = LoggerFactory.getLogger(DfInstDictionaryAction.class);
    public static final String URL = "/office/common/dfinst.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String regon;
    private String nip;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;
    
    private Long searchid;
    private String searchimie;
    private String searchnazwisko;
    private String searchname;
    private String searcholdname;
    private String searchregon;
    private String searchnip;
    private String searchulica;
    private String searchkod;
    private String searchmiejscowosc;
    private String searchtelefon;
    private String searchfaks;
    private String searchemail;
    private String searchkraj;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    
    private DfInst inst;
    private Pager pager;
    private int offset;
    private List<? extends DfInst> results;
    private String documentKindCn;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                
                canRead = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                //tymczasowo 
                //to jest raczej bez sensu
                /*for(DocumentKind dk : DocumentKind.list())
                {
                	canRead = canRead || dk.logic().canReadDocumentDictionaries();
                	canAdd = canAdd || dk.logic().canReadDocumentDictionaries();
                	canDelete = canDelete || dk.logic().canReadDocumentDictionaries();
                }*/

                if(id != null) 
                {
                    inst = DSApi.getObject(DfInst.class, id);
                    if(inst!= null) 
                    {
                        imie = inst.getImie();
                        nazwisko = inst.getNazwisko();
                        name = inst.getName();
                        oldname = inst.getOldname();
                        nip = inst.getNip();
                        regon = inst.getRegon();
                        ulica = inst.getUlica();
                        kod = inst.getKod();
                        miejscowosc = inst.getMiejscowosc();
                        telefon = inst.getTelefon();
                        faks = inst.getFaks();
                        email = inst.getEmail();
                        kraj = inst.getKraj();
                    }
                    else 
                        kraj = "PL";
                }
                else 
                    kraj = "PL";
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
            	log.error(e.getMessage(),e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canAdd = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                //tymczasowo
                //bez sensu?
                /*for(DocumentKind dk : DocumentKind.list())
                {
                	canRead = canRead || dk.logic().canReadDocumentDictionaries();
                	canAdd = canAdd || dk.logic().canReadDocumentDictionaries();
                	canDelete = canDelete || dk.logic().canReadDocumentDictionaries();
                }*/
                canRead = true;
                canAdd = true;
                canDelete = true;
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DfInst();
                
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setRegon(regon);
                inst.setNip(nip);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setKraj(kraj);
                inst.setFaks(faks);
                inst.setTelefon(telefon);
                inst.create();

                id = inst.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",inst.getName()));
            } 
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                
                //rowniez bez sensu
                /*for(DocumentKind dk : DocumentKind.list())
                {
                	canRead = canRead || dk.logic().canReadDocumentDictionaries();
                	canAdd = canAdd || dk.logic().canReadDocumentDictionaries();
                	canDelete = canDelete || dk.logic().canReadDocumentDictionaries();
                }*/
                
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = DSApi.getObject(DfInst.class, id);
                
                
                DockindQuery dockindQuery = new DockindQuery(0, 10);
                DocumentKind kind  = DocumentKind.findByCn(DocumentLogicLoader.DF_KIND);
                dockindQuery.setDocumentKind(kind);
                Field field = kind.getFieldByCn(DfLogic.INST_FIELD_CN);
                dockindQuery.field(field, id);
                SearchResults searchResults = DocumentKindsManager.search(dockindQuery);
                int x = searchResults.count();
               
                dockindQuery = new DockindQuery(0, 10);
                kind  = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_KIND);
                dockindQuery.setDocumentKind(kind);
                field = kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN);
                dockindQuery.field(field, id);
                searchResults = DocumentKindsManager.search(dockindQuery);
                x += searchResults.count();
                
                if (x>0)
                   throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
               
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getName()));

                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
            	log.error(e.getMessage(),e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {               
            
                DSApi.context().begin();
                
                DfInst inst = DSApi.getObject(DfInst.class, id);
                
                //szukanie wszystkich dokument�w z tym kontrahentem, tak zeby je zapisac ponownie
                //gdy czasami tytul czuy opis dokumentu zalezy od kontrahenta i trzeba to zapdejtowa�
                List<Long> ids = new ArrayList<Long>();
                DockindQuery dockindQuery = new DockindQuery(0, 10);
                DocumentKind kind  = DocumentKind.findByCn(DocumentLogicLoader.DF_KIND);
                dockindQuery.setDocumentKind(kind);
                Field field = kind.getFieldByCn(DfLogic.INST_FIELD_CN);
                dockindQuery.field(field, id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                for (Document doc : searchResults.results())
                    ids.add(doc.getId());                
               
                dockindQuery = new DockindQuery(0, 10);
                kind  = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_KIND);
                dockindQuery.setDocumentKind(kind);
                field = kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN);
                dockindQuery.field(field, id);
                searchResults = DocumentKindsManager.search(dockindQuery);
                for (Document doc : searchResults.results())
                    ids.add(doc.getId());   
                
              /*  DockindQuery dockindQuery = new DockindQuery(0, 0);
                DocumentKind kind = DocumentKind.findByCn(DocumentKind.DF_KIND);
                kind.initialize();
                dockindQuery.setDocumentKind(kind);
                
                for (Field field : kind.getFields())
                {
                    if (Field.CLASS.equals(field.getType()))
                    {
                        dockindQuery.field(field, id);
                        dockindQuery.otherClassField(field, "name", inst.getName());
                        dockindQuery.otherClassField(field, "nip", inst.getNip());
                    }
                }
                 
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                Long[] ids = new Long[searchResults.count()];
                Document[] docs = searchResults.results();
                for (int i=0, t=searchResults.count(); i<t;i++)
                    ids[i] = docs[i].getId();
                    */
                SearchDockindDocumentsAction sdda = new SearchDockindDocumentsAction();
                
                SearchDockindDocumentsAction.ArchiveAction arch =  sdda.new ArchiveAction(ids.toArray(new Long[]{}));// ArchiveAction(ids);
                arch.setDaemon(true);
                arch.start();
                              
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setRegon(regon);
                inst.setNip(nip);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setKraj(kraj);
                inst.setFaks(faks);
                inst.setTelefon(telefon);
                inst.create();
                
                DSApi.context().session().save(inst);
                
                DSApi.context().commit();
            }
            catch(EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
            }
        }
    }  
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if(imie != null) form.addProperty("imie", imie);
                if(nazwisko != null) form.addProperty("nazwisko", nazwisko);
                if(name != null) form.addProperty("name", name);
                if(oldname != null) form.addProperty("oldname", oldname);
                if(regon != null) form.addProperty("regon", regon);
                if(nip != null) form.addProperty("nip", nip);
                if(ulica != null) form.addProperty("ulica", ulica);
                if(kod != null) form.addProperty("kod", kod);
                if(miejscowosc != null) form.addProperty("miejscowosc", miejscowosc);
                if(telefon != null) form.addProperty("telefon", telefon);
                if(faks != null) form.addProperty("faks", faks);
                if(email != null) form.addProperty("email", email);
                if(kraj != null) form.addProperty("kraj", kraj);
                

                form.addOrderAsc("name");

                SearchResults<? extends DfInst> results = DfInst.search(form);

                if (results.totalCount() == 0)  
                {
                    //afterSearch = false;
                    throw new EdmException(sm.getString("NieZnalezionoInstytucjiKontrahentowPasujacychDoWpisanychDanych"));
                }
                else 
                {
                    //afterSearch = true;
                    searchimie = imie;
                    searchnazwisko = nazwisko;
                    searchname = name;
                    searcholdname = oldname;
                    searchregon = regon;
                    searchnip = nip;
                    searchulica = ulica;
                    searchkod= kod;
                    searchmiejscowosc= miejscowosc;
                    searchtelefon= telefon;
                    searchfaks = faks;
                    searchemail = email;
                    searchkraj = kraj;
                }
                DfInstDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "imie", imie,
                                "nazwisko", nazwisko,
                                "name", name,
                                "oldname", oldname,
                                "regon", regon,
                                "nip", nip,
                                "ulica", ulica,
                                "kod", kod,
                                "miejscowosc", miejscowosc,
                                "telefon", telefon,
                                "faks", faks,
                                "email", email,
                                "kraj", kraj,
                                //"afterSearch", afterSearch ? "true" : "false",
                                "searchimie", searchimie,
                                "searchnazwisko", searchnazwisko,
                                "searchname", searchname,
                                "searcholdname", searcholdname,
                                "searchregon", searchregon,
                                "searchnip", searchnip,
                                "searchulica", searchulica,
                                "searchkod", searchkod,
                                "searchmiejscowosc", searchmiejscowosc,
                                "searchtelefon", searchtelefon,
                                "searchfaks", searchfaks,
                                "searchemail", searchemail,
                                "searchkraj", searchkraj,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            } 
            catch (EdmException e) 
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            imie = TextUtils.trimmedStringOrNull(imie);
            nazwisko = TextUtils.trimmedStringOrNull(nazwisko);
            name = TextUtils.trimmedStringOrNull(name);
            oldname = TextUtils.trimmedStringOrNull(oldname);
            regon = TextUtils.trimmedStringOrNull(regon);
            nip = TextUtils.trimmedStringOrNull(nip);
            ulica = TextUtils.trimmedStringOrNull(ulica);
            kod = TextUtils.trimmedStringOrNull(kod);
            miejscowosc = TextUtils.trimmedStringOrNull(miejscowosc);
            telefon = TextUtils.trimmedStringOrNull(telefon);
            faks = TextUtils.trimmedStringOrNull(faks);
            email = TextUtils.trimmedStringOrNull(email);
            kraj  = TextUtils.trimmedStringOrNull(kraj);  
        }
    }
        
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(name))
            lista.add(sm.getString("NiePodanoNazwy"));
        if (StringUtils.isEmpty(nip))
            lista.add(sm.getString("NiePodanoNIP"));
        return lista;
    }
      
    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getFaks() 
    {
        return faks;
    }

    public void setFaks(String faks) 
    {
        this.faks = faks;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getImie() 
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getKod() 
    {
        return kod;
    }

    public void setKod(String kod) 
    {
        this.kod = kod;
    }

    public String getKraj() 
    {
        return kraj;
    }

    public void setKraj(String kraj) 
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc() 
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) 
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko() 
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) 
    {
        this.nazwisko = nazwisko;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getOldname() 
    {
        return oldname;
    }

    public void setOldname(String oldname) 
    {
        this.oldname= oldname;
    }

    public String getTelefon() 
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) 
    {
        this.ulica = ulica;
    }

    public String getSearchemail() 
    {
        return searchemail;
    }

    public void setSearchemail(String searchemail) 
    {
        this.searchemail = searchemail;
    }

    public String getSearchfaks() 
    {
        return searchfaks;
    }

    public void setSearchfaks(String searchfaks) 
    {
        this.searchfaks = searchfaks;
    }

    public Long getSearchid() 
    {
        return searchid;
    }

    public void setSearchid(Long searchid) 
    {
        this.searchid = searchid;
    }

    public String getSearchimie() 
    {
        return searchimie;
    }

    public void setSearchimie(String searchimie) 
    {
        this.searchimie = searchimie;
    }

    public String getSearchkod() 
    {
        return searchkod;
    }

    public void setSearchkod(String searchkod) 
    {
        this.searchkod = searchkod;
    }

    public String getSearchkraj() 
    {
        return searchkraj;
    }

    public String getSearchmiejscowosc() 
    {
        return searchmiejscowosc;
    }

    public void setSearchmiejscowosc(String searchmiejscowosc) 
    {
        this.searchmiejscowosc = searchmiejscowosc;
    }

    public String getSearchnazwisko() 
    {
        return searchnazwisko;
    }

    public void setSearchnazwisko(String searchnazwisko) 
    {
        this.searchnazwisko = searchnazwisko;
    }

    public String getSearchname() 
    {
        return searchname;
    }

    public void setSearchname(String searchname) 
    {
        this.searchname = searchname;
    }
    
    public String getSearcholdname() 
    {
        return searcholdname;
    }

    public void setSearcholdname(String searcholdname) 
    {
        this.searcholdname = searcholdname;
    }

    public String getSearchtelefon() 
    {
        return searchtelefon;
    }

    public void setSearchtelefon(String searchtelefon) 
    {
        this.searchtelefon = searchtelefon;
    }

    public String getSearchulica() 
    {
        return searchulica;
    }

    public void setSearchulica(String searchulica) 
    {
        this.searchulica = searchulica;
    }
    
    public Boolean getCanAdd() 
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd) 
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete() 
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) 
    {
        this.canDelete = canDelete;
    }

    public void setSearchkraj(String searchkraj) 
    {
        this.searchkraj = searchkraj;
    }

    public DfInst getInst() 
    {
        return inst;
    }

    public void setInst(DfInst inst) 
    {
        this.inst = inst;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead) 
    {
        this.canRead = canRead;
    }

    public int getOffset() 
    {
        return offset;
    }

    public void setOffset(int offset) 
    {
        this.offset = offset;
    }

    public Pager getPager() 
    {
        return pager;
    }

    public void setPager(Pager pager) 
    {
        this.pager = pager;
    }

    public List<? extends DfInst> getResults() 
    {
        return results;
    }

    public void setResults(List<? extends DfInst> results) 
    {
        this.results = results;
    }

    public String getNip() 
    {
        return nip;
    }

    public void setNip(String nip) 
    {
        this.nip = nip;
    }

    public String getRegon() 
    {
        return regon;
    }

    public void setRegon(String regon) 
    {
        this.regon = regon;
    }

    public String getSearchnip() 
    {
        return searchnip;
    }

    public void setSearchnip(String searchnip) 
    {
        this.searchnip = searchnip;
    }

    public String getSearchregon() 
    {
        return searchregon;
    }

    public void setSearchregon(String searchregon) 
    {
        this.searchregon = searchregon;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}    
}
