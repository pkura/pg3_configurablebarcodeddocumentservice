package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.bouncycastle.crypto.DSA;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.LeasingObjectType;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;



/**
 * Akcja s�ownika dla wnioskow dokument�w leasingowych.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class DlApplicationDictionaryAction extends EventActionSupport
{

    public static final String URL = "/office/common/dlApplicationDictionary.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String numerWniosku;
    private String idKlienta;
    private Long idUmowy;
    private Long idKlientaInt;
    private Long idDostawcy;
    private String opis;
    private String wartosc;
    private Float wartoscFloat;
    private String kwota;
    private Float kwotaFloat;
    private String liczbaRat;
    private Integer liczbaRatInt;
    private String wykup;
    private Float wykupFloat;
    private String rodzaj;
    private String dictionaryDescription;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private String nazwaKlienta;
    private String nazwaDostawcy;
    private String numerUmowy;
    private DlApplicationDictionary inst;
    private Pager pager;
    private int offset;
    private List<? extends DlApplicationDictionary> results;
    private static final String EV_DO = "do";
    private List<String> rodzajList = new ArrayList<String>();
    private Map<String,String> rodzajMap = new HashMap<String,String>();
    private Map<Long,String> contractsIdsMap = new HashMap<Long,String>();
    private List<LeasingObjectType> leasingObjectTypes;
    private Long leasingObjectType;
    
	private static Contractor tmpContractor = new Contractor();
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(DlApplicationDictionaryAction.class);

    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate(1)).
            append(EV_DO, new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate(2)).
            append(EV_DO, new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate(3)).
            append(EV_DO, new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete"). 
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                rodzajList.addAll(DlApplicationDictionary.getRodzajList());
                leasingObjectTypes = LeasingObjectType.list();
                for (Iterator iter = rodzajList.iterator(); iter.hasNext();)
                {
                    String element = (String) iter.next();
                    rodzajMap.put(element,sm.getString(element));
                }
                DSApi.context().begin();

                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);

                if (id != null) 
                {
                    inst = DlApplicationDictionary.getInstance().find(id);
                    for(Long contractId : DlApplicationDictionary.getInstance().getContracts(id))
                    {
                    	if(contractId > 0)
                    	{
	                    	DlContractDictionary dlc = DlContractDictionary.getInstance().find(contractId);
	                    	contractsIdsMap.put(contractId, dlc.getNumerUmowy());
                    	}
                    }
                    if (inst != null) 
                    {
                        numerWniosku = inst.getNumerWniosku();
                        if (inst.getIdKlienta() != null)
                        {
                            idKlienta = inst.getIdKlienta().toString();
                            if(idKlienta != null)
                            {
                                nazwaKlienta = tmpContractor.find(Long.parseLong(idKlienta.toString())).getName();
                            }
                        }
                        if (inst.getIdDostawcy() != null)
                        {
                            idDostawcy = inst.getIdDostawcy();
                            if(idDostawcy != null)
                            {
                                nazwaDostawcy = tmpContractor.find(idDostawcy).getName();
                            }
                        }
                        if(inst.getContractId() != null)
                        {
                        	DlContractDictionary dlc = DlContractDictionary.getInstance().find(inst.getContractId());
                        	idUmowy = dlc.getId();
                        	numerUmowy = dlc.getNumerUmowy();
                        	if(!contractsIdsMap.containsKey(dlc.getId()))
                        	{
                        		contractsIdsMap.put(dlc.getId(), dlc.getNumerUmowy());
                        	}
                        }
                        opis = inst.getOpis();
                        if (inst.getWartosc() != null)
                            wartosc = inst.getWartosc().toString();
                        if (inst.getKwota() != null)
                            kwota = inst.getKwota().toString();
                        if (inst.getWykup() != null)
                            wykup = inst.getWykup().toString();
                        if (inst.getliczbaRat() != null)
                             liczbaRat = inst.getliczbaRat().toString();
                        leasingObjectType = inst.getLeasingObjectType();
                        rodzaj = inst.getRodzaj();
                        dictionaryDescription = inst.getDictionaryDescription();
                    }
                }
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }
    
    
    private void setValue(DlApplicationDictionary inst) throws EdmException
    {
    	inst.setIdKlienta(idKlientaInt);
    	inst.setIdDostawcy(idDostawcy);
        inst.setNumerWniosku(numerWniosku);
        inst.setOpis(opis);
        inst.setWartosc(wartoscFloat);
        inst.setWykup(wykupFloat);
        inst.setliczbaRat(liczbaRatInt);
        inst.setRodzaj(rodzaj);
        inst.setKwota(kwotaFloat);
        inst.setLeasingObjectType(leasingObjectType);
        if(idUmowy != null)
        {
        	log.info("id umowy: idUmowy dla slownika dlapplica " + idUmowy );
        	inst.setContractId(idUmowy);
        }
        
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {  
            try
            {
                DSApi.context().begin();
                DlApplicationDictionary inst = DlApplicationDictionary.getInstance().find(id);
                setValue(inst);             
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            	addActionError(e.getMessage());
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

               
                inst = new DlApplicationDictionary();                
                setValue(inst);
                inst.create();
                id = inst.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",numerWniosku));
            } 
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
           /* try 
            {
            	if(true)
            		return;
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = DlApplicationDictionary.find(id);
                
                DocumentKind kind = DocumentKind.findByCn(DocumentKind.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.NUMER_WNIOSKU_CN);
                dockindQuery.field(f,id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                if (searchResults == null || searchResults.totalCount()<1)
                {
                    inst.delete();
                    id = null;
                    addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getNumerWniosku()));
                }
                else
                    throw new EdmException("NieMoznaUsunacTegoWnioskuPoniewazSaDoNiegoPrzypisaneDokumenty");
                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
            */
        	addActionError("Nie mo�na usun�� wniosku");
        }
    }  
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if (numerWniosku != null) form.addProperty("numerWniosku", numerWniosku);
                if (idKlientaInt != null)
                {
                    form.addProperty("idKlienta", idKlientaInt);
                }
                if (idDostawcy != null)
                {
                    form.addProperty("idDostawcy", idDostawcy);
                }
                if(idUmowy != null)
                {
                    form.addProperty("idUmowy", idUmowy);
                }
                if (opis != null) form.addProperty("opis", opis);
                if (wartoscFloat != null)form.addProperty("wartosc", wartoscFloat);
                if (kwotaFloat != null) form.addProperty("kwota", kwotaFloat);
                if (liczbaRatInt != null) form.addProperty("liczbaRat", liczbaRatInt );
                if (wykupFloat != null)  form.addProperty("wykup",wykupFloat);
                if (rodzaj != null) form.addProperty("rodzaj", rodzaj);      
                form.addOrderAsc("numerWniosku");
                SearchResults<? extends DlApplicationDictionary> results = DlApplicationDictionary.search(form);
                if (results == null || results.totalCount() == 0)
                {
                	DlApplicationDictionary app = null;
                	if(numerWniosku == null)
                	{
                		throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                	}
                	else
                	{
                		List<DlApplicationDictionary> l = DlApplicationDictionary.findByNumerWniosku(numerWniosku);
                		if(l != null && l.size() > 0)
                		{
                			app = l.get(0);
                		}
                		if(app == null)
                		{
                			DSApi.context().begin();
                			app = ImpulsImportUtils.findApplicationByNumber(numerWniosku);
                			DSApi.context().commit();
                		}
                		if(app == null)
                		{
                			throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                		}
                		
                		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
                        {
                        	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
                        	{
                        		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
                        		{
                        			throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        		else
                        		{
                        			Contractor contractor = DSApi.context().load(Contractor.class, app.getIdKlienta());
                        			if(!contractor.patronsContainsUser(DSApi.context().getPrincipalName()))
                        				throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        	}
                        	else
                        	{
                        		Contractor contractor = DSApi.context().load(Contractor.class, app.getIdKlienta());
                        		if(contractor.getRegion() != null)
                        		{
                        			HashSet<String> divisionSet = new HashSet<String>(Arrays.asList(DSApi.context().getDSUser().getDivisionsGuid()));
                        			if(!divisionSet.contains("DV_"+contractor.getRegion().getCn()))
                        				throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        	}
                        }
            			id = app.getId();
            			DlApplicationDictionaryAction.this.results = null;
            			addActionMessage("Zaimportowano wniosek z LEO");
            			return;
                	}
                }
                DlApplicationDictionaryAction.this.results = fun.list(results);
                if(results.totalCount() == 1)
                {
                	id = DlApplicationDictionaryAction.this.results.get(0).getId();
                	results = null;
                	return;
                }
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "numerWniosku", numerWniosku,
                                "idKlienta", idKlienta,
                                "idDostawcy", idDostawcy,
                                "opis", opis,
                                "wartosc", wartoscFloat,
                                "liczbaRat", liczbaRatInt,
                                "kwota",kwotaFloat,
                                "wykup",wykupFloat,
                                "rodzaj",rodzaj,
                                "param", param,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }  
            catch (Exception e) 
            {
                addActionError(e.getMessage() );
                LogFactory.getLog("eprint").error("", e);
                log.error(e.getMessage(),e);
            } 
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            numerWniosku = TextUtils.trimmedStringOrNull(numerWniosku);
        }
    }
        
    private class Validate implements ActionListener 
    {
        private int i;
        Validate(int i)
        {
            this.i = i;
        }
        public void actionPerformed(ActionEvent event) 
        {
            if(i == 1 || i == 2)
            {
                if(numerWniosku == null)
                {
                    addActionError(sm.getString("NumerUmowyJestObowiazkowy"));
                    event.skip(EV_DO);
                    return;
                }
                if(idKlienta == null)
                {
                    addActionError(sm.getString("IdKlientaJestPolemObowiazkowymImusiBycLiczba"));
                    event.skip(EV_DO);
                    return;
                }
                if(idDostawcy==null)
                {
                    addActionError(sm.getString("IdDostawcyJestPolemObowiazkowymImusiBycLiczba"));
                    event.skip(EV_DO);
                    return;
                }
            }
            if( i == 1)
            {
                if (numerWniosku != null)
                {
                	QueryForm form = new QueryForm(offset, LIMIT);
                	form.addProperty("numerWniosku", numerWniosku);
                	SearchResults<? extends DlApplicationDictionary> results = DlApplicationDictionary.search(form);
                	if (results == null || results.totalCount() == 0)
                	{
                		 
                	}
                	else
                	{
                		addActionError(sm.getString("IstniejeJuzUmowaOpodanymNumerze"));
                        event.skip(EV_DO);
                	}
                }
            }
            if(idKlienta != null)
            {
                try
                {
                	
                    idKlientaInt = Long.parseLong(idKlienta);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnyNumerKlienta"));
                    event.skip(EV_DO);
                    return;
                }
            }
        
            if (wartosc != null)
            {
                try
                {
                    String s = wartosc.replace(",", ".");
                    wartoscFloat = Float.parseFloat(s);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnaWartosc"));
                    event.skip(EV_DO);
                    return;
                }
            }
            if ( kwota != null)
            {
                try
                {
                    String s = kwota.replace(",", ".");
                    kwotaFloat = Float.parseFloat(s);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnaKwote"));
                    event.skip(EV_DO);
                    return;
                }
            }
            if ( wykup != null)
            {
                try
                {
                    String s = wykup.replace(",", ".");
                    wykupFloat = Float.parseFloat(s);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnaWartoscWykupu"));
                    event.skip(EV_DO);
                    return;
                }
            }
            if(liczbaRat != null)
            {
                try
                {
                    liczbaRatInt = Integer.parseInt(liczbaRat);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnaLiczbeRat"));
                    event.skip(EV_DO);
                    return;
                }
                          
            }
        }
    }

    public String getDictionaryDescription()
    {
        return dictionaryDescription;
    }

    public void setDictionaryDescription(String dictionaryDescription)
    {
        this.dictionaryDescription = dictionaryDescription;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdKlienta()
    {
        return idKlienta;
    }

    public void setIdKlienta(String idKlienta)
    {
        this.idKlienta = idKlienta;
    }

    public String getKwota()
    {
        return kwota;
    }

    public void setKwota(String kwota)
    {
        this.kwota = kwota;
    }

    public String getLiczbaRat()
    {
        return liczbaRat;
    }

    public void setLiczbaRat(String liczbaRat)
    {
        this.liczbaRat = liczbaRat;
    }

    public String getNumerWniosku()
    {
        return numerWniosku;
    }

    public void setNumerWniosku(String numerWniosku)
    {
        this.numerWniosku = numerWniosku;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public List<? extends DlApplicationDictionary> getResults()
    {
        return results;
    }

    public void setResults(List<? extends DlApplicationDictionary> results)
    {
        this.results = results;
    }

    public String getRodzaj()
    {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj)
    {
        this.rodzaj = rodzaj;
    }

    public String getWartosc()
    {
        return wartosc;
    }

    public void setWartosc(String wartosc)
    {
        this.wartosc = wartosc;
    }

    public String getWykup()
    {
        return wykup;
    }

    public void setWykup(String wykup)
    {
        this.wykup = wykup;
    }

    public List<String> getRodzajList()
    {
        return rodzajList;
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public Map<String, String> getRodzajMap()
    {
        return rodzajMap;
    }

    public void setRodzajMap(Map<String, String> rodzajMap)
    {
        this.rodzajMap = rodzajMap;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public String getNazwaKlienta()
    {
        return nazwaKlienta;
    }

    public void setNazwaKlienta(String nazwaKlienta)
    {
        this.nazwaKlienta = nazwaKlienta;
    }

	public int getOffset()
	{
		return offset;
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public void setIdDostawcy(Long idDostawcy)
	{
		this.idDostawcy = idDostawcy;
	}

	public Long getIdDostawcy()
	{
		return idDostawcy;
	}

	public void setNazwaDostawcy(String nazwaDostawcy)
	{
		this.nazwaDostawcy = nazwaDostawcy;
	}

	public String getNazwaDostawcy()
	{
		return nazwaDostawcy;
	}

	public void setIdUmowy(Long idUmowy)
	{
		this.idUmowy = idUmowy;
	}

	public Long getIdUmowy()
	{
		return idUmowy;
	}

	public void setNumerUmowy(String numerUmowy)
	{
		this.numerUmowy = numerUmowy;
	}

	public String getNumerUmowy()
	{
		return numerUmowy;
	}

	public Long getLeasingObjectType() {
		return leasingObjectType;
	}

	public void setLeasingObjectType(Long leasingObjectType) {
		this.leasingObjectType = leasingObjectType;
	}

	public List<LeasingObjectType> getLeasingObjectTypes() {
		return leasingObjectTypes;
	}

	public void setLeasingObjectTypes(List<LeasingObjectType> leasingObjectTypes) {
		this.leasingObjectTypes = leasingObjectTypes;
	}

	public Map<Long, String> getContractsIdsMap() {
		return contractsIdsMap;
	}

	public void setContractsIdsMap(Map<Long, String> contractsIdsMap) {
		this.contractsIdsMap = contractsIdsMap;
	}

	
}
