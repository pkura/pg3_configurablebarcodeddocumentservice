package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.dictionary.RockwellVendor;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * Akcja s�ownikowa dla s�ownika Instytucji/kontrahent�w (dla dokument�w finansowych).
 * 
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 */

public class RockwellDictionaryAction extends EventActionSupport 
{  
	static final long serialVersionUID = 233244; 
    public static final String URL = "/office/common/rockwellVendor.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String vendorId;
    private String setId;	
    private String shortName;
    private String name;
    private String location;
    private String bankAccountNumber;
    private String bankId;
    private String swiftId;
    private String iban;
    private String address;
    private String city;
    private Integer paymentTerms;
    
    private Long searchid;
    private String searchvendorId;
    private String searchsetId;
    private String searchshortName;
    private String searchname;
    private String searchlocation;
    private String searchbankAccountNumber;
    private String searchbankId;
    private String searchswiftId;
    private String searchiban;
    private String searchaddress;
    private String searchcity;
    private Integer searchpaymentTerms;
       
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private RockwellVendor vendor;
    private Pager pager;
    private int offset;
    private List<? extends RockwellVendor> results;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                /**
                 * @todo - uprawnienia do slownikow
                 */
                canRead = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_EDYTOWANIE);

                if(id != null) 
                {
                    vendor = RockwellVendor.getInstance().find(id);
                    if(vendor!= null) 
                    {
                        vendorId = vendor.getVendorId();
                        setId = vendor.getSetId();
                        shortName = vendor.getShortName();
                        name = vendor.getName();
                        location = vendor.getLocation();
                        bankAccountNumber = vendor.getBankAccountNumber();
                        bankId = vendor.getBankId();
                        swiftId = vendor.getSwiftId();
                        iban = vendor.getIban();
                        address = vendor.getAddress();
                        city = vendor.getCity();
                        paymentTerms = vendor.getPaymentTerms();
                    }
                    
                }                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
            	
            	Criteria c = DSApi.context().session().createCriteria(RockwellVendor.class);            
 	            c.add(Expression.eq("vendorId",vendorId));   
 	            c.add(Expression.eq("setId",setId)); 
 	            List<RockwellVendor> list = (List<RockwellVendor>) c.list();
 	            if(list.size() > 0)
 	            {
 	            	addActionMessage( "Vendor("+vendorId+","+setId+") already exists");
 	            	return;
 	            }
            	
            	
                DSApi.context().begin();

                canRead = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_EDYTOWANIE);
                
                
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                vendor = new RockwellVendor();
                vendor.setVendorId(vendorId);
                vendor.setSetId(setId);
                vendor.setShortName(shortName);
                vendor.setName(name);
                vendor.setLocation(location);
                vendor.setBankAccountNumber(bankAccountNumber);
                vendor.setBankId(bankId);
                vendor.setSwiftId(swiftId);
                vendor.setIban(iban);
                vendor.setAddress(address);
                vendor.setCity(city);
                vendor.setPaymentTerms(paymentTerms);

                vendor.create();

                id = vendor.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",vendor.getName()));
            } 
            catch (EdmException e)
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try 
            {
        		RockwellVendor inst;
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = RockwellVendor.getInstance().find(id);
                
                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ROCKWELL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(RockwellLogic.VENDOR_ID_FIELD_CN);
                dockindQuery.field(f,id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                if (searchResults == null || searchResults.totalCount()<1)
                {
                    inst.delete();
                    id = null;
                    addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getShortName()));
                }
                else
                    throw new EdmException("NieMoznaUsunacTegoVendoraPoniewazSaDoNiegoPrzypisaneDokumenty");
                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {               
            
                DSApi.context().begin();
                canEdit = DSApi.context().hasPermission(DSPermission.ROCKWELL_SLOWNIK_EDYTOWANIE);
                if(!canEdit) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                RockwellVendor inst = RockwellVendor.getInstance().find(id);
                
                if(inst.getVendorId() != null && !inst.getVendorId().equals(vendorId))
                {
                	addActionMessage("You can not modify vendor id");
                }
                if(setId != null && setId.length() > 10)
                {
                	addActionMessage("Set Id can not be longer than 10 characters");
                }

                inst.setAddress(address);
                inst.setBankAccountNumber(bankAccountNumber);
                inst.setBankId(bankId);
                inst.setCity(city);
                inst.setIban(iban);
                inst.setLocation(location);
                inst.setName(name);
                inst.setSetId(StringUtils.left(setId,10));
                inst.setShortName(shortName);
                inst.setSwiftId(swiftId);
                inst.setPaymentTerms(paymentTerms);
               // inst.setVendorId(vendorId);
                
                DSApi.context().session().save(inst);
                
                DSApi.context().commit();
            }
            catch(EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
        }
    }  
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {

                QueryForm form = new QueryForm(offset, LIMIT);

                if(vendorId != null) form.addProperty("vendorId", vendorId);                               
                if(setId != null) form.addProperty("setId", setId);
                if(shortName != null) form.addProperty("shortName", shortName);
                if(name != null) form.addProperty("name", name);
                if(location != null) form.addProperty("location", location);
                if(bankAccountNumber != null) form.addProperty("bankAccountNumber", bankAccountNumber);
                if(bankId != null) form.addProperty("bankId", bankId);
                if(swiftId != null) form.addProperty("swiftId", swiftId);
                if(iban != null) form.addProperty("iban", iban);
                if(address != null) form.addProperty("address", address);
                if(city != null) form.addProperty("city", city);          
                if(paymentTerms != null) form.addProperty("paymentTerms", paymentTerms);  
                form.addOrderAsc("name");

                SearchResults<? extends RockwellVendor> results = RockwellVendor.search(form);

                if (results.totalCount() == 0)  
                {
                    //afterSearch = false;
                    throw new EdmException(sm.getString("NieZnalezionoInstytucjiKontrahentowPasujacychDoWpisanychDanych"));
                }
                else 
                {
                    //afterSearch = true;
                	searchvendorId = vendorId;
                	searchsetId = setId;
                	searchshortName = shortName;
                	searchname = name;
                	searchlocation = location;
                	searchbankAccountNumber = bankAccountNumber;
                	searchbankId = bankId;
                	searchswiftId = swiftId;
                	searchiban = iban;
                	searchaddress = address;
                	searchcity = city;
                	searchpaymentTerms = paymentTerms;
                }
                RockwellDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "vendorId", vendorId,                               
                                "setId", setId,
                                "shortName", shortName,
                                "name", name,
                                "bankAccountNumber", bankAccountNumber,
                                "bankId", bankId,
                                "swiftId", swiftId,
                                "iban", iban,
                                "address", address,
                                "city", city,
                                "param",param,
                                
                                //"afterSearch", afterSearch ? "true" : "false",
                                "searchvendorId", searchvendorId,                               
                                "searchsetId", searchsetId,
                                "searchshortName", searchshortName,
                                "searchname", searchname,
                                "searchbankAccountNumber", searchbankAccountNumber,
                                "searchbankId", searchbankId,
                                "searchswiftId", searchswiftId,
                                "searchiban", searchiban,
                                "searchaddress", searchaddress,
                                "searchcity", searchcity,
                                "searchpaymentTerms",searchpaymentTerms,
                                
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                vendorId = null;
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            setId = TextUtils.trimmedStringOrNull(setId);            
            shortName = TextUtils.trimmedStringOrNull(shortName);
            name = TextUtils.trimmedStringOrNull(name);
            location = TextUtils.trimmedStringOrNull(location);
            bankAccountNumber = TextUtils.trimmedStringOrNull(bankAccountNumber);
            bankId = TextUtils.trimmedStringOrNull(bankId);
            swiftId = TextUtils.trimmedStringOrNull(swiftId);
            iban = TextUtils.trimmedStringOrNull(iban);
            address = TextUtils.trimmedStringOrNull(address);
            city = TextUtils.trimmedStringOrNull(city);
 
        }
    }
        
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        
        return lista;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getSetId() {
		return setId;
	}

	public void setSetId(String setId) {
		this.setId = setId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getSwiftId() {
		return swiftId;
	}

	public void setSwiftId(String swiftId) {
		this.swiftId = swiftId;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getSearchid() {
		return searchid;
	}

	public void setSearchid(Long searchid) {
		this.searchid = searchid;
	}

	public String getSearchvendorId() {
		return searchvendorId;
	}

	public void setSearchvendorId(String searchvendorId) {
		this.searchvendorId = searchvendorId;
	}

	public String getSearchsetId() {
		return searchsetId;
	}

	public void setSearchsetId(String searchsetId) {
		this.searchsetId = searchsetId;
	}

	public String getSearchshortName() {
		return searchshortName;
	}

	public void setSearchshortName(String searchshortName) {
		this.searchshortName = searchshortName;
	}

	public String getSearchname() {
		return searchname;
	}

	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}

	public String getSearchlocation() {
		return searchlocation;
	}

	public void setSearchlocation(String searchlocation) {
		this.searchlocation = searchlocation;
	}

	public String getSearchbankAccountNumber() {
		return searchbankAccountNumber;
	}

	public void setSearchbankAccountNumber(String searchbankAccountNumber) {
		this.searchbankAccountNumber = searchbankAccountNumber;
	}

	public String getSearchbankId() {
		return searchbankId;
	}

	public void setSearchbankId(String searchbankId) {
		this.searchbankId = searchbankId;
	}

	public String getSearchswiftId() {
		return searchswiftId;
	}

	public void setSearchswiftId(String searchswiftId) {
		this.searchswiftId = searchswiftId;
	}

	public String getSearchiban() {
		return searchiban;
	}

	public void setSearchiban(String searchiban) {
		this.searchiban = searchiban;
	}

	public String getSearchaddress() {
		return searchaddress;
	}

	public void setSearchaddress(String searchaddress) {
		this.searchaddress = searchaddress;
	}

	public String getSearchcity() {
		return searchcity;
	}

	public void setSearchcity(String searchcity) {
		this.searchcity = searchcity;
	}

	public RockwellVendor getVendor() {
		return vendor;
	}

	public void setVendor(RockwellVendor vendor) {
		this.vendor = vendor;
	}

	public List<? extends RockwellVendor> getResults() {
		return results;
	}

	public void setResults(List<? extends RockwellVendor> results) {
		this.results = results;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static String getURL() {
		return URL;
	}

	public Boolean getCanEdit() {
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	public Integer getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Integer getSearchpaymentTerms() {
		return searchpaymentTerms;
	}

	public void setSearchpaymentTerms(Integer searchpaymentTerms) {
		this.searchpaymentTerms = searchpaymentTerms;
	}
      
   
}
