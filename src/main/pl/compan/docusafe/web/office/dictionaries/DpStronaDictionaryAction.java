package pl.compan.docusafe.web.office.dictionaries;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.DpStrona;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.Pager;

import std.fun;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 */
public class DpStronaDictionaryAction extends EventActionSupport 
{    
    public static final String URL = "/office/common/strona.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String sygnatura;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;
    
    private Long searchid;
    private String searchimie;
    private String searchnazwisko;
    private String searchsygnatura;
    private String searchulica;
    private String searchkod;
    private String searchmiejscowosc;
    private String searchtelefon;
    private String searchfaks;
    private String searchemail;
    private String searchkraj;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canModify;
    
    private DpStrona strona;
    private Pager pager;
    private int offset;
    private List<? extends DpStrona> results;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canRead = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_USUWANIE);
                canModify = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_MODYFIKACJA);

                if (id != null) 
                {
                    strona = DpStrona.getInstance().find(id);
                    if (strona != null) 
                    {
                        imie = strona.getImie();
                        nazwisko = strona.getNazwisko();
                        sygnatura = strona.getSygnatura();
                        ulica = strona.getUlica();
                        kod = strona.getKod();
                        miejscowosc = strona.getMiejscowosc();
                        telefon = strona.getTelefon();
                        faks = strona.getFaks();
                        email = strona.getEmail();
                        kraj = strona.getKraj();
                    }
                    else 
                        kraj = "PL";
                }
                else 
                    kraj = "PL";
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }

        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canAdd = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

                List<String> lista = Validate();
                if (lista.size()>0)
                {
                    for(int i=0; i < lista.size()-1; i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                strona = new DpStrona();
                strona.setImie(imie);
                strona.setNazwisko(nazwisko);
                strona.setSygnatura(sygnatura);
                strona.setUlica(ulica);
                strona.setKod(kod);
                strona.setMiejscowosc(miejscowosc);
                strona.setEmail(email);
                strona.setKraj(kraj);
                strona.setFaks(faks);
                strona.setTelefon(telefon);
                strona.create();

                id = strona.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",strona.getSygnatura()));
            } 
            catch (EdmException e)
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {                           
                DSApi.context().begin();
                
                DpStrona strona = DpStrona.getInstance().find(id);
                strona.setImie(imie);
                strona.setNazwisko(nazwisko);
                strona.setSygnatura(sygnatura);
                strona.setUlica(ulica);
                strona.setKod(kod);
                strona.setMiejscowosc(miejscowosc);
                strona.setEmail(email);
                strona.setKraj(kraj);
                strona.setFaks(faks);
                strona.setTelefon(telefon);
                
                DSApi.context().session().save(strona);
                
                DSApi.context().commit();
            }
            catch(EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
        }
    }  
         
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZeSlownika"));

                strona = DpStrona.getInstance().find(id);
                
                
                DockindQuery dockindQuery = new DockindQuery(0, 10);
                DocumentKind kind  = DocumentKind.findByCn("dp");
                dockindQuery.setDocumentKind(kind);
                Field field = kind.getFieldByCn("STRONA");
                dockindQuery.field(field, id);
                SearchResults searchResults = DocumentKindsManager.search(dockindQuery);
                int x = searchResults.count();
                
                if (x>0)
                    throw new EdmException(sm.getString("NieMoznaUsunacTejStronyPoniewazSaDoNiejPrzypisaneDokumenty"));
                                 
                strona.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",strona.getSygnatura()));
                    
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if(imie != null) form.addProperty("imie", imie);
                if(nazwisko != null) form.addProperty("nazwisko", nazwisko);
                if(sygnatura != null) form.addProperty("sygnatura", sygnatura);
                if(ulica != null) form.addProperty("ulica", ulica);
                if(kod != null) form.addProperty("kod", kod);
                if(miejscowosc != null) form.addProperty("miejscowosc", miejscowosc);
                if(telefon != null) form.addProperty("telefon", telefon);
                if(faks != null) form.addProperty("faks", faks);
                if(email != null) form.addProperty("email", email);
                if(kraj != null) form.addProperty("kraj", kraj);
                

                form.addOrderAsc("sygnatura");

                SearchResults<? extends DpStrona> results = DpStrona.search(form);

                if (results.totalCount() == 0)  
                {
                    //afterSearch = false;
                    throw new EdmException(sm.getString("NieZnalezionoStronPasujacychDoWpisanychDanych"));
                }
                else 
                {
                    //afterSearch = true;
                    searchimie = imie;
                    searchnazwisko = nazwisko;
                    searchsygnatura = sygnatura;
                    searchulica = ulica;
                    searchkod= kod;
                    searchmiejscowosc= miejscowosc;
                    searchtelefon= telefon;
                    searchfaks = faks;
                    searchemail = email;
                    searchkraj = kraj;
                }
                DpStronaDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "imie", imie,
                                "nazwisko", nazwisko,
                                "sygnatura", sygnatura,
                                "ulica", ulica,
                                "kod", kod,
                                "miejscowosc", miejscowosc,
                                "telefon", telefon,
                                "faks", faks,
                                "email", email,
                                "kraj", kraj,
                                //"afterSearch", afterSearch ? "true" : "false",
                                "searchimie", searchimie,
                                "searchnazwisko", searchnazwisko,
                                "searchsygnatura", searchsygnatura,
                                "searchulica", searchulica,
                                "searchkod", searchkod,
                                "searchmiejscowosc", searchmiejscowosc,
                                "searchtelefon", searchtelefon,
                                "searchfaks", searchfaks,
                                "searchemail", searchemail,
                                "searchkraj", searchkraj,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            imie = TextUtils.trimmedStringOrNull(imie);
            nazwisko = TextUtils.trimmedStringOrNull(nazwisko);
            sygnatura = TextUtils.trimmedStringOrNull(sygnatura);
            ulica = TextUtils.trimmedStringOrNull(ulica);
            kod = TextUtils.trimmedStringOrNull(kod);
            miejscowosc = TextUtils.trimmedStringOrNull(miejscowosc);
            telefon = TextUtils.trimmedStringOrNull(telefon);
            faks = TextUtils.trimmedStringOrNull(faks);
            email = TextUtils.trimmedStringOrNull(email);
            kraj  = TextUtils.trimmedStringOrNull(kraj);  
        }
    }
        
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(imie))
            lista.add("Nie podano imienia");
        if (StringUtils.isEmpty(nazwisko))
            lista.add("Nie podano nazwisko");
        if (StringUtils.isEmpty(sygnatura))
            lista.add("Nie podano sygnatury");
        return lista;
    }
     
    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getFaks() 
    {
        return faks;
    }

    public void setFaks(String faks) 
    {
        this.faks = faks;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getImie() 
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getKod() 
    {
        return kod;
    }

    public void setKod(String kod) 
    {
        this.kod = kod;
    }

    public String getKraj() 
    {
        return kraj;
    }

    public void setKraj(String kraj) 
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc() 
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) 
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko() 
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) 
    {
        this.nazwisko = nazwisko;
    }

    public String getSygnatura() 
    {
        return sygnatura;
    }

    public void setSygnatura(String sygnatura) 
    {
        this.sygnatura = sygnatura;
    }

    public String getTelefon() 
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public String getUlica() 
    {
        return ulica;
    }

    public void setUlica(String ulica) 
    {
        this.ulica = ulica;
    }

    public String getSearchemail() 
    {
        return searchemail;
    }

    public void setSearchemail(String searchemail) 
    {
        this.searchemail = searchemail;
    }

    public String getSearchfaks() 
    {
        return searchfaks;
    }

    public void setSearchfaks(String searchfaks) 
    {
        this.searchfaks = searchfaks;
    }

    public Long getSearchid() 
    {
        return searchid;
    }

    public void setSearchid(Long searchid) 
    {
        this.searchid = searchid;
    }

    public String getSearchimie()
    {
        return searchimie;
    }

    public void setSearchimie(String searchimie) 
    {
        this.searchimie = searchimie;
    }

    public String getSearchkod() 
    {
        return searchkod;
    }

    public void setSearchkod(String searchkod) 
    {
        this.searchkod = searchkod;
    }

    public String getSearchkraj() 
    {
        return searchkraj;
    }

    public String getSearchmiejscowosc() 
    {
        return searchmiejscowosc;
    }

    public void setSearchmiejscowosc(String searchmiejscowosc) 
    {
        this.searchmiejscowosc = searchmiejscowosc;
    }

    public String getSearchnazwisko() 
    {
        return searchnazwisko;
    }

    public void setSearchnazwisko(String searchnazwisko) 
    {
        this.searchnazwisko = searchnazwisko;
    }

    public String getSearchsygnatura() 
    {
        return searchsygnatura;
    }

    public void setSearchsygnatura(String searchsygnatura) 
    {
        this.searchsygnatura = searchsygnatura;
    }

    public String getSearchtelefon() 
    {
        return searchtelefon;
    }

    public void setSearchtelefon(String searchtelefon) 
    {
        this.searchtelefon = searchtelefon;
    }

    public String getSearchulica() 
    {
        return searchulica;
    }

    public void setSearchulica(String searchulica) 
    {
        this.searchulica = searchulica;
    }
    
    public Boolean getCanAdd() 
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd) 
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete() 
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) 
    {
        this.canDelete = canDelete;
    }

    public void setSearchkraj(String searchkraj) 
    {
        this.searchkraj = searchkraj;
    }

    public DpStrona getStrona() 
    {
        return strona;
    }

    public void setStrona(DpStrona strona) 
    {
        this.strona = strona;
    }

    public Boolean getCanRead() 
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead) 
    {
        this.canRead = canRead;
    }

    public int getOffset() 
    {
        return offset;
    }

    public void setOffset(int offset) 
    {
        this.offset = offset;
    }

    public Pager getPager() 
    {
        return pager;
    }

    public void setPager(Pager pager) 
    {
        this.pager = pager;
    }

    public List<? extends DpStrona> getResults() 
    {
        return results;
    }

    public void setResults(List<? extends DpStrona> results) 
    {
        this.results = results;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

	public Boolean getCanModify() {
		return canModify;
	}

	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}
}
