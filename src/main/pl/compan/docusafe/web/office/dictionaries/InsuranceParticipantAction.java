package pl.compan.docusafe.web.office.dictionaries;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.InsuranceParticipant;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.Results;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.*;

/**
 *
 * @author Micha� Sankowski
 */
public class InsuranceParticipantAction extends EventActionSupport{
	private final static Logger LOG = LoggerFactory.getLogger(InsuranceParticipantAction.class);

	private String param;

	private Long id;
	private String name;
	private String surname;
	private String email;
	private String pesel;
	private String oc;
	private String ac;
	private Results results;

	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_SESSION)
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doAdd")
			.append(OPEN_HIBERNATE_SESSION)
			.append(new Add())
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doSearch")
			.append(OPEN_HIBERNATE_SESSION)
			.append(new Search())
			.appendFinally(CLOSE_HIBERNATE_SESSION);
	}

	

	class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			if(id != null){
				InsuranceParticipant ip
					= (InsuranceParticipant) DSApi.context().session()
						.createCriteria(InsuranceParticipant.class)
						.add(Restrictions.eq("id",id))
						.uniqueResult();
				name = ip.getName();
				surname = ip.getSurname();
				email = ip.getEmail();
				pesel = ip.getPesel();
				ac = ip.getAc();
				oc = ip.getOc();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	class Add extends TransactionalActionListener {

		InsuranceParticipant ip = null;

		@Override
		public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
			if(StringUtils.isBlank(name) || StringUtils.isBlank(surname)){
				throw new EdmException("Imi� i nazwisko musi by� uzupe�nione");
			}
		}

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			ip = new InsuranceParticipant();
			ip.setName(name);
			ip.setSurname(surname);
			ip.setPesel(getPesel());
			ip.setEmail(getEmail());
			ip.setAc(ac);
			ip.setOc(oc);
			DSApi.context().session().save(ip);
		}

		@Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			id = ip.getId();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	class Search extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			results = new Results();
			results.header("Imi�", "Nazwisko","PESEL", "Email", "AC", "OC");

			Criteria crit = DSApi.context().session()
						.createCriteria(InsuranceParticipant.class);

			if(name != null) crit.add(Restrictions.eq("name", name));
			if(surname != null) crit.add(Restrictions.eq("surname", surname));
			if(ac != null) crit.add(Restrictions.eq("ac", ac));
			if(oc != null) crit.add(Restrictions.eq("oc", oc));
			if(email != null) crit.add(Restrictions.eq("email", email));

			for(InsuranceParticipant ip:
				(Iterable<InsuranceParticipant>) crit.list()){
				results.entry("/office/common/insurance-participant.action", "param="+param+"&id="+ip.getId(),
					ip.getName(), ip.getSurname(), ip.getPesel(), ip.getEmail(), ip.getAc(), ip.getOc());
			}
			if(!results.isEmpty()){
				event.setResult("list");
			} else {
				event.addActionError("Brak wpis�w pasuj�cych do wprowadzonych danych");
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Results getResults() {
		return results;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pesel
	 */
	public String getPesel() {
		return pesel;
	}

	/**
	 * @param pesel the pesel to set
	 */
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	/**
	 * @return the oc
	 */
	public String getOc() {
		return oc;
	}

	/**
	 * @param oc the oc to set
	 */
	public void setOc(String oc) {
		this.oc = oc;
	}

	/**
	 * @return the ac
	 */
	public String getAc() {
		return ac;
	}

	/**
	 * @param ac the ac to set
	 */
	public void setAc(String ac) {
		this.ac = ac;
	}



	public void setResults(Results results) {
		this.results = results;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	
}
