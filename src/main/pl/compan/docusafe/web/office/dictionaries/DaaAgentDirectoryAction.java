package pl.compan.docusafe.web.office.dictionaries;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.Daa;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;
import std.fun;

/**
 * Akcja s�ownikowa dla s�ownika agent�w (w dokumentach archiwum agenta).
 * 
 * @author Bartlomiej Spychalski mailto:bsp@spychalski.eu
 */
public class DaaAgentDirectoryAction extends EventActionSupport 
{
    public static final String URL = "/office/common/agent.action";
    private static final Log log = LogFactory.getLog(DaaAgentDirectoryAction.class);
    public static final int LIMIT = 15;

    private DaaAgent agent;

    private Long id; //identyfikator agenta
    private String imie; //imie agenta
    private String nazwisko; //nazwisko agenta
    private String numer;
    private Boolean setOfDocuments;
    private Long agencja_id;
    private String agencja_nazwa;
    private String agencja_numer;
    private String agencja_nip;
    private Integer rodzaj_sieci;

    private String searchNazwisko;
    private String searchImie;
    private String searchNumer;
    private Long searchAgencja_id;

    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;

    private DaaAgencja agencja; //agetncja agenta

    private List<DaaAgencja> agencja_lista;

    private List<? extends DaaAgent> results;
    private boolean closeOnReload;

    private boolean canEdit;
    //private boolean canRead;
    private boolean canDelete;
    private boolean canAddAndSubmit;
    private boolean afterSearch;

    private int offset;
    private int limit = LIMIT;
    private Pager pager;

    protected void setup() 
    {
        // domyslna akcja wyswietlajaca dane
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    /**
     * wyswietlenie danych
     */
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

              /*  canRead = DSApi.context().hasPermission(DSPermission.DAA_ODCZYT);
                if(!canRead) throw new EdmException("Brak uprawnien do odczytu danych");     */

                canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_USUWANIE);

                if (id != null ) 
                {
                    agent = new DaaAgent().find(id);
                    if (agent != null) 
                    {
                        imie = agent.getImie();
                        nazwisko = agent.getNazwisko();
                        agencja = agent.getAgencja();
                        numer = agent.getNumer();
                        setOfDocuments = agent.getSetOfDocuments();
                        if (agent.getAgencja() != null) 
                        {
                            agencja_id = agencja.getId();
                            agencja_nazwa = agencja.getNazwa();
                            agencja_nip = agencja.getNip();
                            agencja_numer = agencja.getNumer();
                            rodzaj_sieci = agencja.getRodzaj_sieci();
                        }
                    }
                }

                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * usuniecie rekordu agenta
     */
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();
                canDelete = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_USUWANIE);
                if (!canDelete)
                    throw new EdmException("Brak uprawnie� do usuwanie ze s�ownika");

                agent = new DaaAgent().find(id);

                Document.DoctypeQuery doctypeQuery = new Document.DoctypeQuery(0, 10);
                Doctype doctype = Doctype.findByCn("daa");
                doctypeQuery.doctype(doctype.getId());
                doctypeQuery.field(doctype.getFieldByCn("AGENT").getId(), id);

                SearchResults searchResults = Document.searchByDoctype(doctypeQuery);
                if (searchResults.totalCount() != 0)
                    addActionError("Nie mo�na usun�� ze s�ownika agenta "+agent.getImie()+" "+agent.getNazwisko()+", poniewa� s� do niego przypisane dokumenty");
                else 
                {
                    agent.delete();
                    id = null;
                    addActionMessage("Ze s�ownika usuni�to "+agent.getImie()+" "+agent.getNazwisko());
                }

                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            nazwisko = TextUtils.trimmedStringOrNull(nazwisko);
            imie = TextUtils.trimmedStringOrNull(imie);
            numer = TextUtils.trimmedStringOrNull(numer);
        }
    }

    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if (StringUtils.isEmpty(nazwisko))
                addActionError("Nie podano nazwiska");
            if (StringUtils.isEmpty(imie))
                addActionError("Nie podano imienia");
            if (StringUtils.isEmpty(numer))
                addActionError("Nie podano numeru");
            if (agencja_id == null)
                addActionError("Nie wybrano agencji");

            if (hasActionErrors())
                event.cancel();
        }
    }

    /**
     * utworzenie rekordu agenta
     */
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                if (imie != null || nazwisko != null) 
                {
                    DSApi.context().begin();
                    canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                    if (!canEdit)
                        throw new EdmException("Brak uprawnie� do modyfikacji s�ownika");

                    agent = new DaaAgent();
                    agent.setImie(imie);
                    agent.setNazwisko(nazwisko);
                    agent.setNumer(numer);

                    if (agencja_id != null) 
                    {
                        agencja = new DaaAgencja().find(agencja_id);
                        if (agencja != null) 
                        {
                            agent.setAgencja(agencja);
                        }
                    }

                    agent.create();

                    id = agent.getId();

                    DSApi.context().commit();

                    addActionMessage("W s�owniku utworzono "+agent.getImie()+" "+agent.getNazwisko());
                }

            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    /**
     * modyfikacja rekordu agenta
     */
    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();
                canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                if (!canEdit)
                    throw new EdmException("Brak uprawnie� do modyfikacji s�ownika");

                agent = new DaaAgent().find(id);
                agent.setImie(imie);
                agent.setNazwisko(nazwisko);
                agent.setNumer(numer);
                agent.setSetOfDocuments(setOfDocuments);

                if (agencja_id != null) 
                {
                    agencja = new DaaAgencja().find(agencja_id);
                    if(agencja != null) 
                    {
                        agent.setAgencja(agencja);
                    }
                }
        
                /* ponowna archiwizacja dokument�w przypisanych do zmienianego agenta */
                Document.DoctypeQuery doctypeQuery = new Document.DoctypeQuery(0, 0);
                Doctype doctype = Doctype.findByCn("daa");
                doctypeQuery.doctype(doctype.getId());
                doctypeQuery.field(doctype.getFieldByCn("AGENT").getId(), id);

                SearchResults<Document> searchResults = Document.searchByDoctype(doctypeQuery);
                for (Document document : searchResults.results())
                {
                    Map doctypeFields = doctype.getValueMap(document.getId());

                    Integer rodzaj_sieci = agencja != null ? agencja.getRodzaj_sieci() : (new Integer(doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId()).toString()));
                    Doctype.EnumItem siecEnum = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItem(rodzaj_sieci);
                    Doctype.EnumItem typEnum = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
                    String klasaId = doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()).toString() : null;
                    Doctype.EnumItem klasaRaportuEnum = klasaId != null ? doctype.getFieldByCn("KLASA_RAPORTU").getEnumItem(new Integer(klasaId)) : null;

                    doctype.set(document.getId(),doctype.getFieldByCn("RODZAJ_SIECI").getId(), rodzaj_sieci);
                    doctype.set(document.getId(),doctype.getFieldByCn("AGENCJA").getId(), agencja_id);

                    Daa.daaDocument(document, agent,  agencja,  siecEnum, typEnum.getTitle(), typEnum.getArg1(), klasaRaportuEnum, false);
                    if (document instanceof OfficeDocument)
                        ((OfficeDocument) document).setSummary(document.getDescription());
                }

                DSApi.context().commit();

                addActionMessage("W s�owniku zapisano zmiany");
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e.getMessage());
                }
            }
            
            try
            {
            	DSApi.context().begin();
            	
            	agent = new DaaAgent().find(id);
            	agent.changeFolders();
            	
            	DSApi.context().commit();
            }
            catch (Exception e) 
            {
				e.printStackTrace();
				DSApi.context()._rollback();
			}
        }

    }

    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, limit);

                if (imie != null) form.addProperty("imie", imie);
                if (nazwisko != null) form.addProperty("nazwisko", nazwisko);
                if (numer != null) form.addProperty("numer", numer);
                if (agencja_id != null)
                {
                    agencja = new DaaAgencja().find(agencja_id);
                    if(agencja != null) form.addProperty("agencja", agencja);
                }
                if (setOfDocuments != null) form.addProperty("setOfDocuments", setOfDocuments);

                form.addOrderAsc("nazwisko");

                SearchResults<? extends DaaAgent> results = DaaAgent.search(form);

                if (results.totalCount() == 0) 
                {
                    afterSearch = false;
                    addActionMessage("Nie znaleziono agent�w pasuj�cych do wpisanych danych");
                }
                else 
                {
                    afterSearch = true;
                    searchNazwisko = nazwisko;
                    searchImie = imie;
                    searchNumer = numer;
                    searchAgencja_id = agencja_id;
                }


                DaaAgentDirectoryAction.this.results = fun.list(results);

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] {
                            "doSearch", "true",
                            "nazwisko", nazwisko,
                            "imie", imie,
                            "numer", numer,
                            "agencja_id", agencja_id,
                            "afterSearch", afterSearch ? "true" : "false",
                            "searchNazwisko", searchNazwisko,
                            "searchImie", searchImie,
                            "searchNumer", searchNumer,
                            "searchAgencja_id", searchAgencja_id,
                            "param", param,
                            "offset", String.valueOf(offset)
                        });
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public DaaAgent getAgent() 
    {
        return agent;
    }

    public void setAgent(DaaAgent agent) 
    {
        this.agent = agent;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getImie() 
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getNazwisko() 
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) 
    {
        this.nazwisko = nazwisko;
    }

    public DaaAgencja getAgencja() 
    {
        return agencja;
    }

    public void setAgencja(DaaAgencja agencja) 
    {
        this.agencja = agencja;
    }

    public List<DaaAgencja> getAgencja_lista() 
    {
        return agencja_lista;
    }

    public void setAgencja_lista(List<DaaAgencja> agencja_lista) 
    {
        this.agencja_lista = agencja_lista;
    }

    public List<? extends DaaAgent> getResults() 
    {
        return results;
    }

    public void setResults(List<? extends DaaAgent> results) 
    {
        this.results = results;
    }

    public boolean isCloseOnReload() 
    {
        return closeOnReload;
    }

    public void setCloseOnReload(boolean closeOnReload) 
    {
        this.closeOnReload = closeOnReload;
    }

    public String getNumer() 
    {
        return numer;
    }

    public void setNumer(String numer) 
    {
        this.numer = numer;
    }
    
    public Boolean getSetOfDocuments()
	{
		return setOfDocuments;
	}
    
    public void setSetOfDocuments(Boolean setOfDocuments)
	{
		this.setOfDocuments = setOfDocuments;
	}

    public Long getAgencja_id() 
    {
        return agencja_id;
    }

    public void setAgencja_id(Long agencja_id) 
    {
        this.agencja_id = agencja_id;
    }

    public String getAgencja_nazwa() 
    {
        return agencja_nazwa;
    }

    public void setAgencja_nazwa(String agencja_nazwa) 
    {
        this.agencja_nazwa = agencja_nazwa;
    }

    public String getAgencja_numer() 
    {
        return agencja_numer;
    }

    public void setAgencja_numer(String agencja_numer) 
    {
        this.agencja_numer = agencja_numer;
    }

    public String getAgencja_nip() 
    {
        return agencja_nip;
    }

    public void setAgencja_nip(String agencja_nip) 
    {
        this.agencja_nip = agencja_nip;
    }
    public boolean isCanEdit() 
    {
        return canEdit;
    }

    public String getSearchImie() 
    {
        return searchImie;
    }

    public void setSearchImie(String searchImie) 
    {
        this.searchImie = searchImie;
    }

    public String getSearchNazwisko() 
    {
        return searchNazwisko;
    }

    public void setSearchNazwisko(String searchNazwisko) 
    {
        this.searchNazwisko = searchNazwisko;
    }

    public String getSearchNumer() 
    {
        return searchNumer;
    }

    public void setSearchNumer(String searchNumer) 
    {
        this.searchNumer = searchNumer;
    }

    public Long getSearchAgencja_id() 
    {
        return searchAgencja_id;
    }

    public void setSearchAgencja_id(Long searchAgencja_id) 
    {
        this.searchAgencja_id = searchAgencja_id;
    }

    public void setCanEdit(boolean canEdit) 
    {
        this.canEdit = canEdit;
    }

    public boolean isCanDelete() 
    {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) 
    {
        this.canDelete = canDelete;
    }

    public boolean isCanAddAndSubmit() 
    {
        return canAddAndSubmit;
    }

    public void setCanAddAndSubmit(boolean canAddAndSubmit) 
    {
        this.canAddAndSubmit = canAddAndSubmit;
    }

    public Integer getRodzaj_sieci() 
    {
        return rodzaj_sieci;
    }

    public void setRodzaj_sieci(Integer rodzaj_sieci) 
    {
        this.rodzaj_sieci = rodzaj_sieci;
    }

    public boolean isAfterSearch() 
    {
        return afterSearch;
    }

    public void setAfterSearch(boolean afterSearch) 
    {
        this.afterSearch = afterSearch;
    }

    public Pager getPager()
    {
        return pager;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }
}
