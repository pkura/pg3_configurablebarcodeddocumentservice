package pl.compan.docusafe.web.office.dictionaries;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.CustomsAgency;
import pl.compan.docusafe.core.dockinds.dictionary.CustomsAgencyDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import static pl.compan.docusafe.webwork.event.SessionControl.*;

/**
 *
 * @author Micha� Sankowski
 */
public class CustomsAgencyDictionaryAction extends EventActionSupport{

	private final static Logger LOG = LoggerFactory.getLogger(CustomsAgencyDictionaryAction.class);

	private Integer id;
	private String entryName;
	private List<CustomsAgency> list;

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_SESSION).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doCreate").
            append(OPEN_HIBERNATE_SESSION).
			append(new Create()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doEdit").
            append(OPEN_HIBERNATE_SESSION).
			append(new Edit()).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doPrepareCreate");//nie r�b nic po prostu wy�wietl formularz
	}

    private final static String isoToUtfHack(String string) throws Exception{
		return new String(string.getBytes("ISO-8859-2"),"utf-8");
	}

	private class FillForm extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			list = CustomsAgencyDictionary.INSTANCE.getAll();
			event.setResult("list");
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	private class Create extends TransactionalActionListener{
		@Override
		public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
			if(StringUtils.isBlank(entryName)){
				throw new EdmException("Nazwa nie mo�e by� pusta");
			}
		}

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			CustomsAgencyDictionary.INSTANCE.add(new CustomsAgency(entryName));
		}

		@Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			addActionMessage("Dodano wpis");
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	private class Edit extends TransactionalActionListener{
		@Override
		public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
			if(StringUtils.isBlank(entryName)){
				throw new EdmException("Nazwa nie mo�e by� pusta");
			}
		}

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			CustomsAgencyDictionary.INSTANCE.get(id).setTitle(entryName);
		}

		@Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			addActionMessage("Zmieniono wpis");
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEntryName() {
		return entryName;
	}

	public void setEntryName(String entryName) {
        try {
		    this.entryName = isoToUtfHack(entryName);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
	}

	public List<CustomsAgency> getList(){
		return list;
	}
}
