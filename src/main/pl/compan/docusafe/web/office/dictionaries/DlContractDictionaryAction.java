package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.LeasingObjectType;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * Akcja s�ownika dla um�w dokument�w leasingowych.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class DlContractDictionaryAction extends EventActionSupport
{

    public static final String URL = "/office/common/dlContractDictionary.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String numerUmowy;
    private Long idKlienta;
    private Long idDostawcy;
    private Long idWniosku;
    private String opisUmowy;
    private String wartoscUmowy;
    private Float wartoscUmowyFloat;
    private String dataUmowy;
    private String dataKoncaUmowy;
    private String dictionaryDescription;
    private String status;
    private Integer stan;
   
    
    private Long    searchid;
    private String  searchnumerUmowy;
    private Integer  searchidKlienta;
    private String  searchopisUmowy;
    private Float  searchwartoscUmowy;
    private String  searchdataUmowy;
    private String  searchdataKoncaUmowy;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private String nazwaKlienta;
    private String nazwaDostawcy;
    private String numerWniosku;
    private DlContractDictionary inst;
    private Pager pager;
    private int offset;
    private List<? extends DlContractDictionary> results;
    private static final String EV_DO = "do";
    private List<String> statusList = new ArrayList<String>();
    private Map<String,String> statusMap = new HashMap<String,String>();
    private Map<Integer,String> stanMap  = new HashMap<Integer,String>();
	private static Contractor tmpContractor = new Contractor();
    private List<LeasingObjectType> leasingObjectTypes;
    private Long leasingObjectType;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;

    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(EV_DO, new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(EV_DO, new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            //append(new Validate()).
            append(EV_DO, new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                statusList.addAll(DlContractDictionary.getStatusList());
                leasingObjectTypes = LeasingObjectType.list();
                for (Iterator iter = statusList.iterator(); iter.hasNext();)
                {
                    String element = (String) iter.next();
                    statusMap.put(element,sm.getString(element));
                }
                stanMap = DlContractDictionary.stanList;
                DSApi.context().begin();

                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);

                if (id != null) 
                {
                    inst = DlContractDictionary.getInstance().find(id);
                    if (inst != null) 
                    {
                        numerUmowy = inst.getNumerUmowy();
                        idKlienta = inst.getIdKlienta();
                        if(idKlienta != null)
                        {
                            
                            nazwaKlienta = tmpContractor.find(Long.parseLong(idKlienta.toString())).getName();
                        }
                        if (inst.getIdDostawcy() != null)
                        {
                            idDostawcy = inst.getIdDostawcy();
                            if(idDostawcy != null)
                            {
                                nazwaDostawcy = tmpContractor.find(idDostawcy).getName();
                            }
                        }
                        if(inst.getApplicationId() != null)
                        {
                        	DlApplicationDictionary dla = DlApplicationDictionary.getInstance().find(inst.getApplicationId());
                        	idWniosku = dla.getId();
                        	numerWniosku = dla.getNumerWniosku();
                        }
                        opisUmowy = inst.getOpisUmowy();
                        if (inst.getWartoscUmowy() != null)
                            wartoscUmowy = inst.getWartoscUmowy().toString();
                        if (inst.getDataUmowy() != null)
                            dataUmowy = DateUtils.formatJsDate(inst.getDataUmowy());
                        if (inst.getDataKoncaUmowy() != null)
                            dataKoncaUmowy = DateUtils.formatJsDate(inst.getDataKoncaUmowy());
                        status = inst.getStatus();
                        stan = inst.getStan();
                        dictionaryDescription = inst.getDictionaryDescription();
                        leasingObjectType = inst.getLeasingObjectType();
                    }
                }

                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                inst = new DlContractDictionary();
                setValue(inst);              
                inst.create();
                id = inst.getId();
                DSApi.context().commit();
                addActionMessage(sm.getString("WslownikuUtworzono",numerUmowy));
            } 
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").error("", e);
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  
    
    private void setValue(DlContractDictionary inst) throws EdmException
    {
    	  inst.setIdKlienta(idKlienta);
          inst.setNumerUmowy(numerUmowy);
          inst.setOpisUmowy(opisUmowy);
          inst.setWartoscUmowy(wartoscUmowyFloat);
          if (dataUmowy != null)
              inst.setDataUmowy(DateUtils.nullSafeParseJsDate(dataUmowy));
          if (dataKoncaUmowy != null)
              inst.setDataKoncaUmowy(DateUtils.nullSafeParseJsDate(dataKoncaUmowy));
          inst.setStatus(status);
          if(idWniosku != null)
          {
        	  inst.setApplicationId(idWniosku);
          }
          inst.setIdDostawcy(idDostawcy);
          inst.setStan(stan);
          inst.setLeasingObjectType(leasingObjectType);
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.context().begin();
                DlContractDictionary inst = DlContractDictionary.getInstance().find(id);
                setValue(inst);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            	LogFactory.getLog("eprint").error("", e);
            }
        }
    }  
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	/*
            try 
            {
                DSApi.context().begin();

                canDelete =DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                DocumentKind kind = DocumentKind.findByCn(DocumentKind.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.NUMER_UMOWY_CN);
                dockindQuery.field(f,id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                if (searchResults == null || searchResults.totalCount()<1)
                {   
                    inst = DlContractDictionary.find(id);
                    inst.delete();
                    id = null;
                    addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getNumerUmowy()));
                }
                else
                    throw new EdmException("NieMoznaUsunacTejUmowyPoniewazSaDoNiegoPrzypisaneDokumenty");
                

                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
            */
        	addActionError("Nie mo�na usun�� umowy");
        }
    }
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if (numerUmowy != null) 
                	form.addProperty("numerUmowy", numerUmowy);
                if (stan != null) 
                	form.addProperty("stan", stan);
                if (idWniosku != null) 
                	form.addProperty("idWniosku", idWniosku);
                if (idKlienta != null) 
                	form.addProperty("idKlienta", idKlienta);
                if(idDostawcy != null)
                	form.addProperty("idDostawcy", idDostawcy);
                if (opisUmowy != null) form.addProperty("opisUmowy", opisUmowy);
                if (wartoscUmowy != null)
                {
                    String s = wartoscUmowy.toString().replace(",", ".");
                    wartoscUmowyFloat = Float.parseFloat(s);
                    form.addProperty("wartoscUmowy", wartoscUmowyFloat);
                }
                if (dataUmowy != null)
                    if (DateUtils.nullSafeParseJsDate(dataUmowy) != null) 
                        form.addProperty("dataUmowy", DateUtils.nullSafeParseJsDate(dataUmowy));
                    else
                    {
                        addActionError(sm.getString("PodanoNiepoprawnaDate"));
                        return;
                    } 
                if (dataKoncaUmowy != null)
                    if (DateUtils.nullSafeParseJsDate(dataKoncaUmowy) != null) 
                        form.addProperty("dataKoncaUmowy", DateUtils.nullSafeParseJsDate(dataKoncaUmowy));
                    else
                    {
                        addActionError(sm.getString("PodanoNiepoprawnaDate"));
                        return;
                    }
                if (status != null) form.addProperty("status", status);
                

                form.addOrderAsc("numerUmowy");

                SearchResults<? extends DlContractDictionary> results = DlContractDictionary.search(form);
                if (results == null || results.totalCount() == 0)
                {
                	DlContractDictionary contract = null;
                	if(numerUmowy == null)
                	{
                		throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                	}
                	else
                	{
                		List<DlContractDictionary> l = DlContractDictionary.findByNumerUmowy(numerUmowy);
                		if(l != null && l.size() > 0)
                		{
                			contract = l.get(0);
                		}
                		if(contract == null)
                		{
                			contract =  ImpulsImportUtils.findContractByNumber(numerUmowy);
                			if(contract != null)
                				addActionMessage("Zaimportowano umow� z LEO");
                		}
                		if(contract == null)
                		{
                			throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                		}
                		
                		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
                        {
                        	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
                        	{
                        		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
                        		{
                        			throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        		else
                        		{
                        			Contractor contractor = DSApi.context().load(Contractor.class, contract.getIdKlienta());
                        			if(!contractor.patronsContainsUser(DSApi.context().getPrincipalName()))
                        				throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        	}
                        	else
                        	{
                        		Contractor contractor = DSApi.context().load(Contractor.class, contract.getIdKlienta());
                        		if(contractor.getRegion() != null)
                        		{
                        			HashSet<String> divisionSet = new HashSet<String>(Arrays.asList(DSApi.context().getDSUser().getDivisionsGuid()));
                        			if(!divisionSet.contains("DV_"+contractor.getRegion().getCn()))
                        				throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                        		}
                        	}
                        }
            			id = contract.getId();
            			DlContractDictionaryAction.this.results = null;
            			
            			return;
                	}
                }

//                if (results == null || results.totalCount() == 0)
//                {
//                	if(numerUmowy == null)
//                	{
//                		throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
//                	}
//                	else
//                	{
//                		List<DlContractDictionary> l = DlContractDictionary.findByNumerUmowy(numerUmowy);
//                		if(l == null || l.size() < 1)
//                		{
//                		
//	                		DlContractDictionary con = ImpulsImportUtils.findContractByNumber(numerUmowy);
//	                		if(con != null)
//	                		{
//	                			id = con.getId();
//	                			addActionMessage("Zaimportowano umow� z LEO");
//	                			return;
//	                		}
//	                		else
//	                		{
//	                			throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
//	                		}
//                		}
//                		else
//                		{
//                			id = l.get(0).getId();
//                			DlContractDictionaryAction.this.results = null;
//                			return ;
//                		}
//                	}
//                }
                DlContractDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "numerUmowy", numerUmowy,
                                "idKlienta", idKlienta,
                                "idDostawcy", idDostawcy,
                                "opisUmowy", opisUmowy,
                                "wartoscUmowy", wartoscUmowy,
                                "dataUmowy", dataUmowy,
                                "dataKoncaUmowy", dataKoncaUmowy,
                                "status", status,
                                "searchnumerUmowy", searchnumerUmowy,
                                "searchidKlienta", searchidKlienta,
                                "searchopisUmowy", searchopisUmowy,
                                "searchwartoscUmowy", searchwartoscUmowy,
                                "searchdataUmowy", searchdataUmowy,
                                "searchdataKoncaUmowy", searchdataKoncaUmowy,
                                "param", param,
                                "stan",stan,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }  
            catch (NumberFormatException e)
            {
                addActionError(sm.getString("PodanoNiepoprawnaKwote"));
                return;
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            numerUmowy = TextUtils.trimmedStringOrNull(numerUmowy);
            opisUmowy = TextUtils.trimmedStringOrNull(opisUmowy);
            dataUmowy = TextUtils.trimmedStringOrNull(dataUmowy);
            dataKoncaUmowy = TextUtils.trimmedStringOrNull(dataKoncaUmowy);
        }
    }
        
    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if(numerUmowy == null)
            {
                addActionError(sm.getString("NumerUmowyJestObowiazkowy"));
                event.skip(EV_DO);
                return;
            }
            if(idKlienta==null)
            {
                addActionError(sm.getString("IdKlientaJestPolemObowiazkowymImusiBycLiczba"));
                event.skip(EV_DO);
                return;
            }
            if(idDostawcy==null)
            {
                addActionError(sm.getString("IdDostawcyJestPolemObowiazkowymImusiBycLiczba"));
                event.skip(EV_DO);
                return;
            }
            wartoscUmowy = TextUtils.trimmedStringOrNull(wartoscUmowy);
            if (wartoscUmowy != null)
            {
                try
                {
                    String s = wartoscUmowy.toString().replace(",", ".");
                    wartoscUmowyFloat = Float.parseFloat(s);
                }
                catch (NumberFormatException e)
                {
                    addActionError(sm.getString("PodanoNiepoprawnaKwote"));
                    event.skip(EV_DO);
                    return;
                }
            }
        }
    }

    public static int getLIMIT()
    {
        return LIMIT;
    }

    public static String getURL()
    {
        return URL;
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public String getDataKoncaUmowy()
    {
        return dataKoncaUmowy;
    }

    public void setDataKoncaUmowy(String dataKoncaUmowy)
    {
        this.dataKoncaUmowy = dataKoncaUmowy;
    }

    public String getDataUmowy()
    {
        return dataUmowy;
    }

    public void setDataUmowy(String dataUmowy)
    {
        this.dataUmowy = dataUmowy;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getIdKlienta()
    {
        return idKlienta;
    }

    public void setIdKlienta(Long idKlienta)
    {
        this.idKlienta = idKlienta;
    }

    public DlContractDictionary getInst()
    {
        return inst;
    }

    public void setInst(DlContractDictionary inst)
    {
        this.inst = inst;
    }

    public String getNumerUmowy()
    {
        return numerUmowy;
    }

    public void setNumerUmowy(String numerUmowy)
    {
        this.numerUmowy = numerUmowy;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getOpisUmowy()
    {
        return opisUmowy;
    }

    public void setOpisUmowy(String opisUmowy)
    {
        this.opisUmowy = opisUmowy;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public List<? extends DlContractDictionary> getResults()
    {
        return results;
    }

    public void setResults(List<? extends DlContractDictionary> results)
    {
        this.results = results;
    }

    public String getSearchdataKoncaUmowy()
    {
        return searchdataKoncaUmowy;
    }

    public void setSearchdataKoncaUmowy(String searchdataKoncaUmowy)
    {
        this.searchdataKoncaUmowy = searchdataKoncaUmowy;
    }

    public String getSearchdataUmowy()
    {
        return searchdataUmowy;
    }

    public void setSearchdataUmowy(String searchdataUmowy)
    {
        this.searchdataUmowy = searchdataUmowy;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setStatusList(List<String> statusList)
    {
        this.statusList = statusList;
    }

    public Long getSearchid()
    {
        return searchid;
    }

    public void setSearchid(Long searchid)
    {
        this.searchid = searchid;
    }

    public Integer getSearchidKlienta()
    {
        return searchidKlienta;
    }

    public void setSearchidKlienta(Integer searchidKlienta)
    {
        this.searchidKlienta = searchidKlienta;
    }

    public String getSearchnumerUmowy()
    {
        return searchnumerUmowy;
    }

    public void setSearchnumerUmowy(String searchnumerUmowy)
    {
        this.searchnumerUmowy = searchnumerUmowy;
    }

    public String getSearchopisUmowy()
    {
        return searchopisUmowy;
    }

    public void setSearchopisUmowy(String searchopisUmowy)
    {
        this.searchopisUmowy = searchopisUmowy;
    }

    public Float getSearchwartoscUmowy()
    {
        return searchwartoscUmowy;
    }

    public void setSearchwartoscUmowy(Float searchwartoscUmowy)
    {
        this.searchwartoscUmowy = searchwartoscUmowy;
    }

    public String getWartoscUmowy()
    {
        return wartoscUmowy;
    }

    public void setWartoscUmowy(String wartoscUmowy)
    {
        this.wartoscUmowy = wartoscUmowy;
    }

    public String getDictionaryDescription()
    {
        return dictionaryDescription;
    }

    public List<String> getStatusList()
    {
        return statusList;
    }

    public Map<String, String> getStatusMap()
    {
        return statusMap;
    }

    public void setStatusMap(Map<String, String> statusMap)
    {
        this.statusMap = statusMap;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }
    
    public String getNazwaKlienta()
    {
        return nazwaKlienta;
    }

    public void setNazwaKlienta(String nazwaKlienta)
    {
        this.nazwaKlienta = nazwaKlienta;
    }

	public void setIdDostawcy(Long idDostawcy)
	{
		this.idDostawcy = idDostawcy;
	}

	public Long getIdDostawcy()
	{
		return idDostawcy;
	}

	public void setNazwaDostawcy(String nazwaDostawcy)
	{
		this.nazwaDostawcy = nazwaDostawcy;
	}

	public String getNazwaDostawcy()
	{
		return nazwaDostawcy;
	}

	public void setIdWniosku(Long idWniosku)
	{
		this.idWniosku = idWniosku;
	}

	public Long getIdWniosku()
	{
		return idWniosku;
	}

	public void setNumerWniosku(String numerWniosku)
	{
		this.numerWniosku = numerWniosku;
	}

	public String getNumerWniosku()
	{
		return numerWniosku;
	}

	public Long getLeasingObjectType() {
		return leasingObjectType;
	}

	public void setLeasingObjectType(Long leasingObjectType) {
		this.leasingObjectType = leasingObjectType;
	}

	public List<LeasingObjectType> getLeasingObjectTypes() {
		return leasingObjectTypes;
	}

	public void setLeasingObjectTypes(List<LeasingObjectType> leasingObjectTypes) {
		this.leasingObjectTypes = leasingObjectTypes;
	}

	public Integer getStan() {
		return stan;
	}

	public void setStan(Integer stan) {
		this.stan = stan;
	}

	public Map<Integer, String> getStanMap() {
		return stanMap;
	}

	public void setStanMap(Map<Integer, String> stanMap) {
		this.stanMap = stanMap;
	}
}
