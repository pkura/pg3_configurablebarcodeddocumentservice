package pl.compan.docusafe.web.office.dictionaries;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.CooperationTerm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja s�ownika konkurencji dla kontrahenta.
 *
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class CrmCooperationTermsAction extends EventActionSupport
{
	private Log log = LogFactory.getLog(CrmCooperationTermsAction.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    //@IMPORT/EXPORT
    private Long id;
    private Long contractorId;
    private String param;
    private CooperationTerm cooperationTerm;
    private List<CooperationTerm> cooperationTerms;
    private Long selectedCooperation;
	private Double marzaOd;
	private Double marzaDo;
	private Double prowizjaSalonOd;
	private Double prowizjaSalonDo;
	private Double prowizjaHandlowiec;
	private Double nagrodaHandlowiec;
	private String opis;
	
	private static Contractor tmpContractor = new Contractor();


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }

	public void setParam(String param) {
		this.param = param;
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	cooperationTerm = null;
        	cooperationTerms = null;
            try
            {
            	if(id != null)
            		contractorId = id;
            	id = contractorId;

            	if(contractorId != null)
            	{
            		Contractor con = tmpContractor.find(contractorId);
            		cooperationTerms = con.getCooperationTerms();
            		if(selectedCooperation != null)
            		{
            			cooperationTerm = CooperationTerm.find(selectedCooperation);
            			cooperationTerms.get(0);
            		}
            		else if(cooperationTerms != null && cooperationTerms.size() > 0)
            		{
            			cooperationTerm = cooperationTerms.get(cooperationTerms.size() - 1 );
            			selectedCooperation = cooperationTerm.getId();
            		}
            		if(cooperationTerm != null)
            		{
            			marzaOd = cooperationTerm.getMarzaOd();
            			marzaDo = cooperationTerm.getMarzaDo();
            			prowizjaSalonOd = cooperationTerm.getProwizjaSalonOd();
            			prowizjaSalonDo = cooperationTerm.getProwizjaSalonDo();
            			prowizjaHandlowiec = cooperationTerm.getProwizjaHandlowiec();
            			nagrodaHandlowiec = cooperationTerm.getNagrodaHandlowiec();
            			opis = cooperationTerm.getOpis();
            		}
            	}
			}
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
				log.error("B��d ",e);
			}
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(contractorId == null)
        	{
        		addActionError("Musisz najpierw wybra� kontrahenta");
        		return;
        	}
            try
            {
            	DSApi.context().begin();
            	CooperationTerm coom = new CooperationTerm();
            	Contractor cTmp = tmpContractor.find(contractorId);
            	coom.setContractor(cTmp);
            	coom.setMarzaDo(marzaDo);
            	coom.setMarzaOd(marzaOd);
            	coom.setNagrodaHandlowiec(nagrodaHandlowiec);
            	coom.setOpis(opis);
            	coom.setProwizjaHandlowiec(prowizjaHandlowiec);
            	coom.setProwizjaSalonOd(prowizjaSalonOd);
            	coom.setProwizjaSalonDo(prowizjaSalonDo);
            	coom.setDataDodania(new Date());
            	cTmp.getCooperationTerms().add(coom);
            	coom.create();
            	DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                try
                {
                    DSApi.context().rollback();
                }
                catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(contractorId == null)
        	{
        		addActionError("Musisz najpierw wybra� kontrahenta");
        		return;
        	}
        	if(selectedCooperation == null)
        	{
        		addActionError("Brak wyboru");
        		return;
        	}
            try
            {
            	DSApi.context().begin();
            	Contractor contractor = tmpContractor.find(contractorId);
            	int index = contractor.getCooperationTerms().indexOf(CooperationTerm.find(selectedCooperation));
            	CooperationTerm com = contractor.getCooperationTerms().get(index);
            	Contractor cTmp = tmpContractor.find(contractorId);
            	com.setContractor(cTmp);
            	com.setMarzaDo(marzaDo);
            	com.setMarzaOd(marzaOd);
            	com.setNagrodaHandlowiec(nagrodaHandlowiec);
            	com.setOpis(opis);
            	com.setProwizjaHandlowiec(prowizjaHandlowiec);
            	com.setProwizjaSalonOd(prowizjaSalonOd);
            	com.setProwizjaSalonDo(prowizjaSalonDo);
            	com.save();
            	DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                try
                {
                    DSApi.context().rollback();
                }
                catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getContractorId() {
		return contractorId;
	}

	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}

	public CooperationTerm getCooperationTerm() {
		return cooperationTerm;
	}

	public void setCooperationTerm(CooperationTerm cooperationTerm) {
		this.cooperationTerm = cooperationTerm;
	}

	public List<CooperationTerm> getCooperationTerms() {
		return cooperationTerms;
	}

	public void setCooperationTerms(List<CooperationTerm> cooperationTerms) {
		this.cooperationTerms = cooperationTerms;
	}

	public Long getSelectedCooperation() {
		return selectedCooperation;
	}

	public void setSelectedCooperation(Long selectedCooperation) {
		this.selectedCooperation = selectedCooperation;
	}

	public Double getMarzaOd() {
		return marzaOd;
	}

	public void setMarzaOd(Double marzaOd) {
		this.marzaOd = marzaOd;
	}

	public Double getMarzaDo() {
		return marzaDo;
	}

	public void setMarzaDo(Double marzaDo) {
		this.marzaDo = marzaDo;
	}

	public Double getProwizjaSalonOd() {
		return prowizjaSalonOd;
	}

	public void setProwizjaSalonOd(Double prowizjaSalonOd) {
		this.prowizjaSalonOd = prowizjaSalonOd;
	}

	public Double getProwizjaSalonDo() {
		return prowizjaSalonDo;
	}

	public void setProwizjaSalonDo(Double prowizjaSalonDo) {
		this.prowizjaSalonDo = prowizjaSalonDo;
	}

	public Double getProwizjaHandlowiec() {
		return prowizjaHandlowiec;
	}

	public void setProwizjaHandlowiec(Double prowizjaHandlowiec) {
		this.prowizjaHandlowiec = prowizjaHandlowiec;
	}

	public Double getNagrodaHandlowiec() {
		return nagrodaHandlowiec;
	}

	public void setNagrodaHandlowiec(Double nagrodaHandlowiec) {
		this.nagrodaHandlowiec = nagrodaHandlowiec;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getParam() {
		return param;
	}
}
