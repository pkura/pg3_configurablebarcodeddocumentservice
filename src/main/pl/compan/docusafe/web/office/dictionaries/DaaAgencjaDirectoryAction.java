package pl.compan.docusafe.web.office.dictionaries;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Daa;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * Akcja s�ownikowa dla s�ownika agencji(w dokumentach archiwum agenta).
 * 
 * @author Bartlomiej Spychalski mailto:bsp@spychalski.eu
 */
public class DaaAgencjaDirectoryAction extends EventActionSupport 
{
    private StringManager sm = 
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    public static final String URL = "/office/common/agencja.action";
    public static final int LIMIT = 15;

    private Long id;
    private String nazwa;
    private String nip;
    private String numer;
    private Integer rodzaj_sieci;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String email;
    private String faks;
    private String telefon;

    private String searchNazwa;
    private String searchNip;
    private String searchNumer;
    private Integer searchRodzaj_sieci;
    private String searchUlica;
    private String searchKod;
    private String searchMiejscowosc;
    private String searchEmail;
    private String searchFaks;
    private String searchTelefon;

    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;

    private DaaAgencja agencja;
    private List<? extends DaaAgencja> results;

    private boolean closeOnReload;

    //private boolean canRead;
    private boolean canEdit;
    private boolean canDelete;
    private boolean canAddAndSubmit;
    private boolean afterSearch;
    private boolean forEdit; //czy formatka jest otwarta tylko dla edycji s�ownika(nie w celu umieszczenia w formularzu)

    private int offset;
    private int limit = LIMIT;
    private Pager pager;

    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }


    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

               /* canRead = DSApi.context().hasPermission(DSPermission.DAA_ODCZYT);
                if(!canRead) throw new EdmException("Brak uprawnien do odczytu danych"); */

                canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_USUWANIE);

                if (id != null) 
                {
                    agencja = new DaaAgencja().find(id);
                    if (agencja != null) 
                    {
                        nazwa = agencja.getNazwa();
                        nip = agencja.getNip();
                        numer = agencja.getNumer();
                        rodzaj_sieci = agencja.getRodzaj_sieci();
                        ulica = agencja.getUlica();
                        kod = agencja.getKod();
                        miejscowosc = agencja.getMiejscowosc();
                        email = agencja.getEmail();
                        faks = agencja.getFaks();
                        telefon = agencja.getTelefon();
                        rodzaj_sieci = agencja.getRodzaj_sieci();
                    }
                }
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            nazwa = TextUtils.trimmedStringOrNull(nazwa);
            nip = TextUtils.trimmedStringOrNull(nip);
            numer = TextUtils.trimmedStringOrNull(numer);
            kod = TextUtils.trimmedStringOrNull(kod);
            ulica = TextUtils.trimmedStringOrNull(ulica);
            faks = TextUtils.trimmedStringOrNull(faks);
            miejscowosc = TextUtils.trimmedStringOrNull(miejscowosc);
            telefon = TextUtils.trimmedStringOrNull(telefon);
            email = TextUtils.trimmedStringOrNull(email);
        }
    }

    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if (StringUtils.isEmpty(nazwa))
                addActionError(sm.getString("NiePodanoNazwy"));
            if (StringUtils.isEmpty(numer) && (rodzaj_sieci != null) && (rodzaj_sieci > 1))
                /* Numer musi byc wybrany tylko dla bankow i sieci_Zewn */
                addActionError(sm.getString("NiePodanoNumeru"));
            if (StringUtils.isEmpty(nip) && (rodzaj_sieci != null) && (rodzaj_sieci > 1))
                /* NIP musi byc wybrany tylko dla bankow i sieci_Zewn */
                addActionError(sm.getString("NiePodanoNIP"));
            if (rodzaj_sieci == null || rodzaj_sieci == 0)
                addActionError(sm.getString("NieWybranoRodzajuSieci"));

            if (hasActionErrors())
                event.cancel();
        }
    }

    /**
     * dodanie agencji do s�ownika
     */
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                if(!canEdit) throw new EdmException("BrakUprawnienDoEdycjiSlownika");

                agencja = new DaaAgencja();
                agencja.setNazwa(nazwa);
                agencja.setNip(nip);
                agencja.setNumer(numer);
                agencja.setRodzaj_sieci(rodzaj_sieci);
                agencja.setUlica(ulica);
                agencja.setKod(kod);
                agencja.setMiejscowosc(miejscowosc);
                agencja.setEmail(email);
                agencja.setFaks(faks);
                agencja.setTelefon(telefon);

                agencja.setRodzaj_sieci(rodzaj_sieci);

                agencja.create();

                id = agencja.getId();

                DSApi.context().commit();

                addActionMessage(sm.getString("WSlownikuUtworzono",agencja.getNazwa()));
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }


    /**
     * modyfikacji agencji w s�owniku
     */
    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canEdit = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_DODAWANIE);
                if (!canEdit) 
                    throw new EdmException(sm.getString("BrakUprawnienDoEdycjiSlownika"));

                agencja = new DaaAgencja().find(id);
                agencja.setNazwa(nazwa);
                agencja.setNip(nip);
                agencja.setNumer(numer);
                agencja.setRodzaj_sieci(rodzaj_sieci);
                agencja.setUlica(ulica);
                agencja.setKod(kod);
                agencja.setMiejscowosc(miejscowosc);
                agencja.setEmail(email);
                agencja.setFaks(faks);
                agencja.setTelefon(telefon);

                agencja.setRodzaj_sieci(rodzaj_sieci);

                id = agencja.getId();

                /* ponowna archiwizacja dokument�w przypisanych do zmienianego agenta */
                Document.DoctypeQuery doctypeQuery = new Document.DoctypeQuery(0,0);
                Doctype doctype = Doctype.findByCn("daa");
                doctypeQuery.doctype(doctype.getId());
                doctypeQuery.field(doctype.getFieldByCn("AGENCJA").getId(), id);

                SearchResults<Document> searchResults = Document.searchByDoctype(doctypeQuery);
                for (Document document : searchResults.results())
                {
                    Map doctypeFields = doctype.getValueMap(document.getId());

                    Doctype.EnumItem siecEnum = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItem(rodzaj_sieci/*new Integer(doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId()).toString())*/);
                    Doctype.EnumItem typEnum = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));
                    String klasaId = doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("KLASA_RAPORTU").getId()).toString() : null;
                    Doctype.EnumItem klasaRaportuEnum = klasaId != null ? doctype.getFieldByCn("KLASA_RAPORTU").getEnumItem(new Integer(klasaId)) : null;

                    String agentIds = doctypeFields.get(doctype.getFieldByCn("AGENT").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENT").getId()).toString() : null;
                    //String agencjaIds = doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()) != null ? doctypeFields.get(doctype.getFieldByCn("AGENCJA").getId()).toString() : null;
                    DaaAgent agent = null;
                    //DaaAgencja agencja = null;
                    if (agentIds != null)
                        agent = new DaaAgent().find(new Long(agentIds.trim()));
                    //if (agencjaIds != null)
                    //    agencja = DaaAgencja.find(new Long(agencjaIds.trim()));

                    doctype.set(document.getId(),doctype.getFieldByCn("RODZAJ_SIECI").getId(), rodzaj_sieci);

                    Daa.daaDocument(document, agent,  agencja,  siecEnum, typEnum.getTitle(), typEnum.getArg1(), klasaRaportuEnum, false);
                    if (document instanceof OfficeDocument)
                        ((OfficeDocument) document).setSummary(document.getDescription());
                }

                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuZapisanoZmiany"));
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
            
            try
            {
            	DSApi.context().begin();
            	
            	agencja = new DaaAgencja().find(id);
            	agencja.changeFolders();
            	
            	DSApi.context().commit();
            }
            catch (Exception e) 
            {
				e.printStackTrace();
				DSApi.context()._rollback();
			}
        }
    }

    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_USUWANIE);
                if (!canDelete) 
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZeSlownika"));

                agencja = new DaaAgencja().find(id);
                List<DaaAgent> agenci = DaaAgent.findByAgencja(agencja);
                if (agenci.size() != 0)
                    addActionError(sm.getString("NieMoznaUsunacZeSlownikaAgencjiPoniewazSaDoNiejPrzypisaniAgenci",agencja.getNazwa()));
                else 
                {
                    Document.DoctypeQuery doctypeQuery = new Document.DoctypeQuery(0, 10);
                    Doctype doctype = Doctype.findByCn("daa");
                    doctypeQuery.doctype(doctype.getId());
                    doctypeQuery.field(doctype.getFieldByCn("AGENCJA").getId(), id);

                    SearchResults searchResults = Document.searchByDoctype(doctypeQuery);
                    if (searchResults.totalCount() != 0)
                        addActionError(sm.getString("NieMoznaUsunacZeSlownikaAgencjiPoniewazSaDoNiejPrzypisaneDokumenty",agencja.getNazwa()));
                    else 
                    {
                        agencja.delete();
                        id = null;
                        addActionMessage(sm.getString("ZeSlownikaUsunietoAgencje",agencja.getNazwa()));
                    }
                }
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, limit);

                if (nip != null) form.addProperty("nip", nip);
                if (nazwa != null) form.addProperty("nazwa", nazwa);
                if (numer != null) form.addProperty("numer", numer);
                if (telefon != null) form.addProperty("telefon", telefon);
                if (faks != null) form.addProperty("faks", faks);
                if (ulica != null) form.addProperty("ulica", ulica);
                if (miejscowosc != null) form.addProperty("miejscowosc", miejscowosc);
                if (rodzaj_sieci != null) form.addProperty("rodzaj_sieci", rodzaj_sieci);
                if (email != null) form.addProperty("email", email);
                if (kod != null) form.addProperty("kod", kod);

                form.addOrderAsc("nazwa");

                SearchResults<? extends DaaAgencja> results = DaaAgencja.search(form);

                if (results.totalCount() == 0)  
                {
                    afterSearch = false;
                    addActionMessage(sm.getString("NieZnalezionoAgencjiPasujacychDoWpisanychDanych"));
                }
                else 
                {
                    afterSearch = true;
                    searchNazwa = nazwa;
                    searchNumer = numer;
                    searchNip = nip;
                    searchKod = kod;
                    searchMiejscowosc = miejscowosc;
                    searchFaks = faks;
                    searchTelefon = telefon;
                    searchEmail = email;
                    searchUlica = ulica;
                    searchRodzaj_sieci = rodzaj_sieci;
                }

                DaaAgencjaDirectoryAction.this.results = fun.list(results);

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] {
                            "doSearch", "true",
                            "nazwa", nazwa,
                            "nip", nip,
                            "numer", numer,
                            "kod", kod,
                            "ulica", ulica,
                            "miejscowosc", miejscowosc,
                            "email", email,
                            "faks", faks,
                            "telefon", telefon,
                            "rodzaj_sieci", rodzaj_sieci,
                            "afterSearch", afterSearch ? "true" : "false",
                            "searchNazwa", searchNazwa,
                            "searchNip", searchNip,
                            "searchNumer", searchNumer,
                            "searchUlica", searchUlica,
                            "searchKod", searchKod,
                            "searchFaks", searchFaks,
                            "searchMiejscowosc", searchMiejscowosc,
                            "searchEmail", searchEmail,
                            "searchRodzaj_sieci", searchRodzaj_sieci,
                            "searchTelefon", searchTelefon,
                            "param", param,
                            "offset", String.valueOf(offset)
                        });
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);

            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getSearchNazwa() 
    {
        return searchNazwa;
    }

    public void setSearchNazwa(String searchNazwa) 
    {
        this.searchNazwa = searchNazwa;
    }

    public String getSearchNip() 
    {
        return searchNip;
    }

    public void setSearchNip(String searchNip) 
    {
        this.searchNip = searchNip;
    }

    public String getSearchNumer() 
    {
        return searchNumer;
    }

    public void setSearchNumer(String searchNumer) 
    {
        this.searchNumer = searchNumer;
    }

    public String getSearchUlica() 
    {
        return searchUlica;
    }

    public void setSearchUlica(String searchUlica) 
    {
        this.searchUlica = searchUlica;
    }

    public String getSearchKod() 
    {
        return searchKod;
    }

    public void setSearchKod(String searchKod) 
    {
        this.searchKod = searchKod;
    }

    public String getSearchMiejscowosc() 
    {
        return searchMiejscowosc;
    }

    public void setSearchMiejscowosc(String searchMiejscowosc) 
    {
        this.searchMiejscowosc = searchMiejscowosc;
    }

    public String getSearchEmail() 
    {
        return searchEmail;
    }

    public void setSearchEmail(String searchEmail) 
    {
        this.searchEmail = searchEmail;
    }

    public String getSearchFaks() 
    {
        return searchFaks;
    }

    public void setSearchFaks(String searchFaks) 
    {
        this.searchFaks = searchFaks;
    }

    public String getSearchTelefon() 
    {
        return searchTelefon;
    }

    public void setSearchTelefon(String searchTelefon) 
    {
        this.searchTelefon = searchTelefon;
    }

    public Integer getSearchRodzaj_sieci() 
    {
        return searchRodzaj_sieci;
    }

    public void setSearchRodzaj_sieci(Integer searchRodzaj_sieci) 
    {
        this.searchRodzaj_sieci = searchRodzaj_sieci;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getNazwa() 
    {
        return nazwa;
    }

    public void setNazwa(String nazwa) 
    {
        this.nazwa = nazwa;
    }

    public String getNip() 
    {
        return nip;
    }

    public void setNip(String nip) 
    {
        this.nip = nip;
    }

    public String getNumer() 
    {
        return numer;
    }

    public void setNumer(String numer)
    {
        this.numer = numer;
    }

    public String getUlica()
    {
        return ulica;
    }

    public void setUlica(String ulica)
    {
        this.ulica = ulica;
    }

    public String getKod()
    {
        return kod;
    }

    public void setKod(String kod) 
    {
        this.kod = kod;
    }

    public String getMiejscowosc()
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) 
    {
        this.miejscowosc = miejscowosc;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFaks() 
    {
        return faks;
    }

    public void setFaks(String faks)
    {
        this.faks = faks;
    }

    public String getTelefon() 
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public Integer getRodzaj_sieci() 
    {
        return rodzaj_sieci;
    }

    public void setRodzaj_sieci(Integer rodzaj_sieci)
    {
        this.rodzaj_sieci = rodzaj_sieci;
    }

    public DaaAgencja getAgencja()
    {
        return agencja;
    }

    public void setAgencja(DaaAgencja agencja)
    {
        this.agencja = agencja;
    }

    public List<? extends DaaAgencja> getResults()
    {
        return results;
    }

    public void setResults(List<? extends DaaAgencja> results)
    {
        this.results = results;
    }

    public boolean isCloseOnReload()
    {
        return closeOnReload;
    }

    public void setCloseOnReload(boolean closeOnReload)
    {
        this.closeOnReload = closeOnReload;
    }

    public boolean isCanEdit() 
    {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public boolean isCanAddAndSubmit()
    {
        return canAddAndSubmit;
    }

    public void setCanAddAndSubmit(boolean canAddAndSubmit) 
    {
        this.canAddAndSubmit = canAddAndSubmit;
    }

    public boolean isAfterSearch() 
    {
        return afterSearch;
    }

    public void setAfterSearch(boolean afterSearch) 
    {
        this.afterSearch = afterSearch;
    }

    public boolean isForEdit()
    {
        return forEdit;
    }

    public void setForEdit(boolean forEdit)
    {
        this.forEdit = forEdit;
    }

    public Pager getPager()
    {
        return pager;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }
}
