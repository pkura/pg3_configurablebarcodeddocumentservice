package pl.compan.docusafe.web.office.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.dictionary.VatRate;
import pl.compan.docusafe.core.dockinds.dictionary.VatRateDictionary;
import pl.compan.docusafe.parametrization.ic.VatEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class VatEntryAction extends EventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(VatEntryAction.class);

    List<VatRate> vatRates;
    /** vatentry.id */
    Long id;
    VatEntry entry = new VatEntry();
    String param;
    /** czy zosta� utworzony nowy wpis */
    boolean added;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
                append(OpenHibernateSession.INSTANCE).
                append(new Add()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {
            if(id != null){
                LOG.info("id = {}", id);
                entry = VatEntry.get(id);
            }
            vatRates = VatRateDictionary.INSTANCE.getAll();
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    private class Add extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            if(entry.getId() == null){
                entry.create();
            } else {
                DSApi.context().session().update(entry);
            }
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            LOG.info("created/updated id = {}", entry.getId());
            added = true;
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public VatEntry getEntry() {
        return entry;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public boolean isAdded() {
        return added;
    }

    public List<VatRate> getVatRates() {
        return vatRates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
