package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.prosika.DicProSIKA;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * Akcja s�ownikowa dla s�ownika Instytucji/kontrahent�w (dla dokument�w invoice).
 * 
 * @author Tomasz Lipka
 */
public class DicProSIKAAction extends EventActionSupport
{
	public static final String URL = "/office/common/dicprosika.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long id;
    private String nrEwidencyjny;
	private String nazwaKlienta;
	private String nip;
	private String regon;
	private String nrTelefonuGlowny;
	
	private String as_ulica;
	private String as_kod;
	private String as_miejscowosc;
	private String as_nrdomu;
	private String as_nrlokalu;
	private String ai_ulica;
	private String ai_kod;
	private String ai_miejscowosc;
	private String ai_nrdomu;
	private String ai_nrlokalu;
	private String ak_ulica;
	private String ak_kod;
	private String ak_miejscowosc;
	private String ak_nrdomu;
	private String ak_nrlokalu;
	
    private Long searchid;    
    private String searchnrEwidencyjny;
	private String searchnazwaKlienta;
	private String searchnip;
	private String searchregon;
	private String searchnrTelefonuGlowny;
	
	private String search_as_ulica;
	private String search_as_kod;
	private String search_as_miejscowosc;
	private String search_as_nrdomu;
	private String search_as_nrlokalu;
	private String search_ai_ulica;
	private String search_ai_kod;
	private String search_ai_miejscowosc;
	private String search_ai_nrdomu;
	private String search_ai_nrlokalu;
	private String search_ak_ulica;
	private String search_ak_kod;
	private String search_ak_miejscowosc;
	private String search_ak_nrdomu;
	private String search_ak_nrlokalu;
	
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    
    private DicProSIKA inst;
    private Pager pager;
    private int offset;
    private List<? extends DicProSIKA> results;
    
    
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
	
    protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    }
    
    private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    	
                if(id != null)
                {
                	inst = DicProSIKA.getInstance().find(id);
                	if(inst != null)
                	{
                		nrEwidencyjny = inst.getNrEwidencyjny(); 
                        nazwaKlienta = inst.getNazwaKlienta();
                        nip = inst.getNip();
                        regon = inst.getRegon();
//                        nrTelefonuGlowny = inst.getNrTelefonuGlowny();
            			as_ulica = inst.getAs_ulica();
                    	as_kod = inst.getAs_kod();
                    	as_miejscowosc = inst.getAs_miejscowosc();
                    	as_nrdomu = inst.getAs_nrdomu();
                    	as_nrlokalu = inst.getAs_nrlokalu();
//                    	ai_ulica = inst.getAi_ulica();
//                    	ai_kod = inst.getAi_kod();
//                    	ai_miejscowosc = inst.getAi_miejscowosc();
//                    	ai_nrdomu = inst.getAi_nrdomu();
//                    	ai_nrlokalu = inst.getAi_nrlokalu();
//                    	ak_ulica = inst.getAk_ulica();
//                    	ak_kod = inst.getAk_kod();
//                    	ak_miejscowosc = inst.getAk_miejscowosc();
//                    	ak_nrdomu = inst.getAk_nrdomu();
//                    	ak_nrlokalu = inst.getAk_nrlokalu();
                	}
                	
                }
                DSApi.context().commit();
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    	}
    }
    
    private class Add implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
              
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DicProSIKA();
                
                inst.setNrEwidencyjny(nrEwidencyjny);
                inst.setNazwaKlienta(nazwaKlienta);
                inst.setNip(nip);
                inst.setRegon(regon);
//                inst.setNrTelefonuGlowny(nrTelefonuGlowny);
                inst.setAs_ulica(as_ulica);
                inst.setAs_kod(as_kod);
                inst.setAs_miejscowosc(as_miejscowosc);
                inst.setAs_nrdomu(as_nrdomu);
                inst.setAs_nrlokalu(as_nrlokalu);
//                inst.setAi_ulica(ai_ulica);
//                inst.setAi_kod(ai_kod);
//                inst.setAi_miejscowosc(ai_miejscowosc);
//                inst.setAi_nrdomu(ai_nrdomu);
//                inst.setAi_nrlokalu(ai_nrlokalu);
//                inst.setAk_ulica(ak_ulica);
//                inst.setAk_kod(ak_kod);
//                inst.setAk_miejscowosc(ak_miejscowosc);
//                inst.setAk_nrdomu(ak_nrdomu);
//                inst.setAk_nrlokalu(ak_nrlokalu);
                
                inst.create();
                
                id = inst.getId();
    			
    			DSApi.context().commit();
    			
    			addActionMessage(sm.getString("WslownikuUtworzono",inst.getNrEwidencyjny()));
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                	e.printStackTrace();
                    addActionError(e1.getMessage());
                }
                e.printStackTrace();
                addActionError(e.getMessage());
    		}
    	}
    }
    
    /**
     * uwagi klasie
     * */
    
    private class Delete implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try 
    		{
    			DSApi.context().begin();
    			

                inst = DicProSIKA.getInstance().find(id);
    			
                
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst,getNrEwidencyjny()));
                
    			DSApi.context().commit();
    		}
    		catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Update implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			DicProSIKA inst = DicProSIKA.getInstance().find(id);
    			
    			inst.setNrEwidencyjny(nrEwidencyjny);
                inst.setNazwaKlienta(nazwaKlienta);
                inst.setNip(nip);
                inst.setRegon(regon);
//                inst.setNrTelefonuGlowny(nrTelefonuGlowny);
                inst.setAs_ulica(as_ulica);
                inst.setAs_kod(as_kod);
                inst.setAs_miejscowosc(as_miejscowosc);
                inst.setAs_nrdomu(as_nrdomu);
                inst.setAs_nrlokalu(as_nrlokalu);
//                inst.setAi_ulica(ai_ulica);
//                inst.setAi_kod(ai_kod);
//                inst.setAi_miejscowosc(ai_miejscowosc);
//                inst.setAi_nrdomu(ai_nrdomu);
//                inst.setAi_nrlokalu(ai_nrlokalu);
//                inst.setAk_ulica(ak_ulica);
//                inst.setAk_kod(ak_kod);
//                inst.setAk_miejscowosc(ak_miejscowosc);
//                inst.setAk_nrdomu(ak_nrdomu);
//                inst.setAk_nrlokalu(ak_nrlokalu);
                
                
    			DSApi.context().session().save(inst);
    			DSApi.context().commit();
    		}
    		catch(EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
    	}
    }
    
    private class Search implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			QueryForm form = new QueryForm(offset, LIMIT);
    			
    			if(nrEwidencyjny != null) form.addProperty("nrEwidencyjny", nrEwidencyjny);
    			if(nazwaKlienta != null) form.addProperty("nazwaKlienta", nazwaKlienta);
    			if(nip != null) form.addProperty("nip", nip);
                if(regon != null) form.addProperty("regon", regon);
//                if(nrTelefonuGlowny != null) form.addProperty("nrTelefonuGlowny", nrTelefonuGlowny);
                if(as_ulica != null) form.addProperty("as_ulica", as_ulica);
                if(as_kod != null) form.addProperty("as_kod", as_kod);
                if(as_miejscowosc != null) form.addProperty("as_miejscowosc", as_miejscowosc);
                if(as_nrdomu != null) form.addProperty("as_nrdomu", as_nrdomu);
                if(as_nrlokalu != null) form.addProperty("as_nrlokalu", as_nrlokalu);
//                if(ai_ulica != null) form.addProperty("ai_ulica", ai_ulica);
//                if(ai_kod != null) form.addProperty("ai_kod", ai_kod);
//                if(ai_miejscowosc != null) form.addProperty("ai_miejscowosc", ai_miejscowosc);
//                if(ai_nrdomu != null) form.addProperty("ai_nrdomu", ai_nrdomu);
//                if(ai_nrlokalu != null) form.addProperty("ai_nrlokalu", ai_nrlokalu);
//                if(ak_ulica != null) form.addProperty("ak_ulica", ak_ulica);
//                if(ak_kod != null) form.addProperty("ak_kod", ak_kod);
//                if(ak_miejscowosc != null) form.addProperty("ak_miejscowosc", ak_miejscowosc);
//                if(ak_nrdomu != null) form.addProperty("ak_nrdomu", ak_nrdomu);
//                if(ak_nrlokalu != null) form.addProperty("ak_nrlokalu", ak_nrlokalu);
                
                form.addOrderAsc("nrEwidencyjny");
                
                SearchResults<? extends DicProSIKA> results = DicProSIKA.search(form);
                
                if (results.totalCount() == 0)  
                	throw new EdmException(sm.getString("NieZnalezionoRekorduPasujacychDoWpisanychDanych"));
                else
                {
                	searchnrEwidencyjny = nrEwidencyjny;
                	searchnazwaKlienta = nazwaKlienta; 
                	searchnip = nip;
                	searchregon = regon;
//                	searchnrTelefonuGlowny = nrTelefonuGlowny;
                	search_as_ulica = as_ulica; 
            		search_as_kod =  as_kod;
            		search_as_miejscowosc = as_miejscowosc;
            		search_as_nrdomu = as_nrdomu;
            		search_as_nrlokalu = as_nrlokalu;
//            		search_ai_ulica = ai_ulica;
//            		search_ai_kod = ai_kod;
//            		search_ai_miejscowosc = ai_miejscowosc; 
//            		search_ai_nrdomu = ai_nrdomu;
//            		search_ai_nrlokalu = ai_nrlokalu;
//            		search_ak_ulica = ak_ulica;
//            		search_ak_kod = ak_kod;
//            		search_ak_miejscowosc = ak_miejscowosc;
//            		search_ak_nrdomu = ak_nrdomu;
//            		search_ak_nrlokalu = ak_nrlokalu;
                	
                }
                DicProSIKAAction.this.results = fun.list(results);
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "nrEwidencyjny", nrEwidencyjny,
                                "nazwaKlienta", nazwaKlienta,
                                "nip", nip,
                                "regon" , regon,
//                                "nrTelefonuGlowny", nrTelefonuGlowny,
                                "as_ulica" , as_ulica,
                                "as_kod" , as_kod,
                                "as_miejscowosc" , as_miejscowosc,
                                "as_nrdomu" , as_nrdomu,
                                "as_nrlokalu" , as_nrlokalu,
//                                "ai_ulica" , ai_ulica,
//                                "ai_kod" , ai_kod,
//                                "ai_miejscowosc" , ai_miejscowosc,
//                                "ai_nrdomu" , ai_nrdomu,
//                                "ai_nrlokalu" , ai_nrlokalu,
//                                "ak_ulica" , ak_ulica,
//                                "ak_kod" , ak_kod,
//                                "ak_miejscowosc" , ak_miejscowosc,
//                                "ak_nrdomu" , ak_nrdomu,
//                                "ak_nrlokalu" , ak_nrlokalu,
                                "searchnrEwidencyjny", searchnrEwidencyjny,
                                "searchnazwaKlienta", searchnazwaKlienta,
                                "searchnip", searchnip,
                                "searchregon" , searchregon,
//                                "searchnrTelefonuGlowny", searchnrTelefonuGlowny,
                                "search_as_ulica" , search_as_ulica,
                                "search_as_kod" , search_as_kod,
                                "search_as_miejscowosc" , search_as_miejscowosc,
                                "search_as_nrdomu" , search_as_nrdomu,
                                "search_as_nrlokalu" , search_as_nrlokalu,
//                                "search_ai_ulica" , search_ai_ulica,
//                                "search_ai_kod" , search_ai_kod,
//                                "search_ai_miejscowosc" , search_ai_miejscowosc,
//                                "search_ai_nrdomu" , search_ai_nrdomu,
//                                "search_ai_nrlokalu" , search_ai_nrlokalu,
//                                "search_ak_ulica" , search_ak_ulica,
//                                "search_ak_kod" , search_ak_kod,
//                                "search_ak_miejscowosc" , search_ak_miejscowosc,
//                                "search_ak_nrdomu" , search_ak_nrdomu,
//                                "search_ak_nrlokalu" , search_ak_nrlokalu,
                                "param", param,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
    		}
    		catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
    	}
    }
    
    private class Clean implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		nrEwidencyjny = TextUtils.trimmedStringOrNull(nrEwidencyjny);
    		nazwaKlienta = TextUtils.trimmedStringOrNull(nazwaKlienta);
    		nip = TextUtils.trimmedStringOrNull(nip);
    		regon = TextUtils.trimmedStringOrNull(regon);
//    		nrTelefonuGlowny = TextUtils.trimmedStringOrNull(nrTelefonuGlowny);
    		as_ulica = TextUtils.trimmedStringOrNull(as_ulica);
    		as_kod = TextUtils.trimmedStringOrNull(as_kod);
    		as_miejscowosc = TextUtils.trimmedStringOrNull(as_miejscowosc);
    		as_nrdomu = TextUtils.trimmedStringOrNull(as_nrdomu);
    		as_nrlokalu = TextUtils.trimmedStringOrNull(as_nrlokalu);
//    		ai_ulica = TextUtils.trimmedStringOrNull(ai_ulica);
//    		ai_kod = TextUtils.trimmedStringOrNull(ai_kod);
//    		ai_miejscowosc = TextUtils.trimmedStringOrNull(ai_miejscowosc);
//    		ai_nrdomu = TextUtils.trimmedStringOrNull(ai_nrdomu);
//    		ai_nrlokalu = TextUtils.trimmedStringOrNull(ai_nrlokalu);
//    		ak_ulica = TextUtils.trimmedStringOrNull(ak_ulica);
//    		ak_kod = TextUtils.trimmedStringOrNull(ak_kod);
//    		ak_miejscowosc = TextUtils.trimmedStringOrNull(ak_miejscowosc);
//    		ak_nrdomu = TextUtils.trimmedStringOrNull(ak_nrdomu);
//    		ak_nrlokalu = TextUtils.trimmedStringOrNull(ak_nrlokalu);    		
    	}
    }
    
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(nrEwidencyjny))
            lista.add(sm.getString("NiePodanoNrEwidencyjny"));
        else if (nrEwidencyjny.length()>14)
        	lista.add(sm.getString("ZaDlugiNumerEwidencyjny"));
        
        if(!StringUtils.isNumeric(nip) || nip.length() != 10)
        	lista.add(sm.getString("NipPowinienZawieracTylko10Cyfr"));
        
        if(!StringUtils.isNumeric(nrEwidencyjny))
        	lista.add(sm.getString("NumerEwidencyjnyPowinienZawieracTylkoCyfry"));
        
        return lista;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrEwidencyjny() {
		return nrEwidencyjny;
	}

	public void setNrEwidencyjny(String nrEwidencyjny) {
		this.nrEwidencyjny = nrEwidencyjny;
	}

	public Long getSearchid() {
		return searchid;
	}

	public void setSearchid(Long searchid) {
		this.searchid = searchid;
	}
	
	public String getSearchnrEwidencyjny() {
		return searchnrEwidencyjny;
	}

	public void setSearchnrEwidencyjny(String searchnrEwidencyjny) {
		this.searchnrEwidencyjny = searchnrEwidencyjny;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public DicProSIKA getInst() {
		return inst;
	}

	public void setInst(DicProSIKA inst) {
		this.inst = inst;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<? extends DicProSIKA> getResults() {
		return results;
	}

	public void setResults(List<? extends DicProSIKA> results) {
		this.results = results;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getNazwaKlienta() {
		return nazwaKlienta;
	}

	public void setNazwaKlienta(String nazwaKlienta) {
		this.nazwaKlienta = nazwaKlienta;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getNrTelefonuGlowny() {
		return nrTelefonuGlowny;
	}

	public void setNrTelefonuGlowny(String nrTelefonuGlowny) {
		this.nrTelefonuGlowny = nrTelefonuGlowny;
	}

	
	public String getSearchnazwaKlienta() {
		return searchnazwaKlienta;
	}

	public void setSearchnazwaKlienta(String searchnazwaKlienta) {
		this.searchnazwaKlienta = searchnazwaKlienta;
	}

	public String getSearchnip() {
		return searchnip;
	}

	public void setSearchnip(String searchnip) {
		this.searchnip = searchnip;
	}

	public String getSearchregon() {
		return searchregon;
	}

	public void setSearchregon(String searchregon) {
		this.searchregon = searchregon;
	}

	public String getSearchnrTelefonuGlowny() {
		return searchnrTelefonuGlowny;
	}

	public void setSearchnrTelefonuGlowny(String searchnrTelefonuGlowny) {
		this.searchnrTelefonuGlowny = searchnrTelefonuGlowny;
	}

	public String getAs_ulica() {
		return as_ulica;
	}

	public void setAs_ulica(String as_ulica) {
		this.as_ulica = as_ulica;
	}

	public String getAs_kod() {
		return as_kod;
	}

	public void setAs_kod(String as_kod) {
		this.as_kod = as_kod;
	}

	public String getAs_miejscowosc() {
		return as_miejscowosc;
	}

	public void setAs_miejscowosc(String as_miejscowosc) {
		this.as_miejscowosc = as_miejscowosc;
	}

	public String getAs_nrdomu() {
		return as_nrdomu;
	}

	public void setAs_nrdomu(String as_nrdomu) {
		this.as_nrdomu = as_nrdomu;
	}

	public String getAs_nrlokalu() {
		return as_nrlokalu;
	}

	public void setAs_nrlokalu(String as_nrlokalu) {
		this.as_nrlokalu = as_nrlokalu;
	}

	public String getAi_ulica() {
		return ai_ulica;
	}

	public void setAi_ulica(String ai_ulica) {
		this.ai_ulica = ai_ulica;
	}

	public String getAi_kod() {
		return ai_kod;
	}

	public void setAi_kod(String ai_kod) {
		this.ai_kod = ai_kod;
	}

	public String getAi_miejscowosc() {
		return ai_miejscowosc;
	}

	public void setAi_miejscowosc(String ai_miejscowosc) {
		this.ai_miejscowosc = ai_miejscowosc;
	}

	public String getAi_nrdomu() {
		return ai_nrdomu;
	}

	public void setAi_nrdomu(String ai_nrdomu) {
		this.ai_nrdomu = ai_nrdomu;
	}

	public String getAi_nrlokalu() {
		return ai_nrlokalu;
	}

	public void setAi_nrlokalu(String ai_nrlokalu) {
		this.ai_nrlokalu = ai_nrlokalu;
	}

	public String getAk_ulica() {
		return ak_ulica;
	}

	public void setAk_ulica(String ak_ulica) {
		this.ak_ulica = ak_ulica;
	}

	public String getAk_kod() {
		return ak_kod;
	}

	public void setAk_kod(String ak_kod) {
		this.ak_kod = ak_kod;
	}

	public String getAk_miejscowosc() {
		return ak_miejscowosc;
	}

	public void setAk_miejscowosc(String ak_miejscowosc) {
		this.ak_miejscowosc = ak_miejscowosc;
	}

	public String getAk_nrdomu() {
		return ak_nrdomu;
	}

	public void setAk_nrdomu(String ak_nrdomu) {
		this.ak_nrdomu = ak_nrdomu;
	}

	public String getAk_nrlokalu() {
		return ak_nrlokalu;
	}

	public void setAk_nrlokalu(String ak_nrlokalu) {
		this.ak_nrlokalu = ak_nrlokalu;
	}

	public String getSearch_as_ulica() {
		return search_as_ulica;
	}

	public void setSearch_as_ulica(String search_as_ulica) {
		this.search_as_ulica = search_as_ulica;
	}

	public String getSearch_as_kod() {
		return search_as_kod;
	}

	public void setSearch_as_kod(String search_as_kod) {
		this.search_as_kod = search_as_kod;
	}

	public String getSearch_as_miejscowosc() {
		return search_as_miejscowosc;
	}

	public void setSearch_as_miejscowosc(String search_as_miejscowosc) {
		this.search_as_miejscowosc = search_as_miejscowosc;
	}

	public String getSearch_as_nrdomu() {
		return search_as_nrdomu;
	}

	public void setSearch_as_nrdomu(String search_as_nrdomu) {
		this.search_as_nrdomu = search_as_nrdomu;
	}

	public String getSearch_as_nrlokalu() {
		return search_as_nrlokalu;
	}

	public void setSearch_as_nrlokalu(String search_as_nrlokalu) {
		this.search_as_nrlokalu = search_as_nrlokalu;
	}

	public String getSearch_ai_ulica() {
		return search_ai_ulica;
	}

	public void setSearch_ai_ulica(String search_ai_ulica) {
		this.search_ai_ulica = search_ai_ulica;
	}

	public String getSearch_ai_kod() {
		return search_ai_kod;
	}

	public void setSearch_ai_kod(String search_ai_kod) {
		this.search_ai_kod = search_ai_kod;
	}

	public String getSearch_ai_miejscowosc() {
		return search_ai_miejscowosc;
	}

	public void setSearch_ai_miejscowosc(String search_ai_miejscowosc) {
		this.search_ai_miejscowosc = search_ai_miejscowosc;
	}

	public String getSearch_ai_nrdomu() {
		return search_ai_nrdomu;
	}

	public void setSearch_ai_nrdomu(String search_ai_nrdomu) {
		this.search_ai_nrdomu = search_ai_nrdomu;
	}

	public String getSearch_ai_nrlokalu() {
		return search_ai_nrlokalu;
	}

	public void setSearch_ai_nrlokalu(String search_ai_nrlokalu) {
		this.search_ai_nrlokalu = search_ai_nrlokalu;
	}

	public String getSearch_ak_ulica() {
		return search_ak_ulica;
	}

	public void setSearch_ak_ulica(String search_ak_ulica) {
		this.search_ak_ulica = search_ak_ulica;
	}

	public String getSearch_ak_kod() {
		return search_ak_kod;
	}

	public void setSearch_ak_kod(String search_ak_kod) {
		this.search_ak_kod = search_ak_kod;
	}

	public String getSearch_ak_miejscowosc() {
		return search_ak_miejscowosc;
	}

	public void setSearch_ak_miejscowosc(String search_ak_miejscowosc) {
		this.search_ak_miejscowosc = search_ak_miejscowosc;
	}

	public String getSearch_ak_nrdomu() {
		return search_ak_nrdomu;
	}

	public void setSearch_ak_nrdomu(String search_ak_nrdomu) {
		this.search_ak_nrdomu = search_ak_nrdomu;
	}

	public String getSearch_ak_nrlokalu() {
		return search_ak_nrlokalu;
	}

	public void setSearch_ak_nrlokalu(String search_ak_nrlokalu) {
		this.search_ak_nrlokalu = search_ak_nrlokalu;
	}

	
    
	
}
