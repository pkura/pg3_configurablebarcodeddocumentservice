package pl.compan.docusafe.web.office.dictionaries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.parametrization.presale.ItemOrderDictionary;
import pl.compan.docusafe.parametrization.presale.ProductDictionary;
import pl.compan.docusafe.parametrization.presale.ProductGroupDictionary;
import pl.compan.docusafe.parametrization.presale.ZaliczkaDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ZaliczkaDictionaryAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(ZaliczkaDictionaryAction.class);
	private Long id;
	private Double amount;
	private String numerRachunku;
	private String bank;
	private Boolean documentReassigned;
	private ZaliczkaDictionary inst;
	private Boolean submit; 
	
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAdd").
			append(OpenHibernateSession.INSTANCE).
			append(new Add()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").
			append(OpenHibernateSession.INSTANCE).
			append(new Update()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if(id != null )
				{
					inst = ZaliczkaDictionary.getInstance().find(id);
					amount = inst.getAmount();
					bank = inst.getBank();
					numerRachunku = inst.getNumerRachunku();				
				}
				
			}
			catch (EdmException e)
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("",e);
			}
		}
	}

	private class Add implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				inst = new ZaliczkaDictionary();
				setValue(inst);
				inst.create();
				DSApi.context().commit();
				id = inst.getId();
				documentReassigned = true;
				setSubmit(true);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}

	private class Update implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				inst = ZaliczkaDictionary.getInstance().find(id);
				setValue(inst);
				id = inst.getId();
				DSApi.context().commit();
				documentReassigned = true;
				setSubmit(true);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}
	
	private void setValue(ZaliczkaDictionary itm) throws EdmException
	{
		itm.setAmount(amount);
		itm.setBank(bank);
		itm.setNumerRachunku(numerRachunku);
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Double getAmount()
	{
		return amount;
	}

	public void setAmount(Double amount)
	{
		this.amount = amount;
	}

	public String getNumerRachunku()
	{
		return numerRachunku;
	}

	public void setNumerRachunku(String numerRachunku)
	{
		this.numerRachunku = numerRachunku;
	}

	public String getBank()
	{
		return bank;
	}

	public void setBank(String bank)
	{
		this.bank = bank;
	}

	public Boolean getDocumentReassigned()
	{
		return documentReassigned;
	}

	public void setDocumentReassigned(Boolean documentReassigned)
	{
		this.documentReassigned = documentReassigned;
	}

	public void setSubmit(Boolean submit)
	{
		this.submit = submit;
	}

	public Boolean getSubmit()
	{
		return submit;
	}
}
