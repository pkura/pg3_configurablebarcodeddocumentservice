package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.dictionary.DicAegonCommission;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class DicAegonCommissionAction extends EventActionSupport 
{
	public static final String URL = "/office/common/dicaegoncommission.action";
	public static final int LIMIT = 15;
	
	private Long id;
	private String numer_agenta;
	private String nip;
	private String nazwa_posrednik_agencja;
	
	private Long search_id;
	private String search_numer_agenta;
	private String search_nip;
	private String search_nazwa_posrednik_agencja;
	
	private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    
    private DicAegonCommission inst;
    private Pager pager;
    private int offset;
    private List<? extends DicAegonCommission> results;
	
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
	protected void setup() 
	{
		FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doSearch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
	    registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			canRead = true;   //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_ODCZYTYWANIE);
                canAdd = true;    //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                canDelete = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                
                if(id != null)
                {
                	inst = DicAegonCommission.getInstance().find(id);
                	if(inst != null)
                	{
                		numer_agenta = inst.getNumer_agenta(); 
                		nazwa_posrednik_agencja = inst.getNazwa_posrednik_agencja();
                        nip = inst.getNip();
                	}
                }
                DSApi.context().commit();
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
    		}
    	}
    }

	private class Add implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canAdd = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_DODAWANIE);
                if(!canAdd) throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                
                List<String> lista = Validate();
                if(lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DicAegonCommission();
                
                inst.setNumer_agenta(numer_agenta);
                inst.setNazwa_posrednik_agencja(nazwa_posrednik_agencja);
                inst.setNip(nip);

                inst.create();
                
                id = inst.getId();
    			
    			DSApi.context().commit();
    			
    			addActionMessage(sm.getString("WslownikuUtworzono",inst.getNumer_agenta()));
    		}
    		catch(EdmException e)
    		{
    			try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                	e.printStackTrace();
                    addActionError(e1.getMessage());
                }
                e.printStackTrace();
                addActionError(e.getMessage());
    		}
    	}
    }
	
	private class Delete implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			canDelete = true; //DSApi.context().hasPermission(DSPermission.DF_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                inst = DicAegonCommission.getInstance().find(id);
    			
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst,getNumer_agenta()));
                
    			DSApi.context().commit();
    		}
    		catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
    	}
    }
	
	private class Update implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			DicAegonCommission inst = DicAegonCommission.getInstance().find(id);
    			
    			inst.setNumer_agenta(numer_agenta);
                inst.setNazwa_posrednik_agencja(nazwa_posrednik_agencja);
                inst.setNip(nip);
                
    			DSApi.context().session().save(inst);
    			DSApi.context().commit();
    		}
    		catch(EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
    	}
    }
	
	private class Search implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			QueryForm form = new QueryForm(offset, LIMIT);
    			
    			if(nazwa_posrednik_agencja != null) form.addProperty("nazwa_posrednik_agencja", nazwa_posrednik_agencja);
    			if(numer_agenta != null) form.addProperty("numer_agenta", numer_agenta);
    			if(nip != null) form.addProperty("nip", nip);

                
                form.addOrderAsc("numer_agenta");
                
                SearchResults<? extends DicAegonCommission> results = DicAegonCommission.search(form);
                
                if (results.totalCount() == 0)  
                	throw new EdmException(sm.getString("NieZnalezionoRekorduPasujacychDoWpisanychDanych"));
                else
                {
                	search_nazwa_posrednik_agencja = nazwa_posrednik_agencja;
                	search_numer_agenta = numer_agenta; 
                	search_nip = nip;
                }
                
                DicAegonCommissionAction.this.results = fun.list(results);
                
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "numer_agenta", numer_agenta,
                                "nazwa_posrednik_agencja", nazwa_posrednik_agencja,
                                "nip", nip,
                                "param", param,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
    		}
    		catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
    	}
    }
	
	private class Clean implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		nazwa_posrednik_agencja = TextUtils.trimmedStringOrNull(nazwa_posrednik_agencja);
    		numer_agenta = TextUtils.trimmedStringOrNull(numer_agenta);
    		nip = TextUtils.trimmedStringOrNull(nip);
    	}
    }
	
	private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        return lista;
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumer_agenta() {
		return numer_agenta;
	}

	public void setNumer_agenta(String numer_agenta) {
		this.numer_agenta = numer_agenta;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNazwa_posrednik_agencja() {
		return nazwa_posrednik_agencja;
	}

	public void setNazwa_posrednik_agencja(String nazwa_posrednik_agencja) {
		this.nazwa_posrednik_agencja = nazwa_posrednik_agencja;
	}

	public Long getSearch_id() {
		return search_id;
	}

	public void setSearch_id(Long search_id) {
		this.search_id = search_id;
	}

	public String getSearch_numer_agenta() {
		return search_numer_agenta;
	}

	public void setSearch_numer_agenta(String search_numer_agenta) {
		this.search_numer_agenta = search_numer_agenta;
	}

	public String getSearch_nip() {
		return search_nip;
	}

	public void setSearch_nip(String search_nip) {
		this.search_nip = search_nip;
	}

	public String getSearch_nazwa_posrednik_agencja() {
		return search_nazwa_posrednik_agencja;
	}

	public void setSearch_nazwa_posrednik_agencja(
			String search_nazwa_posrednik_agencja) {
		this.search_nazwa_posrednik_agencja = search_nazwa_posrednik_agencja;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<? extends DicAegonCommission> getResults() {
		return results;
	}

	public void setResults(List<? extends DicAegonCommission> results) {
		this.results = results;
	}

	public StringManager getSm() {
		return sm;
	}

	public void setSm(StringManager sm) {
		this.sm = sm;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public static String getURL() {
		return URL;
	}

	public DicAegonCommission getInst() {
		return inst;
	}

	public void setInst(DicAegonCommission inst) {
		this.inst = inst;
	}

}
