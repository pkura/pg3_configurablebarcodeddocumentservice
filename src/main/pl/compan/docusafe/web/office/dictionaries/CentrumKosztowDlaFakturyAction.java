package pl.compan.docusafe.web.office.dictionaries;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja s�ownikowa dla centrum kosztowych wybieranych dla faktury kosztowej.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CentrumKosztowDlaFakturyAction extends EventActionSupport 
{
	private static final Logger log = LoggerFactory.getLogger(CentrumKosztowDlaFakturyICAction.class);
    // @IMPORT/@EXPORT
    private Long id;
    private String code;
    private Integer centrumId;
    private String amount;
    private String accountNumber;
    private Long documentId;
    private String descriptionAmount;
    private Integer centrum;
    private Boolean simpleAcceptance;
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    // @EXPORT
    /** gdy r�ne od null, oznacza po wykonaniu kt�rej akcji powr�cili�my na stron� JSP */
    private String afterAction;
    private String description;
    private List<CentrumKosztow> centra;
    private List<AccountNumber> accountNumbers;
    /** oznacza, �e to centrum zosta�o "zaakceptowane" - z�o�ono akceptacj� zale�n� od tego centrum */
    private boolean accepted;
    
    private BigDecimal amountFloat;
    private String maxAmount;
    
    private static final String EV_FILL = "fill";
    private static final String EV_DO = "do";
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private FormFile file;
    
    private List<Map<String,Object>> addedFromFile;
    
    private Boolean showDescriptionAmount;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_DO, new Add()).
            append(EV_FILL, fillForm).            
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_DO, new Update()).
            append(EV_FILL, fillForm).            
            appendFinally(CloseHibernateSession.INSTANCE);        
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(EV_FILL, fillForm).            
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Validate()).
        	append(EV_DO, new LoadFile()).
        	append(EV_FILL, fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        	
    }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
			FieldsManager fm = null;
            try
            {
                accountNumbers = AccountNumber.list();
                Document document = null;
				
				if (documentId != null)
                {
                    document = Document.find(documentId);
					fm = document.getDocumentKind().getFieldsManager(document.getId());
				}
			
                if (id != null)
                {
                    CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(id);
                    code = centrum.getCentrumCode();
                    amount = centrum.getRealAmount().toString();
                    accountNumber = centrum.getAccountNumber();
                    accepted = DocumentAcceptance.find(null, null, null, id).size() > 0;  
                    documentId = centrum.getDocumentId();
					document = Document.find(documentId);
					fm = document.getDocumentKind().getFieldsManager(document.getId());
                    descriptionAmount  =centrum.getDescriptionAmount();
                }
                else
                {
                    centra  = new ArrayList<CentrumKosztow>();
                    
//                    if(DocumentKind.INVOICE_IC_KIND.equals(document.getDocumentKind().getCn()))
//                    {
//						if (CentrumKosztow.find(fm.getField(InvoiceLogic.DZIAL_FIELD_CN).getEnumItem((Integer) fm.getKey(InvoiceLogic.DZIAL_FIELD_CN)).getCentrum()).isIntercarsPowered()) {
//							centra = CentrumKosztow.list();
//							for(CentrumKosztow ck: centra){
//								ck.setSymbol(ck.getSymbol() + " (" + ck.getName() + ")");
//							}
//						} else {
//							List<Long> centraList = AcceptanceCondition.findByUserGuids();
//							CentrumKosztow ck = null;
//							for (Long ckId : centraList) {
//								ck = CentrumKosztow.find(Integer.parseInt(ckId.toString()));
//								ck.setSymbol(ck.getSymbol() + " (" + ck.getName() + ")");
//								centra.add(ck);
//							}
//						}
//                    }
//                    else
//                    {
                    	//centra = CentrumKosztow.listBy("symbol");
                    	for(CentrumKosztow ck : CentrumKosztow.listBy("symbol"))
	                    {
                    		ck.setSymbol(ck.getSymbol()+" ("+ck.getName()+")");
           				 	centra.add(ck);
	                    }
//                    }
                    amount = TextUtils.trimmedStringOrNull(amount);
                }
                
                if (documentId != null)
                {
                	DocumentKind dk = document.getDocumentKind();
                    dk.initialize();
                    
                    showDescriptionAmount = false;
                    //DocumentKind dk = Document.find(documentId).getDocumentKind();
                    //dk.initialize();
                    
                    if ("true".equals(dk.getProperties().get(DocumentKind.SHOW_DESCRIPTION_AMOUNT_KEY)))
                    {
                    	showDescriptionAmount = true;
                    }
                    
                    if(centrum != null)
                    {                    	
            	        fm.initialize();
               	        fm.initializeAcceptances();
            	        DataBaseEnumField dbEnum = (DataBaseEnumField) fm.getField(InvoiceLogic.DZIAL_FIELD_CN);
            	        centrumId = dbEnum.getEnumItem(centrum).getCentrum();
                    }
                    
                    String exactAmountFieldCn = document.getDocumentKind().getAcceptancesDefinition().getFinalAcceptance().getExactAmountFieldCn();
                    if (exactAmountFieldCn != null && (fm.getKey(exactAmountFieldCn) instanceof Double))
                    {
                        // obliczam kwote calkowita
                        Double fullAmount = (Double) fm.getKey(exactAmountFieldCn);
                        // obliczam sume aktualnych kwot czastkowych
                        List<CentrumKosztowDlaFaktury> list = CentrumKosztowDlaFaktury.findByDocumentId(documentId);
                        BigDecimal tmpAmount = new BigDecimal(0);
                        for (CentrumKosztowDlaFaktury centrum : list)
                            tmpAmount = tmpAmount.add(centrum.getRealAmount() == null ? new BigDecimal(0) : centrum.getRealAmount());
                        if (id != null)
                            tmpAmount = tmpAmount.subtract(new BigDecimal(amount == null ? "0" : amount));
                        
                        // okre�lam maksymaln� kwot�, kt�r� mo�na wybra�
                        maxAmount = (new BigDecimal(fullAmount)).subtract(tmpAmount).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                        
                        if (id == null && amount == null)
                        {
                            // ustalam domy�lnie kwot� na brakuj�c� reszte do pe�nej kwoty
                            amount = maxAmount;
                        }                                                        
                    }  
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                e.printStackTrace();
            } 
        }
        
    }
    
    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            amount = TextUtils.trimmedStringOrNull(amount);
            accountNumber = TextUtils.trimmedStringOrNull(accountNumber);
            descriptionAmount = TextUtils.trimmedStringOrNull(descriptionAmount, 600);
            if (amount == null)
            {
                addActionError(sm.getString("KwotaCzastkowaJestPolemObowiazkowym"));
                event.skip(EV_DO);
                return;
            }
            try
            {
                String s = amount.toString().replace(",", ".");
                amountFloat = new BigDecimal(s);
				amountFloat.setScale(2, RoundingMode.HALF_UP);
            }
        
            catch (NumberFormatException e)
            {
                addActionError(sm.getString("PodanoNiepoprawnaKwote"));
                event.skip(EV_DO);
                return;
            }
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	if(centrumId == null)
        	{
        		addActionError("Musisz wybra� centrum");
        		return;
        	}
            try
            {
                DSApi.context().begin(); 
                Document document = Document.find(documentId);          
				
				FieldsManager fm = document.getFieldsManager();
                
                CentrumKosztowDlaFaktury centrum = new CentrumKosztowDlaFaktury();
                centrum.setAccountNumber(accountNumber);
                centrum.setRealAmount(amountFloat);
                
                CentrumKosztow c = CentrumKosztow.find(centrumId);
                centrum.setCentrumCode(c.getCn());
                centrum.setCentrumId(centrumId);
                centrum.setDocumentId(documentId);
                centrum.setDescriptionAmount(descriptionAmount);

				if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND)) {
					CentrumKosztow documentCentrum = CentrumKosztow.find(fm.getField(InvoiceLogic.DZIAL_FIELD_CN).getEnumItem((Integer) fm.getKey(InvoiceLogic.DZIAL_FIELD_CN)).getCentrum());

					if (documentCentrum.isIntercarsPowered()) {
						centrum.setAcceptingCentrumId(documentCentrum.getId());
					}
				}
                
                Persister.create(centrum);                                                                
                //simpleAcceptance = c.getDefaultSimpleAcceptance();
                
                description = centrum.getDescription();
                id = centrum.getId();
                
                // dodajemy wpis do historii
                if(document.getDocumentKind().getCn().equalsIgnoreCase(DocumentLogicLoader.INVOICE_IC_KIND))
                	simpleAcceptance = c.getDefaultSimpleAcceptance(); 
                else
                	simpleAcceptance = false;
                
                if (document instanceof OfficeDocument)
                {
                    ((OfficeDocument) document).addWorkHistoryEntry(
                        Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                            sm.getString("PrzypisanoDoDokumentuCentrumKosztowe",centrum.getCentrumCode()),
                            centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                }
                
                // modyfikacja uprawnie�
                document.getDocumentKind().logic().documentPermissions(document);
                
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(documentId);

				if(document.getDocumentKind().logic().getAcceptanceManager() != null){
					document.getDocumentKind().logic().getAcceptanceManager().refresh(document,fm, null);
				}
                                
                DSApi.context().commit();                 
                                
                afterAction = "add";
                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
        
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.context().begin();
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(id);
                
            	String oldAccountNumber =  centrum.getAccountNumber();
            	BigDecimal oldAmountFloat = centrum.getRealAmount();
            	String oldDescriptionAmount = centrum.getDescriptionAmount();            	
                
                centrum.setAccountNumber(accountNumber);
                centrum.setRealAmount(amountFloat);
                centrum.setDescriptionAmount(descriptionAmount);
                
                Document document = Document.find(documentId);
                if(oldAccountNumber != null && !oldAccountNumber.equals(accountNumber))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoKontoZna",oldAccountNumber,accountNumber,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }
                if(oldAmountFloat != null && !oldAmountFloat.equals(amountFloat))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoKwoteZna",oldAmountFloat,amountFloat,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }
                if(oldDescriptionAmount != null && !oldDescriptionAmount.equals(descriptionAmount))
                {
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("ZmienionoOpisZna",oldDescriptionAmount,descriptionAmount,centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                }

                description = centrum.getDescription();
                
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(centrum.getDocumentId());
                
                DSApi.context().commit();
                
                afterAction = "update";                
                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    private class LoadFile implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		//przepisuje linie z pliku do listy
    		List<String> lista = new ArrayList<String>();
    		//lista dla iteratora z pliku centrum-kosztow.jsp
    		addedFromFile = new LinkedList<Map<String,Object>>();
    		try 
    		{

    			try 
    			{
    				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getFile()), "cp1250"));
    				String line = "";
    				int lineCount = 0;
    				while ((line = reader.readLine()) != null)
    				{
    					lineCount++;
    					line = line.trim();
    					//ignorujemy 1 linie pliku [jdenak nie ignorujemy bo Paluch nie kce]
    					if(line.startsWith("#")) continue;
    					lista.add(line);
    				}
    			} 
    			catch(Exception e) 
    			{
    				throw new EdmException(sm.getString("BrakPliku"), e);
    			}
    			//dzili linie i zapisuje
    			DSApi.context().begin();
    			Document document = null;
    			for(int i=0;i<lista.size();i++)
    			{
     				String[] parts = lista.get(i).split("\\s*;\\s*");

     				if(parts.length < 3)
     					throw new EdmException("Z�e formatowanie pliku csv.");

    				CentrumKosztowDlaFaktury centrum = new CentrumKosztowDlaFaktury();

    				if(parts[1].length() > 0)
    				{
    					AccountNumber an = AccountNumber.findByNumber(parts[1]);
    					if(an == null)
    					{
    						throw new EdmException(sm.getString("BrakKontaONumerze"));
    					}
    					else if(!an.isAvailable())
    					{
    						throw new EdmException(sm.getString("KontoJestUkryte",an.getName()));
    					}
    					centrum.setAccountNumber(an.getNumber());
    					
    					/*try 
    					{
                        	centrum.setAccountNumber(AccountNumber.findByNumber(parts[1]).getNumber());
                        } 
    					catch(Exception e) 
    					{
                        	throw new EdmException(sm.getString("BrakKontaONumerze"));
                        }*/
    				} 
    				else centrum.setAccountNumber(null);

                    try 
                    {
                    	centrum.setAmount(new BigDecimal(parts[2]));
                    } 
                    catch(Exception e) 
                    {
                    	throw new EdmException(sm.getString("KwotaNieJestLiczba"),e);
                    }


                    CentrumKosztow c = CentrumKosztow.findBySymbolInAll(parts[0]);
                    if(c == null)
                    {
                    	throw new EdmException(sm.getString("BrakCentrumONumerze"));
                    }
                    else if(!c.isAvailable())
                    {
                    	throw new EdmException(sm.getString("CentrumJestUkryte",c.getName()));
                    }
                    centrum.setCentrumCode(c.getCn());
                    centrum.setCentrumId(c.getId());
                    centrum.setDocumentId(documentId);
                    Persister.create(centrum);
                    description = centrum.getDescription();
                    id = centrum.getId();
                    simpleAcceptance = c.getDefaultSimpleAcceptance();
                    //dodajemy wpis do historii
                    document = Document.find(documentId);
                    if (document instanceof OfficeDocument)
                    {
                       ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.createWithPriority("addCentrum", DSApi.context().getPrincipalName(),
                                sm.getString("PrzypisanoDoDokumentuCentrumKosztowe",centrum.getCentrumCode()),
                                centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                    }
                    //wypelnia liste dla iteratora w centrum-kosztow.jsp
    				Map<String,Object> bean = new LinkedHashMap<String, Object>();
    				bean.put("ff_id", centrum.getDocumentId());
    				bean.put("ff_description", centrum.getDescription());
    				bean.put("ff_amount",centrum.getAmount());
    				addedFromFile.add(bean);
    			}
    			
    			//if(document != null)
    			//	document.getDocumentKind().logic().documentPermissions(document);
    			checkFinalAcceptance(documentId);
    			
    			afterAction = "LoadFile";
                event.skip(EV_FILL);
    			DSApi.context().commit();
    		}
    		catch (EdmException e) {
				log.error(e.getMessage(), e);
    			addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
    		}
    	}
    }
    
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.context().begin();
                
                CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(id);
                Persister.delete(centrum);
                
                // dodajemy wpis do historii
                Document document = Document.find(centrum.getDocumentId());
                if (document instanceof OfficeDocument)
                {
                    ((OfficeDocument) document).addWorkHistoryEntry(
                        Audit.createWithPriority("removeCentrum", DSApi.context().getPrincipalName(),
                            sm.getString("UsunietoZdokumentuCentrumKosztowe",centrum.getCentrumCode()),
                            centrum.getCentrumCode(), Audit.LOWEST_PRIORITY));
                }
                
                // modyfikacja uprawnie�
                document.getDocumentKind().logic().documentPermissions(document);
                
                // sprawdzamy czy nie ulegla zmianie akceptacja finalna dokumentu
                checkFinalAcceptance(centrum.getDocumentId());
                
                DSApi.context().commit();    
                
                afterAction = "delete";
                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }   
    
    private void checkFinalAcceptance(Long documentId) throws EdmException
    {
        Document document = Document.find(documentId);
        document.getDocumentKind().initialize();
        AcceptancesManager.checkFinalAcceptance(document);
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getCentrumId()
    {
        return centrumId;
    }

    public void setCentrumId(Integer centrumId)
    {
        this.centrumId = centrumId;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public String getAfterAction()
    {
        return afterAction;
    }

    public List<CentrumKosztow> getCentra()
    {
        return centra;
    }

    public List<AccountNumber> getAccountNumbers()
    {
        return accountNumbers;
    }

    public String getMaxAmount()
    {
        return maxAmount;
    }

    public String getDescription()
    {
        return description;
    }

    public boolean isAccepted()
    {
        return accepted;
    }    
    
    public FormFile getFile() {
    	return this.file;
    }
    
    public void setFile(FormFile file) 
    {
    	this.file = file;
    }
    
    public List<Map<String, Object>> getAddedFromFile() {
	 	return addedFromFile;
	 }

	public String getDescriptionAmount() {
		return descriptionAmount;
	}

	public void setDescriptionAmount(String descriptionAmount) {
		this.descriptionAmount = descriptionAmount;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public Boolean getShowDescriptionAmount() {
		return showDescriptionAmount;
	}

	public void setShowDescriptionAmount(Boolean showDescriptionAmount) {
		this.showDescriptionAmount = showDescriptionAmount;
	}

	public Boolean getSimpleAcceptance() {
		return simpleAcceptance;
	}

	public void setSimpleAcceptance(Boolean simpleAcceptance) {
		this.simpleAcceptance = simpleAcceptance;
	}
    
}
