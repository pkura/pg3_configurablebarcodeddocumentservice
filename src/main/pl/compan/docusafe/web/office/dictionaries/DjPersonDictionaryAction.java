package pl.compan.docusafe.web.office.dictionaries;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.parametrization.presale.DJPersonDictionary;
import pl.compan.docusafe.parametrization.presale.DJSklepDictionary;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import std.fun;
/**
 * Akcja s�ownika dla pracownikow UL
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class DjPersonDictionaryAction extends EventActionSupport
{

    public static final String URL = "/office/common/personDictionary.action";
    public static final int LIMIT = 15;
    
    //@IMPORT/EXPORT
    private Long   id;
    private String idPracownika;
    private String nazwisko;
    private String imie;
    private String pesel;
    private List<DJSklepDictionary> sklepy;
    private DJSklepDictionary sklep;
    private Long nrsklepu;
    private Boolean pracuje;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    
    private String nrSap;
    private String nrPeoplesoft;
    
    private DJPersonDictionary inst;
    private Pager pager;
    private int offset;
    private List<? extends DJPersonDictionary> results;
    private static final String EV_FILL = "fill";
    private static final String EV_DO = "do";
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(EV_DO, new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddDuplicate").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(EV_DO, new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(EV_DO, new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                canRead = true; //DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = true; //DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = true; //DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = true;
                sklepy = DJSklepDictionary.list();
            }
            catch (Exception e) 
            {
                addActionError(e.getMessage());
            }
            if (id != null) 
            {
                try 
                {
                    DSApi.context().begin();
                    inst = DJPersonDictionary.getInstance().find(id);
                } 
                catch (EdmException e) 
                {
                    try 
                    {
                        DSApi.context().rollback();
                    } 
                    catch (EdmException e1) 
                    {
                        addActionError(e1.getMessage());
                    }
                    addActionError(e.getMessage());
                }
                finally
                {
                    try { DSApi.context().commit(); } catch (Exception e) { }
                }
                if (inst != null) 
                {
                    id = inst.getId();
                    imie = inst.getImie();
                    idPracownika = inst.getIdPracownika();
                    nazwisko = inst.getNazwisko();
                    pesel = inst.getPesel();
                    sklep = inst.getNrsklepu();
                    nrsklepu = inst.getNrsklepu() != null ? inst.getNrsklepu().getId() : null;
                    pracuje = inst.getPracuje();
                    nrSap = inst.getNrSap();
                    nrPeoplesoft = inst.getNrPeoplesoft();
                }
            }
            
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canAdd = true; //DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

               
                
                if(idPracownika.length() > 5)
                {
                	idPracownika = StringUtils.left(idPracownika, 5);
                	addActionMessage("ID Pracownika nie mo�e przekracza� pi�ciu cyfr");
                }
                try
            	{
            		Integer.parseInt(idPracownika);
            	}
            	catch (NumberFormatException e) 
				{
					addActionError("ID pracownika musi by� ci�giem cyfr");
					return;
				}
            	inst = new DJPersonDictionary();
                inst.setIdPracownika(idPracownika);
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                if(nrsklepu != null)
                {
                	inst.setNrsklepu(DJSklepDictionary.find(nrsklepu));
                }                
                inst.setPesel(pesel);
                inst.setNrSap(nrSap);
                inst.setNrPeoplesoft(nrPeoplesoft);
                inst.create();
                inst.setPracuje(pracuje);                
                id = inst.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono",imie+" "+nazwisko));
            } 
            catch (EdmException e)
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
               

                canDelete = true; //DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete)
                {
                	throw new EdmException("Nie masz uprawnie� do usuwania dokument�w");
                }
                DSApi.context().begin();
                inst = DJPersonDictionary.getInstance().find(id);
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getImie()+" "+inst.getNazwisko()));
                
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {  
            try
            {
                DSApi.context().begin();
                DJPersonDictionary inst = DJPersonDictionary.getInstance().find(id);
                
                inst.setIdPracownika(idPracownika);
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setNrsklepu(DJSklepDictionary.find(nrsklepu));
                inst.setPesel(pesel);
                inst.setPracuje(pracuje);
                inst.setNrSap(nrSap);
                inst.setNrPeoplesoft(nrPeoplesoft);
                
                DSApi.context().session().save(inst);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }
        }
    }  
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);
                
                if (nrsklepu != null)
                {
                	sklep = DJSklepDictionary.find(nrsklepu);
                	form.addProperty("nrsklepu",sklep);
                }
                if (idPracownika != null) form.addProperty("idPracownika",idPracownika);
                if (imie != null) form.addProperty("imie",imie);
                if ( nazwisko != null) form.addProperty("nazwisko",nazwisko);
                if (pesel != null) form.addProperty("pesel",pesel);
                if (pracuje != null) form.addProperty("pracuje",pracuje);
                if (nrSap != null) form.addProperty("nrSap",nrSap);
                if (nrPeoplesoft != null) form.addProperty("nrPeoplesoft",nrPeoplesoft);
                
                form.addOrderAsc("idPracownika");

                SearchResults<? extends DJPersonDictionary> results = DJPersonDictionary.search(form);

                if (results == null || results.totalCount() == 0)
                {
                    throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                }
                DjPersonDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",		
                                
								"idPracownika",idPracownika,
								"imie",imie,
								"nazwisko",nazwisko,
								"nrsklepu",nrsklepu,
								"pesel",pesel,
								"nrSap",nrSap,
								"nrPeoplesoft",nrPeoplesoft,
                                "param", param, 
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdPracownika() {
		return idPracownika;
	}

	public void setIdPracownika(String idPracownika) {
		this.idPracownika = idPracownika;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public Long getNrsklepu() {
		return nrsklepu;
	}

	public void setNrsklepu(Long nrsklepu) {
		this.nrsklepu = nrsklepu;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanRead() {
		return canRead;
	}

	public void setCanRead(Boolean canRead) {
		this.canRead = canRead;
	}

	public Boolean getCanEdit() {
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public List<? extends DJPersonDictionary> getResults() {
		return results;
	}

	public void setResults(List<? extends DJPersonDictionary> results) {
		this.results = results;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public List<DJSklepDictionary> getSklepy() {
		return sklepy;
	}

	public void setSklepy(List<DJSklepDictionary> sklepy) {
		this.sklepy = sklepy;
	}

	public DJSklepDictionary getSklep() {
		return sklep;
	}

	public void setSklep(DJSklepDictionary sklep) {
		this.sklep = sklep;
	}

	public Boolean getPracuje() {
		return pracuje;
	}

	public void setPracuje(Boolean pracuje) {
		this.pracuje = pracuje;
	}

	public String getNrSap() {
		return nrSap;
	}

	public void setNrSap(String nrSap) {
		this.nrSap = nrSap;
	}

	public String getNrPeoplesoft() {
		return nrPeoplesoft;
	}

	public void setNrPeoplesoft(String nrPeoplesoft) {
		this.nrPeoplesoft = nrPeoplesoft;
	}
}
