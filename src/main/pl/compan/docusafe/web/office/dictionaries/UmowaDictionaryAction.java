package pl.compan.docusafe.web.office.dictionaries;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.parametrization.presale.UmowaDictionary;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class UmowaDictionaryAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(UmowaDictionaryAction.class);
	private Long id;
	private String termin;
	private DicInvoice kontrahent;
	private Long kontrahent_id;
	private String nazwaKlienta;
	private Attachment zalacznik;
	private Long zalacznik_id;
	private Double kwota;
	private String param;
	private Integer offset = 0;
	private List<? extends UmowaDictionary> results;
	private static final Integer LIMIT = 10;
	public static final String URL = "/office/common/umowa.action";
	private Pager pager;
	private FormFile file;
	private AttachmentRevision mostRecentRevision;
	
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAdd").
			append(OpenHibernateSession.INSTANCE).
			append(new Add()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").
			append(OpenHibernateSession.INSTANCE).
			append(new Update()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSearch").
			append(OpenHibernateSession.INSTANCE).
			append(new Search()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
			
		registerListener("doAddAtta").
			append(OpenHibernateSession.INSTANCE).
			append(new AddAtta()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if(id != null )
				{
					UmowaDictionary ud = new UmowaDictionary();
					UmowaDictionary inst =ud.find(id);
					if(inst.getTermin() != null)
					termin = DateUtils.formatJsDate(inst.getTermin());
					kontrahent = inst.getKontrahent();
					if(kontrahent == null && kontrahent_id != null)
					{
						kontrahent = DicInvoice.getInstance().find(kontrahent_id);
					}
					if(kontrahent != null)
					{
						kontrahent_id = kontrahent.getId();
						nazwaKlienta = kontrahent.getName();
					}
					kwota = inst.getKwota();
					zalacznik = inst.getZalacznik();
					if(zalacznik != null)
					{
						zalacznik_id = zalacznik.getId();
						setMostRecentRevision(zalacznik.getMostRecentRevision());
					}
				}
			}
			catch (EdmException e)
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("",e);
			}
		}
	}
	
	private class AddAtta implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if(id == null)
					throw new EdmException("Nie wybrano umowy");
				DSApi.context().begin();
				UmowaDictionary ud = new UmowaDictionary();
				UmowaDictionary inst = ud.find(id);
				Attachment atta = new Attachment("Umowa");
				atta.setType(Attachment.ATTACHMENT_TYPE_UMOWA);
		        DSApi.context().session().saveOrUpdate(atta);				
				atta.createRevision(file.getFile());
				inst.setZalacznik(atta);
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("",e);
			}
		}
	}
	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
                QueryForm form = new QueryForm(offset, LIMIT);

                if (kwota != null)
                {
					form.addProperty("kwota", kwota);
                }
				if (kontrahent_id != null)
				{
					form.addProperty("kontrahent", kontrahent_id);
				}
				if (termin != null)
				{
					form.addProperty("termin", termin);
				}

                form.addOrderAsc("termin");
                SearchResults<? extends UmowaDictionary> tmpresults = UmowaDictionary.search(form);
                if (tmpresults == null || tmpresults.totalCount() == 0)
                {
                    throw new EdmException("Nie znaleziono umowy");
                }

               results = fun.list(tmpresults);
				for (UmowaDictionary umD : results)
				{
					umD.getZalacznik().getMostRecentRevision();
				}

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                            
                                "doSearch", "true",
                                "kontrahent_id", kontrahent_id,
                                "kwota", kwota,
                                "termin", termin,
                                "param", param,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT, tmpresults.totalCount(), 10);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
			}
		}
	}

	private class Add implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				UmowaDictionary inst = new UmowaDictionary();
				setValue(inst);
				inst.create();
				DSApi.context().commit();
				id = inst.getId();
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}

	private class Update implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				UmowaDictionary ud = new UmowaDictionary();
				UmowaDictionary inst =ud.find(id);
				setValue(inst);
				id = inst.getId();
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}
	
	private void setValue(UmowaDictionary itm) throws EdmException
	{
		if(kontrahent_id != null)
			itm.setKontrahent(DicInvoice.getInstance().find(kontrahent_id));
		itm.setKwota(kwota);
		if(termin != null && termin.length() > 0)
			itm.setTermin(DateUtils.parseJsDate(termin));
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTermin()
	{
		return termin;
	}

	public void setTermin(String termin)
	{
		this.termin = termin;
	}

	public DicInvoice getKontrahent()
	{
		return kontrahent;
	}

	public void setKontrahent(DicInvoice kontrahent)
	{
		this.kontrahent = kontrahent;
	}

	public Long getKontrahent_id()
	{
		return kontrahent_id;
	}

	public void setKontrahent_id(Long kontrahent_id)
	{
		this.kontrahent_id = kontrahent_id;
	}

	public Attachment getZalacznik()
	{
		return zalacznik;
	}

	public void setZalacznik(Attachment zalacznik)
	{
		this.zalacznik = zalacznik;
	}

	public Long getZalacznik_id()
	{
		return zalacznik_id;
	}

	public void setZalacznik_id(Long zalacznik_id)
	{
		this.zalacznik_id = zalacznik_id;
	}

	public Double getKwota()
	{
		return kwota;
	}

	public void setKwota(Double kwota)
	{
		this.kwota = kwota;
	}

	public void setParam(String param)
	{
		this.param = param;
	}

	public String getParam()
	{
		return param;
	}

	public void setNazwaKlienta(String nazwaKlienta)
	{
		this.nazwaKlienta = nazwaKlienta;
	}

	public String getNazwaKlienta()
	{
		return nazwaKlienta;
	}

	public Integer getOffset()
	{
		return offset;
	}

	public void setOffset(Integer offset)
	{
		this.offset = offset;
	}

	public List<? extends UmowaDictionary> getResults()
	{
		return results;
	}

	public void setResults(List<? extends UmowaDictionary> results)
	{
		this.results = results;
	}

	public Pager getPager()
	{
		return pager;
	}

	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	public void setFile(FormFile file)
	{
		this.file = file;
	}

	public FormFile getFile()
	{
		return file;
	}

	public void setMostRecentRevision(AttachmentRevision mostRecentRevision)
	{
		this.mostRecentRevision = mostRecentRevision;
	}

	public AttachmentRevision getMostRecentRevision()
	{
		return mostRecentRevision;
	}
}
