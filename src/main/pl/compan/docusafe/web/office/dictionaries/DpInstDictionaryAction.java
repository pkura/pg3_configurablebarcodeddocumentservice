package pl.compan.docusafe.web.office.dictionaries;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.DpLogic;
import pl.compan.docusafe.parametrization.aegon.InvoicePteLogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.Pager;

import std.fun;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.dockinds.logic.DpPteLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Akcja s�ownikowa dla s�ownika Instytucji/kontrahent�w (dla dokument�w prawnych i dokument�w prawnych pte).
 * 
 * @author Piotr Komisarski mailto:komisarskip@student.mini.pw.edu.pl
 */

public class DpInstDictionaryAction extends EventActionSupport 
{
	private static final Logger log = LoggerFactory.getLogger(DpInstDictionaryAction.class);
    public static final String URL = "/office/common/inst.action";
    public static final int LIMIT = 15;
    
    // @IMPORT/EXPORT
    private Long id;
    private String imie;
    private String nazwisko;
    private String name;
    private String oldname;
    private String ulica;
    private String kod;
    private String miejscowosc;
    private String telefon;
    private String faks;
    private String email;
    private String kraj;
    private String nip;
    private String numerKontrahenta;
    
    private Long searchid;
    private String searchimie;
    private String searchnazwisko;
    private String searchname;
    private String searcholdname;
    private String searchulica;
    private String searchkod;
    private String searchmiejscowosc;
    private String searchtelefon;
    private String searchfaks;
    private String searchemail;
    private String searchkraj;
    private String searchnumerKontrahenta;
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canModify;
    
    private DpInst inst;
    private Pager pager;
    private int offset;
    private List<? extends DpInst> results;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    private final String RENAME_ACTION = "rename";
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
    
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Update()).
            append(RENAME_ACTION, new RenameFolders()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                //DSApi.context().begin();

				canRead = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_ODCZYTYWANIE);
				canAdd = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_DODAWANIE);
				canDelete = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_USUWANIE);
				canModify = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_MODYFIKACJA);

                if (id != null) 
                {
                    inst = DpInst.getInstance().find(id);
                    if (inst!= null) 
                    {
                        imie = inst.getImie();
                        nazwisko = inst.getNazwisko();
                        name = inst.getName();
                        oldname = inst.getOldname();
                        ulica = inst.getUlica();
                        kod = inst.getKod();
                        miejscowosc = inst.getMiejscowosc();
                        telefon = inst.getTelefon();
                        faks = inst.getFaks();
                        email = inst.getEmail();
                        kraj = inst.getKraj();
                        nip = inst.getPrettyNip();
                        numerKontrahenta = inst.getNumerKontrahenta();
                    }
                    else 
                        kraj = "PL";
                }
                else 
                    kraj = "PL";
                //DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                /*try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }*/
                addActionError(e.getMessage());
            }

        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {           
            try 
            {
                DSApi.context().begin();

                canAdd = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

                List<String> lista = Validate();
                if (lista.size()>0)
                {
                    for(int i=0; i<lista.size()-1;i++)
                        addActionError(lista.get(i));
                    throw new EdmException(lista.get(lista.size()-1));
                }
                
                inst = new DpInst();
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setKraj(kraj);
                inst.setFaks(faks);
                inst.setTelefon(telefon);
                inst.setNip(nip);
                inst.setNumerKontrahenta(numerKontrahenta);
                inst.create();

                id = inst.getId();
                
                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono", inst.getName()));
            } 
            catch (EdmException e)
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }  

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	boolean shouldRenameFolders = false;
        	
            try
            {                           
                DSApi.context().begin();
                
                DpInst inst = DpInst.getInstance().find(id);
                
                if (name != null && !name.equals(inst.getName()))
                	shouldRenameFolders = true;
                	
                inst.setImie(imie);
                inst.setNazwisko(nazwisko);
                inst.setName(name);
                inst.setOldname(oldname);
                inst.setUlica(ulica);
                inst.setKod(kod);
                inst.setMiejscowosc(miejscowosc);
                inst.setEmail(email);
                inst.setKraj(kraj);
                inst.setFaks(faks);
                inst.setTelefon(telefon);
                inst.setNip(nip);
                inst.setNumerKontrahenta(numerKontrahenta);
                
                DSApi.context().session().save(inst);
                
                DSApi.context().commit();                
            }
            catch(EdmException e)
            {
                //DSApi.context().setRollbackOnly();
            	DSApi.context()._rollback();
            	log.debug(e.getMessage(),e);
            	shouldRenameFolders = false;
            }
            
            /* poprawienie folderow */
            /*if(shouldRenameFolders)
            {
	            try
	            {
	            	DSApi.context().begin();
	            	
	            	DpInst inst = DpInst.find(id);
	            	inst.cleanFolders();
	            	
	            	DSApi.context().commit();              	
	            }
	            catch (Exception e) 
	            {
	            	//DSApi.context().setRollbackOnly();
	            	DSApi.context()._rollback();
	            	log.debug(e.getMessage(),e);
				}
            }*/
            
            if(!shouldRenameFolders)
            {
            	event.skip(RENAME_ACTION);
            }
            
        }
    }  
    
    private class RenameFolders implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
            {
            	DSApi.context().begin();
            	
            	DpInst inst = DpInst.getInstance().find(id);
            	inst.cleanFolders();
            	
            	DSApi.context().commit();              	
            }
            catch (Exception e) 
            {
            	DSApi.context()._rollback();
            	log.debug(e.getMessage(),e);
            }
    	}
    }
    
    
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DP_SLOWNIK_USUWANIE);
                if (!canDelete) 
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaZeSlownika"));

                inst = DpInst.getInstance().find(id);

				//zak�adam, �e te dokumenty nigy nie b�d� jednocze�nie w jednym systemie
				if(DocumentKind.isAvailable(DocumentLogicLoader.DP_PTE_KIND))
				{
					checkDocumentsExists(DocumentLogicLoader.DP_PTE_KIND, DpPteLogic.INSTYTUCJA_FIELD_CN);
					checkDocumentsExists(DocumentLogicLoader.DP_PTE_KIND, DpPteLogic.INSTYTUCJA_KONTRAHENT_FIELD_CN);
					checkDocumentsExists(DocumentLogicLoader.DP_PTE_KIND, DpPteLogic.KANCELARIA_FIELD_CN);
					checkDocumentsExists(DocumentLogicLoader.DP_PTE_KIND, DpPteLogic.KANCELARIA_KOMORNICZA_FIELD_CN);
					checkDocumentsExists(DocumentLogicLoader.INVOICE_PTE, InvoicePteLogic.DOSTAWCA_FIELD_CN);
				} 
				else if(DocumentKind.isAvailable(DocumentLogicLoader.DP_KIND))
				{
					checkDocumentsExists(DocumentLogicLoader.DP_KIND, DpLogic.INST_FIELD_CN);
				}
                                
                inst.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoInstytucjeKontrahenta",inst.getName()));
                DSApi.context().commit();
            } 
            catch (EdmException e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }


        }

        private void checkDocumentsExists(String dockindCn, String fieldCn) throws EdmException {
                DockindQuery dockindQuery = new DockindQuery(0, 10);
                DocumentKind kind = DocumentKind.findByCn(dockindCn);
                dockindQuery.setDocumentKind(kind);
                Field field = kind.getFieldByCn(fieldCn);
                dockindQuery.field(field, id);
                SearchResults searchResults = DocumentKindsManager.search(dockindQuery);
                int x = searchResults.count();
                if (x > 0) {
                        throw new EdmException(sm.getString("NieMoznaUsunacTejInstytucjiKontrahentaPoniewazSaDoNiejPrzypisaneDokumenty"));
                }
        }
    }
   
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if(imie != null) form.addProperty("imie", imie);
                if(nazwisko != null) form.addProperty("nazwisko", nazwisko);
                if(name != null) form.addProperty("name", name);
                if(oldname != null) form.addProperty("oldname", oldname);
                if(ulica != null) form.addProperty("ulica", ulica);
                if(kod != null) form.addProperty("kod", kod);
                if(miejscowosc != null) form.addProperty("miejscowosc", miejscowosc);
                if(telefon != null) form.addProperty("telefon", telefon);
                if(faks != null) form.addProperty("faks", faks);
                if(email != null) form.addProperty("email", email);
                if(kraj != null) form.addProperty("kraj", kraj);
                if(nip != null) form.addProperty("nip", nip);
                if(numerKontrahenta != null) form.addProperty("numerKontrahenta", numerKontrahenta);

                form.addOrderAsc("name");

                SearchResults<? extends DpInst> results = DpInst.search(form);

                if (results.totalCount() == 0)  
                {
                    //afterSearch = false;
                    throw new EdmException(sm.getString("NieZnalezionoInstytucjiKontrahentowPasujacychDoWpisanychDanych"));
                }
                else 
                {
                    //afterSearch = true;
                    searchimie = imie;
                    searchnazwisko = nazwisko;
                    searchname = name;
                    searcholdname = oldname;
                    searchulica = ulica;
                    searchkod= kod;
                    searchmiejscowosc= miejscowosc;
                    searchtelefon= telefon;
                    searchfaks = faks;
                    searchemail = email;
                    searchkraj = kraj;
                    searchnumerKontrahenta = numerKontrahenta;
                }
                DpInstDictionaryAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new Object[] 
                            {
                                "doSearch", "true",
                                "imie", imie,
                                "nazwisko", nazwisko,
                                "name", name,
                                "oldname", oldname,
                                "ulica", ulica,
                                "kod", kod,
                                "miejscowosc", miejscowosc,
                                "telefon", telefon,
                                "faks", faks,
                                "email", email,
                                "kraj", kraj,
                                "nip",nip,
                                "numerKontrahenta",numerKontrahenta,
                                //"afterSearch", afterSearch ? "true" : "false",
                                "searchimie", searchimie,
                                "searchnazwisko", searchnazwisko,
                                "searchname", searchname,
                                "searcholdname", searcholdname,
                                "searchulica", searchulica,
                                "searchkod", searchkod,
                                "searchmiejscowosc", searchmiejscowosc,
                                "searchtelefon", searchtelefon,
                                "searchfaks", searchfaks,
                                "searchemail", searchemail,
                                "searchkraj", searchkraj,
                                "searchnumerKontrahenta",searchnumerKontrahenta,
                                "offset", String.valueOf(offset),
                                "param", param
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            } 
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            }
        }
    }
     
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
            imie = TextUtils.trimmedStringOrNull(imie);
            nazwisko = TextUtils.trimmedStringOrNull(nazwisko);
            name = TextUtils.trimmedStringOrNull(name);
            oldname = TextUtils.trimmedStringOrNull(oldname);
            ulica = TextUtils.trimmedStringOrNull(ulica);
            kod = TextUtils.trimmedStringOrNull(kod);
            miejscowosc = TextUtils.trimmedStringOrNull(miejscowosc);
            telefon = TextUtils.trimmedStringOrNull(telefon);
            faks = TextUtils.trimmedStringOrNull(faks);
            email = TextUtils.trimmedStringOrNull(email);
            kraj  = TextUtils.trimmedStringOrNull(kraj);  
            numerKontrahenta = TextUtils.trimmedStringOrNull(numerKontrahenta);
        }
    }
        
    private List<String> Validate() 
    {
        List<String> lista = new ArrayList<String>();
        if (StringUtils.isEmpty(name))
            lista.add(sm.getString("NiePodanoZazwy"));
        return lista;
    }
     
    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getFaks() 
    {
        return faks;
    }

    public void setFaks(String faks) 
    {
        this.faks = faks;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getImie() 
    {
        return imie;
    }

    public void setImie(String imie) 
    {
        this.imie = imie;
    }

    public String getKod() 
    {
        return kod;
    }

    public void setKod(String kod) 
    {
        this.kod = kod;
    }

    public String getKraj() 
    {
        return kraj;
    }

    public void setKraj(String kraj) 
    {
        this.kraj = kraj;
    }

    public String getMiejscowosc() 
    {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) 
    {
        this.miejscowosc = miejscowosc;
    }

    public String getNazwisko() 
    {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) 
    {
        this.nazwisko = nazwisko;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getOldname() 
    {
        return oldname;
    }

    public void setOldname(String oldname) 
    {
        this.oldname= oldname;
    }

    public String getTelefon() 
    {
        return telefon;
    }

    public void setTelefon(String telefon) 
    {
        this.telefon = telefon;
    }

    public String getUlica() 
    {
        return ulica;
    }

    public void setUlica(String ulica) 
    {
        this.ulica = ulica;
    }

    public String getSearchemail() 
    {
        return searchemail;
    }

    public void setSearchemail(String searchemail) 
    {
        this.searchemail = searchemail;
    }

    public String getSearchfaks() 
    {
        return searchfaks;
    }

    public void setSearchfaks(String searchfaks) 
    {
        this.searchfaks = searchfaks;
    }

    public Long getSearchid() 
    {
        return searchid;
    }

    public void setSearchid(Long searchid) 
    {
        this.searchid = searchid;
    }

    public String getSearchimie() 
    {
        return searchimie;
    }

    public void setSearchimie(String searchimie) 
    {
        this.searchimie = searchimie;
    }

    public String getSearchkod() 
    {
        return searchkod;
    }

    public void setSearchkod(String searchkod) 
    {
        this.searchkod = searchkod;
    }

    public String getSearchkraj() 
    {
        return searchkraj;
    }

    public String getSearchmiejscowosc() 
    {
        return searchmiejscowosc;
    }

    public void setSearchmiejscowosc(String searchmiejscowosc) 
    {
        this.searchmiejscowosc = searchmiejscowosc;
    }

    public String getSearchnazwisko() 
    {
        return searchnazwisko;
    }

    public void setSearchnazwisko(String searchnazwisko) 
    {
        this.searchnazwisko = searchnazwisko;
    }

    public String getSearchname() 
    {
        return searchname;
    }

    public void setSearchname(String searchname)
    {
        this.searchname = searchname;
    }
    
    public String getSearcholdname() 
    {
        return searcholdname;
    }

    public void setSearcholdname(String searcholdname) 
    {
        this.searcholdname = searcholdname;
    }

    public String getSearchtelefon() 
    {
        return searchtelefon;
    }

    public void setSearchtelefon(String searchtelefon) 
    {
        this.searchtelefon = searchtelefon;
    }

    public String getSearchulica() 
    {
        return searchulica;
    }

    public void setSearchulica(String searchulica) 
    {
        this.searchulica = searchulica;
    }

    public Boolean getCanAdd() 
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd) 
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete() 
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)     
    {
        this.canDelete = canDelete;
    }

    public void setSearchkraj(String searchkraj) 
    {
        this.searchkraj = searchkraj;
    }

    public DpInst getInst() 
    {
        return inst;
    }

    public void setInst(DpInst inst) 
    {
        this.inst = inst;
    }

    public Boolean getCanRead() 
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead) 
    {
        this.canRead = canRead;
    }

    public int getOffset() 
    {
        return offset;
    }

    public void setOffset(int offset) 
    {
        this.offset = offset;
    }

    public Pager getPager() 
    {
        return pager;
    }

    public void setPager(Pager pager) 
    {
        this.pager = pager;
    }

    public List<? extends DpInst> getResults() 
    {
        return results;
    }

    public void setResults(List<? extends DpInst> results) 
    {
        this.results = results;
    }
    
    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public Boolean getCanModify() {
		return canModify;
	}

	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}

	public String getNumerKontrahenta() {
		return numerKontrahenta;
	}

	public void setNumerKontrahenta(String numerKontrahenta) {
		this.numerKontrahenta = numerKontrahenta;
	}

	public String getSearchnumerKontrahenta() {
		return searchnumerKontrahenta;
	}

	public void setSearchnumerKontrahenta(String searchnumerKontrahenta) {
		this.searchnumerKontrahenta = searchnumerKontrahenta;
	}    
}
