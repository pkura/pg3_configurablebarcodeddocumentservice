package pl.compan.docusafe.web.office.dictionaries;

public class ContractorAction extends ContractorBaseAction 
{

	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getBaseLink() {
		return "/office/common/contractor.action";
	}

	@Override
	public boolean isCrm() {
		return false;
	}

	@Override
	public boolean isPopup() {
		return true;
	}
}
