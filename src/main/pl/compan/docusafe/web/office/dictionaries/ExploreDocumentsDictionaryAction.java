package pl.compan.docusafe.web.office.dictionaries;

import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ExploreDocumentsDictionaryAction extends EventActionSupport
{
	private Long id;
	private Long folderId;
	private String param;
	
	private final String FOLDER_REDIRECT = "folder";
	private final String DOCUMENT_REDIRECT = "document";
	
	protected void setup()
	{
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		
    		if(folderId == null)
    		{
    			try
    			{
    				folderId = Folder.findRootFolderChildIdByTitle("Archiwum dokumentów prawnych");
    			}
    			catch (Exception e) 
    			{
    				folderId = null;
				}
    		}
    		
    		if(id != null)
    		{
    			event.setResult(DOCUMENT_REDIRECT);
    		}
    		else
    		{
    			folderId = folderId;
    			event.setResult(FOLDER_REDIRECT);
    		}
    	}
    }

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getFolderId()
	{
		return folderId;
	}

	public void setFolderId(Long folderId)
	{
		this.folderId = folderId;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

}
