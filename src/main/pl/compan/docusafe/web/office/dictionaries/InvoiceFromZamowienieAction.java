package pl.compan.docusafe.web.office.dictionaries;

import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja słownikowa dla centrum kosztowych wybieranych dla faktury kosztowej.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class InvoiceFromZamowienieAction extends EventActionSupport 
{
	private Long id;
	private String nrFrom;
	private String nrTo;
	private Integer status;
	private Double kwota;
	private ArrayList<CentrumKosztowDlaFaktury> centra;
	private Long[] centrumIds;
	
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doFind").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Find()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
        registerListener("doRewrite").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Rewrite()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	
        }
    }
    
    private class Rewrite implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	if(id == null)
        	{
        		addActionError("Brak id dokumentu");
        		return;
        	}
        	
        	if(centrumIds == null || centrumIds.length <= 0)
        	{
        		addActionError("Nie wybrano elementow do przpisania");
        		return;
        	}
        	
        	kwota = 0.0;
        	ArrayList<CentrumKosztowDlaFaktury> tmp = new ArrayList<CentrumKosztowDlaFaktury>();
        	//centra
        	try
        	{
        		DSApi.context().begin();
	        	for(Long str : centrumIds)
	        	{
	        		
	        		CentrumKosztowDlaFaktury cFrom = CentrumKosztowDlaFaktury.getInstance().find(str);
	        		CentrumKosztowDlaFaktury centrum = new CentrumKosztowDlaFaktury();
	                
	        		centrum.setAccountNumber(cFrom.getAccountNumber());
	                centrum.setAmount(cFrom.getAmount()); 
	                
	                CentrumKosztow c = CentrumKosztow.find(cFrom.getCentrumId());
	                centrum.setCentrumCode(c.getCn());
	                centrum.setCentrumId(c.getId());
	                centrum.setDocumentId(id);
	                centrum.setDescriptionAmount(cFrom.getDescriptionAmount());
	                
	                Persister.create(centrum);
	                kwota += centrum.getAmount();
	                
	                tmp.add(centrum);
	                
	                //akceptacje
	                /*Document dFrom = Document.find(cFrom.getDocumentId());
        			FieldsManager fFrom = dFrom.getDocumentKind().getFieldsManager(dFrom.getId());
        			fFrom.initialize();
        			fFrom.initializeAcceptances();
        			*/
        			
        			
	        	}
	        	
	        	DSApi.context().commit();
        	}
        	catch (Exception e) {
        		DSApi.context().setRollbackOnly();
        		LogFactory.getLog("eprint").debug("", e);
			}
        	//kwota
        	try
        	{        		
        		FieldsManager fm = Document.find(id).getDocumentKind().getFieldsManager(id);
	        	Map<String, Object> values = fm.getFieldValues();
	        	values.put(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN, kwota);
	        	//values.put(InvoiceLogic.CENTRUM_KOSZTOWE_FIELD_CN, tmp);
	        	Document.find(id).getDocumentKind().setOnly(id, values);
	        	
	        	
        	}
        	catch (Exception e) {
        		LogFactory.getLog("eprint").debug("", e);
			}
        	
        	
        	addActionMessage("Przepisano wartosci");
         	status = 2;
         	
        }
    }
    
    private class Find implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try
        	{
        		DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ZAMOWIENIE);
	        	DockindQuery dockindQuery = new DockindQuery(0, 0);
	        	dockindQuery.setDocumentKind(kind);
	        	dockindQuery.rangeField(kind.getFieldByCn("NR_ZAMOWIENIA"), nrFrom, nrTo);
	        	SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
	        	
	        	kwota = 0.0;
	        	centra = new ArrayList<CentrumKosztowDlaFaktury>();
	        	
	        	for(Document doc : searchResults.results())
	        	{
	        		FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
	        		fm.initialize();
	        		kwota += (Double) fm.getValue("KWOTA");
	        		
	        		centra.addAll((ArrayList<CentrumKosztowDlaFaktury>) fm.getValue("CENTRUM_KOSZTOWE"));
	        			        		
	        	}
	        	
	        	status = 1;
	        }
        	catch (Exception e) {
        		LogFactory.getLog("eprint").debug("", e);
			}
        	
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrFrom() {
		return nrFrom;
	}

	public void setNrFrom(String nrFrom) {
		this.nrFrom = nrFrom;
	}

	public String getNrTo() {
		return nrTo;
	}

	public void setNrTo(String nrTo) {
		this.nrTo = nrTo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getKwota() {
		return kwota;
	}

	public void setKwota(Double kwota) {
		this.kwota = kwota;
	}

	public ArrayList<CentrumKosztowDlaFaktury> getCentra() {
		return centra;
	}

	public void setCentra(ArrayList<CentrumKosztowDlaFaktury> centra) {
		this.centra = centra;
	}

	public Long[] getCentrumIds() {
		return centrumIds;
	}

	public void setCentrumIds(Long[] centrumIds) {
		this.centrumIds = centrumIds;
	}
    
    
}
