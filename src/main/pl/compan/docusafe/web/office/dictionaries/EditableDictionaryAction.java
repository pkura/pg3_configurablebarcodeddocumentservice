package pl.compan.docusafe.web.office.dictionaries;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.beanutils.BeanUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.*;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.util.Arrays;
import java.util.List;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.webwork.event.SessionControl.*;
/**
 * Uniwersalna klasa wy�wietlaj�ca list� wpis�w w s�owniku
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class EditableDictionaryAction  extends EventActionSupport {
	private final static Logger LOG = LoggerFactory.getLogger(EditableDictionaryAction.class);

    /**
     * id wyr�nionego/edytowanego wpisu
     */
	private Integer id;
    private Object entry;
    private String jspPage;

    /**
     * cn s�ownika (patrz dictionarydictionary)
     */
    private String dictionaryCn;
    private String[] headers;
	private List<Entry> list;
    private int columnsSize;

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_SESSION).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_SESSION);

        registerListener("doPrepareEdit")
            .append(OPEN_HIBERNATE_SESSION)
            .append(new PrepareEdit())
            .appendFinally(CLOSE_HIBERNATE_SESSION);

        registerListener("doUpdate")
            .append(OPEN_HIBERNATE_SESSION)
            .append(new Update())
            .append(fillForm)
            .appendFinally(CLOSE_HIBERNATE_SESSION);

	}

    private static String isoToUtfHack(String string) throws Exception{
		return string;//new String(string.getBytes("ISO-8859-2"),"utf-8");
	}

    private class Update extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            log.info("transaction");
            AbstractEditableDictionary editableDictionary = (AbstractEditableDictionary) DictionaryDictionary.INSTANCE.get(dictionaryCn);
            log.info("entry = {}", BeanUtils.describe(entry));
            log.info("id = {}", BeanUtils.describe(entry));
            if(id == null){
                editableDictionary.add(entry);
            } else {
                editableDictionary.update(entry);
            }
            event.setResult("list");
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    private class PrepareEdit extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            AbstractEditableDictionary editableDictionary = (AbstractEditableDictionary) DictionaryDictionary.INSTANCE.get(dictionaryCn);
            if(id == null){
                entry = editableDictionary.get();
            } else {
                entry = editableDictionary.get(id);
            }
            jspPage = editableDictionary.getEditJspPage();
            headers = editableDictionary.getHeaders();
            columnsSize = headers.length + 1;
            event.setResult("edit-entry");
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public static class Entry {
        Object[] objects;
        Object id;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public Object[] getObjects() {
            return objects;
        }

        public void setObjects(Object[] objects) {
            this.objects = objects;
        }
    }

	private class FillForm extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			event.setResult("list");
            AbstractEditableDictionary ed = (AbstractEditableDictionary) DictionaryDictionary.INSTANCE.get(dictionaryCn);
            list = Lists.newArrayList();
            headers = ed.getHeaders();
            columnsSize = headers.length + 1;
            for(Object entry : ed.getAll()){
                Entry bean = new Entry();
                bean.id = BeanUtils.getSimpleProperty(entry, "id");
                bean.objects= ed.getObjectsForEntry(entry);
                list.add(bean);
            }
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    public String getDictionaryCn() {
        return dictionaryCn;
    }

    public void setDictionaryCn(String dictionaryCn) {
        LOG.info("setDictionaryCn");
        this.dictionaryCn = dictionaryCn;
    }

    public String[] getHeaders() {
        return headers;
    }

    public List<Entry> getList() {
        return list;
    }

    public Object getEntry() throws EdmException {
        if(entry == null){
//            LOG.info("getEntry");
//            LOG.info("params map {}", ServletActionContext.getRequest().getParameterMap());
            LOG.info("dictionaryCn {}", ServletActionContext.getRequest().getParameter("dictionaryCn"));
            String dictionaryCn = ServletActionContext.getRequest().getParameter("dictionaryCn");
            this.dictionaryCn = dictionaryCn;
            entry = ((Supplier) DictionaryDictionary.INSTANCE.get(dictionaryCn)).get();
        }
        return entry;
    }

    public String getJspPage() {
        LOG.info("getJspPage = '{}'", jspPage);
        return jspPage;
    }

    public int getColumnsSize() {
        LOG.info("getColumns size = {}", columnsSize);
        return columnsSize;
    }
}
