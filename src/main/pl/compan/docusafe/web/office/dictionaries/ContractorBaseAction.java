package pl.compan.docusafe.web.office.dictionaries;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Machine;
import pl.compan.docusafe.core.crm.Marka;
import pl.compan.docusafe.core.crm.Patron;
import pl.compan.docusafe.core.crm.Region;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
/**
 * Akcja s�ownika dla klient�w dokument�w leasingowych.
 * 
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public abstract class ContractorBaseAction extends EventActionSupport
{
    public static final int LIMIT = 15;
    private Logger log = LoggerFactory.getLogger(ContractorBaseAction.class);
    
    //@IMPORT/EXPORT
    private Long   id;
    private String name;
    private String pelnaNazwa;
    private String nip;
    private String regon;
    private String krs;
    private String ulica;
    private String kod;
    private String miasto;
    private String kraj;
    private String remarks;
    private String dictionaryDescription;
    private String contractorNo;
    private String fax;
    private String phoneNumber;
    private String phoneNumber2;
    private String www;
    private String email;
    private boolean portfolio;
    private List<Region> regions;
    private Long region;
    private String[] selectedMarki;
    private List<Marka> availableMarki;
    private Map<String,String> dsPatrons;
    private Map<String,String> webPatrons;
    private String[] selectedDsPatrons;
    private String[] selectedWebPatrons;
    private Set<Marka> contractorMarki;
    private String[] selectedMachine;
    private List<Machine> availableMachine;
    private Set<Machine> contractorMachine;
    private List<Role> roles;
    private String[] checkRoles;
    private Map<Long,Long> containRole;
    private String username;
    private Map<String,String> users;
    private String obrot;
	private String ctime;
    
    
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canEdit;
    private boolean addDuplicate;
    private static Contractor tmpContractor = new Contractor();
    private Contractor inst;
    private Pager pager;
    private int offset;
    private List<? extends Contractor> results;
    private static final String EV_FILL = "fill";
    private static final String EV_DO = "do";
    private static final String EV_SP = "doSp";
    private static DecimalFormat decimalFormat;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**
     * u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane
     */
    private String param;
	public boolean insertIntoForm;
    
    public boolean isPutToForm() {
		return insertIntoForm;
	}
	public void setPutToForm(boolean putToForm) {
		this.insertIntoForm = putToForm;
	}
	public abstract String getBaseLink();
    public abstract boolean isCrm();
    public abstract boolean isPopup();
    
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            append(new Validate()).
            append(EV_DO, new Add()).
           // append(EV_SP, new SetSpecific()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddAndPlace").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Clean()).
	        append(new Validate()).
	        append(EV_DO, new Add()).
	        append(EV_SP, new Place()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddDuplicate").
            append(OpenHibernateSession.INSTANCE).
            append(new SetDuplicate()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
           // append(new Validate()).
            append(EV_DO, new Update()).
            append(EV_SP, new SetSpecific()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Clean()).
            //append(new Validate()).
            append(EV_DO, new Search()).
            //append(EV_SP, new SetSpecific()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }
    
    static
    {
    	decimalFormat = new DecimalFormat("###########0.00"); 
		DecimalFormatSymbols decimalSymbols = new DecimalFormatSymbols();
		decimalSymbols.setDecimalSeparator(".".toCharArray()[0]);
		decimalSymbols.setGroupingSeparator(" ".toCharArray()[0]);
		decimalFormat.setDecimalFormatSymbols(decimalSymbols);
		decimalFormat.setGroupingSize(3);
    	decimalFormat.setGroupingUsed(true);
    }
    
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
        	 
            try
            {
                canRead = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                canEdit = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_EDYTOWANIE);
            	regions = Region.regionList();
                roles = Role.roleList();
                availableMarki = Marka.list();
                availableMachine = Machine.list();
                users = getRegionUsers();                
            
	            if (id != null && !addDuplicate) 
	            {
	            	event.setAttribute("id", id);
	                try 
	                {
	                    inst = tmpContractor.find(id);
	                    
	                    
	                } 
	                catch (EdmException e) 
	                {
	                    try 
	                    {
	                        DSApi.context().rollback();
	                    } 
	                    catch (EdmException e1) 
	                    {
	                        addActionError(e1.getMessage());
	                    }
	                    addActionError(e.getMessage());
	                }
	                finally
	                {
	                    try { DSApi.context().commit(); } catch (Exception e) { }
	                }
	                if (inst != null) 
	                {
	                	DSApi.context().session().refresh(inst);
	                    id = inst.getId();
	                    name = inst.getName();
	                    pelnaNazwa = inst.getFullName();
	                    nip = inst.getNip();
	                    regon = inst.getRegon();
	                    krs = inst.getKrs();
	                    ulica = inst.getStreet();
	                    kod = inst.getCode();
	                    miasto = inst.getCity();
	                    kraj = inst.getCountry();
	                    remarks = inst.getRemarks();
	                    dictionaryDescription = inst.getDictionaryDescription();
	                    contractorNo = inst.getContractorNo();
	                    fax = inst.getFax();
	                    phoneNumber = inst.getPhoneNumber();
	                    phoneNumber2 = inst.getPhoneNumber2();
	                    email = inst.getEmail();
	                    www = inst.getWww();
	                                 
	                    if(inst.getCtime()!=null)
	                    {
	                    	ctime = pl.compan.docusafe.util.DateUtils.formatCommonDate(inst.getCtime());
	                    }
	                    if(inst.getObrot() != null)
	                    	obrot = decimalFormat.format(inst.getObrot());
	                    if(inst.getRegion() != null)
	                    	region = inst.getRegion().getId();
	                    if(inst.getMarka() != null)
	                    {
	                    	contractorMarki = inst.getMarka();
	                    	
	                    	for (Marka marka : contractorMarki) 
	                    	{
	                    		availableMarki.remove(marka);
							}
	                    }
	                    if(inst.getMachine() != null)
	                    {
	                    	contractorMachine = inst.getMachine();
	                    	for (Machine machine : contractorMachine) 
	                    	{
	                    		availableMachine.remove(machine);
							}
	                    }
	                    if(inst.getRole() != null)
	                    {
	                    	containRole = new HashMap<Long, Long>();
	                    	for (Role r: inst.getRole())
	                    	{
	                    		containRole.put(r.getId(), r.getId());
							}
	                    }
	                    if(inst.getPatrons() != null)
	                    {
	                    	dsPatrons= new LinkedHashMap<String, String>();
	                    	webPatrons= new LinkedHashMap<String, String>();
	                    	for (Patron p : inst.getPatrons()) 
	                    	{
	                    		String lastNameFirstName = "B��d";
	                    		try
	                    		{
	                    			lastNameFirstName = DSUser.findByUsername(p.getUsername()).asLastnameFirstname();
								}
	                    		catch (Exception e) {
									 addActionError(e.getMessage());
									 log.error(e.getMessage(), e);
								} 
	                    		if("DS".equalsIgnoreCase(p.getOwn()))
	                    			dsPatrons.put(p.getUsername(), lastNameFirstName);
	                    		else
	                    			webPatrons.put(p.getUsername(), lastNameFirstName);
							}
	                    }
	                }
	                if(username == null || username.length()< 1)
	                	username = DSApi.context().getPrincipalName();
	            }
            }
            catch (Exception e) 
            {
            	log.error(e.getMessage(), e);
                addActionError(e.getMessage());
            }
            
        }
    }
    
    private  Map<String,String> getRegionUsers() throws DivisionNotFoundException, EdmException
    {
    	Map<String,String> result = new LinkedHashMap<String, String>();
    	DSUser[] users = DSDivision.find("DL_BINDER_DS").getUsers(true);
    	
    	List<DSUser> userslist = new ArrayList<DSUser>(Arrays.asList(users));
    	users = DSDivision.find("0A00000B013572B7113F7A91EDA94403C1B").getUsers(true);
    	userslist.addAll(new ArrayList<DSUser>(Arrays.asList(users)));
    	Collections.sort(userslist, DSUser.LASTNAME_COMPARATOR);
    	for (DSUser user : userslist)
    	{
    		result.put(user.getName(), user.asLastnameFirstname());
		}
    	
    	
    	Collections.sort(userslist, DSUser.LASTNAME_COMPARATOR);
    	for (DSUser user : userslist)
    	{
    		result.put(user.getName(), user.asLastnameFirstname());
		}
//    	try0A00000B013572B7113F7A91EDA94403C1B
//    	{
//    		ps = DSApi.context().prepareStatement("select u.name, u.firstname, u.lastname from ds_user u, ds_user_to_division du, ds_division d where d.guid like 'DV_%' and du.division_id = d.id and u.id = du.user_id order by u.lastname ");
//    		rs = ps.executeQuery();
//    		
//    		while(rs.next())
//    		{
//    			result.put(rs.getString(1), rs.getString(3)+" "+rs.getString(2));
//    		}
//    	}
//    	catch (Exception e)
//    	{
//			log.error("",e);
//		}
//    	finally
//    	{
//    		DbUtils.closeQuietly(rs);
//    		DbUtils.closeQuietly(ps);
//    	}
    	
    	return result;
    }
    
    private class Place implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event) 
        {
    		insertIntoForm = true;
        }
    }
    
    private class SetSpecific implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event) 
        {
    		//insertIntoForm = true;
        }
    }
    
    private class SetDuplicate implements ActionListener 
    {
    	public void actionPerformed(ActionEvent event) 
        {
    		addDuplicate = true;
        }
    }
    
    private class Add implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();               
                canAdd = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_DODAWANIE);
                if (!canAdd) 
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));

               
                inst = new Contractor(); 
                if(checkRoles == null || checkRoles.length < 1)
                {
                	checkRoles = new String[]{"5"};
                }
                setValue(inst);
                try
                {
                	inst.create(addDuplicate);
                	inst.addPatron(DSApi.context().getPrincipalName(), "DS");
                }
                catch (DSException e)
                {
					addDuplicate = DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL);
					throw new EdmException(e.getMessage());
				}
                id = inst.getId();
                DSApi.context().commit();
                addActionMessage(sm.getString("WslownikuUtworzono",name));
            } 
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
            	addActionError(e.getMessage());
            	DSApi.context()._rollback();
            }
        }
    }  
    
    
    private void setValue(Contractor inst) throws Exception
    {
        inst.setName(name);
        inst.setFullName(pelnaNazwa);
        inst.setNip(nip);
        inst.setRegon(regon);
        inst.setKrs(krs);
        inst.setStreet(ulica);
        inst.setCode(kod);
        inst.setCity(miasto);
        inst.setCountry(kraj);
        inst.setRemarks(remarks);
        inst.setContractorNo(contractorNo);
        inst.setFax(fax);
        
        inst.setPhoneNumber(phoneNumber);
        inst.setPhoneNumber2(phoneNumber2);
        inst.setEmail(email);
        inst.setWww(www);
        if(inst.getId() != null &&( selectedDsPatrons == null || selectedDsPatrons.length < 1))
        {
        	 for (Patron  p : inst.getPatrons())
             {
        		 if("DS".equalsIgnoreCase(p.getOwn()))
             		p.delete();
     		}
        }
        else if(selectedDsPatrons != null && selectedDsPatrons.length > 0 )
        {
	        List <String> list = Arrays.asList(selectedDsPatrons);
	        if(inst.getId() != null)
	        {
		        for (Patron  p : inst.getPatrons())
		        {
		        	if("DS".equalsIgnoreCase(p.getOwn()) && !list.contains(p.getUsername()))
		        		p.delete();
				}
	        }
	        Set<Patron> patrons = new HashSet<Patron>();
	        for (String username : selectedDsPatrons) 
	        {
				patrons.add(Patron.findOrCreate(username, inst));
			}
//	        inst.setPatrons(patrons);
        }
        
       
        
        
        
        if(obrot != null)
        {
        	inst.setObrot(Double.parseDouble(obrot.replace(" ", "")));
        }
        if(region != null)
        	inst.setRegion(Region.find(region));
        Set<Marka> markaSet = new HashSet<Marka>();
        if(selectedMarki != null && selectedMarki.length > 0)
        {
        	for (int i = 0; i < selectedMarki.length; i++) 
        	{
        		markaSet.add(Marka.find(Long.parseLong(selectedMarki[i]))); 
			}
        	inst.setMarka(markaSet);
        }
        else
        {
        	inst.setMarka(null);
        }   
        Set<Machine> machineSet = new HashSet<Machine>();
        if(selectedMachine != null && selectedMachine.length > 0)
        {
        	for (int i = 0; i < selectedMachine.length; i++) 
        	{
        		machineSet.add(Machine.find(Long.parseLong(selectedMachine[i]))); 
			}
        	inst.setMachine(machineSet);
        }
        else
        {
        	inst.setMachine(null);
        } 
        if (checkRoles == null || checkRoles.length == 0)
        {
            addActionError("Nie wybrano �adnej roli");
        }
        else
        {
        	Set<Role> rl = new HashSet<Role>();
        	for (int i=0; i < checkRoles.length; i++)
            {
        		rl.add(Role.find(Long.parseLong(checkRoles[i])));
            }
        	inst.setRole(rl);
        }
    }
       
    private class Delete implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try 
            {
                DSApi.context().begin();

                canDelete = DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_USUWANIE);
                if(!canDelete) throw new EdmException("Brak uprawnie� do usuwania ze s�ownika");

                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
                DockindQuery dockindQuery = new DockindQuery(0,0);
                dockindQuery.setDocumentKind(kind);
                
                Field f = kind.getFieldByCn(DlLogic.KLIENT_CN);
                dockindQuery.field(f,id);
                SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
                if (searchResults == null || searchResults.totalCount()<1)
                {
                    inst = tmpContractor.find(id);
                    inst.delete();
                    id = null;
                    addActionMessage(sm.getString("ZeSlownikaUsunietoStrone",inst.getName()));
                }
                else
                    throw new EdmException(sm.getString("NieMoznaUsunacTegoKlientaPoniewazSaDoNiegoPrzypisaneDokumenty"));
                
                DSApi.context().commit();
            } 
            catch (Exception e) 
            {
                try 
                {
                    DSApi.context().rollback();
                } 
                catch (EdmException e1) 
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
                LogFactory.getLog("eprint").error("", e);
            }
        }
    }

    private class Update implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {  
            try
            {
                DSApi.context().begin();
                Contractor inst = tmpContractor.find(id);                
                setValue(inst);                
                DSApi.context().commit();
                id = inst.getId();
            }
            catch (Exception e)
            {
            	 addActionError(e.getMessage());
            	LogFactory.getLog("eprint").error("", e);
            }
        }
    }  
    
    private class Search implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                QueryForm form = new QueryForm(offset, LIMIT);

                if (name!= null) form.addProperty("name",name);
                if (pelnaNazwa!= null) form.addProperty("fullName",pelnaNazwa);
                if (nip!= null) form.addProperty("nip",nip);
                if ( regon!= null) form.addProperty("regon",regon);
                if (krs!= null) form.addProperty("krs",krs);
                if (ulica!= null) form.addProperty("street",ulica);
                if (kod!= null) form.addProperty("code",kod);
                if (miasto!= null) form.addProperty("city",miasto);
                if (kraj!= null) form.addProperty("country",kraj);
                if (remarks!= null) form.addProperty("contactData",remarks);
                if (contractorNo!= null) form.addProperty("contractorNo",contractorNo);
                if (phoneNumber!= null) form.addProperty("phoneNumber",phoneNumber);
                if (phoneNumber2!= null) form.addProperty("phoneNumber2",phoneNumber2);
                if (fax!= null) form.addProperty("fax",fax);
                if (email!= null) form.addProperty("email",email);
                
                if (checkRoles != null ) form.addProperty("roles",checkRoles);
                if (region!= null) form.addProperty("region",region);
                if (ctime!= null) form.addProperty("ctime", ctime);
                if (obrot!= null) form.addProperty("obrot",obrot);
                if (username!= null) form.addProperty("username",username);
                if (selectedMarki!= null) form.addProperty("marka",selectedMarki);
                if (selectedMachine!= null) form.addProperty("machine",selectedMachine);

                form.addOrderAsc("name");
                

                
                SearchResults<? extends Contractor> results = Contractor.search(form);

                if (results == null || results.totalCount() == 0)
                {
                    if(nip != null && nip.length() > 0)
                	{
                		List<Contractor> list = Contractor.findByNIP(nip);
                		if(list != null && list.size() > 0)
                		{
                			throw new DSException("Kontrahent, kt�rego chcesz wprowadzi� jest ju� w bazie.\n "
                					+ (list.get(0).getPatrons() != null ?
                					"W�a�ciciel kontrahenta: "+list.get(0).getPatronsNames() 
                					: ".") +"\nData rejestracji kontrahenta w bazie: " + list.get(0).getCtime() +"." );
                		}
                	}
                    throw new EdmException(sm.getString("NieZnalezionoInstytucjiUmowyPasujacychDoWpisanychDanych"));
                }
                ContractorBaseAction.this.results = fun.list(results);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(getBaseLink(), new Object[] 
                            {
                                "doSearch", "true",		
                                
								"name",name,                       
								"pelnaNazwa",pelnaNazwa,                       
								"nip",nip,                       
								"regon",regon,                       
								"krs",krs,                       
								"ulica",ulica,                     	
								"kod",kod,                  
								"miasto",miasto ,                      
                                "kraj",kraj,
                                "remarks",remarks,
                                "contractorNo",contractorNo,
                                "phoneNumber",phoneNumber,
                                "phoneNumber2",phoneNumber2,
                                "fax",fax,
                                "email",email,
                                "param", param, 
                                "checkRoles",checkRoles,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e) 
            {
                addActionError(e.getMessage());
            } 
        }
    }
  
    private class Clean implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
			name = TextUtils.trimmedStringOrNull(name);
		    pelnaNazwa = TextUtils.trimmedStringOrNull(pelnaNazwa);
		    nip = TextUtils.trimmedStringOrNull(nip);
		    regon = TextUtils.trimmedStringOrNull(regon);
		    krs = TextUtils.trimmedStringOrNull(krs);
		    ulica = TextUtils.trimmedStringOrNull(ulica);
		    kod = TextUtils.trimmedStringOrNull(kod);
		    miasto = TextUtils.trimmedStringOrNull(miasto);
		    kraj = TextUtils.trimmedStringOrNull(kraj);
		    remarks = TextUtils.trimmedStringOrNull(remarks);
		    ctime = TextUtils.trimmedStringOrNull(ctime);
        }
    }
        
    private class Validate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if (name == null)
            {
                addActionError(sm.getString("NazwaJestObowiazkowa"));
                event.skip(EV_DO);
                return;
            }           
            
            
            if (nip != null )
            {
                List<Contractor> list = Contractor.findByNIP(nip);
        		if(list != null && list.size() > 0)
        		{       
        			String stringError = "Kontrahent ";
                    if(event.hasListener(EV_SP)) 
                    {
                    	event.skip(EV_SP);
                    }
                    event.skip(EV_DO);
                    try
                    {
//                    stringError = "Kontrahent, kt�rego chcesz wprowadzi� jest ju� w bazie.\n "
//            					+ (list.get(0).getDsUser() != null ?
//            					"W�a�ciciel kontrahenta: "+DSUser.findByUsername(list.get(0).getDsUser()).asFirstnameLastname() + "."
//            					: ".") +"\nData rejestracji kontrahenta w bazie: " + list.get(0).getCtime() +".";
                    }
                    catch(Exception e) {}
                    addActionError(stringError);
                    return;
        		}
            	
//                QueryForm form = new QueryForm(offset, LIMIT);
//                form.addProperty("nip",nip);
//                form.addOrderAsc("name");
//                
//                SearchResults<? extends Contractor> results = Contractor.search(form);
//
//                if (!(results == null || results.totalCount() == 0))
//                {
//                    addDuplicate = true;
//                    addActionError("Znaleziono kontrahenta o podanym NIP-e. Doda� duplikat ?");
//                    event.skip(EV_DO);     
//                    return;
//                }
            }
        }
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String daneKontaktowe)
    {
        this.remarks = daneKontaktowe;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getKod()
    {
        return kod;
    }

    public void setKod(String kod)
    {
        this.kod = kod;
    }

    public String getKraj()
    {
        return kraj;
    }

    public void setKraj(String kraj)
    {
        this.kraj = kraj;
    }

    public String getKrs()
    {
        return krs;
    }

    public void setKrs(String krs)
    {
        this.krs = krs;
    }

    public String getMiasto()
    {
        return miasto;
    }

    public void setMiasto(String miasto)
    {
        this.miasto = miasto;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public String getPelnaNazwa()
    {
        return pelnaNazwa;
    }

    public void setPelnaNazwa(String pelnaNazwa)
    {
        this.pelnaNazwa = pelnaNazwa;
    }

    public String getRegon()
    {
        return regon;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public List<? extends Contractor> getResults()
    {
        return results;
    }

    public void setResults(List<? extends Contractor> results)
    {
        this.results = results;
    }

    public String getUlica()
    {
        return ulica;
    }

    public void setUlica(String ulica)
    {
        this.ulica = ulica;
    }

    public String getDictionaryDescription()
    {
        return dictionaryDescription;
    }

    public void setDictionaryDescription(String dictionaryDescription)
    {
        this.dictionaryDescription = dictionaryDescription;
    }

    public Boolean getCanEdit()
    {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit)
    {
        this.canEdit = canEdit;
    }

    public String getContractorNo()
    {
        return contractorNo;
    }

    public void setContractorNo(String contractorNo)
    {
        this.contractorNo = contractorNo;
    }

    public Boolean getAddDuplicate()
    {
        return addDuplicate;
    }

    public void setAddDuplicate(Boolean addDuplicate)
    {
        this.addDuplicate = addDuplicate;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber2()
    {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2)
    {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getWww()
    {
        return www;
    }

    public void setWww(String www)
    {
        this.www = www;
    }
	public String getCtime() 
	{
		return ctime;
	}
	public void setCtime(String ctime) 
	{
		this.ctime = ctime;
	}	
	public boolean isPortfolio() 
	{
		return portfolio;
	}

	public void setPortfolio(boolean portfolio)
	{
		this.portfolio = portfolio;
	}

	public List<Region> getRegions() 
	{
		return regions;
	}

	public void setRegions(List<Region> regions) 
	{
		this.regions = regions;
	}

	public Long getRegion() 
	{
		return region;
	}

	public void setRegion(Long region)
	{
		this.region = region;
	}

	public String[] getCheckRoles() {
		return checkRoles;
	}

	public void setCheckRoles(String[] checkRoles) {
		this.checkRoles = checkRoles;
	}

	public Map<Long, Long> getContainRole() {
		return containRole;
	}

	public void setContainRole(Map<Long, Long> containRole) {
		this.containRole = containRole;
	}
	
	public List<Marka> getAvailableMarki() {
		return availableMarki;
	}
	public void setAvailableMarki(List<Marka> availableMarki) {
		this.availableMarki = availableMarki;
	}
	public Set<Marka> getContractorMarki() {
		return contractorMarki;
	}
	public void setContractorMarki(Set<Marka> contractorMarki) {
		this.contractorMarki = contractorMarki;
	}
	public Contractor getInst() {
		return inst;
	}
	public void setInst(Contractor inst) {
		this.inst = inst;
	}
	public String getObrot() {
		return obrot;
	}
	public void setObrot(String obrot) {
		this.obrot = obrot;
	}
	public String[] getSelectedMarki() {
		return selectedMarki;
	}
	public void setSelectedMarki(String[] selectedMarki) {
		this.selectedMarki = selectedMarki;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setAddDuplicate(boolean addDuplicate) {
		this.addDuplicate = addDuplicate;
	}
	public Map<String, String> getUsers() {
		return users;
	}
	public void setUsers(Map<String, String> users) {
		this.users = users;
	}
	public boolean isInsertIntoForm() {
		return insertIntoForm;
	}
	public void setInsertIntoForm(boolean insertIntoForm) {
		this.insertIntoForm = insertIntoForm;
	}
	public List<Machine> getAvailableMachine() {
		return availableMachine;
	}
	public void setAvailableMachine(List<Machine> availableMachine) {
		this.availableMachine = availableMachine;
	}
	public Set<Machine> getContractorMachine() {
		return contractorMachine;
	}
	public void setContractorMachine(Set<Machine> contractorMachine) {
		this.contractorMachine = contractorMachine;
	}
	public String[] getSelectedMachine() {
		return selectedMachine;
	}
	public void setSelectedMachine(String[] selectedMachine) {
		this.selectedMachine = selectedMachine;
	}
	public Map<String, String> getDsPatrons() {
		return dsPatrons;
	}
	public void setDsPatrons(Map<String, String> dsPatrons) {
		this.dsPatrons = dsPatrons;
	}
	public Map<String, String> getWebPatrons() {
		return webPatrons;
	}
	public void setWebPatrons(Map<String, String> webPatrons) {
		this.webPatrons = webPatrons;
	}
	public String[] getSelectedDsPatrons() {
		return selectedDsPatrons;
	}
	public void setSelectedDsPatrons(String[] selectedDsPatrons) {
		this.selectedDsPatrons = selectedDsPatrons;
	}
	public String[] getSelectedWebPatrons() {
		return selectedWebPatrons;
	}
	public void setSelectedWebPatrons(String[] selectedWebPatrons) {
		this.selectedWebPatrons = selectedWebPatrons;
	}
	
}