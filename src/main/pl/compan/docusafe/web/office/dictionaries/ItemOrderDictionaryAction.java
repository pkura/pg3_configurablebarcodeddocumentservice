package pl.compan.docusafe.web.office.dictionaries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.parametrization.presale.ItemOrderDictionary;
import pl.compan.docusafe.parametrization.presale.ProductDictionary;
import pl.compan.docusafe.parametrization.presale.ProductGroupDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ItemOrderDictionaryAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(ItemOrderDictionaryAction.class);
	private Long id;
	private String param;
	private String description;
	private String amount;
	private String volume;
	private Integer productGroupId;
	private List<AccountNumber> productGroups;
	private Boolean documentReassigned;
	private Boolean submit; 
	
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAdd").
			append(OpenHibernateSession.INSTANCE).
			append(new Add()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").
			append(OpenHibernateSession.INSTANCE).
			append(new Update()).append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				productGroups = AccountNumber.list();
				if(id != null)
				{
					ItemOrderDictionary itm = ItemOrderDictionary.getInstance().find(id);
					productGroupId = itm.getAccountId();
					description = itm.getDescription();
					volume = itm.getVolume().toString();
					amount = itm.getAmount().toString();
				}
			}
			catch (EdmException e)
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("",e);
			}
		}
	}

	private class Add implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				ItemOrderDictionary itm = new ItemOrderDictionary();
				setValue(itm);
				itm.create();
				id = itm.getId();
				DSApi.context().commit();
				documentReassigned = true;
				setSubmit(true);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}

	private class Update implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				if(id == null)
					throw new EdmException("Nie wybrano pozycji zam�wienia");
				
				ItemOrderDictionary itm = ItemOrderDictionary.getInstance().find(id);
				setValue(itm);
				DSApi.context().commit();
				documentReassigned = true;
				setSubmit(false);
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
				DSApi.context()._rollback();
			}
		}
	}
	
	private void setValue(ItemOrderDictionary itm) throws EdmException
	{
		itm.setDescription(description);
		itm.setAmount(Double.parseDouble(amount));
		itm.setVolume(Double.parseDouble(volume));
		itm.setAccountId(productGroupId);
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getParam()
	{
		return param;
	}

	public void setParam(String param)
	{
		this.param = param;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getVolume()
	{
		return volume;
	}

	public void setVolume(String volume)
	{
		this.volume = volume;
	}

	public Integer getProductGroupId()
	{
		return productGroupId;
	}

	public void setProductGroupId(Integer productGroupId)
	{
		this.productGroupId = productGroupId;
	}

	public List<AccountNumber> getProductGroups()
	{
		return productGroups;
	}

	public void setProductGroups(List<AccountNumber> productGroups)
	{
		this.productGroups = productGroups;
	}

	public Boolean getDocumentReassigned()
	{
		return documentReassigned;
	}

	public void setDocumentReassigned(Boolean documentReassigned)
	{
		this.documentReassigned = documentReassigned;
	}

	public Boolean getSubmit()
	{
		return submit;
	}

	public void setSubmit(Boolean submit)
	{
		this.submit = submit;
	}
}
