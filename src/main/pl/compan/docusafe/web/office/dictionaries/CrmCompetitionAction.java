package pl.compan.docusafe.web.office.dictionaries;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Competition;
import pl.compan.docusafe.core.crm.CompetitionKind;
import pl.compan.docusafe.core.crm.CompetitionName;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja s�ownika konkurencji dla kontrahenta.
 *
 * @author Mariusz Kiljanczyk
 */
@SuppressWarnings("serial")
public class CrmCompetitionAction extends EventActionSupport
{
	private Log log = LogFactory.getLog(CrmCompetitionAction.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    //@IMPORT/EXPORT
    private Long id;
    private Long contractorId;
    private String param;
    private Competition competition;
    private Long selectedCompetition;
    private List<Competition> competitions;
	private String competitionName;
	private List<CompetitionName> competitionNames;
	private String competitionKind;
	private List<CompetitionKind> competitionKinds;
	private Boolean oficjalna;
	private Double marzaOd;
	private Double marzaDo;
	private Double prowizjaSalon;
	private Double prowizjaHandlowiec;
	private Double nagrodaHandlowiec;
	private Double wysokoscSprzedazy;
	private String opis;
	private Integer okres;
	public static Map<Integer,String> okresy;
	
	private static Contractor tmpContractor = new Contractor();

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
       }

	public void setParam(String param) {
		this.param = param;
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	competitions = null;
        	competition = null;
            try
            {
            	if(id != null)
            		contractorId = id;
            	id = contractorId;
            	competitionKinds = CompetitionKind.list();
            	competitionNames = CompetitionName.list();
            	okresy = Competition.getOkresy();
            	if(contractorId != null)
            	{
            		Contractor con = tmpContractor.find(contractorId);
            		competitions = con.getCompetitions();
            		if(selectedCompetition != null)
            		{
            			competition = Competition.find(selectedCompetition);
            			competitions.get(0);
            		}
            		else if(competitions != null && competitions.size() > 0)
            		{
            			competition = competitions.get(competitions.size() - 1 );
            			selectedCompetition = competition.getId();
            		}
            		if(competition != null)
            		{
            			if(competition.getCompetitionName() != null)
            				competitionName = competition.getCompetitionName().getCn();
            			if(competition.getCompetitionKind() != null)
            				competitionKind = competition.getCompetitionKind().getCn();

            			oficjalna = competition.getOficjalna();
            			marzaOd = competition.getMarzaOd();
            			marzaDo = competition.getMarzaDo();
            			prowizjaSalon = competition.getProwizjaSalon();
            			prowizjaHandlowiec = competition.getProwizjaHandlowiec();
            			nagrodaHandlowiec = competition.getNagrodaHandlowiec();
            			wysokoscSprzedazy = competition.getWysokoscSprzedazy();
            			opis = competition.getOpis();
            			okres = competition.getOkres();
            		}
            	}
			}
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
				log.error("B��d ",e);
			}
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(contractorId == null)
        	{
        		addActionError("Musisz najpierw wybra� kontrahenta");
        		return;
        	}
            try
            {
            	DSApi.context().begin();
            	Competition com = new Competition();
            	com.setCompetitionKind(CompetitionKind.findByCn(competitionKind));
            	com.setCompetitionName(CompetitionName.findByCn(competitionName));
            	Contractor cTmp = tmpContractor.find(contractorId);
            	com.setContractor(cTmp);
            	com.setMarzaDo(marzaDo);
            	com.setMarzaOd(marzaOd);
            	com.setNagrodaHandlowiec(nagrodaHandlowiec);
            	com.setOficjalna(oficjalna);
            	com.setOkres(okres);
            	com.setOpis(opis);
            	com.setProwizjaHandlowiec(prowizjaHandlowiec);
            	com.setProwizjaSalon(prowizjaSalon);
            	com.setWysokoscSprzedazy(wysokoscSprzedazy);
            	com.setDataDodania(new Date());
            	cTmp.getCompetitions().add(com);
            	com.create();
            	DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                try
                {
                    DSApi.context().rollback();
                }
                catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(contractorId == null)
        	{
        		addActionError("Musisz najpierw wybra� kontrahenta");
        		return;
        	}
        	if(selectedCompetition == null)
        	{
        		addActionError("Brak wyboru");
        		return;
        	}
            try
            {
            	DSApi.context().begin();
            	Contractor contractor = tmpContractor.find(contractorId);
            	int index = contractor.getCompetitions().indexOf(Competition.find(selectedCompetition));
            	Competition com = contractor.getCompetitions().get(index);
            	com.setCompetitionKind(CompetitionKind.findByCn(competitionKind));
            	com.setCompetitionName(CompetitionName.findByCn(competitionName));
            	Contractor cTmp = tmpContractor.find(contractorId);
            	com.setContractor(cTmp);
            	com.setMarzaDo(marzaDo);
            	com.setMarzaOd(marzaOd);
            	com.setNagrodaHandlowiec(nagrodaHandlowiec);
            	com.setOficjalna(oficjalna);
            	com.setOkres(okres);
            	com.setOpis(opis);
            	com.setProwizjaHandlowiec(prowizjaHandlowiec);
            	com.setProwizjaSalon(prowizjaSalon);
            	com.setWysokoscSprzedazy(wysokoscSprzedazy);
            	com.save();
            	DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                try
                {
                    DSApi.context().rollback();
                }
                catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

	public Long getContractorId() {
		return contractorId;
	}

	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public String getCompetitionName() {
		return competitionName;
	}

	public void setCompetitionName(String competitionName) {
		this.competitionName = competitionName;
	}

	public String getCompetitionKind() {
		return competitionKind;
	}

	public void setCompetitionKind(String competitionKind) {
		this.competitionKind = competitionKind;
	}

	public Boolean getOficjalna() {
		return oficjalna;
	}

	public void setOficjalna(Boolean oficjalna) {
		this.oficjalna = oficjalna;
	}

	public Double getMarzaOd() {
		return marzaOd;
	}

	public void setMarzaOd(Double marzaOd) {
		this.marzaOd = marzaOd;
	}

	public Double getMarzaDo() {
		return marzaDo;
	}

	public void setMarzaDo(Double marzaDo) {
		this.marzaDo = marzaDo;
	}

	public Double getProwizjaSalon() {
		return prowizjaSalon;
	}

	public void setProwizjaSalon(Double prowizjaSalon) {
		this.prowizjaSalon = prowizjaSalon;
	}

	public Double getProwizjaHandlowiec() {
		return prowizjaHandlowiec;
	}

	public void setProwizjaHandlowiec(Double prowizjaHandlowiec) {
		this.prowizjaHandlowiec = prowizjaHandlowiec;
	}

	public Double getNagrodaHandlowiec() {
		return nagrodaHandlowiec;
	}

	public void setNagrodaHandlowiec(Double nagrodaHandlowiec) {
		this.nagrodaHandlowiec = nagrodaHandlowiec;
	}

	public Double getWysokoscSprzedazy() {
		return wysokoscSprzedazy;
	}

	public void setWysokoscSprzedazy(Double wysokoscSprzedazy) {
		this.wysokoscSprzedazy = wysokoscSprzedazy;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getParam() {
		return param;
	}

	public List<CompetitionName> getCompetitionNames() {
		return competitionNames;
	}

	public void setCompetitionNames(List<CompetitionName> competitionNames) {
		this.competitionNames = competitionNames;
	}

	public List<CompetitionKind> getCompetitionKinds() {
		return competitionKinds;
	}

	public void setCompetitionKinds(List<CompetitionKind> competitionKinds) {
		this.competitionKinds = competitionKinds;
	}

	public Integer getOkres() {
		return okres;
	}

	public void setOkres(Integer okres) {
		this.okres = okres;
	}

	public static Map<Integer, String> getOkresy() {
		return okresy;
	}

	public static void setOkresy(Map<Integer, String> okresy) {
		CrmCompetitionAction.okresy = okresy;
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public Long getSelectedCompetition() {
		return selectedCompetition;
	}

	public void setSelectedCompetition(Long selectedCompetition) {
		this.selectedCompetition = selectedCompetition;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
