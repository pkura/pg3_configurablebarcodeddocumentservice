package pl.compan.docusafe.web.office.beans;

import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskBean.java,v 1.1 2004/11/06 00:33:30 lk Exp $
 */
public class TaskBean
{
    private Integer officeNumber;
    private String summary;
    private Date receivedTime;
    private String wfSender;
    private String sender;
    private String taskDescription;
    private String taskStatus;

    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public void setOfficeNumber(Integer officeNumber)
    {
        this.officeNumber = officeNumber;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public Date getReceivedTime()
    {
        return receivedTime;
    }

    public void setReceivedTime(Date receivedTime)
    {
        this.receivedTime = receivedTime;
    }

    public String getWfSender()
    {
        return wfSender;
    }

    public void setWfSender(String wfSender)
    {
        this.wfSender = wfSender;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public String getTaskDescription()
    {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription)
    {
        this.taskDescription = taskDescription;
    }

    public String getTaskStatus()
    {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus)
    {
        this.taskStatus = taskStatus;
    }
}
