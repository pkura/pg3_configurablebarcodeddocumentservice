package pl.compan.docusafe.web.office;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.UserToBriefcase;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction.ValueToList;
import pl.compan.docusafe.web.common.event.ActionEvent;


/**
 * Klasa akcji dla  powiazywania sprawy do sprawy 
 * 
 * @author Grzegorz Filip
 * @date: 03.04.2014
 */
public class CaseToCaseAction extends EditCaseAction
{
	 private static final String FORWARD = "office/casetocase";
	    private Logger log = LoggerFactory.getLogger(CaseToCaseAction.class);
		
	    
	    public void createCaseToCase(Long caseIdToAdd , Long currentCaseid) throws UserNotFoundException, EdmException
		{
	    	List<CaseToCase> listLinkedCases  = getCasesAsignedToCase(OfficeCase.find(currentCaseid));
	    	 for (CaseToCase ctc : listLinkedCases){
	    		 if(ctc.getLinkedCaseId()==currentCaseid && ctc.getCaseContainerId()==caseIdToAdd)
	    			 throw new EdmException("Wybrana Spawa jest juz na liscie spraw powiazanych! " );
	    	 }
			DSApi.beginTransacionSafely();

	    	//dodawana sprawa
	    	OfficeCase caseToAdd = OfficeCase.find(caseIdToAdd);
	    	//przegladana sprawa
	    	OfficeCase currentCase = OfficeCase.find(currentCaseid);
	    	
	    	Date date = new Date();
	    	//tworzymy  linkowa  dla przegladanej sprawy
	    	CaseToCase ctcCurrent = new CaseToCase();
	    	ctcCurrent.setCreatingUserName(DSApi.context().getDSUser().getName());
	    	ctcCurrent.setCreatingUserId(DSApi.context().getDSUser().getId());
	    	ctcCurrent.setLinkedCaseId(caseToAdd.getId());
	    	ctcCurrent.setCaseContainerId(currentCase.getId());
	    	ctcCurrent.setLinkToCase("/docusafe/office/edit-case.do?id="+caseToAdd.getId());
	    	ctcCurrent.setCreationTime(date);
			DSApi.context().session().save(ctcCurrent);
			currentCase.getAudit().add(Audit.create("setCase",
            		DSApi.context().getPrincipalName(),
            		"Powiazano sprawe z sprawa  o symbolu :" +caseToAdd.getOfficeId()));
			
			DSApi.context().commit();
			DSApi.beginTransacionSafely();
			
			//tworzymy  linka  dla dodawanej sprawy
			CaseToCase ctcSelected = new CaseToCase();
			ctcSelected.setCreatingUserName(DSApi.context().getDSUser().getName());
			ctcSelected.setCreatingUserId(DSApi.context().getDSUser().getId());
			ctcSelected.setLinkedCaseId(currentCase.getId());
			ctcSelected.setCaseContainerId(caseToAdd.getId());
			ctcSelected.setLinkToCase("/docusafe/office/edit-case.do?id="+currentCase.getId());
			ctcSelected.setCreationTime(new Date());
			DSApi.context().session().save(ctcSelected);
			
			
			
			caseToAdd.getAudit().add(Audit.create("setCase",
            		DSApi.context().getPrincipalName(),
            		"Powiazano sprawe z sprawa  o symbolu :" +currentCase.getOfficeId()));
			
			DSApi.context().commit();
			
		}

		/*public void createCaseToCase(OfficeCase c) throws UserNotFoundException, EdmException{
			
			
			CaseToCase ctc = new CaseToCase();
			ctc.setCreatingUserName(DSApi.context().getDSUser().getName());
			ctc.setCreatingUserId(DSApi.context().getDSUser().getId());
			ctc.setLinkedCaseId(c.getId());
			ctc.setLinkToCase("/docusafe/office/edit-case.do?id="+ctc.getCaseContainerId());
			
			//z sprawy
			ctc.setCaseContainerId(c.getId());
			ctc.setCaseAuthor(c.getAuthor());
			ctc.setCaseTitle(c.getTitle());
			ctc.setCaseOpenDate(c.getOpenDate());
			ctc.setCasefinishDate((c.getFinishDate());
			
			
			
		}*/

        /**
         * usuniecie powi�zania pomiedzy dwoma sprawami w tabeli DS_CASE_TO_CASE
         * sprawy powiazane s� wzajemnie wedlug schematu;
         * mowa o obeikcie CaseToCase
         *  sprawa1.caseContainerId = sprawa2.linkedCaseId
         *  sprawa1.linkedCaseId = sprawa2.caseContainerId
         *
         *
         * @param ctc
         * @param currentCaseid
         * @throws SQLException
         * @throws EdmException
         */
        public void removeCaseToCase(Long caseToRemoveId , Long currentCaseid) throws SQLException, EdmException{
            DSApi.beginTransacionSafely();

            //dodawana sprawa
            OfficeCase caseToRemove = OfficeCase.find(caseToRemoveId);
            //przegladana sprawa
            OfficeCase currentCase = OfficeCase.find(currentCaseid);

            long caseContainerId = currentCaseid;
            long LinkedCaseId =caseToRemoveId;

            String sqlCurrent= "Delete from DS_CASE_TO_CASE where caseContainerId =? and linkedCaseId =? ";
            PreparedStatement psCur = DSApi.context().prepareStatement(sqlCurrent);
            psCur.setLong(1, caseContainerId);
            psCur.setLong(2, LinkedCaseId);
            psCur.execute();
            psCur.close();
            DSApi.context().commit();
            DSApi.beginTransacionSafely();
            String sqlLinked= "Delete from DS_CASE_TO_CASE where linkedCaseId =?  and caseContainerId =?";
            PreparedStatement psLin = DSApi.context().prepareStatement(sqlLinked);
            psLin.setLong(1, caseContainerId);
            psLin.setLong(2, LinkedCaseId);
            psLin.execute();
            psLin.close();

            currentCase.getAudit().add(Audit.create("casetocase",
                    DSApi.context().getPrincipalName(),
                    " Usunieto powiazanie ze  sprawa o symbolu :" +caseToRemove.getOfficeId()));

            caseToRemove.getAudit().add(Audit.create("casetocase",
                    DSApi.context().getPrincipalName(),
                    " Usunieto powiazanie ze  sprawa o symbolu :" +currentCase.getOfficeId()));

            DSApi.context().commit();
        }
        /**
         * usuniecie powi�zania pomiedzy dwoma sprawami w tabeli DS_CASE_TO_CASE
         * sprawy powiazane s� wzajemnie wedlug schematu;
         * mowa o obeikcie CaseToCase
         *  sprawa1.caseContainerId = sprawa2.linkedCaseId
         *  sprawa1.linkedCaseId = sprawa2.caseContainerId
         *
         *
         * @param ctc
         * @param currentCaseid
         * @throws SQLException
         * @throws EdmException
         */
		public void removeCaseToCase(CaseToCase ctc , Long currentCaseid) throws SQLException, EdmException{
            DSApi.beginTransacionSafely();

            //dodawana sprawa
            OfficeCase caseToRemove = OfficeCase.find(ctc.getCaseContainerId());
            //przegladana sprawa
            OfficeCase currentCase = OfficeCase.find(currentCaseid);

            long caseContainerId = ctc.getCaseContainerId();
            long LinkedCaseId =ctc.getLinkedCaseId();

            String sqlCurrent= "Delete from DS_CASE_TO_CASE where caseContainerId =? and linkedCaseId =? ";
            PreparedStatement psCur = DSApi.context().prepareStatement(sqlCurrent);
            psCur.setLong(1, caseContainerId);
            psCur.setLong(2, LinkedCaseId);
            psCur.execute();
            psCur.close();
            DSApi.context().commit();
            DSApi.beginTransacionSafely();
            String sqlLinked= "Delete from DS_CASE_TO_CASE where linkedCaseId =?  and caseContainerId =?";
            PreparedStatement psLin = DSApi.context().prepareStatement(sqlLinked);
            psLin.setLong(1, caseContainerId);
            psLin.setLong(2, LinkedCaseId);
            psLin.execute();
            psLin.close();

            currentCase.getAudit().add(Audit.create("casetocase",
                    DSApi.context().getPrincipalName(),
                    " Usunieto powiazanie ze  sprawa o symbolu :" +caseToRemove.getOfficeId()));

            caseToRemove.getAudit().add(Audit.create("casetocase",
                    DSApi.context().getPrincipalName(),
                    " Usunieto powiazanie ze  sprawa o symbolu :" +currentCase.getOfficeId()));

            DSApi.context().commit();
		}
	    
	    public  List<CaseToCase> getCasesAsignedToCase(OfficeCase c) throws EdmException
		{ 
	    	Criteria criteria = DSApi.context().session().createCriteria(CaseToCase.class);
			criteria.add(Restrictions.eq("linkedCaseId", c.getId())); 
			
			 return  criteria.list();
		}
	    
	    public  List<OfficeCase> getClosedCases() throws EdmException
		{ 
	    	Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
			criteria.add(Restrictions.eq("closed", true));
			
			 return  criteria.list();
		}
	    
	    public  List<OfficeCase> getNotClosedCases() throws EdmException
		{ 
	    	Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
			criteria.add(Restrictions.eq("closed", false));
			
			 return  criteria.list();
		}
	    public  List<OfficeCase> getNotClosedCasesWhereUserIsClerk(DSUser user) throws EdmException
		{ 
	    	Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
			criteria.add(Restrictions.eq("closed", false));
			criteria.add(Restrictions.eq("clerk", user.getName()));
			
			
			 return  criteria.list();
		}
	    public  List<OfficeCase> getNotClosedCasesWhereUserIsAutor(DSUser user) throws EdmException
		{ 
	    	Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
			criteria.add(Restrictions.eq("closed", false));
			criteria.add(Restrictions.eq("autor", user.getName()));
			
			
			 return  criteria.list();
		}

	public void fillCaseToCaseBeans(List caseToCaseBeans, List<CaseToCase> listLinkedCases, ActionEvent event) throws UserNotFoundException, EdmException
	{

		for (Iterator iter = listLinkedCases.iterator(); iter.hasNext();)
		{
			CaseToCase ctc = (CaseToCase) iter.next();

			

			for (OfficeCase c :  findCase(ctc.getCaseContainerId()))
			{

				DynaBean bean = DynaBeans.bean(DynaBeans.caseToCase);
				//z caseto case 
				bean.set("caseToCaseId", ctc.getId());
				bean.set("creatingUserId", ctc.getCreatingUserId());
				bean.set("creatingUserName", DSUser.findByUsername(ctc.getCreatingUserName()).getFirstnameLastnameName());
				bean.set("creationTime", ctc.getCreationTime());
				
				//z sprawy
				bean.set("caseContainerId", c.getId());
				bean.set("caseOfficeId", c.getOfficeId() != null ? c.getOfficeId() : "");
				bean.set("caseTitle", (c.getTitle() != null ? c.getTitle() : ""));
				bean.set("linkToCase", event.getRequest().getContextPath() + "/office/edit-case.do?id=" + ctc.getCaseContainerId());
				bean.set("caseAuthor", (DSUser.findByUsername(c.getAuthor()).getFirstnameLastnameName()));
				bean.set("caseOpenDate", (c.getOpenDate()));

				if (c.getFinishDate() != null)
					bean.set("caseFinishDate", c.getFinishDate());

				caseToCaseBeans.add(bean);
			}
		}


	}
	private  List<OfficeCase>findCase(Long IdCase) throws EdmException
	{
    	Criteria criteria = DSApi.context().session().createCriteria(OfficeCase.class);
		criteria.add(Restrictions.eq("id", IdCase));
		
		 return  criteria.list();
		
	}
	//klasa do wrzucania danych do selecta
		
		/*public class CaseToCaseValue {
			private String value;
			private String key;
			private long containerId;
			 
			public CaseToCaseValue() {
				value = "";
				key = "";
				containerId= 0;
			}
			public CaseToCaseValue(String value, String key ,Long idContainer) {
				this.value = value;
				this.key = key;
				this.containerId = idContainer;
			}
			public void setValue(String value){
				this.value = value;
			}
			public String getValue(){
				return value;
			}
			public void setKey(String key){
				this.key = key;
			}
			public String getKey(){
				return key;
			}
			public void setcontainerId(long containerId){
				this.containerId = containerId;
			}
			public long getcontainerId(){
				return containerId;
			}
		}*/
		
	public class CaseToCaseValue {
		private String value;
		private String key;
		private long containerId;
		 
		public CaseToCaseValue() {
			value = "";
			key = "";
			containerId= 0;
		}
		public CaseToCaseValue(String value, String key ,Long idContainer) {
			this.value = value;
			this.key = key;
			this.containerId = idContainer;
		}
		public void setValue(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
		public void setKey(String key){
			this.key = key;
		}
		public String getKey(){
			return key;
		}
		public void setcontainerId(long containerId){
			this.containerId = containerId;
		}
		public long getcontainerId(){
			return containerId;
		}
	}
		
		public  List getUserCasesToSelect(OfficeCase currentCase) throws UserNotFoundException, EdmException
		{
			List<CaseToCaseValue> casesSelectValues = new ArrayList<CaseToCaseValue>();
			List<OfficeCase> userCases =  OfficeCase.findUserCases(DSApi.context().getDSUser(), null);
			for (OfficeCase userCase :userCases){
				if (!(currentCase.getId()==userCase.getId()))
				casesSelectValues.add( new CaseToCaseValue(userCase.getOfficeId(),userCase.getId().toString(),userCase.getId()));
				
			}
			 return casesSelectValues;
		}

		public  List getLinkedCasesToSelect( OfficeCase currentCase) throws EdmException
		{
			 List<CaseToCase> linkedCases = getCasesAsignedToCase(currentCase);
			 
			 
			List<CaseToCaseValue> casesSelectValues = new ArrayList<CaseToCaseValue>();
		
			for (CaseToCase ctc :linkedCases){
				OfficeCase  linkedCase =OfficeCase.find(ctc.getCaseContainerId());
				casesSelectValues.add(new CaseToCaseValue(linkedCase.getOfficeId(),String.valueOf(ctc.getId()) ,ctc.getId()));
				
			}
			 return casesSelectValues;
		}

		public CaseToCase findById(long CaseToCaseId) throws EdmException
		{
			Criteria criteria = DSApi.context().session().createCriteria(CaseToCase.class);
			criteria.add(Restrictions.eq("id", CaseToCaseId));
			
			if (!criteria.list().isEmpty())
			 return  (CaseToCase) criteria.list().get(0);
			else 
				return null;
		}
	

	   
}
