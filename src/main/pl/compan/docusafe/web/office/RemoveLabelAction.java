package pl.compan.docusafe.web.office;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class RemoveLabelAction extends EventActionSupport{

	private Long documentId;
	private String activity;
	private Long labelId;
	
	protected void setup()
    {	
    	
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new Remove()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
	    	
	private class Remove implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		LabelsManager.removeLabel(documentId, labelId,DSApi.context().getPrincipalName());
        		DSApi.context().commit();
        		if(Document.find(documentId) instanceof InOfficeDocument) event.setResult("in-office-document");
        		else if (Document.find(documentId) instanceof OutOfficeDocument && !((OutOfficeDocument)Document.find(documentId)).isInternal()) event.setResult("out-office-document");
        		else event.setResult("int-office-document");
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        	}
        }
    }

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Long getLabelId() {
		return labelId;
	}

	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}
	
}
