package pl.compan.docusafe.web.office;

import java.util.Collection;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.ChannelBean;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ChannelsViewAction extends EventActionSupport {

	private Collection<ChannelBean> channelBeans;
	
	protected void setup() {
		 FillForm fillForm = new FillForm();

	        registerListener(DEFAULT_ACTION).
	            append(OpenHibernateSession.INSTANCE).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			
			//DSContext ctx = DSApi.context();
			try {
				//ctx.begin();
				setChannelBeans(ChannelBean.prepareBeans());
				//ctx.commit();
			} catch (EdmException e) {
				addActionError(e.getMessage());
				//ctx._rollback();
			}
		}
		
	}
	
	public void setChannelBeans(Collection<ChannelBean> channelBeans) {
		this.channelBeans = channelBeans;
	}

	public Collection<ChannelBean> getChannelBeans() {
		return channelBeans;
	}

	

}
