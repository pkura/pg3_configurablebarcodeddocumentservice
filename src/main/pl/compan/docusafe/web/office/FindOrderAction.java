package pl.compan.docusafe.web.office;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OfficeOrder.Query;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FindCasesAction.java,v 1.31 2007/05/18 10:23:59 mwlizlo Exp $
 */
public class FindOrderAction extends EventActionSupport
{
    private static final int LIMIT = 20;
    private int offset = 0;

    private String thisSearchUrl;

    private boolean popup;

    private List<DSUser> users;
    private List<CaseStatus> statuses;
    private SearchResults<Map> results;
    private Pager pager;
    private Long id;
    private List<Tab> tabs = new ArrayList<Tab>(4);
    private Map<Integer, String> records = new LinkedHashMap<Integer, String>();

    private String officeId;
    private String[] assignedUser;
    private String[] person;
    private Integer seqNum;
    private Integer year;
    private String cdateFrom;
    private String cdateTo;

    private Integer[] statusId;
    private boolean ascending;
    private Integer[] record;
    private Integer rwaId;
    private String prettyRwaCategory;
    private static final String EV_SEARCH = "search";
    
    
    
    private Integer officeNumberYear;
    private Integer officeNumber;
    private Integer fromOfficeNumber;
    private Integer toOfficeNumber;
    private String clerk;
    private String summary;
    private String documentDateFrom;
    private String documentDateTo;
    private Long documentId;
    private String sortField;
    private String status;
    private String priority;
    private String creatingUser;
    private String detailedDescription;
    private Map<String,String> statusMap;
    private Map<String,String> priorityMap;

    

    protected void setup()
    {
        class SetResult implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                event.setResult(popup ? "popup" : SUCCESS);
            }
        }

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new Tabs()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new Tabs()).
          //  append(new ValidateSearch()).
            append(EV_SEARCH, new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Tabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            tabs.add(new Tab("Pisma przychodz�ce", "Pisma przychodz�ce",
                HttpUtils.makeUrl("/office/find-office-documents.action",
                    new String[] { "tab", FindOfficeDocumentsAction.TAB_IN }),
                false));

            if (Configuration.officeAvailable() || Configuration.simpleOfficeAvailable())
            {
                tabs.add(new Tab("Pisma wewn�trzne", "Pisma wewn�trzne",
                    HttpUtils.makeUrl("/office/find-office-documents.action",
                        new String[] { "tab", FindOfficeDocumentsAction.TAB_INT }),
                    false));

                tabs.add(new Tab("Pisma wychodz�ce", "Pisma wychodz�ce",
                    HttpUtils.makeUrl("/office/find-office-documents.action",
                        new String[] { "tab", FindOfficeDocumentsAction.TAB_OUT }),
                    false));
            }

            if (!Configuration.hasExtra("business"))
            {
                tabs.add(new Tab("Sprawy", "Sprawy",
                    HttpUtils.makeUrl("/office/find-cases.action", null),
                    false));
            }
            if (!Configuration.hasExtra("business") && Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
            tabs.add(new Tab("Polecenia", "Polecenia",
                HttpUtils.makeUrl("/office/find-orders.action",null),
                true));

            
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE))
                {
                    users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                else if (DSApi.context().hasPermission(DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA))
                {
                    // tylko z dzia�u
                    users = Arrays.asList(DSApi.context().getDSUser().getNeighbours(
                        new String[] { DSDivision.TYPE_DIVISION, DSDivision.TYPE_POSITION }));
                }
                else
                {
                    users = new ArrayList<DSUser>(1);
                    users.add(DSApi.context().getDSUser());
                }
                statusMap = OfficeOrder.getStatusMap();
                priorityMap = OfficeOrder.getPriorityMap();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateSearch implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date dCdateFrom = DateUtils.nullSafeParseJsDate(cdateFrom);
            Date dCdateTo = DateUtils.nullSafeParseJsDate(cdateTo);
            if (dCdateFrom != null && dCdateTo != null &&
                dCdateFrom.after(dCdateTo))
                addActionError("Pocz�tkowa data utworzenia jest p�niejsza " +
                    "od daty ko�cowej");

            if (hasActionErrors())
                event.skip(EV_SEARCH);
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {

            // url bez sortowania
            thisSearchUrl = "/office/find-orders.action?doSearch=true" +
                "&offset="+offset+
                (priority != null ? "&priority="+priority : "") +
                (status != null ? "&status="+status : "") +
                (creatingUser != null ? "&creatingUser="+creatingUser : "") +
                (clerk != null ? "&clerk="+clerk : "") +
                (summary != null ? "&summary="+summary : "") +
                (detailedDescription != null ? "&detailedDescription="+detailedDescription : "") +
                (documentDateFrom != null ? "&documentDateFrom="+documentDateFrom : "") +
                (documentDateTo != null ? "&documentDateTo="+documentDateTo : "") +
                (documentId != null ? "&documentId="+documentId : "")+
                (officeNumber!= null ? "&officeNumber="+officeNumber : "")+
                (officeNumberYear != null ? "&officeNumberYear="+officeNumberYear : "")+
                (fromOfficeNumber != null ? "&fromOfficeNumber="+fromOfficeNumber : "")+
                (toOfficeNumber != null ? "&toOfficeNumber="+toOfficeNumber : "");
   
            
            {
                try
                {
                    Query query = OfficeOrder.query(offset, LIMIT);
                    query.setClerk(TextUtils.trimmedStringOrNull(clerk));
                    query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
                    query.setDetailedDescription(TextUtils.trimmedStringOrNull(detailedDescription,4000));
                    query.setDocumentDateFrom(DateUtils.nullSafeParseJsDate(documentDateFrom));
                    query.setDocumentDateTo(DateUtils.nullSafeParseJsDate(documentDateTo));
                    query.setPriority(TextUtils.trimmedStringOrNull(priority));
                    query.setStatus(TextUtils.trimmedStringOrNull(status));
                    query.setSummary(TextUtils.trimmedStringOrNull(summary, 80));
                    query.setOfficeNumber(officeNumber);
                    query.setOfficeNumberYear(officeNumberYear);
                    query.setFromOfficeNumber(fromOfficeNumber);
                    query.setToOfficeNumber(toOfficeNumber);
                    query.setDocumentId(documentId);
                    query.setSortField(sortField);
                    query.setAscending(ascending);

                    lambda<OfficeOrder, Map> mapper = new lambda<OfficeOrder, Map>()
                    {
                        public Map act(OfficeOrder officeOrder)
                        {
                            BeanBackedMap result = new BeanBackedMap(officeOrder,
                                "officeNumber","id","clerk","summary","creatingUser","status","priority");
                            try
                            {
                                DSApi.initializeProxy(officeOrder.getStatus());
                                result.put("clerk", DSUser.safeToFirstnameLastname(officeOrder.getClerk()));
                            }
                            catch (EdmException e)
                            {
                                event.getLog().warn(e.getMessage(), e);
                            }
                            return result;
                        }
                    };

                    results = OfficeOrder.search(query, mapper, Map.class);

                    if (results.totalCount() == 0)
                        addActionError("Nie znaleziono �adnych polece�");
                    
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return HttpUtils.makeUrl("/office/find-orders.action",
                                new Object[] {
                                "doSearch", "true",
                                "priority", priority,
                                "status", status,
                                "creatingUser", creatingUser,
                                "clerk", clerk,
                                "summary", summary,
                                "detailedDescription", detailedDescription,
                                "documentDateFrom", documentDateFrom,
                                "documentDateTo", documentDateTo,
                                "documentId",documentId,
                                "officeNumber",officeNumber,
                                "officeNumberYear ",officeNumberYear ,
                                "fromOfficeNumber",fromOfficeNumber,
                                "toOfficeNumber",toOfficeNumber,
                                "sortField", sortField,
                                "ascending", String.valueOf(ascending),
                                "offset", String.valueOf(offset)});
                        }
                                       
                    };

                    pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }


    /**
     * Funkcja wywo�ywana ze strony JSP podczas wypisywania wynik�w wyszukiwania.
     * Funkcja jest wywo�ywana na tej samej instancji ActionSupport, kt�ra
     * wykonywa�a wyszukiwanie.
     */
    public String getCaseLink(Map caseBean)
    {   
        return EditCaseAction.getLink((Long) caseBean.get("id"), null, null, thisSearchUrl);
    }

    /**
     * Funkcja wywo�ywana ze strony JSP.
     */
    public String getSortLink(String field, boolean asc)
    {
        
        return thisSearchUrl + "&sortField="+field + "&ascending="+asc;
    }

    public String getUserFirstnameLastname(String userName) throws UserNotFoundException, EdmException
    {
            DSApi.openAdmin();
            DSUser user = DSUser.findByUsername(userName);
            if(user != null)
            {
                String tmp = user.asFirstnameLastname();
                DSApi.close();
                return tmp;
            }
            else
                DSApi.close();
            return "";

    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public List<CaseStatus> getStatuses()
    {
        return statuses;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public void setAssignedUser(String[] assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public void setPerson(String[] person)
    {
        this.person = person;
    }

    public void setSeqNum(Integer seqNum)
    {
        this.seqNum = seqNum;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public void setStatusId(Integer[] statusId)
    {
        this.statusId = statusId;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public boolean isPopup()
    {
        return popup;
    }

    public void setPopup(boolean popup)
    {
        this.popup = popup;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setCdateFrom(String cdateFrom)
    {
        this.cdateFrom = cdateFrom;
    }

    public void setCdateTo(String cdateTo)
    {
        this.cdateTo = cdateTo;
    }

    public void setOfficeId(String officeId)
    {
        this.officeId = officeId;
    }

    public Long getId()
    {
        return id;
    }

    public List getTabs()
    {
        return tabs;
    }

    public Map<Integer, String> getRecords()
    {
        return records;
    }

    public Integer[] getRecord()
    {
        return record;
    }

    public void setRecord(Integer[] record)
    {
        this.record = record;
    }
    
    public Integer getRwaId()
    {
        return rwaId;
    }
    
    public void setRwaId(Integer rwaId)
    {
        this.rwaId = rwaId;
    }
    
    public String getPrettyRwaCategory()
    {
        return prettyRwaCategory;
    }
    
    public void setPrettyRwaCategory(String prettyRwaCategory)
    {
        this.prettyRwaCategory = prettyRwaCategory;
    }

    public static String getEV_SEARCH()
    {
        return EV_SEARCH;
    }

    public static int getLIMIT()
    {
        return LIMIT;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerk(String clerk)
    {
        this.clerk = clerk;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public String getDetailedDescription()
    {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription)
    {
        this.detailedDescription = detailedDescription;
    }

    public String getDocumentDateFrom()
    {
        return documentDateFrom;
    }

    public void setDocumentDateFrom(String documentDateFrom)
    {
        this.documentDateFrom = documentDateFrom;
    }

    public String getDocumentDateTo()
    {
        return documentDateTo;
    }

    public void setDocumentDateTo(String documentDateTo)
    {
        this.documentDateTo = documentDateTo;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getFromOfficeNumber()
    {
        return fromOfficeNumber;
    }

    public void setFromOfficeNumber(Integer fromOfficeNumber)
    {
        this.fromOfficeNumber = fromOfficeNumber;
    }

    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public void setOfficeNumber(Integer officeNumber)
    {
        this.officeNumber = officeNumber;
    }

    public Integer getOfficeNumberYear()
    {
        return officeNumberYear;
    }

    public void setOfficeNumberYear(Integer officeNumberYear)
    {
        this.officeNumberYear = officeNumberYear;
    }

    public String getPriority()
    {
        return priority;
    }

    public void setPriority(String priority)
    {
        this.priority = priority;
    }

    public Map<String, String> getPriorityMap()
    {
        return priorityMap;
    }

    public void setPriorityMap(Map<String, String> priorityMap)
    {
        this.priorityMap = priorityMap;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Map<String, String> getStatusMap()
    {
        return statusMap;
    }

    public void setStatusMap(Map<String, String> statusMap)
    {
        this.statusMap = statusMap;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getThisSearchUrl()
    {
        return thisSearchUrl;
    }

    public void setThisSearchUrl(String thisSearchUrl)
    {
        this.thisSearchUrl = thisSearchUrl;
    }

    public Integer getToOfficeNumber()
    {
        return toOfficeNumber;
    }

    public void setToOfficeNumber(Integer toOfficeNumber)
    {
        this.toOfficeNumber = toOfficeNumber;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public String[] getAssignedUser()
    {
        return assignedUser;
    }

    public String getCdateFrom()
    {
        return cdateFrom;
    }

    public String getCdateTo()
    {
        return cdateTo;
    }

    public String getOfficeId()
    {
        return officeId;
    }

    public int getOffset()
    {
        return offset;
    }

    public String[] getPerson()
    {
        return person;
    }

    public Integer getSeqNum()
    {
        return seqNum;
    }

    public String getSortField()
    {
        return sortField;
    }

    public Integer[] getStatusId()
    {
        return statusId;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public void setRecords(Map<Integer, String> records)
    {
        this.records = records;
    }

    public void setResults(SearchResults<Map> results)
    {
        this.results = results;
    }

    public void setStatuses(List<CaseStatus> statuses)
    {
        this.statuses = statuses;
    }

    public void setTabs(List<Tab> tabs)
    {
        this.tabs = tabs;
    }

    public void setUsers(List<DSUser> users)
    {
        this.users = users;
    }
    
}

