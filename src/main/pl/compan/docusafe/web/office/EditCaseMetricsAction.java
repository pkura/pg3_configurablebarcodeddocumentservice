package pl.compan.docusafe.web.office;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;

public class EditCaseMetricsAction extends EventActionSupport {

    private static final long serialVersionUID = 1L;
    private static final pl.compan.docusafe.util.Logger log = pl.compan.docusafe.util.LoggerFactory.getLogger(EditCaseMetricsAction.class);
    public static final StringManager sm = GlobalPreferences.loadPropertiesFile(EditCaseMetricsAction.class.getPackage().getName(), null);

    private Long officeCaseId;

    @Override
    protected void setup() {

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doMetrics").
                append(OpenHibernateSession.INSTANCE).
                append(new OfficeCaseMetrics()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {

        }
    }

    private class OfficeCaseMetrics implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {

            File metrics = OfficeCaseMetricsGenerator.getMetrics(officeCaseId);
            try {
                ServletUtils.streamFile(ServletActionContext.getResponse(), metrics, "application/pdf", "Content-Disposition: attachment; filename=\"metryka sprawy.pdf\"");
            }catch (Exception e) {
                log.error("", e);
            }
        }
    }

    public Long getOfficeCaseId() {
        return officeCaseId;
    }

    public void setOfficeCaseId(Long officeCaseId) {
        this.officeCaseId = officeCaseId;
    }
}
