package pl.compan.docusafe.web.office;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NewCaseAction extends EventActionSupport {

	private static Logger log = LoggerFactory.getLogger(NewCaseAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	
	private boolean canView;
    private boolean canChooseClerk;
	private boolean canSetArbitraryCaseId;
    
    private List priorities;
    private List<DSUser> clerks;
    private String targetDivision;
    
	private String caseFinishDate;
	private String caseTitle;
	private String description2;
	private String portfolioTxt;
	private Long portfolioId;
    private Integer priority;
    private String clerk;

    private Integer customSequenceId;
    private Boolean customOfficeId;
    private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;
    
    private String suggestedOfficeId;
    private Integer suggestedSequenceId;
	
	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doCreate").
			append(OpenHibernateSession.INSTANCE).
			append(new Create()).
			append(fillForm).
			append(CloseHibernateSession.INSTANCE);
		
        registerListener("doPreCreate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new PreCreate()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private void clean(){
		caseFinishDate = "";
		caseTitle = "";
		description2 = "";
		portfolioTxt = "";
		portfolioId = null;
	    priority = null;
	    clerk = null;
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				canView = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE) || DSApi.context().isAdmin();
				
				if(!canView){
					event.addActionError(sm.getString("permissionCreateCase"));
            		return;
				}				
				
				priorities = CasePriority.list();
				
				DSUser[] substUsers = DSApi.context().getDSUser().getSubstituted();
				
				if (DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA))
		        {
		            canChooseClerk = true;
		            clerks = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
		            if (clerk == null)
		                clerk = DSApi.context().getPrincipalName();
		        }
		        else if (substUsers.length > 0)
		        {
		            canChooseClerk = true;
		            clerks = new LinkedList<DSUser>(Arrays.asList(substUsers));
		            clerks.add(DSApi.context().getDSUser());
		            // na po�rednim ekranie nie nale�y ju� nadpisywa� warto�ci pola clerk
		            if (clerk == null)
		                clerk = substUsers[0].getName();
		        }
		        else
		        {
		            if (clerk == null)
		                clerk = DSApi.context().getPrincipalName();
		        }
				
		        DSUser user = DSApi.context().getDSUser();
		        DSDivision[] divisions = user.getDivisions();
		        if (divisions.length > 0)
		            targetDivision = divisions[0].getGuid();
			} catch (EdmException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
			}
		}
	}
	
	private class Create implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Date finishDate = DateUtils.nullSafeParseJsDate(caseFinishDate);

            if (finishDate == null)
            {
                addActionError("B��dna data zako�czenia sprawy");
                return;
            }

            if (portfolioId == null)
            {
                addActionError("Nie wybrano teczki");
                return;
            }
            
            if(caseTitle==null || caseTitle.length()==0){
            	portfolioId=null;
            	addActionError("Nie podano tytu�u sprawy");
            	return;
            }

            try
            {
                
                DSApi.context().begin();

                OfficeFolder portfolio = OfficeFolder.find(portfolioId);

                if (portfolio.getRwa().getAcHome() == null)
                    throw new EdmException("Nie mo�na za�o�y� sprawy w RWA nie posiadaj�cym kategorii archiwalnej");

                canChooseClerk = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA);
                canSetArbitraryCaseId = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_DOWOLNY_SYMBOL);

                OfficeCase officeCase = new OfficeCase(portfolio);                
                
                officeCase.setStatus(CaseStatus.find(CaseStatus.ID_OPEN));
                officeCase.setPriority(CasePriority.find(priority));
                officeCase.setFinishDate(finishDate);
                officeCase.setDescription(description2);
                officeCase.setTitle(caseTitle);
                

                //if (canChooseClerk)
                if ((canChooseClerk || DSApi.context().getDSUser().getSubstituted().length > 0) && clerk != null)
                {
                    officeCase.setClerk(DSUser.findByUsername(clerk).getName());
                }
                else
                {
                    officeCase.setClerk(DSApi.context().getPrincipalName());
                }

                if (customOfficeId != null && customOfficeId.booleanValue())
                {
                    if (customSequenceId == null)
                        throw new EdmException("Nie podano numeru kolejnego sprawy lub wpisano niepoprawn� liczb�");

                    if (customSequenceId.intValue() <= 0)
                        throw new EdmException("Numer kolejny sprawy musi by� wi�kszy od zera");

                    if (!officeCase.sequenceIdAvailable(customSequenceId))
                        throw new EdmException("Wybrany numer kolejny ("+customSequenceId+") jest niedost�pny");

                    officeCase.setSequenceId(customSequenceId);

                    if (suggestedSequenceId.intValue() != customSequenceId.intValue())
                    {
                        suggestedSequenceId = customSequenceId;
                        officeCase.setSequenceId(customSequenceId);
                        String[] breakdown = officeCase.suggestOfficeId();
                        suggestedOfficeId = StringUtils.join(breakdown, "");
                        customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                        customOfficeIdPrefixSeparator = breakdown[1];
                        customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                        customOfficeIdSuffixSeparator = breakdown[3];
                        customOfficeIdSuffix = officeIdSuffix = breakdown[4];
                        
                        addActionMessage("Wygenerowano nowy znak sprawy dla zmienionego numeru porz�dkowego");
                        event.setResult("pre-create");
                        
                        return;
                    }

                    String oid = StringUtils.join(new String[] {
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
                    }, "");

                    officeCase.create(oid,
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");
                }
                else
                {
                    officeCase.setSequenceId(suggestedSequenceId);
                    officeCase.create(suggestedOfficeId,
                        officeIdPrefix, officeIdMiddle, officeIdSuffix);
                }

                DSApi.context().commit();

                clean();
                
                addActionMessage("Utworzono spraw� "+officeCase.getOfficeId());   
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
            	log.error(e.getMessage(),e);
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
		}
	}
	
	private class PreCreate implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Date finishDate = DateUtils.nullSafeParseJsDate(caseFinishDate);
			
            if(description2!=null && description2.length()>0){
                if(description2.length()>200)
                {
                    event.addActionMessage("Opis sprawy mo�e wynosi� maksymalnie 200 znak�w");
                    return;
                }
            }
            
            if (finishDate == null)
                addActionError("B��dna data zako�czenia sprawy");
            
            try
            {
            	finishDate.setHours(23);
            	finishDate.setMinutes(59);
                if (finishDate.before(GlobalPreferences.getCurrentDay()))
                    addActionError("Data zako�czenia sprawy jest wcze�niejsza od daty za�o�enia");
            }
            catch (Exception e)
            {                
            }

            if (portfolioId == null)
                addActionError("Nie wybrano teczki");

            if(caseTitle==null || caseTitle.length()==0)
            	addActionError("Nie podano tytu�u sprawy");

            if (hasActionErrors())
                return;

            event.setResult("pre-create");

            try
            {
                OfficeFolder portfolio = OfficeFolder.find(portfolioId);
                DSUser user = DSApi.context().getDSUser();
                boolean sameDiv = false ;
                
                
                for(DSDivision div :user.getOriginalDivisions()){
                	if (div.getGuid().contains(portfolio.getDivisionGuid()))
                		sameDiv=true;
                }
                if(sameDiv &&  !DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE)){
            		sameDiv=false;
            	} 
                
                if (!sameDiv && !canCreateCaseFromListBriefcase(portfolio)){
            	   throw new EdmException("Nie masz uprawnien do zak�adania sprawy w tej teczce");
               }
              
                canChooseClerk = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA);
                canSetArbitraryCaseId = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_DOWOLNY_SYMBOL);

                OfficeCase officeCase = new OfficeCase(portfolio);

                suggestedSequenceId = customSequenceId = officeCase.suggestSequenceId();

                String[] breakdown = officeCase.suggestOfficeId();
                suggestedOfficeId = StringUtils.join(breakdown, "");
                customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                customOfficeIdPrefixSeparator = breakdown[1];
                customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                customOfficeIdSuffixSeparator = breakdown[3];
                customOfficeIdSuffix = officeIdSuffix = breakdown[4];

                event.setResult("pre-create");
            }
            catch (EdmException e)
            {
                event.setResult(SUCCESS);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
		}
	}
	
    private boolean canCreateCaseFromListBriefcase(OfficeFolder portfolio) throws UserNotFoundException, EdmException
	{
    	if (!AvailabilityManager.isAvailable("menu.left.user.to.briefcase"))
    		return true;
    	else
    	return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.canCreateCaseFromListBriefcase(portfolio ,DSApi.context().getDSUser() );
    	
		
	}
	
    public boolean isCanView() {
		return canView;
	}

	public boolean isCanChooseClerk() {
		return canChooseClerk;
	}

	public List getPriorities() {
		return priorities;
	}

	public List<DSUser> getClerks() {
		return clerks;
	}

	public String getTargetDivision() {
		return targetDivision;
	}

	public void setTargetDivision(String targetDivision) {
		this.targetDivision = targetDivision;
	}

	public String getCaseFinishDate() {
		return caseFinishDate;
	}

	public void setCaseFinishDate(String caseFinishDate) {
		this.caseFinishDate = caseFinishDate;
	}

	public String getCaseTitle() {
		return caseTitle;
	}

	public void setCaseTitle(String caseTitle) {
		this.caseTitle = caseTitle;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description) {
		this.description2 = description;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getClerk() {
		return clerk;
	}

	public void setClerk(String clerk) {
		this.clerk = clerk;
	}

	public Long getPortfolioId() {
		return portfolioId;
	}

	public void setPortfolioId(Long portfolioId) {
		this.portfolioId = portfolioId;
	}

	public boolean isCanSetArbitraryCaseId() {
		return canSetArbitraryCaseId;
	}

	public String getPortfolioTxt() {
		return portfolioTxt;
	}

	public void setPortfolioTxt(String portfolioTxt) {
		this.portfolioTxt = portfolioTxt;
	}

	public Integer getCustomSequenceId() {
		return customSequenceId;
	}

	public void setCustomSequenceId(Integer customSequenceId) {
		this.customSequenceId = customSequenceId;
	}

	public Boolean getCustomOfficeId() {
		return customOfficeId;
	}

	public void setCustomOfficeId(Boolean customOfficeId) {
		this.customOfficeId = customOfficeId;
	}

	public String getCustomOfficeIdPrefix() {
		return customOfficeIdPrefix;
	}

	public void setCustomOfficeIdPrefix(String customOfficeIdPrefix) {
		this.customOfficeIdPrefix = customOfficeIdPrefix;
	}

	public String getCustomOfficeIdPrefixSeparator() {
		return customOfficeIdPrefixSeparator;
	}

	public void setCustomOfficeIdPrefixSeparator(
			String customOfficeIdPrefixSeparator) {
		this.customOfficeIdPrefixSeparator = customOfficeIdPrefixSeparator;
	}

	public String getCustomOfficeIdMiddle() {
		return customOfficeIdMiddle;
	}

	public void setCustomOfficeIdMiddle(String customOfficeIdMiddle) {
		this.customOfficeIdMiddle = customOfficeIdMiddle;
	}

	public String getCustomOfficeIdSuffixSeparator() {
		return customOfficeIdSuffixSeparator;
	}

	public void setCustomOfficeIdSuffixSeparator(
			String customOfficeIdSuffixSeparator) {
		this.customOfficeIdSuffixSeparator = customOfficeIdSuffixSeparator;
	}

	public String getCustomOfficeIdSuffix() {
		return customOfficeIdSuffix;
	}

	public void setCustomOfficeIdSuffix(String customOfficeIdSuffix) {
		this.customOfficeIdSuffix = customOfficeIdSuffix;
	}

	public String getOfficeIdPrefix() {
		return officeIdPrefix;
	}

	public void setOfficeIdPrefix(String officeIdPrefix) {
		this.officeIdPrefix = officeIdPrefix;
	}

	public String getOfficeIdSuffix() {
		return officeIdSuffix;
	}

	public void setOfficeIdSuffix(String officeIdSuffix) {
		this.officeIdSuffix = officeIdSuffix;
	}

	public String getOfficeIdMiddle() {
		return officeIdMiddle;
	}

	public void setOfficeIdMiddle(String officeIdMiddle) {
		this.officeIdMiddle = officeIdMiddle;
	}

	public String getSuggestedOfficeId() {
		return suggestedOfficeId;
	}

	public void setSuggestedOfficeId(String suggestedOfficeId) {
		this.suggestedOfficeId = suggestedOfficeId;
	}

	public Integer getSuggestedSequenceId() {
		return suggestedSequenceId;
	}

	public void setSuggestedSequenceId(Integer suggestedSequenceId) {
		this.suggestedSequenceId = suggestedSequenceId;
	}
}
