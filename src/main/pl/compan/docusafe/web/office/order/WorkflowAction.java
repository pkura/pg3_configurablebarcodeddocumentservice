package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.web.office.common.WorkflowTabAction;
import pl.compan.docusafe.core.office.OfficeOrder;

import java.util.List;
/* User: Administrator, Date: 2006-10-17 16:43:28 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class WorkflowAction extends WorkflowTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/order/workflow.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/order/external-workflow.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }
}
