package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.web.office.common.AttachmentsTabAction;
import pl.compan.docusafe.core.office.OfficeOrder;

import java.util.List;
/* User: Administrator, Date: 2006-10-17 16:43:51 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class AttachmentsAction extends AttachmentsTabAction
{
	boolean canCreateDocFromAtt = false;
	
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/order/attachments.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }

	public boolean isCanCreateDocFromAtt() {
		return canCreateDocFromAtt;
	}
}