package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.ActivityId;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.web.office.common.SummaryTabAction;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
/* User: Administrator, Date: 2006-10-17 15:46:23 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class SummaryAction extends SummaryTabAction
{
      // @EXPORT
    private OfficeOrder document;
    private OfficeDocument relatedOfficeDocument;
    private String relatedDocumentKind;
    private String relatedDocumentName;
    private boolean canFinish;

      // @IMPORT
    /**
     * Lista zbiorczej dekretacji; ka�dy element tablicy ma posta�
     * nazwa_u�ytkownika "," CollectiveAssignmentEntry.id
     */
    private String[] assignments;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignments").
            append(OpenHibernateSession.INSTANCE).
            append(new Assignments()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

      /*  registerListener("doWatch").
            append(OpenHibernateSession.INSTANCE).
            append(new Watch()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);*/

        registerListener("doReopenWf").
            append(OpenHibernateSession.INSTANCE).
            append(new ReopenWf()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualFinish").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualFinish()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

      /*  registerListener("doClone").
            append(OpenHibernateSession.INSTANCE).
            append(new CloneDocument()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);*/
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/order/summary.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/order/external-workflow.action";
    }

    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                document = OfficeOrder.findOfficeOrder(getDocumentId());
               
                canFinish = DSApi.context().getPrincipalName().equals(document.getCreatingUser());
                relatedOfficeDocument = document.getRelatedOfficeDocument();
                if (relatedOfficeDocument != null)
                {
                    if (relatedOfficeDocument.getType() == DocumentType.INCOMING)
                    {
                        relatedDocumentKind = "incoming";
                        relatedDocumentName = "przychodz�cym";
                    }
                    else if (relatedOfficeDocument.getType() == DocumentType.INTERNAL)
                    {
                        relatedDocumentKind = "internal";
                        relatedDocumentName = "wewn�trznym";
                    }
                    if (relatedOfficeDocument.getType() == DocumentType.OUTGOING)
                    {
                        relatedDocumentKind = "outgoing";
                        relatedDocumentName = "wychodz�cym";
                    }
                }
                WorkflowActivity activity = getActivity() != null
                    ? WorkflowFactory.getWfActivity(getActivity())
                    : null;
                fillForm(document, activity);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Assignments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (assignments == null || assignments.length == 0)
            {
                addActionError("Nie wybrano u�ytkownik�w do dekretacji");
                return;
            }

            try
            {
                DSApi.context().begin();

                boolean mainDocumentReassigned = collectiveAssignment(assignments, getActivity(), event);

                // TODO: process.terminate()
                if (mainDocumentReassigned)
                {
                    event.setResult("task-list-order");
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                //DSApi.context().setRollbackOnly();
                try { DSApi.context().rollback(); } catch (EdmException f) {};
                addActionError(e.getMessage());
            }
        }
    }


    private class ReopenWf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                WorkflowUtils.reopenWf(getDocumentId());              
                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                TaskSnapshot.updateByDocumentId(document.getId(),OfficeOrder.TYPE);

                DSApi.context().commit();
                
                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                	LogFactory.getLog("workflow_log").debug(
                			"dla dokumentu przywroconego na liste zadan znaleziono "
                			+activityIds.length
                			+" zadan");
                } else {
                	setActivity(activityIds[0]);
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                WorkflowFactory.getInstance().manualFinish(getActivity(), true);

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

               	TaskSnapshot.updateByDocumentId(document.getId(), OfficeOrder.TYPE);

                DSApi.context().commit();

                event.setResult(getTaskListResult(document.getType()));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }


    public OfficeOrder getDocument()
    {
        return document;
    }

    public OfficeDocument getRelatedOfficeDocument()
    {
        return relatedOfficeDocument;
    }

    public boolean isCanFinish()
    {
        return canFinish;
    }

    public String getRelatedDocumentKind()
    {
        return relatedDocumentKind;
    }

    public String getRelatedDocumentName()
    {
        return relatedDocumentName;
    }

    public void setAssignments(String[] assignments)
    {
        this.assignments = assignments;
    }
}
