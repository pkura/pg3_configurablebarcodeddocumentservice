package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;
import pl.compan.docusafe.core.office.OfficeOrder;

import java.util.List;
/* User: Administrator, Date: 2006-10-17 16:47:38 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class AssignmentHistoryAction extends AssignmentHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/order/assignment-history.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }
}

