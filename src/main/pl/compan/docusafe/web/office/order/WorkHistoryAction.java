package pl.compan.docusafe.web.office.order;

import java.util.List;

import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.web.office.common.WorkHistoryTabAction;
/* User: Administrator, Date: 2006-10-17 16:53:02 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class WorkHistoryAction extends WorkHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/order/work-history.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }
}