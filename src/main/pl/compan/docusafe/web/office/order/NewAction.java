package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;
/* User: Administrator, Date: 2006-10-18 09:38:25 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class NewAction extends EventActionSupport
{
    Long relatedOfficeDocumentId;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (relatedOfficeDocumentId != null)
                event.setResult("relatedOrder");
        }
    }

    public Long getRelatedOfficeDocumentId()
    {
        return relatedOfficeDocumentId;
    }

    public void setRelatedOfficeDocumentId(Long relatedOfficeDocumentId)
    {
        this.relatedOfficeDocumentId = relatedOfficeDocumentId;
    }
}
