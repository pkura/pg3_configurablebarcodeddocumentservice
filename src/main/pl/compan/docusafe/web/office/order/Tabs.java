package pl.compan.docusafe.web.office.order;

import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/* User: Administrator, Date: 2006-10-17 16:39:45 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class Tabs {
    StringManager smTab = GlobalPreferences.loadPropertiesFile("", "tab");
    private static final Log log = LogFactory.getLog(Tabs.class);
    private BaseTabsAction base;

    public Tabs(BaseTabsAction base) {
	this.base = base;
    }

    public List createTabs() {
	List tabs = new ArrayList(10);

	String[] params = new String[] { "documentId",
		String.valueOf(base.getDocumentId()), "activity",
		base.getActivity() };

	if (base.getDocumentId() != null) {
	    tabs.add(new Tab(
		    smTab.getString("Podsumowanie"),
		    smTab.getString("Podsumowanie"),
		    HttpUtils.makeUrl("/office/order/summary.action", params),
		    base instanceof pl.compan.docusafe.web.office.order.SummaryAction));

	    if (Configuration.officeAvailable()) {
		tabs.add(new Tab(smTab.getString("document.attachments"), smTab
			.getString("document.attachments"), HttpUtils.makeUrl(
			"/office/order/attachments.action", params),
			base instanceof AttachmentsAction));
	    }

	    tabs.add(new Tab(smTab.getString("Ogolne"), smTab
		    .getString("Ogolne"), HttpUtils.makeUrl(
		    "/office/order/main.action", params),
		    base instanceof MainAction));

	    /*
	     * if (!Configuration.hasExtra("nationwide")) { tabs.add(new
	     * Tab("Sprawa", "Sprawa",
	     * HttpUtils.makeUrl("/office/order/case.action", params), base
	     * instanceof CaseAction)); }
	     */

	    tabs.add(new Tab(smTab.getString("Uwagi"),
		    smTab.getString("Uwagi"), HttpUtils.makeUrl(
			    "/office/order/remarks.action", params),
		    base instanceof RemarksAction));

	    tabs.add(new Tab("Historia polecenia",
		    "Historia pracy nad poleceniem", HttpUtils.makeUrl(
			    "/office/order/work-history.action", params),
		    base instanceof WorkHistoryAction));

	    if (Configuration.officeAvailable()) {
		tabs.add(new Tab(smTab.getString("HistoriaDekretacji"), smTab
			.getString("HistoriaDekretacji"), HttpUtils.makeUrl(
			"/office/order/assignment-history.action", params),
			base instanceof AssignmentHistoryTabAction));
	    }

	    boolean dekretacjaPermission = false;
        Long docuId = base.getDocumentId();
        String docCn = "";
	    try {
	    	dekretacjaPermission = DSApi.context().hasPermission(
	    			DSPermission.WWF_DEKRETACJA_DOWOLNA);
    		Document doc = Document.find(docuId);
    		docCn = doc.getDocumentKind().getCn();
	    } catch (EdmException e) {
		log.warn(e.getMessage());
	    }

	    if (Configuration.officeAvailable() && dekretacjaPermission && !AvailabilityManager.isAvailable("canNotDecree", docCn)) {
		if (!StringUtils.isEmpty(base.getActivity())) {
		    tabs.add(new Tab(smTab.getString("Dekretacja"), smTab
			    .getString("Dekretacja"), HttpUtils.makeUrl(
			    "/office/order/workflow.action", params),
			    base instanceof WorkflowAction));
		}
	    }

	    /*
	     * if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) {
	     * tabs.add(new Tab("Podpisy", "Podpisy",
	     * HttpUtils.makeUrl("/office/order/signatures.action", params),
	     * base instanceof
	     * pl.compan.docusafe.web.office.in.SignaturesAction)); }
	     */
	}

	return tabs;
    }
}
