package pl.compan.docusafe.web.office.order;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.MainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class MainAction extends MainTabAction
{
	private Logger log = LoggerFactory.getLogger(MainAction.class);
	private final static StringManager sm=
        GlobalPreferences.loadPropertiesFile(MainAction.class.getPackage().getName(),null);
    // @EXPORT
    private DSUser[] users;
    private Map priorities;
    private Map statusMap;
    private Long newDocumentId;
    private String creatingUser;
    private String assignedDivision;
    private boolean currentDayWarning;
    private Integer officeNumber;
    private boolean canEdit;
    private boolean canChangeStatus;

    // @EXPORT/@IMPORT
    private String description;
    private String priority;
    private String detailedDescription;
    private String status;
    private String finishDate;
    private String assignedUser;
    private Long relatedOfficeDocumentId;

    // @IMPORT
    private FormFile file;

    public static final String EV_CREATE = "create";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);

        registerListener("doNewOrder").
            append(fillForm);

        registerListener("doCreate").
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(fillForm);

        registerListener("doUpdate").
            append(new ValidateCreate()).
            append(EV_CREATE, new Update()).
            append(fillForm);

        registerListener("doChangeStatus").
            append(new ChangeStatus()).
            append(fillForm);
    }


    public String getBaseLink()
    {
        return "/office/order/main.action";
    }

    public String getDocumentType()
    {
        return OfficeOrder.TYPE;
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                setTabs(new Tabs(MainAction.this).createTabs());

                priorities = OfficeOrder.getPriorityMap();
                statusMap = OfficeOrder.getStatusMap();

                if (getDocumentId() != null)
                {
                    //users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                	users = DSDivision.find(DSDivision.ROOT_GUID).getUsersFromDivision(DSApi.context().getDSUser().getDivisions());
                    UserFactory.sortUsers(users, DSUser.SORT_LASTNAME_FIRSTNAME, true);

                    OfficeOrder order = OfficeOrder.findOfficeOrder(getDocumentId());

                    canEdit = DSApi.context().getPrincipalName().equals(order.getCreatingUser());
                    if (getActivity() != null)
                    {
                    	WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());
                        canChangeStatus = !canEdit && WorkflowFactory.getInstance().isManual(activity);
                    }
                    else
                        canChangeStatus = false;

                    status = order.getStatus();
                    priority = order.getPriority();
                    detailedDescription = order.getDetailedDescription();
                    finishDate = order.getFinishDate() != null ?
                        DateUtils.formatJsDate(order.getFinishDate()) : null;
                    description = order.getSummary();
                    officeNumber = order.getOfficeNumber();

                    creatingUser = DSUser.safeToFirstnameLastname(order.getCreatingUser());
                    assignedUser = order.getClerk();
                    assignedDivision = DSDivision.find(order.getAssignedDivision()).getPrettyPath();
                }
                else
                {
                    currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
                }

            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
                  // walidacja daty
            if (!StringUtils.isEmpty(finishDate) && DateUtils.parseJsDate(finishDate) == null)
                addActionError("Niepoprawny termin polecenia");

            if (StringUtils.isEmpty(description))
                addActionError("Nie podano opisu polecenia");

            if (StringUtils.isEmpty(description))
                addActionError("Nie podano szczegółowej treści polecenia");

            if (hasActionErrors())
            {
            	saveFile();
                event.skip(EV_CREATE);
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            if (hasActionErrors())
                return;

            OfficeOrder order = new OfficeOrder();

            order.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
            order.setDivisionGuid(DSDivision.ROOT_GUID);
            order.setCurrentAssignmentAccepted(Boolean.FALSE);

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                order.setCreatingUser(DSApi.context().getPrincipalName());
                order.setAssignedDivision(DSDivision.ROOT_GUID);
                order.setClerk(DSApi.context().getPrincipalName());
                order.setDocumentDate(new Date());
                System.out.println("Dodajemy date do polecenia: " + new Date());

                order.setSummary(TextUtils.trimmedStringOrNull(description, 80));
                order.setDetailedDescription(TextUtils.trimmedStringOrNull(detailedDescription, 4000));
                order.setPriority(priority);
                order.setStatus(OfficeOrder.STATUS_POLECENIA_OTWARTE);
                order.setFinishDate(DateUtils.nullSafeParseJsDate(finishDate));
                if (relatedOfficeDocumentId != null)
                {
                    OfficeDocument officeDocument = OfficeDocument.findOfficeDocument(relatedOfficeDocumentId);
                    order.setRelatedOfficeDocument(officeDocument);
                }

                order.assignOfficeNumber();

                order.create();

                newDocumentId = order.getId();

                if (file != null && file.sensible())
                {
                    Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                    order.createAttachment(attachment);

                    attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                }
                addFiles(order);
                DSApi.context().commit();
           }
           catch (EdmException e)
           {
        	   log.error(e.getMessage(),e);
        	   saveFile(); 
               DSApi.context().setRollbackOnly();
               addActionError(e.getMessage());
               return;
           }
           finally
           {
               try { DSApi.close(); } catch (Exception e) { }
           }

           try
           {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                if (Configuration.officeAvailable())
                {
                    // tworzenie i uruchamianie wewnętrznego procesu workflow
                	WorkflowFactory.createNewProcess(order,"Pismo utworzone");
                	
                }
                
                DSApi.context().commit();

                if (Configuration.officeAvailable() || Configuration.faxAvailable())
                {
                    event.setResult("task-list-order");
                }
                else
                {
                    addActionMessage("Przyjęto polecenie");
                    event.setResult("simpleoffice-created");
                }
           }
           catch (EdmException e)
           {
        	    log.error(e.getMessage(),e);
        	    saveFile();
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
           }
           finally
           {
                try { DSApi.close(); } catch (Exception e) { }
           }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                OfficeOrder order = OfficeOrder.findOfficeOrder(getDocumentId());

                order.setSummary(TextUtils.trimmedStringOrNull(description, 80));
                order.setDetailedDescription(TextUtils.trimmedStringOrNull(detailedDescription, 4000));
                order.setPriority(priority);
                order.setStatus(status);
                order.setFinishDate(DateUtils.nullSafeParseJsDate(finishDate));

                if (!StringUtils.isEmpty(assignedUser))
                {
                    DSUser.findByUsername(assignedUser);
                    order.setClerk(assignedUser);
                }

                DSApi.context().commit();

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e){
                }

                DSApi.context().begin();
                TaskSnapshot.updateAllTasksByDocumentId(order.getId(), OfficeOrder.TYPE);
                DSApi.context().commit();

                event.addActionMessage(sm.getString("ZapisanoZmiany"));
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class ChangeStatus implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                OfficeOrder order = OfficeOrder.findOfficeOrder(getDocumentId());

                order.setStatus(status);

                DSApi.context().commit();

                event.addActionMessage(sm.getString("ZmienionoStatus"));
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public boolean isCanEdit()
    {
        return canEdit;
    }

    public boolean isCanChangeStatus()
    {
        return canChangeStatus;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public DSUser[] getUsers()
    {
        return users;
    }

    public Map getPriorities()
    {
        return priorities;
    }

    public Map getStatusMap()
    {
        return statusMap;
    }

    public String getPriority()
    {
        return priority;
    }

    public void setPriority(String priority)
    {
        this.priority = priority;
    }

    public String getDetailedDescription()
    {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription)
    {
        this.detailedDescription = detailedDescription;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getFinishDate()
    {
        return finishDate;
    }

    public void setFinishDate(String finishDate)
    {
        this.finishDate = finishDate;
    }

    public Long getRelatedOfficeDocumentId()
    {
        return relatedOfficeDocumentId;
    }

    public void setRelatedOfficeDocumentId(Long relatedOfficeDocumentId)
    {
        this.relatedOfficeDocumentId = relatedOfficeDocumentId;
    }

    public String getAssignedUser()
    {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }
}
