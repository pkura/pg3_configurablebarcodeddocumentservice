package pl.compan.docusafe.web.office;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.parametrization.utp.NormalLogic;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import net.fortuna.ical4j.model.DateTime;

import com.google.common.collect.Sets;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Tworzenie nowej wersji dokumentu po zapisaniu.
 *
 */
public class CloneNewVersion implements ActionListener
{
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(
			CloneNewVersion.class.getPackage().getName(), null);
	private static final Logger log = LoggerFactory.getLogger(CloneNewVersion.class);

	private long stareGuid;
	private int staraWersja;
	private Map copyAttachments = new HashMap();
	private Long folderId;
	private String documentKindCn;
	private Date ctime;
	private Date cDekret;
	private Date newCtime;

	public void actionPerformed(ActionEvent event) {

	}
	public void clone(Long documentId, ActionEvent event, Map<String, Object> values, String nowyOpis) throws EdmException
	{
		try{

			DSApi.context().begin();
			OfficeDocument document = OfficeDocument.find(documentId);
			HttpServletRequest request = ServletActionContext.getRequest();
			Sender tmpSender = new Sender();
			long tmpSenderId = 0;
			List<Audit> listaAudit = new ArrayList<Audit>();
			List<AssignmentHistoryEntry> listaAHE=new ArrayList<AssignmentHistoryEntry>(); 
			if (document instanceof OfficeDocument) {
				if (((OfficeDocument) document).getSenderId() != null) {
					tmpSenderId = ((OfficeDocument) document).getSenderId();
					Person person = Person.find(tmpSenderId);
					tmpSender.fromMap(person.toMap());
					listaAudit=document.getWorkHistory();
					listaAHE=((OfficeDocument) document).getAssignmentHistory();
				}
			}
			stareGuid = document.getVersionGuid();
			staraWersja = document.getVersion();

//			Map<String, String> rob = request.getParameterMap();
			String name = DSApi.context().getDSUser().getName();
			DocumentKind documentKind = document.getDocumentKind();
			if (documentKindCn == null){
				documentKind = document.getDocumentKind();
				documentKindCn = documentKind.getCn();
			} else {
				documentKind = DocumentKind.findByCn(documentKindCn);
			}
			if (AvailabilityManager.isAvailable("p4CloneDocument"))
				values = RequestLoader.loadKeysFromWebWorkRequest();

			// chce utworzyc nowa wersje a nie ma uprawnien
			if (!DSApi.context().hasPermission(DSPermission.WERSJONOWANIE_UTWORZ_NOWA_WERSJE)) {
				throw new EdmException("Brak uprawnie� do tworzenia nowej wersji");
			} else {
				documentKind.initialize();
				documentKind.logic().correctValues(values, documentKind);

				List<Long> attachmentsToClone = new ArrayList<Long>(
						copyAttachments.size());
				List<Long> attachmentsToDelAfter = new ArrayList<Long>();
				for (Iterator iter = copyAttachments.entrySet()
						.iterator(); iter.hasNext();) {
					Map.Entry entry = (Map.Entry) iter.next();
					Long attachmentId = new Long(entry.getKey()
							.toString());
					String copyOrMove = HttpUtils.valueOrNull(entry
							.getValue());
					if ("copy".equals(copyOrMove)
							|| "move".equals(copyOrMove))
						attachmentsToClone.add(attachmentId);
					if ("move".equals(copyOrMove))
						attachmentsToDelAfter.add(attachmentId);
				}

				Document newDocument = document
						.cloneObject((Long[]) attachmentsToClone
								.toArray(new Long[attachmentsToClone
								                  .size()]));
				newDocument.setUnsafeCtime(new Date());

				// wyrzucenie przeniesionych zalacznikow
				for (Long id : attachmentsToDelAfter)
					document.scrubAttachment(id);

				/* zapisywanie danych biznesowych */
				boolean dockindChanged = !document.getDocumentKind().getCn().equals(documentKindCn);
				document.setDocumentKind(documentKind);
				if (dockindChanged) {
					documentKind.setWithHistory(newDocument.getId(), values,/* changeAll */true);
				} else {
					documentKind.setWithHistory(newDocument.getId(), values,/* changeAll */false);
				}

				documentKind.logic().archiveActions(newDocument,
						DocumentKindsManager.getType(newDocument));

				documentKind.logic().documentPermissions(newDocument);

				/*
				 * na wypadek gdyby oryginalny dokument by� podpisany
				 * podpisem elektronicznym
				 */
				newDocument.setBlocked(false);

				// ustawianie wersji

				OfficeDocument doc = OfficeDocument.find(documentId);
				List<OfficeDocument> docActive = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
				for (OfficeDocument od : docActive) {
					od.setCzyAktualny(false);
				}
				newDocument.setCzyAktualny(true);
				newDocument.setCzyCzystopis(false);

				if (document instanceof OfficeDocument
						&& ((OfficeDocument) document).getSenderId() != null) {

					tmpSender.setDocumentId(document.getId());
					tmpSender.setDictionaryGuid("rootdivision");
					tmpSender.setDictionaryType("sender");

					// ustawienie pol sendera dla in
					if (newDocument instanceof InOfficeDocument) {

						Person p = Person
								.find((long) ((OfficeDocument) newDocument)
										.getSenderId());
						p.setDocumentId(newDocument.getId());
						p.setDictionaryGuid("rootdivision");
						p.setDictionaryType("sender");
						//ustawienie dla recipient�w document_id
						List<Recipient> lista = ((OfficeDocument)newDocument).getRecipients();
						Iterator it = lista.iterator();
						while (it.hasNext()) {
							Recipient r = (Recipient) it.next();
							r.setDocumentId(newDocument.getId());
							r.setDocument(newDocument);
						}

					}
					// swtorzenie nowego dla out
					if (document instanceof OutOfficeDocument) {
						Person p = Person
								.find((long) ((OfficeDocument) document)
										.getSenderId());
						p.setDocumentId(newDocument.getId());
						p.setDictionaryType("sender");

						Sender tmpSender2 = new Sender();

						if (document instanceof OfficeDocument) {
							tmpSenderId = ((OfficeDocument) document)
									.getSenderId();
							Person person = Person.find(tmpSenderId);
							tmpSender2.fromMap(person.toMap());

						}
						tmpSender2.setDictionaryGuid(null);
						tmpSender2.setDocument(newDocument);
						tmpSender2.create();
						((OfficeDocument) newDocument)
						.setSender(tmpSender2);
						((OfficeDocument) newDocument)
						.setSenderId(tmpSender2.getId());
					}

				}

				OfficeDocument newDoc = OfficeDocument.find(newDocument.getId());

				String sql = "select MAX(version)as LastVersion from ds_document where version_guid="
						+ stareGuid;
				PreparedStatement ps = DSApi.context()
						.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				rs.next();

				newDocument
				.setVersion(rs.getInt("LastVersion") + 1);
				newDocument.setVersionGuid(stareGuid);
				newDocument.setVersionDesc(nowyOpis);

				if (document instanceof OfficeDocument) {
					listaAudit.add(Audit.create(
							"newVersion",
							DSApi.context()
							.getPrincipalName(),
							sm.getString("UtworzonoNowaWersje")));
					listaAudit
					.add(Audit.create(
							"Active",
							DSApi.context().getPrincipalName(),
							sm.getString("WersjaNieAktywna")
							+ " "
							+ document
							.getVersion()));
					listaAudit
					.add(Audit.create("Active",
							DSApi.context().getPrincipalName(),
							sm.getString("WersjaAktywna") + " "
									+ newDocument.getVersion()));

					newDocument.setCzyAktualny(true);
					newDocument.setCzyCzystopis(false);

				}
				if (newDocument instanceof InOfficeDocument)
					((InOfficeDocument) newDocument)
					.setSummary(newDocument
							.getDescription());
				if (newDocument instanceof OutOfficeDocument)
					((OutOfficeDocument) newDocument)
					.setSummary(newDocument
							.getDescription());

				//w set ja czysci wiec znow uzupelniam(wczesniej zapisana)
				document.setWorkHistory(listaAudit);
				folderId = newDocument.getFolderId();

				// klonowanie historii pisma
				newDocument.setWorkHistory(document.getWorkHistory());
				//int i=0;
				for (Audit audit : document.getWorkHistory()) {
					if (audit.getProperty() != null){
						((OfficeDocument) newDocument)
						.addWorkHistoryEntry(audit);
					}
				}

				if (newDocument instanceof OfficeDocument) {
					//zamkniecie procesu dla starego dokumentu
					List<OfficeDocument> guid = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
					for(OfficeDocument od:guid){
						if(JBPMTaskSnapshot.findByDocumentId(od.getId()) != null){
							List<JBPMTaskSnapshot> taskId = JBPMTaskSnapshot.findByDocumentId(od.getId());
							if(taskId.size() != 0){
								if(taskId.size()>1){
									for(int i=0;i<taskId.size()-1;i++){
										WorkflowFactory wf = WorkflowFactory.getInstance();
										Set<Long> docIds = Sets.newLinkedHashSet();
										docIds.add(taskId.get(i).getDocumentId());
										if(wf instanceof InternalWorkflowFactory){
											for (Long docId : docIds) {
												String activityId = wf.findManualTask(docId);
												wf.manualFinish(activityId, false);
											}
										} else if(wf instanceof Jbpm4WorkflowFactory) {
											for (Long docId : docIds) {
												if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
													String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
													wf.manualFinish(firstActivityId, false);
												}
											}
										} else {
											throw new IllegalStateException("nieznany WorkflowFactory");
										}
									}
								} else {
									for(int i=0;i<taskId.size();i++){
										WorkflowFactory wf = WorkflowFactory.getInstance();
										Set<Long> docIds = Sets.newLinkedHashSet();
										docIds.add(taskId.get(i).getDocumentId());
										if(wf instanceof InternalWorkflowFactory){
											for (Long docId : docIds) {
												String activityId = wf.findManualTask(docId);
												wf.manualFinish(activityId, false);
											}
										} else if(wf instanceof Jbpm4WorkflowFactory) {
											for (Long docId : docIds) {
												if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
													String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
													wf.manualFinish(firstActivityId, false);
												}
											}
										} else {
											throw new IllegalStateException("nieznany WorkflowFactory");
										}
									}
								}
							}
						}
					}

					//utworzenie procesu dla nowego dokumentu
					newDocument.getDocumentKind().logic().onStartProcess(
							(OfficeDocument) newDocument,event);
					TaskSnapshot.updateByDocumentId(newDocument.getId(),
							((OfficeDocument) newDocument).getStringType());
				}


				Map<Long,String> labe = LabelsManager.getLabelBeanByDocumentId(documentId, "label");
				if(!labe.isEmpty()){
					for(Map.Entry<Long, String> l:labe.entrySet()){
						LabelsManager.addLabel(newDocument.getId(), l.getKey(), DSApi.context().getPrincipalName());
					}
				}
				FieldsManager fm = newDocument.getFieldsManager();
				if(fm.getValue("WYSLANO") != null){
					newDocument.setCzyCzystopis(true);
				}

				sql = "delete from dso_document_asgn_history where document_id="
						+ document.getId();
				ps = DSApi.context()
						.prepareStatement(sql);
				ps.executeUpdate();

				((OfficeDocument) document)
				.setAssignmentHistory(listaAHE);

				DSApi.context().commit();

				// sklonowanie uprawnie� dla nowego dokumentu
				// uprawnienia dla grupy
				newDocument.getDocumentKind().logic()
				.documentPermissions(newDocument);
				DSApi.context().session().save(newDocument);
				// uprawniania dla autora
				java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

				DSUser author = DSApi.context().getDSUser();
				String user = author.getName();
				String fullName = author.getLastname() + " "
						+ author.getFirstname();

				perms.add(new PermissionBean(ObjectPermission.READ,
						user, ObjectPermission.USER, fullName + " ("
								+ user + ")"));
				perms.add(new PermissionBean(
						ObjectPermission.READ_ATTACHMENTS, user,
						ObjectPermission.USER, fullName + " (" + user
						+ ")"));
				perms.add(new PermissionBean(ObjectPermission.MODIFY,
						user, ObjectPermission.USER, fullName + " ("
								+ user + ")"));
				perms.add(new PermissionBean(
						ObjectPermission.MODIFY_ATTACHMENTS, user,
						ObjectPermission.USER, fullName + " (" + user
						+ ")"));
				perms.add(new PermissionBean(ObjectPermission.DELETE,
						user, ObjectPermission.USER, fullName + " ("
								+ user + ")"));

				Set<PermissionBean> documentPermissions = DSApi
						.context().getDocumentPermissions(newDocument);
				perms.addAll(documentPermissions);
				((AbstractDocumentLogic) newDocument.getDocumentKind()
						.logic()).setUpPermission(newDocument, perms);

				if (newDocument instanceof OfficeDocument) {
					// flush i tak robi sie w
					// updateDSApi.context().session().flush();
					DSApi.context().begin();
					/*TaskSnapshot.updateByDocumentId(
							newDocument.getId(),
							((OfficeDocument) newDocument)
							.getStringType());*/

				}

				if (AvailabilityManager.isAvailable("p4CloneDocument"))
					event.setResult("task-list");
				else
					event.setResult("task-list");

				/*
				 * Aktualizacja pisma w glownym dzienniku
				 */
				java.util.Date date = new Date();
				java.sql.Date journalDate = new java.sql.Date(date.getTime());
				List<JournalEntry> je = JournalEntry.findByDocumentId(doc.getId());
				for(JournalEntry j:je){
					ps = DSApi
							.context()
							.prepareStatement(
									"INSERT INTO DSO_JOURNAL_ENTRY(DOCUMENTID, SEQUENCEID, CTIME, entrydate, JOURNAL_ID, dailySequenceId, author) values(?,?,?,?,?,?,?)");
					ps.setLong(1, newDoc.getId());
					ps.setInt(2, j.getSequenceId());
					ps.setTimestamp(3, new java.sql.Timestamp(j.getCtime().getTime()));
					ps.setTimestamp(4, new java.sql.Timestamp(j.getEntryDate().getTime()));
					ps.setLong(5, j.getJournal().getId());
					ps.setInt(6, j.getDailySequenceId());
					ps.setString(7, author.getName());
					ps.executeUpdate();

				}
				List<AssignmentHistoryEntry>  hist = doc.getAssignmentHistory();
				for(AssignmentHistoryEntry h : hist){
					if(h.getType().value == 100 || h.getType().value == 115 || h.getType().value == 110 || h.getType().value == 140){
						cDekret = h.getCtime();
					}
				}
					
				//ustawienie czasu otrzymania z pierwszej wersji dokumentu	
				List<OfficeDocument> guid = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
				for(OfficeDocument d:guid){
					if(d.getVersion() == 1){
						ctime = d.getCtime();
						if(cDekret != null){
							if(cDekret.after(ctime)){
								newCtime = cDekret;
							} else {
								newCtime = ctime;
							}
						} else {
							newCtime = ctime;
						}
					}
				}
				ps = DSApi
						.context()
						.prepareStatement(
								"UPDATE DS_DOCUMENT SET CTIME=? WHERE id=?");
				ps.setTimestamp(1, new java.sql.Timestamp(ctime.getTime()));
				ps.setLong(2, newDoc.getId());

				ps.executeUpdate();
				newDoc.setOfficeNumber(doc.getOfficeNumber());
				List<DSUser> dsUsers = doc.getDocumentKind().logic().getUserWithAccessToDocument(newDocument.getId());
				for (DSUser dsUser : dsUsers) {
					DocumentMailHandler.createEvent(newDocument.getId(), NormalLogic.MAILNEWVERSION, dsUser.getEmail());
				}
				//ustawienie daty otrzymania i rejestracji pisma z poprzedniego dokumentu
				List<JBPMTaskSnapshot> task =  JBPMTaskSnapshot.findByDocumentId(newDoc.getId());
				for(JBPMTaskSnapshot t: task){
					t.setDocumentCtimeAndCdate(ctime);
					t.setReceiveDate(newCtime);
				}
				DSApi.context().commit();
				uaktualnijSprawy(documentId, newDocument.getId());
			}

		} catch (EdmException e) {
			DSApi.context().setRollbackOnly();
			log.error("", e);
		} catch (SQLException e) {
			DSApi.context().setRollbackOnly();
			log.error("", e);
		}
	}



	// uaktualnienie dokumentow przypisanych do spraw
	//(dla utp)uaktualnienie bazy powiazanych dokumentow(usuniecie wszystkich dokumentow powiazanych dla dokumentu starego)
	private static void uaktualnijSprawy(Long stareId, Long noweId) {
		try {
			DSApi.context().begin();
			Long caseId = null;
			String caseDocumentId = null;
			String sql = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
			if (Document.find(stareId) instanceof InOfficeDocument) {
				sql = "select case_id,casedocumentid from dso_in_document where id = "
						+ stareId.toString();
				ps = DSApi.context().prepareStatement(sql);
				rs = ps.executeQuery();
				if (rs.next()) {
					caseId = rs.getLong("case_id");
					caseDocumentId = rs.getString("casedocumentid");
				}

				// tabela in

				sql = "update dso_in_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
						+ stareId.toString();
				ps = DSApi.context().prepareStatement(sql);
				ps.executeUpdate();

				// jesli dokument jest przypisany do sprawy
				if (caseId != 0) {
					sql = "update dso_in_document set case_id = "
							+ caseId.toString() + ", casedocumentid='"
							+ caseDocumentId + "' where id = "
							+ noweId.toString();
				} else {
					sql = "update dso_in_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
							+ noweId.toString();
				}
				ps = DSApi.context().prepareStatement(sql);
				ps.executeUpdate();

			} else if (Document.find(stareId) instanceof OutOfficeDocument) {

				sql = "select case_id,casedocumentid from dso_out_document where id = "
						+ stareId.toString();
				ps = DSApi.context().prepareStatement(sql);
				rs = ps.executeQuery();
				if (rs.next()) {
					caseId = rs.getLong("case_id");
					caseDocumentId = rs.getString("casedocumentid");
				}

				// tabela out

				sql = "update dso_out_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
						+ stareId.toString();
				ps = DSApi.context().prepareStatement(sql);

				ps.executeUpdate();
				// jesli dokument jest przypisany do sprawy
				if (caseId != 0) {
					sql = "update dso_out_document set case_id = "
							+ caseId.toString() + ", casedocumentid='"
							+ caseDocumentId + "' where id = "
							+ noweId.toString();
				} else {

					sql = "update dso_out_document set case_id = NULL,CASEDOCUMENTID=NULL where id = "
							+ noweId.toString();
				}
				ps = DSApi.context().prepareStatement(sql);
				ps.executeUpdate();

			}

			//					  sql = "update DSW_JBPM_TASKLIST set  document_id= " +
			//					  noweId.toString() + " where document_id=" +
			//					  stareId.toString(); ps =
			//					  DSApi.context().prepareStatement(sql); 
			//					  ps.executeUpdate();
			String tabela_multi = Document.find(noweId).getDocumentKind().getMultipleTableName();
			if(AvailabilityManager.isAvailable("utp.aktualizujDokumentyPowiazanePrzyZmianieWersji")){
				if(noweId!=stareId){
					sql = "update " + tabela_multi + " set document_id="+noweId +"where (document_id="+stareId.toString()+") and field_cn='LINKS'";
					ps=DSApi.context().prepareStatement(sql); 
					ps.executeUpdate();
					sql = "update " + tabela_multi + " set field_val="+noweId +"where (field_val="+stareId.toString()+") and field_cn='LINKS'";
					ps=DSApi.context().prepareStatement(sql); 
					ps.executeUpdate();
					ps.close();
				}
			}
			else if(AvailabilityManager.isAvailable("utp.usuwajDokumentyPowiazanePrzyZmianieWersji")){
				if(noweId!=stareId){
					sql = "delete from " + tabela_multi + " where (document_id="+stareId.toString()+" or field_val="+stareId.toString()+") and field_cn='LINKS'";
					ps=DSApi.context().prepareStatement(sql); 
					ps.executeUpdate();
					ps.close();
				}
			}
			ps.close();
			DSApi.context().commit();
		} catch (EdmException e) {
			DSApi.context().setRollbackOnly();
			log.error("", e);
		}

		catch (SQLException e) {
			DSApi.context().setRollbackOnly();
			log.error("", e);
		}
	}
}



