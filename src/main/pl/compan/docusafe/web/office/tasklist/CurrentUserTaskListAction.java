package pl.compan.docusafe.web.office.tasklist;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class CurrentUserTaskListAction extends EventActionSupport 
{

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	String currentList = (String)ServletActionContext.getRequest().getSession().getAttribute("tasksCurrentList");
            	if(StringUtils.isNotEmpty(currentList) && currentList.length() > 3)
            		event.setResult(currentList);
            	else if (AvailabilityManager.isAvailable("bookmarks.on"))
            		event.setResult("bookmarks"); 
            	else
            		event.setResult("user-task-list");
            }
            catch (Exception e)
            {
                event.setResult("user-task-list");
            }
        }
    }
}
