package pl.compan.docusafe.web.office.tasklist;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;

public class TaskListCountAction extends EventActionSupport {
	private final static Logger LOG = LoggerFactory.getLogger(TaskListCountAction.class) ;
	
	private String documentType;
	private String username;	
	private Long bookmarkId;

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION).
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new FillForm()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
	private class FillForm extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {

			try {
//                log.info("TaskListCountAction");

				String user;
				if(username == null || username == "")
					user = DSApi.context().getPrincipalName();
				else
					user = DSUser.findByUsername(username).getName();

				TaskListCounterManager manager = new TaskListCounterManager(false);
				int notAcceptedCount = manager.getTaskListCount(user, documentType,bookmarkId);
				if(log.isDebugEnabled())
					log.debug("Liczba niezaakceptowanych zada� (document_type = " + documentType + 
							") u�ytkownika " + user + " = " + notAcceptedCount);
				
				HttpServletResponse resp = ServletActionContext.getResponse();
				resp.setContentType("text/html; charset=iso-8859-2");
				PrintWriter out = resp.getWriter();
				out.write(notAcceptedCount + "");
				out.flush();
				out.close();
			} catch(Exception e) {
				log.error(e.getMessage());
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getBookmarkId() {
		return bookmarkId;
	}

	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}
	
}
