package pl.compan.docusafe.web.office.tasklist.calendar;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.calendar.sql.RejectionReason;
import pl.compan.docusafe.core.calendar.sql.TimeImpl;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Uwaga - data przy komunikacji z javascriptiem jest przekazywana w formacie UnixStamp (= liczba_milisekund_od_1970 / 1000)
 * dlatego przy zapisie obiektu DSTime mno�ymy * 1000
 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
 * @see #start
 * @see #end
 */
@SuppressWarnings("serial")
public class CalendarAjaxAction extends EventActionSupport {
	private final static Logger LOG = LoggerFactory.getLogger(CalendarAjaxAction.class);
	private final static String FAILED = "FAILED";
	private final static String JSON_FAILED = "JSON_FAILED";
	private final static String EVENT_STATUS_CHANGED = "EVENT_STATUS_CHANGED";
	private final static String EVENT_UPDATED = "EVENT_UPDATED";
	private final static String EVENT_REMOVED = "EVENT_REMOVED";
	private final static String GET_CALENDARS_FAILED = "GET_CALENDARS_FAILED";
	private final static String RESOURCE_BUSY = "RESOURCE_BUSY";
	private final static String RESOURCE_FREE = "RESOURCE_FREE";
	private final static String USER_FREE = "USER_FREE";
	private final static String USER_BUSY = "USER_BUSY";
	private final static String CALENDAR_COLOR_CHANGED = "CALENDAR_COLOR_CHANGED";
	private final static String CALENDAR_VISIBILITY_CHANGED = "CALENDAR_VISIBILITY_CHANGED";

	private final static String FAILED_PL = "NIE_POWIOD�O_SI�";
	private final static String JSON_FAILED_PL = "JSON_NIE_POWIOD�O_SI�";
	private final static String EVENT_STATUS_CHANGED_PL = "ZMIANA_STATUSU_ZDARZENIA";
	private final static String EVENT_UPDATED_PL = "ZDARZENIE_ZAKTUALIZOWANE";
	private final static String EVENT_REMOVED_PL = "ZDARZENIE_USUNIETE";
	private final static String GET_CALENDARS_FAILED_PL = "POBIERANIE_KALENDARZY_ZAKONCZONE_PORAZKA";
	private final static String RESOURCE_BUSY_PL = "ZASOB_ZAJETY";
	private final static String RESOURCE_FREE_PL = "ZASOB_WOLNY";
	private final static String USER_FREE_PL = "UZYTKOWNIK_WOLNY";
	private final static String USER_BUSY_PL = "UZYTKOWNIK_ZAJETY";
	private final static String CALENDAR_COLOR_CHANGED_PL = "ZMIENIONO_KOLOR_KALENDARZA";
	private final static String CALENDAR_VISIBILITY_CHANGED_PL = "ZMIENIONO_WIDOCZNOSC_KALENDARZA";
	private Logger kl = LoggerFactory.getLogger("kamil");

	private Long calendarId;

	private List<Calendar> calendars;

	/**
	 * Data w formacie UnixStamp
	 */
	private long start;
	/**
	 * Data w formacie UnixStamp
	 */
	private long end;
	/**
	 * String zawieraj�cy obiekt {@link JSEvent} w formacie JSON
	 */
	private String eventJson;
	/**
	 * Id eventu u�ywane przez akcje: {@link RemoveEvent}, {@link ChangeEventStatus}, {@link CheckResourceAvailability}
	 */
	private long eventId;
	/**
	 * Status eventu.
	 * <ul>
	 * <li><code>true</code> - zaakceptowany
	 * <li><code>false</code> - odrzucony
	 * </ul>
	 * U�ywane przez akcj� {@link ChangeEventStatus}
	 */
	private boolean eventStatus;
	public String type;
	/**
	 * String zawieraj�cy list� <code>long</code> z calendarId w formacie JSON
	 */
	private String calendarsId;
	/**
	 * String zawieraj�cy list� <code>string</code> z codename u�ytkownik�w w formacie JSON <br />
	 * U�ywane przez akcj� {@link CheckUserAvailability}
	 */
	private String usersCn;
	/**
	 * Id koloru kalendarza. <br />
	 * U�ywane przez akcj� {@link SetCalendarColor}
	 */
	private int colorId;
	/**
	 * Komentarz do zmiany statusu eventu: np.: pow�d odrzucenia. <br />
	 * U�ywane przez akcj� {@link ChangeEventStatus}
	 */
	private String eventStatusDesc;

	/**
	 * Lista event�w, kt�re musz� by� od�wie�one. <code>calendarId -> Set<JSEvent></code>
	 */
	private static Map<Long,Set<JSEvent>> obsoleteEvents;
	/**
	 * Lista zawierajaca czasu ostatniej modyfikacji calendarzy
	 */
	private static Map<Long,Date> calendarsModifyTime;
	
	/**
	 * Ustawienie widoczno�ci kalendarza. Je�li <code>true</code>, to kalendarz widoczny (aktywny). <br />
	 * U�ywane przez akcj�: {@link SetCalendarVisibility}
	 */
	private boolean calendarVisible;
	/**
	 * Ustawienie <code>true</code> powoduje od��czenie zdarzenie cyklicznego z grupy
	 */
	private boolean periodicDetached = false;

	private long lastRefresh;
	
	public CalendarAjaxAction() {
		super();
		if(obsoleteEvents == null) {
			obsoleteEvents = new HashMap<Long, Set<JSEvent>>();
		}
		if(calendarsModifyTime == null) {
			calendarsModifyTime = new HashMap<Long, Date>();
		}
	}

	@Override
	protected void setup() {

		registerListener(DEFAULT_ACTION).
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new FillForm()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("updateEvent").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new UpdateEvent()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("createEvent").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new CreateEvent()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("changeEventStatus").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new ChangeEventStatus()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("removeEvent").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new RemoveEvent()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("getObsoleteEvents").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new GetObsoleteEvents()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("checkResourceAvailability").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new CheckResourceAvailability()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("checkUserAvailability").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new CheckUserAvailability()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("setCalendarColor").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new SetCalendarColor()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("clearMonitorEvents").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new ClearMonitorEvents()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("getResources").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new GetResources()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("getUsers").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new GetUsers()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("getCalendars").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new GetCalendars()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("setCalendarVisibility").
			append(OPEN_HIBERNATE_AND_JBPM_SESSION).
			append(new SetCalendarVisibility()).
			appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
    
	private PrintWriter getOutPrintWriter() throws IOException {
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html; charset=ISO-8859-2");
		return resp.getWriter();
	}

	/**
	 * Zwraca wszystkie kalendarze dla bie��cego u�ytkownika <br />
	 * w formacie JSON (lista obiekt�w {@link JSCalendar})
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class GetCalendars extends LoggedActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			List<UserCalendarBinder> ucb = UserCalendarBinder.getUserCalendars(DSApi.context().getPrincipalName(),true );
			UserCalendarBinder.initCalendars(ucb);
			PrintWriter out = getOutPrintWriter();

			try {
				Gson gson = new Gson();
				out.write(gson.toJson(JSCalendar.toJSCalendars(ucb)));
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(GET_CALENDARS_FAILED);
				else out.write(GET_CALENDARS_FAILED_PL);
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	/**
	 * Zwraca wszystkie eventy z kalendarza o id r�wnym {@link CalendarAjaxAction#calendarId} <br/>
	 * od daty {@link CalendarAjaxAction#start} do daty {@link CalendarAjaxAction#end} w formacie JSON (lista obiekt�w {@link JSEvent}).<br/>
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			log.debug("FillForm {}", System.currentTimeMillis());
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			List<JSEvent> events = new ArrayList<JSEvent>();
			Calendar calendar = DSApi.context().load(Calendar.class, calendarId);

			kl.debug("events for calendar #{}", calendarId);
			try
			{
				Date startDate = new Date(start*1000);
				Date stopDate = new Date(end*1000);
				for (CalendarEvent calendarEvent : calendar.getEvents(startDate,stopDate))
				{
					try
					{
						TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
						if(kl.isDebugEnabled()) {
							kl.debug("calendar events for TimeImpl #{}", time.getId());
							for(CalendarEvent timeEvent : time.getCalendarEvents()) {
								kl.debug("event {} {}", timeEvent.getId(), timeEvent.isConfirm());
							}
						}
						JSEvent ev = new JSEvent(calendarEvent,time);
						events.add(ev);
						if(kl.isDebugEnabled()) {
							kl.debug("Added JSEvent {} {}", ev.getId(), ev.getResources());
						}
					}
					catch (Exception e) {
						LOG.error(e.getMessage(),e);
					}
				}
				kl.debug("getEvents for cal #" + calendarId + ": " + events);
				kl.debug("===========");
				out.write(gson.toJson(events));
			} catch (Exception e) {
				LOG.error(e.getMessage(),e);
			} finally {
				out.flush();
				out.close();
			}
			log.debug("FillForm "+ new Date().getTime());
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}

	/**
	 * Uaktualnia event na podstawie JSONa {@link CalendarAjaxAction#eventJson}. <br/>
	 * Dodaje eventy do {@link CalendarAjaxAction#obsoleteEvents}
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class UpdateEvent extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)throws Exception 
		{
			log.debug("UpdateEvent {}", System.currentTimeMillis());
			Gson gson = new Gson();
			PrintWriter out = getOutPrintWriter();
			try 
			{
				JSEvent evt = gson.fromJson(eventJson, JSEvent.class);
				evt.convertIsoToUtf();
				DSApi.context().begin();
				CalendarEvent calendarEvent = DSApi.context().load(CalendarEvent.class, evt.id);
				TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
				updateObsoleteEvents(time.getCalendarEvents(),evt);	
				kl.debug("obsolete events before: {}",obsoleteEvents);
				time.setCalendarEvent(new ArrayList(evt.getResources()),
						new Date(evt.start*1000),new Date(evt.end*1000),evt.desc,evt.place,evt.allDay,evt.getOwner(), 
						evt.periodic, evt.periodKind, evt.periodLength, new Date(evt.periodEnd*1000), evt.id,
						periodicDetached);
				DSApi.context().commit();
				updateObsoleteEvents(time.getCalendarEvents(),evt);	
				kl.debug("obsolete events after: {}",obsoleteEvents);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(EVENT_UPDATED);
				else out.write(EVENT_UPDATED_PL);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(FAILED);
				else out.write(FAILED_PL);
				e.printStackTrace();
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	private void updateObsoleteEvents(Set<CalendarEvent> calendarEvents, JSEvent evt) throws EdmException
	{
		Logger kl = LoggerFactory.getLogger("kamil");
		for (CalendarEvent calEvent : calendarEvents) 
		{
			kl.debug("ObsoleteEvent {} is removed {}", calEvent.getId(), calEvent.isRemoved());
			/**Zmieniamy id poniewaz w kazdym kalendarzu jest inny event obrazujacy to samo wydarzenie*/
			JSEvent clonedEvent = new JSEvent(evt);
			clonedEvent.updateValue(calEvent);
			Long calendarId = calEvent.getCalendarId();
			calendarsModifyTime.put(calendarId, new Date());
			if(obsoleteEvents.get(calendarId) == null)
				obsoleteEvents.put(calendarId, new HashSet<JSEvent>());
			obsoleteEvents.get(calendarId).remove(clonedEvent);
			if(!calEvent.isRemoved())
				obsoleteEvents.get(calendarId).add(clonedEvent);
		}
	}
	
	/**
	 * Tworzy nowy event na podstawie JSONa {@link CalendarAjaxAction#eventJson eventJson}.<br/>
	 * Dodaje eventy do {@link CalendarAjaxAction#obsoleteEvents}
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class CreateEvent extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			log.info("CreateEvent {}",System.currentTimeMillis());
			PrintWriter out = getOutPrintWriter();
			try 
			{
				Gson gson = new Gson();
				JSEvent evt = gson.fromJson(eventJson, JSEvent.class);
				evt.convertIsoToUtf();			
				DSApi.context().begin();
				
				TimeImpl time = new TimeImpl();
				time.setCreator(DSApi.context().getDSUser());
				time.create();
				evt.id = time.setCalendarEvent(new ArrayList<JSResource>(evt.getResources()),new Date(evt.start*1000),new Date(evt.end*1000),evt.desc,evt.place,evt.allDay,evt.getOwner(), 
						evt.periodic, evt.periodKind, evt.periodLength, new Date(evt.periodEnd*1000), null, false);
				DSApi.context().commit();
				evt.title = StringUtils.left(evt.desc,JSEvent.TITLE_LENGTH);
				out.write(gson.toJson(evt));
				updateObsoleteEvents(time.getCalendarEvents(),evt);	
			} catch (Exception e) {
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(FAILED);
				else out.write(FAILED_PL);
				log.error(e.getMessage(), e);
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	/**
	 * Zmienia status eventu {@link CalendarAjaxAction#eventId} <br />
	 * na {@link CalendarAjaxAction#eventStatus} z komentarzem {@link CalendarAjaxAction#eventStatusDesc}<br />
	 * Zwraca {@link CalendarAjaxAction#EVENT_STATUS_CHANGED}, je�li status eventu
	 * zosta� zmieniony.
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class ChangeEventStatus extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			log.debug("ChangeEventStatus {}", System.currentTimeMillis());
			try 
			{
				DSApi.context().begin();
				CalendarEvent calendarEvent = DSApi.context().load(CalendarEvent.class, eventId);
				List<CalendarEvent> events = new ArrayList<CalendarEvent>();
				events.add(calendarEvent);
				
				if(calendarEvent.isPeriodic() && !periodicDetached) {
					events = calendarEvent.getFutureEventsInclusive();
				}
				
				TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
				
				RejectionReason rr = new RejectionReason();
				if(eventStatusDesc != null)
					rr.setReason(TextUtils.isoToUtf(eventStatusDesc));
				rr.setTimeId(time.getId());
				rr.setUsername(DSApi.context().getPrincipalName());
				if(!eventStatus) {
					rr.create();
					time.addRejectionReasons(rr);
				} else {
					time.removeRejectionReason(rr);
				}
				
				for(CalendarEvent evt : events) {
					evt.setConfirm(eventStatus);
					JSEvent ev = new JSEvent(evt,time);
					updateObsoleteEvents(time.getCalendarEvents(),ev);
				}
				
				DSApi.context().commit();
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(EVENT_STATUS_CHANGED);
				else out.write(EVENT_STATUS_CHANGED_PL);
			} catch(Exception e) {
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(FAILED);
				else out.write(FAILED_PL);
				log.error(e.getMessage(), e);
			} finally {
				out.close();
				out.flush();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}

	/**
	 * Usuwa event o id r�wnym {@link CalendarAjaxAction#eventId} <br />
	 * z komentarzem {@link CalendarAjaxAction#eventStatusDesc}
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class RemoveEvent extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
				try {
					DSApi.context().begin();
					
					CalendarEvent calendarEvent = DSApi.context().load(CalendarEvent.class, eventId);
					TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
					if(time.getCreator().getName().equals(calendarEvent.getCalendar().getOwner()))
					{
						DSApi.context().session().refresh(time);
						
						for (CalendarEvent ce : time.getCalendarEvents())
						{
							if(calendarEvent.isPeriodic()) {
								if(periodicDetached) {
									// usuwamy tylko jeden event cykliczny
									if(ce.equals(calendarEvent)) {
										ce.delete();
										JSEvent ev2 = new JSEvent(ce,time);
										//ev2.getResources().clear();
										ce.setRemoved(true);
										updateObsoleteEvents(time.getCalendarEvents(),ev2);
										break;
									}
								} else {
									// usuwamy wszystkie zdarzenia cykliczne w przysz�o�ci
									if(ce.getStartTime().compareTo(calendarEvent.getStartTime()) >= 0) {
										ce.delete();
										JSEvent ev2 = new JSEvent(ce,time);
										//ev2.getResources().clear();
										ce.setRemoved(true);
										updateObsoleteEvents(time.getCalendarEvents(),ev2);
									}
								}
							} else {
								// usuwamy jeden zwyk�y (nie cykliczny) event
								ce.delete();
								JSEvent ev2 = new JSEvent(ce,time);
								ev2.getResources().clear();
								updateObsoleteEvents(time.getCalendarEvents(),ev2);
							}
						}
					}
					else
					{
						JSEvent ev = new JSEvent(calendarEvent,time);
						// usuwamy z listy zasb�w u�ytkownika, kt�ry usun�� event
						for(JSResource jsres : ev.getResources()) {
							if(jsres.getCn().equals(DSApi.context().getPrincipalName())) {
								kl.debug("Removed user {}", jsres.getCn());
								ev.getResources().remove(jsres);
								break;
							}
						}
						if(calendarEvent.isPeriodic()) {
							if(periodicDetached) {
								// usuwamy tylko jedno zdarzenie z listy zdarze� cyklicznych
								if(eventStatusDesc != null) {
									RejectionReason rr = new RejectionReason();
									rr.setReason(eventStatusDesc);
									rr.setUsername(DSApi.context().getPrincipalName());
									rr.setTimeId(time.getId());
									rr.setPeriodIndex(calendarEvent.getPeriodIndex());
									rr.create();
									time.addRejectionReasons(rr);
								}
								calendarEvent.delete();
							} else {
								// usuwamy zdarzenia cykliczne z przysz�o�ci
								List<CalendarEvent> futureEvents = calendarEvent.getFutureEventsInclusive();
								for(CalendarEvent evt : futureEvents) {
									RejectionReason rr = new RejectionReason();
									rr.setReason(eventStatusDesc);
									rr.setUsername(DSApi.context().getPrincipalName());
									rr.setTimeId(time.getId());
									rr.setPeriodIndex(evt.getPeriodIndex());
									rr.create();
									time.addRejectionReasons(rr);
									evt.delete();
								}
							}
						} else {
							calendarEvent.delete();
							if(eventStatusDesc != null) {
								RejectionReason rr = new RejectionReason();
								rr.setReason(eventStatusDesc);
								rr.setUsername(DSApi.context().getPrincipalName());
								rr.setTimeId(time.getId());
								rr.create();
								time.addRejectionReasons(rr);
							}
						}
						
						updateObsoleteEvents(time.getCalendarEvents(),ev);	
					}
					DSApi.context().commit();
					if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(EVENT_REMOVED);
					else out.write(EVENT_REMOVED_PL);
				} catch (Exception e) {
					if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(FAILED);
					else out.write(FAILED_PL);
					log.error(e.getMessage(), e);
				} finally {
					out.flush();
					out.close();
				}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}

	/**
	 * Zwraca eventy kalendarza {@link CalendarAjaxAction#calendarId} <br />
	 * w formacie JSON (lista obiekt�w {@link JSEvent}) wymagaj�ce
	 * od�wie�enia ({@link CalendarAjaxAction#obsoleteEvents}) a nast�pnie usuwa je z tej listy.<br/>
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class GetObsoleteEvents extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception 
		{
			log.debug("GetObsoleteEvents {}", System.currentTimeMillis());
			Date now = new Date();
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			Date lastR = new Date(lastRefresh * 1000); 
			Date lastModify = calendarsModifyTime.get(calendarId);
			if(lastModify != null && lastR != null && lastR.getTime() < lastModify.getTime())
			{
				out.write(gson.toJson(new ObsoleteBean(obsoleteEvents.get(calendarId),now.getTime())));
				kl.debug("GetObsoleteEvents: {} {}", calendarId, obsoleteEvents.get(calendarId));
			}
			else
				out.write(gson.toJson(new ObsoleteBean(null, now.getTime())));
			
			if(lastModify != null && obsoleteEvents.get(calendarId) != null && now.getTime()-lastModify.getTime() > DateUtils.MINUTE*5 )
				obsoleteEvents.get(calendarId).clear();
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	
	private class ObsoleteBean
	{
		Set<JSEvent> events;
		Long time;
		public ObsoleteBean(Set<JSEvent> events, long time)
		{
			this.events = events;
			this.time = time/1000;
		}
		public ObsoleteBean()
		{}
	}

	/**
	 * Czy�ci list� u�ytkownik�w i event�w do od�wie�enia
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class ClearMonitorEvents extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			obsoleteEvents = null;
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}
	
	/**
	 * Sprawdza dost�pno�� zasob�w {@link CalendarAjaxAction#calendarsId} <br />
	 * dla eventu {@link CalendarAjaxAction#eventId} (je�li <code>-1</code>, to nie bierzemy pod uwag�)<br />
	 * od {@link CalendarAjaxAction#start} do {@link CalendarAjaxAction#end} <br />
	 * @return <code>Map<calendarId, isAvailable></code> - mapa o kluczu <code>long</code> r�wnym kalendar id <br />
	 * i warto�ci <code>String</code> ({@link CalendarAjaxAction#RESOURCE_FREE} || {@link CalendarAjaxAction#RESOURCE_BUSY})
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class CheckResourceAvailability extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			List<Long> ids = gson.fromJson(calendarsId, 
					new TypeToken<List<Long>>(){}.getType());
			Map<Long,String> result = new HashMap<Long, String>();
			start = start+1;
			end = end-1;
			try {
				String avail = RESOURCE_FREE;
				Date startDate = new Date(start * 1000);
				Date endDate = new Date(end * 1000);
				CalendarEvent calendarEvent = null;
				
				if(eventId != -1) { // event jeszcze nie zapisany
					calendarEvent = DSApi.context().load(CalendarEvent.class, eventId);
					for(Long id : ids) {
						Calendar calendar = DSApi.context().load(Calendar.class, id);
						List<CalendarEvent> events = calendar.getAllEventsBetween(startDate, endDate);
						
						if(events != null && !events.isEmpty()) {
							avail = RESOURCE_FREE;
							
							for(CalendarEvent evt : events) {
								if(!evt.getTimeId().equals(calendarEvent.getTimeId())) {
									avail = RESOURCE_BUSY;
									break;
								} 
							}
						} 						
						result.put(id, avail);
					}
				} else {	// nie sprawdzamy czy zasoby nale�� do naszego eventu
					for(Long id : ids) {
						Calendar calendar = DSApi.context().load(Calendar.class, id);
						List<CalendarEvent> events = calendar.getAllEventsBetween(startDate, endDate);
						
						if(events != null && !events.isEmpty()) {
							avail = RESOURCE_BUSY;
						}						
						result.put(id, avail);
					}
				}
				
				out.write(gson.toJson(result));
			} catch(Exception e) {
				log.error(e.getMessage(), e);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(JSON_FAILED);
				out.write(JSON_FAILED_PL);
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	/**
	 * Sprawdza dost�pno�� u�ytkownik�w {@link CalendarAjaxAction#usersCn} <br />
	 * dla eventu {@link CalendarAjaxAction#eventId} (je�li <code>-1</code>, to nie bierzemy pod uwag�)<br />
	 * od {@link CalendarAjaxAction#start} do {@link CalendarAjaxAction#end} <br />
	 * @return <code>Map<userCn, isAvailable></code> - mapa o kluczu <code>String</code> r�wnym codename u�ytkownika <br />
	 * i warto�ci <code>String</code> ({@link CalendarAjaxAction#USER_FREE} || {@link CalendarAjaxAction#USER_BUSY})
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class CheckUserAvailability extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			List<String> cns = gson.fromJson(usersCn, 
					new TypeToken<List<String>>(){}.getType());
			Map<String, String> result = new HashMap<String, String>();
						
			start = start+1;
			end = end-1;
			try {
				// TODO: Sprawdza tylko dla jednego (pierwszego z listy) u�ytkownika
				String avail = USER_FREE;
				List<Calendar> cals = Calendar.findUserCalendars(cns.get(0));
				for(Calendar cal : cals) {
					List<CalendarEvent> events = cal.getAllEventsBetween(new Date(start*1000), new Date(end*1000));
					if(events != null && !events.isEmpty()) {
						avail = USER_BUSY;
						break;
					}
				}
				
				result.put(cns.get(0), avail);
				out.write(gson.toJson(result));
			} catch(Exception e) {
				log.error(e.getMessage(), e);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(JSON_FAILED);
				out.write(JSON_FAILED_PL);
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	/**
	 * Ustawia id koloru kalendarza {@link CalendarAjaxAction#calendarId} 
	 * na warto�� r�wn� {@link CalendarAjaxAction#colorId} <br />
	 * @return <code>CALENDAR_COLOR_CHANGED</code>, gdy si� uda�o <br />
	 * 			<code>FAILED</code>, gdy b��d
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class SetCalendarColor extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			
			try {
				//Calendar calendar = DSApi.context().load(Calendar.class, calendarId);
				//calendar.setColorId(colorId);
				DSApi.context().begin();
				UserCalendarBinder.find(calendarId, DSApi.context().getPrincipalName()).setColorId(colorId);
				DSApi.context().commit();
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(CALENDAR_COLOR_CHANGED);
				else out.write(CALENDAR_COLOR_CHANGED_PL);
			} catch(Exception e) {
				log.error(e.getMessage(), e);
				if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(FAILED);
				else out.write(FAILED_PL);
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}

	/**
	 * Zwraca wszystkie zasoby w formacie JSON (tj. list� obiekt�w {@link JSResource}). <br />
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class GetResources extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			out.write(gson.toJson(CalendarFactory.getInstance().getAllResources()));
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}
	
	/**
	 * Zwraca wszystkich u�ytkownik�w w formacie JSON (tj. list� obiekt�w {@link JSResource}). <br />
	 * Pierwszy u�ytkownik na li�cie to ten obecnie zalogowany (czyli wyst�puje na li�cie dwa razy).
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class GetUsers extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception { 	
			PrintWriter out = getOutPrintWriter();
			Gson gson = new Gson();
			System.out.println("005"+ new Date().getTime());
			out.write(gson.toJson(CalendarFactory.getInstance().getAllUsers()));
			System.out.println("006"+ new Date().getTime());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	/**
	 * Ustawia widoczno�� kalendarza {@link CalendarAjaxAction#calendarId} na 
	 * {@link CalendarAjaxAction#calendarVisible}
	 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
	 *
	 */
	private class SetCalendarVisibility extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception 
		{
			DSApi.context().begin();
			UserCalendarBinder.find(calendarId, DSApi.context().getPrincipalName()).setDisable(!calendarVisible);
			DSApi.context().commit();
			PrintWriter out = getOutPrintWriter();
			if(!AvailabilityManager.isAvailable("kalendarz.spolszczoneKomunikaty"))out.write(CALENDAR_VISIBILITY_CHANGED);
			else out.write(CALENDAR_VISIBILITY_CHANGED_PL);
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	public void setStart(long start) {
		this.start = start;
	}

	public long getStart() {
		return start;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public long getEnd() {
		return end;
	}

	public void setEventJson(String eventJson) {
		this.eventJson = eventJson;
	}

	public String getEventJson() {
		return eventJson;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static Map<Long, Set<JSEvent>> getObsoleteEvents() {
		return obsoleteEvents;
	}

	public static void setObsoleteEvents(Map<Long, Set<JSEvent>> obsoleteEvents) {
		CalendarAjaxAction.obsoleteEvents = obsoleteEvents;
	}

	public Long getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(Long calendarId) {
		this.calendarId = calendarId;
	}

	public List<Calendar> getCalendars() {
		return calendars;
	}

	public Long getLastRefresh() {
		return lastRefresh;
	}

	public void setLastRefresh(Long lastRefresh) {
		this.lastRefresh = lastRefresh;
	}

	public void setEventStatus(boolean eventStatus) {
		this.eventStatus = eventStatus;
	}

	public boolean isEventStatus() {
		return eventStatus;
	}

	public void setCalendarsId(String calendarsId) {
		this.calendarsId = calendarsId;
	}

	public String getCalendarsId() {
		return calendarsId;
	}

	public void setUsersCn(String usersCn) {
		this.usersCn = usersCn;
	}

	public String getUsersCn() {
		return usersCn;
	}

	public void setColorId(int colorId) {
		this.colorId = colorId;
	}

	public int getColorId() {
		return colorId;
	}

	public void setEventStatusDesc(String eventStatusDesc) {
		this.eventStatusDesc = eventStatusDesc;
	}

	public String getEventStatusDesc() {
		return eventStatusDesc;
	}

	public void setCalendarVisible(boolean calendarVisible) {
		this.calendarVisible = calendarVisible;
	}

	public boolean isCalendarVisible() {
		return calendarVisible;
	}

	public boolean isPeriodicDetached() {
		return periodicDetached;
	}

	public void setPeriodicDetached(boolean periodicDetached) {
		this.periodicDetached = periodicDetached;
	}
}
