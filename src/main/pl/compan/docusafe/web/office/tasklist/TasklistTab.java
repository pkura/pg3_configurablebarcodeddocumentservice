package pl.compan.docusafe.web.office.tasklist;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;

/**
 * Klasa TasklistTab.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class TasklistTab 
{
	StringManager smTab = GlobalPreferences.loadPropertiesFile("","tab");
	
	private String tab;
	private Tabs tabs;
	private String username;
	private TaskListTabAction base;
	private String sortField;
	private Boolean ascending;
	private String orderTab;
	private Tabs orderTabs;
	
	private static String TAB_IN = TaskListAction.TAB_IN;
	private static String TAB_OUT = TaskListAction.TAB_OUT;
	private static String TAB_INT = TaskListAction.TAB_INT;
	private static String TAB_ORDER = TaskListAction.TAB_ORDER;
	private static String TAB_WATCHES= TaskListAction.TAB_WATCHES;
	private static String TAB_CALENDAR = TaskListAction.TAB_CALENDAR;
	private static String TAB_CASES = TaskListAction.TAB_CASES;
	private static String TAB_MY_ORDERS = TaskListAction.TAB_MY_ORDERS;
	private static String TAB_REALIZE_ORDERS = TaskListAction.TAB_REALIZE_ORDERS;
	private static String TAB_WATCH_ORDERS = TaskListAction.TAB_WATCH_ORDERS;
	private static String TAB_DOCUMENT_PACKAGE = TaskListAction.TAB_DOCUMENT_PACKAGE;;
	
	
	public TasklistTab(TaskListTabAction base)
	{
		this.tab = base.getTab();
		this.username = base.getUsername();;
		this.base = base;
		this.sortField = base.getSortField();
		this.ascending = base.getAscending();
		this.orderTab = base.getOrderTab();
      //  if( ascending == null)
      //      ascending = Boolean.parseBoolean(DSApi.context().userPreferences().node("task-list").get("ascending"+tab, "false"));
	}
	
	public void initTab()
	{
		
         // domy�lna zak�adka - TAB_IN
         if (!base.isCalendarTab() && !TAB_IN.equals(tab) && !TAB_OUT.equals(tab) &&
             !TAB_INT.equals(tab) && !TAB_CASES.equals(tab) &&
             !TAB_ORDER.equals(tab) && !TAB_WATCHES.equals(tab) && !TAB_CALENDAR.equals(tab)
             && !TAB_DOCUMENT_PACKAGE.equals(tab) && !"assignmentJournalIn".equals(tab) && !"assignmentJournalOut".equals(tab))
         {
         	if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
         		tab = TAB_IN;
         	else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
         		tab = TAB_INT;
         	else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
         		tab = TAB_OUT;
         }
         base.setTab(tab);
         
         class TabEx extends Tab
         {
         	public String idName;
         	
         	private TabEx() {
         		super("", "");
         	}
         	
         	TabEx(String name, String title, String link, boolean selected, String idName) {
         		super(name, title, link, selected);
         		this.idName = idName;
         	}
         }
         
         tabs = new Tabs(4);
         
         String link;
         if (username != null)
         {
             link = base.getBaseLink() +
                 "?username="+username+
                 (sortField != null ? "&sortField="+sortField : "") +
                 "&ascending="+ascending;
         }
         else
         {
             link = base.getBaseLink() + "?a=a"; // tylko po to, by u�y� '&tab' ni�ej
         }

         if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
         {
	            tabs.add(new TabEx(smTab.getString("PismaPrzychodzace"), smTab.getString("ListaPismPrzychodzacych"),
	                link+"&tab="+TAB_IN, TAB_IN.equals(tab), TAB_IN));
         }
         
         if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
         {
         	tabs.add(new TabEx(smTab.getString("PismaWewnetrzne"), smTab.getString("ListaPismWewnetrznych"),
         			link+"&tab="+TAB_INT, TAB_INT.equals(tab), TAB_INT));
         }
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace") 
				|| AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo"))
		{
            tabs.add(new TabEx(smTab.getString("PismaWychodzace"), smTab.getString("ListaPismWychodzacych"),
                link+"&tab="+TAB_OUT, TAB_OUT.equals(tab), TAB_OUT));
	    }
         if (Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
         {
             tabs.add(new TabEx(smTab.getString("Polecenia"), smTab.getString("ListaPolecen"),
                 link+"&tab="+TAB_ORDER, TAB_ORDER.equals(tab), TAB_ORDER));
         }

         if (AvailabilityManager.isAvailable("sprawy"))
         {
             tabs.add(new TabEx(smTab.getString("Sprawy"), smTab.getString("ListaSpraw"),
                 link+"&tab="+TAB_CASES, TAB_CASES.equals(tab), TAB_CASES));
         }
         if(AvailabilityManager.isAvailable("menu.left.repository.obserwowane"))
			{
	            tabs.add(new TabEx(smTab.getString("Obserwowane"), smTab.getString("ListaZadanObserwowanych"),
	                link+"&tab="+TAB_WATCHES, TAB_WATCHES.equals(tab), TAB_WATCHES));
			}
         
         if(AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow"))
         {
        	// tabs.add(new TabEx(smTab.getString("Paczki dokument�w"), smTab.getString("ListaZadanPaczki"),
        	 tabs.add(new TabEx(smTab.getString("Przesy�ki"), smTab.getString("ListaZadanPaczki"),
 	                link+"&tab="+TAB_DOCUMENT_PACKAGE, TAB_DOCUMENT_PACKAGE.equals(tab), TAB_DOCUMENT_PACKAGE));
         }
         if (Configuration.additionAvailable(Configuration.ADDITION_CALENDAR))
         {
         	try
             {
         		tabs.add(new Tab(smTab.getString("Kalendarz"), smTab.getString("ZarzadzanieTerminami"),
         			"/office/tasklist/calendar-own.action?daily=true"
         			+"&tab="+TAB_CALENDAR+
         			(base.isOwnTasks()?"":("&username=" + DSApi.context().getDSUser().getName())), base.isCalendarTab()));
         	}
             catch (Exception e)
             {
            	 base.addActionError(e.getMessage());
         	}
         }

        if(AvailabilityManager.isAvailable("assignment.journal")) {
            tabs.add(new Tab(smTab.getString("AssignmentJournalIn"),smTab.getString("AssignmentJournalIn"),"/office/tasklist/assignment-journal.action?type=in","assignmentJournalIn".equals(tab)));
            tabs.add(new Tab(smTab.getString("AssignmentJournalOut"),smTab.getString("AssignmentJournalOut"),"/office/tasklist/assignment-journal.action?type=out","assignmentJournalOut".equals(tab)));
        }

         if (TAB_ORDER.equals(tab))
         {
             if (!TAB_MY_ORDERS.equals(orderTab)
             && !TAB_REALIZE_ORDERS.equals(orderTab) && !TAB_WATCH_ORDERS.equals(orderTab))
                 orderTab = TAB_REALIZE_ORDERS;

             orderTabs = new Tabs(3);

             orderTabs.add(new Tab(smTab.getString("WydanePolecenia"), smTab.getString("WydanePolecenia"),
                 link+"&tab="+TAB_ORDER+"&orderTab="+TAB_MY_ORDERS, TAB_MY_ORDERS.equals(orderTab)));
             orderTabs.add(new Tab(smTab.getString("RealizowanePolecenia"), smTab.getString("RealizowanePolecenia"),
                 link+"&tab="+TAB_ORDER+"&orderTab="+TAB_REALIZE_ORDERS, TAB_REALIZE_ORDERS.equals(orderTab)));
             orderTabs.add(new Tab(smTab.getString("PrzekazanePolecenia"), smTab.getString("PrzekazanePolecenia"),
                 link+"&tab="+TAB_ORDER+"&orderTab="+TAB_WATCH_ORDERS, TAB_WATCH_ORDERS.equals(orderTab)));
         }
         
         base.setOrderTab(orderTabs);
         base.setTabs(tabs);
	}
	
}
