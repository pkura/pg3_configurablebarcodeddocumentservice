package pl.compan.docusafe.web.office.tasklist.calendar;

import java.util.HashSet;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.TimeResource;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.util.TextUtils;

/**
 * Klasa opisująca obiekt {@link pl.compan.docusafe.core.users.DSUser} do serializacji w JSONie. <br/>
 * Jeżeli zapisujemy użytkownika do eventu wystarczy uzupełnione pole {@link JSResource#cn}
 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omelańczuk</a>
 *
 */
public class JSResource
{
	private String cn;
	private String partTwoOfName;
	private String partOneOfName;
	private boolean confirm;
	private Long calendarId;
	private String type = CalendarFactory.DSUSER_RESOURCE_TYPE;

	public JSResource(long id, String cn, String partTwoOfName, String partOneOfName, String type)
	{
			this(id,cn,partTwoOfName,partOneOfName, false, type,null);
	}

	public JSResource(long id, String cn, String partTwoOfName, String partOneOfName, boolean confirm, String type, Long calendarId) {
		this.cn = cn;
		this.partTwoOfName = partTwoOfName;
		this.partOneOfName = partOneOfName;
		this.confirm = confirm;
		this.calendarId = calendarId;
		this.type = type;
	}


	public JSResource(TimeResource resource)
	{
		this.cn = resource.getCn();
		this.partTwoOfName = resource.getPartTwoOfName();
		this.partOneOfName = resource.getPartOneOfName();
		this.type = resource.getType();
	}

	public void convertIsoToUTF() throws Exception {
		partTwoOfName = TextUtils.isoToUtf(partTwoOfName);
		partOneOfName = TextUtils.isoToUtf(partOneOfName);
	}

	public JSResource() {
		// GSon potrzebuje bezargumentowego konstruktora
	}

	public JSResource(CalendarEvent trb) throws EdmException
	{
		this.cn = trb.getResource().getCn();
		this.partTwoOfName = trb.getResource().getPartTwoOfName();
		this.partOneOfName = trb.getResource().getPartOneOfName();
		this.confirm = trb.isConfirm();
		this.calendarId = trb.getCalendarId();
		this.type = trb.getResource().getType();
	}

	public static Set<JSResource> toJSResources(Set<CalendarEvent> resources) throws EdmException {
		Set<JSResource> jsres = new HashSet<JSResource>();
		for(CalendarEvent u : resources)
			jsres.add(new JSResource(u));

		return jsres;
	}

	@Override
	public int hashCode() {
		return  this.calendarId.hashCode() ^ this.cn.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(this.calendarId == null)
			return false;
		if(obj != null && obj instanceof JSResource)
			return this.calendarId.equals(((JSResource)obj).calendarId);
		return false;
	}

	@Override
	public String toString() {
		return String.format("[cn: %s, calId: %d, confirm: %b]",
				 cn, calendarId, confirm);
		//return cn;
	}

	public String getPartOneOfName() {
		return partOneOfName;
	}

	public void setPartOneOfName(String partOneOfName) {
		this.partOneOfName = partOneOfName;
	}

	public String getPartTwoOfName() {
		return partTwoOfName;
	}

	public void setPartTwoOfName(String partTwoOfName) {
		this.partTwoOfName = partTwoOfName;
	}

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public Long getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(Long binderId) {
		this.calendarId = binderId;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}