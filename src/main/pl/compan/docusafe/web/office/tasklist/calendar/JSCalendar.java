package pl.compan.docusafe.web.office.tasklist.calendar;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;

/**
 * Klasa opisująca obiekt {@link Calendar} do serializacji w JSONIE
 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omelańczuk</a>
 *
 */
public class JSCalendar {
	private Long id;
	private String name;
	private String owner;
	private String type;
	private Integer permissionType = CalendarFactory.PERMISSION_SEE_ONLY;
	private Integer colorId = 0;
	private Boolean disable;

	public JSCalendar() {
		// Gson potrzebuje konstruktora bezargumentowego
	}

	public JSCalendar(UserCalendarBinder cal) {
		id = cal.getCalendarId();
		name = cal.getCalendar().getName();
		owner = cal.getCalendar().getOwner();
		type = cal.getCalendar().getType();
		permissionType = cal.getPermissionType();
		colorId = cal.getColorId();
		disable = cal.getDisable();
	}

	public static List<JSCalendar> toJSCalendars(List<UserCalendarBinder> cals) {
		List<JSCalendar> jsCals = new ArrayList<JSCalendar>();

		for(UserCalendarBinder cal : cals) {
			jsCals.add(new JSCalendar(cal));
		}

		return jsCals;
	}

	@Override
	public String toString() {
		return "Calendar#" + id + "(" + name + ", " + owner + ", "
			+ type + ")";
	}

	public Integer getPermissionType() {
		return permissionType;
	}

	public void setPermissionType(Integer permissionType) {
		this.permissionType = permissionType;
	}
}
