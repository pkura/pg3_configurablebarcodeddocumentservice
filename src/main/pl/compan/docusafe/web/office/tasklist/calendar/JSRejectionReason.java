package pl.compan.docusafe.web.office.tasklist.calendar;

import pl.compan.docusafe.core.calendar.sql.RejectionReason;

/**
 * Klasa JSRejectionReason.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class JSRejectionReason 
{
	String username;
	String reason;
	
	public JSRejectionReason()
	{
		
	}
	
	public JSRejectionReason(RejectionReason r)
	{
		this.username = r.getUsername();
		this.reason = r.getReason();
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
