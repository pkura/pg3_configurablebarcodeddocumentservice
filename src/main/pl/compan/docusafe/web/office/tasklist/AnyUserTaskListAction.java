package pl.compan.docusafe.web.office.tasklist;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
/* User: Administrator, Date: 2005-08-23 09:43:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnyUserTaskListAction.java,v 1.10 2010/08/11 12:20:26 mariuszk Exp $
 */
public class AnyUserTaskListAction extends TaskListAction
{
    protected DSUser getDSUser() throws EdmException
    {
        if (getUsername() == null)
            return null;

        try
        {
            return DSUser.findByUsername(getUsername());
        }
        catch (UserNotFoundException e)
        {
            return null;
        }
    }
    
    protected DSDivision getDSDivision() throws EdmException
    {
        if (getUsername() == null)
            return null;

        try
        {
            return DSDivision.find(getUsername());
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public boolean isOwnTasks()
    {
        return false;
    }

    public String getBaseLink()
    {
        return "/office/tasklist/any-user-task-list.action";
    }

    protected List getDSUserList() throws EdmException {
       return UserFactory.getInstance().getCanAssignUsers();
    }
    
//    protected List getDSUserAndDivisionsList() throws EdmException 
//    {   	
//       List l = UserFactory.getInstance().getCanAssignUsers();
//       l.addAll(UserFactory.getInstance().getAllDivisionsDivisionType());
//       return l;
//    } 
    protected LinkedHashMap<String,String> getDSUserAndDivisions() throws EdmException 
    {
//    	HashMap<String,String> tempMap = new HashMap<String,String>();
//    	StringValueComparator bvc =  new StringValueComparator(tempMap);
//        TreeMap<String,String> sortedMap = new TreeMap(bvc);
//        List<DSDivision> dsd = UserFactory.getInstance().getAllDivisionsDivisionType();
//    	for(DSDivision d : dsd)
//    	{   		
//    		if(!d.isHidden()) 
//    		{
//    			tempMap.put(d.getName(), d.getName());
//    		}    	
//    	}
//    	dsd = null;
//    	List<DSUser> dsu = UserFactory.getInstance().getCanAssignUsers();
//        for(DSUser d : dsu)
//        {
//        	tempMap.put(d.getName(),d.asLastnameFirstname());
//        }
//        dsu = null;
//        sortedMap.putAll(tempMap);
//        return sortedMap;
    	 
    	LinkedHashMap<String,String> tempMap = new LinkedHashMap<String,String>();
		List<DSDivision> dsd = UserFactory.getInstance().getAllCanAssignDivisionsDivisionType();
		Collections.sort(dsd, new DSDivision.DivisionComparatorAsName());
        if (AvailabilityManager.isAvailable("task.anyTaksList.addDivision")) {
            for (DSDivision d : dsd) {
                if (!d.isHidden()) {
                    tempMap.put(d.getGuid(), d.getName());
                }
            }
        }
    	List<DSUser> dsu = UserFactory.getInstance().getCanAssignUsers();
		Collections.sort(dsu, new DSUser.UserComparatorAsLastFirstName());    	
        for(DSUser d : dsu)
        {
        	tempMap.put(d.getName(),d.asLastnameFirstname());
        }
        dsu = null;
        return tempMap;
     }


	public void setTab(String tab) {
		super.setTab(tab);
	}

	@Override
	public String getListName() {
		return "any-user-task-list";
	}
    
}
