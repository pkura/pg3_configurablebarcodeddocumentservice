package pl.compan.docusafe.web.office.tasklist.calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.calendar.sql.RejectionReason;
import pl.compan.docusafe.core.calendar.sql.TimeImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;


/**
 * Klasa opisuj�ca obiekt {@link pl.compan.docusafe.core.calendar.DSTime} do serializacji w JSONie. <br/>
 * Uwaga - daty w formacie UnixStamp.
 * @author <a href="mailto:kamil.omelanczuk@docusafe.pl">Kamil Omela�czuk</a>
 *
 */
public class JSEvent
{
	public final static int TITLE_LENGTH = 20;
	long id;
	/**
	 * Czas rozpocz�cia w formacie UnixStamp
	 */
	long start;
	/**
	 * Czas rozpocz�cia w formacie UnixStamp
	 */
	long end;
	/**
	 * Tytu� powienien by� raczej kr�tki - oko�o 10 znak�w
	 */
	String title;
	String desc;
	String place;
	JSResource owner;
	boolean allDay;	// event trwaj�cy ca�y dzie�
	/**
	 * Lista zasob�w {@link JSResource} przypisanych do eventu
	 */
	Set<JSResource> resources;
	Set<RejectionReason>  reasons;
	/**
	 * Id tw�rcy(wla�ciciela) eventu
	 */
	long creatorId = -1;
	long calendarId = -1;
	/** 
	 * Wydarzenie cykliczne.
	 */
	boolean periodic = false;
	/**
	 * D�ugo�� interwa�u (nale�y te� okre�lic <code>periodKind</code>).
	 */
	int periodLength = 0;
	/**
	 * Rodzaj interwa�u dla wydarzenia cyklicznego:
	 * <ol start=0>
	 * <li>= dzie�
	 * <li>= tydzie�
	 * <li>= miesi�c
	 * <li>= rok
	 * </ol>
	 */
	int periodKind = 0;
	/**
	 * Data zako�czenia wydarzenia cyklicznego w formacie UnixStamp
	 */
	long periodEnd;
	/**
	 * Zmienna wskazuje na oryginalny event utworzony przez u�ytkownika
	 */
	long periodParentId = 0;
	/**
	 * Dla wydarze� cyklicznych - wskazuje na usuni�te zdarzenie
	 */
	boolean removed = false;

	public JSEvent(long id, long start, long end, boolean allDay, String title, String desc, String place,JSResource owner) 
	{
		this.id = id;
		this.start = start;
		this.end = end;
		this.allDay = allDay;
		this.title = title;
		this.desc = desc;
		this.place = place;
		this.owner = owner;
	}

	public JSEvent(CalendarEvent calendarEvent,TimeImpl time) throws EdmException
	{
		this(calendarEvent.getId(),calendarEvent.getStartTime().getTime()/1000, calendarEvent.getEndTime().getTime()/1000,
				calendarEvent.isAllDay(), calendarEvent.getShortDescription(TITLE_LENGTH), calendarEvent.getDescription(), 
				calendarEvent.getPlace(),new JSResource(time.getCreator()));
		Logger kl = LoggerFactory.getLogger("kamil");
		resources = new HashSet<JSResource>();
		for (CalendarEvent tcb : time.getCalendarEvents())
		{
			boolean addResource = true;
			
			// dla wydarze� cykliczny sprawdzamy periodIndex
			if(calendarEvent.isPeriodic() && calendarEvent.getPeriodIndex() != null 
				&& calendarEvent.getPeriodIndex().equals(tcb.getPeriodIndex()) == false) {
				addResource = false;
			}
			if(addResource) {
				JSResource jsres = new JSResource(tcb);
				if(kl.isDebugEnabled()) {
					kl.debug("Adding resource eventPeriodIndex#{} tcbId#{} periodIndex#{} {} {}",
							calendarEvent.getPeriodIndex(),
							tcb.getId(), tcb.getPeriodIndex(), jsres.getCn(), jsres.isConfirm());
				}
				resources.add(jsres);
			}
		}
		kl.debug("Resources for event #{} {}", calendarEvent.getId(), resources);
		this.creatorId = time.getCreator().getId();
		this.calendarId = calendarEvent.getCalendarId();
		
		if(calendarEvent.isPeriodic()) {
			// dla wydarze� cyklicznych sprawdzamy periodIndex
			this.reasons = time.getRejectionReasonsByPeriodIndex(calendarEvent.getPeriodIndex());
		} else {
			this.reasons = time.getRejectionReasons();
		}
		
		this.periodic = calendarEvent.isPeriodic();
		this.periodKind = calendarEvent.getPeriodKind();
		this.periodLength = calendarEvent.getPeriodLength();
		try {
			this.periodEnd = calendarEvent.getPeriodEnd().getTime() / 1000;
			this.periodParentId = calendarEvent.getPeriodParentId();
		} catch(NullPointerException e) {
			this.periodEnd = this.end;
		}
	}
	

	public void updateValue(CalendarEvent ev) {
		this.id = ev.getId();
		this.start = ev.getStartTime().getTime()/1000;
		this.end = ev.getEndTime().getTime()/1000;
		this.allDay = ev.isAllDay();
		this.title = ev.getShortDescription(TITLE_LENGTH);
		this.desc = ev.getDescription();
		this.place = ev.getPlace();
		try {
		this.periodic = ev.isPeriodic();
		this.periodLength = ev.getPeriodLength();
		this.periodKind = ev.getPeriodKind();
		this.periodParentId = ev.getPeriodParentId();
		
			this.periodEnd = ev.getPeriodEnd().getTime() / 1000;
		} catch(NullPointerException e) {
			this.periodEnd = this.end;
		}
	}
	
	public void convertIsoToUtf() throws Exception 
	{
		title = TextUtils.isoToUtf(title);
		desc = TextUtils.isoToUtf(desc);
		place = TextUtils.isoToUtf(place);
		owner.convertIsoToUTF();
		for(JSResource u : resources)
			u.convertIsoToUTF();
	}
	
	

	public JSEvent() {
		// GSon potrzebuje bezargumentowego konstruktora
	}

	public JSEvent(JSEvent evt)
	{
		this.id = evt.id;
		this.start = evt.start;
		this.end = evt.end;
		this.allDay = evt.allDay;
		this.title = evt.title;
		this.desc = evt.desc;
		this.place = evt.place;
		this.calendarId = evt.calendarId;
		this.resources = new HashSet<JSResource>(evt.resources);
		if(evt.reasons != null)
			this.reasons = new HashSet<RejectionReason>(evt.reasons);
		this.owner = evt.getOwner();
		this.periodic = evt.periodic;
		this.periodKind = evt.periodKind;
		this.periodLength = evt.periodLength;
		this.periodEnd = evt.periodEnd;
		this.periodParentId = evt.periodParentId;
	}
	

	@Override
	public int hashCode() {
		return (int) this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof JSEvent)
			return this.id == ((JSEvent)obj).id;
		return false;
	}

	public String toString() {
		//return String.format("id: %d, start: %s, end: %s, title: %s, desc: %s, place: %s", id, new Date(start*1000), new Date(end*1000), title, desc, place);
		/*return "JSEvent #" + id + "(Start " + new Date(this.start * 1000) + " end " + new Date(this.end * 1000)  
			+ " desc: " + desc + ")" + resources.toString()
			+ " periodic: " + periodic + " periodicLength: " + periodLength + "\n";*/
		
		return this.id + "" + "-" + this.desc;
	}

	public Set<JSResource> getResources() {
		return resources;
	}

	public void setResources(Set<JSResource> resources) {
		this.resources = resources;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public JSResource getOwner() {
		return owner;
	}

	public void setOwner(JSResource owner) {
		this.owner = owner;
	}

}
