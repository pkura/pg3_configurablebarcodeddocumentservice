package pl.compan.docusafe.web.office.tasklist;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.BoxLine;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-11-16 10:39:35 */

/**
 * Zbiorcze dodawanie pism do pud�a archiwalnego.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MultiBoxingAction.java,v 1.13 2008/10/06 10:40:36 pecet4 Exp $
 */
public class MultiBoxingAction extends EventActionSupport
{
    private Long[] documentIds;
    //private String boxNumber;
    private Map<String,String> boxNumbers = new LinkedHashMap<String, String>();
    private Long boxId;
   /* private Long currentBoxId;
    private String currentBoxNumber;*/
    private String tab;
    private List<BoxLineBean> boxLines;
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private static final String DEFAULT_LINE = "default";
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doBox").
            append(OpenHibernateSession.INSTANCE).
            append(new Boxing()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (documentIds == null || documentIds.length == 0)
            {
                if ("out".equals(tab))
                    event.setResult("task-list-out");
                else if ("internal".equals(tab))
                    event.setResult("task-list-int");
                else
                    event.setResult("task-list");
                return;
            }

            try
            {
                boxLines = new ArrayList<BoxLineBean>();                
                
                // pobranie danych dla domy�lnej linii
                BoxLineBean bean = new BoxLineBean();
                bean.setName("Domy�lna");
                bean.setLine(DEFAULT_LINE);
                
                if (/*boxNumber == null && */GlobalPreferences.isBoxOpen())
                {
                    Long boxId = GlobalPreferences.getBoxId();
                    if (boxId != null)
                    {
                        try
                        {
                            Box box = Box.find(boxId);
                            bean.setCurrentBoxNumber(box.getName());
                            bean.setCurrentBoxId(box.getId());
                        /*    currentBoxNumber = box.getName();
                            currentBoxId = box.getId();*/
                        }
                        catch (EntityNotFoundException e)
                        {
                        }
                    }
                }
                boxLines.add(bean);
                
                // pobranie danych dla pozosta�ych linii
                List<BoxLine> list = BoxLine.list();
                for (BoxLine boxLine : list)
                {
                    bean = new BoxLineBean();
                    bean.setLine(boxLine.getLine());
                    bean.setName(boxLine.getName());
                    
                    Long boxId = GlobalPreferences.getBoxId(boxLine.getLine());
                    if (boxId != null)
                    {
                        Box box = null;
                        try
                        {
                            box = Box.find(boxId);
                            if (box.isOpen())
                            {
                                bean.setCurrentBoxNumber(box.getName());
                                bean.setCurrentBoxId(box.getId());
                            }
                        }
                        catch (EntityNotFoundException e)
                        {
                        }
                    }
                    boxLines.add(bean);
                }                 
                
                if (event.getAttribute("error") == null)
                {
                    for (int i=0; i < documentIds.length; i++)
                    {
                        OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);
                        if (document.getBox() != null)
                        {
                            addActionMessage(sm.getString("PismoNrNieBedzieDodaneDoPudlaPoniewazZnajdujeSieOnoWpudle",document.getOfficeNumber(),document.getBox().getName()));
                        }
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Boxing implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if ("out".equals(tab))
                event.setResult("task-list-out");
            else if ("internal".equals(tab))
                event.setResult("task-list-int");
            else
                event.setResult("task-list");

          /*  boxNumber = TextUtils.trimmedStringOrNull(boxNumber);
            if (boxNumber == null && boxId == null)
            {
                addActionError("Nie wybrano pud�a");
                return;
            } */            

            if (documentIds == null || documentIds.length == 0)
            {
                addActionError(sm.getString("NiePrzekazanoNumerowPism"));
                return;
            }

            try
            {
                DSApi.context().begin();

                // flaga okre�laj�ca, �e nie nale�y wykonywa�
                // przekierowania do listy zada�
                boolean needsNotBox = "x".equalsIgnoreCase(boxNumbers.get(DEFAULT_LINE));
                Box box = null;
                if (boxNumbers.get(DEFAULT_LINE) != null && !needsNotBox)
                {
                    if (boxNumbers.get(DEFAULT_LINE) != null)
                    {
                        box = Box.findByName(null,boxNumbers.get(DEFAULT_LINE));
                    }
                    else // if (boxId != null)
                    {
                        box = Box.find(boxId);
                    }
                }

                for (int i=0; i < documentIds.length; i++)
                {
                    OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);

                    if (document.getBox() == null)
                    {
                        // dokumentom o pewnych rodzajach nale�y przydzieli� pud�o z innej linii
                        // (wynikaj�ce z 'dockind-a')
                        String boxLine = null;
                        if (document.getDocumentKind() != null)
                        {
                            document.getDocumentKind().initialize();
                            boxLine = document.getDocumentKind().getProperties().get(DocumentKind.BOX_LINE_KEY);
                        }
                        if (boxLine != null)
                        {
                            String otherBoxNumber = boxNumbers.get(boxLine);  
                            boolean otherNeedsNotBox = "x".equalsIgnoreCase(otherBoxNumber);
                            if (otherNeedsNotBox)
                                document.addWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                            else
                            {
                                if (otherBoxNumber != null)                                     
                                    document.setBox(Box.findByName(boxLine,otherBoxNumber));
                                else
                                    throw new EdmException(sm.getString("NieWybranoPudlaDlaLinii",BoxLine.find(boxLine).getName()));
                                document.clearWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                            }
                        }
                        else
                        {
                            if (needsNotBox)
                                document.addWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                            else
                            {
                                if (box == null)
                                    throw new EdmException(sm.getString("NieWybranoPudlaDlaLiniiDomyslnej"));
                                else
                                    document.setBox(box);
                                document.clearWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                            }
                        }
                    }
                }
                
                
                
                // flaga okre�laj�ca, �e nie nale�y wykonywa�
                // przekierowania do listy zada�
         /*       boolean needsNotBox = "x".equalsIgnoreCase(boxNumber);
                Box box = null;
                if (!needsNotBox)
                {
                    if (boxNumber != null)
                    {
                        box = Box.findByName(null,boxNumber);
                    }
                    else // if (boxId != null)
                    {
                        box = Box.find(boxId);
                    }
                }

                for (int i=0; i < documentIds.length; i++)
                {
                    OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);

                    if (document.getBox() == null)
                    {
                        if (needsNotBox)
                        {
                            document.addWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                        }
                        else
                        {
                            // dokumentom o pewnych rodzajach nale�y przydzieli� pud�o z innej linii
                            // (wynikaj�ce z 'dockind-a')
                            if (document.getDocumentKind() != null)
                            {
                                document.getDocumentKind().initialize();
                                String boxLine = document.getDocumentKind().getProperties().get(DocumentKind.BOX_LINE_KEY);                                
                                if (boxLine != null)
                                {
                                    Long otherBoxId = GlobalPreferences.getBoxId(boxLine);
                                    // jak nie jest otwarte zadne pudlo w tej linii to nie przydziele ...
                                    if (otherBoxId != null)
                                    {                                       
                                        document.setBox(Box.find(otherBoxId));
                                    }
                                }
                                else
                                    document.setBox(box);
                            }
                            else
                                document.setBox(box);
                            document.clearWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
                        }
                    }
                }*/

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                event.setResult(SUCCESS);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                event.setAttribute("error", Boolean.TRUE);
            }
        }
    }

    public Long[] getDocumentIds()
    {
        return documentIds;
    }

    public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }    
    
   /* public String getBoxNumber()
    {
        return boxNumber;
    }

    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }

    public Long getCurrentBoxId()
    {
        return currentBoxId;
    }

    public String getCurrentBoxNumber()
    {
        return currentBoxNumber;
    }*/

    public Map<String, String> getBoxNumbers()
    {
        return boxNumbers;
    }

    public void setBoxNumbers(Map<String, String> boxNumbers)
    {
        this.boxNumbers = boxNumbers;
    }

    public void setBoxId(Long boxId)
    {
        this.boxId = boxId;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }
    
    public List<BoxLineBean> getBoxLines()
    {
        return boxLines;
    }
    
    public class BoxLineBean
    {
        private String line;
        private String name;
        private String currentBoxNumber;
        private Long currentBoxId;     

        public String getCurrentBoxNumber()
        {
            return currentBoxNumber;
        }

        public void setCurrentBoxNumber(String currentBoxNumber)
        {
            this.currentBoxNumber = currentBoxNumber;
        }

        public Long getCurrentBoxId()
        {
            return currentBoxId;
        }

        public void setCurrentBoxId(Long currentBoxId)
        {
            this.currentBoxId = currentBoxId;
        }

        public String getLine()
        {
            return line;
        }

        public void setLine(String line)
        {
            this.line = line;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }          
        
        public String getJsOnSubmit()
        {
            return "onSubmit('"+line+"')";
        }
    }
}
