/**
 * 
 */
package pl.compan.docusafe.web.office.tasklist;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 11-09-2013
 * docusafe
 * PrintEnvelopesAction.java
 */
public class PrintEnvelopesAction extends EventActionSupport{

	private Long[] documentIds;
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this
			.getClass().getPackage().getName(), null);

	@Override
	protected void setup() {
		PrintEnvelopes PrintEnvelopes = new PrintEnvelopes();
		
		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(PrintEnvelopes).appendFinally(
						CloseHibernateSession.INSTANCE);
	}
	private class PrintEnvelopes implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			File temp = null;
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				FieldsManager fm = null;
				Long personId = null;
				Person recipient = null;
				LinkedHashMap<String, String> mapToRtf = null;
				Map<Long, LinkedHashMap<String, String>> rtfMerge = new HashMap<Long, LinkedHashMap<String, String>>();
				/*for(Long id: documentIds){
					fm = Document.find(id.longValue()).getFieldsManager();
					params = fm.getFieldValues();
					personId = (Long)params.get("RECIPIENT");
					recipient = Person.find(personId.longValue());
					mapToRtf = new LinkedHashMap<String, String>();
					if(recipient.getFirstname() != null && recipient.getLastname() != null) mapToRtf.put("NAME", recipient.getFirstname() + " " + recipient.getLastname());
					if(recipient.getOrganization() != null) mapToRtf.put("ORGANIZATION", recipient.getOrganization());
					if(recipient.getStreet() != null) mapToRtf.put("STREET", recipient.getStreet());
					if(recipient.getZip() != null && recipient.getLocation() != null) mapToRtf.put("LOCATION", recipient.getZip() + " " + recipient.getLocation());
					rtfMerge.put(id, mapToRtf);
				}*/
				
				
				ServletActionContext.getResponse().setContentType("text/pdf"); 
		        String fileName = "wydruk_kopert.pdf";
		        ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				
                com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A6.rotate());
                PdfWriter.getInstance(pdfDoc, ServletActionContext.getResponse().getOutputStream());
                
                pdfDoc.setMargins(30f, 35f, 10f, 0f);
                pdfDoc.open();

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font font = null;
                if(AvailabilityManager.isAvailable("setEnvelopesFont12")){
                	font = new Font(baseFont, 12);
                }else{
                	font = new Font(baseFont, 14);
                }
                
                for (Long ids :documentIds)
                {
                	fm = Document.find(ids.longValue()).getFieldsManager();
					params = fm.getFieldValues();
					personId = (Long)params.get("RECIPIENT");
					recipient = Person.find(personId.longValue());
					mapToRtf = new LinkedHashMap<String, String>();
					if(recipient.getFirstname() != null && recipient.getLastname() != null) mapToRtf.put("NAME", recipient.getFirstname() + " " + recipient.getLastname());
					if(recipient.getOrganization() != null) mapToRtf.put("ORGANIZATION", recipient.getOrganization());
					if(recipient.getStreet() != null) mapToRtf.put("STREET", recipient.getStreet());
					if(recipient.getZip() != null && recipient.getLocation() != null) mapToRtf.put("LOCATION", recipient.getZip() + " " + recipient.getLocation());
					rtfMerge.put(ids, mapToRtf);
                	
                    final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(ids);
                    PdfPTable table = new PdfPTable(1);
                    table.setWidthPercentage(100);
                    table.setKeepTogether(true);
                    PdfPCell senderCell = null;
                    PdfPCell recipientCell = null;
                    /*if(document.getSender() != null){
                    	String senderSummary = new String();
                    	senderSummary = document.getSender().getSummary().toString();
                		String [] senderArray = senderSummary.toString().split("/");
                		StringBuilder sb = new StringBuilder();
                		for(String text : senderArray){
                			sb.append(text).append(System.getProperty("line.separator"));
                		}
                    	senderCell  = new PdfPCell(new Phrase(sb.toString(), font));
                    }else {
                    	senderCell  = new PdfPCell();
                    }*/
                    if(AvailabilityManager.isAvailable("printEnvelopesForPig")){
                    	StringBuilder sb = new StringBuilder();
                    	sb.append(System.getProperty("line.separator"))
                    		.append("Pa�stwowy Instytut Geologiczny")
	                    	.append(System.getProperty("line.separator"))
	                    	.append("Pa�stwowy Instytut Badawczy")
	                    	.append(System.getProperty("line.separator"))
	                    	.append("ul. Rakowiecka 4")
	                    	.append(System.getProperty("line.separator"))
	                    	.append("00-975 Warszawa");
                    	senderCell = new PdfPCell(new Phrase(sb.toString(), font));
                    }else{
                    	senderCell  = new PdfPCell();
                    }
                    
                    if(document.getRecipients() != null){
                    	StringBuilder sb = new StringBuilder();
                    	String recipientSummary = new String();
                    	recipientSummary = (document.getRecipients().get(0)).getSummary().toString();
                    	String [] recipientArray = recipientSummary.toString().split("/");
                    		
                    	if(rtfMerge.get(ids).get("NAME") != null){
                    		sb.append("Pan/Pani ");
                    	}
                    		
                    	for(String text : rtfMerge.get(ids).keySet()){
                    		sb.append(mapToRtf.get(text)).append(System.getProperty("line.separator"));
                    	}
                    	
                    	/*for(String text : recipientArray){
                    		sb.append(text).append(System.getProperty("line.separator"));
                    	}
                    	sb.append(rtfMerge.get(ids).get("ZIP") != null ? rtfMerge.get(ids).get("ZIP").toString() : "");*/
                    	sb.append(System.getProperty("line.separator"));
                    	recipientCell  = new PdfPCell(new Phrase(sb.toString(), font));
                    	
                    }else {
                    	recipientCell  = new PdfPCell();
                    }
                    
                    senderCell.setMinimumHeight(130f);
                    recipientCell.setMinimumHeight(130f);
                   
                    senderCell.setBorder(Rectangle.NO_BORDER);
                    recipientCell.setBorder(Rectangle.NO_BORDER);
                    
                    senderCell.setVerticalAlignment(Element.ALIGN_TOP);
                    senderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    recipientCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                    recipientCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    
    				table.addCell(senderCell);
    				table.addCell(recipientCell);
    				table.setKeepTogether(true);
    				
    				pdfDoc.add(table);
                    pdfDoc.newPage();
                }
                pdfDoc.close();
			} catch (DocumentNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Long[] getDocumentIds() {
		return documentIds;
	}
	public void setDocumentIds(Long[] documentIds) {
		this.documentIds = documentIds;
	}

}
