package pl.compan.docusafe.web.office.tasklist;

import pl.compan.docusafe.web.common.Tabs;

/**
 * Klasa TaskListTabAction.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public interface TaskListTabAction {

	public String getBaseLink();

	public boolean isOwnTasks();

	public void addActionError(String message);

	public void setOrderTab(Tabs orderTabs);

	public void setTabs(Tabs tabs);

	public String getTab();

	public String getUsername();

	public String getSortField();

	public Boolean getAscending();

	public String getOrderTab();

	public void setTab(String tab);
	
	public void setBookmarkId(Long id);
	
	public Long getBookmarkId();
	
	public boolean isCalendarTab();

}
