package pl.compan.docusafe.web.office.tasklist;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.core.dockinds.FieldsManager;

public class MultiPayAction extends EventActionSupport 
{
	private Long[] documentIds;
	private String documentDate;
	private StringManager sm 
		= GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	
	private List<OfficeDocument> documents;
	private List<OfficeDocument> docs = new ArrayList<OfficeDocument>();
	private String tab;
	
	protected void setup() 
	{
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doPay").
        append(OpenHibernateSession.INSTANCE).
        append(new Pay()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

			//w zaleznosci z ktorej listy wywolano akcje system odsyla do niej
			if (documentIds == null || documentIds.length == 0)
            {
                if ("out".equals(tab))
                    event.setResult("task-list-out");
                else if ("internal".equals(tab))
                    event.setResult("task-list-int");
                else
                    event.setResult("task-list");
                return;
            }
			
			
			try 
			{
				DSApi.context().begin();
				FieldsManager fm;
				for(int i=0;i<documentIds.length;i++)
				{
					OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);
					fm = document.getDocumentKind().getFieldsManager(document.getId());
					if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND))
					{
						if(fm.getValue(InvoiceLogic.ZAPLACONO_FIELD_CN.toString()).equals("tak"))
						{
							addActionError(sm.getString("DokumentONrKo")+" : "+document.getOfficeNumber()+" "+sm.getString("JestJuzOplacony"));
						}
						else 
						{
							addActionMessage(sm.getString("DokumentONrKo")+" : "+document.getOfficeNumber()+" "+sm.getString("ZostaniePrzetworzony"));
							docs.add(document);
						}
							
					} else addActionError(sm.getString("DokumentONrKo")+" : "+document.getOfficeNumber()+" "+sm.getString("NieJestFaktura"));
				}
				DSApi.context().commit();
			} 
			catch(EdmException e)
			{
				DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
		}
	}
	
	private class Pay implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(documentDate == null)
			{
				addActionError(sm.getString("NiePodanoDaty"));
				return;
			}

			try
			{
				DSApi.context().begin();
				documents = new ArrayList<OfficeDocument>();
				FieldsManager fm;
				
				for(int i=0;i<documentIds.length;i++)
				{
					OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);
					fm = document.getDocumentKind().getFieldsManager(document.getId());
					if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND) && fm.getValue(InvoiceLogic.ZAPLACONO_FIELD_CN.toString()).equals("nie"))
							documents.add(OfficeDocument.findOfficeDocument(documentIds[i]));
				}
				/* *
				 * Zaznaczenie pola zaplacono i dodanie daty wypelnienia w dokumencie
				 * poprzez mape
				 * */
				
				Map statusMap = new HashMap<String,Object>();
				statusMap.put(InvoiceLogic.ZAPLACONO_FIELD_CN,true);
				statusMap.put(InvoiceLogic.DATA_ZAPLATY_FIELD_CN,DateUtils.nullSafeParseJsDate(documentDate));
				for(int i=0;i<documents.size();i++)
				{
					documents.get(i).getDocumentKind().setWithHistory(documents.get(i).getId(),statusMap,false);					
				}
				documentIds = null;
				DSApi.context().commit();
			} 
			catch(EdmException e) 
			{
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
			
		}
	}
	
	public Long[] getDocumentIds()
    {
        return documentIds;
    }
	
	public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }
	
	public String getDocumentDate()
	{
		return documentDate;
	}
	
	public void setDocumentDate(String documentDate)
	{
		this.documentDate = documentDate;
	}
	
	public List<OfficeDocument> getDocs() {
		return  this.docs;
	}
	
	public String getTab() {
		return this.tab;
	}
	
	public void setTab(String tab) {
		this.tab = tab;
	}
	
}