package pl.compan.docusafe.web.office.tasklist.calendar;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.bookmarks.TasklistBookmarkTab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.tasklist.TaskListTabAction;
import pl.compan.docusafe.web.office.tasklist.TasklistTab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

@SuppressWarnings("serial")
public class CalendarAction extends EventActionSupport implements TaskListTabAction {
	private final static Logger LOG = LoggerFactory.getLogger(CalendarAjaxAction.class);
	private Tabs tabs;
	private String tab;
	private boolean fromBookmarks;

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION).
		append(OPEN_HIBERNATE_AND_JBPM_SESSION).
		append(new PrepareTabs(this)).
		append(new FillForm()).
		appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
    private class PrepareTabs implements ActionListener
    {
    	private TaskListTabAction thisClass;
        public PrepareTabs(TaskListTabAction action) 
        {
        	thisClass = action;
		}

		public void actionPerformed(ActionEvent event)
        {
			if (fromBookmarks)
				new TasklistBookmarkTab(thisClass).initTab();
			else
				new TasklistTab(thisClass).initTab();           
        }
    }

	private class FillForm extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			// TODO Auto-generated method stub

		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

	}
	

	public Boolean getAscending() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getBaseLink() {
		return "/office/tasklist/current-user-task-list.action";
	}

	public String getOrderTab() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSortField() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTab() {
		// TODO Auto-generated method stub
		return tab;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isOwnTasks() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setOrderTab(Tabs orderTabs) {
		// TODO Auto-generated method stub
		
	}

	public void setTabs(Tabs tabs) {
		this.tabs = tabs;
		
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	public Tabs getTabs() {
		return tabs;
	}

	public Long getBookmarkId() {
		return null;
	}

	public void setBookmarkId(Long id) {
		
	}

	public boolean isFromBookmarks() {
		return fromBookmarks;
	}

	public void setFromBookmarks(boolean fromBookmarks) {
		this.fromBookmarks = fromBookmarks;
	}

	public boolean isCalendarTab() {
		// TODO Auto-generated method stub
		return true;
	}
	
}