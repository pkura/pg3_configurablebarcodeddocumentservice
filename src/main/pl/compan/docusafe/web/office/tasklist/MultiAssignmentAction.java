package pl.compan.docusafe.web.office.tasklist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.workflow.PlannedAssignment;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;

/* User: Administrator, Date: 2005-11-14 14:12:04 */

/**
 * Dekretacja tylko do jednej osoby od razu ze strony
 * Przy u�ytkownikach checkboksy, wyb�r typu dekretacji.
 *
 * Chodzenie po dzia�ach, wyb�r konkretnego u�ytkownika i
 * rodzaju dekretacji.
 * Jednak planowanie dekretacji, jak w zak�adce "Dekretacja"
 * Mo�liwo�� zaplanowania tylko jednej dekretacji.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MultiAssignmentAction.java,v 1.45 2009/02/03 14:27:46 pecet1 Exp $
 */
public class MultiAssignmentAction extends EventActionSupport
{
    private String[] activityIds;
    private String divisionGuid;
    private String treeHtml;
    private List users;
    private String divisionPrettyPath;
    private DSDivision targetDivision;
    private boolean canAssign;
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
    
    /**
     * Lista obiekt�w {@link pl.compan.docusafe.web.office.common.PlannedAssignment} u�ywana w JSP.
     */
    private List plannedAssignmentBeans;
    /**
     * Lista napis�w (kt�re mog� by� u�yte jako parametr id w konstruktorze
     * {@link pl.compan.docusafe.web.office.common.PlannedAssignment} okre�laj�cych list� planowanych dekretacji.
     * Lista u�ywana do utworzenia listy p�l input type=hidden w JSP.
     */
    private String[] plannedAssignments;
    /**
     * Flaga okre�laj�ca, �e na li�cie przygotowanych dekretacji znajduje
     * si� ju� dekretacja "do realizacji", wi�c dodanie kolejnej tego typu
     * spowoduje usuni�cie poprzedniej.
     */
    private boolean hasExecutePlanned;
    private String newPlannedAssignment;
    private String onePlannedAssignment;
    /**
     * Zaznaczone dekretacje.
     */
    private String[] plannedAssignmentIds;
    private String tab;

	private List<DSUser> allUsers;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPlanAssignment").
            append(OpenHibernateSession.INSTANCE).
            append(new PlanAssignment()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualPush").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualPush()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

	/**
	 * @return the allUsers
	 */
	public List<DSUser> getAllUsers() {
		return allUsers;
	}

	/**
	 * @param allUsers the allUsers to set
	 */
	public void setAllUsers(List<DSUser> allUsers) {
		this.allUsers = allUsers;
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(smG.getString("NieWybranoDokumentow"));
                return;
            }

            divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid);

            try
            {
				allUsers = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

                // sprawdzenie, czy wybrane pisma mo�na dekretowa�
                if (plannedAssignments != null)
                {
                    // tworz� list�, aby usuwa� elementy powoduj�ce problemy

                    plannedAssignmentBeans = new ArrayList(plannedAssignments.length);
                    List plan = new ArrayList(Arrays.asList(plannedAssignments));

                    for (Iterator iter=plan.iterator(); iter.hasNext(); )
                    {
                        String id = ((String) iter.next()).trim();
                        try
                        {
                            PlannedAssignment pa = new PlannedAssignment(id);
                            plannedAssignmentBeans.add(pa);

                            if (PlannedAssignment.KIND_EXECUTE.equals(pa.getKind()))
                                hasExecutePlanned = true;
                        }
                        catch (EdmException e)
                        {
                            addActionError(e.getMessage());
                        }
                    }

                    plannedAssignments = (String[]) plan.toArray(new String[plan.size()]);
                }

                canAssign = DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA);

                if (divisionGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(divisionGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                // przekierowanie do dzia�u, w kt�rym jest u�ytkownik
                if (targetDivision == null)
                {
                    DSDivision[] divisions =
                        DSApi.context().getDSUser().getDivisions();
                    for (int i=0; i < divisions.length; i++)
                    {
                        if (!divisions[i].isGroup() && !divisions[i].isSwimlane())
                        {
                            targetDivision = divisions[i];
                            break;
                        }
                    }
                }

                // domy�lnie - dzia� g��wny
                if (targetDivision == null)
                {
                    targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }

				final OrganizationUrlProvider provider = new OrganizationUrlProvider() {
					public String getUrl(DSDivision div, DSUser user) {
						try {
							return "javascript:post(\"" + div.getGuid() + "\");";
						} catch (Exception e) {
							event.getLog().error(e.getMessage(), e);
							return null;
						}
					}
				};

				final pl.compan.docusafe.web.tree.HtmlTree tree = ExtendedOrganizationTree.
					createAssignmentTree(targetDivision,
							null,
							hasExecutePlanned,
							null,
							provider,
							ServletActionContext.getRequest().getContextPath());

                treeHtml = tree.generateTree();

                // lista u�ytkownik�w w dziale
                if (targetDivision != null && !targetDivision.isRoot())
                {
                    //final User[] divisionUsers = DSApi.context().getOrganizationHelper().getUsersInDivision(divisionGuid);
                    final DSUser[] divisionUsers = targetDivision.getUsers();

                    users = new ArrayList(divisionUsers.length);

                    for (int i=0, n=divisionUsers.length; i < n; i++)
                    {
                        if (divisionUsers[i].isDeleted())
                            continue;
                            //|| divisionUsers[i].getName().equals(DSApi.context().getDSUser().getName()))

                        Map bean = new HashMap();

                        bean.put("description", divisionUsers[i].asFirstnameLastname());
                        bean.put("name", divisionUsers[i].getName());

                        if (DSApi.context().hasPermission(DSPermission.PODGLAD_LICZBY_ZADAN_WSZEDZIE) ||
                            (DSApi.context().hasPermission(DSPermission.PODGLAD_LICZBY_ZADAN_KOMORKA) &&
                             DSApi.context().getDSUser().sharesDivision(divisionUsers[i])))
                        {
                            int[] taskCount =
                                ((TaskList) ServiceManager.getService(TaskList.NAME)).
                                    getTaskCount(divisionUsers[i]);
                            bean.put("workingTasks", new Integer(taskCount[0]));
                            bean.put("awaitingTasks", new Integer(taskCount[1]));
                            bean.put("assignedCases", new Integer(OfficeCase.countUserCases(divisionUsers[i].getName(), true)));

                            bean.put("canShowTasks", Boolean.TRUE);
                        }

                        users.add(bean);
                    }

                    divisionPrettyPath = targetDivision.getPrettyPath();
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Dodawanie opisu nowej dekretacji do listy przygotowanych
     * dekretacji.
     */
    private class PlanAssignment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            List plan = new ArrayList(plannedAssignments != null ?
                Arrays.asList(plannedAssignments) : new ArrayList());
            

            if (!StringUtils.isEmpty(newPlannedAssignment) &&
                !plan.contains(newPlannedAssignment))
            {
                try
                {
                    PlannedAssignment npa = new PlannedAssignment(newPlannedAssignment);

                    // je�eli dodano dekretacj� "do wykonania", nale�y
                    // usun�� z listy wszystkie inne dekretacje tego typu
                    if (npa.getKind().equals(PlannedAssignment.KIND_EXECUTE))
                    {
                        for (Iterator iter=plan.iterator(); iter.hasNext(); )
                        {
                            PlannedAssignment pa = new PlannedAssignment((String) iter.next());
                            if (pa.getKind().equals(PlannedAssignment.KIND_EXECUTE))
                            {
                                iter.remove();
                                // na li�cie mo�e by� tylko jedna
                                // dekretacja "do realizacji"
                                if (pa.getUser() != null) {
									addActionMessage(sm.getString(
											"ZlistyDekretacjiUsunietoDekretacjeNaUzytkownikaWponiewazNaLiscieMozeZnalezcSieTylkoJednaDekretacja",
											pa.getUser().asFirstnameLastname(),
											(pa.getDivision() != null ? pa.getDivision().getPrettyPath() : ""),
											pa.getKindDescription()));
								} else {
									addActionMessage(sm.getString(
											"ZlistyDekretacjiUsunietoDekretacjeNa" +
											"DzialPoniewazNaLiscieMozeZnalezcSieTylkoJednaDekretacja",
											pa.getDivision().getPrettyPath(),
											pa.getKindDescription()));
								}
                            }
                            
                        }
                    }

                    plan.add(newPlannedAssignment);
                    plannedAssignments = (String[]) plan.toArray(new String[plan.size()]);
                    
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    private class ManualPush implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {            
            if ((plannedAssignments == null || plannedAssignments.length == 0) &&
                StringUtils.isEmpty(onePlannedAssignment))
            {
                addActionError(sm.getString("NieZaplanowanoZadnejDekretacji"));
                return;
            }

            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(smG.getString("NieWybranoDokumentow"));
                return;
            }

            try
            {
                DSApi.context().begin();
                DSUser curUser = DSApi.context().getDSUser();
                WorkflowFactory.multiAssignment(activityIds, curUser.getName(), plannedAssignments, onePlannedAssignment, event);
                event.setResult("confirm");
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    
    /**
     * Usuwanie zaplanowanych dekretacji.
     */
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (plannedAssignmentIds != null &&
                plannedAssignmentIds.length > 0)
            {
                List plan = new ArrayList(plannedAssignments != null ?
                    Arrays.asList(plannedAssignments) : Collections.EMPTY_LIST);

                for (int i=0; i < plannedAssignmentIds.length; i++)
                {
                    plan.remove(plannedAssignmentIds[i]);
                }

                plannedAssignments = (String[]) plan.toArray(new String[plan.size()]);
            }
            else
            {
                addActionError(sm.getString("NieZaznaczonoDekretacjiDoUsuniecia"));
            }
        }
    }

    public String[] getActivityIds()
    {
        return activityIds;
    }

    public void setActivityIds(String[] activityIds)
    {
        this.activityIds = activityIds;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public List getUsers()
    {
        return users;
    }

    public String getDivisionPrettyPath()
    {
        return divisionPrettyPath;
    }

    public DSDivision getTargetDivision()
    {
        return targetDivision;
    }

    public List getPlannedAssignmentBeans()
    {
        return plannedAssignmentBeans;
    }

    public void setPlannedAssignments(String[] plannedAssignments)
    {
        this.plannedAssignments = plannedAssignments;
    }

    public String[] getPlannedAssignments()
    {
        return plannedAssignments;
    }

    public boolean isHasExecutePlanned()
    {
        return hasExecutePlanned;
    }

    public boolean isCanAssign()
    {
        return canAssign;
    }

    public void setNewPlannedAssignment(String newPlannedAssignment)
    {
        this.newPlannedAssignment = newPlannedAssignment;
    }

    public void setOnePlannedAssignment(String onePlannedAssignment)
    {
        this.onePlannedAssignment = onePlannedAssignment;
    }

    public void setPlannedAssignmentIds(String[] plannedAssignmentIds)
    {
        this.plannedAssignmentIds = plannedAssignmentIds;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }
}
