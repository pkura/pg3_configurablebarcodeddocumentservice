package pl.compan.docusafe.web.office.tasklist;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.bookmarks.TasklistBookmarkTab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * User: Tomasz
 * Date: 07.02.14
 * Time: 09:24
 */
public class AssignmentJournalAction extends EventActionSupport implements TaskListTabAction {

    private static final Logger LOG = LoggerFactory.getLogger(AssignmentJournalAction.class);
    public static final String TYPE_IN = "in";
    public static final String TYPE_OUT = "out";
    private String type;
    private String fromDate;
    private List<AssignmentJournalEntry> entries;

    protected String tab;
    private Long bookmarkId;
    protected Boolean ascending;
    private Tabs tabs;
    private Tabs orderTabs;
    protected String orderTab;
    private String username;
    protected String sortField;


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs(this)).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeDate").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs(this)).
            append(new ChangeDate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    public class AssignmentJournalEntry {
        private Long documentId;
        private String actionDate;
        private String documentTitle;
        private String targetUser;
        private String targetDivision;

        public AssignmentJournalEntry(Long documentId, String actionDate, String documentTitle, String targetUser, String targetDivision) {
            this.documentId = documentId;
            this.actionDate = actionDate;
            this.documentTitle = documentTitle;
            this.targetUser = targetUser;
            this.targetDivision = targetDivision;
        }

        public Long getDocumentId() {
            return documentId;
        }

        public String getActionDate() {
            return actionDate;
        }

        public String getDocumentTitle() {
            return documentTitle;
        }

        public String getTargetUser() {
            return targetUser;
        }

        public String getTargetDivision() {
            return targetDivision;
        }
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            LoggerFactory.getLogger("tomekl").debug("AssignmentJournalAction FillForm");
            if(type == null || type.length() < 1) {
                type = TYPE_IN;
            }

            String sql = "select ah.document_id, ah.ctime, doc.TITLE, u.FIRSTNAME, u.LASTNAME, d.NAME from ds_document doc " +
                    "left join dso_document_asgn_history ah on ah.document_id = doc.id " +
                    "left join ds_user u on u.NAME = ah.targetuser " +
                    "left join DS_DIVISION d on d.GUID = ah.targetguid " +
                    "where ah.sourceuser = ? " +
                    "and ah.cdate = ? ";

            if(fromDate == null || fromDate.length() < 1) {
                fromDate = DateUtils.formatJsDate(new Date());
            }

            PreparedStatement ps = null;
            ResultSet rs = null;
            try {

                if(TYPE_IN.equals(type)) {
                    sql += "and ah.type in (120)";

                } else {
                    sql += "and ah.type in (100,110,115,130)";
                }

                entries = new ArrayList<AssignmentJournalEntry>();

                Date d = DateUtils.parseJsDate(fromDate);
                java.sql.Date sd = new java.sql.Date(d.getTime());

                ps = DSApi.context().prepareStatement(sql);
                ps.setString(1,DSApi.context().getPrincipalName());
                ps.setDate(2, sd);
                rs = ps.executeQuery();

                while(rs.next()) {
                	entries.add(new AssignmentJournalEntry(rs.getLong(1), rs.getDate(2) != null ? DateUtils.formatJsDateTime(rs.getTimestamp(2)) : "", rs.getString(3), rs.getString(4) + " " + rs.getString(5), rs.getString(6)));
                }

            } catch (Exception e) {
                LOG.debug(e.getMessage(),e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(ps);
            }
        }
    }

    private class ChangeDate implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            LoggerFactory.getLogger("tomekl").debug("AssignmentJournalAction ChangeDate {}",fromDate);
        }
    }

    private class PrepareTabs implements ActionListener
    {
        private TaskListTabAction thisClass;
        public PrepareTabs(TaskListTabAction action)
        {
            thisClass = action;
        }

        public void actionPerformed(ActionEvent event)
        {
            if(TYPE_IN.equals(type)) {
                tab = "assignmentJournalIn";
            } else {
                tab = "assignmentJournalOut";
            }

            if (isBookmarkTasklist())
                new TasklistBookmarkTab(thisClass).initTab();
            else
                new TasklistTab(thisClass).initTab();
        }
    }

    public boolean isBookmarkTasklist() {
        return false;
    }

    @Override
    public String getTab() {
        return this.tab;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void setOrderTab(Tabs orderTabs) {
        this.orderTabs = orderTabs;
    }

    public String getOrderTab() {
        return orderTab;
    }

    public void setOrderTab(String orderTab) {
        this.orderTab = orderTab;
    }

    public Tabs getOrderTabs() {
        return orderTabs;
    }

    public void setOrderTabs(Tabs orderTabs) {
        this.orderTabs = orderTabs;
    }

    public Tabs getTabs() {
        return tabs;
    }

    public void setTabs(Tabs tabs) {
        this.tabs = tabs;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

    public Long getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(Long bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    public String getBaseLink() {
        return "/office/tasklist/current-user-task-list.action";
    }

    @Override
    public boolean isOwnTasks() {
        return false;
    }

    @Override
    public void setTab(String tab) {
        this.tab = tab;
    }

    @Override
    public boolean isCalendarTab() {
        return false;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public List<AssignmentJournalEntry> getEntries() {
        return entries;
    }
}
