package pl.compan.docusafe.web.office.tasklist;

import com.beust.jcommander.internal.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.hibernate.mapping.Column;
import org.hibernate.type.Type;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import std.fun;
import std.lambda;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Klasa pomocnicza do wyizolowania mechanizmu budowania tasklisty u�ytkownika
 */
public class TaskListHelper {

    private static final Logger log = LoggerFactory.getLogger(TaskListHelper.class);
    static StringManager sm = GlobalPreferences.loadPropertiesFile(TaskListHelper.class.getPackage().getName(), null);

    public static TaskListHelper instance() {
        return new TaskListHelper();
    }

    /**
     * Zwraca zadania u�ytkownika lub jednostki organizacyjnej
     * @param username nazwa usera i giud jednostki
     * @param offset
     * @return
     * @throws EdmException
     */
    public List<Map<String, String>> getTasks(String username, Integer offset) throws EdmException{
        String userOrGuid = getUserNameOrGuid(username);

        TaskListAction action = new AnyUserTaskListAction();
        action.setUsername(userOrGuid);
        action.setOffset(offset);

        action.initAttributes();

        buildTaskList(action);

        List<Map<String, String>> results = action.taskListToViewMap();
        log.error("size: {}", results.size());
        return results;
    }

    private String getUserNameOrGuid(String username) throws EdmException {
        String userOrGuid = null;
        try{
            userOrGuid = DSUser.findByIdOrNameOrExternalName(username).getName();
        }catch(UserNotFoundException e){
        	log.error("Error connected with fetch user {}", e.getMessage());
        	throw new EdmException(e);
        }

        if(StringUtils.isBlank(userOrGuid))
            userOrGuid = DSDivision.find(username).getGuid();
        return userOrGuid;
    }

    public void buildTaskList(final TaskListAction action) {
        action.LIMIT = action.getDefaultLimit();

        if (!action.updatedone) {
            try {
                // new UpdateRWAs();
            } catch (Exception e) {
            }
            action.updatedone = true;
        }

        if (action.addToTask)
            action.addActionMessage(sm.getString("PismaDodanoDoSprawy") + action.getCaseName());

        // try{DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));}catch(Exception
        // e){}

        try {
            action.multiDepartmentEnroll = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
        } catch (EdmException e1) {
            action.multiDepartmentEnroll = false;
        }
        // .userPreferences().node("other").getBoolean("multiDepartmentEnroll",
        // false);

        // ten kod dodaje do combobox-a (tego na samym dole obok przycisku
        // wykonaj na zaznaczonych)
        // dekretacje do osob pobranych ze zbiorczej dekretacji uzytkownika.
        try {
            action.smallAssignments = new ArrayList<TaskListAction.SmallAssignmentBean>();
            Preferences prefs = DSApi.context().userPreferences().node("collectiveassignment");
            String[] keys = prefs.keys();
            Arrays.sort(keys, new Comparator() {
                public int compare(Object o1, Object o2) {
                    return Integer.parseInt(o1.toString())
                            - Integer.parseInt(o2.toString());
                }
            });
            for (int i = 0; i < keys.length; i++) {
                AssignmentDescriptor ad = AssignmentDescriptor.forDescriptor(prefs.get(keys[i], ""));

                DSUser tmpuser = null;
                DSDivision tmpdivision = null;
                if (ad.getUsername() != null) {
                    tmpuser = DSUser.findByUsername(ad.getUsername());
                }
                if (ad.getGuid() != null) {
                    tmpdivision = DSDivision.find(ad.getGuid());
                }

                if (tmpuser != null || tmpdivision != null)
                    action.smallAssignments.add(action.new SmallAssignmentBean(tmpuser, tmpdivision));
            }
        } catch (BackingStoreException bse) {
            log.error(bse.getMessage(), bse);
        } catch (EdmException ee) {
            log.error(ee.getMessage(), ee);
        }

        // ten kod dodaje do combobox-a (tego na samym dole obok przycisku
        // wykonaj na zaznaczonych)
        // dzienniki do zbiorczego przyjecia pisma w wydziale

        boolean viewchanged = false;
        try {
            List<Journal> docJournals = new ArrayList<Journal>();
            String journalType = "";
            if (action.tab == null) {
                action.tab = action.TAB_IN;
            }
            action.incomingDepartments = new ArrayList<TaskListAction.DepartmentEnrollBean>();
            if (action.tab.equals(action.TAB_INT)) {
                journalType = Journal.INTERNAL;
                action.urlTabString = "internal";
            } else if (action.tab.equals(action.TAB_OUT)) {
                journalType = Journal.OUTGOING;
                action.urlTabString = "outgoing";
            } else if (action.tab.equals(action.TAB_IN)) {
                journalType = Journal.INCOMING;
                action.urlTabString = "incoming";
            }

            if (DSApi.context().hasPermission(
                    DSPermission.PISMO_WSZEDZIE_PRZYJECIE)) {
                docJournals = Journal.findByType(journalType);
            } else {
                boolean przyjeciePismaKomorkaNadrzedna = DSApi.context().hasPermission(
                        DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                for (DSDivision division : DSApi.context().getDSUser()
                        .getDivisions()) {
                    // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1
                    // w dziale)

                    docJournals = Journal.findByDivisionGuid(
                            division.getGuid(), journalType);

                    // dzienniki w kom�rkach nadrz�dnych
                    if (przyjeciePismaKomorkaNadrzedna) {
                        DSDivision parent = division.getParent();
                        while (parent != null && !parent.isRoot()) {
                            // TODO: nie jest pokazywana nazwa dziennika
                            // (je�eli > 1 w dziale)
                            List docParentJournals = Journal
                                    .findByDivisionGuid(parent.getGuid(),
                                            journalType);
                            docJournals.addAll(docParentJournals);
                            try {
                                parent = parent.getParent();
                            } catch (ReferenceToHimselfException re) {
                                log.error("", re);
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < docJournals.size(); i++) {
                action.incomingDepartments.add(action.new DepartmentEnrollBean(docJournals.get(i)));
            }
            ;

            if (action.viewedLabels == null) {
                action.viewedLabels = new Vector<Label>();
            }
            if (action.viewedLabelId != null && action.viewedLabelId > 0L) {
                boolean contains = false;
                for (Label lab : action.viewedLabels) {
                    if (lab.getId().equals(action.viewedLabelId))
                        contains = true;
                }
                if (!contains)
                    action.viewedLabels.add(LabelsManager.findLabelById(action.viewedLabelId));
                viewchanged = true;

            } else if (action.viewedLabelId != null && action.viewedLabelId == -1L) {
                log.trace("0L");
                action.viewedLabels = new Vector<Label>();
            }

            if (AvailabilityManager.isAvailable("labels")) {
                List<Label> flags = LabelsManager
                        .getReadLabelsForUser(DSApi.context()
                                .getPrincipalName(), Label.FLAG_TYPE);
                for (Label lab : flags) {
                    lab.setAliasName(sm.getString("Flaga") + " "
                            + lab.getName());
                }
                action.availableLabels = flags;
                List<Label> labels = LabelsManager.getReadLabelsForUser(
                        DSApi.context().getPrincipalName(),
                        Label.LABEL_TYPE);
                for (Label lab : labels) {
                    lab.setAliasName(lab.getName());
                }
                action.availableLabels.addAll(labels);

                Label l = new Label();
                l.setId(-2L);
                l.setAliasName(sm.getString("Default"));
                action.availableLabels.add(0, l);

                Label ll = new Label();
                ll.setId(-1L);
                ll.setAliasName(sm.getString("Wszystkie"));
                action.availableLabels.add(ll);
                action.availableForAddLabels = LabelsManager
                        .getWriteLabelsForUser(DSApi.context()
                                .getPrincipalName(), Label.LABEL_TYPE);

                action.userFlags_label = LabelsManager
                        .getPrivateModifyLabelsForUser(DSApi.context()
                                .getPrincipalName(), Label.FLAG_TYPE);
                action.systemFlags_label = LabelsManager
                        .getNonPrivateModifyLabelsForUser(DSApi.context()
                                .getPrincipalName(), Label.FLAG_TYPE);

                action.labels_label = new ArrayList<Label>();
                action.systemLabels_label = new ArrayList<Label>();
                for (Label lab : LabelsManager.getReadLabelsForUser(DSApi
                        .context().getPrincipalName(), Label.LABEL_TYPE)) {
                    if (lab.getModifiable()) {
                        action.labels_label.add(lab);
                    } else {
                        action.systemLabels_label.add(lab);
                    }
                }
                action.labels_label.add(l);
                action.labels_label.add(ll);
                action.systemLabels_label.add(l);
                action.systemLabels_label.add(ll);
            } else {
                action.availableLabels = new Vector<Label>();
                action.labels_label = new Vector<Label>();
                action.systemLabels_label = new Vector<Label>();
            }
        } catch (EdmException ee) {
            log.error("", ee);
        }

        try {
            action.dockindNames = DocumentKind.list(true);
        } catch (EdmException edme) {
        }

        try {
            // lista u�ytkownik�w do wy?wietlenia
            action.users = action.getDSUserList();
            action.usersAndDivisions = action.getDSUserAndDivisions();
            DSUser user = action.getDSUser();
            DSDivision divisionNS = action.getDSDivision();

            int tasksSize = 0;
            boolean isPager = true;// false;

            if (user != null || divisionNS != null) {
                DSUser currentUser = DSApi.context().getDSUser();

                if (user != null) {
                    action.viewingLoggedUser = user.equals(currentUser);

                    // nale�y sprawdzi�, czy bie�?cy u�ytkownik ma prawo do
                    // przegl?dania zada� wybranego u�ytkownika; je�eli
                    // wybrany
                    // u�ytkownik jest na li?cie zwracanej przez
                    // getDSUsers(),
                    // wszystko w porz?dku
                    if (!action.viewingLoggedUser) {
                        if (action.users == null) {
                            return;
                        }

                        boolean ok = false;
                        for (Iterator iter = action.users.iterator(); iter.hasNext(); ) {
                            if (user.equals(iter.next())) {
                                ok = true;
                                break;
                            }
                        }

                        if (!ok) {
                            return;
                        }
                    }
                }

                action.columns = action.getActualColumns();

                action.filterColumns = action.getFilterColumn();

                if (AvailabilityManager
                        .isAvailable("INS.wlacz.walidacjaFiltrowWyszukiwania")) {
                    for (FilterColumn fc : action.filterColumns) {
                        if (fc.getName().equals(action.filterBy)) {
                            action.filterColumnTitle = fc.getTitle();
                        }
                    }
                }
                    /* sprawdzanie czy uzywamy domyslnego sortowania */
                if (action.sortField == null) {
                    action.sortField = action.getDefaultSortField();

                    boolean is = false;
                    for (Iterator iter = action.columns.iterator(); iter.hasNext(); ) {
                        TableColumn column = (TableColumn) iter.next();
                        String field = column.getProperty();
                        if (field.equals(action.sortField))
                            is = true;
                    }
                    if (!is)
                        action.sortField = "receiveDate";
                }

                if (action.ascending == null) {
                    action.ascending = action.getSortOrder();
                }

					/*
					 * needSort <=> konieczne zewnetrzne (czyli "tutaj")
					 * sortowanie
					 */
                boolean needSort = ("userFlags".equals(action.sortField)
                        || "flags".equals(action.sortField)
                        || "sender".equals(action.sortField)
                        || "documentSender".equals(action.sortField)
                        || "receiver".equals(action.sortField)
                        || "orderStatus".equals(action.sortField)
                        || "author".equals(action.sortField) || "clerk"
                        .equals(action.sortField));

                TaskList taskList = (TaskList) ServiceManager
                        .getService(TaskList.NAME);

                Object filterValue = null;

                if (action.filterBy == null)
                    action.filterBy = "Brak";
                if (action.filterBy.equals("Brak"))
                    action.filterName = "";

                String parsedFilterBy = "";

                log.info("filterBy = " + action.filterBy);
                if (action.UNPARSED_FILTER_BY.contains(action.filterBy)) {
                    parsedFilterBy = action.filterBy;

                } else if (action.filterName != null
                        && action.filterBy != null
                        && (AvailabilityManager
                        .isAvailable("watches.showfilter") || !action.TAB_WATCHES
                        .equals(action.tab))
                        && !action.IGNORED_FILTER_BY.contains(action.filterBy)) {
                    log.warn("parsedFilterBy 1: " + parsedFilterBy);
                    action.filterName = action.filterName.trim();
                    parsedFilterBy = action.filterBy;
                    try {
                        if (parsedFilterBy.equals("clerk")) {
                            parsedFilterBy = "documentClerk";
                        } else if (parsedFilterBy.equals("sender")) {
                            parsedFilterBy = "activity_lastActivityUser"; // dekretujacy
                        }
                        TaskSnapshot ts = TaskSnapshot.getInstance();
                        Column dsa = DSApi.getColumn(ts.getClass(),
                                parsedFilterBy);
                        Type t = DSApi.getColumnType(ts.getClass(),
                                parsedFilterBy);
                        parsedFilterBy = dsa.getName();
                        if (parsedFilterBy.equals("documentClerk")
                                || parsedFilterBy.equals("documentAuthor")
                                || parsedFilterBy.equals("document_author")
                                || parsedFilterBy.equals("document_clerk")
                                || parsedFilterBy
                                .equals("dsw_process_context_param_val")) {
                            DSUser tempUser = null;
                            String[] tmp = action.filterName.split(" ");
                            try {
                                tempUser = DSUser.findByFirstnameLastname(
                                        tmp[0], tmp[1]);
                                if (tempUser == null) {
                                    tempUser = DSUser
                                            .findByFirstnameLastname(
                                                    tmp[1], tmp[0]);
                                    if (tempUser != null)
                                        filterValue = tempUser.getName();
                                } else {
                                    filterValue = tempUser.getName();
                                }
                            } catch (Exception e) {
                                log.error(e.getMessage(), e);
                            }
                        } else if (t.getReturnedClass().equals(
                                Integer.class)) {
                            filterValue = Integer.parseInt(action.filterName);
                        } else if (t.getReturnedClass().equals(Date.class)) {
                            filterValue = DateUtils
                                    .parseDateTimeAnyFormat(action.filterName);
                        } else if (t.getReturnedClass().equals(
                                Timestamp.class)) {
                            filterValue = DateUtils
                                    .parseDateAnyFormat(action.filterName);
                        } else if (t.getReturnedClass()
                                .equals(String.class)) {
                            filterValue = action.filterName;
                        } else if (t.getReturnedClass().equals(Float.class)) {
                            filterValue = Float.parseFloat(action.filterName);
                        } else if (t.getReturnedClass().equals(Long.class)) {
                            filterValue = Long.parseLong(action.filterName);
                        } else {
                            filterValue = action.filterName;
                        }
                    } catch (Exception e) {
                        if (AvailabilityManager
                                .isAvailable("INS.wlacz.walidacjaFiltrowWyszukiwania")) {
                            action.addActionError(sm.getString(
                                    "NiePrawidlowaWartosc",
                                    action.filterColumnTitle));
                        }
                        log.error(e.getMessage(), e);
                        action.filterBy = null;
                        filterValue = null;
                    }
                }

                /**
                 * @TODO Ustalamy zakres etykiet
                 */
                Collection<Label> labels = null;
                if (action.viewedLabels != null) {
                    labels = action.viewedLabels;
                } else {
                    action.viewedLabels = new Vector<Label>();
                    labels = action.viewedLabels;
                }

                int tempLimit = action.LIMIT;
                if ("documentId".equals(action.filterBy)
                        || "documentOfficeNumber".equals(action.filterBy))
                    tempLimit = 0;

                // wy�wietl wszystkie labele je�li przegl�da to inna osoba
                // (mo�e nie mie� uprawnie� do wybrania tych labeli)
                // chyba, �e tasklist.otherUserCheck.omit jest w��czone, w
                // takim przypadku tylko admin mo�e podgl�da� wszystko
                boolean otherWatch = (!action.isOwnTasks() && (!Boolean.TRUE
                        .equals(AvailabilityManager
                                .isAvailable("tasklist.otherUserCheck.omit")) || DSApi
                        .context().isAdmin()));
                // log.info("otherWatch = " + otherWatch);
                boolean allLabels = otherWatch
                        || (action.viewedLabelId != null && action.viewedLabelId == -1L);
                // log.info("allLabels = " + allLabels);

                if (action.isBookmarkTasklist()
                        && AvailabilityManager.isAvailable("labels"))
                    action.prepareBookmarksLabels(labels);

                if (action.isBookmarkTasklist())
                    action.filterConditions = action.prepareFilterConditions();

                if (action.TAB_IN.equals(action.tab) && !action.documentPackageAvailabe) {
                    action.onTaskList = true;
                    Map map = Maps.newHashMap();
                    if (needSort) {
                        map = taskList.getDocumentTasks(divisionNS,
                                user, InOfficeDocument.TYPE,
                                action.searchOfficeNumber, action.sortField,
                                action.ascending, action.offset, tempLimit,
                                parsedFilterBy, filterValue,
                                action.withBackup, labels, allLabels,
                                action.filterConditions);
                    } else {
                        try{
                        map = taskList.getInDocumentTasks(divisionNS,
                                user, action.offset, tempLimit,
                                action.searchOfficeNumber, action.sortField,
                                action.ascending, parsedFilterBy, filterValue,
                                action.withBackup, labels, allLabels,
                                action.filterConditions);
                        }catch(Exception e){
                            log.error("", e);
                        }
                    }
                    tasksSize = (Integer) map.get("size");
                    action.tasks = (List<Task>) map.get("tasks");
                    isPager = true;
                } else if (action.TAB_IN.equals(action.tab)
                        && action.documentPackageAvailabe) {
                    action.onTaskList = true;

                    Map map = taskList.getInDocumentTasksWitoutPackage(
                            divisionNS, user, action.offset, tempLimit,
                            action.searchOfficeNumber, action.sortField, action.ascending,
                            parsedFilterBy, filterValue, action.withBackup,
                            labels, allLabels, action.filterConditions);
                    tasksSize = (Integer) map.get("size");
                    action.tasks = (List<Task>) map.get("tasks");
                    action.taskInPackageCount = (Integer) map.get("taskInPackageCount");
                    isPager = true;
                } else if (action.TAB_DOCUMENT_PACKAGE.equals(action.tab)
                        && action.documentPackageAvailabe) {
                    action.onTaskList = true;
                    Map map = taskList.getInDocumentTasksPackage(
                            divisionNS, user, InOfficeDocument.TYPE,
                            action.searchOfficeNumber, action.sortField, action.ascending,
                            action.offset, tempLimit, parsedFilterBy,
                            filterValue, action.withBackup, labels, allLabels,
                            action.filterConditions);
                    tasksSize = ((Integer) map.get("size")).intValue();
                    action.tasks = (List<Task>) map.get("tasks");
                    // taskInPackageCount = (Integer)
                    // map.get("taskInPackageCount");
                    isPager = true;
                } else if (action.TAB_OUT.equals(action.tab) && !action.documentPackageAvailabe) {
                    action.onTaskList = true;
                    Map map = taskList.getDocumentTasks(divisionNS,
                            user, OutOfficeDocument.TYPE,
                            action.searchOfficeNumber, action.sortField, action.ascending,
                            action.offset, tempLimit, parsedFilterBy,
                            filterValue, action.withBackup, labels, allLabels,
                            action.filterConditions);
                    tasksSize = ((Integer) map.get("size")).intValue();
                    action.tasks = (List<Task>) map.get("tasks");
                    isPager = true;
                } else if (action.TAB_OUT.equals(action.tab) && action.documentPackageAvailabe) {
                  action.onTaskList = true;

                    Map map = taskList
                            .getDocumentTasks(
                                    divisionNS, user,
                                    OutOfficeDocument.TYPE,
                                    action.searchOfficeNumber, action.sortField,
                                    action.ascending, action.offset, tempLimit,
                                    parsedFilterBy, filterValue,
                                    action.withBackup, labels, allLabels,
                                    action.filterConditions);
                    tasksSize = ((Integer) map.get("size")).intValue();
                    action.tasks = (List<Task>) map.get("tasks");
                    action.taskInPackageCount = (Integer) map.get("taskInPackageCount");
                    isPager = true;

                } else if (action.TAB_INT.equals(action.tab)) {
                    action.onTaskList = true;
                    Map map = taskList.getDocumentTasks(divisionNS,
                            user, OutOfficeDocument.INTERNAL_TYPE,
                            action.searchOfficeNumber, action.sortField, action.ascending,
                            action.offset, tempLimit, parsedFilterBy,
                            filterValue, action.withBackup, labels, allLabels,
                            action.filterConditions);
                    tasksSize = ((Integer) map.get("size")).intValue();
                    action.tasks = (List<Task>) map.get("tasks");
                    isPager = true;
                } else if (action.TAB_ORDER.equals(action.tab)) {
                    action.onTaskList = false;
                    if (action.TAB_MY_ORDERS.equals(action.orderTab)) {
                        action.tasks = taskList.getUserOrderTasks(divisionNS,
                                user, action.searchOfficeNumber, action.sortField,
                                action.ascending);
                    } else if (action.TAB_WATCH_ORDERS.equals(action.orderTab)) {
                        action.tasks = taskList.getWatchOrderTasks(user,
                                action.searchOfficeNumber, action.sortField,
                                action.ascending);
                    } else {
                        Map map = taskList.getDocumentTasks(divisionNS,
                                user, OfficeOrder.TYPE,
                                action.searchOfficeNumber, action.sortField,
                                action.ascending, action.offset, tempLimit);
                        tasksSize = ((Integer) map.get("size")).intValue();
                        action.tasks = (List<Task>) map.get("tasks");
                    }
                } else if (action.TAB_CASES.equals(action.tab)) {
                    action.onTaskList = false;
                    List<OfficeCase> tempCases = null;
                    if (user == null
                            && DSDivision.find(action.username) != null) {
                        tempCases = OfficeCase
                                .findByDivision(DSDivision.find(action.username));
                    } else {
                        tempCases = OfficeCase.findUserCases(user, action.searchOfficeId);
                    }

                    tasksSize = tempCases.size();
                    action.searchOfficeId = TextUtils
                            .trimmedStringOrNull(action.searchOfficeId);
                    action.cases = fun.map(tempCases,
                            new lambda<OfficeCase, TaskListAction.CaseTask>() {
                                public TaskListAction.CaseTask act(OfficeCase o) {
                                    return new TaskListAction.CaseTask(o);
                                }
                            });
                } else if (action.TAB_WATCHES.equals(action.tab)) {
                    try {
                        Long[] ids = null;
                        if (user == null && divisionNS != null) {
                            action.setOwnWatches(false);
                            ids = action.getWatchesIDs(divisionNS.getUsers());
                        } else {
                            if (user.equals(currentUser)) {
                                action.setOwnWatches(true);
                            } else {
                                action.setOwnWatches(false);
                            }
                            ids = action.getWatchesIDs(user);
                        }
                        action.onTaskList = true;

                        Map map = taskList.getWatchesDocumentTasks(
                                divisionNS, user,
                                InOfficeDocument.TYPE,
                                action.searchOfficeNumber, action.sortField,
                                action.ascending, action.offset, tempLimit,
                                parsedFilterBy, filterValue,
                                action.withBackup, labels, allLabels,
                                action.filterConditions, ids);
                        tasksSize = (Integer) map.get("size");
                        action.tasks = (List<Task>) map.get("tasks");

                    } catch (EdmException ex) {
                        log.warn(ex.getMessage(), ex);
                    } catch (SQLException e) {
                        log.warn(e.getMessage(), e);
                    } catch (Exception e) {
                        log.warn(e.getMessage(), e);
                    }
                    isPager = true;
                    // watches = updateWatches(user);
                    // watches = null;
                    // tasks = updateWatches(user,
                    // parsedFilterBy,filterValue);
                    // tasksSize = tasks.size();
                } else {
                    throw new EdmException(
                            sm.getString("NieznanyRodzajZak�adki")
                                    + ": " + action.tab);
                }

                if (action.tasks != null && action.searchOfficeNumber != null) {
                    // je�eli nie znaleziono szukanego dokumentu,
                    // wy�wietlana
                    // jest kompletna lista
                    if (action.tasks.size() == 0)
                        action.addActionError(sm.getString("NieZnalezionoZadaniaOzadanymNumerzeKO"));
                }

                if (action.cases != null && action.searchOfficeId != null) {
                    if (action.cases.size() == 0)
                        action.addActionError(sm.getString("NieZnalezionoSprawyOzadanymNumerze"));
                }
                // format u�ywany do formatowania liczb, kt�re pos�u�? za
                // klucze sortowania leksykalnego; dodaje na pocz?tku zera
                final NumberFormat millisFormat = NumberFormat
                        .getIntegerInstance();
                millisFormat.setMinimumIntegerDigits(("" + Long.MAX_VALUE)
                        .length());
                millisFormat.setGroupingUsed(false);
                millisFormat.setParseIntegerOnly(true);
                // napis o najmniejszej mo�liwej warto?ci u�ywany zamiast
                // napisu pustego w sortowaniu leksykalnym
                final String MIN_STR = String.valueOf((char) 0);

                // wyrzucanie z tasklisty tych rodzajow dok., ktore nie byly
                // wybrane (filtr)

                if (action.filterDockindNames != null
                        && action.filterDockindNames.length() > 0) {

                    List<Task> filtered_tasks = new ArrayList<Task>();
                    for (Task task : action.tasks)
                        if (task.getDockindName() != null
                                && task.getDockindName().equals(
                                action.filterDockindNames)) {
                            filtered_tasks.add(task);
                            continue;
                        }
                    action.tasks = filtered_tasks;
                }

                // tu jest dobre miejsce na ustlanie SortFielda

                // sortowanie tablicy zada�
                if (action.tasks != null && action.tasks.size() > 1 && needSort) {
                    // sortowanie przenioslem do kontenera
                    action.tasks = action.sortTask(action.tasks);

                    if (action.TAB_IN.equals(action.tab)) {
                        isPager = true;
                        tasksSize = action.tasks.size();
                        int count = 0;
                        List<Task> tasksCopy = new ArrayList<Task>(action.tasks);
                        for (Iterator iter = tasksCopy.iterator(); iter
                                .hasNext(); ) {
                            Task task = (Task) iter.next();

                            if (count < action.offset || count >= action.offset + action.LIMIT)
                                iter.remove();
                            count++;
                        }
                        action.tasks = tasksCopy;
                    }
                }
                // sortowanie tablicy spraw
                else if (action.cases != null && action.cases.size() > 1) {
                    List<String> sortKeys = new ArrayList<String>(action.cases.size());

                    for (Iterator iter = action.cases.iterator(); iter.hasNext(); ) {
                        TaskListAction.CaseTask task = (TaskListAction.CaseTask) iter.next();
                        String key;
                        String oidKey = task.getCaseOfficeId()
                                .toUpperCase();

                        if ("caseStatus".equals(action.sortField)) {
                            key = task.getCaseStatus() != null ? task
                                    .getCaseStatus().toUpperCase()
                                    : MIN_STR;
                        } else if ("caseOpenDate".equals(action.sortField)) {
                            key = millisFormat.format(task
                                    .getCaseOpenDate() != null ? task
                                    .getCaseOpenDate().getTime() : 0);
                        } else if ("caseFinishDate".equals(action.sortField)) {
                            key = millisFormat.format(task
                                    .getCaseFinishDate() != null ? task
                                    .getCaseFinishDate().getTime() : 0);
                        } else if ("caseDaysToFinish".equals(action.sortField)) {
                            int daysToFinish = 0;
                            if (task.getCaseDaysToFinish() != null)
                                daysToFinish = task.getCaseDaysToFinish()
                                        .intValue();
                            if (daysToFinish < 0) {
                                daysToFinish = Integer.MIN_VALUE
                                        - daysToFinish;
                            }
                            key = millisFormat.format(daysToFinish);
                        } else if ("caseTitle".equals(action.sortField)) {
                            key = (task.getCaseTitle() != null) ? task
                                    .getCaseTitle().toUpperCase() : "";
                        } else {
                            key = oidKey;
                        }

                        // dodawanie pobocznego klucza sortowania (officeId)
                        key += ("<>" + oidKey);

                        sortKeys.add(key);
                    }

                    TaskListAction.CaseTask[] taskArray = (TaskListAction.CaseTask[]) action.cases
                            .toArray(new TaskListAction.CaseTask[action.cases.size()]);
                    std.sort.qsort((String[]) sortKeys
                            .toArray(new String[sortKeys.size()]),
                            taskArray, action.ascending);

                    action.cases = Arrays.asList(taskArray);
                }
                // sortowanie pism obserwowanych
                else if (action.TAB_WATCHES.equals(action.tab) && action.tasks != null
                        && action.tasks.size() > 0) {
                    List<String> sortKeys = new ArrayList<String>(action.tasks.size());
                    for (Iterator iter = action.tasks.iterator(); iter.hasNext(); ) {
                        TaskSnapshot task = (TaskSnapshot) iter.next();
                        String key;
                        String onKey = task.getDocumentOfficeNumber() != null ? millisFormat
                                .format(task.getDocumentOfficeNumber())
                                : millisFormat.format(0);

                        if ("documentCtime".equals(action.sortField)) {
                            key = millisFormat.format(task
                                    .getDocumentCtime() != null ? task
                                    .getDocumentCtime().getTime() : 0);
                        } else if ("dockindNumerDokumentu"
                                .equals(action.sortField)) {
                            key = task.getDockindNumerDokumentu() != null ? task
                                    .getDockindNumerDokumentu()
                                    .toUpperCase() : MIN_STR;
                        } else if ("dockindBusinessAtr1".equals(action.sortField)) {
                            key = task.getDockindBusinessAtr1() != null ? task
                                    .getDockindBusinessAtr1().toUpperCase()
                                    : MIN_STR;
                        } else if ("dockindBusinessAtr2".equals(action.sortField)) {
                            key = task.getDockindBusinessAtr2() != null ? task
                                    .getDockindBusinessAtr2().toUpperCase()
                                    : MIN_STR;
                        } else if ("dockindBusinessAtr3".equals(action.sortField)) {
                            key = task.getDockindBusinessAtr3() != null ? task
                                    .getDockindBusinessAtr3().toUpperCase()
                                    : MIN_STR;
                        } else if ("dockindBusinessAtr4".equals(action.sortField)) {
                            key = task.getDockindBusinessAtr4() != null ? task
                                    .getDockindBusinessAtr4().toUpperCase()
                                    : MIN_STR;
                        } else if ("receiveDate".equals(action.sortField)) {
                            key = millisFormat
                                    .format(task.getReceiveDate() != null ? task
                                            .getReceiveDate().getTime() : 0);
                        } else if ("sender".equals(action.sortField)) {
                            key = task.getSender() != null ? task
                                    .getSender().toUpperCase() : MIN_STR;
                        } else if ("deadlineTime".equals(action.sortField)) {
                            key = millisFormat.format(task
                                    .getDeadlineTime() != null ? task
                                    .getDeadlineTime().getTime() : 0);
                        } else if ("description".equals(action.sortField)) {
                            key = task.getDescription() != null ? task
                                    .getDescription().toUpperCase()
                                    : MIN_STR;
                        } else if ("objective".equals(action.sortField)) {
                            key = task.getObjective() != null ? task
                                    .getObjective().toUpperCase() : MIN_STR;
                        } else if ("documentSummary".equals(action.sortField)) {
                            key = task.getDocumentSummary() != null ? task
                                    .getDocumentSummary().toUpperCase()
                                    : MIN_STR;
                        } else if ("dockindStatus".equals(action.sortField)) {
                            key = task.getDockindStatus() != null ? task
                                    .getDockindStatus().toUpperCase()
                                    : MIN_STR;
                        } else if ("documentId".equals(action.sortField)) {
                            key = task.getDocumentId() != null ? task
                                    .getDocumentId().toString()
                                    .toUpperCase() : MIN_STR;
                        } else // documentOfficeNumber
                        {
                            key = onKey;
                        }

                        // na ko�cu klucza zawsze numer kancelaryjny,
                        // poniewa�
                        // jest to poboczny klucz sortowania
                        key += ("<>" + onKey);
                        sortKeys.add(key);
                    }

                    Task[] taskArray = (Task[]) action.tasks
                            .toArray(new Task[action.tasks.size()]);
                    std.sort.qsort((String[]) sortKeys
                            .toArray(new String[sortKeys.size()]),
                            taskArray, action.ascending);

                    action.tasks = Arrays.asList(taskArray);
                }
            }
            if (action.TAB_IN.equals(action.tab)) {
                action.actionReplyUnnecessary = Configuration
                        .isAdditionOn(Configuration.ADDITION_DO_NOT_REQUIRE_RESPOND);
            }
            if (DSApi.context().hasPermission(
                    DSPermission.ZADANIE_KONCZENIE))
                action.actionManualFinish = true;
            if ((action.TAB_IN.equals(action.tab) || action.TAB_OUT.equals(action.tab) || action.TAB_INT
                    .equals(action.tab))
                    && (!Configuration.hasExtra("business") || AvailabilityManager
                    .isAvailable("sprawy")))
                action.actionAddToCase = true;
            if (Configuration.hasExtra("business")
                    && AvailabilityManager
                    .isAvailable("actionAttachmentsAsPdf.available"))
                action.actionAttachmentsAsPdf = true;
            if (AvailabilityManager
                    .isAvailable("actionPrintEnvelopes.available"))
                action.actionPrintEnvelopes = true;
            if ((action.TAB_OUT.equals(action.tab) && DSApi.context().hasPermission(
                    DSPermission.PISMO_WYCH_PRZYJECIE_KO)))
                action.actionAssignOfficeNumber = true;

            if(AvailabilityManager.isAvailable("usun.ko") && (action.TAB_OUT.equals(action.tab) && DSApi.context().hasPermission(
                    DSPermission.PISMO_WYCH_USUNIECIE_KO))) {
                action.actionDeleteOfficeNumber = true;
            }

            if (DSApi.context().hasPermission(
                    DSPermission.WWF_DEKRETACJA_DOWOLNA))
                action.actionMultiAssignment = true;
            action.actionDepartmentEnroll = true;
            action.actionAssignMe = (action.username != null)
                    && (!action.username
                    .equals(DSApi.context().getPrincipalName()));
            if (Docusafe.hasExtra("business")
                    && AvailabilityManager
                    .isAvailable("addition.box.available")) {
                action.actionMultiBoxing = true;
            }
            if (AvailabilityManager.isAvailable("taskList.multiNote")) {
                action.actionMultiNote = true;
            }


            if (action.tasks != null && isPager) {
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
                    public String getLink(int offset) {
                        if (action.isBookmarkTasklist()) {
                            String l = action.getBaseLink().substring(0,
                                    action.getBaseLink().indexOf("?"));
                            return HttpUtils
                                    .makeUrl(
                                            l,
                                            new Object[]{
                                                    "bookmarkId",
                                                    action.getBookmarkId(),
                                                    "tab",
                                                    action.tab,
                                                    "sortField",
                                                    action.sortField,
                                                    "ascending",
                                                    String.valueOf(action.ascending),
                                                    "username",
                                                    action.username,
                                                    "withBackup",
                                                    action.withBackup,
                                                    "filterBy",
                                                    action.filterBy,
                                                    "filterName",
                                                    action.filterName == null
                                                            || (action.filterName != null && action.filterName
                                                            .equalsIgnoreCase("null")) ? ""
                                                            : action.filterName,
                                                    "filterDockindNames",
                                                    action.filterDockindNames,
                                                    "offset",
                                                    String.valueOf(offset)});
                        }

                        return HttpUtils
                                .makeUrl(
                                        action.getBaseLink(),
                                        new Object[]{
                                                "tab",
                                                action.tab,
                                                "sortField",
                                                action.sortField,
                                                "ascending",
                                                String.valueOf(action.ascending),
                                                "username",
                                                action.username,
                                                "withBackup",
                                                action.withBackup,
                                                "filterBy",
                                                action.filterBy,
                                                "filterName",
                                                action.filterName == null
                                                        || (action.filterName != null && action.filterName
                                                        .equalsIgnoreCase("null")) ? ""
                                                        : action.filterName,
                                                "filterDockindNames",
                                                action.filterDockindNames,
                                                "offset",
                                                String.valueOf(offset)});
                    }
                };
                action.pager = new Pager(linkVisitor, action.offset, action.LIMIT, tasksSize, 10);
            }
            action.taskCount = tasksSize;
        } catch (EdmException e) {
            action.addActionError(e.getMessage());
            log.error(e.getMessage(), e);
        }
        if (action.message != null) {
            action.addActionMessage(sm.getString(action.message));
        }
        if (action.getError() != null) {
            action.addActionError(sm.getString(action.getError()));
        }
    }
}
