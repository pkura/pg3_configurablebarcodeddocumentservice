package pl.compan.docusafe.web.office.tasklist;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.HibernateException;
import org.hibernate.mapping.Column;
import org.hibernate.type.Type;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DockindButtonField;
import pl.compan.docusafe.core.dockinds.field.DocumentJournalField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.names.DocumentURN;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.names.WfActivityURN;
import pl.compan.docusafe.core.names.WfProcessURN;
import pl.compan.docusafe.core.names.WorkflowActivityURN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ActivityId;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshotXml;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmWorkflowService;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.Task.FlagBean;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.bookmarks.TasklistBookmarkTab;
import pl.compan.docusafe.web.common.Links;
import pl.compan.docusafe.web.common.SkipNullify;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.AjaxSerializeActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;
import std.pair;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Akcja wy�wietlaj�ca list� zada�. Zadania workflow s� pobierane z us�ugi
 * TaskList, natomiast sprawy odczytywane przez OfficeCase.find.
 * 
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskListAction.java,v 1.380 2010/08/17 10:56:55 mariuszk Exp $
 */
@SkipNullify(attributes = { "urns" })
public abstract class TaskListAction extends EventActionSupport implements
		DockindButtonAction, TaskListTabAction {
	public StringManager sm = GlobalPreferences.loadPropertiesFile(this
			.getClass().getPackage().getName(), null);
	private StringManager smTab;
	private static final Logger log = LoggerFactory
			.getLogger(TaskListAction.class);

	public static final String TAB_IN = InOfficeDocument.TYPE;
	public static final String TAB_OUT = OutOfficeDocument.TYPE;
	public static final String TAB_INT = OutOfficeDocument.INTERNAL_TYPE;
	public static final String TAB_ORDER = OfficeOrder.TYPE;
	public static final String TAB_CASES = "cases";
	public static final String TAB_WATCHES = "watches";
	public static final String TAB_CALENDAR = "calendar";

	public static final String TAB_MY_ORDERS = "my_order";
	public static final String TAB_REALIZE_ORDERS = "realize_order";
	public static final String TAB_WATCH_ORDERS = "watch_order";
	public static final String TAB_DOCUMENT_PACKAGE = "package";

	/** warto�ci w filterby, kt�re ignorujemy */
	public final static Set<String> UNPARSED_FILTER_BY = Sets.newHashSet(Arrays
			.asList("task_count_1", "task_count_2_or_more"));

	protected final static Set<String> IGNORED_FILTER_BY = Sets
			.newHashSet(Arrays.asList("Brak", "null"));

	static protected boolean updatedone = false;

	private static List<DockindButtonField> buttons;
	private static Map<String, String> taskListEvents;
	protected String tab;
	protected String urlTabString;
	private Long bookmarkId;
	protected Bookmark bookmark;
	protected String orderTab;
	protected String username;
	protected String sortField;
	protected Boolean ascending;

	/** TASKS - wpisy na li�cie zada� */
	protected List<Task> tasks; // <---------------
	private boolean watchesShowFilter = AvailabilityManager
			.isAvailable("watches.showfilter");
	protected boolean documentPackageAvailabe = AvailabilityManager
			.isAvailable("menu.left.repository.paczki.dokumentow");
	protected Collection<CaseTask> cases;
	private Tabs tabs;
	private Tabs orderTabs;
	protected List users;
    protected LinkedHashMap<String, String> usersAndDivisions;
    protected List<TableColumn> columns;
	protected Integer searchOfficeNumber;
	protected String searchOfficeId;
	private String[] activityIds;
	private Long[] actionCaseIds;
	private Long[] documentIds;
	private Long caseId;
	private String caseName;
	// private String caseInOfficeFinishDate;
	private List<Task> watches;
	private String[] urns;
	protected boolean addToTask = false;
	protected boolean multiDepartmentEnroll;
	protected Integer taskCount;
    protected Integer taskCountAllResults;
    protected Integer tempOffset;
    protected boolean allResultsFlag = false;
	protected Integer taskInPackageCount;
	private boolean ownWatches = true;

	protected Boolean withBackup;
	private String nameOfSmallAssignment;
	private String nameOfJournal;
	protected List<SmallAssignmentBean> smallAssignments;
	protected List<DepartmentEnrollBean> incomingDepartments;
	protected List<DocumentKind> dockindNames;// do filtrowania
	protected String filterDockindNames;// do filtrowania
	/** b��d kt�ry wyst�pi� przed przej�ciem na t� stron� */
	private String error;
	private Date caseDeadlineDate;
	protected String message;

	// okre�la, czy przegl�dane s� zadania zalogowanego u�ytkownika
	protected boolean viewingLoggedUser;

	protected boolean actionManualFinish;
	protected boolean actionAssignOfficeNumber;
	protected boolean actionAttachmentsAsPdf;
	protected boolean actionAddToCase;
	protected boolean actionMultiAssignment;
	protected boolean actionMultiBoxing;
	protected boolean actionAssignMe;
	protected boolean actionReplyUnnecessary;
	protected boolean actionSendToEva;
	protected boolean actionDepartmentEnroll;
	protected boolean actionMultiNote;
	protected boolean actionPrintEnvelopes;
	protected boolean actionDeleteOfficeNumber;

	private boolean adminAccess;

	protected boolean onTaskList;

	private Boolean showNumberNewTask;

	protected Pager pager;
	protected Integer offset;
	protected int LIMIT = 20;

    // limit taken from url params
    protected Integer limit;


    protected int tempLimit;

	protected String filterBy;
	protected String filterName;
	protected String filterColumnTitle;
	protected List<FilterColumn> filterColumns;
	protected List<Label> availableLabels;
	protected List<Label> availableForAddLabels;
	private Long labelId;
	protected Long viewedLabelId;
	private Long removeLabelId;
	protected Collection<Label> viewedLabels;
	private List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> enableAcceptances;
	private String acceptanceCn;

	// labels the new way
	protected List<Label> userFlags_label;
    protected List<Label> systemFlags_label;
    protected List<Label> labels_label;
    protected List<Label> systemLabels_label;

	protected boolean showLabels;
	private String dockindEventValue;
	protected List<FilterCondition> filterConditions;

	/**
	 * Zmienne dla TaskListEvent wymagane przy przekierowaniu na inn� strone
	 * 
	 * @return
	 */
	private String documentKindCn;
	private String dependsDocsJson;

	protected boolean showLabels() {

		try {
			log.trace("uzytkownik {}", getDSUser().getName());
			if (getDSUser() == null) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}

	protected void setup() {
		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("jsonView").append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new FillForm()).append(new FillFormJson())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doRemoveFilters")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new RemoveFilter())
				.append(new PrepareTabs(this)).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSwitchWorkflow")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new SwitchWorkflow())
				.append(new PrepareTabs(this)).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doManualFinish")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new ManualFinish()).append(new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doSendToEva").append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new SendToEva()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAssignOfficeNumber").append(new InitAtributes())
				.append(new AssignOfficeNumber()).append(new PrepareTabs(this))
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAddToCase").append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new AddToCase()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doRemoveWatches")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new RemoveWatches()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doRemoveOrders")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new RemoveOrders()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAssignMe").append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new AssignMe()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSmallAssignment")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new SmallAssignment()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDepartmentEnroll")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new MultiDepartmentEnroll()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doReplyUnnecessary")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new ReplyUnnecessary()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDeleteOfficeNumber")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new DeleteOfficeNumber()).append(new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doAcceptance")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new Acceptance()).append(new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doWithdraw").append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new Withdraw()).append(new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doAddLabel").append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new AddLabel()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doRemoveLabelFromView")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new RemoveLabelFromView()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doManualPushToCoor")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new ManualPushToCoor()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doRefreshUserTaskList")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new RefreshUserTaskList()).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDockindEvent")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new DockindEvent(this)).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doTaskListEvent")
				.append(OpenHibernateSession.INSTANCE)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new TaskListEvent(this)).append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doGenerateXls")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new FillForm()).append(new GenerateXLS())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doGenerateCsv")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new FillForm()).append(new GenerateCSV())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doGenerateXml")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new InitAtributes()).append(new PrepareTabs(this))
				.append(new FillForm()).append(new GenerateXML())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doGenerateXlsAll")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(new InitAtributes()).append(new PrepareTabs(this))
                .append(new FillForm()).append(new PrepareReport())
                .append(new FillForm()).append(new GenerateXLS(true))
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doGenerateCsvAll")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(new InitAtributes()).append(new PrepareTabs(this))
                .append(new FillForm()).append(new PrepareReport())
                .append(new FillForm()).append(new GenerateCSV(true))
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doGenerateXmlAll")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(new InitAtributes()).append(new PrepareTabs(this))
                .append(new FillForm()).append(new PrepareReport())
                .append(new FillForm()).append(new GenerateXML(true))
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

	}

	protected PrepareTabs getPrepareTabs() {
		return new PrepareTabs(this);
	}

	private class InitAtributes implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			initAttributes();
		}
	}

	private class TaskListEvent implements ActionListener {

		private TaskListAction taskListAction;

		public TaskListEvent(TaskListAction taskListAction) {
			this.taskListAction = taskListAction;
		}

		public void actionPerformed(ActionEvent event) {

			try {
				if ((activityIds == null || activityIds.length == 0)) {
					addActionError(sm.getString("NieWybranoZadan"));
				}

				AbstractParametrization.getInstance().doTaskListAction(
						taskListAction, dockindEventValue, event, documentIds);

			} catch (EdmException e) {
				log.error(e.getMessage());
			}

		}
	}

	private class DockindEvent implements ActionListener {
		private DockindButtonAction eventActionSupport;

		public DockindEvent(DockindButtonAction eventActionSupport) {
			this.eventActionSupport = eventActionSupport;
		}

		public void actionPerformed(ActionEvent event) {
			try {
				if ((activityIds == null || activityIds.length == 0)) {
					addActionError(sm.getString("NieWybranoZadan"));
				}

				for (int i = 0; i < activityIds.length; i++) {
					try {
						Document document = Document.find(documentIds[i]);
						document.getDocumentKind()
								.logic()
								.doDockindEvent(eventActionSupport, event,
										document, activityIds[i], null);
					} catch (Exception e) {
						log.error("B��d wykonywania akcji "
								+ getDockindEventValue(), e);
						addActionError(e.getMessage());
					}
				}
			} catch (Exception e) {
				log.error("B��d wykonywania akcji " + getDockindEventValue(), e);
				addActionError(e.getMessage());
			}
		}
	}

	/**
	 * U�ytkownik, kt�rego lista zada� ma by� wy?wietlona; null, je�eli nie
	 * wybrano u�ytkownika.
	 */
	protected abstract DSUser getDSUser() throws EdmException;

	// Lista dzialow null jesli nie ma
	protected abstract DSDivision getDSDivision() throws EdmException;

	/**
	 * Zwraca list� u�ytkownik�w do wy?wietlenia na li?cie wyboru. Je�eli lista
	 * ma nie by� wy?wietlana, zwraca null.
	 */
	protected abstract List getDSUserList() throws EdmException;

	protected LinkedHashMap<String, String> getDSUserAndDivisions()
			throws EdmException {
		return null;
	}

	public abstract String getBaseLink();

	/**
	 * Zwraca nazwe listy zadan czy w kancelrai czy zakladka czy moze podglad
	 * zadan uzytkownka
	 */
	public abstract String getListName();

	/**
	 * @return True, je�eli pokazywana jest lista zalogowanego u�ytkownika.
	 */
	public abstract boolean isOwnTasks();

	private static final String[] SIMPLE_TASK_COLUMNS = new String[] {
			"documentOfficeNumber", "receiveDate", "sender", "documentSummary",
			"documentCtime" };

	/**
	 * Wszystkie kolumny ktore trzeba wywalic z prosika
	 */
	public static final String[] COLUMN_TO_DELETE = new String[] { "clerk",
			"caseOfficeId", "documentReferenceId", "deadlineTime",
			"dockindKwota", "process", "answerCounter", "documentSender",
			"caseDeadlineDate", "officeCase" };

	private static Map<String, DocKindProperties> docKindProperties;

	static {
        boolean ok = false;
		try {
			ok = DSApi.openContextIfNeeded();
			initDockind();
		} catch (EdmException e) {
			log.error("", e);
		} finally {
            DSApi.closeContextIfNeeded(ok);
        }
	}

	public static void initDockind() throws EdmException {
		buttons = new ArrayList<DockindButtonField>();
		List<DocumentKind> l = DocumentKind.list(true);
		docKindProperties = new TreeMap<String, DocKindProperties>();
		for (DocumentKind documentKind : l) {
			documentKind.initialize();
			docKindProperties.put(documentKind.getName(),
					new DocKindProperties(documentKind));
			if (documentKind.getFields() != null)
				for (Field field : documentKind.getFields()) {
					if (Field.DOCKIND_BUTTON.equals(field.getType())
							&& ((DockindButtonField) field)
									.isButtonAvailable(DockindButtonField.TASKLIST)) {
						buttons.add((DockindButtonField) field);

					}
				}
		}
		taskListEvents = (AbstractParametrization.getInstance()
				.getTaskListEvent());
		log.debug("initdockind: TasklistEvents: {}", taskListEvents.toString());
	}

	private static class DocKindProperties {
		private String openIn;
		private String cn;
		private String name;

		public DocKindProperties(String openIn, String cn, String name) {
			this.openIn = openIn;
			this.cn = cn;
			this.name = name;
		}

		public DocKindProperties(DocumentKind dockind) {
			String openIn = dockind.getProperties().get(
					DocumentKind.OPEN_TAB_IN_OFFICE);
			this.openIn = openIn != null ? openIn : "";
			this.cn = dockind.getCn();
			this.name = dockind.getName();
		}

		public String getOpenIn() {
			return openIn;
		}

		public void setOpenIn(String openIn) {
			this.openIn = openIn;
		}

		public String getCn() {
			return cn;
		}

		public void setCn(String cn) {
			this.cn = cn;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	protected String transformOrderProperty(String property) {
		if (TAB_ORDER.equals(tab) && "documentOfficeNumber".equals(property))
			return "orderNumber";
		else
			return property;
	}

	/**
	 * Odczytuje z ustawie� u�ytkownika list� kolum dla listy zada� okre?lonej
	 * parametrem tab. W wypadku braku ustawienia u�ywa domy?lnej listy kolumn
	 */
	protected List<TableColumn> getActualColumns() {
		String[] properties;
		if (StringUtils.isEmpty(orderTab)) {
			orderTab = TAB_REALIZE_ORDERS;
		}
		properties = DSApi
				.context()
				.userPreferences()
				.node("task-list")
				.get("columns_" + tab,
						StringUtils.join(TaskListUtils
								.getDefaultColumnProperties(tab, orderTab), ","))
				.split(",");

		List<TableColumn> actualColumns = new ArrayList<TableColumn>(
				properties.length);
		for (int i = 0; i < properties.length; i++) {
			actualColumns
					.add(new TableColumn(
							properties[i],
							TaskListUtils
									.getColumnDescription(transformOrderProperty(properties[i])),
							getBaseLink()
									+ "?username="
									+ username
									+ "&tab="
									+ tab
									+ "&sortField="
									+ ("shortReceiveDate".equals(properties[i]) ? "receiveDate"
											: properties[i])
									+ "&ascending=false&withBackup="
									+ withBackup + "&filterBy=" + filterBy
									+ "&orderTab=" + orderTab + "&filterName="
									+ filterName,

							getBaseLink()
									+ "?username="
									+ username
									+ "&tab="
									+ tab
									+ "&sortField="
									+ ("shortReceiveDate".equals(properties[i]) ? "receiveDate"
											: properties[i])
									+ "&ascending=true" + "&withBackup="
									+ withBackup + "&filterBy=" + filterBy
									+ "&orderTab=" + orderTab + "&filterName="
									+ filterName

							, ""));
		}
		return actualColumns;
	}

	protected List<FilterColumn> getFilterColumn() {
		List<FilterColumn> result = new ArrayList<FilterColumn>();
		String[] userProperties = DSApi
				.context()
				.userPreferences()
				.node("task-list")
				.get("columns_" + tab,
						StringUtils.join(TaskListUtils
								.getDefaultColumnProperties(tab, orderTab), ","))
				.split(",");
		String[] availableProperties = StringUtils.join(
				TaskListUtils.getDefaultColumnProperties("filter", null), ",")
				.split(",");
		Map<String, String> availablePropertiesMap = new TreeMap<String, String>();
		for (int i = 0; i < availableProperties.length; i++) {
			availablePropertiesMap.put(availableProperties[i],
					availableProperties[i]);
		}
		result.add(new FilterColumn("Brak", sm.getString("Brak")));
		result.add(new FilterColumn("documentOfficeNumber", TaskListUtils
				.getColumnDescription("documentOfficeNumber")));
		for (int i = 0; i < userProperties.length; i++) {
			if (availablePropertiesMap.containsKey(userProperties[i])
					&& !"documentOfficeNumber".equals(userProperties[i]))
				result.add(new FilterColumn(userProperties[i], TaskListUtils
						.getColumnDescription(userProperties[i])));
		}

		PermissionCache cache = null;
		try {
			cache = (PermissionCache) ServiceManager
					.getService(PermissionCache.NAME);
			if (cache.hasPermission(DSApi.context().getPrincipalName(),
					DSPermission.TASK_COUNT.getName())) {
				result.add(new FilterColumn("task_count_1",
						"Tylko przypisane zadania"));
				result.add(new FilterColumn("task_count_2_or_more",
						"Tylko wsp�lne zadania"));
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return result;
	}

	private void ilpolAcceptances() {
		if (AvailabilityManager.isAvailable("tasklist.ilpol.acceptances")) {
			enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
			try {
				DocumentKind dk = DocumentKind.findByCn("dlbinder");
				if (dk == null)
					return;
				dk.initialize();
				Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> mapAcc = dk
						.getAcceptancesDefinition().getAcceptances();

				Set<String> set = mapAcc.keySet();
				for (String cn : set) {
					enableAcceptances.add(mapAcc.get(cn));
				}
			} catch (Exception e) {

				log.error("", e);
			}
		}
	}

	/**
	 * Zwraca odno?nik do zadania. Je�eli przegl?dana jest lista zalogowanego
	 * u�ytkownika, odno?niki zawieraj? identyfikator zadania workflow, za?
	 * nieotwarte zadania po klikni�ciu zostan? zaakceptowane. Je�eli
	 * przegl?dana jest lista innego u�ytkownika, odno?nik prowadzi do
	 * formularza edycji dokumentu bez identyfikatora zadania workflow.
	 * <p>
	 * Obecnie obs�ugiwane s? tylko zadania typu documentTask.
	 */
	public String getLink(Task task) {
		try {
			if (task.isDocumentTask()) {
				if (task.getLink() != null) {
					return task.getLink();
				} else {
					// DocKindProperties p =
					// docKindProperties.get(task.getDockindName());
					if (!(TAB_MY_ORDERS.equals(orderTab))
							&& !(TAB_WATCH_ORDERS.equals(orderTab))) {
						String activityId = WorkflowFactory
								.activityIdOfWfNameAndKey(
										task.getWorkflowName(),
										task.getActivityKey());

						if (viewingLoggedUser && !task.isAccepted()
								&& !TAB_WATCHES.equals(tab)) {
							if (task.isDocumentCrm()) {
								return "/office/accept-wf-assignment.action?doAccept=true&activity="
										+ activityId
										+ "&target="
										+ docKindProperties.get(
												task.getDockindName())
												.getOpenIn()
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
							} else if (task.getDockindName() != null
									&& docKindProperties.get(task
											.getDockindName()) != null) {
								return "/office/accept-wf-assignment.action?doAccept=true&activity="
										+ activityId
										+ "&target="
										+ docKindProperties.get(
												task.getDockindName())
												.getOpenIn()
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
							}
							return "/office/accept-wf-assignment.action?doAccept=true&activity="
									+ activityId
									+ "&division="
									+ ((TaskSnapshot) task).getDivisionGuid();
						}

						String link = null;
						String tmp = null;
						if (JbpmWorkflowService.NAME.equals(task
								.getWorkflowName()) && viewingLoggedUser) {
							tmp = "external-workflow.action?doShowTask=true&";
						} else {
							if (task.isDocumentCrm()
									|| (task.getDockindName() != null
											&& docKindProperties.get(task
													.getDockindName()) != null && "document-archive"
												.equals(docKindProperties.get(
														task.getDockindName())
														.getOpenIn())))
								tmp = "document-archive.action?";
							else if (task.getDockindName() != null) {
								if (docKindProperties
										.get(task.getDockindName()) != null
										&& "main".equals(docKindProperties.get(
												task.getDockindName())
												.getOpenIn()))
									tmp = "main-redirect.action?";
								else if (docKindProperties.get(task
										.getDockindName()) != null
										&& "dwr".equals(docKindProperties.get(
												task.getDockindName())
												.getOpenIn()))
									tmp = "dwr-document-main.action?";
								else
									tmp = "summary.action?";
							} else
								tmp = "summary.action?";
						}

						if (TAB_IN.equals(tab)) {
							link = "/office/incoming/" + tmp + "documentId="
									+ task.getDocumentId();
							if (viewingLoggedUser)
								link += "&activity="
										+ activityId
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						} else if (TAB_OUT.equals(tab)) {
							link = "/office/outgoing/" + tmp + "documentId="
									+ task.getDocumentId();
							if (viewingLoggedUser)
								link += "&activity="
										+ activityId
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						} else if (TAB_INT.equals(tab)) {
							link = "/office/internal/" + tmp + "documentId="
									+ task.getDocumentId();
							if (viewingLoggedUser)
								link += "&activity="
										+ activityId
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						} else if (TAB_ORDER.equals(tab)) {
							link = "/office/order/" + tmp + "documentId="
									+ task.getDocumentId();
							if (viewingLoggedUser)
								link += "&activity="
										+ activityId
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						} else if (TAB_WATCHES.equals(tab)) {
							if (InOfficeDocument.TYPE.equals(task
									.getDocumentType()))
								link = "/office/incoming/"
										+ tmp
										+ "documentId="
										+ task.getDocumentId()
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
							else if (OutOfficeDocument.TYPE.equals(task
									.getDocumentType()))
								link = "/office/outgoing/"
										+ tmp
										+ "documentId="
										+ task.getDocumentId()
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
							else
								link = "/office/internal/"
										+ tmp
										+ "documentId="
										+ task.getDocumentId()
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						} else if (TAB_DOCUMENT_PACKAGE.equals(tab)) {
							link = "/office/incoming/" + tmp + "documentId="
									+ task.getDocumentId();
							if (viewingLoggedUser)
								link += "&activity="
										+ activityId
										+ "&division="
										+ ((TaskSnapshot) task)
												.getDivisionGuid();
						}

						return link;
					} else
					// polecenia, ale z zakladki MY_ORDERS lub WATCH_ORDERS wiec
					// zwykly link do summary.action
					{
						return "/office/order/summary.action?documentId="
								+ task.getDocumentId();
					}
				}
			} else {
				return "/office/tasklist/current-user-task-list.action?BLADGENERACJILINKUNIETASK";
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return "/office/tasklist/current-user-task-list.action?BLADGENERACJILINKU";
	}

	/**
	 * okre�la czy zadanie ma by� oznaczone na czerwono (jako przypomnienie)
	 */
	public boolean needsReminder(Task task) {
		if (task instanceof TaskSnapshot) {
			TaskSnapshot t = (TaskSnapshot) task;
			if (t.getProcessStatus() != null
					&& ((t.getProcessStatus() & TaskSnapshot.STATUS_ZAWIESZONY_PRZYPOMNIENIE) > 0))
				// if
				// (Constants.STATUS_ZAWIESZONY_PRZYPOMNIENIE.equals(t.getProcessStatus()))
				return true;
			if (t.getReminderDate() != null) {
				Date act = DateUtils.getCurrentTime();// new Date();
				if (t.getReminderDate().before(act)) {
					return true;
				}

			}
		}
		return false;
	}

	public String statusSymbols(Task task) {
		if (task instanceof TaskSnapshot) {
			TaskSnapshot t = (TaskSnapshot) task;
			if (t.getProcessStatus() == null)
				return null;

			String symbols = "";
			if ((t.getProcessStatus() & TaskSnapshot.STATUS_ZAWIESZONY) > 0)
				symbols += "z";
			if ((t.getProcessStatus() & TaskSnapshot.STATUS_OTWARTE_W_OPS) > 0)
				symbols += "p";
			if ((t.getProcessStatus() & TaskSnapshot.STATUS_WZNOWIENIE_ZADANIA) > 0)
				symbols += "w";
			if ((t.getProcessStatus() & TaskSnapshot.STATUS_NOWY_DOKUMENT_Z_NR_POLISY) > 0)
				symbols += "n";
			return symbols;
		} else
			return null;

	}

	/**
	 * Zwraca odno?nik do sprawy do ktorej przypisane jest zadanie.
	 */
	public String getCaseDeadlineLink(Task task) {
		CaseTask caseTask = null;
		try {
			OfficeCase officeCase = OfficeCase.find(task.getCaseId());
			caseTask = new CaseTask(officeCase);
		} catch (EdmException e) {
			log.error("Blad", e);
		}
		if (caseTask == null) {
			return "";
		} else {
			return "/office/edit-case.do?id=" + caseTask.getCaseFinishDate();
		}
	}

	public String getCaseLink(Task task) {
		return "/office/edit-case.do?id=" + task.getCaseId();
		// return "/office/edit-case.do?id="+task.getc
	}

	public String getAnswerLink(Task task) {
		String activityId = WorkflowFactory.activityIdOfWfNameAndKey(
				task.getWorkflowName(), task.getActivityKey());
		if (TAB_INT.equals(tab)) {
			return "/office/internal/replies.action?documentId="
					+ task.getDocumentId() + "&activity=" + activityId;
		} else {
			return "/office/incoming/replies.action?documentId="
					+ task.getDocumentId() + "&activity=" + activityId;
		}
	}
	
	/**
	 * This method fetchs and returns answer counter from TaskSnapshot class
	 * 
	 * @param  Task - task
	 * @return Integer - answerCounter
	 */
	public Integer getAnswerCounter(Task task) {
		Integer answerCounter = ((JBPMTaskSnapshot)task).getAnswerCounter();
//		answerCounter = answerCounter != null ? answerCounter : 0; 
		return answerCounter;
	}

	/**
	 * Zwraca odno?nik do zadania zwi?zanego ze spraw?.
	 */
	public String getLink(CaseTask caseTask) {
		return "/office/edit-case.do?id=" + caseTask.getCaseId()
				+ "&tabId=summary";
	}

	public String prettyPrint(Object object, String property) {
		if (object == null)
			return "";

		if (object instanceof Date) {
			if (property.startsWith("case")
					|| property.equals("shortReceiveDate")
					|| property.equals("incomingDate"))
				return DateUtils.formatJsDate((Date) object);
			else
				return DateUtils.formatJsDateTime((Date) object);
		} else if (property.equals("cutLastRemark")) {
			return StringUtils
					.left(object.toString(),
							Integer.parseInt(Docusafe
									.getAdditionProperty("tasklist.cutLastRemark.size")));
		} else {
			return object.toString();
		}
	}

	private class PrepareTabs implements ActionListener {
		private TaskListTabAction thisClass;

		public PrepareTabs(TaskListAction action) {
			thisClass = action;
		}

		public void actionPerformed(ActionEvent event) {
			try {
				if (!DSApi.isContextOpen()) {
					DSApi.open(AuthUtil.getSubject(ServletActionContext
							.getRequest()));
				}
			} catch (Exception e) {
				log.error("", e);
			}
			if (isBookmarkTasklist())
				new TasklistBookmarkTab(thisClass).initTab();
			else
				new TasklistTab(thisClass).initTab();
		}
	}

	protected class SmallAssignmentBean {

		public SmallAssignmentBean(DSUser user, DSDivision guid)
				throws EdmException {
			this.name = (user != null ? user.getName() : "") + ";"
					+ (guid != null ? guid.getGuid() : "");
			if (user != null) {
				DSUser subUser = user.getSubstituteUser();
				this.substituteUser = (subUser == null ? "" : subUser
						.getFirstname() + " " + subUser.getLastname());
				this.nameToDisplay = user.getLastname() + " "
						+ user.getFirstname().charAt(0) + ".";
			} else
				this.nameToDisplay = guid.getName();
		}

		private String name;// to jest name po ktorym bede szukac uzytkownikow.
							// Np. pkomisarski
		private String nameToDisplay;// to jest name do wyswietlenia. Np. P.
										// Komisarski
		private String substituteUser; // imie i nazwisko usera, ktory zastepuje

		public String getName() {
			return name;
		}

		public String getNameToDisplay() {
			return nameToDisplay;
		}

		/**
		 * @return the substituteUser
		 */
		public String getSubstituteUser() {
			return substituteUser;
		}
	}

	protected class DepartmentEnrollBean {
		public DepartmentEnrollBean(Journal journal) throws EdmException {
			this.name = journal.getId();
			this.nameToDisplay = journal.getSummary();

		}

		private long name;
		private String nameToDisplay;

		public long getName() {
			return name;
		}

		public String getNameToDisplay() {
			return nameToDisplay;
		}
	}

    private class PrepareReport implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            tempLimit = getDefaultLimit();
            tempOffset = offset;
            allResultsFlag = true;

            try {
                offset = 0;

                DSApi.context().begin();
                DSApi.context().userPreferences().node("task-list").putInt("task-count-per-page"+tab, taskCount);
                DSApi.context().commit();

            } catch (EdmException e) {

                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

    }

	private class ReplyUnnecessary implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (documentIds == null || documentIds.length == 0)
				addActionError(sm.getString("NieWybranoDokumentow"));
			try {
				DSApi.context().begin();
				for (int i = 0; i < documentIds.length; i++) {
					Long id = documentIds[i];
					if (id == null)
						continue;
					InOfficeDocument document = InOfficeDocument
							.findInOfficeDocument(id);
					document.setReplyUnnecessary(true);
					TaskSnapshot.updateByDocumentId(document.getId(),
							document.getStringType());
					event.addActionMessage(sm.getString(
							"OznaczonoPismoJakoNiewymagajaceOdpowiedzi",
							document.getOfficeNumber()));
				}
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	private class RemoveFilter implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			filterBy = null;
			filterName = "";
			ServletActionContext.getRequest().getSession()
					.setAttribute(preparePrefix() + "filterBy", filterBy);
			ServletActionContext.getRequest().getSession()
					.setAttribute(preparePrefix() + "filterName", filterName);
			ServletActionContext.getRequest().getSession()
					.setAttribute(preparePrefix() + "_taskList_offset", null);
		}
	}

	private class SwitchWorkflow implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			WorkflowFactory.switchWorkflow();
		}
	}

	private class DeleteOfficeNumber implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			if (documentIds == null || documentIds.length == 0)
				addActionError(sm.getString("NieWybranoZadan"));

			if (hasActionErrors())
				return;

			for (int i = 0; i < documentIds.length; i++) {
				Long id = documentIds[i];
				if (id == null)
					continue;

				try {
					DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
					DSApi.context().begin();

					OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(id);
					if (document.getOfficeNumber() != null) {

						document.setOfficeNumber(null);

						PreparedStatement ps = DSApi.context().prepareStatement("delete from dso_journal_entry where documentid = ? and journal_id = ?");
						ps.setLong(1, id);
						ps.setLong(2, Journal.getMainOutgoing().getId());
						ps.executeUpdate();

					}
					TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());

					DSApi.context().commit();
				} catch (Exception e) {
					addActionError(e.getMessage());
					log.error("", e);
					return;
				}
				finally {
					DSApi._close();
				}

			}


		}
	}

	private class Acceptance implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			log.trace("TaskListAction.Acceptance");
			if (documentIds == null || documentIds.length == 0
					|| activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoDokumentow"));
				return;
			}
			if (acceptanceCn == null || acceptanceCn.length() < 1) {
				addActionError("Nie wybrano akceptacji");
				return;
			}
			try {
				DSApi.context().begin();
				for (int i = 0; i < documentIds.length; i++) {

					AcceptanceCondition nextAcceptance = null;
					DSUser user = null;
					String division = null;
					OfficeDocument document = (OfficeDocument) Document
							.find(documentIds[i]);
					DocumentKind kind = document.getDocumentKind();
					Map<String, Object> valuesTmp = new TreeMap<String, Object>();
					FieldsManager fm;
					kind.initialize();
					fm = kind.getFieldsManager(documentIds[i]);
					fm.initialize();
					fm.initializeAcceptances();

					if (kind.logic().accept(document, null, acceptanceCn)) {
						jbpmAcceptance(document, i, fm);
					} else if (WorkflowFactory.jbpm && i < activityIds.length) // jbpm4
					{
						AcceptancesDefinition acceptDef = document
								.getFieldsManager().getAcceptancesDefinition();
						if (acceptDef == null)
							continue;
						Map<String, AcceptancesDefinition.Acceptance> acceptMap = acceptDef
								.getAcceptances();
						if (acceptMap == null)
							continue;
						AcceptancesDefinition.Acceptance acceptance = acceptMap
								.get(acceptanceCn);
						if (acceptance == null)
							continue;

						String[] activities = activityIds[i].split(",");
						if (activities.length <= 1)
							continue;
						String taskId = activities[1];
						org.jbpm.api.task.Task task = Jbpm4Provider
								.getInstance().getTaskService().getTask(taskId);
						if (task == null)
							continue;
						String procInstance = Jbpm4Provider.getInstance()
								.getExecutionService()
								.findExecutionById(task.getExecutionId())
								.getProcessInstance().getId();
						log.info("procInstance: {}", procInstance);
						String processName = procInstance.split("\\.")[0];

						ProcessDefinition pdef = document.getDocumentKind()
								.getDockindInfo().getProcessesDeclarations()
								.getProcess(processName);

						ProcessInstance pi = pdef.getLocator().lookupForTaskId(
								activityIds[i].split(",")[1]);

						for (String transactionName : acceptance
								.getTransitionName().split(";")) {
							try {
								pdef.getLogic().process(
										pi,
										ProcessActionContext.action(
												transactionName).document(
												document));
							} catch (org.jbpm.api.JbpmException e) {
								log.warn(e.getMessage());
							}
						}
					} else if (i < activityIds.length) {
						if (oldAcceptanceHorror(event, i, user, division,
								document, valuesTmp, fm))
							continue;
					}
					TaskSnapshot.updateByDocument(Document.find(document
							.getId()));
					// TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
					document.getDocumentKind().setOnly(document.getId(),
							valuesTmp);

				}
				DSApi.context().commit();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}

		public void jbpmAcceptance(OfficeDocument document, int i,
				FieldsManager fm) throws HibernateException, EdmException {
			// akceptacja JBPM
			try {
				DSApi.context().session().flush();
				if (i < activityIds.length) {// brzydki hack - czasem liczba
												// dokument�w si� nie zgadza z
												// liczb� activity, warunek
												// zapobiega
												// ArrayIndexOutOfBounds
												// exception
					document.getDocumentKind().logic().getAcceptanceManager()
							.refresh(document, fm, activityIds[i]);
				}
				addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu")
						+ " ID "
						+ document.getId()
						+ " : "
						+ fm.getAcceptancesDefinition().getAcceptances()
								.get(acceptanceCn).getName());
			} catch (EdmException ex) {
				throw new EdmException("ID " + document.getId() + " : "
						+ ex.getMessage(), ex);
			}
		}

		// zwraca true je�li co� p�jdzie nie tak
		private boolean oldAcceptanceHorror(ActionEvent event, int i,
				DSUser user, String division, OfficeDocument document,
				Map<String, Object> valuesTmp, FieldsManager fm)
				throws EdmException {
			AcceptanceCondition nextAcceptance;
			Boolean simpleAcceptance = (IntercarsAcceptanceMode.from(fm) == IntercarsAcceptanceMode.SIMPLE2);

			if (fm.getAcceptancesDefinition().getAcceptances()
					.get(acceptanceCn) == null) {
				addActionError("Niepoprawny kod akcepatcji : " + acceptanceCn);
				return true;
			}

			if (fm.getAcceptancesDefinition().getAcceptances()
					.get(acceptanceCn).getFieldCn() != null) {
				List<CentrumKosztowDlaFaktury> ckfList = CentrumKosztowDlaFaktury
						.findByDocumentId(documentIds[i]);
				if (ckfList != null && ckfList.size() > 0) {
					for (CentrumKosztowDlaFaktury centrumKosztowDlaFaktury : ckfList) {
						if (fm.getAcceptancesState().getFieldAcceptance(
								acceptanceCn, centrumKosztowDlaFaktury.getId()) == null) {
							Map<Integer, String> centrumAcceptanceCn = new TreeMap<Integer, String>();
							centrumAcceptanceCn.put(
									new Integer(centrumKosztowDlaFaktury
											.getId().toString()), acceptanceCn);
							try {
								AcceptancesManager.giveCentrumAcceptance(
										centrumAcceptanceCn, documentIds[i]);
								addActionMessage(sm
										.getString("WykonanoAkceptacjeDokumentu")
										+ " KO "
										+ document.getOfficeNumber()
										+ " : "
										+ fm.getAcceptancesDefinition()
												.getAcceptances()
												.get(acceptanceCn).getName());

							} catch (DSException e) {
								addActionError("Nie mo�esz wykona� akceptacji "
										+ fm.getAcceptancesDefinition()
												.getAcceptances()
												.get(acceptanceCn).getName()
										+ " dla dokumentu KO : "
										+ document.getOfficeNumber());
								continue;
							}

							if (acceptanceCn.equals("zwykla")) {
								valuesTmp.put("AKCEPTACJA_ZWYKLA", 1);
							}
							valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm
									.getAcceptancesDefinition()
									.getAcceptances().get(acceptanceCn)
									.getLevel());
						}
					}
				} else {
					addActionError("Brak centrum dla dokumentu "
							+ documentIds[i]);
					return true;
				}
			} else {
				if (fm.getAcceptancesState().getGeneralAcceptancesAsMap()
						.get(acceptanceCn) == null) {
					try {
						AcceptancesManager.giveAcceptance(acceptanceCn,
								documentIds[i]);
						addActionMessage(sm
								.getString("WykonanoAkceptacjeDokumentu")
								+ " KO "
								+ document.getOfficeNumber()
								+ " : "
								+ fm.getAcceptancesDefinition()
										.getAcceptances().get(acceptanceCn)
										.getName());
					} catch (DSException e) {
						addActionError("Nie mo�esz wykona� akceptacji "
								+ fm.getAcceptancesDefinition()
										.getAcceptances().get(acceptanceCn)
										.getName() + " dla dokumentu KO : "
								+ document.getOfficeNumber());
						return true;
					}
					valuesTmp.put(
							InvoiceLogic.STATUS_FIELD_CN,
							fm.getAcceptancesDefinition().getAcceptances()
									.get(acceptanceCn).getLevel());
				}
			}
			DSApi.context().session().flush();

			fm.initializeAcceptances();
			nextAcceptance = fm.getAcceptancesState().getThisAcceptation(null,
					simpleAcceptance);

			if (nextAcceptance != null) {
				if (nextAcceptance.getUsername() != null) {
					try {
						user = DSUser.findByUsername(nextAcceptance
								.getUsername());
					} catch (Exception e) {
						addActionError("Nie znalezionu uzytkownika "
								+ nextAcceptance.getUsername());
					}
				}
				if (nextAcceptance.getDivisionGuid() != null) {
					division = nextAcceptance.getDivisionGuid();
				}

				if (user != null || division != null) {
					String[] documentActivity = new String[1];
					documentActivity[0] = activityIds[i];
					String plannedAssignment = "";

					if (user != null
							&& (division == null || division.length() < 1)) {
						DSDivision[] divisions = user.getDivisions();
						plannedAssignment = (divisions != null
								&& divisions.length > 0 ? divisions[0]
								.getGuid() : DSDivision.ROOT_GUID)
								+ "/"
								+ user.getName()
								+ "/"
								+ WorkflowUtils.iwfManualName()
								+ "//"
								+ "realizacja";
					}
					if (division != null) {
						plannedAssignment = division + "//"
								+ WorkflowUtils.iwfManualName()
								+ "/*/realizacja";
					}

					String curUser = DSApi.context().getDSUser().getName();
					WorkflowFactory.multiAssignment(documentActivity, curUser,
							new String[] { plannedAssignment },
							plannedAssignment, event);
				} else {
					event.addActionError(sm
							.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje")
							+ " " + document.getOfficeNumber());
				}
			} else {
				event.addActionError(sm
						.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje")
						+ " " + document.getOfficeNumber());
			}
			return false;
		}

	}

	private class Withdraw extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			for (long docId : documentIds) {
				DSApi.context().begin();
				WorkflowFactory.manualFinish(docId, false);
				Map<String, Object> toSet = Maps.newHashMap();
				toSet.put(InvoiceICLogic.STATUS_FIELD_CN, 666);
				toSet.put(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN, 666);
				Document.find(docId).getDocumentKind().setOnly(docId, toSet);
				DSApi.context().commit();
			}// for
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

	private class FillFormJson extends AjaxSerializeActionListener {

		@Override
		public Object serializeAction(ActionEvent event) throws Exception {
			Map<String, Object> ret = new HashMap<String, Object>();

			ret.put("columns", columns);
			ret.put("results", tasks);

			return ret;
		}
	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
            logRequest(log);
            /** inicjowanie atrybutow listy zadan */
            // initAttributes();
            prepareBookmarksTasklist();

            icAcceptances();
            libertyAcceptances();
            ilpolAcceptances();
            p4Acceptances();
            tktAcceptances();
            try {
                if (!DSApi.isContextOpen())
                    DSApi.open(AuthUtil.getSubject(ServletActionContext
                            .getRequest()));
                adminAccess = DSApi.context().isAdmin();
            } catch (EdmException ex) {
                addActionError(ex.getMessage());
                log.error("", ex);
            }

            handleManualFinish(event);

            TaskListHelper.instance().buildTaskList(TaskListAction.this);
            /*
				 * To na razie odpuszczamy if (tasks!=null) { for (Task task :
				 * tasks) { taskListContainer.addTaskToList(task); }
				 *
				 * }
				 */
            if (AvailabilityManager.isAvailable("modifyTaskListView"))
                modifyTaskListView();
            /** Zapisuje ustawienia listy zadan w sesji */
            saveAttributes();


            if(allResultsFlag) {
                try {
                    DSApi.openContextIfNeeded();
                    DSApi.context().userPreferences().node("task-list").putInt("task-count-per-page" + tab, tempLimit);
                    DSApi.context().commit();

                    offset = tempOffset;
                    allResultsFlag = false;

                } catch (EdmException e) {

                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }

		}

		private void icAcceptances() {
			if (AvailabilityManager.isAvailable("tasklist.acceptances")) {
				enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
				try {
					String userName = DSApi.context().getPrincipalName();
					List<AcceptanceCondition> acceptanceCondition = AcceptanceCondition
							.userAcceptances(DSApi.context().getPrincipalName());
					DocumentKind dk = DocumentKind
							.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
					if (dk == null)
						return;
					dk.initialize();
					Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> mapAcc = dk
							.getAcceptancesDefinition().getAcceptances();

					Set<String> set = mapAcc.keySet();
					for (String cn : set) {
						if (AcceptanceCondition.canAcceptByUser(cn, userName)) {
							enableAcceptances.add(mapAcc.get(cn));
						}
					}
				} catch (Exception e) {

					log.error("", e);
				}
			}
		}

		private void p4Acceptances() {
			if (AvailabilityManager.isAvailable("tasklist.p4.acceptances")) {
				enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
				try {
					// DocumentKind normal = DocumentKind.findByCn("normal");
					DocumentKind parcelRegistry = DocumentKind
							.findByCn("parcel_registry");
					// DocumentKind normalOut =
					// DocumentKind.findByCn("normal_out");

					parcelRegistry.initialize();
					Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> mapAcc = parcelRegistry
							.getAcceptancesDefinition().getAcceptances();

					for (String cn : mapAcc.keySet()) {
						enableAcceptances.add(mapAcc.get(cn));
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}

			}
		}

		private void tktAcceptances() {
			if (AvailabilityManager.isAvailable("tasklist.tkt.acceptances")) {
				enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
				try {
					DocumentKind fakturaZakupu = DocumentKind
							.findByCn("faktura_zakupu");

					fakturaZakupu.initialize();
					Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> mapAcc = fakturaZakupu
							.getAcceptancesDefinition().getAcceptances();

					for (String cn : mapAcc.keySet()) {
						enableAcceptances.add(mapAcc.get(cn));
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}

			}
		}

		private void libertyAcceptances() {
			if (AvailabilityManager.isAvailable("tasklist.liberty.acceptances")) {
				enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
				try {
					String userName = DSApi.context().getPrincipalName();
					List<AcceptanceCondition> acceptanceCondition = AcceptanceCondition
							.userAcceptances(DSApi.context().getPrincipalName());
					DocumentKind dk = DocumentKind.findByCn("liinvoice");
					if (dk == null)
						return;
					dk.initialize();
					Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> mapAcc = dk
							.getAcceptancesDefinition().getAcceptances();

					Set<String> set = mapAcc.keySet();
					for (String cn : set) {
						if (AcceptanceCondition.canAcceptByUser(cn, userName)) {
							enableAcceptances.add(mapAcc.get(cn));
						} else if (cn.equalsIgnoreCase("view")) // potwierdzernie
																// odbioru
						{
							enableAcceptances.add(mapAcc.get(cn));
						} else if (cn.equals("work_finish")) // liberty -
																// zakonczenie
																// pracy z
																// dokumentem
						{
							enableAcceptances.add(mapAcc.get(cn));
						}
					}
				} catch (Exception e) {

					log.error("", e);
				}
			}
		}

		private void handleManualFinish(ActionEvent event) {
			if (actionManualFinish) {
				DSContext context = null;
				try {
					context = DSApi.open(AuthUtil
							.getSubject(ServletActionContext.getRequest()));
					context.begin();

					OfficeCase c = OfficeCase.find(caseId);
					TaskList tasks = (TaskList) ServiceManager
							.getService(TaskList.NAME);
					List<Task> userTasks = tasks
							.getTasks(DSUser.findByUsername(DSApi.context()
									.getPrincipalName()));
					List<OfficeDocument> docList = c.getDocuments();
					for (OfficeDocument officeDoc : docList) {
						for (Task task : userTasks) {

							if (task.getDocumentId().equals(officeDoc.getId())) {
								String activityId = WorkflowFactory
										.getInstance().keyToId(
												task.getActivityKey());
								WorkflowFactory.getInstance().manualFinish(
										activityId, true);
								event.addActionMessage("Zako�czono prace z dokumentem "
										+ officeDoc.getOfficeNumber());
								// System.out.println("Zako�czono prace z dokumentem "+
								// officeDoc.getOfficeNumber());
							}
						}
					}

					context.commit();
					event.addActionMessage("Zapisano zmiany");

				} catch (Exception e) {
					if (context != null)
						DSApi.context().setRollbackOnly();
					e.printStackTrace();
				}
			}
		}

		/**
		 * Metoda umo�liwia zmodyfikowanie informacji o zadaniach wy�wietlanych
		 * na li�cie zada� o dane okre�lone w logice dokumentu powi�zanego z tym
		 * zadaniem.
		 * 
		 * Warto�� klucza modifyTaskListView z pliku available.properties
		 * okre�la czy metoda ma zosta� wywo�ana
		 */
		protected void modifyTaskListView() {
			DocumentLogic documentLogic = null;
			Document document = null;
			if (tasks != null) {
				for (Task task : tasks) {
					try {
						document = Document.find(task.getDocumentId());
						if (document != null) {
							documentLogic = document.getDocumentKind().logic();
							if (documentLogic != null) {
								documentLogic.modifyTaskViewOnList(document,
										tab, task);
							}
						}
					} catch (EdmException e) {
						log.debug("Nie znaleziono dokumentu o id "
								+ task.getDocumentId());
					}
				}
			}
		}

	}

	/** Odczytuje z sesji atrybuty listy zadan */
	protected void initAttributes() {
		//Enumeration<String> e = ServletActionContext.getRequest().getSession()
			//	.getAttributeNames();

		// while (e.hasMoreElements()) {
		// String string = (String) e.nextElement();
		// System.out.println(string+" - "+ServletActionContext.getRequest().getSession().getAttribute(string));
		// }

		showLabels = showLabels();
		/**
		 * Jesli podgladamy liste uzytkownika to domyslne zerowe bez czytania z
		 * sesji
		 */
		if (!isOwnTasks()) {
			if (withBackup == null) {
				withBackup = Boolean.FALSE;
			}
			if (offset == null) {
				offset = 0;
			}
			return;
		}
		/**
		 * Tutaj sprawdzamy czy ustawiony jest jaki� tab. Je�eli poprzez link
		 * nie ustawiono �adnego taba nast�puje pr�ba wczytania ostatniej
		 * rodzaju taba zapisanego w sesji.
		 * 
		 * Dla zak�adek to nie robi r�nicy bo i tak ustawiany jest zawsze
		 * rodzaj, kt�ry zdefiniowany jest w zak�adce.
		 */
		if (tab == null) {
			this.tab = (String) ServletActionContext.getRequest().getSession()
					.getAttribute("lastCurrentTab");
		}
		if (bookmarkId == null) {
			bookmarkId = (Long) ServletActionContext.getRequest().getSession()
					.getAttribute("bookmarkId");
		}

		if (withBackup == null) {
			withBackup = (Boolean) ServletActionContext.getRequest()
					.getSession().getAttribute(preparePrefix() + "withBackup");
		}
		if (withBackup == null) {
			withBackup = Boolean.FALSE;
		}

		if (offset == null
				&& ServletActionContext.getRequest().getSession()
						.getAttribute(preparePrefix() + "_taskList_offset") == null) {
			offset = 0;
		} else if (offset == null
				&& ServletActionContext.getRequest().getSession()
						.getAttribute(preparePrefix() + "_taskList_offset") != null) {
			offset = (Integer) ServletActionContext.getRequest().getSession()
					.getAttribute(preparePrefix() + "_taskList_offset");
		}
		if (viewedLabels == null) {
			viewedLabels = (Collection<Label>) ServletActionContext
					.getRequest().getSession()
					.getAttribute(preparePrefix() + "viewedLabels");
		}

		if (viewedLabelId == null) {
			viewedLabelId = (Long) ServletActionContext.getRequest()
					.getSession()
					.getAttribute(preparePrefix() + "viewedLabelId");
		}

		if (sortField == null) {
			sortField = (String) ServletActionContext.getRequest().getSession()
					.getAttribute(preparePrefix() + "sortField");
		}

		if (ascending == null) {
			ascending = (Boolean) ServletActionContext.getRequest()
					.getSession().getAttribute(preparePrefix() + "ascending");
		}

		if (filterBy == null) {
			filterBy = (String) ServletActionContext.getRequest().getSession()
					.getAttribute(preparePrefix() + "filterBy");
			filterName = (String) ServletActionContext.getRequest()
					.getSession().getAttribute(preparePrefix() + "filterName");
		}

	}

	/** Zapisuje do sesji atrybuty listy zadan */
	private void saveAttributes() {
		/** Jesli podgladamy czyjas to nie zapamietujemy w sesji */
		// if(!isOwnTasks())
		// {
		// return;
		// }

		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "withBackup", withBackup);
		if (isOwnTasks())
			ServletActionContext.getRequest().getSession()
					.setAttribute(preparePrefix() + "_taskList_offset", offset);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "viewedLabelId", viewedLabelId);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "viewedLabels", viewedLabels);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "ascending", ascending);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "filterBy", filterBy);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "filterName", filterName);
		ServletActionContext.getRequest().getSession()
				.setAttribute(preparePrefix() + "sortField", sortField);
		/** Zawira informcja na ktorej liscie byl ostatnio uzytkownik */
		ServletActionContext.getRequest().getSession()
				.setAttribute("tasksCurrentList", getListName());
		ServletActionContext.getRequest().getSession()
				.setAttribute("bookmarkId", getBookmarkId());
		// zapisanie do sesji aktualnego taba
		// wcze�niej robione to by�o przez odpwiedni link ale w zwi�zku z
		// dodaniem zak�adek zapisujemy teraz aktualny tab w sesji
		ServletActionContext.getRequest().getSession()
				.setAttribute("lastCurrentTab", tab);
	}

	public class SendToEva implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			if ((activityIds == null || activityIds.length == 0)
					&& (actionCaseIds == null || actionCaseIds.length == 0))
				addActionError(sm.getString("NieWybranoZadan"));

			Set<String> deletedIds = new HashSet<String>();
			try {

				if (activityIds != null) {
					for (int i = 0; i < activityIds.length; i++) {

						if (deletedIds.contains(activityIds[i])) {
							event.getLog().warn(
									sm.getString("ObiektZostalJuzUsuniety",
											activityIds[i]));
							continue;
						}

						deletedIds.add(activityIds[i]);

						WorkflowActivity activity = new WorkflowActivity(
								WorkflowFactory.getInstance().keyOfId(
										activityIds[i]));

						if (activity == null) {
							event.getLog().warn(
									sm.getString("WzadaniuBrakujeParametru",
											activityIds[i], "ds_finished"));
							continue;
						}

						// szukanie dokumentu zwi?zanego z zadaniem
						Long documentId = activity.getDocumentId();
						if (documentId == null) {
							event.getLog().warn(
									"Z zadaniem " + activityIds[i]
											+ " nie jest zwi�zany "
											+ "dokument (ds_document_id)");
							continue;
						}

						Document document = Document.find(documentId);

						OfficeDocument officeDocument = (OfficeDocument) document;
						if (officeDocument.getDocumentKind() != null
								&& !officeDocument
										.getDocumentKind()
										.getCn()
										.equals(DocumentLogicLoader.ROCKWELL_KIND)) {
							addActionMessage(sm
									.getString(
											"NieMoznaWyslacZadaniaDoEVAPoniewazNieJestToDokumentRockwell",
											officeDocument.getOfficeNumber()));
							continue;
						}

						Map<String, Object> statusMap = new HashMap<String, Object>();
						statusMap.put(RockwellLogic.EVA_STATUS_FIELD_CN, 1);
						statusMap.put(RockwellLogic.SUBMITTED_BY_CN, DSApi
								.context().getPrincipalName());
						statusMap.put(RockwellLogic.SUBMIT_DATE_FIELD_CN,
								new Date());
						statusMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,
								RockwellLogic.STAT_PROCESSED_EVA);

						// Document doc = Document.find(getDocumentId());

						if (document != null) {
							document.getDocumentKind().setWithHistory(
									document.getId(), statusMap, false);

							DSApi.context().begin();

							// WfActivity activity = getActivity() != null
							// ?
							// DSApi.context().getWfActivity(ActivityId.parse(getActivity()))
							// : null;
							WorkflowFactory.getInstance().manualFinish(
									activityIds[i], true);

							try {
								DSApi.context().session().flush();
							} catch (HibernateException e) {
								throw new EdmException(e);
							}

							TaskSnapshot.updateByDocumentId(document.getId(),
									document.getStringType());
							DSApi.context().commit();

							// powiadamiany EVA
							pl.compan.docusafe.parametrization.ra.EvaNotifier
									.notifyEva();
						}
					}
				}

			} catch (Exception e) {
				log.debug("", e);
				addActionError(e.getMessage());
			}
		}

	}

	@Unchecked
	private class ManualFinish extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			if ((activityIds == null || activityIds.length == 0)
					&& (actionCaseIds == null || actionCaseIds.length == 0))
				addActionError(sm.getString("NieWybranoZadanDoZakonczenia"));

			if (hasActionErrors())
				return;
			if (WorkflowFactory.jbpm && activityIds != null) {
				DSApi.context().begin();
				for (String activityId : activityIds) {
					WorkflowFactory.getInstance()
							.manualFinish(activityId, true);
				}
				DSApi.context().commit();
				for (Long id : getDocumentIds()) {
					addActionMessage("Zakonczono prace z pismem o ID = " + id);
				}
			} else {
				legacyActionPerformed(event);
			}
		}

		@Override
		public Logger getLogger() {
			return log;
		}

		public void legacyActionPerformed(ActionEvent event) {

			// zbi�r, w kt�rym ?ledzone s? identyfikatory zako�czonych proces�w
			// na wypadek, gdyby pojawi�a si� pr�ba usuni�cia tego samego
			// procesu
			// dwukrotnie (np. w wypadku zada� pochodz?cych z zast�pstw)
			Set<String> deletedIds = new HashSet<String>();

			int notInternalTasks = 0;

			// TODO trzeba jako� "rozpl�ta�" sprawdzanie warunk�w w tej funkcji
			// powinno korzysta� si� z dockind logic

			try {
				DSApi.context().begin();

				if (activityIds != null) {
					for (int i = 0; i < activityIds.length; i++) {

						if (deletedIds.contains(activityIds[i])) {
							event.getLog().warn(
									sm.getString("ObiektZostalJuzUsuniety",
											activityIds[i]));
							continue;
						}

						deletedIds.add(activityIds[i]);

						/*
						 * tu kiedys bylo sprawdzenie czy zadanie nie jest juz
						 * zamkniete
						 */
						WorkflowActivity activity = WorkflowFactory
								.getWfActivity(activityIds[i]);

						Long documentId = WorkflowFactory
								.getDocumentId(activityIds[i]);
						if (documentId == null) {
							event.getLog().warn(
									sm.getString("Zzadaniem") + " "
											+ activityIds[i] + " "
											+ "nie jest zwi?zany" + " "
											+ sm.getString("dokument")
											+ " (ds_document_id)");
							continue;
						}

						OfficeDocument document = OfficeDocument
								.find(documentId);

						if (document instanceof OfficeOrder) {
							if (!((OfficeOrder) document).getCreatingUser()
									.equals(DSApi.context().getPrincipalName())) {
								addActionError(sm
										.getString(
												"NieMaszUprawnienByZakonczycPolecenieOId",
												document.getId()));
								continue;
							}
						}

						if (Configuration.hasExtra("business")) {

							/***************************************************
							 * przenie�� do DocumentLogic
							 **************************************************/

							if (!"true"
									.equals(document
											.getDocumentKind()
											.getProperties()
											.get(DocumentKind.BOX_NOT_REQUIRED_TO_FINISH))
									&& !document
											.hasWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX)
									&& !document
											.getDocumentKind()
											.getCn()
											.equals(DocumentLogicLoader.INVOICE_IC_KIND)
									&& !document
											.getDocumentKind()
											.getCn()
											.equals(DocumentLogicLoader.CRMTASK_KIND)
									&& document.getType() == DocumentType.INCOMING
									&& !"fax".equals(document.getSource())
									&& ((InOfficeDocument) document).getKind() != null
									&& ((InOfficeDocument) document).getKind()
											.getName().indexOf("iznesowy") > 0
									&& document.getBox() == null) {
								addActionMessage(sm
										.getString(
												"NieZakonczonoPracyZdokumentemPoniewazNieZostalOnPrzypisanyDoPudlaArchiwalnego",
												(document instanceof OfficeDocument ? ((OfficeDocument) document)
														.getOfficeNumber()
														: "ID="
																+ document
																		.getId())));

								continue;
							}

							// Brief IV: je�eli wyznaczono termin zako�czenia
							// zadania,
							// u�ytkownik nie mo�e zako�czy� pracy z dokumentem,
							// dop�ki
							// nie doda uwagi

						}

						// warunki zako�czenia procesu wynikaj�ce z danego
						// rodzaju dokumentu
						if (document.getDocumentKind() != null) {
							try {
								document.getDocumentKind()
										.logic()
										.checkCanFinishProcess(
												(OfficeDocument) document);
							} catch (AccessDeniedException e) {
								addActionMessage(sm
										.getString(
												"NieZakonczonoPracyZdokumentemPoniewazNieSpelniaWszystkichWarunkowZakonczeniaProcesu",
												((OfficeDocument) document)
														.getOfficeNumber()));
								continue;
							}
						}

						// zadanie nie by�o jeszcze zaakceptowane
						if (!activity.getAccepted()) {

							document.changelog(DocumentChangelog.WF_ACCEPT);
							WorkflowFactory.getInstance().acceptTask(
									activityIds[i], event, document,
									DSDivision.ROOT_GUID);

							if (document instanceof OfficeDocument) {

								((OfficeDocument) document)
										.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
												activity.getLastUser(),
												activity.getLastDivision(),
												DSApi.context()
														.getPrincipalName(),
												activity.getCurrentGuid(),
												activity.getObjective(),
												WorkflowFactory.getInstance()
														.isManual(activity) ? sm
														.getString("doRealizacji")
														: sm.getString("doWiadomosci"),
												true,
												WorkflowFactory.getInstance()
														.isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
														: AssignmentHistoryEntry.PROCESS_TYPE_CC));

							}
						}

						if (document instanceof OfficeDocument) {
							((OfficeDocument) document)
									.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
											DSApi.context().getPrincipalName(),
											activity.getProcessDesc(),
											WorkflowFactory.getInstance()
													.isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
													: AssignmentHistoryEntry.PROCESS_TYPE_CC));

							((OfficeDocument) document)
									.setCurrentAssignmentGuid(null);
							((OfficeDocument) document)
									.setCurrentAssignmentUsername(null);
							((OfficeDocument) document)
									.setCurrentAssignmentAccepted(null);
						}

						if (document.getType() == DocumentType.INCOMING) {
							InOfficeDocument in = (InOfficeDocument) document;
							in.setStatus(InOfficeDocumentStatus
									.find(InOfficeDocumentStatus.ID_FINISHED));
						}

						document.changelog(DocumentChangelog.WF_FINISH);
						addActionMessage("Zako�czono prace z dokumentem "
								+ document.getId());

						if (document instanceof OfficeDocument) {
							// to normalnie (w summary action) jest wywo�ywane
							// podczas zamykania sprawy
							WorkflowFactory.getInstance().manualFinish(
									activityIds[i], true);
						} else {
							addActionError(sm
									.getString("ToNieJestDokumentKancelaryjny"));
						}

						TaskSnapshot.updateByDocumentId(document.getId(),
								document.getStringType());
					}// for TODO - zmniejszy� ca�� metod� a w szczeg�lno�ci t�
						// p�tl�
				}
				if (actionCaseIds != null) {
					Integer fsId = CaseStatus.ID_CLOSED;
					CaseStatus finishedStatus = CaseStatus.find(fsId);
					for (int i = 0; i < actionCaseIds.length; ++i) {
						OfficeCase oc = OfficeCase.find(actionCaseIds[i]);
						oc.setStatus(finishedStatus);

						// konczenie pism w sprawie
						TaskList tasks = (TaskList) ServiceManager
								.getService(TaskList.NAME);
						List<Task> userTasks = tasks.getTasks(DSUser
								.findByUsername(DSApi.context()
										.getPrincipalName()));
						List<OfficeDocument> docList = oc.getDocuments();
						for (OfficeDocument officeDoc : docList) {
							for (Task task : userTasks) {

								if (task.getDocumentId().equals(
										officeDoc.getId())) {
									String activityId = WorkflowFactory
											.getInstance().keyToIdActivity(
													task.getActivityKey());
									WorkflowFactory.getInstance().manualFinish(
											activityId, true);
									addActionMessage("Zako�czono prac� ze spraw�: "
											+ officeDoc.getCaseDocumentId() + " (pismo o numerze KO: " + officeDoc.getOfficeNumber() + ") ");
								}
							}
						}
					}
					actionCaseIds = null;
				}

				DSApi.context().commit();

				if (notInternalTasks > 0)
					addActionError(notInternalTasks
							+ " "
							+ sm.getString("ZwybranychZadanNieZakonczonoGdyzNie")
							+ " "
							+ (notInternalTasks == 1 ? sm.getString("jest")
									: sm.getString("s�")) + " "
							+ sm.getString("WprocesieDekretacjiRecznej"));
				// addActionError(smL.getString("NieMoznaZakonczycWybranychZadan"));
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	/**
	 * Nadaje numery kancelaryjne wybranym pismom.
	 */
	private class AssignOfficeNumber implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (documentIds == null || documentIds.length == 0)
				addActionError(sm.getString("NieWybranoZadan"));

			if (hasActionErrors())
				return;

			Date entryDate;
			Long journalId;
			try {
				DSApi.open(AuthUtil.getSubject(ServletActionContext
						.getRequest()));

				entryDate = GlobalPreferences.getCurrentDay();

				// pisma wewn�trzne dostaj? numer KO w momencie tworzenia,
				// ale nie szkodzi umie?ci� tutaj rozr�nienia
				journalId = Journal.getMainOutgoing().getId();
			} catch (EdmException e) {
				addActionError(e.getMessage());
				return;
			} finally {
				DSApi._close();
			}

			for (int i = 0; i < documentIds.length; i++) {
				Long id = documentIds[i];
				if (id == null)
					continue;

				// long time = System.currentTimeMillis();

				try {
					DSApi.open(AuthUtil.getSubject(ServletActionContext
							.getRequest()));
					// konieczne, aby sprawdzi�, �e dokument istnieje
					OutOfficeDocument document = OutOfficeDocument
							.findOutOfficeDocument(id);

					if (document.getOfficeNumber() != null)
						continue;

					if (!Configuration.hasExtra("business")
							&& !OutOfficeDocument.PREP_STATE_FAIR_COPY
									.equals(document.getPrepState())) {
						addActionError(sm
								.getString("NieMoznaNadacNumeruKOdokumentowi")
								+ " "
								+ document.getId()
								+ sm.getString("PoniewazNieJestOnCzystopisem"));
						continue;
					} else if (Configuration.hasExtra("business")
							&& AvailabilityManager
									.isAvailable("checkDocumentsPrepStateOnAssignOfficeNumber")
							&& !OutOfficeDocument.PREP_STATE_FAIR_COPY
									.equals(document.getPrepState())) {
						addActionError(sm
								.getString("NieMoznaNadacNumeruKOdokumentowi")
								+ " "
								+ document.getId()
								+ sm.getString("PoniewazNieJestOnCzystopisem"));
						continue;
					}
				} catch (DocumentNotFoundException e) {
					// addActionError("Nie odnaleziono dokumentu "+id);
					continue;
				} catch (EdmException e) {
					addActionError(e.getMessage());
					return;
				} finally {
					DSApi._close();
				}

				Integer sequenceId;
				try {
					DSApi.open(AuthUtil.getSubject(ServletActionContext
							.getRequest()));
					DSApi.context().begin();
					try {
						sequenceId = Journal.TX_newEntry2(journalId, id,
								entryDate);
					} catch (EdmException e) {

						addActionError(e.getMessage());
						return;
					}
					OutOfficeDocument doc = OutOfficeDocument
							.findOutOfficeDocument(id);
					Journal j = doc.getJournal();

					doc.bindToJournal(journalId, sequenceId, null, event);

					if (j != null) {
						for (Field f : doc.getFieldsManager().getFields()) {
							if (f instanceof DocumentJournalField) {
								((DocumentJournalField) f).persist(doc,
										j.getId());
								break;
							}
						}
					}
					TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),
							doc.getStringType());

					if (AvailabilityManager.isAvailable("p4.setAddToJournal")) {
						journalId = P4Helper.getJournalId(doc);
						Journal.TX_newEntry2(journalId, doc.getId(),
								GlobalPreferences.getCurrentDay());
						if (doc instanceof OfficeDocument)
							((OfficeDocument) doc).setJournal(Journal
									.find(journalId));
					}
					DSApi.context().commit();
				} catch (EdmException e) {
					DSApi.context().setRollbackOnly();
					addActionError(e.getMessage());
				} finally {
					try {
						DSApi.close();
					} catch (Exception e) {
					}
				}
			}
		}
	}

	/**
	 * Wykonanie dekretacji z pola akcji
	 */
	private class SmallAssignment implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoZadan"));
				return;
			}

			try {
				DSApi.context().begin();
				String[] userDivision = nameOfSmallAssignment.split(";");
				String username = userDivision[0];
				String guid = userDivision[1];
				if (StringUtils.isEmpty(guid)) {
					throw new EdmException("division not specified");
				}

				checkAssignmentOrError(guid, username);

				String plannedAssignment = getPlannedAssignment(guid, username);

				if (AvailabilityManager.isAvailable("manual-multi-assignment")
						&& !AvailabilityManager
								.isAvailable("reassign-from-action-list")) {
					OfficeDocument od = null;
					for (String activityId : activityIds) {
						Map<String, Object> params = Maps.newHashMap();
						od = OfficeDocument
								.find(Jbpm4ProcessLocator
										.documentIdForActivity(activityId
												.split(",")[1]));

						if (AvailabilityManager.isAvailable("assignment.block",
								null, activityId)) {
							addActionError(sm.getString(
									"DekredacjaZablokowanaDla", od.getId()));
						} else if (StringUtils.isNotEmpty(username)) {
							params.put(
									ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM,
									username);
							params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,
									sm.getString("doRealizacji"));
							Jbpm4WorkflowFactory
									.reassign(
											activityId,
											od.getId(),
											DSApi.context().getPrincipalName(),
											(String) params
													.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
											params);
						} else {
							params.put(
									ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM,
									guid);
							params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,
									sm.getString("doRealizacji"));
							Jbpm4WorkflowFactory
									.reassign(
											activityId,
											od.getId(),
											DSApi.context().getPrincipalName(),
											null,
											new String[] { (String) params
													.get(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM) },
											params);
						}

						/*
						 * od.getDocumentKind() .getDockindInfo()
						 * .getProcessesDeclarations()
						 * .getProcess(Jbpm4Constants.MANUAL_PROCESS_NAME)
						 * .getLogic() .startProcess(od, params);
						 */

						TaskSnapshot.updateByDocumentId(od.getId(), "anyType");
					}
				} else if (WorkflowFactory.jbpm) {
					for (String activityId : activityIds) {
						Jbpm4WorkflowFactory.reassign(activityId, null, DSApi
								.context().getPrincipalName(), username);
					}
				} else {
					WorkflowFactory.multiAssignment(activityIds, getUsername(),
							null, plannedAssignment, event);
				}
				DSApi.context().commit();
			} catch (EdmException e) {
				addActionError(e.getMessage());
				try {
					DSApi.context().rollback();
				} catch (Exception e2) {
					log.debug("", e2);
				}
			}
		}

		/**
		 * Sprawdza czy dekretacja jest poprawna, tj. czy dzia�, na kt�ry
		 * dekretujemy posiada u�ytkownik�w lub nie puste stanowiska.
		 * 
		 * @param guid
		 * @param username
		 */
		private void checkAssignmentOrError(String guid, String username)
				throws EdmException {
			DSDivision division = DSDivision.find(guid);

			final DSUser[] users = division.getUsers();

			// je�li nie ma user�w rzucamy error, poniewa� getUsers
			// sprawdza te� pod-dzia�y stanowiska
			if (users.length == 0) {
				throw new EdmException("Dzia� " + division.getName()
						+ " jest pusty");
			}
		}

		private String getPlannedAssignment(String guid, String username) {
			if (StringUtils.isNotEmpty(username)) {
				return guid + "/" + username + "/"
						+ WorkflowUtils.iwfManualName() + "//" + "realizacja";
			} else {
				return guid + "//" + WorkflowUtils.iwfManualName() + "//"
						+ "realizacja";
			}
		}
	}

	private class MultiDepartmentEnroll implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			if (activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoZadan"));
				return;
			}

			try {
				// DSApi.context().begin();
				for (int i = 0; i < documentIds.length; i++) {

					DSDivision division;
					Date entryDate = new Date();
					Journal journal;
					try {
						DSApi.open(AuthUtil.getSubject(ServletActionContext
								.getRequest()));
						boolean komorkaPrzyjecie = DSApi.context()
								.hasPermission(
										DSPermission.PISMA_KOMORKA_PRZYJECIE);
						boolean wszedziePrzyjecie = DSApi.context()
								.hasPermission(
										DSPermission.PISMO_WSZEDZIE_PRZYJECIE);
						if (!komorkaPrzyjecie && !wszedziePrzyjecie)
							throw new EdmException(
									sm.getString("BrakUprawnienDoPrzyjmowaniaPismaWdziale"));
						OfficeDocument document = OfficeDocument
								.findOfficeDocument(documentIds[i]);
						journal = Journal.find(new Long(nameOfJournal.trim()));
						if (journal.findDocumentEntry(document.getId()) != null)
							throw new EdmException(
									sm.getString("PismoZnajdujeSieJuzWwybranymDzienniku"));
						if (journal.getOwnerGuid() == null)
							division = DSDivision.find(DSDivision.ROOT_GUID);
						else
							division = DSDivision.find(journal.getOwnerGuid());
					} catch (EdmException e) {
						addActionError(e.getMessage());
						return;
					} finally {
						try {
							DSApi.close();
						} catch (Exception e) {
						}
					}

					Integer sequenceId;
					try {
						sequenceId = Journal.TX_newEntry2(journal.getId(),
								documentIds[i], entryDate);
					} catch (EdmException e) {
						addActionError(e.getMessage());
						return;
					}
					try {
						DSApi.open(AuthUtil.getSubject(ServletActionContext
								.getRequest()));
						DSApi.context().begin();
						OfficeDocument document = OfficeDocument
								.findOfficeDocument(documentIds[i]);
						document.setAssignedDivision(division.getGuid());
						addActionMessage(sm
								.getString(sm
										.getString("PismoZostaloPrzyjeteWdziennikuDzialu"))
								+ " "
								+ division.getName()
								+ " "
								+ sm.getString("PodNumerem") + " " + sequenceId);
						DSApi.context().commit();
					} catch (EdmException e) {
						DSApi.context().setRollbackOnly();
						addActionError(e.getMessage());
					} finally {
						try {
							DSApi.close();
						} catch (Exception e) {
						}
					}

				}
				// DSApi.context().commit();
			} catch (Exception e) {
				// DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}

	}

	/**
	 * Przepisanie zada� innego u�ytkownika na siebie.
	 */
	private class AssignMe implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoZadan"));
				return;
			}
			try {
				DSApi.context().begin();
				DSUser user = null;
				if (nameOfSmallAssignment == null
						|| nameOfSmallAssignment.length() < 1) {
					user = DSApi.context().getDSUser();
				} else {
					try {
						user = DSUser.findByUsername(nameOfSmallAssignment);
					} catch (UserNotFoundException e) {
						log.error("Nie znaleziono uzytkownika Agenta", e);
						new UserNotFoundException(nameOfSmallAssignment);
					}
				}
				if (WorkflowFactory.jbpm) {
					for (String act : activityIds) {
						Long docid = null;
						Jbpm4WorkflowFactory.assignMe(act, docid);
					}
				} else {
					String plannedAssignment = "null/" + user.getName() + "/"
							+ WorkflowUtils.iwfManualName() + "//"
							+ "realizacja";
					WorkflowFactory.multiAssignment(activityIds, username,
							new String[] { plannedAssignment }, null, event);

				}
				DSApi.context().commit();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	private class AddToCase implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (activityIds == null || activityIds.length == 0)
				addActionError(sm.getString("NieWybranoZadan"));

			if (hasActionErrors())
				return;

			try {
				DSApi.context().begin();

				OfficeCase officeCase = OfficeCase.find(caseId);

				for (int i = 0; i < activityIds.length; i++) {

					Long documentId = WorkflowFactory
							.getDocumentId(activityIds[i]);
					if (documentId == null) {
						event.getLog().warn(
								sm.getString("Zzadaniem") + " "
										+ activityIds[i] + " "
										+ "nie jest zwi?zany" + " "
										+ sm.getString("dokument")
										+ " (ds_document_id)");
						continue;
					}

					Document document = Document.find(documentId);

					if (document instanceof OfficeDocument) {
						OfficeDocument doc = (OfficeDocument) document;

						if (doc.getContainingCaseId() == null
								|| !doc.getContainingCaseId().equals(caseId)) {
							doc.setContainingCaseId(caseId, true);
						}
					}
					TaskSnapshot.updateAllTasksByDocumentId(document.getId(),
							document.getStringType());
				}

				DSApi.context().commit();

			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	private class RemoveWatches implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (urns == null || urns.length == 0) {
				addActionError(sm.getString("NieWybranoElementowDoUsuniecia"));
				return;
			}

			try {
				DSApi.context().begin();

				for (String urn : urns) {
					DSApi.context().unwatch(URN.parse(urn));
				}

				DSApi.context().commit();
			} catch (EdmException e) {
				event.getLog().error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	private class RemoveOrders implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoElementowDoUsuniecia"));
				return;
			}

			try {
				DSApi.context().begin();

				if (activityIds != null) {
					for (int i = 0; i < activityIds.length; i++) {
						// WorkflowActivity activity =
						// WorkflowFactory.getWfActivity(activityIds[i]);

						Long documentId = WorkflowFactory
								.getDocumentId(activityIds[i]);
						if (documentId == null) {
							event.getLog().warn(
									sm.getString("Zzadaniem") + " "
											+ activityIds[i] + " "
											+ "nie jest zwi?zany" + " "
											+ sm.getString("dokument")
											+ " (ds_document_id)");
							continue;
						}

						Document document = Document.find(documentId);
						if (document instanceof OfficeDocument) {
							// to normalnie (w summary action) jest wywo�ywane
							// podczas zamykania sprawy
							WorkflowFactory.getInstance().manualFinish(
									activityIds[i], true);
						} else {
							addActionError(sm
									.getString("ToNieJestDokumentKancelaryjny"));
						}
					}
				}

				DSApi.context().commit();
			} catch (EdmException e) {
				event.getLog().error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	/**
	 * Klasa reprezentujaca zadanie CaseTask
	 * 
	 * @TODO - nie jest dobre ze ta klasa jest wewnatrz akcji -- Powinno to byc
	 *       jakos ujednolicone z Taskiem
	 * @author wkutyla
	 * 
	 */
	public static class CaseTask {
		private Long caseId;
		private String caseOfficeId;
		private String caseStatus;
		private Date caseOpenDate;
		private Date caseFinishDate;
		private Integer caseDaysToFinish;
		private String caseDescription;
		private String caseTitle;

		public CaseTask(OfficeCase officeCase) {
			caseId = officeCase.getId();
			caseOfficeId = officeCase.getOfficeId();
			caseStatus = officeCase.getStatus().getName();
			caseOpenDate = officeCase.getOpenDate();
			caseFinishDate = officeCase.getFinishDate();
			caseDaysToFinish = new Integer(officeCase.getDaysToFinish());
			caseDescription = officeCase.getDescription();
			caseTitle = officeCase.getTitle();
		}

		public String getCaseDescription() {
			return caseDescription;
		}

		public Long getCaseId() {
			return caseId;
		}

		public String getCaseOfficeId() {
			return caseOfficeId;
		}

		public String getCaseStatus() {
			return caseStatus;
		}

		public Date getCaseOpenDate() {
			return caseOpenDate;
		}

		public Date getCaseFinishDate() {
			return caseFinishDate;
		}

		public Integer getCaseDaysToFinish() {
			return caseDaysToFinish;
		}

		public String getCaseTitle() {
			return caseTitle;
		}
	}

	private class GenerateXLS implements ActionListener {

		private boolean all = false;
		
		public GenerateXLS(){}
		
		public GenerateXLS(boolean all){
			this.all = all;
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {

			if(all){
				activityIds = null;
			}
			
			HSSFWorkbook workbook = createXlsReport(event);

			try {
				File tmpfile = File.createTempFile("DocuSafe", "tmp");
				tmpfile.deleteOnExit();
				FileOutputStream fis = new FileOutputStream(tmpfile);
				workbook.write(fis);
				fis.close();
				ServletUtils
						.streamFile(ServletActionContext.getResponse(),
								tmpfile, "application/vnd.ms-excel",
								"Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
			} catch (IOException e) {
				log.debug(e.getMessage(), e);
			}

		}

		private HSSFWorkbook createXlsReport(ActionEvent event) {

	        List<Map<String, String>> resultsToGenerate = new ArrayList<Map<String,String>>();
			
			if(tasks!=null) {
				resultsToGenerate = taskListToViewMap(getActualColumns(), tasks);
			}
			else if(cases!=null) {
				resultsToGenerate = casesToViewMap(getActualColumns(), cases);
			}
			
			HSSFWorkbook workbook = new HSSFWorkbook();

			try {
				HSSFSheet sheet = workbook.createSheet("Docusafe Software");

				HSSFCellStyle cellStyle = workbook.createCellStyle();
				cellStyle = workbook.createCellStyle();
				HSSFFont hSSFFont = workbook.createFont();
				hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
				hSSFFont.setFontHeightInPoints((short) 10);
				cellStyle.setFont(hSSFFont);

				HSSFCellStyle headerStyle = workbook.createCellStyle();
				headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setBottomBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setLeftBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setRightBorderColor(HSSFColor.RED.index);

				headerStyle.setTopBorderColor(HSSFColor.RED.index);
				headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);

				int rowNumber = 0;
				
				for (Map<String, String> results : resultsToGenerate) {

					HSSFRow row = sheet.createRow(rowNumber++);
					int cellNumber = 0;

					for (TableColumn c : columns) {
						HSSFCell cell = row.createCell((short) cellNumber++);
						String value = results.get(c.getProperty());
						if (StringUtils.isNotBlank(value)) {
							if (NumberUtils.isNumber(value)) {
								cell.setCellValue(Double.parseDouble(value));
							}
							else {
							cell.setCellValue(value);
							}
						} else {
							cell.setCellValue("");
						}

						if (rowNumber == 1) {
							cell.setCellStyle(headerStyle);
						} else {
							cell.setCellStyle(cellStyle);
						}
					}
				}

			} catch (Exception e) {
				log.debug("", e);
			}

			return workbook;
		}
	}

	private class GenerateCSV implements ActionListener {

		private boolean all = false;
		
		public GenerateCSV(){}
		
		public GenerateCSV(boolean all){
			this.all = all;
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {

			if(all){
				activityIds = null;
			}
			
			List<Map<String, String>> resultsToGenerate = new ArrayList<Map<String,String>>();
			
			if(tasks!=null) {
				resultsToGenerate = taskListToViewMap(getActualColumns(), tasks);
			}
			else if(cases!=null) {
				resultsToGenerate = casesToViewMap(getActualColumns(), cases);
			}
			
			try {

				File tmpcsvfile = File.createTempFile("DocusafeCsv", "tmp");
				CsvDumper dumper = new CsvDumper();

				dumper.openFile(tmpcsvfile);

				for (Map<String, String> results : resultsToGenerate) {
					dumper.newLine();
					for (TableColumn c : columns) {
						String title = results.get(c.getProperty());
						if (StringUtils.isNotBlank(title)) {
							dumper.addText(title);
						} else {
							dumper.addText("");
						}
					}
					dumper.dumpLine();
				}

				ServletUtils
						.streamFile(ServletActionContext.getResponse(),
								tmpcsvfile, "text/csv",
								"Content-Disposition: attachment; filename=\"DocusafeCsvReport.csv\"");

			} catch (Exception e) {
				log.debug("", e);
			}

		}
	}

	private class GenerateXML implements ActionListener {
		
		private boolean all = false;
		
		public GenerateXML(){}
		
		public GenerateXML(boolean all){
			this.all = all;
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {

			if(all){
				activityIds = null;
			}
			
			List<Map<String, String>> resultsToGenerate = new ArrayList<Map<String, String>>();

			if (tasks != null) {
				resultsToGenerate = taskListToViewMap(getActualColumns(), tasks);

			} else if (cases != null) {
				resultsToGenerate = casesToViewMap(getActualColumns(), cases);
			}

			TaskSnapshotXml tasksXml = new TaskSnapshotXml(resultsToGenerate);

			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(TaskSnapshotXml.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				File tmpXmlFile = File.createTempFile("DocusafeXml", "tmp");
				jaxbMarshaller.marshal(tasksXml, tmpXmlFile);
				ServletUtils.streamFile(ServletActionContext.getResponse(), tmpXmlFile, "text/xml", "Content-Disposition: attachment; filename=\"DocusafeXmlReport.xml\"");

			} catch (Exception e) {
				log.error("Blad przy generowaniu pliku XML", e);
			}

		}

	}

	// TODO: czasowo w ten sposob do czasu zalatwienia obserwowanych
	@Deprecated
	private TaskSnapshot initTaskFromID(Long documentId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			if (AvailabilityManager.isAvailable("tasklist.jbpm")
					|| AvailabilityManager.isAvailable("tasklist.jbpm4")) {
				ps = DSApi
						.context()
						.prepareStatement(
								"select ID from DSW_JBPM_TASKLIST WHERE document_id = ? ");
			} else {
				ps = DSApi
						.context()
						.prepareStatement(
								"select ID from dsw_tasklist WHERE dso_document_id = ? ");
			}
			ps.setLong(1, documentId);
			rs = ps.executeQuery();

			if (rs.next()) {
				if (WorkflowFactory.jbpm
						&& Jbpm4Provider.getInstance().isJbpm4Supported()) {
					return DSApi.context().load(JBPMTaskSnapshot.class,
							rs.getLong(1));
				} else {
					return DSApi.context().load(TaskSnapshot.class,
							rs.getLong(1));
				}

			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
		return null;
	}

    public List<Map<String, String>> taskListToViewMap(){
        return taskListToViewMap(getActualColumns(), tasks);
    }
	/**
	 * Metoda s�u�y do generowania mapy z aktualnej task listy wg wybranych
	 * kolumn w panelu u�ytkownika dla pism wychodzacych, przychodzacych, wewnetrznych i obserwowanych
	 * @author Marek Wojciechowski <marek.wojciechowski@docusafe.pl>
	 */

	public List<Map<String, String>> taskListToViewMap(
			List<TableColumn> columns, List<Task> tasks) {

		Map<String, String> columnsComplement = new LinkedHashMap<String, String>();
		List<Map<String, String>> results = new ArrayList<Map<String, String>>();

		for (int i = 0; i < columns.size(); i++) {
			columnsComplement.put(columns.get(i).getProperty().toString(),
					columns.get(i).getTitle().toString());
		}
		results.add(columnsComplement);

		int i = 1;
		
		checkAndPrepareSelectecTask();

		for (Task task : tasks) {
			JBPMTaskSnapshot tasksnp = (JBPMTaskSnapshot) task;
			columnsComplement = new LinkedHashMap<String, String>();
			for (TableColumn c : columns) {

				if ("receiveDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getReceiveDate());
				} else if ("sender".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getSender());
				} else if ("userFlags".equals(c.getProperty())) {
					setColumnList(columnsComplement, c.getProperty(),
							tasksnp.getUserFlags());
				} else if ("documentOfficeNumber".equals(c.getProperty())) {
					setColumnInteger(columnsComplement, c.getProperty(),
							tasksnp.getDocumentOfficeNumber());
				} else if ("documentRecipient".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDocumentRecipient());
				} else if ("documentSummary".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDocumentSummary());
				} else if ("description".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDescription());
				} else if ("documentDelivery".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDocumentDelivery());
				} else if ("objective".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getObjective());
				} else if ("documentPrepState".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDocumentPrepState());
				} else if ("documentReferenceId".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDocumentReferenceId());
				} else if ("deadlineTime".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getDeadlineTime());
				} else if ("clerk".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getClerk());
				} else if ("lastRemark".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getLastRemark());
				} else if ("dockindStatus".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindStatus());
				} else if ("dockindNumerDokumentu".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindNumerDokumentu());
				} else if ("dockindKwota".equals(c.getProperty())) {
					setColumnBigDecimal(columnsComplement, c.getProperty(),
							tasksnp.getDockindKwota());
				} else if ("dockindKategoria".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindKategoria());
				} else if ("documentDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getDocumentDate());
				} else if ("documentId".equals(c.getProperty())) {
					setColumnLong(columnsComplement, c.getProperty(), tasksnp
							.getDocumentId());
				} else if ("documentCtime".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getDocumentCtime());
				} else if ("decretationTo".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDecretationTo());
				} else if ("author".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getAuthor());
				} else if ("decretation_to_div".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDecretation_to_div());
				} else if ("shortReceiveDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getShortReceiveDate());
				} else if ("process".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getProcess());
				} else if ("officeCase".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getOfficeCase());
				} else if ("dockindName".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindName());
				} else if ("newProcessStatus".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getNewProcessStatus());
				} else if ("answerCounter".equals(c.getProperty())) {
					setColumnInteger(columnsComplement, c.getProperty(),
							tasksnp.getAnswerCounter());
				} else if ("incomingDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getIncomingDate());
				} else if ("receiver".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getReceiver());
				} else if ("orderStatus".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getOrderStatus());
				} else if ("caseOfficeId".equals(c.getProperty())) {
					setColumnLong(columnsComplement, c.getProperty(),
							tasksnp.getCaseId());
				} else if ("caseFinishDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							tasksnp.getCaseDeadlineDate());
				} else if ("dockindBusinessAtr1".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr1());
				} else if ("dockindBusinessAtr2".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr2());
				} else if ("dockindBusinessAtr3".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr3());
				} else if ("dockindBusinessAtr4".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr4());
				} else if ("dockindBusinessAtr5".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr5());
				} else if ("dockindBusinessAtr6".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							tasksnp.getDockindBusinessAtr6());
				} else if("numberRows".equals(c.getProperty())){
					setColumnString(columnsComplement, c.getProperty(), (i++)+".");
				}
			}
			results.add(columnsComplement);
		}
		return results;
	}
	/**
	 * Metoda słuzy do sprawrzenia i usuniecia nie zaznaczonych do wydruku tasków
	 */
	private void checkAndPrepareSelectecTask()
	{
		if (activityIds != null && activityIds.length > 0)
		{
			ArrayList<Task> taskToGenerate = new ArrayList<Task>();

			for (Task task : tasks)
			{
				for (String element : activityIds)
					if (element.equals(task.getActivityId().toString()))
					{
						taskToGenerate.add(task);
					}
				if (activityIds.length == taskToGenerate.size())
					break;
			}
			tasks.clear();
			tasks.addAll(taskToGenerate);
		}
		
	}

	
	/**
	 * Metoda s�u�y do generowania mapy z aktualnej task listy wg wybranych
	 * kolumn w panelu u�ytkownika dla spraw
	 * @author Marek Wojciechowski <marek.wojciechowski@docusafe.pl>
	 */
	public List<Map<String, String>> casesToViewMap(
			List<TableColumn> columns, Collection<CaseTask> cases) {

		Map<String, String> columnsComplement = new HashMap<String, String>();
		List<Map<String, String>> results = new ArrayList<Map<String, String>>();

		for (int i = 0; i < columns.size(); i++) {
			columnsComplement.put(columns.get(i).getProperty().toString(),
					columns.get(i).getTitle().toString());
		}
		results.add(columnsComplement);

		for (CaseTask caseTaskList : cases) {
			columnsComplement = new HashMap<String, String>();
			for (TableColumn c : columns) {

				if ("caseOfficeId".equals(c.getProperty())) {
					setColumnLong(columnsComplement, c.getProperty(),
							caseTaskList.getCaseId());
				} else if ("caseStatus".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							caseTaskList.getCaseStatus());
				} 
				else if ("caseFinishDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							caseTaskList.getCaseFinishDate());
				}
				else if ("caseDaysToFinish".equals(c.getProperty())) {
					setColumnInteger(columnsComplement, c.getProperty(),
							caseTaskList.getCaseDaysToFinish());
				}
				else if ("caseTitle".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(),
							caseTaskList.getCaseTitle());
				}
				else if ("caseOpenDate".equals(c.getProperty())) {
					setColumnDate(columnsComplement, c.getProperty(),
							caseTaskList.getCaseOpenDate());
				}
				else if ("documentCtime".equals(c.getProperty())) {
					setColumnString(columnsComplement, c.getProperty(), "");
				}
			}
			results.add(columnsComplement);
		}
		return results;
	}

	protected Long[] getWatchesIDs(DSUser user) throws EdmException {
		log.info("Metoda getWatchesIDs(DSUser");
		List<URN> urns = DSApi.context().watches(user);
		// Long[] ids = new Long[urns.size()];
		List<Long> ids = new ArrayList<Long>();

		for (int i = 0; i < urns.size(); i++) {
			if (urns.get(i) instanceof DocumentURN)
				ids.add(((DocumentURN) urns.get(i)).getId());
		}

		return ids.toArray(new Long[ids.size()]);
	}

	protected Long[] getWatchesIDs(DSUser[] users) throws EdmException {
		log.info("Metoda getWatchesIDs(DSUser[]");
		List<Long> ids = new ArrayList<Long>();

		for (DSUser user : users) {
			List<URN> urns = DSApi.context().watches(user);
			// Long[] ids = new Long[urns.size()];
			for (int i = 0; i < urns.size(); i++) {
				if (urns.get(i) instanceof DocumentURN)
					ids.add(((DocumentURN) urns.get(i)).getId());
			}
		}

		return ids.toArray(new Long[ids.size()]);
	}

	private List<Task> updateWatches(DSUser user, String parsedFilterBy,
			Object filterValue) {
		List<Task> results = new ArrayList<Task>();
		List<Long> documentIds = new ArrayList<Long>();
		try {
			List<URN> urns = DSApi.context().watches(user);

			for (URN urn : urns) {
				Task task = new Task();
				task.setUrn(urn.toExternalForm());
				try {
					if (urn instanceof DocumentURN) {
						// documentIds.add(((DocumentURN) urn).getId());
						task = initTaskFromID(((DocumentURN) urn).getId());
						if (task != null) {
							task.setDocumentTask(true);
							results.add(task);
						}
					} else if (urn instanceof WfActivityURN) {
						if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
							continue;
						}
						WfActivity wfa = WorkflowFactory.getInstance()
								.getOldActivity(((WfActivityURN) urn).getKey());
						Long id = WorkflowUtils.findParameterValue(wfa
								.container().process_context(),
								"ds_document_id", Long.class);
						if (id == null)
							throw new EntityNotFoundException(
									sm.getString("BrakIdentyfikatoraDokumentuWprocesie")
											+ " " + wfa.container().key());
						Document document = Document.find(id);

						task.setLink(Links.getLink(document, "office", true));
						task.setDocumentTask(true);
						task.setDocumentId(document.getId());
						task.setDocumentFax("fax".equals(document.getSource()));
						if (document instanceof OfficeDocument) {
							OfficeDocument doc = (OfficeDocument) document;
							task.setDocumentOfficeNumber(doc.getOfficeNumber());
							task.setDocumentOfficeNumberYear(doc
									.getOfficeNumberYear());
							task.setDocumentSummary(doc.getSummary());
							if (document.getType() == DocumentType.INCOMING) {
								InOfficeDocument indoc = (InOfficeDocument) doc;
								task.setDocumentReferenceId(indoc
										.getReferenceId());
								if (indoc.getSender() != null) {
									task.setDocumentSender(indoc.getSender()
											.getSummary());
								}
							}
						}
						task.setWorkflowName(InternalWorkflowService.NAME);
						task.setActivityKey(wfa.key());
						task.setActivityId(ActivityId.make(
								task.getWorkflowName(), task.getActivityKey()));
						task.setReceiveDate(wfa.last_state_time());
						task.setDescription(wfa.name());
						task.setSender(DSUser
								.safeToFirstnameLastname(WorkflowUtils
										.findParameterValue(
												wfa.process_context(),
												"ds_last_activity_user",
												String.class)));
						task.setObjective(WorkflowUtils.findParameterValue(
								wfa.process_context(), "ds_objective",
								String.class));
						task.setProcess(wfa.container().name());
						// documentType
						task.setPackageId(wfa.container().manager()
								.package_id());
						task.setProcessDefinitionId(wfa.container().manager()
								.process_definition_id());

						if (wfa.container() instanceof WfProcessImpl) {
							WfProcessImpl wfpi = (WfProcessImpl) wfa
									.container();
							if (wfpi.getDeadlineTime() != null) {
								task.setDeadlineTime(wfpi.getDeadlineTime());
								task.setDeadlineRemark(wfpi.getDeadlineRemark());
								task.setDeadlineAuthor(DSUser
										.safeToFirstnameLastname(wfpi
												.getDeadlineAuthor()));

								if (wfpi.getDeadlineTime().getTime() < System
										.currentTimeMillis()) {
									task.setPastDeadline(true);
								}
							}
						}

						results.add(task);
					} else if (urn instanceof WfProcessURN) {
						if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
							continue;
						}
						WfProcess wfp = DSApi.context().getWfProcess(
								(WfProcessURN) urn);
						if (wfp == null)
							throw new EntityNotFoundException(
									sm.getString("BrakProcesu"));
						if (wfp.state().equals("closed.completed"))
							continue;
						Long id = WorkflowUtils.findParameterValue(
								wfp.process_context(), "ds_document_id",
								Long.class);
						if (id == null)
							throw new EntityNotFoundException(
									sm.getString("BrakIdentyfikatoraDokumentuWprocesie")
											+ " " + wfp.key());
						Document document = Document.find(id);

						task.setLink(Links.getLink(
								document,
								"office",
								true,
								new pair<String, String>("process", urn
										.toExternalForm())));
						task.setDocumentTask(true);
						task.setDocumentId(document.getId());
						task.setDocumentFax("fax".equals(document.getSource()));
						if (document instanceof OfficeDocument) {
							OfficeDocument doc = (OfficeDocument) document;
							task.setDocumentOfficeNumber(doc.getOfficeNumber());
							task.setDocumentOfficeNumberYear(doc
									.getOfficeNumberYear());
							task.setDocumentSummary(doc.getSummary());
							if (document.getType() == DocumentType.INCOMING) {
								InOfficeDocument indoc = (InOfficeDocument) doc;
								task.setDocumentReferenceId(indoc
										.getReferenceId());
								if (indoc.getSender() != null) {
									task.setDocumentSender(indoc.getSender()
											.getSummary());
								}
							}
						}
						task.setWorkflowName(InternalWorkflowService.NAME);
						task.setProcess(wfp.name());
						// documentType
						task.setPackageId(wfp.manager().package_id());
						task.setProcessDefinitionId(wfp.manager()
								.process_definition_id());

						if (wfp instanceof WfProcessImpl) {

							WfProcessImpl wfpi = (WfProcessImpl) wfp;
							if (wfpi.getDeadlineTime() != null) {
								task.setDeadlineTime(wfpi.getDeadlineTime());
								task.setDeadlineRemark(wfpi.getDeadlineRemark());
								task.setDeadlineAuthor(DSUser
										.safeToFirstnameLastname(wfpi
												.getDeadlineAuthor()));

								if (wfpi.getDeadlineTime().getTime() < System
										.currentTimeMillis()) {
									task.setPastDeadline(true);
								}
							}

							WfActivity[] wfas = wfp
									.get_activities_in_state_array("open.running");
							if (wfas.length > 0) {
								task.setObjective(WorkflowUtils
										.findParameterValue(
												wfas[0].process_context(),
												"ds_objective", String.class));
								task.setReceiveDate(wfas[0].last_state_time());
								task.setDescription(wfas[0].name());
								task.setSender(DSUser.safeToFirstnameLastname(WorkflowUtils
										.findParameterValue(
												wfas[0].process_context(),
												"ds_last_activity_user",
												String.class)));
							}
						}

						results.add(task);
					} else if (urn instanceof WorkflowActivityURN) {
						String activityKey = ((WorkflowActivityURN) urn)
								.getKey();
						task = initTask(activityKey);

						if (task != null) {
							task.setDocumentTask(true);
							task.setUrn(urn.toExternalForm());
						} else {
							continue;
						}
						// Long id = wfa.getDocumentId();

						/*
						 * String activityId =
						 * WorkflowFactory.activityIdOfWfNameAndKey("internal",
						 * activityKey);
						 * 
						 * WorkflowActivity wfa = null; if
						 * (AvailabilityManager.isAvailable("tasklist.jbpm")) {
						 * try { wfa =
						 * WorkflowFactory.getWfActivity(activityId); } catch
						 * (Exception e){
						 * log.debug("watches: nie znaleziono zadania z kluczem: {}"
						 * ,activityKey); continue; } } else { wfa =
						 * WorkflowFactory.getWfActivity(activityId); } Long id
						 * = wfa.getDocumentId(); if (id == null) throw new
						 * EntityNotFoundException
						 * (sm.getString("BrakIdentyfikatoraDokumentuWprocesie"
						 * )+" "+activityId); Document document =
						 * Document.find(id);
						 * task.setLink(Links.getLink(document, "office",
						 * true)); task.setDocumentTask(true);
						 * task.setDocumentId(document.getId());
						 * task.setDocumentFax
						 * ("fax".equals(document.getSource())); if (document
						 * instanceof OfficeDocument) { OfficeDocument doc =
						 * (OfficeDocument) document;
						 * task.setDocumentOfficeNumber(doc.getOfficeNumber());
						 * task
						 * .setDocumentOfficeNumberYear(doc.getOfficeNumberYear
						 * ()); task.setDocumentSummary(doc.getSummary()); if
						 * (document.getType() == DocumentType.INCOMING) {
						 * InOfficeDocument indoc = (InOfficeDocument) doc;
						 * task.setDocumentReferenceId(indoc.getReferenceId());
						 * if (indoc.getSender() != null) {
						 * task.setDocumentSender
						 * (indoc.getSender().getSummary()); } } }
						 * task.setWorkflowName(InternalWorkflowService.NAME);
						 * task
						 * .setActivityKey(WorkflowFactory.getInstance().keyOfId
						 * (wfa.getTaskFullId()));
						 * //task.setActivityId(ActivityId
						 * .make(task.getWorkflowName(),
						 * task.getActivityKey()));
						 * task.setReceiveDate(wfa.getReceiveDate());
						 * task.setDescription(wfa.getProcessDesc());
						 * task.setSender
						 * (DSUser.safeToFirstnameLastname(wfa.getLastUser()));
						 * task.setObjective(wfa.getObjective());
						 * task.setProcess(wfa.getProcessKind()); //
						 * documentType
						 * //task.setPackageId(wfa.container().manager
						 * ().package_id());
						 * //task.setProcessDefinitionId(wfa.container
						 * ().manager().process_definition_id()); if
						 * (wfa.getDeadlineTime() != null) {
						 * task.setDeadlineTime(wfa.getDeadlineTime());
						 * task.setDeadlineRemark(wfa.getDeadlineRemark());
						 * String deadlineAuthor = wfa.getDeadlineAuthor(); if
						 * (deadlineAuthor != null) {
						 * task.setDeadlineAuthor(DSUser
						 * .safeToFirstnameLastname(deadlineAuthor)); } if
						 * (wfa.getDeadlineTime().getTime() <
						 * System.currentTimeMillis()) {
						 * task.setPastDeadline(true); } }
						 */

						results.add(task);
					}
				} catch (EntityNotFoundException e) {
					log.warn(e.getMessage() + ", urn=" + urn, e);
				}
			}
		} catch (EdmException e) {
			addActionError(e.getMessage());
		}
		if (AvailabilityManager.isAvailable("tasklist.watches.sort")) {
			Collections.sort(results, new Task.TaskComparator());
		}
		return results;
	}

	// TODO: czasowo w ten sposob do czasu zalatwienia obserwowanych
	@Deprecated
	private TaskSnapshot initTask(String activityKey) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
				ps = DSApi.context().prepareStatement(
						"select ID from dsw_tasklist WHERE activity_key = ? ");
			} else {
				ps = DSApi
						.context()
						.prepareStatement(
								"select ID from dsw_tasklist WHERE dsw_activity_activity_key = ? ");
			}
			ps.setString(1, activityKey);
			rs = ps.executeQuery();

			if (rs.next()) {
				return DSApi.context().load(TaskSnapshot.class, rs.getLong(1));
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
		return null;
	}

	private class AddLabel implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (documentIds == null || documentIds.length == 0) {
				addActionError(sm.getString("NieWybranoDokumentow"));
				return;
			}
			try {
				LabelsManager.addLabel(documentIds, labelId, DSApi.context()
						.getPrincipalName());
			} catch (EdmException e) {
				addActionError(e.getMessage());
			}
		}
	}

	private class RemoveLabelFromView implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (removeLabelId != null || removeLabelId > 0) {
				// viewedLabels =
				// (Collection<Label>)ServletActionContext.getRequest().getSession().getAttribute(preparePrefix()+"viewedLabels");
				Label toRemove = null;
				for (Label l : viewedLabels) {
					if (l.getId().equals(removeLabelId))
						toRemove = l;
				}
				viewedLabels.remove(toRemove);
				removeLabelId = null;
				viewedLabelId = 0L;
				if (viewedLabels.size() == 0)
					viewedLabelId = -2L;
				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(preparePrefix() + "viewedLabels",
								viewedLabels);
			}
		}
	}

	private class RefreshUserTaskList implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			LoggerFactory.getLogger("tomekl").debug("refresh " + username);

			if (username == null) {
				addActionError(sm.getString("NieWybranoUzytkownika"));
				return;
			}

			try {
				DSApi.context().begin();
				TasklistSynchro.Synchro(username);
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context()._rollback();
				addActionError(e.getMessage());
			}
		}

	}

	private class ManualPushToCoor implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if ((activityIds == null || activityIds.length == 0))// &&
																	// (actionCaseIds
																	// == null
																	// ||
																	// actionCaseIds.length
																	// == 0))
			{
				addActionError(sm.getString("NieWybranoZadan"));
			}

			for (String activityId : activityIds) {
				try {
					DSApi.context().begin();

					WorkflowFactory.forwardToCoordinator(activityId, event);

					DSApi.context().commit();
				} catch (Exception e) {
					addActionError(e.getMessage());
					DSApi.context()._rollback();
					// LoggerFactory.getLogger("tomekl").debug("wtf",e);
					log.debug(e);
				}
			}

		}
	}

	public String getTab() {
		return tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	public String getOrderTab() {
		return orderTab;
	}

	public void setOrderTab(String orderTab) {
		this.orderTab = orderTab;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List getTasks() {
		return tasks;
	}

	public Tabs getTabs() {
		return tabs;
	}

	public Tabs getOrderTabs() {
		return orderTabs;
	}

	public List getUsers() {
		return users;
	}

	public List getColumns() {
		return columns;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public Collection getCases() {
		return cases;
	}

	public void setSearchOfficeNumber(Integer searchOfficeNumber) {
		this.searchOfficeNumber = searchOfficeNumber;
	}

	public void setSearchOfficeId(String searchOfficeId) {
		this.searchOfficeId = searchOfficeId;
	}

	public void setActivityIds(String[] activityIds) {
		this.activityIds = activityIds;
	}

	public boolean isActionManualFinish() {
		return actionManualFinish;
	}

	public boolean isActionAssignOfficeNumber() {
		return actionAssignOfficeNumber;
	}

	public boolean isActionAttachmentsAsPdf() {
		return actionAttachmentsAsPdf;
	}

	public boolean isActionAddToCase() {
		return actionAddToCase;
	}

	public boolean isActionAssignMe() {
		return actionAssignMe;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	/*
	 * public void setCaseInOfficeFinishDate(String caseInOfficeFinishDate) {
	 * this.caseInOfficeFinishDate = caseInOfficeFinishDate; }
	 */
	public void setDocumentIds(Long[] documentIds) {
		this.documentIds = documentIds;
	}

	public void setActionCaseIds(Long[] actionCaseIds) {
		this.actionCaseIds = actionCaseIds;
	}

	public Long[] getActionCaseIds() {
		return actionCaseIds;
	}

	public boolean isActionMultiAssignment() {
		return actionMultiAssignment;
	}

	public boolean isActionMultiBoxing() {
		return actionMultiBoxing;
	}

	public boolean isActionMultiNote() {
		return actionMultiNote;
	}

	public boolean isActionDeleteOfficeNumber() {
		return actionDeleteOfficeNumber;
	}

	public List<Task> getWatches() {
		return watches;
	}

	public void setUrns(String[] urns) {
		this.urns = urns;
	}

	public Pager getPager() {
		return pager;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Deprecated
	public boolean isSimpleTaskList() {
		return false;
	}

	@Deprecated
	public void setSimpleTaskList(boolean simpleTaskList) {
		//
	}

	@Deprecated
	public boolean isFullTaskList() {
		return false;
	}

	@Deprecated
	public void setFullTaskList(boolean fullTaskList) {
		//
	}

	public boolean getAddToTask() {
		return addToTask;
	}

	public void setAddToTask(boolean addToTask) {
		this.addToTask = addToTask;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	//
	// public String getAddToReceiver()
	// {
	// return addToReceiver;
	// }
	//
	// public void setAddToReceiver(String addToReceiver)
	// {
	// this.addToReceiver = addToReceiver;
	// }

	public List<DocumentKind> getDockindNames() {
		return dockindNames;
	}

	public void setFilterDockindNames(String arg) {
		this.filterDockindNames = arg;
	}

	public String getFilterDockindNames() {
		return filterDockindNames;
	}

	public List<SmallAssignmentBean> getSmallAssignments() {
		return smallAssignments;
	}

	public List<DepartmentEnrollBean> getIncomingDepartments() {
		return incomingDepartments;
	}

	public void setNameOfSmallAssignment(String name) {
		nameOfSmallAssignment = name;
	}

	public void setNameOfJournal(String name) {
		nameOfJournal = name;
	}

	public boolean isActionReplyUnnecessary() {
		return actionReplyUnnecessary;
	}

	public void setActionReplyUnnecessary(boolean actionReplyUnnecessary) {
		this.actionReplyUnnecessary = actionReplyUnnecessary;
	}

	public boolean isActionSendToEva() {
		return actionSendToEva;
	}

	public boolean isActionMultiDepartmentEnroll() {
		return actionDepartmentEnroll;
	}

	public boolean isMultiDepartmentEnroll() {
		return multiDepartmentEnroll;
	}

	public void setMultiDepartmentEnroll(boolean multiDepartmentEnroll) {
		this.multiDepartmentEnroll = multiDepartmentEnroll;
	}

	public String getFilterBy() {
		return filterBy;
	}

	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
		if (filterName == null
				|| (filterName != null && filterName.equalsIgnoreCase("null"))) {
			this.filterName = "";
		}
	}

	public List<FilterColumn> getfilterColumns() {
		return filterColumns;
	}

	public void setfilterColumns(List<FilterColumn> filterColumns) {
		this.filterColumns = filterColumns;
	}

	public Integer getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(Integer taskCount) {
		this.taskCount = taskCount;
	}

	public Boolean getWithBackup() {
		return withBackup;
	}

	public void setWithBackup(Boolean withBackup) {
		this.withBackup = withBackup;
	}

	public boolean isOnTaskList() {
		return onTaskList;
	}

	public void setOnTaskList(boolean onTaskList) {
		this.onTaskList = onTaskList;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public Long getLabelId() {
		return labelId;
	}

	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}

	public Long getViewedLabelId() {
		return viewedLabelId;
	}

	public void setViewedLabelId(Long viewedLabelId) {
		this.viewedLabelId = viewedLabelId;
	}

	public List<Label> getAvailableForAddLabels() {
		return availableForAddLabels;
	}

	public void setAvailableForAddLabels(List<Label> availableForAddLabels) {
		this.availableForAddLabels = availableForAddLabels;
	}

	public List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> getEnableAcceptances() {
		return enableAcceptances;
	}

	public void setEnableAcceptances(
			List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> enableAcceptances) {
		this.enableAcceptances = enableAcceptances;
	}

	public String getAcceptanceCn() {
		return acceptanceCn;
	}

	public void setAcceptanceCn(String acceptanceCn) {
		this.acceptanceCn = acceptanceCn;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public Collection<Label> getViewedLabels() {
		return viewedLabels;
	}

	public void setViewedLabels(Collection<Label> viewedLabels) {
		this.viewedLabels = viewedLabels;
	}

	public Long getRemoveLabelId() {
		return removeLabelId;
	}

	public void setRemoveLabelId(Long removeLabelId) {
		this.removeLabelId = removeLabelId;
	}

	public List<Label> getUserFlags_label() {
		return userFlags_label;
	}

	public void setUserFlags_label(List<Label> userFlags_label) {
		this.userFlags_label = userFlags_label;
	}

	public List<Label> getSystemFlags_label() {
		return systemFlags_label;
	}

	public void setSystemFlags_label(List<Label> systemFlags_label) {
		this.systemFlags_label = systemFlags_label;
	}

	public List<Label> getLabels_label() {
		return labels_label;
	}

	public void setLabels_label(List<Label> labels_label) {
		this.labels_label = labels_label;
	}

	public void setShowLabels(boolean showLabels) {
		this.showLabels = showLabels;
	}

	public boolean isShowLabels() {
		return showLabels;
	}

	public List<Label> getSystemLabels_label() {
		return systemLabels_label;
	}

	public void setSystemLabels_label(List<Label> systemLabels_label) {
		this.systemLabels_label = systemLabels_label;
	}

	public Date getCaseDeadlineDate() {
		return caseDeadlineDate;
	}

	public void setCaseDeadlineDate(Date caseDeadlineDate) {
		this.caseDeadlineDate = caseDeadlineDate;
	}

	public boolean isAdminAccess() {
		return adminAccess;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Sortowanie tasklisty po niektorych parametrach
	 * 
	 * @param tasks
	 * @return
	 */
	public List<Task> sortTask(List<Task> tasks) {
		if (log.isTraceEnabled()) {
			log.trace("sortowanie tasklisty");
			log.trace("sortField={}", sortField);
			log.trace("ascending={}", ascending);
		}
		final NumberFormat millisFormat = NumberFormat.getIntegerInstance();
		millisFormat.setMinimumIntegerDigits(("" + Long.MAX_VALUE).length());
		millisFormat.setGroupingUsed(false);
		millisFormat.setParseIntegerOnly(true);
		// napis o najmniejszej mo�liwej warto?ci u�ywany zamiast
		// napisu pustego w sortowaniu leksykalnym
		final String MIN_STR = String.valueOf((char) 0);
		if (tasks != null && tasks.size() > 1) {
			List<String> sortKeys = new ArrayList<String>(tasks.size());

			for (Iterator iter = tasks.iterator(); iter.hasNext();) {
				TaskSnapshot task = (TaskSnapshot) iter.next();
				String key;
				String onKey = task.getDocumentOfficeNumber() != null ? millisFormat
						.format(task.getDocumentOfficeNumber()) : millisFormat
						.format(0);

				// tylko kilka "if-�w", bo sortowanie po pozosta�ych polach
				// odbywa si� w zapytaniu sql-owym
				if ("documentSender".equals(sortField)) {
					key = task.getDocumentSender() != null ? task
							.getDocumentSender().toUpperCase() : MIN_STR;
				} else if ("sender".equals(sortField)) {
					key = task.getSender() != null ? task.getSender()
							.toUpperCase() : MIN_STR;
				} else if ("receiver".equals(sortField)) {
					key = task.getReceiver() != null ? task.getReceiver()
							.toUpperCase() : MIN_STR;
				} else if ("author".equals(sortField)) {
					key = task.getAuthor() != null ? task.getAuthor()
							.toUpperCase() : MIN_STR;
				} else if ("orderStatus".equals(sortField)) {
					key = task.getOrderStatus() != null ? task.getOrderStatus()
							.toUpperCase() : MIN_STR;
				} else if ("clerk".equals(sortField)) {
					key = task.getClerk() != null ? task.getClerk()
							.toUpperCase() : MIN_STR;
				} else if ("flags".equals(sortField)) {
					StringBuilder sb = new StringBuilder();
					if (task.getFlags() != null) {
						for (int i = 0; i < task.getFlags().size(); i++)
							sb.append((((List<FlagBean>) task.getFlags())
									.get(i).getC()));
						key = sb.toString();
					} else
						key = MIN_STR;
				} else if ("userFlags".equals(sortField)) {
					StringBuilder sb = new StringBuilder();
					if (task.getUserFlags() != null) {
						for (int i = 0; i < task.getUserFlags().size(); i++)
							sb.append((((List<FlagBean>) task.getUserFlags())
									.get(i).getC()));
						key = sb.toString();
					} else
						key = MIN_STR;
				} else // documentOfficeNumber
				{
					key = onKey;
				}

				// na ko�cu klucza zawsze numer kancelaryjny, poniewa�
				// jest to poboczny klucz sortowania
				key += ("<>" + onKey);

				sortKeys.add(key);
			}

			Task[] taskArray = (Task[]) tasks.toArray(new Task[tasks.size()]);
			std.sort.qsort(
					(String[]) sortKeys.toArray(new String[sortKeys.size()]),
					taskArray, ascending);

			return Arrays.asList(taskArray);
		} else {
			return tasks;
		}
	}

	public String[] getActivityIds() {
		return activityIds;
	}

	public Long[] getDocumentIds() {
		return documentIds;
	}

	public String getDockindEventValue() {
		return dockindEventValue;
	}

	public Document getDocument() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPlace() {
		return DockindButtonField.TASKLIST;
	}

	public Map<String, Object> getValues() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDockindEventValue(String dockindEventValue) {
		this.dockindEventValue = dockindEventValue;

	}

	public void setValues(Map<String, Object> values) {
		// TODO Auto-generated method stub

	}

	public static List<DockindButtonField> getButtons() {
		return buttons;
	}

	public static void setButtons(List<DockindButtonField> buttons) {
		TaskListAction.buttons = buttons;
	}

	public void setOrderTabs(Tabs orderTabs) {
		this.orderTabs = orderTabs;
	}

	public LinkedHashMap<String, String> getUsersAndDivisions() {
		return usersAndDivisions;
	}

	public void setTabs(Tabs tabs) {
		this.tabs = tabs;
	}

	public Boolean getAscending() {
		return ascending;
	}

	public void setOrderTab(Tabs orderTabs) {
		this.orderTabs = orderTabs;
	}

	/**
	 * Metoda szablonowa okre�laj�ca czy mamy do czynienia z taksklist�
	 * zak�adek.
	 * 
	 * @return
	 */
	public boolean isBookmarkTasklist() {
		return false;
	}

	/**
	 * Przygotowanie filtr�w dla zak�adek.
	 * 
	 * @return
	 */
	protected List<FilterCondition> prepareFilterConditions() {
		return new ArrayList<FilterCondition>();
	}

	/**
	 * Przygotowanie dodatkowych etykiet dla zak�adek.
	 */
	protected void prepareBookmarksLabels(Collection<Label> labels) {
	}

	public Long getBookmarkId() {
		return bookmarkId;
	}

	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}

	public Boolean getShowNumberNewTask() {
		return true;
	}

	public void setShowNumberNewTask(Boolean showNumberNewTask) {
		this.showNumberNewTask = showNumberNewTask;
	}

	protected Bookmark getBookmark() {
		return bookmark;
	}

	protected void setBookmark(Bookmark bookmark) {
		this.bookmark = bookmark;
	}

	protected void prepareBookmarksTasklist() {
	}

	protected String preparePrefix() {
		return this.tab;
	}

	/**
	 * Metoda zwraca informacj� czy sortowanie jest rosn�ce.
	 * 
	 * @return prawda w przypadku sortowania rosn�cego, fa�sz - sortowanie
	 *         malej�ce
	 */
	protected boolean getSortOrder() {
		return Boolean.parseBoolean(DSApi.context().userPreferences()
				.node("task-list").get("ascending" + tab, "false"));
	}

	/**
	 * Metoda zwraca domy�l� kolumn� po kt�rej sortujemy.
	 * 
	 * @return
	 */
	protected String getDefaultSortField() {
		return DSApi.context().userPreferences().node("task-list")
				.get("sort-column_" + tab, "receiveDate");
	}

	/**
	 * Metoda zwraca domy�l� liczb� zada� na stronie.
	 * 
	 * @return
	 */
	protected Integer getDefaultLimit() {
		return limit != null ? limit : DSApi.context().userPreferences().node("task-list")
				.getInt("task-count-per-page" + tab, 20);
	}

	public boolean isCalendarTab() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getUrlTabString() {
		return urlTabString;
	}

	public void setUrlTabString(String urlTabString) {
		this.urlTabString = urlTabString;
	}

	public static void setTaskListEvents(Map<String, String> taskListEvents) {
		TaskListAction.taskListEvents = taskListEvents;
	}

	public static Map<String, String> getTaskListEvents() {
		log.debug("TaskListEvent: {}", taskListEvents);
		return taskListEvents;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public String getDependsDocsJson() {
		log.debug("TaskList.docIds: {}", dependsDocsJson);
		return dependsDocsJson;
	}

	public void setDependsDocsJson(String dependsDocsJson) {
		this.dependsDocsJson = dependsDocsJson;
	}

	public class StringValueComparator implements Comparator {
		Map base;
		Integer compareResult = null;

		public StringValueComparator(Map base) {
			this.base = base;
		}

		public int compare(Object a, Object b) {
			compareResult = ((String) base.get(a)).compareTo((String) base
					.get(b));
			if (compareResult > 0)
				return 1;
			else if (compareResult < 0)
				return -1;
			else
				return 0;
		}
	}

	public boolean isWatchesShowFilter() {
		return watchesShowFilter;
	}

	public boolean isActionPrintEnvelopes() {
		return actionPrintEnvelopes;
	}

	public void setActionPrintEnvelopes(boolean actionPrintEnvelopes) {
		this.actionPrintEnvelopes = actionPrintEnvelopes;
	}

	public Integer getTaskInPackageCount() {
		return taskInPackageCount;
	}

	public void setTaskInPackageCount(Integer taskInPackageCount) {
		this.taskInPackageCount = taskInPackageCount;
	}

	public boolean isOwnWatches() {
		return ownWatches;
	}

	public void setOwnWatches(boolean ownWatches) {
		this.ownWatches = ownWatches;
	}

	private void setColumnDate(Map<String, String> columnsComplement,
			String prop, Date value) {
		if (value != null) {
			columnsComplement.put(prop, DateUtils.formatJsDate(value));
		} else {
			columnsComplement.put(prop, "");
		}
	}

	private void setColumnString(Map<String, String> columnsComplement,
			String prop, String value) {
		if (StringUtils.isNotBlank(value)) {
			columnsComplement.put(prop, value);
		} else {
			columnsComplement.put(prop, "");
		}
	}

	private void setColumnBigDecimal(Map<String, String> columnsComplement,
			String prop, BigDecimal value) {
		if (value != null) {
			columnsComplement.put(prop, value.toString());
		} else {
			columnsComplement.put(prop, "");
		}
	}

	private void setColumnInteger(Map<String, String> columnsComplement,
			String prop, Integer value) {
		if (value != null) {
			columnsComplement.put(prop, Integer.toString(value));
		} else {
			columnsComplement.put(prop, "");
		}
	}

	private void setColumnList(Map<String, String> columnsComplement,
			String prop, List list) {
		if (!list.isEmpty()) {
			columnsComplement.put(prop, list.toString());
		} else {
			columnsComplement.put(prop, "");
		}
	}

	private void setColumnLong(Map<String, String> columnsComplement,
			String prop, Long value) {
		if (value != null) {
			columnsComplement.put(prop, Long.toString(value));
		} else {
			columnsComplement.put(prop, "");
		}
	}

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

}