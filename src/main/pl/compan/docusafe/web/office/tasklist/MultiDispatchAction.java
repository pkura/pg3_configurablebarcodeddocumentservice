package pl.compan.docusafe.web.office.tasklist;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class MultiDispatchAction extends EventActionSupport {
	
	private String error;
	private String username;
	private Long[] documentIds;
    private String[] activityIds;
    private String message;

	private StringManager sm = GlobalPreferences.loadPropertiesFile(this
			.getClass().getPackage().getName(), null);

	private List<OfficeDocument> documents;
	private List<OfficeDocument> docs = new ArrayList<OfficeDocument>();

	protected void setup() {
		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(new MultiDispatch()).appendFinally(
						CloseHibernateSession.INSTANCE);
	}

	private class MultiDispatch implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			// w zaleznosci z ktorej listy wywolano akcje system odsyla do niej
			if (activityIds == null || activityIds.length == 0) {
				return;
			}

			try{
				DSApi.context().begin();
				
				String guid = GlobalPreferences.getDispatchDivisionGuid();
				if(guid == null){
					throw new EdmException("Nie wybrano dzia�u wysy�ek");
				}
				String username = DSApi.context().getPrincipalName();

				// dekretacja tylko dla czystopisow
				List <String> _activityIds = new ArrayList <String>();
				for (int i = 0; i < documentIds.length; i++) {
					if(OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(OutOfficeDocument.findOutOfficeDocument(documentIds[i]).getPrepState())) {
						_activityIds.add(activityIds[i]);
					}
				}
				
				for(String activityId: _activityIds){
					WorkflowFactory.simpleDivisionAssignment(
							activityId, 
							guid, 
							username, 
							null, 
							WorkflowFactory.SCOPE_DIVISION, 
							sm.getString("doRealizacji"), 
							event);
				}
				
				int activitySize = _activityIds.size();
				if ( activitySize != documentIds.length)
					message = "CoNajmniejJednoZPismJestBrudnopisem";
				
				if (activitySize > 0)
					message = "PismaPrzekazanoDoDzialuWysylki";
				
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				//addActionError(e.getMessage());
				event.setResult("error");
				setError(e.getMessage());
			}
		}
	}
	
	

	public List<OfficeDocument> getDocs() {
		return this.docs;
	}

	public void setActivityIds(String[] activityIds) {
		this.activityIds = activityIds;
	}

	public String[] getActivityIds() {
		return activityIds;
	}

	public void setDocumentIds(Long[] documentIds) {
		this.documentIds = documentIds;
	}

	public Long[] getDocumentIds() {
		return documentIds;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
	
	public String getReturnLink(){
		return "/office/tasklist/"+
		((getUsername()!=null)?"any-user-task-list.action?username="+getUsername()+"&tab=out":
			"current-user-task-list.action?tab=out")+
		((getError()==null)?"":"&error="+getError())+(message == null ? "" : "&message=".concat(message));
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}