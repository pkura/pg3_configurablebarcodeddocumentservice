package pl.compan.docusafe.web.office.tasklist;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.mapping.Column;
import org.hibernate.type.Type;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.names.DocumentURN;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.names.WfActivityURN;
import pl.compan.docusafe.core.names.WfProcessURN;
import pl.compan.docusafe.core.names.WorkflowActivityURN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.ActivityId;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.internal.WfProcessImpl;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmWorkflowService;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.service.tasklist.Task.FlagBean;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.common.Links;
import pl.compan.docusafe.web.common.SkipNullify;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.ReopenHibernateSession;
import std.fun;
import std.lambda;
import std.pair;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Akcja wy�wietlaj�ca list� zada�.  Zadania workflow s� pobierane
 * z us�ugi TaskList, natomiast sprawy odczytywane przez OfficeCase.find.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskListAjaxAction.java,v 1.11 2010/08/17 10:56:55 mariuszk Exp $
 */
@SkipNullify(attributes = {"urns"})
public class TaskListAjaxAction extends EventActionSupport
{
    public StringManager sm = 
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    private StringManager smTab;
    private static final Logger log = LoggerFactory.getLogger(TaskListAjaxAction.class);
    

    public static final String TAB_IN = InOfficeDocument.TYPE;
    public static final String TAB_OUT = OutOfficeDocument.TYPE;
    public static final String TAB_INT = OutOfficeDocument.INTERNAL_TYPE;
    public static final String TAB_ORDER = OfficeOrder.TYPE;
    public static final String TAB_CASES = "cases";
    public static final String TAB_WATCHES = "watches";

    public static final String TAB_MY_ORDERS = "my_order";
    public static final String TAB_REALIZE_ORDERS = "realize_order";
    public static final String TAB_WATCH_ORDERS = "watch_order";
    
    static private boolean updatedone = false;

    private String tab;
    private String orderTab;
    private String username;
    private String sortField;
    private Boolean ascending;
    private List<Task> tasks;
    private Collection<CaseTask> cases;
    private Tabs tabs;
    private Tabs orderTabs;
    private List users;
    private List<TableColumn> columns;
    private Integer searchOfficeNumber;
    private String searchOfficeId;
    private String[] activityIds;
    private Long[] actionCaseIds;
    private Long[] documentIds;
    private Long caseId;
    private String caseName;
    //private String caseInOfficeFinishDate;
    private List<Task> watches;
    private String[] urns;
    private boolean addToTask = false;
    private boolean multiDepartmentEnroll ;
    private Integer taskCount;
    private Boolean withBackup;
    private String nameOfSmallAssignment;
    private String nameOfJournal;
    private List<SmallAssignmentBean> smallAssignments;
    private List<DepartmentEnrollBean> incomingDepartments;
    private List<DocumentKind> dockindNames;//do filtrowania
    private String filterDockindNames;//do filtrowania
    /** b��d kt�ry wyst�pi� przed przej�ciem na t� stron� */
    private String error;
    private Date caseDeadlineDate;

    // okre�la, czy przegl�dane s� zadania zalogowanego u�ytkownika
    private boolean viewingLoggedUser;

    private boolean actionManualFinish;
    private boolean actionAssignOfficeNumber;
    private boolean actionAttachmentsAsPdf;
    private boolean actionAddToCase;
    private boolean actionMultiAssignment;
    private boolean actionMultiBoxing;
    private boolean actionAssignMe;
    private boolean actionReplyUnnecessary;
    private boolean actionSendToEva;
    private boolean actionDepartmentEnroll;
    
    private boolean onTaskList;
    
    private Pager pager;
    private Integer offset;
    private static int LIMIT = 20;
    
    private String filterBy;
    private String filterName;
    private List<FilterColumn> filterColumns;
    private List<Label> availableLabels;
    private List<Label> availableForAddLabels;
    private Long labelId;
    private Long viewedLabelId;
    private Long removeLabelId;
    private Collection<Label> viewedLabels;
    private List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> enableAcceptances;
    private String acceptanceCn;
    
    //labels the new way
    private List<Label> userFlags_label;
   	private List<Label> systemFlags_label;
    private List<Label> labels_label;
    private List<Label> systemLabels_label;
    
    private boolean showLabels;
    
    protected boolean showLabels() {
    	
    	try {
    		log.trace("uzytkownik {}",getDSUser().getName());
    		if (getDSUser() == null) {
    			return false;
        	} else {
        		return true;
        	}
    	} catch (Exception e) {
    		return false;
    	}
    	
    }
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRemoveFilters").
	        append(OpenHibernateSession.INSTANCE).
	        append(new RemoveFilter()).
	        append(new PrepareTabs()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
        
        registerListener("doSwitchWorkflow").
        	append(OpenHibernateSession.INSTANCE).
        	append(new SwitchWorkflow()).
        	append(new PrepareTabs()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doManualFinish").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new ManualFinish()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
            
        registerListener("doSendToEva").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new SendToEva()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignOfficeNumber").
            append(new PrepareTabs()).
            append(new AssignOfficeNumber()).
            append(ReopenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddToCase").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new AddToCase()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRemoveWatches").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new RemoveWatches()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRemoveOrders").
        	append(OpenHibernateSession.INSTANCE).
        	append(new PrepareTabs()).
        	append(new RemoveOrders()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAssignMe").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new AssignMe()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
       registerListener("doSmallAssignment").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new SmallAssignment()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
       
       registerListener("doDepartmentEnroll").
           append(OpenHibernateSession.INSTANCE).
           append(new PrepareTabs()).
           append(new MultiDepartmentEnroll()).
           append(new FillForm()).
           appendFinally(CloseHibernateSession.INSTANCE);
       
       registerListener("doReplyUnnecessary").
           append(OpenHibernateSession.INSTANCE).
           append(new PrepareTabs()).
           append(new ReplyUnnecessary()).
           append(new FillForm()).
           appendFinally(CloseHibernateSession.INSTANCE);
       
       registerListener("doAcceptance").
	       append(OpenHibernateSession.INSTANCE).
	       append(new PrepareTabs()).
	       append(new Acceptance()).
	       append(new FillForm()).
	       appendFinally(CloseHibernateSession.INSTANCE);
       registerListener("doAddLabel").
	       append(OpenHibernateSession.INSTANCE).
	       append(new PrepareTabs()).
	       append(new AddLabel()).
	       append(new FillForm()). 
	       appendFinally(CloseHibernateSession.INSTANCE);
       
       registerListener("doRemoveLabelFromView").
	       append(OpenHibernateSession.INSTANCE).
	       append(new PrepareTabs()).
	       append(new RemoveLabelFromView()).
	       append(new FillForm()).
	       appendFinally(CloseHibernateSession.INSTANCE);
       
       registerListener("doManualPushToCoor").
	       append(OpenHibernateSession.INSTANCE).
	       append(new PrepareTabs()).
	       append(new ManualPushToCoor()).
	       append(new FillForm()).
	       appendFinally(CloseHibernateSession.INSTANCE);
	   
       
    }

    protected DSUser getDSUser() throws EdmException
    {
        return DSApi.context().getDSUser();
    }

    protected List getDSUserList() throws EdmException
    {
        return null;
    }

    public boolean isOwnTasks() 
    {
        return true;
    }

    public String getBaseLink()
    {
        return "/office/tasklist/current-user-task-list.action";
    }
    

    public String getLink(Task task) {
    	return getLinkX(task) + "&ownTask=true";
    }
    /**
     * U�ytkownik, kt�rego lista zada� ma by� wy?wietlona;
     * null, je�eli nie wybrano u�ytkownika.
     *//*
    protected abstract DSUser getDSUser() throws EdmException;

    *//**
     * Zwraca list� u�ytkownik�w do wy?wietlenia na li?cie wyboru.
     * Je�eli lista ma nie by� wy?wietlana, zwraca null.
     *//*
    protected abstract List getDSUserList() throws EdmException;

    public abstract String getBaseLink();

    *//**
     * @return True, je�eli pokazywana jest lista zalogowanego u�ytkownika.
     *//*
    public abstract boolean isOwnTasks();*/

    private static final String[] DEFAULT_TASK_COLUMNS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "description",
            "objective", "documentCtime"//, "deadlineTime"
        };

    private static final String[] SIMPLE_TASK_COLUMNS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "documentSummary", "documentCtime"
        };
    
    private static final String[] DEFAULT_CASE_COLUMNS = new String[]
        {
            "caseOfficeId", "caseStatus", "caseOpenDate", "caseFinishDate",
            "caseDaysToFinish", "documentCtime"
        };

    private static final String[] DEFAULT_WATCHLIST_COLUMNS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "description",
            "objective", "deadlineTime", "documentCtime"
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� przychodz?cych.
     */
    private static final String[] COLUMN_PROPERTIES_IN = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","incomingDate" ,"sender","flags","userFlags","documentSender",
            "documentOfficeNumber","answerCounter","documentSummary","process","clerk","description",
            "objective","documentReferenceId","deadlineTime","officeCase", //"caseInOfficeFinishDate", 
            "dockindName", "lastRemark","viewerLink",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","documentId",
            "caseDeadlineDate", "documentCtime"
        };

    private static final String[] COLUMN_PROPERTIES_WATCHLIST = new String[]
        {
            "documentOfficeNumber", "receiveDate", "sender", "description","objective",
            "process", "documentSummary", "documentReferenceId","documentSender",
            "flags", "userFlags","deadlineTime","documentId", "documentCtime"
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wychodz?cych.
     */
    private static final String[] COLUMN_PROPERTIES_OUT = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","sender","flags","userFlags","documentOfficeNumber",
            "documentRecipient","documentSummary","process","description","documentDelivery",
            "objective","documentPrepState","documentReferenceId","deadlineTime","officeCase","clerk","lastRemark",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","documentId", "documentCtime"
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wewn�trznych.
     */
    private static final String[] COLUMN_PROPERTIES_INT = new String[]
        {
        "numberRows","shortReceiveDate","receiveDate","sender","flags","userFlags",
            "documentOfficeNumber", "documentSummary","description","objective","deadlineTime","clerk","lastRemark","officeCase"
            ,"documentReferenceId",
            "dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate","answerCounter","documentId", "documentCtime"
                
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� wydanych polece�.
     */
    private static final String[] COLUMN_PROPERTIES_MY_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "receiver","documentId", "documentCtime"
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� realizowanych polece�.
     */
    private static final String[] COLUMN_PROPERTIES_REALIZE_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "author", "sender", "documentCtime"
        };

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie zada� obserwowanych polece�.
     */
    private static final String[] COLUMN_PROPERTIES_WATCH_ORDERS = new String[]
        {
            "documentOfficeNumber", "receiveDate", "documentSummary", "orderStatus", "author", "sender", "receiver", "documentCtime"
        };
    
    private static final String[] AVAILABLE_FILTER_COLUMNS = new String[]
            {
    	"documentId","documentOfficeNumber","caseOfficeId","caseOpenDate","caseStatus","clerk","deadlineTime","description","dockindName","documentDelivery",
    	"documentPrepState","documentRecipient","documentReferenceId","documentSummary","incomingDate","objective","officeCase", //"caseInOfficeFinishDate",
    	"orderNumber","orderStatus","process","receiveDate","receiver","sender",
    	"dockindStatus","dockindNumerDokumentu","dockindKwota","dockindKategoria","documentDate",
    	"dockindBusinessAtr1","dockindBusinessAtr2","dockindBusinessAtr3","dockindBusinessAtr4", "documentCtime"
            };

    private static final String[] DEFAULT_MY_ORDERS_COLUMNS = COLUMN_PROPERTIES_MY_ORDERS;

    private static final String[] DEFAULT_REALIZE_ORDERS_COLUMNS = COLUMN_PROPERTIES_REALIZE_ORDERS;

    private static final String[] DEFAULT_WATCH_ORDERS_COLUMNS = COLUMN_PROPERTIES_WATCH_ORDERS;

    /**
     * Wszystkie kolumny, jakie mog? wyst?pi� na li?cie spraw.
     */
    private static final String[] COLUMN_PROPERTIES_CASES = new String[]
        {
            "caseOfficeId", "caseStatus", "caseOpenDate", "caseFinishDate",
            "caseDaysToFinish"
        };
    private static Map<String,DocKindProperties> docKindProperties;
    
    static
    {
    	try
    	{
    		DSApi.openAdmin();
    		initDockind();
	    } 
		catch (EdmException e)
		{
			log.error("", e);
		}
		finally
		{
			try {
				DSApi.close();
			} catch (EdmException e) {
				log.error("", e);
			}
		}
    }
    
    public static void initDockind() throws EdmException
    {
    		
			List<DocumentKind> l = DocumentKind.list(true);			
			docKindProperties = new TreeMap<String, DocKindProperties>();
			for (DocumentKind documentKind : l)
			{
				documentKind.initialize();
				docKindProperties.put(documentKind.getName(), new DocKindProperties(documentKind));
			}	
    }
    
    private static  class DocKindProperties
    {
    	private String openIn;
    	private String cn;
    	private String name;
		public DocKindProperties(String openIn, String cn, String name)
		{
			this.openIn = openIn;
			this.cn = cn;
			this.name = name;
		}
		
		public DocKindProperties(DocumentKind dockind)
		{
			this.openIn = dockind.getProperties().get(DocumentKind.OPEN_TAB_IN_OFFICE);;
			this.cn = dockind.getCn();
			this.name = dockind.getName();
		}
		
		public String getOpenIn() {
			return openIn;
		}
		public void setOpenIn(String openIn) {
			this.openIn = openIn;
		}
		public String getCn() {
			return cn;
		}
		public void setCn(String cn) {
			this.cn = cn;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}	   	
    }
    public static String[] getColumnProperties(String tab, String orderTab)
    {
        if (TAB_OUT.equals(tab))
            return COLUMN_PROPERTIES_OUT;
        else if (TAB_IN.equals(tab))
        {
            return COLUMN_PROPERTIES_IN;
        }
        else if (TAB_INT.equals(tab))
            return COLUMN_PROPERTIES_INT;
        else if (TAB_ORDER.equals(tab))
        {
            if (TAB_MY_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_MY_ORDERS;
            else if (TAB_REALIZE_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_REALIZE_ORDERS;
            else if (TAB_WATCH_ORDERS.equals(orderTab))
                return COLUMN_PROPERTIES_WATCH_ORDERS;
        }
        else if (TAB_CASES.equals(tab))
            return COLUMN_PROPERTIES_CASES;
        else if (TAB_WATCHES.equals(tab))
            return COLUMN_PROPERTIES_WATCHLIST;
        return
            new String[0];
    }

    public static String[] getDefaultColumnProperties(String tab, String orderTab)
    {
        if (TAB_IN.equals(tab) || TAB_OUT.equals(tab) || TAB_INT.equals(tab))
            return DEFAULT_TASK_COLUMNS;
        else if (TAB_ORDER.equals(tab))
        {
            if (TAB_MY_ORDERS.equals(orderTab))
                return DEFAULT_MY_ORDERS_COLUMNS;
            else if (TAB_REALIZE_ORDERS.equals(orderTab))
                return DEFAULT_REALIZE_ORDERS_COLUMNS;
            else if (TAB_WATCH_ORDERS.equals(orderTab))
                return DEFAULT_WATCH_ORDERS_COLUMNS;
            else
                return new String[0];
        }
        else if (TAB_WATCHES.equals(tab))
            return DEFAULT_WATCHLIST_COLUMNS;
        else if("filter".equals(tab))
        {
        	return AVAILABLE_FILTER_COLUMNS;
        }
        else
            return DEFAULT_CASE_COLUMNS;
    }

    public static String getColumnDescription(String property)
    {
    	if(property.startsWith("dockindBusinessAt"))
    	{
    		return Docusafe.getAdditionProperty("column."+property);
    	}
        StringManager smL = GlobalPreferences.loadPropertiesFile(TaskListAjaxAction.class.getPackage().getName(),null);
        return smL.getString("column."+property);
    }


    private String transformOrderProperty(String property)
    {
        if (TAB_ORDER.equals(tab) && "documentOfficeNumber".equals(property))
            return "orderNumber";
        else
            return property;
    }
    
    /**
     * Odczytuje z ustawie� u�ytkownika list� kolum dla listy zada�
     * okre?lonej parametrem tab.  W wypadku braku ustawienia u�ywa
     * domy?lnej listy kolumn
     */
    private List<TableColumn> getActualColumns()
    {
        String[] properties;
       // if (simpleTaskList && TAB_IN.equals(tab))
       //     properties = SIMPLE_TASK_COLUMNS;
      //  else

            properties = DSApi.context().userPreferences().node("task-list").get("columns_"+tab,
            StringUtils.join(getDefaultColumnProperties(tab, orderTab), ",")).split(",");

        /*if(filterName == null ||  (filterName != null && filterName.equalsIgnoreCase("null")))
        	filterName = "";*/
            
        List<TableColumn> actualColumns = new ArrayList<TableColumn>(properties.length);
        for (int i=0; i < properties.length; i++)
        {
            actualColumns.add(new TableColumn(properties[i],
                getColumnDescription(transformOrderProperty(properties[i])),
                getBaseLink() + "?username="+username +
                    "&tab="+tab+
                    "&sortField="+("shortReceiveDate".equals(properties[i]) ? "receiveDate" : properties[i]) +
                    "&ascending=false&withBackup="+withBackup+
                    "&filterBy="+filterBy+
                    "&filterName="+filterName,
                    
                getBaseLink() + "?username="+username +
                    "&tab="+tab+
                    "&sortField="+("shortReceiveDate".equals(properties[i]) ? "receiveDate" : properties[i]) +
                    "&ascending=true" +
                    "&withBackup="+withBackup+
                    "&filterBy="+filterBy+
                    "&filterName="+filterName

                    ,""));
        }

        return actualColumns;
    }
    
    private List<FilterColumn> getFilterColumn()
    {
    	List<FilterColumn> result = new ArrayList<FilterColumn>();
    	String [] userProperties = DSApi.context().userPreferences().node("task-list").get("columns_"+tab,
                StringUtils.join(getDefaultColumnProperties(tab, orderTab), ",")).split(",");
    	String[] availableProperties = StringUtils.join(getDefaultColumnProperties("filter", null), ",").split(",");
    	Map<String,String> availablePropertiesMap = new TreeMap<String, String>();
    	for (int i = 0; i < availableProperties.length; i++) 
    	{
    		availablePropertiesMap.put(availableProperties[i],availableProperties[i]);
		}
    	result.add(new FilterColumn("Brak",sm.getString("Brak")));
    	result.add(new FilterColumn("documentOfficeNumber",getColumnDescription("documentOfficeNumber")));
    	for (int i = 0; i < userProperties.length; i++) 
    	{
    		if(availablePropertiesMap.containsKey(userProperties[i]) && !"documentOfficeNumber".equals(userProperties[i]))
    			result.add(new FilterColumn(userProperties[i],getColumnDescription(userProperties[i])));
		}
		return result;
    }
    
    
    /**
     * Zwraca odno?nik do zadania.  Je�eli przegl?dana jest lista zalogowanego
     * u�ytkownika, odno?niki zawieraj? identyfikator zadania workflow, za?
     * nieotwarte zadania po klikni�ciu zostan? zaakceptowane.  Je�eli przegl?dana
     * jest lista innego u�ytkownika, odno?nik prowadzi do formularza edycji
     * dokumentu bez identyfikatora zadania workflow.
     * <p>
     * Obecnie obs�ugiwane s? tylko zadania typu documentTask.
     */
    public String getLinkX(Task task)
    {
    	try
    	{
	        if (task.isDocumentTask())
	        {
	            if (task.getLink() != null)
	            {
	                return task.getLink();
	            }
	            else
	            {
	                if (!(TAB_MY_ORDERS.equals(orderTab)) && !(TAB_WATCH_ORDERS.equals(orderTab)))
	                {
	                    String activityId = WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey());
	
	                    if (viewingLoggedUser && !task.isAccepted())
	                    {
	                        if (task.isDocumentCrm())
	                        {
	                            return "/office/accept-wf-assignment.action?doAccept=true&activity="+activityId+"&target=archive";
	                        }
	                        else if(task.getDockindName() != null && docKindProperties.get(task.getDockindName()) != null 
	                        		&& "document-archive".equals(docKindProperties.get(task.getDockindName()).getOpenIn()))
	                        {
	                        	return "/office/accept-wf-assignment.action?doAccept=true&activity="+activityId+"&target=archive";
	                        }
	                        return "/office/accept-wf-assignment.action?doAccept=true&activity="+activityId;
	                    }
	
	                    String link = null;
	                    String tmp = null;
	                    if (JbpmWorkflowService.NAME.equals(task.getWorkflowName()) && viewingLoggedUser)
	                    {
	                        tmp = "external-workflow.action?doShowTask=true&";
	                    }
	                    else
	                    {
	                        if (task.isDocumentCrm() ||(task.getDockindName() != null && 
	                        		docKindProperties.get(task.getDockindName()) != null &&
	                        		"document-archive".equals(docKindProperties.get(task.getDockindName()).getOpenIn())) )
	                        	 tmp = "document-archive.action?";
	                        else
	                        	tmp = "summary.action?";
	                    }
	
	                    if (TAB_IN.equals(tab))
	                    {
	                        link = "/office/incoming/"+tmp+"documentId="+task.getDocumentId();
	                        if (viewingLoggedUser)
	                            link += "&activity="+activityId;
	                    }
	                    else if (TAB_OUT.equals(tab))
	                    {
	                        link = "/office/outgoing/"+tmp+"documentId="+task.getDocumentId();
	                        if (viewingLoggedUser)
	                            link += "&activity="+activityId;
	                    }
	                    else if (TAB_INT.equals(tab))
	                    {
	                        link = "/office/internal/"+tmp+"documentId="+task.getDocumentId();
	                        if (viewingLoggedUser)
	                            link += "&activity="+activityId;
	                    }
	                    else if (TAB_ORDER.equals(tab))
	                    {
	                        link = "/office/order/"+tmp+"documentId="+task.getDocumentId();
	                        if (viewingLoggedUser)
	                            link += "&activity="+activityId;
	                    }
	                    return link;
	                }
	                else
	                // polecenia, ale z zakladki MY_ORDERS lub WATCH_ORDERS wiec zwykly link do summary.action
	                {
	                    return "/office/order/summary.action?documentId="+task.getDocumentId();
	                }
	            }
	        }
	        else
	        {
	        	return "/office/tasklist/current-user-task-list.action?BLADGENERACJILINKUNIETASK";
	        }
    	}
    	catch (Exception e) {
			log.error("",e);
		}
    	return "/office/tasklist/current-user-task-list.action?BLADGENERACJILINKU";
    }

    /**
     * okre�la czy zadanie ma by� oznaczone na czerwono (jako przypomnienie)
     */
    public boolean needsReminder(Task task)
    {
        if (task instanceof TaskSnapshot)
        {
            TaskSnapshot t = (TaskSnapshot) task;
            if (t.getProcessStatus() != null && ((t.getProcessStatus() & TaskSnapshot.STATUS_ZAWIESZONY_PRZYPOMNIENIE) > 0))
            //if (Constants.STATUS_ZAWIESZONY_PRZYPOMNIENIE.equals(t.getProcessStatus()))
                return true;
            if (t.getReminderDate() != null)
            {
                Date act = DateUtils.getCurrentTime();//new Date();
                if (t.getReminderDate().before(act))
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    
 
    public String statusSymbols(Task task)
    {
        if (task instanceof TaskSnapshot)
        {
            TaskSnapshot t = (TaskSnapshot) task;
            if (t.getProcessStatus() == null)
                return null;
            
            String symbols = "";
            if ((t.getProcessStatus() & TaskSnapshot.STATUS_ZAWIESZONY) > 0)
                symbols += "z";
            if ((t.getProcessStatus() & TaskSnapshot.STATUS_OTWARTE_W_OPS) > 0)
                symbols += "p";
            if ((t.getProcessStatus() & TaskSnapshot.STATUS_WZNOWIENIE_ZADANIA) > 0)
                symbols += "w";  
            if ((t.getProcessStatus() & TaskSnapshot.STATUS_NOWY_DOKUMENT_Z_NR_POLISY) > 0)
                symbols += "n";        
            return symbols;
        }
        else
            return null;
        
    }
    
    /**
     * Zwraca odno?nik do sprawy do ktorej przypisane jest zadanie.
     */
    public String getCaseDeadlineLink(Task task)    
    {
    	CaseTask caseTask = null;
    	try 
    	{
    		OfficeCase officeCase = OfficeCase.find(task.getCaseId());
    		caseTask = new CaseTask(officeCase);			
		} 
    	catch (EdmException e)
    	{
			log.error("Blad", e);
		}
    	if(caseTask==null)
    	{
    		return "";
    	}
    	else
    	{
    		return "/office/edit-case.do?id="+caseTask.getCaseFinishDate();
    	}
    }
        
    public String getCaseLink(Task task)
    {
    	return "/office/edit-case.do?id="+task.getCaseId();
    	//return "/office/edit-case.do?id="+task.getc
    }
    public String getAnswerLink(Task task)
    {
    	String activityId = WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey());
    	if (TAB_INT.equals(tab))
        {
			return "/office/internal/replies.action?documentId="
					+ task.getDocumentId() + "&activity=" + activityId;
		} else {
			return "/office/incoming/replies.action?documentId="
					+ task.getDocumentId() + "&activity=" + activityId;
		}
    }

    /**
     * Zwraca odno?nik do zadania zwi?zanego ze spraw?.
     */
    public String getLink(CaseTask caseTask)
    {
        return "/office/edit-case.do?id="+caseTask.getCaseId()+"&tabId=summary";
    }

    public String prettyPrint(Object object, String property)
    {
        if (object == null) return "";
        
        
        if (object instanceof Date)
        {
            if (property.startsWith("case") || property.equals("shortReceiveDate") || property.equals("incomingDate"))
                return DateUtils.formatJsDate((Date) object);
            else
                return DateUtils.formatJsDateTime((Date) object);
        }
        else
        {
            return object.toString();
        }
    }

    private class PrepareTabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
            smTab = GlobalPreferences.loadPropertiesFile("","tab");
            // domy�lna zak�adka - TAB_IN
            if (!TAB_IN.equals(tab) && !TAB_OUT.equals(tab) &&
                !TAB_INT.equals(tab) && !TAB_CASES.equals(tab) &&
                !TAB_ORDER.equals(tab) &&
                !TAB_WATCHES.equals(tab))
                tab = TAB_IN;
            
            tabs = new Tabs(4);
            
            String link;
            if (username != null)
            {
                link = getBaseLink() +
                    "?username="+username+
                    (sortField != null ? "&sortField="+sortField : "") +
                    "&ascending="+ascending;
            }
            else
            {
                link = getBaseLink() + "?a=a"; // tylko po to, by u�y� '&tab' ni�ej
            }

            tabs.add(new Tab(smTab.getString("PismaPrzychodzace"), smTab.getString("ListaPismPrzychodzacych"),
                link+"&tab="+TAB_IN, TAB_IN.equals(tab)));
            
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
            {
            	tabs.add(new Tab(smTab.getString("PismaWewnetrzne"), smTab.getString("ListaPismWewnetrznych"),
            			link+"&tab="+TAB_INT, TAB_INT.equals(tab)));
            }
			if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
			{
	            tabs.add(new Tab(smTab.getString("PismaWychodzace"), smTab.getString("ListaPismWychodzacych"),
	                link+"&tab="+TAB_OUT, TAB_OUT.equals(tab)));
		    }
            if (Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
            {
                tabs.add(new Tab(smTab.getString("Polecenia"), smTab.getString("ListaPolecen"),
                    link+"&tab="+TAB_ORDER, TAB_ORDER.equals(tab)));
            }

            if (AvailabilityManager.isAvailable("sprawy"))//!Configuration.hasExtra("business"))
            {
                tabs.add(new Tab(smTab.getString("Sprawy"), smTab.getString("ListaSpraw"),
                    link+"&tab="+TAB_CASES, TAB_CASES.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.repository.obserwowane"))
			{
	            tabs.add(new Tab(smTab.getString("Obserwowane"), smTab.getString("ListaZadanObserwowanych"),
	                link+"&tab="+TAB_WATCHES, TAB_WATCHES.equals(tab)));
			}
            if (Configuration.additionAvailable(Configuration.ADDITION_CALENDAR))
            {
            	try
                {
            		tabs.add(new Tab(smTab.getString("Kalendarz"), smTab.getString("ZarzadzanieTerminami"),
            			"/office/tasklist/calendar" + (isOwnTasks()?"-own":"-any") + ".action?daily=true" +
            			(isOwnTasks()?"":("&username=" + DSApi.context().getDSUser().getName())), false));
            	}
                catch (Exception e)
                {
                    addActionError(e.getMessage());
            	}
            }

            if (TAB_ORDER.equals(tab))
            {
                if (!TAB_MY_ORDERS.equals(orderTab)
                && !TAB_REALIZE_ORDERS.equals(orderTab) && !TAB_WATCH_ORDERS.equals(orderTab))
                    orderTab = TAB_REALIZE_ORDERS;

                orderTabs = new Tabs(3);

                orderTabs.add(new Tab(smTab.getString("WydanePolecenia"), smTab.getString("WydanePolecenia"),
                    link+"&tab="+TAB_ORDER+"&orderTab="+TAB_MY_ORDERS, TAB_MY_ORDERS.equals(orderTab)));
                orderTabs.add(new Tab(smTab.getString("RealizowanePolecenia"), smTab.getString("RealizowanePolecenia"),
                    link+"&tab="+TAB_ORDER+"&orderTab="+TAB_REALIZE_ORDERS, TAB_REALIZE_ORDERS.equals(orderTab)));
                orderTabs.add(new Tab(smTab.getString("PrzekazanePolecenia"), smTab.getString("PrzekazanePolecenia"),
                    link+"&tab="+TAB_ORDER+"&orderTab="+TAB_WATCH_ORDERS, TAB_WATCH_ORDERS.equals(orderTab)));
            }
        }
    }
    
    private class SmallAssignmentBean
    {
                
        public SmallAssignmentBean (DSUser user,DSDivision guid) throws EdmException
        {
            this.name = (user != null ? user.getName() : "")+";"+(guid != null ? guid.getGuid() : "");
            if(user != null)
            	this.nameToDisplay = user.getLastname()+" "+user.getFirstname().charAt(0)+".";
            else
            	this.nameToDisplay = guid.getName();
        }
                
        private String name;//to jest name po ktorym bede szukac uzytkownikow. Np. pkomisarski
        private String nameToDisplay;// to jest name do wyswietlenia. Np. P. Komisarski
        
        public String getName()
        {
            return name;
        }
        
        public String getNameToDisplay()
        {
            return nameToDisplay;
        }
     }
    
    private class DepartmentEnrollBean
    {
        public DepartmentEnrollBean (Journal journal) throws EdmException
        {
            this.name = journal.getId();
            this.nameToDisplay = journal.getSummary();
            
        }
        
        private long name;
        private String nameToDisplay;
        
        
        public long getName()
        {
            return name;
        }
        
        public String getNameToDisplay()
        {
            return nameToDisplay;
        }
    }
    
    private class ReplyUnnecessary implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (documentIds == null || documentIds.length == 0)
                addActionError(sm.getString("NieWybranoDokumentow"));
            try
            {
                DSApi.context().begin();
                for (int i=0; i < documentIds.length; i++)
                {
                    Long id = documentIds[i];
                    if (id == null)
                        continue;
                    InOfficeDocument document = InOfficeDocument.findInOfficeDocument(id);
                    document.setReplyUnnecessary(true);
                    TaskSnapshot.updateByDocumentId(document.getId(),document.getStringType());
                    event.addActionMessage(sm.getString("OznaczonoPismoJakoNiewymagajaceOdpowiedzi",document.getOfficeNumber()));
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class RemoveFilter implements ActionListener {
    	public void actionPerformed(ActionEvent event)
        {
    		filterBy = null;
    		filterName = "";
    		ServletActionContext.getRequest().getSession().setAttribute(tab+"filterBy", filterBy);
         	ServletActionContext.getRequest().getSession().setAttribute(tab+"filterName", filterName);
        }
    }
    private class SwitchWorkflow implements ActionListener {
    	public void actionPerformed(ActionEvent event)
        {
    		WorkflowFactory.switchWorkflow();
        }
    }
    
    private class Acceptance implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			log.trace("TaskListAction.Acceptance");
			if (documentIds == null || documentIds.length == 0 || activityIds == null || activityIds.length == 0) {
				addActionError(sm.getString("NieWybranoDokumentow"));
				return;
			}
			if (acceptanceCn == null || acceptanceCn.length() < 1) {
				addActionError("Nie wybrano akceptacji");
				return;
			}
			try {
				for (int i = 0; i < documentIds.length; i++) {
					DSApi.context().begin();
					AcceptanceCondition nextAcceptance = null;
					DSUser user = null;
          	       	String division = null;
          	       	OfficeDocument document = (OfficeDocument)Document.find(documentIds[i]);          	       	          	       	
          	       	DocumentKind kind = document.getDocumentKind();
          	       	Map<String, Object> valuesTmp = new TreeMap<String, Object>();
          	       	FieldsManager fm;
          	       	kind.initialize();          	  
          	       	fm = kind.getFieldsManager(documentIds[i]);
          	       	fm.initialize();
          	       	fm.initializeAcceptances();
          	       	
          	       	if (kind.logic().accept(document, null, acceptanceCn)) {
						// akceptacja JBPM
						log.trace("document accepted");

						if (document.getDocumentKind().logic()
								.getAcceptanceManager() != null) {

							log.trace("!JBPM acceptance!");

							DSApi.context().session().flush();

							document.getDocumentKind().logic()
									.getAcceptanceManager().refresh(document,fm,
											activityIds[i]);
						}
					} else {
						// stara akceptacja

						Boolean simpleAcceptance = (IntercarsAcceptanceMode
								.from(fm) == IntercarsAcceptanceMode.SIMPLE2);

						if (fm.getAcceptancesDefinition().getAcceptances().get(
								acceptanceCn) == null) {
							addActionError("Niepoprawny kod akcepatcji : "
									+ acceptanceCn);
							continue;
						}

						if (fm.getAcceptancesDefinition().getAcceptances().get(
								acceptanceCn).getFieldCn() != null) {
							List<CentrumKosztowDlaFaktury> ckfList = CentrumKosztowDlaFaktury
									.findByDocumentId(documentIds[i]);
							if (ckfList != null && ckfList.size() > 0) {
								for (CentrumKosztowDlaFaktury centrumKosztowDlaFaktury : ckfList) {
									if (fm.getAcceptancesState()
											.getFieldAcceptance(
													acceptanceCn,
													centrumKosztowDlaFaktury
															.getId()) == null) {
										Map<Integer, String> centrumAcceptanceCn = new TreeMap<Integer, String>();
										centrumAcceptanceCn.put(new Integer(
												centrumKosztowDlaFaktury
														.getId().toString()),
												acceptanceCn);
										try 
										{
											AcceptancesManager.giveCentrumAcceptance(
															centrumAcceptanceCn,
															documentIds[i]);
											addActionMessage(sm
													.getString("WykonanoAkceptacjeDokumentu")
													+ " KO "
													+ document
															.getOfficeNumber()
													+ " : "
													+ fm
															.getAcceptancesDefinition()
															.getAcceptances()
															.get(acceptanceCn)
															.getName());

										} catch (DSException e) {
											addActionError("Nie mo�esz wykona� akceptacji "
													+ fm
															.getAcceptancesDefinition()
															.getAcceptances()
															.get(acceptanceCn)
															.getName()
													+ " dla dokumentu KO : "
													+ document.getOfficeNumber());
											continue;
										}

										if (acceptanceCn.equals("zwykla")) {
											valuesTmp.put("AKCEPTACJA_ZWYKLA",
													1);
										}
										valuesTmp.put(
												InvoiceLogic.STATUS_FIELD_CN,
												fm.getAcceptancesDefinition()
														.getAcceptances().get(
																acceptanceCn)
														.getLevel());
									}
								}
							} else {
								addActionError("Brak centrum dla dokumentu "
										+ documentIds[i]);
								continue;
							}
						} else {
							if (fm.getAcceptancesState()
									.getGeneralAcceptancesAsMap().get(acceptanceCn) == null) {
								try 
								{
									AcceptancesManager.giveAcceptance(
											acceptanceCn, documentIds[i]);
									addActionMessage(sm
											.getString("WykonanoAkceptacjeDokumentu")
											+ " KO "
											+ document.getOfficeNumber()
											+ " : "
											+ fm.getAcceptancesDefinition()
													.getAcceptances().get(acceptanceCn)
													.getName());
								} catch (DSException e) {
									addActionError("Nie mo�esz wykona� akceptacji "
											+ fm.getAcceptancesDefinition()
													.getAcceptances().get(acceptanceCn)
													.getName()
											+ " dla dokumentu KO : "
											+ document.getOfficeNumber());
									continue;
								}
								valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm
										.getAcceptancesDefinition()
										.getAcceptances().get(acceptanceCn)
										.getLevel());
							}
						}
						DSApi.context().session().flush();

						fm.initializeAcceptances();
						nextAcceptance = fm.getAcceptancesState()
								.getThisAcceptation(null, simpleAcceptance);

						if (nextAcceptance != null) {
							if (nextAcceptance.getUsername() != null) {
								try {
									user = DSUser.findByUsername(nextAcceptance
											.getUsername());
								} catch (Exception e) {
									addActionError("Nie znalezionu uzytkownika "
											+ nextAcceptance.getUsername());
								}
							}
							if (nextAcceptance.getDivisionGuid() != null) {
								division = nextAcceptance.getDivisionGuid();
							}

							if (user != null || division != null) {
								String[] documentActivity = new String[1];
								documentActivity[0] = activityIds[i];
								String plannedAssignment = "";

								if (user != null
										&& (division == null || division.length() < 1)) {
									DSDivision[] divisions = user.getDivisions();
									plannedAssignment = 
										(divisions != null && divisions.length > 0 ? divisions[0].getGuid(): DSDivision.ROOT_GUID)
											+ "/"
											+ user.getName()
											+ "/"
											+ WorkflowUtils.iwfManualName()
											+ "//" + "realizacja";
								}
								if (division != null) {
									plannedAssignment = division + "//"
											+ WorkflowUtils.iwfManualName()
											+ "/*/realizacja";
								}

								String curUser = DSApi.context().getDSUser().getName();
								WorkflowFactory.multiAssignment(
										documentActivity, curUser,
										new String[] { plannedAssignment },
										plannedAssignment, event);
							} else {
								event.addActionError(sm
												.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje")
												+ " "
												+ document.getOfficeNumber());
							}
						} else {
							event.addActionError(sm
									.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje")
										+ " " + document.getOfficeNumber());
						}
					}//if(JBPM) - else

        			TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
        			document.getDocumentKind().setOnly(document.getId(), valuesTmp);

					DSApi.context().commit();
                }//for
            }
            catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
        }
    }
    
    private class FillForm implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {       
        	
        	logRequest(log);
        	if(AvailabilityManager.isAvailable("tasklist.acceptances"))
        	{
        		enableAcceptances = new ArrayList<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>();
        		try
        		{
        			String userName = DSApi.context().getPrincipalName();
        			List<AcceptanceCondition> acceptanceCondition = AcceptanceCondition.userAcceptances(DSApi.context().getPrincipalName());
        			DocumentKind dk = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
        			dk.initialize();
    				Map<String, pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance>
					mapAcc = dk.getAcceptancesDefinition().getAcceptances();
    				
					Set<String> set = mapAcc.keySet();
					for (String cn : set) 
					{
						if(AcceptanceCondition.canAcceptByUser(cn, userName))
						{
							enableAcceptances.add(mapAcc.get(cn));
						}
					}
        		}
        		catch (Exception e) 
				{
					log.error("",e);
				}
        	}
        	
        	if(withBackup==null && isOwnTasks())
        		withBackup = (Boolean)ServletActionContext.getRequest().getSession().getAttribute(tab+"withBackup");
            if(withBackup==null) {
            	withBackup=Boolean.FALSE;
        	}
            if (isOwnTasks()) {
        		ServletActionContext.getRequest().getSession().setAttribute(tab+"withBackup", withBackup);
            }
            
            Integer pom = (Integer) ServletActionContext.getRequest().getSession().getAttribute("offset");
            
            ServletActionContext.getRequest().getSession().setAttribute("offset", offset);
            if(pom != null && pom%2 == 1)
            {
                offset = pom - 1;
                ServletActionContext.getRequest().getSession().setAttribute("offset", offset);
            }
            
        	try	{
        		 if (!DSApi.isContextOpen())
                 	DSApi.openAdmin();
        		 LIMIT = DSApi.context().userPreferences().node("task-list").getInt("task-count-per-page"+tab,20);
    	    } 
    		catch (EdmException ex) {
    			log.error("", ex);
    		}
    		finally	{
    			DSApi._close();
    		}            


            ServletActionContext.getRequest().getSession().setAttribute(tab+"taskList_offset", offset);
            
            if (actionManualFinish) {
            	DSContext context = null;
            	try{
            		context = DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            		context.begin();            		
                    
                	
                		OfficeCase c = OfficeCase.find(caseId);
                		TaskList tasks = (TaskList) ServiceManager.getService(TaskList.NAME);
                    	List<Task> userTasks = tasks.getTasks(DSUser.findByUsername(DSApi.context().getPrincipalName()));
                    	List<OfficeDocument> docList = c.getDocuments();
                    	for (OfficeDocument officeDoc : docList) 
                    	{
    						for (Task task : userTasks)
    						{
    							
    							if(task.getDocumentId().equals(officeDoc.getId()))
    							{
    								String activityId = WorkflowFactory.getInstance().keyToId(task.getActivityKey());
    								WorkflowFactory.getInstance().manualFinish(activityId, true);
    								event.addActionMessage("Zako�czono prace z dokumentem "+ officeDoc.getOfficeNumber());
    								System.out.println("Zako�czono prace z dokumentem "+ officeDoc.getOfficeNumber());
    							}
    						}
    					}
                	
                	context.commit();
                    event.addActionMessage("Zapisano zmiany");
                	
            	} catch(Exception e){
            		if (context != null) DSApi.context().setRollbackOnly();
            		e.printStackTrace();
            	}
        	}
            
            if(!updatedone)
            {
                try{
                    //new UpdateRWAs();
                }
                catch(Exception e)
                {
                }updatedone=true;}
            
            	if(addToTask) 
                addActionMessage(sm.getString("PismaDodanoDoSprawy")+ caseName);
            
            	try{DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));}catch(Exception e){}
            
            	try {
				multiDepartmentEnroll = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
            	} catch (EdmException e1) {
				multiDepartmentEnroll = false;
			}
            //.userPreferences().node("other").getBoolean("multiDepartmentEnroll", false);
            
            //ten kod dodaje do combobox-a (tego na samym dole obok przycisku wykonaj na zaznaczonych)
            //dekretacje do osob pobranych ze zbiorczej dekretacji uzytkownika.
            try
            {
                smallAssignments = new ArrayList<SmallAssignmentBean>();
                Preferences prefs = DSApi.context().userPreferences().node("collectiveassignment");
                String[] keys = prefs.keys();
                Arrays.sort(keys, new Comparator()
                {
                    public int compare(Object o1, Object o2)
                    {
                        return Integer.parseInt(o1.toString()) - Integer.parseInt(o2.toString());
                    }
                });
                for (int i=0; i < keys.length; i++)
                {
                    AssignmentDescriptor ad = AssignmentDescriptor.forDescriptor(prefs.get(keys[i], ""));
                    
                    DSUser tmpuser = null;
                    DSDivision tmpdivision = null;
                    if(ad.getUsername()!=null)
                    {
                    	tmpuser = DSUser.findByUsername(ad.getUsername());
                    }
                    if(ad.getGuid() != null)
                    {
                    	tmpdivision = DSDivision.find(ad.getGuid());
                    }
                    	
                    
                    if(tmpuser!=null || tmpdivision != null)
                        smallAssignments.add(new SmallAssignmentBean(tmpuser,tmpdivision));
                }
            }
            catch(BackingStoreException bse){log.error(bse.getMessage(),bse);}
            catch(EdmException ee){log.error(ee.getMessage(),ee);}
            
            //ten kod dodaje do combobox-a (tego na samym dole obok przycisku wykonaj na zaznaczonych)
            //dzienniki do zbiorczego przyjecia pisma w wydziale
            
            boolean viewchanged = false;
            try
            {
            	List<Journal> docJournals = new ArrayList<Journal>();
            	String journalType = "";
            	incomingDepartments = new ArrayList<DepartmentEnrollBean>();
            	
            	if(tab.equals(TAB_INT))
        			journalType = Journal.INTERNAL;
        		if(tab.equals(TAB_OUT))
        			journalType = Journal.OUTGOING;
        		if(tab.equals(TAB_IN))
        			journalType = Journal.INCOMING;
        		
            	if (DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE))
                {
            		docJournals = Journal.findByType(journalType);	
                }
            	else
            	{
            		boolean przyjeciePismaKomorkaNadrzedna =
                        DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                        // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                        
                		docJournals = Journal.findByDivisionGuid(division.getGuid(),journalType);

                        // dzienniki w kom�rkach nadrz�dnych
                        if (przyjeciePismaKomorkaNadrzedna)
                        {
                            DSDivision parent = division.getParent();
                            while (parent != null && !parent.isRoot())
                            {
                                // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), journalType);
                                docJournals.addAll(docParentJournals);
                                parent = parent.getParent();
                            }
                        }
                    }
                }

                for(int i = 0 ; i < docJournals.size() ; i++){
                    incomingDepartments.add(new DepartmentEnrollBean(docJournals.get(i)));
                };
                
                
                //Odzyskiwanie parametrow z sesji
                showLabels = showLabels();
                
                viewedLabels = (Collection<Label>)ServletActionContext.getRequest().getSession().getAttribute(tab+"viewedLabels");
                if(viewedLabelId == null && isOwnTasks())
                	viewedLabelId = (Long)ServletActionContext.getRequest().getSession().getAttribute(tab+"viewedLabelId");

                if (isOwnTasks()) 
            		ServletActionContext.getRequest().getSession().setAttribute(tab+"viewedLabelId", viewedLabelId);
            		
                if(viewedLabels == null) 
                {
                	viewedLabels = new Vector<Label>();
                	//ServletActionContext.getRequest().getSession().setAttribute("viewedLabels", viewedLabels);
                }
                if(viewedLabelId!=null&& viewedLabelId > 0L)
                {
                	boolean contains = false;
                	for(Label lab: viewedLabels)
                	{
                		if(lab.getId().equals(viewedLabelId)) contains = true;
                	}
                	if(!contains)
                		viewedLabels.add(LabelsManager.findLabelById(viewedLabelId));
                	viewchanged = true;
                	
                }
                else if(viewedLabelId!=null && viewedLabelId == -1L )
                {
                	log.trace("0L");
                	viewedLabels = new Vector<Label>();
                }
                
                if (AvailabilityManager.isAvailable("labels"))
                {
                	List<Label> flags = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
                	for(Label lab:flags)
                	{
                		lab.setAliasName(sm.getString("Flaga")+" " +lab.getName());
                	}
                	availableLabels = flags;
                	List<Label> labels = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
                	for(Label lab:labels)
                	{
                		lab.setAliasName(lab.getName());
                	}
                	availableLabels.addAll(labels);
                
                
                
                
	                Label l = new Label();
	                l.setId(-2L);
	                l.setAliasName(sm.getString("Default"));
	                availableLabels.add(0, l);
	                
	                Label ll = new Label();
	                ll.setId(-1L);
	                ll.setAliasName(sm.getString("Wszystkie"));
	                availableLabels.add(ll);
	                availableForAddLabels = LabelsManager.getWriteLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
	                
	                ServletActionContext.getRequest().getSession().setAttribute(tab+"viewedLabels", viewedLabels);
	                
	                userFlags_label = LabelsManager.getPrivateModifyLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
	                systemFlags_label = LabelsManager.getNonPrivateModifyLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
	                
	                labels_label = new ArrayList<Label>();
	                systemLabels_label = new ArrayList<Label>();
	                for(Label lab : LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE))
	                {
	                	if(lab.getModifiable())
                		{
                			labels_label.add(lab);
                		}
                		else
                		{
                			systemLabels_label.add(lab);
                		}
                	}
                	labels_label.add(l);
                	labels_label.add(ll);
                	systemLabels_label.add(l);
                	systemLabels_label.add(ll);                
                }
                else
                {                	
                	availableLabels = new Vector<Label>();    
                	labels_label = new Vector<Label>();
                	systemLabels_label = new Vector<Label>();
                }
            }
            //catch(BackingStoreException bse){}
            catch(EdmException ee){log.error("",ee);}
            
            if(viewchanged)
            {
            	ServletActionContext.getRequest().getSession().setAttribute(tab+"taskList_offset", 0);
            }
            if(offset == null && ServletActionContext.getRequest().getSession().getAttribute(tab+"taskList_offset") == null)
            {
            	offset = 0;
            }
            else if(offset == null && ServletActionContext.getRequest().getSession().getAttribute(tab+"taskList_offset") != null)
            {
            	offset = (Integer) ServletActionContext.getRequest().getSession().getAttribute(tab+"taskList_offset");
            }
            
            try
            {
                dockindNames = DocumentKind.list(true);
            }
            catch(EdmException edme){}
            

            try
            {
                // lista u�ytkownik�w do wy?wietlenia
                users = getDSUserList();

                DSUser user = getDSUser();
                
                
                
                int tasksSize = 0;
                boolean isPager = true;//false;
                
                if (user != null)
                {
                	
                	
                    DSUser currentUser = DSApi.context().getDSUser();
                    viewingLoggedUser = user.equals(currentUser);

                    // nale�y sprawdzi�, czy bie�?cy u�ytkownik ma prawo do
                    // przegl?dania zada� wybranego u�ytkownika; je�eli wybrany
                    // u�ytkownik jest na li?cie zwracanej przez getDSUsers(),
                    // wszystko w porz?dku
                    if (!viewingLoggedUser)
                    {
                        if (users == null)
                            return;

                        boolean ok = false;
                        for (Iterator iter=users.iterator(); iter.hasNext(); )
                        {
                            if (user.equals(iter.next()))
                            {
                                ok = true;
                                break;
                            }
                        }

                        if (!ok)
                            return;
                    }

                    columns = getActualColumns();
                    filterColumns = getFilterColumn();
                    
                    if(sortField == null)
                    {
                    	sortField = (String)ServletActionContext.getRequest().getSession().getAttribute(tab+"sortField");
                    }
                    /* sprawdzanie czy uzywamy domyslnego sortowania */
                    if (sortField == null) {
                    	sortField = DSApi.context().userPreferences().node("task-list").get("sort-column_"+tab, "receiveDate");
                    	
                    	boolean is = false;
                    	for (Iterator iter = columns.iterator(); iter.hasNext();)
                    	{
                    		TableColumn column = (TableColumn) iter.next();
                    		String field = column.getProperty();
                    		if (field.equals(sortField))
                    			is = true;
                    	}
                    	if (!is)
                    		sortField = "receiveDate";
                    }
                    
                    ServletActionContext.getRequest().getSession().setAttribute(tab+"sortField", sortField);
                    
                    if(ascending==null)
                    {
                    	ascending = (Boolean)ServletActionContext.getRequest().getSession().getAttribute(tab+"ascending");
                    }
                    if(ascending==null)
                        ascending = Boolean.parseBoolean(DSApi.context().userPreferences().node("task-list").get("ascending"+tab, "false"));
                    ServletActionContext.getRequest().getSession().setAttribute(tab+"ascending", ascending);
                    
                    
                    /* needSort <=> konieczne zewnetrzne (czyli "tutaj") sortowanie */
                    boolean needSort = ("userFlags".equals(sortField) || "flags".equals(sortField) || "sender".equals(sortField) || "documentSender".equals(sortField) || "receiver".equals(sortField) || "orderStatus".equals(sortField) || "author".equals(sortField) || "clerk".equals(sortField));

                    TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);

                    Object filterValue = null;
                    
                    if(filterBy == null)
                    {
                    	filterBy = (String)ServletActionContext.getRequest().getSession().getAttribute(tab+"filterBy");
                    	filterName = (String)ServletActionContext.getRequest().getSession().getAttribute(tab+"filterName");
                    	
                    }
                    else
                    {
                    	ServletActionContext.getRequest().getSession().setAttribute(tab+"taskList_offset", 0);
                    }
                    if(filterBy==null) filterBy="Brak";
                    if(filterBy.equals("Brak")) filterName = "";
                    
                    
                    ServletActionContext.getRequest().getSession().setAttribute(tab+"filterBy", filterBy);
                	ServletActionContext.getRequest().getSession().setAttribute(tab+"filterName", filterName);
                	
                    String parsedFilterBy = "";
                	
                    if(filterName != null && filterBy != null && !TAB_WATCHES.equals(tab) && !filterBy.equals("null") && !filterBy.equals("Brak"))
                    {	
                    	filterName = filterName.trim();   
                    	parsedFilterBy = filterBy;
                    	try
	                	{
                    		if(parsedFilterBy.equals("clerk"))
	                    	{
                    			parsedFilterBy = "documentClerk";
	                    	}
                    		else if(parsedFilterBy.equals("sender"))
	                    	{
                    			parsedFilterBy = "activity_lastActivityUser"; // dekretujacy
	                    	}
	                    	TaskSnapshot ts = TaskSnapshot.getInstance();
	        	            Column dsa = DSApi.getColumn(ts.getClass(), parsedFilterBy);
	        	            Type t = DSApi.getColumnType(ts.getClass(), parsedFilterBy);
	        	            parsedFilterBy = dsa.getName();
	        	            if(parsedFilterBy.equals("dsw_process_context_param_val") || parsedFilterBy.equals("documentClerk") || parsedFilterBy.equals("documentAuthor"))
	        	            {
	        	            	DSUser tempUser = null;
	        	            	String[] tmp = filterName.split(" ");
	        	            	try
	        	            	{
		        	            	tempUser = DSUser.findByFirstnameLastname(tmp[0], tmp[1]);
		        	            	if(tempUser == null)
		        	            	{
		        	            		tempUser = DSUser.findByFirstnameLastname(tmp[1], tmp[0]);
		        	            		if(tempUser != null)
		        	            			filterValue = tempUser.getName();
		        	            	}
		        	            	else
		        	            	{
		        	            		filterValue = tempUser.getName();
		        	            	}
	        	            	}
	        	            	catch (Exception e) 
	        	            	{
	        	            		log.error("exc",e);		
								}
	        	            }
	        	            else if(t.getReturnedClass().equals(Integer.class))
	        	            {
	        	            	filterValue = Integer.parseInt(filterName);
	        	            }
	        	            else if(t.getReturnedClass().equals(Date.class))
	        	            {
	        	            	filterValue = DateUtils.parseDateAnyFormat(filterName);
	        	            }
	        	            else if(t.getReturnedClass().equals(String.class))
	        	            {
	        	            	filterValue = filterName;
	        	            }
	        	            else if(t.getReturnedClass().equals(Float.class))
	        	            {
	        	            	filterValue = Float.parseFloat(filterName);
	        	            }
	        	            else if(t.getReturnedClass().equals(Long.class))
	        	            {
	        	            	filterValue = Long.parseLong(filterName);
	        	            }
	        	            else
	        	            {
	        	            	filterValue = filterName;
	        	            }	               
	                	}
	                	catch (Exception e) 
	                	{
	                		//event.addActionMessage(sm.getString("NiepoprawnaWartoscWfiltrze"));
	                		filterBy = null;
	                		filterValue = null;
	        			}
                    }
                    
                    /**
                     * @TODO Ustalamy zakres etykiet
                     */
                    Collection<Label> labels = null;
                    if (viewedLabels!= null)
                    {
                    	labels = viewedLabels;
                    }

                    int tempLimit = LIMIT;
                    if ("documentId".equals(filterBy) || "documentOfficeNumber".equals(filterBy))
                    	tempLimit = 0;
                    
                    if (TAB_IN.equals(tab))
                    {
                    	onTaskList = true;
                        if (needSort)
                        {
                            Map map = taskList.getDocumentTasks(null, user, InOfficeDocument.TYPE, searchOfficeNumber, sortField, ascending, offset, tempLimit,parsedFilterBy,filterValue,withBackup,labels,(viewedLabelId!=null && viewedLabelId == -1L ),null);
                        	tasksSize = ((Integer) map.get("size")).intValue();
                        	tasks = (List<Task>) map.get("tasks");
                        	isPager = true;
                        }
                        else
                        {
                            Map map = taskList.getInDocumentTasks(null, user, offset, tempLimit, searchOfficeNumber, sortField, ascending,parsedFilterBy,filterValue,withBackup,labels,(viewedLabelId!=null && viewedLabelId == -1L ),null);
                            tasksSize = ((Integer) map.get("size")).intValue();
                            tasks = (List<Task>) map.get("tasks");
                            isPager = true;
                        }
                    }
                    else if (TAB_OUT.equals(tab))
                    {
                    	onTaskList = true;

                        Map map = taskList.getDocumentTasks(null, user, OutOfficeDocument.TYPE, searchOfficeNumber, sortField, ascending, offset, tempLimit,parsedFilterBy,filterValue,withBackup,labels,(viewedLabelId!=null && viewedLabelId == -1L ),null);
                        tasksSize = ((Integer) map.get("size")).intValue();
                        tasks = (List<Task>) map.get("tasks");
                        isPager = true;
                    }
                    else if (TAB_INT.equals(tab))
                    {
                    	onTaskList = true;

                        Map map = taskList.getDocumentTasks(null, user, OutOfficeDocument.INTERNAL_TYPE, searchOfficeNumber, sortField, ascending, offset, tempLimit,parsedFilterBy,filterValue,withBackup,labels,(viewedLabelId!=null && viewedLabelId == -1L ),null);
                        tasksSize = ((Integer) map.get("size")).intValue();
                        tasks = (List<Task>) map.get("tasks");
                        isPager = true;
                    }
                    else if (TAB_ORDER.equals(tab))
                    {          
                    	onTaskList = false;
                    	//Map map = taskList.getDocumentTasks(user, OfficeOrder.TYPE, searchOfficeNumber, sortField, ascending, offset, LIMIT);                    	
                        
                        if (TAB_MY_ORDERS.equals(orderTab))
                            tasks = taskList.getUserOrderTasks(null, user, searchOfficeNumber, sortField, ascending);
                        else if (TAB_WATCH_ORDERS.equals(orderTab))
                        {
                        	tasks = taskList.getWatchOrderTasks(user, searchOfficeNumber, sortField, ascending);
                        	////tasksSize = ((Integer) map.get("size")).intValue();
                        	//tasks = (List<Task>) map.get("tasks");
                        }
                        else
                        {
                        	Map map = taskList.getDocumentTasks(null, user, OfficeOrder.TYPE, searchOfficeNumber, sortField, ascending, offset, tempLimit);
                        	tasksSize = ((Integer) map.get("size")).intValue();
                            tasks = (List<Task>) map.get("tasks");
                        }
                    }
                    else if (TAB_CASES.equals(tab))
                    {
                    	onTaskList = false;
                    	List<OfficeCase> tempCases = OfficeCase.findUserCases(user, searchOfficeId);
                    	tasksSize = tempCases.size();
                        searchOfficeId = TextUtils.trimmedStringOrNull(searchOfficeId);
                        cases = fun.map(tempCases,
                            new lambda<OfficeCase, CaseTask>()
                            {
                                public CaseTask act(OfficeCase o)
                                {
                                    return new CaseTask(o);
                                }
                            });
                    }
                    else if (TAB_WATCHES.equals(tab))
                    {
                    	onTaskList = false;
                        watches = updateWatches(user);
                    }
                    else
                    {
                        throw new EdmException(sm.getString("NieznanyRodzajZak�adki")+": "+tab);
                    }
                    //LIMIT = 30;
                    
                    if (tasks != null && searchOfficeNumber != null)
                    {
                        // je�eli nie znaleziono szukanego dokumentu, wy�wietlana
                        // jest kompletna lista
                        if (tasks.size() == 0)
                            addActionError(sm.getString("NieZnalezionoZadaniaOzadanymNumerzeKO"));
                    }

                    if (cases != null && searchOfficeId != null)
                    {
                         if (cases.size() == 0)
                            addActionError(sm.getString("NieZnalezionoSprawyOzadanymNumerze"));
                    }
                    // format u�ywany do formatowania liczb, kt�re pos�u�? za
                    // klucze sortowania leksykalnego; dodaje na pocz?tku zera
                    final NumberFormat millisFormat = NumberFormat.getIntegerInstance();
                    millisFormat.setMinimumIntegerDigits((""+Long.MAX_VALUE).length());
                    millisFormat.setGroupingUsed(false);
                    millisFormat.setParseIntegerOnly(true);
                    // napis o najmniejszej mo�liwej warto?ci u�ywany zamiast
                    // napisu pustego w sortowaniu leksykalnym
                    final String MIN_STR = String.valueOf((char) 0);
                    
                    //wyrzucanie z tasklisty tych rodzajow dok., ktore nie byly wybrane (filtr)
                    
                    if(filterDockindNames!=null && filterDockindNames.length()>0)
                    {
                        
                        List<Task> filtered_tasks = new ArrayList<Task>();
                        for(Task task : tasks)
                                if(task.getDockindName()!= null && task.getDockindName().equals(filterDockindNames))
                                {
                                    filtered_tasks.add(task);
                                    continue;
                                }
                            tasks = filtered_tasks;
                    }
                    
                    // sortowanie tablicy zada�
                    if (tasks != null && tasks.size() > 1 && needSort)
                    {       
                    	//sortowanie przenioslem do kontenera
                        tasks = sortTask(tasks);
                                                
                        if (TAB_IN.equals(tab))
                        {
                            isPager = true;
                            tasksSize = tasks.size();
                            int count = 0;
                            List<Task> tasksCopy = new ArrayList<Task>(tasks);
                            for (Iterator iter=tasksCopy.iterator(); iter.hasNext(); )
                            {
                                Task task = (Task) iter.next();
                                
                                if (count < offset || count >= offset + LIMIT)
                                    iter.remove();
                                count++;
                            }
                            tasks = tasksCopy;
                        }
                    }
                    // sortowanie tablicy spraw
                    else if (cases != null && cases.size() > 1)
                    {
                        List<String> sortKeys = new ArrayList<String>(cases.size());

                        for (Iterator iter=cases.iterator(); iter.hasNext(); )
                        {
                            CaseTask task = (CaseTask) iter.next();
                            String key;
                            String oidKey = task.getCaseOfficeId().toUpperCase();

                            if ("caseStatus".equals(sortField))
                            {
                                key = task.getCaseStatus() != null ?
                                    task.getCaseStatus().toUpperCase() : MIN_STR;
                            }
                            else if ("caseOpenDate".equals(sortField))
                            {
                                key = millisFormat.format(task.getCaseOpenDate() != null ?
                                    task.getCaseOpenDate().getTime() : 0);
                            }
                            else if ("caseFinishDate".equals(sortField))
                            {
                                key = millisFormat.format(task.getCaseFinishDate() != null ?
                                    task.getCaseFinishDate().getTime() : 0);
                            }
                            else if ("caseDaysToFinish".equals(sortField))
                            {
                                int daysToFinish = 0;
                                if (task.getCaseDaysToFinish() != null)
                                    daysToFinish = task.getCaseDaysToFinish().intValue();
                                if (daysToFinish < 0)
                                {
                                    daysToFinish = Integer.MIN_VALUE - daysToFinish;
                                }
                                key = millisFormat.format(daysToFinish);
                            }
                            else
                            {
                                key = oidKey;
                            }

                            // dodawanie pobocznego klucza sortowania (officeId)
                            key += ("<>" + oidKey);

                            sortKeys.add(key);
                        }

                        CaseTask[] taskArray = (CaseTask[]) cases.toArray(new CaseTask[cases.size()]);
                        std.sort.qsort(
                            (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                            taskArray,
                            ascending);

                        cases = Arrays.asList(taskArray);
                    }
                    // sortowanie pism obserwowanych
                    else if (watches != null && watches.size() > 1)
                    {
                        List<String> sortKeys = new ArrayList<String>(watches.size());

                        for (Iterator iter=watches.iterator(); iter.hasNext(); )
                        {
                            Task task = (Task) iter.next();
                            String key;
                            String onKey = task.getDocumentOfficeNumber() != null ?
                                    millisFormat.format(task.getDocumentOfficeNumber()) :
                                    millisFormat.format(0);
                            
                            if ("receiveDate".equals(sortField))
                            {
                                key = millisFormat.format(task.getReceiveDate() != null ?
                                        task.getReceiveDate().getTime() : 0);
                            }
                            else if ("sender".equals(sortField))
                            {
                                key = task.getSender() != null ?
                                    task.getSender().toUpperCase() : MIN_STR;
                            }
                            else if ("deadlineTime".equals(sortField))
                            {
                                key = millisFormat.format(task.getDeadlineTime() != null ?
                                        task.getDeadlineTime().getTime() : 0);
                            }
                            else if ("description".equals(sortField))
                            {
                                key = task.getDescription() != null ?
                                    task.getDescription().toUpperCase() : MIN_STR;
                            }
                            else if ("objective".equals(sortField))
                            {
                                key = task.getObjective() != null ?
                                    task.getObjective().toUpperCase() : MIN_STR;
                            }
                            else // documentOfficeNumber
                            {
                                key = onKey;
                            }

                            // na ko�cu klucza zawsze numer kancelaryjny, poniewa�
                            // jest to poboczny klucz sortowania
                            key += ("<>" + onKey);

                            sortKeys.add(key);                            
                        }

                        Task[] taskArray = (Task[]) watches.toArray(new Task[watches.size()]);
                        std.sort.qsort(
                            (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                            taskArray,
                            ascending);

                       watches = Arrays.asList(taskArray);
                    }
                }
                if(TAB_IN.equals(tab)){
                    actionReplyUnnecessary = Configuration.isAdditionOn(Configuration.ADDITION_DO_NOT_REQUIRE_RESPOND);
                }
                if (DSApi.context().hasPermission(DSPermission.ZADANIE_KONCZENIE))
                    actionManualFinish = true;
                if ((TAB_IN.equals(tab) || TAB_OUT.equals(tab) || TAB_INT.equals(tab)) &&
                    !Configuration.hasExtra("business"))
                    actionAddToCase = true;
                if (Configuration.hasExtra("business"))
                    actionAttachmentsAsPdf = true;
                if ((TAB_OUT.equals(tab) && DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO)))
                    actionAssignOfficeNumber = true;
                actionMultiAssignment = true;
                actionDepartmentEnroll = true;
                actionAssignMe = (username != null) && (!username.equals(DSApi.context().getPrincipalName()));
                if (Docusafe.hasExtra("business"))
                {
                    actionMultiBoxing = true;
                    if(DocumentKind.isAvailable(DocumentLogicLoader.PROSIKA)) {
						actionMultiBoxing = false;
					}
                }
                
                // -- start
                if(tasks!=null && AvailabilityManager.isAvailable("labels"))
                {
	                List<Label> labels;
	                List<Task> toremove = new ArrayList<Task>();
	                if(viewedLabelId==null) viewedLabelId = new Long(-2);
	                /*if(viewedLabelId!=null && viewedLabelId > 0)
	                {
	                	LabelsManager.findLabelById(viewedLabelId)
	                	boolean isVisible;
		                for(Task t:tasks)
		                {
		                	isVisible = false;
		                	for(DocumentToLabelBean l : LabelsManager.getReadBeansForDocument(t.getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE))
		                	{
		                		if(l.getLabelId().equals(viewedLabelId)) isVisible = true;
		                	}
		                	if(!isVisible) toremove.add(t);
		                }
	                }
	                else */
	                if(viewedLabelId.equals(new Long(-2)))
	                {
	                	//for(Task t:tasks)
	                    {
	                   // 	for(DocumentToLabelBean l:LabelsManager.getReadBeansForDocument(t.getDocumentId(), DSApi.context().getPrincipalName(),Label.LABEL_TYPE))
	                    	{
	                    //		if(l.getHidding()) toremove.add(t);
	                    	}
	                    	//||DocumentToLabelBean.find(t.getDocumentId(), l.getId()).get(0).getActionTime().after(new Date())
	                    }
	                }
	               // tasks.removeAll(toremove);
                }
                // --------------
                
                /*if(filterName == null || (filterName != null && filterName.equalsIgnoreCase("null")))
                	filterName = "";*/
                
                if (tasks != null && isPager)
                {
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset) 
                        {
                            return
                                HttpUtils.makeUrl(getBaseLink(),
                                new Object[] {
                                "tab", tab,
                                "sortField", sortField,
                                "ascending", String.valueOf(ascending),
                                "username", username,
                                "withBackup",withBackup,
                                "filterBy",filterBy,
                                "filterName",filterName == null || (filterName != null && filterName.equalsIgnoreCase("null")) ? "" : filterName,
                                "filterDockindNames", filterDockindNames,
                                "offset", String.valueOf(offset)});
                        }
                    };
                    pager = new Pager(linkVisitor, offset, LIMIT, tasksSize, 10);
                }
                taskCount = tasksSize;
                /* To na razie odpuszczamy
                if (tasks!=null)
                {
                	for (Task task : tasks)
                	{
                		taskListContainer.addTaskToList(task);
                	}                	
                	
                }
                */
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
            if(getError() != null){
            	addActionError(sm.getString(getError()));
            }
        }
    }
    
    public class SendToEva implements ActionListener
    {
    	
    	public void actionPerformed(ActionEvent event)
    	{
    		if ((activityIds == null || activityIds.length == 0) && (actionCaseIds == null || actionCaseIds.length == 0))
                addActionError(sm.getString("NieWybranoZadan"));
    		
    		 Set<String> deletedIds = new HashSet<String>();
    		try
    		{
                
                if (activityIds != null)
                {
                    for (int i = 0; i < activityIds.length; i++)
                    {
                    

                        if (deletedIds.contains(activityIds[i]))
                        {
                            event.getLog().warn(sm.getString("ObiektZostalJuzUsuniety",activityIds[i]));
                            continue;
                        }
    		
                    	deletedIds.add(activityIds[i]);
                    	
                    	WorkflowActivity activity = new WorkflowActivity(WorkflowFactory.getInstance().keyOfId(activityIds[i]));
                        
                    	
                    	
                        if (activity == null)
                        {
                            event.getLog().warn(sm.getString("WzadaniuBrakujeParametru",activityIds[i],"ds_finished"));
                            continue;
                        }

                        // szukanie dokumentu zwi?zanego z zadaniem
                        Long documentId = activity.getDocumentId();
                        if (documentId == null)
                        {
                            event.getLog().warn(
                                "Z zadaniem " + activityIds[i] + " nie jest zwi�zany " + "dokument (ds_document_id)");
                            continue;
                        }

                        Document document = Document.find(documentId);
                        
                        OfficeDocument officeDocument = (OfficeDocument)document;
	                    if(officeDocument.getDocumentKind() != null && !officeDocument.getDocumentKind().getCn().equals(DocumentLogicLoader.ROCKWELL_KIND))
	                    {
	                    	addActionMessage(sm.getString("NieMoznaWyslacZadaniaDoEVAPoniewazNieJestToDokumentRockwell",officeDocument.getOfficeNumber()));
	                    	continue;
	                    }
						    		
		    			Map<String,Object> statusMap = new HashMap<String,Object>();
		                statusMap.put(RockwellLogic.EVA_STATUS_FIELD_CN,1);
		                statusMap.put(RockwellLogic.SUBMITTED_BY_CN,DSApi.context().getPrincipalName());
		                statusMap.put(RockwellLogic.SUBMIT_DATE_FIELD_CN,new Date());
		                statusMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,RockwellLogic.STAT_PROCESSED_EVA);
		                
		               // Document doc = Document.find(getDocumentId());

		                if(document != null)
		                {
		                	document.getDocumentKind().setWithHistory(document.getId(),statusMap,false);
		                
		                	DSApi.context().begin();
		
		                    
		                 //   WfActivity activity = getActivity() != null
		                //        ? DSApi.context().getWfActivity(ActivityId.parse(getActivity()))
		                //        : null;
		                    WorkflowFactory.getInstance().manualFinish(activityIds[i], true);
		
		                    try
		                    {
		                        DSApi.context().session().flush();
		                    }
		                    catch(HibernateException e)
		                    {
		                        throw new EdmException(e);
		                    }
		
		                   	TaskSnapshot.updateByDocumentId(document.getId(),document.getStringType());
		                   	DSApi.context().commit();
		                   	
		                  //powiadamiany EVA
		                   	pl.compan.docusafe.parametrization.ra.EvaNotifier.notifyEva();
		                }
                    }
                }
						                
    		}                
    		catch(Exception e)
    		{
    			log.debug("", e);
    			addActionError(e.getMessage());
    		}
    	}
    	
    }

    @Unchecked
    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if ((activityIds == null || activityIds.length == 0) && (actionCaseIds == null || actionCaseIds.length == 0))
                addActionError(sm.getString("NieWybranoZadanDoZakonczenia"));

            if (hasActionErrors())
                return;

            // zbi�r, w kt�rym ?ledzone s? identyfikatory zako�czonych proces�w
            // na wypadek, gdyby pojawi�a si� pr�ba usuni�cia tego samego procesu
            // dwukrotnie (np. w wypadku zada� pochodz?cych z zast�pstw)
            Set<String> deletedIds = new HashSet<String>();

            int notInternalTasks = 0;
            
            //TODO trzeba jako� "rozpl�ta�" sprawdzanie warunk�w w tej funkcji powinno korzysta� si� z dockind logic

            try
            {
                DSApi.context().begin();
                
                if (activityIds != null)
                {
                    for (int i = 0; i < activityIds.length; i++)
                    {

                        if (deletedIds.contains(activityIds[i]))
                        {
                            event.getLog().warn(sm.getString("ObiektZostalJuzUsuniety",activityIds[i]));
                            continue;
                        }
                       

                        deletedIds.add(activityIds[i]);

                        /*
                         * tu kiedys bylo sprawdzenie czy zadanie nie jest juz zamkniete
                         */
                        WorkflowActivity activity = WorkflowFactory.getWfActivity(activityIds[i]);
                        
                        Long documentId = WorkflowFactory.getDocumentId(activityIds[i]);
                        if (documentId == null)
                        {
                            event.getLog().warn(sm.getString("Zzadaniem")+" "+activityIds[i]+" "+"nie jest zwi?zany"+" " +
                                sm.getString("dokument")+" (ds_document_id)");
                            continue;
                        }

                        OfficeDocument document = OfficeDocument.find(documentId);

                        if (Configuration.hasExtra("business"))
                        {

                            /***************************************************
							 * Hack dla pism CRM - trzeba przenie�� ca�y ten blok do DocumentLogic
							 **************************************************/
                            {
                            	DocumentLogic logic = null;
                            	if(document.getDoctype() != null){
                            		logic = document.getDocumentKind().logic();
                            	}
                            	if(document.getDocumentKind().getCn().startsWith("crm"))
                            	{
                            		//do nothing
                            	} else if (!document
										.hasWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX)
										&& !document.getDocumentKind().getCn().equals(
														DocumentLogicLoader.INVOICE_IC_KIND)
										&& !document.getDocumentKind().getCn().equals(DocumentLogicLoader.CRMTASK_KIND)				
										&& document.getType() == DocumentType.INCOMING
										&& !"fax".equals(document.getSource())
										&& ((InOfficeDocument) document).getKind() != null
										&& ((InOfficeDocument) document).getKind().getName().indexOf(
														"iznesowy") > 0
										&& document.getBox() == null) 
                            	{
									addActionMessage(sm.getString("NieZakonczonoPracyZdokumentemPoniewazNieZostalOnPrzypisanyDoPudlaArchiwalnego",
													(document instanceof OfficeDocument ? ((OfficeDocument) document).getOfficeNumber(): "ID="+ document.getId())));

									continue;
								}
							}

                            // Brief IV: je�eli wyznaczono termin zako�czenia
							// zadania,
                            // u�ytkownik nie mo�e zako�czy� pracy z dokumentem, dop�ki
                            // nie doda uwagi

                        }

                        // warunki zako�czenia procesu wynikaj�ce z danego rodzaju dokumentu
                        if (document.getDocumentKind() != null)
                        {
                            try
                            {
                                document.getDocumentKind().logic().checkCanFinishProcess((OfficeDocument) document);
                            }
                            catch (AccessDeniedException e)
                            {
                                addActionMessage(sm.getString("NieZakonczonoPracyZdokumentemPoniewazNieSpelniaWszystkichWarunkowZakonczeniaProcesu",
                                    ((OfficeDocument) document).getOfficeNumber()));
                                continue;
                            }
                        }

                        // zadanie nie by�o jeszcze zaakceptowane
                        if (!activity.getAccepted())
                        {
                        	
                            document.changelog(DocumentChangelog.WF_ACCEPT);
                            WorkflowFactory.getInstance().acceptTask(activityIds[i], event, document,DSDivision.ROOT_GUID);

                            if (document instanceof OfficeDocument)
                            {

                                    ((OfficeDocument) document).addAssignmentHistoryEntry(
                                        new AssignmentHistoryEntry(activity.getLastUser(),activity.getLastDivision(),
                                        		DSApi.context().getPrincipalName(), activity.getCurrentGuid(),
                                        		activity.getObjective(), WorkflowFactory.getInstance().isManual(activity)
                                            ? sm.getString("doRealizacji") : sm.getString("doWiadomosci"), true,
                                            WorkflowFactory.getInstance().isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));
                                
                            }
                        }

                        if (document instanceof OfficeDocument)
                        {
                            ((OfficeDocument) document).addAssignmentHistoryEntry(
                                new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), activity.getProcessDesc(),
                                    WorkflowFactory.getInstance().isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));

                            ((OfficeDocument) document).setCurrentAssignmentGuid(null);
                            ((OfficeDocument) document).setCurrentAssignmentUsername(null);
                            ((OfficeDocument) document).setCurrentAssignmentAccepted(null);
                        }

                        if (document.getType() == DocumentType.INCOMING)
                        {
                            InOfficeDocument in = (InOfficeDocument) document;
                            in.setStatus(InOfficeDocumentStatus.find(InOfficeDocumentStatus.ID_FINISHED));
                        }
                        
                        document.changelog(DocumentChangelog.WF_FINISH);
                        
                        if (document instanceof OfficeDocument) {
                        	//to normalnie (w summary action) jest wywo�ywane podczas zamykania sprawy
                        	WorkflowFactory.getInstance().manualFinish(activityIds[i], true);
                        } else {
                        	addActionError(sm.getString("ToNieJestDokumentKancelaryjny"));
                        }

                        TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());
                    }//for TODO - zmniejszy� ca�� metod� a w szczeg�lno�ci t� p�tl�
                }
                if (actionCaseIds != null)
                {
                    Integer fsId = CaseStatus.ID_CLOSED;
                    CaseStatus finishedStatus = CaseStatus.find(fsId);
                    for (int i = 0; i < actionCaseIds.length; ++i)
                    {
                        OfficeCase oc = OfficeCase.find(actionCaseIds[i]);
                        oc.setStatus(finishedStatus);
                        
                        //konczenie pism w sprawie
                        TaskList tasks = (TaskList) ServiceManager.getService(TaskList.NAME);
                    	List<Task> userTasks = tasks.getTasks(DSUser.findByUsername(DSApi.context().getPrincipalName()));
                    	List<OfficeDocument> docList = oc.getDocuments();
                    	for (OfficeDocument officeDoc : docList) 
                    	{
    						for (Task task : userTasks)
    						{
    							
    							if(task.getDocumentId().equals(officeDoc.getId()))
    							{
    								String activityId = WorkflowFactory.getInstance().keyToId(task.getActivityKey());
    								WorkflowFactory.getInstance().manualFinish(activityId, true);
    								addActionMessage("Zako�czono prace z dokumentem "+ officeDoc.getOfficeNumber());								                                
    							}
    						}
    					}
                    }
                    actionCaseIds = null;
                }

                DSApi.context().commit();

                if (notInternalTasks > 0)
                    addActionError(notInternalTasks + " "+sm.getString("ZwybranychZadanNieZakonczonoGdyzNie")+" "+(notInternalTasks == 1 ? sm.getString("jest") : sm.getString("s�"))+" "+sm.getString("WprocesieDekretacjiRecznej"));
                   // addActionError(smL.getString("NieMoznaZakonczycWybranychZadan"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Nadaje numery kancelaryjne wybranym pismom.
     */
    private class AssignOfficeNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (documentIds == null || documentIds.length == 0)
                addActionError(sm.getString("NieWybranoZadan"));

            if (hasActionErrors())
                return;

            Date entryDate;
            Long journalId;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                entryDate = GlobalPreferences.getCurrentDay();

                // pisma wewn�trzne dostaj? numer KO w momencie tworzenia,
                // ale nie szkodzi umie?ci� tutaj rozr�nienia
                journalId = Journal.getMainOutgoing().getId();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                DSApi._close();
            }

            for (int i=0; i < documentIds.length; i++)
            {
                Long id = documentIds[i];
                if (id == null)
                    continue;

                // long time = System.currentTimeMillis();

                try
                {
                    DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                    // konieczne, aby sprawdzi�, �e dokument istnieje
                    OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(id);

                    if (document.getOfficeNumber() != null)
                        continue;
                    
                    if (!Configuration.hasExtra("business")&&!OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(document.getPrepState()))
                    {
                        addActionError(sm.getString("NieMoznaNadacNumeruKOdokumentowi")+" "+document.getId()+
                            sm.getString("PoniewazNieJestOnCzystopisem"));
                        continue;
                    }
                }
                catch (DocumentNotFoundException e)
                {
                    //addActionError("Nie odnaleziono dokumentu "+id);
                    continue;
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
                finally
                {
                    DSApi._close();
                }

                Integer sequenceId;
                synchronized (Journal.class)
                {
                    try
                    {
                        sequenceId = Journal.TX_newEntry2(journalId, id, entryDate);
                    }
                    catch (EdmException e)
                    {
                        addActionError(e.getMessage());
                        return;
                    }
                }

                try
                {
                    DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                    DSApi.context().begin();

                    OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(id);
                    doc.bindToJournal(journalId, sequenceId);

                    TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),doc.getStringType());

                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
                finally
                {
                    try { DSApi.close(); } catch (Exception e) { }
                }
            }
        }
    }

  
    /**
     * Wykonanie dekretacji z pola akcji
     */
    private class SmallAssignment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(sm.getString("NieWybranoZadan"));
                return;
            }
            
            try
            {
            	DSApi.context().begin();
                String[] userDivision  = nameOfSmallAssignment.split(";");
                String username = userDivision[0];
                String guid = userDivision[1];
                if (StringUtils.isEmpty(guid)) {
                	throw  new EdmException("division not specified");
                }
                String plannedAssignment;
                if (StringUtils.isNotEmpty(username)) {
                	plannedAssignment = guid+"/"+username+"/"+WorkflowUtils.iwfManualName()+"//"+"realizacja";
                } else {
                	plannedAssignment = guid+"//"+WorkflowUtils.iwfManualName()+"//"+"realizacja";
                }
             
                WorkflowFactory.multiAssignment(activityIds,getUsername(), null, plannedAssignment, event);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            	try { 
            		DSApi.context().rollback();
            	} catch (Exception e2) {
            		log.debug("", e2);
            	}
            }
        }
    }
    
    private class MultiDepartmentEnroll implements ActionListener{

        public void actionPerformed(ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(sm.getString("NieWybranoZadan"));
                return;
            }
            
             try
            {
                //DSApi.context().begin();
                for (int i=0; i < documentIds.length; i++){
                    
                    DSDivision division;
                    Date entryDate = new Date();
                    Journal journal;
                    try
                    {
                        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                        boolean komorkaPrzyjecie = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
                        boolean wszedziePrzyjecie = DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE);
                        if (!komorkaPrzyjecie && !wszedziePrzyjecie)
                            throw new EdmException(sm.getString("BrakUprawnienDoPrzyjmowaniaPismaWdziale"));
                        OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);
                        journal = Journal.find(new Long(nameOfJournal.trim()));
                        if (journal.findDocumentEntry(document.getId()) != null)
                            throw new EdmException(sm.getString("PismoZnajdujeSieJuzWwybranymDzienniku"));
                        if (journal.getOwnerGuid() == null)
                            division = DSDivision.find(DSDivision.ROOT_GUID);
                        else
                            division = DSDivision.find(journal.getOwnerGuid());
                    }
                    catch (EdmException e)
                    {
                        addActionError(e.getMessage());
                        return;
                    }
                    finally
                    {
                        try { DSApi.close(); } catch (Exception e) { }
                    }

                    Integer sequenceId;
                    try
                    {
                        sequenceId = Journal.TX_newEntry2(journal.getId(), documentIds[i], entryDate);
                    }
                    catch (EdmException e)
                    {
                        addActionError(e.getMessage());
                        return;
                    }
                    try
                    {
                        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                        DSApi.context().begin();
                        OfficeDocument document = OfficeDocument.findOfficeDocument(documentIds[i]);
                        document.setAssignedDivision(division.getGuid());
                        addActionMessage(sm.getString(sm.getString("PismoZostaloPrzyjeteWdziennikuDzialu"))+" "+
                            division.getName()+" "+sm.getString("PodNumerem")+" "+sequenceId);
                        DSApi.context().commit();
                    }
                    catch (EdmException e)
                    {
                        DSApi.context().setRollbackOnly();
                        addActionError(e.getMessage());
                    }
                    finally
                    {
                        try { DSApi.close(); } catch (Exception e) { }
                    }
                    
                }
                //DSApi.context().commit();
            }
            catch (Exception e)
            {
                //DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
        
    }
            
            
    /**
     * Przepisanie zada� innego u�ytkownika na siebie.
     */
    private class AssignMe implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(sm.getString("NieWybranoZadan"));
                return;
            }
            
            try
            {
                DSApi.context().begin();
                DSUser user = null;
                if(nameOfSmallAssignment == null || nameOfSmallAssignment.length() < 1)
                {
                	user = DSApi.context().getDSUser();
                }
                else
                {
                	try
                	{
                		user = DSUser.findByUsername(nameOfSmallAssignment);
                	}
                	catch (UserNotFoundException e) 
					{
                		log.error("Nie znaleziono uzytkownika Agenta",e);
                		new UserNotFoundException(nameOfSmallAssignment);
					}
                }


                String plannedAssignment = "null/"+user.getName()+"/"+WorkflowUtils.iwfManualName()+"//"+"realizacja";
                
                
                
                WorkflowFactory.multiAssignment(activityIds,username, new String[] {plannedAssignment}, null, event);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class AddToCase implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
                addActionError(sm.getString("NieWybranoZadan"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                OfficeCase officeCase = OfficeCase.find(caseId);

                for (int i=0; i < activityIds.length; i++)
                {

                	Long documentId = WorkflowFactory.getDocumentId(activityIds[i]);
                    if (documentId == null)
                    {
                        event.getLog().warn(sm.getString("Zzadaniem")+" "+activityIds[i]+" "+"nie jest zwi?zany"+" " +
                            sm.getString("dokument")+" (ds_document_id)");
                        continue;
                    }

                    Document document = Document.find(documentId);

                    if (document instanceof OfficeDocument)
                    {
                        OfficeDocument doc = (OfficeDocument) document;

                        if (doc.getContainingCase() == null ||
                            !doc.getContainingCase().equals(officeCase))
                        {
                            doc.setContainingCase(officeCase,true);
                        }
                    }
                    TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
                }

                DSApi.context().commit();
                
             
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class RemoveWatches implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (urns == null || urns.length == 0)
            {
                addActionError(sm.getString("NieWybranoElementowDoUsuniecia"));
                return;
            }

            try
            {
                DSApi.context().begin();

                for (String urn : urns)
                {
                    DSApi.context().unwatch(URN.parse(urn));
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    private class RemoveOrders implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (activityIds == null || activityIds.length == 0)
            {
                addActionError(sm.getString("NieWybranoElementowDoUsuniecia"));
                return;
            }

            try
            {
                DSApi.context().begin();

                if (activityIds != null)
                {
                	for (int i = 0; i < activityIds.length; i++)
                	{     
                		//WorkflowActivity activity = WorkflowFactory.getWfActivity(activityIds[i]);
                		
                		Long documentId = WorkflowFactory.getDocumentId(activityIds[i]);
                		if (documentId == null)
                        {
                            event.getLog().warn(sm.getString("Zzadaniem")+" "+activityIds[i]+" "+"nie jest zwi?zany"+" " +
                                sm.getString("dokument")+" (ds_document_id)");
                            continue;
                        }
                		
                		Document document = Document.find(documentId);
                		if (document instanceof OfficeDocument) {
                        	//to normalnie (w summary action) jest wywo�ywane podczas zamykania sprawy
                        	WorkflowFactory.getInstance().manualFinish(activityIds[i], true);
                        } else {
                        	addActionError(sm.getString("ToNieJestDokumentKancelaryjny"));
                        }
                	}
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Klasa reprezentujaca zadanie CaseTask
     * @TODO - nie jest dobre ze ta klasa jest wewnatrz akcji -- 
     * Powinno to byc jakos ujednolicone z Taskiem
     * @author wkutyla
     *
     */
    public static class CaseTask
    {
        private Long caseId;
        private String caseOfficeId;
        private String caseStatus;
        private Date caseOpenDate;
        private Date caseFinishDate;
        private Integer caseDaysToFinish;

        public CaseTask(OfficeCase officeCase)
        {
            caseId = officeCase.getId();
            caseOfficeId = officeCase.getOfficeId();
            caseStatus = officeCase.getStatus().getName();
            caseOpenDate = officeCase.getOpenDate();
            caseFinishDate = officeCase.getFinishDate();
            caseDaysToFinish = new Integer(officeCase.getDaysToFinish());
        }

        public Long getCaseId()
        {
            return caseId;
        }

        public String getCaseOfficeId()
        {
            return caseOfficeId;
        }

        public String getCaseStatus()
        {
            return caseStatus;
        }

        public Date getCaseOpenDate()
        {
            return caseOpenDate;
        }

        public Date getCaseFinishDate()
        {
            return caseFinishDate;
        }

        public Integer getCaseDaysToFinish()
        {
            return caseDaysToFinish;
        }
    }

    private List<Task> updateWatches(DSUser user)
    {
        List<Task> results = new ArrayList<Task>();
        try
        {
            List<URN> urns = DSApi.context().watches(user);

            for (URN urn : urns)
            {
                Task task = new Task();
                task.setUrn(urn.toExternalForm());
                try
                {
                    if (urn instanceof DocumentURN)
                    {
                        Document document = Document.find(((DocumentURN) urn).getId());

                        task.setDocumentTask(true);
                        task.setLink(Links.getLink(document, "office", true));
                        task.setDocumentId(document.getId());
                        task.setDocumentFax("fax".equals(document.getSource()));
                        if (document instanceof OfficeDocument)
                        {
                            OfficeDocument doc = (OfficeDocument) document;
                            task.setDocumentOfficeNumber(doc.getOfficeNumber());
                            task.setDocumentOfficeNumberYear(doc.getOfficeNumberYear());
                            task.setDocumentSummary(doc.getSummary());
                            if (document.getType() == DocumentType.INCOMING)
                            {
                                InOfficeDocument indoc = (InOfficeDocument) doc;
                                task.setDocumentReferenceId(indoc.getReferenceId());
                                if (indoc.getSender() != null)
                                {
                                    task.setDocumentSender(indoc.getSender().getSummary());
                                }
                            }
                        }
                        else
                        {
                        	
                            task.setDescription("Dok. archiwalny ID "+document.getId());
                        }

                        results.add(task);
                    }
                    else if (urn instanceof WfActivityURN)
                    {
                    	if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
                    		continue;
                    	}
                        WfActivity wfa = WorkflowFactory.getInstance().getOldActivity(((WfActivityURN) urn).getKey());
                        Long id = WorkflowUtils.findParameterValue(wfa.container().process_context(), "ds_document_id", Long.class);
                        if (id == null)
                            throw new EntityNotFoundException(sm.getString("BrakIdentyfikatoraDokumentuWprocesie")+" "+wfa.container().key());
                        Document document = Document.find(id);

                        task.setLink(Links.getLink(document, "office", true));
                        task.setDocumentTask(true);
                        task.setDocumentId(document.getId());
                        task.setDocumentFax("fax".equals(document.getSource()));
                        if (document instanceof OfficeDocument)
                        {
                            OfficeDocument doc = (OfficeDocument) document;
                            task.setDocumentOfficeNumber(doc.getOfficeNumber());
                            task.setDocumentOfficeNumberYear(doc.getOfficeNumberYear());
                            task.setDocumentSummary(doc.getSummary());
                            if (document.getType() == DocumentType.INCOMING)
                            {
                                InOfficeDocument indoc = (InOfficeDocument) doc;
                                task.setDocumentReferenceId(indoc.getReferenceId());
                                if (indoc.getSender() != null)
                                {
                                    task.setDocumentSender(indoc.getSender().getSummary());
                                }
                            }
                        }
                        task.setWorkflowName(InternalWorkflowService.NAME);
                        task.setActivityKey(wfa.key());
                        task.setActivityId(ActivityId.make(task.getWorkflowName(), task.getActivityKey()));
                        task.setReceiveDate(wfa.last_state_time());
                        task.setDescription(wfa.name());
                        task.setSender(DSUser.safeToFirstnameLastname(WorkflowUtils.findParameterValue(wfa.process_context(), "ds_last_activity_user", String.class)));
                        task.setObjective(WorkflowUtils.findParameterValue(wfa.process_context(), "ds_objective", String.class));
                        task.setProcess(wfa.container().name());
                        // documentType
                        task.setPackageId(wfa.container().manager().package_id());
                        task.setProcessDefinitionId(wfa.container().manager().process_definition_id());

                        if (wfa.container() instanceof WfProcessImpl)
                        {
                            WfProcessImpl wfpi = (WfProcessImpl) wfa.container();
                            if (wfpi.getDeadlineTime() != null)
                            {
                                task.setDeadlineTime(wfpi.getDeadlineTime());
                                task.setDeadlineRemark(wfpi.getDeadlineRemark());
                                task.setDeadlineAuthor(DSUser.safeToFirstnameLastname(
                                    wfpi.getDeadlineAuthor()));

                                if (wfpi.getDeadlineTime().getTime() < System.currentTimeMillis())
                                {
                                    task.setPastDeadline(true);
                                }
                            }
                        }
/*
                        if (wfa.container() instanceof WfProcessImpl)
                        {
                            WfProcessImpl wfp = (WfProcessImpl) wfa.container();
                            if (wfp.getDeadlineTime() != null)
                            {
                                task.setDeadlineTime(wfp.getDeadlineTime());
                                task.setDeadlineRemark(wfp.getDeadlineRemark());
                                task.setDeadlineAuthor(DSUser.safeToFirstnameLastname(wfp.getDeadlineAuthor()));
                            }
                        }

*/

                        results.add(task);
                    }
                    else if (urn instanceof WfProcessURN)
                    {
                    	if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
                    		continue;
                    	}
                        WfProcess wfp = DSApi.context().getWfProcess((WfProcessURN) urn);
                        if(wfp == null)
                            throw new EntityNotFoundException(sm.getString("BrakProcesu"));
                        if (wfp.state().equals("closed.completed"))
                            continue;
                        Long id = WorkflowUtils.findParameterValue(wfp.process_context(), "ds_document_id", Long.class);
                        if (id == null)
                            throw new EntityNotFoundException(sm.getString("BrakIdentyfikatoraDokumentuWprocesie")+" "+wfp.key());
                        Document document = Document.find(id);

                        task.setLink(Links.getLink(document, "office", true,
                            new pair<String, String>("process", urn.toExternalForm())));
                        task.setDocumentTask(true);
                        task.setDocumentId(document.getId());
                        task.setDocumentFax("fax".equals(document.getSource()));
                        if (document instanceof OfficeDocument)
                        {
                            OfficeDocument doc = (OfficeDocument) document;
                            task.setDocumentOfficeNumber(doc.getOfficeNumber());
                            task.setDocumentOfficeNumberYear(doc.getOfficeNumberYear());
                            task.setDocumentSummary(doc.getSummary());
                            if (document.getType() == DocumentType.INCOMING)
                            {
                                InOfficeDocument indoc = (InOfficeDocument) doc;
                                task.setDocumentReferenceId(indoc.getReferenceId());
                                if (indoc.getSender() != null)
                                {
                                    task.setDocumentSender(indoc.getSender().getSummary());
                                }
                            }
                        }
                        task.setWorkflowName(InternalWorkflowService.NAME);
                        task.setProcess(wfp.name());
                        // documentType
                        task.setPackageId(wfp.manager().package_id());
                        task.setProcessDefinitionId(wfp.manager().process_definition_id());

                        if (wfp instanceof WfProcessImpl)
                        {
                        	
                            WfProcessImpl wfpi = (WfProcessImpl) wfp;
                            if (wfpi.getDeadlineTime() != null)
                            {
                                task.setDeadlineTime(wfpi.getDeadlineTime());
                                task.setDeadlineRemark(wfpi.getDeadlineRemark());
                                task.setDeadlineAuthor(DSUser.safeToFirstnameLastname(
                                    wfpi.getDeadlineAuthor()));

                                if (wfpi.getDeadlineTime().getTime() < System.currentTimeMillis())
                                {
                                    task.setPastDeadline(true);
                                }
                            }

                            WfActivity[] wfas = wfp.get_activities_in_state_array("open.running");
                            if (wfas.length > 0)
                            {
                                task.setObjective(WorkflowUtils.findParameterValue(wfas[0].process_context(), "ds_objective", String.class));
                                task.setReceiveDate(wfas[0].last_state_time());
                                task.setDescription(wfas[0].name());
                                task.setSender(DSUser.safeToFirstnameLastname(WorkflowUtils.findParameterValue(wfas[0].process_context(), "ds_last_activity_user", String.class)));
                            }
                        }
                        results.add(task);
                    }
                    else if (urn instanceof WorkflowActivityURN)
                    {
                    	String activityKey = ((WorkflowActivityURN)urn).getKey();
                    	String activityId = WorkflowFactory.activityIdOfWfNameAndKey("internal", activityKey);
                    	WorkflowActivity wfa = null;
                    	if (AvailabilityManager.isAvailable("tasklist.jbpm")) {
                    		try {
                    			wfa = WorkflowFactory.getWfActivity(activityId);
                    		} catch (Exception e){
                    			log.debug("watches: nie znaleziono zadania z kluczem: {}",activityKey);
                    			continue;
                    		}
                    	} else {
                    		wfa = WorkflowFactory.getWfActivity(activityId);
                    	}
                        Long id = wfa.getDocumentId();
                        if (id == null)
                            throw new EntityNotFoundException(sm.getString("BrakIdentyfikatoraDokumentuWprocesie")+" "+activityId);
                        Document document = Document.find(id);
                        task.setLink(Links.getLink(document, "office", true));
                        task.setDocumentTask(true);
                        task.setDocumentId(document.getId());
                        task.setDocumentFax("fax".equals(document.getSource()));
                        if (document instanceof OfficeDocument)
                        {
                            OfficeDocument doc = (OfficeDocument) document;
                            task.setDocumentOfficeNumber(doc.getOfficeNumber());
                            task.setDocumentOfficeNumberYear(doc.getOfficeNumberYear());
                            task.setDocumentSummary(doc.getSummary());
                            if (document.getType() == DocumentType.INCOMING)
                            {
                                InOfficeDocument indoc = (InOfficeDocument) doc;
                                task.setDocumentReferenceId(indoc.getReferenceId());
                                if (indoc.getSender() != null)
                                {
                                    task.setDocumentSender(indoc.getSender().getSummary());
                                }
                            }
                        }
                        task.setWorkflowName(InternalWorkflowService.NAME);
                        task.setActivityKey(WorkflowFactory.getInstance().keyOfId(wfa.getTaskFullId()));
                        //task.setActivityId(ActivityId.make(task.getWorkflowName(), task.getActivityKey()));
                        task.setReceiveDate(wfa.getReceiveDate());
                        task.setDescription(wfa.getProcessDesc());
                        task.setSender(DSUser.safeToFirstnameLastname(wfa.getLastUser()));
                        task.setObjective(wfa.getObjective());
                        task.setProcess(wfa.getProcessKind());
                        // documentType
                        //task.setPackageId(wfa.container().manager().package_id());
                        //task.setProcessDefinitionId(wfa.container().manager().process_definition_id());
                        if (wfa.getDeadlineTime() != null) {
                        	task.setDeadlineTime(wfa.getDeadlineTime());
                        	task.setDeadlineRemark(wfa.getDeadlineRemark());
                        	String deadlineAuthor = wfa.getDeadlineAuthor();
                        	if (deadlineAuthor != null) {
                        		task.setDeadlineAuthor(DSUser.safeToFirstnameLastname(deadlineAuthor));
                        	}
                        	if (wfa.getDeadlineTime().getTime() < System.currentTimeMillis())
                        	{
                        		task.setPastDeadline(true);
                        	}
                        }
                     results.add(task);
                    }
                }
                catch (EntityNotFoundException e)
                {
                    log.warn(e.getMessage()+", urn="+urn, e);
                }
            }
        }
        catch (EdmException e)
        {
            addActionError(e.getMessage());
        }

        return results;
    }
    
    private class AddLabel implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (documentIds == null || documentIds.length == 0)
        	{
                addActionError(sm.getString("NieWybranoDokumentow"));
                return;
        	}
            try
            {
                LabelsManager.addLabel(documentIds, labelId, DSApi.context().getPrincipalName());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    private class RemoveLabelFromView implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (removeLabelId != null || removeLabelId > 0)
        	{
        		viewedLabels = (Collection<Label>)ServletActionContext.getRequest().getSession().getAttribute(tab+"viewedLabels");
        		Label toRemove = null;
        		for(Label l:viewedLabels)
        		{
        			if(l.getId().equals(removeLabelId)) toRemove = l;
        		}
        		viewedLabels.remove(toRemove);
        		removeLabelId = null;
        		viewedLabelId = 0L;
        		if(viewedLabels.size()==0) viewedLabelId = -2L;
        		ServletActionContext.getRequest().getSession().setAttribute(tab+"viewedLabels", viewedLabels);
        	}
        }
    }
    
    private class ManualPushToCoor implements ActionListener
    {
		public void actionPerformed(ActionEvent event) 
		{
			
			if ((activityIds == null || activityIds.length == 0) && (actionCaseIds == null || actionCaseIds.length == 0))
                addActionError(sm.getString("NieWybranoZadan"));
			
			try
			{
			
				for(String activityId : activityIds)
				{
					WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);
					if (!WorkflowFactory.getInstance().isManual(activity))
					{
	                    addActionError(sm.getString("WymaganyJestProcesDekretacjiRecznej."));
	                    continue;
					}
					
					OfficeDocument document = OfficeDocument.findOfficeDocument(activity.getDocumentId());
					if(document == null || document.getDocumentKind() == null)
					{
						addActionError(sm.getString("NieMoznaUstalicKoordynatora"));
						continue;
					}
					ProcessCoordinator pc =document.getDocumentKind().logic().getProcessCoordinator(document);
					if(pc == null)
					{
						addActionError(sm.getString("NieMoznaUstalicKoordynatora")+" : "+document.getOfficeNumber());
						continue;
					}
					
					try
					{
					
						DSApi.context().begin();
						WorkflowFactory.simpleAssignment(activityId, pc.getGuid(), pc.getUsername(), null, null, null, null, event);
			                       
		                TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());
	
		                DSApi.context().commit();
	                
					}
					catch (EdmException e) 
					{
						DSApi.context()._rollback();
					}
				}
			}
			catch (Exception e) 
			{
				event.getLog().error(e.getMessage(), e);
			}
		}
    }

    
    
    private class FilterColumn
    {
    	private String name;
    	private String title;
    	
		public FilterColumn(String name, String title)
		{
			this.name = name;
			this.title = title;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public String getOrderTab()
    {
        return orderTab;
    }

    public void setOrderTab(String orderTab)
    {
        this.orderTab = orderTab;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public List getTasks()
    {
        return tasks;
    }

    public Tabs getTabs()
    {
        return tabs;
    }

    public Tabs getOrderTabs()
    {
        return orderTabs;
    }

    public List getUsers()
    {
        return users;
    }

    public List getColumns()
    {
        return columns;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public Collection getCases()
    {
        return cases;
    }

    public void setSearchOfficeNumber(Integer searchOfficeNumber)
    {
        this.searchOfficeNumber = searchOfficeNumber;
    }

    public void setSearchOfficeId(String searchOfficeId)
    {
        this.searchOfficeId = searchOfficeId;
    }

    public void setActivityIds(String[] activityIds)
    {
        this.activityIds = activityIds;
    }

    public boolean isActionManualFinish()
    {
        return actionManualFinish;
    }

    public boolean isActionAssignOfficeNumber()
    {
        return actionAssignOfficeNumber;
    }

    public boolean isActionAttachmentsAsPdf()
    {
        return actionAttachmentsAsPdf;
    }

    public boolean isActionAddToCase()
    {
        return actionAddToCase;
    }

    public boolean isActionAssignMe()
    {
        return actionAssignMe;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }
    /*
    public void setCaseInOfficeFinishDate(String caseInOfficeFinishDate)
    {
        this.caseInOfficeFinishDate = caseInOfficeFinishDate;
    }
    */
    public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }

    public void setActionCaseIds(Long[] actionCaseIds)
    {
        this.actionCaseIds = actionCaseIds;
    }

    public Long[] getActionCaseIds()
    {
        return actionCaseIds;
    }

    public boolean isActionMultiAssignment()
    {
        return actionMultiAssignment;
    }

    public boolean isActionMultiBoxing()
    {
        return actionMultiBoxing;
    }

    public List<Task> getWatches()
    {
        return watches;
    }

    public void setUrns(String[] urns)
    {
        this.urns = urns;
    }
    
    public Pager getPager()
    {
        return pager;
    }
    
    public void setOffset(int offset)
    {
        this.offset = offset;
    }
   
    @Deprecated
    public boolean isSimpleTaskList()
    {
        return false;
    }
    
    @Deprecated
    public void setSimpleTaskList(boolean simpleTaskList)
    {
        //
    }
    
    @Deprecated
    public boolean isFullTaskList()
    {
        return false;
    }
    
    @Deprecated
    public void setFullTaskList(boolean fullTaskList)
    {
        //
    }

    public boolean getAddToTask()
    {
        return addToTask;
    }

    public void setAddToTask(boolean addToTask)
    {
        this.addToTask = addToTask;
    }

    public String getCaseName()
    {
        return caseName;
    }

    public void setCaseName(String caseName)
    {
        this.caseName = caseName;
    }

//
//    public String getAddToReceiver()
//    {
//        return addToReceiver;
//    }
//
//    public void setAddToReceiver(String addToReceiver)
//    {
//        this.addToReceiver = addToReceiver;
//    }
    
    public List<DocumentKind> getDockindNames()
    {
        return dockindNames;
    }

    public void setFilterDockindNames(String arg)
    {
        this.filterDockindNames = arg;
    }
    
    public String getFilterDockindNames()
    {
        return filterDockindNames;
    }
    
    
    public List<SmallAssignmentBean> getSmallAssignments()
    {
        return smallAssignments;
    }
    
    public List<DepartmentEnrollBean> getIncomingDepartments()
    {
        return incomingDepartments;
    }
    
    public void setNameOfSmallAssignment(String name)
    {
        nameOfSmallAssignment = name;
    }
    
    public void setNameOfJournal(String name)
    {
        nameOfJournal = name;
    }

    public boolean isActionReplyUnnecessary()
    {
        return actionReplyUnnecessary;
    }
    
    public void setActionReplyUnnecessary(boolean actionReplyUnnecessary)
    {
        this.actionReplyUnnecessary = actionReplyUnnecessary;
    }
    
    public boolean isActionSendToEva()
    {
    	return actionSendToEva;
    }
    
    public boolean isActionMultiDepartmentEnroll()
    {
        return actionDepartmentEnroll;
    }

    public boolean isMultiDepartmentEnroll()
    {
        return multiDepartmentEnroll;
    }

    public void setMultiDepartmentEnroll(boolean multiDepartmentEnroll)
    {
        this.multiDepartmentEnroll = multiDepartmentEnroll;
    }

	public String getFilterBy() {
		return filterBy;
	}

	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
		if(filterName == null || (filterName != null && filterName.equalsIgnoreCase("null")))
		{
			this.filterName = "";
		}
	}

	public List<FilterColumn> getfilterColumns() {
		return filterColumns;
	}

	public void setfilterColumns(List<FilterColumn> filterColumns) {
		this.filterColumns = filterColumns;
	}

	public Integer getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(Integer taskCount) {
		this.taskCount = taskCount;
	}

	public Boolean getWithBackup() {
		return withBackup;
	}

	public void setWithBackup(Boolean withBackup) {
		this.withBackup = withBackup;
	}

	public boolean isOnTaskList() {
		return onTaskList;
	}

	public void setOnTaskList(boolean onTaskList) {
		this.onTaskList = onTaskList;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public Long getLabelId() {
		return labelId;
	}

	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}

	public Long getViewedLabelId()
	{
		return viewedLabelId;
	}

	public void setViewedLabelId(Long viewedLabelId)
	{
		this.viewedLabelId = viewedLabelId;
	}

	public List<Label> getAvailableForAddLabels() {
		return availableForAddLabels;
	}

	public void setAvailableForAddLabels(List<Label> availableForAddLabels) {
		this.availableForAddLabels = availableForAddLabels;
	}

	public List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> getEnableAcceptances() {
		return enableAcceptances;
	}

	public void setEnableAcceptances(
			List<pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance> enableAcceptances) {
		this.enableAcceptances = enableAcceptances;
	}

	public String getAcceptanceCn() {
		return acceptanceCn;
	}

	public void setAcceptanceCn(String acceptanceCn) {
		this.acceptanceCn = acceptanceCn;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public Collection<Label> getViewedLabels() {
		return viewedLabels;
	}

	public void setViewedLabels(Collection<Label> viewedLabels) {
		this.viewedLabels = viewedLabels;
	}

	public Long getRemoveLabelId() {
		return removeLabelId;
	}

	public void setRemoveLabelId(Long removeLabelId) {
		this.removeLabelId = removeLabelId;
	}

	public List<Label> getUserFlags_label() {
		return userFlags_label;
	}

	public void setUserFlags_label(List<Label> userFlags_label) {
		this.userFlags_label = userFlags_label;
	}

	public List<Label> getSystemFlags_label() {
		return systemFlags_label;
	}

	public void setSystemFlags_label(List<Label> systemFlags_label) {
		this.systemFlags_label = systemFlags_label;
	}

	public List<Label> getLabels_label() {
		return labels_label;
	}

	public void setLabels_label(List<Label> labels_label) {
		this.labels_label = labels_label;
	}

	public void setShowLabels(boolean showLabels) {
		this.showLabels = showLabels;
	}

	public boolean isShowLabels() {
		return showLabels;
	}

	public List<Label> getSystemLabels_label() {
		return systemLabels_label;
	}

	public void setSystemLabels_label(List<Label> systemLabels_label) {
		this.systemLabels_label = systemLabels_label;
	}

    public Date getCaseDeadlineDate()
    {
        return caseDeadlineDate;
    }

    public void setCaseDeadlineDate(Date caseDeadlineDate)
    {
        this.caseDeadlineDate = caseDeadlineDate;
    }

    /**
	 * Sortowanie tasklisty po niektorych parametrach
	 * @param tasks
	 * @return
	 */
	public List<Task> sortTask(List<Task> tasks)
	{
		if (log.isTraceEnabled())
		{
			log.trace("sortowanie tasklisty");
			log.trace("sortField={}",sortField);
			log.trace("ascending={}",ascending);
		}
		final NumberFormat millisFormat = NumberFormat.getIntegerInstance();
        millisFormat.setMinimumIntegerDigits((""+Long.MAX_VALUE).length());
        millisFormat.setGroupingUsed(false);
        millisFormat.setParseIntegerOnly(true);
        // napis o najmniejszej mo�liwej warto?ci u�ywany zamiast
        // napisu pustego w sortowaniu leksykalnym
        final String MIN_STR = String.valueOf((char) 0);
		if (tasks != null && tasks.size() > 1)
        {
            List<String> sortKeys = new ArrayList<String>(tasks.size());

            for (Iterator iter=tasks.iterator(); iter.hasNext(); )
            {
                TaskSnapshot task = (TaskSnapshot) iter.next();
                String key;
                String onKey = task.getDocumentOfficeNumber() != null ?
                        millisFormat.format(task.getDocumentOfficeNumber()) :
                        millisFormat.format(0);

                // tylko kilka "if-�w", bo sortowanie po pozosta�ych polach odbywa si� w zapytaniu sql-owym         
                if ("documentSender".equals(sortField))
                {
                    key = task.getDocumentSender() != null ?
                        task.getDocumentSender().toUpperCase() : MIN_STR;
                }
                else if ("sender".equals(sortField))
                {
                    key = task.getSender() != null ?
                        task.getSender().toUpperCase() : MIN_STR;
                }
                else if ("receiver".equals(sortField))
                {
                    key = task.getReceiver() != null ?
                        task.getReceiver().toUpperCase() : MIN_STR;
                }
                else if ("author".equals(sortField))
                {
                    key = task.getAuthor() != null ?
                        task.getAuthor().toUpperCase() : MIN_STR;
                }
                else if ("orderStatus".equals(sortField))
                {
                    key = task.getOrderStatus() != null ?
                        task.getOrderStatus().toUpperCase() : MIN_STR;
                }
                else if ("clerk".equals(sortField))
                {
                    key = task.getClerk() != null ?
                        task.getClerk().toUpperCase() : MIN_STR;  
                }
                else if ("flags".equals(sortField)) 
                {                            	
                	StringBuilder sb = new StringBuilder();
                	if (task.getFlags() != null) {
                		for(int i=0; i < task.getFlags().size(); i++)
                			sb.append((((List<FlagBean>)task.getFlags()).get(i).getC()));
                		key = sb.toString();
                	}
                	else
                		key = MIN_STR;
                }
                else if ("userFlags".equals(sortField)) 
                {                            	
                	StringBuilder sb = new StringBuilder();
                	if (task.getUserFlags() != null) {
                		for(int i=0; i < task.getUserFlags().size(); i++)
                			sb.append((((List<FlagBean>)task.getUserFlags()).get(i).getC()));
                		key = sb.toString();
                	}
                	else
                		key = MIN_STR;
                }
                else // documentOfficeNumber
                {
                    key = onKey;
                }

                // na ko�cu klucza zawsze numer kancelaryjny, poniewa�
                // jest to poboczny klucz sortowania
                key += ("<>" + onKey);

                sortKeys.add(key);
            }

            Task[] taskArray = (Task[]) tasks.toArray(new Task[tasks.size()]);
            std.sort.qsort(
                (String[]) sortKeys.toArray(new String[sortKeys.size()]),
                taskArray,
                ascending);

            return Arrays.asList(taskArray);
        }
		else
		{
			return tasks;
		}
	}
}
