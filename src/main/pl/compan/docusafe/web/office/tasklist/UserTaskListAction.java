package pl.compan.docusafe.web.office.tasklist;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CurrentUserTaskListAction.java,v 1.7 2010/05/17 07:32:16 mariuszk Exp $
 */
public class UserTaskListAction extends TaskListAction
{
    protected DSUser getDSUser() throws EdmException
    {
        return DSApi.context().getDSUser();
    }

    //po co tutaj to �adowa� ???
    protected List getDSUserList() throws EdmException {
        return null;//UserFactory.getInstance().getCanAssignUsers();
     }

    public boolean isOwnTasks() 
    {
        return true;
    }

    public String getBaseLink()
    {
        return "/office/tasklist/user-task-list.action";
    }
    

	public void setTab(String tab) {
		super.setTab(tab);
		
	}

	@Override
	public String getListName() {
		return "user-task-list";
	}

	@Override
	protected DSDivision getDSDivision() throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}
}
