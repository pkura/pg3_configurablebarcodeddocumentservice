package pl.compan.docusafe.web.office.tasklist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.PlannedAssignment;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;

/* User: Administrator, Date: 2005-11-14 14:12:04 */

/**
 * Dekretacja tylko do jednej osoby od razu ze strony
 * Przy u�ytkownikach checkboksy, wyb�r typu dekretacji.
 *
 * Chodzenie po dzia�ach, wyb�r konkretnego u�ytkownika i
 * rodzaju dekretacji.
 * Jednak planowanie dekretacji, jak w zak�adce "Dekretacja"
 * Mo�liwo�� zaplanowania tylko jednej dekretacji.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MultiAssignmentAction.java,v 1.45 2009/02/03 14:27:46 pecet1 Exp $
 */
public class MultiNoteAction extends EventActionSupport
{
	private Long[] documentIds;
    private String[] activityIds;
    private String note;
    private String search;
    
    private String divisionGuid;
    private String treeHtml;
    private String divisionPrettyPath;
    private DSDivision targetDivision;
    private boolean canAssign;
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
    
    /**
     * Zaznaczone dekretacje.
     */
    private String tab;

	private List<DSUser> allUsers;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
		registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

	/**
	 * @return the allUsers
	 */
	public List<DSUser> getAllUsers() {
		return allUsers;
	}

	/**
	 * @param allUsers the allUsers to set
	 */
	public void setAllUsers(List<DSUser> allUsers) {
		this.allUsers = allUsers;
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            if (documentIds == null || documentIds.length == 0)
            {
                addActionError(smG.getString("NieWybranoDokumentow"));
                return;
            }     
        }
    }
    
    private class Add implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
	       	 try
	         {
	            if (documentIds == null || documentIds.length == 0)
	            {
	                 addActionError(smG.getString("NieWybranoDokumentow"));
	                 return;
	            }     
	            if (note == null)
	            {
	                throw new EdmException("Nie wpisano tre�ci notatki");
	            }
	                
	
	            DSApi.context().begin();
	            for(Long docId : documentIds)
	            {		            
		            OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
		            doc.addRemark(new Remark(StringUtils.left(note, 4000), DSApi.context().getPrincipalName()));
	            }
	            DSApi.context().commit();
	            addActionMessage("Notatka zosta�a dodana do dokumentu/�w od ID: " + Arrays.toString(documentIds));
 
	        }
	        catch (EdmException e)
	        {
	            DSApi.context().setRollbackOnly();
	            addActionError(e.getMessage());
	        }
        }
    }

  

    
    

    public String[] getActivityIds()
    {
        return activityIds;
    }

    public void setActivityIds(String[] activityIds)
    {
        this.activityIds = activityIds;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }


    public String getDivisionPrettyPath()
    {
        return divisionPrettyPath;
    }

    public DSDivision getTargetDivision()
    {
        return targetDivision;
    }


    public boolean isCanAssign()
    {
        return canAssign;
    }


    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}
	
	public Long[] getDocumentIds()
    {
        return documentIds;
    }
	
	public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }
	   
}
