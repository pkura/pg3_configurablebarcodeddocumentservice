package pl.compan.docusafe.web.office;


import java.awt.Font;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.general.DefaultPieDataset;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ShowBudgetAction extends EventActionSupport
{
	private static final Log log = LogFactory.getLog(ShowBudgetAction.class);
	private int mpkId;
	
	private Map <Integer, String> mpkList;
	
	/** suma zamowien */
	private BigDecimal sumAllOrders;
	
	/** budzet danego centrum */
	private BigDecimal mpkBudget;
	
	/** kwota pozostala do rozdysponowania */
	private BigDecimal budgetLeft;
	
	/** procent kwoty do wykorzystania z budzetu */
	private String budgetLeftPercent;
	
	/** id dokuemntu */
	private int documentId;
	
	/** suma kwot zamowien z rodzieleniem na centra */
	private Map  <String, BigDecimal> sumAmounts;
	
	/** kwota biezacych zamowien */
	private BigDecimal ordersAmountSum;
	
	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);		
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {       	
        	mpkList = new HashMap <Integer, String> ();        	

        	try {
				for (CentrumKosztow centrum : CentrumKosztow.findCentrum()) {
					mpkList.put(centrum.getId(), centrum.getName());
				}
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}			
			
			initMpkBudget();
			initSumAllOrdersForMpk();
			initSumAccountAmounts();
			initDocumentOrdersAmountSum() ;
			
			if (sumAllOrders == null)
				sumAllOrders = new BigDecimal(0);
			if (mpkBudget == null)
				mpkBudget = new BigDecimal(0);
			if (budgetLeft == null)
				budgetLeft = new BigDecimal(0);
			if (ordersAmountSum == null)
				ordersAmountSum = new BigDecimal(0);

			budgetLeft = mpkBudget.subtract(sumAllOrders);
			if (!budgetLeft.equals(BigDecimal.ZERO) && !mpkBudget.equals(BigDecimal.ZERO))
				try {
					budgetLeftPercent = new String((budgetLeft.divide(mpkBudget, 2, BigDecimal.ROUND_DOWN).scaleByPowerOfTen(2)).toString().concat("%"));
				} catch (Exception e) {
					budgetLeftPercent = "100%";
				}
			generateImage();
        }
    }
    
    private void initSumAllOrdersForMpk() {
    	PreparedStatement ps = null;
    	ResultSet rs = null;    	
    	try {
			ps = DSApi.context().prepareStatement("SELECT SUM(kwota) AS suma FROM dsg_santander WHERE mpk=?;");
			ps.setInt(1, mpkId);
			rs = ps.executeQuery();
			if(rs.next()) {
				sumAllOrders = rs.getBigDecimal("suma");
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		finally {
			DSApi.context().closeStatement(ps);
		}
    }
    
    private void initDocumentOrdersAmountSum() {
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("SELECT kwota_pln");
    	sbQuery.append(" FROM dsg_santander_multiple_value");
    	sbQuery.append(" WHERE smv.DOCUMENT_ID = ?");
    	PreparedStatement ps = null;
    	ResultSet rs = null;    	
    	try {
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			ps.setLong(1, documentId);
			rs = ps.executeQuery();
			if(rs.next()) {
				ordersAmountSum = rs.getBigDecimal("kwota_pln");
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
    }
    
    private void initMpkBudget() {
    	PreparedStatement ps = null;
    	ResultSet rs = null;    	
    	try {
			ps = DSApi.context().prepareStatement("SELECT budget FROM dsg_budget WHERE centrum_kosztow_id=?;");
			ps.setInt(1, mpkId);
			rs = ps.executeQuery();
			if(rs.next()) {
				mpkBudget = rs.getBigDecimal("budget");
			}
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
    }
    
    private void initSumAccountAmounts() {
    	sumAmounts = new HashMap<String, BigDecimal> ();
    	StringBuilder sbQuery = new StringBuilder();
    	sbQuery.append("SELECT acc.account_name, SUM(item.amount) AS suma");
    	sbQuery.append(" FROM dsg_santander_multiple_value smv");
    	sbQuery.append(" JOIN dsg_item_order item ON smv.FIELD_VAL = item.id");
    	sbQuery.append(" JOIN dsg_account_number acc ON item.productGroup = acc.id");
    	sbQuery.append(" WHERE smv.DOCUMENT_ID = ?");
    	sbQuery.append(" GROUP BY account_name");
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			ps.setLong(1, documentId);
			rs = ps.executeQuery();
			while(rs.next()) {
				sumAmounts.put(rs.getString("account_name"), rs.getBigDecimal("suma"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
    }

	private void generateImage() {
    	int width = 460;
    	int height = 345;    	
    	DefaultPieDataset dataset = new DefaultPieDataset();
    	dataset.setValue("Pozosta�o", budgetLeft);
    	dataset.setValue("Wykorzystane", sumAllOrders);
    	dataset.setValue("Obecne", ordersAmountSum);
    	JFreeChart chart = ChartFactory.createPieChart3D("Diagram wykorzystania bud�etu", dataset, true, true, false);
    	chart.setBackgroundImageAlpha(0.3f);
    	PiePlot3D plot = (PiePlot3D) chart.getPlot();
    	plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
    	plot.setCircular(false);
    	plot.setLabelGap(0.02);
    	plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}"));

    	ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
    	try {
			String filename = ServletUtilities.saveChartAsJPEG(chart,
					width,
					height,
					info,
					ServletActionContext.getRequest().getSession()
			);
			ServletActionContext.getRequest().setAttribute("filename",
					 "/jfree/DisplayChart?filename=".concat(filename));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
    }
	
    public Map<String, BigDecimal> getSumAmounts() {
		return sumAmounts;
	}

	public int getMpkId() {
		return mpkId;
	}

	public void setMpkId(int mpkId) {
		this.mpkId = mpkId;
	}

	public Map<Integer, String> getMpkList() {
		return mpkList;
	}
	
	public BigDecimal getSumAllOrders() {
		return sumAllOrders;
	}

	public BigDecimal getBudgetLeft() {
		return budgetLeft;
	}

	public String getBudgetLeftPercent() {
		return budgetLeftPercent;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	
	public BigDecimal getMpkBudget() {
		return mpkBudget;
	}

	public BigDecimal getOrdersAmountSum() {
		return ordersAmountSum;
	}
}
