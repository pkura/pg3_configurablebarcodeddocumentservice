package pl.compan.docusafe.web.office;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



public class PackageInNrAction extends PackageBaseAction
{
	private static final Log log = LogFactory.getLog(PackageInAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private String[] numery;
    private FormFile file;
    private static final String EV_ADD = "Add";
    

	protected void setup() 
	{		
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
        append(OpenHibernateSession.INSTANCE).
        append(new Validation()).
        append(EV_ADD, new Add()).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddFromXls").
        append(OpenHibernateSession.INSTANCE).
        append(new AddFromXls()).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        
	}
	
	private class Validation implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {		
			boolean validate = true;
			if (lokalizacja == null) {
				validate = false;
				event.addActionError(sm.getString("PoleLokalizacjaNieMozeBycPuste"));
			} else 	if (typDokumentu == null) {
				validate = false;
				event.addActionError(sm.getString("PoleTypDokumentowNieMozeBycPuste"));
			} else if (numery == null) {
				validate = false;
				event.addActionError(sm.getString("NieWprowadzonoZadnegoNumeruDokumentu"));
			}
			if (validate == false)
				event.skip(EV_ADD);
        }
    }
	
	private class FillForm implements ActionListener
    {

		public void actionPerformed(ActionEvent event)
        {
			try
			{
				fillForm();
			}
			catch (Exception e) 
			{
				log.error("", e);
				addActionError(e.getMessage());
			}
        }
    }
	
	/**
	 * Dodanie z XLSa 
	 * FIXME - troche krzywe - najpierw powinno sie wszystko posprawdzac
	 * dopiero potem otwierac transakcje
	 * @author wkutyla
	 *
	 */
	public class AddFromXls implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
			
        }
    }

	
    private class Add implements ActionListener
    {

		public void actionPerformed(ActionEvent event)
        {		
	        	
	    } 
    }
	
  
}
