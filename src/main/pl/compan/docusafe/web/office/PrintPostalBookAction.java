package pl.compan.docusafe.web.office;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintPostalBookAction.java,v 1.6 2006/02/20 15:42:41 lk Exp $
 */
public class PrintPostalBookAction extends EventActionSupport
{
    private Long journalId;
    private String date;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File temp = null;

            try
            {
                Journal journal = Journal.find(journalId);
                if (!Journal.OUTGOING.equals(journal.getJournalType()))
                    throw new EdmException("Spodziewano si� dziennika pism wychodz�cych");

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 12);
                Font headingFont = new Font(baseFont, 14);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                pdfDoc.open();

                Table table = new Table(13);
                table.setWidths(new int[] { 4,  13,  16,  5, 5,  5, 5,  5, 5,  8,  15,  8, 6 });
                table.setWidth(100);

                Cell c;

                // nag��wki
                c = new Cell(new Phrase("Lp", font));
                table.addCell(c);

                c = new Cell(new Phrase("ADRESAT", font));
                table.addCell(c);

                c = new Cell(new Phrase("Miejsce przeznaczenia", font));
                table.addCell(c);

                c = new Cell(new Phrase("Warto��", font));
                c.setColspan(2);
                table.addCell(c);

                c = new Cell(new Phrase("Masa", font));
                c.setColspan(2);
                table.addCell(c);

                c = new Cell(new Phrase("Kwota pobrania", font));
                c.setColspan(2);
                table.addCell(c);

                c = new Cell(new Phrase("Uwagi", font));
                table.addCell(c);

                c = new Cell(new Phrase("Nr nadawczy", font));
                table.addCell(c);

                c = new Cell(new Phrase("Op�ata", font));
                c.setColspan(2);
                table.addCell(c);

                table.addCell(""); // Lp
                table.addCell(""); // Adresat
                table.addCell(""); // Miejsce przeznaczenia
                table.addCell(new Phrase("z�", font)); // warto��
                table.addCell(new Phrase("gr", font));
                table.addCell(new Phrase("kg", font)); // masa
                table.addCell(new Phrase("g", font));
                table.addCell(new Phrase("z�", font)); // kwota pobrania
                table.addCell(new Phrase("gr", font));
                table.addCell(""); // uwagi
                table.addCell(""); // nr nadawczy
                table.addCell(new Phrase("z�", font)); // op�ata
                table.addCell(new Phrase("gr", font));


                JournalEntry[] entries = journal.getEntries(DateUtils.nullSafeParseJsDate(date));
                for (int i=0; i < entries.length; i++)
                {
                    final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(entries[i].getDocumentId());

                    table.addCell(new Phrase(""+(i+1), font));
                    if (document.getRecipients().size() > 0)
                    {
                        table.addCell(new Phrase(((Recipient) document.getRecipients().get(0)).getSummary(), font));
                    }
                    else
                    {
                        table.addCell("");
                    }

                    table.addCell(""); // Miejsce przeznaczenia
                    table.addCell(new Phrase("", font)); // warto��
                    table.addCell(new Phrase("", font));
                    table.addCell(new Phrase("", font)); // masa
                    table.addCell(new Phrase("", font));
                    table.addCell(new Phrase("", font)); // kwota pobrania
                    table.addCell(new Phrase("", font));
                    table.addCell(""); // uwagi
                    table.addCell(""); // nr nadawczy
                    table.addCell(new Phrase("", font)); // op�ata
                    table.addCell(new Phrase("", font));
                }


                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (IOException e)
            {
                addActionError(e.getMessage());
                event.setResult(ERROR);
            }
            catch (DocumentException e)
            {
                addActionError(e.getMessage());
                event.setResult(ERROR);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                event.setResult(ERROR);
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("rozmiar pliku "+temp+": "+temp.length());
                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    public Long getJournalId()
    {
        return journalId;
    }

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
}
