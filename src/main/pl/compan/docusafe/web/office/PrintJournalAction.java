package pl.compan.docusafe.web.office;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintJournalAction.java,v 1.30 2008/10/06 11:01:12 pecet4 Exp $
 */
public class PrintJournalAction extends EventProcessingAction
{
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(PrintJournalAction.class.getPackage().getName(),null);

    public static final String FORWARD = "office/print-journal";

    public static final String CODENAME_MAIN_INCOMING = "mainIncoming";
    public static final String CODENAME_MAIN_OUTGOING = "mainOutgoing";

    /**
     * Format daty u�ywany przy generowaniu pliku PDF.
     */
    public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    protected void setup()
    {
        registerListener(DEFAULT_ACTION, new GeneratePdf());
    }

    private class GeneratePdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long journalId = (Long) event.getDynaForm().get("journalId");
            String sDate = (String) event.getDynaForm().get("date");

            if (sDate == null)
                event.getErrors().add(sm.getString("viewJournal.invalidDate"));

            Date date = DateUtils.nullSafeParseJsDate(sDate);

            if (date == null)
                event.getErrors().add(sm.getString("viewJournal.invalidDate"));

            if (event.getErrors().size() > 0)
                return;

            DSContext ctx = null;
            File temp = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                Journal journal = Journal.find(journalId);
                DSDivision division = journal.getOwnerGuid() != null ?
                    DSDivision.find(journal.getOwnerGuid()) : null;

                // itext (pdf)

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 12);

                if (Journal.INCOMING.equals(journal.getJournalType()))
                {
                    JournalEntry[] entries = journal.getEntries(DateUtils.nullSafeParseJsDate(sDate));

                    temp = File.createTempFile("docusafe_", "_tmp");
                    com.lowagie.text.Document pdfDoc =
                        new com.lowagie.text.Document(PageSize.A4.rotate());
                    PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                    pdfDoc.open();

                    pdfDoc.add(new Phrase(journal.getSummary(), font));

                    int[] widths;
                    if (journal.isMain())
                        widths = (new int[] { 8, 16, 20, 12, 12, 16, 16 });
                    else
                        widths = (new int[] { 4, 6, 16, 18, 12, 12, 16, 16 });

                    PdfPTable table = new PdfPTable(widths.length);
                    table.setWidths(widths);
                    table.setWidthPercentage(100);

                    table.addCell(new Phrase(sm.getString("NrKO"), font));
                    if (!journal.isMain())
                        table.addCell(new Phrase(sm.getString("NrKolejny"), font));
                    table.addCell(new Phrase(sm.getString("DataOtrzymania"), font));
                    table.addCell(new Phrase(sm.getString("ZnakDataKorespondencji"), font));
                    table.addCell(new Phrase(sm.getString("Nadawca"), font));
                    table.addCell(new Phrase(sm.getString("Odbiorca"), font));
                    table.addCell(new Phrase(sm.getString("Opis"), font));
                    table.addCell(new Phrase(sm.getString("Uwagi"), font));

                    pdfDoc.add(table);

                    table = new PdfPTable(widths.length);
                    table.setWidths(widths);
                    table.setWidthPercentage(100);

                    for (int i=0; i < entries.length; i++)
                    {
                        final InOfficeDocument document = InOfficeDocument.findInOfficeDocument(entries[i].getDocumentId());

                        table.addCell(new Phrase(String.valueOf(document.getFormattedOfficeNumber()), font));

                        // numer porz�dkowy pisma w dzienniku
                        if (!journal.isMain())
                            table.addCell(new Phrase(String.valueOf(entries[i].getSequenceId()), font));

                        // data otrzymania
                        if (document.getIncomingDate() != null)
                        {
                            synchronized (dateFormat)
                            {
                                table.addCell(
                                    new Phrase(dateFormat.format(document.getIncomingDate()), font));
                            }
                        }
                        else
                        {
                            table.addCell(new Phrase("", font));
                        }

                        StringBuilder znak = new StringBuilder();

                        // data pisma
                        if (document.getDocumentDate() != null)
                        {
                            synchronized (dateFormat)
                            {
                                znak.append(dateFormat.format(document.getDocumentDate()));
                            }
                        }

                        if (document.getReferenceId() != null)
                        {
                            znak.append("\n"+document.getReferenceId());
                        }

                        table.addCell(new Phrase(znak.toString(), font));

                        // nadawca
                        Sender s = document.getSender();
                        if (s != null)
                        {
                            table.addCell(new Phrase(s.getSummary(), font));
                        }
                        else
                        {
                            table.addCell(new Phrase("", font));
                        }

                        // odbiorca
                        if (document.getRecipients().size() > 0)
                        {
                            Recipient r = (Recipient) document.getRecipients().get(0);
                            table.addCell(new Phrase(r.getSummary(), font));
                        }
                        else
                        {
                            table.addCell(new Phrase("", font));
                        }

                        // opis
                        table.addCell(new Phrase(document.getSummary(), font));

                        // uwagi
                        table.addCell("");
                    }

                    if (entries.length > 0) pdfDoc.add(table);

                    pdfDoc.close();
                }
                else if (Journal.OUTGOING.equals(journal.getJournalType()))
                {
                    temp = File.createTempFile("docusafe_", "_tmp");
                    com.lowagie.text.Document pdfDoc =
                        new com.lowagie.text.Document(PageSize.A4.rotate());
                    PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                    pdfDoc.open();

                    pdfDoc.add(new Phrase(journal.getSummary(), font));

                    PdfPTable headerTable = new PdfPTable(5);
                    headerTable.setWidths(new int[] { 6, 11, 11, 16, 56 });
                    headerTable.setWidthPercentage(100);

                    // TODO: rok pobiera� z dziennika
                    headerTable.addCell(new Phrase("2004", font));
                    headerTable.addCell(new Phrase(ctx.getPrincipalName(), font));
                    headerTable.addCell(new Phrase(division != null ? division.getName() : sm.getString("KancelariaOgolna"), font));
                    headerTable.addCell("");
                    headerTable.addCell("");
                    //headerTable.addCell("");

                    // drugi wiersz zawiera opisy zawarto�ci pierwszego wiersza
                    headerTable.addCell(new Phrase(sm.getString("Rok"), font));
                    headerTable.addCell(new Phrase(sm.getString("Referent"), font));
                    headerTable.addCell(new Phrase(sm.getString("SymbolKomorki"), font));
                    headerTable.addCell(new Phrase(sm.getString("OznaczenieTeczki"), font));
                    headerTable.addCell(new Phrase(sm.getString("TytulTeczkiWedleWykazuAkt"), font));
                    //headerTable.addCell(new Phrase("Uwagi", font));

                    pdfDoc.add(headerTable);

                    // dane o dokumentach w dzienniku

                    // nag��wki

                    Table table = new Table(8);
                    table.setWidths(new int[] { 6, 17, 15, 12, 12, 12, 12, 14 });
                    table.setWidth(100);

                    Cell _lp = new Cell(new Phrase(sm.getString("Lp."), font));
                    _lp.setRowspan(2);
                    table.addCell(_lp);

                    Cell _case = new Cell(new Phrase(sm.getString("Sprawa"), font));
                    _case.setRowspan(2);
                    table.addCell(_case);

                    Cell _from = new Cell(new Phrase(sm.getString("OdKogoWplynela"), font));
                    _from.setColspan(2);
                    table.addCell(_from);

                    Cell _date = new Cell(new Phrase(sm.getString("OdKogoWplynela"), font));
                    _date.setColspan(2);
                    table.addCell(_date);

                    Cell _remarks = new Cell(new Phrase(sm.getString("UwagiSposobZalatwienia"), font));
                    _remarks.setRowspan(2);
                    table.addCell(_remarks);

                    Cell _memo = new Cell(new Phrase(sm.getString("Notatki"), font));
                    _memo.setRowspan(2);
                    table.addCell(_memo);

                    Cell _mark = new Cell(new Phrase(sm.getString("ZnakPisma"), font));
                    table.addCell(_mark);

                    Cell _day = new Cell(new Phrase(sm.getString("Zdnia"), font));
                    table.addCell(_day);

                    Cell _caseBegun = new Cell(new Phrase(sm.getString("WszczeciaSprawy"), font));
                    table.addCell(_caseBegun);

                    Cell _caseFinished = new Cell(new Phrase(sm.getString("OstatecznegoZalatwienia"), font));
                    table.addCell(_caseFinished);

                    table.setLastHeaderRow(2);

                    // dane w dzienniku

                    JournalEntry[] entries = journal.getEntries(DateUtils.nullSafeParseJsDate(sDate));
                    for (int i=0; i < entries.length; i++)
                    {
                        int lp = i+1;
                        final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(entries[i].getDocumentId());

                        // Lp.
                        _lp = new Cell(new Phrase(String.valueOf(lp), font));
                        _lp.setRowspan(2);
                        table.addCell(_lp);

                        OfficeCase c = document.getContainingCase();

                        // Sprawa
                        if (c != null)
                        {
                            _case = new Cell(new Phrase(c.getOfficeId(), font));
                            _case.setRowspan(2);
                            table.addCell(_case);
                        }
                        else
                        {
                            _case = new Cell("");
                            _case.setRowspan(2);
                            table.addCell(_case);
                        }

                        // od kogo wyp�yn�a

                        StringBuilder sender = new StringBuilder(100);
                        Sender s = document.getSender();
                        if (s != null)
                        {
                            if (s.getFirstname() != null)
                                sender.append(s.getFirstname());
                            if (sender.length() > 0)
                                sender.append(" ");
                            if (s.getLastname() != null)
                                sender.append(s.getLastname());
                            if (!StringUtils.isEmpty(s.getOrganization()))
                            {
                                if (sender.length() > 0) sender.append(" / ");
                                sender.append(s.getOrganization());
                            }
                        }

                        _from = new Cell(new Phrase(sender.toString(), font));
                        _from.setColspan(2);
                        table.addCell(_from);

                        // data wszcz�cia/za�atwienia sprawy
                        _date = new Cell("");
                        _date.setColspan(2);
                        table.addCell(_date);

                        // uwagi (spos�b za�atwienia)
                        _remarks = new Cell("");
                        _remarks.setRowspan(2);
                        table.addCell(_remarks);

                        _memo = new Cell("");
                        _memo.setRowspan(2);
                        table.addCell(_memo);

                        // znak pisma
                        _mark = new Cell(new Phrase(document.getCaseDocumentId() != null ? document.getCaseDocumentId() : "", font));
                        table.addCell(_mark);

                        // z dnia
                        _day = new Cell(new Phrase(document.getDocumentDate() != null ? DateUtils.formatCommonDate(document.getDocumentDate()) : "", font));
                        table.addCell(_day);

                        _caseBegun = new Cell("");
                        table.addCell(_caseBegun);

                        _caseFinished = new Cell("");
                        table.addCell(_caseFinished);

                        table.addCell(new Cell(""));
                    }

                    if (entries.length > 0) pdfDoc.add(table);
                    pdfDoc.close();
                }
            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            catch (IOException e)
            {
                // b��dy w okienku (alert)
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            catch (Exception e)
            {
                event.getLog().error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(event.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    public static String getLink(HttpServletRequest request, Long journalId, Date date)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        // formatJsDate, bo warto�� tego pola mo�e te� pochodzi� z formularza
        return fc.getPath()+
            "?journalId="+journalId +
            "&date="+DateUtils.formatJsDate(date);
    }
}
