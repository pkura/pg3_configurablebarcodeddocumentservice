package pl.compan.docusafe.web.office;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;




public class PackageInNrToBarAction extends PackageBaseAction
{
	private static final Log log = LogFactory.getLog(PackageInNrToBarAction.class);

	private List<Package> packages;
	private Package thisPackage;

	private String[] elementId;
    private Long id;
    private String nr_dokumentu;
    private String[] barkodes;
    private String nrPaczki;
    private String numerPaczki;
    private String numer;
    private String packageStatus;    
	private boolean barcodeExist = false;
    
    
    protected void setup()
    {

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }    
    
	

}