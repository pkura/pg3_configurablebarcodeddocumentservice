package pl.compan.docusafe.web.office;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AcceptWfAssignmentAction.java,v 1.59 2010/08/16 11:01:32 mariuszk Exp $
 */
public class AcceptWfAssignmentAction extends EventActionSupport
{
	static final Logger log = LoggerFactory.getLogger(AcceptWfAssignmentAction.class);
    private Long documentId;
    private String division;
    private String activity;
    private String redirectUrl;
    private String target;
    private Boolean openViewer;
    /** Czy akcja wywo�ana ze strony current-user-task-list */
    private Boolean ownTask;
    private Boolean setInitialValues;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private final String ACCEPT_EVT = "accept";
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(new SetResultListener("task-list"));

        registerListener("doAccept").
            append(OpenHibernateSession.INSTANCE).
            append(new SetResultListener("task-list")).
            append(ACCEPT_EVT, new Accept(this)).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class CheckIfAlreadyAssigned implements ActionListener{
		
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin(); 
				
                Document document;                
                documentId = WorkflowFactory.getDocumentId(activity);
                if (documentId == null)
                	throw new EdmException(sm.getString("WdefinicjiProcesuPracyNieZnalezionoIdentyfikatoraDokumentu"));

                document = Document.find((Long) documentId);
                if(document instanceof OfficeDocument){
                	OfficeDocument oDoc = (OfficeDocument)document;
                	log.debug("AcceptWfAssignmentAction.java: currentAssignedUser = " + oDoc.getCurrentAssignmentUsername());
                }
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				log.debug(
						"AcceptWfAssignmentAction.java:65 " + e, e);
			}
		}
    }

    private class Accept implements ActionListener
    {
    	private EventActionSupport eventActionSupport;
    	public Accept(EventActionSupport eventActionSupport) 
    	{
			this.eventActionSupport = eventActionSupport;
		}
        public void actionPerformed(ActionEvent event)
        {
            if (activity == null)
                return;

            try
            {
                DSApi.context().begin();                     

                OfficeDocument document;
                
                documentId = WorkflowFactory.getDocumentId(activity);

                if (documentId == null)
                	throw new EdmException(sm.getString("WdefinicjiProcesuPracyNieZnalezionoIdentyfikatoraDokumentu"));

                document = OfficeDocument.find(documentId);
                log.debug(
						"AcceptWfAssignmentAction.java:" + document + "\n" + document.getCurrentAssignmentUsername() + "\n" + isOwnTask());
                //ochrona przed podw�jnym przydzia�em z listy *swoich* zada�
//                if(isOwnTask() 
//                		&& (document instanceof OfficeDocument)
//                		&& ((OfficeDocument)document).getCurrentAssignmentUsername() != null){
//                	
//                	throw new EdmException(sm.getString("ZadanieZostaloJuzPrzydzielone"));
//                }

                if (SnapshotUtils.isAssignedToUser(DSApi.context().getPrincipalName(), getDocumentId())) {
                	WorkflowFactory.getInstance().acceptTask(activity, event, document,division);
                	
                	 // dodanie pe�nych uprawnie� dla u�ytkownika
            		try {
            			addAcceptedUserPermission(document, DSApi.context().getPrincipalName());
            		} catch (EdmException e) {
            			log.error(null, e.toString());
            		}
                } else {
                	throw new EdmException(sm.getString("BrakPrzypisaniaDlaUzytkownika"));
                }
                
                TaskSnapshot.updateByDocumentId(document.getId(),document.getStringType());

                // TODO: ten redirect pierwszy ponizszy chyba nie uzywany - MOZNA USUNAC
                if (!StringUtils.isEmpty(redirectUrl))
                {
                    if (redirectUrl.endsWith("?documentId="))
                        redirectUrl = redirectUrl + documentId;
                	redirectUrl = redirectUrl + "&activity="+activity; 
                    event.setResult("redirect");
                }
                else
                {
                    if (document.getType() == DocumentType.INCOMING)
                    {
                        if( target != null )
                            event.setResult("in-office-document-"+target);
                        else
                            event.setResult("in-office-document");
                    }
                    else if (document.getType() == DocumentType.OUTGOING)
                    {
                    	if( target != null )
                            event.setResult("out-office-document-"+target);
                        else
                            event.setResult("out-office-document");
                    }
                    else if (document.getType() == DocumentType.INTERNAL)
                    {
                        if( target != null)
                            event.setResult("int-office-document-"+target);
                        else
                            event.setResult("int-office-document");
                    }
                    else if (document.getType() == DocumentType.ORDER)
                    {
                        event.setResult("order-office-document");
                    }
                    
                }

                DSApi.context().commit();
                
                if (document.getType() == DocumentType.INCOMING)
                {
                    AccessLog.logPersonalDataAccess(DSApi.context().getPrincipalName(),
                        documentId,
                            document.getSender() != null
                                    ? Arrays.asList(document.getSender())
                                    : Collections.emptyList());
                }
                else if (document.getType() == DocumentType.OUTGOING)
                {
                    AccessLog.logPersonalDataAccess(DSApi.context().getPrincipalName(),
                        documentId, document.getRecipients());
                }
            }

            catch (EdmException e)
            {
                log.error("AcceptWfAssignmentAction.java:168",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
			catch (Exception e)
            {
                log.error("AcceptWfAssignmentAction.java:168",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

	private void addAcceptedUserPermission (Document doc, String name) throws UserNotFoundException, EdmException {
		
		if (!AvailabilityManager.isAvailable("permissions.dontGivePermissionToUserOnEnterToDocument")){
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		DSUser user = DSUser.findByUsername(name);
		String fullName = user.getLastname() + " " + user.getFirstname();

		perms.add(new PermissionBean(ObjectPermission.READ, name, ObjectPermission.USER, fullName + " (" + name + ")"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, name, ObjectPermission.USER, fullName + " (" + name + ")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, name, ObjectPermission.USER, fullName + " (" + name + ")"));

		DocumentLogic docLogic = doc.getDocumentKind().logic();
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(doc);
		perms.addAll(documentPermissions);
		
		if (docLogic instanceof AbstractDocumentLogic) {
			((AbstractDocumentLogic) docLogic).setUpPermission(doc, perms);
		} else {
			throw new EdmException("Niew�a�ciwa klasa parametryzujaca, oczekuje implementacji AbstractDocumentLogic");
		}
		}
	}
    
    public void setActivity(String activity)
    {
        this.activity = activity;
    }

    public String getActivity()
    {
        return activity;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

	public Boolean getOpenViewer() {
		return openViewer;
	}

	public void setOpenViewer(Boolean openViewer) {
		this.openViewer = openViewer;
	}

	public void setOwnTask(Boolean ownTask) {
		this.ownTask = ownTask;
	}

	public Boolean isOwnTask() { 
		return ownTask != null && ownTask;
	}

	public void setSetInitialValues(Boolean setInitialValues)
	{
		this.setInitialValues = setInitialValues;
	}

	public Boolean getSetInitialValues()
	{
		return setInitialValues;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
}
