package pl.compan.docusafe.web.office;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Bazowa akcja wy�wietlaj�ca teczki i pozwalaj�ca na ich zak�adanie.
 * Zak�adanie teczki powinno by� dwustopniowe: najpierw u�ytkownik
 * wybiera dzia� i RWA, po czym widzi sugerowany numer teczki, kt�ry
 * mo�e wyedytowa�. Dopiero wtedy teczka jest zak�adana.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PortfoliosBaseAction.java,v 1.60 2009/05/25 08:41:50 pecet3 Exp $
 */
public abstract class PortfoliosBaseAction extends EventActionSupport
{
	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
    // @EXPORT
    private DSDivision targetDivision;
    private String treeHtml;
    private List portfolios;
    private boolean canCreate;
    private int currentYear;
    private int minYear;
    private Integer year;
    private List<Long> selectedOfficeFolders;
    private String user;
    private List<Integer> years;
    private Map<String,Integer> yearsMap;


    // @IMPORT
    private Integer rwaCategoryId;
    private Long targetOfficeFolderId;
    
    // @EXPORT/@IMPORT
    private String divisionGuid;
    private String name;
    private boolean showAll;
    private String days;
    private String documentIncomingDate;
    /**
     * Opis wybranego RWA. Nieu�ywany w tej akcji, ale powinien by�
     * pokazywany w JSP r�wnie� po prze�adowaniu strony.
     */
    private String prettyRwaCategory;
    private String barcode;
    private String rok; 	
	
	private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;

    private String suggestedOfficeId;

    private Boolean customOfficeId;

    private static final String EV_CREATE = "create";
    private static final String EV_PRECREATE = "precreate";
    private boolean first;

    public boolean isFirst()
    {
        return first;
    }

    public void setFirst(boolean first)
    {
        this.first = first;
    }

    public abstract String getBaseLink();

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidatePreCreate()).
            append(EV_PRECREATE, new PreCreate()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private boolean canCreatePortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_TWORZENIE_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_TWORZENIE_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            try
            {
                
                user = DSApi.context().getPrincipalName();
                currentYear = GlobalPreferences.getCurrentYear();

                // drzewo struktury organizacyjnej
                DSUser user = DSApi.context().getDSUser();
                targetDivision = null;
                try
                {
                    if (!StringUtils.isEmpty(divisionGuid))
                    {
                        DSDivision[] divisions = user.getDivisions();
                        //targetDivision = divisions[1];
                        targetDivision = DSDivision.find(divisionGuid);
                    }
                }
                catch (DivisionNotFoundException e)
                {
                	
                }
                
                //warunek spe�niony gdy wchodzimy do spraw w pi�mie i klikamy wybierz 
                if(first)
                {
                    DSDivision[] divisions = user.getDivisions();
                    /*if (divisions.length > 0)
                    {
                        if(divisions[0].getDivisionType().equals(targetDivision.ROOT_GUID))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                            //divisionGuid = DSDivision.ROOT_GUID;
                            //targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                        }
                        else if(divisions[0].getDivisionType().equals(DSDivision.TYPE_GROUP))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                        }
                        else if(divisions[0].getDivisionType().equals(DSDivision.TYPE_POSITION))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                        }
                        else
                        {
                            divisionGuid = divisions[0].getGuid();
                            targetDivision = divisions[0];
                        }
                    }
                    else
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);*/
                    
                    //wyb�r katalogu
                    int pp = 0;
                    for (int i = 0; i < divisions.length ; i++)
                    {
                        if(divisions.length == 1)
                        {
                            divisionGuid = DSDivision.ROOT_GUID;
                            targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            pp=1;
                            break;
                        }
                        if(divisions[i].getDivisionType().equals(DSDivision.TYPE_DIVISION))
                        {
                            divisionGuid = divisions[i].getGuid();
                            targetDivision = divisions[i];
                            pp=1;
                            break;
                        }
                    }
                    if(pp==0)
                    {
                        divisionGuid = DSDivision.ROOT_GUID;
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                    }
                    if(divisions.length == 0)
                    {
                        divisionGuid = DSDivision.ROOT_GUID;
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                    }
                    
                }
                //warunek spe�niany gdy wchodzimy do Teczek
                if (targetDivision == null)
                {
                    int pom = 0;
                    int j = 0;
                    DSDivision[] divisions = user.getDivisions();

                    /*if (divisions.length > 0)
                    {
                        if(divisions[0].getDivisionType().equals(targetDivision.ROOT_GUID))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                                if(divisions[divisions.length-1].getDivisionType().equals(targetDivision.ROOT_GUID) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_GROUP) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_POSITION))
                                {
                                    divisionGuid = divisions[divisions.length-2].getGuid();
                                    targetDivision = divisions[divisions.length-2];
                                }
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                            //divisionGuid = DSDivision.ROOT_GUID;
                            //targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                        }
                        else if(divisions[0].getDivisionType().equals(DSDivision.TYPE_GROUP))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                                if(divisions[divisions.length-1].getDivisionType().equals(targetDivision.ROOT_GUID) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_GROUP) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_POSITION))
                                {
                                    divisionGuid = divisions[divisions.length-2].getGuid();
                                    targetDivision = divisions[divisions.length-2];
                                }
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                        }
                        else if(divisions[0].getDivisionType().equals(DSDivision.TYPE_POSITION))
                        {
                            if(divisions.length > 1)
                            {
                                divisionGuid = divisions[divisions.length-1].getGuid();
                                targetDivision = divisions[divisions.length-1];
                                if(divisions[divisions.length-1].getDivisionType().equals(targetDivision.ROOT_GUID) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_GROUP) || divisions[divisions.length-1].getDivisionType().equals(DSDivision.TYPE_POSITION))
                                {
                                    divisionGuid = divisions[divisions.length-2].getGuid();
                                    targetDivision = divisions[divisions.length-2];
                                }
                            }
                            else if(divisions.length == 1)
                            {
                                divisionGuid = DSDivision.ROOT_GUID;
                                targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            }
                            else
                            {
                                divisionGuid = divisions[0].getGuid();
                                targetDivision = divisions[0];
                            }
                        }
                        else
                        {
                            divisionGuid = divisions[0].getGuid();
                            targetDivision = divisions[0];
                        }
                    }
                    else
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);*/
                                        
                    //Wyb�r kt�ry katalog ma by� otwarty.
                    int pp = 0;
                    for (int i = 0; i < divisions.length ; i++)
                    {
                        if(divisions.length == 1)
                        {
                            divisionGuid = DSDivision.ROOT_GUID;
                            targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                            pp=1;
                            break;
                        }
                        if(divisions[i].getDivisionType().equals(DSDivision.TYPE_DIVISION))
                        {
                            divisionGuid = divisions[i].getGuid();
                            targetDivision = divisions[i];
                            pp=1;
                            break;
                        }
                        /*else
                        {
                            divisionGuid = divisions[0].getGuid();
                            targetDivision = divisions[0];
                        }*/
                    }
                    if(pp==0)
                    {
                        divisionGuid = DSDivision.ROOT_GUID;
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                    }
                    if(divisions.length == 0)
                    {
                        divisionGuid = DSDivision.ROOT_GUID;
                        targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                    }
                }

                canCreate = canCreatePortfolio(targetDivision);

                // klasa tworz�ca urle dla element�w drzewa
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                    	    //if(documentIncomingDate==null)documentIncomingDate="";
                            String url =
                            ServletActionContext.getRequest().getContextPath()+
                            getBaseLink() +
                            "?divisionGuid="+((DSDivision) element).getGuid()+
                            "&documentIncomingDate="+documentIncomingDate;
                        	return url;
                    }
                };

                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    false, false, false);

              
                treeHtml = tree.generateTree();

                List<Long> selected = new ArrayList<Long>();
                if (targetOfficeFolderId != null) {
                    OfficeFolder folder = OfficeFolder.find(targetOfficeFolderId);
                    
                    do {
                        selected.add(0,folder.getId());
                        DSApi.initializeProxy(folder.getParent());
                        folder = folder.getParent();
                    } while (folder != null);
                }
         
                if (targetDivision != null)
                {
                                   
                    
                    divisionGuid = targetDivision.getGuid();
                    portfolios = OfficeFolder.findByDivisionGuid(divisionGuid);
                    
                    years = new ArrayList();
                    
                    for(Iterator iter=portfolios.iterator(); iter.hasNext();)
                    {
                        OfficeFolder officeFolder = (OfficeFolder) iter.next();
                        if(!years.contains(officeFolder.getYear()))
                        {
                            years.add(officeFolder.getYear());
                        }

                    }
                    
                    Date date = GlobalPreferences.getCurrentDay();
                    minYear = currentYear;
                    if (date.getMonth() == 0)
                        minYear = minYear - 1;
                    if (showAll)
                    {
                    	currentYear = (new Date()).getYear();
                        portfolios = OfficeFolder.findByDivisionGuidAndYear(targetDivision.getGuid(), minYear);
                    }
                    else
                    {
                        if(year == null)
                        {
                            year =GlobalPreferences.getCurrentYear();
                        }
                        portfolios = OfficeFolder.findByDivisionGuidYear(targetDivision.getGuid(),year);
                    }

                    if (PortfoliosBaseAction.this instanceof PickPortfolioAction)
                    {
                        for (Iterator iter=portfolios.iterator(); iter.hasNext(); )
                        {
                            Container c = (Container) iter.next();
                            System.out.println(c.getRwa());
                            if ((c instanceof OfficeFolder) && (selected.contains(((OfficeFolder) c).getId())))
                            {
                            	((OfficeFolder) c).getRwa();
                                ((OfficeFolder) c).initializeChildrenRecursive(selected);
                            }
                        }
                    }
                }
                selectedOfficeFolders = selected;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(divisionGuid))
                addActionError(sm.getString("NieWybranoDzialu"));
            if (StringUtils.isEmpty(name))
                addActionError(sm.getString("NiePodanoOpisuTeczki"));
            if (TextUtils.trimmedStringOrNull(days) != null)
            {
                try 
                {
                    Integer _days = new Integer(TextUtils.trimmedStringOrNull(days));
                }
                catch (Exception e)
                {
                    addActionError(sm.getString("PodanaLiczbaDniJestNieprawid�owa"));
                }
            }
            
            if (rwaCategoryId == null)
            {
                if (StringUtils.isEmpty(prettyRwaCategory))
                {
                    addActionError("Nie wybrano kategorii RWA");
                }
                else
                {
                    try
                    {
                        int rwa = Integer.parseInt(prettyRwaCategory);
                        if (rwa < 0 || rwa > 99999)
                            addActionError("Wpisano niepoprawny symbol RWA: "+prettyRwaCategory);
                    }
                    catch (NumberFormatException e)
                    {
                        addActionError("Wpisano niepoprawny symbol RWA: "+prettyRwaCategory);
                    }
                }
            }

            if (hasActionErrors())
                event.skip(EV_CREATE);
        }     
    }

    /**
     * Tworzenie teczki w dziale.
     * <p>
     * Tworzenie teczki musi by� dwustopniowe - najpierw prezentacja numeru,
     * kt�ry zostanie nadany, mo�liwo�� modyfikacji numeru przez u�ytkownika
     * i utworzenie teczki.
     * <p>
     * Tworzenie podteczki - dodatkowo podanie kolejnego numeru w rejestrze
     * (Portfolio.sequenceId)
     */
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Rwa rwaCategory;
                if (rwaCategoryId != null)
                {
                    rwaCategory = Rwa.find(rwaCategoryId);
                }
                else
                {
                    rwaCategory = Rwa.findByCode(prettyRwaCategory);
                }

                if (rwaCategory.getAcHome() == null)
                    throw new EdmException("Nie mo�na za�o�y� teczki w RWA "+
                        "nie posiadaj�cym kategorii archiwalnej");

                if(!rwaCategory.getIsActive())
                {
                	throw new EdmException("Nie mo�na za�o�y� teczki w RWA, "+
                    "kt�re jest nieaktywne"); 
                }
                OfficeFolder officeFolder = new OfficeFolder(name, DSDivision.find(divisionGuid),
                    rwaCategory, null);
                officeFolder.setClerk(DSApi.context().getPrincipalName());
                
                // W zale�no�ci od opcji, mo�emy za�o�y� teczk� w roku wskazanym
                if (AvailabilityManager.isAvailable("teczki.otwieranie.poprzednielata"))
                {
                	// Ustawiamy rok teczki z formularza rokotwarcia
                	
                	officeFolder.setYear(Integer.parseInt(rok));
                }
                else
                {
                	// Rok teczki taki sam jak bie��cy
                	officeFolder.setYear(new Integer(GlobalPreferences.getCurrentYear()));                	
                }
                
                Integer _days = null;
                if (TextUtils.trimmedStringOrNull(days) != null) 
                {
                    try 
                    {
                        _days = new Integer(TextUtils.trimmedStringOrNull(days));
                    }
                    catch (Exception e)
                    {    
                    }
                }
                if(_days != null){
                	officeFolder.setDays(_days);
                }
                officeFolder.setBarcode(barcode);
                
                if (customOfficeId != null && customOfficeId.booleanValue())
                {
                    String oid = StringUtils.join(new String[] {
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
                    }, "");

                    officeFolder.create(oid,
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");
                }
                else
                {
                    officeFolder.create(suggestedOfficeId,
                        officeIdPrefix, officeIdMiddle, officeIdSuffix);
                }

             
                DSApi.context().commit();

                addActionMessage("Utworzono teczk� "+officeFolder.getOfficeId());
                days = null;
                barcode = null;
                name = null;
                rok = null;
                rwaCategoryId=null;
                rwaCategory=null;
            }
            catch (EdmException e)
            {
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
            	 event.setResult("pre-create");
                 DSApi.context().setRollbackOnly();
                 addActionError(e.getMessage());
			} 
        }
    }

    private class ValidatePreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(divisionGuid))
                addActionError(sm.getString("NieWybranoDzialu"));
            if (StringUtils.isEmpty(name))
                addActionError(sm.getString("NiePodanoOpisuTeczki"));
            if (TextUtils.trimmedStringOrNull(days) != null) 
            {
                try      
                {
                    Integer _days = new Integer(TextUtils.trimmedStringOrNull(days));
                    if(_days < 0)
                    {
                    	addActionError("Ilo�� dni na za�atwienie nie mo�e by� liczb� ujemn�.");
                    }
                }
                catch (Exception e)
                {
                    addActionError(sm.getString("PodanaLiczbaDniJestNieprawidlowa"));
                }
            }
            
            if (AvailabilityManager
					.isAvailable("teczki.otwieranie.poprzednielata")) {
				
				if (StringUtils.isEmpty(rok))
					addActionError(sm.getString("NieWpisanoRokuOtwarciaTeczki"));
				else
					try {
						int rwa = Integer.parseInt(rok);
						if (rwa < 0 || rwa > 99999)
							addActionError("Wpisano niepoprawny rok "
									+ rok);
					} catch (NumberFormatException e) {
						addActionError("Wpisano niepoprawny rok " + rok);
					}
			}
            
            
            if (rwaCategoryId == null)
            {
                if (StringUtils.isEmpty(prettyRwaCategory))
                {
                    addActionError("Nie wybrano kategorii RWA");
                }
                else
                {
                    try
                    {
                        int rwa = Integer.parseInt(prettyRwaCategory);
                        if (rwa < 0 || rwa > 99999)
                            addActionError("Wpisano niepoprawny symbol RWA: "+prettyRwaCategory);
                    }
                    catch (NumberFormatException e)
                    {
                        addActionError("Wpisano niepoprawny symbol RWA: "+prettyRwaCategory);
                    }
                }
            }

            if (barcode != null && !"".equals(barcode)){
            	try {
            		List list = OfficeFolder.findByBarcode(barcode);
            		List<OfficeDocument> bar = OfficeDocument.findAllByBarcode(barcode);
            		if (bar.size()>0){
            			addActionError("Niepoprawna warto�c kodu kreskowego. Musi by� unikalny: "+barcode);
            		}
            		if (list != null && list.size()>0)
            			addActionError("Niepoprawna warto�c kodu kreskowego. Musi by� unikalny: "+barcode);
            	} catch (EdmException e) {
            		addActionError("B��d podczas sprawdzania unikalno�ci kodu kreskowego "+barcode);
            	}
            }

            if (hasActionErrors())
                event.skip(EV_PRECREATE);
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("pre-create");

            try
            {
                Rwa rwaCategory;
                if (rwaCategoryId != null)
                {
                    rwaCategory = Rwa.find(rwaCategoryId);
                }
                else
                {
                    rwaCategory = Rwa.findByCode(prettyRwaCategory);
                }

                if (rwaCategory.getAcHome() == null)
                    throw new EdmException("Nie mo�na za�o�y� teczki w RWA "+
                        "nie posiadaj�cym kategorii archiwalnej");

                if(!rwaCategory.getIsActive())
                {
                	throw new EdmException("Nie mo�na za�o�y� teczki w RWA, "+
                    "kt�re jest nieaktywne"); 
                }
                
                Integer _days = null;
                if (TextUtils.trimmedStringOrNull(days) != null) 
                {
                    try 
                    {
                        _days = new Integer(TextUtils.trimmedStringOrNull(days));
                    }
                    catch (Exception e)
                    {    
                        throw new EdmException("Wystapil blad");
                    }
                }
                
                
                OfficeFolder officeFolder = new OfficeFolder(name, DSDivision.find(divisionGuid),
                    rwaCategory, null);
                officeFolder.setDays(_days);
                officeFolder.setBarcode(barcode);
                
                
                if (AvailabilityManager
    					.isAvailable("teczki.otwieranie.poprzednielata")) {
                	Integer _rok = null;
                    if (TextUtils.trimmedStringOrNull(rok) != null) 
                    {
                        try 
                        {
                        	_rok = new Integer(TextUtils.trimmedStringOrNull(rok));
                        }
                        catch (Exception e)
                        {    
                            throw new EdmException("Wystapil blad");
                        }
                    }
                    
                    officeFolder.setYear(_rok);
                }
                
                

                String[] breakdown = officeFolder.suggestOfficeId();
                suggestedOfficeId = StringUtils.join(breakdown, "");
                customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                customOfficeIdPrefixSeparator = breakdown[1];
                customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                customOfficeIdSuffixSeparator = breakdown[3];
                customOfficeIdSuffix = officeIdSuffix = breakdown[4];

            }
            catch (EdmException e)
            {
                event.setResult(SUCCESS);
                addActionError(e.getMessage());
            }
        }
    }

    private class YEAR
    {
        private String name;
        private Integer value;
        public YEAR(String nazwa, Integer wartosc)
        {
            super();
            this.name = nazwa;
            this.value = wartosc;
        }
        public String getName()
        {
            return name;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public Integer getValue()
        {
            return value;
        }
        public void setValue(Integer value)
        {
            this.value = value;
        }
   
        
        
    }
    public DSDivision getTargetDivision()
    {
        return targetDivision;
    }

    public List<Long> getSelectedOfficeFolders()
    {
        return selectedOfficeFolders;
    }
    
    public void setTargetOfficeFolderId(Long targetOfficeFolderId)
    {
        this.targetOfficeFolderId = targetOfficeFolderId;
    }
    
    public String getTreeHtml()
    {
        return treeHtml;
    }

    public List getPortfolios()
    {
        return portfolios;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRwaCategoryId(Integer rwaCategoryId)
    {
        this.rwaCategoryId = rwaCategoryId;
    }

    public Integer getRwaCategoryId()
    {
        return rwaCategoryId;
    }

    public String getPrettyRwaCategory()
    {
        return prettyRwaCategory;
    }

    public void setPrettyRwaCategory(String prettyRwaCategory)
    {
        this.prettyRwaCategory = prettyRwaCategory;
    }

    public boolean isCanCreate()
    {
        return canCreate;
    }

    public boolean isShowAll()
    {
        return showAll;
    }
    
    public void setShowAll(boolean showAll)
    {
        this.showAll = showAll;
    }    
        
    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public String getCustomOfficeIdPrefix()
    {
        return customOfficeIdPrefix;
    }

    public String getCustomOfficeIdPrefixSeparator()
    {
        return customOfficeIdPrefixSeparator;
    }

    public String getCustomOfficeIdMiddle()
    {
        return customOfficeIdMiddle;
    }

    public String getCustomOfficeIdSuffixSeparator()
    {
        return customOfficeIdSuffixSeparator;
    }

    public String getCustomOfficeIdSuffix()
    {
        return customOfficeIdSuffix;
    }

    public void setCustomOfficeId(Boolean customOfficeId)
    {
        this.customOfficeId = customOfficeId;
    }

    public String getOfficeIdPrefix()
    {
        return officeIdPrefix;
    }

    public String getOfficeIdSuffix()
    {
        return officeIdSuffix;
    }

    public String getOfficeIdMiddle()
    {
        return officeIdMiddle;
    }

    public String getSuggestedOfficeId()
    {
        return suggestedOfficeId;
    }

    public void setSuggestedOfficeId(String suggestedOfficeId)
    {
        this.suggestedOfficeId = suggestedOfficeId;
    }

    public void setCustomOfficeIdPrefix(String customOfficeIdPrefix)
    {
        this.customOfficeIdPrefix = customOfficeIdPrefix;
    }

    public void setCustomOfficeIdPrefixSeparator(String customOfficeIdPrefixSeparator)
    {
        this.customOfficeIdPrefixSeparator = customOfficeIdPrefixSeparator;
    }

    public void setCustomOfficeIdMiddle(String customOfficeIdMiddle)
    {
        this.customOfficeIdMiddle = customOfficeIdMiddle;
    }

    public void setCustomOfficeIdSuffixSeparator(String customOfficeIdSuffixSeparator)
    {
        this.customOfficeIdSuffixSeparator = customOfficeIdSuffixSeparator;
    }

    public void setCustomOfficeIdSuffix(String customOfficeIdSuffix)
    {
        this.customOfficeIdSuffix = customOfficeIdSuffix;
    }

    public void setOfficeIdPrefix(String officeIdPrefix)
    {
        this.officeIdPrefix = officeIdPrefix;
    }

    public void setOfficeIdSuffix(String officeIdSuffix)
    {
        this.officeIdSuffix = officeIdSuffix;
    }

    public void setOfficeIdMiddle(String officeIdMiddle)
    {
        this.officeIdMiddle = officeIdMiddle;
    }

    public int getCurrentYear()
    {
        return currentYear;
    }
    
    public int getMinYear()
    {
        return minYear;
    }
    
    public String getDays()
    {
        return days;
    }
    
    public void setDays(String days)
    {
        this.days = days;
    }
    
    public String getUser()
    {
        return user;
    }
    
    public String getDocumentIncomingDate()
    {
        return documentIncomingDate;
    }
    
    public void setDocumentIncomingDate(String documentIncomingDate)
    {
        this.documentIncomingDate = documentIncomingDate;
    }

    public Integer getYear()
    {
        return year;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public List getYears()
    {
        return years;
    }

    public void setYears(List years)
    {
        this.years = years;
    }

    public Map<String, Integer> getYearsMap()
    {
        return yearsMap;
    }

    public void setYearsMap(Map<String, Integer> yearsMap)
    {
        this.yearsMap = yearsMap;
    }
    
  
    public String getRok() {
		return rok;
	}

	public void setRok(String rok) {
		this.rok = rok;
	}

}
