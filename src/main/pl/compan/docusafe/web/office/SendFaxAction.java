package pl.compan.docusafe.web.office;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class SendFaxAction extends EventActionSupport
{
	private static final Log log = LogFactory.getLog(SendFaxAction.class);
	private Long id;
	private String numerTelefonu;
	private String opis;
	private static String relayFaxInBox;
	
	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSend").
	        append(OpenHibernateSession.INSTANCE).
	        append(new SendFax()).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
	}
	
	 private class SendFax implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(numerTelefonu == null || numerTelefonu.length() < 7 )
        		addActionError("BladNumeru"+numerTelefonu);
        	try 
        	{
        		if(getId() == null)
					throw new EdmException("BrakZalacznika");
				
				AttachmentRevision revision = AttachmentRevision.find(getId());
				
				if(revision == null )
					throw new EdmException("NieZnalazlZalacznika");
				
				Document doc = revision.getAttachment().getDocument();
				Long[] documentIds = new Long[1];
				documentIds[0] = doc.getId();
        		if(DSApi.context().getDSUser().getEmail() == null )
            		addActionError("BrakAdresuEmailWdanychUzytkownika");
        		
        		File pdf = File.createTempFile("docusafe_tmp_", ".pdf");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));
                
                PdfUtils.attachmentsAsPdf(documentIds, false, os, null, null, null, false);
                os.close();
        		

				((Mailer) ServiceManager.getService(Mailer.NAME)).send(Docusafe.getAdditionProperty("attachments.send.fax.relayFaxInBox"),
						DSApi.context().getDSUser().getEmail(),DSApi.context().getDSUser().asFirstnameLastname(), numerTelefonu, opis,pdf.getPath());
			} 
        	catch (Exception e) 
			{
        		log.error("",e); 
        		addActionError(e.getMessage());
				e.printStackTrace();
			}
        }
    }
	 
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
			try 
        	{
				if(getId() == null)
					throw new EdmException("BrakZalacznika");
				
				AttachmentRevision revision = AttachmentRevision.find(getId());
				
				if(revision == null )
					throw new EdmException("NieZnalazlZalacznika");
				
				Document doc = revision.getAttachment().getDocument();
				numerTelefonu = doc.getDocumentKind().logic().getPhoneNumber(doc);
			} 
			catch (EdmException e) 
			{
				addActionError(e.getMessage());
				log.error("",e);
			}
        }
    }


	public String getNumerTelefonu() {
		return numerTelefonu;
	}


	public void setNumerTelefonu(String numerTelefonu) {
		this.numerTelefonu = numerTelefonu;
	}


	public String getOpis() {
		return opis;
	}


	public void setOpis(String opis) {
		this.opis = opis;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getId() {
		return id;
	}

}
