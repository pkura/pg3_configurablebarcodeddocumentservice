package pl.compan.docusafe.web.office.alfresco;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.AlfrescoFolderTree;
import pl.compan.docusafe.web.tree.RwaTree;
import pl.compan.docusafe.webwork.event.*;

/**
 * Akcja wy�wietla drzewo folder�w
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class PickAlfrescoFolderAction extends EventActionSupport {

    private final static Logger log = LoggerFactory.getLogger(PickAlfrescoFolderAction.class);
    private String uuid;
    private String treeHtml;
    private String chooseType = "DOCUMENT";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try {
                AlfrescoApi.checkConnection();
            } catch (Exception e) {
                log.error(e.getMessage());
                event.addActionError(e.getMessage());
                return;
            }
            try {
                treeHtml = AlfrescoFolderTree.newTree(uuid, ServletActionContext.getRequest().getContextPath(), AlfrescoFolderTree.ChooseType.valueOf(chooseType)).generateTree();
            } catch (Exception e) {
                log.error("", e);
                event.addActionError(e.getMessage());
            }
        }
    }

    public String getTreeHtml() {
        return treeHtml;
    }

    public void setTreeHtml(String treeHtml) {
        this.treeHtml = treeHtml;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getChooseType() {
        return chooseType;
    }

    public void setChooseType(String chooseType) {
        this.chooseType = chooseType;
    }
}
