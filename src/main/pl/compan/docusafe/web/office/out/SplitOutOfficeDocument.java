package pl.compan.docusafe.web.office.out;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**	
 * 
 * @author Grzegorz Filip <grzegorz.filip@docusafe.pl>
 * @date 06.08.2013
 *
 */

public class SplitOutOfficeDocument
{
	private static final String ID = "ID";
	private static final String multiDictionaryValues = "M_DICT_VALUES";
	private static final String firstRecipient = "RECIPIENT_1";
	private static Map<String,Object> oldValues = new HashMap<String, Object>();
	private static Map<String,Object> multiValues = new HashMap<String, Object>();
	private static Map<String,Object> tempMultiValues = new HashMap<String, Object>();
	private static Map<String,Object> sigleValues = new HashMap<String, Object>();
	private static OfficeDocument officeDocument;
	private static OutOfficeDocument newOutDocument;
	private static  List<Long> attachmentsToClone = new ArrayList<Long>();
    
    
    /**
     * Metoda dzieli pismo  posiadajace wielu  odbiorc�w na pojedyncze pisma 
     * oraz w zalew�nosci od przekazanaego parametru assignKo 
     * nadaje kolejno tworzonym pismom pismom numery KO 
     * @param doc
     * @param values
     * @param assignKo
     * @param event
     * @throws EdmException
     * @throws SQLException
     */
	public static void splitOutOfficeDocument(OfficeDocument doc, Map<String, Object> values, boolean assignKo, ActionEvent event) throws EdmException,
			SQLException
	{
			
		setUpallValues(doc, values);
		if (multiValues != null)
		{
			for (String key : multiValues.keySet())
			{
				//pierwszego zostawiamy 
				if (!key.contains(firstRecipient))
				{
					oldValues = getCopyValues(values);
					sigleValues = (Map<String, Object>) multiValues.get(key);
					newOutDocument = createNewDocument(officeDocument, sigleValues, oldValues, assignKo);
					
				} else if (assignKo && doc.getOfficeNumber() == null || (doc.getOfficeNumber() != null && doc.getFormattedOfficeNumber().equals("Brak")))
				{
					OutOfficeDocument document = (OutOfficeDocument) doc;
					assignOfficeDocumentId(document);
					DSApi.context().session().saveOrUpdate(document);
				}
			}

			//Usuwanie odbiorc�w dla kt�rych zosta�y utworozne odrebne dokumenty czyli wszystkie poza pierwszym 
			Iterator<String> it = multiValues.keySet().iterator();
			while (it.hasNext())
			{
				String key = it.next();
				if (!key.equals(firstRecipient))
					it.remove();
			}
			doc.getDocumentKind().logic().validate(values, 	doc.getDocumentKind(), 	doc.getId());
			doc.getDocumentKind().saveDictionaryValues(doc, values);
			doc.getDocumentKind().saveMultiDictionaryValues(doc, values);
			DSApi.context().session().saveOrUpdate(doc);
		}
	}

	private static OutOfficeDocument createNewDocument(OfficeDocument document, Map<String, Object> sigleValues, Map<String, Object> odlValues, boolean assignKo)
			throws EdmException, SQLException
	{
		
		tempMultiValues = getCopyValues(multiValues);
		newOutDocument = copyAttachmentAndCreateDocument(document);
		newOutDocument.setUnsafeCtime(new Date());
		newOutDocument.setPermissionsOn(false);
		DocumentKind documentKind = document.getDocumentKind();
		removeNotValidRecipient(sigleValues, tempMultiValues);
		moodifyRecipientValues(tempMultiValues);
		addRecipientToDocument(newOutDocument);
		//dodanie zmodyfikowanych warto�ci do dokumentu 
		oldValues.put(multiDictionaryValues, tempMultiValues);
		
		//jesli ma by� nadany numer Ko 
		if(assignKo)
			assignOfficeDocumentId(newOutDocument);
			
		/* zapisywanie  zmodyfikowanych danych */
		newOutDocument.getDocumentKind().logic().validate(oldValues, documentKind, newOutDocument.getId());
		documentKind.saveDictionaryValues(newOutDocument, oldValues);
		documentKind.saveMultiDictionaryValues(newOutDocument, oldValues);
		documentKind.set(newOutDocument.getId(), oldValues);
		
		documentKind.logic().archiveActions(newOutDocument, DocumentKindsManager.getType(newOutDocument));
		documentKind.logic().documentPermissions(newOutDocument);
		
		
		// tworzenie i uruchamianie wewn�trznego procesu workflow
		newOutDocument.getDocumentKind().logic().onStartProcess(newOutDocument);
		TaskSnapshot.updateAllTasksByDocumentId(newOutDocument.getId(), ((OfficeDocument) newOutDocument).getStringType());
		DSApi.context().session().saveOrUpdate(newOutDocument);
		return newOutDocument;
	}
	/**
	 * metoda usuwa nieprawid�owe warto�ci multiDictionaryValues na podstawie
	 *  wpisu kt�ry ma sie znale�� na dokumencie a pozosta�ymi warto�ciami slownika
	 * @param sigleValues
	 * @param tempMultiValues
	 */
	private static void removeNotValidRecipient(Map<String, Object> sigleValues, Map<String, Object> tempMultiValues)
	{
		Iterator<String> it = tempMultiValues.keySet().iterator();
		while (it.hasNext())
		{
			String key = it.next();
			// usuwanie starych warto�ci multi recipienta  
			if (!sigleValues.toString().contains(tempMultiValues.get(key).toString()))
				it.remove();
		}
		
	}

	/**
	 * Tworzy wpis w s�owniku pism wychod��cy nadaj�c numer Ko  pismu 
	 * @param newOutDocument 
	 * @throws EdmException 
	 */
	private static void assignOfficeDocumentId(OutOfficeDocument document) throws EdmException
	{
		Date entryDate = new Date();
		Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), document.getId(), entryDate);
		document.bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
		
	}

	/**
	 * Tworzy poprawny  pojedynczy wpis w s�owniku mdictionary values
	 * zamieniajac kolejnego recipienta na 1;
	 * kt�ry bedzie pozniej widoczny  na formatce 
	 * @param sigleValues 
	 * @param tempMultiValues
	 */
	private static void moodifyRecipientValues(Map<String, Object> tempMultiValues)
	{
		String _key = tempMultiValues.keySet().iterator().next();
		Object tmp = tempMultiValues.get(_key);
		tempMultiValues.remove(_key);
		_key = _key.substring(0, (_key.lastIndexOf('_'))) + "_1";
		tempMultiValues.put(_key, tmp);
	}

	/**
	 * Dodaje odbiorce na podstawie id persona z mapy pojedy�czego wpisu z multidictionary
	 * @param doc
	 * @throws EdmException
	 */
	
    private static void addRecipientToDocument(OutOfficeDocument doc) throws EdmException
	{
    	Recipient rec = new Recipient();
		Person p = new Person();
		FieldData fd = (FieldData) sigleValues.get(ID);
		long id = Long.parseLong(fd.getStringData());
		rec.recipientFromMap(p.find(id).toMap());
		rec.setBasePersonId(id);
		rec.create();
		doc.addRecipient(rec);
		
	}
    /**
     * Metoda tworzy klon dokument oraz za�acznik�w 
     * @param document
     * @return
     * @throws EdmException
     * @throws SQLException
     */
	private static OutOfficeDocument copyAttachmentAndCreateDocument(OfficeDocument document) throws EdmException, SQLException
	{
    	for (Attachment attach : Attachment.findAllAttachmentByDocumentId(document.getId()))
		{
			attachmentsToClone.add(attach.getId());
		}

		DocumentKind documentKind = document.getDocumentKind();

		OutOfficeDocument newDocument = (OutOfficeDocument) document.cloneObject((Long[]) attachmentsToClone.toArray(new Long[attachmentsToClone.size()]));
		return newDocument;
	}

	/**
     * kopiuje przekazan�  map� aby nie utracic danych 
     * @param values
     * @return copyValues
     */
    private static Map<String, Object> getCopyValues(Map<String, Object> values)
	{
    	Map<String,Object> temp = new HashMap<String, Object>();
    	for (String _key : values.keySet())
		{
    		temp.put(_key, values.get(_key));
		}
		return temp;
	}
	private static void setUpallValues(OfficeDocument doc ,Map<String,Object> values)
    {
		oldValues = getCopyValues(values);
    	officeDocument = doc;
		multiValues = (Map<String, Object>) oldValues.get(multiDictionaryValues);
    }
    
  
    
    
}
