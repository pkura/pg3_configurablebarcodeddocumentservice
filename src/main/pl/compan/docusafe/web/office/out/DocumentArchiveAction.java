package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;

import java.util.List;
/* User: Administrator, Date: 2007-02-28 14:35:13 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentArchiveAction extends DocumentArchiveTabAction
{
    public String getBaseLink()
    {
        return "/office/outgoing/document-archive.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/outgoing/external-workflow.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }

    public List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }
	public int getDocType() {
		return DocumentLogic.TYPE_OUT_OFFICE;
	}

}
