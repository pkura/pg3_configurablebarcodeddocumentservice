package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.DocumentVersionJcrTabAction;
import pl.compan.docusafe.web.office.in.Tabs;

import java.util.List;

public class DocumentVersionJcrAction extends DocumentVersionJcrTabAction
	{
	    protected List prepareTabs()
	    {
	    	 return new Tabs(this).createTabs();
	    }

	    public String getBaseLink()
	    {
	        return "/office/outgoing/document-version-jcr.action";
	    }

	    public String getDocumentType()
	    {
	        return OutOfficeDocument.TYPE;
	    }
		    
	}
