package pl.compan.docusafe.web.office.out;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.dockinds.process.WorkflowUtils;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.office.CloneDocumentManager;
import pl.compan.docusafe.web.office.common.SummaryTabAction;
import pl.compan.docusafe.web.office.common.SummaryTabAction.Graph;
import pl.compan.docusafe.web.office.common.SummaryTabAction.XesLogFromDocument;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SummaryAction.java,v 1.67 2010/02/22 07:33:03 mariuszk Exp $
 */
public class SummaryAction extends SummaryTabAction
{
	private final static Logger log = LoggerFactory.getLogger(SummaryAction.class);
    StringManager sm;
    // @EXPORT
    private OutOfficeDocument document;
    private String dispatchDivisionGuid;
    private String creatingUser;
    private String assignedDivision;
    private String caseDocumentId;
    private String cancelledByUser;
    private String caseLink;
    private List<Order> orders = new ArrayList<Order>();
    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    private static final String EV_FILL="fill";

    // @IMPORT
    /**
     * Lista zbiorczej dekretacji; ka�dy element tablicy ma posta�
     * nazwa_u�ytkownika "," CollectiveAssignmentEntry.id
     */
    private String[] assignments;
	private boolean canAssignOfficeNumber;
	public Integer officeNumber;
    
	private static final String EV_MANUAL_FINISH = "evManualFinish";
    private Collection<RenderBean> processRenderBeans;
	
    protected void setup()
    {

    	
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignments").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Assignments()).
	        append(EV_FILL,new FillDocumentForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doWatch").
            append(OpenHibernateSession.INSTANCE).
            append(new Watch()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAssignOfficeNumber").
	        //append(OpenHibernateSession.INSTANCE).
        append(OpenHibernateSession.INSTANCE).
        	append(new CanAssignOfficNumberSeter()).
	        append(new AssignOfficeNumber()).
	        //append(EV_FILL_CREATE, fillCreateForm).
	        append(new FillDocumentForm()).
        appendFinally(CloseHibernateSession.INSTANCE);

        
        registerListener("doManualFinish").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ManualFinish()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReopenWf").
            append(OpenHibernateSession.INSTANCE).
            append(new ReopenWf()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClone").
            append(OpenHibernateSession.INSTANCE).
            append(new CloneDocument()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCompileAttachments").
            append(OpenHibernateSession.INSTANCE).
            append(new CompileAttachments()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGiveAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new GiveAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    
        registerListener("doWithdrawAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new WithdrawAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGenerateDocumentView").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateDocumentView()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("sendToEva").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new SendToEva()).
	    	append(new FillDocumentForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
                       
        registerListener("sendToDispatchDivision").
    		append(OpenHibernateSession.INSTANCE).
    		append(new SendToDispatchDivision()).
    		append(new FillDocumentForm()).
    		appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doHotIcr").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new hotIcr()).	
	    	append(EV_FILL, new FillDocumentForm()).
	    	append(EV_MANUAL_FINISH ,new ManualFinish()).
	    	//append(new FillDocumentForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdateBox").
	        append(OpenHibernateSession.INSTANCE).
	        append(new UpdateBox()).
	        append(new FillDocumentForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDocumentMetrics").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DocumentMetrics()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doPrintCard").
        	append(OpenHibernateSession.INSTANCE).
        	append(new PrintCard()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetXesLog").
        append(OpenHibernateSession.INSTANCE).
        append(new XesLogFromDocument()).
        append(new FillDocumentForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetGraph").
        append(OpenHibernateSession.INSTANCE).
        append(new Graph()).
        append(new FillDocumentForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
    
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/summary.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/outgoing/external-workflow.action";
    }
    
    private class SendToDispatchDivision implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			
			try {
				DSApi.context().begin();								
				
				WorkflowFactory.simpleAssignment(getActivity(), 
						GlobalPreferences.getDispatchDivisionGuid(), 
						null, 
						DSApi.context().getPrincipalName(),
						null, 
						WorkflowFactory.SCOPE_DIVISION, 
						GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null).getString("doRealizacji"), 
						event);
				
				DSApi.context().commit();
				event.setResult("task-list");
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				log.debug("SummaryAction.java:189", e);
				event.addActionError(e.getMessage());
			}
		}
    	
    }
    
    private class CanAssignOfficNumberSeter implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
				canAssignOfficeNumber =Configuration.hasExtra("business")||(
						document!=null && 
				        OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(document.getPrepState()) &&
				        DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO));
				
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			}/*catch (NullPointerException e){
				Logger log = Logger.getLogger("debug");
				log.debug("OutOfficeDocument.PREP_STATE_FAIR_COPY="+OutOfficeDocument.PREP_STATE_FAIR_COPY);
				log.debug("document="+document);
				log.debug("DSApi.context()="+DSApi.context());
				throw e; 
			} */
        }
    }
    private class AssignOfficeNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date entryDate;
            Long journalId;
            try
            {


                entryDate = GlobalPreferences.getCurrentDay();

                // konieczne, aby sprawdzi�, �e dokument istnieje
                OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                // pisma wewn�trzne dostaj? numer KO w momencie tworzenia,
                // ale nie szkodzi umie?ci� tutaj rozr�nienia
                journalId = Journal.getMainOutgoing().getId();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {}

            Integer sequenceId;
            synchronized (Journal.class)
            {
                try
                {
                    sequenceId = Journal.TX_newEntry2(journalId, getDocumentId(), entryDate);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
            }

            try
            {

                DSApi.context().begin();

                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
                doc.bindToJournal(journalId, sequenceId);
                doc.setOfficeDate(new Date());

                DSApi.context().commit();
                try{
                    DSApi.context().session().flush();
                    }catch(HibernateException e){
                    	
                    }
                    DSApi.context().begin();
                    	TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),OutOfficeDocument.TYPE);
                    DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
            }
        }
    }
    
    
    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	dispatchDivisionGuid = GlobalPreferences.getDispatchDivisionGuid();
            	prepareLabels();
                Integer pom = (Integer) ServletActionContext.getRequest().getSession().getAttribute("offset");
               
                if(pom != null && pom%2 != 1)
                {
                    pom += 1;
                    ServletActionContext.getRequest().getSession().setAttribute("offset", pom);
                }
                
                document = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
                for(Recipient dr : document.getRecipients())
                	dr.getId();
                document.getSender();
                officeNumber = document.getOfficeNumber();
                canAssignOfficeNumber =Configuration.hasExtra("business")||(
                        OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(document.getPrepState()) &&
                        DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO));
                if (document.getContainingCase() != null)
                	caseLink = "/office/edit-case.do?id="+document.getContainingCase().getId()+"&tabId=summary";

                if(WorkflowFactory.jbpm) {
                    if(getActivity() != null){
                        processRenderBeans = ProcessUtils.renderProcessesForActivityId(document, getActivity(), ProcessActionContext.SUMMARY);
                    } else if(!AvailabilityManager.isAvailable("process.dontShowAll")) {
                        //jak nie ma activity to wy�wietlaj w archiwizacji tak jak w przypadku archiwum (ARCHIVE_VIEW)
                        processRenderBeans = ProcessUtils.renderAllProcesses(document, ProcessActionContext.SUMMARY);
                    }
                }

                WorkflowActivity activity = null;
                if (getActivity() != null ) {
                	activity = WorkflowFactory.getWfActivity(getActivity());
                    canAssignToCoordinator = WorkflowUtils.canAssignToCoordinator(getActivity());
                    if(WorkflowFactory.jbpm){
                        canAddToWatched = canAssignToCoordinator;
                    }
                }

                fillForm(document, activity);

                if (getActivity() != null ) {
                    canAssignToCoordinator = WorkflowUtils.canAssignToCoordinator(getActivity());
                    if(WorkflowFactory.jbpm){
                        canAddToWatched = canAssignToCoordinator;
                    }
                }
                
                if(!Docusafe.hasExtra("business"))
                {
                	PreparedStatement ps = null;
                    try
                    {                        
                        ps = DSApi.context().prepareStatement("select id, officeNumber from DSO_ORDER where DOCUMENT_ID = ? and CURRENTASSIGNMENTUSERNAME is not null" );
                        ps.setLong(1, getDocumentId());
                        ResultSet rs = ps.executeQuery();                        
                        while(rs.next())
                        {
                            orders.add(new Order(rs.getLong("id"),rs.getInt("officeNumber")));
                        }                        
                        rs.close();
                    }
                    catch (HibernateException e1)
                    {
                    	LogFactory.getLog("eprint").debug("", e1);

                    }
                    catch (SQLException e1)
                    {
                    	LogFactory.getLog("eprint").debug("", e1);
                    }
                    finally
                    {
                    	DSApi.context().closeStatement(ps);
                    }
                }
                
            }
            catch (EdmException e)
            {
            	log.error("",e);
                addActionError(e.getMessage());
            }
        }
    }

 /*   private class Watch implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                if (getActivity() != null)
                {
                    DSApi.context().watch(URN.create(DSApi.context().getWfActivity(ActivityId.parse(getActivity()))));
                }
                else
                {
                    DSApi.context().watch(URN.create(Document.find(getDocumentId())));
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    } */

    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                WorkflowFactory.getInstance().manualFinish(getActivity(), true);

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

               	TaskSnapshot.updateByDocumentId(document.getId(),OutOfficeDocument.TYPE);
               	DSApi.context().commit();

                event.setResult(getTaskListResult(document.getType()));
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Assignments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (assignments == null || assignments.length == 0)
            {
                addActionError(sm.getString("NieWybranoUzytkownikowDoDekretacji"));
                return;
            }

            try
            {
                DSApi.context().begin();

                boolean mainDocumentReassigned = collectiveAssignment(assignments, getActivity(), event);

                event.setResult("confirm");
                
                // TODO: process.terminate()
                if (mainDocumentReassigned)
                	 event.skip(EV_FILL);

                DSApi.context().commit();
               
 
            }
            catch (EdmException e)
            {
            	log.error("",e);
                //DSApi.context().setRollbackOnly();
                try { DSApi.context().rollback(); } catch (EdmException f) {};
                addActionError(e.getMessage());
            }
        }
    }

    
    private class ReopenWf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                pl.compan.docusafe.core.office.workflow.WorkflowUtils.reopenWf(getDocumentId());

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                TaskSnapshot.updateByDocumentId(document.getId(),OutOfficeDocument.TYPE);

                DSApi.context().commit();
                
                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                	LogFactory.getLog("workflow_log").debug(
                			"dla dokumentu przywroconego na liste zadan znaleziono "
                			+activityIds.length
                			+" zadan");
                } else {
                	setActivity(activityIds[0]);
                }
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class CloneDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            try
            {
                DSApi.context().begin();

                CloneDocumentManager.clone(getDocumentId());

                DSApi.context().commit();

                addActionMessage(sm.getString("DokumentZostalSklonowany"));

            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

        }
    }
    
    private class Order 
    {
        private Integer officeNumber;
        private Long id;

        public Order(Long id, Integer officeNumber)
        {
            this.id = id;
            this.officeNumber = officeNumber;

        }

        public Long getId()
        {
            return id;
        }
        public Integer getOfficeNumber()
        {
            return officeNumber;
        }
        
    }

    public OfficeDocument getDocument()
    {
        return document;
    }

    public String getCancelledByUser()
    {
        return cancelledByUser;
    }

    public void setAssignments(String[] assignments)
    {
        this.assignments = assignments;
    }

	public String getCaseLink() {
		return caseLink;
	}

	public void setCaseLink(String caseLink) {
		this.caseLink = caseLink;
	}
    public List<Order> getOrders()
    {
        return orders;
    }

    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

	public boolean isCanAssignOfficeNumber() {
		return canAssignOfficeNumber;
	}

	public void setCanAssignOfficeNumber(boolean canAssignOfficeNumber) {
		this.canAssignOfficeNumber = canAssignOfficeNumber;
	}

	public Integer getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(Integer officeNumber) {
		this.officeNumber = officeNumber;
	}

	public boolean isDispatchPossible(){
		//Logger.getLogger("MICHAEL").info("SummaryAction.java: isDispatchPossible = " + (dispatchOfficeGuid != null));
		
		return Configuration.additionAvailable(Configuration.ADDITION_DISPATCH_DIVISION) &&
			dispatchDivisionGuid != null && 
			document != null && 
			StringUtils.isNotEmpty(getActivity()) &&
			OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(document.getPrepState());//
	}

    public Collection<RenderBean> getProcessRenderBeans() {
        return processRenderBeans;
    }

    public void setProcessRenderBeans(Collection<RenderBean> processRenderBeans) {
        this.processRenderBeans = processRenderBeans;
    }
}
