package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.ElectronicSignaturesTabAction;

import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ElectronicSignaturesAction extends ElectronicSignaturesTabAction {
    private final static Logger LOG = LoggerFactory.getLogger(ElectronicSignaturesAction.class);

    @Override public String getDocumentType(){return OutOfficeDocument.TYPE;}
    @Override protected List<Tab> prepareTabs() {return new Tabs(this).createTabs();}
}