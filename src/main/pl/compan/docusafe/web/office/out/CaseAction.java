package pl.compan.docusafe.web.office.out;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.web.office.common.CaseTabAction;

import java.sql.PreparedStatement;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseAction.java,v 1.18 2008/10/06 09:33:11 pecet4 Exp $
 */
public class CaseAction extends CaseTabAction
{
 /*
	protected boolean setCase(OfficeDocument document, OfficeCase c) throws EdmException
    {
        document.setContainingCase(c,false);
        // TODO: numer w sprawie
        return false;
    }
*/
	 protected boolean setCase(OfficeDocument document, OfficeCase c) throws EdmException
	 {
	        if (document.getContainingCase() == null)
	        {
	            document.setContainingCase(c,false);

	            return false;
	        }
	        else
	        {
	            // klonowanie dokumentu
	            OutOfficeDocument clone = (OutOfficeDocument) document.cloneObject(null);
	            clone.setMasterDocument((OutOfficeDocument) document);
	            
	            //WorkflowFactory.createNewProcess(clone,true);

	            // nowy proces tylko wtedy, gdy pozwala na to ustawienie konfiguracyjne
	            // i gdy istnieje proces podstawowy (musi by� dost�pny modu� office)
	            if (Configuration.officeAvailable() &&
	                !StringUtils.isEmpty(getActivity()) &&
	                GlobalPreferences.isWfProcessForClonedInDocuments())
	            {
	                // tworzenie i uruchamianie wewn�trznego procesu workflow
	            	
	            	//tylko po co dwa razy?
	                WorkflowFactory.createNewProcess(clone,false,"Kopia przyj�tego pisma" , 
	                		DSApi.context().getPrincipalName(), 
	                		clone.getAssignedDivision(),
	                		DSApi.context().getPrincipalName());
	            	
	            	
	            }

	            clone.setContainingCase(c,false);
	            // TODO: symbol w sprawie

	            //BipUtils.writeXml(c);
                if (AvailabilityManager.isAvailable("setCaseIdAsDocumentNr", document.getDocumentKind().getCn()))
                {
                    PreparedStatement ps = null;
                    try {
                        ps = DSApi.context().prepareStatement("update dsg_normal_dockind set NR_DOKUMENTU = ? where document_id = ?");
                        ps.setString(1, document.getContainingCase().getOfficeId());
                        ps.setLong(2, document.getId());
                        ps.executeUpdate();
                        ps.close();

                        DSApi.context().closeStatement(ps);
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    } finally {
                        DSApi.context().closeStatement(ps);
                    }
                }
	            return true;
	        }
	    }
	
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/case.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
