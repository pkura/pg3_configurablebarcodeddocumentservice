package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.ImportMainTabAction;
import pl.compan.docusafe.web.office.massimport.FieldDataProvider;
import pl.compan.docusafe.web.office.massimport.FieldsHandler;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OutgoingImportAction extends ImportMainTabAction {

    protected Logger log = LoggerFactory.getLogger(OutgoingImportAction.class);

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
                .append(OpenHibernateSession.INSTANCE)
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate")
                .append(OpenHibernateSession.INSTANCE)
                .append(new ValidateCreate())
                .append(EV_CREATE, new ImportMainTabAction.Create())
                .append(EV_FILL, fillForm)
                .append(EV_REMOVEFM, new RemoveFMSessionAttribute())
                .appendFinally(CloseHibernateSession.INSTANCE);
    }


    public FieldsHandler getDefaultFieldsParser() {
        return new OutgoingFieldsHandler();
    }

    public static class OutgoingFieldsHandler extends FieldsHandler {

        private static final String DICT_RECIPIENT = "RECIPIENT";
        private static final String DICT_RECIPIENT_PARAM = "RECIPIENT_PARAM";
        private static final String DICT_SENDER_HERE = "SENDER_HERE";

        public OutgoingFieldsHandler() {
            addDictCn(DICT_RECIPIENT, DICT_SENDER_HERE);
        }


        @Override
        public Object getDictionaryId(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider fieldDataProvider, String cn) throws EdmException {

            if (DICT_RECIPIENT.equalsIgnoreCase(cn)) {
                boolean recepientParams = isRecepientParams(updatingDockindKeys);

                Person person = fieldDataProvider.getOrCreatePerson(DICT_RECIPIENT, getDictToFieldLinker());
                if (person == null) return null;

                Recipient recipient = new Recipient();
                recipient.fromMap(person.toMap());
                recipient.setDictionaryGuid("rootdivision");
                recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
                recipient.setBasePersonId(person.getId());
                recipient.setDocumentId(updatingDocument.getId());
                recipient.setDocument(updatingDocument);
                recipient.create();
                updatingDocument.addRecipient(recipient);

                if (recepientParams)
                    updatingDockindKeys.put(DICT_RECIPIENT_PARAM, recipient.toMap());
                return recipient.getId();
            }
            if (DICT_SENDER_HERE.equalsIgnoreCase(cn)) {
                String sender = fieldDataProvider.getSender(DICT_SENDER_HERE,getDictToFieldLinker());
                return sender;
            }

            return null;
        }

        private boolean isRecepientParams(Map<String, Object> dockindKeys) {
            Object recepientParams = dockindKeys.get(DICT_RECIPIENT_PARAM);
            return recepientParams != null && recepientParams instanceof Map && !((Map) recepientParams).isEmpty();
        }
        private boolean isRecepient(Map<String, Object> dockindKeys) {
            Object recepientParams = dockindKeys.get(DICT_RECIPIENT);
            return recepientParams != null && recepientParams instanceof Long;
        }

        @Override
        public boolean isRequired(Field field, Map<String, Object> dockindKeys) {
            String cn = field.getCn();
            if (DICT_RECIPIENT.equalsIgnoreCase(cn)) {
                if (isRecepientParams(dockindKeys) || isRecepient(dockindKeys))
                    return false;
            }
            return super.isRequired(field, dockindKeys);
        }
    }

}
