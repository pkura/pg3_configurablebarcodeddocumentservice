package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.WorkflowTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowAction.java,v 1.6 2006/12/19 16:08:46 lk Exp $
 */
public class WorkflowAction extends WorkflowTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/workflow.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/outgoing/external-workflow.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
