package pl.compan.docusafe.web.office.out;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyledDocument;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.json.JSONObject;

import pl.compan.docusafe.common.rtf.RTF;
import pl.compan.docusafe.common.rtf.RTFException;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OutOfficeDocumentReturnReason;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.bip.Bip;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;
import pl.compan.docusafe.service.eurzad.EUrzadService;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.MainTabAction;
import pl.compan.docusafe.web.office.common.RecipientsUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MainAction.java,v 1.170 2010/08/05 15:57:13 mariuszk Exp $
 */
public class MainAction extends MainTabAction
{
	private static final Logger log = LoggerFactory.getLogger(MainAction.class);
    private final static StringManager sm=
        GlobalPreferences.loadPropertiesFile(MainAction.class.getPackage().getName(),null);
    // @EXPORT (tylko odczytywane przez formularz, nie modyfikowane)
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private OutOfficeDocument document;
    private Map<String, String> recipientsMap;
    private InOfficeDocument inDocument;
    private DSUser[] users;
    private String officeNumber;
    private String creatingUser;
    private String assignedDivision;
    private Map<String, String> incomingDivisions;
    private boolean intoJournalPermission;
    private String caseDocumentId;
    private Map<String, Integer> journalMessages;
    private boolean readOnly;
    private List deliveries;
    private Map<String, String> epuapMethods;
    private boolean canAssignOfficeNumber;
    private OutOfficeDocumentDelivery delivery;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private Boolean canCreateWithOfficeNumber;
    private List returnReasons;
    private boolean currentDayWarning;
    private String personDirectoryDivisionGuid;
    private boolean blocked;
    private String ctime;

    // @IMPORT/EXPORT
    private Long inDocumentId;
    private String documentDate;
    private String description;
    private boolean senderAnonymous;
    private String senderTitle;
    private String senderFirstname;
    private String senderLastname;
    private String senderOrganization;
    private String senderStreetAddress;
    private String senderZip;
    private String senderLocation;
    private String senderCountry;
    private String senderEmail;
    private String senderFax;
    private String senderPesel;
    private String senderNip;
    private String senderRegon;
    private String inActivity;
    private String referenceId;
    private String weightKg;
    private String weightG;
    private Boolean priority;
    /**
     * Warto?� ustawiana po wys�aniu formularza przez u�ytkownika.
     * Warto?ciami s? wyra�enia OGNL tworz?ce obiekty java.util.Map
     * opisuj?ce odbiorc�w pisma.
     */
    private String[] recipients;
    private String postalRegNumber;
    private String prepState;
    private String assignedUser;
    private String zpoDate;
    private Long intoJournalId;
    private Integer deliveryId;
    private String epuapMethodId;
    private String barcode;
    private String reason;
    private String stampFee;
    private boolean daa;
    private boolean canCreateMultiReplies;
    private Boolean createMultiReplies;

    private FormFile file;
    private String identifikatorEpuap;
    private String adresSkrytkiEpuap;
    private boolean save;

    // @IMPORT
    private Boolean assignOfficeNumber;
    private Integer returnReasonId;
    private boolean returned;
    private Long grantRequestId;
    private String group;   /* nazwa grupy osob, ktora dodamy do Recipients */
    private boolean nextReply;
	public Long senderId;
	private boolean attachCase;

    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_CREATE = "create";
    public static final String CREATED = "created"; // atrybut ActionEvent
    
    
    public static final String EPUAP_SKRYTKA= "Przed�o�enie";
    public static final String EPUAP_DORECZYCIEL= "Dor�czenie";

    public String getBaseLink()
    {
        return "/office/outgoing/main.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }

    protected void setup()
    {
        FillPulldowns fillPulldowns = new FillPulldowns();
        FillCreateForm fillCreateForm = new FillCreateForm();
        FillDocumentForm fillDocumentForm = new FillDocumentForm(true);

        // ogl?danie istniej?cego dokumentu
        registerListener(DEFAULT_ACTION).
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        // tworzenie nowego dokumentu
        registerListener("doNewDocument").
            //append(OpenHibernateSession.INSTANCE).
            append(new FillNewDocumentForm()).
            append(fillPulldowns);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAutoCreate").
        	append(new FillNewDocumentForm()).
        	append(fillPulldowns).
        	append(new Create()).
            append(EV_FILL_CREATE, fillCreateForm).
            append(EV_FILL_DOCUMENT, fillDocumentForm);

        registerListener("doCreate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            //append(new FillCreateUpdateForm()).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(EV_FILL_CREATE, fillCreateForm).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSendToEpuap").
	        //append(OpenHibernateSession.INSTANCE).
	        append(fillPulldowns).
	        //append(new FillCreateUpdateForm()).
	        append(new Update()).
	        append(new SendToEpuap()).
	        append(EV_FILL_CREATE, fillCreateForm).
	        append(EV_FILL_DOCUMENT, fillDocumentForm);
	        //appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            //append(new FillCreateUpdateForm()).
            append(new Update()).
            append(EV_FILL_CREATE, fillCreateForm).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignOfficeNumber").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new AssignOfficeNumber()).
            //append(EV_FILL_CREATE, fillCreateForm).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSplitAndAssignOfficeNumber").
            //append(OpenHibernateSession.INSTANCE).
            //append(fillPulldowns).
            append(new SplitAndAssignOfficeNumber()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSplit").
            //append(OpenHibernateSession.INSTANCE).
            //append(fillPulldowns).
            append(new Split()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doIntoJournal").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new IntoJournal()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddGroupToRecipients").
            append(fillPulldowns).
            append(new AddGroupToRecipients()).
            append(new FillDocumentForm(false));
        
        registerListener("doGenerateCSV").
	        append(fillPulldowns).
	        append(new GenerateCSVFromRecipients()).
	        append(new FillDocumentForm(false));
    }

    private class SendToEpuap implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            	DSApi.context().begin();
            	if( epuapMethodId.equals(EPUAP_SKRYTKA))
            		EpuapExportManager.sendToEpuap(getDocumentId(),true,EpuapExportDocument.USLUGA_SKRYTKA);
            	else if( epuapMethodId.equals(EPUAP_DORECZYCIEL))
            		EpuapExportManager.sendToEpuap(getDocumentId(),true,EpuapExportDocument.USLUGA_DORECZYCIEL);
            	DSApi.context().commit();
            	addActionMessage(sm.getString("WyslanoDoEpuap"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                LOG.error(e.getMessage(),e);
            }
            finally
            { 
                try { DSApi.close(); } catch (Exception e) { } 
            }
        }
    }
            
    
    private class FillPulldowns implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                setTabs(new Tabs(MainAction.this).createTabs());

                //users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                users = DSDivision.find(DSDivision.ROOT_GUID).getUsersFromDivision(DSApi.context().getDSUser().getDivisions());
                UserFactory.sortUsers(users, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                deliveries = OutOfficeDocumentDelivery.list();
                // ePUAP
                
                epuapMethods = new LinkedHashMap<String,String>();
                epuapMethods.put(EPUAP_DORECZYCIEL,EPUAP_DORECZYCIEL);
                epuapMethods.put(EPUAP_SKRYTKA, EPUAP_SKRYTKA);
                
                returnReasons = OutOfficeDocumentReturnReason.list();
                //canAssignOfficeNumber = DSApi.context().hasPermission(Permissions.PISMO_WYCH_PRZYJECIE_KO);
                // kody kreskowe
                useBarcodes = Boolean.valueOf(GlobalPreferences.isUseBarcodes());
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();

                canCreateWithOfficeNumber = new Boolean(
                    DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO) &&
                    DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA_CZYSTOPISU));

                canCreateMultiReplies = DSApi.context().hasPermission(DSPermission.PISMO_WYCH_WIELE_ODPOWIEDZI);
                
                //zablokowanie mozliwosci zapisu
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.oznaczJakoCzystopis")){
                	Document doc = Document.find(documentId);
                	if(doc.getCzyCzystopis())
                		save=true;
                	
            	}
            }
            catch (EdmException e)
            {
            	saveFile();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Odczytuje dane dokumentu.
     */
    private class FillDocumentForm implements ActionListener
    {
        boolean fillDocumentFields;

        public FillDocumentForm(boolean fillDocumentFields)
        {
            this.fillDocumentFields = fillDocumentFields;
        }

        public void actionPerformed(ActionEvent event)
        {
           // sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (getDocumentId() == null)
            {
                if (!createMultiReplies && group == null)
                {
                    if (Configuration.officeAvailable())
                        event.setResult("task-list-out");
                    else
                        event.setResult("simpleoffice-created");
                }
                event.cancel();
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                daa = (doc.getDoctype() != null) && ("daa".equals(doc.getDoctype().getCn()));

                if (doc.isInternal())
                    throw new EdmException(sm.getString("DokumentJestPismemWewnetrznym",doc.getId()));

                DSApi.initializeProxy(doc.getReturnReason());

                document = doc;
                readOnly = !((OutOfficeDocument) document).canBeUpdate();

                personDirectoryDivisionGuid =
                    document.getCurrentAssignmentGuid() != null ?
                    document.getCurrentAssignmentGuid() : document.getAssignedDivision();

                intoJournalPermission = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
                boolean a1 =OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(doc.getPrepState());
                	boolean a2 =DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO);
                		//boolean a3 =
                canAssignOfficeNumber =Configuration.hasExtra("business")||( a1 &&  a2);
                referenceId = doc.getReferenceId();
                if (fillDocumentFields)
                {
                    officeNumber = doc.getFormattedOfficeNumber();
                    inDocument = doc.getInDocument();
                    prepState = doc.getPrepState();
                    caseDocumentId = doc.getCaseDocumentId();
                    delivery = doc.getDelivery(); DSApi.initializeProxy(delivery);
                    stampFee = doc.getStampFee() != null ? doc.getStampFee().toString() : null;
                }
                if(doc.getWeightG()==null)
                	weightG = null;
                else
                	weightG = doc.getWeightG().toString();
                if(doc.getWeightKg()==null)
                	weightKg = null;
                else
                	weightKg = doc.getWeightKg().toString();
                priority = doc.getPriority();
                journalMessages = new LinkedHashMap<String, Integer>();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                if (fillDocumentFields)
                {
                    creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                    assignedUser = doc.getClerk();
                    assignedDivision = DSDivision.find(doc.getAssignedDivision()).getPrettyPath();
                    documentDate = doc.getDocumentDate() != null ?
                        DateUtils.formatJsDate(doc.getDocumentDate()) : null;
                    zpoDate = doc.getZpoDate() != null ?
                        DateUtils.formatJsDate(doc.getZpoDate()) : null;
                    description = doc.getSummary();
                    barcode = doc.getBarcode();

                    if (doc.getSender() != null)
                    {
                        senderTitle = doc.getSender().getTitle();
                        senderFirstname = doc.getSender().getFirstname();
                        senderLastname = doc.getSender().getLastname();
                        senderOrganization = doc.getSender().getOrganization();
                        senderStreetAddress = doc.getSender().getStreet();
                        senderZip = doc.getSender().getZip();
                        senderLocation = doc.getSender().getLocation();
                        senderCountry = doc.getSender().getCountry();
                        senderEmail = doc.getSender().getEmail();
                        senderFax = doc.getSender().getFax();
                        senderId = doc.getSenderId();
                    }

                    if (doc.getRecipients().size() > 0)
                    {
                        recipientsMap = new LinkedHashMap<String, String>();
                        for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
                        {
                            Recipient recipient = (Recipient) iter.next();
                            JSONObject ono = new JSONObject(recipient.toMap());
                            recipientsMap.put(ono.toString(), recipient.getSummary());
                        }
                    }
                    postalRegNumber = doc.getPostalRegNumber();
                }

                if (true/*doc.getOfficeNumber() != null*/)
                {
                    // lista dzia��w, w kt�rych mo�na przyj?� bie�?cy dokument do dziennika
                    incomingDivisions = new LinkedHashMap<String, String>();
                    if (DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE))
                    {
                        List docJournals = Journal.findByType(Journal.OUTGOING);
                        for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                        {
                            Journal j = (Journal) iter.next();
                            try
                            {
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(),
                                        (j.getOwnerGuid() != null ?
                                            DSDivision.find(j.getOwnerGuid()).getName()+" - " : "")
                                        +j.getDescription());
                            }
                            catch (DivisionNotFoundException e)
                            {
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(),
                                        "[nieznany dzia�] - " +
                                        j.getDescription());
                            }
                        }
                    }
                    else
                    {
                        boolean przyjeciePismaKomorkaNadrzedna =
                            DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                        for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                        {
                            // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                            List docJournals = Journal.findByDivisionGuid(division.getGuid(), Journal.OUTGOING);
                            for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                            {
                                Journal j = (Journal) iter.next();
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(), division.getName()+" - "+j.getDescription());
                            }

                            // dzienniki w kom�rkach nadrz�dnych
                            if (przyjeciePismaKomorkaNadrzedna)
                            {
                                DSDivision parent = division.getParent();
                                while (parent != null && !parent.isRoot())
                                {
                                    // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                    List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), Journal.OUTGOING);
                                    for (Iterator iter=docParentJournals.iterator(); iter.hasNext(); )
                                    {
                                        Journal j = (Journal) iter.next();
                                        if (j.findDocumentEntry(getDocumentId()) == null)
                                            incomingDivisions.put(j.getId().toString(), parent.getName()+" - "+j.getDescription());
                                        // by�o divisions[i].getName()
                                    }
                                    parent = parent.getParent();
                                }
                            }
                        }
                    }
                    
                    
                    
                    /*boolean przyjeciePismaKomorkaNadrzedna =
                        DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                    DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
                    for (int i=0; i < divisions.length; i++)
                    {
                        List docJournals = Journal.findByDivisionGuid(divisions[i].getGuid(), Journal.OUTGOING);
                        for (Iterator iter=docJournals.iterator(); iter.hasNext(); )
                        {
                            Journal j = (Journal) iter.next();
                            if (j.findDocumentEntry(getDocumentId()) == null)
                                incomingDivisions.put(j.getId().toString(), divisions[i].getName()+" - "+j.getDescription());
                        }

                        // dzienniki w kom�rkach nadrz�dnych
                        if (przyjeciePismaKomorkaNadrzedna)
                        {
                            DSDivision parent = divisions[i].getParent();
                            while (parent != null && !parent.isRoot())
                            {
                                // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), Journal.OUTGOING);
                                for (Iterator iter=docParentJournals.iterator(); iter.hasNext(); )
                                {
                                    Journal j = (Journal) iter.next();
                                    if (j.findDocumentEntry(getDocumentId()) == null)
                                        incomingDivisions.put(j.getId().toString(), parent.getName()+" - "+j.getDescription());
                                }
                                parent = parent.getParent();
                            }
                        }
                    }

                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("intoJournalPerm="+intoJournalPermission+
                            " inDivisions="+incomingDivisions);*/
                }

                /* czy zablokowany przed edycja */
                blocked = (document.getBlocked() != null && document.getBlocked());

            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class FillCreateForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           // sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            event.skip(EV_FILL_DOCUMENT);

            prepState = OutOfficeDocument.PREP_STATE_DRAFT;

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                if (recipients != null)
                {
                    recipientsMap = new LinkedHashMap<String, String>();

                    // tylko Update
                    if (getDocumentId() != null)
                    {
                        OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
                        inDocument = doc.getInDocument();
                        prepState = doc.getPrepState();
                    }

                    for (int i=0; i < recipients.length; i++)
                    {
/*
                        Map personMap = (Map) new OgnlExpression(recipients[i]).getTotal();
                        recipientsMap.put(recipients[i],
                            (personMap.get("firstname") != null ? (String) personMap.get("firstname")+" " : "") +
                            (personMap.get("lastname") != null ? (String) personMap.get("lastname") : "") +
                            "/" +
                            (personMap.get("organization") != null ? (String) personMap.get("organization") : "") +
                            "/" +
                            (personMap.get("location") != null ? (String) personMap.get("location") : ""));
*/
                        recipientsMap.put(recipients[i], new Person().fromJSON(recipients[i]).getSummary());
                    }
                }


            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu"));
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Wype�nia w�asno?ci u�ywane przez formularz tworzenia
     * nowego dokumentu.
     * <p>
     * U�ywane r�wnie� przed akcj? tworz?c? nowy dokument.
     */
    private class FillNewDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
//            if (inDocumentId == null)
//                return;
            event.getLog().debug("MAIN in:"+inActivity);
            prepState = OutOfficeDocument.PREP_STATE_DRAFT;

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    addActionError("Brak uprawnienia do przyjmowania pism w KO");

                InOfficeDocument doc = null;

                if (inDocumentId != null && !nextReply)
                {
                    try
                    {
                        doc = InOfficeDocument.findInOfficeDocument(inDocumentId);

                        if (doc.getOutgoingDelivery() != null)
                            deliveryId = doc.getOutgoingDelivery().getId();

                        description = sm.getString("OdpowiedzNaPismoNr")+" "+doc.getOfficeNumber()+
                            (doc.getDocumentDate() != null ?
                                " "+sm.getString("Zdnia")+" "+DateUtils.formatCommonDate(doc.getDocumentDate()) :
                            "");

                        if (!doc.getRecipients().isEmpty())
                        {
                            Recipient recipient = (Recipient) doc.getRecipients().get(0);
                            senderTitle = recipient.getTitle();
                            senderFirstname = recipient.getFirstname();
                            senderLastname = recipient.getLastname();
                            senderOrganization = recipient.getOrganization();
                            senderStreetAddress = recipient.getStreet();
                            senderZip = recipient.getZip();
                            senderLocation = recipient.getLocation();
                            senderCountry = recipient.getCountry();
                            senderEmail = recipient.getEmail();
                            senderFax = recipient.getFax();
                        }

                        if (doc.getSender() != null)
                        {
                            Sender sender = doc.getSender();
                            if (!sender.isAnonymous() &&
                                !(StringUtils.isEmpty(sender.getFirstname()) &&
                                  StringUtils.isEmpty(sender.getLastname()) &&
                                  StringUtils.isEmpty(sender.getOrganization())))
                            {
                                recipientsMap = new HashMap<String, String>();
                                recipientsMap.put(
                                    new JSONObject(sender.toMap()).toString(),
                                    sender.getSummary());
                                recipients = new String[1];
                                recipients[0] = new JSONObject(sender.toMap()).toString();
                            }
                        }
                    }
                    catch (DocumentNotFoundException e)
                    {
                    }
                }
                else  if (inDocumentId != null)
                {
                    try
                    {
                        doc = InOfficeDocument.findInOfficeDocument(inDocumentId);

                        if (recipientsMap == null || recipientsMap.isEmpty())
                        {
                            OutOfficeDocument lastReply = doc.getLastOutDocuments();
                            if (lastReply != null && lastReply.getRecipients().size() > 0)
                            {
                                recipientsMap = new LinkedHashMap<String, String>();
                                for (Iterator iter = lastReply.getRecipients().iterator(); iter.hasNext(); )
                                {
                                    Recipient recipient = (Recipient) iter.next();
                                    JSONObject ono = new JSONObject(recipient.toMap());
                                    recipientsMap.put(ono.toString(), recipient.getSummary());
                                }
                            }
                        }
                    }
                    catch (DocumentNotFoundException e)
                    {
                    }
                }

                if (doc == null)
                {
                    Map address = GlobalPreferences.getAddress();
                    senderOrganization = (String) address.get(GlobalPreferences.ORGANIZATION);
                    senderStreetAddress = (String) address.get(GlobalPreferences.STREET);
                    senderZip = (String) address.get(GlobalPreferences.ZIP);
                    senderLocation = (String) address.get(GlobalPreferences.LOCATION);
                    senderCountry = "PL";
                }

                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

/*
    private class FillCreateUpdateForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (recipients != null)
            {
                recipientsMap = new LinkedHashMap();

                try
                {
                    // tylko Update
                    if (getDocumentId() != null)
                    {
                        OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
                        inDocument = doc.getInDocument();
                        prepState = doc.getPrepState();
                    }

                    for (int i=0; i < recipients.length; i++)
                    {
                        Map personMap = (Map) new OgnlExpression(recipients[i]).getTotal();
                        recipientsMap.put(recipients[i],
                            (personMap.get("firstname") != null ? (String) personMap.get("firstname")+" " : "") +
                            (personMap.get("lastname") != null ? (String) personMap.get("lastname") : "") +
                            "/" +
                            (personMap.get("organization") != null ? (String) personMap.get("organization") : ""));
                    }
                }
                catch (EdmException e)
                {
                    event.getLog().error(e.getMessage(), e);
                    addActionError("B�?d podczas tworzenia dokumentu. Skontaktuj si� z administratorem systemu.");
                    return;
                }
            }
        }
    }
*/

    private class IntoJournal implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
          //  sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (intoJournalId == null)
                return;

            DSDivision division;
            Date entryDate;
            Journal journal;

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                if (!DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoPrzyjmowaniaPismaWdziale"));

                Long documentId = getDocumentId();

                // DocumentNotFoundException
                OfficeDocument.findOfficeDocument(documentId);

                journal = Journal.find(intoJournalId);

                if (journal.findDocumentEntry(documentId) != null)
                    throw new EdmException(sm.getString("PismoZnajdujeSieJuzWwybranymDzienniku"));

                division = DSDivision.find(journal.getOwnerGuid());

                entryDate = GlobalPreferences.getCurrentDay();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            Integer sequenceId;
            try
            {
                sequenceId = Journal.TX_newEntry2(journal.getId(), getDocumentId(), entryDate);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                document.setAssignedDivision(division.getGuid());

                addActionMessage(sm.getString("PismoZostaloPrzyjeteWdziennikuDzialuPodNumerem",division.getName(),sequenceId));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }


    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           // sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            // walidacja daty
        	if(TextUtils.nullSafeParseBigDecimal(stampFee, 2) == null && stampFee != null)
        		addActionError(sm.getString("WartoscZnaczkaNieJestLiczba"));
        	if (!StringUtils.isEmpty(zpoDate) && DateUtils.parseJsDate(zpoDate) == null && zpoDate != null)
                addActionError(sm.getString("FormatDatyJestNiepoprawny"));
            if (!StringUtils.isEmpty(documentDate) && DateUtils.parseJsDate(documentDate) == null)
                addActionError(sm.getString("NiepoprawnaDataPisma"));

            if (StringUtils.isEmpty(description))
                addActionError(sm.getString("NiePodanoOpisuPisma"));

            if (hasActionErrors())
            {
            	saveFile();
                event.skip(EV_CREATE);
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			//if (!StringUtils.isEmpty(zpoDate) && DateUtils.parseJsDate(zpoDate) == null)
			    //addActionError(sm.getString("FormatDatyJestNiepoprawny"));
       //   sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
			if (deliveryId == null)
			    addActionError(sm.getString("NieWybranoSposobuOdbioruPisma"));

			if (recipients == null || recipients.length == 0)
			    addActionError(sm.getString("NiePodanoOdbiorcyPisma"));

			if (hasActionErrors())
			{
				saveFile();
			    return;
			}

			if (createMultiReplies == null)
			    createMultiReplies = Boolean.FALSE;

			int repliesCount = 1;
			if (createMultiReplies)
			{
			    if (!canCreateMultiReplies)
			    {
			    	saveFile();
			        addActionError(sm.getString("BrakUprawnienDoTworzeniaWieluOdpowiedziJednoczesnie"));
			        return;
			    }
			    repliesCount = recipients.length;
			}

			for (int j=0; j < repliesCount; j++)
			{
			    try
			    {
			        create(event, j);
			    }
			    catch (EdmException e)
			    {
			        if (createMultiReplies)
			        {
			            try 
			            {
			                Recipient rec = new Recipient().fromJSON(recipients[j]);
			                addActionError(sm.getString("NieUdaloSieUtworzycPismaDlaOdbiorcy")+" : " +rec.getShortSummary()/*Firstname()+" "+rec.getLastname()*/);
			            }
			            catch (EdmException f)
			            {
			            }
			        }
			    }
			}

			if (!hasActionErrors())
			{
				if(attachCase)
	            {
	            	event.setResult("case");
	            	event.cancel();
	            }
				else
			    if (grantRequestId != null)
			    {
			        event.setResult("task-list-out");
			    }
			    else if (inDocumentId != null)
			    {
			        event.setResult("reply-created");
			    }
			    else if (Configuration.officeAvailable())
			    {
			        event.setResult("task-list-out");
			    }
			    else
			    {
			        event.setResult("simpleoffice-created");
			    }

			    event.skip(EV_FILL_CREATE);
			}
			else
			{
				saveFile();
			}
        }
    }


    private void create(ActionEvent event, int j) throws EdmException
    {
    	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
    	DSApi.context().begin();
      //  sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
        OutOfficeDocument doc = new OutOfficeDocument();

        doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        doc.setDivisionGuid(DSDivision.ROOT_GUID);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        //if(zpoDate != null)
        	//doc.setZpoDate(DateUtils.nullSafeParseJsDate(zpoDate));
        Sender sender = new Sender();
        sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
        sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
        sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
        sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
        sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
        sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
        sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
        sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
        sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
        sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));

        doc.setSender(sender);

        // warto?ci okre?laj?ce odbiorc�w pobrane z formularza s?
        // skryptami OGNL tworz?cymi obiekty Map zawieraj?ce w�asno?ci
        // obiektu Person

        Date entryDate;
        Long journalId;


        try
        {
//                if (assignOfficeNumber != null && assignOfficeNumber.booleanValue() &&
//                    !DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");

            // trzeba mie� uprawnienia do nadawania numeru KO
            if (assignOfficeNumber != null && assignOfficeNumber.booleanValue() &&
                !DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
                throw new EdmException(sm.getString("BrakUprawnienDoNadawaniaNumeruKancelaryjnego"));

            entryDate = GlobalPreferences.getCurrentDay();

            doc.setPostalRegNumber(postalRegNumber);

            doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
            doc.setCreatingUser(DSApi.context().getPrincipalName());
            doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

            doc.setAssignedDivision(DSDivision.ROOT_GUID);

            if(weightG!=null)
            {
            	try
            	{
                	doc.setWeightG(Integer.parseInt(weightG));
            	}
            	catch (NumberFormatException  e)
            	{
            		throw new EdmException("Nie mo�na skonwertowa� warto�ci "+weightG+" do liczby rzeczywistej");
				}

            }else{
            	doc.setWeightG(null);
            }
            if(weightKg!=null)
            {
            	try
            	{
            		doc.setWeightKg(Integer.parseInt(weightKg));
            	}
            	catch (NumberFormatException  e)
            	{
            		throw new EdmException("Nie mo�na skonwertowa� warto�ci "+weightKg+" do liczby rzeczywistej");
				}
            	
            }else{
            	doc.setWeightKg(null);
            }
            if(priority==null || priority.booleanValue()==false){
            	doc.setPriority(new Boolean(false));
            }
            else
            	doc.setPriority(new Boolean(true));

            doc.setClerk(DSApi.context().getPrincipalName());
            doc.setCaseDocumentId(caseDocumentId);
            doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));

            doc.setDelivery(OutOfficeDocumentDelivery.find(deliveryId));
            doc.setStampFee(TextUtils.nullSafeParseBigDecimal(stampFee, 2));

            doc.setZpoDate(DateUtils.nullSafeParseJsDate(zpoDate));

            InOfficeDocument indoc = null;
            if (inDocumentId != null)
            {
                indoc = InOfficeDocument.findInOfficeDocument(inDocumentId);
                doc.setInDocumentId(indoc.getId());

                // ze wzgl�du na interfejs BIP, kt�ry iteruje kolekcj�
                // indoc.outDocuments
                indoc.addOutDocument(doc);

                OfficeCase c = indoc.getContainingCase();
                if (c != null)
                {
                    doc.setContainingCase(c,false);
                }
            }

            doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));
            
            if (recipients != null)
            {
                Map personMap = null;
                //String ognlExpr = null;
                String json = null;
                try
                {
                	if(createMultiReplies)
                	{
                		json = recipients[j];
                        Recipient recipient = new Recipient();
                        recipient.fromJSON(json);
                        doc.addRecipient(recipient);
                	}
                	else
                	{
	                    for (int i=0; i < recipients.length; i++)
	                    {
	                        json = recipients[i];
	                        Recipient recipient = new Recipient();
	                        recipient.fromJSON(json);
	                        doc.addRecipient(recipient);
	                    }
                	}
                }
                catch (EdmException e)
                {
                	saveFile();
                    event.getLog().error(e.getMessage(), e);
                    addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu")+" json="+json+
                        " personMap="+personMap);
                    return;
                }
            }

            doc.create();

            if(StringUtils.isEmpty(doc.getBarcode()))
            {
            	doc.setBarcode(BarcodeUtils.getKoBarcode(doc));
            }
            if (inDocumentId != null)
            {
                if("eurzad".equals(indoc.getSender().getLparam()))
                {
                	EUrzadService.addOutgoingToEurzad(doc, indoc.getId());
                }
            }
            doc.setPrepState1(prepState);
			//doc.setPrepState(prepState);
            newDocumentId = doc.getId();
            if (event.getLog().isDebugEnabled())
                event.getLog().debug("newDocumentId="+newDocumentId);



            if (assignOfficeNumber != null && assignOfficeNumber.booleanValue())
            {
                doc.setPrepState(OutOfficeDocument.PREP_STATE_FAIR_COPY);
                //doc.assignOfficeNumber();
            }

            Journal journal = Journal.getMainOutgoing();
            journalId = journal.getId();

            if (file != null && file.sensible())
            {
                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                doc.createAttachment(attachment);

                attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
            }
            addFiles(doc);

            DSApi.context().commit();

        }
        catch (EdmException e)
        {
        	log.error(e.getMessage(),e);
        	addActionError(e.getMessage());
            DSApi.context().setRollbackOnly();            
            throw new EdmException(sm.getString("BladPodczasTworzenia"));
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }




        if (grantRequestId != null)
        {
            createGrantRequestAttachment(newDocumentId);
        }

        try
        {
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            DSApi.context().begin();
            Integer sequenceId = null;
            if (assignOfficeNumber != null && assignOfficeNumber.booleanValue())
            {
                try
                {
                    sequenceId = Journal.TX_newEntry2(journalId, newDocumentId, entryDate);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
            }
            InOfficeDocument indoc = null;
            if (assignOfficeNumber != null && assignOfficeNumber.booleanValue())
            {
                doc = OutOfficeDocument.findOutOfficeDocument(newDocumentId);
                indoc = doc.getInDocument();
                doc.bindToJournal(journalId, sequenceId);
            }

            if (Configuration.officeAvailable())
            {
                // tworzenie i uruchamianie wewn�trznego procesu workflow
                WorkflowFactory.createNewProcess(doc, false, "PismoUtworzone");
            }


            DSApi.context().commit();

            try
            {
                DSApi.context().session().flush();
            }
            catch(HibernateException e){
            }

            DSApi.context().begin();
            TaskSnapshot.updateByDocumentId(doc.getId(),OutOfficeDocument.TYPE);
            if (inDocumentId != null)
                // trzeba uaktualnic ze wzgledu na zmiane liczby odpowiedzi na pismo
                TaskSnapshot.updateByDocumentId(inDocumentId, InOfficeDocument.TYPE);
            DSApi.context().commit();

            if (indoc != null && indoc.isSubmitToBip())
            {
            	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(indoc.getId());
            	/*
                try
                {
                    Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                    bip.submit(indoc);
                }
                catch (ServiceException e)
                {
                    event.getLog().error(e.getMessage(), e);
                }
                */
            }


            AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                doc.getId(),
                doc.getRecipients());

            // parametr u�ywany przez result=created w xwork.xml
            if (!createMultiReplies)
                setDocumentId(doc.getId());


            if (grantRequestId != null)
            {
            }
            else if (inDocumentId != null)
            {
                addActionMessage(sm.getString("UtworzonoPismo") +
                    (doc.getOfficeNumber() != null ? " "+sm.getString("nr")+" "+doc.getOfficeNumber() : ""));
            }
            else if (Configuration.officeAvailable())
            {
            }
            else
            {
                addActionMessage(sm.getString("UtworzonoPismo") +
                    (doc.getOfficeNumber() != null ? " "+sm.getString("nr")+" "+doc.getOfficeNumber() : ""));
            }

        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            addActionError(e.getMessage());
            throw new EdmException(sm.getString("BladPodczasTworzenia"));
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }

    }


    private class AddGroupToRecipients implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	saveFile();
            if (createMultiReplies == null)
                createMultiReplies = Boolean.FALSE;

            QueryForm form = new QueryForm(0, 0);

            if (!StringUtils.isEmpty(group))
            {
                form.addProperty("personGroup", group.trim());

                try
                {
                    DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                    DSApi.context().begin();
                    recipientsMap = new LinkedHashMap<String, String>();

                    SearchResults<? extends Person> results = Person.search(form);

                    if (results.totalCount() == 0)
                        addActionMessage(sm.getString("NieZnalezionoOsobWpodanejGrupie"));
                    else
                    {
                        Person[] rec = results.results();
                        if (recipients == null)
                            recipients = new String[0];
                        for (int i=0; i < rec.length; i++)
                        {
                        	Recipient recipient = new Recipient().recipientFromMap(rec[i].toMap());
                        	recipient.setPersonGroup(null);
                        	//DSApi.context().session().save(recipient);
                        	recipientsMap.put(new JSONObject(recipient.toMap()).toString(), recipient.getSummary());
                        }

                        for (int i=0; i < recipients.length; i++)
                        {
                            Recipient recipient = new Recipient().fromJSON(recipients[i]);
                            recipientsMap.put(recipients[i], recipient.getSummary());
                        }
                    }
                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                	log.error("",e);
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
                finally
                {
                    try { DSApi.close(); } catch (Exception e) { }
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
         //   sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (StringUtils.isEmpty(description))
                addActionError(sm.getString("NiePodanoOpisuPisma"));

            if(TextUtils.nullSafeParseBigDecimal(stampFee, 2) == null && stampFee != null)
        		addActionError(sm.getString("WartoscZnaczkaNieJestLiczba"));

            if (!StringUtils.isEmpty(zpoDate) && DateUtils.parseJsDate(zpoDate) == null)
                addActionError(sm.getString("FormatDatyJestNiepoprawny"));

            if (returned && returnReasonId == null)
                addActionError(sm.getString("OznaczonoPismoJakoZwroconeAleNieWybranoPrzyczynyZwrotu"));

            if (returnReasonId != null && !returned)
                addActionError(sm.getString("WybranoPrzyczyneZwrotuPismaAleNieOznaczonoPismaJakoZwroconego"));

            if (hasActionErrors())
            {
                event.skip(EV_FILL_DOCUMENT);
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                if (OutOfficeDocument.PREP_STATE_DRAFT.equals(prepState) &&
                    OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(doc.getPrepState()) &&
                    !DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoZmianyCzystopisuNaBrudnopis"));

                if (OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(prepState) &&
                    OutOfficeDocument.PREP_STATE_DRAFT.equals(doc.getPrepState()) &&
                    !DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA_CZYSTOPISU))
                    throw new EdmException(sm.getString("BrakUprawnienDoAkceptacjiCzystopisu"));


                Sender sender = doc.getSender();
                if(sender == null)
                {
                	sender = new Sender();
                	doc.setSender(sender);
                }
                sender.setAnonymous(false);
                sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
                sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
                sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
                sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
                sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
                sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
                sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
                sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
                sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
                sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));

                // warto?ci okre?laj?ce odbiorc�w pobrane z formularza s?
                // skryptami OGNL tworz?cymi obiekty Map zawieraj?ce w�asno?ci
                // obiektu Person
                if(recipients != null)
                	doc.setRecipients(recipients);
                   

                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
                doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));
                doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber, 20));
                doc.setZpoDate(DateUtils.nullSafeParseJsDate(zpoDate));
                doc.setPrepState(prepState);
                if (deliveryId != null)
                    doc.setDelivery(OutOfficeDocumentDelivery.find(deliveryId));
                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                doc.setStampFee(TextUtils.nullSafeParseBigDecimal(stampFee, 2));
                doc.setCaseDocumentId(caseDocumentId);
                doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));
                if(weightG!=null){
                	doc.setWeightG(Integer.parseInt(weightG));
                }else{
                	doc.setWeightG(null);
                }
                if(weightKg!=null){
                	doc.setWeightKg(Integer.parseInt(weightKg));
                }else{
                	doc.setWeightKg(null);
                }
                if(priority==null || priority.booleanValue()==false){
                	doc.setPriority(new Boolean(false));
                }
                else
                	doc.setPriority(new Boolean(true));

                if (!StringUtils.isEmpty(assignedUser))
                {
                    DSUser.findByUsername(assignedUser);
                    doc.setClerk(assignedUser);
                }

                if (returnReasonId != null)
                {
                    doc.setReturnReason(OutOfficeDocumentReturnReason.find(returnReasonId));
                }
                doc.setReturned(returned);

                DSApi.context().commit();
                try{
                    DSApi.context().session().flush();
                    }catch(HibernateException e){

                    }
                    DSApi.context().begin();
                    	TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),OutOfficeDocument.TYPE);
                    DSApi.context().commit();

                if (doc.getInDocument() != null && doc.getInDocument().isSubmitToBip())
                {
                    try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        bip.submit(doc.getInDocument());
                    }
                    catch (ServiceException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                }

                event.skip(EV_FILL_CREATE);

                event.addActionMessage(sm.getString("ZapisanoZmiany"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class Split implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                List<OutOfficeDocument> docs = new ArrayList<OutOfficeDocument>();

                OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                InOfficeDocument inOfficeDocument = document.getInDocument();

                List<Recipient> recipients = new ArrayList<Recipient>(document.getRecipients());

                List<Long> attachmentsToClone = new ArrayList<Long>(document.getAttachments().size());
                List<Attachment> attachments = document.getAttachments();
                for (int i=0; i < attachments.size(); i++) 
                {
                    Attachment attachment = (Attachment) attachments.get(i);
                    attachmentsToClone.add(attachment.getId());
                }

                DocumentKind dkNormal = DocumentKind.findByCn(DocumentKind.NORMAL_KIND);
                for (int i=0; i < recipients.size(); i++)
                {

                    OutOfficeDocument newDocument;
                    if (i == 0)
                        newDocument = document;
                    else
                    {
                        newDocument = (OutOfficeDocument) document.cloneObject((Long[]) attachmentsToClone.toArray(new Long[attachmentsToClone.size()]));
                        newDocument.setDocumentKind(dkNormal);
                    }

                    for (Recipient delRec : new ArrayList<Recipient>(newDocument.getRecipients()))
                    {
                        newDocument.removeRecipient(delRec);
                    }
                    Recipient newRec = new Recipient().recipientFromMap(recipients.get(i).toMap());
                    newDocument.addRecipient(newRec);
                    DSApi.context().session().save(newRec);
                    // musimy rozpocz�� procesy dla nowych dokument�w
                    if (i > 0)
                    {
                        // tworzenie i uruchamianie wewn�trznego procesu workflow
                        WorkflowFactory.createNewProcess(newDocument, "Pismo przyj�te");
                    }

                    if(inOfficeDocument != null && inOfficeDocument.getId() != null )
                    {
                    	PreparedStatement updateAnswerCounter = null;
                        try
                        {
                            String del="update dso_out_document set INDOCUMENT=? where id=?";
                            updateAnswerCounter = DSApi.context().prepareStatement(del);
                            updateAnswerCounter.setLong(1, inOfficeDocument.getId());
                            updateAnswerCounter.setLong(2, newDocument.getId());
                            updateAnswerCounter.execute();
                        }
                        catch(SQLException esql)
                        {
                        	esql.printStackTrace();
                        }
                        finally
                        {
                        	DSApi.context().closeStatement(updateAnswerCounter);
                        }
                    }
                    try
                    {
                        DSApi.context().session().flush();
                    }
                    catch (HibernateException e)
                    {
                        throw new EdmException(e);
                    }

                    TaskSnapshot.updateAllTasksByDocumentId(newDocument.getId(), newDocument.getStringType());
                    if(inOfficeDocument != null) TaskSnapshot.updateByDocumentId(inOfficeDocument.getId(), inOfficeDocument.getStringType());
                    addActionMessage(sm.getString("ZapisanoDokumentDlaOdbiorcy",recipients.get(i).getShortSummary()));
                    docs.add(newDocument);
                }

                DSApi.context().commit();

                event.skip(EV_FILL_DOCUMENT);
                event.setResult("split");
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    // narazie poznizsze nieuzywane - zastapione przez 'Split'
    private class SplitAndAssignOfficeNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
          //  sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                List<OutOfficeDocument> docs = new ArrayList<OutOfficeDocument>();

                Date entryDate = GlobalPreferences.getCurrentDay();
                Long journalId = Journal.getMainOutgoing().getId();
                Integer sequenceId;

                OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                List<Recipient> recipients = document.getRecipients();

                List<Long> attachmentsToClone = new ArrayList<Long>(document.getAttachments().size());
                List<Attachment> attachments = document.getAttachments();
                for (int i=0; i < attachments.size(); i++)
                {
                    Attachment attachment = (Attachment) attachments.get(i);
                    attachmentsToClone.add(attachment.getId());
                }

                for (int i=0; i < recipients.size(); i++)
                {


                    OutOfficeDocument newDocument;
                    if (i == 0)
                        newDocument = document;
                    else
                    {
                        newDocument = (OutOfficeDocument) document.cloneObject((Long[]) attachmentsToClone.toArray(new Long[attachmentsToClone.size()]));

                        for (Iterator iter=newDocument.getRecipients().iterator(); iter.hasNext(); )
                        {
                            ((Recipient) iter.next()).delete();
                            iter.remove();
                        }
                    }

                  //  newDocument.setRecipients(null);
                    //List<Recipient> newRecipients = new ArrayList<Recipient>();
                    //newRecipients.add(recipients.get(i));
                    newDocument.addRecipient(recipients.get(i));

                    // musimy rozpocz�� procesy dla nowych dokument�w
                    if (i > 0)
                    {
                        WorkflowFactory.createNewProcess(newDocument, "Pismo przyj�te");

                    }

                    try
                    {
                        DSApi.context().session().flush();
                    }
                    catch (HibernateException e)
                    {
                        throw new EdmException(e);
                    }

                    synchronized (Journal.class)
                    {
                        sequenceId = Journal.TX_newEntry2(journalId, newDocument.getId(), entryDate);
                    }

                    newDocument.bindToJournal(journalId, sequenceId);

                    TaskSnapshot.updateAllTasksByDocumentId(newDocument.getId(), newDocument.getStringType());



                    docs.add(newDocument);
                }

                DSApi.context().commit();

                event.skip(EV_FILL_DOCUMENT);
                event.setResult("split");

                for (OutOfficeDocument doc : docs)
                {
                    addActionMessage(sm.getString("ZapisanoDokumentDlaOdbiorcyInadanoMuNumerKancelaryjny",doc.getRecipients().get(0).getShortSummary(),doc.getOfficeNumber()));
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class AssignOfficeNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date entryDate;
            Long journalId;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                entryDate = GlobalPreferences.getCurrentDay();

                // konieczne, aby sprawdzi�, �e dokument istnieje
                OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                // pisma wewn�trzne dostaj? numer KO w momencie tworzenia,
                // ale nie szkodzi umie?ci� tutaj rozr�nienia
                journalId = Journal.getMainOutgoing().getId();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
                Integer sequenceId;
                synchronized (Journal.class)
                {
                    try
                    {
                        sequenceId = Journal.TX_newEntry2(journalId, getDocumentId(), entryDate);
                    }
                    catch (EdmException e)
                    {
                    	log.error("",e);
                        addActionError(e.getMessage());
                        return;
                    }
                }
                
                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
                doc.bindToJournal(journalId, sequenceId);
                doc.setOfficeDate(new Date());
                DSApi.context().commit();
                try{
                    DSApi.context().session().flush();
                    }catch(HibernateException e){

                    }
                    DSApi.context().begin();
                    	TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),OutOfficeDocument.TYPE);
                    DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private void createGrantRequestAttachment(Long newDocumentId)
    {
    	OutOfficeDocument outDocument = null;
    	List<StyledDocument> documents = new ArrayList<StyledDocument>();
    	List<String> mimes = new ArrayList<String>();
    	List<String> filenames = new ArrayList<String>();
        StyledDocument document = null;
        String mime = null;
        String filename = null;
    	int beneficiaryCount = 0;
        try
        {
        	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            DSApi.context().begin();

	    	GrantRequest grantRequest = GrantRequest.find(grantRequestId);
	    	beneficiaryCount = grantRequest.getBeneficiaries().size();

	    	for (int i=0; i < beneficiaryCount; i++)
	    	{
		        Resource resource = Resource.findResource(grantRequest.getTemplateName(i));
		        mime = resource.getMime();
		        filename = resource.getFileName();

		        document = RTF.load(resource.getBinaryStream());

		        GrantRequest.fillTemplateRTF(document, grantRequest, i);

		        documents.add(document);
		        mimes.add(mime);
		        filenames.add(filename);
	    	}

	        DSApi.context().commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
        }
        catch (RTFException e)
        {
            DSApi.context().setRollbackOnly();
            addActionError(e.getMessage());
        }
        finally
        {
            DSApi._close();
        }

        for (int i=0; i < beneficiaryCount; i++)
        {
        	document = documents.get(i);
        	mime = mimes.get(i);
        	filename = filenames.get(i);

	        if (document != null)
	        {
	        	File temp = null;
	            try
	            {
	            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

	            	DSApi.context().begin();

	            	outDocument = OutOfficeDocument.findOutOfficeDocument(newDocumentId);

	                temp = File.createTempFile("docusafe_", ".tmp");
	                RTF.write(document, temp);


	                Attachment attachment = new Attachment(sm.getString("SzablonOdpowiedzi"), sm.getString("SzablonOdpowiedzi")+" "+sm.getString("WpostaciRTF"));
	                outDocument.createAttachment(attachment);
	                AttachmentRevision revision = attachment.createRevision(temp);
	                revision.setOriginalFilename(filename);
	                revision.setMime(mime);

	                DSApi.context().commit();
	            }
	            catch (IOException e)
	            {
	            	addActionError(e.getMessage());
	            }
	            catch (EdmException e)
	            {
	                DSApi.context().setRollbackOnly();
	                addActionError(e.getMessage());
	            }
	            catch (Throwable t)
	            {
	            	addActionError(t.getMessage());
	            }
	            finally
	            {
	                if (temp != null) temp.delete();
	                DSApi._close();
	            }
	        }
        }
    }

    private class GenerateCSVFromRecipients implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		List<Recipient> recs = new ArrayList<Recipient>();
            	for (int i=0; i < recipients.length; i++)
                {
                    String json = recipients[i];
                    Recipient recipient = new Recipient();
                    recipient.fromJSON(json);
                    recs.add(recipient);
                }
            	File f = File.createTempFile("odbiorcy", ".csv");
            	RecipientsUtils.createCSVFromRecipients(recs, f);
            	ServletUtils.streamFile(ServletActionContext.getResponse(), f, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"Odbiorcy.csv\"");
        	}
        	catch (Exception e) 
        	{
				addActionError(e.getMessage());
				log.debug(e.getMessage(),e);
			}
        	
        	
        }
    }
    
    
    public void setRecipients(String[] recipients)
    {
        this.recipients = recipients;
    }


    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isSenderAnonymous()
    {
        return senderAnonymous;
    }

    public void setSenderAnonymous(boolean senderAnonymous)
    {
        this.senderAnonymous = senderAnonymous;
    }

    public String getSenderTitle()
    {
        return senderTitle;
    }

    public void setSenderTitle(String senderTitle)
    {
        this.senderTitle = senderTitle;
    }

    public String getSenderFirstname()
    {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname)
    {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname()
    {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname)
    {
        this.senderLastname = senderLastname;
    }

    public String getSenderOrganization()
    {
        return senderOrganization;
    }

    public void setSenderOrganization(String senderOrganization)
    {
        this.senderOrganization = senderOrganization;
    }

    public String getSenderStreetAddress()
    {
        return senderStreetAddress;
    }

    public void setSenderStreetAddress(String senderStreetAddress)
    {
        this.senderStreetAddress = senderStreetAddress;
    }

    public String getSenderZip()
    {
        return senderZip;
    }

    public void setSenderZip(String senderZip)
    {
        this.senderZip = senderZip;
    }

    public String getSenderLocation()
    {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation)
    {
        this.senderLocation = senderLocation;
    }

    public String getSenderCountry()
    {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry)
    {
        this.senderCountry = senderCountry;
    }

    public String getSenderEmail()
    {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail)
    {
        this.senderEmail = senderEmail;
    }

    public String getSenderFax()
    {
        return senderFax;
    }

    public void setSenderFax(String senderFax)
    {
        this.senderFax = senderFax;
    }

    public String getSenderPesel()
    {
        return senderPesel;
    }

    public void setSenderPesel(String senderPesel)
    {
        this.senderPesel = senderPesel;
    }

    public String getSenderNip()
    {
        return senderNip;
    }

    public void setSenderNip(String senderNip)
    {
        this.senderNip = senderNip;
    }

    public String getSenderRegon()
    {
        return senderRegon;
    }

    public void setSenderRegon(String senderRegon)
    {
        this.senderRegon = senderRegon;
    }

    public Map getRecipientsMap()
    {
        return recipientsMap;
    }

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber)
    {
        this.postalRegNumber = postalRegNumber;
    }

    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }

    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    public InOfficeDocument getInDocument()
    {
        return inDocument;
    }

    public String getPrepState()
    {
        return prepState;
    }

    public void setPrepState(String prepState)
    {
        this.prepState = prepState;
    }

    public String getAssignedUser()
    {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public DSUser[] getUsers()
    {
        return users;
    }

    public String getZpoDate()
    {
        return zpoDate;
    }

    public void setZpoDate(String zpoDate)
    {
        this.zpoDate = zpoDate;
    }

    public String getOfficeNumber()
    {
        return officeNumber;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public Map getIncomingDivisions()
    {
        return incomingDivisions;
    }

    public boolean isIntoJournalPermission()
    {
        return intoJournalPermission;
    }

    public void setIntoJournalId(Long intoJournalId)
    {
        this.intoJournalId = intoJournalId;
    }

    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public void setCaseDocumentId(String id)
    {
        this.caseDocumentId=id;
    }

    public Map getJournalMessages()
    {
        return journalMessages;
    }

    public boolean isReadOnly()
    {
        return readOnly;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public boolean isCanAssignOfficeNumber()
    {
        return canAssignOfficeNumber;
    }

    public OutOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public Boolean getUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public OutOfficeDocument getDocument()
    {
        return document;
    }

    public void setAssignOfficeNumber(Boolean assignOfficeNumber)
    {
        this.assignOfficeNumber = assignOfficeNumber;
    }

    public Boolean getCanCreateWithOfficeNumber()
    {
        return canCreateWithOfficeNumber;
    }

    public void setReturned(boolean returned)
    {
        this.returned = returned;
    }

    public boolean getReturned()
    {
        return returned;
    }

    public void setReturnReasonId(Integer returnReasonId)
    {
        this.returnReasonId = returnReasonId;
    }

    public Integer getReturnReasonId()
    {
        return returnReasonId;
    }

    public List getReturnReasons()
    {
        return returnReasons;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public String getPersonDirectoryDivisionGuid()
    {
        return personDirectoryDivisionGuid;
    }

    public String getStampFee()
    {
        return stampFee;
    }

    public void setStampFee(String stampFee)
    {
        this.stampFee = stampFee;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public void setGrantRequestId(Long grantRequestId)
    {
    	this.grantRequestId = grantRequestId;
    }

    public Long getGrantRequestId()
    {
    	return grantRequestId;
    }

    public boolean isDaa()
    {
    	return daa;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public boolean isCanCreateMultiReplies()
    {
        return canCreateMultiReplies;
    }

    public void setCreateMultiReplies(Boolean createMultiReplies)
    {
        this.createMultiReplies = createMultiReplies;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setNextReply(boolean nextReply)
    {
        this.nextReply = nextReply;
    }

    public String getInActivity()
    {
        return inActivity;
    }

    public void setInActivity(String inActivity)
    {
        this.inActivity = inActivity;
    }

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getWeightKg() {
		return weightKg;
	}

	public void setWeightKg(String weightKg) {
		this.weightKg = weightKg;
	}

	public String getWeightG() {
		return weightG;
	} 

	public void setWeightG(String weightG) {
		this.weightG = weightG;
	}

	public Boolean getPriority() {
		return priority;
	}

	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Map getEpuapMethods()
	{
		return epuapMethods;
	}

	public void setEpuapMethods(Map epuapMethods)
	{
		this.epuapMethods= epuapMethods;
	}

	public String getEpuapMethodId()
	{
		return epuapMethodId;
	}

	public void setEpuapMethodId(String epuapMethodId)
	{
		this.epuapMethodId= epuapMethodId;
	}

	public static String getEpuapSkrytka()
	{
		return EPUAP_SKRYTKA;
	}

	public static String getEpuapDoreczyciel()
	{
		return EPUAP_DORECZYCIEL;
	}

	public boolean isAttachCase()
	{
		return attachCase;
	}

	public void setAttachCase(boolean attachCase)
	{
		this.attachCase= attachCase;
	}
	
	public boolean getSave() 
	{
		return save;
	}
	
	public void setSave(boolean save)
	{
		this.save=save;
	}
}
