package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DwrDocumentMainAction extends DwrDocumentMainTabAction
{
	private static Logger log = LoggerFactory.getLogger(DwrDocumentMainAction.class);
	static final StringManager sm = GlobalPreferences.loadPropertiesFile(DwrDocumentMainAction.class.getPackage().getName(),null);

	public Logger getLog()
	{
		return log;
	}

	public int getDocType()
	{
		return DocumentLogic.TYPE_OUT_OFFICE;
	}

	public OfficeDocument getOfficeDocument()
	{
		return new OutOfficeDocument();
	}

	public void setAfterCreate(OfficeDocument document) throws EdmException
	{
		try
		{
			if (!AvailabilityManager.isAvailable("permissions.dontGivePermissionToAuthorOnCreateDocument")){
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
		}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	/*@Override
	public void setBeforeCreate(OfficeDocument document, ActionEvent event) throws EdmException, Exception
	{
		if (event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM) != null
				&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM)).isEmpty()
				&& event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM) != null
				&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)).isEmpty())
		{
			fastAssignmentSelectDivision = (String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
			fastAssignmentSelectUser = (String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
			setBeforeCreate(document , true);
		}else 
		
			setBeforeCreate(document , false);
		



		
	}
	public void setBeforeCreate(OfficeDocument document ,boolean isSetAssigneParamsToDocument) throws EdmException, Exception
	{
		if (!DSApi.context().hasPermission(DSPermission.PISMO_WYCHODZACE_TWORZENIE))
			throw new EdmException(sm.getString("dspermission.PISMO_WYCHODZACE_TWORZENIE"));

		OutOfficeDocument doc = (OutOfficeDocument) document;
		if (getDocumentId() == null)
		{
			DSUser loggedUser = DSApi.context().getDSUser();

			fastAssignmentSelectUser = loggedUser.getName();

			DSDivision[] listaDzialow = loggedUser.getDivisionsWithoutGroupPosition();
			if (listaDzialow.length > 0)
			{
				fastAssignmentSelectDivision = listaDzialow[0].getGuid();
			}
			else
			{
				// getDivisionsWithoutGroupPosition() mog�o zwr�ci� zerow� tablic�,
				// gdy np. u�ytkownik testowy nie ma �adnych przypisanych dzia��w
				fastAssignmentSelectDivision = DSDivision.ROOT_GUID;
			}

		}
		
		if (dockindKeys.containsKey("RECIPIENT_PARAM") && 
				(!dockindKeys.containsKey("RECIPIENT") || (dockindKeys.containsKey("RECIPIENT") && dockindKeys.get("RECIPIENT") == null) ) && doc.getRecipients().isEmpty())
    	{
			if(DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "RECIPIENT").isOnlyPopUpCreate())
			{
				throw new EdmException(sm.getString("OnlyPopupCreate"));
			}
    		Person person = new Person();
    		person.fromMapUpper((Map)dockindKeys.get("RECIPIENT_PARAM"));
    		person.setDictionaryGuid("rootdivision");
	        person.create();
	        Recipient recipient = new Recipient();
	        recipient.fromMap(person.toMap());
	        recipient.setDictionaryGuid("rootdivision");
	        recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
	        recipient.setBasePersonId(person.getId());
	        recipient.setDocumentId(doc.getId());
	        recipient.setDocument(doc);
	        recipient.create();
	        doc.addRecipient(recipient);
    	}
		
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setInDocumentId(inDocumentId);
		entryDate = GlobalPreferences.getCurrentDay();
				
	}*/

	public void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException
	{
		if(AvailabilityManager.isAvailable("outofficedocument.bindToJournal"))
		{
		  OutOfficeDocument doc = (OutOfficeDocument) document; 
		  Journal journal = Journal.getMainOutgoing();
		  journalId = journal.getId();
		  DSApi.context().session().flush(); 
		  Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
		  doc.bindToJournal(journalId, sequenceId,null,event);
		  doc.setOfficeDate(entryDate);
		}
	}

	public boolean isStartProcess() throws EdmException
	{
		return true;
	}

	public List<Tab> prepareTabs()
	{
		return new Tabs(this).createTabs();
	}

	public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException  {
		dwrDocumentHelper.setBeforeUpdateOut(DwrDocumentMainAction.this);
	}

	public String getDocumentType()
	{
		return OutOfficeDocument.TYPE;
	}

	@Override
	public void setBeforeCreate(OfficeDocument document) throws EdmException, Exception
	{

		if (!DSApi.context().hasPermission(DSPermission.PISMO_WYCHODZACE_TWORZENIE))
			throw new EdmException(sm.getString("dspermission.PISMO_WYCHODZACE_TWORZENIE"));

		OutOfficeDocument doc = (OutOfficeDocument) document;
		if (getDocumentId() == null)
		{
			DSUser loggedUser = DSApi.context().getDSUser();

			fastAssignmentSelectUser = loggedUser.getName();

			DSDivision[] listaDzialow = loggedUser.getDivisionsWithoutGroupPosition();
			if (listaDzialow.length > 0)
			{
				fastAssignmentSelectDivision = listaDzialow[0].getGuid();
			}
			else
			{
				// getDivisionsWithoutGroupPosition() mog�o zwr�ci� zerow� tablic�,
				// gdy np. u�ytkownik testowy nie ma �adnych przypisanych dzia��w
				fastAssignmentSelectDivision = DSDivision.ROOT_GUID;
			}

		}
		
		if (dockindKeys.containsKey("RECIPIENT_PARAM") && 
				(!dockindKeys.containsKey("RECIPIENT") || (dockindKeys.containsKey("RECIPIENT") && dockindKeys.get("RECIPIENT") == null) ) && doc.getRecipients().isEmpty())
    	{
			if(DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "RECIPIENT").isOnlyPopUpCreate())
			{
				throw new EdmException(sm.getString("OnlyPopupCreate"));
			}
    		Person person = new Person();
    		person.fromMapUpper((Map)dockindKeys.get("RECIPIENT_PARAM"));
    		person.setDictionaryGuid("rootdivision");
	        person.create();
	        Recipient recipient = new Recipient();
	        recipient.fromMap(person.toMap());
	        recipient.setDictionaryGuid("rootdivision");
	        recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
	        recipient.setBasePersonId(person.getId());
	        recipient.setDocumentId(doc.getId());
	        recipient.setDocument(doc);
	        recipient.create();
	        doc.addRecipient(recipient);
    	}
		
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setInDocumentId(inDocumentId);
		entryDate = GlobalPreferences.getCurrentDay();
		
	}

	
}
