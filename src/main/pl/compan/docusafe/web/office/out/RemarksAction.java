package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.RemarksTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RemarksAction.java,v 1.5 2006/02/20 15:42:46 lk Exp $
 */
public class RemarksAction extends RemarksTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/remarks.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
