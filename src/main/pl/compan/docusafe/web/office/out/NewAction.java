package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa wykonuj�ca przekierowanie do w�a�ciwego serwletu
 * obs�uguj�cego przyj�cie pisma wychodz�cego/wewn�trznego.
 * W wi�kszo�ci spe�nia te same funkcje, co
 * {@link pl.compan.docusafe.webwork.NewOfficeDocumentRedirectAction},
 * ale dodatkowo obs�uguje parametr <code>internal</code>.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewAction.java,v 1.12 2009/10/29 12:05:48 mariuszk Exp $
 */
public class NewAction extends EventActionSupport
{
    private boolean internal;
    private Long inDocumentId;
    private Long rcConstructionApplicationId;
    private String inActivity;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    /**
     * Klasa implementuj�ca akcje odpowiedzialn� za wype�nienie formularza
     * 
     * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
     * @version 1.10
     */
    class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo") || DSApi.context().userPreferences().node("other").getBoolean("simpledocumentmainout", false))
       		 	event.setResult("simple");
        	else if (AvailabilityManager.isAvailable("menu.left.kancelaria.dwrprzyjmipismo"))
        	{
        		event.setResult("dwr");
        	}
        	else if (Configuration.hasExtra("business"))
                event.setResult("dockind");
            else if (rcConstructionApplicationId != null)
            {
                event.setResult("success.rcConstruction");
            }
        }
    }

    /**
     * Checks if is internal.
     *
     * @return true, if is internal
     */
    public boolean isInternal()
    {
        return internal;
    }

    /**
     * Sets the internal.
     *
     * @param internal
     */
    public void setInternal(boolean internal)
    {
        this.internal = internal;
    }

    /**
     * Gets the in document id.
     *
     * @return in document id
     */
    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    /**
     * Sets the in document id.
     *
     * @param inDocumentId
     */
    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }

    /**
     * Gets the rc construction application id.
     *
     * @return rc construction application id
     */
    public Long getRcConstructionApplicationId()
    {
        return rcConstructionApplicationId;
    }

    /**
     * Sets the rc construction application id.
     *
     * @param rcConstructionApplicationId
     */
    public void setRcConstructionApplicationId(Long rcConstructionApplicationId)
    {
        this.rcConstructionApplicationId = rcConstructionApplicationId;
    }

    /**
     * Gets the in activity.
     *
     * @return in activity
     */
    public String getInActivity()
    {
        return inActivity;
    }

    /**
     * Sets the in activity.
     *
     * @param inActivity
     */
    public void setInActivity(String inActivity)
    {
        this.inActivity = inActivity;
    }
    
}
