package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentHistoryAction.java,v 1.5 2006/02/20 15:42:46 lk Exp $
 */
public class AssignmentHistoryAction extends AssignmentHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/assignment-history.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
