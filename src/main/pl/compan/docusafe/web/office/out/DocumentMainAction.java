package pl.compan.docusafe.web.office.out;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.DocumentMainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentMainAction extends DocumentMainTabAction
{
    private StringManager sm;
    private Logger log = LoggerFactory.getLogger(DocumentMainAction.class);
    
    private static OfficeDocumentDelivery defaultDelivery;
   // @EXPORT (tylko odczytywane przez formularz, nie modyfikowane)
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private OutOfficeDocument document;
    private Map<String, String> recipientsMap;
    private InOfficeDocument inDocument;
    private List users;
    private Integer officeNumber;
    private String creatingUser;
    private String assignedDivision;
    private Map<String, String> incomingDivisions;
    private boolean intoJournalPermission;
    private String caseDocumentId;
    private Map<String, Integer> journalMessages;
    private boolean readOnly;
    private List deliveries;
    private boolean canAssignOfficeNumber;
    private OutOfficeDocumentDelivery delivery;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private Boolean canCreateWithOfficeNumber;
    private List returnReasons;
    private boolean currentDayWarning;
    private String personDirectoryDivisionGuid;
    private boolean blocked;

    // @IMPORT/EXPORT
    private Long inDocumentId;
    private String documentDate;
    private String description;
    private boolean senderAnonymous;
    private String senderTitle;
    private String senderFirstname;
    private String senderLastname;
    private String senderOrganization;
    private String senderStreetAddress;
    private String senderZip;
    private String senderLocation;
    private String senderCountry;
    private String senderEmail;
    private String senderFax;
    private String senderPesel;
    private String senderNip;
    private String senderRegon;
    private String weightG;
    private String weightKg;
    private Boolean priority;
    /**
     * Warto?� ustawiana po wys�aniu formularza przez u�ytkownika.
     * Warto?ciami s? wyra�enia OGNL tworz?ce obiekty java.util.Map
     * opisuj?ce odbiorc�w pisma.
     */
    private String[] recipients;
    private String postalRegNumber;
    private String prepState;
    private String assignedUser;
    private String zpoDate;
    private Long intoJournalId;
    private Integer deliveryId;
    private String barcode;
    private String reason;
    private String stampFee;
    private boolean daa;
    private boolean canCreateMultiReplies;
    private Boolean createMultiReplies;
    private String userToAssigned;
    private String divisionToAssigned;

    private Long boxId;
	private String boxName;
	private Boolean addToBox;

    private FormFile file;

    // @IMPORT
    private Boolean assignOfficeNumber;
    private Integer returnReasonId;
    private boolean returned;
    private Long grantRequestId;
    private String group;   /* nazwa grupy osob, ktora dodamy do Recipients */
    private boolean nextReply;

    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_CREATE = "create";
    public static final String CREATED = "created"; // atrybut ActionEvent

    private final static Logger LOG = LoggerFactory.getLogger(DocumentMainAction.class);



    public String getBaseLink()
    {
        return "/office/outgoing/document-main.action";
    }

    public boolean isInternal()
    {
        return false;
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }

    protected void setup()
    {

        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);

        registerListener("doCreate").
            append(new Create()).
            append(EV_FILL_CREATE, new FillCreate()).
            append(fillForm);

        registerListener("doAddGroupToRecipients").
            append(new FillFormWhenAddGroup()).
            append(new AddGroupToRecipients()).
            append(fillForm);
    }

	public void setDefaultDelivery() throws EdmException
	{
		deliveryId = Integer.parseInt(Docusafe.getAdditionProperty("out.default.delivery"));
	}
	
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            try
            {
            	sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                if(!DSApi.context().hasPermission(DSPermission.PISMO_WYCHODZACE_TWORZENIE)){
                	addActionError(sm.getString("NieMaszUprawnienDoTworzeniaPismWychodzacych"));
                	return;
                }
//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    addActionError("Brak uprawnienia do przyjmowania pism w KO");

           //     canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);

                deliveries = OutOfficeDocumentDelivery.list();
                prepState = OutOfficeDocument.PREP_STATE_FAIR_COPY;

                if (!isInternal())
                {
                    Map address = GlobalPreferences.getAddress();
                    senderOrganization = (String) address.get(GlobalPreferences.ORGANIZATION);
                    senderStreetAddress = (String) address.get(GlobalPreferences.STREET);
                    senderZip = (String) address.get(GlobalPreferences.ZIP);
                    senderLocation = (String) address.get(GlobalPreferences.LOCATION);
                    senderCountry = "PL";
                }

                try
                {
	                Sender indocsender = InOfficeDocument.findInOfficeDocument(inDocumentId).getSender();
	                if(indocsender != null && !indocsender.isAnonymous())
	                {
	                	if(recipientsMap == null)
	                		recipientsMap = new LinkedHashMap();

	                	Recipient recipient = new Recipient();
	                	recipient.setFirstname(indocsender.getFirstname());
	                	recipient.setLastname(indocsender.getLastname());
	                	recipient.setNip(indocsender.getNip());
	                	recipient.setFax(indocsender.getFax());
	                	recipient.setPesel(indocsender.getPesel());
	                	recipient.setRegon(indocsender.getRegon());
	                	recipient.setTitle(indocsender.getTitle());
	                    recipient.setOrganization(indocsender.getOrganization());
	                    recipient.setStreet(indocsender.getStreet());
	                    recipient.setZip(indocsender.getZip());
	                    recipient.setLocation(indocsender.getLocation());
	                    recipient.setCountry(indocsender.getCountry());
	                    recipient.setAdresSkrytkiEpuap(indocsender.getAdresSkrytkiEpuap());
	                    recipient.setIdentifikatorEpuap(indocsender.getIdentifikatorEpuap());

	                    recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
	                        recipient.getSummary());
	                }
                }
                catch (Exception e)
                {
					//log.debug(e.getMessage(),e);
				}

                fillForm(DocumentLogic.TYPE_OUT_OFFICE);
                String boxLine = documentKind.getProperties().get(DocumentKind.BOX_LINE_KEY);
                if (boxLine == null)
                {
                    // pud�o z linii domy�lnej
                    if (GlobalPreferences.isBoxOpen())
                    {
                        boxId = GlobalPreferences.getBoxId();
                    }
                    else{
                    	boxId = null;
                    }

                }
                else
                {
                    boxId = GlobalPreferences.getBoxId(boxLine);
                }
                if (boxId != null)
                {
                    try
                    {
                        Box box = Box.find(boxId);
                        boxName = box.getName();
                        boxId = box.getId();
                    }
                    catch (EntityNotFoundException e)
                    {
                        boxId = null;
                    }
                }
                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class FillCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // recipients to skrypty OGNL tworz�ce obiekty java.util.Map
            // trzeba tu odtworzy� nazwy opcji recipients, bo formularz
            // przekazuje tylko ich warto�ci jako skrypty OGNL tworz�ce
            // obiekty Map
            sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (recipients != null)
            {
                recipientsMap = new LinkedHashMap();

                try
                {
                    for (int i=0; i < recipients.length; i++)
                    {
                        recipientsMap.put(recipients[i], new Person().fromJSON(recipients[i]).getSummary());
                    }
                }
                catch (EdmException e)
                {
                    event.getLog().error(e.getMessage(), e);
                    addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu"));
                    return;
                }
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
/*
            if (file == null || file.getFile() == null || !file.getFile().exists() ||
                file.getFile().length() == 0)
                addActionError("Nie wybrano pliku lub przekazany plik jest pusty");
*/

            if (hasActionErrors())
                return;

/*
            if (deliveryId == null)
                addActionError("Nie wybrano sposobu odbioru pisma");
*/
            Date entryDate;
            Long journalId = null;
            OutOfficeDocument doc =  null;
            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();
	            sm = GlobalPreferences.loadPropertiesFile(DocumentMainAction.class.getPackage().getName(),null) ;

	            if (!isInternal() && (recipients == null || recipients.length == 0))
	                addActionError(sm.getString("NiePodanoOdbiorcyPisma"));

	            if (hasActionErrors())
	                return;

	            doc = new OutOfficeDocument();

	            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	            doc.setDivisionGuid(DSDivision.ROOT_GUID);
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);

	            Sender sender = new Sender();

	            if (!isInternal())
	            {
	                sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
	                sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
	                sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
	                sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
	                sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
	                sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
	                sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
	                sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
	                sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
	                sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
	            }

	            doc.setSender(sender);

	            // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
	            // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
	            // obiektu Person
	            if (!isInternal() && recipients != null)
	            {
	            	Map personMap = null;
	                //String ognlExpr = null;
	                String json = null;
	                try
	                {
	                    for (int i=0; i < recipients.length; i++)
	                    {
	                        json = recipients[i];
	                        Recipient recipient = new Recipient();
	                        recipient.fromJSON(json);
	                        recipient.setDictionaryType(null);
	                        recipient.setDictionaryGuid(null);
	                        recipient.setPersonGroup(null);
	                        doc.addRecipient(recipient);
	                    }
	                }
	                catch (EdmException e)
	                {
	                	saveFile();
	                    event.getLog().error(e.getMessage(), e);
	                    addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu")+" json="+json+
	                        " personMap="+personMap);
	                    return;
	                }
	            }
                

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");



                // trzeba mie� uprawnienia do nadawania numeru KO
/*
                if (assignOfficeNumber != null && assignOfficeNumber.booleanValue() &&
                    !DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
                    throw new EdmException("Brak uprawnie� do nadawania numeru kancelaryjnego");
*/

                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);

                documentKind.logic().correctValues(values, documentKind);
                // walidacja nie jest tu raczej potrzebna, gdy� wykonuje si� ona ju� na poziomie Javascriptu
                //documentKind.logic().validate(values, documentKind);

                doc.setSummary(description != null ? description : documentKind.getName());

                entryDate = GlobalPreferences.getCurrentDay();

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.setClerk(DSApi.context().getPrincipalName());

                doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
                if(weightG!=null)
                {
                	try
                	{
                    	doc.setWeightG(Integer.parseInt(weightG));
                	}
                	catch (NumberFormatException  e)
                	{
                		throw new EdmException("Nie mo�na skonwertowa� warto�ci "+weightG+" do liczby rzeczywistej");
    				}

                }else{
                	doc.setWeightG(null);
                }
                if(weightKg!=null)
                {
                	try
                	{
                		doc.setWeightKg(Integer.parseInt(weightKg));
                	}
                	catch (NumberFormatException  e)
                	{
                		throw new EdmException("Nie mo�na skonwertowa� warto�ci "+weightKg+" do liczby rzeczywistej");
    				}
                	
                }else{
                	doc.setWeightKg(null);
                }

                if(priority==null || priority.booleanValue()==false){
                	doc.setPriority(new Boolean(false));
                }
                else
                	doc.setPriority(new Boolean(true));

                if (deliveryId != null)
                    doc.setDelivery(OutOfficeDocumentDelivery.find(deliveryId));

/*
                InOfficeDocument indoc = null;
                if (inDocumentId != null)
                {
                    indoc = InOfficeDocument.findInOfficeDocument(inDocumentId);
                    doc.setInDocument(indoc);

                    // ze wzgl�du na interfejs BIP, kt�ry iteruje kolekcj�
                    // indoc.outDocuments
                    indoc.addOutDocument(doc);

                    OfficeCase c = indoc.getContainingCase();
                    if (c != null)
                    {
                        doc.setContainingCase(c);
                    }
                }
*/
                doc.setDocumentKind(documentKind);

                doc.setInternal(isInternal());

                InOfficeDocument indoc = null;
                if (inDocumentId != null)
                {
                    indoc = InOfficeDocument.findInOfficeDocument(inDocumentId);
                    doc.setInDocumentId(indoc.getId());

                    // ze wzgl�du na interfejs BIP, kt�ry iteruje kolekcj�
                    // indoc.outDocuments
                    indoc.addOutDocument(doc);

                }

                doc.create();

//                if (inDocumentId != null)
//                {
//                    if("eurzad".equals(indoc.getSender().getLparam()))
//                    {
//                    	EUrzadService.addOutgoingToEurzad(doc, indoc.getId());
//                    }
//                }

                newDocumentId = doc.getId();
                //event.getLog().debug("newDocumentId="+newDocumentId);
                //if (OutOfficeDocument.PREP_STATE_FAIR_COPY.equals(prepState))
                //ustalenia sa takie ze w dokumentach biznesowych
                //(dla administracji do tworzenia pism wychodzacych jest inna akcja)
                //jest na sztywno ustawione ze to jest czystopis
                //doc.setPrepState(OutOfficeDocument.PREP_STATE_FAIR_COPY);
                //else
                //doc.setPrepState(OutOfficeDocument.PREP_STATE_DRAFT);

                documentKind.set(newDocumentId, values);

                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_OUT_OFFICE);
                documentKind.logic().documentPermissions(doc);
                documentKind.logic().onStartProcess(doc);

                if (addToBox!=null && addToBox && boxId != null)
                {
                    doc.setBox(Box.find(boxId));
                }

                if (isInternal())
                {
                    journalId = Journal.getMainInternal().getId();
                    doc.setInternal(true);
                }
/*
                else
                {
                    journalId = Journal.getMainOutgoing().getId();
                }
*/
                addFiles(doc);
                specificAdditions(doc);
                DSApi.context().commit();

                if (inDocumentId!=null)
                	indoc.addWorkHistoryEntry(Audit.create("replies", DSApi.context().getPrincipalName(),
                			sm.getString("UtworzonoOdpowiedzDlaTegoDokumentu")));
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
            	saveFile();
            	try
            	{
            		DSApi.context().setRollbackOnly();
            	}
            	catch (Exception e2) {}
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            Integer sequenceId = null;
            if (isInternal())
            {
                try
                {
                    sequenceId = Journal.TX_newEntry2(journalId, newDocumentId, entryDate);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                    return;
                }
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                doc = OutOfficeDocument.findOutOfficeDocument(newDocumentId);

                if (isInternal())
                {
                    doc.bindToJournal(journalId, sequenceId);
                }

                if(AvailabilityManager.isAvailable("coordinators"))
                	WorkflowFactory.createNewProcess(doc, true);
                else
                	WorkflowFactory.createNewProcess(doc, false);


                if (inDocumentId != null)
                    TaskSnapshot.updateByDocumentId(inDocumentId, InOfficeDocument.TYPE);
                DSApi.context().commit();

                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                    doc.getId(),
                    doc.getRecipients());

/*
                // parametr u�ywany przez result=created w xwork.xml
                setDocumentId(doc.getId());

                if (inDocumentId != null)
                {
                    event.setResult("reply-created");
                }
                else if (Configuration.officeAvailable())
                {
                    event.setResult("task-list-out");
                }
                else
                {
                    addActionMessage("Utworzono pismo" +
                        (doc.getOfficeNumber() != null ? " nr "+doc.getOfficeNumber() : ""));
                    event.setResult("simpleoffice-created");
                }
*/
                if(createNextDocument)
                {
                	StringBuffer sb = new StringBuffer();
                	sb.append("Utwozono dokument, id");
                	sb.append(' ');
                	sb.append(doc.getId());
                	addActionMessage(sb.toString());
                }
                else if (NR_SZKODY != null && DSApi.context().hasPermission(DSPermission.NW_WPIS_RS))
                {
                    event.setResult("nw-drs");
                }
                else if (isInternal())
                    event.setResult("task-list-int");
                else
                    event.setResult("task-list-out");

                event.skip(EV_FILL_CREATE);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class AddGroupToRecipients implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            QueryForm form = new QueryForm(0, 0);



            if (!StringUtils.isEmpty(group))
            {
                form.addProperty("personGroup", group.trim());
                form.addProperty("DISCRIMINATOR", "PERSON");

                try
                {
                    DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                   //DSApi.context().begin();

                    SearchResults<? extends Person> results = Person.search(form);

                    if (results.totalCount() == 0)
                    {
                        sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
                        addActionMessage(sm.getString("NieZnalezionoOsobWpodanejGrupie"));
                    }
                    else
                    {
                        Person[] rec = results.results();
                        if (recipients == null)
                            recipients = new String[0];
                        //String[] newRecipients = new String[recipients.length + rec.length];

                        recipientsMap = new LinkedHashMap<String, String>();

                        for (int i=0; i < recipients.length; i++)
                        {
                            Recipient recipient = new Recipient().fromJSON(recipients[i]);
                            recipientsMap.put(recipients[i], recipient.getSummary());
                            //newRecipients[i] = recipients[i];
                        }

                        for (int i=0; i < rec.length; i++)
                        {
                            JSONObject ono = new JSONObject(rec[i].toMap());
                            recipientsMap.put(ono.toString(), rec[i].getSummary());
                            //newRecipients[recipients.length + i] = ono.toString();
                        }

                        //recipients = newRecipients;
                    }

                  //  DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    //DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
                finally
                {
                    try { DSApi.close(); } catch (Exception e) { }
                }
            }
        }
    }

    private class FillFormWhenAddGroup implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                deliveries = OutOfficeDocumentDelivery.list();
                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
                saveFile();
            }
            catch (EdmException e)
            {
            	saveFile();
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }


    public String[] getRecipients()
    {
        return recipients;
    }

    public void setRecipients(String[] recipients)
    {
        this.recipients = recipients;
    }

    public Map getRecipientsMap()
    {
        return recipientsMap;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public String getSenderTitle()
    {
        return senderTitle;
    }

    public void setSenderTitle(String senderTitle)
    {
        this.senderTitle = senderTitle;
    }

    public String getSenderFirstname()
    {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname)
    {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname()
    {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname)
    {
        this.senderLastname = senderLastname;
    }

    public String getSenderOrganization()
    {
        return senderOrganization;
    }

    public void setSenderOrganization(String senderOrganization)
    {
        this.senderOrganization = senderOrganization;
    }

    public String getSenderStreetAddress()
    {
        return senderStreetAddress;
    }

    public void setSenderStreetAddress(String senderStreetAddress)
    {
        this.senderStreetAddress = senderStreetAddress;
    }

    public String getSenderZip()
    {
        return senderZip;
    }

    public void setSenderZip(String senderZip)
    {
        this.senderZip = senderZip;
    }

    public String getSenderLocation()
    {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation)
    {
        this.senderLocation = senderLocation;
    }

    public String getSenderCountry()
    {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry)
    {
        this.senderCountry = senderCountry;
    }

    public String getSenderEmail()
    {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail)
    {
        this.senderEmail = senderEmail;
    }

    public String getSenderFax()
    {
        return senderFax;
    }

    public void setSenderFax(String senderFax)
    {
        this.senderFax = senderFax;
    }

    public String getSenderPesel()
    {
        return senderPesel;
    }

    public void setSenderPesel(String senderPesel)
    {
        this.senderPesel = senderPesel;
    }

    public String getSenderNip()
    {
        return senderNip;
    }

    public void setSenderNip(String senderNip)
    {
        this.senderNip = senderNip;
    }

    public String getSenderRegon()
    {
        return senderRegon;
    }

    public void setSenderRegon(String senderRegon)
    {
        this.senderRegon = senderRegon;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber)
    {
        this.postalRegNumber = postalRegNumber;
    }

    public String getPrepState()
    {
        return prepState;
    }

    public void setPrepState(String prepState)
    {
        this.prepState = prepState;
    }

    public Long getInDocumentId()
    {
        return inDocumentId;
    }

    public void setInDocumentId(Long inDocumentId)
    {
        this.inDocumentId = inDocumentId;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public void setNewDocumentId(Long newDocumentId)
    {
        this.newDocumentId = newDocumentId;
    }

 /*   public boolean isCanReadDAASlowniki() {
        return canReadDAASlowniki;
    }

    public void setCanReadDAASlowniki(boolean canReadDAASlowniki) {
        this.canReadDAASlowniki = canReadDAASlowniki;
    }
   */
    public void setGroup(String group)
    {
        this.group = group;
    }

	public Long getBoxId() {
		return boxId;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;
	}

	public String getBoxName() {
		return boxName;
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public Boolean getAddToBox() {
		return addToBox;
	}

	public void setAddToBox(Boolean addToBox) {
		this.addToBox = addToBox;
	}

	public String getWeightG() {
		return weightG;
	}

	public void setWeightG(String weightG) {
		this.weightG = weightG;
	}

	public String getWeightKg() {
		return weightKg;
	}

	public void setWeightKg(String weightKg) {
		this.weightKg = weightKg;
	}

	public Boolean getPriority() {
		return priority;
	}

	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	public String getUserToAssigned()
	{
		return userToAssigned;
	}

	public String getDivisionToAssigned()
	{
		return divisionToAssigned;
	}

	public void setUserToAssigned(String userToAssigned)
	{
		this.userToAssigned = userToAssigned;
	}

	public void setDivisionToAssigned(String divisionToAssigned)
	{
		this.divisionToAssigned = divisionToAssigned;
	}


}
