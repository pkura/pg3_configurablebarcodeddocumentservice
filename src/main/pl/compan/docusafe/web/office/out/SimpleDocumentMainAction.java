package pl.compan.docusafe.web.office.out;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.SimpleDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class SimpleDocumentMainAction extends SimpleDocumentMainTabAction
{
	private static OfficeDocumentDelivery defaultDelivery;

	private List<OutOfficeDocumentDelivery> deliveries;
	
	@Override
	public DocumentType getDocumentType()
	{
		return DocumentType.OUTGOING;//OutOfficeDocument.TYPE;
	}

	@Override
	public OfficeDocument getOfficeDocument()
	{
		return new OutOfficeDocument();
	}

	public String getBaseUrl()
	{
		return "/office/outgoing/simple-document-main.action";
	}

	@Override
	public void setAfterCreate(OfficeDocument document) throws EdmException
	{
	}


	@Override
	public void setDocument(OfficeDocument odc) throws EdmException {
		OutOfficeDocument document = (OutOfficeDocument) odc;
		documentDate = DateUtils.formatJsDate(document.getDocumentDate());
	}
    
	@Override
    public void loadFillForm() throws EdmException
    {
		if(StringUtils.isEmpty(userLastname))
		{
			userFirstname = DSApi.context().getDSUser().getFirstname();
			userLastname = DSApi.context().getDSUser().getLastname();
			fastAssignmentSelectUser = DSApi.context().getDSUser().getName();
			DSDivision[] divs = DSApi.context().getDSUser().getDivisionsWithoutGroup();
			if(divs != null && divs.length > 0)
			{
				
				userDivision = divs[0].getName();
				fastAssignmentSelectDivision = divs[0].getGuid();
				reserveFastAssignmentSelectDivision = fastAssignmentSelectDivision;
			}
		}
	}

	@Override
	public void setBeforeCreate(OfficeDocument document) throws EdmException
	{
		OutOfficeDocument doc = (OutOfficeDocument) document;

        Recipient recipient = new Recipient();
		if (personAnonymous)
		{
			recipient.setAnonymous(true);
		}
		else if(personLastname == null && personOrganization == null)
		{
		       throw new EdmException("Nie wybrano odbiorcy");
		}
		else
		{
			setPersonValue(recipient);
		}
		doc.addRecipient(recipient);

		/**dodajemy do slownika odbiorcow*/
		if(createPerson)
		{
			Person person = new Person();
			setPersonValue(person);
	        person.setDictionaryGuid("rootdivision");
	        person.setDictionaryType(Person.DICTIONARY_RECIPIENT);
	        person.createIfNew();
	        DSApi.context().session().flush();
		}
		
        if (userFirstname != null)
        {
        	Sender sender = new Sender();
        	sender.setFirstname(userFirstname);
        	sender.setLastname(userLastname);
        	sender.setOrganization(userDivision);
        	doc.setSender(sender);
        }
        else if(!StringUtils.isEmpty(userDivision))
        {
        	Map address = GlobalPreferences.getAddress();
        	Sender sender = new Sender();
        	sender.setOrganization(userDivision);
        	sender.setStreet((String) address.get(GlobalPreferences.STREET));
        	sender.setLocation((String) address.get(GlobalPreferences.LOCATION));
        	sender.setZip((String) address.get(GlobalPreferences.ZIP));
        	doc.setSender(sender);
        }
        else
        {
        	Map address = GlobalPreferences.getAddress();
        	Sender sender = new Sender();
			sender.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
			sender.setStreet((String) address.get(GlobalPreferences.STREET));
			sender.setZip((String) address.get(GlobalPreferences.ZIP));
			sender.setLocation( (String) address.get(GlobalPreferences.LOCATION));
			doc.setSender(sender);
        }
        doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
        doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
        entryDate = GlobalPreferences.getCurrentDay();
        doc.setCreatingUser(DSApi.context().getPrincipalName());
        if (deliveryId != null)
        {
            doc.setDelivery(OutOfficeDocumentDelivery.find(deliveryId));
        }
	}

	public void bindToJournal(OfficeDocument document,ActionEvent event) throws EdmException
	{
		if(AvailabilityManager.isAvailable("out.simple.bindToJournal"))
		{
			OutOfficeDocument doc = (OutOfficeDocument) document;
	        Journal journal = Journal.getMainOutgoing();
	        journalId = journal.getId();
	        DSApi.context().session().flush();
	        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
	        doc.bindToJournal(journalId, sequenceId,null,event);
	        doc.setOfficeDate(entryDate);
		}
	}

	public void setDeliveries(List<OutOfficeDocumentDelivery> deliveries)
	{
		this.deliveries = deliveries;
	}

	public List<OutOfficeDocumentDelivery> getDeliveries()
	{
		return deliveries;
	}

	public void loadDelivery() throws EdmException
	{
		setDeliveries(OutOfficeDocumentDelivery.list());
		if(defaultDelivery == null){
			defaultDelivery = (OfficeDocumentDelivery) DSApi.context().session()
				.createCriteria(OutOfficeDocumentDelivery.class)
				.add(Restrictions.like("name", "Poczta"))
				.uniqueResult();
		}
		if(defaultDelivery == null)
		{
			defaultDelivery = OutOfficeDocumentDelivery.list().get(0);
		}
		if(deliveryId == null){
			setDeliveryId(defaultDelivery.getId());
		}
	}

	public String getProcess() {
		return null;
	}

	@Override
	public void loadTabs() throws EdmException {
		setTabs(new Tabs(this).createTabs());
	}

	@Override
	public void updateSender(Person sender) throws EdmException {
		sender.setFirstname(userFirstname);
		sender.setLastname(userLastname);
		sender.setOrganization(userDivision);
	}

	@Override
	public void updateReceiver(Person receiver) {
		setPersonValue(receiver);
	}

	@Override
	public void setSender(Person person) throws EdmException {
		setUser(person);
	}

	@Override
	public void setReceiver(Person person) throws EdmException {
		setPerson(person);
	}

	@Override
	public boolean isStartProcess() throws EdmException 
	{
		return AvailabilityManager.isAvailable("simple.document.out.startProcess");
	}




}
