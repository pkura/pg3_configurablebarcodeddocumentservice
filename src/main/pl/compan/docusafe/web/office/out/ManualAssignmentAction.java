package pl.compan.docusafe.web.office.out;

import java.util.List;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.ManualAssignmentTabAction;
import pl.compan.docusafe.web.office.out.Tabs;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ManualAssignmentAction extends ManualAssignmentTabAction{
    @Override
    public String getDocumentType() {
        return OutOfficeDocument.TYPE;
    }
    protected List prepareTabs() {
        return new Tabs(this).createTabs();
    }
}
