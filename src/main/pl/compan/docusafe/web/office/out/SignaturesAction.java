package pl.compan.docusafe.web.office.out;

import java.util.List;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.AttachmentsTabAction;
import pl.compan.docusafe.web.office.common.SignaturesTabAction;

/**
 * User: Michal Manski mailto:michal.manski@com-pan.pl
 * Date: 2006-05-23
 */
public class SignaturesAction extends SignaturesTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/signatures.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
