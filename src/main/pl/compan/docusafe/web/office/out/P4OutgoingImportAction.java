package pl.compan.docusafe.web.office.out;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class P4OutgoingImportAction extends EventActionSupport {

	private final static Logger log = LoggerFactory.getLogger(P4OutgoingImportAction.class);
	
	 private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(P4OutgoingImportAction.class.getPackage().getName(),null);

    private FormFile file;
    private Map<Long,String> journals;
    private Long subJournalId;
    
	public void setFile(FormFile file) {
		this.file = file;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doImport").
            append(OpenHibernateSession.INSTANCE).
            append(new Import()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	journals = new HashMap<Long, String>();
        	try
        	{
        		for(Journal journal :Journal.findByType(Journal.OUTGOING, false))
        		{
        			if(journal.getSymbol() != null && journal.getSymbol().length() > 0 && !journal.isClosed())
        			{
        				journals.put(journal.getId(), journal.getSymbol());
        			}
        		}
        	}
        	catch (Exception e) 
        	{
				log.debug(e.getMessage(),e);
			}
        }
    }
	
	private class Import implements ActionListener 
	{
        public void actionPerformed(ActionEvent event)
        {
        	try 
			{	
				File xlsfile = file.getFile();
				InputStream is = new FileInputStream(xlsfile);
			    POIFSFileSystem fs = new POIFSFileSystem( is );
	            HSSFWorkbook wb = new HSSFWorkbook(fs);	            
	            HSSFSheet sheet = wb.getSheetAt(0);
	            //Iterator<HSSFRow> rows = sheet.rowIterator();
	            Iterator<Row> rows = sheet.rowIterator();
	            List<String> nums = new ArrayList<String>();
	            while( rows.hasNext() )  
	            {
	        	    Row row = rows.next();
	        	    if(row.getRowNum() > 1)
	        	    {
	        	    	createDoc(row);
	        	    }
	            }
			} 
			catch (Exception e) 
			{
				log.error(e.getMessage(), e);
				addActionError(e.getClass()+" "+e.getMessage());
			}
        	
        }
    }
	
	
	private void createDoc(Row row)
	{
		OutOfficeDocument doc = null;
         try
         {
        	String personFirstname = row.getCell(1).getStringCellValue();
        	String personLastname = row.getCell(2).getStringCellValue();
        	String personOrganization = row.getCell(3).getStringCellValue();
        	String personStreetaddress = row.getCell(4).getStringCellValue();
			String personZip = null;
			try
			{
				personZip = row.getCell(5).getStringCellValue();
			}
			catch (Exception e) 
			{
				personZip = String.valueOf(row.getCell(5).getNumericCellValue());
			}
			String personLocation = row.getCell(6).getStringCellValue();
			String userFirstname = row.getCell(8).getStringCellValue();
			String userLastname = row.getCell(9).getStringCellValue();
			if(StringUtils.isEmpty(personLastname) && StringUtils.isEmpty(personOrganization) && StringUtils.isEmpty(personStreetaddress))
			{
				return;
			}
			DSUser user = DSUser.findByFirstnameLastname(userFirstname, userLastname);
			DSDivision[] divs = user.getDivisions();
			String userDivision = null;
			if(divs != null && divs.length > 0)
				userDivision = divs[0].getName();

			
         	DSApi.context().begin();
            doc = new OutOfficeDocument();
            Recipient recipient = new Recipient();
			if(personLastname == null && personOrganization == null)
    		{
    			recipient.setAnonymous(true);
    		}
    		else
    		{
				recipient.setFirstname(TextUtils.trimmedStringOrNull(personFirstname, 50));
				recipient.setLastname(TextUtils.trimmedStringOrNull(personLastname, 50));
    			recipient.setOrganization(TextUtils.trimmedStringOrNull(personOrganization, 50));
				recipient.setStreet(TextUtils.trimmedStringOrNull(personStreetaddress, 50));
				recipient.setZip(TextUtils.trimmedStringOrNull(personZip, 14));
				recipient.setLocation(TextUtils.trimmedStringOrNull(personLocation, 50));
    		}
    		doc.addRecipient(recipient);
            
			if (userFirstname != null)
            {
            	Sender sender = new Sender();
            	sender.setFirstname(userFirstname);
				sender.setLastname(userLastname);
				sender.setOrganization(Journal.find(subJournalId).getSymbol());
				sender.setOrganizationDivision(userDivision);
            	doc.setSender(sender);
            }
            else
            {
            	throw new EdmException("Nie wybrano nadawcy");
            }

			//doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
            Date entryDate = GlobalPreferences.getCurrentDay();
            doc.setCreatingUser(DSApi.context().getPrincipalName());
            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
            doc.setDivisionGuid(DSDivision.ROOT_GUID);
            doc.setCurrentAssignmentAccepted(Boolean.FALSE);
             if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
                 throw new EdmException(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));
             DocumentKind documentKind = DocumentKind.findByCn(DocumentKind.getDefaultKind());
             doc.setSummary(documentKind.getName());
             doc.setAssignedDivision(DSDivision.ROOT_GUID);
             doc.setDocumentKind(documentKind);
             doc.setDelivery(getDelivery(row.getCell(7).getStringCellValue()));
             //doc.setPostalRegNumber(postalRegNumber);
             String authorFN = row.getCell(8).getStringCellValue();
             String authorLN = row.getCell(9).getStringCellValue();
             DSUser userA = DSUser.findByFirstnameLastname(authorFN, authorLN);
             if(userA != null)
            	 doc.setAuthor(userA.getName());
             doc.create();          
             Long newDocumentId = doc.getId();
             Map<String,Object> values = new HashMap<String, Object>();
             documentKind.set(newDocumentId, values);
             doc.getDocumentKind().logic().archiveActions(doc, 0);
             DSApi.context().session().save(doc);
             
             Journal journal = Journal.getMainOutgoing();
             Long journalId = journal.getId();
             DSApi.context().session().flush();
             Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
             doc.bindToJournal(journalId, sequenceId);
             
             LoggerFactory.getLogger("tomekl").debug("journal {} sequenceId {}",journalId,sequenceId);
             
             if(subJournalId != null)
             {
            	 Journal j = Journal.find(subJournalId);
            	 Long jId = j.getId();
            	 Integer sId = Journal.TX_newEntry2(jId, doc.getId(), entryDate);        	 
            	 DSApi.context().session().flush();
            	 doc.bindToJournal(jId, sId);
            	 LoggerFactory.getLogger("tomekl").debug("journal {} sequenceId {}",jId,sId);
             }
             
             doc.setOfficeDate(entryDate);
             
             DSApi.context().commit();
             addActionMessage("Przyj�to pismo o numerze KO "+doc.getFormattedOfficeNumber());
         }
         catch (EdmException e)
         {
             DSApi.context().setRollbackOnly();
             addActionError(e.getMessage());
			 log.error(e.getMessage(), e);
         }
	}

	private OutOfficeDocumentDelivery getDelivery(String name) throws EdmException
	{
		if(!StringUtils.isEmpty(name))
			return OutOfficeDocumentDelivery.findByName(name.trim());
		else
			return null;
	}

	public Map<Long, String> getJournals() {
		return journals;
	}

	public void setJournals(Map<Long, String> journals) {
		this.journals = journals;
	}

	public Long getSubJournalId() {
		return subJournalId;
	}

	public void setSubJournalId(Long subJournalId) {
		this.subJournalId = subJournalId;
	}	
}
