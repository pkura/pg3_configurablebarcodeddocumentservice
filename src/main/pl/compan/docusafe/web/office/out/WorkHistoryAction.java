package pl.compan.docusafe.web.office.out;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.WorkHistoryTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkHistoryAction.java,v 1.6 2007/09/13 11:38:00 mariuszk Exp $
 */
public class WorkHistoryAction extends WorkHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/outgoing/work-history.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.TYPE;
    }
}
