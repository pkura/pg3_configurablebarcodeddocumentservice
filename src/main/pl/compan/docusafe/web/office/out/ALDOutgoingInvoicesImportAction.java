package pl.compan.docusafe.web.office.out;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.imports.SimpleImportsManager;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ALDOutgoingInvoicesImportAction extends EventActionSupport {

	private final static Logger log = LoggerFactory.getLogger(ALDOutgoingInvoicesImportAction.class);
	
	 private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ALDOutgoingInvoicesImportAction.class.getPackage().getName(),null);

    // @IMPORT
    private FormFile file;
    private String select;
    private List<String> options;
    private String ext;
    private String reportName;
    private boolean done;

    private String typimportu;
    private String tiffFolder = Docusafe.getAdditionProperty("import.attachments.path");
    private Long journalId;
    private String dataPrzyjecia;
    private List<Journal> journals;
    
    private List<ImportTypeBean> importTypes;
    
	public void setFile(FormFile file) {
		this.file = file;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doShowReport").
	        append(OpenHibernateSession.INSTANCE).
	        append(new ShowReport()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		importTypes = new ArrayList<ImportTypeBean>();
        		journals = Journal.list();
        		
	        	if (DocumentKind.list().contains(DocumentKind.findByCn(DocumentLogicLoader.DC_KIND))) {
					ext = ".xls";
					importTypes.add(new ImportTypeBean("DC", sm.getString("DCImportKind"), false, false, true));
					importTypes.add(new ImportTypeBean("FAX", sm.getString("FAXImportKind"), true, true));
				} else if (DocumentKind.list().contains(DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND))) {
					ext = ".csv. " + sm.getString("PoszczegolnePolaOddzieloneSrednikami");
					importTypes.add(new ImportTypeBean("ALD", sm.getString("ALDImportKind"), false));
				}
	        	
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        		log.error("", e);
        	}
            options = new ArrayList<String>();
            
            	options.add("ALDImport");
        }
    }
	
	private class ImportTypeBean
	{
		private String importCode;
		private String importName;
		private Boolean needTiffDir;
		private Boolean needJournals;
		private Boolean needDate;
		
		public ImportTypeBean(String importCode, String importName, Boolean needTiffDir) 
		{
			this.importCode = importCode;
			this.importName = importName;
			this.needTiffDir = needTiffDir;
			this.needJournals = false;

		}

		public ImportTypeBean(String importCode, String importName, Boolean needTiffDir, Boolean needJournals, Boolean needDate) 
		{
			this.importCode = importCode;
			this.importName = importName;
			this.needTiffDir = needTiffDir;
			this.needJournals = needJournals;
			this.needDate = needDate;
			
		}

		
		public ImportTypeBean(String importCode, String importName, Boolean needTiffDir, Boolean needJournals) 
		{
			this.importCode = importCode;
			this.importName = importName;
			this.needTiffDir = needTiffDir;
			this.needJournals = needJournals;

		}

		
		public String getImportCode() {
			return importCode;
		}

		public void setImportCode(String importCode) {
			this.importCode = importCode;
		}

		public String getImportName() {
			return importName;
		}

		public void setImportName(String importName) {
			this.importName = importName;
		}

		public Boolean getNeedTiffDir() {
			return needTiffDir;
		}

		public void setNeedTiffDir(Boolean needTiffDir) {
			this.needTiffDir = needTiffDir;
		}

		public Boolean getNeedJournals() {
			return needJournals;
		}

		public void setNeedJournals(Boolean needJournals) {
			this.needJournals = needJournals;
		}

		public Boolean getNeedDate() {
			return needDate;
		}

		public void setNeedDate(Boolean needDate) {
			this.needDate = needDate;
		}
		
		
	}
	
	private class LoadFile implements ActionListener {
        public void actionPerformed(ActionEvent event) {
			try {
				OutputStream os;
				File dir = null;

				try {
					File report = File.createTempFile("importOutgoingReport" + System.currentTimeMillis(), ".txt");
					os = new FileOutputStream(report);
					reportName = report.getAbsolutePath();
					ServletActionContext.getContext().getSession().put("reportName", reportName);

					if (file == null) {
						throw new NullPointerException("Nie podano pliku");
					}

					if (tiffFolder != null) {
						dir = new File(tiffFolder);
						if (!dir.isDirectory() || !dir.exists()) {
							throw new EdmException("B�edny folder:" + dir.getAbsolutePath());
						}
					}

					if ("DC".equalsIgnoreCase(typimportu)) 
					{
						if(dataPrzyjecia == null || dataPrzyjecia.length() < 1)
						{
							throw new EdmException("Nie podano daty");
						}
						done = SimpleImportsManager.parseAndSaveNWOutgoingFile(file.getFile(), os, dataPrzyjecia);
					} else if ("ALD".equalsIgnoreCase(typimportu)) {
						done = SimpleImportsManager.parseAndSaveALDOutgoingFile(file.getFile(), os);
					} else if ("FAX".equalsIgnoreCase(typimportu)) {
						if (journalId == null) {
							throw new EdmException("Nie podano dziennika");
						}
						done = SimpleImportsManager.parseAndSaveFaxFile(file.getFile(), os, dir, journalId);
					}
				} catch (EdmException e) {
					addActionError(e.getMessage());
					DSApi.context().setRollbackOnly();
					log.error(e.getMessage(), e);
				} catch (IOException e) {
					addActionError(e.getMessage());
					addActionError("ImportZostalWykonanyAleNieMoznaByloZapisacRaportu");
					log.error(e.getMessage(), e);
				} catch (Exception e) {
					if (!StringUtils.isBlank(e.getMessage())) {
						addActionError(e.getMessage());
					}
					log.error(e.getMessage(), e);
				}

				if (done) {
					addActionMessage(sm.getString("PomyslnieZakonczonoImportPismWychodzacych"));
				} else {
					addActionError(sm.getString("WystapilyProblemyZimportemPism"));
				}
				event.setResult("report");
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				addActionError(e.getClass()+" "+e.getMessage());
			}
        }
    }
	
	
	private class ShowReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	reportName = (String)ServletActionContext.getContext().getSession().get("reportName");
        	//System.out.println(reportName);
        	HttpServletResponse response = ServletActionContext.getResponse();
            
        	File rep = new File(reportName);
        	
        	
            try{
	            response.setContentType("application/octet-stream");
	            response.setHeader("Content-Disposition", "attachment; filename=\"raport.txt\"");
	            FileReader fr = new FileReader(rep);
	            OutputStream os = response.getOutputStream();
	
	            int c;
	            while(true)
	            {
	            	c = fr.read();
	            	if(c==-1) break;
	            	os.write(c);
	            	
	            }
	        	os.flush();
            }
            catch(IOException e)
            {
            	addActionError(sm.getString("NieMoznaPobracPlikuRaportu"));
            	log.error("", e);
            }

           
        }
    }
	
	public String getZaladuj()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("Zaladuj");
    }

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getTypimportu() {
		return typimportu;
	}

	public void setTypimportu(String typimportu) {
		this.typimportu = typimportu;
	}

	public List<ImportTypeBean> getImportTypes() {
		return importTypes;
	}

	public void setImportTypes(List<ImportTypeBean> importTypes) {
		this.importTypes = importTypes;
	}

	public String getTiffFolder() {
		return tiffFolder;
	}

	public void setTiffFolder(String tiffFolder) {
		this.tiffFolder = tiffFolder;
	}

	public Long getJournalId() {
		return journalId;
	}

	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}

	public List<Journal> getJournals() {
		return journals;
	}

	public void setJournals(List<Journal> journals) {
		this.journals = journals;
	}

	public String getDataPrzyjecia() {
		return dataPrzyjecia;
	}

	public void setDataPrzyjecia(String dataPrzyjecia) {
		this.dataPrzyjecia = dataPrzyjecia;
	}
}
