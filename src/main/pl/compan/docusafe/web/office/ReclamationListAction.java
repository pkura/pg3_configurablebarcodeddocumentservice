package pl.compan.docusafe.web.office;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.TypNiezgodnosci;
import pl.compan.docusafe.parametrization.yetico.hb.Niezgodnosc;
import pl.compan.docusafe.parametrization.yetico.hb.NiezgodnoscWew;
import pl.compan.docusafe.parametrization.yetico.hb.ReclamationCosts;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.common.collect.ImmutableMap;
import com.opensymphony.webwork.ServletActionContext;

public class ReclamationListAction extends EventActionSupport {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ReclamationListAction.class);
	boolean canRead = false;
	private SearchResults<NiezgodnoscWew> niezgodnosci;

	private String sortField;
	private boolean ascending = true;

	private Integer statusNiezgodnosci;
	private Integer statusRealizacji;
	private Long documentId;

	private Map<Integer, String> statusyNiezgodnosci;
	private Map<Integer, String> statusyRealizacji;
	private Map<Integer, String> zaklady;
	private Map<Integer, String> rodzajeNiezgodnosci;
	
	public static ImmutableMap<Integer, String> typyKosztu = ImmutableMap.<Integer, String>builder()
			.put(10, "Wytworzenie")
			.put(15, "Folia")
			.put(20, "Transport PL")
			.put(25, "Transport DE")
			.put(30, "Recykling")
			.put(35, "Inne")
			.build();
	
	private List<TypNiezgodnosci> typyNiezgodnosci;
	private List<DSUser> autorzy;

	private Long autor;
	private String numerNiezgodnosci;
	private Integer rodzajNiezgodnosci;
	private Integer typNiezgodnosci;

	private String dataWykryciaOd;
	private String dataWykryciaDo;
	private Integer dotyczyZakladu;
	private String miejsceWykrycia;
	private String osobaPoinformowana;
	private String numerZdjecia;
	private Integer zaklad;
	private Integer typKosztu;
	private Double kosztJednostkowy;
	private Double ilosc;
	private Double kosztCalkowity;

	private boolean needsFilling = true;
	public boolean omitLimit;
	public String actionName;
	public static final int LIMIT = 20;
	private Pager pager;
	private int offset;
	public static final String URL = "/office/reclamation-list.action";

	public static Map<String, String> columns = new LinkedHashMap<String, String>();

	static {
		columns.put("documentId", "ID dokumentu");
		columns.put("statusNiezgodnosci", "Status Niezgodno�ci");
		columns.put("statusRealizacji", "Status realizacji");
		columns.put("nrReklamacji", "Numer reklamacji");
		columns.put("rodzaj", "Rodzaj niezgodno�ci");
		columns.put("typ", "Typ niezgodno�ci");
		columns.put("dataRejestracji", "Data rejestracji");
		columns.put("autor", "Autor");
		columns.put("dataWykrycia", "Data wykrycia");
		columns.put("dotyczyZakladu", "Dotyczy zak�adu");
		columns.put("miejsceWykrycia", "Miejsce wykrycia");
		columns.put("osobaPoinformowana", "Osoba poinformowana");
		columns.put("nrZdjecia", "Nr zdj�cia");
		columns.put("informacje", "Informacje");
		columns.put("zaklad", "Zak�ad");
		columns.put("typKosztu", "Typ kosztu");
		columns.put("kosztJednostkowy", "Koszt jednostkowy");
		columns.put("ilosc", "Ilo��");
		columns.put("kosztCalkowity", "Koszt ca�kowity");
	}

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append("FILL_FORM", fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Search())
			.append("FILL_FORM", fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doGenerateXls")
			.append(OpenHibernateSession.INSTANCE)
			.append(new SetActionName())
			.append(new Search())
		 	.append(null, new GenerateXLS())
		 	.appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			log.info(" --- ReclamationListAction.FillForm --- ");
			try {
				//
				log.info("status niezgodno�ci = " + statusNiezgodnosci);
				log.info("status realizacji = " + statusRealizacji);
				log.info("nr niezgodno�ci = " + numerNiezgodnosci);
				log.info("rodzaj = " + rodzajNiezgodnosci);
				log.info("typ = " + typNiezgodnosci);
				log.info("autor = " + autor);
				log.info("data rejestracji od = " + dataWykryciaOd);
				log.info("data rejestracji do = " + dataWykryciaDo);
				log.info("dotyczy zakladu = " + dotyczyZakladu);
				log.info("miejsce wykrycia = " + miejsceWykrycia);
				log.info("osoba poinformowana = " + osobaPoinformowana);
				log.info("numer zdj�cia = " + numerZdjecia);

				statusyNiezgodnosci = NiezgodnoscWew.getEnumItemMap(NiezgodnoscWew.STATUS_NIEZGODNOSCI);
				statusyRealizacji = NiezgodnoscWew.getEnumItemMap(NiezgodnoscWew.STATUS);
				zaklady = NiezgodnoscWew.getEnumItemMap(NiezgodnoscWew.DOT_ZAKLADU);
				rodzajeNiezgodnosci = Niezgodnosc.rodzajNiezgodnosciList();
				typyNiezgodnosci = Niezgodnosc.typNiezgodnosciList();
				autorzy = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				Pager.LinkVisitor linkVisitor = null;
				if (needsFilling) {
					niezgodnosci = NiezgodnoscWew.allList(offset, LIMIT, sortField, ascending);
					linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset) {
							return HttpUtils.makeUrl(URL, new Object[] { 
									"sortField", sortField == null ? "statusNiezgodnosci" : sortField,
									"ascending", String.valueOf(ascending), 
									"offset", String.valueOf(offset) });
						}
					};
				} else {

					linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset) {
							return HttpUtils.makeUrl(URL, new Object[] { 
									"doSearch", true, 
									"statusNiezgodnosci", statusNiezgodnosci, 
									"statusRealizacji", statusRealizacji, 
									"numerNiezgodnosci", numerNiezgodnosci,
									"rodzajNiezgodnosci", rodzajNiezgodnosci,
									"typNiezgodnosci", typNiezgodnosci,
									"autor", autor, 
									"dataWykryciaOd", dataWykryciaOd,
									"dataWykryciaDo", dataWykryciaDo, 
									"dotyczyZakladu", dotyczyZakladu, 
									"miejsceWykrycia", miejsceWykrycia,
									"osobaPoinformowana", osobaPoinformowana, 
									"numerZdjecia", numerZdjecia,
									"sortField", sortField == null ? "statusNiezgodnosci" : sortField,
									"ascending", String.valueOf(ascending), 
									"offset", String.valueOf(offset) });
						}
					};

				}
				pager = new Pager(linkVisitor, offset, LIMIT, niezgodnosci.totalCount(), 10);
				
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	private class Search implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			needsFilling = false;  
			if ("doGenerateXls".equals(actionName)) {
				omitLimit = true;
			} else {
				omitLimit = false;
			}
			try {
				niezgodnosci = NiezgodnoscWew.znajdzNiezgodnosci(statusNiezgodnosci, statusRealizacji, numerNiezgodnosci,
						rodzajNiezgodnosci, typNiezgodnosci, autor, dataWykryciaOd, dataWykryciaDo, dotyczyZakladu, miejsceWykrycia, osobaPoinformowana,
						numerZdjecia, offset, LIMIT, sortField, ascending, omitLimit, zaklad, typKosztu, kosztJednostkowy, ilosc, kosztCalkowity);

			} catch (ParseException e) {
				log.error(e.getMessage(), e);
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
		}

	}
	
	private class SetActionName implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			setActionName("doGenerateXls");
		}
	}
	
	private class GenerateXLS implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			try {
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet("Niezgodnosci");

				if (niezgodnosci.totalCount() == 0)
					return;

				int rowNumber = 1;

				HSSFRow headerRow = sheet.createRow(0);

				List<String> keys = new ArrayList<String>();

				for (String key : columns.keySet()) {
					if (!keys.contains(key))
						keys.add(key);
				}

				int headerNumber = 0;

				HSSFCellStyle headerStyle = workbook.createCellStyle();
				headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setBottomBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setLeftBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setRightBorderColor(HSSFColor.RED.index);

				headerStyle.setTopBorderColor(HSSFColor.RED.index);
				headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);

				for (String key : keys) {

					HSSFCell headercell = headerRow.createCell(headerNumber++);
					headercell.setCellStyle(headerStyle);
					String title = columns.get(key);
					title = title == null ? key : title;
					headercell.setCellValue(title);
				}

				List<Map<String, String>> resultsListOfMap = new ArrayList<Map<String, String>>();

				for (NiezgodnoscWew entry : Arrays.asList(niezgodnosci.results())) {
					Map<String, String> e = new HashMap<String, String>();
					
					e.put("documentId", entry.getDocumentId() == null ? "-" : entry.getDocumentId().toString());
					e.put("statusNiezgodnosci", entry.textStatus());
					e.put("statusRealizacji", entry.textStatusRealizacji());
					e.put("nrReklamacji", entry.getNrReklamacji() == null ? "-" : entry.getNrReklamacji());
					e.put("rodzaj", entry.textRodzaj());
					e.put("typ", entry.textTyp());
					e.put("dataRejestracji", entry.getDataRejestracji() == null ? "-" : DateUtils.formatJsDate(entry.getDataRejestracji()));
					e.put("autor", entry.textAutor());
					e.put("dataWykrycia", entry.getDataWykrycia() == null ? "-" : DateUtils.formatJsDate(entry.getDataWykrycia()));
					e.put("dotyczyZakladu", entry.getDotyczyZakladuString());
					e.put("miejsceWykrycia", entry.getMiejsceWykrycia() == null ? "-" : entry.getMiejsceWykrycia());
					e.put("osobaPoinformowana", entry.getOsobaPoinformowana() == null ? "-" : entry.getOsobaPoinformowana());
					e.put("nrZdjecia", entry.getNrZdjecia() == null ? "-" : entry.getNrZdjecia());
					e.put("informacje", entry.getInformacje() == null ? "-" : entry.getInformacje());
					if (entry.getReclamationCosts() != null && !entry.getReclamationCosts().isEmpty()) {
						Iterator<ReclamationCosts> iter = entry.getReclamationCosts().iterator();
						while(iter.hasNext()) {
							ReclamationCosts item = iter.next();
							e.put("zaklad", item.getWorksString());
							e.put("typKosztu", item.getCostTypeString());
							e.put("kosztJednostkowy", item.getUnitCost().toString());
							e.put("ilosc", item.getNumber().toString());
							e.put("kosztCalkowity", item.getTotalCost().toString());
							if (iter.hasNext()) {
								resultsListOfMap.add(e);
								e = new HashMap<String, String>();
								e.put("documentId", "");
								e.put("statusNiezgodnosci", "");
								e.put("statusRealizacji", "");
								e.put("nrReklamacji", "");
								e.put("rodzaj", "");
								e.put("typ", "");
								e.put("dataRejestracji", "");
								e.put("autor", "");
								e.put("dataWykrycia", "");
								e.put("dotyczyZakladu", "");
								e.put("miejsceWykrycia", "");
								e.put("osobaPoinformowana", "");
								e.put("nrZdjecia", "");
								e.put("informacje", "");
							}
						}
					} else {
						e.put("zaklad", "-");
						e.put("typKosztu", "-");
						e.put("kosztJednostkowy", "-");
						e.put("ilosc", "-");
						e.put("kosztCalkowity", "-");
					}
					resultsListOfMap.add(e);
				}

				// styl komorki - ten sam dla kazdej
				HSSFCellStyle style = workbook.createCellStyle();

				for (Map<String, String> values : resultsListOfMap) {
					HSSFRow row = sheet.createRow(rowNumber++);
					int cellNumber = 0;
					for (String str : keys) {

						HSSFCell cell = row.createCell(cellNumber++);

						cell.setCellValue(values.get(str) == null ? "-" : values.get(str).toString());

						cell.setCellStyle(style);
					}
				}

				for (int i = 0; i < keys.size(); i++) {
					try {
						sheet.autoSizeColumn(i);
					} catch (Exception exc) {

					}
				}

				File tmpfile = File.createTempFile("DocuSafe", "tmp");
				tmpfile.deleteOnExit();
				FileOutputStream fis = new FileOutputStream(tmpfile);
				workbook.write(fis);
				fis.close();
				ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel",
						"Content-Disposition: attachment; filename=\"Niezgodnosci_Wewnetrzne_" + DateUtils.formatCommonDate(new Date()) +".xls\"");
			} catch (IOException ioe) {
				event.addActionError(ioe.getMessage());
				log.error(ioe.getMessage(), ioe);
			}
			event.setResult(NONE);

		}
	}

	public boolean isCanRead() {
		return canRead;
	}

	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}

	public SearchResults<NiezgodnoscWew> getNiezgodnosci() {
		return niezgodnosci;
	}

	public void setNiezgodnosci(SearchResults<NiezgodnoscWew> niezgodnosci) {
		this.niezgodnosci = niezgodnosci;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public Integer getStatusNiezgodnosci() {
		return statusNiezgodnosci;
	}

	public void setStatusNiezgodnosci(Integer statusNiezgodnosci) {
		this.statusNiezgodnosci = statusNiezgodnosci;
	}

	public Integer getStatusRealizacji() {
		return statusRealizacji;
	}

	public void setStatusRealizacji(Integer statusRealizacji) {
		this.statusRealizacji = statusRealizacji;
	}

	public Map<Integer, String> getStatusyNiezgodnosci() {
		return statusyNiezgodnosci;
	}

	public void setStatusyNiezgodnosci(Map<Integer, String> statusyNiezgodnosci) {
		this.statusyNiezgodnosci = statusyNiezgodnosci;
	}

	public Map<Integer, String> getStatusyRealizacji() {
		return statusyRealizacji;
	}

	public void setStatusyRealizacji(Map<Integer, String> statusyRealizacji) {
		this.statusyRealizacji = statusyRealizacji;
	}

	public Map<Integer, String> getRodzajeNiezgodnosci() {
		return rodzajeNiezgodnosci;
	}

	public void setRodzajeNiezgodnosci(Map<Integer, String> rodzajeNiezgodnosci) {
		this.rodzajeNiezgodnosci = rodzajeNiezgodnosci;
	}

	public List<TypNiezgodnosci> getTypyNiezgodnosci() {
		return typyNiezgodnosci;
	}

	public void setTypyNiezgodnosci(List<TypNiezgodnosci> typyNiezgodnosci) {
		this.typyNiezgodnosci = typyNiezgodnosci;
	}

	public List<DSUser> getAutorzy() {
		return autorzy;
	}

	public void setAutorzy(List<DSUser> autorzy) {
		this.autorzy = autorzy;
	}

	public Long getAutor() {
		return autor;
	}

	public void setAutor(Long autor) {
		this.autor = autor;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Map<Integer, String> getZaklady() {
		return zaklady;
	}

	public void setZaklady(Map<Integer, String> zaklady) {
		this.zaklady = zaklady;
	}

	public String getNumerNiezgodnosci() {
		return numerNiezgodnosci;
	}

	public void setNumerNiezgodnosci(String numerNiezgodnosci) {
		this.numerNiezgodnosci = numerNiezgodnosci;
	}

	public String getDataWykryciaOd() {
		return dataWykryciaOd;
	}

	public void setDataWykryciaOd(String dataWykryciaOd) {
		this.dataWykryciaOd = dataWykryciaOd;
	}

	public String getDataWykryciaDo() {
		return dataWykryciaDo;
	}

	public void setDataWykryciaDo(String dataWykryciaDo) {
		this.dataWykryciaDo = dataWykryciaDo;
	}

	public Integer getDotyczyZakladu() {
		return dotyczyZakladu;
	}

	public void setDotyczyZakladu(Integer dotyczyZakladu) {
		this.dotyczyZakladu = dotyczyZakladu;
	}

	public String getMiejsceWykrycia() {
		return miejsceWykrycia;
	}

	public void setMiejsceWykrycia(String miejsceWykrycia) {
		this.miejsceWykrycia = miejsceWykrycia;
	}

	public String getOsobaPoinformowana() {
		return osobaPoinformowana;
	}

	public void setOsobaPoinformowana(String osobaPoinformowana) {
		this.osobaPoinformowana = osobaPoinformowana;
	}

	public String getNumerZdjecia() {
		return numerZdjecia;
	}

	public void setNumerZdjecia(String numerZdjecia) {
		this.numerZdjecia = numerZdjecia;
	}

	public Integer getRodzajNiezgodnosci() {
		return rodzajNiezgodnosci;
	}

	public void setRodzajNiezgodnosci(Integer rodzajNiezgodnosci) {
		this.rodzajNiezgodnosci = rodzajNiezgodnosci;
	}

	public Integer getTypNiezgodnosci() {
		return typNiezgodnosci;
	}

	public void setTypNiezgodnosci(Integer typNiezgodnosci) {
		this.typNiezgodnosci = typNiezgodnosci;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public static int getLimit() {
		return LIMIT;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Integer getZaklad() {
		return zaklad;
	}

	public void setZaklad(Integer zaklad) {
		this.zaklad = zaklad;
	}

	public Integer getTypKosztu() {
		return typKosztu;
	}

	public void setTypKosztu(Integer typKosztu) {
		this.typKosztu = typKosztu;
	}

	public Double getKosztJednostkowy() {
		return kosztJednostkowy;
	}

	public void setKosztJednostkowy(Double kosztJednostkowy) {
		this.kosztJednostkowy = kosztJednostkowy;
	}

	public Double getIlosc() {
		return ilosc;
	}

	public void setIlosc(Double ilosc) {
		this.ilosc = ilosc;
	}

	public Double getKosztCalkowity() {
		return kosztCalkowity;
	}

	public void setKosztCalkowity(Double kosztCalkowity) {
		this.kosztCalkowity = kosztCalkowity;
	}

	public Map<Integer, String> getTypyKosztu() {
		return typyKosztu;
	}
}
