package pl.compan.docusafe.web.office;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeManager;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeRange;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class AssignFromExistingRangeAction  extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	private StringManager sm = StringManager.getManager(AssignFromExistingRangeAction.class.getPackage().getName());
	private Long rangeId;
	private List<BarcodeRange> ranges;
	private Long masterRangeIdentity;
	private String rangeStart;
	private String rangeEnd;
	private Boolean confirmAssign;
	private BarcodeRange br;
	
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAssignFromOwn").
        	append(OpenHibernateSession.INSTANCE).
        	append(new AssignFromOwn()).
        	appendFinally(CloseHibernateSession.INSTANCE);
    }

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	        	BarcodeManager.getActivePool();
	        	if(masterRangeIdentity!=null)
	        	{
	        		ranges = new ArrayList<BarcodeRange>();
	        		ranges.add(BarcodeManager.findRangeById(masterRangeIdentity));
	        	}
	        	else
	        		ranges = BarcodeManager.getRangeForClient(DSApi.context().getPrincipalName(), BarcodeManager.CLIENT_TYPE_USER, BarcodeManager.RANGE_STATUS_INUSE);
        	}
        	catch(EdmException e)
        	{
        		addActionError(sm.getString("NieUdaloSieZaladowacListyZakresow"));
        	}
        }
    }
	
	private class AssignFromOwn implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		BarcodeManager.getActivePool();
	    		ranges = BarcodeManager.getRangeForClient(DSApi.context().getPrincipalName(), BarcodeManager.CLIENT_TYPE_USER, BarcodeManager.RANGE_STATUS_INUSE);
	    		if(rangeStart == null || rangeEnd == null || rangeStart.length()<12 || rangeEnd.length() < 12)
	    		{
	    			addActionError(sm.getString("PodajKoniecIPoczatekZakresu"));
	    			return;
	    		}
	    		rangeStart = rangeStart.substring(1, rangeStart.length()-1);
	    		rangeEnd = rangeEnd.substring(1, rangeEnd.length()-1);
	    		Long start = Long.parseLong(rangeStart);
	    		Long end = Long.parseLong(rangeEnd);
	    		confirmAssign = true;
	    		br = BarcodeManager.acceptBarcodeRangeFromRange(rangeId, BarcodeManager.RANGE_STATUS_SENT, masterRangeIdentity, start, end);
	    		//event.setResult("assigned");
        	}
        	catch(EdmException e)
        	{
        		addActionError(sm.getString("NieUdaloSiePrzydzielicZakresuzZakresu"));
        	}
        }
    }

	

	public Long getRangeId() {
		return rangeId;
	}

	public void setRangeId(Long rangeId) {
		this.rangeId = rangeId;
	}

	public List<BarcodeRange> getRanges() {
		return ranges;
	}

	public void setRanges(List<BarcodeRange> ranges) {
		this.ranges = ranges;
	}

	public Long getMasterRangeIdentity() {
		return masterRangeIdentity;
	}

	public void setMasterRangeIdentity(Long masterRangeIdentity) {
		this.masterRangeIdentity = masterRangeIdentity;
	}

	public String getRangeStart() {
		return rangeStart;
	}

	public void setRangeStart(String rangeStart) {
		this.rangeStart = rangeStart;
	}

	public String getRangeEnd() {
		return rangeEnd;
	}

	public void setRangeEnd(String rangeEnd) {
		this.rangeEnd = rangeEnd;
	}

	public Boolean getConfirmAssign() {
		return confirmAssign;
	}

	public void setConfirmAssign(Boolean confirmAssign) {
		this.confirmAssign = confirmAssign;
	}

	public BarcodeRange getBr() {
		return br;
	}

	public void setBr(BarcodeRange br) {
		this.br = br;
	}

	
	
	

}