package pl.compan.docusafe.web.office.internal;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.MainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MainAction.java,v 1.47 2010/08/11 14:47:22 mariuszk Exp $
 */
public class MainAction extends MainTabAction
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(MainAction.class.getPackage().getName(),null);
    // @EXPORT (tylko odczytywane przez formularz, nie modyfikowane)
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private OutOfficeDocument document;
    private DSUser[] users;
    private Integer officeNumber;
    private String creatingUser;
    private String assignedDivision;
    private Map incomingDivisions;
    private boolean intoJournalPermission;
    private String caseDocumentId;
    private Map journalMessages;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private boolean currentDayWarning;
    private String personDirectoryDivisionGuid;
    private boolean blocked;
    /** Dokument na kt�ry odpowiadamy */
    private Long replyDocumentId;
    private OfficeDocument replyDocument;
    private String replyAuthor;

    /** Dokument na ktory juz odpowiedzielismy */
    private Document repliedDocument;

    // @IMPORT/EXPORT
    private String documentDate;
    private String summary;
    private String clerk;
    private Long intoJournalId;
    private String barcode;
    private String referenceId;
    private boolean attachCase;

    private FormFile file;
    private boolean save;

    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    //public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_CREATE = "create";
    public static final String EV_UPDATE = "update";
    public static final String CREATED = "created"; // atrybut ActionEvent

    public String getBaseLink()
    {
        return "/office/internal/main.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }

    protected void setup()
    {
        FillPulldowns fillPulldowns = new FillPulldowns();
        FillDocumentForm fillDocumentForm = new FillDocumentForm();

        // ogl�danie istniej�cego dokumentu
        registerListener(DEFAULT_ACTION).
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        // tworzenie nowego dokumentu
        registerListener("doNewDocument").
            //append(OpenHibernateSession.INSTANCE).
            append(new FillNewDocumentForm()).
            append(fillPulldowns);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(fillPulldowns).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);

        registerListener("doUpdate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            //append(new FillCreateUpdateForm()).
            append(new ValidateUpdate()).
            append(EV_UPDATE, new Update()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doIntoJournal").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new IntoJournal()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillPulldowns implements ActionListener
    {


		public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                setTabs(new Tabs(MainAction.this).createTabs());
                //DSApi.context().getDSUser().getDivisions()[0].getGuid()
                users = DSDivision.find(DSDivision.ROOT_GUID).getUsersFromDivision(DSApi.context().getDSUser().getDivisions());
                UserFactory.sortUsers(users, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                //users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                useBarcodes = Boolean.valueOf(GlobalPreferences.isUseBarcodes());
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
                

                if(replyDocumentId != null){ //jesli odpowiadamy na inne pismo (tworzymy nowy dokument)
                	replyDocument = OfficeDocument.findOfficeDocument(replyDocumentId);
                	setReplyAuthor(DSUser.findByUsername(replyDocument.getAuthor()).asFirstnameLastname());
                }
                
                //zablokowanie mozliwosci zapisu
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.oznaczJakoCzystopis")){
                	Document doc = Document.find(documentId);
                	if(doc.getCzyCzystopis())
                		save=true;
            	}
            }
            catch (EdmException e)
            {
            	saveFile();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Odczytuje dane dokumentu.
     */
    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (getDocumentId() == null)
            {
                if (Configuration.officeAvailable())
                    event.setResult("task-list-int");
                else
                    event.setResult("simpleoffice-created");
                event.cancel();
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                document = doc;

                if(document!= null && document.getRepliedDocumentId() != null){
                	setRepliedDocument(OfficeDocument.find(document.getRepliedDocumentId()));
                }

                personDirectoryDivisionGuid =
                    document.getCurrentAssignmentGuid() != null ?
                    document.getCurrentAssignmentGuid() : document.getAssignedDivision();

                intoJournalPermission = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
                officeNumber = doc.getOfficeNumber();
                caseDocumentId = doc.getCaseDocumentId();

                journalMessages = new LinkedHashMap();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                clerk = doc.getClerk();
                assignedDivision = DSDivision.find(doc.getAssignedDivision()).getPrettyPath();
                documentDate = doc.getDocumentDate() != null ?
                    DateUtils.formatJsDate(doc.getDocumentDate()) : null;
                summary = doc.getSummary();
                barcode = doc.getBarcode();
                referenceId = doc.getReferenceId();

               /* if (doc.getOfficeNumber() != null)
                {*/
                    // lista dzia��w, w kt�rych mo�na przyj�� bie��cy dokument do dziennika
                    incomingDivisions = new LinkedHashMap();
                    
                    if (DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE))
                    {
                        List docJournals = Journal.findByType(Journal.INTERNAL);
                        for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                        {
                            Journal j = (Journal) iter.next();
                            try
                            {
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(),
                                        (j.getOwnerGuid() != null ?
                                            DSDivision.find(j.getOwnerGuid()).getName()+" - " : "")
                                        +j.getDescription());
                            }
                            catch (DivisionNotFoundException e)
                            {
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(),
                                        "[nieznany dzia�] - " +
                                        j.getDescription());
                            }
                        }
                    }
                    else
                    {
                        boolean przyjeciePismaKomorkaNadrzedna =
                            DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                        for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                        {
                            // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                            List docJournals = Journal.findByDivisionGuid(division.getGuid(), Journal.INTERNAL);
                            for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                            {
                                Journal j = (Journal) iter.next();
                                if (j.findDocumentEntry(getDocumentId()) == null)
                                    incomingDivisions.put(j.getId().toString(), division.getName()+" - "+j.getDescription());
                            }

                            // dzienniki w kom�rkach nadrz�dnych
                            if (przyjeciePismaKomorkaNadrzedna)
                            {
                                DSDivision parent = division.getParent();
                                while (parent != null && !parent.isRoot())
                                {
                                    // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                    List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), Journal.INTERNAL);
                                    for (Iterator iter=docParentJournals.iterator(); iter.hasNext(); )
                                    {
                                        Journal j = (Journal) iter.next();
                                        if (j.findDocumentEntry(getDocumentId()) == null)
                                            incomingDivisions.put(j.getId().toString(), parent.getName()+" - "+j.getDescription());
                                        // by�o divisions[i].getName()
                                    }
                                    parent = parent.getParent();
                                }
                            }
                        }
                    }
                    
                    /*boolean przyjeciePismaKomorkaNadrzedna =
                        DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                    DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
                    for (int i=0; i < divisions.length; i++)
                    {
                        List docJournals = Journal.findByDivisionGuid(divisions[i].getGuid(), Journal.INTERNAL);
                        for (Iterator iter=docJournals.iterator(); iter.hasNext(); )
                        {
                            Journal j = (Journal) iter.next();
                            if (j.findDocumentEntry(getDocumentId()) == null)
                                incomingDivisions.put(j.getId().toString(), divisions[i].getName()+" - "+j.getDescription());
                        }

                        // dzienniki w kom�rkach nadrz�dnych
                        if (przyjeciePismaKomorkaNadrzedna)
                        {
                            DSDivision parent = divisions[i].getParent();
                            while (parent != null && !parent.isRoot())
                            {
                                // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), Journal.INTERNAL);
                                for (Iterator iter=docParentJournals.iterator(); iter.hasNext(); )
                                {
                                    Journal j = (Journal) iter.next();
                                    if (j.findDocumentEntry(getDocumentId()) == null)
                                        incomingDivisions.put(j.getId().toString(), parent.getName()+" - "+j.getDescription());
                                }
                                parent = parent.getParent();
                            }
                        }
                    }

                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("intoJournalPerm="+intoJournalPermission+
                            " inDivisions="+incomingDivisions);*/
                /*}*/

                /* czy zablokowany przed edycja */
                blocked = (document.getBlocked() != null && document.getBlocked());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Wype�nia w�asno�ci u�ywane przez formularz tworzenia
     * nowego dokumentu.
     * <p>
     * U�ywane r�wnie� przed akcj� tworz�c� nowy dokument.
     */
    private class FillNewDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    addActionError("Brak uprawnienia do przyjmowania pism w KO");

                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class IntoJournal implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           // sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null) ;
            if (intoJournalId == null)
                return;

            DSDivision division;
            Date entryDate;
            Journal journal;

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                if (!DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoPrzyjmowaniaPismaWdziale"));

                Long documentId = getDocumentId();

//                OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
//
                journal = Journal.find(intoJournalId);

                if (journal.findDocumentEntry(documentId) != null)
                    throw new EdmException(sm.getString("PismoZnajdujeSieJuzWwybranymDzienniku"));

                division = DSDivision.find(journal.getOwnerGuid());

                entryDate = GlobalPreferences.getCurrentDay();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            Integer sequenceId;
            try
            {
                sequenceId = Journal.TX_newEntry2(journal.getId(), getDocumentId(), entryDate);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                document.setAssignedDivision(division.getGuid());

                addActionMessage(sm.getString("PismoZostaloPrzyjeteWdziennikuDzialuPodNumerem",division.getName(),sequenceId));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class ValidateUpdate implements ActionListener{

		public void actionPerformed(ActionEvent event) {

        	if( documentDate != null && DateUtils.parseJsDate(documentDate) == null){
            	addActionError(sm.getString("NiepoprawnaDataPisma"));
            }

            if (StringUtils.isEmpty(summary)){
                addActionError(sm.getString("NiePodanoOpisuPisma"));
            }

            if (hasActionErrors()){
                event.skip(EV_UPDATE);
                event.setResult(ERROR);
            }
		}

    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
          //  sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null) ;

        	if( documentDate != null && DateUtils.parseJsDate(documentDate) == null){
            	addActionError(sm.getString("NiepoprawnaDataPisma"));
            }

            if (StringUtils.isEmpty(summary))
                addActionError(sm.getString("NiePodanoOpisuPisma"));

            if (hasActionErrors())
            {
            	saveFile();
                event.skip(EV_CREATE);
                event.skip(EV_FILL_DOCUMENT);
                event.setResult(ERROR);
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
//            if (hasActionErrors())
//                return;
        	Date entryDate;
            Long journalId;
            OutOfficeDocument doc;
        	try
            {
	            doc = new OutOfficeDocument();
	            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	            doc.setDivisionGuid(DSDivision.ROOT_GUID);
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);

	            doc.setSender(new Sender());

                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");

                entryDate = GlobalPreferences.getCurrentDay();

                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.setClerk(DSApi.context().getPrincipalName());

                doc.setSummary(TextUtils.trimmedStringOrNull(summary, 80));

                doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));

                doc.setInternal(true);

                if(replyDocumentId!=null){
                	replyDocument = OfficeDocument.findOfficeDocument(replyDocumentId);
                }

                //odpowied� nale�y do tej samej sprawy co doument, na kt�ry odpowiadamy
                if(replyDocument!=null && replyDocument.getCaseDocumentId()!=null){
            		doc.setContainingCase(OfficeCase.findByOfficeId(replyDocument.getCaseDocumentId()), false);
            	}

                doc.create();



                newDocumentId = doc.getId();
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("newDocumentId="+newDocumentId);

                if (file != null && file.sensible())
                {
                    Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                    doc.createAttachment(attachment);

                    attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                }

                addFiles(doc);

                Journal journal = Journal.getMainInternal();
                journalId = journal.getId();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	saveFile();
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            Integer sequenceId = null;
            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            	DSApi.context().begin();
                sequenceId = Journal.TX_newEntry2(journalId, newDocumentId, entryDate);
              

                doc = OutOfficeDocument.findOutOfficeDocument(newDocumentId);

                doc.bindToJournal(journalId, sequenceId);

                if (Configuration.officeAvailable())
                {
                    // tworzenie i uruchamianie wewn�trznego procesu workflow
                	if (event.getLog().isDebugEnabled())
                        event.getLog().debug("Tworzenie procesu pracy");
                	if(replyDocumentId == null){
                		WorkflowFactory.createNewProcess(doc,"Pismo wewn�trzne");
                	} else {//w przypadku odpowiedzi na inne pismo wewnetrzne
                		replyDocument = OfficeDocument.findOfficeDocument(replyDocumentId);
                		WorkflowFactory.createNewProcess(doc,
                				false,
                				"Pismo wewn�trzne",
                				replyDocument.getAuthor(),
                				DSDivision.ROOT_GUID,
                				replyDocument.getAuthor());
                        replyDocument.addReply(doc);
                		TaskSnapshot.updateByDocumentId(replyDocumentId, replyDocument.getStringType());

                	}
                }

                DSApi.context().commit();

                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                    doc.getId(),
                    doc.getRecipients());

                // parametr u�ywany przez result=created w xwork.xml
                setDocumentId(doc.getId());


                if(attachCase)
                {
                	event.setResult("case");
                	event.cancel();
                }
                else
                if(replyDocument!=null) {
                	addActionMessage(sm.getString("UtworzonoOdpowiedz"));
                	event.setResult("reply-created");
                } else if (Configuration.officeAvailable())
                {
                    event.setResult("task-list-int");
                }
                else
                {
                 //   sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null) ;
                    addActionMessage(sm.getString("UtworzonoPismo") +
                        (doc.getOfficeNumber() != null ? sm.getString("nr")+doc.getOfficeNumber() : ""));
                    event.setResult("simpleoffice-created");
                }

                //event.skip(EV_FILL_CREATE);
            }
            catch (EdmException e)
            {
            	saveFile();
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
          //  sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null) ;

            if (hasActionErrors())
            {
                event.skip(EV_FILL_DOCUMENT);
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(getDocumentId());

                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
                doc.setSummary(TextUtils.trimmedStringOrNull(summary, 80));
                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));

                doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));

                if (!StringUtils.isEmpty(clerk))
                {
                    DSUser.findByUsername(clerk);
                    doc.setClerk(clerk);
                }

                DSApi.context().commit();

                try{
                    DSApi.context().session().flush();
                }
                catch(HibernateException e){
                }

                DSApi.context().begin();
                TaskSnapshot.updateAllTasksByDocumentId(doc.getId(),OutOfficeDocument.INTERNAL_TYPE);
                DSApi.context().commit();

                //event.skip(EV_FILL_CREATE);

                event.addActionMessage(sm.getString("ZapisanoZmiany"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerk(String clerk)
    {
        this.clerk = clerk;
    }

    public DSUser[] getUsers()
    {
        return users;
    }


    public Integer getOfficeNumber()
    {
        return officeNumber;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public Map getIncomingDivisions()
    {
        return incomingDivisions;
    }

    public boolean isIntoJournalPermission()
    {
        return intoJournalPermission;
    }

    public void setIntoJournalId(Long intoJournalId)
    {
        this.intoJournalId = intoJournalId;
    }

    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public Map getJournalMessages()
    {
        return journalMessages;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public Boolean getUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public OutOfficeDocument getDocument()
    {
        return document;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public String getPersonDirectoryDivisionGuid()
    {
        return personDirectoryDivisionGuid;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

	public void setReplyDocumentId(Long replyDocumentId) {
		this.replyDocumentId = replyDocumentId;
	}

	public Long getReplyDocumentId() {
		return replyDocumentId;
	}

	public void setReplyDocument(OfficeDocument replyDocument) {
		this.replyDocument = replyDocument;
	}

	public OfficeDocument getReplyDocument() {
		return replyDocument;
	}

	public void setReplyAuthor(String replyAuthor) {
		this.replyAuthor = replyAuthor;
	}

	public String getReplyAuthor() {
		return replyAuthor;
	}

	public void setRepliedDocument(Document repliedDocument) {
		this.repliedDocument = repliedDocument;
	}

	public Document getRepliedDocument() {
		return repliedDocument;
	}
	
	public boolean getSave() {
		return save;
	}
	
	public void setSave(boolean save) {
		this.save=save;
	}
}
