package pl.compan.docusafe.web.office.internal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class RepliesAction extends BaseTabsAction {
	private OfficeDocument document;
	private Set outDocuments;
	private boolean replyUnnecessary;
	private boolean canCreateOutgoing;

	protected List prepareTabs() {
		return new Tabs(this).createTabs();
	}

	public String getBaseLink() {
		return "/office/internal/replies.action";
	}

	public String getDocumentType() {
		return OutOfficeDocument.INTERNAL_TYPE;
	}

	protected void setup() {
		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(new FillForm()).appendFinally(
						CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").append(OpenHibernateSession.INSTANCE)
				.append(new Update()).append(new FillForm()).appendFinally(
						CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (getDocumentId() == null)
				return;

			try {
				canCreateOutgoing = DSApi.context().hasPermission(
						DSPermission.PISMO_WYCHODZACE_TWORZENIE);
				setTabs(prepareTabs());

				document = OfficeDocument.findOfficeDocument(getDocumentId());
				document.getReplies().size();//lazy initialization
						
				//DSApi.initializeProxy(document.getRecipients());

				//outDocuments = document.getOutDocuments();
				//DSApi.initializeProxy(outDocuments);
				/*
				if (outDocuments != null) {
					for (Iterator iter = outDocuments.iterator(); iter
							.hasNext();) {
						DSApi.initializeProxy(((OutOfficeDocument) iter.next())
								.getRecipients());
					}
				}*/

				//replyUnnecessary = document.isReplyUnnecessary();
			} catch (EdmException e) {
				addActionError(e.getMessage());
			}
		}
	}

	private class Update implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				/*OfficeDocument document = OfficeDocument
						.findOfficeDocument(getDocumentId());
				document.setReplyUnnecessary(replyUnnecessary);
				DSApi.context().session().save(document);
				TaskSnapshot.updateByDocumentId(document.getId(),
						InOfficeDocument.TYPE);*/
				DSApi.context().commit();
			} catch (EdmException e) {
				addActionError(e.getMessage());
			}
		}
	}

	public Set<Document> getReplies() throws EdmException {
		return document.getReplies();
	}

	public OfficeDocument getDocument() {
		return document;
	}

	public boolean isReplyUnnecessary() {
		return replyUnnecessary;
	}

	public void setReplyUnnecessary(boolean replyUnnecessary) {
		this.replyUnnecessary = replyUnnecessary;
	}

	public boolean isCanCreateOutgoing() {
		return canCreateOutgoing;
	}

	public void setCanCreateOutgoing(boolean canCreateOutgoing) {
		this.canCreateOutgoing = canCreateOutgoing;
	}
}