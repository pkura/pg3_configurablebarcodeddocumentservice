package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.WorkflowTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowAction.java,v 1.3 2006/12/19 16:08:45 lk Exp $
 */
public class WorkflowAction extends WorkflowTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/internal/workflow.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/internal/external-workflow.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }
}
