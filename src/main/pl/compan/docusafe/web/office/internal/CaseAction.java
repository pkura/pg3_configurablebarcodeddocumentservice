package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WfProcessMgr;
import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowManager;
import pl.compan.docusafe.web.office.common.CaseTabAction;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseAction.java,v 1.9 2008/10/06 09:33:10 pecet4 Exp $
 */
public class CaseAction extends CaseTabAction
{
/*
	protected boolean setCase(OfficeDocument document, OfficeCase c) throws EdmException
    {
        document.setContainingCase(c,false);
        // TODO: numer w sprawie
        return false;
    }
*/
	
	 protected boolean setCase(OfficeDocument document, OfficeCase c) throws EdmException
	 {
	        if (document.getContainingCase() == null)
	        {
	            document.setContainingCase(c,false);

	            return false;
	        }
	        else
	        {
	            // klonowanie dokumentu
	            OutOfficeDocument clone = (OutOfficeDocument) document.cloneObject(null);
	            clone.setMasterDocument((OutOfficeDocument) document);
	            
	            //WorkflowFactory.createNewProcess(clone,true);

	            // nowy proces tylko wtedy, gdy pozwala na to ustawienie konfiguracyjne
	            // i gdy istnieje proces podstawowy (musi by� dost�pny modu� office)
	            if (Configuration.officeAvailable() &&
	                !StringUtils.isEmpty(getActivity()) &&
	                GlobalPreferences.isWfProcessForClonedInDocuments())
	            {
	                // tworzenie i uruchamianie wewn�trznego procesu workflow
	            	// k.�. dwa razy, nie wiem po co, na razie zostaje
	            	WorkflowFactory.createNewProcess(clone,"Kopia przyj�tego pisma");
	            	
	            }

	            clone.setContainingCase(c,false);
	            // TODO: symbol w sprawie

	            //BipUtils.writeXml(c);

	            return true;
	        }
	    }
	
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/internal/case.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }
}
