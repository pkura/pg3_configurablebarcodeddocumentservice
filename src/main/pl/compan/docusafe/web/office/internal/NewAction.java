package pl.compan.docusafe.web.office.internal;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/* User: Administrator, Date: 2005-08-18 14:02:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: NewAction.java,v 1.7 2009/10/29 12:05:37 mariuszk Exp $
 */
public class NewAction extends EventActionSupport
{
	private Long replyDocumentId;
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);
    }

    public void setReplyDocumentId(Long replyDocumentId) {
    	this.replyDocumentId = replyDocumentId;
	}

	public Long getReplyDocumentId() {
		return replyDocumentId;
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo"))
       		 	event.setResult("simple");
        	else if (AvailabilityManager.isAvailable("menu.left.kancelaria.dwrprzyjmipismo"))
        	{
        		event.setResult("dwr");
        	}
        	else if (Configuration.hasExtra("business") /*&& !Configuration.hasExtra("nationwide")*/)
                event.setResult("dockind");

        }
    }
}
