package pl.compan.docusafe.web.office.internal;

import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class DwrDocumentMainAction extends DwrDocumentMainTabAction
{
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(DwrDocumentMainAction.class);
	static final StringManager sm = GlobalPreferences.loadPropertiesFile(DwrDocumentMainAction.class.getPackage().getName(),null);

	public Logger getLog()
	{
		return log;
	}

	public int getDocType()
	{
		return DocumentLogic.TYPE_INTERNAL_OFFICE;
	}

	public OfficeDocument getOfficeDocument()
	{
		return new OutOfficeDocument();
	}

	public void setAfterCreate(OfficeDocument document) throws EdmException
	{
		((OutOfficeDocument) document).setInternal(true);
		try
		{	if (!AvailabilityManager.isAvailable("permissions.dontGivePermissionToAuthorOnCreateDocument")){
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
		}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public void setBeforeCreate(OfficeDocument document) throws EdmException
	{
		if (!DSApi.context().hasPermission(DSPermission.PISMO_WEWNETRZNE_TWORZENIE))
			throw new EdmException(sm.getString("dspermission.PISMO_WEWNETRZNE_TWORZENIE"));

		if (getDocumentId() == null)
		{
			DSUser loggedUser = DSApi.context().getDSUser();

			fastAssignmentSelectUser = loggedUser.getName();
			
			DSDivision[] listaDzialow = loggedUser.getDivisionsWithoutGroupPosition();
			if (listaDzialow.length > 0)
			{
				fastAssignmentSelectDivision = listaDzialow[0].getGuid();
			}
			else
			{
				fastAssignmentSelectDivision = DSDivision.ROOT_GUID;
			}
			
			if (dockindKeys.containsKey("SENDER_PARAM") && (!dockindKeys.containsKey("SENDER") || dockindKeys.get("SENDER") == null))
			{
				if (DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "SENDER").isOnlyPopUpCreate())
				{
					throw new EdmException(sm.getString("OnlyPopupCreate"));
				}
				Person person = new Person();
				person.fromMapUpper((Map) dockindKeys.get("SENDER_PARAM"));
				person.setDictionaryGuid("rootdivision");
				person.create();
				Sender sender = new Sender();
				sender.fromMap(person.toMap());
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(person.getId());
				sender.create();
				document.setSender(sender);
			}
			if (AvailabilityManager.isAvailable("assignment.normal.internalToRecipient"))
			{
				if (dockindKeys.containsKey("RECIPIENT_HERE"))
				{
                    fastAssignmentSelectUser = null;
                    fastAssignmentSelectDivision = null;
					log.debug("RECIPIENT_HERE");
					String assignee = dockindKeys.get("RECIPIENT_HERE").toString();
					String[] ids = assignee.split(";");
					if (ids[0].contains("u:"))
						fastAssignmentSelectUser = ids[0].split(":")[1];
					if (ids[1].contains("d:"))
						fastAssignmentSelectDivision = ids[1].split(":")[1];
				}
			}
		}

		OutOfficeDocument doc = (OutOfficeDocument) document;
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		entryDate = GlobalPreferences.getCurrentDay();
	}

	public void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException
	{
		OutOfficeDocument doc = (OutOfficeDocument) document;
		Journal journal = Journal.getMainInternal();
		journalId = journal.getId();
		DSApi.context().session().flush();
		Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
		doc.bindToJournal(journalId, sequenceId, null, event);
		doc.setOfficeDate(entryDate);
	}

	public boolean isStartProcess() throws EdmException
	{
		return true;
	}

	public List<Tab> prepareTabs()
	{
		return new Tabs(this).createTabs();
	}

	public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException
	{
		dwrDocumentHelper.setBeforeUpdateInt(DwrDocumentMainAction.this);
	}

	public String getDocumentType()
	{
		return OutOfficeDocument.INTERNAL_TYPE;
	}
}
