package pl.compan.docusafe.web.office.internal;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;

import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.*;
import pl.compan.docusafe.web.office.in.*;
import pl.compan.docusafe.web.office.in.WorkflowAction;
import pl.compan.docusafe.core.office.ContractManagment;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.web.office.in.DwrDocumentMainAction;
import pl.compan.docusafe.web.office.internal.ProcessDiagramAction;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Tabs.java,v 1.34 2010/06/02 15:21:24 mariuszk Exp $
 */
public class Tabs
{
    private static final Log log = LogFactory.getLog(Tabs.class);

    private BaseTabsAction base;

    public Tabs(BaseTabsAction base)
    {
        this.base = base;
    }

    public List createTabs()
    {
        List<Tab> tabs = new ArrayList<Tab>(10);
		StringManager smTab = GlobalPreferences.loadPropertiesFile("","tab") ;

        String[] params = new String[] {
            "documentId", String.valueOf(base.getDocumentId()),
            "activity", base.getActivity() };

        if (base.getDocumentId() != null)
        {
        	boolean business = Configuration.hasExtra("business");

        	if(AvailabilityManager.isAvailable("zadanie.podsumowanie"))
        	{
        		tabs.add(new Tab(smTab.getString("Podsumowanie"), smTab.getString("PodsumowanieInformacjiOpismie"),
        				HttpUtils.makeUrl("/office/internal/summary.action", params),
        				base instanceof SummaryAction));
        	}
            if (AvailabilityManager.isAvailable("sprawy"))
            {
                tabs.add(new Tab(smTab.getString("Sprawa"), smTab.getString("ZalozSpraweDoPisma"),
                    HttpUtils.makeUrl("/office/internal/case.action", params),
                    base instanceof CaseAction));
            }

            /* Odpowiedz na pismo wewenetrzne, na razie tylko dla administracji */
            if(!business){
            	/*String[] replyParams = Arrays.copyOf(params, params.length);
            	replyParams[params.length] = "replyId";
            	replyParams[params.length + 1] = reply*/  
            	tabs.add(new Tab(smTab.getString("Odpowiedzi"),smTab.getString("OdpowiedziDoPisma"),
                        HttpUtils.makeUrl("/office/internal/replies.action", params),
                        base instanceof RepliesAction));           	
            }

            if (AvailabilityManager.isAvailable("kancelaria.zalaczniki", null, base.getActivity()) && Configuration.officeAvailable())
            {
                tabs.add(new Tab(smTab.getString("Zalaczniki"), smTab.getString("ZobaczIdodajZalaczniki"),
                    HttpUtils.makeUrl("/office/internal/attachments.action", params),
                    base instanceof AttachmentsAction));
            }

            if(AvailabilityManager.isAvailable("electronicSignature.document") || AvailabilityManager.isAvailable("electronicSignature.attachment")){
                tabs.add(new Tab(smTab.getString("Podpisy"), smTab.getString("ListaPodpisowDlaTegoDokumentu"),
	                    HttpUtils.makeUrl("/office/internal/electronic-signatures.action", params),
	                    base instanceof ElectronicSignaturesTabAction));
            }

            if (AvailabilityManager.isAvailable("zadanie.uwagi"))
            {
                 	String count = "";
                 	if (AvailabilityManager.isAvailable("showRemarksCount"))
                 	{
                 		int size;
     					try {
     						size = Document.find(base.getDocumentId()).getRemarks().size();
     						if ( size > 0)
     	            			count = " (" + size + ")";
     					} catch (Exception e) {
     						log.info(e.getMessage());
     					}
                 	}
            	tabs.add(new Tab(smTab.getString("Uwagi" + count), smTab.getString("ZobaczIdodajUwagiDoPisma"),
                    HttpUtils.makeUrl("/office/internal/remarks.action", params),
                    base instanceof RemarksAction));
            }
            if(!AvailabilityManager.isAvailable("kancelaria.ogolne.hidden"))
            {
	            tabs.add(new Tab(smTab.getString("Ogolne"), smTab.getString("OgolneInformacjeOpismie"),
	                HttpUtils.makeUrl("/office/internal/main-redirect.action", params),
	                base instanceof MainAction));
            }
			if (AvailabilityManager.isAvailable("kancelaria.archiwizacja") /* && business */) {
				try {
					// if (AvailabilityManager.isAvailable("kancelaria.archiwizacja.dwr"))
					// {
					// log.info("kancelaria.archiwizacja.dwr");
					// tabs.add(new Tab(smTab.getString("Archiwizacja"),smTab.getString("ArchiwizacjaPisma"),
					// HttpUtils.makeUrl("/office/internal/dwr-document-main.action", params),
					// base instanceof DwrDocumentMainAction));
					// }
					// else
					{
						Document document= Document.find(base.getDocumentId());
						if ((document.getDocumentKind() != null && 
								(document.getDocumentKind().getCn().equals(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY)
								 || document.getDocumentKind().getCn().equals(DocumentLogicLoader.PROTOKOL_BRAKOWANIA)
							)) || business) {

							if (document.getDocumentKind() != null
									&& !document.getDocumentKind().logic().needsEditDoctypeAction())
								tabs.add(new Tab(smTab.getString("Archiwizacja"), smTab.getString("ArchiwizacjaPisma"),
										HttpUtils.makeUrl("/office/internal/document-archive.action", params),
										base instanceof DocumentArchiveTabAction
												|| base instanceof DwrDocumentMainTabAction));
                        }
					}
				} catch (EdmException e) {
				}
			}

            if(AvailabilityManager.isAvailable("kancelaria.HistoriaPisma"))
            {
            	tabs.add(new Tab(smTab.getString("HistoriaPisma"), smTab.getString("HistoriaPracyNadPismem"),
            			HttpUtils.makeUrl("/office/internal/work-history.action", params),
            			base instanceof WorkHistoryAction));
            }
            
//            if(AvailabilityManager.isAvailable("kancelaria.zarzadzanieUmowa"))
//            {
//                boolean isDP = false;
//                 try{
//                    isDP = ContractManagment.isDocumentInDP(base.getDocumentId());
//                }catch(Exception e){
//                    
//                    log.error("Tab.class:134", e);
//                }
//
//                if(isDP){
//                    tabs.add(new Tab(smTab.getString("ZarzadzanieUmowa"), smTab.getString("DaneDotyczaceZarzadzaniaUmowa"),
//                        HttpUtils.makeUrl("/office/incoming/contract-managment.action", params),
//                        (base instanceof ContractManagmentTabAction) ));
//                }
//            }
            
            if(AvailabilityManager.isAvailable("kancelaria.HistoriaDekretacji"))
            {
	            if (Configuration.officeAvailable())
	            {
	                tabs.add(new Tab(smTab.getString("HistoriaDekretacji"), smTab.getString("HistoriaDekretacji"),
	                    HttpUtils.makeUrl("/office/internal/assignment-history.action", params),
	                    base instanceof AssignmentHistoryTabAction));
	            }
            }
            
            boolean dekretacjaPermission = false;
            Long docuId = base.getDocumentId();
            String docCn = "";
            try {
            	dekretacjaPermission = DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA);
	    		Document doc = Document.find(docuId);
	    		docCn = doc.getDocumentKind().getCn();        	
            } catch (EdmException e) {
        	log.warn(e.getMessage());
            }
            
            if(AvailabilityManager.isAvailable("kancelaria.dekretacja", null, base.getActivity()) && dekretacjaPermission
            		&& !AvailabilityManager.isAvailable("canNotDecree", docCn))
            {
	            if (Configuration.officeAvailable())
	            {
	                try
	                {
	                    // zakladka dekretacja tylko gdy nie jest to workflow zewnetrzny
                        if (!StringUtils.isEmpty(base.getActivity())) {

                            if(WorkflowFactory.jbpm && AvailabilityManager.isAvailable("manual-multi-assignment")) {
                                tabs.add(new Tab(smTab.getString("DekretacjaReczna"), smTab.getString("DekretacjaReczna"),
                                    HttpUtils.makeUrl("/office/internal/manual-multi-assignment.action", params),
                                    base instanceof ManualMultiAssignmentTabAction));
                            }
                            else if (WorkflowFactory.jbpm) {
                                tabs.add(new Tab(smTab.getString("DekretacjaReczna"), smTab.getString("DekretacjaReczna"),
                                        HttpUtils.makeUrl("/office/internal/manual-assignment.action", params),
                                        base instanceof ManualAssignmentTabAction));
                            } else {
                                tabs.add(new Tab(smTab.getString("Dekretacja"), smTab.getString("Dekretacja"),
                                        HttpUtils.makeUrl("/office/internal/workflow.action", params),
                                        base instanceof WorkflowAction));
                            }
                        }
	                }
	                catch (Exception e)
	                {
	                    log.error(e.getMessage(), e);
	                }
	            }
            }
            
            if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu", null, base.getActivity())){
            	
            		tabs.add(new Tab(smTab.getString("Wersja dokumentu"), smTab.getString("Wersja dokumentu"),
            				HttpUtils.makeUrl("/office/internal/document-version.action", params),
            				base instanceof DocumentVersionTabAction));
            }

            if(AvailabilityManager.isAvailable("kancelaria.wlacz.jackrabbit.wersjeDokumentu", null, base.getActivity())){

                tabs.add(new Tab(smTab.getString("Wersje dokumentów"), smTab.getString("Wersje dokumentów z Jackrabbita"),
                        HttpUtils.makeUrl("/office/internal/document-version-jcr.action", params),
                        base instanceof DocumentVersionJcrTabAction));
            }
            
            if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) 
            {
                tabs.add(new Tab(smTab.getString("Podpisy"), smTab.getString("PodpiszPismo"),
                        HttpUtils.makeUrl("/office/internal/signatures.action", params),
                        base instanceof SignaturesAction));
            }

            if(AvailabilityManager.isAvailable("kancelaria.szablony")) {
                tabs.add(new Tab(smTab.getString("Szablony"),smTab.getString("Szablony"),
                        HttpUtils.makeUrl("/office/internal/templates.action", params),
                        base instanceof TemplatesAction));
            }
			
			if(AvailabilityManager.isAvailable("process-diagram")) {
                tabs.add(new Tab(smTab.getString("process-diagram"),smTab.getString("process-diagram"),
                        HttpUtils.makeUrl("/office/internal/process-diagram.action", params),
                        base instanceof ProcessDiagramAction));
            }

        }

        return tabs;
    }
}

