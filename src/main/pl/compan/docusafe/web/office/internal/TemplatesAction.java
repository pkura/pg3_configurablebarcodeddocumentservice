package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.TemplatesTabAction;

import java.util.List;

public class TemplatesAction extends TemplatesTabAction {

    @Override
    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

}
