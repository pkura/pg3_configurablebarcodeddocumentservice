package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.ProcessDiagramTabAction;

import java.util.List;

/**
 * User: Tomasz
 * Date: 27.05.14
 * Time: 23:00
 */
public class ProcessDiagramAction extends ProcessDiagramTabAction {

    @Override
    protected List prepareTabs() {
        return new pl.compan.docusafe.web.office.internal.Tabs(this).createTabs();
    }

    public String getBaseLink() {
        return "/office/internal/process-diagram.action";
    }

    public String getDocumentType() {
        return OutOfficeDocument.INTERNAL_TYPE;
    }
}