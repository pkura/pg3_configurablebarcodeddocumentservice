package pl.compan.docusafe.web.office.internal;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.dockinds.process.WorkflowUtils;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.office.CloneDocumentManager;
import pl.compan.docusafe.web.office.common.SummaryTabAction;
import pl.compan.docusafe.web.office.common.SummaryTabAction.DocumentMetrics;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SummaryAction.java,v 1.74 2010/06/24 08:19:54 pecet1 Exp $
 */
public class SummaryAction extends SummaryTabAction
{
	private static final Logger log = LoggerFactory.getLogger(SummaryAction.class);
    private StringManager sm;
    // @EXPORT(odczytywane przez formularz)
    private OutOfficeDocument document;
    // dokument na kt�ry ten   (^) jest odpowiedzia 
    private OfficeDocument repliedDocument; 
    private boolean documentFax;

    // @IMPORT
    private String[] assignments;

    private List<Flags.Flag> globalFlags;
    private List<Flags.Flag> userFlags;
    private boolean flagsPresent;
    // tablice s� d�u�sze od MAX_FLAGS o 1, poniewa� flagi indeksowane
    // s� od 1 i te indeksy u�yte s� w tre�ci strony JSP
    private Long[] globalFlag;
    private Long[] userFlag;
    private String caseLink;
    private List<Order> orders = new ArrayList<Order>();
    
    private static final String EV_FILL="fill";
    

    private static final String EV_MANUAL_FINISH = "evManualFinish";
    private Collection<RenderBean> processRenderBeans;
    
	protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignments").
            append(OpenHibernateSession.INSTANCE).
            append(new Assignments()).
            append(EV_FILL,new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualFinish").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualFinish()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doWatch").
            append(OpenHibernateSession.INSTANCE).
            append(new Watch()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReopenWf").
            append(OpenHibernateSession.INSTANCE).
            append(new ReopenWf()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdateFlags").
            append(OpenHibernateSession.INSTANCE).
            append(new UpdateFlags()).
            append(new UpdateBox()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClone").
            append(OpenHibernateSession.INSTANCE).
            append(new CloneDocument()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCompileAttachments").
            append(OpenHibernateSession.INSTANCE).
            append(new CompileAttachments()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGiveAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new GiveAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    
        registerListener("doWithdrawAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new WithdrawAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGenerateDocumentView").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateDocumentView()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("sendToEva").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new SendToEva()).
	    	append(new FillDocumentForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doHotIcr").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new hotIcr()).	
	    	append(EV_FILL, new FillDocumentForm()).
	    	append(EV_MANUAL_FINISH ,new ManualFinish()).
	    	//append(new FillDocumentForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdateBox").
	        append(OpenHibernateSession.INSTANCE).
	        append(new UpdateBox()).
	        append(new FillDocumentForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
       
        registerListener("doDocumentMetrics").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DocumentMetrics()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doPrintCard").
    		append(OpenHibernateSession.INSTANCE).
    		append(new PrintCard()).
    		append(new FillDocumentForm()).
    		appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/internal/summary.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/internal/external-workflow.action";
    }

    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                WorkflowFactory.getInstance().manualFinish(getActivity(), true);
                DSApi.context().commit();

                event.setResult(getTaskListResult(document.getType()));
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	prepareLabels();
                /*Integer pom = (Integer) ServletActionContext.getRequest().getSession().getAttribute("offset");
                
                if(pom%2 != 1)
                {
                    pom += 1;
                    ServletActionContext.getRequest().getSession().setAttribute("offset", pom);
                }*/
                document = OutOfficeDocument.findOutOfficeDocument(getDocumentId());   
                
                if(document.getRepliedDocumentId()!= null){
                	repliedDocument = OfficeDocument.findOfficeDocument(document.getRepliedDocumentId());
                }
                
                for(Recipient dr : document.getRecipients())
                	dr.getId();
                document.getSender();
                
                if(document.getContainingCase()!=null)
                	caseLink = "/office/edit-case.do?id="+document.getContainingCase().getId()+"&tabId=summary";

                if(WorkflowFactory.jbpm) {
                    if(getActivity() != null){
                        processRenderBeans = ProcessUtils.renderProcessesForActivityId(document, getActivity(), ProcessActionContext.SUMMARY);
                    } else if(!AvailabilityManager.isAvailable("process.dontShowAll")) {
                        //jak nie ma activity to wy�wietlaj w archiwizacji tak jak w przypadku archiwum (ARCHIVE_VIEW)
                        processRenderBeans = ProcessUtils.renderAllProcesses(document, ProcessActionContext.SUMMARY);
                    }
                }

                WorkflowActivity activity = null;
                if (getActivity() != null ) {
                	activity = WorkflowFactory.getWfActivity(getActivity());
                    canAssignToCoordinator = WorkflowUtils.canAssignToCoordinator(getActivity());
                    if(WorkflowFactory.jbpm){
                        canAddToWatched = canAssignToCoordinator;
                    }
                }

                fillForm(document, activity);
                documentFax = "fax".equals(document.getSource());

                if(!Docusafe.hasExtra("business"))
                {
                	PreparedStatement ps = null;
                    try
                    {                        
                        ps = DSApi.context().prepareStatement("select id, officeNumber from DSO_ORDER where CURRENTASSIGNMENTUSERNAME is not null and DOCUMENT_ID = ?" );
                        ps.setLong(1, getDocumentId());
                        ResultSet rs = ps.executeQuery();
                        
                        while(rs.next())
                        {
                            orders.add(new Order(rs.getLong("id"),rs.getInt("officeNumber")));
                        }                        
                    }
                    catch (HibernateException e1)
                    {
                    	LogFactory.getLog("eprint").debug("", e1);
                    }
                    catch (SQLException e1)
                    {
                    	LogFactory.getLog("eprint").debug("", e1);
                    }
                    finally
                    {
                    	DSApi.context().closeStatement(ps);
                    }
                }
                
                
                // flagi
                String flagsOn = 
                    Configuration.getProperty("flags") == null ? 
                            "false" : Configuration.getProperty("flags");
                if ("true".equals(flagsOn))
                {
                	globalFlags = document.getFlags().getDocumentFlags(false);

                    userFlags = document.getFlags().getDocumentFlags(true);
                    
                    flagsPresent = globalFlags.size() > 0 || userFlags.size() > 0;
                }
                else
                    flagsPresent = false;

            }
            catch (Exception e)
            {
            	log.error("", e);
                addActionError(e.getMessage());
            }
        }
    }

 /*   private class Watch implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                if (getActivity() != null)
                {
                    DSApi.context().watch(URN.create(DSApi.context().getWfActivity(ActivityId.parse(getActivity()))));
                }
                else
                {
                    DSApi.context().watch(URN.create(Document.find(getDocumentId())));
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }     */

    private class Assignments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            if (assignments == null || assignments.length == 0)
            {
                addActionError(sm.getString("NieWybranoUzytkownikowDoDekretacji"));
                return;
            }

            try
            {
                DSApi.context().begin();

                boolean mainDocumentReassigned = collectiveAssignment(assignments, getActivity(), event);

                event.setResult("confirm");
                
                if (mainDocumentReassigned)
                	event.skip(EV_FILL);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                //DSApi.context().setRollbackOnly();
                try { DSApi.context().rollback(); } catch (EdmException f) {};
                addActionError(e.getMessage());
            }
        }
    }

    private class ReopenWf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                WorkflowFactory.reopenWf(getDocumentId());

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                TaskSnapshot.updateByDocumentId(document.getId(), OutOfficeDocument.INTERNAL_TYPE);

                DSApi.context().commit();
                
                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                	LogFactory.getLog("workflow_log").debug(
                			"dla dokumentu przywroconego na liste zadan znaleziono "
                			+activityIds.length
                			+" zadan");
                } else {
                	setActivity(activityIds[0]);
                }
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class UpdateFlags implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId());

                List<Long> ids;
                if(globalFlag!=null)
                {
		            for (Long id : globalFlag)
		            {
		                if(!LabelsManager.existsDocumentToLabelBean(getDocumentId(), id)) LabelsManager.addLabel(getDocumentId(), id, DSApi.context().getPrincipalName());
		            }
		            globalFlags = document.getFlags().getDocumentFlags(false);
		            ids = Arrays.asList(globalFlag);
		            for(Flags.Flag f: globalFlags)
		            {
		            	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())&& f.isCanClear()) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(false))
		            {
		            	if(LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())&& f.isCanClear()) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                if(userFlag!=null)
                {
	                for (Long id :userFlag)
	                {
	                    if(!LabelsManager.existsDocumentToLabelBean(getDocumentId(), id)) LabelsManager.addLabel(getDocumentId(), id, DSApi.context().getPrincipalName());
	                }
	                userFlags = document.getFlags().getDocumentFlags(true);
	                ids = Arrays.asList(userFlag);
	                for(Flags.Flag f: userFlags)
	                {
	                	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
	                }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(true))
 	                {
 	                	if( LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
 	                }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class CloneDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null) ;
            try
            {
                DSApi.context().begin();

                CloneDocumentManager.clone(getDocumentId());

                DSApi.context().commit();

                addActionMessage(sm.getString("DokumentZostalSklonowany"));

            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

        }
    }
    


    private class Order 
    {
        private Integer officeNumber;
        private Long id;

        public Order(Long id, Integer officeNumber)
        {
            this.id = id;
            this.officeNumber = officeNumber;

        }

        public Long getId()
        {
            return id;
        }
        public Integer getOfficeNumber()
        {
            return officeNumber;
        }
        
    }

    public OutOfficeDocument getDocument()
    {
        if(document == null){
            try {
                document = OutOfficeDocument.findOutOfficeDocument(getDocumentId());
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        return document;
    }

    public String[] getAssignments()
    {
        return assignments;
    }

    public void setAssignments(String[] assignments)
    {
        this.assignments = assignments;
    }

    public boolean isDocumentFax() {
        return documentFax;
    }

    public void setDocumentFax(boolean documentFax) {
        this.documentFax = documentFax;
    }

    public List getGlobalFlags()
    {
        return globalFlags;
    }

    public List getUserFlags()
    {
        return userFlags;
    }

    public boolean isFlagsPresent()
    {
        return flagsPresent;
    }


    public Long[] getGlobalFlag() {
		return globalFlag;
	}

	public void setGlobalFlag(Long[] globalFlag) {
		this.globalFlag = globalFlag;
	}

	public Long[] getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(Long[] userFlag) {
		this.userFlag = userFlag;
	}

	public void setGlobalFlags(List<Flags.Flag> globalFlags) {
		this.globalFlags = globalFlags;
	}

	public void setUserFlags(List<Flags.Flag> userFlags) {
		this.userFlags = userFlags;
	}

	public String getCaseLink() {
		return caseLink;
	}

	public void setCaseLink(String caseLink) {
		this.caseLink = caseLink;
	}
    public List<Order> getOrders()
    {
        return orders;
    }

    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

	public void setRepliedDocument(OfficeDocument repliedDocument) {
		this.repliedDocument = repliedDocument;
	}

	public OfficeDocument getRepliedDocument() {
		return repliedDocument;
	}

    public Collection<RenderBean> getProcessRenderBeans() {
        return processRenderBeans;
    }

    public void setProcessRenderBeans(Collection<RenderBean> processRenderBeans) {
        this.processRenderBeans = processRenderBeans;
    }
}
