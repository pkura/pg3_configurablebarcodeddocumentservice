package pl.compan.docusafe.web.office.internal;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.DocumentMainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentMainAction extends DocumentMainTabAction
{
    // @EXPORT (tylko odczytywane przez formularz, nie modyfikowane)
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
	private StringManager sm =
        GlobalPreferences.loadPropertiesFile(DocumentMainAction.class.getPackage().getName(),null);
        private static final Logger log = LoggerFactory.getLogger(DocumentMainAction.class.getPackage().getName());
    private OutOfficeDocument document;
    private List users;
    private Integer officeNumber;
    private String creatingUser;
    private String assignedDivision;
    private Map incomingDivisions;
    private boolean intoJournalPermission;
    private String caseDocumentId;
    private Map journalMessages;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private boolean currentDayWarning;
    private String personDirectoryDivisionGuid;
    private boolean blocked;
    private String description; 
    // @IMPORT/EXPORT
    private String documentDate;
    private String summary;
    private String clerk;
    private Long intoJournalId;
    private String barcode;
    private String referenceId;

    private FormFile file;
	private Long boxId;
		private String boxName;
		private Boolean addToBox;
    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_CREATE = "create";
    public static final String CREATED = "created"; // atrybut ActionEvent

    

    public String getBaseLink()
    {
        return "/office/internal/document-main.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }

    protected void setup()
    {

        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);

        registerListener("doCreate").
        	append(new ValidateCreate()).
            append(new Create()).
            append(fillForm);
        
        
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    addActionError("Brak uprawnienia do przyjmowania pism w KO");

              //  canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);

                fillForm(DocumentLogic.TYPE_INTERNAL_OFFICE);
                String boxLine = documentKind.getProperties().get(DocumentKind.BOX_LINE_KEY);
                if (boxLine == null)
                {
                    // pud�o z linii domy�lnej
                    if (GlobalPreferences.isBoxOpen())
                    {
                        boxId = GlobalPreferences.getBoxId();                       
                    }
                    else
                        boxId = null;
                }
                else
                {
                    boxId = GlobalPreferences.getBoxId(boxLine);
                }
                if (boxId != null)
                {
                    try
                    {
                        Box box = Box.find(boxId);
                        boxName = box.getName();
                        boxId = box.getId();
                        
                    }
                    catch (EntityNotFoundException e)
                    {
                        boxId = null;
                    }
                }  
                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
    
    private class ValidateCreate implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			
            if( documentDate != null && DateUtils.parseJsDate(documentDate) == null){
            	addActionError(sm.getString("NiepoprawnaDataPisma"));
            }
            if (hasActionErrors())
			{
            	saveFile();
			}			
		}
    	
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (hasActionErrors()){
                return;
            }
            OutOfficeDocument doc = null;
            Long journalId = null;
            Date entryDate = null;
            try
            {
	            doc = new OutOfficeDocument();
	
	            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	            doc.setDivisionGuid(DSDivision.ROOT_GUID);
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);
	
	            Sender sender = new Sender();
	
	            doc.setSender(sender);
	
	           

                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

//                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
//                    throw new EdmException("Brak uprawnienia do przyjmowania pism w KO");

                DSApi.context().begin();

                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
               
                documentKind.logic().correctValues(values, documentKind);
                // walidacja nie jest tu raczej potrzebna, gdy� wykonuje si� ona ju� na poziomie Javascriptu
                // Ale js mozna wylaczyc
                //documentKind.logic().validate(values, documentKind);
                
                doc.setSummary(summary != null ? summary : description != null ? description : "");
                
                
                // dla Raport�w numer polisy nie jest wymagany
                // Nationwide.validateNR_POLISY(doctype, doctypeFields);

                // trzeba mie� uprawnienia do nadawania numeru KO
/*
                if (assignOfficeNumber != null && assignOfficeNumber.booleanValue() &&
                    !DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
                    throw new EdmException("Brak uprawnie� do nadawania numeru kancelaryjnego");
*/

                entryDate = GlobalPreferences.getCurrentDay();

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.setClerk(DSApi.context().getPrincipalName());

                doc.setSummary(description != null ? description : documentKind.getName());
                doc.setTitle(description != null ? description : documentKind.getName());
                doc.create();

                doc.setDocumentKind(documentKind);
                newDocumentId = doc.getId();
                doc.setPermissionsOn(false);
                System.out.println(doc.isPermissionsOn());
                documentKind.set(newDocumentId, values);
                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_INTERNAL_OFFICE);
                documentKind.logic().onStartProcess(doc);
                documentKind.logic().documentPermissions(doc);
               
                if (addToBox!=null && addToBox && boxId != null)
                {
                    doc.setBox(Box.find(boxId));
                }
                addFiles(doc);       
                specificAdditions(doc);
                journalId = Journal.getMainInternal().getId();
                doc.setInternal(true);
                  
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	saveFile();
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("Blad przy dodawaniu nowego dokumentu", e);
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            Integer sequenceId = null;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();
                sequenceId = Journal.TX_newEntry2(journalId, newDocumentId, entryDate);

                
                doc = OutOfficeDocument.findOutOfficeDocument(newDocumentId);

                doc.bindToJournal(journalId, sequenceId);

                WorkflowFactory.createNewProcess(doc, AvailabilityManager.isAvailable("coordinators"));

                
                DSApi.context().commit();

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e){
                }

                
                DSApi.context().begin();
                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
//                TaskSnapshot.updateByDocumentId(doc.getId(),OutOfficeDocument.INTERNAL_TYPE);
                documentKind.logic().onEndCreateProcess(doc);
                DSApi.context().commit();
                

                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                    doc.getId(),
                    doc.getRecipients());

/*
                // parametr u�ywany przez result=created w xwork.xml
                setDocumentId(doc.getId());

                if (inDocumentId != null)
                {
                    event.setResult("reply-created");
                }
                else if (Configuration.officeAvailable())
                {
                    event.setResult("task-list-out");
                }
                else
                {
                    addActionMessage("Utworzono pismo" +
                        (doc.getOfficeNumber() != null ? " nr "+doc.getOfficeNumber() : ""));
                    event.setResult("simpleoffice-created");
                }
*/				
                if(createNextDocument)
                {
                	StringBuffer sb = new StringBuffer();
                	sb.append("Utwozono dokument, id");
                	sb.append(' ');
                	sb.append(doc.getId());
                	addActionMessage(sb.toString());
                }
                else if (NR_SZKODY != null && DSApi.context().hasPermission(DSPermission.NW_WPIS_RS))
                {
                    event.setResult("nw-drs");
                }
                else 
                {
                	ServletActionContext.getRequest().getSession().setAttribute("lastCurrentTab", "internal");
                    event.setResult("task-list-int");
                }

                //event.skip(EV_FILL_CREATE); // poprawka po bledzie
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public void setNewDocumentId(Long newDocumentId)
    {
        this.newDocumentId = newDocumentId;
    }

	public Long getBoxId() {
		return boxId;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;
	}

	public String getBoxName() {
		return boxName;
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public Boolean getAddToBox() {
		return addToBox;
	}

	public void setAddToBox(Boolean addToBox) {
		this.addToBox = addToBox;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setDefaultDelivery() throws EdmException {
		// TODO Auto-generated method stub
		
	}

  /*  public boolean isCanReadDAASlowniki() {
        return canReadDAASlowniki;
    }

    public void setCanReadDAASlowniki(boolean canReadDAASlowniki) {
        this.canReadDAASlowniki = canReadDAASlowniki;
    } */
}
