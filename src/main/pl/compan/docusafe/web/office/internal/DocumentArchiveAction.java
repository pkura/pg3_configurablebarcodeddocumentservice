package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;

import java.util.List;
/* User: Administrator, Date: 2007-02-28 14:34:47 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentArchiveAction extends DocumentArchiveTabAction
{
    public String getBaseLink()
    {
        return "/office/internal/document-archive.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/internal/external-workflow.action";
    }

    public String getDocumentType()
    {
        return OutOfficeDocument.INTERNAL_TYPE;
    }

    public List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }
	public int getDocType() {
		return DocumentLogic.TYPE_INTERNAL_OFFICE;
	}
}
