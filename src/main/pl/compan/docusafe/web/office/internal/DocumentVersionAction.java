package pl.compan.docusafe.web.office.internal;

import java.util.List;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.DocumentVersionTabAction;

public class DocumentVersionAction extends DocumentVersionTabAction
	{
	    protected List prepareTabs()
	    {
	    	 return new Tabs(this).createTabs();
	    }

	    public String getBaseLink()
	    {
	        return "/office/internal/document-version.action";
	    }

	    public String getDocumentType()
	    {
	        return OutOfficeDocument.INTERNAL_TYPE;
	    }
		    
	}
