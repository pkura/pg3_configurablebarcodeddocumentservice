package pl.compan.docusafe.web.office.internal;

import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction;

import java.util.List;

/**
 * User: Tomasz
 * Date: 20.02.13
 * Time: 15:09
 */
public class ManualMultiAssignmentAction extends ManualMultiAssignmentTabAction {
    @Override
    public String getDocumentType() {
        return OutOfficeDocument.INTERNAL_TYPE;
    }
    protected List prepareTabs() {
        return new Tabs(this).createTabs();
    }
}
