/**
 * 
 */
package pl.compan.docusafe.web.office.returns;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 30-01-2014
 * ds_trunk_2014
 * ReturnsAction.java
 */
public class MassReturnsAction extends EventActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(MassReturnsAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(MassReturnsAction.class.getPackage().getName(),null);
	
	private String returnsDate;
	/**
	 * Zawiera numery przesy�ki rejestrowanej
	 */
	private Set<String> postalRegNuembersSet =  new HashSet<String>();
	private String postalRegNumberChain;
	
	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSave").
        append(OpenHibernateSession.INSTANCE).
        append(new DoSave()).
        appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			postalRegNumberChain = "";
		}
		
	}
	
	private class DoSave extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log){
			boolean contextOpened = false;
			try {
				
				if (postalRegNumberChain != null && postalRegNumberChain.length() > 0 && returnsDate != null) {
					
					Date dataZwrotu = DateUtils.parseJsDate(returnsDate);
					postalRegNuembersSet.addAll(Arrays.asList(postalRegNumberChain.split(",")));
					contextOpened = DSApi.openContextIfNeeded();
					for (String entry : postalRegNuembersSet) {
						if(entry.length() > 0){
							List<OutOfficeDocument> outDoc = OutOfficeDocument.findByPostalRegNumber(entry);
							if (outDoc != null) {
								for (OutOfficeDocument bean : outDoc) {
									bean.setZpoDate(dataZwrotu);
									bean.setZpo(true);
									addActionMessage(sm.getString("Numer listu poleconego " + bean.getPostalRegNumber() + " powi�zano z dokumentem o ID: " + bean.getId()));
									DSApi.context().session().save(bean);
									DSApi.context().session().flush();
								}
							}
						}
					}
				}
			} catch (EdmHibernateException e) {
				log.error(e.getMessage(), e);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				if (contextOpened) {
					DSApi.closeContextIfNeeded(contextOpened);
				}
			}
		}

		@Override
		public pl.compan.docusafe.util.Logger getLogger() {
			return log;
		}
	}

	public String getReturnsDate() {
		return returnsDate;
	}

	public void setReturnsDate(String returnsDate) {
		this.returnsDate = returnsDate;
	}

	public Set<String> getPostalRegNuembersSet() {
		return postalRegNuembersSet;
	}

	public void setPostalRegNuembersSet(Set<String> postalRegNuembersSet) {
		this.postalRegNuembersSet = postalRegNuembersSet;
	}

	public String getPostalRegNumberChain() {
		return postalRegNumberChain;
	}

	public void setPostalRegNumberChain(String postalRegNumberChain) {
		this.postalRegNumberChain = postalRegNumberChain;
	}

	
}
