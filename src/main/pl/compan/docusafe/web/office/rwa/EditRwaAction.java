package pl.compan.docusafe.web.office.rwa;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.office.RwaNotFoundException;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.RwaElementVisitor;
import pl.compan.docusafe.web.tree.RwaTree;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditRwaAction.java,v 1.20 2009/12/10 15:30:44 tomekl Exp $
 */
public class EditRwaAction extends EventActionSupport
{
    // @EXPORT
    private String treeHtml;
    private Rwa rwa;
    private boolean canModify;
    
    // @EXPORT/@IMPORT
    private Integer id;
    private String newDescription;
    private String newRwaCode;
    private String newAcHome;
    private String newAcOther;
    private String newRemarks;
    private String newRwaValue;

    // @IMPORT
    private String description;
    private String rwaCode;
    private String acHome;
    private String acOther;
    private String remarks;
    private boolean isActive;
	private boolean showActive;
    
    private FormFile updateRwa;
    private boolean rwaUpdateFromFile;
    
    
    protected void setup()
    {
    	
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdateRwa").
            append(OpenHibernateSession.INSTANCE).
            append(new UpdateRwa()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(newDescription))
                addActionError("Nie podano opisu kategorii RWA");
            if (StringUtils.isEmpty(newRwaCode))
                addActionError("Nie podano warto�ci kodu RWA");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
                    throw new EdmException("Brak uprawnie� do modyfikacji RWA");

                if (id != null)
                {
                    Rwa rwa = Rwa.find(id);

                    rwa.createChild(newRwaCode, newDescription,
                        TextUtils.trimmedStringOrNull(newRemarks),
                        TextUtils.trimmedStringOrNull(newAcHome),
                        TextUtils.trimmedStringOrNull(newAcOther));
                }
                else
                {
                    Rwa.createRoot(newRwaCode, newDescription,
                        TextUtils.trimmedStringOrNull(newRemarks),
                        TextUtils.trimmedStringOrNull(newAcHome),
                        TextUtils.trimmedStringOrNull(newAcOther));
                }

                DSApi.context().commit();

                newRwaCode = null;
                newDescription = null;
                newRemarks = null;
                newAcHome = null;
                newAcOther = null;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(description))
                addActionError("Nie podano opisu kategorii RWA");

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
                    throw new EdmException("Brak uprawnie� do modyfikacji RWA");

                if (id == null){
                	DSApi.context().setRollbackOnly();
                	return;
                }
                
                Rwa rwa = Rwa.find(id);                
                if(newRwaValue.equals(rwa.getRwa()))
                {    
                    rwa.setDescription(description);
                    rwa.setRemarks(TextUtils.trimmedStringOrNull(remarks));
                    rwa.setAcHome(TextUtils.trimmedStringOrNull(acHome));
                    rwa.setAcOther(TextUtils.trimmedStringOrNull(acOther));
                    rwa.setIsActive(isActive);
                    DSApi.context().commit();
                	return;
                }
                
                if(newRwaValue.startsWith(rwa.getRwa()))
                	throw new EdmException("Nie mo�na podmontowac rekordu do samego siebie");
                
                boolean exists = true;
                try{
                	Rwa.findByCode(newRwaValue);
                }
                catch(RwaNotFoundException rnfe){
                	exists = false;
                }
                
                if(Integer.parseInt(newRwaValue)<0)
                	throw new EdmException("Nie mo�na podac ujemnego kodu !");
                
                if(exists)
                	throw new EdmException("Istnieje ju� rekord o podanym kodzie !");
                
                exists = true;
                if(newRwaValue.length()>1)
                {
                	try{
                		Rwa.findByCode(newRwaValue.substring(0,newRwaValue.length()-1));		
                	}
                	catch(RwaNotFoundException rnfe){
                		exists = false;
                	}
                }	
                if(!exists)
                	throw new EdmException("Nie istnieje rekord o kodzie "+
                						   newRwaValue.substring(0,newRwaValue.length()-1)+ 
                						   " do kt�rego mo�naby podmontowa� podany rekord.");
                
                String root_rwa = rwa.getRwa();
                Stack<Rwa> stos = new Stack<Rwa>();
                List<Rwa> lista = new LinkedList<Rwa>();
                int start_level = rwa.getLevel(),max_level = 0;
                stos.push(rwa);
                while(!stos.empty())
                {
                	Rwa tmp_rwa = stos.pop();
                	lista.add(tmp_rwa);
                	int lev = tmp_rwa.getLevel();
                	max_level = max_level>=lev?max_level:lev;
                	List l = tmp_rwa.getChildren(showActive);
                	for(int i=0;i<l.size();i++)
                			stos.push((Rwa)l.get(i));
                }
                
                int sub_level = max_level-start_level;
                if(newRwaValue.length()+sub_level>5)
                	throw new EdmException("Nie mo�na podmontowa� tego rekordu, gdy� najd�u�sza �ciezka " +
                						   "przekroczy�aby 5");
                
                int root_len = rwa.getLevel();
                int len = newRwaValue.length() - rwa.getLevel();
                for(int i=0;i<lista.size();i++)
                {
                	Rwa tmp_rwa = lista.get(i);
                	String current_rwa = tmp_rwa.getRwa();
                	int set_len = tmp_rwa.getLevel()+len;
                	tmp_rwa.setLevel(set_len);
                	for(int j=0;j<set_len;j++)
                	{
                		String c;
                		if(j<newRwaValue.length())
                			c = ""+newRwaValue.charAt(j);
                		else
                			c = ""+current_rwa.charAt(root_rwa.length()+j-newRwaValue.length());
                		tmp_rwa.setDigitAt(j+1,c);
                	}
                	for(int j=set_len;j<=4;j++)
                		tmp_rwa.setDigitAt(j+1,null);
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
                    throw new EdmException("Brak uprawnie� do modyfikacji RWA");

                if (id != null)
                {
                    Rwa rwa = Rwa.find(id);
                    if (rwa.canDelete())
                    	rwa.delete();
                    else
                    	addActionError("Nie mo�na usun�� podanej kategorii poniewa� jest ona u�ywana.");
                    id=null;
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final UrlVisitor urlVisitor = new UrlVisitor()
            {
                public String getUrl(Object element)
                {
                    if (element instanceof Rwa)
                        return ServletActionContext.getRequest().getContextPath()+"/office/rwa/edit-rwa.action?id="+((Rwa) element).getId()+"&showActive="+ getShowActive() ;
                    else
                        return ServletActionContext.getRequest().getContextPath()+"/office/rwa/edit-rwa.action";
                }
            };
            rwaUpdateFromFile = DSApi.context().userPreferences().node("other").getBoolean("rwaUpdateFromFile", false);
            RwaElementVisitor rwaElementVisitor = new RwaElementVisitor()
            {
                public String getDescription(Rwa rwa)
                {
                    return rwa.getRwa()+": "+rwa.getDescription();
                }
                public String getLink(Rwa rwa)
                {
                    return urlVisitor.getUrl(rwa);
                }
            };

            try
            {
                rwa = id != null ? Rwa.find(id) : null;
                DSApi.initializeProxy(rwa);

                canModify = DSApi.context().hasPermission(DSPermission.RWA_ZMIANA);

                treeHtml = RwaTree.newTree(urlVisitor, rwaElementVisitor, rwa,
                    ServletActionContext.getRequest().getContextPath(), showActive).generateTree();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class UpdateRwa implements ActionListener
    {
        
        public void actionPerformed(ActionEvent event)
        {
            
            if (updateRwa == null || !updateRwa.sensible())
                addActionError("Nie podano nazwy pliku lub plik ma zerowa dlugosc");
            if (updateRwa.getFile().length() < 64)
                addActionError("Przekazany plik jest zbyt maly, byc moze nie jest to prawidlowy plik z RWA");
            
            try
            {
                
                File file = updateRwa.getFile();
                FileReader fr = new FileReader(file);
                LineNumberReader lr = new LineNumberReader(fr);
                String line;
                
                String i = "";
                String poziom = "";
                String opis = "";
                String kat_m = "";
                String kat_inn = "";
                String uwagi = "";
                
                int counter = -1;
                
                String[] rwa_temp = null;
                
                while ((line = lr.readLine()) != null)
                {
                    line = line.trim();
                    if(line.length() < 1) continue;
                    //if(line.equals("")) continue;
                    line = new String(line.getBytes(),"UTF-8");
                    
                    try
                    {
                        
                        int i_int = Integer.parseInt(line);
                        
                        rwa_temp = new String[]{i,poziom,opis,kat_m,kat_inn,uwagi};
                        goThroughLines(rwa_temp);
                        i = new String(line);
                        
                        poziom = "" + i.length();
                        opis = "";
                        kat_m = "";
                        kat_inn = "";
                        uwagi = "";
                        counter = -1;
                    }
                    catch(NumberFormatException e)
                    {
                        
                        switch(counter)
                        {
                            case 0: opis = new String(line);
                            case 1: if(opis.equals(line)) break; 
                                kat_m = new String(line);
                            case 2: if(kat_m.equals(line)) break; 
                                if(!(line.length() > 3)) kat_inn = new String(line); 
                                else uwagi = new String(line);
                            case 3: if(kat_inn.equals(line)) break; 
                                uwagi = new String(line);
                            default: break;
                        }
                    }
                    catch(Exception e)
                    {
                        continue; 
                    }
                    finally
                    {
                        counter++;
                    }
                    
                }
                fr.close();
                lr.close();
            }
            catch (IOException e)
            {
                addActionError("Nie mozna odczytac przekazanego pliku, skontaktuj sie z administratorem systemu");
            }

        }
        
        private boolean goThroughLines(String[] lines)
        {
            boolean result = false;
            {
                RWA rwa = new RWA(new String(lines[0]),
                        new String(lines[1]),
                        new String(lines[2]),
                        new String(lines[3]),
                        new String(lines[4]),
                        new String(lines[5]));
                try
                {
                rwa.updateRwaInDB();
                }
                catch(Exception e)
                {
                    //addActionError(e.getMessage());
                }
                result = true;
            }
            return result;
        }
    }
    
    private class RWA
    {
        String kod;
        String poziom;
        String opis;
        String kat_m;
        String kat_inn;
        String uwagi;
        Boolean isActive;
        
       



		RWA(String kod, String poziom, String opis, String kat_m, String kat_inn, String uwagi) {
            this.kod = kod;
            this.poziom = poziom;
            this.opis = opis;
            this.kat_m = kat_m;
            this.kat_inn = kat_inn;
            this.uwagi = uwagi;
        }

		public String toString()
        {
            return "Kod: " + kod 
            + "\nPoziom: " + poziom 
            + "\nOpis: " + opis 
            + "\nKategoria macierzysta: " + kat_m
            + "\nKategoria pozostala: " + kat_inn
            + "\nUwagi: " + uwagi;
        }
        
        public void updateRwaInDB() throws EdmException{
            try
            {
                DSApi.context().begin();
                if (!DSApi.context().hasPermission(DSPermission.RWA_ZMIANA))
                    throw new EdmException("Brak uprawnien do modyfikacji RWA");
                Rwa rwa = Rwa.findByCode(kod);
                /* podane rwa istnieje - wystarczy je wyedytowac */
                    rwa.setDescription(TextUtils.trimmedStringOrNull(opis));
                    rwa.setRemarks(TextUtils.trimmedStringOrNull(uwagi));
                    rwa.setAcHome(TextUtils.trimmedStringOrNull(kat_m));
                    rwa.setAcOther(TextUtils.trimmedStringOrNull(kat_inn));
            }
            catch (RwaNotFoundException e)
            {
                if(kod != null && kod.length() > 0){
                    /* podane rwa nie istnieje - nalezy je utworzyc */

                    Rwa rwa = Rwa.findByCode(kod.substring(0,kod.length()-1));
                    rwa.createChild(TextUtils.trimmedStringOrNull(kod),
                        TextUtils.trimmedStringOrNull(opis),
                        TextUtils.trimmedStringOrNull(uwagi),
                        TextUtils.trimmedStringOrNull(kat_m),
                        TextUtils.trimmedStringOrNull(kat_inn));
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                //addActionError(e.getMessage());
            }
            finally
            {
                try
                {
                    DSApi.context().commit();
                    DSApi.context().session().flush();
                    DSApi.context().session().clear();
                }
                catch (EdmException e)
                {
                }
            }
        }
        
    }

    
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public Rwa getRwa()
    {
        return rwa;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setRwaCode(String rwaCode)
    {
        this.rwaCode = rwaCode;
    }

    public void setAcHome(String acHome)
    {
        this.acHome = acHome;
    }

    public void setAcOther(String acOther)
    {
        this.acOther = acOther;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getNewDescription()
    {
        return newDescription;
    }

    public void setNewDescription(String newDescription)
    {
        this.newDescription = newDescription;
    }

    public String getNewRwaCode()
    {
        return newRwaCode;
    }

    public void setNewRwaCode(String newRwaCode)
    {
        this.newRwaCode = newRwaCode;
    }

    public String getNewAcHome()
    {
        return newAcHome;
    }

    public void setNewAcHome(String newAcHome)
    {
        this.newAcHome = newAcHome;
    }

    public String getNewAcOther()
    {
        return newAcOther;
    }

    public void setNewAcOther(String newAcOther)
    {
        this.newAcOther = newAcOther;
    }

    public String getNewRemarks()
    {
        return newRemarks;
    }

    public void setNewRemarks(String newRemarks)
    {
        this.newRemarks = newRemarks;
    }

    public boolean isCanModify()
    {
        return canModify;
    }

	public String getNewRwaValue() {
		return newRwaValue;
	}

	public void setNewRwaValue(String newRwaValue) {
		this.newRwaValue = newRwaValue;
	}
    
    public void setUpdateRwa(FormFile updateRwa)
    {
        this.updateRwa = updateRwa;
    }
    
    public FormFile getUpdateRwa()
    {
        return this.updateRwa;
    }
    
    public boolean getRwaUpdateFromFile()
    {
        return rwaUpdateFromFile;
    }
    
    public void setRwaUpdateFromFile(boolean rwaUpdateFromFile)
    {
        this.rwaUpdateFromFile = rwaUpdateFromFile;
    }
    
    public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public boolean isShowActive() {
			return showActive;
	}
	public boolean getShowActive() {
		return showActive;
	}

	public void setShowActive(boolean showActive) {
			this.showActive = showActive;
	}
}
