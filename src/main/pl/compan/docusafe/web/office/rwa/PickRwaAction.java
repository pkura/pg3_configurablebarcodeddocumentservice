package pl.compan.docusafe.web.office.rwa;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.office.RwaNotFoundException;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.RwaElementVisitor;
import pl.compan.docusafe.web.tree.RwaTree;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PickRwaAction.java,v 1.7 2006/02/20 15:42:46 lk Exp $
 */
public class PickRwaAction extends EventActionSupport
{
    private Integer id;
    private String treeHtml;

    private String requestedRwa;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            UrlVisitor urlVisitor = new UrlVisitor()
            {
                public String getUrl(Object element)
                {
                    if (element instanceof Rwa)
                        return ServletActionContext.getRequest().getContextPath()
                            + "/office/rwa/pick-rwa.action?id="+((Rwa) element).getId();
                    else
                        return ServletActionContext.getRequest().getContextPath()
                            + "/office/rwa/pick-rwa.action";
                }
            };

            RwaElementVisitor rwaElementVisitor = new RwaElementVisitor()
            {
                public String getDescription(Rwa rwa)
                {
                    return rwa.getRwa()+": "+rwa.getDescription();
                }

                public String getLink(Rwa rwa)
                {
                    if (rwa != null)
                    {
                        return "javascript:void(pickRwa("+rwa.getId()+
                            ", \""+rwa.getRwa()+": "+rwa.getDescription()+"\"))";
                    }
                    else
                    {
                        return "javascript:void(null)";
                    }
                }
            };

            try
            {
                Rwa rwa;

                // je�eli istnieje parametr "requestedRwa", znajduje si�
                // w nim kod ��danego rwa, na kt�rym powinno zosta� otwarte
                // drzewko
                if (!StringUtils.isEmpty(requestedRwa))
                {
                    try
                    {
                        id = Rwa.findByCode(requestedRwa).getId();
                        event.setResult("branch");
                        event.cancel();
                        return;
                    }
                    catch (RwaNotFoundException e)
                    {
                        rwa = id != null ? Rwa.find(id) : null;
                    }
                }
                else
                {
                    try
                    {
                        rwa = id != null ? Rwa.find(id) : null;
                    }
                    catch (RwaNotFoundException e)
                    {
                        rwa = null;
                    }
                }

                treeHtml = RwaTree.newTree(urlVisitor, rwaElementVisitor, rwa,
                    ServletActionContext.getRequest().getContextPath(), false ).generateTree();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public void setRequestedRwa(String requestedRwa)
    {
        this.requestedRwa = requestedRwa;
    }
}
