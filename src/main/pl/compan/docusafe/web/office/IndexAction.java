package pl.compan.docusafe.web.office;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class IndexAction extends EventActionSupport 
{

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if(AvailabilityManager.isAvailable("office.index.new-int"))
            	{
            		if(DSApi.context().hasPermission(DSPermission.PISMO_WEWNETRZNE_TWORZENIE))
            			event.setResult("new-int");
            		else
            			event.setResult("abs-without-procces");
            		return;
            	}
                if (Docusafe.moduleAvailable(Modules.MODULE_OFFICE) || Docusafe.moduleAvailable(Modules.MODULE_FAX)) 
                {
                   	if (AvailabilityManager.isAvailable("bookmarks.on")) 
                	{
                		event.setResult("bookmarks");
                	}
                	else if(AvailabilityManager.isAvailable("menu.left.kancelaria.listazadan"))
                	{
                		event.setResult("task-list");
                	} 
                	else
                	{
                		event.setResult("new-in");
                	}
				} 
                else
                {
					event.setResult("find");
				}
            }
            catch (Exception e)
            {
                event.setResult("find");
            }
        }
    }
}
