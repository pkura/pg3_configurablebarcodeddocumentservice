package pl.compan.docusafe.web.office;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.webwork.event.EventActionSupport;



public class PackageBaseAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(PackageBaseAction.class);

    String nr_paczki;
    String nrpaczki;
    Integer typDokumentu;
    Map<Integer,String> typyDokumentow;
    String opis;
    Long lokalizacja;
    Long odbiorcaLokalizacja;


	Integer rodzajPrzesylki;

    static DocumentKind documentKind;

    String transportServiceName;
    
	@Override
	protected void setup() 
	{

	}
	
	
	/**
	 * Tworzy nowa paczke na podstawie danych zapisanych w akcji
	
	
	/**
	 * Metoda okresla czy uzytkownik ma prawo operowac na paczce
	 * @return
	 * @throws EdmException
	 */
	
	public void fillForm() throws EdmException
    {
		if(documentKind == null)
		{
			documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
		}
		
		if (odbiorcaLokalizacja == null)
		{
			log.trace("lokalizacja odbiorcy = null - przyjmujemy domyslna");
			try
			{
				String recId = Docusafe.getAdditionProperty("prosika.default.inparcel.receiver");
				log.trace("prosika.default.inparcel.receiver={}",recId);
				if (recId != null)
				{					
					odbiorcaLokalizacja = new Long(recId); 
				}
				else
				{
					log.warn("nie ustalono wlasnosci prosika.default.inparcel.receiver");
				}
			}
			catch (Exception e)
			{
				log.warn("Blad w czasie odczytu domyslnej lokalizacji odbiorcy paczki",e);
			}
		}
		else
		{
			log.trace("lokalizacja odbiorcy={}",odbiorcaLokalizacja);
		}

    }

	public String getNr_paczki() {
		return nr_paczki;
	}

	public void setNr_paczki(String nr_paczki) {
		this.nr_paczki = nr_paczki;
	}

	public Integer getTypDokumentu() {
		return typDokumentu;
	}

	public void setTypDokumentu(Integer typDokumentu) {
		this.typDokumentu = typDokumentu;
	}

	public Map<Integer, String> getTypyDokumentow() {
		return typyDokumentow;
	}

	public void setTypyDokumentow(Map<Integer, String> typyDokumentow) {
		this.typyDokumentow = typyDokumentow;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Long getLokalizacja() {
		return lokalizacja;
	}

	public void setLokalizacja(Long lokalizacja) {
		this.lokalizacja = lokalizacja;
	}

	public Integer getRodzajPrzesylki() {
		return rodzajPrzesylki;
	}

	public void setRodzajPrzesylki(Integer rodzajPrzesylki) {
		this.rodzajPrzesylki = rodzajPrzesylki;
	}

	

	public static DocumentKind getDocumentKind() {
		return documentKind;
	}

	public static void setDocumentKind(DocumentKind documentKind) {
		PackageBaseAction.documentKind = documentKind;
	}



	public void setNrpaczki(String nrpaczki) {
		this.nrpaczki = nrpaczki;
	}

	public String getNrpaczki() {
		return nrpaczki;
	}

	public String getTransportServiceName() {
		return transportServiceName;
	}

	public void setTransportServiceName(String transportServiceName) {
		this.transportServiceName = transportServiceName;
	}
	
    public Long getOdbiorcaLokalizacja() {
		return odbiorcaLokalizacja;
	}

	public void setOdbiorcaLokalizacja(Long odbiorcaLokalizacja) {
		this.odbiorcaLokalizacja = odbiorcaLokalizacja;
	}

}
