package pl.compan.docusafe.web.office;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;

import javax.servlet.http.HttpServletRequest;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RolesAction.java,v 1.8 2008/04/09 11:52:25 mariuszk Exp $
 */
public class RolesAction extends EventProcessingAction
{
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(RolesAction.class.getPackage().getName(),null);
    public static final String FORWARD = "office/roles";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doCreate").
            append(new ValidateCreate()).
            append("create", new Create()).
            append(fillForm).
            append(new SetActionForwardListener("main"));

        registerListener("doDelete").
            append(new ValidateDelete()).
            append("delete", new Delete()).
            append(fillForm).
            append(new SetActionForwardListener("main"));
    }

    private class ValidateDelete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long[] roleIds = (Long[]) event.getDynaForm().get("roleIds");

            if (roleIds == null || roleIds.length == 0)
                event.getErrors().add(sm.getString("roles.noRoleChecked"));

            if (event.getErrors().size() > 0)
                event.skip("delete");
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long[] roleIds = (Long[]) event.getDynaForm().get("roleIds");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);

                for (int i=0; i < roleIds.length; i++)
                {
                    Role role = Role.find(roleIds[i]);
                    event.getMessages().add(sm.getString("UsunietoRole",role.getName()));
                    role.delete();
                    cache.remove(role);
                }

                ctx.commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String name = (String) event.getDynaForm().get("roleName");

            if (StringUtils.isEmpty(name))
                event.getErrors().add(sm.getString("roles.missingName"));

            if (event.getErrors().size() > 0)
                event.skip("create");
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String name = (String) event.getDynaForm().get("roleName");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                Role role = new Role(StringUtils.trim(name));
                role.create();

                ctx.commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));

                Role[] roles = Role.list();
                DynaBean[] beans = new DynaBean[roles.length];

                for (int i=0, n=roles.length; i < n; i++)
                {
                    DynaBean bean = DynaBeans.bean(DynaBeans.role);
                    beans[i] = bean;

                    bean.set("id", roles[i].getId());
                    bean.set("name", roles[i].getName());
                }

                event.getRequest().setAttribute("roles", beans);
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(HttpServletRequest request)
    {
        ModuleConfig config = (ModuleConfig) request.getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath();
    }
}
