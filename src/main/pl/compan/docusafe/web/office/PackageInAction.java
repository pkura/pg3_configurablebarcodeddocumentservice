package pl.compan.docusafe.web.office;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



@SuppressWarnings("serial")
public class PackageInAction extends PackageBaseAction
{

	private static final Log log = LogFactory.getLog(PackageInAction.class);
	
	
	private String[] barkod;
	private String[] kody;
    
	protected void setup() 
	{		
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		
	}
	
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			try
			{
				
			}
			catch (Exception e) 
			{
				LogFactory.getLog("eprint").error("", e);
				addActionError(e.getMessage());
			}
        }
    }
	
   
}