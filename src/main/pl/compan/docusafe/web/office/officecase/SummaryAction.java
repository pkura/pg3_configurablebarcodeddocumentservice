package pl.compan.docusafe.web.office.officecase;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.Date;
import java.util.List;
/* User: Administrator, Date: 2005-09-05 11:59:26 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SummaryAction.java,v 1.4 2009/03/19 07:58:27 mariuszk Exp $
 */
public class SummaryAction extends BaseAction
{
	private static final Logger log = LoggerFactory.getLogger(SummaryAction.class);
    private String officeId;
    private Date finishDate;
    private String author;
    private String rwa;
    private String portfolioOfficeId;
    private String priority;
    private String status;
    private List documents;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                OfficeCase officeCase = OfficeCase.find(getCaseId());

                officeId = officeCase.getOfficeId();
                finishDate = officeCase.getFinishDate();
                author = DSUser.safeToFirstnameLastname(officeCase.getAuthor());
                rwa = officeCase.getRwa().toString();
                portfolioOfficeId = officeCase.getParent().getOfficeId();
                priority = officeCase.getPriority().getName();
                status = officeCase.getStatus().getName();

                documents = officeCase.getDocuments();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                addActionError(e.getMessage());
            }
        }
    }

    public String getOfficeId()
    {
        return officeId;
    }

    public Date getFinishDate()
    {
        return finishDate;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getRwa()
    {
        return rwa;
    }

    public String getPortfolioOfficeId()
    {
        return portfolioOfficeId;
    }

    public String getPriority()
    {
        return priority;
    }

    public String getStatus()
    {
        return status;
    }

    public List getDocuments()
    {
        return documents;
    }
}
