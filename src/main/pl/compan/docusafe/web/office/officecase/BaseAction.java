package pl.compan.docusafe.web.office.officecase;

import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/* User: Administrator, Date: 2005-09-05 11:57:04 */

/**
 * TODO: automatyczne tworzenie linka bazowego do stron, wyszukiwania etc.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BaseAction.java,v 1.2 2006/02/20 15:42:46 lk Exp $
 */
public abstract class BaseAction extends EventActionSupport
{
    private Long caseId;
    private String returnUrl;

    public Tabs getTabs()
    {
        Tabs tabs = new Tabs();
        tabs.add(new Tab("Podsumowanie", "Podsumowanie",
            HttpUtils.makeUrl("/office/officecase/summary.action", new Object[] { "id", caseId }),
            this instanceof SummaryAction));

        return tabs;
    }

    public final Long getCaseId()
    {
        return caseId;
    }

    public final void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public final String getReturnUrl()
    {
        return returnUrl;
    }

    public final void setReturnUrl(String returnUrl)
    {
        this.returnUrl = returnUrl;
    }
}
