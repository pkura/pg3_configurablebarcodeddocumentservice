package pl.compan.docusafe.web.office.journals;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import std.fun;
import std.lambda;

/**
 * Klasa bazowa dla akcji wy�wietlaj�cych dzienniki w r�nych formatach.
 * Deklaruje szereg metod abstrakcyjnych, kt�rych wynik dzia�ania
 * zale�y od rodzaju pism w dzienniku, poza tym udost�pnia og�lne
 * metody s�u��ce do wyszukiwania w dziennikach.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: JournalActionBase.java,v 1.57 2010/06/29 06:53:12 mariuszk Exp $
 */
public abstract class JournalActionBase extends EventActionSupport
{
    public static final String SEARCH_DAY = "day";
    public static final String SEARCH_DAYRANGE = "dayrange";
    public static final String SEARCH_NUMRANGE = "numrange";
    public static final String SEARCH_NUMRANGE_DAILY = "numrangedaily";
    public static final String SEARCH_NUMLIST = "numlist";

    private String postalregnumber;
    private Long id;
    private String searchType;
    private String day;
    private String dayDailyOfficeNumber;
    private String dayNumList;
    private String fromDay;
    private String toDay;
    private Integer fromHour;
    private Integer toHour;
    private Integer fromNum;
    private Integer toNum;
    private Integer fromNumD;
    private Integer toNumD;
    private String numList;
    private boolean alldelivery;
    private Integer[] delivery;
    private String linkType;
    private String printLinkType;
    protected List deliveryList;

    // dodatkowe kryteria dla pism przychodz�cych
    private String sender;
    private String referenceId;
    private Integer[] kindIds;
    private boolean allKinds;
    private String author;
    private String creatingEntry;

    // dodatkowe kryteria dla pism wychodz�cych
    private String recipient;
    private String summary;

    private boolean zpo;
    private boolean sortByLocation;
    private boolean sortByRegNumber;
    private Integer minNum;
    private Integer maxNum;
    private Journal journal;
    private List results;
    private String printLink;
    private String printParamLink;
    private String title;
    private List deliveries;
    private List kinds;
    private List users;
    private Pager pager;
    private int totalCount;
    private int limit = 10;
    private int offset;
    private boolean redirected;

    private String searchLink;
    private String sortBy;
    private boolean ascending;    
	protected String userDivision;
	protected String userLastname;
	protected String userFirstname;
	protected String recipientUser;
    protected List<DSUser> fastAssignmentUser;
    protected List<DSDivision> fastAssignmentDivision;
    protected String authorDivision;
    protected String weigth;
    private Integer weigthFrom;
    private Integer weigthTo;
    private boolean canReadDictionary;
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);



    public Integer getFromNumD() {
		return fromNumD;
	}

	public void setFromNumD(Integer fromNumD) {
		this.fromNumD = fromNumD;
	}

	public Integer getToNumD() {
		return toNumD;
	}

	public void setToNumD(Integer toNumD) {
		this.toNumD = toNumD;
	}

	protected abstract void checkJournalType(Journal journal) throws EdmException;

    protected abstract OfficeDocument getDocument(Long documentId) throws EdmException;

    protected abstract String getDocumentLink(OfficeDocument document);

    protected abstract String getDocumentWhere(OfficeDocument document) throws EdmException;

    protected abstract List<? extends OfficeDocumentDelivery> getDocumentDeliveries() throws EdmException;

    protected abstract OfficeDocumentDelivery getDocumentDelivery(OfficeDocument document)
        throws EdmException;

    protected abstract String getJournalPrintLink();
    
    private List<TableColumn> columns;
    
    public List<TableColumn> getColumns() {
		return columns;
	}

    /**
     * Sprawdza poprawno�� przekazanych argument�w, znalezione
     * b��dy s� zapisywane przy pomocy {@link #addActionError(String)} .
     * <p>
     * (Metoda jest implementacj� wzorca Template Method).
     */
    protected boolean validateForm()
    {
        if (SEARCH_DAY.equals(getSearchType()))
        {
            if (DateUtils.nullSafeParseJsDate(getDay()) == null)
            {
            	addActionError(sm.getString("NiePodanoDatyLubPodanaDataJestNieprawidlowa"));
                return false;
            }
        }
        else if (SEARCH_DAYRANGE.equals(getSearchType()))
        {
            if (DateUtils.nullSafeParseJsDate(getFromDay()) == null ||
                DateUtils.nullSafeParseJsDate(getToDay()) == null)
            {
            	addActionError(sm.getString("NiePodanoJednejLubObydwuDatZakresuLubDatySaNieprawidlowe"));
                return false;
            }
        }
        else if (SEARCH_NUMRANGE.equals(getSearchType()))
        {
            if (getFromNum() == null || getToNum() == null)
            {
            	addActionError(sm.getString("NiePodanoJednegoLubObydwuNumerowZakresu"));
                return false;
            }
        }
        else if (SEARCH_NUMLIST.equals(getSearchType()))
        {
            if (StringUtils.isEmpty(getNumList()))
            {
            	addActionError(sm.getString("NiePodanoNumerowPozycjiDziennika"));
                return false;
            }
            if (AvailabilityManager.isAvailable("dailyOfficeNumber") && DateUtils.nullSafeParseJsDate(getDayNumList()) == null)
            {
            	addActionError(sm.getString("NiePodanoDatyLubPodanaDataJestNieprawidlowa"));
                return false;
            }

            StringTokenizer st = new StringTokenizer(getNumList(), ", ");
            try
            {
                while (st.hasMoreTokens())
                {
                    String range = st.nextToken();
                    if (range.indexOf('-') > 0)
                    {
                    	Integer.parseInt(range.substring(0, range.indexOf('-')));
                        Integer.parseInt(range.substring(range.indexOf('-')+1));
                    }
                    else
                    {
                        if (Integer.parseInt(range) < 0)
                        {
                        	addActionError(sm.getString("NumeryPozycjiDziennikaNieMogaBycUjemne"));
                            return false;
                        }
                    }
                }
            }
            catch (NumberFormatException e)
            {
            	addActionError(sm.getString("CoNajmniejJedenZpodanychNumerowPozycjiDziennikaJestNieprawidlowy"));
                return false;
            }
        }
        return true;
    }

    /**
     * Zwraca list� bean�w (Map) zawieraj�cych w�asno�ci: document, entry,
     * where, link.
     * @return List<Map>
     */
    protected List find(Journal journal) throws EdmException
    {
    	return find(journal, null);
    }

    /**
     * Zwraca list� bean�w (Map) zawieraj�cych w�asno�ci: document, entry,
     * where, link.
     * @return List<Map>
     */
    protected List find(Journal journal, String orderBy) throws EdmException
    {
    	Date dateDay = DateUtils.nullSafeParseJsDate(getDay());
    	Date dateDayDayDailyOfficeNumber = DateUtils.nullSafeParseJsDate(getDayDailyOfficeNumber());
    	Date dateDaynumList = DateUtils.nullSafeParseJsDate(getDayNumList());
        Date dateFromDay = DateUtils.nullSafeParseJsDate(getFromDay());
        Date dateToDay = DateUtils.nullSafeParseJsDate(getToDay());

        // nie wybrano �adnego sposobu dostarczenia - zero wynik�w
        if (!alldelivery && (getDelivery() == null || getDelivery().length == 0))
            return null;

        sender = TextUtils.trimmedStringOrNull(sender);
        referenceId = TextUtils.trimmedStringOrNull(referenceId);
        author = TextUtils.trimmedStringOrNull(author);
        recipient = TextUtils.trimmedStringOrNull(recipient);
        summary = TextUtils.trimmedStringOrNull(summary);
        author = TextUtils.trimmedStringOrNull(author);

        journal = Journal.find(getId());
        // XXX: zmiana

        checkJournalType(journal);
//        if (!journal.isIncoming())
//            throw new EdmException("Spodziewano si� dziennika pism przychodz�cych");

        Journal.JournalQuery query = new Journal.JournalQuery(getId(),limit,offset);
        query.sortByLocation(sortByLocation);
        query.sortByRegNumber(sortByRegNumber);
        query.setZpo(zpo);
        query.summary(summary);
        query.sender(sender);
        query.recipient(recipient);
        query.setRecipientFirstname(TextUtils.trimmedStringOrNull(userFirstname));
        query.setRecipientLastname(TextUtils.trimmedStringOrNull(userLastname));
        query.setRecipientOrganization(TextUtils.trimmedStringOrNull(userDivision));
       // query.referenceId(referenceId);
        query.setAuthor(author);
        query.setSummary(summary);
        query.setPostalregnumber(postalregnumber);
        query.setAuthorDivision(authorDivision);
        query.setCreatingEntry(creatingEntry);
        
        query.setRecipientUser(recipientUser);
        
        
        if (weigthFrom != null)
        	query.setWeigthFrom(weigthFrom);
        if (weigthTo != null)
        	query.setWeigthTo(weigthTo);
        /*if (sortBy != null) {
	        query.setOrderBy(sortBy);
	        query.setAscending(ascending);
        } else if (DocumentKind.isAvailable(DocumentKind.NATIONWIDE_KIND)) {
        	query.setOrderBy("officenumber");
 	        query.setAscending(false);
        }*/
        
        
        if(sortBy == null && (DocumentKind.isAvailable(DocumentLogicLoader.NATIONWIDE_KIND) || DocumentKind.isAvailable(DocumentLogicLoader.DP_PTE_KIND)))
        {
        	sortBy = "officenumber";
        	ascending = false;
        }
        else if(sortBy == null && (DocumentKind.isAvailable(DocumentLogicLoader.P4_KIND)))
        {
        	sortBy = "incomingdate";
        	ascending = Boolean.FALSE;
        }
        
        query.setOrderBy(sortBy);
        query.setAscending(ascending);
        

        if (!alldelivery)
        {
            deliveryList = getDocumentDeliveries();
            Integer[] deliveryIds = getDelivery();

            if (deliveryIds != null && deliveryList != null && deliveryIds.length == deliveryList.size())
                alldelivery = true;

            if (!alldelivery)
                query.deliveries(getDelivery());
        }

        if (!allKinds)
        {
            List kindList = InOfficeDocumentKind.list();
            if (kindIds != null && kindIds.length == kindList.size())
                allKinds = true;

            if (!allKinds)
                query.incomingKinds(getKindIds());
        }

        if (SEARCH_DAY.equals(getSearchType()))
        {
            query.setDay(dateDay);
        }
        else if (SEARCH_DAYRANGE.equals(getSearchType()))
        {
            query.setFromDay(dateFromDay);
            query.setToDay(dateToDay);
            if(toHour != null)
                query.getToDay().setHours(toHour);
            else
                query.getToDay().setHours(24);
            if(fromHour != null)
                query.getFromDay().setHours(fromHour);
        }
        else if (SEARCH_NUMLIST.equals(getSearchType()))
        {
            query.setNumList(numList);
            query.setDay(dateDaynumList);
        }
        else if (SEARCH_NUMRANGE.equals(getSearchType()))
        {
            query.setFromNum(fromNum);
            query.setToNum(toNum);
        }
        else if (SEARCH_NUMRANGE_DAILY.equals(getSearchType()))
        {
            query.setFromNumD(fromNumD);
            query.setToNumD(toNumD);            
            query.setDay(dateDayDayDailyOfficeNumber);
        }
        List tmp = Journal.search(query); 
        if(query != null) totalCount = query.getTotalCount();
        return tmp;
        
    }

    protected void redirect(ActionEvent event) throws EdmException
    {
        // "zaznaczenie" wszystkich checkboks�w przy sposobach dostarczenia
        setDelivery((Integer[]) fun.map(getDocumentDeliveries(),
            new lambda<OfficeDocumentDelivery, Integer>()
            {
                public Integer act(OfficeDocumentDelivery o)
                {
                    return o.getId();
                }
            }).toArray(new Integer[0]));

        setSearchType(SEARCH_DAY);
        setDay(DateUtils.formatJsDate(new Date()));
        event.setResult("today");
    }

    protected void search() throws EdmException
    {
        journal = Journal.find(getId());
        checkJournalType(journal);

        results = find(journal, sortBy);
        if (results.isEmpty()){
        	throw new EdmException("Nie znaleziono pism");
        }
        
        checkJournalType(journal);

        if (Journal.OUTGOING.equals(journal.getJournalType()))
        {
            linkType= "/office/view-out-journal.action";
            printLinkType = "/office/print-out-journal.action";

        }
        else if(Journal.INCOMING.equals(journal.getJournalType()))
        {
            linkType = "/office/view-in-journal.action";
            printLinkType = "/office/print-in-journal.action";

        }
        else 
        {
            linkType = "/office/view-int-journal.action";
            printLinkType = "/office/print-int-journal.action";

        }

        Object[] linkVisObject = new Object[] {
                "doSearch", "true",
                "searchType",searchType,
                "id",id,
                "day",day,
                "fromDay", fromDay,
                "toDay", toDay,
                "fromHour",fromHour,
                "toHour",toHour,
                "fromNum", fromNum,
                "toNum", toNum,
                "delivery", delivery,
                "kindIds", kindIds,
                "summary", summary,
                "sender", sender,
                "recipient", recipient,
                "referenceId", referenceId,
                "author", author,
                "postalregnumber",postalregnumber,
                "numList", numList,
                "dayNumList",dayNumList,
                "fromNumD",fromNumD,
                "toNumD",toNumD,
                "userDivision",userDivision,
                "userLastname",userLastname,
                "creatingEntry",creatingEntry,
                "userFirstname",userFirstname,
                "dayDailyOfficeNumber",dayDailyOfficeNumber,
                "zpo",zpo,
                "authorDivision",authorDivision,
                "weigthFrom", weigthFrom,
                "weigthTo", weigthTo,
                "alldelivery",(delivery == null)  ? alldelivery: false,
                (Journal.INTERNAL.equals(journal.getJournalType()) ? "alldelivery": ""),
                (Journal.INTERNAL.equals(journal.getJournalType()) ? "true": ""),
                "offset", String.valueOf(offset)};

        searchLink = HttpUtils.makeUrl(linkType, linkVisObject);
        

        Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
        {
            public String getLink(int offset)
            {
                return HttpUtils.makeUrl(linkType, new Object[] {
                        "doSearch", "true",
                        "searchType",searchType,
                        "id",id,
                        "day",day,
                        "fromDay", fromDay,
                        "toDay", toDay,
                        "fromHour",fromHour,
                        "toHour",toHour,
                        "fromNum", fromNum,
                        "toNum", toNum,
                        "delivery", delivery,
                        "kindIds", kindIds,
                        "summary", summary,
                        "sender", sender,
                        "recipient", recipient,
                        "referenceId", referenceId,
                        "author", author,
                        "postalregnumber",postalregnumber,
                        "numList", numList,
                        "dayNumList",dayNumList,
                        "fromNumD",fromNumD,
                        "toNumD",toNumD,
                        "userDivision",userDivision,
                        "userLastname",userLastname,
                        "userFirstname",userFirstname,
                        "dayDailyOfficeNumber",dayDailyOfficeNumber,
                        "alldelivery",(delivery == null)  ? alldelivery: false,
                        (Journal.INTERNAL.equals(journal.getJournalType()) ? "alldelivery": ""),
                        (Journal.INTERNAL.equals(journal.getJournalType()) ? "true": ""),
                        "offset", String.valueOf(offset),
                        "sortBy", sortBy,
                        "creatingEntry",creatingEntry,
                        "zpo",zpo,
                        "authorDivision",authorDivision,
                        "weigthFrom", weigthFrom,
                        "weigthTo", weigthTo,
                        "ascending", ascending });
            }
        };

        pager = new Pager(linkVisitor, offset, limit, totalCount, 10);

        printLink = HttpUtils.makeUrl(printLinkType,
            new Object[] {
            "doSearch", "true",
            "searchType",searchType,
            "id",id,
            "day",day,
            "fromDay", fromDay,
            "toDay", toDay,
            "fromNum", fromNum,
            "fromHour",fromHour,
            "toHour",toHour,
            "toNum", toNum,
            "delivery", delivery,
            "kindIds", kindIds,
            "summary", summary,
            "sender", sender,
            "recipient", recipient,
            "referenceId", referenceId,
            "author", author,
            "numList", numList,
            "postalregnumber",postalregnumber,
            "sortByLocation",sortByLocation,
            "dayNumList",dayNumList,
            "fromNumD",fromNumD,
            "toNumD",toNumD,
            "userDivision",userDivision,
            "userLastname",userLastname,
            "userFirstname",userFirstname,
            "dayDailyOfficeNumber",dayDailyOfficeNumber,
            "alldelivery",(delivery == null)  ? alldelivery: false,
            (Journal.INTERNAL.equals(journal.getJournalType()) ? "alldelivery": ""),
            (Journal.INTERNAL.equals(journal.getJournalType()) ? "true": ""),
            "offset", -1,
            "sortByRegNumber",sortByRegNumber,
            "sortBy",sortBy,
            "ascending",ascending,
            "authorDivision",authorDivision,
            "zpo",zpo,
            "weigthFrom", weigthFrom,
            "weigthTo", weigthTo,
            "creatingEntry",creatingEntry
            });
        
        printParamLink = HttpUtils.makeUrl("/office/print-param-journal.action",
                new Object[] {
                "doSearch", "true",
                "searchType",searchType,
                "id",id,
                "day",day,
                "fromDay", fromDay,
                "toDay", toDay,
                "fromNum", fromNum,
                "fromHour",fromHour,
                "toHour",toHour,
                "toNum", toNum,
                "delivery", delivery,
                "kindIds", kindIds,
                "summary", summary,
                "sender", sender,
                "recipient", recipient,
                "referenceId", referenceId,
                "author", author,
                "numList", numList,
                "postalregnumber",postalregnumber,
                "sortByLocation",sortByLocation,
                "dayNumList",dayNumList,
                "fromNumD",fromNumD,
                "toNumD",toNumD,
                "userDivision",userDivision,
                "userLastname",userLastname,
                "userFirstname",userFirstname,
                "dayDailyOfficeNumber",dayDailyOfficeNumber,
                "alldelivery",(delivery == null)  ? alldelivery: false,
                (Journal.INTERNAL.equals(journal.getJournalType()) ? "alldelivery": ""),
                (Journal.INTERNAL.equals(journal.getJournalType()) ? "true": ""),
                "offset", -1,
                "sortByRegNumber",sortByRegNumber,
                "sortBy",sortBy,
                "ascending",ascending,
                "authorDivision",authorDivision,
                "zpo",zpo,
                "creatingEntry",creatingEntry
                });
        

        title = journal.getSummary();
    }

	public void setWeigth(String weigth) {
		this.weigth = weigth;
		try {
			if (this.weigth.compareTo("less") == 0){
				weigthFrom = new Integer(0);
				weigthTo = new Integer(50);
			}
			if (this.weigth.compareTo("more") == 0){
				weigthFrom = new Integer(50);
				weigthTo = new Integer(Integer.MAX_VALUE);
			}
		}catch (Exception e){
			;
		}
	}
    protected void fillForm() throws EdmException
    {
    	canReadDictionary = false;
    	if(getId()==null){
    		id = Journal.getMainIncoming().getId();
    		throw new EdmException("Nie wybrano dziennika do podgladu");
    	}
    	else {
    		  journal = Journal.find(getId());
    		//jezeli g�owny i niema uprawnien   podgladu gluwnego
    	if(journal.isIncoming() && !DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
    		throw new EdmException("Nie masz uprawnien do podgladu dziennika");	
    	}
    	////jezeli wydzia�owy  i niema uprawnien podgaldu w dziale mimo ze jes tw dziale 
    	if(!journal.isIncoming() && DSApi.context().getDSUser().inDivisionByGuid(journal.getOwnerGuid()) && !DSApi.context().hasPermission(DSPermission.DZIENNIK_KOMORKA_PODGLAD))
    		throw new EdmException("Nie masz uprawnien do podgladu dziennika");	
    	
    	
    	canReadDictionary=true;
        deliveries = getDocumentDeliveries();
        
        journal = Journal.find(getId());
        columns = JournalManager.getTableColumns(journal,getSearchLink(),sm);
        
        checkJournalType(journal);

        if (Journal.INCOMING.equals(journal.getJournalType()))
        {
            kinds = InOfficeDocumentKind.list();
        }

        users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
        fastAssignmentUser = users;
    	//fastAssignmentUser = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
    	fastAssignmentDivision = Arrays.asList(DSDivision.getOnlyDivisionsAndPositions(false));
        Collections.sort(fastAssignmentDivision, new DSDivision.DivisionComparatorAsName());

        minNum = journal.getMinSequenceId();
        maxNum = journal.getMaxSequenceId();
    		}
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSearchType()
    {
        return searchType;
    }

    public void setSearchType(String searchType)
    {
        this.searchType = searchType;
    }

    public String getDay()
    {
        return day;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

    public String getFromDay()
    {
        return fromDay;
    }

    public void setFromDay(String fromDay)
    {
        this.fromDay = fromDay;
    }

    public String getToDay()
    {
        return toDay;
    }

    public void setToDay(String toDay)
    {
        this.toDay = toDay;
    }

    public Integer getFromNum()
    {
        return fromNum;
    }

    public void setFromNum(Integer fromNum)
    {
        this.fromNum = fromNum;
    }

    public Integer getToNum()
    {
        return toNum;
    }

    public void setToNum(Integer toNum)
    {
        this.toNum = toNum;
    }

    public String getNumList()
    {
        return numList;
    }

    public void setNumList(String numList)
    {
        this.numList = numList;
    }

    public final boolean isAlldelivery()
    {
        return alldelivery;
    }

    public final void setAlldelivery(boolean alldelivery)
    {
        this.alldelivery = alldelivery;
    }

    public final Integer[] getDelivery()
    {
        return delivery;
    }

    public final void setDelivery(Integer[] delivery)
    {
        this.delivery = delivery;
    }

    public Integer getMinNum()
    {
        return minNum;
    }

    public Integer getMaxNum()
    {
        return maxNum;
    }

    public Journal getJournal()
    {
        return journal;
    }

    public List getResults()
    {
        return results;
    }

    public String getPrintLink()
    {
        return printLink;
    }

    public String getTitle()
    {
        return title;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public Integer[] getKindIds()
    {
        return kindIds;
    }

    public void setKindIds(Integer[] kindIds)
    {
        this.kindIds = kindIds;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getRecipient()
    {
        return recipient;
    }

    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public boolean isAllKinds()
    {
        return allKinds;
    }

    public void setAllKinds(boolean allKinds)
    {
        this.allKinds = allKinds;
    }

    public List getKinds()
    {
        return kinds;
    }

    public List getUsers()
    {
        return users;
    }

    public void setSortByLocation(boolean sortByLocation)
    {
        this.sortByLocation = sortByLocation;
    }

    public boolean isSortByLocation()
    {
        return sortByLocation;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }
    public boolean getDeliveryAll()
    {
        return alldelivery;
    
    }
    public boolean isRedirected()
    {
        return redirected;
    }

    public void setRedirected(boolean redirected)
    {
        this.redirected = redirected;
    }

    public Integer getFromHour()
    {
        return fromHour;
    }

    public void setFromHour(Integer fromHour)
    {
        this.fromHour = fromHour;
    }

    public Integer getToHour()
    {
        return toHour;
    }

    public void setToHour(Integer toHour)
    {
        this.toHour = toHour;
    }

	public boolean isSortByRegNumber() {
		return sortByRegNumber;
	}

	public void setSortByRegNumber(boolean sortByRegNumber) {
		this.sortByRegNumber = sortByRegNumber;
	}

	/**
	 * @return the sortBy
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param sortBy the sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * @return the ascending
	 */
	public boolean isAscending() {
		return ascending;
	}

	/**
	 * @param ascending the ascending to set
	 */
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	/**
	 * @return the searchLink
	 */
	public String getSearchLink() {
		return searchLink;
	}

	public String getDayDailyOfficeNumber() {
		return dayDailyOfficeNumber;
	}

	public void setDayDailyOfficeNumber(String dayDailyOfficeNumber) {
		this.dayDailyOfficeNumber = dayDailyOfficeNumber;
	}

	public String getDayNumList() {
		return dayNumList;
	}

	public void setDayNumList(String dayNumList) {
		this.dayNumList = dayNumList;
	}

	public String getPostalregnumber() {
		return postalregnumber;
	}

	public void setPostalregnumber(String postalregnumber) {
		this.postalregnumber = postalregnumber;
	}

	public List<DSDivision> getFastAssignmentDivision() {
		return fastAssignmentDivision;
	}

	public void setFastAssignmentDivision(List<DSDivision> fastAssignmentDivision) {
		this.fastAssignmentDivision = fastAssignmentDivision;
	}

	public List<DSUser> getFastAssignmentUser() {
		return fastAssignmentUser;
	}

	public void setFastAssignmentUser(List<DSUser> fastAssignmentUser) {
		this.fastAssignmentUser = fastAssignmentUser;
	}

	public String getUserDivision() {
		return userDivision;
	}

	public void setUserDivision(String userDivision) {
		this.userDivision = userDivision;
	}

	public String getUserFirstname() {
		return userFirstname;
	}

	public void setUserFirstname(String userFirstname) {
		this.userFirstname = userFirstname;
	}

	public String getUserLastname() {
		return userLastname;
	}

	public void setUserLastname(String userLastname) {
		this.userLastname = userLastname;
	}

	public String getAuthorDivision() {
		return authorDivision;
	}

	public void setAuthorDivision(String authorDivision) {
		this.authorDivision = authorDivision;
	}

	public String getPrintParamLink() {
		return printParamLink;
	}

	public void setPrintParamLink(String printParamLink) {
		this.printParamLink = printParamLink;
	}

	public List getDeliveryList() {
		return deliveryList;
	}

	public void setDeliveryList(List deliveryList) {
		this.deliveryList = deliveryList;
	}

	public String getCreatingEntry() {
		return creatingEntry;
	}

	public void setCreatingEntry(String creatingEntry) {
		this.creatingEntry = creatingEntry;
	}

	public boolean isZpo() {
		return zpo;
	}

	public void setZpo(boolean zpo) {
		this.zpo = zpo;
	}

	public boolean isCanReadDictionary() {
		return canReadDictionary;
	}

	public void setCanReadDictionary(boolean canReadDictionary) {
		this.canReadDictionary = canReadDictionary;
	}
	
}
