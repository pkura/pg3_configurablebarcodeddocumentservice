package pl.compan.docusafe.web.office.journals;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.office.CurrentDayAction;
import pl.compan.docusafe.web.office.CurrentDayAction.JournalType;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Usuni�cie checkboks�w z dziennik�w, kt�rych nie mo�na skasowa�
 * (beany przekazuj�ce wywo�ania do klas Journal i dodaj�ce metod�
 * canDelete - nie mo�na jej wywo�a� w jsp, bo sesja jest zamkni�ta).
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionJournalsAction.java,v 1.15 2009/11/10 12:59:04 mariuszk Exp $
 */
public class DivisionJournalsAction extends EventActionSupport
{
	private Logger  log = LoggerFactory.getLogger(DivisionJournalsAction.class);
	public static final Boolean SHOW_JOURNALS_WITHOUT_TREE = AvailabilityManager.isAvailable("journals.showJournals.withoutTree");
	public static final Boolean JOURNALS_PERMISSIONS = AvailabilityManager.isAvailable("journals.Permissions");
	public static final String PREFIX_TO_JOURNAL_PERMISSION_GUID = "PERMISSION_TO_JOURNAL_";
    // @EXPORT/@IMPORT
    private String divisionGuid;

    // @IMPORT
    private String description;
    private String type;
    private Long[] journalIds;
    private String symbol;

    // @EXPORT
    private DSDivision division;
    private String tree;
    /**
     * Lista obiekt�w Journal.
     */
    private List journals;
    private List<Journal> availableJournals;
    private List closedJournals;
    private Journal mainIncoming;
    private Journal mainOutgoing;
    private Journal mainInternal;
    private boolean canViewJournals;
    private boolean canEditJournals;
    private Integer year;
    private List<Integer> years;
    private boolean empty;
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (journalIds == null || journalIds.length == 0)
                return;

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.DZIENNIK_USUWANIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaDziennika"));

                for (int i=0; i < journalIds.length; i++)
                {
                    Journal journal = Journal.find(journalIds[i]);
                    journal.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(description))
                addActionError(sm.getString("NiePodanoOpisuDziennika"));

            // dodatkowy dziennik w dziale g��wnym musi mie� symbol
            if (DSDivision.ROOT_GUID.equals(divisionGuid) &&
                StringUtils.isEmpty(symbol))
                addActionError(sm.getString("NiePodanoSymboluDziennika"));

            if (hasActionErrors())
                return;

            description = description.trim();

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.DZIENNIK_TWORZENIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoTworzeniaDziennika"));

                /*Division division = */DSDivision.find(divisionGuid); // DivisionNotFoundException

                if (Journal.findByDivisionGuidAndDescription(divisionGuid, description).size() > 0)
                    throw new EdmException(sm.getString("WtymDzialeIstniejeJuzDziennikOnazwie",description));

                if (Journal.findByDivisionGuidAndSymbol(divisionGuid, symbol).size() > 0)
                    throw new EdmException(sm.getString("WtymDzialeIstniejeJuzDziennikOsymbolu",symbol));

                Journal journal = new Journal(description, divisionGuid, type);
                journal.setSymbol(TextUtils.trimmedStringOrNull(symbol));
                journal.create();
                
                if (JOURNALS_PERMISSIONS) {
                	setPermissions(journal);
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
        
        private void setPermissions(Journal journal) throws EdmException {
        	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
    		perms.add(new PermissionBean(ObjectPermission.READ, PREFIX_TO_JOURNAL_PERMISSION_GUID + journal.getId().toString(), ObjectPermission.GROUP, "Dziennik - odczyt - " + journal.getDescription()));
        	
    		for (PermissionBean perm : perms) {
    			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
    				String groupName = perm.getGroupName();
    				if (groupName == null) {
    					groupName = perm.getSubject();
    				}
    				createOrFind(journal, groupName, perm.getSubject());
    			}

    			DSApi.context().session().flush();
    		}
        }
        
    	private DSDivision createOrFind(Journal journal, String name, String guid) throws EdmException {
    		try {
    			return DSDivision.find(guid.trim());
    		} catch (DivisionNotFoundException e) {
    			DSDivision parent = null;
    			try {
    				parent= DSDivision.findByName(DSDivision.UP);
    			} catch (DivisionNotFoundException ee) {
    				parent= DSDivision.find(DSDivision.ROOT_GUID);
    			}
    			return parent.createGroup(name, guid);
    		}
    	}
    	
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            if (StringUtils.isEmpty(divisionGuid) && AvailabilityManager.isAvailable("showUserDivisionJournal"))
            {
                try {
                    DSUser user = DSApi.context().getDSUser();
                    for (DSDivision division : user.getDivisionsWithoutGroupPosition())
                    {
                        if (Journal.findByDivisionGuid(division.getGuid()).size() > 0)
                        {
                            divisionGuid = division.getGuid();
                            break;
                        }
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
                if (StringUtils.isEmpty(divisionGuid))
                    divisionGuid = DSDivision.ROOT_GUID;
            }
            else if (StringUtils.isEmpty(divisionGuid))
                divisionGuid = DSDivision.ROOT_GUID;

            try
            {
                division = DSDivision.find(divisionGuid);
                division.getPrettyPath();

                if (division.isRoot())
                {
                    canViewJournals =
                        DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD);
                }
                else
                {
                    canViewJournals =
                        DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD) ||
                        (DSApi.context().hasPermission(DSPermission.DZIENNIK_KOMORKA_PODGLAD) &&
                         DSApi.context().getDSUser().inDivision(division, true));
                }
                canEditJournals = DSApi.context().hasPermission(DSPermission.DZIENNIK_ZMIANA_NUMERU_KOLEJNEGO);
                UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        return ServletActionContext.getRequest().getContextPath() +
                            "/office/division-journals.action?divisionGuid="+
                            ((DSDivision) element).getGuid();
                    }
                };

                tree = OrganizationTree.newTree(division, urlVisitor,
                    ServletActionContext.getRequest(),
                    false, false, false).generateTree();

                journals = new ArrayList(10);

                journals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.INCOMING));
                journals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.OUTGOING));
                journals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.INTERNAL));
                
                if (SHOW_JOURNALS_WITHOUT_TREE) {
                	journals.clear();
                	for(JournalType type : CurrentDayAction.JournalType.values()) {
                		for (Journal journal : Journal.findByType(type.getValue())) {
                			if (journal.getOwnerGuid() != null) {										//nie dodawanie do listy dziennikow dziennikow glownych (ownerGuid w dziennikach glownych = null)
                				journals.add(journal);
                			}
                		}
                	}
                }
                
                if (JOURNALS_PERMISSIONS) {
                	availableJournals = new ArrayList<Journal>();
                	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)) {
                		availableJournals.addAll(journals);
                	} else {
                		for (Object item : journals) {
                			if (item instanceof Journal) {
                				Journal journal = (Journal) item;
                				if (hasPermissionsToJournal(journal)) {
                					availableJournals.add(journal);
                				}
                			}
                		}
                	}
                }
                
                years = new ArrayList<Integer>();
                closedJournals = new ArrayList(10);
                closedJournals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.INCOMING, true));
                closedJournals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.OUTGOING, true));
                closedJournals.addAll(Journal.findByDivisionGuid(divisionGuid, Journal.INTERNAL, true));
                
                if (division.isRoot() || SHOW_JOURNALS_WITHOUT_TREE)
                {
                    mainIncoming = Journal.getMainIncoming();
                    mainOutgoing = Journal.getMainOutgoing();
                    mainInternal = Journal.getMainInternal();
                    closedJournals.addAll(Journal.findMainJournals(true));
                }
                if(closedJournals.size() != 0 && year == null)
                {
                	//Journal j = (Journal) closedJournals.get(0);
                	year = ((Journal) closedJournals.get(0)).getCyear();
                }
                
                for (int k = 0; k < closedJournals.size(); k++) {
                	boolean znal = false;
					Journal journal = (Journal) closedJournals.get(k);
					if(years.contains(journal.getCyear()))
						znal = true;
					if(znal == false)
						years.add(journal.getCyear());
					if(journal.getCyear() != year)
					{
						closedJournals.remove(k);
						k--;
					}
				}
                if(closedJournals.size() == 0)
                	empty = true;
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
        
        private boolean hasPermissionsToJournal(Journal journal) {
        	boolean hasPermissions;
        	try {
				DSDivision division = DSDivision.find(PREFIX_TO_JOURNAL_PERMISSION_GUID + journal.getId().toString());
				if (DSApi.context().getDSUser().inDivision(division, true)) {
					hasPermissions = true;
				} else {
					hasPermissions = false;
				}
			} catch (DivisionNotFoundException e) {
				hasPermissions = false;
			} catch (UserNotFoundException e) {
				hasPermissions = false;
			} catch (EdmException e) {
				hasPermissions = false;
			}
        	return hasPermissions;
        }
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public DSDivision getDivision()
    {
        return division;
    }

    public String getTree()
    {
        return tree;
    }

    public List getJournals()
    {
        return journals;
    }

    public void setJournalIds(Long[] journalIds)
    {
        this.journalIds = journalIds;
    }

    public Journal getMainIncoming()
    {
        return mainIncoming;
    }

    public Journal getMainOutgoing()
    {
        return mainOutgoing;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    public boolean isCanViewJournals()
    {
        return canViewJournals;
    }

    public Journal getMainInternal()
    {
        return mainInternal;
    }

    public List getClosedJournals()
    {
        return closedJournals;
    }

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<Integer> getYears() {
		return years;
	}

	public void setYears(List<Integer> years) {
		this.years = years;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public boolean isCanEditJournals() {
		return canEditJournals;
	}

	public void setCanEditJournals(boolean canEditJournals) {
		this.canEditJournals = canEditJournals;
	}

	public List<Journal> getAvailableJournals() {
		return availableJournals;
	}

	public void setAvailableJournals(List<Journal> availableJournals) {
		this.availableJournals = availableJournals;
	}

	public static Boolean getSHOW_JOURNALS_WITHOUT_TREE() {
		return SHOW_JOURNALS_WITHOUT_TREE;
	}
}
