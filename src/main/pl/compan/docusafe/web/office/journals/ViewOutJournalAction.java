package pl.compan.docusafe.web.office.journals;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewOutJournalAction.java,v 1.10 2009/11/24 13:46:20 mariuszk Exp $
 */

public class ViewOutJournalAction extends OutJournalActionBase
{
    private static final String EV_SEARCH = "search";
    private static String baseUrl;
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Redirect()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_SEARCH, new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String getBaseLink()
    {
        return "/office/view-out-journal.action";
    }

    private class Redirect implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                redirect(event);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!validateForm())
                event.skip(EV_SEARCH);
        }
    }
    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	search();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                fillForm();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

	public static String getBaseUrl()
	{
		if(AvailabilityManager.isAvailable("zadanie.podsumowanie"))
			return  "/office/outgoing/summary.action";
		else
			return  "/office/outgoing/main-redirect.action";
	}
}
