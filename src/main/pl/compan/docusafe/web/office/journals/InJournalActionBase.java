package pl.compan.docusafe.web.office.journals;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InJournalActionBase.java,v 1.13 2009/11/19 12:20:25 mariuszk Exp $
 */
public abstract class InJournalActionBase extends JournalActionBase
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

	protected void checkJournalType(Journal journal) throws EdmException
    {
        if (!journal.isIncoming())
            throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismPrzychodzacych"));
    }

    protected OfficeDocument getDocument(Long documentId) throws EdmException
    {
        InOfficeDocument document = InOfficeDocument.findInOfficeDocument(documentId);
        DSApi.initializeProxy(document.getRecipients());
        return document;
    }

    protected String getDocumentLink(OfficeDocument document)
    {
        return "/office/incoming/summary.action?documentId="+document.getId();
    }

    protected String getDocumentWhere(OfficeDocument document) throws EdmException
    {
        if (Configuration.officeAvailable())
        {
            StringBuilder where = new StringBuilder();
            where.append(DSUser.safeToFirstnameLastnameName(document.getCurrentAssignmentUsername()));

            if (document.getDivisionGuid() != null)
            {
                where.append(" w dziale ");
                where.append(DSDivision.safeGetPrettyPath(document.getDivisionGuid()));
            }

            return where.toString();
        }
        else if (Configuration.simpleOfficeAvailable() && document.getAssignedDivision() != null)
        {
            return DSDivision.safeGetPrettyPath(document.getAssignedDivision());
        }

        return null;
    }

    protected List<InOfficeDocumentDelivery> getDocumentDeliveries() throws EdmException
    {
        return InOfficeDocumentDelivery.list();
    }

    protected OfficeDocumentDelivery getDocumentDelivery(OfficeDocument document) throws EdmException
    {
        OfficeDocumentDelivery delivery = ((InOfficeDocument) document).getDelivery();
        DSApi.initializeProxy(delivery);
        return delivery;
    }

    protected String getJournalPrintLink()
    {
        return "/office/print-in-journal.action";
    }
}
