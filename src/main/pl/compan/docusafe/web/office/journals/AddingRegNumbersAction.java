package pl.compan.docusafe.web.office.journals;

import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class AddingRegNumbersAction extends EventActionSupport {

	/**
	 * 
	 */
	//private String id;
	private Long koNr1;
	private Long koNr2;
	private Long regNr;
	private Long id;
	
	private static final Log log = LogFactory.getLog(EdmException.class);
	
	protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddNumbers").
        	append(OpenHibernateSession.INSTANCE).
        	append(new AddNumbers()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
    } 
	
	private class FillForm implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
        }
    }
	private class AddNumbers implements ActionListener
    {
        @SuppressWarnings("unchecked")
		public void actionPerformed(ActionEvent event)
        {
        	if(koNr1 == null || koNr2 == null || regNr == null || id == null)
        	{
        		addActionError("Nie podano podstawowych danych.");
        		return;
        	}
        	try
        	{
        		//List<OutOfficeDocument> dl = OutOfficeDocument.findByOfficeNumber(koNr1, koNr2, id);
        		//Collections.sort(dl, new OfficeDocument.OfficeDocumentComparatorByOfficeNumber());
        		
        		List<OfficeDocument> dl = OfficeDocument.findByOfficeNumberRange(koNr1.intValue(), koNr2.intValue(), id);
        		
        		if(dl.size() < 1)
        			throw new Exception("Nie znaleziono zadnego pisma");
        		
        		Boolean test = false;
        		for(OfficeDocument doc : dl)
        		{
        			if(((OutOfficeDocument)doc).getPostalRegNumber() != null)
        			{
        				addActionError("Pismo "+doc.getOfficeNumber()+" ma juz nadany numer "+((OutOfficeDocument)doc).getPostalRegNumber());
        				test = true;
        			}
        		}
        		if(test)
    				throw new Exception("Pisma maja juz nadany numer");
        		
        		DSApi.context().begin();
        		for(OfficeDocument doc : dl)
        		{
        			((OutOfficeDocument)doc).setPostalRegNumber(regNr.toString());
        			addActionMessage("Dokument "+doc.getOfficeNumber()+" nadano numer "+regNr.toString());
        			regNr++;        			
        		}
        		DSApi.context().commit();
        		
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        		log.debug("", e);
        		DSApi.context().setRollbackOnly();
        	}
        	catch (Exception e) {        		
        		addActionError(e.getMessage());
        		log.error("",e);        		
			}
        }
    }
	public void setId(Long id) {
		this.id = id;
	}
	public Long getKoNr1() {
		return koNr1;
	}
	public void setKoNr1(Long koNr1) {
		this.koNr1 = koNr1;
	}
	public Long getKoNr2() {
		return koNr2;
	}
	public void setKoNr2(Long koNr2) {
		this.koNr2 = koNr2;
	}
	public Long getRegNr() {
		return regNr;
	}
	public void setRegNr(Long regNr) {
		this.regNr = regNr;
	}
	public Long getId() {
		return id;
	}
}
