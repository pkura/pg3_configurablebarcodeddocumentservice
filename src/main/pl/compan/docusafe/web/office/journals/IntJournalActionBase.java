package pl.compan.docusafe.web.office.journals;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.util.StringManager;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: IntJournalActionBase.java,v 1.4 2007/09/13 11:37:56 mariuszk Exp $
 */
public abstract class IntJournalActionBase extends JournalActionBase
{
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    protected void checkJournalType(Journal journal) throws EdmException
    {
        if (!journal.isInternal())
            throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));
    }

    protected OfficeDocument getDocument(Long documentId) throws EdmException
    {
        OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(documentId);
        DSApi.initializeProxy(document.getRecipients());
        return document;
    }

    protected String getDocumentLink(OfficeDocument document)
    {
        return "/office/internal/summary.action?documentId="+document.getId();
    }

    protected String getDocumentWhere(OfficeDocument document) throws EdmException
    {
        return null;
    }

    protected List<OutOfficeDocumentDelivery> getDocumentDeliveries() throws EdmException
    {
        return OutOfficeDocumentDelivery.list();
    }

    protected OfficeDocumentDelivery getDocumentDelivery(OfficeDocument document) throws EdmException
    {
        OfficeDocumentDelivery delivery = ((OutOfficeDocument) document).getDelivery();
        DSApi.initializeProxy(delivery);
        return delivery;
    }

    protected String getJournalPrintLink()
    {
        return "/office/print-int-journal.action";
    }
}
