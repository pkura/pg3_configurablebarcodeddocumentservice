package pl.compan.docusafe.web.office.journals;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * TODO: dok�adna kopia PrintOut... - zrobi� to wsp�lnie
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintIntJournalAction.java,v 1.18 2009/06/25 07:53:25 pecet3 Exp $
 */
public class PrintIntJournalAction extends IntJournalActionBase
{
    /**
     * Format daty u�ywany przy generowaniu pliku PDF.
     */
    public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    private boolean extended;
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).   
            append(OpenHibernateSession.INSTANCE).
            append(new Pdf()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Pdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File temp = null;

            try
            {
                Journal journal = Journal.find(getId());
                if (!journal.isInternal())
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWewnetrznych"));

                List results = find(journal);

                // itext (pdf)

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, (float)10);

                temp = File.createTempFile("docusafe_", "_tmp");
                Document pdfDoc =
                    new Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                
                HeaderFooter footer =
                  	 new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                  		   DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                   footer.setAlignment(Element.ALIGN_CENTER);
                   pdfDoc.setFooter(footer);
                pdfDoc.open();

                PdfUtils.addHeaderImageToPdf(pdfDoc, journal.getSummary(), baseFont);

                int[] widths;
                if (extended)
                {
                	if(AvailabilityManager.isAvailable("printIntJournal.ins.special")){
                		if (journal.isMain())
	                        widths = (new int[] { 4,    6, 7, 13, 13, 13,7 });
	                    else
	                        widths = (new int[] { 4, 4, 6, 7, 13, 13, 13,7 });
                	}
                	else{
	                    if (journal.isMain())
	                        widths = (new int[] { 4,    7, 15, 15, 15,7 });
	                    else
	                        widths = (new int[] { 4, 4, 7, 15, 15, 15,7 });
                	}
                }
                else
                {
                	if(AvailabilityManager.isAvailable("printIntJournal.ins.special")){
                		if (journal.isMain())
	                        widths = (new int[] { 4,    6, 8, 13, 16, 16});
	                    else
	                        widths = (new int[] { 4, 4, 6, 8, 13, 16, 16});
                	}
                	else{
	                    if (journal.isMain())
	                        widths = (new int[] { 4,    8, 17, 17, 17});
	                    else
	                        widths = (new int[] { 4, 4, 8, 17, 17, 17});
                	}
                }
                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(2);
                //table.setLastHeaderRow(0);

                Cell _ko = new Cell(new Phrase(sm.getString("NrKO"), font));
                _ko.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_ko);
                if (!journal.isMain())
                {
                    Cell _lp = new Cell(new Phrase(sm.getString("NrKolej"), font));
                    _lp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                    table.addCell(_lp);
                }
                Cell _data;
                if(AvailabilityManager.isAvailable("printJournal.IntJournalDataPrzyjeciaZamiastDatyPisma")){
                	_data=	new Cell(new Phrase(sm.getString("DataPrzyjecia"), font));
                }
                else{
                	_data=	new Cell(new Phrase(sm.getString("DataPisma"), font));
                }
                 
                _data.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_data);
                
                if(AvailabilityManager.isAvailable("printIntJournal.ins.special")){
	                Cell _numerDokumentu = new Cell(new Phrase(sm.getString("NumerDokumentu"), font));
	                _numerDokumentu.setHorizontalAlignment(Cell.ALIGN_CENTER);
	                table.addCell(_numerDokumentu);
                }

                Cell _od = new Cell(new Phrase(sm.getString("Autor"), font));
                _od.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_od);

                Cell _sum = new Cell(new Phrase(sm.getString("OpisPisma"), font));
                _sum.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_sum);

                Cell _uw ;
                if(AvailabilityManager.isAvailable("printJournal.Dzial/wydzialZamiastWydzial"))
                	_uw= new Cell(new Phrase(sm.getString("Dzial/wydzial"), font));
                else 
                	_uw= new Cell(new Phrase(sm.getString("Dzial"), font));
                _uw.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_uw);
                
                if (extended)
                {
                    Cell _oth = new Cell(new Phrase(sm.getString("Inne"), font));
                    _uw.setHorizontalAlignment(Cell.ALIGN_CENTER);
                    table.addCell(_oth);
                }

                table.endHeaders();

                //pdfDoc.add(table);

                // dane w dzienniku

//                table = new Table(widths.length);
//                table.setWidths(widths);
//                table.setWidth(100);

//                JournalEntry[] entries = journal.getEntries(DateUtils.parseJsDate(sDate));
//                for (int i=0; i < entries.length; i++)
                for (int i=0; i < results.size(); i++)
                {

                    Journal.EntryBean bean = (Journal.EntryBean) results.get(i);

                    table.addCell(new Phrase(String.valueOf(bean.getOfficeNumber()), font));

                     // numer porz�dkowy pisma w dzienniku
                    if (!journal.isMain())
                        table.addCell(new Phrase(String.valueOf(bean.getSequenceId()), font));

                    // data pisma
                    if (bean.getDocumentDate() != null)
                    {
                        synchronized (dateFormat)
                        {
                        	if(AvailabilityManager.isAvailable("printJournal.IntJournalDataPrzyjeciaZamiastDatyPisma")){
                        		table.addCell(
                                        new Phrase(dateFormat.format(bean.getIncomingDate()), font));	
                        	}
                        	else{
                            table.addCell(
                                new Phrase(dateFormat.format(bean.getDocumentDate()), font));
                        	}
                        }
                    }
                    else
                    {
                        table.addCell(new Phrase("", font));
                    }

                    if(AvailabilityManager.isAvailable("printIntJournal.ins.special")){
            			String numerDokumentu = OfficeDocument.find(bean.getDocumentId()).getFieldsManager().getStringValue("DOC_NUMBER");
            			
            			if(numerDokumentu!=null)
            				table.addCell( new Phrase(numerDokumentu, font));
            			else
            				table.addCell( new Phrase("", font));
                    }
                    // utworzy�
                    table.addCell(new Phrase(bean.getCreatingUser(), font));

                    // opis
                    table.addCell(new Phrase(bean.getSummary(), font));

                    // gdzie
                    String pom="";
                     if (bean.getDivisionName() != null) {
                    	 if(AvailabilityManager.isAvailable("printJournal.dzialZamiastWydzial"))
                    		 pom=sm.getString("Dzial")+": "+bean.getDivisionName();
                    	 else
                    		 pom=sm.getString("Wydzial")+": "+bean.getDivisionName();
                         if(bean.getCurrentAssignmentUsername() != null) pom=pom+" / "+ sm.getString("Referent")+": "+bean.getCurrentAssignmentUsername();
                     } else if(bean.getCurrentAssignmentUsername() != null)   pom=sm.getString("Referent")+": "+bean.getCurrentAssignmentUsername();


                    table.addCell(new Phrase(pom, font));
                    //Inne puste
                    if (extended)
                        table.addCell(new Phrase("", font));


                }

                pdfDoc.add(table);

                //pdfDoc.add(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",
                //    DSApi.context().getDSUser().asFirstnameLastname(),
                //    DateUtils.formatCommonDateTime(new Date())), font));

                pdfDoc.close();
            }
            catch (EdmException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            catch (DocumentException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            catch (IOException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length(),
                        "Content-Disposition: inline; filename=\"dziennik.pdf\"");
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    public boolean isExtended()
    {
        return extended;
    }

    public void setExtended(boolean extended)
    {
        this.extended = extended;
    }
    
}
