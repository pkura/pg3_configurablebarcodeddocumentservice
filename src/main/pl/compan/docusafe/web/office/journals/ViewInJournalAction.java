package pl.compan.docusafe.web.office.journals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewInJournalAction.java,v 1.15 2010/01/15 08:53:52 mariuszk Exp $
 */
public class ViewInJournalAction extends InJournalActionBase
{
    private static final String EV_SEARCH = "search";
    private static String baseUrl ;
  

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Redirect()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_SEARCH, new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Redirect implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                redirect(event);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!validateForm())
                event.skip(EV_SEARCH);

        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                search();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {

                fillForm();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

	public static String getBaseUrl()
	{
		if(AvailabilityManager.isAvailable("zadanie.podsumowanie"))
			return  "/office/incoming/summary.action";
		else
			return  "/office/incoming/main-redirect.action";
	}
}
