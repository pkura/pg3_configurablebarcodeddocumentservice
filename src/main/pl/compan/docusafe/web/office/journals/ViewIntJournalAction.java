package pl.compan.docusafe.web.office.journals;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewIntJournalAction.java,v 1.7 2009/11/24 13:46:20 mariuszk Exp $
 */
public class ViewIntJournalAction extends IntJournalActionBase
{
    private static final String EV_SEARCH = "search";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Redirect()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_SEARCH, new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String getBaseLink()
    {
        return "/office/view-int-journal.action";
    }

    public boolean isInternal()
    {
        return true;
    }

    private class Redirect implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                redirect(event);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!validateForm())
                event.skip(EV_SEARCH);
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                search();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                fillForm();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    
	public static String getBaseUrl()
	{
		if(AvailabilityManager.isAvailable("zadanie.podsumowanie"))
			return  "/office/internal/summary.action";
		else
			return  "/office/internal/main-redirect.action";
	}
}
