package pl.compan.docusafe.web.office.journals;

import com.google.common.collect.*;
import com.lowagie.text.*;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.Collections;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.parametrization.uek.NormalLogic;
import pl.compan.docusafe.parametrization.uek.hbm.DeliveryCostUek;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintOutJournalAction.java,v 1.32 2009/11/13 09:50:35 mariuszk Exp $
 */
public class PrintOutJournalAction extends OutJournalActionBase
{


	private static final long serialVersionUID = 1L;
	
	public static  Boolean PRINTOUTJOURNAL_POSTALREGNUMBER_JURNALSUMMARY_DISABLED = AvailabilityManager.isAvailable("printOutJournal.journalSummary.disabled");
    public static  Boolean P_4_PRINTJOURNAL = AvailabilityManager.isAvailable("p4printjournal");
    public static  Boolean PRINTOUTJOURNAL_POSTALREGNUMBER_WIDER = AvailabilityManager.isAvailable("printOutJournal.postalRegNumber.wider");
    public static  Boolean PRINTOUTJOURNAL_POSTALREGNUMBER_WIDER_TO_5_CM = AvailabilityManager.isAvailable("printOutJournal.postalRegNumber.widerTo5Cm");
    public static  Boolean PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY = AvailabilityManager.isAvailable("printOutJournal.doPrzeniesiena.pusty");
    public static  Boolean PRINTJOURNAL_REMARKS_AS_PR = AvailabilityManager.isAvailable("printJournel.RemarksAsPR");
    public static  Boolean PRINTOUTJORNAL_ADDPAGESNUMBER = AvailabilityManager.isAvailable("printoutjouranl.addpagesnumber");
    public static  Boolean PRINTOUTJOURNAL_NIEZREJESTROWANE = AvailabilityManager.isAvailable("printOutJournal.niezarejestrowane");
    public static  Boolean PRINTOUTJOURNAL_UTP_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.utp.special");
    public static  Boolean PRINTJOURNAL_AMS_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.ams.special");
    public static Boolean PRINTOUTJOURNAL_INS_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.ins.special");
    public static Boolean PRINTOUTJOURNAL_UKW_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.ukw.special");
    
    public static  Boolean IMGW_JOURNAL_CONF_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.imgw.special");
    public static  Boolean UEK_JOURNAL_CONF_SPECIAL = AvailabilityManager.isAvailable("printOutJournal.uek.special");
    boolean extended;
    
	
	boolean numerKoPism;
    boolean ilosciowy =false;

    boolean zestawienie;
    boolean krajowy;
    boolean zagraniczny;
    //naklejka adresowa z dokuemntu
    boolean etykieta;
    long  documentId;
    private int fontsize;

    private Logger log = LoggerFactory.getLogger(PrintOutJournalAction.class);
    private Date date= new Date();
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
    	if(PRINTJOURNAL_AMS_SPECIAL && !extended){
    		registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new PrintOutJournalPdf()).
            appendFinally(CloseHibernateSession.INSTANCE);
    	} else {
    		registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Pdf()).
            appendFinally(CloseHibernateSession.INSTANCE);
    	}
        
    }

    private class PrintOutJournalPdf implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			File temp = null;
            try
            {
                Journal journal = Journal.find(getId());
                if (journal.isIncoming())
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));

                List results = find(journal);
                
                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 10);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                FileOutputStream fis = new FileOutputStream(temp);
                PdfWriter.getInstance(pdfDoc, fis);
                
                if (!AvailabilityManager.isAvailable("Print.Out.Journal.Footer.Hide")) {
                	HeaderFooter footer = null;
                	
                	if(extended){
                		footer =
                				new HeaderFooter(new Phrase(sm.getString("WydrukRozSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                						DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                	} else if (!extended) {
                		footer =
                				new HeaderFooter(new Phrase(sm.getString("WydrukPodstSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                						DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                	} else {
                		footer =
                				new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                						DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                	}
                	
                	footer.setAlignment(Element.ALIGN_CENTER);
                	pdfDoc.setFooter(footer);
                }
                
                pdfDoc.open();
                
               
                //PdfUtils.addHeaderImageToPdf(pdfDoc, journal.getSummary(), baseFont);

                int[] widths;
                //cofiguracja w pliku journal.config
                if(journal.isMain()){
                	widths = JournalManager.getAdditionalOutPrintWidths();
                } else {
                	//TO DO - narazie brak specjalnej implementacji dla dzialowych
                	widths = JournalManager.getAdditionalOutPrintWidths();
                }
                	
                
                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(2);
                
                List<String> inColumn;
                if(journal.isMain()){
                	inColumn = JournalManager.getAdditionalOutPrintColumn();
                } else {
                	//TO DO - narazie brak specjalnej implementacji dla dzialowych
                	inColumn = JournalManager.getAdditionalOutPrintColumn();
                }
                	
                int lp = 0;
                for (String cellName : inColumn) 
                {
                	    table.addCell(JournalManager.getCellTitleByName(cellName, font, sm));
				}

                table.endHeaders();

                for (int i=0; i < results.size(); i++)
                {
                	lp++;
                    Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
                    for (String cellName : inColumn) 
                    {
                    	    JournalManager.getCellValueByName(table,bean,cellName,font,sm,lp);
    				}
                }
                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (Exception e)
            {
            	log.error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }

            if (temp != null && temp.exists())
            {
                if (log.isDebugEnabled())
                		log.debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
    		        if(AvailabilityManager.isAvailable("printJournal.PIG")){
    		        	ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                                "application/pdf", (int) temp.length(),
                                "Content-Disposition: attachment; filename=\"dziennik-" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf\"");
    		        }else{
    		        	ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                                "application/pdf", (int) temp.length(),
                                "Content-Disposition: inline; filename=\"dziennik-" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf\"");
    		        }
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {
                	log.error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
		}
    	
    }
    
    private class Pdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event) throws Exception
        {
        	if (ilosciowy){
                PdfIlosciowy p = new PdfIlosciowy();
        		p.actionPerformed(event);
        		return;
        	}

            if (zestawienie) {
                if (AvailabilityManager.isAvailable("view.journal.zestawienie.ilosciowo.wartosciowe.new")){
                    ZestawienieNowe zestawienie = new ZestawienieNowe();
                    zestawienie.actionPerformed(event);
                    return;
                }else{
                    Zestawienie zestawienie = new Zestawienie();
                    zestawienie.actionPerformed(event);
                    return;
                }
            }
            if (etykieta) {
            	GenerujEtykiete etykieta = new GenerujEtykiete();
            	etykieta.actionPerformed(event);
            	return;
            }

            File temp = null;
            int rows = Docusafe.getAdditionProperty("numOfrowsOnPage") != null && !Docusafe.getAdditionProperty("numOfrowsOnPage").isEmpty() ?
					Integer.valueOf(Docusafe.getAdditionProperty("numOfrowsOnPage")) : 10;
            try
            {
                Journal journal = Journal.find(getId());
                //System.out.print("POJ "+getId());
                if (!journal.isOutgoing())
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));

                List results = find(journal);

                // itext (pdf)

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 10);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                FileOutputStream fis = new FileOutputStream(temp);
                PdfWriter.getInstance(pdfDoc, fis);
                
                if (!AvailabilityManager.isAvailable("Print.Out.Journal.Footer.Hide")) {
                	HeaderFooter footer = null;
                    
                    if(extended){
                    	footer =
                             	 new HeaderFooter(new Phrase(sm.getString("WydrukRozSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                           			   DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                    } else if (!extended) {
                    	 footer =
                              	 new HeaderFooter(new Phrase(sm.getString("WydrukPodstSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                            			   DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                    } else {
                    	footer =
                             	 new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                           			   DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                    }
                   
                    footer.setAlignment(Element.ALIGN_CENTER);
                    pdfDoc.setFooter(footer);
                }
                
                
                
                pdfDoc.open();
               
                String summ = journal.getSummary();

                //p.setAlignment(Element.ALIGN_RIGHT);
               // pdfDoc.add(p);
                if (!PRINTOUTJOURNAL_POSTALREGNUMBER_JURNALSUMMARY_DISABLED)
                    PdfUtils.addHeaderImageToPdf(pdfDoc, journal.getSummary(), baseFont);
				if (!journal.isMain())
                {
                    if (P_4_PRINTJOURNAL)
                    {
                        setSenderForP4(journal, baseFont, pdfDoc);
                    }
                }
                if(AvailabilityManager.isAvailable("PAA"))
                    PdfUtils.addHeaderImageToPdf(pdfDoc, "Pa�stwowa Agencja Atomistyki", baseFont);
               if(PRINTOUTJOURNAL_UTP_SPECIAL)
            	   PdfUtils.addHeaderImageToPdfUTP(pdfDoc, "Uniwersytet Technologiczno Przyrodniczy  "+
            			   DateUtils.formatCommonDate(new Date()), baseFont,true);
               if(PRINTOUTJOURNAL_INS_SPECIAL)
            	   PdfUtils.addHeaderImageToPdfUTP(pdfDoc, sm.getString("PocztowaKsiazkaNadawcza"), baseFont,true);

                //czy w adsach jest dodany numer kolumny ko do wydruku��w  pism wychodacych 
                numerKoPism = AvailabilityManager.isAvailable("numerKoPrzyDziennikuPismWychodzacych");
                float[] widths;
               
                //z kolumna KO
                
				if (numerKoPism) {
                	 widths = new float[] { 0.0308f, 0.0600f, 0.2000f, 0.1795f, 0.0564f, 0.0308f,//kolumna KO  0.0600f
													0.0410f, 0.0410f, 0.0926f, 0.0610f, 0.0410f,
													0.0308f, 0.0564f, 0.0308f};//dwa ostatnie co by�o  pobranie  0.0410f, 0.0308f 
                	
				} else {
					// bez kolumny ko
                    if (PRINTOUTJOURNAL_POSTALREGNUMBER_WIDER) // kolumne numer nadawczy  0.1200f
                        widths = new float[] { 0.0308f, 0.2000f, 0.1795f, 0.0564f, 0.0308f, 0.0410f, 0.0410f, 0.1200f, 0.0610f, 0.0410f,
                                0.0308f, 0.0564f, 0.0308f };
                    else if(IMGW_JOURNAL_CONF_SPECIAL)
                    	widths = new float[] { 0.0308f, 0.1900f, 0.1695f, 0.2200f, 0.0410f, 0.0410f, 0.1000f, 0.0610f, 0.0410f,
                            0.0308f, 0.0564f, 0.0308f };
                    else if(PRINTOUTJOURNAL_POSTALREGNUMBER_WIDER_TO_5_CM)
                    	widths = new float[] { 0.0308f, 0.2000f, 0.1795f, 0.0564f, 0.0308f, 0.0410f, 0.0410f, 0.2000f, 0.0610f, 0.0410f,
                            0.0308f, 0.0564f, 0.0308f };
                    else if (PRINTOUTJOURNAL_UTP_SPECIAL)
                    	widths = new float[] { 0.0308f, 0.2000f, 0.1795f, 0.0564f, 0.0308f, 0.0410f, 0.0410f, 0.2000f, 0.0610f, 0.0410f,
                            0.0308f, 0.0564f, 0.0308f };
                    else if (PRINTOUTJOURNAL_UKW_SPECIAL)
                    	widths = new float[] { 0.0308f, 0.2000f, 0.1795f, 0.0564f, 0.0308f, 0.0610f, 0.0610f, 0.2000f, 0.0610f, 0.0410f,
                            0.0308f, 0.0564f, 0.0308f };
                    else if (UEK_JOURNAL_CONF_SPECIAL)
                    	widths = new float[] { 0.0308f, 0.2000f, 0.2000f, 0.0500f, 0.0308f, 0.1500f, 0.2000f, 0.1500f, 0.0500f, 0.0410f};
                    else
                        widths = new float[] { 0.0308f, 0.2000f, 0.1795f, 0.0564f, 0.0308f, 0.0410f, 0.0410f, 0.2000f, 0.0610f, 0.0410f,
                                0.0308f, 0.0564f, 0.0308f };
				}

				Table headerWithLabels = new Table(widths.length);
				buildTableHeader(font, widths, headerWithLabels, !extended);
				
                Cell _lp;
                Cell _recipient;
                Cell _address;
                Cell _zpo;
                Cell _remarks;
                Cell _tmp;
                
                Table table = headerWithLabels;

				if (extended) {
					pdfDoc.add(headerWithLabels);
					table = new Table(widths.length);
	                table.setOffset(0);
					table.setWidths(widths);
					table.setWidth(100.0f);
					table.setCellsFitPage(true);
					table.setPadding(2);
					
					if (numerKoPism) {
						// z kolumna KO
						for (int i = 1; i <= 10; i++) {
							_tmp = new Cell(new Phrase("" + i, font));
							_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
							if (i == 5 || i == 6 || i == 9 || i == 10)
								_tmp.setColspan(2);
							table.addCell(_tmp);
						}
					} else {
						if (UEK_JOURNAL_CONF_SPECIAL) {
							for (int i = 1; i <= 8; i++) {
								_tmp = new Cell(new Phrase("" + i, font));
								_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
								if (i == 4 || i == 8) {
									_tmp.setColspan(2);
								}	
								table.addCell(_tmp);
							}
						} else {
							for (int i = 1; i <= 9; i++) {
								_tmp = new Cell(new Phrase("" + i, font));
								_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
								if(IMGW_JOURNAL_CONF_SPECIAL){
									if (i == 5 || i == 8 || i == 9)
										_tmp.setColspan(2);
								} else {
								if (i == 4 || i == 5 || i == 8 || i == 9)
									_tmp.setColspan(2);
								}
								table.addCell(_tmp);
							}
						}
						
					}

					_tmp = new Cell(new Phrase(sm.getString("Zprzeniesienia"), font));
					_tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					if (numerKoPism)
						_tmp.setColspan(4);
					else if(IMGW_JOURNAL_CONF_SPECIAL)
						_tmp.setColspan(8);
					else if(UEK_JOURNAL_CONF_SPECIAL)
						_tmp.setColspan(8);
					else
						_tmp.setColspan(3);
					table.addCell(_tmp);

					if(!IMGW_JOURNAL_CONF_SPECIAL){
					for (int i = 1; i <= 2; i++) {
						_tmp = new Cell(new Phrase("", font));
						_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
						table.addCell(_tmp);
					}
					}

					if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
					_tmp = new Cell(new Phrase(sm.getString("Zprzeniesienia"), font));
					_tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					_tmp.setColspan(4);
					table.addCell(_tmp);
					}

					// Opłata zł gr 2 pierwsze oraz kwota pobrania
					if(!UEK_JOURNAL_CONF_SPECIAL){
						for (int i = 1; i <= 4; i++) {
							if (i == 1 || i == 2)
	                        {
	                            if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
	                                _tmp = new Cell(new Phrase("", font));
	                            else
								    _tmp = new Cell(new Phrase("0", font));
	                        }
							else
								_tmp = new Cell(new Phrase(" ", font));
							_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
							table.addCell(_tmp);
						}	
					}
					
					
					
					table.endHeaders();
				}
                //koniec nagłówka
                
                //wypenianie danymi pdfa 
                
                int lp = 0;
                BigDecimal sumOfStampFees = new BigDecimal(0d);
                // WYLICZENIE CA�OWIEKTEJ LICZBY STRON
                int  pagesCount = calculatePageNumber(results);
                for (int i = 0; i < results.size(); i++)
                {
					final Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
					final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(bean.getDocumentId());
					FieldsManager fm = document.getFieldsManager();
					for (Iterator iter = document.getRecipients().iterator(); iter.hasNext();) {
						_lp = new Cell(new Phrase(String.valueOf(++lp), font));
						_lp.setHorizontalAlignment(Cell.ALIGN_CENTER);
						table.addCell(_lp);

						if (numerKoPism) {
							// numer ko
							String ko = "";
							ko = document.getOfficeNumber() != null ? document.getOfficeNumber().toString() : "";
							Cell _ko = new Cell(new Phrase(ko, font));
							table.addCell(_ko);
						}

						// adresat
						String recipient = null;
						String address = null;
						String zpo = null;
						String remarks = null;

						Recipient rcpt = (Recipient) iter.next();

						// adresat
						StringBuilder sbRecipient = new StringBuilder(100);
						if (!StringUtils.isEmpty(rcpt.getOrganization())) {
							sbRecipient.append(rcpt.getOrganization() + " ");
							if (AvailabilityManager.isAvailable("print.journal.organization_name_extended")) {
								sbRecipient.append((!StringUtils.isEmpty(rcpt.getFirstname()) ? rcpt.getFirstname() + " " : "")
										+ (!StringUtils.isEmpty(rcpt.getLastname()) ? rcpt.getLastname() : ""));
							}
						}
						else
							sbRecipient.append((!StringUtils.isEmpty(rcpt.getFirstname()) ? rcpt.getFirstname() + " " : "")
									+ (!StringUtils.isEmpty(rcpt.getLastname()) ? rcpt.getLastname() : ""));
						
						recipient = sbRecipient.toString();
						
						if(PRINTOUTJOURNAL_INS_SPECIAL){
							List<String> recipientBuilder = new ArrayList<String>();
							
							if(!StringUtils.isEmpty(rcpt.getFirstname()))
								recipientBuilder.add(rcpt.getFirstname());
							if(!StringUtils.isEmpty(rcpt.getLastname()))
								recipientBuilder.add(rcpt.getLastname());
							if(!StringUtils.isEmpty(rcpt.getOrganization()))
								recipientBuilder.add(rcpt.getOrganization());
							if(!StringUtils.isEmpty(rcpt.getOrganizationDivision()))
								recipientBuilder.add(rcpt.getOrganizationDivision());
							
							recipient = StringUtils.join(recipientBuilder, "\n");
						}

						address = TextUtils.nullSafeString(rcpt.getStreet()) + "\n" + TextUtils.nullSafeString(rcpt.getZip()) + " "
								+ TextUtils.nullSafeString(rcpt.getLocation());

						zpo = TextUtils.nullSafeString(document.getPostalRegNumber());
						if (AvailabilityManager.isAvailable("printJournel.ZPOInremark") && document.getZpo() != null && document.getZpo()) {
							remarks = "ZPO";
						}else if (AvailabilityManager.isAvailable("printJournal.KOInremark")){
								remarks = document.getOfficeNumber() !=null ? document.getOfficeNumber().toString():"Brak KO";
							}
						else if (!AvailabilityManager.isAvailable("printJournel.ZPOInremark")) {
							remarks = document.getDelivery() != null ? document.getDelivery().getName() : null;
						}

						if (AvailabilityManager.isAvailable("printJournal.ZpoAndGaugeAndPrInRemark")) {
							remarks = "";
							if (document.getZpo() != null && document.getZpo().equals(Boolean.TRUE)) {
								remarks = sm.getString("ZPO");
							}
							
							if (document.getGauge() != null && !AvailabilityManager.isAvailable("printJournal.disable.gauge")) {
								Integer id = document.getGauge();
								String val = Gauge.find(id).getTitle();
								if (remarks.contains(sm.getString("ZPO"))) {
									remarks += ", ";
								}
								if(!AvailabilityManager.isAvailable("hideWordGabaryt")){
									remarks += "Gabaryt " + val;
								} else {
									remarks += val;
								}
								
							}
							
							if (document.getDelivery() != null) {
								OutOfficeDocumentDelivery delivery = document.getDelivery();
								String deliveryName = delivery.getName().toLowerCase();
								if (deliveryName.contains("priorytet")) {
									if (remarks.length() > 0 || remarks.contains(sm.getString("ZPO")) || remarks.contains("Gabaryt")) {
										remarks += ", ";
									}
									remarks += sm.getString("PR");
								}
							}
						}
						
						if (AvailabilityManager.isAvailable("printJournal.ZpoAndGaugeAndPrInRemarkAms")) {
							remarks = "";
							if(document.getDelivery() != null) {
								OutOfficeDocumentDelivery delivery = document.getDelivery();
								String deliveryName = delivery.getName().toLowerCase();
								if (deliveryName.contains("priorytet") && document.getZpo() != null && document.getZpo().equals(Boolean.FALSE)) {
									remarks = sm.getString("PR");
								} else if (deliveryName.contains("priorytet") && document.getZpo() != null && document.getZpo().equals(Boolean.TRUE)) {
									remarks = sm.getString("ZPO");
								}  
							}
						}
						
                        if (PRINTJOURNAL_REMARKS_AS_PR)
                            remarks = "PR";

                        if(AvailabilityManager.isAvailable("printJournal.hideRecipientIfNotRegistryLetter")){
                        	if (!isRegistryLetter(document)){
                        	_recipient = new Cell(new Phrase("", font));
                        	table.addCell(_recipient);	_address = new Cell(new Phrase("", font));
                        	
    						_address = new Cell(new Phrase("", font));
    						table.addCell(_address);
                        }else{
                        	String registryZpo = " ZPO ";
							if (letterHasZpo(document))
							remarks = remarks + registryZpo ;
							_recipient = new Cell(new Phrase(recipient, font));
							table.addCell(_recipient);

							_address = new Cell(new Phrase(address, font));
							table.addCell(_address);
                        }
                        }else {
						_recipient = new Cell(new Phrase(recipient, font));
						table.addCell(_recipient);

						_address = new Cell(new Phrase(address, font));
						table.addCell(_address);
                        }
						// _value
                        if(AvailabilityManager.isAvailable("UKW.dziennik.wlacz.wyswietlanieWartoscKwota")){
                        	if(document.getStampFee() != null){
                        		if(fm.getValue("ILOSC") != null && !fm.getValue("ILOSC").equals(0)){
                        		int ilosc = Integer.valueOf(String.valueOf(fm.getValue("ILOSC")));
                        		String suma = document.getStampFee().divide(new BigDecimal(ilosc)).setScale(2).toString();
                            	Cell stampfeeCell = new Cell(new Phrase(document.getStampFee().toString(), font));
                            	stampfeeCell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                            	table.addCell(suma.substring(0, suma.indexOf(".")));
                            	table.addCell(suma.substring(suma.indexOf(".")+1));
                            	}else { 
                            		String kwota = document.getStampFee().setScale(2).toString();
                            		table.addCell(new Cell(kwota.substring(0, kwota.indexOf("."))));
                            		table.addCell(new Cell(kwota.substring(kwota.indexOf(".")+1)));
                            	}
                        	}else {
                            		table.addCell(new Cell(""));
            						table.addCell(new Cell(""));
                            	}
                        } else if(!IMGW_JOURNAL_CONF_SPECIAL){                 	
						table.addCell(new Cell(""));
						table.addCell(new Cell(""));
                        }

                        if(IMGW_JOURNAL_CONF_SPECIAL){
                        	table.addCell(new Cell(new Phrase(document.getSummary(), font)));
                        }

						// _weight
                        if(AvailabilityManager.isAvailable("UKW.dziennik.wlacz.wyswietlanieWagZPrzedzialu")){
                        	if(document.getWeightKg() == null && document.getWeightG() == null){
                        		if(fm.getEnumItem("WAGAKRAJ") != null){
                        			String nazwa = fm.getEnumItem("WAGAKRAJ").getTitle();
                        			if(nazwa.contains("kg")){
                        				table.addCell(new Cell(nazwa));
                        				table.addCell(new Cell(""));
                        			} else {
                        				table.addCell(new Cell(""));
                        				table.addCell(new Cell(nazwa));
                        			}
                        		} else {
                        			table.addCell(new Cell(""));
                        			table.addCell(new Cell(""));
                        		}
                        	} else {
                        		Cell weight = new Cell(new Phrase(document.getWeightKg().toString(), font));
    							weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
    							table.addCell(weight);
    							
    							Cell weightG = new Cell(new Phrase(document.getWeightG().toString(), font));
    							weightG.setHorizontalAlignment(Cell.ALIGN_CENTER);
    							table.addCell(weightG);
                        	}
                        } else if (UEK_JOURNAL_CONF_SPECIAL) {
                        	try {
                        		MailWeightKind weight = MailWeightKind.find(document.getMailWeight());
                        		Cell weightCell = new Cell(new Phrase(weight.getName(), font));
                        		weightCell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                        		weightCell.setColspan(1);
                        		table.addCell(weightCell);
							} catch (Exception e) {
								log.error(e.getMessage(), e);
								Cell weightCell = new Cell("");
								weightCell.setColspan(1);
								table.addCell(weightCell);
							}
                        } else {
							if (document.getWeightKg() == null) {
								table.addCell(new Cell(""));
							} else {
								Cell weight = new Cell(new Phrase(document.getWeightKg().toString(), font));
								weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
								table.addCell(weight);
							}
							if (document.getWeightG() == null) {
								table.addCell(new Cell(""));
							} else {
								Cell weight = new Cell(new Phrase(document.getWeightG().toString(), font));
								weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
								table.addCell(weight);
							}
                        }
						_zpo = new Cell(new Phrase(zpo != null ? zpo : "", font));
						_zpo.setHorizontalAlignment(Cell.ALIGN_CENTER);
						table.addCell(_zpo);

						// uwagi
						if(PRINTOUTJOURNAL_INS_SPECIAL){
							if(document.getZpo() != null && document.getZpo())
								remarks = ((remarks==null)?"":(remarks+" ZPO "));
							
							remarks = ((remarks==null)?"":(remarks+" "));
							
							remarks += ((fm.getStringValue("DOC_NUMBER")==null)?"":(fm.getStringValue("DOC_NUMBER")) + " ");
							
							remarks += sm.getString("KO") + ": " + ((document.getOfficeNumber() != null)?(document.getOfficeNumber()+" "):"brak ");
						}
						
						_remarks = new Cell(new Phrase(remarks != null ? remarks : "", font));
						_remarks.setHorizontalAlignment(Cell.ALIGN_CENTER);
						table.addCell(_remarks);

						// _fee
						if (document.getStampFee() == null) {
							table.addCell(new Cell(""));
							table.addCell(new Cell(""));
						} else {
							sumOfStampFees = sumOfStampFees.add(document.getStampFee());

							String fee = document.getStampFee().setScale(2).toString();
							if (fee.indexOf('.') != -1) {
								table.addCell(new Cell(fee.substring(0, fee.indexOf('.'))));
								table.addCell(new Cell(fee.substring(fee.indexOf('.') + 1)));
							} else {
								if (AvailabilityManager.isAvailable("printOutJournal.set.emptyStampfeeCell")) {
									table.addCell(new Cell(""));
									table.addCell(new Cell(""));
								} else {
									table.addCell(new Cell(fee));
									table.addCell(new Cell("0"));
								}
								
							}
						}
						// _kwota
						Cell kwotaPobraniaZl;
						Cell kwotaPobraniaGr;
						if(IMGW_JOURNAL_CONF_SPECIAL){
							try{
								String sql = "select kwota_pobrania from dsg_normal_dockind where document_id = " + document.getId();
								Statement stmt = DSApi.context().createStatement();
								ResultSet rs = stmt.executeQuery(sql);
								BigDecimal kwotaPobrania = BigDecimal.ZERO;
								if(rs.next()){
									kwotaPobrania = rs.getBigDecimal(1);
								}
								
								if(kwotaPobrania == null){
									kwotaPobrania = BigDecimal.ZERO;
								}
								
								BigDecimal fraction = kwotaPobrania.remainder(BigDecimal.ONE);
								fraction = fraction.multiply(new BigDecimal(100));
								fraction = fraction.setScale(0, BigDecimal.ROUND_HALF_UP);
								kwotaPobraniaZl = new Cell(new Phrase(String.valueOf(kwotaPobrania.intValue()), font));
								kwotaPobraniaZl.setHorizontalAlignment(Cell.ALIGN_CENTER);
								table.addCell(kwotaPobraniaZl);
								kwotaPobraniaZl = new Cell(new Phrase(fraction.toString(), font));
								kwotaPobraniaZl.setHorizontalAlignment(Cell.ALIGN_CENTER);
								table.addCell(kwotaPobraniaZl);
								
								stmt.close();
							} catch (Exception e){
								log.error(null, e.toString());
								e.printStackTrace();
							} 
						} else {
							if (!UEK_JOURNAL_CONF_SPECIAL) {
								table.addCell(new Cell(""));
								table.addCell(new Cell(""));
							}
						}
						
					
									
					if ((lp-7)%rows == 0 && !AvailabilityManager.isAvailable("pritnOutJournal.bezDoPrzeniesienia")){
                        //jesli rozszezony dodajemy do przeniesienia na koncu strony poprzedniej 
                        if (extended)
                        {
                        	_tmp = new Cell(new Phrase(sm.getString("DoPrzeniesienia"), font));
                            _tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                            
                            if(numerKoPism)
                            	_tmp.setColspan(4);
                            else if(IMGW_JOURNAL_CONF_SPECIAL)
                            	_tmp.setColspan(8);
                            else if(UEK_JOURNAL_CONF_SPECIAL)
                                _tmp.setColspan(8);
                            else 
                            	_tmp.setColspan(3);
                            _tmp.setBorderWidthBottom(0);
                            _tmp.setBorderWidthLeft(0);
                            table.addCell(_tmp);

                            if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
                            for (int k=1; k <=2; k++)
                            {
                                _tmp = new Cell(new Phrase(" ", font));
                                _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                table.addCell(_tmp);
                            }
                            }

                            if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
                            _tmp = new Cell(new Phrase(sm.getString("DoPrzeniesienia"), font));
                            _tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                            _tmp.setColspan(4);
                            _tmp.setBorderWidthBottom(0);
                            table.addCell(_tmp);
                            }
                            
                            //kwota do przeniesienia 
                         	String doPrzeniesienia = (sumOfStampFees.equals(0d)) ? "" : sumOfStampFees.setScale(2).toString();
                            if(doPrzeniesienia.indexOf('.')!=-1)
                            {
                            	table.addCell(new Cell(doPrzeniesienia.substring(0, doPrzeniesienia.indexOf('.'))));
                            	table.addCell(new Cell(doPrzeniesienia.substring(doPrzeniesienia.indexOf('.')+1)));
                            }
                            else
                            {
                                if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
                                    table.addCell(new Cell(""));
                                else
                            	    table.addCell(new Cell(doPrzeniesienia));
                                if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
                                    table.addCell(new Cell(""));
                                else
                            	    table.addCell(new Cell("0"));
                            }       

                            if (!UEK_JOURNAL_CONF_SPECIAL) {
                            	_tmp = new Cell(new Phrase(" ", font));
                                _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                _tmp.setColspan(2);
                                _tmp.setBorderWidthBottom(0);
                                table.addCell(_tmp);
                            }

                        }
                    	
                        //  konczymy tabele i tworzymy nowa tabele na nastepnej stronie 

                    	pdfDoc.add(table);

                        if (lp%7==0 && PRINTOUTJORNAL_ADDPAGESNUMBER)
                        {
                            addPageNumberOfTotalPagesNumber(font, pdfDoc, pagesCount);
                        }
                    	pdfDoc.newPage();
                    	table.deleteAllRows();
                    	table = new Table(widths.length);
                    	if (!extended) {
                    		buildTableHeader(font, widths, table, true);
                    	}
                    	 //PdfPTable table = new PdfPTable(widths.length);
                        table.setWidths(widths);
                        table.setWidth(100.0f);
                        table.setCellsFitPage(true);
                        table.setPadding(2);
                        
                  		 
                    //jesli rozszerzony dodajemy wartosc z przeniesienia w nagĹ‚Ăłwku
                    if (extended)
                    {
                    	//z kolumna ko 
                    	 if(numerKoPism){
                             for ( int j=1; j <=10; j++)
                             {
                                 _tmp = new Cell(new Phrase(""+j, font));
                                 _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                 if (j == 5 || j == 6 || j == 9 || j == 10 )
                                     _tmp.setColspan(2);
                                 table.addCell(_tmp);
                             }
                         //bez kolumny ko 
                         }else { 
                        	 if (UEK_JOURNAL_CONF_SPECIAL) {
     							for (int k = 1; k <= 8; k++) {
     								_tmp = new Cell(new Phrase("" + k, font));
     								_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
     								if (k == 4 || k == 8) {
     									_tmp.setColspan(2);
     								}	
     								table.addCell(_tmp);
     							}
     						} else {
     							for (int j=1; j <=9; j++)
                                {
                                    _tmp = new Cell(new Phrase(""+j, font));
                                    _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                    if(IMGW_JOURNAL_CONF_SPECIAL){
        								if (j == 5 || j == 8 || j == 9)
        									_tmp.setColspan(2);
        							} else {
        							if (j == 4 || j == 5 || j == 8 || j == 9)
        								_tmp.setColspan(2);
        							}
        							table.addCell(_tmp);
                                }
     						}
                         }
                    	 
                        _tmp = new Cell(new Phrase(sm.getString("Zprzeniesienia"), font));
                        _tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                        
                        if (numerKoPism)
     						_tmp.setColspan(4);
     					else if(IMGW_JOURNAL_CONF_SPECIAL)
     						_tmp.setColspan(8);
     					else if(UEK_JOURNAL_CONF_SPECIAL)
     						_tmp.setColspan(8);
     					else
     						_tmp.setColspan(3);
                        
                        table.addCell(_tmp);
                        
                        if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
         					for (int j = 1; j <= 2; j++) {
         						_tmp = new Cell(new Phrase("", font));
         						_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
         						table.addCell(_tmp);
         					}
         				}

                        if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
         					_tmp = new Cell(new Phrase(sm.getString("Zprzeniesienia"), font));
         					_tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
         					_tmp.setColspan(4);
         					table.addCell(_tmp);
         				}
                        
                        for (int j=1; j <=4; j++)
                        {
                        	 if (j==1){
                             	String kwotaZprzeniesienia= "";
                             	kwotaZprzeniesienia = sumOfStampFees.setScale(2).toString();
                             	 if(kwotaZprzeniesienia.indexOf('.')!=-1)
                                  { //kwota  zĹ‚ote
                             		 _tmp = new Cell(kwotaZprzeniesienia.substring(0, kwotaZprzeniesienia.indexOf('.')));
                                  	 _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                      table.addCell(_tmp);
                                    ////kwota grosze
                                      _tmp = new Cell(kwotaZprzeniesienia.substring(kwotaZprzeniesienia.indexOf('.')+1));
                                      _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                      table.addCell(_tmp);
                                      j= 2;
                                  }else
                                  {//kwota
                                	  _tmp = new Cell(kwotaZprzeniesienia);
                                	  _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                      table.addCell(_tmp);
                                      if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
                                          _tmp = new Cell("");
                                	  else
                                          _tmp = new Cell("0");
                                	  _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                      table.addCell(_tmp);
                                      j= 2;
                                } 
                        	 } else {
                            	  if(!UEK_JOURNAL_CONF_SPECIAL) {
                            		  _tmp = new Cell(new Phrase(" ", font));
                                      _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                                      table.addCell(_tmp);
                            	  }
                              	}
                        }
                    }
                    //koniec nagĹ‚Ăłwka nowej strony 
                    table.endHeaders();
                    }
                }
               }
                // wpis na ostatniej stronie z Ĺ‚acznÄ… kwotÄ… do przeniesienia 
                if (extended)
                {

                    _tmp = new Cell(new Phrase(sm.getString("DoPrzeniesienia"), font));
                    _tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                    if(numerKoPism)
                    	_tmp.setColspan(4);
                    else if(IMGW_JOURNAL_CONF_SPECIAL){
                    	_tmp.setColspan(8);
                    }
                    else if(UEK_JOURNAL_CONF_SPECIAL){
                    	_tmp.setColspan(8);
                    }
                    else 
                    	_tmp.setColspan(3);
                    _tmp.setBorderWidthBottom(0);
                    _tmp.setBorderWidthLeft(0);
                    table.addCell(_tmp);

                    if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
                    for (int i=1; i <=2; i++)
                    {
                        _tmp = new Cell(new Phrase(" ", font));
                        _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                        table.addCell(_tmp);
                    }
                    }

                    if(!IMGW_JOURNAL_CONF_SPECIAL && !UEK_JOURNAL_CONF_SPECIAL){
                    _tmp = new Cell(new Phrase(sm.getString("DoPrzeniesienia"), font));
                    _tmp.setHorizontalAlignment(Cell.ALIGN_RIGHT);
                    _tmp.setColspan(4);
                    _tmp.setBorderWidthBottom(0);
                    table.addCell(_tmp);
                    }

                    
                    String feesSum = sumOfStampFees.setScale(2).toString();
                    if(feesSum.indexOf('.')!=-1)
                    {
                    	table.addCell(new Cell(feesSum.substring(0, feesSum.indexOf('.'))));
                    	table.addCell(new Cell(feesSum.substring(feesSum.indexOf('.')+1)));
                    }
                    else
                    {
                        if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
                            table.addCell(new Cell(""));
                        else
                    	    table.addCell(new Cell(feesSum));
                        if (PRINTOUTJOURNAL_DOPRZENIESIENIA_PUSTY)
                            table.addCell(new Cell(""));
                        else
                    	    table.addCell(new Cell("0"));
                    }       

                    _tmp = new Cell(new Phrase(" ", font));
                    _tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                    _tmp.setColspan(2);
                    _tmp.setBorderWidthBottom(0);
                    table.addCell(_tmp);

                }
                pdfDoc.add(table);
                // koniec tabeli

                // licza niezarejestrowanych ekonomicznie
                if (PRINTOUTJOURNAL_NIEZREJESTROWANE)
                {
                    addNoRegistered(font, pdfDoc);
                }
                if (PRINTOUTJORNAL_ADDPAGESNUMBER)
                {
                    int pageNumber = pdfDoc.getPageNumber();
                    String msg = "Strona " + pagesCount + " z " + pagesCount + ".";
                    Phrase phrase = new Phrase(msg, font);
                    Paragraph p = new Paragraph(phrase);
                    pdfDoc.add(p);
                }
                // zamykamy dokument
                pdfDoc.close();
            }
            catch (Exception e)
            {
                log.error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }

            if (temp != null && temp.exists())
            {
                if (log.isDebugEnabled())
                    log.debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                showPDF(temp);
            }
        }

		private Table buildTableHeader(Font font, float[] widths, Table headerWithLabels, boolean endHeader) throws BadElementException {
			headerWithLabels.setWidths(widths);
			headerWithLabels.setWidth(100.0f);
			headerWithLabels.setCellsFitPage(true);
			headerWithLabels.setPadding(2);
			
			Cell _lp = new Cell(new Phrase(sm.getString("Lp"), font));
			_lp.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_lp.setRowspan(2);
			headerWithLabels.addCell(_lp);
            
            
			//KolumnaKO
			if (numerKoPism) {
				Cell _ko = new Cell(new Phrase(sm.getString("NumerKO"), font));
				_ko.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_ko.setRowspan(2);
				headerWithLabels.addCell(_ko);
			}
			
			
			Cell _recipient =null;
			if (PRINTOUTJOURNAL_UTP_SPECIAL)
				_recipient = new Cell(new Phrase(sm.getString("ADRESATImieInazwiskoLubZazwa"), font));
			else if(PRINTOUTJOURNAL_INS_SPECIAL)
				_recipient = new Cell(new Phrase(sm.getString("AdresatImieNazwiskoNazwaDzial"), font));
			else
				_recipient = new Cell(new Phrase(sm.getString("ODBIORCAImieInazwiskoLubZazwa"), font));
				_recipient.setHorizontalAlignment(Cell.ALIGN_LEFT);
				_recipient.setRowspan(2);
				headerWithLabels.addCell(_recipient);

				
			
			Cell _address = new Cell(new Phrase(sm.getString("DokladneMiejsceDoreczenia"), font));
			_address.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_address.setRowspan(2);
			headerWithLabels.addCell(_address);

			Cell _value=null;
			if(PRINTOUTJOURNAL_UTP_SPECIAL){
			_value = new Cell(new Phrase(sm.getString("kwotZadkWart"), font));
			} else {
				if(!IMGW_JOURNAL_CONF_SPECIAL){
					_value = new Cell(new Phrase(sm.getString("WartoscKwota"), font));
					_value.setHorizontalAlignment(Cell.ALIGN_CENTER);
					_value.setColspan(2);
					headerWithLabels.addCell(_value);
				} else {
					Cell _summary = new Cell(new Phrase(sm.getString("Opis"), font));
					_summary.setHorizontalAlignment(Cell.ALIGN_CENTER);
					_summary.setRowspan(2);
					headerWithLabels.addCell(_summary);
				}
			}

			
			
			if(UEK_JOURNAL_CONF_SPECIAL) {
				Cell _weight = new Cell(new Phrase(sm.getString("Masa"), font));
				_weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_weight.setRowspan(2);
				headerWithLabels.addCell(_weight);
			} else {
				Cell _weight = new Cell(new Phrase(sm.getString("Masa"), font));
				_weight.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_weight.setColspan(2);
				headerWithLabels.addCell(_weight);
			}
			

			Cell _zpo = null;
			if(PRINTOUTJOURNAL_UTP_SPECIAL)
			_zpo = new Cell(new Phrase(sm.getString("NrNadawczy"), font));
			else
			_zpo = new Cell(new Phrase(sm.getString("NumerNadawczy"), font));	
			_zpo.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_zpo.setRowspan(2);
			headerWithLabels.addCell(_zpo);

			Cell _remarks = new Cell(new Phrase(sm.getString("Uwagi"), font));
			_remarks.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_remarks.setRowspan(2);
			headerWithLabels.addCell(_remarks);

			Cell _fee = new Cell(new Phrase(sm.getString("Oplata"), font));
			_fee.setHorizontalAlignment(Cell.ALIGN_CENTER);
			_fee.setColspan(2);
			headerWithLabels.addCell(_fee);

			if (!UEK_JOURNAL_CONF_SPECIAL) {
				Cell _kwota = new Cell(new Phrase(sm.getString("KwotaPobrania"), font));
				_kwota.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_kwota.setColspan(2);
				headerWithLabels.addCell(_kwota);
			}

			if(PRINTOUTJOURNAL_UTP_SPECIAL){
				Cell _oplata = new Cell(new Phrase("", font));
				_oplata.setHorizontalAlignment(Cell.ALIGN_CENTER);
				_oplata.setColspan(2);
				headerWithLabels.addCell(_oplata);
			}
//                Cell _oplata = new Cell(new Phrase(sm.getString("OplataZaPobranie"), font));
//                _oplata.setHorizontalAlignment(Cell.ALIGN_CENTER);
//                _oplata.setColspan(2);
//                headerWithLabels.addCell(_oplata);

			// drugi wiersz

			Cell _tmp;

			if(!IMGW_JOURNAL_CONF_SPECIAL){
				_tmp = new Cell(new Phrase(sm.getString("zl"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
	
				_tmp = new Cell(new Phrase(sm.getString("gr"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
			}

			if(!UEK_JOURNAL_CONF_SPECIAL) {
				_tmp = new Cell(new Phrase(sm.getString("kg"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
				
				_tmp = new Cell(new Phrase(sm.getString("g"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
			}

			_tmp = new Cell(new Phrase(sm.getString("zl"), font));
			_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
			headerWithLabels.addCell(_tmp);

			_tmp = new Cell(new Phrase(sm.getString("gr"), font));
			_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
			headerWithLabels.addCell(_tmp);

			if(!UEK_JOURNAL_CONF_SPECIAL) {
				_tmp = new Cell(new Phrase(sm.getString("zl"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
				
				_tmp = new Cell(new Phrase(sm.getString("gr"), font));
				_tmp.setHorizontalAlignment(Cell.ALIGN_CENTER);
				headerWithLabels.addCell(_tmp);
			}
			
			if (endHeader) {
				headerWithLabels.endHeaders();
			}
			
			return headerWithLabels;
		}
    }

    private boolean letterHasZpo(OutOfficeDocument document)
	{
		if (document.getZpo()!=null)
				return true;
		return false;
	}
    
    private boolean isRegistryLetter(OutOfficeDocument document)
	{
		 
		if (document.getDelivery() != null && Docusafe.getAdditionProperty("idRegistyletter")!=null && !Docusafe.getAdditionProperty("idRegistyletter").isEmpty() ){
			String listId = Docusafe.getAdditionProperty("idRegistyletter");
			for( String l :listId.split(",")){
				if(Integer.parseInt(l.trim())==document.getDelivery().getId())
				return true;
			}
		 }
		return false;
		
	}
    private void addNoRegistered(Font font, Document pdfDoc) throws DocumentException
    {
        Table table2 = new Table (2);
        table2.setWidth(35);
        float [] width2 = { 2f, 0.5f};
        table2.setWidths(width2);
        table2.setAlignment(Table.ALIGN_LEFT);
        Cell _nieZarejestrowane = new Cell(new Phrase("Listy niezarejestrowane ekonomicznie", font));
        _nieZarejestrowane.setHorizontalAlignment(Cell.ALIGN_LEFT);
        table2.setPadding(2);
        table2.addCell(_nieZarejestrowane);
        table2.addCell(new Cell(""));
        // koniec tabeli
        pdfDoc.add(table2);
    }

	
    public void showPDF(File temp)
	{
		try
        {
            ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                "application/pdf", (int) temp.length());
        }
        catch (IOException e)
        {
            log.error("", e);
        }
        finally
        {
            temp.delete();
        }
	}
/**
 * 
 * @author Grzegorz Filip <grzegorz.filip@docusafe.pl>
 * @date 05.09.2013
	@param mapaRodzajowWag
	@param mapaKosztow
	@param mapaIloscPoszczegolnychWag
	@version 1.0
 * @Opis 
    Adds journal.printOut.ilosciowy
	Klasa generuje dokument pdf  w formacie zestawienia ilo�ciowego poszczegolnych  przesy�ek listowych NIE REJESTROWANYCH przez poczte polsk� 
	Krajowych oraz Zagranicznych. Dziel�c je na poszczeg�le kategorie Gabarytowe , Wagowe oraz ilo�ciowe  w formie  tabeli :
	Dla Krajowych 
	w domy�lnej wersji : 
												|EKONOMICZNE |
	 | Przedzial wagowy | 		GABARYT A				|	 	GABARYT B 				 |
	 					|ILOSC |CENA JEDNOSTKOWA | SUMA | ILOSC |CENA JEDNOSTKOWA | SUMA |
	 | do 350 g			|	   |				 |		|	   |				  |		 |
	 | do 1000g			|...
	 | do 2000g			|...
	 | RAZEM 			|	   |				 |		|	   |				  |		 |
	 
	 
												|PRIORYTETOWE |
	 | Przedzial wagowy | 		GABARYT A				|	 	GABARYT B 				 |
	 					|ILOSC |CENA JEDNOSTKOWA | SUMA | ILOSC |CENA JEDNOSTKOWA | SUMA |
	 | do 350 g			|	   |				 |		|	   |				  |		 |
	 | do 1000g			|...
	 | do 2000g			|...
	 | RAZEM 			|	   |				|		|	   |				  |		 |
	
	
	@Warunki  wygenerowania raportu :
	1 -Okreslenie na dokumencie czy jest to list krajowy czy zagraniczny 
	2 -Okreslenie na dokumencie czy jest to list priorytetowy czy ekonomiczny
	3 -Okreslenie na dokumencie Gabarytu listu 
	4 -(  jeszcze nie istnieje )Okreslenie na dokumencie fielda wagi konkretnego listu 
	W przypadku nie okre�lenia parametr�w  na documencie documenty trafia do nieOkreslonych i zostan wypisane na koniec raportu 
	
	@Metoda 
	
	Do czasu jak nie powstana fieldy wag listu 
	Na logice dokumentu w medodzie  addWeightAndCostsLettersSelectedByValues  
	Nalezy uzupe�ni� z przekazanych parametr�w oraz z np z  fieldManagera  map� koszt�w  oraz map� Ilosci PoszczegolnychWag	
	mapa koszt�w sk�ada si� z ->klucza Gabaryt i mapy <Integer , BigDecimal> wag ,ktora mo�na nadpisa� na logice dokumentu ,
	w ktorej zliczane powinny byc   koszty listu
	mapa rodzajow wag   sk�ada si� z ->klucza Gabaryt i mapy <Integer,Integer> wag ktora mo�na  nadpisa� na logice dokumentu 
	w ktorej zliczane powinny byc ilosci poszczegolnych pism 
	 
	
	@dzia�anie 
	
	Przy generowaniu wydruku clasa kolejno bedzie przekazywa� do logiki dokumentu  posortowane po kraj/swiat priorytet/ekonomiczny dokumenty
	w kolejno�ci  o ile jakies  w dane jgrupie isniej� istniej� :
	1 - krajowe ekonomiczne
	2 - krajowe priorytetowe
	3 - swiat(w trakcie budowy )
	 
	Przyk�ad wypelnienia map - mo�e nie elegancki ale jakikolwiek w PaaDocOutLogic
 */

	public class GenerujEtykiete implements ActionListener
	{


	public void actionPerformed(ActionEvent event) throws Exception
	{
		 
          try {
            
        	  OfficeDocument doc =   OfficeDocument.find(documentId);
        	  List<Recipient> recipients = doc.getRecipients();
        	  Collections.sort(recipients, new Comparator<Recipient>(){
                  public int compare (Recipient r1, Recipient r2){
                      return r1.getId().compareTo(r2.getId());
                  }
              });
        	  File temp = null;
        	  int i = 0 ;
        	  int  recsize = recipients.size();
        	  com.lowagie.text.Document pdfDoc = null;
        	  for(Recipient r :recipients){
  
        	 
              File fontDir = new File(Configuration.getHome(), "fonts");
              File arial = new File(fontDir, "arial.ttf");
              BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                      BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

              Font font10 = new Font(baseFont, 10);
              Font font8 = new Font(baseFont, 7);

              temp = File.createTempFile("docusafe_", "_tmp");
             
              pdfDoc = new com.lowagie.text.Document(PageSize.A6.rotate());
            
              FileOutputStream fis = new FileOutputStream(temp);
              PdfWriter.getInstance(pdfDoc, fis);

              pdfDoc.open();
              StringBuilder wpis = new StringBuilder("") ;
              if(r.getFirstname()!=null || r.getLastname()!=null){
              Paragraph p1 = new Paragraph(new Phrase("Imi� i Nazwisko:", font8));
              p1.setAlignment(Element.ALIGN_LEFT);
             
              wpis.append( r.getFirstname()!=null? r.getFirstname() + " " : "");
              wpis.append( r.getLastname()!=null? r.getLastname() : "");
              pdfDoc.add(p1);
              Paragraph p2 = new Paragraph(new Phrase(wpis.toString(), font10));
              p2.setAlignment(Element.ALIGN_LEFT);
              pdfDoc.add(p2);
              }
              Paragraph p3 = new Paragraph(new Phrase("Nazwa firmy:", font8));
              p3.setAlignment(Element.ALIGN_LEFT);
              Paragraph p4 = new Paragraph(new Phrase(r.getOrganization()!=null? r.getOrganization() : "", font10));
              p4.setAlignment(Element.ALIGN_LEFT);
              
              Paragraph p5 = new Paragraph(new Phrase("Adres:", font8));
              p5.setAlignment(Element.ALIGN_LEFT);
              Paragraph p6 = new Paragraph(new Phrase(r.getStreet()!=null ? r.getStreet() : "", font10));
              p6.setAlignment(Element.ALIGN_LEFT);
             
              Paragraph p7 = new Paragraph(new Phrase("Kod i miejscowo��: ", font8));
              p7.setAlignment(Element.ALIGN_LEFT);
              wpis = new StringBuilder("");
              wpis.append(r.getZip()!=null ? r.getZip() +"," : "");
              wpis.append(r.getLocation()!= null ? r.getLocation() :"");
              Paragraph p8 = new Paragraph(new Phrase(wpis.toString(), font10));
              p8.setAlignment(Element.ALIGN_LEFT);
             
              pdfDoc.add(p3);
              pdfDoc.add(p4);
              pdfDoc.add(p5);
              pdfDoc.add(p6);
              pdfDoc.add(p7);
              pdfDoc.add(p8);
              
					if (r.getCountry() != null)
					{
						Paragraph p9 = new Paragraph(new Phrase("Kraj: ", font8));
						p9.setAlignment(Element.ALIGN_LEFT);
						String countryName = getCountryName(r);
						Paragraph p10 = new Paragraph(new Phrase(countryName, font10));
						p10.setAlignment(Element.ALIGN_LEFT);
						pdfDoc.add(p9);
						pdfDoc.add(p10);
					}

              i++;
              if(recsize>i){
            	  pdfDoc.newPage();
              }
            
        	  }  
        	  if(pdfDoc!=null){
              pdfDoc.close();

              showPDF(temp);
        	  }

          } catch (Exception e) {
              log.error(e.getMessage(), e);
          }
		
	}

	private String getCountryName(Recipient r) throws SQLException, EdmException
	{
		String sql = "select title from DSR_COUNTRY where id =?";
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.setLong(1, Long.parseLong(r.getCountry()));
		 ResultSet rs = ps.executeQuery();
		 String countryName = "";
		 if(rs.next())
		  countryName = rs.getString("title");
		return countryName;
	}

	}


	 class PdfIlosciowy implements ActionListener
	{
		
		/**
		 * Mapa Okrelajaca rodzaje wag dla poszczeg�lnych list�w w zale�no�ci cyz listy krajowe czy zagraniczne a wartosciami jest mapa kosztow wszystkich listow danego gabarytu ,wagi i typu listu  (ekonomiczny , priorytetowy) 
		 */
		public Map<Integer, String> mapaRodzajowWag = new HashMap();
		/**
		 * Mapa ma zawierac jako klucz gabaryt  a wartosciami jest mapa kosztow wszystkich listow danego gabarytu ,wagi i typu listu  (ekonomiczny , priorytetowy) 
		 */
		public HashMap<Integer, Map<Integer, BigDecimal>> mapaKosztow = new HashMap<Integer, Map<Integer, BigDecimal>>();
		/**
		 * Mapa  ma zawierac jako klucz Gabaryt a wartosciami jest mapa  zliczonych ilosci wszystkich listow danego gabarytu , wagi i typu listu  (ekonomiczny , priorytetowy) 
		 */
		public HashMap<Integer, Map<Integer, Integer>> tempIloscPoszczegolnychWag = new HashMap<Integer, Map<Integer, Integer>>();

		private Integer countGabarytA = 0, countGabarytB = 0, countGabarytC = 0, countGabarytD = 0, nieOkreslony = 0;
		private BigDecimal sumOfGabA = new BigDecimal(0d);
		private BigDecimal sumOfGabB = new BigDecimal(0d);
		private BigDecimal sumOfGabC = new BigDecimal(0d);
		private BigDecimal sumOfGabD = new BigDecimal(0d);
	
		private List<OutOfficeDocument> allDocuments = new ArrayList();
		/**
		 * Lista dokument�w  nie spelniajacych kryteria raportu brak kt�regos z parametr�w : (getCountryLetter()  i/lub getPriority() )
		 */
		private List<OutOfficeDocument> listaDocumentowNieokreslonych = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=0)
		 */
		private List<OutOfficeDocument> listaDokumentowZagranicznych = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=1)
		 */
		private List<OutOfficeDocument> listaDokumentowKraj = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=1 oraz getPriority()=1)
		 */
		private List<OutOfficeDocument> listaDokumentowPriorytetowychKraj = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=1 oraz getPriority()=0)
		 */
		private List<OutOfficeDocument> listaDokumentowEkonomicznychKraj = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=0 oraz getPriority()=1)
		 */
		private List<OutOfficeDocument> listaDokumentowPriorytetowychZagranicznych = new ArrayList();
		/**
		 * Lista dokument�w  posiadajacych parametr : getCountryLetter()=0 oraz getPriority()=0)
		 */
		private List<OutOfficeDocument> listaDokumentowEkonomicznychZagranicznych = new ArrayList();

		private Font font;
		private boolean ListEkonomiczny = true;
		private boolean listyKrajowe = true;
		private Table table = null;


		public void actionPerformed(ActionEvent event)
		{
			File temp = null;

			try
			{
				Journal journal = Journal.find(getId());
				//System.out.print("POJ "+getId());
				if (!journal.isOutgoing())
					throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));

				List results = find(journal);

				// itext (pdf)

				File fontDir = new File(Configuration.getHome(), "fonts");
				File arial = new File(fontDir, "arial.ttf");
				BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

				font = new Font(baseFont, 10);

				temp = File.createTempFile("docusafe_", "_tmp");
				com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A4.rotate());
				FileOutputStream fis = new FileOutputStream(temp);
				PdfWriter.getInstance(pdfDoc, fis);
				
				HeaderFooter footer = new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",
						DSApi.context().getDSUser().asFirstnameLastname(), DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
				footer.setAlignment(Element.ALIGN_CENTER);
				pdfDoc.setFooter(footer);

				float[] widths;
				widths = new float[] { 0.200f, 0.2000f, 0.1795f, 0.1795f, 0.1795f, 0.1795f, 0.1795f };
				table = new Table(widths.length);





				pdfDoc.open();

				if (AvailabilityManager.isAvailable("PAA"))
					PdfUtils.addHeaderImageToPdf(pdfDoc, "Pa�stwowa Agencja Atomistyki", baseFont);
				int pagesCount = calculatePageNumber(results);


				for (int i = 0; i < results.size(); i++)
				{
					final Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
					final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(bean.getDocumentId());
					allDocuments.add(document);
				}

				prepareSortetDocumentsByTypeAndCountry(allDocuments);



				// wype�nianie pdfa dla ekonomicznych krajowych gab A i B 
				if (listaDokumentowEkonomicznychKraj.isEmpty())
				{
					table = buildTableHeaderIlosciowy(font, widths, table, true, "Priorytetowe");
					generujMapyDlaPismKrajowyhPriorytetowych();
				} else
				{
					generujMapyDlaPismKrajowyhEkonomicznych();
					table = buildTableHeaderIlosciowy(font, widths, table, true, "Ekonomiczne");
					for (OutOfficeDocument document : listaDokumentowEkonomicznychKraj)
					{

						fillPdf(document, listyKrajowe, ListEkonomiczny, pdfDoc);

					}
					addResulstRows();
					addSummaryRow();
					cleanCounts();
				}

				// wype�nianie pdfa dla priorytetowych krajowych gab A i B 
				if (listaDokumentowPriorytetowychKraj.isEmpty())
				{

				} else
				{

					if (!listaDokumentowEkonomicznychKraj.isEmpty())
						buildTableHeaderIlosciowy(font, widths, table, false, "Priorytetowe");

					generujMapyDlaPismKrajowyhPriorytetowych();
					for (OutOfficeDocument document : listaDokumentowPriorytetowychKraj)
					{
						fillPdf(document, listyKrajowe, ListEkonomiczny, pdfDoc);
					}
					addResulstRows();
					addSummaryRow();
					cleanCounts();
				}
				wypiszNiePrawidloweDokumenty();
				
				pdfDoc.add(table);
				pdfDoc.close();



				showPDF(temp);
			} catch (Exception e)
			{
				log.error("", e);
			}

		}

		private void wypiszNiePrawidloweDokumenty() throws BadElementException
		{
			Cell _temp;

			_temp = new Cell(" ");
			_temp.setColspan(7);
			table.addCell(_temp);
			_temp = new Cell(new Phrase("Wypis Dokumentow  nie spe�niajaca kryteriow pism nie rejestrowanych przez Poczte Polsk� , badz wadliwie okre�lonych  w procesie obiegu dokumentu", font));
			_temp.setColspan(7);
			_temp.setRowspan(2);
			_temp.setHorizontalAlignment(Cell.ALIGN_CENTER);
			table.addCell(_temp);
			table.addCell(new Cell(new Phrase("�acznie Dokument�w :"+(listaDocumentowNieokreslonych.size() + 1) + "", font)));
			table.addCell(new Cell(new Phrase("Numery Ko pism:  ", font)));
			StringBuilder sb = new StringBuilder();
			for (OutOfficeDocument doc : this.listaDocumentowNieokreslonych)
			{
				sb.append(doc.getOfficeNumber());
				sb.append(", ");
			}
			_temp = new Cell(new Phrase(sb.toString(), font));
			_temp.setColspan(5);
			_temp.setHorizontalAlignment(Cell.ALIGN_CENTER);
			table.addCell(_temp);



		}
		private void fillPdf(OutOfficeDocument document, boolean listyKrajowe, boolean listEkonomiczny, Document pdfDoc) throws EdmException,
				BadElementException
		{
			Integer rodzajGabaryt = 0;
			int countRecipients = 0;
			for (Iterator iter = document.getRecipients().iterator(); iter.hasNext();)
			{
				iter.next();
				rodzajGabaryt = document.getGauge();

				if (rodzajGabaryt == null)
				{
					nieOkreslony++;
					listaDocumentowNieokreslonych.add(document);
					continue;
				} else if (rodzajGabaryt == 1)
				{
					countGabarytA++;
					sumOfGabA = sumOfGabA.add(document.getStampFee());
					countRecipients++;

				} else if (rodzajGabaryt == 2)
				{
					countGabarytB++;
					sumOfGabB = sumOfGabB.add(document.getStampFee());
					countRecipients++;
				} else if (rodzajGabaryt == 3)
				{
					countGabarytC++;
					sumOfGabC = sumOfGabC.add(document.getStampFee());
					countRecipients++;
				} else if (rodzajGabaryt == 4)
				{
					countGabarytD++;
					sumOfGabD = sumOfGabD.add(document.getStampFee());
					countRecipients++;

				} else
				{
					nieOkreslony++;
					listaDocumentowNieokreslonych.add(document);
				}
				document.getDocumentKind().logic()
						.addWeightAndCostsLettersSelectedByValues(document, rodzajGabaryt, listyKrajowe, ListEkonomiczny, mapaKosztow, mapaRodzajowWag,
								tempIloscPoszczegolnychWag);
			}
		}


		private void addResulstRows() throws BadElementException
		{
			for (Integer key : mapaRodzajowWag.keySet())
			{
				Cell _temp;
				_temp = new Cell(new Phrase(this.mapaRodzajowWag.get(key)));
				table.addCell(_temp);
				for (Integer gabaryt : mapaKosztow.keySet())
				{
					String ilosc = this.tempIloscPoszczegolnychWag.get(gabaryt).get(key).toString();
					BigDecimal kwota = this.mapaKosztow.get(gabaryt).get(key);
					BigDecimal i = BigDecimal.valueOf(Long.valueOf(ilosc));
					String oplata = "";
					if (kwota.doubleValue() > 0 && i.doubleValue() > 0)
					{
						oplata = kwota.divide(i, new MathContext(4)).toString();
					}
					_temp = new Cell(new Phrase(this.tempIloscPoszczegolnychWag.get(gabaryt).get(key).toString(), font));
					table.addCell(_temp);
					_temp = new Cell(new Phrase(oplata, font));
					table.addCell(_temp);
					_temp = new Cell(new Phrase(this.mapaKosztow.get(gabaryt).get(key).toString(), font));
					table.addCell(_temp);
				}
			}
		}


		private void addSummaryRow() throws BadElementException
		{
			Cell _temp;
			_temp = new Cell(new Phrase("Razem", font));
			table.addCell(_temp);

			//gab A
			table.addCell(new Cell(new Phrase(this.countGabarytA.toString(), font)));
			table.addCell(new Cell(""));
			table.addCell(new Cell(new Phrase(this.sumOfGabA.toString(), font)));
			//gab B
			table.addCell(new Cell(new Phrase(this.countGabarytB.toString(), font)));
			table.addCell(new Cell(""));
			table.addCell(new Cell(new Phrase(this.sumOfGabB.toString(), font)));

		}


		private void cleanCounts()
		{
			this.countGabarytA = 0;
			this.countGabarytB = 0;
			this.countGabarytC = 0;
			this.countGabarytD = 0;
			this.sumOfGabA = new BigDecimal(0d);
			this.sumOfGabB = new BigDecimal(0d);
			this.sumOfGabC = new BigDecimal(0d);
			this.sumOfGabD = new BigDecimal(0d);
		}


		private void generujMapyDlaPismKrajowyhEkonomicznych()
		{
			listyKrajowe = true;
			ListEkonomiczny = true;
			mapaRodzajowWag = getRodzajWagKraj();
			mapaKosztow = getMapakosztow(listyKrajowe);
			tempIloscPoszczegolnychWag = getIloscPoszczegolnychWag(listyKrajowe);
		}

		private void generujMapyDlaPismKrajowyhPriorytetowych()
		{
			listyKrajowe = true;
			ListEkonomiczny = false;
			mapaRodzajowWag = getRodzajWagKraj();
			mapaKosztow = getMapakosztow(listyKrajowe);
			tempIloscPoszczegolnychWag = getIloscPoszczegolnychWag(listyKrajowe);
		}

		private void generujMapyDlaPismSwiatowych()
		{
			listyKrajowe = false;
			ListEkonomiczny = false;
			mapaRodzajowWag = getRodzajWagSwiat();
			mapaKosztow = getMapakosztow(listyKrajowe);
			tempIloscPoszczegolnychWag = getIloscPoszczegolnychWag(listyKrajowe);
		}



		private HashMap<Integer, Map<Integer, Integer>> getIloscPoszczegolnychWag(boolean listyKrajowe)
		{
			if (listyKrajowe)
			{
				tempIloscPoszczegolnychWag.put(1, getIloscWagaKraj());
				tempIloscPoszczegolnychWag.put(2, getIloscWagaKraj());
			} else
			{
				tempIloscPoszczegolnychWag.put(1, getIloscWagaSwiat());
				tempIloscPoszczegolnychWag.put(2, getIloscWagaSwiat());
				tempIloscPoszczegolnychWag.put(3, getIloscWagaSwiat());
				tempIloscPoszczegolnychWag.put(4, getIloscWagaSwiat());
			}

			return tempIloscPoszczegolnychWag;
		}


		private Map<Integer, Integer> getIloscWagaKraj()
		{

			Map<Integer, Integer> mapa = new HashMap();
			mapa.put(1, 0);
			mapa.put(2, 0);
			mapa.put(3, 0);

			return mapa;

		}

		private Map<Integer, Integer> getIloscWagaSwiat()
		{

			Map<Integer, Integer> mapa = new HashMap();
			mapa.put(1, 0);
			mapa.put(2, 0);
			mapa.put(3, 0);
			mapa.put(4, 0);
			mapa.put(5, 0);
			mapa.put(6, 0);

			return mapa;
		}



		private void prepareSortetDocumentsByTypeAndCountry(List<OutOfficeDocument> documents)
		{
			for (OutOfficeDocument document : documents)
			{

				if (document.getCountryLetter() != null)
				{
					if (document.getCountryLetter())
						listaDokumentowKraj.add(document);
					else
						listaDokumentowZagranicznych.add(document);
				} else
				{
					listaDocumentowNieokreslonych.add(document);
				}

			}

			for (OutOfficeDocument document : listaDokumentowKraj)
			{
				if (document.getPriority() != null)
				{
					if (document.getPriority())
						listaDokumentowPriorytetowychKraj.add(document);
					else
						listaDokumentowEkonomicznychKraj.add(document);
				} else
				{
					listaDocumentowNieokreslonych.add(document);
				}
			}

			for (OutOfficeDocument document : listaDokumentowZagranicznych)
			{
				if (document.getPriority() != null)
				{
					if (document.getPriority())
						listaDokumentowPriorytetowychZagranicznych.add(document);
					else
						listaDokumentowEkonomicznychZagranicznych.add(document);
				} else
				{
					listaDocumentowNieokreslonych.add(document);
				}
			}
		}


		private Map<Integer, String> getRodzajWagKraj()
		{
			Map<Integer, String> mapa = new HashMap();
			mapa.put(1, "Do 350 gram ");
			mapa.put(2, "Do 1000 gram ");
			mapa.put(3, "Do 2000 gram ");

			return mapa;
		}

		private Map<Integer, String> getRodzajWagSwiat()
		{
			Map<Integer, String> mapa = new HashMap();
			mapa.put(1, "Do 50 gram ");
			mapa.put(2, "Do 100 gram ");
			mapa.put(3, "Do 350 gram ");
			mapa.put(4, "Do 500 gram ");
			mapa.put(5, "Do 1000 gram ");
			mapa.put(6, "Do 2000 gram ");

			return mapa;
		}

		private HashMap<Integer, Map<Integer, BigDecimal>> getMapakosztow(boolean listyKrajowe)
		{

			if (listyKrajowe)
			{
				mapaKosztow.put(1, getMapakosztowKraj());
				mapaKosztow.put(2, getMapakosztowKraj());

			} else
			{
				mapaKosztow.put(1, getMapakosztowSwiat());
				mapaKosztow.put(2, getMapakosztowSwiat());
				mapaKosztow.put(3, getMapakosztowSwiat());
				mapaKosztow.put(4, getMapakosztowSwiat());

			}
			return mapaKosztow;
		}



		private Map<Integer, BigDecimal> getTempMapKosztowSwiat()
		{
			Map<Integer, BigDecimal> temp = new HashMap<Integer, BigDecimal>();
			temp.put(1, BigDecimal.ZERO);
			temp.put(2, BigDecimal.ZERO);
			temp.put(3, BigDecimal.ZERO);
			temp.put(4, BigDecimal.ZERO);
			temp.put(5, BigDecimal.ZERO);
			temp.put(6, BigDecimal.ZERO);
			return temp;
		}

		private Map<Integer, BigDecimal> getMapakosztowSwiat()
		{
			Map<Integer, BigDecimal> mapa = new HashMap();
			mapa.put(1, BigDecimal.ZERO);
			mapa.put(2, BigDecimal.ZERO);
			mapa.put(3, BigDecimal.ZERO);
			mapa.put(4, BigDecimal.ZERO);
			mapa.put(5, BigDecimal.ZERO);
			mapa.put(6, BigDecimal.ZERO);
			return mapa;
		}


		private Map<Integer, BigDecimal> getMapakosztowKraj()
		{

			Map<Integer, BigDecimal> mapa = new HashMap();
			mapa.put(1, BigDecimal.ZERO);
			mapa.put(2, BigDecimal.ZERO);
			mapa.put(3, BigDecimal.ZERO);

			return mapa;

		}
	}

	private Table buildTableHeaderIlosciowy(Font font, float[] widths, Table table , boolean isHeader , String rodzajListu) throws BadElementException {
		
		if (isHeader){
		table.setWidths(widths);
		table.setWidth(100.0f);
		table.setCellsFitPage(true);
		table.setPadding(2);
		}
		//7 kolumn
		//pierwszy wiersz
		Cell _up = new Cell(new Phrase(sm.getString("PrzesylkiListoweNieRejestrowane"), font));
		_up.setColspan(widths.length);
		_up.setRowspan(1);
		_up.setHorizontalAlignment(Cell.ALIGN_CENTER);
		table.addCell(_up);
		
		//2 wiersz  
		//1 columna
		Cell _gab = new Cell(new Phrase(sm.getString("przedzialWagowy"), font));
		_gab.setRowspan(3);
		_gab.setHorizontalAlignment(Cell.ALIGN_CENTER);
		table.addCell(_gab);
		//2 cloumna 
		Cell _rodz = new Cell(new Phrase(rodzajListu, font));
		_rodz.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_rodz.setColspan(6);
		_rodz.setRowspan(1);
		table.addCell(_rodz);
        //3 wiersz
		//2-4columna 
		Cell _gabA = new Cell(new Phrase(sm.getString("gabA"), font));
		_gabA.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_gabA.setColspan(3);
		_gabA.setRowspan(1);
		table.addCell(_gabA);
		//5-7columna 
		Cell _gabB = new Cell(new Phrase(sm.getString("gabB"), font));
		_gabB.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_gabB.setColspan(3);
		_gabB.setRowspan(1);
		table.addCell(_gabB);
		//4 wiersz
		//2columna 
		Cell _liczbA = new Cell(new Phrase("Liczba", font));
		_liczbA.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_liczbA.setColspan(1);
		_liczbA.setRowspan(1);
		table.addCell(_liczbA);
		
		//3columna 
		Cell _cenaJednostkowaA = new Cell(new Phrase(sm.getString("CenaJednostkowa"), font));
		_cenaJednostkowaA.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_cenaJednostkowaA.setColspan(1);
		_cenaJednostkowaA.setRowspan(1);
		table.addCell(_cenaJednostkowaA);
		//4columna 
		Cell _cenaLacznaA = new Cell(new Phrase(sm.getString("LacznaWartosc"), font));
		_cenaLacznaA.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_cenaLacznaA.setColspan(1);
		_cenaLacznaA.setRowspan(1);
		table.addCell(_cenaLacznaA);
		//5columna
		Cell _liczbB = new Cell(new Phrase("Liczba", font));
		_liczbB.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_liczbB.setColspan(1);
		_liczbB.setRowspan(1);
		table.addCell(_liczbB);
		
		//6columna 
		Cell _cenaJednostkowaB = new Cell(new Phrase(sm.getString("CenaJednostkowa"), font));
		_cenaJednostkowaB.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_cenaJednostkowaB.setColspan(1);
		_cenaJednostkowaB.setRowspan(1);
		table.addCell(_cenaJednostkowaB);
		//7columna 
		Cell _cenaLacznaB = new Cell(new Phrase(sm.getString("LacznaWartosc"), font));
		_cenaLacznaB.setHorizontalAlignment(Cell.ALIGN_CENTER);
		_cenaLacznaB.setColspan(1);
		_cenaLacznaB.setRowspan(1);
		table.addCell(_cenaLacznaB);
		if(isHeader)
		table.endHeaders();
		return table;
	}

    
    private void addPageNumberOfTotalPagesNumber(Font font, Document pdfDoc, int pagesCount) throws DocumentException
    {
        int pageNumber = pdfDoc.getPageNumber();
        String msg = "Strona " + ++pageNumber + " z " + pagesCount + ".";
        Phrase phrase = new Phrase(msg, font);
        Paragraph p = new Paragraph(phrase);
        pdfDoc.add(p);
    }

    private int calculatePageNumber(List results)
    {
        int pagesCount = 0;
        if (results.size() <= 7)
            pagesCount = 1;
        else
        {
            Float a =  (float)results.size()/7f;
            String aa = a.toString().substring(0, a.toString().indexOf('.'));
            String bb = a.toString().substring(a.toString().indexOf('.')+1);
            pagesCount = Integer.valueOf(aa);
            int bbb = Integer.valueOf(bb);

            if (bbb > 0)
                ++pagesCount;
        }
        return pagesCount;
    }

    private void setSenderForP4(Journal journal, BaseFont baseFont, Document pdfDoc) throws DocumentException
    {
        String nadawca =  "";
        if (journal.getOwnerGuid().equals(P4Helper.MINERALNA_GUID))
            nadawca = Docusafe.getAdditionProperty("p4.nadawca.mineralna");
        else
            nadawca = Docusafe.getAdditionProperty("p4.nadawca.tasmowa");

        Table nadawcaTable = new Table(2);
        nadawcaTable.setPadding(2);
        nadawcaTable.setWidth(100);
        float [] c = {3f ,0.7f};
        nadawcaTable.setWidths(c);
        nadawcaTable.setBorder(Table.NO_BORDER);

        Font nadawcaFont = new Font(baseFont, 18);
        nadawcaFont.setStyle("bold");
        Cell _nadawca = new Cell(new Phrase(nadawca, nadawcaFont));
        _nadawca.setVerticalAlignment(Cell.ALIGN_MIDDLE);
        _nadawca.setHorizontalAlignment(Cell.ALIGN_CENTER);
        nadawcaTable.addCell(_nadawca);

        String taxMsg = "OP�ATA POBRANA\n" +
                "TAXE PER�UE- POLOGNE\n" +
                "Umowa Nr 1955/CP RH13/2011/D-PF\n" +
                "z Poczt� Polsk� S.A.\n" +
                "z dnia 14.11.2011r. Nadano w UP Warszawa 22";

        Font taxFont = new Font(baseFont, 6);
        nadawcaTable.addCell(new Phrase(taxMsg, taxFont));
        pdfDoc.add(nadawcaTable);
    }

    public void setExtended(boolean extended)
    {
        this.extended = extended;
    }

    public PdfPTable getHeaderTable(int x, int y) {
        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(527);
        table.setLockedWidth(true);
        table.getDefaultCell().setFixedHeight(20);
        table.getDefaultCell().setBorder(Rectangle.BOTTOM);
        table.addCell("FOOBAR FILMFESTIVAL");
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(String.format("Page %d of %d", x, y));
        return table;
    }

    /**
     *    Klasa generuj�ca cztery rodzaje zestawie� ilo�ciowo-warto�ciowych przesy�ek na potrzeby UEK.
     *    1. Przesy�ki krajowe do 50g
     *    2. Przesy�ki krajowe powy�ej 50g
     *    3. Przesy�ki zagraniczne do 50g
     *    4. Przesy�ki zagraniczne powy�ej 50g
     */
    private class Zestawienie implements ActionListener{

    	public static final String PRIORYTETOWE = "PRIORYTETOWE";
        public static final String EKONOMICZNE = "EKONOMICZNE";
        public static final String PRIORYTETOWE_NIEREJESTROWANE = "PRIORYTETOWE_NIEREJESTROWANE";
        public static final String PRIORYTETOWE_POLECONE = "PRIORYTETOWE_POLECONE";
        public static final String EKONOMICZNE_NIEREJESTROWANE = "EKONOMICZNE_NIEREJESTROWANE";
        public static final String EKONOMICZNE_POLECONE = "EKONOMICZNE_POLECONE";
        public static final String REKLAMOWE = "REKLAMOWE";
        private List<OutOfficeDocument> allDocuments = new ArrayList();
        private List<OutOfficeDocument> listaDokumentowKrajowych = new ArrayList();
        private List<OutOfficeDocument> listaDokumentowKrajowychPoleconych = new ArrayList();
        private List<OutOfficeDocument> listaDokumentowZagranicznych = new ArrayList();

        public HashMap<Integer, Map<Integer, Integer>> iloscPoszczegolnychWag = new HashMap<Integer, Map<Integer, Integer>>();
        private Map<Integer, String> mapaWagi = new HashMap();
        private com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> tabelaIlosci;
        private com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> tabelaKosztow;
        private Integer countEcoQuantity = 0, countPriorQuantity = 0, countAdQuantity = 0;
        private Integer countEcoQuantityPolecony = 0, countPriorQuantityPolecony = 0, countAdQuantityPolecony = 0;
        private BigDecimal sumEcoCost = BigDecimal.ZERO, sumPriorCost = BigDecimal.ZERO, sumAdCost = BigDecimal.ZERO;
        private BigDecimal sumEcoCostPolecony = BigDecimal.ZERO, sumPriorCostPolecony = BigDecimal.ZERO, sumAdCostPolecony = BigDecimal.ZERO;
        private Integer countZoneA = 0, countZoneB = 0, countZoneC = 0, countZoneD = 0, countEur = 0, countEurOut = 0;
        private BigDecimal sumZoneA = BigDecimal.ZERO, sumZoneB = BigDecimal.ZERO, sumZoneC = BigDecimal.ZERO, sumZoneD = BigDecimal.ZERO, sumEur = BigDecimal.ZERO, sumEurOut = BigDecimal.ZERO;
        
        public final Map<String, BigDecimal> CENY_JEDNOSTKOWE = ImmutableMap.<String, BigDecimal>builder()
                .put("EKONOMICZNE_NIEREJESTROWANE_1_6", new BigDecimal(1.60))
                .put("EKONOMICZNE_NIEREJESTROWANE_1_7", new BigDecimal(3.70))
                .put("EKONOMICZNE_NIEREJESTROWANE_1_8", new BigDecimal(6.30))
                .put("EKONOMICZNE_NIEREJESTROWANE_2_6", new BigDecimal(3.75))
                .put("EKONOMICZNE_NIEREJESTROWANE_2_7", new BigDecimal(4.75))
                .put("EKONOMICZNE_NIEREJESTROWANE_2_8", new BigDecimal(7.30))
                .put("PRIORYTETOWE_NIEREJESTROWANE_1_6", new BigDecimal(2.25))
                .put("PRIORYTETOWE_NIEREJESTROWANE_1_7", new BigDecimal(4.50))
                .put("PRIORYTETOWE_NIEREJESTROWANE_1_8", new BigDecimal(8.80))
                .put("PRIORYTETOWE_NIEREJESTROWANE_2_6", new BigDecimal(5.10))
                .put("PRIORYTETOWE_NIEREJESTROWANE_2_7", new BigDecimal(7.10))
                .put("PRIORYTETOWE_NIEREJESTROWANE_2_8", new BigDecimal(10.90))
                /*.put("EKONOMICZNE_POLECONE_1_6", new BigDecimal(3.80))
                .put("EKONOMICZNE_POLECONE_1_7", new BigDecimal(5.90))
                .put("EKONOMICZNE_POLECONE_1_8", new BigDecimal(8.50))
                .put("EKONOMICZNE_POLECONE_2_6", new BigDecimal(7.50))
                .put("EKONOMICZNE_POLECONE_2_7", new BigDecimal(8.30))
                .put("EKONOMICZNE_POLECONE_2_8", new BigDecimal(9.50))
                .put("PRIORYTETOWE_POLECONE_1_6", new BigDecimal(4.55))
                .put("PRIORYTETOWE_POLECONE_1_7", new BigDecimal(7.20))
                .put("PRIORYTETOWE_POLECONE_1_8", new BigDecimal(11.00))
                .put("PRIORYTETOWE_POLECONE_2_6", new BigDecimal(8.30))
                .put("PRIORYTETOWE_POLECONE_2_7", new BigDecimal(11.00))
                .put("PRIORYTETOWE_POLECONE_2_8", new BigDecimal(14.50))*/
                .build();
        
        @Override
        public void actionPerformed(ActionEvent event) {
            File temp = null;
            try {
                Journal journal = Journal.find(getId());
                if (!journal.isOutgoing()) {
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));
                }

                List results = find(journal);

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                        BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font10 = new Font(baseFont, 10);
                Font font8 = new Font(baseFont, 7);
                //Font font10Bold = new Font(baseFont, 10, Font.BOLD);
                Font font10Bold = new Font(baseFont, 10);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc;
                if (zagraniczny) {
                    pdfDoc = new com.lowagie.text.Document(PageSize.A4.rotate());
                } else {
                    pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                }
                FileOutputStream fis = new FileOutputStream(temp);
                PdfWriter.getInstance(pdfDoc, fis);

                HeaderFooter footer =
                        new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                                DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font10), new Phrase("."));
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);

                pdfDoc.open();

                Paragraph p1 = new Paragraph(new Phrase("Za��cznik nr 3 do umowy nr ...................................", font10));
                p1.setAlignment(Element.ALIGN_RIGHT);
                Paragraph p2 = new Paragraph(new Phrase(sm.getString("ZestawieniaTitle") + " w obrocie krajowym nadanych w dniu ............", font10));
                p2.setAlignment(Element.ALIGN_CENTER);
                Paragraph p3 = new Paragraph(new Phrase("Nazwa firmy       ..............................", font10));
                p2.setAlignment(Element.ALIGN_LEFT);
                Paragraph p4 = new Paragraph(new Phrase("Adres                  ..............................", font10));
                p2.setAlignment(Element.ALIGN_LEFT);
                StringBuilder sb = new StringBuilder();
                sb.append("Forma op�aty za przesy�ki ....................../Umowa nr .............. z dnia ")
                .append(new Date().toLocaleString().split(" ")[0]);
                Paragraph p5 = new Paragraph(new Phrase(sb.toString(), font10));
                p2.setAlignment(Element.ALIGN_LEFT);

                pdfDoc.add(p1);
                pdfDoc.add(p2);
                pdfDoc.add(p3);
                pdfDoc.add(p4);
                pdfDoc.add(p5);

                Table headerNational = null;
                Table headerForeign = null;
                Table headerAdvertisment = null;

                float[] widths;
                if (krajowy) {
                    widths = new float[]{4.2f, 1.9f, 1.9f, 1.9f, 1.9f, 1.9f, 1.9f};
                    headerNational = new Table(widths.length);
                    buildTableHeaderNational(font10, font8, widths, headerNational);
                } else {
                    widths = new float[]{4.5f, 2.3f, 2.0f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f};
                    headerForeign = new Table(widths.length);
                    buildTableHeaderForeign(font10, font8, widths, headerForeign);
                }

                for (int i = 0; i < results.size(); i++)
                {
                    final Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
                    final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(bean.getDocumentId());
                    allDocuments.add(document);
                }

                sortDocumentsByWeightAndCountry(allDocuments);

                if (krajowy) {
                    generujMapyDlaKrajowych();
                    fillMapsNational(listaDokumentowKrajowych);
                    //fillMapsNationalPolecony(listaDokumentowKrajowychPoleconych);
                    sumQuantityAndCostNational();
                    addMapsAndSummaryToTable(headerNational, false, font10, font8, font10Bold);
                    pdfDoc.add(headerNational);
                } else if (zagraniczny) {
                    generujMapyDlaZagranicznych();
                    fillMapsForeign(listaDokumentowZagranicznych);
                    sumQuantityAndCostForeign();
                    fillRowsForeign(headerForeign, font10, font8, font10Bold);
                    pdfDoc.add(headerForeign);
                }
                pdfDoc.close();

                showPDF(temp);

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        private void addMapsAndSummaryToTable(Table table, boolean reklamowe, Font font1, Font font2, Font font3) throws BadElementException {
        	fillRows(table, 1, "GABARYT A", reklamowe, font1, font2, font3);
            fillRows(table, 2, "GABARYT B", reklamowe, font1, font2, font3);
            
            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantity), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCost.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantity), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCost.setScale(2)), font1))));
            
            /*table.addCell(new Cell(""));
            Cell headerCell = new Cell(new Phrase(sm.get("PrzesylkiListowePolecone"), font1));
            headerCell.setColspan(6);
            headerCell.setRowspan(1);
            headerCell.setHorizontalAlignment(Cell.ALIGN_CENTER);
    		table.addCell(headerCell);
    		
    		fillRowsPolecone(table, 1, "GABARYT A", reklamowe, font1, font2, font3);
            fillRowsPolecone(table, 2, "GABARYT B", reklamowe, font1, font2, font3);
            
            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantityPolecony), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCostPolecony.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantityPolecony), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCostPolecony.setScale(2)), font1))));*/
            
            StringBuilder sb = new StringBuilder();
            sb.append("Og�em:  ").append(sumEcoCost.add(sumPriorCost).setScale(2, RoundingMode.HALF_EVEN)).append("  ");
            Cell lastCell = new Cell(new Phrase(sb.toString(), font3));
            lastCell.setColspan(7);
            lastCell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
            table.addCell(lastCell);
        }
        
        private void fillRows(Table table, int gabaryt, String title, boolean reklamowe, Font font1, Font font2, Font font3) throws BadElementException {
            Cell temp;
            temp = new Cell(new Phrase(title, font3));
            temp.setHorizontalAlignment(Element.ALIGN_LEFT);
            temp.setColspan(7);
            table.addCell(temp);

            Integer iloscEko = 0;
            Integer iloscPrior = 0;
            BigDecimal kosztEko = BigDecimal.ZERO;
            BigDecimal kosztPrior = BigDecimal.ZERO;
            for (Integer waga : mapaWagi.keySet()) {
                iloscEko = tabelaIlosci.get(EKONOMICZNE_NIEREJESTROWANE, gabaryt).get(waga);
                kosztEko = tabelaKosztow.get(EKONOMICZNE_NIEREJESTROWANE, gabaryt).get(waga);
                iloscPrior = tabelaIlosci.get(PRIORYTETOWE_NIEREJESTROWANE, gabaryt).get(waga);
                kosztPrior = tabelaKosztow.get(PRIORYTETOWE_NIEREJESTROWANE, gabaryt).get(waga);

                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEko == 0 ? "" : iloscEko), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(CENY_JEDNOSTKOWE.get(EKONOMICZNE_NIEREJESTROWANE + "_" + gabaryt + "_" + waga).setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEko.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEko.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscPrior == 0 ? "" : iloscPrior), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(CENY_JEDNOSTKOWE.get(PRIORYTETOWE_NIEREJESTROWANE + "_" + gabaryt + "_" + waga).setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztPrior.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztPrior.setScale(2)), font1));
                table.addCell(temp);
            }
        }
        
        private void fillRowsPolecone(Table table, int gabaryt, String title, boolean reklamowe, Font font1, Font font2, Font font3) throws BadElementException {
            Cell temp;
            temp = new Cell(new Phrase(title, font3));
            temp.setHorizontalAlignment(Element.ALIGN_LEFT);
            temp.setColspan(7);
            table.addCell(temp);

            Integer iloscEko = 0;
            Integer iloscPrior = 0;
            BigDecimal kosztEko = BigDecimal.ZERO;
            BigDecimal kosztPrior = BigDecimal.ZERO;
            for (Integer waga : mapaWagi.keySet()) {
                iloscEko = tabelaIlosci.get(EKONOMICZNE_POLECONE, gabaryt).get(waga);
                kosztEko = tabelaKosztow.get(EKONOMICZNE_POLECONE, gabaryt).get(waga);
                iloscPrior = tabelaIlosci.get(PRIORYTETOWE_POLECONE, gabaryt).get(waga);
                kosztPrior = tabelaKosztow.get(PRIORYTETOWE_POLECONE, gabaryt).get(waga);

                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEko == 0 ? "" : iloscEko), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(CENY_JEDNOSTKOWE.get(EKONOMICZNE_POLECONE + "_" + gabaryt + "_" + waga).setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEko.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEko.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscPrior == 0 ? "" : iloscPrior), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(CENY_JEDNOSTKOWE.get(PRIORYTETOWE_POLECONE + "_" + gabaryt + "_" + waga).setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztPrior.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztPrior.setScale(2)), font1));
                table.addCell(temp);
            }
        }

        private void fillRowsForeign(Table table, Font font1, Font font2, Font font3) throws BadElementException {
            Cell temp;

            Integer iloscA;
            Integer iloscB;
            Integer iloscC;
            Integer iloscD;
            Integer iloscEur;
            Integer iloscEurOut;
            BigDecimal kosztA;
            BigDecimal kosztB;
            BigDecimal kosztC;
            BigDecimal kosztD;
            BigDecimal kosztEur;
            BigDecimal kosztEurOut;
            for (Integer waga : mapaWagi.keySet()) {
                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);

                iloscA = tabelaIlosci.get(PRIORYTETOWE, 3).get(waga);
                iloscB = tabelaIlosci.get(PRIORYTETOWE, 4).get(waga);
                iloscC = tabelaIlosci.get(PRIORYTETOWE, 5).get(waga);
                iloscD = tabelaIlosci.get(PRIORYTETOWE, 6).get(waga);
                iloscEur = tabelaIlosci.get(EKONOMICZNE, 7).get(waga);
                iloscEurOut = tabelaIlosci.get(EKONOMICZNE, 8).get(waga);
                kosztA = tabelaKosztow.get(PRIORYTETOWE, 3).get(waga);
                kosztB = tabelaKosztow.get(PRIORYTETOWE, 4).get(waga);
                kosztC = tabelaKosztow.get(PRIORYTETOWE, 5).get(waga);
                kosztD = tabelaKosztow.get(PRIORYTETOWE, 6).get(waga);
                kosztEur = tabelaKosztow.get(EKONOMICZNE, 7).get(waga);
                kosztEurOut = tabelaKosztow.get(EKONOMICZNE, 8).get(waga);

                temp = new Cell(new Phrase(String.valueOf(iloscEur == 0 ? "" : iloscEur), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEur.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEur.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEurOut == 0 ? "" : iloscEurOut), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEurOut.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEurOut.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscA == 0 ? "" : iloscA), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztA.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztA.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscB == 0 ? "" : iloscB), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztB.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztB.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscC == 0 ? "" : iloscC), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztC.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztC.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscD == 0 ? "" : iloscD), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztD.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztD.setScale(2)), font1));
                table.addCell(temp);
            }
            
            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEur), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEur.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countEurOut), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEurOut.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneA), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneA.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneB), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneB.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneC), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneC.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneD), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneD.setScale(2)), font1))));
        }

        private void fillMapsNational(List<OutOfficeDocument> listaDokumentow) {
            for (OutOfficeDocument doc : listaDokumentow) {
                Integer mailWeight = doc.getMailWeight();
                Integer gauge = doc.getGauge();
                String priority;
                if (doc.getDelivery().getId() == DeliveryCostUek.KRAJOWY_ZWYKLY_EKO) {
                    priority = EKONOMICZNE_NIEREJESTROWANE;
                    if(priority != null && gauge != null) {
                    	tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getId() == DeliveryCostUek.KRAJOWY_ZWYKLY_PRIO) {
                	priority = PRIORYTETOWE_NIEREJESTROWANE;
                	 if(priority != null && gauge != null) {
                     	tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                     }
                }
            }
        }
        
        private void fillMapsNationalPolecony(List<OutOfficeDocument> listaDokumentow) {
            for (OutOfficeDocument doc : listaDokumentow) {
                Integer mailWeight = doc.getMailWeight();
                Integer gauge = doc.getGauge();
                String priority;
                if (doc.getDelivery().getId() == DeliveryCostUek.KRAJOWY_POLECONY_EKO) {
                    priority = EKONOMICZNE_POLECONE;
                    if(priority != null && gauge != null) {
                    	tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getId() == DeliveryCostUek.KRAJOWY_POLECONY_PRIO) {
                	priority = PRIORYTETOWE_POLECONE;
                	 if(priority != null && gauge != null) {
                     	tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                     }
                }
               
                
            }
        }

        private void fillMapsForeign(List<OutOfficeDocument> listaDokumentow) throws EdmException {
            for (OutOfficeDocument doc : listaDokumentow) {
                Integer mailWeight = doc.getMailWeight();
                Integer zone = doc.getFieldsManager().getEnumItem(NormalLogic.DOC_MAIL_ZONE).getId();
                
                if (doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_EKO) {
                	String priority = EKONOMICZNE;
                    if(priority != null && zone != null) {
    	                tabelaIlosci.get(priority, zone).put(mailWeight, tabelaIlosci.get(priority, zone).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
    	                tabelaKosztow.get(priority, zone).put(mailWeight, tabelaKosztow.get(priority, zone).get(mailWeight).add(doc.getStampFee()));
                    } 
                } else if (doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_POLECONY_PRIO || doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_PRIO) {
                	String priority = PRIORYTETOWE;
                    if(priority != null && zone != null) {
    	                tabelaIlosci.get(priority, zone).put(mailWeight, tabelaIlosci.get(priority, zone).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
    	                tabelaKosztow.get(priority, zone).put(mailWeight, tabelaKosztow.get(priority, zone).get(mailWeight).add(doc.getStampFee()));
                    } 
                }
                
                
            }
        }

        private void sumQuantityAndCostNational() {
            for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantity += map.get(weight);
                }
            }
            /*for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantityPolecony += map.get(weight);
                }
            }*/
            for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantity += map.get(weight);
                }
            }
            /*for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantityPolecony += map.get(weight);
                }
            }*/
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCost = sumEcoCost.add(map.get(weight));
                }
            }
            /*for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCostPolecony = sumEcoCostPolecony.add(map.get(weight));
                }
            }*/
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCost = sumPriorCost.add(map.get(weight));
                }
            }
            /*for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCostPolecony = sumPriorCostPolecony.add(map.get(weight));
                }
            }*/
        }

        private void sumQuantityAndCostForeign() {

            Map<Integer, Integer> strefaA = tabelaIlosci.get(PRIORYTETOWE, 3);
            Map<Integer, Integer> strefaB = tabelaIlosci.get(PRIORYTETOWE, 4);
            Map<Integer, Integer> strefaC = tabelaIlosci.get(PRIORYTETOWE, 5);
            Map<Integer, Integer> strefaD = tabelaIlosci.get(PRIORYTETOWE, 6);
            Map<Integer, Integer> strefaEur = tabelaIlosci.get(EKONOMICZNE, 7);
            Map<Integer, Integer> strefaEurOut = tabelaIlosci.get(EKONOMICZNE, 8);

            for (Integer weight : strefaA.keySet()) {
                countZoneA += strefaA.get(weight);
            }
            for (Integer weight : strefaB.keySet()) {
                countZoneB += strefaB.get(weight);
            }
            for (Integer weight : strefaC.keySet()) {
                countZoneC += strefaC.get(weight);
            }
            for (Integer weight : strefaD.keySet()) {
                countZoneD += strefaD.get(weight);
            }
            for (Integer weight : strefaEur.keySet()) {
                countEur += strefaEur.get(weight);
            }
            for (Integer weight : strefaEurOut.keySet()) {
                countEurOut += strefaEurOut.get(weight);
            }

            Map<Integer, BigDecimal> strefaACosts = tabelaKosztow.get(PRIORYTETOWE, 3);
            Map<Integer, BigDecimal> strefaBCosts = tabelaKosztow.get(PRIORYTETOWE, 4);
            Map<Integer, BigDecimal> strefaCCosts = tabelaKosztow.get(PRIORYTETOWE, 5);
            Map<Integer, BigDecimal> strefaDCosts = tabelaKosztow.get(PRIORYTETOWE, 6);
            Map<Integer, BigDecimal> strefaEurCosts = tabelaKosztow.get(EKONOMICZNE, 7);
            Map<Integer, BigDecimal> strefaEurOutCosts = tabelaKosztow.get(EKONOMICZNE, 8);

            for (Integer weight : strefaACosts.keySet()) {
                sumZoneA = sumZoneA.add(strefaACosts.get(weight));
            }
            for (Integer weight : strefaBCosts.keySet()) {
                sumZoneB = sumZoneB.add(strefaBCosts.get(weight));
            }
            for (Integer weight : strefaCCosts.keySet()) {
                sumZoneC = sumZoneC.add(strefaCCosts.get(weight));
            }
            for (Integer weight : strefaDCosts.keySet()) {
                sumZoneD = sumZoneD.add(strefaDCosts.get(weight));
            }
            for (Integer weight : strefaEurCosts.keySet()) {
                sumEur = sumEur.add(strefaEurCosts.get(weight));
            }
            for (Integer weight : strefaEurOutCosts.keySet()) {
                sumEurOut = sumEurOut.add(strefaEurOutCosts.get(weight));
            }
        }

        private void generujMapyDlaKrajowych() {
            mapaWagi = getWagi(false);
            tabelaIlosci = getIloscTable(false);
            tabelaKosztow = getKosztTable(false);
        }

        private void generujMapyDlaZagranicznych() {
            mapaWagi = getWagi(true);
            tabelaIlosci = getIloscTable(true);
            tabelaKosztow = getKosztTable(true);
        }

        private com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> getKosztTable(boolean zagraniczne) {
            com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> table = HashBasedTable.create();
            if (zagraniczne) {
                table.put(PRIORYTETOWE, 3, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE, 4, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE, 5, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE, 6, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE, 7, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE, 8, getKosztMap(zagraniczne));
            } else {
                table.put(EKONOMICZNE_NIEREJESTROWANE, 1, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE_NIEREJESTROWANE, 2, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE_NIEREJESTROWANE, 3, getKosztMap(zagraniczne));
                /*table.put(EKONOMICZNE_POLECONE, 1, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE_POLECONE, 2, getKosztMap(zagraniczne));
                table.put(EKONOMICZNE_POLECONE, 3, getKosztMap(zagraniczne));*/
                table.put(PRIORYTETOWE_NIEREJESTROWANE, 1, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE_NIEREJESTROWANE, 2, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE_NIEREJESTROWANE, 3, getKosztMap(zagraniczne));
                /*table.put(PRIORYTETOWE_POLECONE, 1, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE_POLECONE, 2, getKosztMap(zagraniczne));
                table.put(PRIORYTETOWE_POLECONE, 3, getKosztMap(zagraniczne));*/
            }
            return table;
        }

        private com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> getIloscTable(boolean zagraniczne) {
        	//<EKONOMICZNE|PRIORYTETOWE, WIERSZ, WAGI>
            com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> table = HashBasedTable.create();
            if (!zagraniczne) {
            	table.put(EKONOMICZNE_NIEREJESTROWANE, 1, getIloscMap(zagraniczne));
                table.put(EKONOMICZNE_NIEREJESTROWANE, 2, getIloscMap(zagraniczne));
                /*table.put(EKONOMICZNE_POLECONE, 1, getIloscMap(zagraniczne));
                table.put(EKONOMICZNE_POLECONE, 2, getIloscMap(zagraniczne));*/
                table.put(PRIORYTETOWE_NIEREJESTROWANE, 1, getIloscMap(zagraniczne));
                table.put(PRIORYTETOWE_NIEREJESTROWANE, 2, getIloscMap(zagraniczne));
                /*table.put(PRIORYTETOWE_POLECONE, 1, getIloscMap(zagraniczne));
                table.put(PRIORYTETOWE_POLECONE, 2, getIloscMap(zagraniczne));*/
            } else {
            	table.put(PRIORYTETOWE, 3, getIloscMap(zagraniczne));
                table.put(PRIORYTETOWE, 4, getIloscMap(zagraniczne));
                table.put(PRIORYTETOWE, 5, getIloscMap(zagraniczne));
                table.put(PRIORYTETOWE, 6, getIloscMap(zagraniczne));
                table.put(EKONOMICZNE, 7, getIloscMap(zagraniczne));
                table.put(EKONOMICZNE, 8, getIloscMap(zagraniczne));
                
            }
            return table;
        }

        private Map<Integer,BigDecimal> getKosztMap(boolean zagraniczne) {
            Map<Integer, BigDecimal> mapa = new HashMap();

            if (!zagraniczne) {
            	mapa.put(DeliveryCostUek.WEIGTH_0_350, BigDecimal.ZERO);
                mapa.put(DeliveryCostUek.WEIGTH_350_1000, BigDecimal.ZERO);
                mapa.put(DeliveryCostUek.WEIGTH_1000_2000, BigDecimal.ZERO);
            } else {
            	mapa.put(DeliveryCostUek.WEIGTH_0_50, BigDecimal.ZERO);
                mapa.put(DeliveryCostUek.WEIGTH_50_100, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_100_350, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_350_500, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_500_1000, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_1000_2000, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_2000_2500, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_2500_3000, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_3000_3500, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_3500_4000, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_4000_4500, BigDecimal.ZERO);
            	mapa.put(DeliveryCostUek.WEIGTH_4500_5000, BigDecimal.ZERO);
            }
            return mapa;
        }

        private Map<Integer, Integer> getIloscMap(boolean zagraniczne) {
            Map<Integer, Integer> mapa = new HashMap();
            if (!zagraniczne) {
                mapa.put(DeliveryCostUek.WEIGTH_0_350, 0);
                mapa.put(DeliveryCostUek.WEIGTH_350_1000, 0);
                mapa.put(DeliveryCostUek.WEIGTH_1000_2000, 0);
            } else {
            	mapa.put(DeliveryCostUek.WEIGTH_0_50, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_50_100, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_100_350, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_350_500, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_500_1000, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_1000_2000, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_2000_2500, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_2500_3000, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_3000_3500, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_3500_4000, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_4000_4500, 0);
            	mapa.put(DeliveryCostUek.WEIGTH_4500_5000, 0);
            }
            return mapa;
        }

        private Map<Integer, String> getWagi(boolean zagraniczne) {
            Map<Integer, String> mapa = new HashMap();

            if (!zagraniczne) {
                mapa.put(DeliveryCostUek.WEIGTH_0_350, "do 350 g ");
                mapa.put(DeliveryCostUek.WEIGTH_350_1000, "ponad 350 do 1000 g ");
                mapa.put(DeliveryCostUek.WEIGTH_1000_2000, "ponad 1000 do 2000 g ");
            } else {
            	mapa.put(DeliveryCostUek.WEIGTH_0_50, "do 50 g ");
                mapa.put(DeliveryCostUek.WEIGTH_50_100, "ponad 50 do 100 g ");
                mapa.put(DeliveryCostUek.WEIGTH_100_350, "ponad 100 do 350 g ");
                mapa.put(DeliveryCostUek.WEIGTH_350_500, "ponad 350 do 500 g ");
                mapa.put(DeliveryCostUek.WEIGTH_500_1000, "ponad 500 do 1000 g ");
                mapa.put(DeliveryCostUek.WEIGTH_1000_2000, "ponad 1000 do 2000 g ");
                mapa.put(DeliveryCostUek.WEIGTH_2000_2500, "ponad 2000 do 2500 g ");
                mapa.put(DeliveryCostUek.WEIGTH_2500_3000, "ponad 2500 do 3000 g ");
                mapa.put(DeliveryCostUek.WEIGTH_3000_3500, "ponad 3000 do 3500 g ");
                mapa.put(DeliveryCostUek.WEIGTH_3500_4000, "ponad 3500 do 4000 g ");
                mapa.put(DeliveryCostUek.WEIGTH_4000_4500, "ponad 4000 do 4500 g ");
                mapa.put(DeliveryCostUek.WEIGTH_4500_5000, "ponad 4500 do 5000 g ");
            }
            return mapa;
        }

        private void sortDocumentsByWeightAndCountry(List<OutOfficeDocument> allDocuments) {
        		 for (OutOfficeDocument document : allDocuments) {
                     if (document.getDelivery() != null && (document.getDelivery().getId() == DeliveryCostUek.KRAJOWY_ZWYKLY_EKO || document.getDelivery().getId() == DeliveryCostUek.KRAJOWY_ZWYKLY_PRIO)) {
                         if (document.getMailWeight() != null && (document.getMailWeight() == DeliveryCostUek.WEIGTH_0_350 || document.getMailWeight() == DeliveryCostUek.WEIGTH_350_1000 || document.getMailWeight() == DeliveryCostUek.WEIGTH_1000_2000) ){
                             listaDokumentowKrajowych.add(document);
                         } 
                     } else if (document.getDelivery() != null && (document.getDelivery().getId() == DeliveryCostUek.KRAJOWY_POLECONY_EKO || document.getDelivery().getId() == DeliveryCostUek.KRAJOWY_POLECONY_PRIO)) {
                         if (document.getMailWeight() != null && (document.getMailWeight() == DeliveryCostUek.WEIGTH_0_350 || document.getMailWeight() == DeliveryCostUek.WEIGTH_350_1000 || document.getMailWeight() == DeliveryCostUek.WEIGTH_1000_2000) ){
                             listaDokumentowKrajowychPoleconych.add(document);
                         }
                     } else  if (document.getDelivery() != null && (document.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_EKO || document.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_PRIO)) {
                     	if (document.getMailWeight() != null && (document.getMailWeight() != DeliveryCostUek.WEIGTH_0_350 && document.getMailWeight() != DeliveryCostUek.WEIGTH_350_1000 && document.getMailWeight() != DeliveryCostUek.WEIGTH_1000_2000)) {
                             listaDokumentowZagranicznych.add(document);
                         } 
                     }
                 }
        }

        private void buildTableHeaderNational(Font font1, Font font2, float[] widths, Table table) throws BadElementException {
            table.setWidths(widths);
            table.setWidth(100.0f);
            table.setCellsFitPage(true);
            table.setPadding(2);

            //1 wiersz
            //1 kolumna
            Cell przedzialWagowy = new Cell(new Phrase("Przedzia� wagowy", font1));
            przedzialWagowy.setHorizontalAlignment(Cell.ALIGN_CENTER);
            przedzialWagowy.setRowspan(3);
            table.addCell(przedzialWagowy);

            //1 wiersz
            //2 kolumna
            Cell name = new Cell(new Phrase("Przesy�ki listowe nierejestrowane", font1));
            name.setHorizontalAlignment(Cell.ALIGN_CENTER);
            name.setColspan(6);
            table.addCell(name);

            //2 wiersz
            //2 kolumna
            Cell ekon = new Cell(new Phrase("ekonomiczne", font1));
            ekon.setHorizontalAlignment(Cell.ALIGN_CENTER);
            ekon.setColspan(3);
            table.addCell(ekon);

            //2 wiersz
            //3 kolumna
            Cell prior = new Cell(new Phrase("priorytetowe", font1));
            prior.setHorizontalAlignment(Cell.ALIGN_CENTER);
            prior.setColspan(3);
            table.addCell(prior);

            //3 wiersz
            //2 kolumna
            Cell liczba = new Cell(new Phrase("liczba", font2));
            liczba.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba);

            //3 wiersz
            //3 kolumna
            Cell cena = new Cell(new Phrase("cena jednostkowa", font2));
            cena.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cena);

            //3 wiersz
            //4 kolumna
            Cell wartosc = new Cell(new Phrase("��czna warto��", font2));
            wartosc.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc);

            //3 wiersz
            //5 kolumna
            Cell liczba2 = new Cell(new Phrase("liczba", font2));
            liczba2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba2);

            //3 wiersz
            //6 kolumna
            Cell cena2 = new Cell(new Phrase("cena jednostkowa", font2));
            cena2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cena2);

            //3 wiersz
            //7 kolumna
            Cell wartosc2 = new Cell(new Phrase("��czna warto��", font2));
            wartosc2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc2);

            table.endHeaders();
        }

        private void buildTableHeaderForeign(Font font, Font font2, float[] widths, Table table) throws BadElementException {
            table.setWidths(widths);
            table.setWidth(100.0f);
            table.setCellsFitPage(true);
            table.setPadding(2);

            //1 wiersz
            //1 kolumna
            Cell przedzialWagowy = new Cell(new Phrase("Przedzia� wagowy", font));
            przedzialWagowy.setRowspan(4);
            table.addCell(przedzialWagowy);

            //1 wiersz
            //2 kolumna
            Cell name = new Cell(new Phrase("Przesy�ki listowe nierejestrowane", font));
            name.setHorizontalAlignment(Cell.ALIGN_CENTER);
            name.setColspan(12);
            table.addCell(name);

            //2 wiersz
            //2 kolumna
            Cell ekon = new Cell(new Phrase("ekonomiczne", font));
            ekon.setHorizontalAlignment(Cell.ALIGN_CENTER);
            ekon.setColspan(4);
            table.addCell(ekon);

            //2 wiersz
            //3 kolumna
            Cell prior = new Cell(new Phrase("priorytetowe", font));
            prior.setHorizontalAlignment(Cell.ALIGN_CENTER);
            prior.setColspan(8);
            table.addCell(prior);

            //3 wiersz
            //2 kolumna
            Cell eur = new Cell(new Phrase("Kraje europejskie (��cznie z Cyprem, ca�� Rosj� i Izraelem", font2));
            eur.setHorizontalAlignment(Cell.ALIGN_CENTER);
            eur.setColspan(2);
            table.addCell(eur);

            //3 wiersz
            //3 kolumna
            Cell outEur = new Cell(new Phrase("Kraje pozaeuropejskie", font));
            outEur.setHorizontalAlignment(Cell.ALIGN_CENTER);
            outEur.setColspan(2);
            table.addCell(outEur);

            //3 wiersz
            //4 kolumna
            Cell strefaA = new Cell(new Phrase("STREFA \"A\"", font));
            strefaA.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaA.setColspan(2);
            table.addCell(strefaA);

            //3 wiersz
            //5 kolumna
            Cell strefaB = new Cell(new Phrase("STREFA \"B\"", font));
            strefaB.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaB.setColspan(2);
            table.addCell(strefaB);

            //3 wiersz
            //6 kolumna
            Cell strefaC = new Cell(new Phrase("STREFA \"C\"", font));
            strefaC.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaC.setColspan(2);
            table.addCell(strefaC);

            //3 wiersz
            //7 kolumna
            Cell strefaD = new Cell(new Phrase("STREFA \"D\"", font));
            strefaD.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaD.setColspan(2);
            table.addCell(strefaD);

            //4 wiersz
            //2 kolumna
            Cell liczba = new Cell(new Phrase("liczba", font));
            liczba.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba);

            //4 wiersz
            //3 kolumna
            Cell wartosc = new Cell(new Phrase("warto��", font));
            wartosc.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc);

            //4 wiersz
            //4 kolumna
            Cell liczba2 = new Cell(new Phrase("liczba", font));
            liczba2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba2);

            //4 wiersz
            //5 kolumna
            Cell wartosc2 = new Cell(new Phrase("warto��", font));
            wartosc2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc2);

            //4 wiersz
            //6 kolumna
            Cell liczba3 = new Cell(new Phrase("liczba", font));
            liczba3.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba3);

            //4 wiersz
            //7 kolumna
            Cell wartosc3 = new Cell(new Phrase("warto��", font));
            wartosc3.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc3);

            //4 wiersz
            //8 kolumna
            Cell liczba4 = new Cell(new Phrase("liczba", font));
            liczba4.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba4);

            //4 wiersz
            //9 kolumna
            Cell wartosc4 = new Cell(new Phrase("warto��", font));
            wartosc4.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc4);

            //4 wiersz
            //10 kolumna
            Cell liczba5 = new Cell(new Phrase("liczba", font));
            liczba5.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba5);

            //4 wiersz
            //11 kolumna
            Cell wartosc5 = new Cell(new Phrase("warto��", font));
            wartosc5.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc5);

            //4 wiersz
            //12 kolumna
            Cell liczba6 = new Cell(new Phrase("liczba", font));
            liczba6.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba6);

            //4 wiersz
            //13 kolumna
            Cell wartosc6 = new Cell(new Phrase("warto��", font));
            wartosc6.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc6);

            table.endHeaders();
        }

    }

    /**
     *    Klasa generuj�ca cztery rodzaje zestawie� ilo�ciowo-warto�ciowych przesy�ek na potrzeby UEK.
     *    1. Przesy�ki krajowe do 50g
     *    2. Przesy�ki krajowe powy�ej 50g
     *    3. Przesy�ki zagraniczne do 50g
     *    4. Przesy�ki zagraniczne powy�ej 50g
     */
    private class ZestawienieNowe implements ActionListener{

        public static final String PRIORYTETOWE = "PRIORYTETOWE";
        public static final String EKONOMICZNE = "EKONOMICZNE";
        public static final String PRIORYTETOWE_NIEREJESTROWANE = "PRIORYTETOWE_NIEREJESTROWANE";
        public static final String PRIORYTETOWE_POLECONE = "PRIORYTETOWE_POLECONE";
        public static final String PRIORYTETOWE_POLECONE_ZPO = "PRIORYTETOWE_POLECONE_ZPO";
        public static final String EKONOMICZNE_NIEREJESTROWANE = "EKONOMICZNE_NIEREJESTROWANE";
        public static final String EKONOMICZNE_POLECONE = "EKONOMICZNE_POLECONE";
        public static final String EKONOMICZNE_POLECONE_ZPO = "EKONOMICZNE_POLECONE_ZPO";
        public static final String EKONOMICZNE_POLECONE_DODATKOWE_ZPO = "EKONOMICZNE_POLECONE_DODATKOWE_ZPO";
        public static final String PRIORYTETOWE_POLECONE_DODATKOWE_ZPO = "PRIORYTETOWE_POLECONE_DODATKOWE_ZPO";

        private List<OutOfficeDocument> allDocuments = new ArrayList();
        private List<OutOfficeDocument> listaDokumentowKrajowych = new ArrayList();
        private List<OutOfficeDocument> listaDokumentowZagranicznych = new ArrayList();

        private Map<Integer, String> mapaWagi = new HashMap();
        private com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> tabelaIlosci;
        private com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> tabelaKosztow;
        private Integer countEcoQuantity = 0, countPriorQuantity = 0;
        private Integer countEcoQuantityPolecony = 0, countPriorQuantityPolecony = 0;
        private Integer countEcoQuantityPoleconyZPO = 0,countPriorQuantityPoleconyZPO = 0;
        private Integer countEcoQuantityPoleconyDodatkoweZPO = 0,countPriorQuantityPoleconyDodatkoweZPO = 0;
        private BigDecimal sumEcoCost = BigDecimal.ZERO, sumPriorCost = BigDecimal.ZERO;
        private BigDecimal sumEcoCostPolecony = BigDecimal.ZERO, sumPriorCostPolecony = BigDecimal.ZERO;
        private BigDecimal sumEcoCostPoleconyZPO = BigDecimal.ZERO, sumPriorCostPoleconyZPO = BigDecimal.ZERO;
        private BigDecimal sumEcoCostPoleconyDodatkoweZPO = BigDecimal.ZERO, sumPriorCostPoleconyDodatkoweZPO = BigDecimal.ZERO;
        private Integer countZoneA = 0, countZoneB = 0, countZoneC = 0, countZoneD = 0, countEur = 0, countEurOut = 0;
        private BigDecimal sumZoneA = BigDecimal.ZERO, sumZoneB = BigDecimal.ZERO, sumZoneC = BigDecimal.ZERO, sumZoneD = BigDecimal.ZERO, sumEur = BigDecimal.ZERO, sumEurOut = BigDecimal.ZERO;
        public BigDecimal KOSZT_ZPO = new BigDecimal(Docusafe.getAdditionProperty("zpo.cost.sadyIProkuratury"));

        @Override
        public void actionPerformed(ActionEvent event) {
            File temp = null;
            try {
                Journal journal = Journal.find(getId());
                if (!journal.isOutgoing()) {
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismWychodzacych"));
                }

                List results = find(journal);

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                        BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font10 = new Font(baseFont, 10);
                Font font8 = new Font(baseFont, 7);
                //Font font10Bold = new Font(baseFont, 10, Font.BOLD);
                Font font10Bold = new Font(baseFont, 10);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc;
                if (zagraniczny) {
                    pdfDoc = new com.lowagie.text.Document(PageSize.A4.rotate());
                } else {
                    pdfDoc = new com.lowagie.text.Document(PageSize.A4);
                }
                FileOutputStream fis = new FileOutputStream(temp);
                PdfWriter.getInstance(pdfDoc, fis);

                HeaderFooter footer =
                        new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                                DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font10), new Phrase("."));
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);

                pdfDoc.open();

                Paragraph p1 = new Paragraph(new Phrase("Za��cznik nr 3 do umowy nr ...................................", font10));
                p1.setAlignment(Element.ALIGN_RIGHT);
                Paragraph p2 = new Paragraph(new Phrase(sm.getString("ZestawieniaTitle") + " w obrocie krajowym nadanych w dniu ............", font10));
                p2.setAlignment(Element.ALIGN_CENTER);
                Paragraph p3 = new Paragraph(new Phrase("Nazwa firmy       ..............................", font10));
                p2.setAlignment(Element.ALIGN_LEFT);
                Paragraph p4 = new Paragraph(new Phrase("Adres                  ..............................", font10));
                p2.setAlignment(Element.ALIGN_LEFT);
                StringBuilder sb = new StringBuilder();
                sb.append("Forma op�aty za przesy�ki ....................../Umowa nr .............. z dnia ")
                        .append(new Date().toLocaleString().split(" ")[0]);
                Paragraph p5 = new Paragraph(new Phrase(sb.toString(), font10));
                p2.setAlignment(Element.ALIGN_LEFT);

                pdfDoc.add(p1);
                pdfDoc.add(p2);
                pdfDoc.add(p3);
                pdfDoc.add(p4);
                pdfDoc.add(p5);

                Table headerNational = null;
                Table headerNationalPolecone = null;
                Table headerNationalEcoPoleconeZPO = null;
                Table headerNationalPriorPoleconeZPO = null;
                Table headerForeign = null;

                float[] widths;
                if (krajowy) {
                    widths = new float[]{4.2f, 1.9f, 1.9f, 1.9f, 1.9f, 1.9f, 1.9f};
                    headerNational = new Table(widths.length);
                    buildTableHeaderNational(font10, font8, widths, headerNational, "Przesy�ki listowe nierejestrowane");
                    headerNationalPolecone = new Table(widths.length);
                    buildTableHeaderNational(font10, font8, widths, headerNationalPolecone, sm.get("PrzesylkiListowePolecone"));
                    headerNationalEcoPoleconeZPO = new Table(widths.length);
                    buildTableHeaderNationalZPO(font10, font8, widths, headerNationalEcoPoleconeZPO, sm.get("PrzesylkiListoweEkoPoleconeZPO"));
                    headerNationalPriorPoleconeZPO = new Table(widths.length);
                    buildTableHeaderNationalZPO(font10, font8, widths, headerNationalPriorPoleconeZPO, sm.get("PrzesylkiListowePriorPoleconeZPO"));
                } else {
                    widths = new float[]{4.5f, 2.3f, 2.0f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f, 1.3f, 1.8f};
                    headerForeign = new Table(widths.length);
                    buildTableHeaderForeign(font10, font8, widths, headerForeign);
                }

                for (int i = 0; i < results.size(); i++)
                {
                    final Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
                    final OutOfficeDocument document = OutOfficeDocument.findOutOfficeDocument(bean.getDocumentId());
                    allDocuments.add(document);
                }

                sortDocumentsByWeightAndCountry(allDocuments);

                boolean contextOpened = false;
                if (!DSApi.isContextOpen())
                    contextOpened = DSApi.openContextIfNeeded();

                try {
                    if (krajowy) {
                        generujMapyDlaKrajowychFromDB();
                        fillMapsNationalFromDB(listaDokumentowKrajowych);
                        sumQuantityAndCostNational();
                        addMapsAndSummaryToTableForUnregistered(headerNational, font10, font8, font10Bold);
                        addMapsAndSummaryToTableForRegistered(headerNationalPolecone, font10, font8, font10Bold);
                        addMapsAndSummaryToTableForRegisteredZPO(headerNationalEcoPoleconeZPO, false, font10, font8, font10Bold);
                        addMapsAndSummaryToTableForRegisteredZPO(headerNationalPriorPoleconeZPO, true, font10, font8, font10Bold);
                        pdfDoc.add(headerNational);
                        pdfDoc.add(headerNationalPolecone);
                        pdfDoc.newPage();
                        pdfDoc.add(headerNationalEcoPoleconeZPO);
                        pdfDoc.add(headerNationalPriorPoleconeZPO);
                    } else if (zagraniczny) {
                        generujMapyDlaZagranicznychFromDB();
                        fillMapsForeign(listaDokumentowZagranicznych);
                        sumQuantityAndCostForeign();
                        fillRowsForeign(headerForeign, font10, font8, font10Bold);
                        pdfDoc.add(headerForeign);
                    }
                } catch (Exception e) {
                    log.error("Blad podczas generowania zestawienia ilosciowo-wartosciowego. "+e);
                } finally {
                    DSApi.closeContextIfNeeded(contextOpened);
                }

                pdfDoc.close();

                showPDF(temp);

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        private void addMapsAndSummaryToTableForUnregistered(Table table, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            /*przesy�ki nierejestrowane*/
            fillUnregisteredSection(table, font1, font2, font3);

            /* PODSUMOWANIE */
            fillSummarySection(table, font3, sumEcoCost, sumPriorCost);
        }

        private void addMapsAndSummaryToTableForRegistered(Table table, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            /*przesy�ki polecone*/
            fillRegisteredSection(table, font1, font2, font3);

            /* PODSUMOWANIE */
            fillSummarySection(table, font3, sumEcoCostPolecony, sumPriorCostPolecony);
        }

        private void addMapsAndSummaryToTableForRegisteredZPO(Table table, boolean priority, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            /*przesy�ki polecone z zpo*/
            fillRegisteredZPOSection(table, priority,  font1, font2, font3);

            /* PODSUMOWANIE */
            if (priority)
                fillSummarySection(table, font3, sumPriorCostPoleconyZPO, sumPriorCostPoleconyDodatkoweZPO);
            else
                fillSummarySection(table, font3, sumEcoCostPoleconyZPO, sumEcoCostPoleconyDodatkoweZPO);
        }

        private void fillSummarySection(Table table, Font font3, BigDecimal sumEcoCost,BigDecimal sumPriorCost) throws BadElementException {
            StringBuilder sb = new StringBuilder();
            sb.append("Og�em:  ").append(sumEcoCost.add(sumPriorCost).setScale(2, RoundingMode.HALF_EVEN)).append("  ");
            Cell lastCell = new Cell(new Phrase(sb.toString(), font3));
            lastCell.setColspan(7);
            lastCell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
            table.addCell(lastCell);
        }

        private void fillRegisteredZPOSection(Table table, boolean priority, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            for(Gauge gauge : Gauge.list()){
                fillRowsPoleconeZPO(table, priority, gauge.getId(), "GABARYT " + gauge.getTitle(), font1, font2, font3);
            }

            table.addCell(new Cell(new Phrase("Razem", font3)));
            if (priority){
                table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantityPoleconyZPO), font1))));
                table.addCell(new Cell((new Phrase("", font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCostPoleconyZPO.setScale(2)), font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantityPoleconyDodatkoweZPO), font1))));
                table.addCell(new Cell((new Phrase("", font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCostPoleconyDodatkoweZPO.setScale(2)), font1))));
            } else {
                table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantityPoleconyZPO), font1))));
                table.addCell(new Cell((new Phrase("", font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCostPoleconyZPO.setScale(2)), font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantityPoleconyDodatkoweZPO), font1))));
                table.addCell(new Cell((new Phrase("", font1))));
                table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCostPoleconyDodatkoweZPO.setScale(2)), font1))));
            }
        }

        private void fillUnregisteredSection(Table table, Font font1, Font font2, Font font3) throws EdmException, BadElementException {
            for(Gauge gauge : Gauge.list()){
                fillRowsFromDB(table, gauge.getId(), "GABARYT " + gauge.getTitle(), font1, font2, font3);
            }

            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantity), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCost.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantity), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCost.setScale(2)), font1))));
        }

        private void fillRegisteredSection(Table table, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            for(Gauge gauge : Gauge.list()){
                fillRowsPolecone(table, gauge.getId(), "GABARYT " + gauge.getTitle(), false,font1, font2, font3);
            }

            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEcoQuantityPolecony), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEcoCostPolecony.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countPriorQuantityPolecony), font1))));
            table.addCell(new Cell((new Phrase("", font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumPriorCostPolecony.setScale(2)), font1))));
        }

        private void fillRowsPolecone(Table table, int gabaryt, String title, boolean zpo, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            Cell temp;
            temp = new Cell(new Phrase(title, font3));
            temp.setHorizontalAlignment(Element.ALIGN_LEFT);
            temp.setColspan(7);
            table.addCell(temp);

            Integer iloscEko = 0;
            Integer iloscPrior = 0;
            BigDecimal kosztEko = BigDecimal.ZERO;
            BigDecimal kosztPrior = BigDecimal.ZERO;
            for (Integer waga : mapaWagi.keySet()) {
                iloscEko = tabelaIlosci.get(zpo ? EKONOMICZNE_POLECONE_ZPO : EKONOMICZNE_POLECONE, gabaryt).get(waga);
                kosztEko = tabelaKosztow.get(zpo ? EKONOMICZNE_POLECONE_ZPO : EKONOMICZNE_POLECONE, gabaryt).get(waga);
                iloscPrior = tabelaIlosci.get(zpo ? PRIORYTETOWE_POLECONE_ZPO : PRIORYTETOWE_POLECONE, gabaryt).get(waga);
                kosztPrior = tabelaKosztow.get(zpo ? PRIORYTETOWE_POLECONE_ZPO : PRIORYTETOWE_POLECONE, gabaryt).get(waga);

                ShippingUnitPrice ecoPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                        OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.REGISTERED_ECONOMIC.getId(),false),zpo);
                ShippingUnitPrice priorPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                        OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.REGISTERED_PRIORITY.getId(),false),zpo);

                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEko == 0 ? "" : iloscEko), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(ecoPrice.getPrice().setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEko.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEko.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscPrior == 0 ? "" : iloscPrior), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(priorPrice.getPrice().setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztPrior.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztPrior.setScale(2)), font1));
                table.addCell(temp);
            }
        }

        private void fillRowsPoleconeZPO(Table table, boolean priority, int gabaryt, String title, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            Cell temp;
            temp = new Cell(new Phrase(title, font3));
            temp.setHorizontalAlignment(Element.ALIGN_LEFT);
            temp.setColspan(7);
            table.addCell(temp);

            Integer iloscEko = 0,iloscPrior = 0,iloscDodatkoweZPO = 0;
            BigDecimal kosztEko = BigDecimal.ZERO,kosztPrior = BigDecimal.ZERO,kosztDodatkoweZPO = BigDecimal.ZERO;
            boolean first = true;
            for (Integer waga : mapaWagi.keySet()) {
                if (priority){
                    iloscPrior = tabelaIlosci.get(PRIORYTETOWE_POLECONE_ZPO, gabaryt).get(waga);
                    kosztPrior = tabelaKosztow.get(PRIORYTETOWE_POLECONE_ZPO, gabaryt).get(waga);
                    iloscDodatkoweZPO= tabelaIlosci.get(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO, gabaryt).get(waga);
                    kosztDodatkoweZPO= tabelaKosztow.get(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO, gabaryt).get(waga);

                    ShippingUnitPrice priorPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                            OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.REGISTERED_PRIORITY.getId(),false),true);
                    temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(iloscPrior == 0 ? "" : iloscPrior), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(priorPrice.getPrice().setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(kosztPrior.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztPrior.setScale(2)), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(iloscDodatkoweZPO == 0 ? "" : iloscDodatkoweZPO), font1));
                    table.addCell(temp);
                    if (first){
                        temp = new Cell(new Phrase(KOSZT_ZPO.setScale(2).toString(), font1));
                        temp.setRowspan(mapaWagi.size());
                        table.addCell(temp);
                        first=false;
                    }
                    temp = new Cell(new Phrase(String.valueOf(kosztDodatkoweZPO.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztDodatkoweZPO.setScale(2)), font1));
                    table.addCell(temp);
                } else {
                    iloscEko = tabelaIlosci.get(EKONOMICZNE_POLECONE_ZPO, gabaryt).get(waga);
                    kosztEko = tabelaKosztow.get(EKONOMICZNE_POLECONE_ZPO, gabaryt).get(waga);
                    iloscDodatkoweZPO= tabelaIlosci.get(EKONOMICZNE_POLECONE_DODATKOWE_ZPO, gabaryt).get(waga);
                    kosztDodatkoweZPO= tabelaKosztow.get(EKONOMICZNE_POLECONE_DODATKOWE_ZPO, gabaryt).get(waga);

                    ShippingUnitPrice ecoPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                            OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.REGISTERED_ECONOMIC.getId(),false),true);
                    temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(iloscEko == 0 ? "" : iloscEko), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(ecoPrice.getPrice().setScale(2,RoundingMode.HALF_EVEN).toString(), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(kosztEko.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEko.setScale(2)), font1));
                    table.addCell(temp);
                    temp = new Cell(new Phrase(String.valueOf(iloscDodatkoweZPO == 0 ? "" : iloscDodatkoweZPO), font1));
                    table.addCell(temp);
                    if (first){
                        temp = new Cell(new Phrase(KOSZT_ZPO.setScale(2).toString(), font1));
                        temp.setRowspan(mapaWagi.size());
                        table.addCell(temp);
                        first=false;
                    }
                    temp = new Cell(new Phrase(String.valueOf(kosztDodatkoweZPO.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztDodatkoweZPO.setScale(2)), font1));
                    table.addCell(temp);
                }
            }
        }

        private void fillRowsFromDB(Table table, int gabaryt, String title, Font font1, Font font2, Font font3) throws BadElementException, EdmException {
            Cell temp;
            temp = new Cell(new Phrase(title, font3));
            temp.setHorizontalAlignment(Element.ALIGN_LEFT);
            temp.setColspan(7);
            table.addCell(temp);

            Integer iloscEko = 0;
            Integer iloscPrior = 0;
            BigDecimal kosztEko = BigDecimal.ZERO;
            BigDecimal kosztPrior = BigDecimal.ZERO;
            for (Integer waga : mapaWagi.keySet()) {
                iloscEko = tabelaIlosci.get(EKONOMICZNE_NIEREJESTROWANE, gabaryt).get(waga);
                kosztEko = tabelaKosztow.get(EKONOMICZNE_NIEREJESTROWANE, gabaryt).get(waga);
                iloscPrior = tabelaIlosci.get(PRIORYTETOWE_NIEREJESTROWANE, gabaryt).get(waga);
                kosztPrior = tabelaKosztow.get(PRIORYTETOWE_NIEREJESTROWANE, gabaryt).get(waga);

                ShippingUnitPrice ecoPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                        OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.ECONOMIC.getId(),false),false);
                ShippingUnitPrice priorPrice = ShippingUnitPrice.findByParameters(Gauge.find(gabaryt),MailWeightKind.find(waga),
                        OutOfficeDocumentDelivery.findByPriorityKindAndForeignDelivery(OutOfficeDocumentDelivery.PriorityKind.PRIORITY.getId(), false),false);

                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEko == 0 ? "" : iloscEko), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(ecoPrice.getPrice().setScale(2, RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEko.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEko.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscPrior == 0 ? "" : iloscPrior), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(priorPrice.getPrice().setScale(2, RoundingMode.HALF_EVEN).toString(), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztPrior.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztPrior.setScale(2)), font1));
                table.addCell(temp);
            }
        }

        private void fillRowsForeign(Table table, Font font1, Font font2, Font font3) throws BadElementException {
            Cell temp;

            Integer iloscA;
            Integer iloscB;
            Integer iloscC;
            Integer iloscD;
            Integer iloscEur;
            Integer iloscEurOut;
            BigDecimal kosztA;
            BigDecimal kosztB;
            BigDecimal kosztC;
            BigDecimal kosztD;
            BigDecimal kosztEur;
            BigDecimal kosztEurOut;
            for (Integer waga : mapaWagi.keySet()) {
                temp = new Cell(new Phrase(mapaWagi.get(waga), font2));
                table.addCell(temp);

                iloscA = tabelaIlosci.get(PRIORYTETOWE, 3).get(waga);
                iloscB = tabelaIlosci.get(PRIORYTETOWE, 4).get(waga);
                iloscC = tabelaIlosci.get(PRIORYTETOWE, 5).get(waga);
                iloscD = tabelaIlosci.get(PRIORYTETOWE, 6).get(waga);
                iloscEur = tabelaIlosci.get(EKONOMICZNE, 7).get(waga);
                iloscEurOut = tabelaIlosci.get(EKONOMICZNE, 8).get(waga);
                kosztA = tabelaKosztow.get(PRIORYTETOWE, 3).get(waga);
                kosztB = tabelaKosztow.get(PRIORYTETOWE, 4).get(waga);
                kosztC = tabelaKosztow.get(PRIORYTETOWE, 5).get(waga);
                kosztD = tabelaKosztow.get(PRIORYTETOWE, 6).get(waga);
                kosztEur = tabelaKosztow.get(EKONOMICZNE, 7).get(waga);
                kosztEurOut = tabelaKosztow.get(EKONOMICZNE, 8).get(waga);

                temp = new Cell(new Phrase(String.valueOf(iloscEur == 0 ? "" : iloscEur), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEur.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEur.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscEurOut == 0 ? "" : iloscEurOut), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztEurOut.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztEurOut.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscA == 0 ? "" : iloscA), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztA.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztA.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscB == 0 ? "" : iloscB), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztB.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztB.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscC == 0 ? "" : iloscC), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztC.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztC.setScale(2)), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(iloscD == 0 ? "" : iloscD), font1));
                table.addCell(temp);
                temp = new Cell(new Phrase(String.valueOf(kosztD.compareTo(BigDecimal.ZERO) == 0 ? "" : kosztD.setScale(2)), font1));
                table.addCell(temp);
            }

            table.addCell(new Cell(new Phrase("Razem", font3)));
            table.addCell(new Cell((new Phrase(String.valueOf(countEur), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEur.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countEurOut), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumEurOut.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneA), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneA.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneB), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneB.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneC), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneC.setScale(2)), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(countZoneD), font1))));
            table.addCell(new Cell((new Phrase(String.valueOf(sumZoneD.setScale(2)), font1))));
        }

        private void fillMapsNationalFromDB(List<OutOfficeDocument> listaDokumentow) throws EdmException {
            for (OutOfficeDocument doc : listaDokumentow) {
                Integer mailWeight = doc.getMailWeight();
                Integer gauge = doc.getGauge();
                String priority;
                if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.ECONOMIC.getId()) {
                    priority = EKONOMICZNE_NIEREJESTROWANE;
                    if(priority != null && gauge != null) {
                        tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.PRIORITY.getId()) {
                    priority = PRIORYTETOWE_NIEREJESTROWANE;
                    if(priority != null && gauge != null) {
                        tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.REGISTERED_ECONOMIC.getId()
                        && (doc.getZpo()==null || !doc.getZpo())) {
                    priority = EKONOMICZNE_POLECONE;
                    if(priority != null && gauge != null) {
                        tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.REGISTERED_PRIORITY.getId()
                        && (doc.getZpo()==null || !doc.getZpo())) {
                    priority = PRIORYTETOWE_POLECONE;
                    if(priority != null && gauge != null) {
                        tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                    }
                }  else if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.REGISTERED_ECONOMIC.getId()
                        && (doc.getZpo()!=null && doc.getZpo())) {
                    priority = EKONOMICZNE_POLECONE_ZPO;
                    if(priority != null && gauge != null) {
                        if (doc.getAdditionalZpo() != null && doc.getAdditionalZpo()){
                            Integer iloscDok = doc.getDocumentKind().logic().getOfficeDocumentsCount(doc);
                            BigDecimal kosztDodatkowegoZpo = KOSZT_ZPO.multiply(BigDecimal.valueOf(iloscDok));
                            tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                    doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                            tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight)
                                    .add(doc.getStampFee().subtract(kosztDodatkowegoZpo)));

                            priority = EKONOMICZNE_POLECONE_DODATKOWE_ZPO;

                            tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                    iloscDok);
                            tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight)
                                    .add(kosztDodatkowegoZpo));
                        }else{
                            tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                    doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                            tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight).add(doc.getStampFee()));
                        }
                    }
                } else if (doc.getDelivery().getPriorityKind() == OutOfficeDocumentDelivery.PriorityKind.REGISTERED_PRIORITY.getId()
                        && (doc.getZpo()!=null && doc.getZpo())) {
                    priority = PRIORYTETOWE_POLECONE_ZPO;
                    if(priority != null && gauge != null) {
                       if (doc.getAdditionalZpo() != null && doc.getAdditionalZpo()){
                           Integer iloscDok = doc.getDocumentKind().logic().getOfficeDocumentsCount(doc);
                           BigDecimal kosztDodatkowegoZpo = KOSZT_ZPO.multiply(BigDecimal.valueOf(iloscDok));
                           tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                   doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                           tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight)
                                   .add(doc.getStampFee().subtract(kosztDodatkowegoZpo)));

                           priority = PRIORYTETOWE_POLECONE_DODATKOWE_ZPO;

                           tabelaIlosci.get(priority, gauge).put(mailWeight, tabelaIlosci.get(priority, gauge).get(mailWeight) +
                                    iloscDok);
                            tabelaKosztow.get(priority, gauge).put(mailWeight, tabelaKosztow.get(priority, gauge).get(mailWeight)
                                    .add(kosztDodatkowegoZpo));


                        }
                    }
                }
            }
        }

        private void fillMapsForeign(List<OutOfficeDocument> listaDokumentow) throws EdmException {
            for (OutOfficeDocument doc : listaDokumentow) {
                Integer mailWeight = doc.getMailWeight();
                Integer zone = doc.getFieldsManager().getEnumItem(NormalLogic.DOC_MAIL_ZONE).getId();

                if (doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_EKO) {
                    String priority = EKONOMICZNE;
                    if(priority != null && zone != null) {
                        tabelaIlosci.get(priority, zone).put(mailWeight, tabelaIlosci.get(priority, zone).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, zone).put(mailWeight, tabelaKosztow.get(priority, zone).get(mailWeight).add(doc.getStampFee()));
                    }
                } else if (doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_POLECONY_PRIO || doc.getDelivery().getId() == DeliveryCostUek.ZAGRANICZNY_ZWYKLY_PRIO) {
                    String priority = PRIORYTETOWE;
                    if(priority != null && zone != null) {
                        tabelaIlosci.get(priority, zone).put(mailWeight, tabelaIlosci.get(priority, zone).get(mailWeight) + doc.getDocumentKind().logic().getOfficeDocumentsCount(doc));
                        tabelaKosztow.get(priority, zone).put(mailWeight, tabelaKosztow.get(priority, zone).get(mailWeight).add(doc.getStampFee()));
                    }
                }
            }
        }

        private void sumQuantityAndCostNational() {
            for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantity += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantityPolecony += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_POLECONE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantityPoleconyZPO += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(EKONOMICZNE_POLECONE_DODATKOWE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    countEcoQuantityPoleconyDodatkoweZPO += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantity += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantityPolecony += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_POLECONE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantityPoleconyZPO += map.get(weight);
                }
            }
            for (Map<Integer, Integer> map : tabelaIlosci.row(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    countPriorQuantityPoleconyDodatkoweZPO += map.get(weight);
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCost = sumEcoCost.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCostPolecony = sumEcoCostPolecony.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_POLECONE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCostPoleconyZPO = sumEcoCostPoleconyZPO.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(EKONOMICZNE_POLECONE_DODATKOWE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    sumEcoCostPoleconyDodatkoweZPO = sumEcoCostPoleconyDodatkoweZPO.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_NIEREJESTROWANE).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCost = sumPriorCost.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_POLECONE).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCostPolecony = sumPriorCostPolecony.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_POLECONE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCostPoleconyZPO = sumPriorCostPoleconyZPO.add(map.get(weight));
                }
            }
            for (Map<Integer, BigDecimal> map : tabelaKosztow.row(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO).values()) {
                for (Integer weight : map.keySet()) {
                    sumPriorCostPoleconyDodatkoweZPO = sumPriorCostPoleconyDodatkoweZPO.add(map.get(weight));
                }
            }
        }

        private void sumQuantityAndCostForeign() {

            Map<Integer, Integer> strefaA = tabelaIlosci.get(PRIORYTETOWE, 3);
            Map<Integer, Integer> strefaB = tabelaIlosci.get(PRIORYTETOWE, 4);
            Map<Integer, Integer> strefaC = tabelaIlosci.get(PRIORYTETOWE, 5);
            Map<Integer, Integer> strefaD = tabelaIlosci.get(PRIORYTETOWE, 6);
            Map<Integer, Integer> strefaEur = tabelaIlosci.get(EKONOMICZNE, 7);
            Map<Integer, Integer> strefaEurOut = tabelaIlosci.get(EKONOMICZNE, 8);

            for (Integer weight : strefaA.keySet()) {
                countZoneA += strefaA.get(weight);
            }
            for (Integer weight : strefaB.keySet()) {
                countZoneB += strefaB.get(weight);
            }
            for (Integer weight : strefaC.keySet()) {
                countZoneC += strefaC.get(weight);
            }
            for (Integer weight : strefaD.keySet()) {
                countZoneD += strefaD.get(weight);
            }
            for (Integer weight : strefaEur.keySet()) {
                countEur += strefaEur.get(weight);
            }
            for (Integer weight : strefaEurOut.keySet()) {
                countEurOut += strefaEurOut.get(weight);
            }

            Map<Integer, BigDecimal> strefaACosts = tabelaKosztow.get(PRIORYTETOWE, 3);
            Map<Integer, BigDecimal> strefaBCosts = tabelaKosztow.get(PRIORYTETOWE, 4);
            Map<Integer, BigDecimal> strefaCCosts = tabelaKosztow.get(PRIORYTETOWE, 5);
            Map<Integer, BigDecimal> strefaDCosts = tabelaKosztow.get(PRIORYTETOWE, 6);
            Map<Integer, BigDecimal> strefaEurCosts = tabelaKosztow.get(EKONOMICZNE, 7);
            Map<Integer, BigDecimal> strefaEurOutCosts = tabelaKosztow.get(EKONOMICZNE, 8);

            for (Integer weight : strefaACosts.keySet()) {
                sumZoneA = sumZoneA.add(strefaACosts.get(weight));
            }
            for (Integer weight : strefaBCosts.keySet()) {
                sumZoneB = sumZoneB.add(strefaBCosts.get(weight));
            }
            for (Integer weight : strefaCCosts.keySet()) {
                sumZoneC = sumZoneC.add(strefaCCosts.get(weight));
            }
            for (Integer weight : strefaDCosts.keySet()) {
                sumZoneD = sumZoneD.add(strefaDCosts.get(weight));
            }
            for (Integer weight : strefaEurCosts.keySet()) {
                sumEur = sumEur.add(strefaEurCosts.get(weight));
            }
            for (Integer weight : strefaEurOutCosts.keySet()) {
                sumEurOut = sumEurOut.add(strefaEurOutCosts.get(weight));
            }
        }

        private void generujMapyDlaKrajowychFromDB() throws EdmException {
            mapaWagi = getWagiFromDB(false);
            tabelaIlosci = getIloscTableFromDB(false);
            tabelaKosztow = getKosztTableFromDB(false);
        }

        private void generujMapyDlaZagranicznychFromDB() throws EdmException {
            mapaWagi = getWagiFromDB(true);
            tabelaIlosci = getIloscTableFromDB(true);
            tabelaKosztow = getKosztTableFromDB(true);
        }

        private Map<Integer, String> getWagiFromDB(boolean zagraniczne) throws EdmException {
            Map<Integer, String> mapa = new HashMap();

            if (!zagraniczne) {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),weight.getName());
                }
            } else {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),weight.getName());
                }
            }
            return mapa;
        }

        private com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> getKosztTableFromDB(boolean zagraniczne) throws EdmException {
            com.google.common.collect.Table<String, Integer, Map<Integer, BigDecimal>> table = HashBasedTable.create();
            if (zagraniczne) {
                /*(priorytet,strefa,wartosc)*/
                table.put(PRIORYTETOWE, 3, getKosztMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 4, getKosztMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 5, getKosztMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 6, getKosztMapFromDB(zagraniczne));
                table.put(EKONOMICZNE, 7, getKosztMapFromDB(zagraniczne));
                table.put(EKONOMICZNE, 8, getKosztMapFromDB(zagraniczne));
            } else {
                /*(priorytet,gabaryt,wartosc)*/
                List<Gauge> gaugeList = Gauge.list();
                for (Gauge gauge : gaugeList){
                    table.put(EKONOMICZNE_NIEREJESTROWANE, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE_ZPO, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE_DODATKOWE_ZPO, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_NIEREJESTROWANE, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE_ZPO, gauge.getId(), getKosztMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO, gauge.getId(), getKosztMapFromDB(zagraniczne));
                }
            }
            return table;
        }

        private com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> getIloscTableFromDB(boolean zagraniczne) throws EdmException {
            //<EKONOMICZNE|PRIORYTETOWE, WIERSZ, WAGI>
            com.google.common.collect.Table<String, Integer, Map<Integer, Integer>> table = HashBasedTable.create();
            if (!zagraniczne) {
                /*(priorytet,gabaryt,wartosc)*/
                List<Gauge> gaugeList = Gauge.list();
                for (Gauge gauge : gaugeList){
                    table.put(EKONOMICZNE_NIEREJESTROWANE, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE_ZPO, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(EKONOMICZNE_POLECONE_DODATKOWE_ZPO, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_NIEREJESTROWANE, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE_ZPO, gauge.getId(), getIloscMapFromDB(zagraniczne));
                    table.put(PRIORYTETOWE_POLECONE_DODATKOWE_ZPO, gauge.getId(), getIloscMapFromDB(zagraniczne));
                }
            } else {
                /*(priorytet,strefa,wartosc)*/
                table.put(PRIORYTETOWE, 3, getIloscMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 4, getIloscMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 5, getIloscMapFromDB(zagraniczne));
                table.put(PRIORYTETOWE, 6, getIloscMapFromDB(zagraniczne));
                table.put(EKONOMICZNE, 7, getIloscMapFromDB(zagraniczne));
                table.put(EKONOMICZNE, 8, getIloscMapFromDB(zagraniczne));

            }
            return table;
        }

        private Map<Integer, Integer> getIloscMapFromDB(boolean zagraniczne) throws EdmException {
            Map<Integer, Integer> mapa = new HashMap();
            if (!zagraniczne) {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),0);
                }
            } else {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),0);
                }
            }
            return mapa;
        }

        private Map<Integer,BigDecimal> getKosztMapFromDB(boolean zagraniczne) throws EdmException {
            Map<Integer, BigDecimal> mapa = new HashMap();

            if (!zagraniczne) {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),BigDecimal.ZERO);
                }
            } else {
                //TODO: zaimplementowa� pobieranie radzaj�w wag krajowych i zagranicznych
                for (MailWeightKind weight : MailWeightKind.list()){
                    mapa.put(weight.getId(),BigDecimal.ZERO);
                }
            }
            return mapa;
        }

        private void sortDocumentsByWeightAndCountry(List<OutOfficeDocument> allDocuments) {
            for (OutOfficeDocument document : allDocuments) {
                Boolean foreignDelivery = document.getDelivery().getForeignDelivery();
                if (foreignDelivery != null && !foreignDelivery){
                    listaDokumentowKrajowych.add(document);
                } else if (foreignDelivery != null && foreignDelivery){
                    listaDokumentowZagranicznych.add(document);
                }
            }
        }

        private void buildTableHeaderNational(Font font1, Font font2, float[] widths, Table table, String title) throws BadElementException {
            table.setWidths(widths);
            table.setWidth(100.0f);
            table.setCellsFitPage(true);
            table.setPadding(2);
            table.setDefaultHorizontalAlignment(Element.ALIGN_CENTER);
            table.setDefaultVerticalAlignment(Element.ALIGN_MIDDLE);

            //1 wiersz
            //1 kolumna
            Cell przedzialWagowy = new Cell(new Phrase("Przedzia� wagowy", font1));
            przedzialWagowy.setHorizontalAlignment(Cell.ALIGN_CENTER);
            przedzialWagowy.setRowspan(3);
            table.addCell(przedzialWagowy);

            //1 wiersz
            //2 kolumna
            Cell name = new Cell(new Phrase(title, font1));
            name.setHorizontalAlignment(Cell.ALIGN_CENTER);
            name.setColspan(6);
            table.addCell(name);

            //2 wiersz
            //2 kolumna
            Cell ekon = new Cell(new Phrase("ekonomiczne", font1));
            ekon.setHorizontalAlignment(Cell.ALIGN_CENTER);
            ekon.setColspan(3);
            table.addCell(ekon);

            //2 wiersz
            //3 kolumna
            Cell prior = new Cell(new Phrase("priorytetowe", font1));
            prior.setHorizontalAlignment(Cell.ALIGN_CENTER);
            prior.setColspan(3);
            table.addCell(prior);

            //3 wiersz
            //2 kolumna
            Cell liczba = new Cell(new Phrase("liczba", font2));
            liczba.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba);

            //3 wiersz
            //3 kolumna
            Cell cena = new Cell(new Phrase("cena jednostkowa", font2));
            cena.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cena);

            //3 wiersz
            //4 kolumna
            Cell wartosc = new Cell(new Phrase("��czna warto��", font2));
            wartosc.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc);

            //3 wiersz
            //5 kolumna
            Cell liczba2 = new Cell(new Phrase("liczba", font2));
            liczba2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba2);

            //3 wiersz
            //6 kolumna
            Cell cena2 = new Cell(new Phrase("cena jednostkowa", font2));
            cena2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cena2);

            //3 wiersz
            //7 kolumna
            Cell wartosc2 = new Cell(new Phrase("��czna warto��", font2));
            wartosc2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc2);

            table.endHeaders();
        }

        private void buildTableHeaderNationalZPO(Font font1, Font font2, float[] widths, Table table, String title) throws BadElementException {
            table.setWidths(widths);
            table.setWidth(100.0f);
            table.setCellsFitPage(true);
            table.setPadding(2);
            table.setDefaultHorizontalAlignment(Element.ALIGN_CENTER);
            table.setDefaultVerticalAlignment(Element.ALIGN_MIDDLE);

            //1 wiersz
            //1 kolumna
            Cell przedzialWagowy = new Cell(new Phrase("Przedzia� wagowy", font1));
            przedzialWagowy.setHorizontalAlignment(Cell.ALIGN_CENTER);
            przedzialWagowy.setRowspan(3);
            table.addCell(przedzialWagowy);

            //1 wiersz
            //2 kolumna
            Cell name = new Cell(new Phrase(title, font1));
            name.setHorizontalAlignment(Cell.ALIGN_CENTER);
            name.setColspan(6);
            table.addCell(name);


            //3 wiersz
            //2 kolumna
            Cell liczba = new Cell(new Phrase("liczba", font2));
            liczba.setHorizontalAlignment(Cell.ALIGN_CENTER);
            liczba.setRowspan(2);
            table.addCell(liczba);

            //3 wiersz
            //3 kolumna
            Cell cena = new Cell(new Phrase("cena jednostkowa", font2));
            cena.setHorizontalAlignment(Cell.ALIGN_CENTER);
            cena.setRowspan(2);
            table.addCell(cena);

            //3 wiersz
            //4 kolumna
            Cell wartosc = new Cell(new Phrase("��czna warto��", font2));
            wartosc.setHorizontalAlignment(Cell.ALIGN_CENTER);
            wartosc.setRowspan(2);
            table.addCell(wartosc);

            //2 wiersz
            //3 kolumna
            Cell prior = new Cell(new Phrase(sm.getString("DodatkoweZPOSadyiProkuratury"), font2));
            prior.setHorizontalAlignment(Cell.ALIGN_CENTER);
            prior.setColspan(3);
            table.addCell(prior);

            //3 wiersz
            //5 kolumna
            Cell liczba2 = new Cell(new Phrase("liczba", font2));
            liczba2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba2);

            //3 wiersz
            //6 kolumna
            Cell cena2 = new Cell(new Phrase("cena jednostkowa", font2));
            cena2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cena2);

            //3 wiersz
            //7 kolumna
            Cell wartosc2 = new Cell(new Phrase("��czna warto��", font2));
            wartosc2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc2);

            table.endHeaders();
        }

        private void buildTableHeaderForeign(Font font, Font font2, float[] widths, Table table) throws BadElementException {
            table.setWidths(widths);
            table.setWidth(100.0f);
            table.setCellsFitPage(true);
            table.setPadding(2);

            //1 wiersz
            //1 kolumna
            Cell przedzialWagowy = new Cell(new Phrase("Przedzia� wagowy", font));
            przedzialWagowy.setRowspan(4);
            table.addCell(przedzialWagowy);

            //1 wiersz
            //2 kolumna
            Cell name = new Cell(new Phrase("Przesy�ki listowe nierejestrowane", font));
            name.setHorizontalAlignment(Cell.ALIGN_CENTER);
            name.setColspan(12);
            table.addCell(name);

            //2 wiersz
            //2 kolumna
            Cell ekon = new Cell(new Phrase("ekonomiczne", font));
            ekon.setHorizontalAlignment(Cell.ALIGN_CENTER);
            ekon.setColspan(4);
            table.addCell(ekon);

            //2 wiersz
            //3 kolumna
            Cell prior = new Cell(new Phrase("priorytetowe", font));
            prior.setHorizontalAlignment(Cell.ALIGN_CENTER);
            prior.setColspan(8);
            table.addCell(prior);

            //3 wiersz
            //2 kolumna
            Cell eur = new Cell(new Phrase("Kraje europejskie (��cznie z Cyprem, ca�� Rosj� i Izraelem", font2));
            eur.setHorizontalAlignment(Cell.ALIGN_CENTER);
            eur.setColspan(2);
            table.addCell(eur);

            //3 wiersz
            //3 kolumna
            Cell outEur = new Cell(new Phrase("Kraje pozaeuropejskie", font));
            outEur.setHorizontalAlignment(Cell.ALIGN_CENTER);
            outEur.setColspan(2);
            table.addCell(outEur);

            //3 wiersz
            //4 kolumna
            Cell strefaA = new Cell(new Phrase("STREFA \"A\"", font));
            strefaA.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaA.setColspan(2);
            table.addCell(strefaA);

            //3 wiersz
            //5 kolumna
            Cell strefaB = new Cell(new Phrase("STREFA \"B\"", font));
            strefaB.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaB.setColspan(2);
            table.addCell(strefaB);

            //3 wiersz
            //6 kolumna
            Cell strefaC = new Cell(new Phrase("STREFA \"C\"", font));
            strefaC.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaC.setColspan(2);
            table.addCell(strefaC);

            //3 wiersz
            //7 kolumna
            Cell strefaD = new Cell(new Phrase("STREFA \"D\"", font));
            strefaD.setHorizontalAlignment(Cell.ALIGN_CENTER);
            strefaD.setColspan(2);
            table.addCell(strefaD);

            //4 wiersz
            //2 kolumna
            Cell liczba = new Cell(new Phrase("liczba", font));
            liczba.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba);

            //4 wiersz
            //3 kolumna
            Cell wartosc = new Cell(new Phrase("warto��", font));
            wartosc.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc);

            //4 wiersz
            //4 kolumna
            Cell liczba2 = new Cell(new Phrase("liczba", font));
            liczba2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba2);

            //4 wiersz
            //5 kolumna
            Cell wartosc2 = new Cell(new Phrase("warto��", font));
            wartosc2.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc2);

            //4 wiersz
            //6 kolumna
            Cell liczba3 = new Cell(new Phrase("liczba", font));
            liczba3.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba3);

            //4 wiersz
            //7 kolumna
            Cell wartosc3 = new Cell(new Phrase("warto��", font));
            wartosc3.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc3);

            //4 wiersz
            //8 kolumna
            Cell liczba4 = new Cell(new Phrase("liczba", font));
            liczba4.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba4);

            //4 wiersz
            //9 kolumna
            Cell wartosc4 = new Cell(new Phrase("warto��", font));
            wartosc4.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc4);

            //4 wiersz
            //10 kolumna
            Cell liczba5 = new Cell(new Phrase("liczba", font));
            liczba5.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba5);

            //4 wiersz
            //11 kolumna
            Cell wartosc5 = new Cell(new Phrase("warto��", font));
            wartosc5.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc5);

            //4 wiersz
            //12 kolumna
            Cell liczba6 = new Cell(new Phrase("liczba", font));
            liczba6.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(liczba6);

            //4 wiersz
            //13 kolumna
            Cell wartosc6 = new Cell(new Phrase("warto��", font));
            wartosc6.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(wartosc6);

            table.endHeaders();
        }

    }

    public boolean isIlosciowy()
	{
		return ilosciowy;
	}

	
	public void setIlosciowy(boolean ilosciowy)
	{
		this.ilosciowy = ilosciowy;
	}
	public boolean isExtended()
	{
		return extended;
	}

    public boolean isZestawienie() {
        return zestawienie;
    }

    public void setZestawienie(boolean zestawienie) {
        this.zestawienie = zestawienie;
    }

    public boolean isKrajowy() {
        return krajowy;
    }

    public void setKrajowy(boolean krajowy) {
        this.krajowy = krajowy;
    }


    public boolean isZagraniczny() {
        return zagraniczny;
    }

    public void setZagraniczny(boolean zagraniczny) {
        this.zagraniczny = zagraniczny;
    }

	
	public boolean isEtykieta()
	{
		return etykieta;
	}

	
	public void setEtykieta(boolean etykieta)
	{
		this.etykieta = etykieta;
	}

	
	public long getDocumentId()
	{
		return documentId;
	}

	
	public void setDocumentId(long documentId)
	{
		this.documentId = documentId;
	}

}