package pl.compan.docusafe.web.office.journals;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalManager;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintInJournalAction.java,v 1.36 2009/11/19 12:20:25 mariuszk Exp $
 */
public class PrintInJournalAction extends InJournalActionBase
{
	private static final Logger log = LoggerFactory.getLogger(PrintInJournalAction.class);
	private int fontsize;
    /**
     * Format daty u�ywany przy generowaniu pliku PDF.
     */
    public static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Pdf()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Pdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File temp = null;
            try
            {
                Journal journal = Journal.find(getId());
                if (!journal.isIncoming())
                    throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismPrzychodzacych"));

                List results = find(journal);
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, (float)fontsize);
                temp = File.createTempFile("docusafe_", "_tmp");
                Document pdfDoc =
                    new Document(PageSize.A4.rotate(),30,60,30,30);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                
                HeaderFooter footer =
               	 new HeaderFooter(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
                            DateUtils.formatCommonDateTime(new Date()) + " Strona nr "), font), new Phrase("."));
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);                
                pdfDoc.open();
                PdfUtils.addHeaderImageToPdf(pdfDoc, journal.getSummary(), baseFont);

                int[] widths;                
                if(journal.isMain())
                	widths = JournalManager.getInMainWidths();
                else
                	widths = JournalManager.getInWidths();
                if (AvailabilityManager.isAvailable("p4.printInJournal.SignatureOnlyInTasmowa") && journal.getOwnerGuid().equals("MINERALNA"))
                {
                    // ostatnie 10, 10 przeznaczone na    recipientUser,recipientDate
                   int [] mineralnaWidths = new int[widths.length - 2];
                    for (int i = 0; i < widths.length - 2; i++)
                    {
                        mineralnaWidths[i] = widths[i];
                    }
                    widths =  mineralnaWidths;
                }
                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(2);
                
                List<String> inColumn;
                if(journal.isMain())
                	inColumn = JournalManager.getInPrintMainColumn();
                else
                	inColumn = JournalManager.getInPrintColumn();
                int lp = 0;
                for (String cellName : inColumn) 
                {
                    if (!(AvailabilityManager.isAvailable("p4.printInJournal.SignatureOnlyInTasmowa") &&
                            journal.getOwnerGuid().equals("MINERALNA") &&
                            (cellName.equals("recipientUser") || cellName.equals("recipientDate"))))
                	    table.addCell(JournalManager.getCellTitleByName(cellName, font, sm));
				}

                table.endHeaders();

                for (int i=0; i < results.size(); i++)
                {
                	lp++;
                    Journal.EntryBean bean = (Journal.EntryBean) results.get(i);
                    for (String cellName : inColumn) 
                    {
                        if (!(AvailabilityManager.isAvailable("p4.printInJournal.SignatureOnlyInTasmowa") &&
                                journal.getOwnerGuid().equals("MINERALNA") &&
                                (cellName.equals("recipientUser") || cellName.equals("recipientDate"))))
                    	    JournalManager.getCellValueByName(table,bean,cellName,font,sm,lp);
    				}
                }
                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (Exception e)
            {
            	log.error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }

            if (temp != null && temp.exists())
            {
                if (log.isDebugEnabled())
                		log.debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
    		        if(AvailabilityManager.isAvailable("printJournal.PIG")){
    		        	ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                                "application/pdf", (int) temp.length(),
                                "Content-Disposition: attachment; filename=\"dziennik-" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf\"");
    		        }else{
    		        	ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                                "application/pdf", (int) temp.length(),
                                "Content-Disposition: inline; filename=\"dziennik-" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf\"");
    		        }
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {
                	log.error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }
    public void setFontsize(int size){
    	this.fontsize=size;
    }
}
