package pl.compan.docusafe.web.office.users;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Constants.java,v 1.1 2004/07/16 14:23:59 lk Exp $
 */
public class Constants
{
    public static final String Package = Constants.class.getPackage().getName();
}
