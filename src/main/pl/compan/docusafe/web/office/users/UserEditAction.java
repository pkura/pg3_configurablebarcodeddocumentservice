package pl.compan.docusafe.web.office.users;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.hibernate.Query;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSDivisionFilter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.Parametrization;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import std.fun;
import std.lambda;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserEditAction.java,v 1.44 2009/09/18 11:01:36 pecet1 Exp $
 */
public final class UserEditAction extends EventProcessingAction
{
	private static final Logger log =  pl.compan.docusafe.util.LoggerFactory.getLogger(UserEditAction.class);
	public static final String TAB_DATA = "data";
	public static final String TAB_HIST = "history";

	private String username;
	private Boolean mySubstitude;
	private String tab;
	private Long substId;
    private Tabs tabs;
    private String divisionGuid;
    private ArrayList hist;
    private ArrayList histBy;
    private ArrayList acctualSubst;

    private static StringManager sm = GlobalPreferences.loadPropertiesFile(UserEditAction.class.getPackage().getName(),null);
    boolean onlySubstitutionFromSameGroup = AvailabilityManager.isAvailable("substitutionFromSameGroupList");
    private static final String FORWARD = "office/users/user-edit";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(new PrepareTabs()).
            append(fillForm);
            
        registerListener("doAddSubst").
            append(setForward).
            append(new PrepareTabs()).
            append(new DoAddSubst()).
            append(fillForm);
        
        registerListener("doDeleteSubst").
	        append(setForward).
	        append(new PrepareTabs()).
	        append(new doDeleteSubst()).
	        append(fillForm);
    }
    
        
    private class PrepareTabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	final DynaActionForm form = event.getDynaForm();

        	username=(String)form.get("username");
        	mySubstitude = Boolean.TRUE.equals((Boolean) form.get("mySubstitude"));
        	divisionGuid=(String)form.get("divisionGuid");
        	tab=(String)form.get("tab");

        	if (!TAB_DATA.equals(tab) && !TAB_HIST.equals(tab)) tab = TAB_DATA;

            tabs = new Tabs(1);
            String link;
            if (username != null)
            {
	                link = event.getRequest().getContextPath()+getBaseLink() +
	                    (!mySubstitude ? "?username="+username : "?mySubstitude=true")+
	                    (divisionGuid != null ? "&divisionGuid="+divisionGuid : "");
            }
            else
            {
                link = event.getRequest().getContextPath()+getBaseLink() + "?a=a"; // tylko po to, by u�y� '&tab' ni�ej
            }

            tabs.add(new Tab(sm.getString("KartaUzytkownika"), sm.getString("KartaUzytkownika"),
                link+"&tab="+TAB_DATA, TAB_DATA.equals(tab)));
            tabs.add(new Tab(sm.getString("HistoriaZastepstw"), sm.getString("HistoriaZastepstw"),
                link+"&tab="+TAB_HIST, TAB_HIST.equals(tab)));
            event.getRequest().setAttribute("tabs", tabs);
            event.getRequest().setAttribute("tab", tab);
            form.set("tab", tab);
        }
    }
    

    private final class doDeleteSubst implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
            final DynaActionForm form = event.getDynaForm();

            PreparedStatement pst = null;
            try
            {
            	DSApi.openAdmin();
                DSApi.context().begin();
                SubstituteHistory sh = SubstituteHistory.find((Long)form.get("substId"));

                sh.setStatus(SubstituteHistory.DELETED);
                sh.setCancelDate(new Date());
                DSApi.context().commit();
                
              
                if(AvailabilityManager.isAvailable("zastepstwa.odswiezajPrzyUsunieciu")){
                	DSApi.context().begin();
                	DSUser subs=DSUser.findByUsername(sh.getSubstLogin());

                	List<SubstituteHistory> substitutedUsersHist =SubstituteHistory.findActualSubstituted(subs.getName());
                	Set<DSUser> substitutedUsers=new LinkedHashSet<DSUser>();

                	for (SubstituteHistory substituteHistory : substitutedUsersHist) {
                		DSUser su = DSUser.findByUsername(substituteHistory.getLogin());
                		substitutedUsers.add(su);
                	}
                	((UserImpl)subs).setSubstitutedUsers(substitutedUsers);
                	DSApi.context().commit();
                }
                
            }
            catch (Exception e)
            {
                log.error(e.getMessage(),e);
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi.context().closeStatement(pst);
            	try {
					DSApi.close();
				} catch (EdmException e) {
					log.error(e.getMessage(),e);
				}
            }
        }
    }
    
    private class DoAddSubst implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final DynaActionForm form = event.getDynaForm();
            final String substitutedBy = !StringUtils.isEmpty((String) form.get("substitutedBy")) ? (String) form.get("substitutedBy") : null;

            Date substitutedThrough = null;
            Date substitutedFrom = null;
            substitutedFrom = DateUtils.nullSafeParseJsDate((String) form.get("substitutedFrom") + " 23:59:59");
           // do ko�ca danego dnia
            substitutedThrough = DateUtils.parseJsDateTime(
            		(String) form.get("substitutedThrough") + " 23:59:59");

            try{
                if (substitutedFrom==null||substitutedThrough==null)
                {
                    throw new EdmException("Nie wybrano okresu zast�pstwa");
                }
                String substFrom = DateUtils.formatJsDateTimeWithSeconds(substitutedFrom);
                String substThrough = DateUtils.formatJsDateTimeWithSeconds(substitutedThrough);

                //}

                boolean success = false;
                //Date now =  new GregorianCalendar().getTime(); // DateUtils.nullSafeParseJsDate((String) form.get("substitutedFrom") + " 0:00:00");
                //DateUtils.commonDateFormat.format(now);
                //DateUtils.commonDateFormat.format(substitutedFrom);

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                Date yesterday =  cal.getTime();
                DateUtils.commonDateFormat.format(yesterday);


                if (substitutedBy == null)
                {
            		throw new EdmException(sm.getString("NiePodanoUzytkownikaZastepujacego"));
            	}
                if (substitutedThrough.before(substitutedFrom))
                {
            		throw new EdmException("Data rozpocz�cia nie mo�e by� p�niejsza ni� data ko�ca");
                }
                if (substitutedFrom.before(yesterday))
                {
            		throw new EdmException("Zakres nie mo�e by� w przesz�o�ci");
                }


                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSApi.context().begin();

                if (SubstituteHistory.validateSubstitution(username,substitutedFrom,substitutedThrough))
                {
                    throw new EdmException("Nie mo�na przypisa� dw�ch zast�pstw przypadaj�cych na ten sam okres.");
                }

                String username = getUsername(form);
                
                DSUser user = DSUser.findByUsername(username);
                DSUser subst = DSUser.findByUsername(substitutedBy);
                
				if(AvailabilityManager.isAvailable("substitutionFromSameGroup"))
				{
					boolean theSame=false;
					DSUser substBy = DSUser.findByUsername(substitutedBy);
					for(String guidTemp: user.getGroupsGuid())
					{
						for(String guidCheck: substBy.getGroupsGuid())
						{
							if(guidCheck.equals(guidTemp)) theSame=true;
						}
					}
					if(!theSame) throw new EdmException("U�ytkownicy nie znajduj� si� w tej samej grupie");
				}
                
				SubstituteHistory history = new SubstituteHistory(username, substitutedFrom, substitutedThrough, substitutedBy, DSApi.context().getDSUser().getName());

                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);

                //jezeli zastepstwo jest na dzisiaj to od razu wpisujemy do ds_user  i ewentualnie od razu wpis do historii pism
                Date now = new Date();
                if(now.after(substitutedFrom)) {
                	user.setSubstitutionFromHistory(history);

                	if(AvailabilityManager.isAvailable("Zastepstwa.dodajWpisdoDokumentowZastepowanego")){
                		TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
                		List<Task> listaZadanUzytkownika=taskList.getTasks(user);
                		for(Task task:listaZadanUzytkownika){
                			boolean jestJuzWHistorii=false;
                			DateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                			for(Audit audi:OfficeDocument.find(task.getDocumentId()).getWorkHistory()){
                				try{
                					if(audi.getProperty()!=null&&audi.getDescription()!=null&&audi.getLparam()!=null&&audi.getWparam()!=null&&audi.getUsername()!=null){
                						//nie ma powtarzajacego sie wpisu, lub jest, ale z data zakonczenia zastepstwa wieksza niz data poczatku nowego zastepstwa
                						if(("substitute").equals(audi.getProperty())&&DSApi.context().getPrincipalName().equals(audi.getUsername())&&
                								sm.getString("DokumentPrzejalWZastepstwie",subst.asLastnameFirstname()).equals(audi.getDescription())&&task.getDocumentId().equals(audi.getWparam())
                								&&DateUtils.parseDateAnyFormat(sdf.format(substitutedFrom)).compareTo(DateUtils.parseDateAnyFormat(audi.getLparam()))<=0)
                							jestJuzWHistorii=true;
                					}

                				}
                				catch (ParseException e) {
                					//jak nie moze sparsowac lparam do daty, to znaczy, ze nie jest wpis z zastepstwa
                				}
                			}
                			if(!jestJuzWHistorii){
                				OfficeDocument.find(task.getDocumentId()).addWorkHistoryEntry(Audit.create("substitute", DSApi.context().getPrincipalName(), sm.getString("DokumentPrzejalWZastepstwie",subst.asLastnameFirstname()),sdf.format(substitutedThrough),task.getDocumentId()));
                			}
                		}
                	}
                }
                DSApi.context().session().save(history);      
                Parametrization.getInstance().afterAddSubstution(history,event);
                DSApi.context().commit();

                String userMail = user.getEmail();
                

                if ( userMail == null && AvailabilityManager.isAvailable("userSubstitutedSendMail") ) {
                    throw new EdmException("Uzytkownik nie ma przypisanego adresu email");
                }
                if (AvailabilityManager.isAvailable("userSubstitutedSendMail")) {
                    Map<String, Object> ctext = new HashMap<String, Object>();
                    ctext.put("imie", user.getFirstname());
                    ctext.put("nazwisko", user.getLastname());
                    ctext.put("od", substFrom);
                    ctext.put("do", substThrough);
                    ctext.put("zastepstwo", subst.getFirstnameLastnameName());

                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.SUBSTITUTION_MAIL), ctext);
                    event.getMessages().add(sm.getString("WyslanoMaila"));
                }

                success = true;
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(),e);
                event.getErrors().add(e.getMessage());
            } catch (IOException e) {
                log.error(e.getMessage(),e);
                event.getErrors().add(e.getMessage());
            } finally
            {
            	try {
					DSApi.close();
				} catch (EdmException e) {
					log.error(e.getMessage(),e);
				}
            }
        }
    }
    
    /**
     * Zwraca nazw� u�ytkownika na formularz. Je�li formularz ma ustawiony
     * mySubstitude tzn ze u�ytkownik przeglada swoje zast�pstwa
     * u�ywa� w otwartym konteksie, pobiera usera z sesji
     * @param form
     * @return
     */
    private String getUsername(DynaActionForm form)
    {
    	String username = (String) form.get("username");
    	//zabezpieczenie przed nullem
    	mySubstitude = Boolean.TRUE.equals((Boolean) form.get("mySubstitude"));
       
        if(mySubstitude)
           username = DSApi.context().getPrincipalName();
            
    	return username;
    }
//    private final class Update implements ActionListener
//    {
//        public void actionPerformed(ActionEvent event)
//        {
//            final DynaActionForm form = event.getDynaForm();
//            final String identifier = (String) form.get("identifier");
//            final String email = (String) form.get("email");
//            final String externalName = (String) form.get("externalName");
//            final String firstname = (String) form.get("firstname");
//            final String lastname = (String) form.get("lastname"); 
//            final String substitutedBy = !StringUtils.isEmpty((String) form.get("substitutedBy")) ? (String) form.get("substitutedBy") : null;
//            final String phoneNum = (String) form.get("phoneNum");
//            final String mobileNum = (String) form.get("mobileNum");
//
//            Date substitutedThrough = null;
//            Date substitutedFrom = null;
//            substitutedFrom = DateUtils.nullSafeParseJsDate((String) form.get("substitutedFrom"));
//           // do ko�ca danego dnia
//            substitutedThrough = DateUtils.parseJsDateTime(
//            		(String) form.get("substitutedThrough") + " 23:59:59");
//            //}
//
//            boolean success = false;
//            try
//            {
//                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
//                ctx.begin();
//                if (substitutedBy == null && (substitutedFrom!=null||substitutedThrough!=null))
//                {
//            		throw new EdmException(sm.getString("NiePodanoUzytkownikaZastepujacego"));
//            	}
//                boolean subst = DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA);
//
//                DSUser user = DSUser.findByUsername((String) form.get("username"));
//
//                UpdateUser.updateUser(
//                    (String) form.get("username"),
//                    !StringUtils.isEmpty(externalName) ? externalName : null,
//                    !StringUtils.isEmpty(firstname) ? firstname : user.getFirstname(),
//                    !StringUtils.isEmpty(lastname) ? lastname : user.getLastname(),
//                    !StringUtils.isEmpty(identifier) ? identifier : user.getIdentifier(),
//                    !StringUtils.isEmpty(email) ? email : user.getEmail(),
//                    null, null, /* password, password2 */
//                    false,
//                    !StringUtils.isEmpty((String) form.get("supervisor")) ? (String) form.get("supervisor") : null,
//                    subst ? substitutedBy : (user.getSubstituteUser() != null ? user.getSubstituteUser().getName() : null),
//                    subst ? substitutedFrom : user.getSubstitutedFrom(),
//                    subst ? substitutedThrough : user.getSubstitutedThrough(),
//                    null, /* roles */
//                    null, //(String[]) form.get("groups"),
//                    null, //(Long[]) form.get("officeRoles"));
//                    null,
//                    null // certificateLogin
//					);
//
//                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
//                cache.invalidate(user);
//
//                ctx.commit();
//                success = true;
//            }
//            catch (EdmException e)
//            {
//                DSApi.context().setRollbackOnly();
//                event.getErrors().add(e.getMessage());
//            }
//            finally
//            {
//                DSApi._close();
//            }
//
//            if (success)
//            {
//                event.getMessages().add(sm.getString("ZapisanoZmiany"));
//                event.setForward(new ActionForward(UserListAction.getLink((String) form.get("divisionGuid"))));
//            }
//        }
//    }
//
//    private final class Delete implements ActionListener
//    {
//        public void actionPerformed(ActionEvent event)
//        {
//            final DynaActionForm form = event.getDynaForm();
//            final String identifier = (String) form.get("identifier");
//            final String email = (String) form.get("email");
//            final String firstname = (String) form.get("firstname");
//            final String lastname = (String) form.get("lastname");
//            final String phoneNum = (String) form.get("phoneNum");
//            final String mobileNum = (String) form.get("mobileNum");
//
//            boolean success = false;
//            try
//            {
//                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
//                ctx.begin();
//                boolean subst = DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA);
//
//                DSUser user = DSUser.findByUsername((String) form.get("username"));
//
//                SubstituteHistory history=new SubstituteHistory(user.getName(),user.getSubstitutedFrom(),DateUtils.getCurrentTime(),user.getSubstituteUser().getName());
//                ctx.session().save(history);
//                user.cancelSubstitution();
//
//                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
//                cache.update(user.getName());
//
//                ctx.commit();
//                success = true;
//            }
//            catch (EdmException e)
//            {
//                DSApi.context().setRollbackOnly();
//                event.getErrors().add(e.getMessage());
//            }
//            catch(NullPointerException e)
//            {
//                DSApi.context().setRollbackOnly();
//                event.getErrors().add(e.getMessage());
//            }
//            finally
//            {
//                DSApi._close();
//            }
//
//            if (success)
//            {
//                event.getMessages().add(sm.getString("ZapisanoZmiany"));
//                event.setForward(new ActionForward(UserListAction.getLink((String) form.get("divisionGuid"))));
//            }
//        }
//    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final DynaActionForm form = event.getDynaForm();
           
            try
            {
            	DSApi.open(AuthUtil.getSubject(event.getRequest()));
                DSApi.context().begin();
                String username = getUsername(form);
                	
                
                boolean condCanEditSubst = mySubstitude || DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA); 
//                if (DocumentKind.getAvailableDocumentKindsCn().contains(DocumentKind.PROSIKA)) {
//                	condCanEditSubst = condCanEditSubst && DSApi.context().hasPermission(DSPermission.);
//                }
                event.getRequest().setAttribute("canEditSubstitutions", Boolean.valueOf(condCanEditSubst));

                final DSUser user = DSUser.findByUsername(username);

                form.set("username", user.getName());
                form.set("externalName", user.getExternalName());
                form.set("firstname", user.getFirstname());
                form.set("lastname", user.getLastname());
                form.set("identifier", user.getIdentifier());
                form.set("email", user.getEmail());
                form.set("supervisor", user.getSupervisor() != null ? user.getSupervisor().getName() : null);
                //zmiana
                form.set("phoneNum", user.getPhoneNum());
                form.set("mobileNum", user.getMobileNum());

                if (user.getSubstituteUser() != null){
                	form.set("substitutedBy", user.getSubstituteUser().getName());
                    form.set("substitutedThrough", user.getSubstitutedThrough() != null ?
                        DateUtils.formatJsDate(user.getSubstitutedThrough()) : null);
                    form.set("substitutedFrom", user.getSubstitutedFrom() != null ?
                        DateUtils.formatJsDate(user.getSubstitutedFrom()) : null);
                    event.getRequest().setAttribute("substituted",true);
                }

                // dzia�y, w kt�rych znajduje si� u�ytkownik (z wyj�tkiem grup)
                final DSDivision[] divisions = user.getDivisions();
                final Collection divisionNames = fun.map(
                    divisions, new lambda()
                {
                    public Object act(Object o)
                    {
                        DSDivision division = (DSDivision) o;
                        if (division.isGroup())
                            throw new NoSuchElementException();
                        try
                        {
                            return division.getPrettyPath();
                        }
                        catch (EdmException e)
                        {
                            throw new NoSuchElementException();
                        }
                    }
                });

                event.getRequest().setAttribute("divisionNames", divisionNames);

                // lista wszystkich u�ytkownik�w z wyj�tkiem edytowanego
                Collection supervisors =
                    fun.map(DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true).results(),
                        new lambda()
                        {
                            public Object act(Object o)
                            {
                                DSUser u = (DSUser) o;
                                if (u.getName().equals(user.getName()))
                                    throw new NoSuchElementException();
                                DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
                                bean.set("label", u.asLastnameFirstname());
                                bean.set("value", u.getName());
                                return bean;
                            }
                        });


                // wszyscy u�ytkownicy z wyj�tkiem zast�powanego i tych,
                // kt�rych zast�puje ju� bie��cy u�ytkownik
                Collection substitutedBys =
                    fun.map(
                        DSUser.search(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true, null).results(),
                        new lambda()
                        {
                            public Object act(Object o)
                            {
                                DSUser u = (DSUser) o;
                                try
                                {
                                    if (onlySubstitutionFromSameGroup)
                                    {
                                        if (!isUserInThisDivision(u) || u.getName().equals(user.getName()) ||
                                                (u.getSubstituteUser() != null && u.getSubstituteUser().getName().equals(user.getName())))
                                            throw new NoSuchElementException();
                                    }
                                    else if (u.getName().equals(user.getName()) ||
                                        (u.getSubstituteUser() != null && u.getSubstituteUser().getName().equals(user.getName())))
                                        throw new NoSuchElementException();
                                }
                                catch (EdmException e)
                                {
                                    throw new NoSuchElementException();
                                }

                                DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);

                                String label = u.asLastnameFirstname();
                                if (AvailabilityManager.isAvailable("substitutions.user-list.add-division-description")) {
                                    String divisionDescription = "";
                                    try {
                                        divisionDescription = StringUtils.left(Joiner.on(", ").join(FluentIterable.from(Lists.newArrayList(u.getDivisionsWithoutGroup())).transform(new Function<DSDivision, String>() {
                                            @Override
                                            public String apply(DSDivision input) {
                                                return StringUtils.left(input.getName(), 60);
                                            }
                                        })), 150);
                                    } catch (EdmException e) {
                                        log.error(e.getMessage(), e);
                                    }
                                    label +=  " / " + divisionDescription;
                                }

                                bean.set("label", label);
                                bean.set("value", u.getName());
                                return bean;
                            }
                        });

                // atrybut supervisors - lista prze�o�onych
                event.getRequest().setAttribute("supervisors", supervisors);


                // atrybut substitutedBys - lista bean�w htmlOption
                event.getRequest().setAttribute("substitutedBys", substitutedBys);

                // grupy, do kt�rych mo�na doda� u�ytkownika - tylko te
                // znajduj�ce si� na najwy�szym poziomie drzewa organizacji

                DSDivision[] groups = DSDivision.find(DSDivision.ROOT_GUID).getChildren(new DSDivisionFilter()
                {
                    public boolean accept(DSDivision division)
                    {
                        return division.isGroup();
                    }
                });

                event.getRequest().setAttribute("groups", groups);


                // wszystkie role
                Role[] roles = Role.list();
                Collection roleBeans = fun.map(
                    roles, new lambda()
                {
                    public Object act(Object o)
                    {
                        Role role = (Role) o;
                        DynaBean bean = DynaBeans.bean(DynaBeans.checkbox);
                        bean.set("value", role.getId());
                        bean.set("label", role.getName());
                        //bean.set("checked", Boolean.valueOf(role.getUsernames().contains(user.getName())));
                        return bean;
                    }
                });

                event.getRequest().setAttribute("officeRoles", roleBeans);

                // role, w kt�rych u�ytkownik si� znajduje
                Long[] roleIds = (Long[]) fun.map(
                    roles, new lambda()
                    {
                        public Object act(Object o)
                        {
                            Role role = (Role) o;
                            if (role.getUsernames().contains(user.getName()))
                                return role.getId();
                            else
                                throw new NoSuchElementException();
                        }
                    }).toArray(new Long[0]);

                form.set("officeRoles", roleIds);


                DSUser[] substitutedUsers = user.getSubstituted();
                List substitutedBeans = new ArrayList(substitutedUsers.length);

                for (int i=0, n=substitutedUsers.length; i < n; i++)
                {
                    DynaBean bean = DynaBeans.newHtmlLink();
                    bean.set("link", UserEditAction.getLink(substitutedUsers[i].getName(), (String) form.get("divisionGuid")));
                    bean.set("title", substitutedUsers[i].asLastnameFirstnameName());
                        //" do "+DateUtils.formatJsDateTime(substitutedUsers[i].getSubstitutedThrough()));
                    substitutedBeans.add(bean);
                }

                event.getRequest().setAttribute("substitutedUsers", substitutedBeans);
                
                Date now = new Date();
                Query queryHistBy =  DSApi.context().session().createQuery("from SubstituteHistory where (login=?) and (substitutedThrough < ?)");
                queryHistBy.setString(0, username);
                queryHistBy.setDate(1, now);
                histBy=(ArrayList) queryHistBy.list();
                
                Query queryHist =  DSApi.context().session().createQuery("from SubstituteHistory where (substLogin=?) and (substitutedThrough < ?)");
                queryHist.setString(0, username);
                queryHist.setDate(1, now);
                hist=(ArrayList) queryHist.list();
                
                Query queryAcctualSubst =  DSApi.context().session().createQuery("from SubstituteHistory where (login=?) and (substitutedThrough >= ?) and (status <> ?)");
                queryAcctualSubst.setString(0, username);
                queryAcctualSubst.setDate(1, now);
                queryAcctualSubst.setInteger(2, SubstituteHistory.DELETED);
                acctualSubst =(ArrayList) queryAcctualSubst.list();

                DynaBean[] beans = null;
                DynaBean[] beansBy = new DynaBean[histBy.size()];

                DynaBean[] acctualSubstbeans = new DynaBean[acctualSubst.size()];

                for (int i=0, n=acctualSubst.size(); i < n; i++)
                {
                    DynaBean bean = DynaBeans.bean(DynaBeans.substituteHistory);
                    bean.set("login", ((SubstituteHistory)(acctualSubst.get(i))).getLogin());
                    bean.set("substId", ((SubstituteHistory)(acctualSubst.get(i))).getId());
                    bean.set("substitutedThrough", ((SubstituteHistory)(acctualSubst.get(i))).getSubstitutedThrough());
                    bean.set("substitutedFrom", ((SubstituteHistory)(acctualSubst.get(i))).getSubstitutedFrom());
                    bean.set("substLogin", ((SubstituteHistory)(acctualSubst.get(i))).getSubstLogin());
                    bean.set("username", DSUser.findByUsername(((SubstituteHistory)(acctualSubst.get(i))).getLogin()).asFirstnameLastnameName());
                    bean.set("substUsername", DSUser.findByUsername(((SubstituteHistory)(acctualSubst.get(i))).getSubstLogin()).asFirstnameLastnameName());
                    bean.set("substituteCreator", DSUser.findByUsername(((SubstituteHistory)(acctualSubst.get(i))).getSubstituteCreator()).asFirstnameLastnameName());
                    bean.set("status", SubstituteHistory.getStatusMap().get(((SubstituteHistory)(acctualSubst.get(i))).getStatus()));
                    
                    acctualSubstbeans[i] = bean;
                }
                
                if(hist != null)
                {
                	beans = new DynaBean[hist.size()];
                
                    for (int i=0, n=hist.size(); i < n; i++)
                    {
                        DynaBean bean = DynaBeans.bean(DynaBeans.substituteHistory);
                        bean.set("login", ((SubstituteHistory)(hist.get(i))).getLogin());
                        bean.set("substitutedThrough", ((SubstituteHistory)(hist.get(i))).getSubstitutedThrough());
                        bean.set("substitutedFrom", ((SubstituteHistory)(hist.get(i))).getSubstitutedFrom());
                        bean.set("substLogin", ((SubstituteHistory)(hist.get(i))).getSubstLogin());
                        bean.set("username", DSUser.findByUsername(((SubstituteHistory)(hist.get(i))).getLogin()).asFirstnameLastnameName());
                        bean.set("substUsername", DSUser.findByUsername(((SubstituteHistory)(hist.get(i))).getSubstLogin()).asFirstnameLastnameName());
                        bean.set("substituteCreator", DSUser.findByUsername(((SubstituteHistory)(hist.get(i))).getSubstituteCreator()).asFirstnameLastnameName());
						bean.set("status", SubstituteHistory.getStatusMap().get(((SubstituteHistory)(hist.get(i))).getStatus()));
						Date cancelDate = ((SubstituteHistory)(hist.get(i))).getCancelDate();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String cancelDateString = cancelDate == null ? "-" : sdf.format(cancelDate);
						bean.set("cancelDate", cancelDateString);
						
						beans[i] = bean;
                    }     
                }
                
                for (int i=0, n=histBy.size(); i < n; i++)
                {
                    DynaBean bean = DynaBeans.bean(DynaBeans.substituteHistory);
                    bean.set("login", ((SubstituteHistory)(histBy.get(i))).getLogin());
                    bean.set("substitutedThrough", ((SubstituteHistory)(histBy.get(i))).getSubstitutedThrough());
                    bean.set("substitutedFrom", ((SubstituteHistory)(histBy.get(i))).getSubstitutedFrom());
                    bean.set("substLogin", ((SubstituteHistory)(histBy.get(i))).getSubstLogin());
                    bean.set("username", DSUser.findByUsername(((SubstituteHistory)(histBy.get(i))).getLogin()).asFirstnameLastnameName());
                    bean.set("substUsername", DSUser.findByUsername(((SubstituteHistory)(histBy.get(i))).getSubstLogin()).asFirstnameLastnameName());
                    bean.set("substituteCreator", DSUser.findByUsername(((SubstituteHistory)(histBy.get(i))).getSubstituteCreator()).asFirstnameLastnameName());
					bean.set("status", SubstituteHistory.getStatusMap().get(((SubstituteHistory)(histBy.get(i))).getStatus()));
					Date cancelDate = ((SubstituteHistory)(histBy.get(i))).getCancelDate();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String cancelDateString = cancelDate == null ? "-" : sdf.format(cancelDate);
					bean.set("cancelDate", cancelDateString);

					beansBy[i] = bean;
                }

                event.getRequest().setAttribute("hist", beans);
                event.getRequest().setAttribute("histBy", beansBy);
                event.getRequest().setAttribute("acctualSubst", acctualSubstbeans);


                DSApi.context().commit();                
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(), e);
                event.getErrors().add(e.getMessage());
            	DSApi.context().setRollbackOnly();
            }
            finally
            {
                DSApi._close();
            }
        }

        private boolean isUserInThisDivision(DSUser u)
        {
            try
            {
                for (DSDivision div : u.getDivisions())
                {
                    if (div.getGuid().equals(divisionGuid))
                        return true;
                }
            }
            catch (EdmException e)
            {
                log.error(e.getMessage(), e);
                return false;
            }
            return false;
        }
    }

    public static String getLink(String username, String divisionGuid)
    {
        final ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
            "?username="+username +
            (divisionGuid != null ? "&divisionGuid="+divisionGuid : "");
    }

    public String getBaseLink(){
    	ModuleConfig config = (ModuleConfig)Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
    	ForwardConfig fc = config.findForwardConfig(FORWARD);
    	return fc.getPath();
        //return "/docusafe/office/users/user-edit.do";
    }

	public Long getSubstId() {
		return substId;
	}

	public void setSubstId(Long substId) {   
		this.substId = substId;
	}

}
