package pl.compan.docusafe.web.office.users;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NewUsersFromXlsAction extends EventActionSupport
{
	private StringManager sm = GlobalPreferences.loadPropertiesFile(NewUsersFromXlsAction.class.getPackage().getName(),null);
	protected final Logger log = LoggerFactory.getLogger(NewUsersFromXlsAction.class);
    private FormFile file;
    
	protected void setup()
	{
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doImport").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Import()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
 
    public void setFile(FormFile file)
	{
		this.file = file;
	}

	public FormFile getFile()
	{
		return file;
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
        	try 
        	{
        		
        	}
        	catch (Exception e) 
        	{
        		log.error("",e);
				addActionError(e.getMessage());
			}
        }
    }
    
    private class Import implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
        	int n = 0;
        	POIFSFileSystem fs = null;
        	HSSFWorkbook wb = null;        	
        	try 
        	{
        		if(file == null)
					throw new ValidationException("Nie wybrano pliku");
				
				File xlsfile = file.getFile();
				InputStream is = new FileInputStream(xlsfile);
			    fs = new POIFSFileSystem( is );
	            wb = new HSSFWorkbook(fs);	            
	            HSSFSheet sheet = wb.getSheetAt(0);
	            //Iterator<HSSFRow> rowsIter = sheet.rowIterator();
	            Iterator<Row> rowsIter = sheet.rowIterator();
	            //ominiecie pierwszego
	            rowsIter.next();	            
	            while(rowsIter.hasNext())
				{
	            	n++;
	            	//HSSFRow row = rowsIter.next();
	            	Row row = rowsIter.next();
	            	String username = getValueFromCell(row.getCell(0));
	            	String firstname = getValueFromCell(row.getCell(2));
	            	String lastname = getValueFromCell(row.getCell(3));
	            	try
	            	{
						DSApi.context().begin();
						DSUser user = UserFactory.getInstance().createUser(username, firstname, lastname);
		                user.setEmail(getValueFromCell(row.getCell(1)));
		                user.setPassword(getValueFromCell(row.getCell(4)));
		                user.setKpx(getValueFromCell(row.getCell(5)));;
		                user.setPhoneNum(getValueFromCell(row.getCell(6)));
		                user.setMobileNum(getValueFromCell(row.getCell(7)));	       
		                user.setCtime(new Timestamp(new Date().getTime()));			
						DSApi.context().commit();
						addActionMessage(sm.getString("DodalUzytkownika",user.asFirstnameLastnameName()));
	            	}
	            	catch (Exception e) 
	            	{
	            		log.error("",e);
	            		DSApi.context().rollback();
	            		addActionError("B��d w lini "+n+" :"+e.getMessage());
					}
				}
        	}
        	catch (Exception e) 
        	{
        		log.error("",e);
        		DSApi.context()._rollback();
			}
        	finally
        	{
        		
        	}
        }
    }
    
    private String getValueFromCell(Cell c)
    {
    	if(c == null)
    		return "";
    	if(c.CELL_TYPE_NUMERIC == c.getCellType())
    	{
    		Double d = c.getNumericCellValue();
    		return ""+d.intValue();
    	}
    	return c.toString();
    }
    
    private String getValueFromCell(HSSFCell c)
    {
    	if(c == null)
    		return "";
    	if(c.CELL_TYPE_NUMERIC == c.getCellType())
    	{
    		Double d = c.getNumericCellValue();
    		return ""+d.intValue();
    	}
    	return c.toString();
    }
}
