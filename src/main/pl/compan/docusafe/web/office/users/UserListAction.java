package pl.compan.docusafe.web.office.users;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.common.event.SetActionForwardListener;
import pl.compan.docusafe.web.tree.OrganizationTree;

import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserListAction.java,v 1.15 2009/04/06 08:33:44 kubaw Exp $
 */
public final class UserListAction extends EventProcessingAction
{

    private static final String FORWARD = "office/users/user-list";

    protected void setup()
    {
        final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doUpdate").
            append(setForward).
            append(new Update()).
            append("fillForm", fillForm);
    }

    private final class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            final DynaActionForm form = event.getDynaForm();

            try
            {
                final DSContext ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();


                ctx.commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
           final DynaActionForm form = event.getDynaForm();
            final String guid = (String) event.getDynaForm().get("divisionGuid");

            try
            {
                DSApi.open(AuthUtil.getSubject(event.getRequest()));
                String userProfileKeys = Docusafe.getAdditionProperty("userProfileKeys");
                boolean canEditSubstitutions = DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA);
                event.getRequest().setAttribute(
                    "canEditSubstitutions",
                    Boolean.valueOf(DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA)));
                if (canEditSubstitutions)
                {
                    DSDivision division = null;

                    if (!StringUtils.isEmpty(guid))
                    {
                        try
                        {
                            division = DSDivision.find(guid);
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }

                    if (division == null)
                    {
                        division = DSDivision.find(DSDivision.ROOT_GUID);
                    }

                    UrlVisitor urlVisitor = new UrlVisitor()
                    {
                        public String getUrl(Object element)
                        {
                            return event.getRequest().getContextPath()+
                                getLink(((DSDivision) element).getGuid());
                        }
                    };

                    boolean canEditOnlyMyDivision = AvailabilityManager.isAvailable("substitution.only.my.division") && !DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA_WSZYSTKICH);

                    //if (DocumentKind.getAvailableDocumentKindsCn().contains(DocumentKind.PROSIKA)) {
                    if(canEditOnlyMyDivision) {
                        //wyswietlam tylko strukture z pionami w ktorych jest uzytkownik, patrz newTree
                        DSUser user = DSApi.context().getDSUser();
                        DSDivision[] userDivs = user.getDivisions();
                        List<DSDivision> userDivsParents = new LinkedList<DSDivision>();
                        for (DSDivision div : userDivs) {
                            DSDivision tmp = div;
                            while (!tmp.isRoot()) {
                                userDivsParents.add(tmp);
                                tmp = tmp.getParent();
                            }
                        }
                        event.getRequest().setAttribute("tree",
                                OrganizationTree.newTree(division, userDivsParents, urlVisitor, event.getRequest(),
                                    true, false, false).generateTree());

                    } else {

                        event.getRequest().setAttribute("tree",
                            OrganizationTree.newTree(division, urlVisitor, event.getRequest(),
                                true, true, false).generateTree());
                    }

                    // użytkownicy

                    if (division != null)
                    {
                        List<DSUser> listusers;
                        boolean canEditSubstitutionsInDivision = DSApi.context().hasPermission(DSPermission.ZASTEPSTWA_EDYCJA, division.getGuid());
                        if (userProfileKeys!= null && Boolean.valueOf(userProfileKeys) && canEditSubstitutionsInDivision)
                        {
                            listusers = new ArrayList(Arrays.asList(division.getOriginalUsers()));
                        }
                        else if (userProfileKeys== null || !Boolean.valueOf(userProfileKeys))
                        {
                            if(canEditOnlyMyDivision
                                    && division.isInAnyChildDivision(DSApi.context().getDSUser()))
                            {
                                if(AvailabilityManager.isAvailable("dekretacja.userlist.poddzialy"))
                                    listusers = new ArrayList(Arrays.asList(division.getUsers()));
                                else
                                    listusers = new ArrayList(Arrays.asList(division.getOriginalUsers()));
                            }
                            else if (!canEditOnlyMyDivision)
                                listusers = new ArrayList(Arrays.asList(division.getOriginalUsers()));
                            else
                                return;
                        }
                        else
                            return;

                        Collections.sort(listusers, DSUser.LASTNAME_COMPARATOR);
                        DynaBean[] newbeans = new DynaBean[listusers.size()];
                        int i = 0;
                        for(DSUser user : listusers)
                        {
                            //LoggerFactory.getLogger("tomekl").debug(user.getName());
                            DynaBean bean = DynaBeans.newHtmlLink();
                            bean.set("link", UserEditAction.getLink(user.getName(), division.getGuid()));
                            bean.set("title", user.asLastnameFirstname());
                            newbeans[i] = bean;
                            i++;
                        }


                        /*DSUser[] users = division.getUsers();
                        DynaBean[] beans = new DynaBean[users.length];

                        for (int i=0, n=users.length; i < n; i++)
                        {
                            DynaBean bean = DynaBeans.newHtmlLink();
                            bean.set("link", UserEditAction.getLink(users[i].getName(), division.getGuid()));
                            bean.set("title", users[i].asFirstnameLastnameName());
                            beans[i] = bean;
                        }
                        */
                        event.getRequest().setAttribute("userBeans", newbeans);
                    }
                }
            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    public static String getLink(String divisionGuid)
    {
        final ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        final ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
            (divisionGuid != null ? "?divisionGuid="+divisionGuid : "");
    }
}
