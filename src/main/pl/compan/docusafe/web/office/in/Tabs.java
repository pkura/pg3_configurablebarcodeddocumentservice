package pl.compan.docusafe.web.office.in;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.DpLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Tabs.java,v 1.56 2009/12/12 14:03:40 wkuty Exp $
 */
public class Tabs
{
    private static final Logger log = LoggerFactory.getLogger(Tabs.class);

    private DocumentTabAction base;

    public Tabs(DocumentTabAction base)
    {
        log.info("Tabs  from: {}", base.getClass().getSimpleName());
        this.base = base;
    }

    public List<Tab> createTabs()
    {
        List<Tab> tabs = new ArrayList<Tab>(11);
        StringManager smTab = GlobalPreferences.loadPropertiesFile("","tab") ;
        String[] params = new String[] {
            "documentId", String.valueOf(base.getDocumentId()),
            "activity", base.getActivity(),
            "process", base.getProcess()};

        if (base.getDocumentId() != null)
        {
        	 if (AvailabilityManager.isAvailable("zadanie.podsumowanie"))
        	 {
	            tabs.add(new Tab(smTab.getString("Podsumowanie"), smTab.getString("PodsumowanieInfromacjiOpismie"),
	                HttpUtils.makeUrl("/office/incoming/summary.action", params),
	                base instanceof SummaryAction));
        	 }

            if (AvailabilityManager.isAvailable("sprawy"))
            {
                tabs.add(new Tab(smTab.getString("Sprawa"),smTab.getString("ZalozSpraweDoPisma"),
                    HttpUtils.makeUrl("/office/incoming/case.action", params),
                    base instanceof CaseAction));
            }

            if ((Configuration.simpleOfficeAvailable() || Configuration.officeAvailable()) && AvailabilityManager.isAvailable("zadanie.odpowiedzi"))
            {
                tabs.add(new Tab(smTab.getString("Odpowiedzi"),smTab.getString("OdpowiedziDoPisma"),
                    HttpUtils.makeUrl("/office/incoming/replies.action", params),
                    base instanceof RepliesAction));
            }

            if (AvailabilityManager.isAvailable("kancelaria.zalaczniki", null, base.getActivity()) && (Configuration.officeAvailable() || Configuration.faxAvailable()))
            {
                tabs.add(new Tab(smTab.getString("Zalaczniki"), smTab.getString("ZobaczIdodajZalaczniki"),
                    HttpUtils.makeUrl("/office/incoming/attachments.action", params),
                    base instanceof AttachmentsTabAction));
            }

            if (AvailabilityManager.isAvailable("zadanie.uwagi"))
       	 	{
            	String count = "";
            	if (AvailabilityManager.isAvailable("showRemarksCount"))
            	{
            		int size;
					try {
						size = Document.find(base.getDocumentId()).getRemarks().size();
						if ( size > 0)
	            			count = " (" + size + ")";
					} catch (Exception e) {
						log.info(e.getMessage());
					}
            	}

	            tabs.add(new Tab(smTab.getString("Uwagi" + count), smTab.getString("PrzeczytajIdodajUwagiDoPisma"),
	                    HttpUtils.makeUrl("/office/incoming/remarks.action", params),
	                    base instanceof RemarksTabAction));
       	 	}

            if(AvailabilityManager.isAvailable("electronicSignature.document") || AvailabilityManager.isAvailable("electronicSignature.attachment")){
                tabs.add(new Tab(smTab.getString("Podpisy"), smTab.getString("ListaPodpisowDlaTegoDokumentu"),
	                    HttpUtils.makeUrl("/office/incoming/electronic-signatures.action", params),
	                    base instanceof ElectronicSignaturesTabAction));
            }

			if(!AvailabilityManager.isAvailable("kancelaria.ogolne.hidden")) {

            	tabs.add(new Tab(smTab.getString("Ogolne"),smTab.getString("OgolneInformacjeOpismie"),
            			HttpUtils.makeUrl("/office/incoming/main-redirect.action", params),
            			base instanceof MainAction));
            }

//            if (AvailabilityManager.isAvailable("kancelaria.archiwizacja.dwr")) {
//                log.info("kancelaria.archiwizacja.dwr");
//                tabs.add(new Tab(smTab.getString("Archiwizacja"), smTab.getString("ArchiwizacjaPisma"),
//                            HttpUtils.makeUrl("/office/incoming/dwr-document-main.action", params),
//                            base instanceof DwrDocumentMainAction));
//            } else 
			if (AvailabilityManager.isAvailable("kancelaria.archiwizacja") /* && Configuration.hasExtra("business") */) {

				try {
					Document document= Document.find(base.getDocumentId());
					if ( (document.getDocumentKind()!=null && (document.getDocumentKind().getCn().equals(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY)
							|| document.getDocumentKind().getCn().equals(DocumentLogicLoader.PROTOKOL_BRAKOWANIA)))
							|| Configuration.hasExtra("business")) 
					{
						log.info("kancelaria.archiwizacja");
						if (document.getDocumentKind() != null
								&& !document.getDocumentKind().logic().needsEditDoctypeAction())
							tabs.add(new Tab(smTab.getString("Archiwizacja"), smTab.getString("ArchiwizacjaPisma"),
									HttpUtils.makeUrl("/office/incoming/document-archive.action", params),
									base instanceof DocumentArchiveTabAction
											|| base instanceof DwrDocumentMainTabAction));
					}
				} catch (EdmException e) {
				}
			}
            	

            tabs.add(new Tab(smTab.getString("HistoriaPisma"), smTab.getString("HistoriaPracyNadPismem"),
                HttpUtils.makeUrl("/office/incoming/work-history.action", params),
                (base instanceof WorkHistoryTabAction) ));
            
            if(AvailabilityManager.isAvailable("kancelaria.zarzadzanieUmowa"))
            {
                boolean isDP = false;
                 try{
                    Document document = Document.find(base.getDocumentId());
                    
                    if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.DP_KIND))
                    {
                        FieldsManager fm = document.getFieldsManager();
                        if(fm.getKey(DpLogic.KLASA_FIELD_CN).equals(10))
                        {
                            isDP = true;
                        }
                    }
                    
                }catch(Exception e){
                    
                    log.error("Tab.class:134", e);
                }

                if(isDP){
                    tabs.add(new Tab(smTab.getString("ZarzadzanieUmowa"), smTab.getString("DaneDotyczaceZarzadzaniaUmowa"),
                        HttpUtils.makeUrl("/office/incoming/contract-managment.action", params),
                        (base instanceof ContractManagmentTabAction) ));
                }
            }
            
            if (AvailabilityManager.isAvailable("kancelaria.historiaDekretacji") && (Configuration.officeAvailable() || Configuration.faxAvailable()))
            {
                tabs.add(new Tab(smTab.getString("HistoriaDekretacji"), smTab.getString("HistoriaDekretacji"),
                    HttpUtils.makeUrl("/office/incoming/assignment-history.action", params),
                    base instanceof AssignmentHistoryTabAction));
            }

            boolean dekretacjaPermission = false;
            Long docuId = base.getDocumentId();
            String docCn = "";
            try {
        	dekretacjaPermission = DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA);
	    	Document doc = Document.find(docuId);
	    	docCn = doc.getDocumentKind().getCn();
            } catch (EdmException e) {
        	log.warn(e.getMessage());
            }
            
            if (AvailabilityManager.isAvailable("kancelaria.dekretacja", null, base.getActivity()) && Configuration.officeAvailable() && dekretacjaPermission
            		 && !AvailabilityManager.isAvailable("canNotDecree", docCn))
            {
                try
                {
                    if (!StringUtils.isEmpty(base.getActivity()))
                    {
                        if(WorkflowFactory.jbpm && AvailabilityManager.isAvailable("manual-multi-assignment")) {
                            tabs.add(new Tab(smTab.getString("DekretacjaReczna"), smTab.getString("DekretacjaReczna"),
                                HttpUtils.makeUrl("/office/incoming/manual-multi-assignment.action", params),
                                base instanceof ManualMultiAssignmentTabAction));
                        }
                        else if(WorkflowFactory.jbpm){
                            tabs.add(new Tab(smTab.getString("DekretacjaReczna"), smTab.getString("DekretacjaReczna"),
                                HttpUtils.makeUrl("/office/incoming/manual-assignment.action", params),
                                base instanceof ManualAssignmentTabAction));
                        } else {
                            tabs.add(new Tab(smTab.getString("Dekretacja"), smTab.getString("Dekretacja"),
                                HttpUtils.makeUrl("/office/incoming/workflow.action", params),
                                base instanceof WorkflowAction));
                        }
                    }
                    else
                    {
                        // pobieranie wszystkich zada� workflow tego u�ytkownika,
                        // kt�re s� zwi�zane z bie��cym dokumentm

                            String[] activityIds = WorkflowFactory.findDocumentTasks(base.getDocumentId());
                            // link do accept-assignment
                            if (activityIds.length == 1)
                            {
                            	WorkflowActivity activity = WorkflowFactory.getWfActivity(activityIds[0]);
                                // link do bie��cej zak�adki (Dekretacja)
                                String thisUrl = HttpUtils.makeUrl("/office/incoming/workflow.action",
                                    new String[] {
                                        "documentId", String.valueOf(base.getDocumentId()),
                                        "activity", activity.getTaskFullId() } );

                                if (activity.getAccepted())
                                {
                                    tabs.add(new Tab(smTab.getString("Dekretacja"), smTab.getString("Dekretacja"),
                                        thisUrl,
                                        base instanceof WorkflowAction));
                                }
                                else
                                {
                                    // normalnie accept-wf-assignment przekieruje u�ytkownika
                                    // do strony summary pisma, parametr redirect powoduje,
                                    // �e u�ytkownik b�dzie przekierowany do bie��cej strony,
                                    // tzn. "Dekretacja"
                                    tabs.add(new Tab(smTab.getString("Dekretacja"), smTab.getString("Dekretacja"),
                                        HttpUtils.makeUrl("/office/accept-wf-assignment.action",
                                            new String[] {
                                                "doAccept", "true",
                                                "activity", activity.getTaskFullId(),
                                                "redirectUrl", thisUrl } ),
                                        base instanceof WorkflowAction));
                                }
                            }
                            else if (activityIds.length > 1)
                            {
                                // u�ytkownik ma kilka zada� zwi�zanych z tym pismem, nale�y
                                // pokaza� mu list� tych zada� i pozwoli� na wyb�r jednego z nich

                                // w nag��wku b�dzie numer KO pisma,
                            }
                    }
                }
                catch (EdmException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
            
            if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu", null, base.getActivity())){
            	
            		tabs.add(new Tab(smTab.getString("Wersja dokumentu"), smTab.getString("Wersja dokumentu"),
            				HttpUtils.makeUrl("/office/incoming/document-version.action", params),
            				base instanceof DocumentVersionTabAction));
            }

            if(AvailabilityManager.isAvailable("kancelaria.wlacz.jackrabbit.wersjeDokumentu", null, base.getActivity())){

                tabs.add(new Tab(smTab.getString("Wersje dokument�w"), smTab.getString("Wersje dokument�w z Jackrabbita"),
                        HttpUtils.makeUrl("/office/incoming/document-version-jcr.action", params),
                        base instanceof DocumentVersionJcrTabAction));
            }

            
            if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) 
            {
                tabs.add(new Tab(smTab.getString("Podpisy"), smTab.getString("Podpisy"),
                        HttpUtils.makeUrl("/office/incoming/signatures.action", params),
                        base instanceof SignaturesAction));
            }

            if(AvailabilityManager.isAvailable("kancelaria.szablony")) {
                tabs.add(new Tab(smTab.getString("Szablony"),smTab.getString("Szablony"),
                        HttpUtils.makeUrl("/office/incoming/templates.action", params),
                        base instanceof TemplatesAction));
            }
			
			if(AvailabilityManager.isAvailable("process-diagram")) {
                tabs.add(new Tab(smTab.getString("process-diagram"),smTab.getString("process-diagram"),
                        HttpUtils.makeUrl("/office/incoming/process-diagram.action", params),
                        base instanceof ProcessDiagramAction));
            }

        }

        return tabs;
    }
}

