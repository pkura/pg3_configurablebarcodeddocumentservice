package pl.compan.docusafe.web.office.in;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentKindRequiredAttachment;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.bip.Bip;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Uwagi dla os�b aktualizuj�cych t� klas�: <ul>
 * <li>Pola formularza niezale�ne od dokumentu (lub jego braku) powinny
 *   by� wype�niane w klasie {@link pl.compan.docusafe.web.office.in.BokAction.FillPulldowns}.
 * </ul>
 *
 * Po dodaniu pola w main.jsp nale�y doda� je te� w main-user-reference-id.jsp
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: BokAction.java,v 1.27 2010/07/27 11:26:22 kamilj Exp $
 */
public class BokAction extends BaseTabsAction
{
	private static Logger log = LoggerFactory.getLogger(BokAction.class);
    // klasa w czesici przerobiona na wersje jezykowa
    // @EXPORT (tylko odczytywane przez formularz lub xwork.xml, nie modyfikowane)
    private Boolean canExportToBip;
    private List kinds;
    private List users;
    private List outgoingDeliveries;
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private Map recipientsMap;
    private InOfficeDocumentKind kind;
    private OutOfficeDocumentDelivery outgoingDelivery;
    private String creatingUser;
    private Map journalMessages;
    private String trackingNumber;
    private String caseDocumentId;
    private String assignedDivision;
    private InOfficeDocument document;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private boolean currentDayWarning;

    // @IMPORT (tylko ustawiane przez formularz)
    private Long incomingJournalId;

    // @IMPORT/EXPORT
    private Integer kindId; // export dla main-used-reference-id
    private Integer outgoingDeliveryId;
    private String documentDate;
    private boolean submitToAsiBip;
    private String description;
    private String location;
    private boolean senderAnonymous;
    private String senderTitle;
    private String senderFirstname;
    private String senderLastname;
    private String senderOrganization;
    private String senderStreetAddress;
    private String senderZip;
    private String senderLocation;
    private String senderCountry;
    private String senderEmail;
    private String senderFax;
    private String senderPesel;
    private String senderNip;
    private String senderRegon;
    /**
     * Warto�� ustawiana po wys�aniu formularza przez u�ytkownika.
     * Warto�ciami s� wyra�enia OGNL tworz�ce obiekty java.util.Map
     * opisuj�ce odbiorc�w pisma.
     */
    private String[] recipients;
    private String otherRemarks;
    private int numAttachments;
    private String assignedUser;
    private String barcode;

    private boolean bipEnabled;

    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_CREATE = "create";

    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    //public static final String CREATED = "created"; // atrybut ActionEvent

    /*
        Tworzenie zak�adek w otwartej sesji Hibernate.
        Je�eli nie ma parametru activityId, szukane s� wszystkie zadania
        u�ytkownika zwi�zane z tym pismem. Je�eli tylko jedno, dawany
        jest link do dokumentu z tym activityId (zak�adka workflow).
        Je�eli wi�cej ni� jedno, strona przej�ciowa z tymi zadaniami.
        Je�eli brak, nie dajemy zak�adki Dekretacja.

    */

    public String getBaseLink()
    {
        return "/office/incoming/main.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    protected void setup()
    {
        // sta�e elementy formularza (listy rozwijane)
        FillPulldowns fillPulldowns = new FillPulldowns();
        // dane z bie��cego dokumentu
        FillDocumentForm fillDocumentForm = new FillDocumentForm();

        // ogl�danie istniej�cego dokumentu
        registerListener(DEFAULT_ACTION).
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        // tworzenie nowego dokumentu
        registerListener("doNewDocument").
            //append(OpenHibernateSession.INSTANCE).
            append(new FillNewDocumentForm()).
            append(fillPulldowns);
            //append(fillDocumentForm).
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            //appendIf("event.getAttribute('created') != null", new FillCreateForm()).
            // FillCreate tylko wtedy, gdy tworzenie dokumentu si� nie powiedzie
            append(EV_FILL_CREATE, new FillCreateForm()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate"). 
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new Update()).
            // FillCreate tylko wtedy, gdy aktualizacja dokumentu si� nie powiedzie
            // append(and().append(FillCreate()).append(FillDocument())
            append(EV_FILL_CREATE, new FillCreateForm()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

    }

    /**
     * Tutaj wype�niane s� pola formularza niezale�ne warto�ci
     * p�l dokumentu (np. lista rodzaj�w pisma).
     */
    private class FillPulldowns implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                setTabs(new Tabs(BokAction.this).createTabs());


/*
                WfAssignment[] asg = DSApi.context().getWfAssignments(getDocumentId());
                for (int i=0; i < asg.length; i++)
                {
                }
*/

                try
                {
                    ServiceManager.getService("bip");
                    bipEnabled = true;
                }
                catch(ServiceDriverNotSelectedException sdnse)
                {
                    bipEnabled = false;
                }

                canExportToBip =
                    Boolean.valueOf(
                        //Boolean.valueOf(DSApi.context().getProperty(Properties.ASI_USE, "false")).booleanValue() &&
                        DSApi.context().hasPermission(DSPermission.BIP_EKSPORT));
                kinds = InOfficeDocumentKind.list();
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                outgoingDeliveries = OutOfficeDocumentDelivery.list();
                submitToAsiBip = true;

                // kody kreskowe
                useBarcodes = Boolean.valueOf(GlobalPreferences.isUseBarcodes());
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Odczytuje dane dokumentu.
     */
    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (getDocumentId() == null)
            {
/*
                if (event.getAttribute("error") != null)
                {
                    event.setResult(ERROR);
                    return;
                }
*/

                if (event.getResult() == null)
                {
                    if (Configuration.officeAvailable() || Configuration.faxAvailable())
                        event.setResult("task-list");
                    else
                        event.setResult("simpleoffice-created");
                    event.cancel();
                }
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(getDocumentId());

                document = doc;

                trackingNumber = doc.getTrackingNumber();
                caseDocumentId = doc.getCaseDocumentId();
                creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                journalMessages = new LinkedHashMap();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                // tylko nazwa u�ytkownika, opisy s� ju� w kolekcji users
                assignedUser = doc.getClerk();
                assignedDivision = DSDivision.find(doc.getAssignedDivision()).getPrettyPath();
                documentDate = doc.getDocumentDate() != null ?
                    DateUtils.formatJsDate(doc.getDocumentDate()) : null;
                description = doc.getSummary();
                location = doc.getLocation();
                submitToAsiBip = doc.isSubmitToBip();
                senderAnonymous = doc.getSender().isAnonymous();

                if (doc.getSender() != null)
                {
                    senderTitle = doc.getSender().getTitle();
                    senderFirstname = doc.getSender().getFirstname();
                    senderLastname = doc.getSender().getLastname();
                    senderOrganization = doc.getSender().getOrganization();
                    senderStreetAddress = doc.getSender().getStreet();
                    senderZip = doc.getSender().getZip();
                    senderLocation = doc.getSender().getLocation();
                    senderCountry = doc.getSender().getCountry();
                    senderEmail = doc.getSender().getEmail();
                    senderFax = doc.getSender().getFax();
                }

                if (doc.getRecipients().size() > 0)
                {
                    recipientsMap = new LinkedHashMap();
                    for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
                    {
                        Recipient recipient = (Recipient) iter.next();
                        recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
                            recipient.getSummary());
                    }
                }

                otherRemarks = doc.getOtherRemarks();
                numAttachments = doc.getNumAttachments();
                kind = doc.getKind(); DSApi.initializeProxy(kind);
                outgoingDelivery = doc.getOutgoingDelivery(); DSApi.initializeProxy(outgoingDelivery);
                barcode = doc.getBarcode();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Wype�nia w�asno�ci u�ywane przez formularz tworzenia
     * nowego dokumentu.
     */
    private class FillNewDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date today = new Date();

            documentDate = DateUtils.formatJsDate(today);

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                Map address = GlobalPreferences.getAddress();
                if (address.get(GlobalPreferences.ORGANIZATION) != null)
                {
                    recipientsMap = new LinkedHashMap();

                    Recipient recipient = new Recipient();
                    recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                    recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                    recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                    recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                    recipient.setCountry("PL");

                    recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
                        recipient.getSummary());
                }

                currentDayWarning = !DateUtils.datesEqual(today, GlobalPreferences.getCurrentDay());
                if (GlobalPreferences.getBokDivisionGuid() == null)
                    addActionError(sm.getString("NieOkreslonoDzialuObslugujacegoBOK"));

                String bokGuid = GlobalPreferences.getBokDivisionGuid();
                if (bokGuid == null)
                    throw new EdmException(sm.getString("NieWybranoStanowiskaDoObslugiBOKwUstawieniachAdministracyjnych"));

                try
                {
                    DSDivision.find(bokGuid);
                }
                catch (DivisionNotFoundException e)
                {
                    addActionError(sm.getString("NieZnalezionoDzialuObslugujacegoBOK"));
                }
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class FillCreateForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.skip(EV_FILL_DOCUMENT);

            // recipients to skrypty OGNL tworz�ce obiekty java.util.Map
            // trzeba tu odtworzy� nazwy opcji recipients, bo formularz
            // przekazuje tylko ich warto�ci jako skrypty OGNL tworz�ce
            // obiekty Map
            if (recipients != null)
            {
                recipientsMap = new LinkedHashMap();

                try
                {
                    for (int i=0; i < recipients.length; i++)
                    {
                        recipientsMap.put(recipients[i],
                            new Person().fromJSON(recipients[i]).getSummary());
                    }
                }
                catch (EdmException e)
                {
                	log.error(e.getMessage(),e);
                    addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu"));
                    return;
                }
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date dtDocumentDate = DateUtils.nullSafeParseJsDate(documentDate);

            Date now = new Date();

            if (!StringUtils.isEmpty(documentDate) && dtDocumentDate == null)
                addActionError(sm.getString("NiepoprawnaDataPisma"));
            else if (dtDocumentDate != null && dtDocumentDate.after(now))
                addActionError(sm.getString("DataPismaZnajdujeSieWprzyszlosci"));

            if (kindId == null)
                addActionError(sm.getString("NieWybranoRodzajuPisma"));

            if (StringUtils.isEmpty(description))
                addActionError(sm.getString("NiePodanoOpisuPisma"));

            if (hasActionErrors())
            {
                event.setAttribute("error", Boolean.TRUE);
                event.skip(EV_CREATE);
            }
        }
    }

    private class Create implements ActionListener
    {
        public synchronized void actionPerformed(ActionEvent event)
        {
        	InOfficeDocument doc = null;
        	Date entryDate = null;
	        Long journalId = null;
        	 try
             {
                 DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                 DSApi.context().begin();
		        doc = new InOfficeDocument();
		
		        doc.setBok(true);
		        doc.setSubmitToBip(submitToAsiBip);
		
		        Sender sender = new Sender();
		        if (senderAnonymous)
		        {
		            sender.setAnonymous(true);
		        }
		        else
		        {
		            sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
		            sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
		            sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
		            sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
		            sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
		            sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
		            sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
		            sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
		            sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
		            sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
		        }
		
		        doc.setSender(sender);
		
		        // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
		        // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
		        // obiektu Person
		        if (recipients != null)
		        {
		            Map personMap = null;
		            String json = null;
		            try
		            {
		                for (int i=0; i < recipients.length; i++)
		                {
		                    json = recipients[i];
		                    Recipient recipient = new Recipient();
		                    recipient.fromJSON(json);
		                    doc.addRecipient(recipient);
		                }
		            }
		            catch (EdmException e)
		            {
		                event.getLog().error(e.getMessage(), e);
		                addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu"));
		                return;
		            }
		        }
		
		        doc.setOtherRemarks(TextUtils.trimmedStringOrNull(otherRemarks, 240));
		
		        doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		        doc.setDivisionGuid(DSDivision.ROOT_GUID);
		        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		
		        // data, z jak� zostanie utworzony wpis w dzienniku
		        
		
		        // sprawdzam, czy w bazie istnieje ju� dokument o danym referenceId
           

                if (GlobalPreferences.getBokDivisionGuid() == null)
                    throw new EdmException(sm.getString("NieOkreslonoDzialuObslugujacegoBOK"));

                try
                {
                    DSDivision.find(GlobalPreferences.getBokDivisionGuid());
                }
                catch (DivisionNotFoundException e)
                {
                    throw new EdmException(sm.getString("NieOkreslonoDzialuObslugujacegoBOK"));
                }

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());
                entryDate = currentDay.getTime();

                Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

                // je�eli faktyczna data jest p�niejsza ni� otwarty
                // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
                if (new Date().after(breakOfDay))
                {
                    currentDay.setTime(breakOfDay);
                }
                else
                {
                    Calendar now = Calendar.getInstance();
                    currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                    currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                    currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
                }

                Journal journal;

                // je�eli numer kancelaryjny ma by� pobierany z innego dziennika
                // ni� g��wny, znajduj� ten dziennik
/*
                if (GlobalPreferences.isOfficeNumbersFromAuxJournals())
                {
                    if (incomingJournalId == null)
                        throw new EdmException("Nie wybrano dziennika pism przychodz�cych");

                    journal = Journal.find(incomingJournalId);
                    if (!DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()) ||
                        !Journal.INCOMING.equals(journal.getJournalType()))
                        throw new EdmException("Spodziewano si� dziennika pism przychodz�cych " +
                            "w dziale g��wnym");
                }
                else
                {
                    journal = Journal.getMainIncoming();
                }
*/

                journal = Journal.getMainIncoming();
                journalId = journal.getId();

                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                doc.setNumAttachments(numAttachments);
                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

                doc.setIncomingDate(currentDay.getTime());

                doc.setLocation(TextUtils.trimmedStringOrNull(location, 60));


                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                // referent nie jest na razie przydzielany
                //doc.setClerk(DSApi.context().getDSUser().getName());
                doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
                doc.setKind(kind);

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                if (outgoingDeliveryId != null)
                {
                    doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));
                }

                doc.create();

                doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));

                newDocumentId = doc.getId();

                // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
                // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
                // nie zwi�zanego z folderem (createAttachment zapisuje dokument
                // w bazie).
                if (kind.getRequiredAttachments().size() > 0)
                {
                    for (Iterator iter=kind.getRequiredAttachments().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                        doc.createAttachment(new Attachment(req.getTitle()));
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            // tworzenie wpisu w dzienniku

            Integer sequenceId;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);

                // powi�zanie dokumentu z bie��c� sesj�
                doc = InOfficeDocument.findInOfficeDocument(doc.getId());
                doc.bindToJournal(journalId, sequenceId);

                if (doc.isSubmitToBip())
                {
                	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(doc.getId());
                	/*
                    try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        bip.submit(doc);
                    }
                    catch (ServiceException e)
                    {
                    	log.error(e.getMessage(),e);
                    }*/
                }


                DSDivision bokDivision = DSDivision.find(GlobalPreferences.getBokDivisionGuid());

                WorkflowFactory.createNewProcess(doc, false,"Pismo przyj�te" , "$bok", bokDivision.getGuid(), null);

                DSApi.context().commit();

                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                    doc.getId(),
                    Arrays.asList(new Sender[] { doc.getSender() }));

                // parametr u�ywany przez result=created w xwork.xml
                //setDocumentId(doc.getId());
                if (Configuration.officeAvailable() || Configuration.faxAvailable())
                {
                    event.setResult("task-list");
                }
                else
                {
                    addActionMessage("Przyj�to pismo "+doc.getFormattedOfficeNumber());
                    event.setResult("simpleoffice-created");
                }

                event.skip(EV_FILL_CREATE);
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(getDocumentId());

                doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));

                if (DSApi.context().hasPermission(DSPermission.BIP_EKSPORT))
                    doc.setSubmitToBip(submitToAsiBip);

                trackingNumber = doc.getTrackingNumber();
                creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                journalMessages = new LinkedHashMap();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
                doc.setLocation(TextUtils.trimmedStringOrNull(location, 60));
                doc.setOtherRemarks(TextUtils.trimmedStringOrNull(otherRemarks, 240));
                doc.setNumAttachments(numAttachments);
                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));

                if (!StringUtils.isEmpty(assignedUser))
                {
                    // UserNotFoundException
                    doc.setClerk(DSUser.findByUsername(assignedUser).getName());
                }

                // XXX: dodano 18.10.2004 - edycja wszystkich p�l
                //doc.setSenderAnonymous(senderAnonymous);

                Sender sender = doc.getSender();
                if (senderAnonymous)
                {
                    sender.setAnonymous(true);
                    sender.setTitle(null);
                    sender.setFirstname(null);
                    sender.setLastname(null);
                    sender.setOrganization(null);
                    sender.setStreet(null);
                    sender.setZip(null);
                    sender.setLocation(null);
                    sender.setCountry(null);
                    sender.setEmail(null);
                    sender.setFax(null);
                }
                else
                {
                    sender.setAnonymous(false);
                    sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
                    sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
                    sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
                    sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
                    sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
                    sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
                    sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
                    sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
                    sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
                    sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
                }

                // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
                // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
                // obiektu Person
                if (recipients != null)
                {
                    String json = null;
                    try
                    {
                        for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
                        {
                            ((Recipient) iter.next()).delete();
                            iter.remove();
                        }

                        for (int i=0; i < recipients.length; i++)
                        {
                            json = recipients[i];
                            doc.addRecipient(new Recipient().fromJSON(json));
                        }
                    }
                    catch (EdmException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                        addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu")
                        		+" json="+json);
                        return;
                    }
                }

                if (outgoingDeliveryId != null)
                {
                    doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));
                }

                DSApi.context().commit();

                if (doc.isSubmitToBip())
                {
                	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(doc.getId());
                    /*
                	try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        bip.submit(doc);
                    }
                    catch (ServiceException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }*/
                }
                event.skip(EV_FILL_CREATE);
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    public Boolean getCanExportToBip()
    {
        return canExportToBip;
    }

    public void setCanExportToBip(Boolean canExportToBip)
    {
        this.canExportToBip = canExportToBip;
    }

    public List getKinds()
    {
        return kinds;
    }

    public void setRecipients(String[] recipients)
    {
        this.recipients = recipients;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public boolean isSubmitToAsiBip()
    {
        return submitToAsiBip;
    }

    public void setSubmitToAsiBip(boolean submitToAsiBip)
    {
        this.submitToAsiBip = submitToAsiBip;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isSenderAnonymous()
    {
        return senderAnonymous;
    }

    public void setSenderAnonymous(boolean senderAnonymous)
    {
        this.senderAnonymous = senderAnonymous;
    }

    public String getSenderTitle()
    {
        return senderTitle;
    }

    public void setSenderTitle(String senderTitle)
    {
        this.senderTitle = senderTitle;
    }

    public String getSenderFirstname()
    {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname)
    {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname()
    {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname)
    {
        this.senderLastname = senderLastname;
    }

    public String getSenderOrganization()
    {
        return senderOrganization;
    }

    public void setSenderOrganization(String senderOrganization)
    {
        this.senderOrganization = senderOrganization;
    }

    public String getSenderStreetAddress()
    {
        return senderStreetAddress;
    }

    public void setSenderStreetAddress(String senderStreetAddress)
    {
        this.senderStreetAddress = senderStreetAddress;
    }

    public String getSenderZip()
    {
        return senderZip;
    }

    public void setSenderZip(String senderZip)
    {
        this.senderZip = senderZip;
    }

    public String getSenderLocation()
    {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation)
    {
        this.senderLocation = senderLocation;
    }

    public String getSenderCountry()
    {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry)
    {
        this.senderCountry = senderCountry;
    }

    public String getSenderEmail()
    {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail)
    {
        this.senderEmail = senderEmail;
    }

    public String getSenderFax()
    {
        return senderFax;
    }

    public void setSenderFax(String senderFax)
    {
        this.senderFax = senderFax;
    }

    public String getSenderPesel()
    {
        return senderPesel;
    }

    public void setSenderPesel(String senderPesel)
    {
        this.senderPesel = senderPesel;
    }

    public String getSenderNip()
    {
        return senderNip;
    }

    public void setSenderNip(String senderNip)
    {
        this.senderNip = senderNip;
    }

    public String getSenderRegon()
    {
        return senderRegon;
    }

    public void setSenderRegon(String senderRegon)
    {
        this.senderRegon = senderRegon;
    }

    public Map getRecipientsMap()
    {
        return recipientsMap;
    }

    public String getOtherRemarks()
    {
        return otherRemarks;
    }

    public void setOtherRemarks(String otherRemarks)
    {
        this.otherRemarks = otherRemarks;
    }

    public int getNumAttachments()
    {
        return numAttachments;
    }

    public void setNumAttachments(int numAttachments)
    {
        this.numAttachments = numAttachments;
    }

    public InOfficeDocumentKind getKind()
    {
        return kind;
    }


    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }



    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getAssignedUser()
    {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public List getUsers()
    {
        return users;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public Map getJournalMessages()
    {
        return journalMessages;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public InOfficeDocument getDocument()
    {
        return document;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setIncomingJournalId(Long incomingJournalId)
    {
        this.incomingJournalId = incomingJournalId;
    }

    public OutOfficeDocumentDelivery getOutgoingDelivery()
    {
        return outgoingDelivery;
    }

    public Integer getOutgoingDeliveryId()
    {
        return outgoingDeliveryId;
    }

    public void setOutgoingDeliveryId(Integer outgoingDeliveryId)
    {
        this.outgoingDeliveryId = outgoingDeliveryId;
    }

    public List getOutgoingDeliveries()
    {
        return outgoingDeliveries;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public Boolean getUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public boolean getBipEnabled()
    {
        return bipEnabled;
    }
}
