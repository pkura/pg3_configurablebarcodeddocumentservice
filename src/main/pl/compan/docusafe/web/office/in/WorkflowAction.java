package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.WorkflowTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowAction.java,v 1.6 2006/12/19 16:08:45 lk Exp $
 */
public class WorkflowAction extends WorkflowTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/incoming/external-workflow.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
