package pl.compan.docusafe.web.office.in;

import java.util.List;

public class ContractManagmentAction extends ContractManagmentTabAction {

    @Override
    public String getDocumentType() {
        return null;
    }

    protected List prepareTabs() {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink() {
        return "/office/incoming/contract-managment.action";
    }
}
