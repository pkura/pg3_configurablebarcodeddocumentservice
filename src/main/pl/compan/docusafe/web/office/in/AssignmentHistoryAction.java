package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentHistoryAction.java,v 1.4 2006/02/20 15:42:43 lk Exp $
 */
public class AssignmentHistoryAction extends AssignmentHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/assignment-history.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
