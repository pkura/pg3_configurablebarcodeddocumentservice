package pl.compan.docusafe.web.office.in;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NewZamowienieAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	private Long newDocumentId;
	private String activity;
	
	private StringManager sm = StringManager.getManager(NewZamowienieAction.class.getPackage().getName());

	Logger log = LoggerFactory.getLogger(NewZamowienieAction.class);

	protected void setup() 
	{
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new CreateDocument()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	public void setNewDocumentId(Long newDocumentId)
	{
		this.newDocumentId = newDocumentId;
	}

	public Long getNewDocumentId()
	{
		return newDocumentId;
	}


	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getActivity()
	{
		return activity;
	}


	private class CreateDocument implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				InOfficeDocument doc = new InOfficeDocument();
				DSApi.context().begin();
				
				//doc.setOriginal(false);
      	      
        	    //Sender sender = new Sender();
        		//sender.setAnonymous(true);
        		//doc.setSender(sender);
        	    doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        	    doc.setDivisionGuid(DSDivision.ROOT_GUID);
        	    doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        	    

                
                doc.setDocumentDate(new Date());

                DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ZAMOWIENIE);                
                doc.setSummary(documentKind.getName());

                //InOfficeDocumentKind kind = InOfficeDocumentKind.list().get(0);
                //doc.setKind(kind);

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                //doc.setIncomingDate(currentDay.getTime());
                doc.setIncomingDate(new Date());

                doc.setAssignedDivision(DSDivision.ROOT_GUID);

                //Calendar cal = Calendar.getInstance();
                //cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                doc.create();    
                
                doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));     
                                          
                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));
                  
                doc.setDocumentKind(documentKind);

                setNewDocumentId(doc.getId());

                Map<String,Object> values = new HashMap<String,Object>();
            //    values.put(Prosikalogic.BARCODE_FIELD_CN, Barcode);
            //    values.put(Prosikalogic.SPOSOB_PRZYJECIA_CN, 50);
               
            //    documentKind.setOnly(newDocumentId, values);
                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                documentKind.logic().documentPermissions(doc);
                
                //DSApi.context().session().save(doc);
                DSApi.context().session().flush();
                //DSApi.context().session().refresh(doc);
                 
				doc.setPermissionsOn(true);
                DSApi.context().session().flush();
                
                Integer sequenceId;
                Long journalId;
                Journal journal = Journal.getMainIncoming();
                journalId = journal.getId();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date());
                doc.bindToJournal(journalId, sequenceId);
        	    
                WorkflowFactory.createNewProcess(doc, false);
                doc.getDocumentKind().logic().onStartProcess(doc);
                
                
        	    
				DSApi.context().commit();
				// nie wime jak to lepiej zrobic
                TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);                
				ArrayList<Task> officeTask = new ArrayList<Task>(taskList.getTasks(DSApi.context().getDSUser(),newDocumentId));			
				Task task = officeTask.get(0);
				setActivity(WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey()));
				event.setResult("view-doc");
			}
			catch (Exception e)
			{
				addActionError(sm.getString("nieUdaloSiePrzyjacDokumentu"));
				log.error(e.getMessage(), e);
				DSApi.context()._rollback();
			}
		}
	}
}
