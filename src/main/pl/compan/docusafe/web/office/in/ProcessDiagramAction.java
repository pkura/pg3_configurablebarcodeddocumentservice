package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.ProcessDiagramTabAction;

import java.util.List;

/**
 * User: Tomasz
 * Date: 27.05.14
 * Time: 22:58
 */
public class ProcessDiagramAction extends ProcessDiagramTabAction {

    @Override
    protected List prepareTabs() {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink() {
        return "/office/incoming/process-diagram.action";
    }

    public String getDocumentType() {
        return InOfficeDocument.TYPE;
    }
}
