package pl.compan.docusafe.web.office.in;

import java.util.List;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.ManualAssignmentTabAction;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ManualAssignmentAction extends ManualAssignmentTabAction{
    @Override
    public String getDocumentType() {
         return InOfficeDocument.TYPE;
    }
    
    public List prepareTabs() {
        return new Tabs(this).createTabs();
    }

}
