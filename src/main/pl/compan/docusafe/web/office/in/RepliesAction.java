package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;

import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import pl.compan.docusafe.core.base.DocumentFlags;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RepliesAction.java,v 1.16 2009/10/05 09:34:58 mariuszk Exp $
 */
public class RepliesAction extends BaseTabsAction
{
    private InOfficeDocument document;
    private Set outDocuments;
    private boolean replyUnnecessary;
	//Okre�la czy na pismo udzielono odpowiedzi ale nie zarejestrowano tego w systemie
	private boolean replyBOK;
    private boolean canCreateOutgoing;
    private boolean canUpdate;

	private static Logger log = LoggerFactory.getLogger(RepliesAction.class);
    
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/replies.action";
    }

    public String getDocumentType()
    {
        return "in";
    }

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }


    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (getDocumentId() == null)
                return;

            try
            {
            	canCreateOutgoing = DSApi.context().hasPermission(DSPermission.PISMO_WYCHODZACE_TWORZENIE);
                setTabs(prepareTabs());

                document = InOfficeDocument.findInOfficeDocument(getDocumentId());
                DSApi.initializeProxy(document.getRecipients());

                outDocuments = document.getOutDocuments();
                DSApi.initializeProxy(outDocuments);

                if (outDocuments != null)
                {
                    for (Iterator iter=outDocuments.iterator(); iter.hasNext(); )
                    {
                    	OutOfficeDocument out = ((OutOfficeDocument) iter.next());
                    	//if (!out.isCanceled()){
                        DSApi.initializeProxy(out.getRecipients());
                        DSApi.initializeProxy(out.getSender());
                    //	}
                    }
                }
				log.debug("replyUnnecessary = {}; replyBOK = {};", replyUnnecessary, replyBOK);
				if (Configuration.additionAvailable(Configuration.ADDITION_WAITING_REPLIES) 
						&& LabelsManager.findLabelByName("BOK") != null
						&& document.getFlags().flagPresent(LabelsManager.findLabelByName("BOK").getId())
						&& document.isReplyUnnecessary()) {
					replyUnnecessary = true;
					setReplyBOK(true);
				} else {
					replyUnnecessary = document.isReplyUnnecessary();
					setReplyBOK(false);
				}
				
				canUpdate = true;
				
                if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin())
                	canUpdate = PermissionManager.isDocumentInTaskList(document);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                InOfficeDocument document = InOfficeDocument.findInOfficeDocument(getDocumentId());
				
                document.setReplyUnnecessary(replyUnnecessary);
				log.debug("replyUnnecessary = {}; replyBOK = {};", replyUnnecessary, replyBOK);
				if (Configuration.additionAvailable(Configuration.ADDITION_WAITING_REPLIES)) {
					log.debug("ADDITION waiting replies");
					pl.compan.docusafe.core.labels.Label labelBOK = LabelsManager.findLabelByName("BOK");
					Long BOKFlagId = (labelBOK == null ? null : labelBOK.getId());
					log.debug("BOKFlagId = {}", BOKFlagId);
					
					if (BOKFlagId != null) {
						if (isReplyBOK()) {
							DocumentFlags flags = document.getFlags();
							flags.setFl(BOKFlagId,isReplyBOK());
							document.setReplyUnnecessary(true);
						} else {
							log.debug("Usuwanie flagi");
							DocumentFlags flags = document.getFlags();
							if(flags.flagPresent(BOKFlagId)){
								log.debug("Usuwanie flagi {}", BOKFlagId);
								flags.setFl(BOKFlagId, false);
								document.setReplyUnnecessary(false);
							}
						}
					}
				}
                DSApi.context().session().save(document);
                TaskSnapshot.updateByDocumentId(document.getId(), InOfficeDocument.TYPE);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public Set getOutDocuments()
    {
        return outDocuments;
    }

    public InOfficeDocument getDocument()
    {
        return document;
    }

    public boolean isReplyUnnecessary()
    {
        return replyUnnecessary;
    }

    public void setReplyUnnecessary(boolean replyUnnecessary)
    {
        this.replyUnnecessary = replyUnnecessary;
    }

	public boolean isCanCreateOutgoing() {
		return canCreateOutgoing;
	}

	public void setCanCreateOutgoing(boolean canCreateOutgoing) {
		this.canCreateOutgoing = canCreateOutgoing;
	}

	/**
	 * @return the replyBOK
	 */
	public boolean isReplyBOK() {
		return replyBOK;
	}

	/**
	 * @param replyBOK the replyBOK to set
	 */
	public void setReplyBOK(boolean replyBOK) {
		this.replyBOK = replyBOK;
	}

	public boolean isCanUpdate() {
		return canUpdate;
	}

}
