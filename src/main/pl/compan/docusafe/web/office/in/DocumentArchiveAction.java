package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.List;
/* User: Administrator, Date: 2007-02-28 10:08:11 */
import java.util.Map;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentArchiveAction extends DocumentArchiveTabAction
{
    public String getBaseLink()
    {
        return "/office/incoming/document-archive.action";
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/incoming/external-workflow.action";
    }
    
    public static String getLink(Long id)
    {
        return "/office/incoming/document-archive.action?documentId="+id;
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    @Override
    public List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

	public int getDocType() {
		return DocumentLogic.TYPE_IN_OFFICE;
	}
}
