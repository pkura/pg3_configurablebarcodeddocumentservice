package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction;

import java.util.List;

/**
 * User: Tomasz
 * Date: 20.02.13
 * Time: 15:08
 */
public class ManualMultiAssignmentAction extends ManualMultiAssignmentTabAction {
    @Override
    public String getDocumentType() {
        return InOfficeDocument.TYPE;
    }

    public List prepareTabs() {
        return new Tabs(this).createTabs();
    }
}
