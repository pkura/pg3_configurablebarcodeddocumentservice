package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.ElectronicSignaturesTabAction;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ElectronicSignaturesAction extends ElectronicSignaturesTabAction {
    private final static Logger LOG = LoggerFactory.getLogger(ElectronicSignaturesAction.class);

    @Override
    public String getDocumentType() {
        return InOfficeDocument.TYPE;
    }

    @Override
    protected List<Tab> prepareTabs() {
        return new Tabs(this).createTabs();
    }
}