package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.RemarksTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RemarksAction.java,v 1.7 2006/02/20 15:42:44 lk Exp $
 */
public class RemarksAction extends RemarksTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/remarks.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
