package pl.compan.docusafe.web.office.in;


import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.NormalLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.p4.ReportTask;
import pl.compan.docusafe.service.zebra.ZebraPrinter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DwrDocumentMainAction extends DwrDocumentMainTabAction
{
	private static Logger log = LoggerFactory.getLogger(DwrDocumentMainAction.class);
	static final StringManager sm = GlobalPreferences.loadPropertiesFile(DwrDocumentMainAction.class.getPackage().getName(),null);

	public Logger getLog()
	{
		return log;
	}

	public String getDocumentType()
	{
		return InOfficeDocument.TYPE;
	}

	public int getDocType()
	{
		return DocumentLogic.TYPE_IN_OFFICE;
	}

	public OfficeDocument getOfficeDocument()
	{
		return new InOfficeDocument();
	}
	
	public void isPrinterDefined() {
		if (AvailabilityManager.isAvailable("print.barcode.create") && !ZebraPrinter.isPrinterDefinedForUser())
			addActionError(sm.getString("PrinterNotDefined"));		
	}

	public void setAfterCreate(OfficeDocument document) throws EdmException
	{
		try
		{
			if (!AvailabilityManager.isAvailable("permissions.dontGivePermissionToAuthorOnCreateDocument")){
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
		}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

        if(AvailabilityManager.isAvailable("p4TaskReport") && document.getDocumentKind().getCn().equals("normal"))
        {
            ReportTask rt = new ReportTask();
            rt.setDocumentId(document.getId());
            rt.setTime(document.getCtime());
            rt.setUserName(fastAssignmentSelectUser);
            rt.setTaskId(1L);

            DSApi.context().session().save(rt);
        }
	}

	/*public void setBeforeCreate(OfficeDocument document, ActionEvent event) throws EdmException
	{

		if (event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM) != null
				&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM)).isEmpty()
				&& event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM) != null
				&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)).isEmpty())
		{
			fastAssignmentSelectDivision = (String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
			fastAssignmentSelectUser = (String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
			setBeforeCreate(document , true);
		}else 
		
			setBeforeCreate(document , false);
		



	}
	public void setBeforeCreate(OfficeDocument document , boolean isSetAssigneParamsToDocument) throws EdmException
	{
		if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
			throw new EdmException(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));

		InOfficeDocument doc = (InOfficeDocument) document;
		entryDate = GlobalPreferences.getCurrentDay();
		if (getDocumentId() == null && !isSetAssigneParamsToDocument)
		{
			if (dockindKeys.containsKey(NormalLogic.RECIPIENT_HERE))
			{
				log.debug(NormalLogic.RECIPIENT_HERE);
				String assignee = dockindKeys.get(NormalLogic.RECIPIENT_HERE).toString();
				String[] ids = assignee.split(";");
				if (ids[0].contains("u:"))
					fastAssignmentSelectUser = ids[0].split(":")[1];
				if (ids.length > 1 && ids[1].contains("d:"))
					fastAssignmentSelectDivision = ids[1].split(":")[1];
			}
			if (dockindKeys.containsKey("SENDER_HERE"))
			{
				log.debug("SENDER_HERE");
				String assignee = dockindKeys.get("SENDER_HERE").toString();
				String[] ids = assignee.split(";");
				if (ids[0].contains("u:"))
					fastAssignmentSelectUser = ids[0].split(":")[1];
				if (ids.length > 1 && ids[1].contains("d:"))
					fastAssignmentSelectDivision = ids[1].split(":")[1];
			}
		}
			
        	if (dockindKeys.containsKey("SENDER_PARAM") && (!dockindKeys.containsKey("SENDER") || dockindKeys.get("SENDER") == null))
        	{
        		if(DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "SENDER") != null && DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "SENDER").isOnlyPopUpCreate())
				{
					throw new EdmException(sm.getString("OnlyPopupCreate"));
				}
				Person person = new Person();
				person.fromMapUpper((Map) dockindKeys.get("SENDER_PARAM"));
				person.setDictionaryGuid("rootdivision");
				person.create();
				log.debug("Person.firstname: {}", person.getFirstname());
				Sender sender = new Sender();
				sender.fromMap(person.toMap());
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(person.getId());
				sender.create();
				doc.setSender(sender);
			}
		

		doc.setKind(getDefaultKind());
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setIncomingDate(GlobalPreferences.getCurrentDay());

		try
		{
			// powinno byc bo jest w skrypcie tworzacym ale na wszelik
			// wypadek
			InOfficeDocumentStatus status = InOfficeDocumentStatus.findByCn("PRZYJETY");
			doc.setStatus(status);
		}
		catch (Exception e)
		{
			doc.setStatus(InOfficeDocumentStatus.list().get(0));
		}

		if (InOfficeDocumentDelivery.findByName("Poczta") != null)
			doc.setDelivery(InOfficeDocumentDelivery.findByName("Poczta"));
		else
		{
			if(!AvailabilityManager.isAvailable("utp.wylacz.dostawaPoczta")){
				InOfficeDocumentDelivery delivery = new InOfficeDocumentDelivery("Poczta");
				delivery.create();
				doc.setDelivery(delivery);
			}
		}

	}*/

	public void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException
	{
		InOfficeDocument doc = (InOfficeDocument) document;
		Journal journal = Journal.getMainIncoming();
		journalId = journal.getId();
		DSApi.context().session().flush();
		Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
		doc.bindToJournal(journalId, sequenceId, event);
	}

	public boolean isStartProcess() throws EdmException
	{
		return true;
	}

	public List<Tab> prepareTabs()
	{
		return new Tabs(this).createTabs();
	}

	public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException
	{
		dwrDocumentHelper.setBeforeUpdateIn(DwrDocumentMainAction.this);

	}

	@Override
	public void setBeforeCreate(OfficeDocument document) throws EdmException, Exception
	{
		if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
			throw new EdmException(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));

		InOfficeDocument doc = (InOfficeDocument) document;
		entryDate = GlobalPreferences.getCurrentDay();
		if (getDocumentId() == null )
		{
			if (dockindKeys.containsKey(NormalLogic.RECIPIENT_HERE))
			{
				log.debug(NormalLogic.RECIPIENT_HERE);
				String assignee = dockindKeys.get(NormalLogic.RECIPIENT_HERE).toString();
				String[] ids = assignee.split(";");
				if (ids[0].contains("u:"))
					fastAssignmentSelectUser = ids[0].split(":")[1];
				if (ids.length > 1 && ids[1].contains("d:"))
					fastAssignmentSelectDivision = ids[1].split(":")[1];
			}
			if (dockindKeys.containsKey("SENDER_HERE"))
			{
				log.debug("SENDER_HERE");
				String assignee = dockindKeys.get("SENDER_HERE").toString();
				String[] ids = assignee.split(";");
				if (ids[0].contains("u:"))
					fastAssignmentSelectUser = ids[0].split(":")[1];
				if (ids.length > 1 && ids[1].contains("d:"))
					fastAssignmentSelectDivision = ids[1].split(":")[1];
			}
		
			
        	if (dockindKeys.containsKey("SENDER_PARAM") && (!dockindKeys.containsKey("SENDER") || dockindKeys.get("SENDER") == null))
        	{
        		if(DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "SENDER") != null && DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "SENDER").isOnlyPopUpCreate())
				{
					throw new EdmException(sm.getString("OnlyPopupCreate"));
				}
				Person person = new Person();
				person.fromMapUpper((Map) dockindKeys.get("SENDER_PARAM"));
				person.setDictionaryGuid("rootdivision");
				person.create();
				log.debug("Person.firstname: {}", person.getFirstname());
				Sender sender = new Sender();
				sender.fromMap(person.toMap());
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(person.getId());
				sender.create();
				doc.setSender(sender);
			}
		

		doc.setKind(getDefaultKind());
		doc.setCreatingUser(DSApi.context().getPrincipalName());
		doc.setIncomingDate(GlobalPreferences.getCurrentDay());

		try
		{
			// powinno byc bo jest w skrypcie tworzacym ale na wszelik
			// wypadek
			InOfficeDocumentStatus status = InOfficeDocumentStatus.findByCn("PRZYJETY");
			doc.setStatus(status);
		}
		catch (Exception e)
		{
			doc.setStatus(InOfficeDocumentStatus.list().get(0));
		}

		if (InOfficeDocumentDelivery.findByName("Poczta") != null)
			doc.setDelivery(InOfficeDocumentDelivery.findByName("Poczta"));
		else
		{
			if(!AvailabilityManager.isAvailable("utp.wylacz.dostawaPoczta")){
				InOfficeDocumentDelivery delivery = new InOfficeDocumentDelivery("Poczta");
				delivery.create();
				doc.setDelivery(delivery);
			}
		}
		
		}
	}
}
