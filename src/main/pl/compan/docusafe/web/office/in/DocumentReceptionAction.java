package pl.compan.docusafe.web.office.in;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.ajax.AjaxJsonResult;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.p4.CometBarcodeMessage;
import pl.compan.docusafe.parametrization.p4.CometBarcodeMessage.BarcodeOperation;
import pl.compan.docusafe.parametrization.p4.CometSignatureMessage;
import pl.compan.docusafe.parametrization.p4.CometSignatureMessage.MessageStatus;
import pl.compan.docusafe.parametrization.p4.HandWrittenSignatureHelper;
import pl.compan.docusafe.parametrization.p4.ShortDocumentMetric;
import pl.compan.docusafe.parametrization.p4.SignatureUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.comet.CometMessage;
import pl.compan.docusafe.web.comet.CometMessage.MessageKind;
import pl.compan.docusafe.web.comet.CometMessageManager;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.webwork.ServletActionContext;

public class DocumentReceptionAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(DocumentReceptionAction.class);
	private static final Logger kl = LoggerFactory.getLogger("kamil");
	
	private String barcode;
	private String login;
	private String firstname;
	private String lastname;
	private String fromUrl;
	
	private Gson gson = new GsonBuilder()
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampSerializer)
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampDeserializer)
		.create();

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("rejectReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new RejectReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("addBarcode")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new AddBarcode())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);	
		
		registerListener("removeBarcode")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new RemoveBarcode())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("checkSession")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new CheckSession())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
	private PrintWriter getOutPrintWriter() throws IOException {
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html; charset=ISO-8859-2");
		return resp.getWriter();
	}
	
	private class AjaxDocumentInfo extends AjaxJsonResult {
		ShortDocumentMetric documentMetric = new ShortDocumentMetric();
	}

	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			if(StringUtils.isEmpty(login)) {
				addActionError("Nie podano loginu");
			} else {
				try {
					DSUser user = DSUser.findByUsername(login);
					firstname = user.getFirstname();
					lastname = user.getLastname();
				} catch(EdmException e) {
					LOG.error(e.getMessage(), e);
					addActionError(e.getLocalizedMessage());
				}
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	/**
	 * Dodaje barkod <code>barcode</code> i zwraca informacje o dokumencie w JSONie
	 * @author Kamil
	 *
	 */
	private class AddBarcode extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			AjaxDocumentInfo docInfo = new AjaxDocumentInfo();
			CometMessageManager manager = CometMessageManager.getInstance();
			
			if(StringUtils.isEmpty(barcode)) {
				docInfo.success = false;
				docInfo.errorMessage = "Nie podano barkodu dokumentu";
			} else {
				try {
					InOfficeDocument doc = InOfficeDocument.findLatestByPostalRegNumber(barcode, cutBarCode(barcode) );
					
					docInfo.documentMetric.barcode = barcode;
					docInfo.documentMetric.date = doc.getCtime();
					docInfo.documentMetric.documentId = doc.getId().toString();
					if(doc.getSender() != null) {
						docInfo.documentMetric.sender = doc.getSender().getSummary();
					}
					docInfo.documentMetric.delivery = doc.getDelivery() != null ? doc.getDelivery().getName() : "";
					
					List<Recipient> recipients = doc.getRecipients();
					if(recipients != null && !recipients.isEmpty()) {
						docInfo.documentMetric.recipient = recipients.get(0).getSummary();
					}
					
					String recipientUserName = doc.getRecipientUser();
					if(recipientUserName != null) {
						try {
							DSUser recipientUser = DSUser.findByUsername(recipientUserName);
							Map<String, String> userInfo = new HashMap<String, String>();
							userInfo.put("name", recipientUserName);
							userInfo.put("lastname", recipientUser.getLastname());
							userInfo.put("firstname", recipientUser.getFirstname());
							docInfo.documentMetric.recipientUser = userInfo;
						} catch(UserNotFoundException e) {
							LOG.error(e.getMessage(), e.getLocalizedMessage());
						}
						docInfo.success = false;
						docInfo.error = "duplicated reception";
					} else {
						HandWrittenSignatureHelper.getInstance().add(barcode, doc.getId());
						
						// dodajemy komunikat typu comet o dodaniu barkodu do u�ytkownika MONITOR DOTYKOWY
						CometMessage msg = new CometBarcodeMessage(MessageKind.BARCODE_RECIPIENT, barcode, 
								BarcodeOperation.ADD, docInfo.documentMetric);
						manager.addCometMessage(SignatureUtils.MONITOR_DOTYKOWY_USER, msg);
					}
				} catch(Exception e) {
					LOG.error(e.getMessage(), e);
					docInfo.success = false;
					docInfo.errorMessage = e.getLocalizedMessage();
				}
				
			}
			
			out.write(docInfo.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private String cutBarCode(String prn)
	{
		if(prn != null && prn.length() > 9)
	    	{
	    		return prn.substring(prn.length() - 10, prn.length() - 1);
	    	}
		return prn;
	}

	
	/**
	 * Usuwa barkod <code>barcode</code> i zwraca informacje o dokumencie w JSONie
	 * @author Kamil
	 *
	 */
	private class RemoveBarcode extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			AjaxDocumentInfo docInfo = new AjaxDocumentInfo();
			
			if(StringUtils.isEmpty(barcode)) {
				docInfo.success = false;
				docInfo.errorMessage = "Nie podano barkodu dokumentu";
			} else {
				// TODO - obs�uga usuni�cia barkodu (je�li potrzebna)
				docInfo.documentMetric.barcode = barcode;
				CometMessage msg = new CometBarcodeMessage(MessageKind.BARCODE_RECIPIENT, barcode, 
						BarcodeOperation.REMOVE, null);
				manager.addCometMessage(SignatureUtils.MONITOR_DOTYKOWY_USER, msg);
				
				HandWrittenSignatureHelper.getInstance().removeByBarcode(barcode);
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	/**
	 * Odrzuca odbi�r 
	 * @author Kamil
	 *
	 */
	private class RejectReception extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			
			try {
				DSUser recipient = DSUser.findByUsername(login);
				// komunikat wysy�any do MONITOR DOTYKOWY
				CometMessage msg = new CometSignatureMessage(recipient, MessageStatus.CANCEL, MessageKind.SIGNATURE_REQUEST_REJECT);
				manager.addCometMessage(SignatureUtils.MONITOR_DOTYKOWY_USER, msg);
				
				HandWrittenSignatureHelper.getInstance().clear();
			} catch(EdmException e) {
				LOG.error(e.getMessage(), e);
			}
			
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	/**
	 * Sprawdza czy u�ytkownikowi nie wygas�a sesja. <br />
	 * Zwraca <code>OK</code>, je�li sesja aktywna. <br />
	 * Je�li nie - zwracany jest standardowy kod strony - czyli ekran logowania
	 * @author Kamil
	 *
	 */
	private class CheckSession extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			
			out.write("OK");
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getFromUrl() {
		return fromUrl;
	}

	public void setFromUrl(String fromUrl) {
		this.fromUrl = fromUrl;
	}

}
