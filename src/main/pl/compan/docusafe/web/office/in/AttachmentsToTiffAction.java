package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.AttachmentsToTiffTabAction;

import java.util.List;
/* User: Administrator, Date: 2005-12-09 13:42:43 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentsToTiffAction.java,v 1.2 2006/02/20 15:42:43 lk Exp $
 */
public class AttachmentsToTiffAction extends AttachmentsToTiffTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/attachments-to-tiff.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
