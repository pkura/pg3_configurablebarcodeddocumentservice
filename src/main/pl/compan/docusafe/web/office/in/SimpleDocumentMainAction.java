package pl.compan.docusafe.web.office.in;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.parametrization.p4.ReportTask;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.SimpleDocumentMainTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class SimpleDocumentMainAction extends SimpleDocumentMainTabAction
{
	private static OfficeDocumentDelivery defaultDelivery;

	private List<InOfficeDocumentDelivery> deliveries;

	@Override
	public DocumentType getDocumentType()
	{
		return DocumentType.INCOMING;
	}

	public String getBaseUrl()
	{
		return "/office/incoming/simple-document-main.action";
	}

	@Override
	public OfficeDocument getOfficeDocument()
	{
		return new InOfficeDocument();
	}

	@Override
	public void setDocument(OfficeDocument odc) throws EdmException {
		InOfficeDocument document = (InOfficeDocument) odc;
		documentDate = DateUtils.formatJsDate(document.getDocumentDate());
	}

	@Override
	public void updateSender(Person sender) throws EdmException {
		setPersonValue(sender);
	}

	@Override
	public void updateReceiver(Person receiver) {
		receiver.setFirstname(userFirstname);
		receiver.setLastname(userLastname);
		receiver.setOrganization(userDivision);
	}

	@Override
	public void setAfterCreate(OfficeDocument document) throws EdmException
	{
		if(AvailabilityManager.isAvailable("p4TaskReport"))
		{
			//tutaj ma byc dodawanie do bazy zadania
			//id
			//id dokumentu
			//czas
			//user id

			ReportTask rt = new ReportTask();
			rt.setDocumentId(document.getId());
			rt.setTime(document.getCtime());
			rt.setUserName(fastAssignmentSelectUser);
			rt.setTaskId(1L);

			DSApi.context().session().save(rt);
		}
	}

	public void bindToJournal(OfficeDocument document,ActionEvent event) throws EdmException
	{
		InOfficeDocument  doc = (InOfficeDocument) document;
        Journal journal = Journal.getMainIncoming();
        journalId = journal.getId();
        DSApi.context().session().flush();
        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
        doc.bindToJournal(journalId, sequenceId,event);
        if (AvailabilityManager.isAvailable("simple.document.show.dailyOfficeNumber"))
        {
     	   addActionMessage("Dodano pismo do dzienika g��wnego pod numerem KO " + doc.getDailyOfficeNumber());
        }
        else
     	   addActionMessage("Dodano pismo do dzienika g��wnego pod numerem KO "+doc.getOfficeNumber());
	}

	@Override
	public void setBeforeCreate(OfficeDocument document) throws EdmException
	{
		InOfficeDocument doc = (InOfficeDocument) document;
		if (original == null)
		{
			
			doc.setOriginal(true);
		}
		else
		{
			doc.setOriginal(original);
		}

        Sender sender = new Sender();
		if (personAnonymous)
		{
			sender.setAnonymous(true);
		}
		else if(personLastname == null && personOrganization == null)
		{
		    throw new EdmException("Nie wybrano nadawcy");
		}
		else
		{
			setPersonValue(sender);
		}
		/**dodajemy do slownika nadawcow*/
		if(createPerson)
		{
	        Person person = new Person();
	        setPersonValue(person);
	        person.setDictionaryGuid("rootdivision");
	        person.setDictionaryType(Person.DICTIONARY_SENDER);
	        person.createIfNew();
	        DSApi.context().session().flush();
		}
		
        if(userFirstname != null)
        {
        	Recipient recipient = new Recipient();
        	recipient.setFirstname(userFirstname);
        	recipient.setLastname(userLastname);
        	recipient.setOrganization(userDivision);
        	doc.addRecipient(recipient);
        }
        else if(!StringUtils.isEmpty(userDivision))
        {
        	Map address = GlobalPreferences.getAddress();
        	Recipient recipient = new Recipient();
         	recipient.setOrganization(userDivision);
         	recipient.setStreet((String) address.get(GlobalPreferences.STREET));
         	recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
         	recipient.setZip((String) address.get(GlobalPreferences.ZIP));
         	doc.addRecipient(recipient);
        }
		else
        {
        	Map address = GlobalPreferences.getAddress();
            Recipient recipient = new Recipient();
         	recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
         	recipient.setStreet((String) address.get(GlobalPreferences.STREET));
         	recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
         	recipient.setZip((String) address.get(GlobalPreferences.ZIP));
         	doc.addRecipient(recipient);
        }
        
        doc.setSender(sender);
        doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
        doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
        doc.setKind(getDefaultKind());
        doc.setCreatingUser(DSApi.context().getPrincipalName());
        entryDate = GlobalPreferences.getCurrentDay();
        doc.setIncomingDate(GlobalPreferences.getCurrentDay());
        try
        {
        	//powinno byc bo jest w skrypcie tworzacym ale na wszelik wypadek
        	InOfficeDocumentStatus status = InOfficeDocumentStatus.findByCn("PRZYJETY");
        	doc.setStatus(status);
        }
        catch (Exception e) {
			doc.setStatus(InOfficeDocumentStatus.list().get(0));
		}

        if (deliveryId != null)
        {
            doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
        }
	}

	public void loadDelivery() throws EdmException
	{
		setDeliveries(InOfficeDocumentDelivery.list());
		if(defaultDelivery == null){
			defaultDelivery = (OfficeDocumentDelivery) DSApi.context().session()
				.createCriteria(InOfficeDocumentDelivery.class)
				.add(Restrictions.eq("name", "Poczta"))
				.uniqueResult();
		}
		if(deliveryId == null){
			setDeliveryId(defaultDelivery.getId());
		}
	}

	public void setDeliveries(List<InOfficeDocumentDelivery> deliveries)
	{
		this.deliveries = deliveries;
	}

	public List<InOfficeDocumentDelivery> getDeliveries()
	{
		return deliveries;
	}

	@Override
	public void loadTabs() throws EdmException {
		this.setTabs(new Tabs(this).createTabs());
	}

	public String getProcess() {    
		return null;
	}

	@Override
	public void setSender(Person person) throws EdmException {
		setPerson(person);
	}

	@Override
	public void setReceiver(Person person) throws EdmException {
		setUser(person);
	}

	@Override
	public boolean isStartProcess() throws EdmException 
	{
		return true;
	}

	@Override
	public void loadFillForm() throws EdmException {
		personAnonymous = false;
	}


}
