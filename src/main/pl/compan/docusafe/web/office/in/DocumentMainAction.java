package pl.compan.docusafe.web.office.in;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentKindRequiredAttachment;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.service.upload.Upload;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.tiff.Tiff;
import pl.compan.docusafe.web.office.common.DocumentMainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.asprise.util.tiff.TIFFWriter;
import com.opensymphony.webwork.ServletActionContext;
/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentMainAction extends DocumentMainTabAction
{
	private final static Logger LOG = LoggerFactory.getLogger(DocumentMainAction.class);
	
	private static OfficeDocumentDelivery defaultDelivery;

    // @EXPORT (tylko odczytywane przez formularz lub xwork.xml, nie modyfikowane)
    private Boolean canExportToBip;
    private boolean canSetClerk;
    private boolean canChangeKind;
    private boolean canCreate;
    private List<InOfficeDocumentKind> kinds;
    private List statuses;
    private List users;
    private List deliveries;
    private List outgoingDeliveries;
    private Set instantAssignmentUsers;
    private String personDirectoryDivisionGuid;
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private Map recipientsMap;
    private InOfficeDocumentKind kind;
    private InOfficeDocumentStatus status;
    private InOfficeDocumentDelivery delivery;
    private OutOfficeDocumentDelivery outgoingDelivery;
    private String creatingUser;
    private Map journalMessages;
    private String trackingNumber;
    private Map incomingDivisions;
    private String caseDocumentId;
    private String assignedDivision;
    private boolean intoJournalPermission;
    private InOfficeDocument document;
    private DSDivision[] divisions;
    /**
     * Lista istniej�cych ju� dokument�w o danym referenceId. U�ywane
     * podczas tworzenia nowego dokumentu.
     */
    private List existingDocuments;
    private List incomingJournals;
    private DSUser[] clerks;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private boolean currentDayWarning;
    private boolean useBarcodePrinter;
    private String barcodePrinterDevice;
    private String documentBarcode;
    private boolean blocked;
    private boolean useScanner;
    /**
     * czy dost�pny wyb�r rodzaju pisma przychodz�cego - zale�ne od 'dockind-a'
     */
    private boolean canChooseKind;

    // @IMPORT (tylko ustawiane przez formularz)
    private Long intoJournalId;
    private Long incomingJournalId;
    private boolean createNext;

    // @IMPORT/EXPORT
    private String summary;
    private String multiToken;
    private boolean multitiff;
    private FormFile file;
    private Long retainedObjectId;
    private Long messageId;     // id wiadmosci na podstawie ktorej przyjmujmey pismo
    private Integer statusId; // export dla main-used-reference-id
    private Integer kindId; // export dla main-used-reference-id
    private Integer deliveryId;
    private Integer outgoingDeliveryId;
    private String documentDate;
    private String stampDate;
    private boolean submitToAsiBip;
    private String description;
    private String location;
    private boolean senderAnonymous;
    private String senderTitle;
    private String senderFirstname;
    private String senderLastname;
    private String senderOrganization;
    private String senderStreetAddress;
    private String senderZip;
    private String senderLocation;
    private String senderCountry;
    private String senderEmail;
    private String senderFax;
    private String senderPesel;
    private String senderNip;
    private String senderRegon;
    private Boolean original;
	/**
     * Warto�� ustawiana po wys�aniu formularza przez u�ytkownika.
     * Warto�ciami s� wyra�enia OGNL tworz�ce obiekty java.util.Map
     * opisuj�ce odbiorc�w pisma.
     */
    private String[] recipients;
    private String postalRegNumber;
    private String packageRemarks;
    private String otherRemarks;
    private int numAttachments;
    private String assignedUser;
    private Map<String,String> substitute;
    /**
     * U�ytkownik, na kt�rego pismo zostanie od razu zadekretowane.
     */
    private String instantAssignment;
    private String referenceId;
    private String barcode;
    /**
     * Je�eli to pole ma warto�� true, podczas tworzenia dokumentu nie b�dzie
     * sprawdzane, czy pole referenceId jest unikalne.
     */
    private boolean acceptDuplicateReferenceId;
    private String reason;
    private boolean daa;
    private boolean duplicate;
    private List <Document> duplicateList;
    private Long duplicateDocId;

    private static final String EV_FILL_CREATE = "fillCreate";
    private static final String EV_CREATE = "create";

	private Boolean goToDocument;

    StringManager sm =
        GlobalPreferences.loadPropertiesFile(DocumentMainAction.class.getPackage().getName(),null);
	private Long boxId;
	private String boxName;
	private Boolean addToBox;

	public String currentAssignmentUsername;
	public String currentAssignmentGuid;
	
	

    public String getBaseLink()
    {
        return "/office/incoming/document-main.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
         //   append(OpenHibernateSession.INSTANCE).
            append(fillForm);
           // appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
//            append(OPEN_JBPM_SESSION).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(EV_FILL_CREATE, new FillCreate()).
            append(fillForm);
//            appendFinally(CLOSE_JBPM_SESSION);

        registerListener("doCD").
        //   append(OpenHibernateSession.INSTANCE).
          // append(new ValidateCreate()).
           append(new Create()).
           append(EV_FILL_CREATE, new FillCreate()).
           append(fillForm);

        registerListener("doChangeDockind").
          //  append(OpenHibernateSession.INSTANCE).
            append(fillForm);
           // appendFinally(CloseHibernateSession.INSTANCE);
    }
    
	public void setDefaultDelivery() throws EdmException
	{
		deliveryId = Integer.parseInt(Docusafe.getAdditionProperty("in.default.delivery"));
	}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
				LOG.trace("FillForm");
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                if(AvailabilityManager.isAvailable("documentMain.chooseCurrentDivision"))
                {
                	divisions = DSDivision.getOnlyDivisionsAndPositions(false);
                }
                canChangeKind = DSApi.context().hasPermission(DSPermission.PISMO_ZMIANA_RODZAJU);

                multiToken = ((Upload) ServiceManager.getService(Upload.NAME)).
                    createMultiToken();

                useScanner = DSApi.context().userPreferences().node("scanner").getBoolean("use", false);
                multitiff = true;

                deliveries = InOfficeDocumentDelivery.list();

                // wype�nienie adresu odbiorcy
                Map address = GlobalPreferences.getAddress();
                if (address.get(GlobalPreferences.ORGANIZATION) != null)
                {
                    recipientsMap = new LinkedHashMap();

                    Recipient recipient = new Recipient();
                    recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                    recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                    recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                    recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                    recipient.setCountry("PL");

                    recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
                        recipient.getSummary());
                }

                if (recipients != null)
                {
                    recipientsMap = new LinkedHashMap();

                    try
                    {
                        for (int i=0; i < recipients.length; i++)
                        {
                            recipientsMap.put(recipients[i], new Person().fromJSON(recipients[i]).getSummary());
                        }
                    }
                    catch (EdmException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                        addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu"));
                        return;
                    }
                }

                fillForm(DocumentLogic.TYPE_IN_OFFICE);

                // inicjuje rodzaj pisma przychodz�cego
                kinds = InOfficeDocumentKind.list();
                String kindName = documentKind.logic().getInOfficeDocumentKind();
                String boxLine = documentKind.getProperties().get(DocumentKind.BOX_LINE_KEY);
                if (boxLine == null)
                {
                    // pud�o z linii domy�lnej
                    if (GlobalPreferences.isBoxOpen())
                    {
                        boxId = GlobalPreferences.getBoxId();
                    }
                    else
                        boxId = null;
                }
                else
                {
                    boxId = GlobalPreferences.getBoxId(boxLine);
                }
                if (boxId != null)
                {
                    try
                    {
                        Box box = Box.find(boxId);
                        boxName = box.getName();
                        boxId = box.getId();
                    }
                    catch (EntityNotFoundException e)
                    {
                        boxId = null;
                    }
                }
                canChooseKind = (kindName == null);
                if (!canChooseKind)
                {
                    // nie mo�na wybiera� na formatce - rodzaj pisma ustalony
                    for (InOfficeDocumentKind inKind : kinds)
                    {
                        if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                        {
                            kindId = inKind.getId();
                        }
                    }
                }
                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
				LOG.trace("FillForm end");
            }
            catch (EdmException e)
            {
            	LOG.error("",e);
                addActionError(e.getMessage());
            }
            finally
            {
               DSApi._close();
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			LOG.trace("validate create");
            if (documentKindCn == null)
                throw new IllegalStateException(sm.getString("NieJestWybranyZadenTypDokument"));

            if (kindId == null)
                addActionError(sm.getString("NieWybranoRodzajuPisma"));

            if( documentDate != null && DateUtils.parseJsDate(documentDate) == null){
            	addActionError(sm.getString("NiepoprawnaDataPisma"));
            }

            DocumentKind documentKind = null;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                documentKind = DocumentKind.findByCn(documentKindCn);


                SearchResults<Document> duplicateDoc = documentKind.checkDuplicate(values);
                
                
                if (duplicateDoc == null || duplicateDoc.count() < 1)
                {
                }
                else
                {
                	duplicateList = new LinkedList<Document>();
                	while(duplicateDoc.hasNext())
                	{
                		duplicateList.add(duplicateDoc.next());
                	}
                	duplicate = true;
                	duplicateDocId  = duplicateList.get(0).getId();
                	/*duplicateList = new ArrayList<Document>();
                    while(duplicateDoc.hasNext())
                    {
                        Document tmpdoc = duplicateDoc.next();
                        LoggerFactory.getLogger("tomekl").debug("id = "+tmpdoc.getId());
                        duplicateList.add(tmpdoc);
                    }
                    duplicate = true;
                    Long lastDoc = new Long(0);
                    for (Iterator iter = duplicateList.iterator(); iter.hasNext();)
                    {
                        Document element = (Document) iter.next();
                        if(element.getId() > lastDoc)
                            lastDoc = element.getId();
                    }
                    duplicateDocId = lastDoc;*/
                    event.addActionError(sm.getString("IstniejeJuzDokumentOpodanychParametrach"));
                    event.skip(EV_CREATE);
                }
            }
            catch (EdmException e)
            {
            	saveFile();
            	event.addActionError(e.getMessage());
            	LOG.error(e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            }
            if (hasActionErrors())
			{
            	saveFile();
			}
        }
    }

    private class FillCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            // recipients to skrypty OGNL tworz�ce obiekty java.util.Map
            // trzeba tu odtworzy� nazwy opcji recipients, bo formularz
            // przekazuje tylko ich warto�ci jako skrypty OGNL tworz�ce
            // obiekty Map
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                if (recipients != null)
                {
                    recipientsMap = new LinkedHashMap();


                        for (int i=0; i < recipients.length; i++)
                        {
                            recipientsMap.put(recipients[i], new Person().fromJSON(recipients[i]).getSummary());
                        }
                }
                fillForm(DocumentLogic.TYPE_IN_OFFICE);
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu"));
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class Create implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
            if (hasActionErrors())
            { 
				LOG.error("action errors Create ommited");
                return;
			}
            Date entryDate = null;
            Long journalId = null;
            InOfficeDocument doc = null;
            try
            {
	            doc = new InOfficeDocument();

	            if (original == null) {
					doc.setOriginal(false);
				} else {
					doc.setOriginal(original);
				}
	            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
				DSjBPM.open();

                DSApi.context().begin();
	            Sender sender = new Sender();
	            if (senderAnonymous) 
	            {
					sender.setAnonymous(true);
				} else {
	                sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
	                sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
	                sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
	                sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
	                sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
	                sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
	                sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
	                sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
	                sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
	                sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
	            }
	           // DSApi.context().session().flush();
	            doc.setSender(sender);

	            // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
	            // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
	            // obiektu Person
	            if (recipients != null)
	            {
	            	Map personMap = null;
	                //String ognlExpr = null;
	                String json = null;
	                try
	                {
	                    for (int i=0; i < recipients.length; i++)
	                    {
	                        json = recipients[i];
	                        Recipient recipient = new Recipient();
	                        recipient.fromJSON(json);
	                        doc.addRecipient(recipient);
	                    }
	                }
	                catch (EdmException e)
	                {
	                	saveFile();
	                    event.getLog().error(e.getMessage(), e);
	                    addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu")+" json="+json+
	                        " personMap="+personMap);
	                    return;
	                }
	            }

	            doc.setCurrentAssignmentUsername(currentAssignmentUsername);
	           // doc.setCurrentAssignmentGuid((currentAssignmentGuid!= null ? currentAssignmentGuid :DSDivision.ROOT_GUID ));
	            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	            doc.setDivisionGuid((currentAssignmentGuid!= null ? currentAssignmentGuid :DSDivision.ROOT_GUID ));
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);
                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
                    throw new EdmException(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));

                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));

                documentKind = DocumentKind.findByCn(documentKindCn);

                documentKind.logic().correctValues(values, documentKind);

                // walidacja nie jest tu raczej potrzebna, gdy� wykonuje si� ona ju� na poziomie Javascriptu
				//RE: jest potrzebna, w logice mog� by� dodane nowe warunki
                documentKind.logic().validate(values, documentKind, null);

                doc.setSummary(summary != null ? summary : documentKind.getName());
                doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));

                //Jesli duplikat
                if(duplicateDocId != null)
                {
                    doc.setDuplicateDoc(duplicateDocId);
                }

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
                doc.setKind(kind);

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());

                entryDate = new Date(currentDay.getTime().getTime());

                Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

                // je�eli faktyczna data jest p�niejsza ni� otwarty
                // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
                if (new Date().after(breakOfDay))
                {
                    currentDay.setTime(breakOfDay);
                }
                else
                {
                    Calendar now = Calendar.getInstance();
                    currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                    currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                    currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
                }

                Journal journal = Journal.getMainIncoming();

                journalId = journal.getId();

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setIncomingDate(currentDay.getTime());

                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                // referent nie jest na razie przydzielany
                //doc.setClerk(DSApi.context().getDSUser().getName());

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));

                if (deliveryId != null)
                {
                    doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
                }
                doc.create();
                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));

                doc.setDocumentKind(documentKind);

                newDocumentId = doc.getId();

                documentKind.set(newDocumentId, values);

                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                documentKind.logic().onStartProcess(doc);

                DSApi.context().session().save(doc);

                // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
                // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
                // nie zwi�zanego z folderem (createAttachment zapisuje dokument
                // w bazie).

                Upload up = (Upload) ServiceManager.getService(Upload.NAME);

                if (multiToken != null && up.isMultiTokenValid(multiToken))
                {
                    Iterator iter = up.retrieveMultiFiles(multiToken);

                    if (multitiff)
                    {
                        Tiff tiff = new Tiff();
                        while (iter.hasNext())
                        {
                            Map.Entry entry = (Map.Entry) iter.next();
                            String filename = (String) entry.getKey();
                            File file = (File) entry.getValue();

                            tiff.add(file, MimetypesFileTypeMap.getInstance().
                                    getContentType(filename));
                        }

                        File tif = tiff.write(
                            TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
                            TIFFWriter.TIFF_COMPRESSION_GROUP4);
                        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                        doc.createAttachment(attachment);
                        attachment.createRevision(tif).setOriginalFilename(tif.getName());
                    }
                    else // !multitiff
                    {
                        while (iter.hasNext())
                        {
                            Map.Entry entry = (Map.Entry) iter.next();
                            String filename = (String) entry.getKey();
                            File file = (File) entry.getValue();

                            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                            doc.createAttachment(attachment);
                            attachment.createRevision(file).setOriginalFilename(filename);
                        }
                    }
                }
                if (kind.getRequiredAttachments().size() > 0)
                {
                    for (Iterator iter=kind.getRequiredAttachments().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                        doc.createAttachment(new Attachment(req.getTitle()));
                    }
                }
                if (addToBox!=null && addToBox && boxId != null)
                {

                	Box b = Box.find(boxId);
                    doc.setBox(b);
                }
                addFiles(doc);
                specificAdditions(doc);
                DSApi.context().commit();
            }
            catch (Throwable e)
            {
            	saveFile();
            	try
            	{
            		DSApi.context().setRollbackOnly();
            	}
            	catch (Exception e2) {}
				LOG.error(e.getMessage(), e);
                addActionError(e.getMessage());
                return;
            }
            finally
            {
				try {
					DSjBPM.closeContextQuietly(DSjBPM.context());
				} catch (EdmException ex) {
					LOG.error(ex.getLocalizedMessage(), ex);
				}
                try { DSApi.close(); } catch (Exception e) { }
            }
            // tworzenie wpisu w dzienniku
            Integer sequenceId;

            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
				DSjBPM.open();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);                
                
                // powi�zanie dokumentu z bie��c� sesj�
                doc = InOfficeDocument.findInOfficeDocument(doc.getId());
                doc.bindToJournal(journalId, sequenceId);

                //Journal journal = DSApi.context().getOfficeHelper().getJournal(Journal.INCOMING, Division.ROOT_GUID);
                //doc.setOfficeNumber(entry.getSequenceId());

               	WorkflowFactory.createNewProcess(doc, AvailabilityManager.isAvailable("coordinators"));


                doc.getDocumentKind().logic().onStartProcess(doc,event);
                documentKind.logic().documentPermissions(doc);    //sprawdzanie uprawnie� na samym ko�cu
                
                DSApi.context().commit();
                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),doc.getId(),Arrays.asList(new Sender[] { doc.getSender() }));

                // parametr u�ywany przez result=created w xwork.xml
                //setDocumentId(doc.getId());
                if(createNextDocument)
                {
                	StringBuffer sb = new StringBuffer();
                	sb.append("Utwozono dokument, id");
                	sb.append(' ');
                	sb.append(doc.getId());
                	addActionMessage(sb.toString());
                }
                else if (NR_SZKODY != null && DSApi.context().hasPermission(DSPermission.NW_WPIS_RS))
                {
                    event.setResult("nw-drs");
                }
                else if(goToDocument != null && goToDocument)
                {
    				// nie wime jak to lepiej zrobic
                    TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);                
    				ArrayList<Task> officeTask = new ArrayList<Task>(taskList.getTasks(DSApi.context().getDSUser(),newDocumentId));			
    				Task task = officeTask.get(0);
    				setActivity(WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey()));
                	event.setResult("go-to-document");
                }
                else if (Configuration.officeAvailable() || Configuration.faxAvailable())
                {
                        event.setResult("task-list");
                }
                else
                {
                    event.setResult("simpleoffice-created");
                }
                event.skip(EV_FILL_CREATE);
                addActionMessage(sm.getString("PrzyjetoPismo")+" "+doc.getFormattedOfficeNumber());
            }
            catch (Exception e)
            {
            	saveFile();
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
				LOG.error(e.getMessage(), e);
            }
            finally
            {
				DSjBPM.closeContextQuietly();
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getSenderTitle()
    {
        return senderTitle;
    }

    public void setSenderTitle(String senderTitle)
    {
        this.senderTitle = senderTitle;
    }

    public String getSenderFirstname()
    {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname)
    {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname()
    {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname)
    {
        this.senderLastname = senderLastname;
    }

    public String getSenderOrganization()
    {
        return senderOrganization;
    }

    public void setSenderOrganization(String senderOrganization)
    {
        this.senderOrganization = senderOrganization;
    }

    public String getSenderStreetAddress()
    {
        return senderStreetAddress;
    }

    public void setSenderStreetAddress(String senderStreetAddress)
    {
        this.senderStreetAddress = senderStreetAddress;
    }

    public String getSenderZip()
    {
        return senderZip;
    }

    public void setSenderZip(String senderZip)
    {
        this.senderZip = senderZip;
    }

    public String getSenderLocation()
    {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation)
    {
        this.senderLocation = senderLocation;
    }

    public String getSenderCountry()
    {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry)
    {
        this.senderCountry = senderCountry;
    }

    public String getSenderEmail()
    {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail)
    {
        this.senderEmail = senderEmail;
    }

    public String getSenderFax()
    {
        return senderFax;
    }

    public void setSenderFax(String senderFax)
    {
        this.senderFax = senderFax;
    }

    public String getSenderPesel()
    {
        return senderPesel;
    }

    public void setSenderPesel(String senderPesel)
    {
        this.senderPesel = senderPesel;
    }

    public String getSenderNip()
    {
        return senderNip;
    }

    public void setSenderNip(String senderNip)
    {
        this.senderNip = senderNip;
    }

    public String getSenderRegon()
    {
        return senderRegon;
    }

    public void setSenderRegon(String senderRegon)
    {
        this.senderRegon = senderRegon;
    }

    public String[] getRecipients()
    {
        return recipients;
    }

    public void setRecipients(String[] recipients)
    {
        this.recipients = recipients;
    }

    public InOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    public void setDelivery(InOfficeDocumentDelivery delivery)
    {
        this.delivery = delivery;
    }

    public InOfficeDocumentKind getKind()
    {
        return kind;
    }

    public void setKind(InOfficeDocumentKind kind)
    {
        this.kind = kind;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public List getKinds()
    {
        return kinds;
    }

    public Map getRecipientsMap()
    {
        return recipientsMap;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public boolean isSenderAnonymous()
    {
        return senderAnonymous;
    }

    public void setSenderAnonymous(boolean senderAnonymous)
    {
        this.senderAnonymous = senderAnonymous;
    }

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber)
    {
        this.postalRegNumber = postalRegNumber;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public boolean isUseScanner()
    {
        return useScanner;
    }

    public boolean isMultitiff()
    {
        return multitiff;
    }

    public void setMultitiff(boolean multitiff)
    {
        this.multitiff = multitiff;
    }

    public String getMultiToken()
    {
        return multiToken;
    }

    public void setMultiToken(String multiToken)
    {
        this.multiToken = multiToken;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public boolean isCanChangeKind()
    {
        return canChangeKind;
    }

    public boolean isCanChooseKind()
    {
        return canChooseKind;
    }

    public void setRecipientsMap(Map recipientsMap)
    {
        this.recipientsMap = recipientsMap;
    }

    public boolean isDuplicate()
    {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate)
    {
        this.duplicate = duplicate;
    }

    public List<Document> getDuplicateList()
    {
        return duplicateList;
    }

    public void setDuplicateList(List<Document> duplicateList)
    {
        this.duplicateList = duplicateList;
    }

    public Long getDuplicateDocId()
    {
        return duplicateDocId;
    }

    public void setDuplicateDocId(Long duplicateDocId)
    {
        this.duplicateDocId = duplicateDocId;
    }

	public Boolean getOriginal() {
		return original;
	}

	public void setOriginal(Boolean original) {
		this.original = original;
	}

	public Long getBoxId() {
		return boxId;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;
	}

	public String getBoxName() {
		return boxName;
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public Boolean getAddToBox() {
		return addToBox;
	}

	public void setAddToBox(Boolean addToBox) {
		this.addToBox = addToBox;
	}

	public static OfficeDocumentDelivery getDefaultDelivery() {
		return defaultDelivery;
	}

	public static void setDefaultDelivery(OfficeDocumentDelivery defaultDelivery) {
		DocumentMainAction.defaultDelivery = defaultDelivery;
	}

	public String getCurrentAssignmentGuid() {
		return currentAssignmentGuid;
	}

	public void setCurrentAssignmentGuid(String currentAssignmentGuid) {
		this.currentAssignmentGuid = currentAssignmentGuid;
	}

	public String getCurrentAssignmentUsername() {
		return currentAssignmentUsername;
	}

	public void setCurrentAssignmentUsername(String currentAssignmentUsername) {
		this.currentAssignmentUsername = currentAssignmentUsername;
	}

	public List getUsers() {
		return users;
	}

	public void setUsers(List users) {
		this.users = users;
	}

	public DSDivision[] getDivisions() {
		return divisions;
	}

	public void setDivisions(DSDivision[] divisions) {
		this.divisions = divisions;
	}

	public Boolean getGoToDocument() {
		return goToDocument;
	}

	public void setGoToDocument(Boolean goToDocument) {
		this.goToDocument = goToDocument;
	}
	
}
