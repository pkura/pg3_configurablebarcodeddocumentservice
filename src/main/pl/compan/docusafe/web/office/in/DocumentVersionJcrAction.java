package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.DocumentVersionJcrTabAction;
import pl.compan.docusafe.web.office.common.DocumentVersionTabAction;

import java.util.List;

public class DocumentVersionJcrAction extends DocumentVersionJcrTabAction
	{
	    protected List prepareTabs()
	    {
	    	 return new Tabs(this).createTabs();
	    }

	    public String getBaseLink()
	    {
	        return "/office/incoming/document-version-jcr.action";
	    }

	    public String getDocumentType()
	    {
	        return InOfficeDocument.TYPE;
	    }
		    
	}
