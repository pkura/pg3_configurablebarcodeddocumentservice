package pl.compan.docusafe.web.office.in;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKindRequiredAttachment;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrintReceiptAction.java,v 1.27 2009/02/12 07:33:29 pecet3 Exp $
 */
public final class PrintReceiptAction extends EventActionSupport
{
    private Long documentId;
    
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        //final SetActionForwardListener setForward = new SetActionForwardListener("main");
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File temp = null;
            try
            {
                InOfficeDocument document = InOfficeDocument.findInOfficeDocument(documentId);

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 12);
                Font headingFont = new Font(baseFont, 14);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                pdfDoc.open();


                Paragraph heading = new Paragraph(sm.getString("PotwierdzeniePrzyjeciaPisma"), headingFont);
                heading.setAlignment(Paragraph.ALIGN_CENTER);
                pdfDoc.add(heading);

                DSUser user = DSApi.context().getDSUser();

                Table table = new Table(2);
                table.setWidths(new int[] { 40, 60 } );
                table.setBorder(Table.NO_BORDER);
                table.setPadding(5);
                table.setDefaultCellBorder(Table.NO_BORDER);

                table.addCell(new Phrase(sm.getString("NumerPisma")+":", font));
                table.addCell(new Phrase(String.valueOf(document.getOfficeNumber()), font));
              
                table.addCell(new Phrase(sm.getString("Identyfikator dokumentu")+":", font));
                table.addCell(new Phrase(document.getBarcode() != null ? document.getBarcode(): "", font));
                
                table.addCell(new Phrase(sm.getString("DataPrzyjeciaPisma")+":", font));
                table.addCell(new Phrase(document.getIncomingDate() != null ? DateUtils.formatCommonDateTime(document.getIncomingDate()) : "", font));

                table.addCell(new Phrase(sm.getString("OsobaPrzyjmujacaPismo")+":", font));
                table.addCell(new Phrase(DSUser.safeToFirstnameLastname(document.getCreatingUser()), font));

                String sender = "";
                if (document.getSender() != null)
                {
                    sender = TextUtils.nullSafeString(document.getSender().getFirstname())+
                        " " +
                        TextUtils.nullSafeString(document.getSender().getLastname()) +
                        " / " +
                        TextUtils.nullSafeString(document.getSender().getOrganization());
                }
                table.addCell(new Phrase(sm.getString("Nadawca")+":", font));
                table.addCell(new Phrase(sender, font));

                String recipient = "";
                if (document.getRecipients().size() > 0)
                {
                    Recipient rec = (Recipient) document.getRecipients().get(0);
                    recipient = TextUtils.nullSafeString(rec.getFirstname()) +
                        " " +
                        TextUtils.nullSafeString(rec.getLastname()) +
                        " / " +
                        TextUtils.nullSafeString(rec.getOrganization());
                }
                table.addCell(new Phrase(sm.getString("Odbiorca")+":", font));
                table.addCell(new Phrase(recipient, font));

                table.addCell(new Phrase(sm.getString("DataPisma")+":", font));
                table.addCell(new Phrase(document.getDocumentDate() != null ? DateUtils.formatCommonDate(document.getDocumentDate()) : "", font));

                table.addCell(new Phrase(sm.getString("Opis")+":", font));
                table.addCell(new Phrase(document.getSummary() != null ? document.getSummary() : "", font));

                table.addCell(new Phrase(sm.getString("LiczbaZalacznikow")+":", font));
                table.addCell(new Phrase(String.valueOf(document.getNumAttachments()), font));

				if (AvailabilityManager.isAvailable("printBipOnBusiness"))
				{
					
						if (document.getTrackingNumber() != null)
						{
							table.addCell(new Phrase(sm.getString("NumerSledzeniaWBIP") + ":", font));
							table.addCell(new Phrase(document.getTrackingNumber(), font));

						}
						if (document.getTrackingPassword() != null)
						{
							table.addCell(new Phrase(sm.getString("HasloWBIP") + ":", font));
							table.addCell(new Phrase(document.getTrackingPassword(), font));
						}
					
				}
                if (!Configuration.hasExtra("business"))
                {
                    table.addCell(new Phrase(sm.getString("NumerSledzeniaWBIP")+":", font));
                    table.addCell(new Phrase(document.getTrackingNumber(), font));

                    table.addCell(new Phrase(sm.getString("HasloWBIP")+":", font));
                    table.addCell(new Phrase(document.getTrackingPassword(), font));
                }

                Date finishDate;
                if (document.getContainingCase() != null)
                {
                    OfficeCase c = document.getContainingCase();
                    finishDate = c.getFinishDate();
                }
                else
                {
                    finishDate = DateUtils.plusBusinessDays(document.getIncomingDate(), document.getKind().getDays());
                }

                table.addCell(new Phrase(sm.getString("TerminOdpowiedzi")+":", font));
                //document.getContainingCase() != null ? document.getContainingCase().
                table.addCell(new Phrase(DateUtils.formatCommonDate(finishDate), font));

                if (document.getKind().getRequiredAttachments().size() > 0)
                {
                    StringBuilder sbReqAtt = new StringBuilder(100);
                    java.util.List req = document.getKind().getRequiredAttachments();
                    for (int i=0; i < req.size(); i++)
                    {
                        sbReqAtt.append(((InOfficeDocumentKindRequiredAttachment) req.get(i)).getTitle());
                        if (i < req.size()-1)
                            sbReqAtt.append(", ");
                    }
                    table.addCell(new Phrase(sm.getString("WymaganeZalaczniki")+":", font));
                    table.addCell(new Phrase(sbReqAtt.toString(), font));
                }




                pdfDoc.add(table);

                Paragraph foot = new Paragraph(sm.getString("pieczecUrzedu")+":", font);
                foot.setAlignment(Paragraph.ALIGN_RIGHT);
                foot.setIndentationRight(75f);
                pdfDoc.add(foot);

                
                pdfDoc.add(new Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",
                    DSApi.context().getDSUser().asFirstnameLastname(),
                    DateUtils.formatCommonDate(new Date())), font));

                pdfDoc.close();
            }
            catch (IOException e)
            {
                event.getLog().error(e.getMessage(), e);
                addActionError(sm.getString("BladPodczasTworzeniaPotwierdzeniaOdbioru"));
            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
                addActionError(sm.getString("BladPodczasTworzeniaPotwierdzeniaOdbioru"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            catch(NullPointerException e)
            {
            	addActionError(e.getMessage());
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("rozmiar pliku "+temp+": "+temp.length());
                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
}
