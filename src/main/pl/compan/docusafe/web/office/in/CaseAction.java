package pl.compan.docusafe.web.office.in;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.web.office.common.CaseTabAction;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseAction.java,v 1.39 2010/08/17 10:56:54 mariuszk Exp $
 */
public class CaseAction extends CaseTabAction
{
    protected boolean setCase(OfficeDocument document, OfficeCase c) throws EdmException
    {
        if (document.getContainingCase() == null)
        {
            document.setContainingCase(c,false);

            // dodawanie do sprawy odpowiedzi udzielonych wcze�niej na to pismo
            if (document.getType() == DocumentType.INCOMING)
            {
                Set out = ((InOfficeDocument) document).getOutDocuments();
                for (Iterator iter=out.iterator(); iter.hasNext(); )
                {
                    OutOfficeDocument outdoc = (OutOfficeDocument) iter.next();
                    if (outdoc.getContainingCase() == null)
                        outdoc.setContainingCase(c,false);
                }
            }
            
            InOfficeDocument indoc = (InOfficeDocument)document;
            if (indoc.isSubmitToBip())
            {
            	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(indoc.getId());
            }
            /*
            try
            {
                Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                bip.submit((InOfficeDocument)document);
            }
            catch (Exception e)
            {
            }*/
            //BipUtils.writeXml(c);
            return false;
        }
        else
        {
            // klonowanie dokumentu
            InOfficeDocument clone = (InOfficeDocument) document.cloneObject(null);
            clone.setMasterDocument((InOfficeDocument) document);
            
//            WorkflowFactory.createNewProcess(clone,true);

            // nowy proces tylko wtedy, gdy pozwala na to ustawienie konfiguracyjne
            // i gdy istnieje proces podstawowy (musi by� dost�pny modu� office)
            if (Configuration.officeAvailable() &&
                !StringUtils.isEmpty(getActivity()) &&
                GlobalPreferences.isWfProcessForClonedInDocuments())
            {
                // tworzenie i uruchamianie wewn�trznego procesu workflow
            	// k.�. - dwa razy, nie wiem po co, na razie zostaje
                WorkflowFactory.createNewProcess(clone, false, "Kopia przyj�tego pisma");
            }
            //NewBipDriver.getXmlFromCase(c);
            clone.setContainingCase(c,false);
            // TODO: symbol w sprawie
            InOfficeDocument indoc = (InOfficeDocument)document;
            if (indoc.isSubmitToBip())
            {
            	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(indoc.getId());
            }
            /*
            try
            {
                Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                bip.submit((InOfficeDocument)document);
            }
            catch (Exception e)
            {
            }*/
            //BipUtils.writeXml(c);
            
            return true;
        }
        
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/case.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
