package pl.compan.docusafe.web.office.in;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.json.JSONObject;

import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentKindRequiredAttachment;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.service.stagingarea.RetainedObjectAttachment;
import pl.compan.docusafe.service.upload.Upload;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.tiff.Tiff;
import pl.compan.docusafe.web.office.common.MainTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.asprise.util.tiff.TIFFWriter;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Uwagi dla os�b aktualizuj�cych t� klas�: <ul>
 * <li>Pola formularza niezale�ne od dokumentu (lub jego braku) powinny
 *   by� wype�niane w klasie {@link FillPulldowns}.
 * </ul>
 *
 * Po dodaniu pola w main.jsp nale�y doda� je te� w main-user-reference-id.jsp
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MainAction.java,v 1.171 2010/08/17 10:56:54 mariuszk Exp $
 */
public class MainAction extends MainTabAction
{
	private static final Logger log = LoggerFactory.getLogger(MainAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(MainAction.class.getPackage().getName(),null);
    // @EXPORT (tylko odczytywane przez formularz lub xwork.xml, nie modyfikowane)
    private Boolean canExportToBip;
    private boolean canSetClerk;
    private boolean canChangeKind;
    private boolean canCreate;
    private List kinds;
    private List statuses;
 //   private List users;
    private List deliveries;
    private List outgoingDeliveries;
    private Set instantAssignmentUsers;
    private String personDirectoryDivisionGuid;
    /**
     * Tablica, na podstawie kt�rej wype�niane jest pole recipients
     * w formularzu.
     */
    private Map recipientsMap;
    private InOfficeDocumentKind kind;
    private InOfficeDocumentStatus status;
    private InOfficeDocumentDelivery delivery;
    private OutOfficeDocumentDelivery outgoingDelivery;
    private String creatingUser;
    private Map journalMessages;
    private String trackingNumber;
    private Map incomingDivisions;
    private String caseDocumentId;
    private String assignedDivision;
    private boolean intoJournalPermission;
    private InOfficeDocument document;
    /**
     * Lista istniej�cych ju� dokument�w o danym referenceId. U�ywane
     * podczas tworzenia nowego dokumentu.
     */
    private List existingDocuments;
    private List incomingJournals;
    private DSUser[] clerks;
    private Boolean useBarcodes;
    private String barcodePrefix;
    private String barcodeSuffix;
    private Long newDocumentId;
    private boolean currentDayWarning;
    private boolean useBarcodePrinter;
    private String barcodePrinterDevice;
    private String documentBarcode;
    private boolean blocked;
    private boolean useScanner;

    // @IMPORT (tylko ustawiane przez formularz)
    private Long intoJournalId;
    private Long incomingJournalId;
    private boolean createNext;
    private boolean attachCase;

    // @IMPORT/EXPORT
    private String multiToken;
    private boolean multitiff;
    private FormFile file;
    private Long retainedObjectId;
    private Long messageId;     // id wiadmosci na podstawie ktorej przyjmujmey pismo
    private Integer statusId; // export dla main-used-reference-id
    private Integer kindId; // export dla main-used-reference-id
    private Integer deliveryId;
    private Integer outgoingDeliveryId;
    private String documentDate;
    private String stampDate;
    private boolean submitToAsiBip;
    private String description;
    private String location;
    private boolean senderAnonymous;
    private String senderTitle;
    private String senderFirstname;
    private String senderLastname;
    private String senderOrganization;
    private String senderStreetAddress;
    private String senderZip;
    private String senderLocation;
    private String senderCountry;
    private String senderEmail;
    private String senderFax;
    private String senderPesel;
    private String senderNip;
    private String senderRegon;
    private Integer numAttachments;
    /**
     * Warto�� ustawiana po wys�aniu formularza przez u�ytkownika.
     * Warto�ciami s� wyra�enia OGNL tworz�ce obiekty java.util.Map
     * opisuj�ce odbiorc�w pisma.
     */
    private String[] recipients;
    private String postalRegNumber;
    private Boolean original;
    private String packageRemarks;
    private String otherRemarks;
    //private int numAttachments;
    private String assignedUser;
    private Map<String,String> substitute;
    /**
     * U�ytkownik, na kt�rego pismo zostanie od razu zadekretowane.
     */
    private String instantAssignment;
    private String referenceId;
    private String barcode;
    /**
     * Je�eli to pole ma warto�� true, podczas tworzenia dokumentu nie b�dzie
     * sprawdzane, czy pole referenceId jest unikalne.
     */
    private boolean acceptDuplicateReferenceId;
    private String reason;
    private boolean daa;
	public String currentAssignmentGuid;
	public DSDivision[] divisions;
	public Long senderId;

	private boolean save;
	
    public static final String EV_FILL_DOCUMENT = "fillDocumentForm";
    public static final String EV_FILL_CREATE = "fillCreateForm";
    public static final String EV_PRINT_LABEL= "printLabel";
    public static final String EV_CREATE = "create";
    public static final String EV_UPDATE = "update";
    //public static final String CREATED = "created"; // atrybut ActionEvent

    /*
        Tworzenie zak�adek w otwartej sesji Hibernate.
        Je�eli nie ma parametru activityId, szukane s� wszystkie zadania
        u�ytkownika zwi�zane z tym pismem. Je�eli tylko jedno, dawany
        jest link do dokumentu z tym activityId (zak�adka workflow).
        Je�eli wi�cej ni� jedno, strona przej�ciowa z tymi zadaniami.
        Je�eli brak, nie dajemy zak�adki Dekretacja.

    */

    public String getBaseLink()
    {
        return "/office/incoming/main.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    protected void setup()
    {
        // sta�e elementy formularza (listy rozwijane)
        FillPulldowns fillPulldowns = new FillPulldowns();
        // dane z bie��cego dokumentu
        FillDocumentForm fillDocumentForm = new FillDocumentForm();

        // ogl�danie istniej�cego dokumentu
        registerListener(DEFAULT_ACTION).
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new PrepareScanner()).
            append(fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        // tworzenie nowego dokumentu
        registerListener("doNewDocument").
            //append(OpenHibernateSession.INSTANCE).
            append(new FillNewDocumentForm()).
            append(new PrepareScanner()).
            append(fillPulldowns);
            //append(fillDocumentForm).
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new ValidateCreateOrUpdate()).
            append(EV_CREATE, new Create()).
            append(EV_PRINT_LABEL, new PrintLabel()).
            //appendIf("event.getAttribute('created') != null", new FillCreateForm()).
            // FillCreate tylko wtedy, gdy tworzenie dokumentu si� nie powiedzie
            append(EV_FILL_CREATE, new FillCreateForm()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new ValidateCreateOrUpdate()).
            append(EV_UPDATE, new Update()).
            // FillCreate tylko wtedy, gdy aktualizacja dokumentu si� nie powiedzie
            // append(and().append(FillCreate()).append(FillDocument())
            append(EV_FILL_CREATE, new FillCreateForm()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doIntoJournal").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new IntoJournal()).
            append(EV_FILL_DOCUMENT, fillDocumentForm);
            //appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAcceptDuplicateReferenceId").
            //append(OpenHibernateSession.INSTANCE).
            append(fillPulldowns).
            append(new PrepareScanner()).
            append(EV_FILL_CREATE, new FillCreateForm()).
            // pusty listener, kt�ry i tak zostanie wyrzucony z przetwarzania
            // przez FillCreateForm
            append(EV_FILL_DOCUMENT, new ActionListener() { public void actionPerformed(ActionEvent event) {} });
            //appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("printLabel").
	        //append(OpenHibernateSession.INSTANCE).
        	append(fillPulldowns).
	        append(new PrintLabel()).
	        append(fillDocumentForm);
	        //appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class PrintLabel implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                if((AvailabilityManager.isAvailable("print.barcode.create") || 
                		AvailabilityManager.isAvailable("ogolne.print.barcode") )&& getDocumentId() != null)
                {
                	InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(getDocumentId());
                	ZebraPrinterManager manager = new ZebraPrinterManager();
                	manager.printLabelFromDocument(doc);
                }
                if (createNext)
                {
                    document = null;
                    setDocumentId(null);
                    newDocumentId = null;
                }
            }
            catch (Exception e) 
            {
            	 addActionError(e.getMessage());
                 log.error(e.getMessage(),e);
			}
            finally
            {
            	try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private void prepareScanner()
    {
        try
        {
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            multiToken = ((Upload) ServiceManager.getService(Upload.NAME)).
                createMultiToken();
            multitiff = true;
        }
        catch (Exception e)
        {
            addActionError(e.getMessage());
            log.error(e.getMessage(),e);
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }
    }

    private class PrepareScanner implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            prepareScanner();
        }
    }

    /**
     * Tutaj wype�niane s� pola formularza niezale�ne warto�ci
     * p�l dokumentu (np. lista rodzaj�w pisma).
     */
    private class FillPulldowns implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                setTabs(new Tabs(MainAction.this).createTabs());
                if(AvailabilityManager.isAvailable("documentMain.chooseCurrentDivision"))
                {
                	divisions = DSDivision.getOnlyDivisionsAndPositions(false);
                }

/*
                WfAssignment[] asg = DSApi.context().getWfAssignments(getDocumentId());
                for (int i=0; i < asg.length; i++)
                {
                }
*/

                useScanner = DSApi.context().userPreferences().node("scanner").getBoolean("use", false);

                canCreate = DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE);
                canChangeKind = DSApi.context().hasPermission(DSPermission.PISMO_ZMIANA_RODZAJU);
                canExportToBip =
                    Boolean.valueOf(
                        //Boolean.valueOf(DSApi.context().getProperty(Properties.ASI_USE, "false")).booleanValue() &&
                        DSApi.context().hasPermission(DSPermission.BIP_EKSPORT));
                kinds = InOfficeDocumentKind.list();
                statuses = InOfficeDocumentStatus.list();
               // users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                deliveries = InOfficeDocumentDelivery.list();
                outgoingDeliveries = OutOfficeDocumentDelivery.list();
                submitToAsiBip = true;

                String instantAssignmentGroup = GlobalPreferences.getInstantAssignmentGroup();
                if ((Configuration.officeAvailable() || Configuration.faxAvailable()) &&
                    instantAssignmentGroup != null)
                {
                    instantAssignmentUsers = new LinkedHashSet();
                    substitute = new LinkedHashMap<String, String>();
                    try
                    {
                        DSDivision group = DSDivision.find(instantAssignmentGroup);
                        DSUser[] groupUsers = group.getUsers();
                        for (int i=0; i < groupUsers.length; i++)
                        {
                            DSDivision[] divisions = groupUsers[i].getDivisions();
                            for (int d=0; d < divisions.length; d++)
                            {
                                DSDivision division;
                                if (divisions[d].isGroup())
                                {
                                    continue;
                                }
                                else if (divisions[d].isPosition())
                                {
                                    if (divisions[d].getParent() != null &&
                                        divisions[d].getParent().isDivision())
                                    {
                                        division = divisions[d].getParent();
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    division = divisions[d];
                                }

                                Map bean = new HashMap();
                                bean.put("value", division.getGuid()+"/"+groupUsers[i].getName());
                                bean.put("label", division.getName()+" - "+groupUsers[i].asFirstnameLastname());
                                if(groupUsers[i].getSubstituteUser()!=null)
                                	substitute.put(groupUsers[i].asFirstnameLastname(),groupUsers[i].getSubstituteUser().asFirstnameLastname());

                                instantAssignmentUsers.add(bean);
                            }
                        }
                    }
                    catch (DivisionNotFoundException e)
                    {
                        event.getLog().warn(sm.getString("NieZnalezionoGrupyNatychmiastowejDekretacji")+" : "+instantAssignmentGroup);
                    }
                }

                // lista g��wnych dziennik�w przychodz�cych
                // je�eli w��czone jest numerowanie pism przychodz�cych
                // na podstawie dziennik�w dodatkowych
                if (GlobalPreferences.isOfficeNumbersFromAuxJournals())
                {
                    List journals = Journal.findByDivisionGuid(DSDivision.ROOT_GUID, Journal.INCOMING);
                    incomingJournals = journals;
                }

                // kody kreskowe
                useBarcodes = Boolean.valueOf(GlobalPreferences.isUseBarcodes());
                barcodePrefix = GlobalPreferences.getBarcodePrefix();
                barcodeSuffix = GlobalPreferences.getBarcodeSuffix();
                
                //zablokowanie mozliwosci zapisu
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.oznaczJakoCzystopis")){
                	Document doc = Document.find(documentId);
                	if(doc.getCzyCzystopis())
                		save=true;
                	
            	}
                
            }
            catch (Exception e)
            {
            	saveFile();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Odczytuje dane dokumentu.
     */
    private class FillDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (getDocumentId() == null)
            {
/*
                if (event.getAttribute("error") != null)
                {
                    event.setResult(ERROR);
                    return;
                }
*/

                if (event.getResult() == null)
                {
                    if (Configuration.officeAvailable() || Configuration.faxAvailable())
                        event.setResult("task-list");
                    else
                        event.setResult("simpleoffice-created");
                    event.cancel();
                }
                return;
            }

            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(getDocumentId());

                document = doc;
            //    document.getAssociatedDocuments().size(); // lazy (hibernate)

                daa = (doc.getDoctype() != null) && ("daa".equals(doc.getDoctype().getCn()));

                // sugerowany dzia� s�ownika nadawc�w/odbiorc�w
                personDirectoryDivisionGuid =
                    document.getCurrentAssignmentGuid() != null ?
                    document.getCurrentAssignmentGuid() : document.getAssignedDivision();

                
                try
                {
//                	 kody kreskowe
                    useBarcodePrinter = GlobalPreferences.getUseBarcodePrinter();
                    // ta linia jako pierwsza, je�eli wyst�pi wyj�tek useBarcodePrinter=false
                    if(StringUtils.isEmpty(doc.getBarcode()))
                    {
                    	documentBarcode = BarcodeUtils.getKoBarcode(document);
                    }
                  
                    barcodePrinterDevice = GlobalPreferences.getBarcodePrinterDevice();
                }
                catch (EdmException e)
                {
                    addActionError(sm.getString("WystapilBladPodczasGenerowaniaKoduKreskowego")+": "+e.getMessage());
                    event.getLog().error(e.getMessage(), e);
                }

                intoJournalPermission = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
                currentAssignmentGuid = doc.getDivisionGuid();
                trackingNumber = doc.getTrackingNumber();
                caseDocumentId = doc.getCaseDocumentId();
                creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                journalMessages = new LinkedHashMap();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                // tylko nazwa u�ytkownika, opisy s� ju� w kolekcji users
                assignedUser = doc.getClerk();
                assignedDivision = DSDivision.find(doc.getAssignedDivision()).getPrettyPath();
                documentDate = doc.getDocumentDate() != null ?
                    DateUtils.formatJsDate(doc.getDocumentDate()) : null;
                stampDate = doc.getStampDate() != null ?
                    DateUtils.formatJsDate(doc.getStampDate()) : null;
                description = doc.getSummary();
                location = doc.getLocation();
                submitToAsiBip = doc.isSubmitToBip();

                canSetClerk = doc.canSetClerk();

                if (doc.getSender() != null)
                {
                    senderAnonymous = doc.getSender().isAnonymous();
                    senderTitle = doc.getSender().getTitle();
                    senderFirstname = doc.getSender().getFirstname();
                    senderLastname = doc.getSender().getLastname();
                    senderOrganization = doc.getSender().getOrganization();
                    senderStreetAddress = doc.getSender().getStreet();
                    senderZip = doc.getSender().getZip();
                    senderLocation = doc.getSender().getLocation();
                    senderCountry = doc.getSender().getCountry();
                    senderEmail = doc.getSender().getEmail();
                    senderFax = doc.getSender().getFax();
                    senderId = doc.getSenderId();
                }

                if (!doc.getRecipients().isEmpty())
                {
                    recipientsMap = new LinkedHashMap();
                    for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
                    {
                        Recipient recipient = (Recipient) iter.next();
                        recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
                            recipient.getSummary());
                    }
                }

                postalRegNumber = doc.getPostalRegNumber();
                original = doc.getOriginal();
                packageRemarks = doc.getPackageRemarks();
                otherRemarks = doc.getOtherRemarks();
                numAttachments = doc.getNumAttachments();
                kind = doc.getKind();
                if(kind != null)
                	kindId = kind.getId();
                status = doc.getStatus(); DSApi.initializeProxy(status);
                referenceId = doc.getReferenceId();
                delivery = doc.getDelivery(); DSApi.initializeProxy(delivery);
                outgoingDelivery = doc.getOutgoingDelivery(); DSApi.initializeProxy(outgoingDelivery);
                barcode = doc.getBarcode();

                // wszyscy u�ytkownicy z dzia�u, w kt�rym jest pismo

                clerks = DSDivision.find(doc.getAssignedDivision()).getUsersFromDivision(DSApi.context().getDSUser().getDivisions());
                UserFactory.sortUsers(clerks, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                // je�eli na li�cie nie ma bie��cego referenta pisma, trzeba go doda�
                if (doc.getClerk() != null)
                {
                    boolean clerkOnList = false;
                    for (int i=0; i < clerks.length; i++)
                    {
                        if (clerks[i].getName().equals(doc.getClerk()))
                        {
                            clerkOnList = true;
                            break;
                        }
                    }
                    if (!clerkOnList)
                    {
                        List tmp = new ArrayList(Arrays.asList(clerks));
                        tmp.add(DSUser.findByUsername(doc.getClerk()));
                        clerks = (DSUser[]) tmp.toArray(new DSUser[clerks.length]);
                        UserFactory.sortUsers(clerks, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                    }
                }

                // lista dzia��w, w kt�rych mo�na przyj�� bie��cy dokument do dziennika
                incomingDivisions = new LinkedHashMap();

                if (DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE))
                {
                    List docJournals = Journal.findByType(Journal.INCOMING);
                    for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                    {
                        Journal j = (Journal) iter.next();
                        try
                        {
                            if (j.findDocumentEntry(getDocumentId()) == null)
                                incomingDivisions.put(j.getId().toString(),
                                    (j.getOwnerGuid() != null ?
                                        DSDivision.find(j.getOwnerGuid()).getName()+" - " : "")
                                    +j.getDescription());
                        }
                        catch (DivisionNotFoundException e)
                        {
                            if (j.findDocumentEntry(getDocumentId()) == null)
                                incomingDivisions.put(j.getId().toString(),
                                    "[nieznany dzia�] - " +
                                    j.getDescription());
                        }
                    }
                }
                else
                {
                    boolean przyjeciePismaKomorkaNadrzedna =
                        DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_NADRZEDNA_PRZYJECIE);

                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                        // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                        List docJournals = Journal.findByDivisionGuid(division.getGuid(), Journal.INCOMING);
                        for (Iterator iter = docJournals.iterator(); iter.hasNext(); )
                        {
                            Journal j = (Journal) iter.next();
                            if (j.findDocumentEntry(getDocumentId()) == null)
                                incomingDivisions.put(j.getId().toString(), division.getName()+" - "+j.getDescription());
                        }

                        // dzienniki w kom�rkach nadrz�dnych
                        if (przyjeciePismaKomorkaNadrzedna)
                        {
                            DSDivision parent = division.getParent();
                            while (parent != null && !parent.isRoot())
                            {
                                // TODO: nie jest pokazywana nazwa dziennika (je�eli > 1 w dziale)
                                List docParentJournals = Journal.findByDivisionGuid(parent.getGuid(), Journal.INCOMING);
                                for (Iterator iter=docParentJournals.iterator(); iter.hasNext(); )
                                {
                                    Journal j = (Journal) iter.next();
                                    if (j.findDocumentEntry(getDocumentId()) == null)
                                        incomingDivisions.put(j.getId().toString(), parent.getName()+" - "+j.getDescription());
                                    // by�o divisions[i].getName()
                                }
                                parent = parent.getParent();
                            }
                        }
                    }
                }

                /* czy zablokowany przed edycja */
                blocked = (document.getBlocked() != null && document.getBlocked());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(),e);
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    /**
     * Wype�nia w�asno�ci u�ywane przez formularz tworzenia
     * nowego dokumentu.
     */
    private class FillNewDocumentForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                canCreate = DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE);
                if (!canCreate)
                    addActionError(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));

                //domyslnie - zarejestrowany
                statusId = 1;

                Map address = GlobalPreferences.getAddress();
                if (address.get(GlobalPreferences.ORGANIZATION) != null)
                {
                    recipientsMap = new LinkedHashMap();

                    Recipient recipient = new Recipient();
                    recipient.setOrganization((String) address.get(GlobalPreferences.ORGANIZATION));
                    recipient.setStreet((String) address.get(GlobalPreferences.STREET));
                    recipient.setZip((String) address.get(GlobalPreferences.ZIP));
                    recipient.setLocation((String) address.get(GlobalPreferences.LOCATION));
                    recipient.setCountry("PL");

                    recipientsMap.put(new JSONObject(recipient.toMap()).toString(),
                        recipient.getSummary());
                }

                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());

                if (retainedObjectId != null)
                {
//                    properties.put("banner", fax.getBanner());
//                    properties.put("faxID", fax.getFaxID());
//                    properties.put("subject", fax.getSubject());
//                    properties.put("fromline", fax.getFromline());
//                    properties.put("rfaxid", fax.getRfaxid());
//                    properties.put("port", fax.getPort());
                    RetainedObject object = RetainedObject.find(retainedObjectId);
                    senderOrganization = object.get("fromline");
                    senderFax = object.get("rfaxid");
                    packageRemarks = sm.getString("FaksZnumeru")+" "+senderFax;
                    description = "Faks" +
                        (object.get("subject") != null ? " ("+object.get("subject")+")" : "");

                    List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.findByPartialName("faks");
                    if (!kinds.isEmpty())
                        kindId = kinds.get(0).getId();
                }
                original = true;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class FillCreateForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.skip(EV_FILL_DOCUMENT);

            // recipients to skrypty OGNL tworz�ce obiekty java.util.Map
            // trzeba tu odtworzy� nazwy opcji recipients, bo formularz
            // przekazuje tylko ich warto�ci jako skrypty OGNL tworz�ce
            // obiekty Map
            if (recipients != null)
            {
                recipientsMap = new LinkedHashMap();

                try
                {
                    for (int i=0; i < recipients.length; i++)
                    {
//                        Map personMap = (Map) new OgnlExpression(recipients[i]).getTotal();
//                        recipientsMap.put(recipients[i], new Person().fromMap(personMap).getSummary());
                        recipientsMap.put(recipients[i], new Person().fromJSON(recipients[i]).getSummary());
//                            (personMap.get("firstname") != null ? (String) personMap.get("firstname")+" " : "") +
//                            (personMap.get("lastname") != null ? (String) personMap.get("lastname") : "") +
//                            "/" +
//                            (personMap.get("organization") != null ? (String) personMap.get("organization") : "")+
//                            "/" +
//                            (personMap.get("location") != null ? (String) personMap.get("location") : ""));
                    }
                }
                catch (EdmException e)
                {
                    event.getLog().error(e.getMessage(), e);
                    addActionError(sm.getString("BladPodczasTworzeniaDokumentu.SkontaktujSieZadministratoremSystemu"));
                    return;
                }
            }

            prepareScanner();
        }
    }

    private class ValidateCreateOrUpdate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date dtDocumentDate = DateUtils.nullSafeParseJsDate(documentDate);
            Date dtStampDate = DateUtils.nullSafeParseJsDate(stampDate);

            Date now = new Date();

            if (StringUtils.isEmpty(documentDate) || dtDocumentDate == null)
                addActionError("Pola data pisma nie zosta�o uzupe�nione.");
            
            if (!StringUtils.isEmpty(documentDate) && dtDocumentDate == null)
                addActionError(sm.getString("NiepoprawnaDataPisma"));
            else if (dtDocumentDate != null && dtDocumentDate.after(now))
                addActionError(sm.getString("DataPismaZnajdujeSieWprzyszlosci"));

            if (!StringUtils.isEmpty(stampDate) && dtStampDate == null)
                addActionError(sm.getString("NiepoprawnaDataZnaczka"));
            else if (dtStampDate != null && dtStampDate.after(now))
                addActionError(sm.getString("DataStemplaZnajdujeSieWprzyszlosci"));

            if (kindId == null)
                addActionError(sm.getString("NieWybranoRodzajuPisma"));

            if (StringUtils.isEmpty(description))
                addActionError(sm.getString("NiePodanoOpisuPisma"));

            if (!StringUtils.isEmpty(senderZip) && !senderZip.matches("[0-9]{2}-[0-9]{3}"))
                addActionError(sm.getString("NiepoprawnyKodPocztowyPoprawnyKodToNp"));

            if (hasActionErrors())
            {
            	saveFile();
                event.setAttribute("error", Boolean.TRUE);
                if(event.hasListener(EV_CREATE)){
                	event.skip(EV_CREATE);
                } else if(event.hasListener(EV_UPDATE)){
                	event.skip(EV_UPDATE);
                }
            }
        }
    }

    private class Create implements ActionListener
    {
        public synchronized void actionPerformed(ActionEvent event)
        {
        	InOfficeDocument doc = new InOfficeDocument();
            Journal journal = null;
            Date entryDate = null;
            Long journalId = null;
            boolean instantWf = false;
            doc.setSubmitToBip(submitToAsiBip);
            
            try
            {
            	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
	            Sender sender = new Sender();
	            if (senderAnonymous)
	            {
	                sender.setAnonymous(true);
	            }
	            else
	            {
	                sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
	                sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
	                sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
	                sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
	                sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
	                sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
	                sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
	                sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
	                sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
	                sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
	            }

	            doc.setSender(sender);

	            // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
	            // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
	            // obiektu Person
	            if (recipients != null)
	            {
	                Map personMap = null;
	                //String ognlExpr = null;
	                String json = null;
	                try
	                {
	                    for (int i=0; i < recipients.length; i++)
	                    {
	                        json = recipients[i];
	                        Recipient recipient = new Recipient();
	                        recipient.fromJSON(json);
	                        doc.addRecipient(recipient);
	                    }
	                }
	                catch (EdmException e)
	                {
	                	saveFile();
	                    event.getLog().error(e.getMessage(), e);
	                    addActionError(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu")+" json="+json+
	                        " personMap="+personMap);
	                    return;
	                }
	            }

	            doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber, 20));

	            if(original==null){
	            	doc.setOriginal(false);
	            }
	            else
	            	doc.setOriginal(original);
	            doc.setPackageRemarks(TextUtils.trimmedStringOrNull(packageRemarks, 240));
	            doc.setOtherRemarks(TextUtils.trimmedStringOrNull(otherRemarks, 240));

	            doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
	            doc.setDivisionGuid((currentAssignmentGuid!= null ? currentAssignmentGuid :DSDivision.ROOT_GUID ));
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);

	            // sprawdzanie unikalno�ci znaku pisma
	            if (!acceptDuplicateReferenceId && TextUtils.trimmedStringOrNull(referenceId) != null)
	            {
	                try
	                {
	                //    DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

	                    existingDocuments = InOfficeDocument.findByReferenceId(TextUtils.trimmedStringOrNull(referenceId));
	                    if (existingDocuments.size() > 0)
	                    {
	                        event.setResult("used-reference-id");
	                        return;
	                    }
	                }
	                catch (EdmException e)
	                {
	                	saveFile();
	                    addActionError(e.getMessage());
	                    return;
	                }
	                finally
	                {
	                //    try { DSApi.close(); } catch (Exception e) { }
	                }
	            }

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());

                entryDate = new Date(currentDay.getTime().getTime());

//                Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());
//
//                // je�eli faktyczna data jest p�niejsza ni� otwarty
//                // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
//                if (new Date().after(breakOfDay))
//                {
//                    currentDay.setTime(breakOfDay);
//                }
//                else
//                {
//                    Calendar now = Calendar.getInstance();
//                    currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
//                    currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
//                    currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
//                }

/*
                DateFormat df = new SimpleDateFormat(GlobalConstants.DAY_FORMAT);
                Calendar currentDay = Calendar.getInstance();
                try
                {
                    currentDay.setTime(df.parse(DSApi.context().getProperty(Properties.CURRENT_DAY, null)));
                    Calendar now = Calendar.getInstance();
                    currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                }
                catch (ParseException e)
                {
                    throw new EdmException("Niepoprawna posta� daty systemowej");
                }
*/

                //List journals = Journal.findByDivisionGuid(Division.ROOT_GUID, Journal.INCOMING);



                // je�eli numer kancelaryjny ma by� pobierany z innego dziennika
                // ni� g��wny, znajduj� ten dziennik
                if (GlobalPreferences.isOfficeNumbersFromAuxJournals())
                {
                    if (incomingJournalId == null)
                        throw new EdmException(sm.getString("NieWybranoDziennikaPismPrzychodzacych"));

                    journal = Journal.find(incomingJournalId);
                    if (!DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()) ||
                        !Journal.INCOMING.equals(journal.getJournalType()))
                        throw new EdmException(sm.getString("SpodziewanoSieDziennikaPismPrzychodzacychWdzialeGlownym"));
                }
                else
                {
                    journal = Journal.getMainIncoming();
                }

                journalId = journal.getId();
                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                if(numAttachments != null)doc.setNumAttachments(numAttachments);
                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
                doc.setStampDate(DateUtils.nullSafeParseJsDate(stampDate));


                doc.setIncomingDate(currentDay.getTime());

                doc.setLocation(TextUtils.trimmedStringOrNull(location, 60));
                doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));


                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                // referent nie jest na razie przydzielany
                //doc.setClerk(DSApi.context().getDSUser().getName());
                doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
                doc.setKind(kind);

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                if (statusId != null)
                {
                    doc.setStatus(InOfficeDocumentStatus.find(statusId));
                }

                if (deliveryId != null)
                {
                    doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
                }

                if (outgoingDeliveryId != null)
                {
                    doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));
                }

                doc.create();
                newDocumentId = doc.getId();

                /*try
                {
                    doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NATIONWIDE_KIND));
                }
                catch(Exception exc)
                {*/
                doc.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));
                //}

                // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
                // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
                // nie zwi�zanego z folderem (createAttachment zapisuje dokument
                // w bazie).
                if (kind.getRequiredAttachments().size() > 0)
                {
                    for (Iterator iter=kind.getRequiredAttachments().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                        doc.createAttachment(new Attachment(req.getTitle()));
                    }
                }

                // tworzenie za��cznika z maila
                if (messageId != null)
                {
                    MailMessage message = MailMessage.find(messageId);

                    Attachment attachment = new Attachment("Mail");
                    doc.createAttachment(attachment);
                    attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
                    attachment.setWparam(messageId);
                    message.setProcessed(Boolean.TRUE);
                }

                // kopiowanie za��cznik�w z obiektu przechowalni
                if (retainedObjectId != null)
                {
                    RetainedObject object = RetainedObject.find(retainedObjectId);
                    doc.setSource(object.getSource());
                    if (!object.getAttachments().isEmpty())
                    {
                        for (RetainedObjectAttachment objectAttachment : object.getAttachments())
                        {
                            Attachment attachment = new Attachment("Faks");
                            doc.createAttachment(attachment);
                            attachment.createRevision(objectAttachment.getInputStream(),
                                objectAttachment.getSize(), objectAttachment.getFilename());
                        }
                    }
                    object.delete();
                }

                // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
                // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
                // nie zwi�zanego z folderem (createAttachment zapisuje dokument
                // w bazie).

                Upload up = (Upload) ServiceManager.getService(Upload.NAME);

                if (multiToken != null && up.isMultiTokenValid(multiToken))
                {
                    Iterator iter = up.retrieveMultiFiles(multiToken);

                    if (multitiff)
                    {
                        Tiff tiff = new Tiff();
                        while (iter.hasNext())
                        {
                            Map.Entry entry = (Map.Entry) iter.next();
                            String filename = (String) entry.getKey();
                            File file = (File) entry.getValue();

                            tiff.add(file, MimetypesFileTypeMap.getInstance().
                                    getContentType(filename));
                        }

                        File tif = tiff.write(
                            TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
                            TIFFWriter.TIFF_COMPRESSION_GROUP4);
                        Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                        doc.createAttachment(attachment);
                        attachment.createRevision(tif).setOriginalFilename(tif.getName());
                    }
                    else // !multitiff
                    {
                        while (iter.hasNext());
                        {
                            Map.Entry entry = (Map.Entry) iter.next();
                            String filename = (String) entry.getKey();
                            File file = (File) entry.getValue();

                            Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                            doc.createAttachment(attachment);
                            attachment.createRevision(file).setOriginalFilename(filename);
                        }
                    }
                }

                if (file != null && file.sensible())
                {
                    Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull("Skan", 254));
                    doc.createAttachment(attachment);
                    attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
                }
		        addFiles(doc);
		        if (doc.isSubmitToBip())
                {
                	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(doc.getId());
                	DataMartEventBuilder.get().log();
                	
                	//DataMartManager.storeEvent(new DataMartEvent(false, doc.getId() , null, DataMartDefs.BIP_DOCUMENT_INFO, null, null, null, null));
                }
                DSApi.context().commit();
                if(doc != null)
                	setDocumentId(doc.getId());
            }
            catch (Throwable e)
            {
            	event.skip(EV_PRINT_LABEL);
            	saveFile();
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(sm.getString("WystapilBlad")+" ("+e.getMessage()+")");
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }

            // tworzenie wpisu w dzienniku

            Integer sequenceId;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);

                // powi�zanie dokumentu z bie��c� sesj�
                doc = InOfficeDocument.findInOfficeDocument(doc.getId());
                doc.bindToJournal(journalId, sequenceId);
                if(StringUtils.isEmpty(doc.getBarcode()))
                {
                	doc.setBarcode(BarcodeUtils.getKoBarcode(doc));
                }
                //Journal journal = DSApi.context().getOfficeHelper().getJournal(Journal.INCOMING, Division.ROOT_GUID);
                //doc.setOfficeNumber(entry.getSequenceId());

                String instantUser = null;
                String instantDivision = null;

                if (Configuration.officeAvailable() || Configuration.faxAvailable())
                {
                    // tworzenie i uruchamianie wewn�trznego procesu workflow


                    if (!StringUtils.isEmpty(instantAssignment) &&
                        instantAssignment.indexOf('/') > 0)
                    {
                        String guid = instantAssignment.substring(0, instantAssignment.indexOf('/'));
                        String username = instantAssignment.substring(instantAssignment.indexOf('/')+1);
                        DSDivision division = DSDivision.find(guid);
                        DSUser user = DSUser.findByUsername(username);

                        // id uczestnika (patrz WfProcessImpl.findResources())
                        WorkflowFactory.createNewProcess(doc,false, "Pismo przyj�te", username, division.getGuid(), username);

                        instantWf = true;
                        instantUser = user.asFirstnameLastname();
                        instantDivision = division.getPrettyPath();
                    }
                    // dekretacja na bie��cego u�ytkownika
                    else
                    {
                        // id uczestnika (patrz WfProcessImpl.findResources())
                    	WorkflowFactory.createNewProcess(doc, false, "Pismo przyj�te");
                    }

                }

                DSApi.context().commit();

               

                AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                    doc.getId(),
                    Arrays.asList(new Sender[] { doc.getSender() }));

                // parametr u�ywany przez result=created w xwork.xml
                //setDocumentId(doc.getId());
                if(attachCase)
                {
                	addActionMessage(sm.getString("PrzyjetoPismo")+" "+doc.getFormattedOfficeNumber());
                	event.setResult("case");
                	event.setAttribute("documentId", doc.getId());
                	event.cancel();
                }
                else
                if (createNext)
                {
                    addActionMessage(sm.getString("PrzyjetoPismo")+" "+doc.getFormattedOfficeNumber());
                    event.skip(EV_FILL_DOCUMENT); 
                }
                else
                {
                    if (Configuration.officeAvailable() || Configuration.faxAvailable())
                    {
                        if (instantWf)
                        {
                        	addActionMessage(sm.getString("PrzyjetoPismoIprzekazanoJeDoUzytkownikaWdziale", doc.getFormattedOfficeNumber(), instantUser, instantDivision));
                            /*addActionMessage("Przyj�to pismo "+doc.getFormattedOfficeNumber()+
                                " i przekazano je do u�ytkownika "+instantUser+
                                " w dziale "+instantDivision);*/
                            event.setResult("instantwf-created");
                        }
                        else 
                        {
                            event.setResult("task-list");
                        }
                    }
                    else
                    {
                        addActionMessage(sm.getString("PrzyjetoPismo")+" "+doc.getFormattedOfficeNumber());
                        event.setResult("simpleoffice-created");
                    }
                    event.skip(EV_FILL_CREATE);
                }
            }
            catch (EdmException e)
            {
              	event.skip(EV_PRINT_LABEL);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                DSApi.context().begin();

                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(getDocumentId());

                doc.setSummary(TextUtils.trimmedStringOrNull(description, 80));

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
                doc.setKind(kind);

                if (DSApi.context().hasPermission(DSPermission.BIP_EKSPORT))
                    doc.setSubmitToBip(submitToAsiBip);

                if (statusId != null)
                {
                    InOfficeDocumentStatus status = InOfficeDocumentStatus.find(statusId);

                    if (Configuration.hasExtra("business"))
                    {
                        if ("ODRZUCONY".equals(status.getCn()))
                        {
                            event.setResult("nationwide-ODRZUCONY");
                        }
                        else
                        {
                            doc.setStatus(status);
                            if (status.getCn() != null)
                            {
                                doc.getDoctype().set(doc.getId(),
                                    doc.getDoctype().getFieldByCn("STATUS").getId(),
                                    doc.getDoctype().getFieldByCn("STATUS").getEnumItemByCn(status.getCn()).getId());
                            }
                        }
                    }
                    else
                    {
                        doc.setStatus(status);
                    }
                }

                trackingNumber = doc.getTrackingNumber();
                creatingUser = DSUser.safeToFirstnameLastname(doc.getCreatingUser());
                journalMessages = new LinkedHashMap();
                List journals = Journal.findByDocumentId(doc.getId());
                for (Iterator iter=journals.iterator(); iter.hasNext(); )
                {
                    Journal journal = (Journal) iter.next();
                    // g��wny dziennik jest uwzgl�dniony w polu officeNumber
                    if (journal.getOwnerGuid() != null &&
                        !DSDivision.ROOT_GUID.equals(journal.getOwnerGuid()))
                    {
                        try
                        {
                            DSDivision division = DSDivision.find(journal.getOwnerGuid());
                            journalMessages.put(division.getName(), journal.findDocumentEntry(doc.getId()).getSequenceId());
                        }
                        catch (DivisionNotFoundException e)
                        {
                        }
                    }
                }

                doc.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
                doc.setStampDate(DateUtils.nullSafeParseJsDate(stampDate));
                doc.setLocation(TextUtils.trimmedStringOrNull(location, 60));
                doc.setPackageRemarks(TextUtils.trimmedStringOrNull(packageRemarks, 240));
                doc.setOtherRemarks(TextUtils.trimmedStringOrNull(otherRemarks, 240));
                doc.setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber, 20));
                doc.setDivisionGuid((currentAssignmentGuid!= null ? currentAssignmentGuid :DSDivision.ROOT_GUID ));
                if(original==null){
                	doc.setOriginal(false);
                }
                else
                	doc.setOriginal(original);
                if(numAttachments != null)doc.setNumAttachments(numAttachments);
                doc.setReferenceId(TextUtils.trimmedStringOrNull(referenceId, 60));
                doc.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));

                if (!StringUtils.isEmpty(assignedUser))
                {
                    // UserNotFoundException
                    doc.setClerk(DSUser.findByUsername(assignedUser).getName());
                }

                // XXX: dodano 18.10.2004 - edycja wszystkich p�l
                //doc.setSenderAnonymous(senderAnonymous);

                Sender sender = doc.getSender();
                if (senderAnonymous)
                {
                    sender.setAnonymous(true);
                    sender.setTitle(null);
                    sender.setFirstname(null);
                    sender.setLastname(null);
                    sender.setOrganization(null);
                    sender.setStreet(null);
                    sender.setZip(null);
                    sender.setLocation(null);
                    sender.setCountry(null);
                    sender.setEmail(null);
                    sender.setFax(null);
                }
                else
                {
                    sender.setAnonymous(false);
                    sender.setTitle(TextUtils.trimmedStringOrNull(senderTitle, 30));
                    sender.setFirstname(TextUtils.trimmedStringOrNull(senderFirstname, 50));
                    sender.setLastname(TextUtils.trimmedStringOrNull(senderLastname, 50));
                    sender.setOrganization(TextUtils.trimmedStringOrNull(senderOrganization, 50));
                    sender.setStreet(TextUtils.trimmedStringOrNull(senderStreetAddress, 50));
                    sender.setZip(TextUtils.trimmedStringOrNull(senderZip, 14));
                    sender.setLocation(TextUtils.trimmedStringOrNull(senderLocation, 50));
                    sender.setCountry(TextUtils.trimmedStringOrNull(senderCountry, 3));
                    sender.setEmail(TextUtils.trimmedStringOrNull(senderEmail, 50));
                    sender.setFax(TextUtils.trimmedStringOrNull(senderFax, 30));
                }

                // warto�ci okre�laj�ce odbiorc�w pobrane z formularza s�
                // skryptami OGNL tworz�cymi obiekty Map zawieraj�ce w�asno�ci
                // obiektu Person
                if (recipients != null)
                {
                    String json = null;
                    try
                    {
                    	doc.setRecipients(recipients);
                    }
                    catch (EdmException e)
                    {
                        LOG.error(e.getMessage(), e);
                        throw new EdmException(sm.getString("BladPodczasTworzeniaDokumentuSkontaktujSieZadministratoremSystemu")+" json="+json);
                    }
                }
                else
                {
					for (Recipient recipient : doc.getRecipients())
					{
						recipient.delete();
					}
                }

                if (deliveryId != null)
                {
                    doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));
                }

                if (outgoingDeliveryId != null)
                {
                    doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));
                }

                if (doc.isSubmitToBip())
                {
                	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(doc.getId());
                	//DataMartEventBuilder.get().log();
                }
                
                DSApi.context().commit();
                try{
                    DSApi.context().session().flush();
                }catch(HibernateException e){

                }
                DSApi.context().begin();
                	TaskSnapshot.updateAllTasksByDocumentId(doc.getId(), InOfficeDocument.TYPE);
                DSApi.context().commit();
                
                /*
                if (doc.isSubmitToBip())
                {
                    try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        bip.submit(doc);
                    }
                    catch (ServiceException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                }
                */

                event.skip(EV_FILL_CREATE);
            }
            catch (EdmException e)
            {
            	LOG.error("", e);
                addActionError(e.getMessage());
                try{DSApi.context().setRollbackOnly();}catch(Exception e2){};
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }

    private class IntoJournal implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (intoJournalId == null)
                return;

            DSDivision division;
            Date entryDate;
            Journal journal;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

                boolean komorkaPrzyjecie = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);
                boolean wszedziePrzyjecie = DSApi.context().hasPermission(DSPermission.PISMO_WSZEDZIE_PRZYJECIE);

                if (!komorkaPrzyjecie && !wszedziePrzyjecie)
                    throw new EdmException(sm.getString("BrakUprawnienDoPrzyjmowaniaPismaWdziale"));

                // TODO: porz�dniejsze sprawdzenie uprawnie� do przyj�cia pisma w dzienniku

//                ActivityId activityId = WorkflowUtils.parseActivityId(getActivity());
//                WfActivity activity = DSApi.context().getWfActivity(activityId.getWfName(), activityId.getActivityKey());
//
//                final Long documentId = (Long) WorkflowUtils.findParameter(
//                    activity.process_context(), "ds_document_id").
//                    value().value();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                entryDate = GlobalPreferences.getCurrentDay();

                journal = Journal.find(intoJournalId);

                if (journal.findDocumentEntry(document.getId()) != null)
                    throw new EdmException(sm.getString("PismoZnajdujeSieJuzWwybranymDzienniku"));

                if (journal.getOwnerGuid() == null)
                    division = DSDivision.find(DSDivision.ROOT_GUID);
                else
                    division = DSDivision.find(journal.getOwnerGuid());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }


            Integer sequenceId;
            try
            {
                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                DSApi.context().begin();
                sequenceId = Journal.TX_newEntry2(journal.getId(), getDocumentId(), entryDate);


                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                document.setAssignedDivision(division.getGuid());
                addActionMessage(sm.getString("PismoZostaloPrzyjeteWdziennikuDzialuPodNumerem", division.getName(), sequenceId));
                /*addActionMessage("Pismo zosta�o przyj�te w dzienniku dzia�u "+
                    division.getName()+" pod numerem "+sequenceId);*/

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                try { DSApi.close(); } catch (Exception e) { }
            }
        }
    }


    public Boolean getCanExportToBip()
    {
        return canExportToBip;
    }

    public void setCanExportToBip(Boolean canExportToBip)
    {
        this.canExportToBip = canExportToBip;
    }

    public List getKinds()
    {
        return kinds;
    }

    public void setRecipients(String[] recipients)
    {
        this.recipients = recipients;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getStampDate()
    {
        return stampDate;
    }

    public void setStampDate(String stampDate)
    {
        this.stampDate = stampDate;
    }

    public boolean isSubmitToAsiBip()
    {
        return submitToAsiBip;
    }

    public void setSubmitToAsiBip(boolean submitToAsiBip)
    {
        this.submitToAsiBip = submitToAsiBip;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isSenderAnonymous()
    {
        return senderAnonymous;
    }

    public void setSenderAnonymous(boolean senderAnonymous)
    {
        this.senderAnonymous = senderAnonymous;
    }

    public String getSenderTitle()
    {
        return senderTitle;
    }

    public void setSenderTitle(String senderTitle)
    {
        this.senderTitle = senderTitle;
    }

    public String getSenderFirstname()
    {
        return senderFirstname;
    }

    public void setSenderFirstname(String senderFirstname)
    {
        this.senderFirstname = senderFirstname;
    }

    public String getSenderLastname()
    {
        return senderLastname;
    }

    public void setSenderLastname(String senderLastname)
    {
        this.senderLastname = senderLastname;
    }

    public String getSenderOrganization()
    {
        return senderOrganization;
    }

    public void setSenderOrganization(String senderOrganization)
    {
        this.senderOrganization = senderOrganization;
    }

    public String getSenderStreetAddress()
    {
        return senderStreetAddress;
    }

    public void setSenderStreetAddress(String senderStreetAddress)
    {
        this.senderStreetAddress = senderStreetAddress;
    }

    public String getSenderZip()
    {
        return senderZip;
    }

    public void setSenderZip(String senderZip)
    {
        this.senderZip = senderZip;
    }

    public String getSenderLocation()
    {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation)
    {
        this.senderLocation = senderLocation;
    }

    public String getSenderCountry()
    {
        return senderCountry;
    }

    public void setSenderCountry(String senderCountry)
    {
        this.senderCountry = senderCountry;
    }

    public String getSenderEmail()
    {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail)
    {
        this.senderEmail = senderEmail;
    }

    public String getSenderFax()
    {
        return senderFax;
    }

    public void setSenderFax(String senderFax)
    {
        this.senderFax = senderFax;
    }

    public String getSenderPesel()
    {
        return senderPesel;
    }

    public void setSenderPesel(String senderPesel)
    {
        this.senderPesel = senderPesel;
    }

    public String getSenderNip()
    {
        return senderNip;
    }

    public void setSenderNip(String senderNip)
    {
        this.senderNip = senderNip;
    }

    public String getSenderRegon()
    {
        return senderRegon;
    }

    public void setSenderRegon(String senderRegon)
    {
        this.senderRegon = senderRegon;
    }

    public Map getRecipientsMap()
    {
        return recipientsMap;
    }

    public String getPostalRegNumber()
    {
        return postalRegNumber;
    }

    public void setPostalRegNumber(String postalRegNumber)
    {
        this.postalRegNumber = postalRegNumber;
    }

    public String getPackageRemarks()
    {
        return packageRemarks;
    }

    public void setPackageRemarks(String packageRemarks)
    {
        this.packageRemarks = packageRemarks;
    }

    public String getOtherRemarks()
    {
        return otherRemarks;
    }

    public void setOtherRemarks(String otherRemarks)
    {
        this.otherRemarks = otherRemarks;
    }

    /*
    public int getNumAttachments()
    {
        return numAttachments;
    }

    public void setNumAttachments(int numAttachments)
    {
        this.numAttachments = numAttachments;
    }
    */

    public InOfficeDocumentKind getKind()
    {
        return kind;
    }

    public InOfficeDocumentStatus getStatus()
    {
        return status;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public void setStatusId(Integer statusId)
    {
        this.statusId = statusId;
    }

    public List getStatuses()
    {
        return statuses;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getAssignedUser()
    {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

//    public List getUsers()
//    {
//        return users;
//    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public Map getJournalMessages()
    {
        return journalMessages;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public void setIntoJournalId(Long intoJournalId)
    {
        this.intoJournalId = intoJournalId;
    }

    public Map getIncomingDivisions()
    {
        return incomingDivisions;
    }

    public String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public Set getInstantAssignmentUsers()
    {
        return instantAssignmentUsers;
    }

    public void setInstantAssignment(String instantAssignment)
    {
        this.instantAssignment = instantAssignment;
    }

    public String getInstantAssignment()
    {
        return instantAssignment;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getAssignedDivision()
    {
        return assignedDivision;
    }

    public boolean isIntoJournalPermission()
    {
        return intoJournalPermission;
    }

    public InOfficeDocument getDocument()
    {
        return document;
    }

    public List getExistingDocuments()
    {
        return existingDocuments;
    }

    public boolean isAcceptDuplicateReferenceId()
    {
        return acceptDuplicateReferenceId;
    }

    public void setAcceptDuplicateReferenceId(boolean acceptDuplicateReferenceId)
    {
        this.acceptDuplicateReferenceId = acceptDuplicateReferenceId;
    }

    public Integer getStatusId()
    {
        return statusId;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public List getIncomingJournals()
    {
        return incomingJournals;
    }

    public void setIncomingJournalId(Long incomingJournalId)
    {
        this.incomingJournalId = incomingJournalId;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public InOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public OutOfficeDocumentDelivery getOutgoingDelivery()
    {
        return outgoingDelivery;
    }

    public Integer getOutgoingDeliveryId()
    {
        return outgoingDeliveryId;
    }

    public void setOutgoingDeliveryId(Integer outgoingDeliveryId)
    {
        this.outgoingDeliveryId = outgoingDeliveryId;
    }

    public List getOutgoingDeliveries()
    {
        return outgoingDeliveries;
    }

    public DSUser[] getClerks()
    {
        return clerks;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public Boolean getUseBarcodes()
    {
        return useBarcodes;
    }

    public String getBarcodePrefix()
    {
        return barcodePrefix;
    }

    public String getBarcodeSuffix()
    {
        return barcodeSuffix;
    }

    public Long getNewDocumentId()
    {
        return newDocumentId;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }

    public boolean isCanSetClerk()
    {
        return canSetClerk;
    }

    public boolean isUseBarcodePrinter()
    {
        return useBarcodePrinter;
    }

    public String getBarcodePrinterDevice()
    {
        return barcodePrinterDevice;
    }

    public String getDocumentBarcode()
    {
        return documentBarcode;
    }

    public String getPersonDirectoryDivisionGuid()
    {
        return personDirectoryDivisionGuid;
    }

    public Long getRetainedObjectId()
    {
        return retainedObjectId;
    }

    public void setRetainedObjectId(Long retainedObjectId)
    {
        this.retainedObjectId = retainedObjectId;
    }

    public Long getMessageId()
    {
        return messageId;
    }

    public void setMessageId(Long messageId)
    {
        this.messageId = messageId;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public boolean isCanChangeKind() {
        return canChangeKind;
    }

    public boolean isCanCreate() {
        return canCreate;
    }

    public boolean isDaa()
    {
        return daa;
    }

    public boolean isUseScanner()
    {
        return useScanner;
    }

    public boolean isMultitiff()
    {
        return multitiff;
    }

    public void setMultitiff(boolean multitiff)
    {
        this.multitiff = multitiff;
    }

    public String getMultiToken()
    {
        return multiToken;
    }

    public void setMultiToken(String multiToken)
    {
        this.multiToken = multiToken;
    }
    public Map getSubstitute(){
    	return substitute;
    }

    public void setCreateNext(boolean createNext)
    {
        this.createNext = createNext;
    }

	public Integer getNumAttachments() {
		return numAttachments;
	}

	public void setNumAttachments(Integer numAttachments) {
		this.numAttachments = numAttachments;
	}

	public Boolean getOriginal() {
		return original;
	}

	public void setOriginal(Boolean original) {
		this.original = original;
	}

	public String getCurrentAssignmentGuid() {
		return currentAssignmentGuid;
	}

	public void setCurrentAssignmentGuid(String currentAssignmentGuid) {
		this.currentAssignmentGuid = currentAssignmentGuid;
	}

	public DSDivision[] getDivisions() {
		return divisions;
	}

	public void setDivisions(DSDivision[] divisions) {
		this.divisions = divisions;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public boolean isAttachCase()
	{
		return attachCase;
	}

	public void setAttachCase(boolean attachCase)
	{
		this.attachCase= attachCase;
	}
	
	public boolean getSave()
	{
		return save;
	}
	
	public void setSave(boolean save)
	{
		this.save=save;
	}
}
