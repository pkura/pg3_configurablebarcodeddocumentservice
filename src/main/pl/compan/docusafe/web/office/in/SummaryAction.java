package pl.compan.docusafe.web.office.in;

import java.awt.Point;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.dockinds.process.WorkflowUtils;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.DrsInfo;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.commons.OfficeDocumentHelper;
import pl.compan.docusafe.web.commons.RemarkableAction;
import pl.compan.docusafe.web.office.CloneDocumentManager;
import pl.compan.docusafe.web.office.common.SummaryTabAction;
import pl.compan.docusafe.web.office.common.RemarksTabAction.RemarkBean;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.pair;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SummaryAction.java,v 1.175 2010/08/13 10:30:29 pecet5 Exp $
 */
public class SummaryAction extends SummaryTabAction implements RemarkableAction
{
    // @EXPORT
	static final Logger log = LoggerFactory.getLogger(SummaryAction.class);
    private static final String EV_FILL = "fill";


    private InOfficeDocument document;
    private String trackingNumber;
    private List<Flags.Flag> globalFlags;
    private List<Flags.Flag> userFlags;
    private boolean flagsPresent;
    // tablice s� d�u�sze od MAX_FLAGS o 1, poniewa� flagi indeksowane
    // s� od 1 i te indeksy u�yte s� w tre�ci strony JSP
    private Long[] globalFlag;
    private Long[] userFlag;
    private Long[] label;
    private List<pair<Label, Boolean>> hackedLabels;
    private String signatureSrc;
    private Long signatureDocumentId;

    private List<DrsInfo> drsDocs;
    private Date deadlineTime;
    private String deadlineRemark;
    private String deadlineAuthor;
    private String caseLink;
    private String folderPrettyPath;//sciezka katalogowa dokumentu
    private List<Order> orders = new ArrayList<Order>();
    private Map<String, Acceptance> generalAcceptancesToGo;
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private Remark remark;
    private String remarkText;
    private List<RemarkBean> remarks;
    private Integer zgodnoscZumowa;

    private Collection<RenderBean> processRenderBeans;
    // @IMPORT
    /**
     * Lista zbiorczej dekretacji; ka�dy element tablicy ma posta�
     * nazwa_u�ytkownika "," CollectiveAssignmentEntry.id
     */
    private String[] assignments;

    private boolean documentFax;

    private static final String EV_MANUAL_FINISH = "evManualFinish";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignments").
            append(OpenHibernateSession.INSTANCE).
            append(new Assignments()).
            append(EV_FILL, new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        //przycisk zapisz
        registerListener("doUpdateFlags").
            append(OpenHibernateSession.INSTANCE).
            append(new UpdateFlags()).
            append(new UpdateBox()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReopenWf").
            append(OpenHibernateSession.INSTANCE).
            append(new ReopenWf()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doProcessAction")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new ProcessAction().skipListener(EV_FILL))
				.append(EV_FILL,new FillDocumentForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doManualFinish").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualFinish()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doWatch").
            append(OpenHibernateSession.INSTANCE).
            append(new Watch()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClone").
            append(OpenHibernateSession.INSTANCE).
            append(new CloneDocument()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCompileAttachments").
            append(OpenHibernateSession.INSTANCE).
            append(new CompileAttachments()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGiveAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new GiveAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doWithdrawAcceptance").
            append(OpenHibernateSession.INSTANCE).
            append(new WithdrawAcceptance()).
            append(new FillDocumentForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGenerateDocumentView").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateDocumentView()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGenerateCsvDocument").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GenerateCsvDocument()).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("sendToEva").
        	append(OpenHibernateSession.INSTANCE).
        	append(new SendToEva()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSendToDecretation").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new sendToDecretation()).
	    	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doHotIcr").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new hotIcr()).
	    	append(EV_FILL, new FillDocumentForm()).
	    	append(EV_MANUAL_FINISH ,new ManualFinish()).
	    	//append(new FillDocumentForm()).
	    	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignMe").
	        append(OpenHibernateSession.INSTANCE).
	        append(new AssignMe()).
	        append(new FillDocumentForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualPushToCoor").
	        append(OpenHibernateSession.INSTANCE).
	        append(new ManualPushToCoor()).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddRemark").
        	append(OpenHibernateSession.INSTANCE).
			append(new AddRemark()).
			append(new FillDocumentForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdateBox").
	        append(OpenHibernateSession.INSTANCE).
	        append(new UpdateBox()).
	        append(new FillDocumentForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDocumentMetrics").
			append(OpenHibernateSession.INSTANCE).
			append(new DocumentMetrics()).
			append(new FillDocumentForm()).
			appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPrint").
			append(OpenHibernateSession.INSTANCE).
			append(new Print()).
			append(new FillDocumentForm()).
			appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPrintCard").
        	append(OpenHibernateSession.INSTANCE).
        	append(new PrintCard()).
        	append(new FillDocumentForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetXesLog").
        append(OpenHibernateSession.INSTANCE).
        append(new XesLogFromDocument()).
        append(new FillDocumentForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetGraph").
        append(OpenHibernateSession.INSTANCE).
        append(new Graph()).
        append(new FillDocumentForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
    

    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/summary.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    protected String getExternalWorkflowBaseLink()
    {
        return "/office/incoming/external-workflow.action";
    }

    private class ManualPushToCoor implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				WorkflowFactory.forwardToCoordinator(getActivity(), event);
				event.setResult("confirm");
                // TODO: process.terminate()
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
				DSApi.context()._rollback();
				log.debug(e.getLocalizedMessage(),e);
			}


			/*try {
				WorkflowFactory.forwardToCoordinator(event, new String[] { getActivity()}, document);
				event.setResult("task-list");
			} catch (EdmException e) {
				event.addActionError(e.getMessage());
				log.error("", e);
			}*/
		}
    }

    private class FillDocumentForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
            try {

                labelStuff();

            	Integer pom = (Integer) ServletActionContext.getRequest().getSession().getAttribute("offset");
                if(pom != null && pom%2 != 1) {
                    pom += 1;
                    ServletActionContext.getRequest().getSession().setAttribute("offset", pom);
                }

                document = InOfficeDocument.findInOfficeDocument(getDocumentId());

                if(WorkflowFactory.jbpm) {
                    if(getActivity() != null){
                        processRenderBeans = ProcessUtils.renderProcessesForActivityId(document, getActivity(), ProcessActionContext.SUMMARY);
                    } else if(!AvailabilityManager.isAvailable("process.dontShowAll")) {
                        //jak nie ma activity to wy�wietlaj w archiwizacji tak jak w przypadku archiwum (ARCHIVE_VIEW)
                        processRenderBeans = ProcessUtils.renderAllProcesses(document, ProcessActionContext.SUMMARY);
                    }
                }


                new OfficeDocumentHelper().retrieveRemarks(SummaryAction.this);

                for(Recipient dr : document.getRecipients()){
                	dr.getId();
                }
                document.getSender();

                if (document.getDocumentKind() != null){

                    document.getDocumentKind().initialize();
                    if ("true".equals(document.getDocumentKind().getProperties().get(DocumentKind.SHOW_FOLDER_IN_SUMMARY_KEY))) {
                        folderPrettyPath = document.getFolderPath("/");
                    }

                    if(document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND) || 
                       document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_PTE)) {
                        invoiceKindStuff();
                    }
                }

                if (document.getContainingCase()!=null){
                	caseLink = "/office/edit-case.do?id="+document.getContainingCase().getId()+"&tabId=summary";
                }

                documentFax = "fax".equals(document.getSource());
                
                WorkflowActivity activity = null;

                if (getActivity() != null ) {
                	activity = WorkflowFactory.getWfActivity(getActivity());
                    canAssignToCoordinator = WorkflowUtils.canAssignToCoordinator(getActivity());
                    if(WorkflowFactory.jbpm){
                        canAddToWatched = canAssignToCoordinator;
                    }
                }

                fillForm(document, activity);
                
                if (activity != null) {
                	deadlineAuthor = activity.getDeadlineAuthor();
                	deadlineTime = activity.getDeadlineTime();
                	deadlineRemark = activity.getDeadlineRemark();
                }

                if(!Docusafe.hasExtra("business")){
                    nonBusinessStuff();
                }

                trackingNumber = document.getTrackingNumber();

                flagsStuff();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(),e);
            }
        }

        private void flagsStuff() throws EdmException {
            String flagsOn = Configuration.getProperty("flags");
            if ("true".equals(flagsOn))
            {
                globalFlags = document.getFlags().getDocumentFlags(false);

                userFlags = document.getFlags().getDocumentFlags(true);



                flagsPresent = globalFlags.size() > 0 || userFlags.size() > 0;
            }
            else
                flagsPresent = false;
        }

        private void labelStuff() throws EdmException {
            prepareLabels();
            List<Label> docLabs = LabelsManager.getReadLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
            hackedLabels = new LinkedList<pair<Label,Boolean>>();
            for (Label lab : modifiableLabels) {
                hackedLabels.add(new pair<Label, Boolean>(lab, docLabs.contains(lab)));
            }
        }

        private void invoiceKindStuff() throws EdmException {
            FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
            fm.initialize();
            fm.initializeAcceptances();
            generalAcceptancesToGo = fm.getAcceptancesState().getGeneralAcceptancesDef();


            //FIXME przeniesc obsloge sprawdzenie zgodnosci
            /*if(fm.getValue("ZGODNOSC_Z_UMOWA") != null)
zgodnoscZumowa = (Boolean) fm.getKey("ZGODNOSC_Z_UMOWA");*/
            if(fm.getValue("POTWIERDZENIE_ZGODNOSCI") != null)
                zgodnoscZumowa = (Integer) fm.getKey("POTWIERDZENIE_ZGODNOSCI");


            if(!fm.getValue("AKCEPTACJA_PROSTA").equals("tak") && generalAcceptancesToGo.containsKey("uproszczona")) {
                generalAcceptancesToGo.remove("uproszczona");
            }
        }

        private void nonBusinessStuff() throws EdmException {
            PreparedStatement ps = null;
            try
            {
                ps = DSApi.context().prepareStatement(
                "select id, officeNumber from DSO_ORDER where DOCUMENT_ID = ? and CURRENTASSIGNMENTUSERNAME is not null" );
                ps.setLong(1, getDocumentId());
                ResultSet rs = ps.executeQuery();
                while(rs.next())
                {
                    orders.add(new Order(rs.getLong("id"),rs.getInt("officeNumber")));
                }
                rs.close();

            }
            catch (HibernateException e1)
            {
                log.debug("", e1);

            }
            catch (SQLException e1)
            {
                log.debug("", e1);

            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }

    }

    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                if(!WorkflowFactory.jbpm){
                    // --- WPIS DO HISTORII - tomek uporzadkuj to
                    WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());
                    if (document instanceof OfficeDocument)
                    {
                        document.addAssignmentHistoryEntry(
                                new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), activity.getProcessDesc(),
                                        WorkflowFactory.getInstance().isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));

                        document.setCurrentAssignmentGuid(null);
                        document.setCurrentAssignmentUsername(null);
                        document.setCurrentAssignmentAccepted(null);
                    }
                    boolean finish;
                    finish = StringUtils.isNotEmpty(getActivity())
                            && !WorkflowFactory.getInstance().isManual(getActivity());
                    WorkflowFactory.getInstance().manualFinish(getActivity(), !finish);
                } else { //jbpm4
                    WorkflowFactory.getInstance().manualFinish(getActivity(), true);
                }

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

               	TaskSnapshot.updateByDocumentId(document.getId(),InOfficeDocument.TYPE);
               	DSApi.context().commit();

                event.setResult(getTaskListResult(document.getType()));
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                try {DSApi.context().rollback(); } catch (EdmException f) {};
                addActionError(e.getMessage());
            }
        }
    }
    
    private class sendToDecretation implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			Document doc = Document.find(getDocumentId());
    			if(doc.getDocumentKind().logic() instanceof Acceptable)
    			{
    				try
    				{
    					DSApi.context().begin();
    					
    					AcceptanceCondition ac = ((Acceptable) doc.getDocumentKind().logic()).nextAcceptanceCondition(doc);
	    				String guid = ac.getDivisionGuid() != null ? ac.getDivisionGuid() : DSDivision.ROOT_GUID;
	    				String user = ac.getUsername() != null ? ac.getUsername() : "";
	    				String plannedAssignment = null;
	    				// je�li nazwa uzytkownika, zaczyna sie od "ext:", dekretacja na agenta z adds'�w 
	    				if (user.startsWith("ext:"))
	    				{
	    					String agent = Docusafe.getAdditionProperty("external.acceptance.agent");
	    					plannedAssignment =  guid + "/" + agent + "/" + WorkflowFactory.wfManualName() + "/*/" + user.substring(4);
	    				}
	    				else
	    				{
	    					plannedAssignment = guid + "/" + user + "/" + WorkflowFactory.wfManualName() + "/*/realizacja";
	    				}
    					
	    				WorkflowFactory.multiAssignment(new String[] { getActivity() },DSApi.context().getPrincipalName(), new String[] {plannedAssignment}, plannedAssignment, event);
	    				((Acceptable) doc.getDocumentKind().logic()).onSendDecretation(doc, ac);
	    				event.setResult("task-list");
	    				DSApi.context().commit();
    				}
    				catch (Exception e) 
    				{
						DSApi.context().setRollbackOnly();
						throw new EdmException("NieUdaloSiePrzekazacPisma");
					}
    			}
    			else
    			{
    				throw new EdmException("NiekompatybilnaLogika");
    			}
    		}
    		catch (Exception e) 
    		{
				addActionError(e.getMessage());
			}
    	}
    }

    private class AssignMe implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                String act = WorkflowFactory.findManualTask(getDocumentId());
                WorkflowFactory.getInstance().assignMe(act,event);
                setActivity(act);
                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                TaskSnapshot.updateByDocumentId(document.getId(),InOfficeDocument.TYPE);

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class UpdateFlags implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId());
                List<Long> ids;
                if(globalFlag!=null)
                {
		            for (Long id : globalFlag)
		            {
		                if(!LabelsManager.existsDocumentToLabelBean(getDocumentId(), id)) LabelsManager.addLabel(getDocumentId(), id, DSApi.context().getPrincipalName());
		            }
		            globalFlags = document.getFlags().getDocumentFlags(false);
		            ids = Arrays.asList(globalFlag);
		            for(Flags.Flag f: globalFlags)
		            {
		            	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId()) && f.isCanClear()) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(false))
		            {
		            	if(LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId()) && f.isCanClear()) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
		            }
                }
                if(userFlag!=null)
                {
	                for (Long id :userFlag)
	                {
	                    if(!LabelsManager.existsDocumentToLabelBean(getDocumentId(), id)) LabelsManager.addLabel(getDocumentId(), id, DSApi.context().getPrincipalName());
	                }
	                userFlags = document.getFlags().getDocumentFlags(true);
	                ids = Arrays.asList(userFlag);
	                for(Flags.Flag f: userFlags)
	                {
	                	if(!ids.contains(f.getId()) && LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
	                }
                }
                else
                {
                	for(Flags.Flag f: document.getFlags().getDocumentFlags(true))
 	                {
 	                	if( LabelsManager.existsDocumentToLabelBean(getDocumentId(), f.getId())) LabelsManager.removeLabel(getDocumentId(), f.getId(),DSApi.context().getPrincipalName());
 	                }
                }
                if (AvailabilityManager.isAvailable("labels")) {
	                prepareLabels();
	                if(label!=null)
	                {
		                for (Long id :label)
		                {
		                    if(!LabelsManager.existsDocumentToLabelBean(getDocumentId(), id)) LabelsManager.addLabel(getDocumentId(), id, DSApi.context().getPrincipalName());
		                }
		                //modifiableLabels = document.getFlags().getDocumentFlags(true);
		                ids = Arrays.asList(label);
		                for(Label l: modifiableLabels)
		                {
		                	if(!ids.contains(l.getId()) && LabelsManager.existsDocumentToLabelBean(getDocumentId(), l.getId())) LabelsManager.removeLabel(getDocumentId(), l.getId(),DSApi.context().getPrincipalName());
		                }
	                }
	                else
	                {
	                	for(Label l: modifiableLabels)
	 	                {
	 	                	if( LabelsManager.existsDocumentToLabelBean(getDocumentId(), l.getId())) LabelsManager.removeLabel(getDocumentId(), l.getId(),DSApi.context().getPrincipalName());
	 	                }
	                }
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Assignments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (assignments == null || assignments.length == 0)
            {
                addActionError(sm.getString("NieWybranoUzytkownikowDoDekretacji"));
                return;
            }

            try
            {
                DSApi.context().begin();


                boolean mainDocumentReassigned = collectiveAssignment(assignments, getActivity() , event);

                event.setResult("confirm");
                // TODO: process.terminate()
                if (mainDocumentReassigned)
                    event.skip(EV_FILL);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error("",e);
                try { DSApi.context().rollback(); } catch (EdmException f) {};
                addActionError(e.getMessage());
            }
        }
    }

    private class ReopenWf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                WorkflowFactory.reopenWf(getDocumentId());

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                TaskSnapshot.updateByDocumentId(document.getId(),InOfficeDocument.TYPE);

                DSApi.context().commit();

                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                	LogFactory.getLog("workflow_log").debug(
                			"dla dokumentu przywroconego na liste zadan znaleziono "
                			+activityIds.length
                			+" zadan");
                } else {
                	setActivity(activityIds[0]);
                }

            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class CloneDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                CloneDocumentManager.clone(getDocumentId());

                DSApi.context().commit();

                addActionMessage(sm.getString("DokumentZostalSklonowany"));

            }
            catch (EdmException e)
            {
            	log.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }

        }
    }

    private class Order
    {
        private Integer officeNumber;
        private Long id;
        public Order(Long id, Integer officeNumber)
        {
            this.id = id;
            this.officeNumber = officeNumber;

        }

        public Long getId()
        {
            return id;
        }
        public Integer getOfficeNumber()
        {
            return officeNumber;
        }

    }

    private class AddRemark implements ActionListener {

    	public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				setRemarkText(TextUtils.trimmedStringOrNull(getRemarkText()));

				if(document == null){
					document = InOfficeDocument.findInOfficeDocument(getDocumentId());
				}

				if (getRemarkText() != null) {
					((OfficeDocument) document).addRemark(new Remark(
							StringUtils.left(getRemarkText(), 4000), DSApi.context()
									.getPrincipalName()));
				}
				addActionMessage(sm.getString("DodanoUwage"));
				DSApi.context().commit();

			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getLocalizedMessage(), e);
			}
		}
    }

	/**
	 * Sprawdza czy dokument jest pismem przychodz�cym
	 * @return false je�li nie jest pismem przychodz�cym lub dokument jest null
	 */
	public boolean isInOfficeDocument()
	{
		if (getDocument() == null)
			return false;
		return getDocument() instanceof InOfficeDocument;
	}

	private class Print implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				//DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
				if(AvailabilityManager.isAvailable("print.barcode.create")&& getDocumentId() != null)
				{
					Document doc = getDocument();
					if (!(doc instanceof InOfficeDocument))
					{
						addActionError("DokumentNieJestPismemPrzychodzacym");
						return;
					}
					ZebraPrinterManager manager = new ZebraPrinterManager();
					manager.printLabelFromDocument((InOfficeDocument)doc);
					addActionMessage(sm.getString("WydrukowanoPismo" + doc.getId()));
				}
				else
					addActionError("Nie wybrano dokumentu");
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public void setAssignments(String[] assignments)
    {
        this.assignments = assignments;
    }

    public List getGlobalFlags()
    {
        return globalFlags;
    }

    public List getUserFlags()
    {
        return userFlags;
    }

    public boolean isFlagsPresent()
    {
        return flagsPresent;
    }

    public String getSignatureSrc()
    {
        return signatureSrc;
    }

    public Long getSignatureDocumentId()
    {
        return signatureDocumentId;
    }

    public List<DrsInfo> getDrsDocs()
    {
        return drsDocs;
    }



    public Long[] getGlobalFlag() {
		return globalFlag;
	}

	public void setGlobalFlag(Long[] globalFlag) {
		this.globalFlag = globalFlag;
	}

	public Long[] getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(Long[] userFlag) {
		this.userFlag = userFlag;
	}

	public void setGlobalFlags(List<Flags.Flag> globalFlags) {
		this.globalFlags = globalFlags;
	}

	public void setUserFlags(List<Flags.Flag> userFlags) {
		this.userFlags = userFlags;
	}

	public Date getDeadlineTime()
    {
        return deadlineTime;
    }

    public String getDeadlineRemark()
    {
        return deadlineRemark;
    }

    public String getDeadlineAuthor()
    {
        return deadlineAuthor;
    }

    public boolean isDocumentFax() {
        return documentFax;
    }

	public String getCaseLink() {
		return caseLink;
	}

	public void setCaseLink(String caseLink) {
		this.caseLink = caseLink;
	}

    public String getFolderPrettyPath() {
        return folderPrettyPath;
    }

    public List<Order> getOrders()
    {
        return orders;
    }

    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

	public Map<String, Acceptance> getGeneralAcceptancesToGo() {
		return generalAcceptancesToGo;
	}

	public void setGeneralAcceptancesToGo(
			Map<String, Acceptance> generalAcceptancesToGo) {
		this.generalAcceptancesToGo = generalAcceptancesToGo;
	}

	public void setLabel(Long[] label) {
		this.label = label;
	}

	public Long[] getLabel() {
		LogFactory.getLog("kuba").debug("biore label: " + label.length);
		return label;
	}

	public void setHackedLabels(List<pair<Label, Boolean>> hackedLabels) {
		this.hackedLabels = hackedLabels;
	}

	public List<pair<Label, Boolean>> getHackedLabels() {
		return hackedLabels;
	}


	public List<RemarkBean> getRemarks() {
		return remarks;
	}


	public void setLastRemark(Remark remark) {
		this.remark = remark;
	}


	public void setRemarks(List<RemarkBean> remarks) {
		this.remarks = remarks;
	}


	public void setDocument(OfficeDocument document) {
		this.document = (InOfficeDocument)document;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public Integer getZgodnoscZumowa() {
		return zgodnoscZumowa;
	}

    public boolean isCanAssignToCoordinator() {
        return canAssignToCoordinator;
    }

    public void setCanAssignToCoordinator(boolean canAssignToCoordinator) {
        this.canAssignToCoordinator = canAssignToCoordinator;
    }

    public Collection<RenderBean> getProcessRenderBeans() {
        return processRenderBeans;
    }

    public void setProcessRenderBeans(Collection<RenderBean> processRenderBeans) {
        this.processRenderBeans = processRenderBeans;
    }

}
