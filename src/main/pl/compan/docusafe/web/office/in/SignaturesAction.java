package pl.compan.docusafe.web.office.in;

import java.util.List;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.SignaturesTabAction;

/**
 * User: Michal Manski mailto:michal.manski@com-pan.pl
 * Date: 2006-05-23
 */
public class SignaturesAction extends SignaturesTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/signatures.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
