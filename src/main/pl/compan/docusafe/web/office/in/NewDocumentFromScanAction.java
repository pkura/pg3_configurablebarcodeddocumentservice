package pl.compan.docusafe.web.office.in;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeHandler;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import com.asprise.util.tiff.TIFFReader;
import com.java4less.vision.Barcode1DReader;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Akcja obslugujaca tzw. mala kancelarie
 * @author wkutyla
 *
 */
public class NewDocumentFromScanAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	private FormFile file;
	private Long newDocumentId;
	/**
	 * Czy po przyjeciu dokument ma zostac wyslany do indeksowania
	 */
	private boolean assignToIndexing;
	private StringManager sm = StringManager.getManager(NewDocumentFromScanAction.class.getPackage().getName());

	Logger log = LoggerFactory.getLogger(NewDocumentFromScanAction.class);
	
	private final String CREATE_DOCUMENT_ACTION = "create_document_action";
	private final String LOOK_FOR_BARCODE_ACTION = "look_for_barcode_action";
	private final String FILL_FORM_ACTION = "fill_form";
	
	private final int SIZE_RATIO = 4;
	
	private String Barcode;
	private String BarcodePageFileName;
	private String TiffFileLocation;
	private Rectangle BarcodeRectangle;
	
	/**
	 * Seter ktory tworzy objekt Rectangle ze stringa przekazanego przez javascript
	 * @param barcodeRectangle
	 */
	public void setBarcodeRectangle(String barcodeRectangle) 
	{
		BarcodeRectangle = new Rectangle();
		String[] coords  =  barcodeRectangle.split(";");
		BarcodeRectangle.setRect((Double.parseDouble(coords[0]) * SIZE_RATIO) , (Double.parseDouble(coords[1]) * SIZE_RATIO), (Double.parseDouble(coords[2]) * SIZE_RATIO), (Double.parseDouble(coords[3]) * SIZE_RATIO));
	}

	protected void setup() 
	{
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doLoadFile").
        	append(OpenHibernateSession.INSTANCE).
        	append(new LoadFile()).
        	append(LOOK_FOR_BARCODE_ACTION, new LookForBarcode()).
        	append(CREATE_DOCUMENT_ACTION, new CreateDocument()).
        	append(FILL_FORM_ACTION, new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doManualy").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(LOOK_FOR_BARCODE_ACTION, new LookForBarcode()).
        	append(CREATE_DOCUMENT_ACTION, new CreateDocument()).
        	append(FILL_FORM_ACTION, new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Klasa nic nie robi
	 * @author Tomasz Lipka
	 *
	 */
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }
	
	/**
	 * Klasa tworzy dokument na podstawie barcodu
	 * @author Tomasz Lipka
	 */
	private class CreateDocument implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				log.debug("create new document");
				InOfficeDocument doc = new InOfficeDocument();
				DSApi.context().begin();
				
				//doc.setOriginal(false);
      	      
        	    //Sender sender = new Sender();
        		//sender.setAnonymous(true);
        		//doc.setSender(sender);
        	    doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        	    doc.setDivisionGuid(DSDivision.ROOT_GUID);
        	    doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        	    

                
                doc.setDocumentDate(new Date());

                DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);                
                doc.setSummary(documentKind.getName());

                //InOfficeDocumentKind kind = InOfficeDocumentKind.list().get(0);
                //doc.setKind(kind);

                Calendar currentDay = Calendar.getInstance();
                currentDay.setTime(GlobalPreferences.getCurrentDay());

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                //doc.setIncomingDate(currentDay.getTime());
                doc.setIncomingDate(new Date());

                doc.setAssignedDivision(DSDivision.ROOT_GUID);

                //Calendar cal = Calendar.getInstance();
                //cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

                doc.create();    
                
                doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));     
                                          
                doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION));
                  
                doc.setDocumentKind(documentKind);

                newDocumentId = doc.getId();

                Map<String,Object> values = new HashMap<String,Object>();
                values.put(Prosikalogic.BARCODE_FIELD_CN, Barcode);
                values.put(Prosikalogic.SPOSOB_PRZYJECIA_CN, 50);
               
                documentKind.setOnly(newDocumentId, values);
                documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                documentKind.logic().documentPermissions(doc);
                
                //DSApi.context().session().save(doc);
                DSApi.context().session().flush();
                //DSApi.context().session().refresh(doc);
                 
                	 
                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(Barcode, 254));
                //musze wylaczyc uprawnienia
                doc.setPermissionsOn(false);
                doc.createAttachment(attachment);
                AttachmentRevision ar = attachment.createRevision(file != null ? file.getFile() : new File(TiffFileLocation));
                
                //zakladamy ze przez 'mala kancelarie' i tak przechodza tylko tiffy
                ar.setOriginalFilename(Barcode+".tif");
                
                if(Attachment.ATTACHMENTS_IN_LOCAL_FILE_SYSTEM)
                	ar.setOnDisc(AttachmentRevision.FILE_ON_DISC);
                else
                	ar.setOnDisc(AttachmentRevision.FILE_IN_DATABASE);
				doc.setPermissionsOn(true);
                DSApi.context().session().flush();
                
                Integer sequenceId;
                Long journalId;
                Journal journal = Journal.getMainIncoming();
                journalId = journal.getId();
                sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date());
                doc.bindToJournal(journalId, sequenceId);
        	                                                    
        	    log.trace("utworzono dokument id={}",doc.getId());
        	    log.trace("assignToIndexing ={}",assignToIndexing );
				if (assignToIndexing)
				{																				
					log.trace("document bedzie dekretowany do koordynatora");
					WorkflowFactory.createNewProcess(doc, true);
					event.setResult("document-assigned");
					String mess = "document "+doc.getId()+" o barkodzie "+Barcode + " przekazany do indeksowania";
					event.addActionMessage(mess);
					log.trace(mess);
				}
				else
				{
					WorkflowFactory.createNewProcess(doc, false);
					log.trace("view-doc");
					event.setResult("view-doc");
				}
				doc.getDocumentKind().logic().onStartProcess(doc);
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				addActionError(sm.getString("nieUdaloSiePrzyjacDokumentu"));
				log.error(e.getMessage(), e);
				DSApi.context()._rollback();
			}
			
			
		}
	}
	
	/**
	 * Klasa sprawdza czy podano plik
	 * @author Tomasz Lipka
	 */
	private class LoadFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	if (file == null )
            {
                addActionError("NiePodanoPliku");
                event.skip(LOOK_FOR_BARCODE_ACTION);
                event.skip(CREATE_DOCUMENT_ACTION);
                return;
            }        	
        }
    }
	
	/**
	 * Klasa szukajaca barkodu na przekazanym pliku
	 * 1) szuka barcodu na calym dokumecie
	 * 2) szuka barcodu na obszarach dokumentu (dokument dzieli sie na 6 obszarow)
	 * 3) jesli barcod nie zostal znaleziony i uzytkownik wskazal miejsce szukania barcod jest szukany w obszaze wskazanym przez uzytkownika
	 * @author Tomasz Lipka
	 */
	private class LookForBarcode implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {           
            try
            {
            	if (Docusafe.getPngTempFile() == null)
            		addActionError("Nie mozna uzyskac dostepu do folderu z tymczasowymi plikami.");
            	if(TiffFileLocation == null)
            	{
            		TiffFileLocation = file.getFile().getAbsolutePath();
            	}
            	
            	TIFFReader tiffReader = new TIFFReader(new File(TiffFileLocation));
            	BufferedImage image = (BufferedImage) tiffReader.getPage(0);
            	
            	String bar = BarcodeHandler.DEFAULT_BARCODE; //null;
            	
            	if(BarcodeRectangle == null)
            	{
	            	bar = BarcodeHandler.scanArea(image, Barcode1DReader.INTERLEAVED25, new Rectangle(0 ,0, image.getWidth(), image.getHeight()));
	            	if(BarcodeHandler.DEFAULT_BARCODE.equals(bar))
	            	{
	            		//awaryjna metoda szukania
	            		Rectangle rectangle = new Rectangle(0, 0, image.getWidth(), image.getHeight());
	                	bar = BarcodeHandler.scanAreas(image, Barcode1DReader.INTERLEAVED25, BarcodeHandler.devideRectangle(rectangle, 2, 3));
	            		
	            		if(BarcodeHandler.DEFAULT_BARCODE.equals(bar))
	            		{
	            			File barcodePage = File.createTempFile("barcodePage", ".png", new File(Docusafe.getPngTempFile()));
	            			image = ImageKit.resize(image, (image.getWidth() / SIZE_RATIO), (image.getHeight() / SIZE_RATIO));
	            			ImageIO.write(image, "png", barcodePage);
	            			BarcodePageFileName = ServletActionContext.getRequest().getContextPath()
	            				.concat("/".concat(Docusafe.TEMP_FOLDER_NAME_FOR_PNG_FILES.concat("/")
	            				.concat(barcodePage.getName())));
	            			addActionMessage("ZaznaczBarkod");
	            		}
	            	}
            	}
            	else
            	{
            		bar = BarcodeHandler.scanArea(image, Barcode1DReader.INTERLEAVED25, BarcodeRectangle);
            	}
            	
            	log.trace("{}",bar);
            	if(BarcodeHandler.DEFAULT_BARCODE.equals(bar))
            	{
            		addActionError(sm.getString("NieUdaloSieZnalezcBarkodu"));
            	}
            	else
            	{
            		Integer check = BarcodeManager.isValidReturnInteger(bar);
            		if (check == -1) {
            			addActionError(BarcodeManager.getBarcodeValidTipMessage());
            		}
            		else if(check == -2)
                	{
                		addActionError(sm.getString("BarkodZostalJuzUzyty"));
                	}
                	else if(check == -1)
                	{
                		addActionError(sm.getString("NiepoprawnyBarkod"));
                	}
            	}            	
            	
            	if(hasActionErrors())
            	{
            		event.skip(CREATE_DOCUMENT_ACTION);
            		return;
            	}
            	Barcode = bar;
            }
            catch (Exception e)
            {            	
            	log.error(e.getMessage(), e);
    		}	
        }
    }
	
	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	public Long getNewDocumentId() {
		return newDocumentId;
	}

	public void setNewDocumentId(Long newDocumentId) {
		this.newDocumentId = newDocumentId;
	}

	public String getBarcodePageFileName() {
		return BarcodePageFileName;
	}

	public String getTiffFileLocation() {
		return TiffFileLocation;
	}

	public void setTiffFileLocation(String tiffFileLocation) {
		TiffFileLocation = tiffFileLocation;
	}

	public void setAssignToIndexing(boolean assignToIndexing) {
		this.assignToIndexing = assignToIndexing;
	}

	public boolean getAssignToIndexing() {
		return assignToIndexing;
	}
}
