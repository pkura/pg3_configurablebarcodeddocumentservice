package pl.compan.docusafe.web.office.in;

import org.hibernate.dialect.FirebirdDialect;
import org.hibernate.dialect.SQLServerDialect;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.webwork.event.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

/* User: Administrator, Date: 2005-10-27 13:12:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MainLockedAction.java,v 1.10 2008/10/29 23:06:52 wkuty Exp $
 */
public class MainLockedAction extends EventActionSupport
{
    private String summary;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                InOfficeDocument doc = new InOfficeDocument();
                Sender sender = new Sender();
                sender.setAnonymous(true);
                doc.setSender(sender);
                doc.setSummary(summary);
                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setKind(InOfficeDocumentKind.find(new Integer(1)));
                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.create();

                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("Stworzono dokument "+doc);

                if (Configuration.officeAvailable() || Configuration.faxAvailable())
                {
                    // tworzenie i uruchamianie wewn�trznego procesu workflow
                    WorkflowFactory.createNewProcess(doc, false,"Pismo przyj�te" );
                }

                Journal journal = Journal.getMainIncoming();

                Statement st = DSApi.context().createStatement();
                ResultSet rs;

                // odczytanie bie��cego numeru z ca�kowit� blokad� wiersza

                if (DSApi.getDialect() instanceof FirebirdDialect)
                {
                    rs = st.executeQuery("select sequenceid from dso_journal where id="+
                        journal.getId()+" for update with lock");
                }
                else if (DSApi.getDialect() instanceof SQLServerDialect)
                {
                    rs = st.executeQuery("select sequenceid from dso_journal with (rowlock xlock) " +
                        "where id="+journal.getId());
                }
                else
                {
                    throw new Exception("Nieznany dialekt bazy danych");
                }

                if (!rs.next())
                    throw new Exception("Nie znaleziono dziennika");
                
                int seq = rs.getInt(1);
                rs.close();
                PreparedStatement ps = DSApi.context().prepareStatement("INSERT INTO DSO_JOURNAL_ENTRY " +
                    "(DOCUMENTID,SEQUENCEID,CTIME,ENTRYDATE,JOURNAL_ID) " +
                    "VALUES (?, ?, ?, ?, ?)");
                ps.setLong(1, doc.getId().longValue());
                ps.setInt(2, seq);
                ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                ps.setDate(4, new java.sql.Date(System.currentTimeMillis()));
                ps.setLong(5, journal.getId().longValue());

                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("seq="+seq);

                doc.bindToJournal(journal.getId(), new Integer(seq));

                st.executeUpdate("update dso_journal set sequenceid="+(seq+1)+
                    " where id="+journal.getId());
                DSApi.context().closeStatement(st);
                DSApi.context().commit();
                
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                event.getLog().error(e.getMessage(), e);
            }
        }
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }
}
