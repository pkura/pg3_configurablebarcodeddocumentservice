package pl.compan.docusafe.web.office.in;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class LoadAttachmentAction extends EventActionSupport 
{

	private FormFile file;
	private List<Att> atts;
	
	protected void setup() 
	{
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doAdd").
        append(OpenHibernateSession.INSTANCE).
        append(new Add()).
        append(new FillForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	public class Att
	{
		private Long id;
		private String name;
		private String date;
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		public String toString()
		{
			return this.id+" "+this.name;
		}
		
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getLink()
		{
			return "/office/incoming/dwr-document-main.action?attId="+this.id;
		}
		
	}
	
	private class FillForm implements ActionListener
    {
	    public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("tomekl").debug("LoadAttachmentAction");
        	atts = new LinkedList<Att>();
        	PreparedStatement ps = null;
        	ResultSet rs = null;
        	try
        	{
        		ps = DSApi.context().prepareStatement("select ID, ORIGINALFILENAME, CTIME from DS_ATTACHMENT_REVISION where lparam = 'TO_SHOW'");
        		rs = ps.executeQuery();
        		Att att = null;
        		while(rs.next())
				{
        			att = new Att();
        			att.setId(rs.getLong("ID"));
        			att.setName(rs.getString("ORIGINALFILENAME"));
        			
        			att.setDate(DateUtils.formatJsDateTime(new Date(rs.getTimestamp("CTIME").getTime())));
        			atts.add(att);        			
				}
        	}
        	catch(Exception e)
        	{
        		e.printStackTrace();
        	}        	
        	finally
        	{
        		DbUtils.closeQuietly(rs);
        		DbUtils.closeQuietly(ps);
        	}
        }
    }
	
	private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		
        		if(file == null)
        		{
        			throw new EdmException("Brak pliku");
        		}
        		
        		LoggerFactory.getLogger("tomekl").debug("FN1 {}",file.getFile().getName().toUpperCase());
        		LoggerFactory.getLogger("tomekl").debug("FN2 {}",file.getName().toUpperCase());
        		
        		String name = file.getFile().getName().toUpperCase();
        		
        		if(!name.endsWith("TIFF") && !name.endsWith("TIF"))
        		{
        			throw new EdmException("Format pliku nieznany");
        		}
        		
        		pl.compan.docusafe.core.base.Document document = new pl.compan.docusafe.core.base.Document("test", "test");
        		
        		document.setFolder(Folder.getRootFolder());
                document.setDocumentKind(DocumentKind.findByCn(DocumentKind.NORMAL_KIND));
        		
                document.create();
                DSApi.context().session().flush();
                
                Attachment attachment = new Attachment("TO_SHOW");
                document.createAttachment(attachment);
                AttachmentRevision rev = attachment.createRevision(file.getFile());
                rev.setOriginalFilename(file.getName());
                rev.setLparam("TO_SHOW");       
                
                java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
                perms.add(new PermissionBean(ObjectPermission.READ, "A", ObjectPermission.ANY, "A"));
    			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "A", ObjectPermission.ANY, "A"));
    			perms.add(new PermissionBean(ObjectPermission.MODIFY, "A", ObjectPermission.ANY, "A"));
    			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "A", ObjectPermission.ANY, "A"));
    			perms.add(new PermissionBean(ObjectPermission.DELETE, "A", ObjectPermission.ANY, "A"));

    			//Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
    			//perms.addAll(documentPermissions);
    			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
                
        		DSApi.context().commit();
        	}
        	catch(Exception e)
        	{
        		DSApi.context()._rollback();
        		addActionError(e.getMessage());
        		e.printStackTrace();
        	}
        }
    }

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	public List<Att> getAtts() {
		return atts;
	}

	public void setAtts(List<Att> atts) {
		this.atts = atts;
	}
	
}
