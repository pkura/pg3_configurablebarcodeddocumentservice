package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.AttachmentsTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentsAction.java,v 1.6 2006/03/22 15:39:29 bspych Exp $
 */
public class AttachmentsAction extends AttachmentsTabAction
{
    
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/attachments.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
