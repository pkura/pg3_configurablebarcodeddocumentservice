package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.TemplatesTabAction;

import java.util.List;

public class TemplatesAction extends TemplatesTabAction {

    @Override
    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

}
