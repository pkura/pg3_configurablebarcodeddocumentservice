package pl.compan.docusafe.web.office.in;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.web.office.common.WorkHistoryTabAction;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkHistoryAction.java,v 1.6 2006/02/20 15:42:44 lk Exp $
 */
public class WorkHistoryAction extends WorkHistoryTabAction
{
    protected List prepareTabs()
    {
        return new Tabs(this).createTabs();
    }

    public String getBaseLink()
    {
        return "/office/incoming/work-history.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }
}
