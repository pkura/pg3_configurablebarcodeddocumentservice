package pl.compan.docusafe.web.office.massimport.xls;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import pl.compan.docusafe.web.office.massimport.ColumnParser;
import pl.compan.docusafe.web.office.massimport.ColumnParserBase;
import pl.compan.docusafe.web.office.massimport.TypeParser;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class XlsFieldsGroupProvider {

    HashMap<String,ColumnParser> columnsReader = new HashMap<String, ColumnParser>();

    public static XlsFieldsGroupProvider create() {
        return new XlsFieldsGroupProvider();
    }

    public XlsFieldsGroupProvider add(String columnCn, TypeParser typeParser) {
        columnsReader.put(columnCn, new ColumnParserBase(columnCn, typeParser));
        return this;
    }

    public XlsFieldsGroupProvider add(ColumnParser columnParser) {
        columnsReader.put(columnParser.columnCn, columnParser);
        return this;
    }

    public Map<String, Object> read(Map<String, Integer> columnHeader, Row documentData) {
        Map<String, Object> values = new HashMap<String, Object>();

        for (ColumnParser columnParser : columnsReader.values()) {
            Object value = read(columnHeader, documentData, columnParser);
            if (value != null)
                values.put(columnParser.columnCn, value);
        }
        return values;
    }

    public static Object read(Map<String, Integer> columnHeader, Row documentData, ColumnParser columnParser) {
        if (StringUtils.isBlank(columnParser.columnCn)) return null;

        Integer columnIdx = columnHeader.get(columnParser.columnCn);
        if (columnIdx == null) return null;
        Cell valueCell = documentData.getCell(columnIdx);
        if (valueCell==null) return null;

//            if (cell!=null){
//                RichTextString richTextString = cell.getRichStringCellValue();
//                if (richTextString!=null){
//                    String stringValue = richTextString.getString();
//                    values.put(columnParser.columnCn, columnParser.parseValue(stringValue));
//                }
//            }
//            switch (valueCell.getCellType()) {
//                case Cell.CELL_TYPE_NUMERIC:
//                    values.put(columnParser.columnCn, valueCell.getNumericCellValue());
//                    break;
//                case Cell.CELL_TYPE_STRING:
//                    values.put(columnParser.columnCn, valueCell.getStringCellValue());
//                    break;
//            }

        switch (valueCell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                return columnParser.parse(valueCell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return columnParser.parse(valueCell.getStringCellValue());
        }

        return null;
    }
}
