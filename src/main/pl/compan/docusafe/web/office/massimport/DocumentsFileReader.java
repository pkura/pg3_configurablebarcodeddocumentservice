package pl.compan.docusafe.web.office.massimport;

import java.io.IOException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class DocumentsFileReader {

    protected ErrorNotifier errorNotifier;
//    protected Set<String> errorNoColumnsInDockind = new HashSet<String>();

    public DocumentsFileReader(ErrorNotifier errorNotifier) {
        this.errorNotifier = errorNotifier;
    }

    public abstract boolean hasNext();

    public abstract FieldDataProvider next();

//    public void addErrorNoColumnInDockind(String columnCn) {
//        errorNoColumnsInDockind.add(columnCn);
//    }
//
//    public String[] getErrorNoColumnsInDockind() {
//        return errorNoColumnsInDockind.toArray(new String[errorNoColumnsInDockind.size()]);
//    }
//
//    public boolean isErrorColumns() {
//        return errorNoColumnsInDockind != null && !errorNoColumnsInDockind.isEmpty();
//    }
//
//    public void cleanErrorColumns() {
//        errorNoColumnsInDockind.clear();
//    }

    public abstract void close() throws IOException;

    public static abstract class ErrorNotifier {
        public abstract void notifyError(String msg);

        public void notifyError(Exception e) {
            notifyError(e.getMessage(), e);
        }

        public abstract void notifyError(String msg, Exception e);
    }
}
