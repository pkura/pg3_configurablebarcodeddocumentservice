package pl.compan.docusafe.web.office.massimport;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;

import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class FieldDataProvider {

    public abstract Map<String, Object> getValues();

    public abstract Object getValueByCn(String columnCn);

    public abstract Set<String> getColumnsForDict(String dictCn, String dictToFieldLinker);

    public abstract Person getOrCreatePerson(String dictCn, String linker) throws EdmException;

    /**
     * @param dictCn
     * @param linker
     * @return value like u:admin;d:7F000001150270021404ED203110A36569C
     */
    public abstract String getSender (String dictCn, String linker) throws EdmException;

    public abstract String getIdentifierDocumentRow ();

    public abstract Set<String> getFieldsCns(String dictToFieldLinker);
}
