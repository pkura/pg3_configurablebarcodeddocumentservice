package pl.compan.docusafe.web.office.massimport;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ColumnParserBase extends ColumnParser {
    public final TypeParser typeParser;

    public ColumnParserBase(String columnCn, TypeParser typeParser) {
        super(columnCn);
        this.typeParser = typeParser;
    }

    public Object parse(String stringValue) {
        return typeParser.parse(stringValue);
    }

    public Object parse(Double numericCellValue) {
        return typeParser.parse(numericCellValue);
    }
}
