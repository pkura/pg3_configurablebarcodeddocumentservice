package pl.compan.docusafe.web.office.massimport;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum TypeParser {
    STRING, DOUBLE, FLOAT, LONG, INTEGER, SHORT;

    public Object parse(String stringValue) {
        switch (this) {
            case STRING:
                return stringValue;
            case DOUBLE:
                return Double.parseDouble(stringValue);
            case FLOAT:
                return ((Double) Double.parseDouble(stringValue)).floatValue();
            case LONG:
                return ((Double) Double.parseDouble(stringValue)).longValue();
            case INTEGER:
                return ((Double) Double.parseDouble(stringValue)).intValue();
            case SHORT:
                return ((Double) Double.parseDouble(stringValue)).shortValue();
            default:
                throw new IllegalArgumentException("Unexpected type: " + name());
        }
    }

    public Object parse(Double numericCellValue) {
        switch (this) {
            case STRING:
                return numericCellValue.toString();
            case DOUBLE:
                return numericCellValue;
            case FLOAT:
                return numericCellValue.floatValue();
            case LONG:
                return numericCellValue.longValue();
            case INTEGER:
                return numericCellValue.intValue();
            case SHORT:
                return numericCellValue.shortValue();
            default:
                throw new IllegalArgumentException("Unexpected type: " + name());
        }
    }
}