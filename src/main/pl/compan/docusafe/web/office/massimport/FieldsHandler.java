package pl.compan.docusafe.web.office.massimport;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.CustomPersistence;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class FieldsHandler {

    protected static final String LINKER = ".";
    private Set<String> dictsCns = new HashSet<String>();
    private Set<String> multipleDictsCns = new HashSet<String>();
    private Set<String> enumsCns = new HashSet<String>();

    /**
     * Nale�y zaimplementowa� metod� getDictionaryid
     */

    public static FieldsHandler getFildsParser(Provider provider) {
        return getFildsParser(provider, new FieldsHandler());
    }

    public static FieldsHandler getFildsParser(Provider provider, FieldsHandler defaultFieldsHandler) {
        if (provider != null) {
            FieldsHandler fieldsHandler = provider.getFieldsHandler();
            if (fieldsHandler != null)
                return fieldsHandler;
        }

        return defaultFieldsHandler;
    }

    /**
     * Nale�y zaimplementowa� metod� getDictionaryid
     */
    public void addDictCn(String... dictCn) {
        dictsCns.addAll(Arrays.asList(dictCn));
    }

    public void addMultipleDictCn(String... multipleDictCn) {
        multipleDictsCns.addAll(Arrays.asList(multipleDictCn));
    }

    public void addEnumCn(String... enumCn) {
        enumsCns.addAll(Arrays.asList(enumCn));
    }

    /**
     * Aktualizacja dokumentu (przez modyfikacj� updatingDocument lub zmian� p�l z formatki updatingDockindKeys).
     * Aktualizacja podzielona jest na kilka etap�w:
     * <li> 1. sprawdzenie czy cn dotyczy pola s�ownikowego -> obs�uga, koniec</li>
     * <li> 2. sprawdzenie czy cn dotyczy pola enum -> obs�uga, koniec</li>
     * <li> 3. aktualizacja specjalnie dla p�l typu CustomPersistance - obs�uga, mo�liwo�� omini�cia pkt 5</li>
     * <li> 4. aktualizacja specjalnie dla p�l NonColumnFiled - obs�uga, mo�liwo�� omini�cia pkt 5</li>
     * <li> 5. podmiana warto�ci w dockindKeys na zaimportowan� warto��</li>
     *
     * @param field                 pole z dockind
     * @param updatingDocument      dokument, kt�remu mo�na zakutalizowa� w miar� potrzeb dane
     * @param updatingDockindKeys   zbi�r danych z formatki do zast�pienia w miar� potrzeb
     * @param importDocumentUpdater zaimportowane dane
     * @param cn                    cn pola, kt�rego dotyczy aktualizowane pole
     * @throws pl.compan.docusafe.core.EdmException
     */
    public void update(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {
//        Object formValue = updatingDockindKeys.get(cn);
//        if (!isValueSet(formValue))
//            return;


        if (handleDictionary(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn))
            return;

        if (handleEnum(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn))
            return;

        if (handleSpecialField(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn))
            return;


        Object importedValue = importDocumentUpdater.getValueByCn(cn);
        updateField(field, updatingDocument, updatingDockindKeys, cn, importedValue);
    }

    /**
     * @return czy pole jest slownikowe. Je�eli tak, to nie b�dzie innego sposobu aktualizacji danych dokumentu dla tego CN.
     */
    public boolean handleDictionary(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {
        if (dictsCns.contains(cn)) {
            Object dictId = getDictionaryId(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn);
            updateDockindKeys(updatingDockindKeys, cn, dictId);
            return true;
        }
        if (multipleDictsCns.contains(cn)) {
            Object dictId = getMultiDictionaryIds(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn);
            updateDockindKeys(updatingDockindKeys, cn, dictId);
            return true;
        }

        return false;
    }

    private Object getMultiDictionaryIds(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException { // updatingDocument.getDocumentKind().dic
        throw new IllegalArgumentException("Not implemented method");
    }

    public Object getDictionaryId(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {

        //        obsluga slownikow :
        //
        //        pogrupowa� kolumny w s�owniki  ||  wyszuka� kolumny reczenie dla slownikow
        //        musze napisac pelna obsluge dla danego slownika - odp klasa, odp metody
        //        utworzony wpis w slowniku wstawic do dockindKeys  ||  najpierw trzeba sprawdzic czy czasem taki nie istnieje
        //
        //        kwestia zastepowania
        //        to co na formatce - wazniejsze
        //
        //
        //
        //        nazwa slownika - sprawdzic czy jest taka nazwa w dockindKeys
        //        nazwy pol slownika
        //        typy pol slownika

        throw new IllegalArgumentException("Not implemented method");
    }

    public void updateDockindKeys(Map<String, Object> updatingDockindKeys, String cn, Object importedValue) throws EdmException {
        if (isValueSet(importedValue))
            updatingDockindKeys.put(cn, importedValue.toString());
    }

    public boolean isValueSet(Object formValue) {
        return formValue != null && StringUtils.isNotBlank(formValue.toString());
    }

    public boolean handleEnum(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {
        if (enumsCns.contains(cn)) {
            Object enumId = getEnumId(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn);
            updateDockindKeys(updatingDockindKeys, cn, enumId);
            return true;
        }
        return false;
    }

    public Object getEnumId(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EntityNotFoundException {
        EnumItem enumItem = field.getEnumItemByTitle(cn);
        return enumItem.getId();
    }

    public boolean handleSpecialField(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {
        return false;
    }

    public void updateField(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, String cn, Object importedValue) throws EdmException {
        if (importedValue == null)
            return;
        boolean updatedKeys = true;

        if (field instanceof CustomPersistence)
            updatedKeys = updateCustomPersistenceField(field, updatingDocument, updatingDockindKeys, cn, importedValue) || updatedKeys;

        if (field instanceof NonColumnField)
            updatedKeys = updateNonColumnField(field, updatingDocument, updatingDockindKeys, cn, importedValue) || updatedKeys;

        if (!updatedKeys)
            updateDockindKeys(updatingDockindKeys, cn, importedValue);
    }

    /**
     * @return czy dockindKeys by�y aktualizowane i nie jest juz wymagana ich aktualizacja
     */
    public boolean updateCustomPersistenceField(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, String cn, Object importedValue) throws EdmException {
        ((CustomPersistence) field).persist(updatingDocument, importedValue);
        return false;

//            if (!"false".equalsIgnoreCase(Docusafe.getAdditionProperty("import.documents.FieldsHandler.CustomPersistenceField.updateDockindKeys")))
//                updateDockindKeys(updatingDockindKeys, cn, importedValue);
//            else if ("true".equalsIgnoreCase(Docusafe.getAdditionProperty("import.documents.FieldsHandler.CustomPersistenceField.removeDockindKey")))
//                updatingDockindKeys.remove(cn);
    }

    /**
     * @return czy dockindKeys by�y aktualizowane i nie jest juz wymagana ich aktualizacja
     */
    public boolean updateNonColumnField(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, String cn, Object importedValue) throws EdmException {
//            if ("true".equalsIgnoreCase(Docusafe.getAdditionProperty("import.documents.FieldsHandler.NonColumnField.updateDockindKeys"))) {
//                updateDockindKeys(updatingDockindKeys, cn, importedValue);
//            } else if ("true".equalsIgnoreCase(Docusafe.getAdditionProperty("import.documents.FieldsHandler.default.ignoringNonColumnField"))) {
//                //do nothing, ignore
//            } else {
//                throw new EdmException("Not implemented method to update NonColumnField. Column CN: " + cn); // todo
//            }
        updateDockindKeys(updatingDockindKeys, cn, field.simpleCoerce(importedValue));
        return true;
    }

    public boolean isRequired(Field field, Map<String, Object> dockindKeys) {
        return field.isRequired() && !field.isHidden() && !field.isDisabled() && dockindKeys.containsKey(field.getCn()) && (dockindKeys.get(field.getCn()) == null || StringUtils.isBlank(dockindKeys.get(field.getCn()).toString()));
    }

    public String getDictToFieldLinker() {
        return LINKER;
    }


    public static interface Provider {

        FieldsHandler getFieldsHandler();

    }
}


///**
// * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
// *         <p/>
// *         <p/>
// *         import.documents.FieldsHandler.default.ignoringNonColumnField - unset is false
// *         import.documents.FieldsHandler.CustomPersistenceField.updateDockindKeys - unset is true
// *         import.documents.FieldsHandler.CustomPersistenceField.removeDockindKey - unset is false
// *         import.documents.FieldsParserIgnoringNonColumnField.NonColumnField.updateDockindKeys - unset is false
// */

//        /**
//         * Aktualizacja dokumentu (przez modyfikacj� updatingDocument lub zmien� p�l z formatki updatingDockindKeys)
//         *
//         * @param field               pole z dockind
//         * @param updatingDocument    dokument, kt�remu mo�na zakutalizowa� w miar� potrzeb dane
//         * @param updatingDockindKeys zbi�r danych z formatki do zast�pienia w miar� potrzeb
//         * @param cn                  cn pola, kt�rego dotyczy rozpatrywana warto��
//         * @param importedValue       wartosc pobrana z xls (Double lub String)
//         * @throws EdmException
//         */
//        public void update(Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, String cn, Object importedValue) throws EdmException {
//            if (field instanceof CustomPersistence) {
//                updateCustomPersistenceField(field, updatingDocument, updatingDockindKeys, cn, importedValue);
//            } else if (field instanceof NonColumnField) {
//                updateNonColumnField(field, updatingDocument, updatingDockindKeys, cn, importedValue);
//            } else {
//                updateDockindKeys(field, updatingDocument, updatingDockindKeys, cn, importedValue);
//            }
//        }

//    /**
//     * Szuka Person-a na podstawie nipu i organizacji, kt�re pobierane s� z zaimportowanych danych xls. Danymi s� nip lub organizacja, kt�re wyszukiwane s� wed�ug nazwy kolumny wed�ug wzorca: [S�OWNIK np. 'RECEPIENT'][LINKER np. '.'][POLE np. 'NIP']
//     */
//    public static Person findPerson(FieldsHandlerUtils.FieldDataProvider fieldDataProvider, String dictCn, String dictToFieldLinker) throws EdmException {
//        Person person = null;
//
////        Object nip = FieldsHandlerUtils.Reader.read(fieldDataProvider.getColumns(), fieldDataProvider.getRow(), new FieldsHandlerUtils.ColumnParserBase(dictCn + dictToFieldLinker + "NIP", FieldsHandlerUtils.TypeParser.STRING));
////        if (nip != null) person = Person.findPersonByNip((String) nip);
////        if (person != null) return person;
////
////        Object organization = FieldsHandlerUtils.Reader.read(fieldDataProvider.getColumns(), fieldDataProvider.getRow(), new FieldsHandlerUtils.ColumnParserBase(dictCn + dictToFieldLinker + "ORGANIZATION", FieldsHandlerUtils.TypeParser.STRING));
////        if (organization != null) Person.findPersonByOrganization((String) organization);
//
//        return person;
//    }