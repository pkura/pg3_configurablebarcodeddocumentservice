package pl.compan.docusafe.web.office.massimport.xls;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.massimport.ColumnParserBase;
import pl.compan.docusafe.web.office.massimport.FieldDataProvider;
import pl.compan.docusafe.web.office.massimport.TypeParser;

import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class XlsFieldDataProvider extends FieldDataProvider {

    protected Map<String, Integer> columns = null;
    protected Row row = null;

    public static Map<String, Integer> loadColumnsHeader(Iterator<Row> rowIterator) {
        Map<String, Integer> columns = new HashMap<String, Integer>();

        if (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        columns.put(cell.getStringCellValue(), cell.getColumnIndex());
                        break;
                }
            }
        }

        return columns;
    }

//        public boolean isPreparedData() {
//            return isColumnsPrepared() && isRowPrepared();
//        }

    public boolean prepareColumns(Map<String, Integer> columns) {
        this.columns = columns;
        return isColumnsPrepared();
    }

    public boolean isColumnsPrepared() {
        return columns != null && !columns.isEmpty();
    }

    public void resetData() {
        resetColumns();
        cleanRow();
    }

    public void resetColumns() {
        columns = null;
    }

    public void cleanRow() {
        row = null;
    }

    @Override
    public Map<String, Object> getValues() {
        Map<String, Object> values = new HashMap<String, Object>();

        for (String columnCn : columns.keySet()) {
            Object value = getValueByCn(columnCn);
            if (value != null)
                values.put(columnCn, value);
        }

        return values;
    }

    @Override
    /**
     * @return cn -> String/Double
     */
    public Object getValueByCn(String columnCn) {
        Integer columnIdx = columns.get(columnCn);
        if (columnIdx == null) return null;
        Cell valueCell = row.getCell(columnIdx);

//                if (valueCell!=null){
//                    RichTextString richTextString = valueCell.getRichStringCellValue();
//                    if (richTextString!=null){
//                        String stringValue = richTextString.getString();
//                        values.put(columnCn, stringValue);
//                    }
//                }

        switch (valueCell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC:
                return valueCell.getNumericCellValue();
            case Cell.CELL_TYPE_STRING:
                return valueCell.getStringCellValue();
        }

        return null;
    }

    @Override
    public Set<String> getColumnsForDict(String dictCn, String dictToFieldLinker) {
        Set<String> dictColumns = new HashSet<String>();
        String beginCn = (dictCn + dictToFieldLinker).toUpperCase();
        for (String columnCn : columns.keySet()) {
            if (columnCn.toUpperCase().startsWith(beginCn))
                dictColumns.add(columnCn);
        }
        return dictColumns;
    }

    @Override
    public Person getOrCreatePerson(String dictCn, String dictToFieldLinker) throws EdmException {
        return getOrCreatePerson(this, dictCn, dictToFieldLinker);
    }

    public static Person getOrCreatePerson(XlsFieldDataProvider importDocumentUpdater, String dictCn, String dictToFieldLinker) throws EdmException {
        Map personMap = getPersonMap(importDocumentUpdater, dictCn, dictToFieldLinker);
        if (personMap == null) return null;
        Person person = new Person();
        person.fromMapUpper(personMap);
        person.createIfNew();

        return getPerson(personMap);
    }

    public static Map getPersonMap(XlsFieldDataProvider importDocumentUpdater, String dictCn, String dictToFieldLinker) {
        XlsFieldsGroupProvider xlsFieldsGroupProvider = XlsFieldsGroupProvider.create();
        Set<String> dictColumns = importDocumentUpdater.getColumnsForDict(dictCn, dictToFieldLinker);
        for (String column : dictColumns) {
            xlsFieldsGroupProvider.add(column, getType(column, dictCn, dictToFieldLinker));
        }

        Map<String, Object> data = xlsFieldsGroupProvider.read(importDocumentUpdater.getColumns(), importDocumentUpdater.getRow());
        if (data == null || data.isEmpty()) return null;
        return removeDictCnFromPersonMap(data, dictCn, dictToFieldLinker);
    }

    public Map<String, Integer> getColumns() {
        return columns;
    }

    public Row getRow() {
        return row;
    }

    public static Map removeDictCnFromPersonMap(Map<String, Object> dataWithDictCn, String dictCn, String dictToFieldLinker) {
        int beginFrom = (dictCn + dictToFieldLinker).length();
        Map<String, Object> data = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : dataWithDictCn.entrySet()) {
            String key = entry.getKey().substring(beginFrom);
            data.put(key, entry.getValue());
        }
        return data;
    }

    private static TypeParser getType(String column, String dictCn, String dictToFieldLinker) {
//                reader
//                        .add(dictCn + dictToFieldLinker + "LOCATION", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "FIRSTNAME", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "PHONENUMBER", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "REGON", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "NIP", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "ZIP", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "ORGANIZATION", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "EMAIL", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "STREET", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "FAX", FieldsHandlerUtils.TypeParser.STRING)
//                        .add(dictCn + dictToFieldLinker + "LASTNAME", FieldsHandlerUtils.TypeParser.STRING);
//        if ((dictCn + dictToFieldLinker + "NIP").equalsIgnoreCase(column))
//            return TypeParser.INTEGER;
        return TypeParser.STRING;
    }

    public static Person getPerson(Map<String, Object> personMap) throws EdmException {
        QueryForm form = new QueryForm(0, 1);
        for (Map.Entry<String, Object> entry : personMap.entrySet())
            form.addProperty(entry.getKey().toLowerCase(), entry.getValue());
        form.addProperty("DISCRIMINATOR".toLowerCase(), "PERSON");
        Person[] persons = Person.search(form).results();
        if (persons.length > 0) return persons[0];
        return null;
    }

    @Override
    public String getSender(String dictCn, String linker) throws EdmException {
        String senderUername = (String) XlsFieldsGroupProvider.read(columns, row, new ColumnParserBase(dictCn, TypeParser.STRING));
        if (StringUtils.isBlank(senderUername)) return null;

        DSUser user = DSUser.findByUsername(senderUername);
        String[] divisions = user.getDivisionsGuid();
        //String senderDivision = divisions != null && divisions.length > 0 ? divisions[0] : "";

        //return "u:" + senderUername + ";d:" + senderDivision;
        return "u:" + senderUername + ";"+ (divisions != null && divisions.length > 0 ? "d:" + divisions[0] : "");
    }

    @Override
    public String getIdentifierDocumentRow() {
        return "numer wiersza = " + (row.getRowNum()+1) + ", strona = " + row.getSheet().getSheetName();
    }

    @Override
    public Set<String> getFieldsCns(String dictToFieldLinker) {
        Set<String> fieldsCns = new HashSet<String>();
        for (String col : columns.keySet()){
            String fCn = TextUtils.getTextToSign(col,dictToFieldLinker);
            if (fCn!=null) fCn = fCn.trim();
            if (StringUtils.isNotBlank(fCn)) fieldsCns.add(fCn);
        }
        return fieldsCns;
    }

    public boolean prepare(Row row) {
        this.row = row;
        return isRowPrepared();
    }

    public boolean isRowPrepared() {
        return row != null;
    }
}
