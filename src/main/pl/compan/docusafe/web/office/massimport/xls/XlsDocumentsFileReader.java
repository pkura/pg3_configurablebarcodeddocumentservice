package pl.compan.docusafe.web.office.massimport.xls;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import pl.compan.docusafe.web.office.massimport.DocumentsFileReader;
import pl.compan.docusafe.web.office.massimport.FieldDataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class XlsDocumentsFileReader extends DocumentsFileReader {

    java.io.InputStream fis = null;
    HSSFWorkbook workbook = null;
    Integer currentSheetIdx = null;
    HSSFSheet currentSheet = null;
    Map<String, Integer> currentSheetColumns = null;
    Iterator<Row> rowIterator = null;

    protected XlsDocumentsFileReader(ErrorNotifier errorNotifier) {
        super(errorNotifier);
    }

    @Override
    public boolean hasNext() {
        if (rowIterator == null) return false;
        if (rowIterator.hasNext())
            return true;
        ++currentSheetIdx;
        goToSheet(currentSheetIdx);
        return hasNext();
    }

    @Override
    public FieldDataProvider next() {
        if (hasNext()) {
            XlsFieldDataProvider xlsFieldDataProvider = new XlsFieldDataProvider();
            xlsFieldDataProvider.prepareColumns(currentSheetColumns);
            xlsFieldDataProvider.prepare(rowIterator.next());
            return xlsFieldDataProvider;
        }
        return null;
    }

    public static XlsDocumentsFileReader create(File file, ErrorNotifier errorNotifier) throws IOException {
        XlsDocumentsFileReader xlsDocumentsFileReader = new XlsDocumentsFileReader(errorNotifier);
        xlsDocumentsFileReader.init(file);
        return xlsDocumentsFileReader;
    }

    public void init(File file) throws IOException {
        fis = new FileInputStream(file);
        workbook = new HSSFWorkbook(fis); //XSSFWorkbook
        goToSheet(0);
    }

    private void goToSheet(int sheetIdx) {
        if (sheetIdx > workbook.getNumberOfSheets() - 1) {
            currentSheet = null;
            rowIterator = null;
            currentSheetColumns = null;
            return;
        }
        currentSheetIdx = sheetIdx;
        currentSheet = workbook.getSheetAt(currentSheetIdx);
        rowIterator = currentSheet.iterator();
        currentSheetColumns = XlsFieldDataProvider.loadColumnsHeader(rowIterator);
    }

    @Override
    public void close() throws IOException {
        if (fis != null)
            fis.close();
    }
}
