package pl.compan.docusafe.web.office.massimport;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class ColumnParser {
    public final String columnCn;

    /**
     * @param columnCn je�eli b�dzie blank, metoda read jako importowany obekt zwr�ci null
     */
    public ColumnParser(String columnCn) {
        this.columnCn = columnCn;
    }

    public abstract Object parse(String stringValue);

    public abstract Object parse(Double numericCellValue);
}
