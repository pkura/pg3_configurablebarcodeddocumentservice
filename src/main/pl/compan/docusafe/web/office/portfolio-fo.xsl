<?xml version="1.0" encoding="iso-8859-2" ?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format" >

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/">
    <fo:root >
        <fo:layout-master-set>
            <fo:simple-page-master  master-name="simple"
                                    page-height="29.7cm"
                                    page-width="21cm"
                                    margin-top="1cm"
                                    margin-bottom="1cm"
                                    margin-left="1cm"
                                    margin-right="1cm">
                <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
                <fo:region-before extent="1cm"/>
            </fo:simple-page-master>

        </fo:layout-master-set>


        <fo:page-sequence master-reference="simple">


            <fo:flow flow-name="xsl-region-body">

                <fo:table table-layout="fixed" border="1pt solid black">

                    <fo:table-column column-width="1cm"/>
                    <fo:table-column column-width="2.5cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="7cm"/>

                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell><fo:block font-family="Arial" font-size="10pt">rok</fo:block></fo:table-cell>
                            <fo:table-cell><fo:block font-family="Arial" font-size="10pt">referent</fo:block></fo:table-cell>
                            <fo:table-cell><fo:block font-family="Arial" font-size="10pt">symbol kom�rki organizacyjnej</fo:block></fo:table-cell>
                            <fo:table-cell><fo:block font-family="Arial" font-size="10pt">oznaczenie teczki</fo:block></fo:table-cell>
                            <fo:table-cell><fo:block font-family="Arial" font-size="10pt">tytu� teczki wg wykazu akt</fo:block></fo:table-cell>
                        </fo:table-row>
						<fo:table-row>
							<fo:table-cell><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="/teczka/rok" /></fo:block></fo:table-cell>
							<fo:table-cell><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="/teczka/referent" /></fo:block></fo:table-cell>
							<fo:table-cell><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="/teczka/symbol" /></fo:block></fo:table-cell>
							<fo:table-cell><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="/teczka/rwa" /></fo:block></fo:table-cell>
							<fo:table-cell><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="/teczka/tytul" /></fo:block></fo:table-cell>
						</fo:table-row>
                    </fo:table-body>

                </fo:table>

                <fo:block></fo:block>


                <fo:table table-layout="fixed" border="1pt solid black" >
                    <fo:table-column column-width="1cm" />
                    <fo:table-column column-width="4.5cm" />
                    <fo:table-column column-width="3cm" />
                    <fo:table-column column-width="2cm" />
                    <fo:table-column column-width="2.5cm" />
                    <fo:table-column column-width="2.5cm" />
                    <fo:table-column column-width="3cm" />
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt">L.p.</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt">SPRAWA</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt">Od kogo</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt">DATA</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt" >UWAGI</fo:block></fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" >nr KO / znak sprawy</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" >z dnia</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" >za�o�enia sprawy</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" >ostatecznego zalatwienia</fo:block></fo:table-cell>
                        </fo:table-row>

                        <xsl:apply-templates select="//sprawa" />

                    </fo:table-body>
                </fo:table>
                
                  <fo:table table-layout="fixed" border="1pt solid black" >
                    <fo:table-column column-width="1cm" />
                    <fo:table-column column-width="17.5cm" />
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"></fo:block></fo:table-cell>
                        </fo:table-row>

                        <xsl:apply-templates select="//stopka" />
                    </fo:table-body>
                </fo:table>

            </fo:flow>

        </fo:page-sequence>
    </fo:root>
</xsl:template>

<xsl:template match="sprawa">
    <fo:table-row>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="lp" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="numer_sprawy" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="nadawca" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" number-columns-spanned="1" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="data_wszczecia_sprawy" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" number-columns-spanned="1" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="data_zalatwienia" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="uwagi" /></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row>
        <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="znak_pisma" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="z_dnia" /></fo:block></fo:table-cell>
    </fo:table-row>

</xsl:template>

<xsl:template match="stopka">
    <fo:table-row>
		 <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="nadawca" /></fo:block></fo:table-cell>
    </fo:table-row>

</xsl:template>

</xsl:stylesheet>
