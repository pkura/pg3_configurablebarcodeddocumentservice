package pl.compan.docusafe.web.office;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import de.schlichtherle.io.ArchiveDetector;
import de.schlichtherle.io.File;

/**
 */
public class AddBarcodePackageScansAction extends EventActionSupport
{
  
    private static Logger log = LoggerFactory.getLogger(AddBarcodePackageScansAction.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    private FormFile file;
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

	          
           
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	 if(file == null)
  	        	   throw new EdmException("NIe podano pliku");
            	 File destination = new File("D:/docusafe/HOMES/HOME/ocrs");//Docusafe.getAdditionProperty("ul.ocrs"));
            	 
            	 de.schlichtherle.io.File zipFile = new de.schlichtherle.io.File(file.getFile());
         	    boolean result = zipFile.archiveCopyAllTo(new File(destination),ArchiveDetector.NULL);
         	    zipFile.umount();
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

}
