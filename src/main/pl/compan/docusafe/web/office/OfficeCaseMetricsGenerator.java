package pl.compan.docusafe.web.office;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Klasa odpowiedzialna za generowanie metryki sprawy
 * @author Marek Wojciechowski
 */
public class OfficeCaseMetricsGenerator {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(OfficeCaseMetricsGenerator.class);
    public static final StringManager sm = GlobalPreferences.loadPropertiesFile(OfficeCaseMetricsGenerator.class.getPackage().getName(), null);

    /**
     * @param officeCaseId Id sprawy
     * @return Metryka sprawy
     */
    public static File getMetrics(Long officeCaseId) {

        File metrics = null;

        try {
            OfficeCase officeCaseToDoMetrics = OfficeCase.find(officeCaseId);

            List<OfficeDocument> docs= officeCaseToDoMetrics.getDocuments();
            List<HistoryEntry> historia = getData(docs);
            File pdfFile= null;
            try {
                // font
                File fontDir= new File(Docusafe.getHome(), "fonts");
                File arial= new File(fontDir, "arial.ttf");
                BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font fontSuperSmall= new Font(baseFont, (float) 6);
                Font fontSmall= new Font(baseFont, (float) 8);
                Font fontNormal= new Font(baseFont, (float) 12);
                Font fontNormal_Large= new Font(baseFont, (float) 14);
                Font fontLarge= new Font(baseFont, (float) 16);

                // file
                pdfFile= File.createTempFile("docusafe_", "_tmp");
                pdfFile.deleteOnExit();
                Document pdfDoc= new Document(PageSize.A4);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

                pdfDoc.open();

                // content

				/* ********************************** */
                Table topTable= new Table(2);
                topTable.setCellsFitPage(true);
                topTable.setWidth(100);
                topTable.setPadding(3);
                topTable.setBorder(1);
                topTable.setWidths( new int[]{25,75});

                Cell oznaczenieSprawyLabel = new Cell(new Paragraph("Oznaczenie sprawy", fontNormal));
                Cell oznaczenieSprawy= new Cell(new Paragraph(officeCaseToDoMetrics.getOfficeId(), fontNormal));

                Cell tytulSprawyLabel = new Cell(new Paragraph("Tytu� sprawy", fontNormal));
                Cell tytulSprawy= new Cell(new Paragraph(officeCaseToDoMetrics.getTitle(), fontNormal));

                topTable.addCell(oznaczenieSprawyLabel);
                topTable.addCell(oznaczenieSprawy);
                topTable.addCell(tytulSprawyLabel);
                topTable.addCell(tytulSprawy);

                pdfDoc.add(topTable);

                // Tabela z wlasciwymi danymi
                Table contentTable= new Table(5);
                contentTable.setCellsFitPage(true);
                contentTable.setWidth(100);
                contentTable.setPadding(3);
                contentTable.setBorder(1);
                contentTable.setWidths( new int[]{10,15,20,35,20});

                // Naglowki
                contentTable.addCell( new Cell(new Paragraph("Lp.", fontNormal)));
                contentTable.addCell( new Cell(new Paragraph("Data podj�tej czynno�ci", fontNormal)));
                contentTable.addCell( new Cell(new Paragraph("Oznaczenie osoby podejmuj�cej dan� czynno��", fontNormal)));
                contentTable.addCell( new Cell(new Paragraph("Okre�lenie podejmowanej czynno�ci", fontNormal)));
                contentTable.addCell( new Cell(new Paragraph("Wskazanie identyfikatora w aktach sprawy, do kt�rego odnosi si� dana czynno��", fontNormal)));

                Integer lp_index = 1;
               
                //Sortowanie wszystkich wpis�w po dacie
                Collections.sort(historia, new Comparator<HistoryEntry>(){
                    public int compare (HistoryEntry h1, HistoryEntry h2){
                        return h1.data.compareTo(h2.data);
                    }
                });
                // Tresc tabeli
                for( HistoryEntry wpis : historia)
                {
                    Cell lp = new Cell(new Paragraph(lp_index.toString(), fontNormal));
                    Cell data = new Cell(new Paragraph(DateUtils.formatCommonDateTime(wpis.data), fontNormal));
                    Cell osoba;
                 
                    if (!AvailabilityManager.isAvailable("pg.externalname.metrics")) {
                        osoba = new Cell(new Paragraph(wpis.osoba, fontNormal));
                    } else {
                        String[] splited = DSUser.getUserByFirstnameLastnameBySplit(wpis.osoba, " ");
                        if(splited != null) {

                            DSDivision[] dzialy = DSUser.findByFirstnameLastname(splited[0], splited[1]).getDivisionsWithoutGroup();
                            String dzial =  "";
                            for(int i = 0; i < dzialy.length; i++) {
                                dzial = dzial + dzialy[i].getName() + " ";
                            }

                            osoba = new Cell(new Paragraph(wpis.osoba + " (" + DSUser.findByFirstnameLastname(splited[0], splited[1]).getExternalName() + ") " + dzial, fontNormal));
                        } else {
                            osoba = new Cell(new Paragraph(wpis.osoba, fontNormal));
                        }
                    }
                    Cell czynnosc = new Cell(new Paragraph(wpis.czynnosc, fontNormal));
                    StringBuilder ident= null;
                    if(wpis.barcode!=null){
                     ident = new StringBuilder("ID:");
                    ident.append(wpis.identyfikator);
                    ident.append(",Barkod: ");
                    ident.append(wpis.barcode);
                    }
                    else{
                    	ident = new StringBuilder(wpis.identyfikator);
                    }
                    Cell identyfikator = new Cell(new Paragraph(ident.toString(), fontNormal));

                    contentTable.addCell(lp);
                    contentTable.addCell(data);
                    contentTable.addCell(osoba);
                    contentTable.addCell(czynnosc);
                    contentTable.addCell(identyfikator);

                    ++lp_index;
                }

                pdfDoc.add(contentTable);
                pdfDoc.close();
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new EdmException(e);
            }

            return pdfFile;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return metrics;
    }

    private static List<HistoryEntry> getData(List<OfficeDocument> docs) throws EdmException
    {
        List<HistoryEntry> historia = new ArrayList<HistoryEntry>();
        for(OfficeDocument doc : docs)
        {
            List<AssignmentHistoryEntry> historiaDekretacji = doc.getAssignmentHistory();
            List<Audit> historiaPisma = doc.getWorkHistory();

            String docId = doc.getId().toString();

            for( AssignmentHistoryEntry ashistory : historiaDekretacji)
            {
                AssignmentHistoryTabAction.AssignmentHistoryBean ashistoryBean = new AssignmentHistoryTabAction.AssignmentHistoryBean(ashistory);
                historia.add(new HistoryEntry(
                        ashistory.getCtime(),	// data
                        DSUser.safeToFirstnameLastname(ashistory.getSourceUser()),	// osoba
                        ashistoryBean.getContent(),	// czynnosc
                        docId,	// identyfikator
                        doc.getBarcode() //barkode
                ));
            }

            for( Audit audit : historiaPisma)
                historia.add(new HistoryEntry(
                        audit.getCtime(),	// data
                        DSUser.safeToFirstnameLastname(audit.getUsername()),	// osoba
                        audit.getDescription(),	// czynnosc
                        docId,	// identyfikator
                        doc.getBarcode() //barkode
                ));
        }

        // usuwanie duplikat�w
        for (int a = 0; a < historia.size(); a++) {
            for (int b = a + 1; b < historia.size(); b++) {
                String h1 = historia.get(a).data.toString().substring(0, 20);
                String h2 = historia.get(b).data.toString().substring(0, 20);
                if (h1.equals(h2) && historia.get(a).czynnosc.equals(historia.get(b).czynnosc)
                        && historia.get(a).osoba.equals(historia.get(b).osoba)
                        && historia.get(a).identyfikator.equals(historia.get(b).identyfikator))

                    historia.remove(historia.get(b));
            }
        }
        return historia;
    }

}

class HistoryEntry
{
    Date data;
    String osoba;
    String czynnosc;
    String identyfikator;
    String barcode;

    HistoryEntry(Date data, String osoba, String czynnosc, String identyfikator,String barcode){
        this.data=data;
        this.osoba=osoba;
        this.czynnosc=czynnosc;
        this.identyfikator=identyfikator;
        this.barcode=barcode;
    }

}

