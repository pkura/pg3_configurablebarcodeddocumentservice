package pl.compan.docusafe.web.office;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.DynaProperty;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeCase;


/**
 * Klasa odwzorowuj�ca tabelk� DS_CASE_TO_CASE
 * 
 * @author Grzegorz Filip
 * @date: 03.04.2014
 */

public class CaseToCase  implements Serializable
{



	private long id;
	private String caseOfficeId;
	private String caseTitle;
    /** Sprawa do kt�rej dowi�zujemy inne sprawy*/
	private long caseContainerId;
	private long creatingUserId;
	private String creatingUserName;
	private String caseAuthor;
	private String linkToCase;
    /** Sprawy powi�zane do caseContainerId */
	private long linkedCaseId;
	private Date creationTime;
	private Date caseOpenDate;
	private Date casefinishDate;
   
	 
	/**
	 * @param id
	 * @param caseId
	 * @param creatingUserId
	 * @param creatingUserName
	 * @param linkToCase
	 * @param linkedCaseId
	 * @param creationLinkTime 
	 * @param caseTitle 
	 * @param creationTime
	 */
/*	public CaseToCase(long id, long caseId, long creatingUserId, String creatingUserName, String linkToCase, long linkedCaseId, Date creationTime)
	{
		super();
		this.id = id;
		this.caseId = caseId;
		this.caseTitle = caseTitle;
		
		this.creatingUserId = creatingUserId;
		this.creatingUserName = creatingUserName;
		this.linkToCase = linkToCase;
		this.linkedCaseId = linkedCaseId;
		this.creationLinkTime = creationLinkTime;
	}
*/
	/**
	 * Domy�lny konstruktor
	 */
	public CaseToCase() 
	{
	}
	
	
	public CaseToCase(long id, String caseOfficeId, long caseContainerId, long creatingUserId, String creatingUserName, String caseAuthor, String linkToCase,
			long linkedCaseId, Date creationTime, Date caseOpenDate, Date casefinishDate, Date creationLinkTime, String caseTitle)
	{
		super();
		this.id = id;
		this.caseOfficeId = caseOfficeId;
		this.caseTitle = caseTitle;
		this.creationTime = creationTime;
		this.caseContainerId = caseContainerId;
		this.creatingUserId = creatingUserId;
		this.creatingUserName = creatingUserName;
		this.caseAuthor = caseAuthor;
		this.linkToCase = linkToCase;
		this.linkedCaseId = linkedCaseId;
		
		this.caseOpenDate = caseOpenDate;
		this.casefinishDate = casefinishDate;
	}

    /**
     * Zwraca list� spraw powi�zane z dan� spraw�.
     * @param c
     * @return
     * @throws EdmException
     */
    public static List<OfficeCase> findLinkedOfficeCases(OfficeCase c) throws EdmException {
        List<OfficeCase> cases = DSApi.context().session().createQuery("SELECT DISTINCT c FROM OfficeCase c where c.id in ( " +
                "SELECT ctc.linkedCaseId FROM CaseToCase ctc WHERE ctc.caseContainerId = :caseId )")
                .setParameter("caseId", c.getId())
                .list();

        return cases;
    }
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getCaseOfficeId()
	{
		return caseOfficeId;
	}
	
	public void setCaseOfficeId(String caseOfficeId)
	{
		this.caseOfficeId = caseOfficeId;
	}
	
	public long getCreatingUserId()
	{
		return creatingUserId;
	}
	
	public void setCreatingUserId(long creatingUserId)
	{
		this.creatingUserId = creatingUserId;
	}
	
	public String getCreatingUserName()
	{
		return creatingUserName;
	}
	
	public void setCreatingUserName(String creatingUserName)
	{
		this.creatingUserName = creatingUserName;
	}
	
	public String getLinkToCase()
	{
		return linkToCase;
	}
	
	public void setLinkToCase(String linkToCase)
	{
		this.linkToCase = linkToCase;
	}
	
	public long getLinkedCaseId()
	{
		return linkedCaseId;
	}
	
	public void setLinkedCaseId(long linkedCaseId)
	{
		this.linkedCaseId = linkedCaseId;
	}
	
	public Date getCreationTime()
	{
		return creationTime;
	}
	
	public void setCreationTime(Date creationTime)
	{
		this.creationTime = creationTime;
	}
	
	public long getCaseContainerId()
	{
		return caseContainerId;
	}
	
	public void setCaseContainerId(long caseContainerId)
	{
		this.caseContainerId = caseContainerId;
	}
	
	public String getCaseAuthor()
	{
		return caseAuthor;
	}
	
	public void setCaseAuthor(String caseAuthor)
	{
		this.caseAuthor = caseAuthor;
	}
	
	
	public Date getCaseOpenDate()
	{
		return caseOpenDate;
	}
	
	public void setCaseOpenDate(Date caseOpenDate)
	{
		this.caseOpenDate = caseOpenDate;
	}
	
	public Date getCasefinishDate()
	{
		return casefinishDate;
	}
	
	public void setCasefinishDate(Date casefinishDate)
	{
		this.casefinishDate = casefinishDate;
	}
	
	public String getCaseTitle()
	{
		return caseTitle;
	}
	
	public void setCaseTitle(String caseTitle)
	{
		this.caseTitle = caseTitle;
	}
	
	
}
