package pl.compan.docusafe.web.office;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class GetDocument extends EventActionSupport
{
	private static Logger log = LoggerFactory.getLogger(CurrentDayAction.class);

	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

	private String barcode;

	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").
			append(OpenHibernateSession.INSTANCE).
			append(new Search()).
			append(fillForm).
			append(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

		}
	}
	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

			if (barcode != null) {

				List<InOfficeDocument> doc;
				List<OutOfficeDocument> docOut;
				String[] po = barcode.split(", ");
				for(int i=0; i<po.length;i++){
					String nowyBarcode = po[i];
					Date data = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					try {
						doc = InOfficeDocument.findByBarcode(nowyBarcode);
						docOut = OutOfficeDocument.findByBarcode(nowyBarcode);
						if(doc.size() != 0){
							for(InOfficeDocument inO: doc){
								OfficeDocument docIn = OfficeDocument.find(inO.getId());
								if(docIn.getCzyAktualny()){
									docIn.addWorkHistoryEntry(Audit.create("barcode", 
											DSApi.context().getPrincipalName(), sm.getString("PrzyjeciePisma", sdf.format(data))));
									event.addActionMessage("Przyj�to pismo: " + nowyBarcode);								
								}
							}
						} else if(docOut.size() != 0){
							for(OutOfficeDocument outO:docOut){
								OfficeDocument docO = OfficeDocument.find(outO.getId());
								if(docO.getCzyAktualny()){
									OutOfficeDocument internal = OutOfficeDocument.findOutOfficeDocument(docO.getId());
									if(internal.isInternal()){
										docO.addWorkHistoryEntry(Audit.create("barcode",
												DSApi.context().getPrincipalName(), sm.getString("PrzyjeciePisma", sdf.format(data))));
										event.addActionMessage("Przyj�to pismo: " + nowyBarcode);
									}else{
										docO.addWorkHistoryEntry(Audit.create("barcode",
												DSApi.context().getPrincipalName(), sm.getString("PrzyjeciePisma", sdf.format(data))));
										event.addActionMessage("Przyj�to pismo: " + nowyBarcode);
									}
								}
							}
						} else {
							event.addActionError("nie isnieje kod kreskowy o podanym numerze " + barcode);
						}
					} catch (EdmException e) {
						log.error("", e);
					}
				}
			}
		}
	}

	public void setBarcode(String barcode)
	{
		this.barcode=barcode;
	}

}
