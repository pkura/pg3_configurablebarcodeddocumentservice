package pl.compan.docusafe.web.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

public class TicketsListAction extends EventActionSupport {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(EventActionSupport.class);
	boolean canRead = false;
	private List<DSUser> users;
	private List<Ticket> bilety;
	
	private String sortField;
    private boolean ascending = true;
    private boolean showGenerujButton = false;
    private String accepted;
    private String tripPurpose;
    private String trasa;
    private Integer workerId;
    
    private String dateTripFrom;
    private String dateTripTo;
	private Integer nrZestawienia;
    
    private static List<String> filtr = new ArrayList<String>();
	
    static {
		filtr.add("Wszystkie");
		filtr.add("Zaakceptowane");
		filtr.add("Zestawione");
		filtr.add("Wykupione");
	}	
	
	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		 registerListener(DEFAULT_ACTION)
		 	.append(OpenHibernateSession.INSTANCE)
		 	.append("FILL_FORM", fillForm)
		 	.appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doSearch")
		 	.append(OpenHibernateSession.INSTANCE)
		 	.append("FILL_FORM", fillForm)
		 	.appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doGenerateZestawienie")
		 	.append(OpenHibernateSession.INSTANCE)
		 	.append("FILL_FORM", fillForm)
		 	.append(null, new GenerateZestawienie())
		 	.appendFinally(CloseHibernateSession.INSTANCE);
		 
	}

	private class FillForm implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent event) {
			log.info(" --- TicketsListAction.FillForm --- ");
			try {
        		users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
        		
        		log.info("sortField = " + sortField);
        		log.info("ascending = " + ascending);
        		log.info("tripPurpose = " + tripPurpose);
        		log.info("workerId = " + workerId);
        		log.info("accepted = " + accepted);
        		log.info("nrZestawienia = " + nrZestawienia);
        		
        		bilety = Ticket.list(sortField, ascending, tripPurpose, trasa, workerId, accepted, dateTripFrom, dateTripTo, nrZestawienia);
        		
        		if (bilety!= null && bilety.size() > 0 && accepted!= null && accepted.equalsIgnoreCase("Zaakceptowane"))
        			showGenerujButton = true;
			} catch (SQLException e) {
				log.error(e.getMessage(),e);
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			} 
		}
		
	}

	public class GenerateZestawienie implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent event) {
			
			File pdfFile = null;
        	try
        	{
        		for (Ticket bilet : bilety) {
					log.info("bilet.getStatus() = " + bilet.getStatus());
					if (bilet.getStatus() == 30) 
						throw new EdmException("Wniosek o id=" + bilet.getDocumentId() + " znajduje si� ju� na zestawieniu nr " + bilet.getNrZestawienia());
					else if (bilet.getStatus() == 150)
						throw new EdmException("Bilet o numerze rezerwacji " + bilet.getNrRezerwacji() + " zosta� ju� wykupiony");
					else if (bilet.getStatus() == null || bilet.getStatus() != 20 ) {
						throw new EdmException("Wszystkie wnioski musz� mie� zaakceptowane �r�d�o finansowania");
					}
				}
        		nrZestawienia = Ticket.nadajNowyNumerZestawienia();
				for (Ticket bilet : bilety) {
					bilet.zapiszNrZestawienia(nrZestawienia);
				}
				
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                int fontsize = 10;
                Font font = new Font(baseFont, (float)fontsize);

                pdfFile = File.createTempFile("docusafe_", "_tmp");
                Document pdfDoc =
                    new Document(PageSize.A4.rotate(),30,60,30,30);
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));
                
                HeaderFooter footer =
               	 new HeaderFooter(new Phrase("Sporz�dzone przez "+DSApi.context().getDSUser().asFirstnameLastname()+ " " +
                            DateUtils.formatCommonDateTime(new Date()) + " Strona nr ", font), new Phrase("."));
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);
                
                pdfDoc.open();

                PdfUtils.addHeaderImageToPdf(pdfDoc, "Zestawienie nr " + nrZestawienia, baseFont);

                int[] widths = new int[] { 3, 4 , 5, 5, 2, 2};

                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(3);

                Cell cell_nr = new Cell(new Phrase("Numer rezerwacji", font));
                cell_nr.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_nr);

                Cell cell_worker = new Cell(new Phrase("Pracownik delegowany", font));
                cell_worker.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_worker);
                table.endHeaders();
                
                Cell cell_tripPurpose = new Cell(new Phrase("Cel podr�y", font));
                cell_tripPurpose .setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_tripPurpose);
                table.endHeaders();
                
                Cell cell_trasa = new Cell(new Phrase("Trasa", font));
                cell_trasa .setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_trasa);
                table.endHeaders();
                
                Cell cell_startDate = new Cell(new Phrase("Data wyjazdu", font));
                cell_startDate.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_startDate);
                table.endHeaders();
                
                Cell cell_finishDate = new Cell(new Phrase("Data powrotu", font));
                cell_finishDate.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(cell_finishDate);
                table.endHeaders();
                
                for (Ticket row : bilety)
                {
                	table.addCell(String.format(new Locale("pl"), row.getNrRezerwacji()));
                	table.addCell(String.format(new Locale("pl"), row.getWorkerName()));
                	table.addCell(row.getTripPurpose());
                	table.addCell(row.getTrasa());
                	table.addCell(row.getStartDate() == null ? "" : row.getStartDate().toString());
                	table.addCell(row.getFinishDate() == null ? "" : row.getFinishDate().toString());
                }
                
                        
                
                pdfDoc.add(table);
                pdfDoc.close();
			} catch (EdmException e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			} catch (Exception ex) {
				addActionError(ex.getMessage());
				log.error("", ex);
			}
        	
            if (pdfFile != null && pdfFile.exists())
            {
                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdfFile),
                        "application/pdf", (int) pdfFile.length(),
                        "Content-Disposition: attachment; filename=\"zestawienie.pdf\"");
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {                	
                    log.error("", e);
                }
                finally
                {
                	pdfFile.delete();
                }
            }
		
          sortField = null;
		ascending = false;
		tripPurpose = null;
		workerId = null;
		accepted = null;
		nrZestawienia = null;
     		
		}
		
	}
	
	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public List<Ticket> getBilety() {
		return bilety;
	}

	public void setBilety(List<Ticket> bilety) {
		this.bilety = bilety;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public String getAccepted() {
		return accepted;
	}

	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}

	public String getTripPurpose() {
		return tripPurpose;
	}

	public void setTripPurpose(String tripPurpose) {
		this.tripPurpose = tripPurpose;
	}

	public Integer getWorkerId() {
		return workerId;
	}

	public void setWorkerId(Integer workerId) {
		this.workerId = workerId;
	}

	public String getDateTripFrom() {
		return dateTripFrom;
	}

	public void setDateTripFrom(String dateTripFrom) {
		this.dateTripFrom = dateTripFrom;
	}

	public String getDateTripTo() {
		return dateTripTo;
	}

	public void setDateTripTo(String dateTripTo) {
		this.dateTripTo = dateTripTo;
	}

	public Integer getNrZestawienia() {
		return nrZestawienia;
	}

	public void setNrZestawienia(Integer nrZestawienia) {
		this.nrZestawienia = nrZestawienia;
	}

	public static List<String> getFiltr() {
		return filtr;
	}

	public static void setFiltr(List<String> filtr) {
		TicketsListAction.filtr = filtr;
	}

	public boolean isShowGenerujButton()
	{
		return showGenerujButton;
	}

	public void setShowGenerujButton(boolean showGenerujButton)
	{
		this.showGenerujButton = showGenerujButton;
	}

	public String getTrasa()
	{
		return trasa;
	}

	public void setTrasa(String trasa)
	{
		this.trasa = trasa;
	}
	
	
}
