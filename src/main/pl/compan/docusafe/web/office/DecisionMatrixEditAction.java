package pl.compan.docusafe.web.office;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.ProsikaDecisionRule;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DecisionMatrixEditAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(DecisionMatrixEditAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(DecisionMatrixEditAction.class.getPackage().getName(), null);
	
	private Long id;
	private Integer priorytet;
	private String segment;
	private String typ;
	private String podtyp;
	private String kanalWplywu;
	private Integer pnaFrom;
	private Integer pnaTo;
	private String division;
	private String user;
	private String divisionName;
	private String userName;
	private Boolean auto;
	private DocumentKind dk;
	private String matrixGuid;

	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSave").
			append(OpenHibernateSession.INSTANCE).
			append(new Save()).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doCreate").
			append(OpenHibernateSession.INSTANCE).
			append(new Create()).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doDelete").
			append(OpenHibernateSession.INSTANCE).
			append(new Delete()).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if(id != null) 
				{
					ProsikaDecisionRule pdr = ProsikaDecisionRule.find(id);
					priorytet = pdr.getPriority();
					segment = pdr.getSegment();
					typ = pdr.getTyp();
					podtyp = pdr.getPodTyp();
					kanalWplywu = pdr.getKanalWplywu();
					pnaFrom = pdr.getPnaGroupFrom();
					pnaTo = pdr.getPnaGroupTo();
					division = pdr.getTargetGuid();
					user = pdr.getTargetUser();
					try
					{
						divisionName = DSDivision.find(division).getName();
					}catch (Exception e){}				
					try
					{
						userName = DSUser.findByUsername(user).asFirstnameLastname();
					}catch (Exception e) {}
					
					auto = pdr.getAutoAssign();
				}
				dk = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
				//if(division == null || division.equals(""))
				//	division = matrixGuid;
				//if(division == null)
				//	division = DSDivision.ROOT_GUID;
			}
			catch (Exception e)
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("", e);
			}
		}
	}
	
	private class Save implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				ProsikaDecisionRule pdr = ProsikaDecisionRule.find(id);
				pdr.setAutoAssign(auto);
				pdr.setKanalWplywu(kanalWplywu);
				pdr.setMatrixGuid(getMatrixGuid());
				pdr.setPnaGroupFrom(pnaFrom);
				pdr.setPnaGroupTo(pnaTo);
				pdr.setPodTyp(podtyp);
				pdr.setPriority(priorytet);
				pdr.setSegment(segment);
				pdr.setTargetGuid(division);
				pdr.setTargetUser(user);
				pdr.setTyp(typ);		
				DSApi.context().commit();
				addActionMessage(sm.getString("ZapisanoZmiany"));
			}
			catch (Exception e) 
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("", e);
			}
		}
	}
	
	private class Delete implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				ProsikaDecisionRule pdr = ProsikaDecisionRule.find(id);
				pdr.delete();
				id= null;
				DSApi.context().commit();
				addActionMessage(sm.getString("UsunietoRegule"));
			}
			catch (Exception e) 
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("", e);
			}
		}
	}
	
	private class Create implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				
				ProsikaDecisionRule pdr = new ProsikaDecisionRule();
				pdr.setAutoAssign(auto);
				pdr.setKanalWplywu(kanalWplywu);
				pdr.setMatrixGuid(getMatrixGuid());
				pdr.setPnaGroupFrom(pnaFrom);
				pdr.setPnaGroupTo(pnaTo);
				pdr.setPodTyp(podtyp);
				if(priorytet == null)
					throw new EdmException(sm.getString("PriorytetJestObowiazkowy"));
				pdr.setPriority(priorytet);
				pdr.setSegment(segment);
				if(priorytet == null)
					throw new EdmException(sm.getString("DzialJestObowiazkowy"));
				pdr.setTargetGuid(division);
				pdr.setTargetUser(user);
				pdr.setTyp(typ);
				pdr.create();			
				DSApi.context().commit();
				addActionMessage("DodanoRegule");
			}
			catch (Exception e) 
			{
				addActionError("B��d :"+ e.getMessage());
				log.error("", e);
			}
		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Integer getPriorytet()
	{
		return priorytet;
	}

	public void setPriorytet(Integer priorytet)
	{
		this.priorytet = priorytet;
	}

	public String getSegment()
	{
		return segment;
	}

	public void setSegment(String segment)
	{
		this.segment = segment;
	}

	public String getTyp()
	{
		return typ;
	}

	public void setTyp(String typ)
	{
		this.typ = typ;
	}

	public String getPodtyp()
	{
		return podtyp;
	}

	public void setPodtyp(String podtyp)
	{
		this.podtyp = podtyp;
	}

	public String getKanalWplywu()
	{
		return kanalWplywu;
	}

	public void setKanalWplywu(String kanalWplywu)
	{
		this.kanalWplywu = kanalWplywu;
	}

	public Integer getPnaFrom()
	{
		return pnaFrom;
	}

	public void setPnaFrom(Integer pnaFrom)
	{
		this.pnaFrom = pnaFrom;
	}

	public Integer getPnaTo()
	{
		return pnaTo;
	}

	public void setPnaTo(Integer pnaTo)
	{
		this.pnaTo = pnaTo;
	}

	public String getDivision()
	{
		return division;
	}

	public void setDivision(String division)
	{
		this.division = division;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getDivisionName()
	{
		return divisionName;
	}

	public void setDivisionName(String divisionName)
	{
		this.divisionName = divisionName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public Boolean getAuto()
	{
		return auto;
	}

	public void setAuto(Boolean auto)
	{
		this.auto = auto;
	}

	public DocumentKind getDk()
	{
		return dk;
	}

	public void setDk(DocumentKind dk)
	{
		this.dk = dk;
	}

	public void setMatrixGuid(String matrixGuid)
	{
		this.matrixGuid = matrixGuid;
	}

	public String getMatrixGuid()
	{
		return matrixGuid;
	}
}
