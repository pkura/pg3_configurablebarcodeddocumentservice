package pl.compan.docusafe.web.office;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class Ticket {

	private static Logger log = LoggerFactory.getLogger(Ticket.class);

	private Long documentId;
	private Integer status;
	private Integer workerId;
	private String workerName;
	private Date startDate;
	private Date finishDate;
	private String tripPurpose;
	private String trasa;
	private BigDecimal koszt;
	private String nrRezerwacji;
	private Date purchaseDate;
	private int workerDivisionId;
	private Long delegacjaId;
	private Integer nrZestawienia;

	public static List<Ticket> list(Integer nrZestawienia) throws SQLException, EdmException {
		return list(null, true, null, null, null, null, null, null, nrZestawienia);
	}
	
	public static List<Ticket> list(String sortField, boolean ascending, String tripPurpose, String trasa, Integer workerId, String accepted, String dateTripFrom, String dateTripTo, Integer nrZestawienia)
			throws SQLException, EdmException {

		List<Ticket> results = new ArrayList<Ticket>();
		
		if ((accepted == null || accepted.equals("")) && nrZestawienia == null)
			return results;
		if (dateTripFrom != null)
		{
			try
			{
				Date dateFrom = DateUtils.parseDateAnyFormat(dateTripFrom);
				dateTripFrom = DateUtils.sqlDateFormat.format(dateFrom);
			}
			catch (ParseException e)
			{
				log.error(e.getMessage(), e);
			}
		}
		
		if (dateTripTo != null)
		{
			try
			{
				Date dateTrip = DateUtils.parseDateAnyFormat(dateTripTo);
				dateTripTo = DateUtils.sqlDateFormat.format(dateTrip);
			}
			catch (ParseException e)
			{
				log.error(e.getMessage(), e);
			}
		}
		
		String sql = createSQL(sortField, ascending, tripPurpose, trasa, workerId, accepted, dateTripFrom, dateTripTo, nrZestawienia);
		
		log.info("Ticket.sql: " + sql);
		
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		

		if (rs != null) {
			for (; rs.next();) {
				Ticket ticket = new Ticket();
				ticket.setDocumentId(rs.getLong("DOCUMENT_ID"));
				ticket.setStatus(rs.getInt("STATUS"));
				ticket.setWorkerId(rs.getInt("WORKER"));
				ticket.setStartDate(rs.getDate("start_date"));
				ticket.setFinishDate(rs.getDate("finish_date"));
				ticket.setTripPurpose(rs.getString("trip_purpose"));
				ticket.setTrasa(rs.getString("trasa"));
				ticket.setKoszt(rs.getBigDecimal("koszt"));
				ticket.setNrRezerwacji(rs.getString("nr_rezerwacji"));
				ticket.setPurchaseDate(rs.getDate("purchase_date"));
				ticket.setWorkerDivisionId(rs.getInt("worker_division"));
				ticket.setDelegacjaId(rs.getLong("instr_wyjazdowa"));
				ticket.setNrZestawienia(rs.getInt("nr_zestawienia"));
				ticket.setWorkerName(rs.getString("LASTNAME") + " " + rs.getString("FIRSTNAME"));
				results.add(ticket);
			}
		}
		rs.close();
		DSApi.context().closeStatement(ps);
		return results;
	}

	/**
	 * @param tripPurpose
	 * @param tripPurpose2 
	 * @param ascending 
	 * @param workerId
	 * @param accepted
	 * @return
	 */
	public static String createSQL(String sortField, boolean ascending, String tripPurpose, String trasa, Integer workerId, String accepted, String dateTripFrom, String dateTripTo, Integer nrZestawienia) {
		StringBuilder select = new StringBuilder("SELECT " +
				"b.DOCUMENT_ID, " +
				"b.STATUS, " +
				"b.WORKER, " +
				"b.start_date, " +
				"b.finish_date," +
				"b.trip_purpose, " +
				"b.trasa, " +
				"b.koszt, " +
				"b.nr_rezerwacji, " +
				"b.purchase_date, " +
				"b.worker_division, " + 
				"b.instr_wyjazdowa, " +
				"b.nr_zestawienia, " +
				"u.LASTNAME, " +
				"u.FIRSTNAME");
		StringBuilder from = new StringBuilder("FROM dsg_ifpan_bilety b, ds_user u");
		StringBuilder where = new StringBuilder("WHERE u.ID = b.WORKER");
		StringBuilder order = new StringBuilder("ORDER BY ");

		if (tripPurpose != null) {
			where.append(" AND ");
			where.append("b.trip_purpose like '%" + tripPurpose + "%'");
		}
		if (trasa != null) {
			where.append(" AND ");
			where.append("b.trasa like '%" + tripPurpose + "%'");
		}
		if (workerId != null && workerId > 0) {
			where.append(" AND ");
			where.append("b.worker = " + workerId);
		}
		if (accepted != null && "Zaakceptowane".equals(accepted)) {
			where.append(" AND ");
			where.append("b.status = 20");
		} else if (accepted != null && "Zestawione".equals(accepted)) {
			where.append(" AND ");
			where.append("b.status = 30");
		} else if (accepted != null && "Wykupione".equals(accepted)) {
			where.append(" AND ");
			where.append("b.status = 150");
		}
		if (dateTripFrom != null) {
			where.append(" AND ");
			where.append("b.start_date >= '" + dateTripFrom + "'");
		}
		if (dateTripTo != null) {
			where.append(" AND ");
			where.append("b.finish_date <= '" + dateTripTo + "'");
		}
		if (nrZestawienia != null) {
			where.append(" AND ");
			where.append("b.nr_zestawienia = '" + nrZestawienia + "'");
			where.append(" AND b.status = 30");
		}
		if (sortField == null){
			if (ascending) order.append("u.lastname");
			else order.append("u.lastname desc");
		} else {
			if (ascending) order.append("b." + sortField);
			else order.append("b." + sortField + " desc"); 
		}

		return select + " " + from + " " + where + " " + order;
	}
	
	public void zapiszNrZestawienia(Integer nrZestawienia) throws SQLException, EdmException {
		this.setNrZestawienia(nrZestawienia);
		PreparedStatement ps = DSApi.context().prepareStatement("UPDATE dsg_ifpan_bilety set nr_zestawienia = ?, STATUS = 30 where DOCUMENT_ID = ?");
		ps.setInt(1, nrZestawienia);
		ps.setLong(2, documentId);
		
		ps.executeUpdate();
		DSApi.context().closeStatement(ps);
		
	}
	
	public static Integer nadajNowyNumerZestawienia() throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement("SELECT MAX(nr_zestawienia) FROM dsg_ifpan_bilety");
		ResultSet rs = ps.executeQuery();
		rs.next();
		Integer nowyNumer = rs.getInt(1);
		if (nowyNumer == null || nowyNumer <= 0)
			nowyNumer = 1;
		else {
			nowyNumer += 1;
		}
		return nowyNumer;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getWorkerId() {
		return workerId;
	}

	public void setWorkerId(Integer workerId) {
		this.workerId = workerId;
	}

	public String getWorkerName() {
		return workerName;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getTripPurpose() {
		return tripPurpose;
	}

	public void setTripPurpose(String tripPurpose) {
		this.tripPurpose = tripPurpose;
	}

	public BigDecimal getKoszt() {
		return koszt;
	}

	public void setKoszt(BigDecimal koszt) {
		this.koszt = koszt;
	}

	public String getNrRezerwacji() {
		return nrRezerwacji;
	}

	public void setNrRezerwacji(String nrRezerwacji) {
		this.nrRezerwacji = nrRezerwacji;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getWorkerDivisionId() {
		return workerDivisionId;
	}

	public void setWorkerDivisionId(int workerDivisionId) {
		this.workerDivisionId = workerDivisionId;
	}

	public Long getDelegacjaId() {
		return delegacjaId;
	}

	public void setDelegacjaId(Long delegacjaId) {
		this.delegacjaId = delegacjaId;
	}

	public Integer getNrZestawienia() {
		return nrZestawienia;
	}

	public void setNrZestawienia(Integer nrZestawienia) {
		this.nrZestawienia = nrZestawienia;
	}

	public String getTrasa()
	{
		return trasa;
	}

	public void setTrasa(String trasa)
	{
		this.trasa = trasa;
	}


}
