package pl.compan.docusafe.web.office;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.sql.SQLException;
import java.util.*;

/**
 * Okre�lenie aktualnie otwartego roku. Otwarty rok ma wp�yw
 * m.in. na tworzenie spraw.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CurrentYearAction.java,v 1.12 2007/09/26 12:46:26 mariuszk Exp $
 */
public final class CurrentYearAction extends EventActionSupport
{
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(CurrentYearAction.class.getPackage().getName(),null);

    // @EXPORT
    private Integer currentYear;
    private Map journals;
    private Map portfolios;
    private boolean canCloseYear;
    private boolean canOpenAnyYear;
    private List years;

    // @IMPORT
    private Integer newYear;
    private Long[] journalIds;

    protected void setup()
    {
        final FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCloseYearPrep").
            append(OpenHibernateSession.INSTANCE).
            append(new CloseYearPrep()).
            append("fillForm", fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doOpenAnyYear").
            append(OpenHibernateSession.INSTANCE).
            append(new OpenAnyYear()).
            append("fillForm", fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCloseYear").
            append(OpenHibernateSession.INSTANCE).
            append(new CloseYear()).
            append("fillForm", fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    /**
     * Otwarcie roku w aplikacji.
     */
    private final class CloseYearPrep implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.ROK_ZAMKNIECIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoZamykaniaRoku"));

                // CurrentYearNotOpenException
                GlobalPreferences.getCurrentYear();

                journals = new LinkedHashMap();
                portfolios = new LinkedHashMap();

                for (Iterator iter=Journal.list().iterator(); iter.hasNext(); )
                {
                    Journal j = (Journal) iter.next();
                    journals.put(j.getId(),
                        (j.getOwnerGuid() != null ?
                            DSDivision.find(j.getOwnerGuid()).getPrettyPath()+" - " :
                            "") +
                        j.getDescription());
                }

/*
                for (Iterator iter=OfficeFolder.findAll().iterator(); iter.hasNext(); )
                {
                    OfficeFolder p = (OfficeFolder) iter.next();
                    portfolios.put(p.getId(),
                        DSDivision.find(p.getDivisionGuid()).getPrettyPath()+" - "+
                        p.getName());
                }
*/

                event.setResult("close-year");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class CloseYear implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.ROK_ZAMKNIECIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoZamykaniaRoku"));

                int currentYear = GlobalPreferences.getCurrentYear();
                int newYear = currentYear + 1;

                if (journalIds != null && journalIds.length > 0)
                {
                    for (int i=0; i < journalIds.length; i++)
                    {
                        Journal journal = Journal.find(journalIds[i]);
                        /*Journal copy = */journal.reopen(newYear);
                    }
                }

                // znalezienie wszystkich teczek nadrz�dnych, nadanie
                // im nowych numer�w, nast�pnie ta sama operacja dla
                // ich teczek podrz�dnych (bo ich numer zawiera {pp} ...)
                if(!Docusafe.hasExtra("business"))
                    OfficeFolder.newYearFolders(newYear);

                GlobalPreferences.setCurrentYear(new Integer(newYear));

                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, newYear);
                cal.set(Calendar.MONTH, Calendar.JANUARY);
                cal.set(Calendar.DAY_OF_MONTH, 1);

                GlobalPreferences.setCurrentDay(cal.getTime());

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
				 DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
        }
    }

    private class OpenAnyYear implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (newYear == null)
                addActionError(sm.getString("NiePodanoOtwieranegoRoku"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().isAdmin())
                    throw new EdmException(sm.getString("BrakUprawnienDoOtwieraniaDowolnegoRoku"));

                try
                {
                    GlobalPreferences.getCurrentYear();
                    throw new EdmException(sm.getString("RokZostalJuzOtwarty"));
                }
                catch (GlobalPreferences.CurrentYearNotOpenException e)
                {
                    GlobalPreferences.setCurrentYear(newYear);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private final class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                try
                {
                    currentYear = new Integer(GlobalPreferences.getCurrentYear());
                }
                catch (GlobalPreferences.CurrentYearNotOpenException e)
                {
                }

                years = new ArrayList(2);
                int year = Calendar.getInstance().get(Calendar.YEAR);
                years.add(new Integer(year));
                years.add(new Integer(year+1));

                canCloseYear = DSApi.context().hasPermission(DSPermission.ROK_ZAMKNIECIE);
                canOpenAnyYear = DSApi.context().isAdmin();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public Integer getCurrentYear()
    {
        return currentYear;
    }

    public void setNewYear(Integer newYear)
    {
        this.newYear = newYear;
    }

    public Map getJournals()
    {
        return journals;
    }

    public Map getPortfolios()
    {
        return portfolios;
    }

    public boolean isCanCloseYear()
    {
        return canCloseYear;
    }

    public boolean isCanOpenAnyYear()
    {
        return canOpenAnyYear;
    }

    public List getYears()
    {
        return years;
    }

    public void setJournalIds(Long[] journalIds)
    {
        this.journalIds = journalIds;
    }
}
