package pl.compan.docusafe.web.office;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.DecisionMatrix;
import pl.compan.docusafe.parametrization.prosika.DecisionMatrixProcessor;
import pl.compan.docusafe.parametrization.prosika.ProsikaDecisionRule;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DecisionMatrixAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(DecisionMatrixAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(DecisionMatrixAction.class.getPackage().getName(), null);

	private String selectedGuid;
	private Map<String, DSDivision> divisions;
	private Collection<ProsikaDecisionRule> decisionRules;
	private DocumentKind dk;
	private Map<String, String> users;
	private Map<String, String> decisionRulesDivision;
	private boolean toMyDivision;

	protected void setup()
	{
		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(new FillForm()).appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Collection<DecisionMatrix> matrix;
			try
			{
				users = new HashMap<String, String>();
				for (DSUser user : DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME))
				{
					users.put(user.getName(), user.asFirstnameLastname());
				}
				if (toMyDivision)
				{
					DSDivision[] divisionsTab = DSApi.context().getDSUser().getDivisionsWithoutGroup();
					divisions = new HashMap<String, DSDivision>();
					for (DSDivision div : divisionsTab)
					{
						divisions.put(div.getGuid(), div);
						if (selectedGuid == null)
							selectedGuid = div.getGuid();
					}
					DecisionMatrix dm = new DecisionMatrix(selectedGuid);
					decisionRules = dm.getTargetingRules();
					decisionRulesDivision = new HashMap<String, String>();
					for (ProsikaDecisionRule decisonRule : decisionRules)
					{
						decisionRulesDivision.put(decisonRule.getMatrixGuid(), DSDivision.find(decisonRule.getMatrixGuid()).getName());
					}
				}
				else
				{
					DecisionMatrixProcessor dmp = new DecisionMatrixProcessor();
					matrix = dmp.getMatrixList(DSApi.context().getDSUser());
					divisions = new HashMap<String, DSDivision>();
					for (DecisionMatrix mx : matrix)
					{
						divisions.put(mx.getMatrixGuid(), DSDivision.find(mx.getMatrixGuid()));
						if (selectedGuid == null)
							selectedGuid = mx.getMatrixGuid();
					}
					DecisionMatrix dm = new DecisionMatrix(selectedGuid);
					decisionRules = dm.getMatrixRules();
					decisionRulesDivision = new HashMap<String, String>();
					for (ProsikaDecisionRule decisonRule : decisionRules)
					{
						decisionRulesDivision.put(decisonRule.getTargetGuid(), DSDivision.find(decisonRule.getTargetGuid()).getName());
					}
				}				
				dk = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
				
			}
			catch (Exception e)
			{
				addActionError("B��d wczytywania regu� " + e.getMessage());
				log.error("", e);
			}
		}
	}

	public String getValueAsString(String cn, String value)
	{
		try
		{
			return dk.getFieldByCn(cn).getEnumItemByCn(value).getTitle();
		}
		catch (Exception e)
		{}
		return "";
	}

	public String getUserAsString(String username)
	{
		return users.get(username);
	}

	public String getAutoAsString(Boolean isAuto)
	{
		if (isAuto == null || !isAuto)
		{
			return sm.getString("nie");
		}
		else
		{
			return sm.getString("tak");
		}
	}

	public String getGuidName(String guid)
	{
		try
		{
			return decisionRulesDivision.get(guid);
		}
		catch (Exception e)
		{
			log.debug("", e);
		}
		return "";
	}

	public void setSelectedGuid(String selectedGuid)
	{
		this.selectedGuid = selectedGuid;
	}

	public String getSelectedGuid()
	{
		return selectedGuid;
	}

	public void setDivisions(Map<String, DSDivision> divisions)
	{
		this.divisions = divisions;
	}

	public Map<String, DSDivision> getDivisions()
	{
		return divisions;
	}

	public Collection<ProsikaDecisionRule> getDecisionRules()
	{
		return decisionRules;
	}

	public void setDecisionRules(Collection<ProsikaDecisionRule> decisionRules)
	{
		this.decisionRules = decisionRules;
	}

	public void setDk(DocumentKind dk)
	{
		this.dk = dk;
	}

	public DocumentKind getDk()
	{
		return dk;
	}

	public void setToMyDivision(boolean toMyDivision)
	{
		this.toMyDivision = toMyDivision;
	}

	public boolean isToMyDivision()
	{
		return toMyDivision;
	}
}
