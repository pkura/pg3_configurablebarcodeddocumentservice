package pl.compan.docusafe.web.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.fop.apps.Driver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.fop.Fop;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;


/**
 * User: Bartlomiej.Spychalski volvoV70tdi@gmail.com
 * Date: 2006-04-06
 * Time: 13:48:19
 */
public class PrintPortfolioAction extends EventActionSupport {
    private OfficeFolder portfolio;
    private Long id;
    static final Logger log = LoggerFactory.getLogger(PrintPortfolioAction.class);
    String foxsl = "pl/compan/docusafe/web/office/portfolio-fo.xsl";

    private String searchType;
    private String day;
    private String fromDay;
    private String toDay;
    private Integer fromNum;
    private Integer toNum;
    
    private Date _day;
    private Date _fromDay;
    private Date _toDay;
    
    public static final String SEARCH_DAY = "day";
    public static final String SEARCH_DAYRANGE = "dayrange";
    public static final String SEARCH_NUMRANGE = "numrange";
    
    protected void setup() {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doPrint").
        append(OpenHibernateSession.INSTANCE).
        append(new Print()).
        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setSearchType(SEARCH_DAY);
            event.setResult("criteria");
        }
    }
    
    private class Print implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try 
            {
                _fromDay = DateUtils.nullSafeParseJsDate(getFromDay());
                _toDay = DateUtils.nullSafeParseJsDate(getToDay());
                _day = DateUtils.nullSafeParseJsDate(getDay());
                
                portfolio = OfficeFolder.find(id);
                
                if (portfolio != null) 
                {

                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document dom = db.newDocument();
                    Element root = dom.createElement("teczka");
                    dom.appendChild(root);
                    
                    Element element;
                    //rok
                    element = dom.createElement("rok");
                    if (portfolio.getCtime() != null) element.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy").format(portfolio.getCtime())));
                    root.appendChild(element);
                    //referent
                    element = dom.createElement("referent");
                    if(AvailabilityManager.isAvailable("utp.wlacz.wydrukSpisuSpraw")){
                    	DSUser user = DSUser.findByUsername(portfolio.getClerk());
                    	 element.appendChild(dom.createTextNode(user.asFirstnameLastname()));
                    } else {
                    	 element.appendChild(dom.createTextNode(portfolio.getClerk()));	
                    }
                    root.appendChild(element);
                    //symbol
                    element = dom.createElement("symbol");
                    if(AvailabilityManager.isAvailable("utp.wlacz.wydrukSpisuSpraw")){
                    	 element.appendChild(dom.createTextNode(portfolio.getOfficeIdPrefix()));
                    } else {
                    	 element.appendChild(dom.createTextNode(portfolio.getRwa().getRwa()));
                    }
                    root.appendChild(element);
                    //rwa
                    element = dom.createElement("rwa");
                    element.appendChild(dom.createTextNode(portfolio.getRwa().getRwa()));
                    root.appendChild(element);
                    //tytul
                    element = dom.createElement("tytul");
                    element.appendChild(dom.createTextNode(portfolio.getRwa().getDescription()));
                    root.appendChild(element);
                    //sprawy
                    Element sprawa_attr;

                    Integer lp = 0;
                    
                    for (Container container : portfolio.getChildren())
                        if (container != null && container instanceof OfficeCase && fulfillCriteria((OfficeCase)container)) {
                            OfficeCase oc = (OfficeCase) container;

                            element = dom.createElement("sprawa");
                            //l.p.
                            lp++;
                            sprawa_attr = dom.createElement("lp");
                            sprawa_attr.appendChild(dom.createTextNode(lp.toString()));
                            element.appendChild(sprawa_attr);                            
                            
                            //tytul
                            sprawa_attr = dom.createElement("znak_sprawy");
                            sprawa_attr.appendChild(dom.createTextNode(oc.getOfficeId()));
                            element.appendChild(sprawa_attr);
                            
                            //Przeniesienia sprawy do innych teczek
                            sprawa_attr = dom.createElement("przeniesienia_sprawy_do_innych_teczek");
                            String przeniesienia = "";
                            for(Audit audit: oc.getAudit()){
                            	if(audit.getProperty().equals("parent"))
                            		przeniesienia += audit.getDescription()+"; ";
                            }
                            sprawa_attr.appendChild(dom.createTextNode(przeniesienia));
                            element.appendChild(sprawa_attr);
                            
                            if (oc.getDocuments() != null && oc.getDocuments().size() > 0) {
                                OfficeDocument dokument = (OfficeDocument) oc.getDocuments().get(0);
                                
                                List listaDokumentowPrzychodzacych = oc.getDocuments(InOfficeDocument.class);
                                
                                InOfficeDocument dokumentPrzychodzacy = null;
                                
                                if(listaDokumentowPrzychodzacych != null && listaDokumentowPrzychodzacych.size() > 0)
                                	dokumentPrzychodzacy = (InOfficeDocument) listaDokumentowPrzychodzacych.get(0);
                                                             
                                if(dokumentPrzychodzacy != null){
                                	//znak pisma
                                    sprawa_attr = dom.createElement("znak_pisma_przychodzacego");
                                    String number = dokumentPrzychodzacy.getOfficeNumber() != null ? dokumentPrzychodzacy.getOfficeNumber() + " / " : "";
                                    String officeId = oc.getOfficeId();
                                    String officeIdWraped = "";
                                    for(int i = 0 ; i < officeId.length() ; i++)
                                    {
                                        officeIdWraped = officeIdWraped + officeId.charAt(i) + "\u200B";
                                    }
                                    sprawa_attr.appendChild(dom.createTextNode(number+officeIdWraped));
                                    element.appendChild(sprawa_attr);
                                    
                                    //z dnia
                                    sprawa_attr = dom.createElement("z_dnia_przychodzacego");
                                    if (dokument.getCtime() != null)
                                        sprawa_attr.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy-MM-dd").format(dokumentPrzychodzacy.getCtime())));
                                    element.appendChild(sprawa_attr);
                                    
                                    //nadawca pisma
                                    sprawa_attr = dom.createElement("nadawca_przychodzace");
                                    Sender sender = ((InOfficeDocument) dokumentPrzychodzacy).getSender();
                                    String nadawca = (sender == null ? "brak nadawcy" : sender.getShortSummary());

                                    sprawa_attr.appendChild(dom.createTextNode(nadawca));
                                    
                                    element.appendChild(sprawa_attr);
                                    
                                    if (dokument != null && dokumentPrzychodzacy.getId()==dokument.getId()) {
                                    	sprawa_attr = dom.createElement("od_kogo");
                                    	sprawa_attr.appendChild(dom.createTextNode("1"));                                  	
                                    }
                                    else{
                                    	sprawa_attr = dom.createElement("od_kogo");
                                    	sprawa_attr.appendChild(dom.createTextNode("0"));
                                    }
                                    
                                    element.appendChild(sprawa_attr);
                                }
                                else{
                                	sprawa_attr = dom.createElement("od_kogo");
                                	sprawa_attr.appendChild(dom.createTextNode("0"));
                                	
                                	element.appendChild(sprawa_attr);
                                }
                                                            
                                if (dokument != null) {
                                    
                                    // numer
                                    sprawa_attr = dom.createElement("numer_sprawy");
                                    String desc = dokument.getSummary() != null ? dokument.getSummary() + " / ": "";
                                    if (oc.getDescription() != null)
                                        desc += oc.getDescription();
                                    sprawa_attr.appendChild(dom.createTextNode(desc));
                                    element.appendChild(sprawa_attr);
                                    
                                    //tresc - CHYBA NIEUZYWANE
                                    sprawa_attr = dom.createElement("tresc");
                                    
                                    sprawa_attr.appendChild(dom.createTextNode(dokument.getSummary()));
                                    element.appendChild(sprawa_attr);
                                    //nadawca pisma
                                    sprawa_attr = dom.createElement("nadawca");
                                    Sender sender = null;
                                    if (dokument instanceof InOfficeDocument)
                                        sender = ((InOfficeDocument) dokument).getSender();
                                    else if (dokument instanceof OutOfficeDocument)
                                        sender = ((OutOfficeDocument) dokument).getSender();
                                    String nadawca = (sender == null ? "brak nadawcy" : sender.getShortSummary());//(dokument.getSource()==null)?"brak nadawcy":dokument.getSource();
                                        
                                    
                                    sprawa_attr.appendChild(dom.createTextNode(nadawca));
                                    element.appendChild(sprawa_attr);
                                    //znak pisma
                                    sprawa_attr = dom.createElement("znak_pisma");
                                    String number = dokument.getOfficeNumber() != null ? dokument.getOfficeNumber() + " / " : "";
                                    String officeId = oc.getOfficeId();
                                    String officeIdWraped = "";
                                    for(int i = 0 ; i < officeId.length() ; i++)
                                    {
                                        officeIdWraped = officeIdWraped + officeId.charAt(i) + "\u200B";
                                    }
                                    sprawa_attr.appendChild(dom.createTextNode(number+officeIdWraped));
                                    element.appendChild(sprawa_attr);
                                    //z dnia
                                    sprawa_attr = dom.createElement("z_dnia");
                                    if (dokument.getCtime() != null)
                                        sprawa_attr.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy-MM-dd").format(dokument.getCtime())));
                                    element.appendChild(sprawa_attr);
                                }
                                
                            }
                            
                            //data za�o�enia sprawy
                            sprawa_attr = dom.createElement("data_wszczecia_sprawy");
                            if (oc.getOpenDate() != null)
                                sprawa_attr.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy-MM-dd").format(oc.getOpenDate())));                           
                            element.appendChild(sprawa_attr);
                            
                            //data zakonczenia
                            sprawa_attr = dom.createElement("data_zakonczenia_sprawy");
                            if(oc.isClosed() && oc.getFinishDate()!=null)
                            	sprawa_attr.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy-MM-dd").format(oc.getFinishDate())));
                            else
                            	sprawa_attr.appendChild(dom.createTextNode(""));
                            element.appendChild(sprawa_attr);
                            
                            //data ostatecznego zalatwienia
                            sprawa_attr = dom.createElement("data_zalatwienia");
                            if (oc.getFinishDate() != null) {
                                sprawa_attr.appendChild(dom.createTextNode(new SimpleDateFormat("yyyy-MM-dd").format(oc.getFinishDate())));
                            }
                            element.appendChild(sprawa_attr);
                            
                            //uwagi
                            sprawa_attr = dom.createElement("uwagi");
                            List<Remark> remarks = (List<Remark>)oc.getRemarks();
                            
                            String summaryRemarks = "";
                            
                            if (remarks!=null){
	                            for(Remark remark : remarks){
	                            	summaryRemarks += "["+ remark.getAuthor()+"] "+remark.getContent();
	                            }
                            }
                            StringBuilder sb = new StringBuilder();
                            String[] words = summaryRemarks.split(" ");
                            
                            for (String test : words) {
                        		if(test.length()>15 ){
                        			test = test.substring(0,15)+ " "+test.substring(15, test.length()); 
                        		}
                        		sb.append(test+ " " );
                            }
                            sprawa_attr.appendChild(dom.createTextNode(sb.toString()));
                            element.appendChild(sprawa_attr);

                            root.appendChild(element);
                        }

                    if (lp == 0) {
                       
                        /* jak nie ma spraw to linijka informujaca o tym */
                        element = dom.createElement("sprawa");
                    /*    //l.p.
                        sprawa_attr = dom.createElement("lp");
                        sprawa_attr.appendChild(dom.createTextNode(lp.toString()));
                        element.appendChild(sprawa_attr);
                        //numer_sprawy
                        sprawa_attr = dom.createElement("numer_sprawy");
                        sprawa_attr.appendChild(dom.createTextNode(lp.toString()));
                        element.appendChild(sprawa_attr);*/
                        //uwagi
                        sprawa_attr = dom.createElement("uwagi");
                        sprawa_attr.appendChild(dom.createTextNode("Brak spraw"));
                        element.appendChild(sprawa_attr);

                        root.appendChild(element);
                    }
                    
                    String str = "Wydruk sporz�dzony przez: "+
                        DSApi.context().getDSUser().asFirstnameLastname()+
                        " w dniu "+DateUtils.formatCommonDateTime(new Date());
                                       
                    Element stopka = dom.createElement("stopka");
                    element = dom.createElement("nadawca");
                    element.appendChild(dom.createTextNode(str));
                    stopka.appendChild(element);
                    root.appendChild(stopka);
                    
                    Driver driver = new Driver();
                    try 
                    {
                        driver.setRenderer(Driver.RENDER_PDF);
                        File temp;
                        temp = File.createTempFile("docusafe_report_", "_tmp");
                        //temp = new File("c:/ttt.xml");

                        writeAsXml(dom, new FileOutputStream(temp));

                        File pdf = File.createTempFile("docusafe_report_pdf", ".pdf");
                        OutputStream os = new FileOutputStream(pdf);
                        
                        InputStream xsl = null;
                        
                        if(Docusafe.getAdditionProperty("printportfolio.custom_template")!=null){
                        	foxsl = Docusafe.getHome().getAbsolutePath()+"/templates/"+Docusafe.getAdditionProperty("printportfolio.custom_template");
                        	xsl = new FileInputStream(foxsl);
                        }
                        else{
                        	xsl = PrintPortfolioAction.class.getClassLoader().getResourceAsStream(foxsl);
                        }
                        
                        if (xsl == null) 
                        {
                            log.error("Nie mozna znalezc zasobu "+foxsl);
                            throw new EdmException("Nie mo�na znale�� zasobu "+foxsl);
                        }
                        if (temp == null) 
                        {
                            log.error("Blad pobrania raportu do xml");
                            throw new EdmException("Bad pobrania raportu do xml");
                        }
                        Fop.renderPdf(new FileInputStream(temp), xsl, os);
                        if (pdf != null && pdf.exists())
                        {
                            if (event.getLog().isDebugEnabled())
                                event.getLog().debug("rozmiar pliku "+pdf+": "+pdf.length());
                            try
                            {
                                ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdf),
                                    "application/pdf", (int) pdf.length());
                            }
                            catch (IOException e)
                            {
                                event.getLog().error("", e);
                            }
                            finally
                            {
                                pdf.delete();
                            }
                        }
                    }   
                    catch(Exception e) 
                    {
                        log.error("Blad utworzenia pliku pdf", e);
                    }

                }


            } catch (EdmException e) {
                log.error("Problem z pobraniem danych");
            } catch (ParserConfigurationException e) {
                log.error(e.getMessage());
            }

            }
        }

    private boolean fulfillCriteria(OfficeCase oc)
    {
        if (SEARCH_DAY.equals(getSearchType()))
        {
            if (_day != null && !oc.getOpenDate().equals(_day))
                return false;
        }
        else if (SEARCH_DAYRANGE.equals(getSearchType()))
        {
            if (_fromDay != null && oc.getOpenDate().compareTo(_fromDay) < 0)
                return false;
            if (_toDay != null && oc.getOpenDate().compareTo(_toDay) > 0)
                return false;
        }
        else if (SEARCH_NUMRANGE.equals(getSearchType()))
        {
            if (fromNum != null && oc.getSequenceId() < fromNum)
                return false;
            if (toNum != null && oc.getSequenceId() > toNum)
                return false;
        }
        return true;
    }
    
    public static void writeAsXml(Document dom, FileOutputStream fos) {
        Source source = new DOMSource(dom);
        Result result = new StreamResult(fos);
        Transformer transformer;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        } catch (TransformerException e) {
            log.error("Blad zapisu pliku xml z DOM");
             log.error(e.getMessage());
        }
    }


    public OfficeFolder getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(OfficeFolder portfolio) {
        this.portfolio = portfolio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getSearchType()
    {
        return searchType;
    }

    public void setSearchType(String searchType)
    {
        this.searchType = searchType;
    }
    
    public String getDay()
    {
        return day;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

    public String getFromDay()
    {
        return fromDay;
    }

    public void setFromDay(String fromDay)
    {
        this.fromDay = fromDay;
    }

    public String getToDay()
    {
        return toDay;
    }

    public void setToDay(String toDay)
    {
        this.toDay = toDay;
    }

    public Integer getFromNum()
    {
        return fromNum;
    }

    public void setFromNum(Integer fromNum)
    {
        this.fromNum = fromNum;
    }

    public Integer getToNum()
    {
        return toNum;
    }

    public void setToNum(Integer toNum)
    {
        this.toNum = toNum;
    }

}
