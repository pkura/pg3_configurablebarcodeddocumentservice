package pl.compan.docusafe.web.office;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;
import std.filter;
import std.fun;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditRolePermissionsAction.java,v 1.14 2009/09/23 12:07:12 pecet1 Exp $
 */
public class EditRolePermissionsAction extends EventActionSupport
{
    // @EXPORT/@IMPORT
    private Long id;
    private String roleName;

    // @EXPORT
    private List categories;
    private List freePermissions;
    private List boundPermissions;
    private List zamowienie;

    // @IMPORT
    private String[] selectedPermissions;
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("roles"));
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {  
            try
            {
                categories = fun.filter(DSPermission.categories(),
                    new filter<DSPermission.Category>()
                    {
                        public boolean accept(DSPermission.Category cat)
                        {
                            return cat.permissions().size() > 0;
                        }
                    });

                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("categories = "+categories);

                Role role = Role.find(id);

                roleName = role.getName();

                List allPermissions = DSPermission.getAvailablePermissions();
                List rolePermissions = role.getPermissionsObjects();

                if (event.getLog().isDebugEnabled())
                {
                    event.getLog().debug("allPermissions="+allPermissions);
                    event.getLog().debug("rolePermissions="+rolePermissions);
                }

                boundPermissions = new ArrayList(allPermissions);
                boundPermissions.retainAll(rolePermissions);

                freePermissions = new ArrayList(allPermissions);
                freePermissions.removeAll(role.getPermissionsObjects());

                if (event.getLog().isDebugEnabled())
                {
                    event.getLog().debug("boundPermissions="+boundPermissions);
                    event.getLog().debug("freePermissions="+freePermissions);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(roleName))
                addActionError(sm.getString("NiePodanoNazwyRoli"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Role role = Role.find(id);

                role.setName(roleName);

				Set<String> oldPermissions = new HashSet<String>(role.getPermissions());
                role.clearPermissions();

				DataMartEventBuilder.get()
					.defaultObjectId(role.getId());

                if (selectedPermissions != null)
                {
                    List allPermissions = DSPermission.getAvailablePermissions();
                    for (int i=0; i < selectedPermissions.length; i++)
                    {
                        DSPermission perm = DSPermission.getByName(selectedPermissions[i]);
                        if (allPermissions.contains(perm)){
							if(!oldPermissions.contains(perm.getName())){
								DataMartEventBuilder.get()
									.event(EventType.ROLE_PERMISSION_ADD)
									.valueChange(null, null, perm.getName());
							}
                            role.addPermission(perm);
						}
                    }
					oldPermissions.removeAll(role.getPermissions());
					
					for(String removedPermission: oldPermissions){
						DataMartEventBuilder.get()
							.event(EventType.ROLE_PERMISSION_REMOVE)
							.valueChange(null, removedPermission, null);
					}
                }

                PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                cache.reload(role);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getCategories()
    {
        return categories;
    }

    public List getFreePermissions()
    {
        return freePermissions;
    }

    public List getBoundPermissions()
    {
        return boundPermissions;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRoleName()
    {
        return roleName;
    }

    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }

    public void setSelectedPermissions(String[] selectedPermissions)
    {
        this.selectedPermissions = selectedPermissions;
    }

    public List getZamowienie()
    {
        return zamowienie;
    }

    public void setZamowienie(List zamowienie)
    {
        this.zamowienie = zamowienie;
    }
}
