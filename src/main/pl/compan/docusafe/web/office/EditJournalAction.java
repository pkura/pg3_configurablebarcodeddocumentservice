package pl.compan.docusafe.web.office;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditJournalAction.java,v 1.11 2007/11/27 12:11:28 mariuszk Exp $
 */
public class EditJournalAction extends EventActionSupport
{
    // @EXPORT
    private Journal journal;
    private DSDivision division;
    private String author;
    private boolean canSetSequenceId;

    // @EXPORT/@IMPORT
    private Long id;
    private String description;
    private String symbol;
    // tylko do zapewnienia powrotu do w�a�ciwego miejsca w drzewie
    // w division-journals.jsp, warto�� nie jest zapisywana w obiekcie dziennika
    private String divisionGuid;
    private Integer sequenceId;
    
    private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(CurrentYearAction.class.getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                journal = Journal.find(id);
                division = DSDivision.find(divisionGuid);
                division.getPrettyPath(); // hibernate - lazy initialize
                canSetSequenceId = DSApi.context().hasPermission(DSPermission.DZIENNIK_ZMIANA_NUMERU_KOLEJNEGO);
                author = DSUser.safeToFirstnameLastname(journal.getAuthor());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		if (StringUtils.isEmpty(description))
    			addActionError(sm.getString("NiePodanoTytuluDziennika"));
    		
    		if (sequenceId == null)
    			addActionError(sm.getString("brakNumeruKolejnegoPisma"));
    			
    		if (hasActionErrors())
    			return;

    		try
    		{
    			DSApi.context().begin();

    			Journal j = Journal.find(id);

    			if (!j.isMain())
    			{
    				if (StringUtils.isEmpty(symbol))
    					addActionError(sm.getString("NiePodanoSymboluDziennika"));
    				if (hasActionErrors())
    					return;
    				if (!j.getDescription().equals(description) &&
    						Journal.findByDivisionGuidAndDescription(j.getOwnerGuid(), description).size() > 0)
    					throw new EdmException(sm.getString("WtymDzialeIstniejeJuzDziennikOnazwie",description));
    				if(!j.getSymbol().equals(TextUtils.trimmedStringOrNull(symbol))&&Journal.findByDivisionGuidAndSymbol(j.getOwnerGuid(), TextUtils.trimmedStringOrNull(symbol)).size()>0){
    					throw new EdmException(sm.getString("WtymDzialeIstniejeJuzDziennikOsymbolu",symbol));
    				}
    			
    				j.setSymbol(TextUtils.trimmedStringOrNull(symbol));
    				j.setDescription(description.trim());
    			}

    			if (sequenceId != null)
    				j.setSequenceId(sequenceId);

    			DSApi.context().commit();
    			event.setResult("list");
    		}
    		catch (EdmException e)
    		{
    			DSApi.context().setRollbackOnly();
    			addActionError(e.getMessage());
    		}
    	}
    }

    public Journal getJournal()
    {
        return journal;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public DSDivision getDivision()
    {
        return division;
    }

    public String getAuthor()
    {
        return author;
    }

    public boolean isCanSetSequenceId()
    {
        return canSetSequenceId;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }
}
