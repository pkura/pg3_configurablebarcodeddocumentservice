package pl.compan.docusafe.web.office;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowManager;
import pl.compan.docusafe.util.StringManager;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.HibernateException;
/* User: Administrator, Date: 2006-09-15 11:59:01 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class CloneDocumentManager
{
    public static void clone(Long documentId) throws EdmException
    {

        Document document = Document.find(documentId);
        
        List<Long> attachmentsToClone = new ArrayList<Long>(document.getAttachments().size());
        List<Attachment> attachments = document.getAttachments();
        for (int i=0; i < attachments.size(); i++)
        {
            Attachment attachment = (Attachment) attachments.get(i);
            attachmentsToClone.add(attachment.getId());
        }

        if (document instanceof OfficeDocument)
        {
            StringManager sm =
                GlobalPreferences.loadPropertiesFile(CloneDocumentManager.class.getPackage().getName(),null);
            ((OfficeDocument) document).addWorkHistoryEntry(Audit.create("::nw_clone", DSApi.context().getPrincipalName(),
                sm.getString("DokumentZostalSklonowany")));
        }

        Document newDocument = document.cloneObject((Long[]) attachmentsToClone.toArray(new Long[attachmentsToClone.size()]));

        /* na wypadek gdyby oryginalny dokument by� podpisany podpisem elektronicznym */
        newDocument.setBlocked(false);

        if (document instanceof OfficeDocument)
        {
            OfficeDocument newOfficeDocument = (OfficeDocument) newDocument;
            OfficeDocument officeDocument = (OfficeDocument) document;

            if (officeDocument.getContainingCase() != null)
            {
                newOfficeDocument.removeFromCase();

                if (newOfficeDocument.getType() == DocumentType.INTERNAL)
                {
                    ((OutOfficeDocument) newOfficeDocument).setReferenceId(officeDocument.getCaseDocumentId());
                }
                else if (newOfficeDocument.getType() == DocumentType.INCOMING)
                {
                    ((InOfficeDocument) newOfficeDocument).setReferenceId(officeDocument.getCaseDocumentId());
                }
            }

            // tworzenie i uruchamianie wewn�trznego procesu workflow
            WorkflowFactory.createNewProcess(newOfficeDocument, false, "Pismo przyj�te");

            try
            {
                DSApi.context().session().flush();
            }
            catch (HibernateException e)
            {
                throw new EdmException(e);
            }

        }
    }
}
