package pl.compan.docusafe.web.office;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.ChannelRequestBean;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.ChannelRequestBean.ChannelRequestStatus;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ChannelsRequestAction extends EventActionSupport {
	
	private List<ChannelRequestBean> requestBeans;
	private String newRequestContent;
	private Boolean channelAdmin;
	private String operation;
	private Long requestToProcess;
	private String comment;
	
	protected void setup() {
		 FillForm fillForm = new FillForm();

	        registerListener(DEFAULT_ACTION).
	            append(OpenHibernateSession.INSTANCE).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);
	        
	        registerListener("doSubmit").
            append(OpenHibernateSession.INSTANCE).
            append(new Submit()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
	        
	        registerListener("doProcess").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new Process()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	
	private class FillForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			try {
				setRequestBeans(ChannelRequestBean.getChannelRequestBeans());
				Collections.reverse(getRequestBeans());
				channelAdmin = DSApi.context().hasPermission(DSPermission.KANALY_MODYFIKACJA);
			} catch (EdmException e) {
				addActionError(e.getMessage());
			}			
		}
		
	}
	
	private class Submit implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			DSContext ctx = DSApi.context();
			try {
				ctx.begin();
				ChannelRequestBean.create(newRequestContent);
				ctx.commit();
			} catch (EdmException e) {
				ctx._rollback();
				addActionError(e.getMessage());
			}
		}
	}
	
	private class Process implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			DSContext ctx = DSApi.context();
			try {
				ctx.begin();
				for (ChannelRequestBean bean : getRequestBeans()) {
					if (bean.getId().equals(requestToProcess)) {
						if (operation.equals("process")) {
							bean.process(comment);
						} else if (operation.equals("invalid")) {
							bean.markInvalid(comment);
						}
					}
				}
				ctx.commit();
			} catch (EdmException e) {
				ctx._rollback();
				addActionError(e.getMessage());
			}
		}
	}
	


	public void setNewRequestContent(String newRequestContent) {
		this.newRequestContent = newRequestContent;
	}

	public String getNewRequestContent() {
		return newRequestContent;
	}

	public void setChannelAdmin(Boolean channelAdmin) {
		this.channelAdmin = channelAdmin;
	}

	public Boolean getChannelAdmin() {
		return channelAdmin;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getOperation() {
		return operation;
	}

	public void setRequestToProcess(Long requestToProcess) {
		this.requestToProcess = requestToProcess;
	}

	public Long getRequestToProcess() {
		return requestToProcess;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setRequestBeans(List<ChannelRequestBean> requestBeans) {
		this.requestBeans = requestBeans;
	}

	public List<ChannelRequestBean> getRequestBeans() {
		return requestBeans;
	}

}
