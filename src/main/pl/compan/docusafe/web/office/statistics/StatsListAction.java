package pl.compan.docusafe.web.office.statistics;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class StatsListAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	Logger log = LoggerFactory.getLogger(StatsListAction.class);
	StringManager sm = StringManager.getManager(this.getClass().getPackage().getName());
	private List<StatBean> stats;
	
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
		
        public void actionPerformed(ActionEvent event)
        {
        	prepareStats();
        }

    }
	
	private void prepareStats()
	{
		stats = new ArrayList<StatBean>();
		stats.add(new StatBean(sm.getString("StatystykaNiezaakceptowanychDokumentowWKolejckachDesc"),sm.getString("StatystykaNiezaakceptowanychDokumentowWKolejckach"), "queue-stats.action"));
		stats.add(new StatBean(sm.getString("MojaStatystykaDesc"),sm.getString("MojaStatystyka"), "my-stats.action"));
		stats.add(new StatBean(sm.getString("StatystykaListZadanDesc"), sm.getString("StatystykaListZadan"), "tasklist-stats.action"));
	}
	
	public class StatBean
	{
		private String title;
		private String desc;
		private String link;
		
		public StatBean(String desc, String title, String link)
		{
			this.desc = desc;
			this.title = title;
			this.link = link;
		}
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		
		
	}

	public List<StatBean> getStats() {
		return stats;
	}

	public void setStats(List<StatBean> stats) {
		this.stats = stats;
	}
	
}
