package pl.compan.docusafe.web.office.statistics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.Collections;

public class QueueReportAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	Logger log = LoggerFactory.getLogger(QueueReportAction.class);
	List<QueueReportEntry> list;
	private String sortField;
	private Boolean ascending;
	private Map<String, String> availableAspects;
	private Map<Integer, String> availableTypes;
	private Map<Integer, String> availableSegments;
	private StringManager sm = StringManager.getManager(QueueReportAction.class.getPackage().getName());
	private String[] selectedAspects;
	private Integer[] selectedTypes;
	private Integer[] selectedSegments;
	private QueueReportEntry tot;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
		
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		
        		availableTypes = new LinkedHashMap<Integer, String>();
        		availableSegments = new LinkedHashMap<Integer, String>();
        		availableAspects = new HashMap<String, String>();
        		
        		//selectedAspects = (String[])ServletActionContext.getRequest().getSession().getAttribute("selectedAspects");
        		if(ServletActionContext.getRequest().getSession().getAttribute("selectedTypes")!=null)
        			selectedTypes = (Integer[])ServletActionContext.getRequest().getSession().getAttribute("selectedTypes");
        		if(ServletActionContext.getRequest().getSession().getAttribute("selectedSegments")!=null)
        			selectedSegments = (Integer[])ServletActionContext.getRequest().getSession().getAttribute("selectedSegments");
        		
        		if(selectedAspects == null) selectedAspects = new String[0];
        		if(selectedTypes == null) selectedTypes = new Integer[0];
        		if(selectedSegments == null) selectedSegments = new Integer[0];
        		
        		DocumentKind act = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
        		List<Aspect> asps = act.getAspectsForUser(DSApi.context().getPrincipalName());
        		List<EnumItem> types = new ArrayList<EnumItem>();
        		List<EnumItem> segments  = new ArrayList<EnumItem>();
	    		
        		
	    		for(Aspect a:asps)
	    		{
	    			availableAspects.put(a.getCn(), a.getName());
	    			//System.out.println(a.getName());
	    		}
	    		
	    		for(String a:selectedAspects)
	        	{
	    			if(act.getDockindInfo().getAspectsMap().get(a).getEnumItems().get(Prosikalogic.SEGMENT_FIELD_CN)!=null)
	    			{
		        		for(EnumItem ei: act.getDockindInfo().getAspectsMap().get(a).getEnumItems().get(Prosikalogic.SEGMENT_FIELD_CN))
		        		{
		        			//System.out.println(ei.getId());
		        			if(!segments.contains(ei))
		        				segments.add(ei);
		        		}
	    			}
	    			else
	    				segments = act.getFieldByCn(Prosikalogic.TYP_DOKUMENTU_FIELD_CN).getEnumItems();
	    			
	    			
	        		if(act.getDockindInfo().getAspectsMap().get(a).getEnumItems().get(Prosikalogic.TYP_DOKUMENTU_FIELD_CN)!=null)
	        		{
		        		for(EnumItem ei: act.getDockindInfo().getAspectsMap().get(a).getEnumItems().get(Prosikalogic.TYP_DOKUMENTU_FIELD_CN))
		        		{
		        			//System.out.println(ei.getId());
		        			if(!segments.contains(ei))
		        				types.add(ei);
		        		}
	        		}
	        		else
	        			types = act.getFieldByCn(Prosikalogic.TYP_DOKUMENTU_FIELD_CN).getEnumItems();
	        	}
	    		
	    		
	    		
	    		//segments.addAll(act.getFieldByCn(Prosikalogic.SEGMENT_FIELD_CN).getEnumItems());
	    		Collections.sort(segments, new EnumItem.EnumItemComparator());
	    		//types.addAll(act.getFieldByCn(Prosikalogic.TYP_DOKUMENTU_FIELD_CN).getEnumItems());
	    		Collections.sort(types, new EnumItem.EnumItemComparator());
	    		
	    		for(EnumItem ei : types)
	    		{
	    			availableTypes.put(ei.getId(), ei.getTitle());
	    		}
	    		
	    		
	    		for(EnumItem ei : segments)
	    		{
	    			availableSegments.put(ei.getId(), ei.getTitle());
	    		}
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        		
        		
        }
    }
	
	private class Update implements ActionListener
    {
		
        public void actionPerformed(ActionEvent event)
        {
        	Long total = 0L;
        	Long total24 = 0L;
        	Long total2472 = 0L;
        	Long total72 = 0L;
        	tot = new QueueReportEntry();
        	
        	if(selectedSegments==null && selectedTypes==null)
        	{
        		selectedAspects = (String[])ServletActionContext.getRequest().getSession().getAttribute("selectedAspects");
        		selectedTypes = (Integer[])ServletActionContext.getRequest().getSession().getAttribute("selectedTypes");
        		selectedSegments = (Integer[])ServletActionContext.getRequest().getSession().getAttribute("selectedSegments");
        	}
        	
        	if(selectedSegments!=null && selectedTypes!=null)
        	{
        		ServletActionContext.getRequest().getSession().setAttribute("selectedAspects", selectedAspects);
        		ServletActionContext.getRequest().getSession().setAttribute("selectedTypes", selectedTypes);
        		ServletActionContext.getRequest().getSession().setAttribute("selectedSegments", selectedSegments);
	        	//List<Integer> segments = new ArrayList<Integer>();

        		list = new ArrayList<QueueReportEntry>();
        		QueueReportEntry qre;
	        	ResultSet rs;
	        	PreparedStatement ps = null;
	        	try
	        	{
	        		/*for(String a:selectedAspects)
		        	{
		        		DocumentKind act = DocumentKind.findByCn(DocumentKind.PROSIKA);
		        		for(EnumItem ei: act.getDockindInfo().getAspectsMap().get(a).getEnumItems().get(Prosikalogic.SEGMENT_FIELD_CN))
		        		{
		        			//System.out.println(ei.getId());
		        			segments.add(ei.getId());
		        		}
		        	}*/
	        		//DSApi.context().begin();
	        		//String sql = "execute prosika_report";
	        		/*ps = DSApi.context().session().connection().prepareStatement(sql);
	        		ps.execute();
	        		ps.close();*/
	        		
	        		//for(int i=0;i<segments.size();i++)
	        		//{
	        			for(int j=0;j <selectedTypes.length;j++)
	        			{
	        				
	        				qre = new QueueReportEntry();
		        			//qre.setSegmentCode(segments.get(i));
		        			qre.setTypeCode(selectedTypes[j]);
		        			/*try
    	        			{
    	        				qre.setSegment(DocumentKind.findByCn(DocumentKind.PROSIKA).getFieldByCn(Prosikalogic.SEGMENT_FIELD_CN).getEnumItem(segments.get(i)).getTitle());
    	        			}
    	        			catch(EntityNotFoundException f)
    	        			{
    	        				qre.setSegment("Nie zdefiniowany");
    	        			}*/
    	        			try
    	        			{
    	        				qre.setType(DocumentKind.findByCn(DocumentLogicLoader.PROSIKA).getFieldByCn(Prosikalogic.TYP_DOKUMENTU_FIELD_CN).getEnumItem(selectedTypes[j]).getTitle());
    	        			}
    	        			catch(EntityNotFoundException f)
    	        			{
    	        				qre.setType("Nie zdefiniowany");
    	        			}
		        			qre.setLess24(0L);
    	        			qre.setBetween24and72(0L);
    	        			qre.setMore72(0L);
    	        			qre.setTotal(0L);
		        			list.add(qre);
	        			}
	        		//}
	        		
	        		StringBuffer sql = new StringBuffer();
	        		sql.append("select type, sum(lessthanday) lessthanday, sum(onetothreedays) onetothreedays, sum(morethanthreedays) morethanthreedays, sum(total) total from dsg_prosika_queue_report where segment in (");
	        		for(int i=0;i<selectedSegments.length;i++)
	        		{
	        			if(i!=0) 
	        				sql.append(",");
	        			sql.append("?");
	        		}
	        		sql.append(") and type in (");
	        		
        			for(int i=0;i<selectedTypes.length;i++)
	        		{
	        			if(i!=0) 
	        				sql.append(",");
	        			sql.append("?");
	        		}
        			sql.append(") ");
        			sql.append(" group by type ");
	        		
	        		ps = DSApi.context().prepareStatement(sql.toString());
	        		for(int i=0;i<selectedSegments.length;i++)
	        		{
	        			ps.setInt(i+1, selectedSegments[i]);
	        		}
	        		for(int i=0;i<selectedTypes.length;i++)
	        		{
	        			ps.setInt(selectedSegments.length+i+1, selectedTypes[i]);
	        		}
	        		rs = ps.executeQuery();

	        		while(rs.next())
	        		{
	        			Integer typeCode = rs.getInt("type");
	        			//Integer segmentCode = rs.getInt("segment");
	        			//System.out.println("result");
	        			for(QueueReportEntry q:list)
	        			{
	        				if(q.getTypeCode().equals(typeCode)/*&&q.getSegmentCode().equals(segmentCode)*/)
	        				{
	    	        			q.setLess24(rs.getLong("lessthanday"));
	    	        			q.setBetween24and72(rs.getLong("onetothreedays"));
	    	        			q.setMore72(rs.getLong("morethanthreedays"));
	    	        			q.setTotal(rs.getLong("total"));
	    	        			total+=q.getTotal();
	    	        			total24+=q.getLess24();
	    	        			total2472+=q.getBetween24and72();
	    	        			total72+=q.getMore72();
	        				}
	        			}
	        		}
	        		final Collator collator = Collator.getInstance();
        		    collator.setStrength(Collator.SECONDARY);
	        		if(sortField!=null && sortField.equals("type"))
	        		{
		        		Collections.sort(list, new Comparator<QueueReportEntry>()
		        				{
									public int compare(QueueReportEntry o1,QueueReportEntry o2) 
									{ 
										if(ascending==true)
											return collator.compare(o1.getType(),o2.getType());
										else
											return collator.compare(o2.getType(),o1.getType());
									}
		        				}
		        		);
	        		}
	        		else if(sortField!=null && sortField.equals("lessthanday"))
	        		{
	        			Collections.sort(list, new Comparator<QueueReportEntry>()
		        				{
									public int compare(QueueReportEntry o1,QueueReportEntry o2) 
									{ 
										if(ascending==true)
											return o1.less24.compareTo(o2.less24);
										else
											return o2.less24.compareTo(o1.less24);
									}
		        				}
		        		);
	        		}
	        		else if(sortField!=null && sortField.equals("onetothreedays"))
	        		{
	        			Collections.sort(list, new Comparator<QueueReportEntry>()
		        				{
									public int compare(QueueReportEntry o1,QueueReportEntry o2) 
									{ 
										if(ascending==true)
											return o1.between24and72.compareTo(o2.between24and72);
										else
											return o2.between24and72.compareTo(o1.between24and72);
									}
		        				}
		        		);
	        		}
	        		else if(sortField!=null && sortField.equals("morethanthreedays"))
	        		{
	        			Collections.sort(list, new Comparator<QueueReportEntry>()
		        				{
									public int compare(QueueReportEntry o1,QueueReportEntry o2) 
									{ 
										if(ascending==true)
											return o1.more72.compareTo(o2.more72);
										else
											return o2.more72.compareTo(o1.more72);
									}
		        				}
		        		);
	        		}
	        		else if(sortField!=null && sortField.equals("total"))
	        		{
	        			Collections.sort(list, new Comparator<QueueReportEntry>()
		        				{
									public int compare(QueueReportEntry o1,QueueReportEntry o2) 
									{ 
										if(ascending==true)
											return o1.total.compareTo(o2.total);
										else
											return o2.total.compareTo(o1.total);
									}
		        				}
		        		);
	        		}
	        		rs.close();
	        		ps.close();
	        		tot.setLess24(total24);
	        		tot.setBetween24and72(total2472);
	        		tot.setMore72(total72);
	        		tot.setTotal(total);
	        	  		
	        	}
	        	catch(EdmException e)
	        	{
	        		addActionError(e.getMessage());
	        		log.error("",e);
	        	}
	        	catch(SQLException e)
	        	{
	        		addActionError(e.getMessage());
	        		log.error("",e);
	        	}
	        	finally
	        	{
	        		DSApi.context().closeStatement(ps);
	        	}
        	}
        	else
        	{
        		addActionMessage(sm.getString("OkreslKryteria"));
        	}
        
        }
    }
	public class QueueReportEntry
	{
		private Integer segmentCode;
		private Integer typeCode;
		private String segment;
		private String type;
		private Long less24;
		private Long between24and72;
		private Long more72;
		private Long total;
		public String getSegment() {
			return segment;
		}
		public void setSegment(String segment) {
			this.segment = segment;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public Long getLess24() {
			return less24;
		}
		public void setLess24(Long less24) {
			this.less24 = less24;
		}
		public Long getBetween24and72() {
			return between24and72;
		}
		public void setBetween24and72(Long between24and72) {
			this.between24and72 = between24and72;
		}
		public Long getMore72() {
			return more72;
		}
		public void setMore72(Long more72) {
			this.more72 = more72;
		}
		public Long getTotal() {
			return total;
		}
		public void setTotal(Long total) {
			this.total = total;
		}
		
		public Integer getSegmentCode() {
			return segmentCode;
		}
		public void setSegmentCode(Integer segmentCode) {
			this.segmentCode = segmentCode;
		}
		public Integer getTypeCode() {
			return typeCode;
		}
		public void setTypeCode(Integer typeCode) {
			this.typeCode = typeCode;
		}
		public boolean equals(Object o)
		{
			if(!(o instanceof QueueReportEntry) || o==null)
			{
				return false;
			}
			
			QueueReportEntry qre = (QueueReportEntry)o;
			if(qre.getSegment().equals(this.getSegment()) && qre.getType().equals(this.getType())) return true;
			return false;
			
		}
		
	}
	public List<QueueReportEntry> getList() {
		return list;
	}
	public void setList(List<QueueReportEntry> list) {
		this.list = list;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public Boolean getAscending() {
		return ascending;
	}
	public void setAscending(Boolean ascending) {
		this.ascending = ascending;
	}
	public Map<String, String> getAvailableAspects() {
		return availableAspects;
	}
	public void setAvailableAspects(Map<String, String> availableAspects) {
		this.availableAspects = availableAspects;
	}
	public String[] getSelectedAspects() {
		return selectedAspects;
	}
	public void setSelectedAspects(String[] selectedAspects) {
		this.selectedAspects = selectedAspects;
	}
	public Map<Integer, String> getAvailableTypes() {
		return availableTypes;
	}
	public void setAvailableTypes(Map<Integer, String> availableTypes) {
		this.availableTypes = availableTypes;
	}
	public Integer[] getSelectedTypes() {
		return selectedTypes;
	}
	public void setSelectedTypes(Integer[] selectedTypes) {
		this.selectedTypes = selectedTypes;
	}
	public QueueReportEntry getTot() {
		return tot;
	}
	public void setTot(QueueReportEntry tot) {
		this.tot = tot;
	}
	public Map<Integer, String> getAvailableSegments() {
		return availableSegments;
	}
	public void setAvailableSegments(Map<Integer, String> availableSegments) {
		this.availableSegments = availableSegments;
	}
	public Integer[] getSelectedSegments() {
		return selectedSegments;
	}
	public void setSelectedSegments(Integer[] selectedSegments) {
		this.selectedSegments = selectedSegments;
	}
	
}
