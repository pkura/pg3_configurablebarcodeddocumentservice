package pl.compan.docusafe.web.office.statistics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class TasklistStatsAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	private ArrayList<TaskListStatBean> taskListBeans;
	private String sortStyle;
	
	protected void setup() 
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doTest").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Test()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private static class BeansComparator implements Comparator<TaskListStatBean>
	{
		private String style;
		
		public BeansComparator(String style) 
		{
			this.style = style;
		}
		
		public int compare(TaskListStatBean o1, TaskListStatBean o2) {
			
			if("name".equalsIgnoreCase(style))
			{	
				return o1.getUser().getLastname().compareTo(o2.getUser().getLastname());
			}
			else if("new".equalsIgnoreCase(style))
			{
				return o2.getNewTasks().compareTo(o1.getNewTasks());
			}
			else if("old".equalsIgnoreCase(style))
			{
				return o2.getOldTasks().compareTo(o1.getOldTasks());
			}
			else if("all".equalsIgnoreCase(style))
			{
				return o2.getAllTasks().compareTo(o1.getAllTasks());
			}
			return o1.getUser().getLastname().compareTo(o2.getUser().getLastname());
		}
	}
	
	private class TaskListStatBean implements java.io.Serializable
	{
		private DSUser user;
		private Integer newTasks = 0;
		private Integer oldTasks = 0;
		
		public TaskListStatBean() {}

		public DSUser getUser() {
			return user;
		}

		public void setUser(DSUser user) {
			this.user = user;
		}

		public Integer getNewTasks() {
			return newTasks;
		}

		public void setNewTasks(Integer newTasks) {
			this.newTasks = newTasks;
		}

		public Integer getOldTasks() {
			return oldTasks;
		}

		public void setOldTasks(Integer oldTasks) {
			this.oldTasks = oldTasks;
		}
	
		public Integer getAllTasks() {
			return this.oldTasks + this.newTasks;
		}
	}
	
	private class Test implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
			LoggerFactory.getLogger("tomekl").debug(""+taskListBeans);
        }
    }
	
	private class FillForm implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
			if(sortStyle != null && ServletActionContext.getRequest().getSession().getValue("taskListBeans") != null)
			{
				taskListBeans = (ArrayList<TaskListStatBean>) ServletActionContext.getRequest().getSession().getValue("taskListBeans");       	
	        	Collections.sort(taskListBeans, new BeansComparator(sortStyle));  
	        	return;
			}
			
			Map<String, TaskListStatBean> statsMap = new HashMap<String, TaskListStatBean>();
        	
        	PreparedStatement ps = null;
        	try
        	{
        		List<DSUser> users = new ArrayList<DSUser>(UserFactory.getInstance().getCanAssignUsers());
        		String questionMarks = "( ";
        		for(int i=0; i < users.size()-1; i++)
        		{
        			questionMarks += "?,";
        		}
        		questionMarks += "? )";
        		
        		ps = DSApi.context().prepareStatement("select assigned_resource, accepted, COUNT(accepted) as counts from dsw_jbpm_tasklist where assigned_resource in "+questionMarks+" group by accepted, assigned_resource order by assigned_resource;");
        		for(int i=0;i<users.size();i++)
        		{
        			ps.setString(i+1, users.get(i).getName());
        		}
        		ResultSet rs = ps.executeQuery();
        		
        		while(rs.next())
        		{
        			String username = rs.getString(1);
        			Integer accepted = rs.getInt(2);
        			Integer count = rs.getInt(3);
        			
        			if(!statsMap.containsKey(username))
        			{
        				TaskListStatBean tlsb = new TaskListStatBean();
        				tlsb.setUser(DSUser.findByUsername(username));
        				statsMap.put(username, tlsb);
        			}
        			
        			if(accepted == 0)
        			{
        				statsMap.get(username).setNewTasks(count);
        			}
        			else if(accepted == 1)
        			{
        				statsMap.get(username).setOldTasks(count);
        			}        			
        		}
        		
        		rs.close();
        		ps.close();
        		ps = null;
        	}
        	catch (Exception e)
        	{
				LoggerFactory.getLogger("tomekl").debug("wtf",e);
			}
        	finally
        	{
        		DSApi.context().closeStatement(ps);
        	}
        	taskListBeans = new ArrayList<TaskListStatBean>(statsMap.values());
        	ServletActionContext.getRequest().getSession().putValue("taskListBeans", taskListBeans);
        	Collections.sort(taskListBeans, new BeansComparator(sortStyle));
        }
    }

	public ArrayList<TaskListStatBean> getTaskListBeans() {
		return taskListBeans;
	}

	public void setTaskListBeans(ArrayList<TaskListStatBean> taskListBeans) {
		this.taskListBeans = taskListBeans;
	}

	public String getSortStyle() {
		return sortStyle;
	}

	public void setSortStyle(String sortStyle) {
		this.sortStyle = sortStyle;
	}   

}
