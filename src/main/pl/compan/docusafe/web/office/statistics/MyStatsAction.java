package pl.compan.docusafe.web.office.statistics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class MyStatsAction extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	Logger log = LoggerFactory.getLogger(this.getClass());
	StringManager sm = StringManager.getManager(this.getClass().getPackage().getName());
	private Integer processed;
	private Integer processedFromBeg;
	private Integer received;
	private Integer receivedFromBeg;
	
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
		
        public void actionPerformed(ActionEvent event)
        {
        	PreparedStatement ps = null;
        	try
        	{
        		ps = DSApi.context().session().connection().prepareStatement("select count(id) from dsw_task_history_entry where username=? and finishDate=?");
        		ps.setString(1, DSApi.context().getPrincipalName());
        		ps.setDate(2, new java.sql.Date(new Date().getTime()));
        		ResultSet rs = ps.executeQuery();
        		if(rs.next())
        			processed = rs.getInt(1);
        		else
        			processed = 0;
        		
        		rs.close();
        		DSApi.context().closeStatement(ps);
        		
        		ps = DSApi.context().session().connection().prepareStatement("select count(id) from dsw_task_history_entry where username=? and finishDate>=? and finishDate<=?");
        		Calendar c = Calendar.getInstance();
        		c.setTime(new Date());
        		c.set(Calendar.DAY_OF_MONTH, 1);
        		ps.setString(1, DSApi.context().getPrincipalName());
        		ps.setDate(2, new java.sql.Date(c.getTimeInMillis()));
        		ps.setDate(3, new java.sql.Date(new Date().getTime()));
        		rs = ps.executeQuery();
        		if(rs.next())
        			processedFromBeg = rs.getInt(1);
        		else
        			processedFromBeg = 0;
        		rs.close();
        		DSApi.context().closeStatement(ps);
        		
        		ps = DSApi.context().session().connection().prepareStatement("select count(id) from dsw_task_history_entry where username=? and receiveDate=?"); 		
        		ps.setString(1, DSApi.context().getPrincipalName());
        		ps.setDate(2, new java.sql.Date(new Date().getTime()));
        		rs = ps.executeQuery();
        		if(rs.next())
        			received = rs.getInt(1);
        		else
        			received = 0;
        		rs.close();
        		DSApi.context().closeStatement(ps);
        		
        		ps = DSApi.context().session().connection().prepareStatement("select count(id) from dsw_task_history_entry where username=? and receiveDate>=? and receiveDate<=?");
        		ps.setString(1, DSApi.context().getPrincipalName());
        		ps.setDate(2, new java.sql.Date(c.getTimeInMillis()));
        		ps.setDate(3, new java.sql.Date(new Date().getTime()));
        		rs = ps.executeQuery();
        		if(rs.next())
        			receivedFromBeg = rs.getInt(1);
        		else
        			receivedFromBeg = 0;
        		rs.close();
        		
        	}
        	catch(Exception e)
        	{
        		addActionError(e.getMessage());
        		log.debug(e.getMessage(), e);
        	}
        	finally
        	{
        		DSApi.context().closeStatement(ps);
        	}
        }

    }

	public Integer getProcessed() {
		return processed;
	}

	public void setProcessed(Integer processed) {
		this.processed = processed;
	}

	public Integer getProcessedFromBeg() {
		return processedFromBeg;
	}

	public void setProcessedFromBeg(Integer processedFromBeg) {
		this.processedFromBeg = processedFromBeg;
	}

	public Integer getReceived() {
		return received;
	}

	public void setReceived(Integer received) {
		this.received = received;
	}

	public Integer getReceivedFromBeg() {
		return receivedFromBeg;
	}

	public void setReceivedFromBeg(Integer receivedFromBeg) {
		this.receivedFromBeg = receivedFromBeg;
	}
	
	

}
