package pl.compan.docusafe.web.office;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CurrentDayAction.java,v 1.14 2009/12/01 09:09:41 mariuszk Exp $
 */
public class CurrentDayAction extends EventActionSupport
{
    public static Boolean P_4_CURRENT_DAY = AvailabilityManager.isAvailable("p4CurrentDay");
    public static final Boolean SET_OPEN_DAY_NEW_METHOD = AvailabilityManager.isAvailable("journals.setOpenDay.newMethod");
    public static final Boolean JOURNALS_PERMISSIONS = AvailabilityManager.isAvailable("journals.Permissions");
    /**
     * Dziennik g��wny pism przychodz�cych
     */
    private Journal mainIncoming;
    /**
     * Dziennik g��wny pism wychodz�cych
     */
    private Journal mainOutgoing;
    /**
     * Dziennik g��wny pism wew�trznych
     */
    private Journal mainInternal;
    private static Logger log = LoggerFactory.getLogger(CurrentDayAction.class);
    
    /**
     * Enum zawieraj�cy typy dziennik�w
     */
    public static enum JournalType {
    	INCOMING(Journal.INCOMING), OUTGOING(Journal.OUTGOING), INTERNAL(Journal.INTERNAL);
    	
    	private String value;
    	private JournalType(String value) {
    		this.value = value;
    	}
    	
    	public String getValue() {
    		return this.value;
    	}
    };
    // @EXPORT
    private String currentDay;
    private boolean canCloseDay;
    private boolean canSetAnyDay;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    public List<Journal> getJournals()
    {
        return journals;
    }

    public void setJournals(List<Journal> journals)
    {
        this.journals = journals;
    }

    public static Boolean getP_4_CURRENT_DAY()
    {
        return P_4_CURRENT_DAY;
    }

    public static void setP_4_CURRENT_DAY(Boolean p_4_CURRENT_DAY)
    {
        P_4_CURRENT_DAY = p_4_CURRENT_DAY;
    }

    private List<Journal> journals;
    // @IMPORT
    private String day;
    private Long id;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSetCurrent").
            append(OpenHibernateSession.INSTANCE).
            append(new SetCurrent()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCloseDay").
            append(OpenHibernateSession.INSTANCE).
            append(new CloseDay()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSetOpenDayNewMethod").
        	append(OpenHibernateSession.INSTANCE).
        	append(new SetOpenDayNewMethod()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                try
                {
                    if (P_4_CURRENT_DAY)
                    {
                        String journalGuidOwner = P4Helper.journaGuidOwner();
                        if (journalGuidOwner != null)
                        {
                            journals = Journal.findByDivisionGuid(journalGuidOwner);
                            currentDay = DateUtils.formatJsDate(GlobalPreferences.getCurrentDay(journalGuidOwner));
                        }
                        else
                            currentDay = DateUtils.formatJsDate(GlobalPreferences.getCurrentDay());
                    } else if (SET_OPEN_DAY_NEW_METHOD) {
                    	journals = Lists.newArrayList();
                    	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)) {
                    		mainIncoming = Journal.getMainIncoming();
                    		mainOutgoing = Journal.getMainOutgoing();
                    		mainInternal = Journal.getMainInternal();
                    	}
                    	for(JournalType type : JournalType.values()) {
                    		for (Journal journal : Journal.findByType(type.getValue())) {
                				if (JOURNALS_PERMISSIONS) {
                					if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)) {
                						if (journal.getOwnerGuid() != null) {										//nie dodawanie do listy dziennikow dziennikow glownych (ownerGuid w dziennikach glownych = null)
                							journals.add(journal);
                						}
                					} else {
                						if (hasPermissionsToJournal(journal) && journal.getOwnerGuid() != null) {					//nie dodawanie do listy dziennikow dziennikow glownych (ownerGuid w dziennikach glownych = null)
                							journals.add(journal);
                						}
                					}
                				}
                    		}
                    	}
                    } else
                        currentDay = DateUtils.formatJsDate(GlobalPreferences.getCurrentDay());
                }
                catch (GlobalPreferences.CurrentDayNotOpenException e)
                {
                }

                canCloseDay = DSApi.context().hasPermission(DSPermission.DZIEN_ZAMKNIECIE);
                canSetAnyDay = DSApi.context().hasPermission(DSPermission.DZIEN_DOWOLNY_OTWARCIE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class CloseDay implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (P_4_CURRENT_DAY)
                    p4CloseDay();
                else
                    normalCloseDay();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

        private void p4CloseDay()  throws EdmException
        {
            DSApi.context().begin();

            if (!DSApi.context().hasPermission(DSPermission.DZIEN_ZAMKNIECIE))
                throw new EdmException(sm.getString("BrakUprawnienDoZamykaniaDnia"));

            Date openDay = GlobalPreferences.getCurrentDay();
            String journalGuidOwner = P4Helper.journaGuidOwner();
            int currentYear = GlobalPreferences.getCurrentYear();

            Calendar cal = Calendar.getInstance();
            cal.setTime(openDay);
            cal.set(Calendar.HOUR_OF_DAY,0);
            cal.set(Calendar.MINUTE,0);
            cal.add(Calendar.DATE, 1);

            if (cal.get(Calendar.YEAR) != currentYear)
            {
                GlobalPreferences.setCurrentDay(null, journalGuidOwner);
                addActionMessage(sm.getString("NowyDzienNieZostalOtwartyPoniewazNieBylbyZgodny" +
                        "ZrokiemOtwartymWaplikacji.NalezyNajpierwZamknacRok",currentYear));
            }
            else if (cal.getTime().after(new Date()))
            {
                throw new EdmException(sm.getString("NowyDzienNieZostalOtwartyPoniewazNieBylbyZgodny" +
                        "ZdataSystemowaZnajdowalbySieWprzyszlosci"));
            }
            else
            {
                GlobalPreferences.setCurrentDay(cal.getTime(), journalGuidOwner);
            }

            DSApi.context().commit();
        }

        private void normalCloseDay() throws EdmException
        {
            DSApi.context().begin();

            if (!DSApi.context().hasPermission(DSPermission.DZIEN_ZAMKNIECIE))
                throw new EdmException(sm.getString("BrakUprawnienDoZamykaniaDnia"));

            Date openDay = GlobalPreferences.getCurrentDay();

            int currentYear = GlobalPreferences.getCurrentYear();

            Calendar cal = Calendar.getInstance();
            cal.setTime(openDay);
            cal.set(Calendar.HOUR_OF_DAY,0);
            cal.set(Calendar.MINUTE,0);
            cal.add(Calendar.DATE, 1);

            if (cal.get(Calendar.YEAR) != currentYear)
            {
                GlobalPreferences.setCurrentDay(null);
                addActionMessage(sm.getString("NowyDzienNieZostalOtwartyPoniewazNieBylbyZgodny" +
                    "ZrokiemOtwartymWaplikacji.NalezyNajpierwZamknacRok",currentYear));
            }
            else if (cal.getTime().after(new Date()))
            {
                throw new EdmException(sm.getString("NowyDzienNieZostalOtwartyPoniewazNieBylbyZgodny" +
                    "ZdataSystemowaZnajdowalbySieWprzyszlosci"));
            }
            else
            {
                GlobalPreferences.setCurrentDay(cal.getTime());
            }

            DSApi.context().commit();
        }
    }

    /**
     * Ustawia bie��cy dzie� jako otwarty.
     */
    private class SetCurrent implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // data w bie��cej strefie czasowej
            Date date = DateUtils.nullSafeParseJsDate(day);
            if (date == null)
                addActionError(sm.getString("PodanoNiepoprawnaDate"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.DZIEN_DOWOLNY_OTWARCIE))
                    throw new EdmException(sm.getString("BrakUprawnienDoOtwarciaDowolnegoDnia"));

                // je�eli w jakimkolwiek dzienniku istniej� wpisy z p�niejszego
                // dnia, nie mo�na zmieni� daty
                // (jsDate, bo komunikat b�dzie pokazany u�ytkownikowi)
                if (!P_4_CURRENT_DAY && JournalEntry.newerEntriesExist(date))
                    throw new EdmException(sm.getString("NieMoznaUstawicDatyPoniewazWprzynajmniejJednym" +
                            "DziennikuZnajdujaSieDokumentyOznaczoneDataNowszaNizZadana",DateUtils.formatJsDate(date)));

                int currentYear = GlobalPreferences.getCurrentYear();

                Calendar calDate = Calendar.getInstance();
                calDate.setTime(date);

                if (calDate.get(Calendar.YEAR) != currentYear)
                    throw new EdmException(sm.getString("MoznaOtworzycTylkoDzienZgodnyZotwartymRokiem",currentYear));

                if (P_4_CURRENT_DAY)
                {
                    String journalGuidOwner = P4Helper.journaGuidOwner();
                    GlobalPreferences.setCurrentDay(date, journalGuidOwner);
                }
                else
                    GlobalPreferences.setCurrentDay(date);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     *	Nowy spos�b ustawiania otwartego dnia. Ka�dy dziennik posiada sw�j
     *	odr�bny otwarty dzie�. Klasa ustawia dla danego dziennika wskazan� dat�
     *	jako dzie� otwarty.
     */
    private class SetOpenDayNewMethod implements ActionListener {
    	
    	public void actionPerformed(ActionEvent event) {
    		Date date = DateUtils.nullSafeParseJsDate(day);
    		if (date == null) {
    			addActionError(sm.getString("PodanoNiepoprawnaDate"));
    		}
    		
    		if (hasActionErrors()) {
    			return;
    		}
    		
    		try {
    			if (!DSApi.context().hasPermission(DSPermission.DZIEN_DOWOLNY_OTWARCIE)) {
    				throw new EdmException(sm.getString("BrakUprawnienDoOtwarciaDowolnegoDnia"));
    			}
    			
                if (JournalEntry.newerEntriesExist(date)) {
                    throw new EdmException(sm.getString("NieMoznaUstawicDatyPoniewazWprzynajmniejJednym" +
                            "DziennikuZnajdujaSieDokumentyOznaczoneDataNowszaNizZadana",DateUtils.formatJsDate(date)));
                }
                
                GlobalPreferences.setCurrentDay(date, id);
                
    		} catch (EdmException e) {
    			log.error(e.getMessage(), e);
    			DSApi.context().setRollbackOnly();
    			addActionError(e.getMessage());
    		}
    	}
    	
    }
    
    private boolean hasPermissionsToJournal(Journal journal) {
    	boolean hasPermissions;
    	try {
			DSDivision division = DSDivision.find(journal.getDescription());
			if (DSApi.context().getDSUser().inDivision(division, true)) {
				hasPermissions = true;
			} else {
				hasPermissions = false;
			}
		} catch (DivisionNotFoundException e) {
			hasPermissions = false;
		} catch (UserNotFoundException e) {
			hasPermissions = false;
		} catch (EdmException e) {
			hasPermissions = false;
		}
    	return hasPermissions;
    }

    public String getCurrentDay()
    {
        return currentDay;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

    public boolean isCanCloseDay()
    {
        return canCloseDay;
    }

    public boolean isCanSetAnyDay()
    {
        return canSetAnyDay;
    }

	public static Boolean getNEW_METHOD_SET_OPEN_DAY() {
		return SET_OPEN_DAY_NEW_METHOD;
	}

	public Journal getMainIncoming() {
		return mainIncoming;
	}

	public void setMainIncoming(Journal mainIncoming) {
		this.mainIncoming = mainIncoming;
	}

	public Journal getMainOutgoing() {
		return mainOutgoing;
	}

	public void setMainOutgoing(Journal mainOutgoing) {
		this.mainOutgoing = mainOutgoing;
	}

	public Journal getMainInternal() {
		return mainInternal;
	}

	public void setMainInternal(Journal mainInternal) {
		this.mainInternal = mainInternal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
