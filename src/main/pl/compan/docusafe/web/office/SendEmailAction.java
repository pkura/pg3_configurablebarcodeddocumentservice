package pl.compan.docusafe.web.office;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.ajax.AjaxJsonResult;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.comet.CometMessageManager;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

/**
 * User: Tomasz
 * Date: 04.02.13
 * Time: 15:13
 */
public class SendEmailAction extends EventActionSupport
{
    private final static Logger LOG = LoggerFactory.getLogger(SendEmailAction.class);

    private String toEmail;
    private String subject;
    private String message;
    private String attIds;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION)
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new FillForm())
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doSendEmail")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new SendEmail())
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doSendEmailWAT")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new SendEmailWithAttachements())
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {}

        @Override
        public Logger getLogger() { return LOG; }
    }

    private class SendEmailWithAttachements extends  LoggedActionListener
    {
        public void actionPerformed(ActionEvent event, Logger log) throws Exception
        {
            PrintWriter out = getOutPrintWriter();
            AjaxJsonResult result = new AjaxJsonResult();

            result.success = true;

            try
            {
                Preconditions.checkNotNull(toEmail, "toEmail cannot be null");
                Preconditions.checkNotNull(subject, "subject cannot be null");
                Preconditions.checkNotNull(message, "message cannot be null");

                Attachment curAtt = null;
                List<Mailer.Attachment> attachments = new ArrayList<Mailer.Attachment>();

                if(attIds != null && attIds.length() > 0)
                {
                    for(String sId : Splitter.on(",").trimResults().split(attIds))
                    {
                        curAtt = Attachment.find(Long.valueOf(sId));
                        attachments.add(new Mailer.Attachment(curAtt.getMostRecentRevision().getOriginalFilename(), curAtt.getMostRecentRevision().saveToTempFile().toURI().toURL()));
                    }
                }
                ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail,toEmail,subject,message, attachments);
            }
            catch(Exception e)
            {
                result.success = false;
                result.errorMessage = e.getMessage();
            }

            out.write(result.toJson());
            out.flush();
            out.close();
        }

        public Logger getLogger() { return LOG; }
    }

    private class SendEmail extends LoggedActionListener
    {
        public void actionPerformed(ActionEvent event, Logger log) throws Exception
        {
            PrintWriter out = getOutPrintWriter();
            AjaxJsonResult result = new AjaxJsonResult();

            result.success = true;

            try
            {
                Preconditions.checkNotNull(toEmail, "toEmail cannot be null");
                Preconditions.checkNotNull(subject, "subject cannot be null");
                Preconditions.checkNotNull(message, "message cannot be null");
                ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail,toEmail,subject,message);
            }
            catch(Exception e)
            {
                result.success = false;
                result.errorMessage = e.getMessage();
            }

            out.write(result.toJson());
            out.flush();
            out.close();
        }

        public Logger getLogger() { return LOG; }
    }

    private PrintWriter getOutPrintWriter() throws IOException {
        HttpServletResponse resp = ServletActionContext.getResponse();
        resp.setContentType("text/html; charset=UTF-8");
        return resp.getWriter();
    }

    public String getAttIds() {
        return attIds;
    }

    public void setAttIds(String attIds) {
        this.attIds = attIds;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
