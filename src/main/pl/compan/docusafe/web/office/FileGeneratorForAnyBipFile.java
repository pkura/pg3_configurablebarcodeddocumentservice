package pl.compan.docusafe.web.office;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.bip.AbstractBipDriver;
import pl.compan.docusafe.service.bip.Bip;
import pl.compan.docusafe.service.bip.MaxusBipDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class FileGeneratorForAnyBipFile extends EventActionSupport
{
		private static final long serialVersionUID = 1L;
		private File[] files;
		private String fileName;
	    private boolean deleteBipFiles;
	    private String BIP_NAME_MAXUS = "maxus";
	    private String BIP_NAME_FILE = "file";
		Logger log = LoggerFactory.getLogger(FileGeneratorForAnyBipFile.class);

		protected void setup()
		{
			registerListener(DEFAULT_ACTION).
		          append(OpenHibernateSession.INSTANCE).
		          append(new FillForm()).
		          appendFinally(CloseHibernateSession.INSTANCE);

		    registerListener("doGenerate").
		          append(OpenHibernateSession.INSTANCE).
		          append(new Generate()).
		          append(new FillForm()).
		          appendFinally(CloseHibernateSession.INSTANCE);
		    registerListener("doGetFile").
		    	append(OpenHibernateSession.INSTANCE).
	          	append(new GetFile()).
	          	append(new FillForm()).
	          	appendFinally(CloseHibernateSession.INSTANCE);

		}


	 	private class FillForm implements ActionListener
	    {
	        public void actionPerformed(ActionEvent event)
	        {
	        	try
	        	{
	        		String bipServiceName = ServiceManager.getDriver(Bip.NAME);
		        	if(bipServiceName.equals(BIP_NAME_MAXUS) || bipServiceName.equals(BIP_NAME_FILE))
		        	{
		        		File bipFilesDir = new File((String)ServiceManager.getService("bip").getProperty("directory").getValue());
		        		files = bipFilesDir.listFiles();
		        	}
	        	}
	        	catch(Exception e)
	        	{
	        		if (e.getMessage() != null)
	        			addActionError(e.getMessage());
	        		else
	        			addActionError("Us�uga nie zosta�a uruchomiona.");
	        		log.debug("",e);
	        	}
	        }
	    }

	 	private class Generate implements ActionListener
	    {
	        public void actionPerformed(ActionEvent event)
	        {
	        	try{
	        		if (deleteBipFiles) {
		        		File bipFilesDir = new File(
		        				(String)ServiceManager.getService(Bip.NAME).getProperty("directory").getValue());
		        		files = bipFilesDir.listFiles();
		        		for (File f : files)
		        			f.delete();
	        		}
	        		int maxCountCaseInFile = 1000;
	        		try {
	        			maxCountCaseInFile = Integer.parseInt(Docusafe.getAdditionProperty("bip.file.generator.maxcountcaseinfile"));
	        		} catch (NumberFormatException ex) { }
	        		File file = new File((String)ServiceManager.getService("bip").getProperty("directory").getValue());
	        		if (ServiceManager.getDriver(Bip.NAME).equals(BIP_NAME_MAXUS))
	        			createXMLFileForMaxusBip(file, maxCountCaseInFile);
	        		else if (ServiceManager.getDriver(Bip.NAME).equals(BIP_NAME_FILE))
	        			createXMLFileForFileBip(file, maxCountCaseInFile);
	        	}
	        	catch(Exception e)
	        	{
	        		if (e.getMessage() != null)
	        			addActionError(e.getMessage());
	        		else
	        			addActionError("Us�uga nie zosta�a uruchomiona.");
	        		log.debug("",e);
	        	}
	        }
	    }

	 	private class GetFile implements ActionListener
	 	{
	 		public void actionPerformed(ActionEvent event)
	        {
	        	try
	        	{
	        		File rep = new File(new File((String)ServiceManager.getService(Bip.NAME).getProperty("directory").getValue()), fileName);
		            ServletUtils.streamFile(ServletActionContext.getResponse(), rep, "application/xml", "Content-Disposition: attachment; filename=\"bip.xml\"");
		            event.setResult(NONE);
	        	}
	            catch(Exception e)
	            {
	            	addActionError(e.getMessage());
	            	log.debug("",e);
	            }
	        }
	 	}

	 	private void createXMLFileForFileBip(File directory, int maxCountCaseInFile) {
	 		org.dom4j.io.XMLWriter writer = null;
        	ResultSet rs = null;
            PreparedStatement ps = null;
            List<Long> todelete = new ArrayList<Long>();
        	try
            {
        		DSApi.openAdmin();
	        	DocumentFactory factory = org.dom4j.DocumentFactory.getInstance();
	            String data = DSApi.context().systemPreferences().node("file_bip").get("file_export_date", "");
	        	java.util.Date lastGeneratingDate = null;

	        	if(data.length() > 0)
	        	{
	        		 lastGeneratingDate = DateUtils.parseJsDateTime(data);
	        	}

            	ps = DSApi.context().prepareStatement("SELECT * FROM dso_file_bip_state");
            	rs = ps.executeQuery();
            	if (directory == null)
	                throw new ServiceException("Nie okre�lono katalogu na pliki BIP");

	            if (!(directory.isDirectory() || directory.mkdirs()))
	                throw new ServiceException("Nie mo�na utworzy� katalogu na pliki BIP: " + directory);

	            Document document = factory.createDocument();
            	document.addDocType("sprawa", AbstractBipDriver.PUBLICID, AbstractBipDriver.SYSTEMID);
                Element spr = document.addElement("EksportBIP_G");
                int counter = 0;
            	while(rs.next())
            	{
            		if(lastGeneratingDate == null || rs.getTimestamp("modify_date").after(new Timestamp(lastGeneratingDate.getTime())))
            		{
            			++counter;
	            		appendFileBipXml(InOfficeDocument.findInOfficeDocument(rs.getLong("document_id")),spr,rs.getString("stan"));
	            		todelete.add(rs.getLong("document_id"));
            		}
            	}

            	if(counter > 0)
        		{
        		   File file = new File(directory, "bip."+System.currentTimeMillis()+".xml");
        		   OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file),"UTF-8");
 	               writer = new org.dom4j.io.XMLWriter(fw);
 	               writer.write(document);
 	               writer.close();
 	               document = factory.createDocument();
 	               document.addDocType("sprawa", MaxusBipDriver.PUBLICID, MaxusBipDriver.SYSTEMID);
 	               spr = document.addElement("EksportBIP_G");
        		}

            	DSApi.context().systemPreferences().node("file_bip").put("file_export_date", DateUtils.formatJsDateTime(new java.util.Date(System.currentTimeMillis())));
            	DSApi.context().systemPreferences().flush();
            }
            catch (Exception e)
            {
                log.debug("B��d zapisu pliku dla BIP", e);
                e.printStackTrace();
            }
            finally
            {
                if (writer != null) try { writer.close(); } catch (Exception e) { }
                DSApi.context().closeStatement(ps);
            }
	 	}

	 	private void createXMLFileForMaxusBip(File directory, int maxCountCaseInFile)
	    {

	        	org.dom4j.io.XMLWriter writer = null;
	        	ResultSet rs = null;
	            PreparedStatement ps = null;
	            List<Long> todelete = new ArrayList<Long>();
	        	try
	            {
	        		DSApi.openAdmin();
		        	DocumentFactory factory = org.dom4j.DocumentFactory.getInstance();
		            String data = DSApi.context().systemPreferences().node("maxus_bip").get("maxus_export_date", "");
		        	java.util.Date d = null;

		        	if(data.length()>0)
		        	{
		        		d = DateUtils.parseJsDateTime(data);
		        	}


		            //if(!(cal.before(new java.util.Date())||data.length()==0)) return;


	            	ps = DSApi.context().prepareStatement("select * from dso_maxus_bip_state");
	            	rs = ps.executeQuery();
	            	if (directory == null)
		                throw new ServiceException("Nie okre�lono katalogu na pliki BIP");

		            if (!(directory.isDirectory() || directory.mkdirs()))
		                throw new ServiceException("Nie mo�na utworzy� katalogu na pliki BIP: "+directory);

		            Document document = factory.createDocument();
	            	document.addDocType("sprawa", AbstractBipDriver.PUBLICID, AbstractBipDriver.SYSTEMID);
	                Element spr = document.addElement("EksportBIP_G");
	                int counter = 0;
	                int mainCounter = 0;
	            	while(rs.next())
	            	{

	            		if(d==null || rs.getTimestamp("modify_date").after(new Timestamp(d.getTime())))
	            		{
		            		appendXml(OfficeCase.find(rs.getLong("case_id")),spr,rs.getString("stan"));
		            		todelete.add(rs.getLong("case_id"));
		            		counter++;
		            		mainCounter++;
	            		}
	            		if(counter >= 3)
	            		{
	            			counter = 0;
	            			File file = new File(directory, "bip."+System.currentTimeMillis()+".xml");

	            			OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file),"UTF-8");

	     	                writer = new org.dom4j.io.XMLWriter(fw);
	     	                    //new OutputFormat("    ", true));
	     	                writer.write(document);
	     	                writer.close();

	     	               document = factory.createDocument();
	     	               document.addDocType("sprawa", MaxusBipDriver.PUBLICID, MaxusBipDriver.SYSTEMID);
	     	               spr = document.addElement("EksportBIP_G");
	            		}
	            	}
	            	DSApi.context().systemPreferences().node("maxus_bip").put("maxus_export_date", DateUtils.formatJsDateTime(new java.util.Date(System.currentTimeMillis())));
	            	DSApi.context().systemPreferences().flush();

	            	if(mainCounter>0)
	            	{
		            	File file = new File(directory, "bip."+System.currentTimeMillis()+".xml");
	 	                FileOutputStream os = new FileOutputStream(file);


	 	                writer = new org.dom4j.io.XMLWriter(os);
	 	                    //new OutputFormat("    ", true));
	 	                writer.write(document);
	 	                writer.close();
	            	}
	            }
	            catch (Exception e)
	            {
	            	//e.printStackTrace();
	                log.debug("B��d zapisu pliku dla BIP", e);
	                e.printStackTrace();
	            }
	            finally
	            {
	                if (writer != null) try { writer.close(); } catch (Exception e) { }
	                DSApi.context().closeStatement(ps);
	            }

	    }

	 	private void appendFileBipXml(InOfficeDocument doc, Element elem, String akcja) throws EdmException {
	 		 Element sprawa = elem.addElement("sprawa");
	         sprawa.addAttribute("tracking", doc.getTrackingNumber());
	         sprawa.addAttribute("password", doc.getTrackingPassword());
	         sprawa.addAttribute("timestamp", String.valueOf(System.currentTimeMillis()));
	         Element action = sprawa.addElement("Akcja");
	         action.setText(akcja);

	         if (doc.getContainingCase() != null && doc.getContainingCase().isClosed())
	         {
	             sprawa.addElement("zakonczona");
	         }

	         if (doc.getContainingCase() != null)
	         {
	             OfficeCase c = doc.getContainingCase();
	             sprawa.addElement("numer").addCDATA(c.getOfficeId());
	             if (c.getOpenDate() != null)
	                 sprawa.addElement("data-zalozenia").addCDATA(DateUtils.formatBipDate(c.getOpenDate()));
	             if (c.getFinishDate() != null)
	                 sprawa.addElement("data-zakonczenia").addCDATA(DateUtils.formatBipDate(c.getFinishDate()));
	             try
	             {
	                 sprawa.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(c.getClerk()));
	                 sprawa.addElement("dzial").addCDATA(DSDivision.safeGetName(c.getDivisionGuid()));
	             }
	             catch (EdmException e)
	             {
	             }
	             // dzial
	             if (c.getStatus() != null)
	                 sprawa.addElement("status").addCDATA(c.getStatus().getName());
	         }
	         else
	         {
	             if (doc.getCaseDocumentId() != null)
	                 sprawa.addElement("numer").setText(doc.getCaseDocumentId());
	             sprawa.addElement("data-zalozenia").setText(DateUtils.formatBipDate(doc.getIncomingDate()));
	             try
	             {
	                 sprawa.addElement("referent").setText(DSUser.safeToFirstnameLastname(doc.getClerk()));
	             }
	             catch (EdmException e)
	             {
	             }
	         }
	         Element pisma = sprawa.addElement("pisma");

	         Element pismoPrzyjete = pisma.addElement("pismo-przyjete");
	         pismoPrzyjete.addElement("numer-kancelaryjny").setText(String.valueOf(doc.getOfficeNumber()));
	         if (doc.getCaseDocumentId() != null)
	             pismoPrzyjete.addElement("sygnatura").addCDATA(doc.getCaseDocumentId());
	         pismoPrzyjete.addElement("data-przyjecia").setText(DateUtils.formatBipDate(doc.getIncomingDate()));
	         if (doc.getDocumentDate() != null)
	             pismoPrzyjete.addElement("data-pisma").setText(DateUtils.formatBipDate(doc.getDocumentDate()));
	         pismoPrzyjete.addElement("tresc").addCDATA(doc.getSummary());
	         if (doc.getReferenceId() != null)
	             pismoPrzyjete.addElement("znak").setText(doc.getReferenceId());
	         try
	         {
	             pismoPrzyjete.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(doc.getClerk()));
	         }
	         catch (EdmException e)
	         {
	         }
	         if (doc.getSender() != null)
	         {
	             Element nadawcy = pismoPrzyjete.addElement("nadawcy");
	             Element kontakt = nadawcy.addElement("kontakt");
	             updateElementKontakt(kontakt, doc.getSender());
	         }

	         if (doc.getRecipients().size() > 0)
	         {
	             Element odbiorcy = pismoPrzyjete.addElement("odbiorcy");
	             for (Iterator iter=doc.getRecipients().iterator(); iter.hasNext(); )
	             {
	                 Element kontakt = odbiorcy.addElement("kontakt");
	                 updateElementKontakt(kontakt, (Recipient) iter.next());
	             }
	         }

	         for (Iterator iter=doc.getOutDocuments().iterator(); iter.hasNext(); )
	         {

	             OutOfficeDocument outdoc = (OutOfficeDocument) iter.next();

	             Element pismo = pisma.addElement("pismo-wyslane");
	             if (outdoc.getCaseDocumentId() != null)
	                 pismo.addElement("sygnatura").addCDATA(outdoc.getCaseDocumentId());
	             if (outdoc.getDocumentDate() != null)
	                 pismo.addElement("data-pisma").setText(DateUtils.formatBipDate(outdoc.getDocumentDate()));
	             pismo.addElement("tresc").addCDATA(outdoc.getSummary());
	             try
	             {
	                 pismo.addElement("referent").addCDATA(DSUser.safeToFirstnameLastname(doc.getClerk()));
	             }
	             catch (EdmException e)
	             {
	             }
	             if (outdoc.getSender() != null)
	             {
	                 Element nadawcy = pismo.addElement("nadawcy");
	                 Element kontakt = nadawcy.addElement("kontakt");
	                 updateElementKontakt(kontakt, outdoc.getSender());
	             }
	             if (outdoc.getRecipients().size() > 0)
	             {
	                 Element odbiorcy = pismo.addElement("odbiorcy");
	                 for (Iterator recipientsIter=outdoc.getRecipients().iterator(); recipientsIter.hasNext(); )
	                 {
	                     Person person = (Person) recipientsIter.next();
	                     Element kontakt = odbiorcy.addElement("kontakt");
	                     updateElementKontakt(kontakt, person);
	                 }
	             }
	         }
	 	}

	    private void updateElementKontakt(Element kontakt, Person person)
	    {
	        if (person.getFirstname() != null)
	            kontakt.addElement("imie").addCDATA(person.getFirstname());
	        if (person.getLastname() != null)
	            kontakt.addElement("nazwisko").addCDATA(person.getLastname());
	        if (person.getOrganization() != null || person.getOrganizationDivision() != null)
	        {
	            StringBuilder org = new StringBuilder();
	            if (person.getOrganization() != null)
	                org.append(person.getOrganization());
	            if (person.getOrganizationDivision() != null)
	            {
	                if (org.length() > 0) org.append(" / ");
	                org.append(person.getOrganizationDivision());
	            }
	            kontakt.addElement("organizacja").addCDATA(org.toString());
	        }
	        if (person.getStreet() != null)
	            kontakt.addElement("adres").addCDATA(person.getStreet());
	        if (person.getZip() != null)
	            kontakt.addElement("kod").setText(person.getZip());
	        if (person.getLocation() != null)
	            kontakt.addElement("miejscowosc").addCDATA(person.getLocation());
	    }

	    protected void appendXml(OfficeCase sprawa, Element elem, String akcja)
	    {
	        String val;

	        Element temp;

	        Element spr = elem.addElement("Sprawa");

	        temp = spr.addElement("Id");
	        val = sprawa.getId().toString();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Akcja");
	        temp.setText(akcja);
	      //TODO:opisac


	        temp = spr.addElement("Numer");
	        val = sprawa.getOfficeId();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("KodSprawy");
	        val = sprawa.getOfficeId();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("NumerWywolujacy");
	        val = sprawa.getTrackingNumber();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("KodWywolujacy");
	        val = sprawa.getTrackingPassword();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Rodzaj");
	        /*val = sprawa.getRwa().getDescription();
	        if(val!=null) temp.setText(val);*/

	        temp = spr.addElement("Status");
	        val = sprawa.getStatus().getName();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Stan");
	        val = sprawa.getStatus().getName();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("KategoriaArchiwalna");
	        val = sprawa.getRwa().getAcHome();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Data");
	        val = DateUtils.formatSqlDate(sprawa.getCtime());
	        if(val!=null) temp.setText(val);


	        temp = spr.addElement("Termin");
	        val = DateUtils.formatSqlDate(sprawa.getFinishDate());
	        if(val!=null) temp.setText(val);


	        temp = spr.addElement("Zalatwiono");
	        if(sprawa.getStatus().getId()== CaseStatus.ID_TMP_CLOSED||sprawa.getStatus().getId()== CaseStatus.ID_CLOSED)
	        {
	        	val = DateUtils.formatSqlDate(sprawa.getFinishDate());
	 	        if(val!=null) temp.setText(val);
	        }
	      //TODO:opisac

	        temp = spr.addElement("Temat");
	        val = sprawa.getDescription();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Przebieg");
	      //TODO:opisac

	        temp = spr.addElement("Sposob");
	      //TODO:opisac

	        temp = spr.addElement("NumerWTeczce");
	        //val = sprawa.getOfficeId();
	        //if(val!=null) temp.setText(val);

	        temp = spr.addElement("NumerWKomorce");
	        //val = sprawa.getOfficeId();
	        //if(val!=null) temp.setText(val);

	        temp = spr.addElement("NumerUPracownika");
	        //val = sprawa.getOfficeId();
	        //if(val!=null) temp.setText(val);

	        temp = spr.addElement("TeczkaSymbol");
	        val = sprawa.getParent().getOfficeId();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("TeczkaNazwa");
	        val = sprawa.getParent().getName();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("PodteczkaNumer");
	      //TODO:opisac

	        temp = spr.addElement("PodteczkaNazwa");
	      //TODO:opisac

	        temp = spr.addElement("Prowadzacy");
	        try
	        {
	        	val = DSUser.findByUsername(sprawa.getClerk()).asFirstnameLastname();
	        }
	        catch(Exception unfe)
	        {
	        	val = sprawa.getClerk();
	        }
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("Komorka");
	        try
	        {
	        	val = DSDivision.find(sprawa.getDivisionGuid()).getName();
	        	if(val!=null) temp.setText(val);
	        }
	        catch(EdmException e)
	        {
	        	//nic nie robimy
	        }
	        temp = spr.addElement("KomorkaSkrot");
	        val = sprawa.getDivisionGuid();
	        if(val!=null) temp.setText(val);

	        temp = spr.addElement("KodStrony");
	      //TODO:opisac
	        try{
		        Sender s = ((InOfficeDocument)sprawa.getDocuments(InOfficeDocument.class).get(0)).getSender();
		        if(s!=null){
			        temp = spr.addElement("Nazwisko");
			        val = s.getLastname();
			        if(val!=null) temp.setText(val);


			        temp = spr.addElement("Imie");
			        val = s.getFirstname();
			        if(val!=null) temp.setText(val);


			        temp = spr.addElement("Instytucja");
			        val = s.getOrganization();
			        if(val!=null) temp.setText(val);


			        temp = spr.addElement("NIP");
			        val = s.getNip();
			        if(val!=null) temp.setText(val);


			        temp = spr.addElement("PESEL");
			        val = s.getPesel();
			        if(val!=null) temp.setText(val);


			        temp = spr.addElement("REGON");
			        val = s.getRegon();
			        if(val!=null) temp.setText(val);
		        }
	        }
	        catch(Exception e )
	        {
	        	//nie robimy nic. Nie udalo sie po rpostu dodac informacji o nadawcy pisma
	        }
	        //spr.
	        //System.out.println(document.asXML());
	        //return document;
	    }
		public File[] getFiles() {
			return files;
		}
		public void setFiles(File[] files) {
			this.files = files;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public boolean isDeleteBipFiles() {
			return deleteBipFiles;
		}
		public void setDeleteBipFiles(boolean deleteBipFiles) {
			this.deleteBipFiles = deleteBipFiles;
		}
}
