package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DwrDockindAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(DwrDockindAction.class);

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
		.append(OpenHibernateSession.INSTANCE)
		.append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			
			
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
}
