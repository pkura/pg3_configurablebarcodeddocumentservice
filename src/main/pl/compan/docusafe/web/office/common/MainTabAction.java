package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public abstract class MainTabAction extends BaseTabsAction
{
	private final static Logger log = LoggerFactory.getLogger(MainTabAction.class);

    protected List<FormFile> multiFiles = new ArrayList<FormFile>();
    protected Map<String,String> loadFiles;
    protected String[] returnLoadFiles;
    protected String filestoken;    

    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    /**Metoda zapoisuje pobrane zalaczniki to katalogu tymczasowego i dodaje wpis do loadFiles
     * Jest to robione na wypadek wystapienia bledu i powrotu jeszcze raz na formularz dodani dokumenu.
     * Zalaczniki sa pamietane i nie trzeba ich jeszcze raz przesylac */
    protected void saveFile()
    {
    	try
    	{
    		if(filestoken == null || filestoken.length() < 1)
    			filestoken = ServletActionContext.getRequest().getRequestedSessionId()+ new Date().getTime();
    		if(loadFiles == null)
    			loadFiles = new HashMap<String, String>();
	    	if(multiFiles != null && multiFiles.size() > 0)
	        {
	        	for (FormFile formFile : multiFiles)
				{   		
					File tmp = new File(Docusafe.getTemp(),filestoken+"_"+formFile.getFile().getName());
					FileOutputStream os = new FileOutputStream(tmp);
					FileInputStream stream = new FileInputStream(formFile.getFile());
					byte[] buf = new byte[2048];
					int count;
					while ((count = stream.read(buf)) > 0)
					{
					    os.write(buf, 0, count);
					}
					os.close();
					stream.close();
					loadFiles.put(tmp.getAbsolutePath(), formFile.getFile().getName());
				}
	        }
	    	if(returnLoadFiles != null && returnLoadFiles.length > 0)
	    	{
	    		for (String path : returnLoadFiles)
				{
	    			String[] tab = path.split("\\\\");
	    			String fileName = tab[tab.length -1].replace(filestoken+"_", "");
	    			loadFiles.put(path,fileName);
				}
	    	}
    	}
    	catch (Exception e) 
    	{
    		LOG.error("",e);
			addActionError("B��d zapisu plik�w "+e.getMessage());
		}
    }
    
    /**
     * Metoda dodaj�ca za��czniki do dokumentu zaimplementowana wspolnie dla biznesu i administarcji 
     * (main.jsp i document-main.jsp) w klasie BaseTabsAction
     * @param doc
     * @throws AccessDeniedException
     * @throws EdmException
     */
    protected void addFiles(OfficeDocument doc) throws AccessDeniedException, EdmException
    {
    	if(multiFiles != null && multiFiles.size() > 0)
        {
        	for (FormFile formFile : multiFiles)
			{
        		 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			}            	
        }
        if( returnLoadFiles != null && returnLoadFiles.length > 0 )
        {
        	for (String fileKey : returnLoadFiles)
			{
        		String path = fileKey.replace("\\","/");
				File f = new File(path);
				 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(f).setOriginalFilename(f.getName().replace(filestoken+"_", ""));
			}
        }
    }
    
	public Map<String, String> getLoadFiles()
	{
		return loadFiles;
	}

	public String[] getReturnLoadFiles()
	{
		return returnLoadFiles;
	}

	public void setLoadFiles(Map<String, String> loadFiles)
	{
		this.loadFiles = loadFiles;
	}

	public void setReturnLoadFiles(String[] returnLoadFiles)
	{
		this.returnLoadFiles = returnLoadFiles;
	}

	public FormFile getMultiFiles() 
	{
		if(this.multiFiles != null && this.multiFiles.size() > 0 )
			return this.multiFiles.get(0);
		else
			return null;
	}
    
	public void setMultiFiles(FormFile file) {
		this.multiFiles.add(file);
	}

	public void setFilestoken(String filestoken)
	{
		this.filestoken = filestoken;
	}

	public String getFilestoken()
	{
		return filestoken;
	}
}
