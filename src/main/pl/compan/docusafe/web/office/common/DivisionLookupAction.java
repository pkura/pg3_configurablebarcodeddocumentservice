package pl.compan.docusafe.web.office.common;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

public class DivisionLookupAction extends EventActionSupport {
	private final static int RESULTS_COUNT = 10;
	private final static Logger LOG = LoggerFactory.getLogger(DivisionLookupAction.class);

	private List<DSDivision> divisions;
	private String name;
	
	
	private String firstname;
	private String lastname;

	protected void setup() {
		registerListener(DEFAULT_ACTION).append(OPEN_HIBERNATE_AND_JBPM_SESSION).append(new FillForm()).appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			try {
				Criteria crit = DSApi.context().session().createCriteria(DSDivision.class);
				if (!StringUtils.isEmpty(name)) {
					crit.add(Restrictions.like("name", name + "%").ignoreCase());
					crit.add(Restrictions.eq("hidden", false));
				}
				crit.setMaxResults(RESULTS_COUNT);
				setDivisions(crit.list());
			} catch (Exception e) {
				log.error("", e);
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public List<DSDivision> getDivisions() {
		return divisions;
	}

	public void setDivisions(List<DSDivision> divisions) {
		this.divisions = divisions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname()
	{
		return firstname;
	}

	
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	
	public String getLastname()
	{
		return lastname;
	}

	
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}

}
