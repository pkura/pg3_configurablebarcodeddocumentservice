package pl.compan.docusafe.web.office.common;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.base.PostalCode;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author Mariusz Kiljanczyk
 */
public class CodeLookupAction extends EventActionSupport{
	private final static Logger LOG = LoggerFactory.getLogger(CodeLookupAction.class);
	private final static int RESULTS_COUNT = 200;
	private static final String LocationNotFoundError = "LocNotFound";

	private String location;

	private List<? extends Person>  senders;

	protected void setup() {
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private static String isoToUtfHack(String string) throws Exception{
		return string; //new String(string.getBytes("ISO-8859-2"),"utf-8");
	}

	private class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			try
			 {
			 	String str_CodeTo   = "";
			 	String str_CodeFrom = "";
				 
				 String asd = isoToUtfHack(location); 
			     List <PostalCode> pc = PostalCode.findByLocation(isoToUtfHack(location.trim()));
			    
			     if(pc.size() == 0)
			     {		    	
			    	 str_CodeTo = LocationNotFoundError;
			     }
			     else if(pc.size()==1)
			     {
			    	 str_CodeTo = Integer.toString(pc.get(0).getCode_to()/1000);
			    	 String secondPartTo = Integer.toString(pc.get(0).getCode_to()%1000);
			    	 String validatedCode_To = validateCode(str_CodeTo, secondPartTo);
			    	 
			    	 str_CodeFrom = Integer.toString(pc.get(0).getCode_from()/1000);
			    	 String secondPartFrom = Integer.toString(pc.get(0).getCode_from()%1000);
			    	 String validatedCode_From = validateCode(str_CodeFrom, secondPartFrom);
			    	 
			    	 if(validatedCode_To.equals(validatedCode_From))
			    	 {
			    		 str_CodeTo = validatedCode_To;
			    	 }
			    	 else
			    	 {
			    		 str_CodeTo = validatedCode_From + " - " + validatedCode_To;
			    	 }
	 	 
			     }
			     else if(pc.size()>1)
			     {
			    	 String finalStr_CodeTo = "";
			    	 for (Iterator <PostalCode> i = pc.iterator( ); i.hasNext( ); ) {
			    		 PostalCode postcode = i.next();
			    		 
			    		 str_CodeTo = Integer.toString(postcode.getCode_to()/1000);
			    		 String secondPartTo = Integer.toString(postcode.getCode_to()%1000);
				    	 String validatedCode_To = validateCode(str_CodeTo, secondPartTo);
			    		 
				    	 str_CodeFrom = Integer.toString(postcode.getCode_from()/1000);
				    	 String secondPartFrom = Integer.toString(postcode.getCode_from()%1000);
				    	 String validatedCode_From = validateCode(str_CodeFrom, secondPartFrom);
				    	 
				    	 
				    	 finalStr_CodeTo = finalStr_CodeTo + "" + validatedCode_From + " - " + validatedCode_To + "  ; ";	 
			    	 }
			    	 str_CodeTo = finalStr_CodeTo;
			     }
			 	HttpServletResponse response = 	ServletActionContext.getResponse();
				response.setContentType("text/html; charset=iso-8859-2");				
		    	PrintWriter out = response.getWriter();	   
		    	List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		    	Map<String,String> map = new HashMap<String,String>();
		    	map.put("code", str_CodeTo);
		    	result.add(map);
		    	out.write(toJSON(result));
		    	out.flush();
		    	out.close(); 
			 }
			 catch (Exception e) 
			 {
				 log.error(e.getMessage());
			 }                                                                                                                                         
		}

		@Override
		public Logger getLogger()
		{
			return LOG;
		}
	}
	
	private String toJSON(List<Map<String,String>> result)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Map<String, String> map : result) 
		{
			Set<String> keys = map.keySet();
			sb.append("{");
			for (String  key: keys) 
			{
				sb.append("\"").append(key).append(("\":\"")).append(map.get(key)).append("\",");
			}
			sb.delete(sb.length()-1, sb.length());
			sb.append("},");
		}
		sb.delete(sb.length()-1, sb.length());
		sb.append("]");
		return sb.toString();
	}
	
	 public String validateCode(String code, String secondPart) 
	 {
		 if(code.length()==1)
    	 {
    		 code = "0" + code + "-";
    	 }
    	 else
    	 {
    		 code = code + "-";
    	 }
    	 
    	 	if(secondPart.length()==0) code = code + "000" + secondPart;
    	 	if(secondPart.length()==1) code = code + "00" + secondPart;
    	 	if(secondPart.length()==2) code = code + "0" + secondPart;
    	 	if(secondPart.length()==3) code = code + secondPart;
    	 	
    	 	
    	 	return code;
	 }

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
