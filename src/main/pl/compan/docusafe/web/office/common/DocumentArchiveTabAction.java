package pl.compan.docusafe.web.office.common;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.api.user.office.TemplateService;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.certificates.ElectronicSignatureBean;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.certificates.ElectronicSignatureUtils;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.part.DSPartsConfig;
import pl.compan.docusafe.core.cfg.part.DSPartsManager;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.*;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.*;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessUtils;
import pl.compan.docusafe.core.dockinds.process.WorkflowUtils;
import pl.compan.docusafe.core.drools.DroolsUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.naming.StringResource;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.*;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.templating.DocumentSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.core.xes.XesLogVerification;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.integration.DocumentConverter;
import pl.compan.docusafe.parametrization.aegon.AbsenceRequestLogic;
import pl.compan.docusafe.parametrization.aegon.overtime.OvertimeAppLogic;
import pl.compan.docusafe.parametrization.aegon.overtime.OvertimeReceiveLogic;
import pl.compan.docusafe.parametrization.aegon.wf.WorkflowLogic;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.parametrization.ic.SadLogic;
import pl.compan.docusafe.parametrization.ilpoldwr.BinderLogic;
import pl.compan.docusafe.parametrization.pg.BarcodePool;
import pl.compan.docusafe.parametrization.pg.PgDocInLogic;
import pl.compan.docusafe.parametrization.presale.ZamowienieLogic;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.parametrization.utp.NormalLogic;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignature;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignatureEntity;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignatureManager;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;
import pl.compan.docusafe.service.zebra.ZebraPrinter;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.spring.user.office.TemplateServiceImpl;
import pl.compan.docusafe.spring.user.office.TemplateServicePgImpl;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.DrsInfo;
import pl.compan.docusafe.web.archive.repository.search.SearchDocumentsRequest;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.commons.*;
import pl.compan.docusafe.web.office.CloneNewVersion;
import pl.compan.docusafe.web.office.common.RemarksTabAction.RemarkBean;
import pl.compan.docusafe.web.office.out.SplitOutOfficeDocument;
import pl.compan.docusafe.webwork.event.*;
import std.pair;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public abstract class DocumentArchiveTabAction extends ProcessBaseTabsAction implements DwrOfficeDocument, BoxAction, DockindButtonAction, OfficeDocumentAction, RemarkableAction
{
    private static final Logger log = LoggerFactory.getLogger(DocumentArchiveTabAction.class);
	private static final DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
    private Map<String, Acceptance> generalAcceptancesToGo;
    
	// @EXPORT
	private FieldsManager fm;
	/**Tylko dla ILPL trzeba do tego zrobic mechanizm*/
	private ArrayList<EnumItem> avalilabeTypes;
	private Map<String, String> documentKinds;
	private boolean canReadDictionaries;
	private List<DrsInfo> drsDocs;
	private boolean canChangeDockind;
	/** czy dokument jest zablokowany podpisem */
	private boolean blocked;
	private boolean ifFinAccepted;
	private Boolean doWiadomosci;
    /** czy istnieje mo?liwo?? zapisywania dokumentu bez sprawdzenia poprawno?ci */
    private boolean forceSaveButton;

    private String startingProcessName;
    private Collection<ProcessDefinition> processesManuallyStarted;

	// @IMPORT
	protected Map<String, Object> values = new HashMap<String, Object>();
	private String acceptanceCn;
	private Map<Integer, String> centrumAcceptanceCn = new LinkedHashMap<Integer, String>();
	/** identyfikator obiektu, dla kt?rego wycofa? akceptacj? */
	private Long objectId;

	// @EXPORT/@IMPORT
	protected String documentKindCn;
	private InOfficeDocumentKind defaultKind;
	private String remark;
	private Map<String, Object> dockindKeys;
	private OfficeDocument document;
	private String folderPrettyPath;
	private Long folderId;
	private Long boxId;
	private String boxNumber;
	private Long currentBoxId;
	private String currentBoxNumber;
	private Long archiveBox;
	private boolean canClone;
	private boolean canUpdate;
	private boolean canAssignMe;
	private boolean needsNotBox;
	private boolean boxNumberReadonly;
	private boolean canAddToRS;
    private boolean electronicSignatureAvailable;
	private FinishOrAssignState finishOrAssignState;
	private boolean documentFax;
	private Long id;
	private boolean crm;
	private Collection<Map> attachments;
	private List<Flags.Flag> globalFlags;
	private List<Flags.Flag> userFlags;
	private List<pair<Label, Boolean>> hackedLabels;
	private List<Label> nonModifiableLabels;
	private Long[] globalFlag;
	private Long[] userFlag;
	private Long[] label;
	private boolean flagsPresent;
	private Remark lastRemark;
	private String lastRemarkAutchor;
	private Audit lastHistory;
	private String lastHistoryAutchor;
	private String newNote;
	private List<Adnotation> noteList;
	public boolean showSendToEva;
	private Map<String, String> substituted;
	private Map<String, String> assignmentsMap = Collections.emptyMap();
	private Map<String, String> wfProcesses;
	private List objectives;
	private Boolean showObjectives;
	private Boolean showReject;
	private String[] assignments;
	private Boolean showLine;
	private String evaMessage;
	private String content;
	private String returnAcceptance;
	private Long returnAcceptanceObject;
	private Long centrumToAcceptanceId;
	private Map<Long, Map<String, String>> userToNextAcceptance;
	private Map<String, String> acceptancesUserList;
	private Map<String, String> acceptancesCnList;
	private String discardCnAcceptance;
	private Boolean acceptanceToUser;
	private Boolean generalAcceptances;
	private Map<String, String> userToGeneralAcceptance;
	private Boolean finishOn;
	private Boolean discardOn;
	private List<RemarkBean> remarks;
	private boolean archiveWithoutLabel;
	private String czystopis;
	private boolean save;
	private boolean sign;
	private boolean czyCzystopis = false;
	private boolean czyUkryc = true;
	private boolean maPrawaDoOznaczaniaJakoCzystopis=true;
	private boolean maPrawaDoPodpisywania=true;
	private String location;
	private Boolean canReopenWf;

    public Map<String, String> availableTemplates;
	private Map<String, String> userToAcceptance;
	private String userAcceptance;
	private String discardUserAcceptance;

    //IC - pokazywanie zbudowanego konta kosztowego
    private boolean showFullAccount;
    private boolean rejectObjectAcceptanceAvailable;

	// rzeczy zwiazane z zewnetrznym Workflow JBPM
	private Map<Long, String> jbpmProcesses;
	private Long jbpmProcessId;
	private String jbpmLink;
	private String initJbpmLink;
	private boolean canInitJbpmProcess;
	private boolean acceptanceButtonsForCurrentUserVisible = true;

    private boolean noScheme = false;

	// akceptacje jbpm
	private boolean jbpmAcceptances = false;
	private Map<String, String> acceptanceNames;
	private CentrumKosztow centrum;
	private Set<String> cnsNeeded;
	private boolean showProcessActions;
	
	private List<Aspect> documentAspects;
	private String documentAspectCn;
    private List<DSUser> usersByDocumentToUpdate;
    private String droolsRuleCn;

	/** Uproszczona przyspieszona dekretacja */
	private boolean fastAssignment;

	// do wywalenia po prezentacji OBI
	private List<CentrumKosztow> centra;
	private Map<Integer, String> centraToRachunek;
	private Boolean setInitialValues;
	
	//nowa wersja przy aktualizacji
	private int staraWersja;
	private long stareGuid;
	private String nowyOpis;
	private Map copyAttachments = new HashMap();
	private String caseName;
	private boolean newVersion;
	
	private boolean pudloModify;

	private Boolean openViewer;
	private Map<String, AcceptanceCondition> acceptanceCondition;

	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			DocumentArchiveTabAction.class.getPackage().getName(), null);
	public static final String EV_FILL = "fill";
	public static final String EV_ARCHIVE = "arch";
	protected static final String EV_UPDATE = "update";
    public static final String EV_CHECK_DWR = "check-dwr";
    public static final Integer USLUGA_DORECZYCIEL = 50;
	public static final Integer USLUGA_SKRYTKA = 51;
	
	/**
	 * Wywoluje na dokumencie metod? powiadomienia mailowego
	 * Wywolywana na przycisku Wyslij powiadomienie.
	 * PRzycisk jest uwidaczniany po dodaniu do dockinda: dockind.powiadomienie.enabled
	 */
	protected boolean mailNotification = false;
    /**
     * Wywoluje na dokumencie metode kt�ra powina zawiera� akcje wys�ania do zewn�trznego repozytorium
     */
    protected boolean sendToExternalRepo = false;
    /**
     * warto�� wymagana do wys�ana do zewn�trznego repo np. id folderu
     */
    protected String propUuid;

	private Collection<RenderBean> processRenderBeans;

	private boolean manualToCoordinator = true;
	private String dockindEventValue;

	protected static OfficeDocumentHelper officeDocumentHelper = new OfficeDocumentHelper();
	
	/**
	 * mapa z mozliwymi osobami do dekretacji wniosku za pomoca akceptacji, dla AEGON
	 */
	private Map<String, String> acceptancesMap = new LinkedHashMap<String, String>();
	private String userName;
	private boolean returnToTasklist = false;
	private boolean splitAndAssignOfficeNumber = false;
	private boolean canAssignOfficeNumber=false; 
	private boolean documentHaveOfficeNumber =false;
    //URL do powrotu na t? stron?
    private String returnUrl;
    
    private boolean hasOnlyUpdateFields;

    protected String mobileReferrer;

    @Override
    public String modifyResult(String result) {
        if(mobileView) {
            if(SUCCESS.equals(result)) {
                return "mobile";
            }

            if("task-list".equals(result)) {
                mobileRedirect = result;
                return "mobile";
            }
        }
        return result;
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm(false);
        JsonView jsonView = new JsonView();

        registerListener("doCreateDocument")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(new CreateDocument())
//                .append(new FillForm(false))
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener(DEFAULT_ACTION)
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(fillForm)
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("jsonView").
                append(OPEN_HIBERNATE_AND_JBPM_SESSION).
                append(fillForm).
                append(jsonView).
                appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("processActions").
                append(OPEN_HIBERNATE_AND_JBPM_SESSION).
                append(fillForm).
                append(new ProcessActions()).
                appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doExecuteDroolsRule")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append(new ExecuteDroolsRule())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doProcessAction")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new DocumentSaveSkipper())
            .append(EV_CHECK_DWR, checkDwrVal())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append("PROCESS", new ProcessAction().skipListener(EV_FILL))
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doProcessJsonAction")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new DocumentSaveSkipper())
            .append(EV_CHECK_DWR, checkDwrVal())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append("PROCESS", new ProcessAction().skipListener(EV_FILL))
            .append(EV_FILL, new FillForm(false))
            .append(jsonView)
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doDockindEventProcessAction")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append(new DockindEvent(this))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doManualStartProcess")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(EV_ARCHIVE, new Archive())
            .append(new ProcessManualStart())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doManualStartProcessJson")
                .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
                .append(checkDwrVal())
                .append(EV_ARCHIVE, new Archive())
                .append(new ProcessManualStart())
                .append(EV_FILL, new FillForm(false))
                .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doChangeDockind")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new FillForm(true))
            .append(new XesLogVerification())
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doForceArchive")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .appendAdminCheck()
            .append(archiveNoValidate())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doArchive")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doArchiveGoToTasklist")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new ArchiveGoToTasklist())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doTmp")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doClone").append(OpenHibernateSession.INSTANCE)
            .append(new Clone())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdateBox")
            .append(OpenHibernateSession.INSTANCE)
            .append(new UpdateBox())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doManualFinish")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new ManualFinish())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doArchiveNote")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new ArchiveNote())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doDiscard")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Discard())
            .append(new XesLogVerification())
            .append(new ManualFinish())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doDiscardTo").append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new DiscardTo())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doDiscardToIM")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new DiscardToIM())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("sendToEva").append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new SendToEva())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doAssignments")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new Assignments())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doArchiveAssignments")
            .append(OpenHibernateSession.INSTANCE)
            .append(new Archive())
            .append(new Assignments())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doReject")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Reject())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doWithdraw")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new WithdrawDocument())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doGiveAcceptance")
            .append(OpenHibernateSession.INSTANCE)
            .append(new Archive())
            .append(new GiveAcceptance())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doWithdrawAcceptance")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new WithdrawAcceptance())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doSendToDecretation")
            .append(OpenHibernateSession.INSTANCE)
            .append(new sendToDecretation())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		// akceptacja intercarsa
		registerListener("doNextDecretation")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new nextDecretation())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		// pe?na akceptacja intercarsa
		registerListener("doFullAcceptance")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new FullAcceptance())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        //odrzucanie akceptacji dla centrum IC
        registerListener("doDiscardObjectAcceptance")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new Archive())
            .append(new DiscardObjectAcceptance())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doUpdateFlags")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new UpdateFlags())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doAssignMe")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new AssignMe())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doReopenWf")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new ReopenWf())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doDockindEvent")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new ValidateCreate())
            .append(new DockindEvent(this))
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doManualToCoordinator")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(new ManualPushToCoor())
            .append(new XesLogVerification())
            .append(EV_FILL, new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doUpdate")
            .append(OpenHibernateSession.INSTANCE)
            .append(checkDwrVal())
            .append("UpdateFlags", new UpdateFlags())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("sendToExternalRepo")
                .append(OpenHibernateSession.INSTANCE)
                .append(checkDwrVal())
                .append("UpdateFlags", new UpdateFlags())
                .append(EV_ARCHIVE, new Archive())
                .append(new XesLogVerification())
                .append(new FillForm(false))
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdateJson")
                .append(OpenHibernateSession.INSTANCE)
                .append(checkDwrVal())
                .append("UpdateFlags", new UpdateFlags())
                .append(EV_ARCHIVE, new Archive())
                .append(new XesLogVerification())
                .append(new FillForm(false))
                .append(jsonView)
                .appendFinally(CloseHibernateSession.INSTANCE);


		registerListener("doUpdateGoToTasklist")
            .append(OpenHibernateSession.INSTANCE)
            .append(new ValidateCreate())
            .append("UpdateFlags", new UpdateFlags())
            .append(EV_UPDATE, new Archive())
            .append(new XesLogVerification())
            .append(new ArchiveGoToTasklist())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doPrint")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(EV_ARCHIVE, new Archive())
            .append(new Print())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);


        registerListener("doPrintBarcodePool")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(new PrintBarcodePool())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		
		registerListener("doCzystopis")
			.append(OpenHibernateSession.INSTANCE)
			.append(new OznaczCzystopis())
			.append(new XesLogVerification())
			.append(new FillForm(false))
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSign")
			.append(OpenHibernateSession.INSTANCE)
			.append(new InternalSign())
			.append(new XesLogVerification())
			.append(new FillForm(false))
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSaveNewVersion")
			.append(OpenHibernateSession.INSTANCE)
			.append(checkDwrVal())
			.append(EV_ARCHIVE, new SaveNewVersion())
			.append(new XesLogVerification())
			.append(new FillForm(false))
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSplitDocument")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(new SplitDocument())
            .append(new XesLogVerification())
            .append(EV_ARCHIVE, new Archive())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doSplitAndAssingOficeNumber")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(new splitAndAssignOfficeNumber())
            .append(new SplitDocument())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doSaveAndAssignOfficeNumber")
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(checkDwrVal())
            .append(new AssignOfficeNumber())
            .append(EV_ARCHIVE, new Archive())
            .append(new XesLogVerification())
            .append(new ArchiveGoToTasklist())
            .append(new FillForm(false))
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doClose")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new Close())
			.append(new FillForm(false))
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doAlternativeUpdate")
            .append(OpenHibernateSession.INSTANCE)
            .append(checkDwrVal())
            .append("UpdateFlags", new UpdateFlags())
            .append(new AlternativeUpdate())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doWyslijDoEpuap")
            .append(OpenHibernateSession.INSTANCE)
            .append(checkDwrVal())
            .append(new SendToEpuap())
            .append(new XesLogVerification())
            .append(new FillForm(false))
            .appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	public abstract List prepareTabs();

	protected abstract String getExternalWorkflowBaseLink();

	private ActionListener checkDwrVal() {
		if (AvailabilityManager.isAvailable("dwr")) {
			return new ValidateCreate();
        } else {
            return new DefaultActionListener();
        }
	}
	
	private ActionListener checkDwrUp() {
		if (AvailabilityManager.isAvailable("dwr")) {
			return new Update();
        } else {
            return new DefaultActionListener();
        }
	}

        
    /**
     *  Funkcja u?ywana do pobierania ustawionych wiadomo?ci w  logice dokumentu poprzez sesj?
     * @return void
     */    
    private void addFlashActionMessage() {
        String sessionFlashMessage = (String)DSHttpContext.getHttpFlashAttribute("document.archive.flash.message");
        if(sessionFlashMessage != null) {
            addActionMessage(sessionFlashMessage);
        }
    }

    private class DefaultActionListener extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {}

        @Override
        public Logger getLogger() {
            return log;
        }
    }
	/**
	 * @param manualToCoordinator the manualToCoordinator to set
	 */
	public void setManualToCoordinator(boolean manualToCoordinator) {
		this.manualToCoordinator = manualToCoordinator;
	}

    private class ExecuteDroolsRule extends TransactionalActionListener {
        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            log.info("EXECUTE DROOLS RULE cn = " + droolsRuleCn);
            assert droolsRuleCn != null && getDocumentId() != null;
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            DroolsUtils.invokeDroolsButton(getDocumentId(), droolsRuleCn);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class DocumentSaveSkipper implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            LoggerFactory.getLogger("tomekl").debug("TmpSkip");
            if(AvailabilityManager.isAvailable("blockAtributesIfCC", null, getActivity())) {
                try {
                    DSApi.context().begin();
                    checkRemark();
                    DSApi.context().commit();
                } catch (Exception e) {
                    DSApi.context()._rollback();
                }

                event.skip(EV_CHECK_DWR);
                event.skip(EV_ARCHIVE);
            }
        }

        private void checkRemark() throws EdmException {
            if (newNote != null || (content != null && !content.equals(sm.getString("WpiszTrescUwagi")) && !TextUtils.latinize(content).equals("Wpisz tre__ uwagi"))) {
                if (content != null) {
                    document.addRemark(new Remark(StringUtils.left(content, 4000),DSApi.context().getPrincipalName()));
                } else if (newNote != null) {
                    document.addRemark(new Remark(StringUtils.left(newNote, 4000),DSApi.context().getPrincipalName()));
                }
                addActionMessage(sm.getString("DodanoUwage"));
            }
        }
    }
    
    private class DockindEvent implements ActionListener {
		private DockindButtonAction eventActionSupport;

		public DockindEvent(DockindButtonAction eventActionSupport) {
			this.eventActionSupport = eventActionSupport;
		}

		public void actionPerformed(ActionEvent event)  {
			try  {
				getDocument();
				document.getDocumentKind().logic().doDockindEvent(eventActionSupport,event,document, getActivity(), getValues(),getDockindKeys());
			} catch (Exception e) {
				log.error("B??d wykonywania akcji " + getDockindEventValue(), e);
				addActionError(e.getMessage());
			}
		}
	}
    
	private class Update extends TransactionalActionListener{
		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			try {
				dwrDocumentHelper.update(DocumentArchiveTabAction.this);
			} catch (Exception e) {
				if (event.hasListener(EV_ARCHIVE))
					event.skip(EV_ARCHIVE);
				if (event.hasListener("PROCESS"))
					event.skip("PROCESS");
				
				throw e;
			}
		}
		
		@Override
		public Logger getLogger() {
			return log;
		}
	}
	
	private class Opinion extends TransactionalActionListener{
		
		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception 
		{
			try {
				dwrDocumentHelper.update(DocumentArchiveTabAction.this);
			} catch (Exception e) {
				if (event.hasListener("UpdateFlags"))
						event.skip("UpdateFlags");
				if (event.hasListener(EV_ARCHIVE))
					event.skip(EV_ARCHIVE);
				if (event.hasListener("PROCESS"))
					event.skip("PROCESS");
				
				throw e;
			}
		}
		
		@Override
		public Logger getLogger() {
			return log;
		}
	}
	
	private class ValidateCreate extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception  {
        	log.info("loading dwr fields");
            setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class ArchiveGoToTasklist implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                returnToTasklist = true;
                event.setResult("task-list");
            } catch (Exception e) {
                log.error("B??d wykonywania akcji " + getDockindEventValue(), e);
                addActionError(e.getMessage());
            }
        }
    }

    private class ProcessManualStart extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            log.info("process start " + startingProcessName);
            document = OfficeDocument.find(getDocumentId());
            document.getDocumentKind()
                    .getDockindInfo()
                    .getProcessesDeclarations()
                    .getProcess(startingProcessName)
                    .getLogic()
                    .startProcess(document, Collections.<String, Object>emptyMap());
        }

        @Override
        public void handleException(Exception e, ActionEvent event) {
            super.handleException(e, event);
            event.skip(EV_ARCHIVE);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

	private class WithdrawDocument extends TransactionalActionListener{

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			WorkflowFactory.manualFinish(getDocumentId(), false);
			getDocument().getDocumentKind().setOnly(getDocumentId(),
					Collections.singletonMap(InvoiceICLogic.STATUS_FIELD_CN, (Object)666));
			event.setResult(getTaskListResult());
		}

		@Override
		public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			DSApi.context().session().flush();
			event.addActionMessage("Dokument wycofano");
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}

    public Document getNewDocument() {
        return dwrDocumentHelper.getNewDocument(DocumentArchiveTabAction.this);
    }

    private class JsonView extends AjaxSerializeActionListener {

        @Override
        public Object serializeAction(ActionEvent event) throws Exception {
            Map<String, Object> ret = new HashMap<String, Object>();

            ret.put("document", new DocumentView(document));
            ret.put("documentKindCn", documentKindCn);
            ret.put("documentKindName", DocumentKind.findByCn(documentKindCn).getName());
            ret.put("activity", getActivity());
            ret.put("taskListResult", getTaskListResult());
//            ret.put("processRenderBeansTemplates", FluentIterable.from(processRenderBeans).transform(new Function<RenderBean, String>() {
//                @Nullable
//                @Override
//                public String apply(@Nullable RenderBean input) {
//                    return input.getTemplate();
//                }
//            }).toList());

            return ret;
        }
    }

    private class ProcessActions extends RichActionListener {

        @Override
        public void action(ActionEvent event) throws Exception {
            event.setResult("process-actions");
        }
    }

    private class FillForm implements ActionListener
    {
        private boolean changeDockind;

        public FillForm(boolean changeDockind)
        {
            this.changeDockind = changeDockind;
            
        }

        public void actionPerformed(ActionEvent event)
        {
        	
        	log.trace("FillForm");
            fastAssignment = AvailabilityManager.isAvailable("archiwizacja.szybkaDekretacja");
        	logRequest(log);
            setTabs(prepareTabs());

            try
            {
            	/**DWR NEW DOCKIND*/
                dwrDocumentHelper.fillForm(DocumentArchiveTabAction.this);
                document = OfficeDocument.findOfficeDocument(getDocumentId());
                document.getSender();
                if(AvailabilityManager.isAvailable("forceSaveButton")){
                    forceSaveButton = DSApi.context().isAdmin();
                }
                showProcessActions = SnapshotUtils.isAssignedToUser(DSApi.context().getPrincipalName(), getDocumentId());
                if (AvailabilityManager.isAvailable("kancelaria.szablony.simple")){
                    JsonArray jsonArray = getTemplateService().getAvailableTemplates(document.getDocumentKind().getCn());
                    availableTemplates = new LinkedHashMap<String, String>(jsonArray.size());
                    for(JsonElement jsonElement: jsonArray){
                        JsonObject json = jsonElement.getAsJsonObject();
                        String documentName = json.get("name").getAsString();
                        Boolean isUserTemplate = jsonElement.getAsJsonObject().get("isUserTemplate").getAsBoolean();
                        availableTemplates.put(documentName,(isUserTemplate?"(U�ytkownika) ": "")+ documentName);
                    }
                }

                if(WorkflowFactory.jbpm) {
                    if(getActivity() != null){
                        processRenderBeans = ProcessUtils.renderProcessesForActivityId(document, getActivity(), ProcessActionContext.VIEW);
                    } else if(!AvailabilityManager.isAvailable("process.dontShowAll")) {
                        //jak nie ma activity to wy?wietlaj w archiwizacji tak jak w przypadku archiwum (ARCHIVE_VIEW)
                        processRenderBeans = ProcessUtils.renderAllProcesses(document, ProcessActionContext.ARCHIVE_VIEW);
                    }
                } else {
                    processRenderBeans = ProcessUtils.renderAllProcesses(document, ProcessActionContext.VIEW);
                }
                
                processesManuallyStarted = document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getManuallyStarted(document);
                
                prepareAttachments(document);
                folderPrettyPath = document.getFolderPath();
                folderId = document.getFolderId();
                canClone = AvailabilityManager.isAvailable("archiwizacja.klonowanie");
                
                canUpdate = document.canModify();
                
                if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin())
                	canUpdate = PermissionManager.isDocumentInTaskList(document);
                
                setCanAssignMe(!AvailabilityManager.isAvailable("canAssignMe", document.getDocumentKind().getCn()) ? false : WorkflowFactory.getInstance().checkCanAssignMe(getDocumentId(), getActivity()));
                finishOn = true;

                canChangeDockind = DSApi.context().hasPermission(DSPermission.DOKUMENT_DOCUSAFE_ZMIANA_RODZAJU);                
                if(document != null && document.getDocumentKind() != null){
                	canChangeDockind = document.getDocumentKind().logic().canChangeDockind(document);
                }

                // pobranie listy wszystkich dostepnych rodzaj?w dokument?w
                List<DocumentKind> docKinds = DocumentKind.listForEdit();
                docKinds = DocumentKind.filterByCreatingPermission(docKinds, documentKindCn);
                documentKinds = new LinkedHashMap<String,String>();
                DocumentKind documentKind;

                for (DocumentKind docKind : docKinds){
                    documentKinds.put(docKind.getCn(), docKind.getName());
                }

                if (documentKindCn == null) {
                    documentKind = document.getDocumentKind() != null ? document.getDocumentKind() :
                    DocumentKind.findByCn(DocumentKind.getDefaultKind());
                    documentKindCn = documentKind.getCn();
                } else {
                    documentKind = DocumentKind.findByCn(documentKindCn);
                }

                if(ElectronicSignatureUtils.isAvailable(documentKind) && ElectronicSignatureStore.getInstance().findSignaturesForDocument(documentId).size()==0)
                	electronicSignatureAvailable = true;

                // pobranie warto?ci dla danego dokumentu
                // 
                fm = documentKind.getFieldsManager(documentId).withActivityId(getActivity());
                fm.initialize();
                fm.initializeAcceptances();
                if("dlbinder".equals(documentKindCn) && DSApi.context().getDSUser().inDivisionByGuid("DL_BINDER_DO"))
        		{
                	avalilabeTypes = BinderLogic.getAvalilabeTypes(fm);
        		}
                
                if(noScheme) {
                    if(!DSApi.context().isAdmin()){
                        event.addActionError("Nie masz uprawnie? do NoScheme");
                    } else {
                        fm.setDockindScheme(null);
                    }
                }
                
                if(AvailabilityManager.isAvailable("archiwizacja.uwagi")
                        || AvailabilityManager.isAvailable("archiwizacja.uwagi",documentKindCn)
                        || AvailabilityManager.isAvailable("archiwizacja.uwagi.dol")
                        || AvailabilityManager.isAvailable("archiwizacja.uwagi.dol",documentKindCn)) {
                	log.trace("pobieram uwagi do dokumentu");
                	officeDocumentHelper.retrieveRemarks(DocumentArchiveTabAction.this);
                }

                initDocumentAspects(documentKind);
                
                if(document.getDocumentKind() != null && fm.getAcceptancesState() != null) 
                {
                	// mozliwe akceptacje do wykonania
                    if(AvailabilityManager.isAvailable("generalAcceptances.ic")){
                        generalAcceptancesToGo = Collections.emptyMap();
                    } else { //poni?szy kod wykonuje si? bardzo d?ugo (~5 sekund), wywalam z IC
                	    generalAcceptancesToGo = fm.getAcceptancesState().getGeneralAcceptancesDef();
                        // lista z mozliwymi osobami do dekretacji akceptacji wniosku urlopowego dla AEGON
                        if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.ABSENCE_REQUEST))
                        {
                            AbsenceRequestLogic requestLogic = ((AbsenceRequestLogic) document.getDocumentKind().logic());
                            acceptancesMap = requestLogic.getAcceptancesMap(document);
                        }
                    }
                }

                overtimeStuff();

                if(document.getDocumentKind() != null && (document.getDocumentKind().getCn().equals(DocumentLogicLoader.ZAMOWIENIE)))
                {               	
                	centra = CentrumKosztow.listAll();
                	if(centra != null && centra.size() > 0)
                	{
                		setCentraToRachunek(new HashMap<Integer, String>());
	                	for (CentrumKosztow ck : centra)
						{
							getCentraToRachunek().put(ck.getId(),ZamowienieLogic.getNrRachunku(ck.getId()));
						}
                	}
                }
                
                if(documentKindCn.equals(DocumentLogicLoader.INVOICE_IC_KIND)
                        || documentKindCn.equals(DocumentLogicLoader.SAD_KIND)){
					prepareForIC();
                }

                // sprawdzanie czy mo?na czyta? s?owniki na stronie JSP
                canReadDictionaries = documentKind.logic().canReadDocumentDictionaries();
                
                // gdy zmieniamy typ (i dokument nie jest w?a?nie tego typu), to ustalam warto?ci pocz?tkowe
                if ( (setInitialValues != null && setInitialValues) || (changeDockind && (document.getDocumentKind() == null || !documentKindCn.equals(document.getDocumentKind().getCn()))))
                    documentKind.logic().setInitialValues(fm, DocumentLogic.TYPE_ARCHIVE);

                // jak byl blad, to powrot na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
                if (hasActionErrors())
                {
                    fm.reloadValues(values);
                }

                // inicjalizacja na potrzeaby zewnetrzengo workflow JBPM
                canInitJbpmProcess = JbpmManager.canInitJbpmProcess(document, getActivity());
                if (Configuration.workflowAvailable())
                {
                    jbpmProcesses = JbpmManager.prepareJbpmProcesses();
                    jbpmProcessId = JbpmManager.defaultProcess(document);

                    String[] params = new String[] {
                        "documentId", String.valueOf(getDocumentId()),
                        "activity", getActivity(),
                        "process", getProcess()};

                    String tmpLink = HttpUtils.makeUrl(getExternalWorkflowBaseLink(), params);
                    initJbpmLink = tmpLink + "&doInitJbpmProcess=true";
                    jbpmLink = tmpLink + "&doShowTask=true";
                }                  

                officeDocumentHelper.initBox(DocumentArchiveTabAction.this);

                if (StringUtils.isNotEmpty(getActivity())) {
                    prepareCollectiveAssignments();
                } else {
                	doWiadomosci = false;
                	showObjectives = false;
                }

                setExternalWorkflow(WorkflowFactory.jbpm);

                if (!isExternalWorkflow()){
                    internalWorkflowStuff();
                } else {
                	canReopenWf = false;
                }
                documentFax = "fax".equals(document.getSource());

                if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                {
                    blocked = (document.getBlocked() != null && document.getBlocked());
                }
                else
                    blocked = false;
                
                if(AvailabilityManager.isAvailable("blockAtributesIfCC", null, getActivity()) && doWiadomosci)
                {
                	blocked = true;
                }

                canAssignOfficeNumber = DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO);
                if ( getDocument().getOfficeNumber()!=null && !getDocument().getFormattedOfficeNumber().equals("Brak") )
            		   documentHaveOfficeNumber = true;
                //Dodatkowe dla rockwella 
                String flagsOn = Configuration.getProperty("flags");
                if ("true".equals(flagsOn))
                {
                	globalFlags = document.getFlags().getDocumentFlags(false);

                    userFlags = document.getFlags().getDocumentFlags(true);

                    flagsPresent = globalFlags.size() > 0 || userFlags.size() > 0;
                    
                    if (AvailabilityManager.isAvailable("labels")) {
	                    List<Label> labels = LabelsManager.getWriteLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
	                    List<Label> docLabels = LabelsManager.getReadLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
	                    setNonModifiableLabels(LabelsManager.getLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE, true, false, false));
	                    hackedLabels = new LinkedList<pair<Label, Boolean>>();
	                    for (Label lab : labels) {
	                    	hackedLabels.add(new pair<Label, Boolean>(lab, docLabels.contains(lab)));
	                    }
                    }
                    
                }
                else
                    flagsPresent = false;

                if(AvailabilityManager.isAvailable("archiwizacja.historia") && document.getRemarks()!= null && document.getRemarks().size() > 0 )
                {
                	int j =document.getRemarks().size();
                	lastRemark = document.getRemarks().get(j-1);
                	lastRemarkAutchor = DSUser.findByUsername(lastRemark.getAuthor()).asLastnameFirstname();
                }
                noteList = Adnotation.list();

                lastHistory = document.getLastHistoryEntry();
                if (lastHistory!=null)
                {
                	try
                	{
                		lastHistoryAutchor = DSUser.findByUsername(lastHistory.getUsername()).asLastnameFirstname();
                	}
                	catch (UserNotFoundException e) 
                	{
						//Zmiana etykiety przez system idzie do historii 
                		//ale nie pomyslano o tym ze nie znajdzie pozniej uzytkownika #system#                		
                		lastHistoryAutchor = lastHistory.getUsername();
					}
                }

                showSendToEva = false;
                showReject = false;
                if (document.getDocumentKind() != null && document.getDocumentKind().getCn() != null &&
                		document.getDocumentKind().getCn().equals(DocumentLogicLoader.ROCKWELL_KIND))
                {
                	showReject = true;
                	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId()).withActivityId(getActivity());
                	Integer tmp = (Integer)fm.getKey(RockwellLogic.EVA_STATUS_FIELD_CN);
                	if(tmp == null || tmp == 0)
                	{
                		evaMessage = null;
                		showSendToEva = true;
                	}
                	else
                	{
                		evaMessage = "Document was sent to Eva";
                	}
                }
                
                if (StringUtils.isEmpty(getActivity())) {
                	finishOn = false;
                	showReject = false;
                }
                
                if(AvailabilityManager.isAvailable("archiwizacja.flagi") || 
                		AvailabilityManager.isAvailable("archiwizacja.zalaczniki") || 
                		AvailabilityManager.isAvailable("archiwizacja.historia") || 
                		AvailabilityManager.isAvailable("archiwizacja.uwagi") || 
                		AvailabilityManager.isAvailable("archiwizacja.dekretacja")) 
                	showLine = true;

                /*
                 *  zablokowanie mozliwosci zapisu
                 *  dla pism wychodzacych mozliwosc zapisu oraz edycji pola "otrzymano zwrotke"
                 */
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.oznaczJakoCzystopis")){
                	Document doc = Document.find(documentId);
                	if(doc.getCzyCzystopis()){
                		setCzystopis("Czystopis");
                		save=true;
                		czyCzystopis=true;
                		if(doc.getDocumentKind().getCn().equals("normal_out")){
                            if (fm.containsField("RODZAJ_PISMA_OUT"))
                                fm.getField("RODZAJ_PISMA_OUT").setReadOnly(true);
                            if (fm.containsField("DOC_DESCRIPTION"))
                                fm.getField("DOC_DESCRIPTION").setReadOnly(true);
                            if (fm.containsField("SENDER_HERE"))
                                fm.getField("SENDER_HERE").setReadOnly(true);
                            if (fm.containsField("DOC_DATE"))
                                fm.getField("DOC_DATE").setReadOnly(true);
                            if (fm.containsField("POSTAL_REG_NR"))
                                fm.getField("POSTAL_REG_NR").setReadOnly(true);
                            if (fm.containsField("GABARYT"))
                                fm.getField("GABARYT").setReadOnly(true);
                            if (fm.containsField("BARCODE"))
                                fm.getField("BARCODE").setReadOnly(true);
                            if (fm.containsField("potwierdzenie_odbioru"))
                                fm.getField("potwierdzenie_odbioru").setReadOnly(true);
                            if (fm.containsField("DOC_DELIVERY"))
                                fm.getField("DOC_DELIVERY").setReadOnly(true);
                            if (fm.containsField("sposob_obslugi"))
                                fm.getField("sposob_obslugi").setReadOnly(true);
                            if (fm.containsField("DOC_FORM"))
                                fm.getField("DOC_FORM").setDisableSubmitDictionary(true);
                            if (fm.containsField("WAGA"))
                                fm.getField("WAGA").setReadOnly(true);
                            if (fm.containsField("DOC_NUMBER"))
                                fm.getField("DOC_NUMBER").setReadOnly(true);
                            if (fm.containsField("RECIPIENT"))
                                fm.getField("RECIPIENT").setReadOnly(true);
                            if (fm.containsField("JOURNAL"))
                                fm.getField("JOURNAL").setReadOnly(true);
                			save=false;
                		}
                	}else{
                		setCzystopis("Brudnopis");
                		czyCzystopis=false;
                		if(doc.getDocumentKind().getCn().equals("normal_out")){
                            if (fm.containsField("RODZAJ_PISMA_OUT"))
                			    fm.getField("RODZAJ_PISMA_OUT").setReadOnly(false);
                            if (fm.containsField("DOC_DESCRIPTION"))
                			    fm.getField("DOC_DESCRIPTION").setReadOnly(false);
                            if (fm.containsField("SENDER_HERE"))
                                fm.getField("SENDER_HERE").setReadOnly(false);
                            if (fm.containsField("DOC_DATE"))
                                fm.getField("DOC_DATE").setReadOnly(false);
                            if (fm.containsField("POSTAL_REG_NR"))
                                fm.getField("POSTAL_REG_NR").setReadOnly(false);
                            if (fm.containsField("GABARYT"))
                                fm.getField("GABARYT").setReadOnly(false);
                            if (fm.containsField("BARCODE"))
                                fm.getField("BARCODE").setReadOnly(false);
                            if (fm.containsField("potwierdzenie_odbioru"))
                                fm.getField("potwierdzenie_odbioru").setReadOnly(false);
                            if (fm.containsField("DOC_DELIVERY"))
                                fm.getField("DOC_DELIVERY").setReadOnly(false);
                            if (fm.containsField("sposob_obslugi"))
                                fm.getField("sposob_obslugi").setReadOnly(false);
                            if (fm.containsField("DOC_FORM"))
                                fm.getField("DOC_FORM").setReadOnly(false);
                            if (fm.containsField("WAGA"))
                                fm.getField("WAGA").setReadOnly(false);
                		}
                	}
                }

                if(AvailabilityManager.isAvailable("kancelaria.wlacz.podpisWewnetrzny")){
                	DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
                	long loginId=user.getId();
                	if(InternalSignature.getInstance().isSignByUser(loginId, documentId)){
                		setSign(true);	
                	}

                }
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.LokalizacjaDokumentu")){
                	List<Audit> documentHistory = DataMartManager.getWorkHistory(document);
                	for(Audit audit: documentHistory ){
                		if(audit.getProperty().equals("barcode")){
                			DSUser user = DSUser.findByUsername(audit.getUsername());
                			setLocation(user.getFirstname() + " " + user.getLastname() + " (" + audit.getUsername() + ") " + audit.getCtime());
                		}
                	}
                }
                if(AvailabilityManager.isAvailable("UTP.mail.wyswietl.sprawy")){
                	if(document.getContainingCaseId() != null){
                		PreparedStatement ps = null;
                		String nazwy = "";
                		ps = DSApi.context().prepareStatement("select CASEDOCUMENTID from DSO_OUT_DOCUMENT where MASTER_DOCUMENT_ID = ?");
            			ps.setLong(1, document.getId());
            			ResultSet rs = ps.executeQuery();
            			while(rs.next()){
            			if (rs.getString("CASEDOCUMENTID")!=null)
            			{
            				nazwy += ",  ";
            				nazwy += rs.getString("CASEDOCUMENTID");
            			}
            			}
            			rs.close();
            			DSApi.context().closeStatement(ps);
                		OfficeCase.find(document.getContainingCaseId());
                		OfficeDocument doc = null;
                		if(document.getMasterDocumentId() != null){
                			doc = OfficeDocument.find(document.getMasterDocumentId());
                			doc.getMasterDocumentId();
                			doc.getCaseDocumentId();
                		}
                		
                		setCaseName(document.getCaseDocumentId() + nazwy);
                	}
                }
                
                //zmiana przycisku zapisz dla wymuszania nowej wersji dokumentu
                if(AvailabilityManager.isAvailable("INS.wlacz.automatyczneTworzenieNowejWersji") 
                		&& AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu") 
                		&& !documentKind.getCn().equalsIgnoreCase("email")){
                	//ustawienie opisu wersji dokumentu
                	if(document.getVersionDesc() != null){
                     	setNowyOpis(document.getVersionDesc());
                	}
                	setNewVersion(true);
                } else {
                	setNewVersion(false);
                }
                
                hasOnlyUpdateFields = isHasOnlyUpdateFields();
                
                /*
                 * Ukrycie przycisku klonuj, kopiuj, przeka� do koordynatora dla typ�w wskazanych
                 *  w addsie kancelaria.przyciskiDoUkrycia
                 */
                if(AvailabilityManager.isAvailable("kancelaria.wlacz.ukrywaniePrzyciskow")){
                	String name = Docusafe.getAdditionProperty("kancelaria.przyciskiDoUkrycia");
                	if(name != null){
                		String[] codeName= name.split(", ");
                		for(int i=0; i<codeName.length; i++){
                			if(documentKind.getCn().equalsIgnoreCase(codeName[i])){
                				setManualToCoordinator(false);
                				setCzyCzystopis(false);
                				setCzyUkryc(false);
                				setNewVersion(false);
                				break;
                			} else {
                				setManualToCoordinator(true);
                				setCzyCzystopis(true);
                				setCzyUkryc(true);
                			}
                		}
                	}

                }
            	//jesli nie ma praw, to ukryj przyciski
            	if(AvailabilityManager.isAvailable("podpisWewnetrzny.sprawdzajPrawaDostepu")&&!DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE)){
            		setMaPrawaDoPodpisywania(false);
            	}
            	if(AvailabilityManager.isAvailable("oznaczJakoCzystopis.sprawdzajPrawaDostepu")&&!DSApi.context().hasPermission(DSPermission.OZNACZ_JAKO_CZYSTOPIS)){
            		setMaPrawaDoOznaczaniaJakoCzystopis(false);
            	}

                document.eventDocumentView();
            }
			catch (Exception e)
			{
				event.addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
            
            addFlashActionMessage();
		}

        private void overtimeStuff() throws EdmException {
            //lista akceptacji dla dokumentu zgloszenie nadgodzin
            if ( document.getDocumentKind() != null && document.getDocumentKind().getCn().equals(DocumentKind.OVERTIME_APP))
            {
                OvertimeAppLogic overtimeLogic = ( (OvertimeAppLogic)document.getDocumentKind().logic());
                acceptancesMap = overtimeLogic.getAcceptancesMap(document);
            }

            //lista akceptacji dla dokumentu zgloszenie nadgodzin
            if ( document.getDocumentKind() != null && document.getDocumentKind().getCn().equals(DocumentKind.OVERTIME_RECEIVE))
            {
                OvertimeReceiveLogic overtimeLogic = ( (OvertimeReceiveLogic)document.getDocumentKind().logic());
                acceptancesMap = overtimeLogic.getAcceptancesMap(document);
            }
            //Lista akcpetacji dla dokumentu zg?oszenie nadgodzin
            if ( document.getDocumentKind() != null && document.getDocumentKind().getCn().equals(DocumentKind.WORKFLOW_WF))
            {
                WorkflowLogic wfLogic = ( (WorkflowLogic)document.getDocumentKind().logic());
                acceptancesMap = wfLogic.getAcceptancesMap(document);
            }
        }

        private void initDocumentAspects(DocumentKind documentKind) throws EdmException {
            documentAspects = documentKind.getAspectsForUser(DSApi.context().getPrincipalName());
            if (documentAspects == null) {
                documentAspectCn = null;
            } else if (documentAspects != null && documentAspectCn == null) {
                documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
            }
        }

        //metoda d?ugo si? wywo?uje (~ 1 sekunda)
        private void prepareCollectiveAssignments() throws EdmException {
            doWiadomosci = !WorkflowFactory.getInstance().isManual(getActivity());

            Map<String,Map<String,String>> results = WorkflowFactory.getInstance().prepareCollectiveAssignments(getActivity());
            if (results != null)
            {
                 assignmentsMap = results.get("assignmentsMap");
                 substituted = results.get("substituted");
                 wfProcesses = results.get("wfProcesses");
            }

            if (DSApi.context().userPreferences().node("other").getBoolean("multiAssignmentObjective", false) || AvailabilityManager.isAvailable("archiwizacja.dekretacja.showObjectives"))
            {
                objectives = AssignmentObjective.list();
                showObjectives = true;
            }

            manualToCoordinator &= WorkflowUtils.canAssignToCoordinator(getActivity());
        }

        private void internalWorkflowStuff() throws EdmException {
            WorkflowActivity activity = getActivity() != null ?
                    WorkflowFactory.getWfActivity(getActivity()) : null;
            finishOrAssignState = WorkflowFactory.getFinishOrAssignState(document, activity);
            canReopenWf = !InternalWorkflowService.documentHasTasks(getDocumentId(),
                    pl.compan.docusafe.core.office.workflow.WorkflowUtils.iwfPackageName(),
                    pl.compan.docusafe.core.office.workflow.WorkflowUtils.iwfManualName());
        }

        private void icProcessCentra(List<CentrumKosztowDlaFaktury> ckf,
				Boolean simpleAcceptance) throws EdmException {
            rejectObjectAcceptanceAvailable = getActivity() != null && DSApi.context().hasPermission(DSPermission.INVOICE_REJECT_OBJECT_ACCEPTANCE);
			for (CentrumKosztowDlaFaktury centrumDlaFaktury : ckf) {
				centrumDlaFaktury.initAcceptancesModes();
                if(showFullAccount){
                    centrumDlaFaktury.getFullAccountNumber();
                }
				if (centrumDlaFaktury.getAcceptanceMode() != null) {
					cnsNeeded.addAll(centrumDlaFaktury.getAcceptanceCnsNeeded());
				}
				Map<String, String> tmpUser = fm.getAcceptancesState()
						.userToNextAcceptance(centrumDlaFaktury,
								simpleAcceptance);
				userToNextAcceptance.put(centrumDlaFaktury.getId(), tmpUser);
				String nextAcc = fm.getAcceptancesState().getNextAcceptation(
						centrumDlaFaktury, simpleAcceptance);
                log.info("nextAcc ='{}'", nextAcc);
				if (nextAcc != null){
					userToGeneralAcceptance = tmpUser;
				} else {
                    continue;
                }
				if (centrum == null || !centrum.isIntercarsPowered()) {
					for (IntercarsAcceptanceMode ic : centrumDlaFaktury
							.getPossibleAcceptanceModes()) {
						String name = StringResource.get("acceptanceModeName."
								+ centrumDlaFaktury.getCentrumId() + "."
								+ ic.getId());
						if (name != null) {
							acceptanceNames.put(centrumDlaFaktury
									.getCentrumId()
									+ "." + ic.getId(), name);
						} else {
							acceptanceNames.put(centrumDlaFaktury
									.getCentrumId()
									+ "." + ic.getId(), ic.getSimpleName());
						}
					}
				} else {
					for (IntercarsAcceptanceMode ic : centrum
							.getAvailableAcceptanceModes()) {
						String name = StringResource.get("acceptanceModeName."
								+ centrum.getId() + "." + ic.getId());
						if (name != null) {
							acceptanceNames.put(centrum.getId() + "."
									+ ic.getId(), name);
						} else {
							acceptanceNames.put(centrum.getId() + "."
									+ ic.getId(), ic.getSimpleName());
						}
					}
				}
			}
		}

		private void prepareForIC() throws EdmException {
			List<CentrumKosztowDlaFaktury> ckf = (List<CentrumKosztowDlaFaktury>) fm.getValue("CENTRUM_KOSZTOWE");
			userToNextAcceptance = new HashMap<Long, Map<String, String>>();
			generalAcceptances = true;
			acceptanceNames = new HashMap<String, String>();
			cnsNeeded = new HashSet<String>();
			jbpmAcceptances = true;
            showFullAccount = getActivity() == null || fm.getEnumItemCn(InvoiceICLogic.STATUS_FIELD_CN).equals("DYREKTORA_FIN"); //pokazuj pe?ne konto tylko ksi?gowym

			fastAssignment = InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(
				fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN));

			manualToCoordinator = !InvoiceICLogic.FAKTURY_ZAKUPOWE_CNS.contains(fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN));

			Integer dzial = (Integer) fm.getKey(InvoiceLogic.DZIAL_FIELD_CN);
			if (dzial != null) {
				centrum = (CentrumKosztow.find(fm.getField(InvoiceLogic.DZIAL_FIELD_CN).getEnumItem(dzial).getCentrum()));
			}
			if (ckf != null) {
                if(!AvailabilityManager.isAvailable("ignore.acceptances")){
				    icProcessCentra(ckf, false);
                }
			} else {
				log.error(this.getClass().getName().concat(":FillForm:icProcessCentra(null, false"));
			}
			acceptancesUserList = fm.getAcceptancesState().getAcceptancesUserList();
			acceptancesCnList = new LinkedHashMap<String, String>(IntercarsAcceptanceMode.getAllAcceptanceCodes().size() / 2);
			for(DocumentAcceptance da: fm.getAcceptancesState().getFieldAcceptances()){
				acceptancesCnList.put(da.getAcceptanceCn(), da.getAcceptanceCn());
			}
			for(DocumentAcceptance da: fm.getAcceptancesState().getGeneralAcceptances()){
				acceptancesCnList.put(da.getAcceptanceCn(), da.getAcceptanceCn());
			}
			
			acceptanceButtonsForCurrentUserVisible =
				document.getDocumentKind().logic() instanceof SadLogic //oni widz? zawsze
				|| (document.getDocumentKind().logic() instanceof InvoiceICLogic
                        &&((InvoiceICLogic)document.getDocumentKind().logic())
					        .isAcceptanceButtonsForCurrentUserVisible(document,fm));
			
			try {
				document.getDocumentKind().logic().checkCanFinishProcess(document);
			} catch (Exception e) {
				finishOn = false;
			}
		}
    }

    protected void prepareAttachments(OfficeDocument document) throws EdmException {
        List<Attachment> attachmentList = OfficeDocumentHelper.filterAttachments(document, document.getAttachments());

        attachments = Collections2.transform(attachmentList, new Function<Attachment, Map>() {
            public Map apply(Attachment attachment) {
                BeanBackedMap map = new BeanBackedMap(attachment, "id", "title", "barcode", "lparam", "mostRecentRevision", "cn");
                try {
                    map.put("userSummary", DSUser.safeToFirstnameLastname(attachment.getAuthor()));
                    map.put("attCtime", attachment.getCtime());
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
                return map;
            }
        });
        attachments = Lists.newArrayList(attachments);
    }

    private class ManualFinish implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                if (!DSApi.context().isTransactionOpen())
                    DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                if (StringUtils.isNotEmpty(getActivity())) {
                    doWiadomosci = !WorkflowFactory.getInstance().isManual(getActivity());
                } else
                    doWiadomosci = false;

                document.getDocumentKind().logic().onCommitManualFinish(document, doWiadomosci);

                WorkflowFactory.getInstance().manualFinish(getActivity(), !doWiadomosci);

                TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());


                DSApi.context().commit();

                if (document instanceof OutOfficeDocument &&
                        ((OutOfficeDocument) document).isInternal())
                    event.setResult("task-list-int");
                else if (document instanceof OutOfficeDocument)
                    event.setResult("task-list-out");
                else
                    event.setResult("task-list");
            } catch (EdmException e) {
                log.error("", e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Close implements ActionListener {


        public Close() {
        }

        public void actionPerformed(ActionEvent event) {
            DSContext ctx = null;
            try {
                ctx = DSApi.context();
                ctx.begin();
                WorkflowFactory wf = WorkflowFactory.getInstance();
                Set<Long> docIds = Sets.newLinkedHashSet();
                docIds.add(documentId);

                if (wf instanceof InternalWorkflowFactory) {
                    for (Long docId : docIds) {
                        String activityId = wf.findManualTask(docId);
                        wf.manualFinish(activityId, false);
                    }
                } else if (wf instanceof Jbpm4WorkflowFactory) {
                    for (Long docId : docIds) {
                        String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
                        wf.manualFinish(firstActivityId, false);
                    }
                } else {
                    throw new IllegalStateException("nieznany WorkflowFactory");
                }

                ctx.commit();
                event.setResult("task-list");
                //DSApi.close();
            } catch (Exception e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
                if (ctx.isTransactionOpen()) ctx._rollback();
            }
        }
    }

    private class ReopenWf implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();
                WorkflowFactory.reopenWf(getDocumentId());
                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                document.getDocumentKind().logic().canReopenDocument(document);
                TaskSnapshot.updateByDocument(document);
                DSApi.context().commit();
                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                    log.debug("dla dokumentu przywroconego na liste zadan znaleziono "
                            + activityIds.length + " zadan");
                } else {
                    setActivity(activityIds[0]);
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class UpdateBox implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();
                officeDocumentHelper.updateBox(DocumentArchiveTabAction.this);
                DSApi.context().commit();
                addActionMessage(sm.getString("ZapisanoNumerPudlaArchiwalnego"));
            } catch (EdmException e) {
                log.error("", e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    Archive archiveNoValidate(){return new Archive(false);}

    private class AlternativeUpdate implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            try {

                DSApi.context().begin();
                document = OfficeDocument.findOfficeDocument(getDocumentId());

                if (AvailabilityManager.isAvailable("dwr")) {
                    values = getDockindKeys();
                }

                DocumentKind documentKind;
                if (documentKindCn == null) {
                    documentKind = document.getDocumentKind();
                    documentKindCn = documentKind.getCn();
                } else {
                    documentKind = DocumentKind.findByCn(documentKindCn);
                }

                documentKind.initialize();
                documentKind.logic().beforeSetWithHistory(document, values, getActivity());
                documentKind.setWithHistory(document.getId(), values, false);
                documentKind.logic().archiveActions(document, DocumentKindsManager.getType(document));
                documentKind.logic().documentPermissions(document);

                String updateActionMessage = documentKind.logic().onAlternativeUpdate(document, values, getActivity());

                TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());
                DSApi.context().commit();

                addActionMessage(updateActionMessage);
                HttpSession session = ServletActionContext.getRequest().getSession();
                session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                event.addActionError(e.getMessage());
                if (event.hasListener(EV_ARCHIVE))
                    event.skip(EV_ARCHIVE);
                if (event.hasListener("PROCESS"))
                    event.skip("PROCESS");
            }
        }
    }

    private class CreateDocument extends FileActionListener {

        @Override
        public FileInfo fileAction(ActionEvent event) throws Exception {
            final Long documentId = Long.valueOf(ServletActionContext.getRequest().getParameter("documentId"));
            final String dockind = ServletActionContext.getRequest().getParameter("documentKindCn");
            final String templateName = ServletActionContext.getRequest().getParameter("templateKind");

            Optional<FileInfo> fileInfo = new RichEventActionSupport.ActionTransaction(event).execute(new DSTransaction.Function<FileInfo>() {

                @Override
                public FileInfo apply() throws Exception {
                    DocumentSnapshot documentSnapshot = getTemplateService().createSnapshot(documentId, null);
                    JsonArray jsonArray = getTemplateService().getAvailableTemplates(dockind);
                    Boolean isUserTemplate = false;
                    for(JsonElement jsonElement:jsonArray){
                        if(jsonElement.isJsonObject()){
                            JsonObject jsonObject = (JsonObject) jsonElement;
                            if(jsonObject.get("name").getAsString().equals(templateName)){
                                isUserTemplate = jsonObject.get("isUserTemplate").getAsBoolean();
                            }
                        }
                    }
                    JsonElement jsonElement = getTemplateService().createFile(documentSnapshot, documentId, templateName, dockind, isUserTemplate);
                    JsonObject jsonObject = (JsonObject)jsonElement;
                    String documentName = jsonObject.get("name").getAsString();
                    return getTemplateService().getFile(documentSnapshot.getId(), documentId, documentName);
                }

            });

            if(fileInfo.isPresent()) {
                return fileInfo.get();
            } else {
                throw new EdmException("B��d podczas generowania dokumentu");
            }

        }

    }

	private class Archive implements ActionListener {
        private final boolean validate;

        Archive(boolean validate){this.validate = validate;}
        Archive(){this(true);}

		public void actionPerformed(ActionEvent event)
		{
				try {
					
					DSApi.context().begin();
					document = OfficeDocument.findOfficeDocument(getDocumentId());
					// zapisuje uwagi
					newNote = TextUtils.trimmedStringOrNull(newNote);
					content = TextUtils.trimmedStringOrNull(content);
					if (AvailabilityManager.isAvailable("dwr"))
					{
						values = getDockindKeys();
					}
	                checkRemark();
	               
	                checkIntercars();

					DocumentKind documentKind;
					if (documentKindCn == null){
						documentKind = document.getDocumentKind();
						documentKindCn = documentKind.getCn();
					} else {
						documentKind = DocumentKind.findByCn(documentKindCn);
	                }
			
					documentKind.initialize();
					documentKind.logic().correctValues(values, documentKind);

					setBeforeUpdate(getDockindKeys());
					
	                if(validate){
	                	Map<String,Object> vals = new HashMap<String, Object>();
	                    vals.putAll(values);
	                    vals.put("processAction",getProcessAction());
						documentKind.logic().validate(vals, documentKind, document.getId());
						documentKind.logic().validateAcceptances(vals, documentKind,getDocumentId());
					    documentKind.logic().setAfterCreateBeforeSaveDokindValues(documentKind ,document.getId(), values);
	                }
	              
					getDocument().setSummary(
	                        getDockindKeys().get("DOC_DESCRIPTION") != null || getDocument().getDescription() != null
	                                ? (String) Objects.firstNonNull(getDockindKeys().get("DOC_DESCRIPTION"), getDocument().getDescription())
	                                : getDocument().getDocumentKind().getName()
	                );

					boolean dockindChanged = !document.getDocumentKind().getCn().equals(documentKindCn);
					document.setDocumentKind(documentKind);

					boolean finishProcess = documentKind.logic().checkRemarkField(document, values, remark);
	                documentKind.logic().beforeSetWithHistory(document, values, getActivity());
					if (dockindChanged) {
						documentKind.setWithHistory(document.getId(), values,/* changeAll */true);
					} else {
						documentKind.setWithHistory(document.getId(), values,/* changeAll */false);
					}

					officeDocumentHelper.updateBox(DocumentArchiveTabAction.this);

	               // iznesoweWTF(documentKind);

	                if (!archiveWithoutLabel && (AvailabilityManager.isAvailable("archiwizacja.flagi")
	                		|| AvailabilityManager.isAvailable("archiwizacja.flagi.dol"))) {
	                    handleFlags();
	                    handleLabels();
	                }

					documentKind.logic().archiveActions(document, DocumentKindsManager.getType(document));
                    documentKind.logic().documentPermissions(document);

					AcceptancesManager.checkFinalAcceptance(document);

					boolean finish = documentKind.logic().processActions(document, getActivity());
					
					if(AvailabilityManager.isAvailable("kancelaria.wlacz.konczenieProcesuPoWyslaniuEmail")){
						OfficeDocument doc = OfficeDocument.find(documentId);
						if(doc.getDocumentKind().getCn().equalsIgnoreCase("email")){
							FieldsManager fm = doc.getFieldsManager().withActivityId(getActivity());
							if(fm.getValue("WYSLAC").equals("Tak")){
								returnToTasklist = true;
								finishProcess = true;
							}
						}
					}

					if (finishProcess && !finish) {
						// ko?czymy zadanie
						WorkflowFactory.getInstance().manualFinish(getActivity(), true);
					}
					
					if(mailNotification)
					{
						log.error("Wysylam powiadomienie mailowe dla dokumentu {} {}", documentKindCn, documentId);
						try
						{
							boolean send = documentKind.logic().sendMailNotification(document.getFieldsManager());
						
							if(send)
								addActionMessage(sm.getString("powiadomienieZosta?oWyslane"));
							
						}catch(Exception e){
							log.error("", e);
							addActionError(e.getMessage());
						}
						
						mailNotification = false;
					}

                    if(sendToExternalRepo){
                        log.info("wysylam dokument do zewn�trznego repozytorium {}", documentId);
                        try{
                            boolean send = documentKind.logic().sendToExternaRepostiory(document.getFieldsManager(), propUuid);

                            if(send)
                                addActionMessage("Dokument zosta� wys�any do zewn�trznego repozytorium");

                        }catch(Exception e){
                            log.error(e.getMessage(), e);
                            addActionError(e.getMessage());
                        }

                        sendToExternalRepo = false;
                    }
                    OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                    doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);
					TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());

					DSApi.context().commit();

					addActionMessage(sm.getString("ZarchiwizowanoDokument"));

					if (finishProcess || finish)
						event.setResult(getTaskListResult());

					// jak akcja sko?czy?a si? pomy?lnie to na pewno domy?lnie nie
					// mo?e pojawi? si? pole z uwag?
					remark = null;
					if(returnToTasklist)
					{
						event.setResult(getTaskListResult(document.getType()));
					}
					HttpSession session = ServletActionContext.getRequest().getSession();
					session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
					
				//XesLog.isXesLogForThisAction(event, ServletActionContext.getRequest().getRequestURI());
			
				} 
				catch (Exception e) {
					log.error(e.getMessage(), e);
					DSApi.context().setRollbackOnly();
					event.addActionError(e.getMessage());
					if (event.hasListener(EV_ARCHIVE))
						event.skip(EV_ARCHIVE);
					if (event.hasListener("PROCESS"))
						event.skip("PROCESS");
				}
		}

        private void checkRemark() throws EdmException {
            if (newNote != null || (content != null && !content.equals(sm.getString("WpiszTrescUwagi")) && !TextUtils.latinize(content).equals("Wpisz tre__ uwagi"))) {
                if (content != null) {
                    document.addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
                } else if (newNote != null) {
                    document.addRemark(new Remark(StringUtils.left(newNote, 4000), DSApi.context().getPrincipalName()));
                }
                addActionMessage(sm.getString("DodanoUwage"));
            }
        }

        private void handleFlags() throws EdmException {
            List<Long> ids;
            if (globalFlag != null) {
                for (Long id : globalFlag) {
                    if (!LabelsManager.existsDocumentToLabelBean(
                            getDocumentId(), id))
                        LabelsManager.addLabel(getDocumentId(), id,
                                DSApi.context().getPrincipalName());
                }
                globalFlags = document.getFlags().getDocumentFlags(
                        false);
                ids = Arrays.asList(globalFlag);
                for (Flags.Flag f : globalFlags) {
                    if (!ids.contains(f.getId())
                            && LabelsManager.existsDocumentToLabelBean(
                                    getDocumentId(), f.getId())
                            && f.isCanClear())
                        LabelsManager.removeLabel(getDocumentId(), f
                                .getId(), DSApi.context()
                                .getPrincipalName());
                }
            } else {
                for (Flags.Flag f : document.getFlags()
                        .getDocumentFlags(false)) {
                    if (LabelsManager.existsDocumentToLabelBean(
                            getDocumentId(), f.getId())
                            && f.isCanClear())
                        LabelsManager.removeLabel(getDocumentId(), f
                                .getId(), DSApi.context()
                                .getPrincipalName());
                }
            }
            if (userFlag != null) {
                for (Long id : userFlag) {
                    if (!LabelsManager.existsDocumentToLabelBean(
                            getDocumentId(), id))
                        LabelsManager.addLabel(getDocumentId(), id,
                                DSApi.context().getPrincipalName());
                }
                userFlags = document.getFlags().getDocumentFlags(true);
                ids = Arrays.asList(userFlag);
                for (Flags.Flag f : userFlags) {
                    if (!ids.contains(f.getId())
                            && LabelsManager.existsDocumentToLabelBean(
                                    getDocumentId(), f.getId()))
                        LabelsManager.removeLabel(getDocumentId(), f
                                .getId(), DSApi.context()
                                .getPrincipalName());
                }
            } else {
                for (Flags.Flag f : document.getFlags()
                        .getDocumentFlags(true)) {
                    if (LabelsManager.existsDocumentToLabelBean(
                            getDocumentId(), f.getId()))
                        LabelsManager.removeLabel(getDocumentId(), f
                                .getId(), DSApi.context()
                                .getPrincipalName());
                }
            }
        }

        private void handleLabels() throws EdmException {
            List<Long> ids;
            if (AvailabilityManager.isAvailable("labels")) {
                List<Label> modifiableLabels = LabelsManager
                        .getWriteLabelsForUser(DSApi.context()
                                .getPrincipalName(), Label.LABEL_TYPE);
                if (label != null) {
                    for (Long id : label) {
                        if (!LabelsManager.existsDocumentToLabelBean(
                                getDocumentId(), id))
                            LabelsManager.addLabel(getDocumentId(), id,
                                    DSApi.context().getPrincipalName());
                    }
                    ids = Arrays.asList(label);
                    for (Label l : modifiableLabels) {
                        if (!ids.contains(l.getId())
                                && LabelsManager
                                        .existsDocumentToLabelBean(
                                                getDocumentId(), l
                                                        .getId()))
                            LabelsManager.removeLabel(getDocumentId(),
                                    l.getId(), DSApi.context()
                                            .getPrincipalName());
                    }
                } else {
                    for (Label l : modifiableLabels) {
                        if (LabelsManager.existsDocumentToLabelBean(
                                getDocumentId(), l.getId()))
                            LabelsManager.removeLabel(getDocumentId(),
                                    l.getId(), DSApi.context()
                                            .getPrincipalName());
                    }
                }
            }
        }

        private void checkIntercars() throws EdmException {
			if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND)
					|| document.getDocumentKind().getCn().equals(DocumentLogicLoader.SAD_KIND)) {

				List<CentrumKosztowDlaFaktury> ckdfs = CentrumKosztowDlaFaktury.findByDocumentId(getDocumentId());
				boolean refresh = false;
				for (CentrumKosztowDlaFaktury ckdf : ckdfs) {
					String value;
					if ((value = ServletActionContext.getRequest().getParameter("centrum" + ckdf.getId())) != null) {
						Integer integer = new Integer(value);
						if (!integer.equals(ckdf.getAcceptanceMode())) {
							log.trace("zmiana acceptance mode [id={}] na {}", ckdf.getId(), value);
							ckdf.setAcceptanceMode(integer);
							refresh = true;
						}
					}
				}

				if (refresh) {
					document.getDocumentKind().logic().getAcceptanceManager()
							.refresh(document, document.getFieldsManager(), null);
				}
			}
		}
	}

	private class GiveAcceptance implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			if (hasActionErrors())
				return;
			
			try
			{
				DSApi.context().begin();
				if (acceptanceCn != null)
				{
					AcceptancesManager.giveAcceptance(acceptanceCn,getDocumentId());
					Document document = Document.find(getDocumentId());
					document.getDocumentKind().logic().onGiveAcceptance(document, acceptanceCn);
				}	
				else 
					AcceptancesManager.giveCentrumAcceptance(centrumAcceptanceCn, getDocumentId());
					
				addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu"));
				
				
				if(document.getDocumentKind().logic() instanceof Acceptable)
				{
					try
					{
						Acceptable logic = (Acceptable) document.getDocumentKind().logic();
						logic.onAccept(acceptanceCn, getDocumentId());
					}
					catch (EdmException e) 
					{
						if(AvailabilityManager.isAvailable("skipMerytorycznaCheck", document.getDocumentKind().getCn()))
                		{
                			//addActionError(e.getMessage());
                		}
                		else
                		{
                			throw e;
                		}						
					}
				}
				 
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
                log.error("blad podczas akceptacji",e);
				addActionError(e.getMessage());
				DSApi.context().setRollbackOnly();
			}
		}
	}

    private class Discard implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();

                newNote = TextUtils.trimmedStringOrNull(newNote);
                content = TextUtils.trimmedStringOrNull(content);
                document = OfficeDocument.findOfficeDocument(getDocumentId());
                if (newNote != null || content != null) {
                    if (content != null) {
                        ((OfficeDocument) document).addRemark(new Remark(
                                StringUtils.left(content, 4000), DSApi
                                .context().getPrincipalName()));
                    } else if (newNote != null) {
                        ((OfficeDocument) document).addRemark(new Remark(
                                StringUtils.left(newNote, 4000), DSApi
                                .context().getPrincipalName()));
                    }
                    addActionMessage(sm.getString("DodanoUwage"));

                }

                InvoiceLogic.getInstance().discard(document, getActivity());

                if (document.getDocumentKind().logic().getAcceptanceManager() != null) {
                    document.getDocumentKind().logic().getAcceptanceManager()
                            .refresh(document, document.getFieldsManager(), null);
                }

                DSApi.context().commit();

                event.setResult("task-list");
            } catch (EdmException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
                addActionError(e.getMessage());
            }
        }
    }

	private class DiscardTo implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();
				if (discardUserAcceptance != null) {
					discardUserAcceptance = discardUserAcceptance.replace(",",
							"");
					newNote = TextUtils.trimmedStringOrNull(newNote);
					content = TextUtils.trimmedStringOrNull(content);
					document = OfficeDocument
							.findOfficeDocument(getDocumentId());

					String[] documentActivity = new String[1];
					documentActivity[0] = getActivity();
					String plannedAssignment = "";
					DSUser user = DSUser.findByUsername(discardUserAcceptance);
					if (user != null) { // najwa?niejsze czynno?ci s? wykonywane
										// tutaj
						AcceptanceUtils.discardToUser(document, getActivity(),
								event, user);
						DSDivision[] divisions = user
								.getDivisionsWithoutGroup();
						plannedAssignment = (divisions != null
								&& divisions.length > 0 ? divisions[0]
								.getGuid() : DSDivision.ROOT_GUID)
								+ "/"
								+ user.getName()
								+ "/"
								+ WorkflowFactory.wfManualName()
								+ "//"
								+ "realizacja";
					} else {
						event.addActionError("Nie znaleziono osoby");
					}

					String curUser = DSApi.context().getDSUser().getName();
					if (user != null && user.getName().equals(curUser)) {
					} else {
						WorkflowFactory.multiAssignment(documentActivity,
								curUser, new String[] { plannedAssignment },
								plannedAssignment, event);
					}
					addNote();
					event.setResult("task-list");
				} else if(discardCnAcceptance != null) {
					if(document == null) document = OfficeDocument.find(getDocumentId());
					AcceptanceUtils.discardToCn(document, getActivity(), event, discardCnAcceptance, null);
					addNote();
					event.setResult("task-list");
				} else {
					event.addActionError("Nie wybrano osoby");
				}
				DSApi.context().commit();

			} catch (EdmException e) {
				event.setResult("error");
				DSApi.context().setRollbackOnly();
				log.debug(e.getMessage(), e);
				addActionError(e.getMessage());
			} catch (Exception e) {
				event.setResult("error");
				DSApi.context().setRollbackOnly();
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}

		private void addNote() throws EdmException {
			if (newNote != null || content != null) {
				if (content != null) {
					((OfficeDocument) document).addRemark(new Remark(StringUtils.left(content, 4000), DSApi.context().getPrincipalName()));
				} else if (newNote != null) {
					((OfficeDocument) document).addRemark(new Remark(StringUtils.left(newNote, 4000), DSApi.context().getPrincipalName()));
				}
				addActionMessage(sm.getString("DodanoUwage"));
			}
		}
	}

	private class DiscardToIM implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();

				newNote = TextUtils.trimmedStringOrNull(newNote);
				content = TextUtils.trimmedStringOrNull(content);
				document = OfficeDocument.findOfficeDocument(getDocumentId());
				if (newNote != null || content != null) {
					if (content != null) {
						((OfficeDocument) document).addRemark(new Remark(
								StringUtils.left(content, 4000), DSApi
										.context().getPrincipalName()));
					} else if (newNote != null) {
						((OfficeDocument) document).addRemark(new Remark(
								StringUtils.left(newNote, 4000), DSApi
										.context().getPrincipalName()));
					}
					addActionMessage(sm.getString("DodanoUwage"));

				}

				InvoiceLogic.getInstance().returnToIM(document, getActivity(),
						event);

				DSApi.context().commit();

				event.setResult("task-list");
			} catch (EdmException e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}

    private class ArchiveNote implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();
                newNote = TextUtils.trimmedStringOrNull(newNote);
                content = TextUtils.trimmedStringOrNull(content);
                document = OfficeDocument.findOfficeDocument(getDocumentId());
                if (newNote != null
                        || (content != null && !content.equals(sm
                        .getString("WpiszTrescUwagi")))) {
                    if (content != null) {
                        ((OfficeDocument) document).addRemark(new Remark(
                                StringUtils.left(content, 4000), DSApi
                                .context().getPrincipalName()));
                    } else if (newNote != null) {
                        ((OfficeDocument) document).addRemark(new Remark(
                                StringUtils.left(newNote, 4000), DSApi
                                .context().getPrincipalName()));
                    }
                    addActionMessage(sm.getString("DodanoUwage"));

                }
                DSApi.context().commit();
            } catch (EdmException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

	private class FullAcceptance implements ActionListener {
		public void actionPerformed(ActionEvent event) {
            LOG.info("FullAcceptance");
            if(hasActionErrors()){
                addActionError("Wyst?pi?y b??dy - akceptacja nie mo?e zosta? wykonana");
                return;
            }
			try {
				DSApi.context().begin();
                LOG.info("userAcceptance = " + userAcceptance);
                if(userAcceptance == null){
				    AcceptanceUtils.fullAccept(getDocumentId(), getActivity());
                } else {
                    AcceptanceUtils.fullAcceptWithNextUser(getDocumentId(), getActivity(), userAcceptance);
                }
				DSApi.context().commit();
				event.setResult(getTaskListResult()); // wszysko posz?o ok
            } catch (IntercarsAcceptanceManager.SafoExportException e) {
                event.setResult("error");
                addActionError(e.getMessage());
                try {
                    DSApi.context().session().flush();
                    DSApi.context().session().clear();
                    DSApi.context().rollback();
                    CLOSE_HIBERNATE_AND_JBPM_SESSION.actionPerformed(event);
                    LOG.error("Zapisywanie komunikatu '" + e.getMessage() + "'");
                    OPEN_HIBERNATE_AND_JBPM_SESSION.actionPerformed(event);
                    DSApi.context().begin();
                    DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).setOnly(getDocumentId(), 
                            Collections.singletonMap(InvoiceICLogic.SAFO_LAST_MESSAGE_FIELD_CN, e.getMessage()));
                    DSApi.context().commit();
                } catch (Exception ex) {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                    LOG.error(ex);
                }
			} catch (EdmException e) {
				event.setResult("error");
				addActionError(e.getMessage());
				log.debug(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
			} catch (Exception e) {
				event.setResult("error");
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
			}
		}
	}

   
	private class DiscardObjectAcceptance extends TransactionalActionListener {

        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(hasActionErrors()){
                throw new EdmException("Wystapily bledy, odrzucanie akceptacji nie moze zostac wykonane.");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            log.error("transaction|{}|{}", objectId, acceptanceCn);
            AcceptanceUtils.discardToCn(document, getActivity(), event, acceptanceCn, objectId);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            event.setResult(getTaskListResult());
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

	private class nextDecretation implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			log.trace("nd->nextDecretation");

            if(hasActionErrors()){
                addActionError("Wyst?pi?y b??dy - akceptacja nie mo?e zosta? wykonana");
                return;
            }

			try {
				if (!DSApi.context().isTransactionOpen())
					DSApi.context().begin();
				AcceptanceCondition thisAcceptance = null;
				String nextAcceptance = null;

                DocFacade docFacade = Documents.document(getDocumentId());
				Document document = docFacade.getDocument();
				DocumentKind kind = document.getDocumentKind();
				Map<String, Object> valuesTmp = new TreeMap<String, Object>();
				FieldsManager fm;
				kind.initialize();
				fm = docFacade.getFieldsManager();
				fm.initialize();
				fm.initializeAcceptances();
				Long centrumId = null;
				CentrumKosztowDlaFaktury ckf = null;

				if (document.getDocumentKind().logic().accept(document,
						centrumToAcceptanceId)) {
					log.trace("[{}] JBPM - dokument zaakceptowany", document
							.getId());

					if (document.getDocumentKind().logic()
							.getAcceptanceManager() != null) {

						log.trace("!JBPM acceptance!");

						DSApi.context().session().flush();
						if (acceptanceToUser == Boolean.TRUE) {
							log.trace("Acceptance to user:{}",
                                    acceptanceToUser);
							document.getDocumentKind().logic()
									.getAcceptanceManager().refresh(document,fm,
											null);
							sendToAfterAssignment(event);
						} else {
							document.getDocumentKind().logic()
									.getAcceptanceManager().refresh(document,fm,
											getActivity());
						}
					}
				} else {
					log.trace("[{}] stary tryb akceptacji", document.getId());
					if (centrumToAcceptanceId == null) {
						ckf = CentrumKosztowDlaFaktury.findByDocumentId(
								getDocumentId()).get(0);
					} else {
						ckf = CentrumKosztowDlaFaktury.getInstance()
								.find(centrumToAcceptanceId);
					}

					Boolean simpleAcceptance = (IntercarsAcceptanceMode
							.from(fm).equals(IntercarsAcceptanceMode.SIMPLE2));

					thisAcceptance = fm.getAcceptancesState()
							.getThisAcceptation(ckf, simpleAcceptance);
					nextAcceptance = fm.getAcceptancesState()
							.getNextAcceptation(null, simpleAcceptance);

					centrumId = ckf.getId();

					if (thisAcceptance == null) {
						addActionError("Wszystkie akceptacje zosta?y wykonane dla dokumentu ");
					} else {
						this.accept(thisAcceptance, document, valuesTmp, fm,
								centrumId, ckf);
					}

					// dekretacja
					log.trace("fm.getField(InvoiceLogic.JBPM_PROCESS_ID)->"
							+ fm.getField(InvoiceLogic.JBPM_PROCESS_ID));
					sendAfterAsignment(event, thisAcceptance, nextAcceptance,
							fm, ckf);
				}
				DSApi.context().session().flush();
				fm = kind.getFieldsManager(getDocumentId());
				if (!fm.getBoolean(InvoiceLogic.AKCEPTACJA_FINALNA_FIELD_CN)) {
					event.setResult(getTaskListResult());
				}
				TaskSnapshot.updateAllTasksByDocumentId(document.getId(),
						document.getStringType());
				document.getDocumentKind().setOnly(document.getId(), valuesTmp);
				DSApi.context().commit();
			} catch (Exception e) {
				event.setResult("error");
				addActionError(e.getMessage());
				log.error("DocumentArchiveTabAction.java:" + e.getMessage(), e);
				DSApi.context().setRollbackOnly();
			}
		}

		private void sendAfterAsignment(ActionEvent event,
				AcceptanceCondition thisAcceptance, String nextAcceptance,
				FieldsManager fm, CentrumKosztowDlaFaktury ckf)
				throws EdmException, DivisionNotFoundException,
				UserNotFoundException {

			DSUser user = null;
			DSDivision division = null;

			log.trace("saa::acceptanceToUser->" + acceptanceToUser);

			if (!acceptanceToUser) {
				log.trace("saa::nextAcceptance->" + nextAcceptance);
				log.trace("saa::thisAcceptance->" + thisAcceptance);
				if (nextAcceptance != null) {
					Integer minTask = Integer.MAX_VALUE;
					DSUser userTmp = null;
					AcceptanceCondition acTmp = null;

					List<CentrumKosztowDlaFaktury> centraDoAkceptacji = (List<CentrumKosztowDlaFaktury>) fm
							.getValue(fm.getAcceptancesDefinition()
									.getCentrumKosztFieldCn());
					for (CentrumKosztowDlaFaktury ckf2 : centraDoAkceptacji) {
						if (!(nextAcceptance.equals(thisAcceptance.getCn())
								&& ckf2.getAcceptingCentrumId().equals(
										ckf.getAcceptingCentrumId()) && centrumToAcceptanceId != null)) {
							List<AcceptanceCondition> accList = AcceptanceCondition
									.find(nextAcceptance, ckf2
											.getAcceptingCentrumId().toString());
							if (accList != null && accList.size() > 0) {
								acTmp = accList.get(0);

							}
						}
					}

					log.trace("saa::acTmp->" + acTmp);

					if (acTmp == null
							&& fm.getAcceptancesDefinition()
									.getGeneralAcceptances().containsKey(
											nextAcceptance)) {
						List<AcceptanceCondition> accList = AcceptanceCondition
								.find(nextAcceptance);
						if (accList != null && accList.size() > 0) {
							acTmp = accList.get(0);
						}
					}

					log.trace("saa::acTmp->" + acTmp);

					if (acTmp != null) {

						if (acTmp.getDivisionGuid() != null) {
							division = DSDivision.find(acTmp.getDivisionGuid());
						} else if (acTmp.getUsername() != null) {
							userTmp = DSUser
									.findByUsername(acTmp.getUsername());
							Integer taskSize = 2;
							if (taskSize < minTask) {
								minTask = taskSize;
								user = userTmp;
							}
						}
					}

					if (user != null || division != null) {
						String[] documentActivity = new String[1];
						documentActivity[0] = getActivity();
						String plannedAssignment = "";

						if (user != null) {
							DSDivision[] divisions = user
									.getDivisionsWithoutGroup();
							plannedAssignment = (divisions != null
									&& divisions.length > 0 ? divisions[0]
									.getGuid() : DSDivision.ROOT_GUID)
									+ "/"
									+ "/"
									+ user.getName()
									+ "/"
									+ WorkflowFactory.wfManualName()
									+ "//"
									+ "realizacja";
						}
						if (division != null) {
							plannedAssignment = division.getGuid() + "//"
									+ WorkflowFactory.wfManualName()
									+ "/*/realizacja";
						}

						log.trace("saa::pa -> " + plannedAssignment);

						String curUser = DSApi.context().getPrincipalName();
						WorkflowFactory.multiAssignment(documentActivity,
								curUser, new String[] { plannedAssignment },
								plannedAssignment, event);
						event.setResult("task-list");

					} else {
						event
								.addActionError(sm
										.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
					}
				} else {
					event
							.addActionError(sm
									.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
				}
			} else {
				sendToAfterAssignment(event);
			}
		}

		private void accept(AcceptanceCondition thisAcceptance,
				Document document, Map<String, Object> valuesTmp,
				FieldsManager fm, Long centrumId, CentrumKosztowDlaFaktury ckf)
				throws EdmException, DSException, HibernateException,
				SQLException {

			if (fm.getAcceptancesDefinition().getAcceptances().get(
					thisAcceptance.getCn()).getFieldCn() == null) {
				AcceptancesManager.giveAcceptance(thisAcceptance.getCn(),
						getDocumentId());
				valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm
						.getAcceptancesDefinition().getAcceptances().get(
								thisAcceptance.getCn()).getLevel());
			} else {
				Map<Integer, String> centrumAcceptanceCn = new TreeMap<Integer, String>();
				centrumAcceptanceCn.put(new Integer(centrumId.toString()),
						thisAcceptance.getCn());
				AcceptancesManager.giveCentrumAcceptance(centrumAcceptanceCn,
						getDocumentId());

				if (thisAcceptance.getCn().equals("zwykla")) {
					valuesTmp.put("AKCEPTACJA_ZWYKLA", 1);
					log.trace("acceptanceMode - zwykla");
				}
				valuesTmp.put(InvoiceLogic.STATUS_FIELD_CN, fm
						.getAcceptancesDefinition().getAcceptances().get(
								thisAcceptance.getCn()).getLevel());
			}
			addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu")
					+ " : " + thisAcceptance.getCn());
		}

		private void sendToAfterAssignment(ActionEvent event)
				throws EdmException {
			DSUser user = null;
			DSDivision division = null;
			userAcceptance = userAcceptance.replace(",", "");
			try {
				user = DSUser.findByUsername(userAcceptance);
			} catch (UserNotFoundException e) {
				try {
					division = DSDivision.find(userAcceptance);
				} catch (DivisionNotFoundException e2) {
					addActionError(sm
							.getString("NieZnalezionoOsobyMogacejWykonacDalszeAkceptacje"));
				}
			}
			if (user != null || division != null) {
				String[] documentActivity = new String[1];
				documentActivity[0] = getActivity();
				String plannedAssignment = "";
				if (user != null) {
					DSDivision[] divisions = user.getDivisionsWithoutGroup();
					plannedAssignment = (divisions != null
							&& divisions.length > 0 ? divisions[0].getGuid()
							: DSDivision.ROOT_GUID)
							+ "/"
							+ user.getName()
							+ "/"
							+ WorkflowFactory.wfManualName()
							+ "//"
							+ "realizacja";
				}
				if (division != null) {
					plannedAssignment = division.getGuid() + "//"
							+ WorkflowFactory.wfManualName() + "/*/realizacja";
				}

				log.trace("staa::pa -> " + plannedAssignment);

				String curUser = DSApi.context().getPrincipalName();
				WorkflowFactory.multiAssignment(documentActivity, curUser,
						new String[] { plannedAssignment }, plannedAssignment,
						event);
				event.setResult("task-list");
			}
		}
	}

    private class ManualPushToCoor extends TransactionalActionListener {
        @Override
        public Logger getLogger() {
            return log;
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            WorkflowFactory.forwardToCoordinator(getActivity(), event);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            event.skip(EV_FILL);
            event.setResult("task-list");
        }
    }

	private class sendToDecretation implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
    		{
    			Document doc = Document.find(getDocumentId());
    			if(doc.getDocumentKind().logic() instanceof Acceptable)
    			{
    				try
    				{
    					DSApi.context().begin();
    					
    					AcceptanceCondition ac = ((Acceptable) doc.getDocumentKind().logic()).nextAcceptanceCondition(doc);

    					if (userName != null && userName.startsWith("ext:"))
    					{
    						String guid = ac.getDivisionGuid() != null ? ac.getDivisionGuid() : DSDivision.ROOT_GUID;
    						String agent = Docusafe.getAdditionProperty("external.acceptance.agent");
    						String plannedAssignment = null;
	    					plannedAssignment =  guid + "/" + agent + "/" + WorkflowFactory.wfManualName() + "/*/" + userName;
	    					WorkflowFactory.multiAssignment(new String[] { getActivity() },DSApi.context().getPrincipalName(), new String[] {plannedAssignment}, plannedAssignment, event);
                                                
                                                doc.getDocumentKind().logic().onDecretationWhenUsernameSet(doc, userName);
    					}
    					else if (userName != null && !userName.equals(""))
    					{
    						String plannedAssignment = "rootdivision/" + userName + "/" + WorkflowFactory.wfManualName() + "/*/realizacja";
    						WorkflowFactory.multiAssignment(new String[] { getActivity() },DSApi.context().getPrincipalName(), 
    								new String[] {plannedAssignment}, plannedAssignment, event);
                                                doc.getDocumentKind().logic().onDecretationWhenUsernameSet(doc, userName);
    					}
    					else 
    					{
    	    				String guid = ac.getDivisionGuid() != null ? ac.getDivisionGuid() : DSDivision.ROOT_GUID;
    	    				String user = ac.getUsername() != null ? ac.getUsername() : "";
    	    				String plannedAssignment = null;
    	    				// je?li nazwa uzytkownika, zaczyna sie od "ext:", dekretacja na agenta z adds'?w 
    	    				if (user.startsWith("ext:"))
    	    				{
    	    					String agent = Docusafe.getAdditionProperty("external.acceptance.agent");
    	    					plannedAssignment =  guid + "/" + agent + "/" + WorkflowFactory.wfManualName() + "/*/" + user;
    	    				}
    	    				else
    	    				{
    	    					plannedAssignment = guid + "/" + user + "/" + WorkflowFactory.wfManualName() + "/*/realizacja";
    	    				}
        					
    	    				WorkflowFactory.multiAssignment(new String[] { getActivity() },DSApi.context().getPrincipalName(), new String[] {plannedAssignment}, plannedAssignment, event);
    					}
    					
    					doc.getDocumentKind().logic().onDecretation(doc);
    					((Acceptable) doc.getDocumentKind().logic()).onSendDecretation(doc, ac);
    					
    					
    					if (doc instanceof OutOfficeDocument &&
    		                    ((OutOfficeDocument) doc).isInternal())
		                    event.setResult("task-list-int");
    		            else if (doc instanceof OutOfficeDocument)
		                    event.setResult("task-list-out");
    		            else
		                    event.setResult("task-list");
    					
	    				DSApi.context().commit();
    				}
    				catch (Exception e) 
    				{
						DSApi.context().setRollbackOnly();
                        log.error("blad w wysylaniu dekretacji", e);
						throw new EdmException("NieUdaloSiePrzekazacPisma");
					}
    			}
    			else
    			{
    				throw new EdmException("NiekompatybilnaLogika");
    			}
    		}
    		catch (Exception e) 
    		{
				addActionError(e.getMessage());
			}

		}
	}

	private class Assignments implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			if (assignments == null || assignments.length == 0) {
				addActionError(sm.getString("NieWybranoUzytkownikowDoDekretacji"));
				return;
			}
			if (StringUtils.isEmpty(getActivity())) {
				addActionError(sm.getString("NieMaZadania"));
				return;
			}

			if(hasActionErrors()){
				return;
			}
			try
			{
				DSApi.context().begin();

				// WfActivity wfActivity =
				// WorkflowFactory.getWfActivity(getActivity());

				// AssigmentsBean assigmentsBean = new AssigmentsBean();
				String[] plannedAssignments = new String[assignments.length];
				for (int i = 0; i < assignments.length; i++)
				{
//					String[] guidUsername = assignments[i].split(";");
					String processDesc = ServletActionContext.getRequest().getParameter("assignmentProcess_" + assignments[i]);
					String kind;
					if(processDesc != null){
						kind = processDesc.split("::")[1];
					} else {
						kind = WorkflowFactory.wfManualName();
					}
					String objective;
					if (kind.equals(WorkflowFactory.wfManualName())) {
						objective = sm.getString("doRealizacji");
					} else {
						objective = sm.getString("doWiadomosci");
					}
					plannedAssignments[i] = WorkflowFactory.getPreparedAssignment(assignments[i], kind, objective);
				}
				WorkflowFactory.multiAssignment(new String[]
				{ getActivity() }, null, plannedAssignments, null, event);
				event.skip(EV_FILL);
				event.setResult("confirm");
				DSApi.context().commit();
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException f)
				{
				}
				addActionError(e.getMessage());
			}

		}
	}

    public class Reject implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();
                Map<String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,RockwellLogic.STAT_REJECT_AP);
                Document doc = Document.find(getDocumentId());
                if (doc != null) {
                    doc.getDocumentKind().setOnly(doc.getId(), valueMap);
                }
                WorkflowFactory.simpleDivisionAssignment(getActivity(),RockwellLogic.GUID_TO_REJECT, null, WorkflowFactory.wfManualName(), null, "zwrot", event);
                DSApi.context().commit();
                event.skip(EV_FILL);
                event.setResult("return-on-tasklist");

            } catch (Exception e) {
                log.error("", e);
                DSApi.context()._rollback();
                addActionError(e.getMessage());
            }
        }
    }

    public class SendToEva implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                Map<String, Object> statusMap = new HashMap<String, Object>();
                statusMap.put(RockwellLogic.EVA_STATUS_FIELD_CN, 1);
                statusMap.put(RockwellLogic.SUBMITTED_BY_CN, DSApi.context().getPrincipalName());
                statusMap.put(RockwellLogic.SUBMIT_DATE_FIELD_CN, new Date());
                statusMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN, RockwellLogic.STAT_PROCESSED_EVA);

                Document doc = Document.find(getDocumentId());
                if (doc != null) {
                    doc.getDocumentKind().setWithHistory(doc.getId(),
                            statusMap, false);

                    DSApi.context().begin();
                    OfficeDocument document = (OfficeDocument) doc;
                    WorkflowFactory.getInstance().manualFinish(getActivity(),true);

                    try {
                        DSApi.context().session().flush();
                    } catch (HibernateException e) {
                        throw new EdmException(e);
                    }

                    TaskSnapshot.updateByDocumentId(document.getId(), "anyType");
                    DSApi.context().commit();
                    // powiadamiany EVA
                    pl.compan.docusafe.parametrization.ra.EvaNotifier.notifyEva();
                    event.setResult(getTaskListResult(document.getType()));
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                addActionError(e.getMessage());
            }
        }
    }

	private class UpdateFlags implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			try {
				DSApi.context().begin();

				Document document = Document.find(getDocumentId());
				List<Long> ids;
				if (globalFlag != null) 
				{
					for (Long id : globalFlag) {
						if (!LabelsManager.existsDocumentToLabelBean(
								getDocumentId(), id))
							LabelsManager.addLabel(getDocumentId(), id, DSApi
									.context().getPrincipalName());
					}
					globalFlags = document.getFlags().getDocumentFlags(false);
					ids = Arrays.asList(globalFlag);
					for (Flags.Flag f : globalFlags) {
						if (!ids.contains(f.getId())
								&& LabelsManager.existsDocumentToLabelBean(
										getDocumentId(), f.getId())
								&& f.isCanClear())
							LabelsManager.removeLabel(getDocumentId(), f
									.getId(), DSApi.context()
									.getPrincipalName());
					}
				} else {
					for (Flags.Flag f : document.getFlags().getDocumentFlags(
							false)) {
						if (LabelsManager.existsDocumentToLabelBean(
								getDocumentId(), f.getId())
								&& f.isCanClear())
							LabelsManager.removeLabel(getDocumentId(), f
									.getId(), DSApi.context()
									.getPrincipalName());
					}
				}
				if (userFlag != null) {
					for (Long id : userFlag) {
						if (!LabelsManager.existsDocumentToLabelBean(
								getDocumentId(), id))
							LabelsManager.addLabel(getDocumentId(), id, DSApi
									.context().getPrincipalName());
					}
					userFlags = document.getFlags().getDocumentFlags(true);
					ids = Arrays.asList(userFlag);
					for (Flags.Flag f : userFlags) {
						if (!ids.contains(f.getId())
								&& LabelsManager.existsDocumentToLabelBean(
										getDocumentId(), f.getId()))
							LabelsManager.removeLabel(getDocumentId(), f
									.getId(), DSApi.context()
									.getPrincipalName());
					}
				} else {
					for (Flags.Flag f : document.getFlags().getDocumentFlags(
							true)) {
						if (LabelsManager.existsDocumentToLabelBean(
								getDocumentId(), f.getId()))
							LabelsManager.removeLabel(getDocumentId(), f
									.getId(), DSApi.context()
									.getPrincipalName());
					}
				}
				if (AvailabilityManager.isAvailable("labels")) {
					List<Label> modifiableLabels = LabelsManager
							.getWriteLabelsForUser(DSApi.context()
									.getPrincipalName(), Label.LABEL_TYPE);
					if (label != null) {
						for (Long id : label) {
							if (!LabelsManager.existsDocumentToLabelBean(
									getDocumentId(), id))
								LabelsManager.addLabel(getDocumentId(), id,
										DSApi.context().getPrincipalName());
						}
						ids = Arrays.asList(label);
						for (Label l : modifiableLabels) {
							if (!ids.contains(l.getId())
									&& LabelsManager.existsDocumentToLabelBean(
											getDocumentId(), l.getId()))
								LabelsManager.removeLabel(getDocumentId(), l
										.getId(), DSApi.context()
										.getPrincipalName());
						}
					} else {
						if(!AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
							for (Label l : modifiableLabels) {
								if (LabelsManager.existsDocumentToLabelBean(
										getDocumentId(), l.getId()))
									LabelsManager.removeLabel(getDocumentId(), l
											.getId(), DSApi.context()
											.getPrincipalName());
							}
						}
					}
				}
				DSApi.context().commit();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				DSApi.context().setRollbackOnly();
				
			}
		}
	}

    private class AssignMe implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                DSApi.context().begin();

                if (WorkflowFactory.jbpm) {
                    String firstActivityId = Jbpm4ProcessLocator.taskIds(getDocumentId()).iterator().next();
                    Jbpm4WorkflowFactory.assignMe(firstActivityId, getDocumentId());
                } else {
                    String act = WorkflowFactory.findManualTask(getDocumentId());
                    WorkflowFactory.getInstance().assignMe(act, event);
                    setActivity(act);
                    try {
                        DSApi.context().session().flush();
                    } catch (HibernateException e) {
                        throw new EdmException(e);
                    }
                }
                OfficeDocument document = OfficeDocument
                        .findOfficeDocument(getDocumentId());
                TaskSnapshot.updateByDocumentId(document.getId(), document
                        .getStringType());

                DSApi.context().commit();

            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class SendToEpuap implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	values = getDockindKeys();
            	Document doc = getDocument();
            	DSApi.context().begin();
            	values.get("EPUAP");
            	//int id = 50;
            	if ( values.get("EPUAP")==USLUGA_DORECZYCIEL){
            		ElectronicSignatureStore signs = ElectronicSignatureStore.getInstance();
            		List<ElectronicSignatureBean> signatures = signs.findSignaturesForDocument(doc.getId());
        			if ((signatures.size()>0)){
            		EpuapExportManager.sendToEpuap(getDocumentId(),true,EpuapExportDocument.USLUGA_DORECZYCIEL);
            		
        			 values.put("ZABLOKUJ", true);
        			 doc.getDocumentKind().set(doc.getId(),values);
            		DSApi.context().commit();
            		addActionMessage(sm.getString("WyslanoDoEpuap"));
        			}else {
        				 addActionError("Dokument wysy�any dor�czeniem musi by� podpisany");
        				 DSApi.context().setRollbackOnly();
        			}
            	}	//else if((Integer) values.get("EPUAP")==(USLUGA_SKRYTKA)){
            	else if(values.get("EPUAP")==USLUGA_SKRYTKA){
            		EpuapExportManager.sendToEpuap(getDocumentId(),true,EpuapExportDocument.USLUGA_SKRYTKA);
            		values.put("ZABLOKUJ", true);
            		 doc.getDocumentKind().set(doc.getId(),values);
            		DSApi.context().commit();
            	addActionMessage(sm.getString("WyslanoDoEpuap"));
            }
            	else {
            		 addActionError("Nieokre�lono na dokumencie rodzaju wysy�ki (dor�czenie,przed�o�enie)");
            		 DSApi.context().setRollbackOnly();
    				 }
            	}
            catch (Exception e)
            {
                addActionError(e.getMessage());
                LOG.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
            }
        }
           
    }

    private static double round(double value, int decimalPlace) {
        double power_of_ten = 1;
        while (decimalPlace-- > 0)
            power_of_ten *= 10.0;
        return Math.round(value * power_of_ten) / power_of_ten;
    }

    private class WithdrawAcceptance implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (hasActionErrors())
                return;

            try {
                DSApi.context().begin();

                Document doc = Document.find(getDocumentId());
                DocumentLogic logic = doc.getDocumentKind().logic();
                logic.canWithdrawAcceptance(doc, acceptanceCn);

                if (objectId != null) {
                    AcceptancesManager.withdrawCentrumAcceptanceAndAssign(
                            doc, objectId,
                            getActivity());
                } else {
                    AcceptancesManager
                            .withdrawAcceptanceAndAssign(doc, acceptanceCn,
                                    getActivity());
                }

                doc.getDocumentKind().logic().onWithdrawAcceptance(doc, acceptanceCn);

                if (logic instanceof Acceptable) {
                    Acceptable acceptable = (Acceptable) logic;
                    acceptable.onWithdraw(acceptanceCn, getDocumentId());
                }

                event.setResult("success");

                DSApi.context().commit();
                addActionMessage(sm.getString("WycofanoAkceptacjeDokumentu"));
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

	public OfficeDocument getDocument() {
		try{
			if(document == null) document = OfficeDocument.find(getDocumentId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return document;
	}

	/**
	 * Sprawdza czy dokument jest pismem przychodz?cym
	 * @return false je?li nie jest pismem przychodz?cym lub dokument jest null
	 */
	public boolean isInOfficeDocument()
	{
		if (getDocument() == null)
			return false;
		return getDocument() instanceof InOfficeDocument;
	}

	
	/**
	 * Drukowanie przy pomocy Zebra printer
	 */
	public class Print implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if(AvailabilityManager.isAvailable("print.barcode.create")&& getDocumentId() != null)
				{
					Document doc = getDocument();
					if (!(doc instanceof InOfficeDocument))
					{
						addActionError("DokumentNieJestPismemPrzychodzacym");
						return;
					}
					ZebraPrinterManager manager = new ZebraPrinterManager();
					manager.printLabelFromDocument((InOfficeDocument)doc);
					addActionMessage(sm.getString("WydrukowanoPismo" + doc.getId()));
				}
				else
					addActionError("Nie wybrano dokumentu");
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
		}
	}

    public class Clone implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event) {
            try{
            if(Docusafe.getAdditionProperty("ifpan.klonowanie").equals("true")){

            DSApi.context().begin();
            Document document = Document.find(documentId);

            String nrSprawy = (String) document.getFieldsManager().getValue("NR_SPRAWY");
            String[] parts = nrSprawy.split("/");
            boolean klon = false;
            try
            {
                Integer i = Integer.parseInt(parts[1]);
            }
            catch(NumberFormatException e)
            {
                klon = true;
            }
            if(!klon)
            {
            HttpServletRequest request = ServletActionContext.getRequest();
            Sender tmpSender = new Sender();
            long tmpSenderId = 0;
            List<Audit> listaAudit = new ArrayList<Audit>();
            List<AssignmentHistoryEntry> listaAHE=new ArrayList<AssignmentHistoryEntry>();
            if (document instanceof OfficeDocument) {
                if (((OfficeDocument) document).getSenderId() != null) {
                    tmpSenderId = ((OfficeDocument) document).getSenderId();
                    Person person = Person.find(tmpSenderId);
                    tmpSender.fromMap(person.toMap());
                    listaAudit=document.getWorkHistory();
                    listaAHE=((OfficeDocument) document).getAssignmentHistory();
                }
            }
//            stareGuid = document.getVersionGuid();
//            staraWersja = document.getVersion();

            String tmp = request.getParameter("nowa_wersja");
//            Map<String, String> rob = request.getParameterMap();
            boolean nowa_wersja = false;
            String name = DSApi.context().getDSUser().getName();

            DocumentKind documentKind = document.getDocumentKind();
            if (AvailabilityManager.isAvailable("p4CloneDocument"))
                values = RequestLoader.loadKeysFromWebWorkRequest();

            // sprawdzenie czy pole data jest puste, a nie jest
            // zaznaczona
            // opcja dokumentu bez daty w przypadku dokumentu
            // przychodzacego
            boolean jestData = true;
            if (document instanceof InOfficeDocument) {
                if (Boolean.parseBoolean((String) values
                        .get("dokument_bez_daty"))) {
                } else {
                    if (values.get("DOC_DATE") == null)
                        jestData = false;

                    else {
                        if (values.get("DOC_DATE").equals(""))
                            jestData = false;

                    }
                }
            }
            //pole data zwrotki w wypadku dokumentu wychodzacego
            if(document instanceof OutOfficeDocument){
                if (!Boolean.parseBoolean((String)values.get("zwrotka"))) {
                } else {
                    if (values.get("datazwrotki")==null)
                        jestData = false;

                    else {
                        if (values.get("datazwrotki").equals(""))
                            jestData = false;
                    }
                }
            }
            // niewpisana data przy niezaznaczonym checkobxie dokument
            // bez daty
            if (jestData == false) {
                if(document instanceof InOfficeDocument)addActionError("Pole data mo�e by� puste tylko w przypadku dokumentu bez daty.");
                else addActionError("Pola data zwrotki mo�e by� puste tylko w wypadku dokumentu bez zwrotki.");
            } else {
                // wyzerowanie daty w przypadku dokumentu bez daty
                if (Boolean.parseBoolean((String) values
                        .get("dokument_bez_daty"))&& document instanceof InOfficeDocument) {
                    values.remove("DOC_DATE");
                    values.put("DOC_DATE", null);
                }
                if (Boolean.parseBoolean((String) values
                        .get("datazwrotki")) && document instanceof OutOfficeDocument) {
                    values.remove("datazwrotki");
                    values.put("datazwrotki", null);
                }
                // chce utworzyc nowa wersje a nie ma uprawnien
                if (nowa_wersja
                        && !DSApi
                        .context()
                        .hasPermission(
                                DSPermission.WERSJONOWANIE_UTWORZ_NOWA_WERSJE)) {
                    addActionError("Brak uprawnie� do tworzenia nowej wersji");
                    return;
                } else {

                    documentKind.logic()
                            .correctValues(values, documentKind);
                    // walidacja nie jest tu raczej potrzebna, gdy� wykonuje
                    // si�
                    // ona
                    // ju� na poziomie Javascriptu
                    if(AvailabilityManager.isAvailable("utp.wlacz.unikalneBarcody")){
                        OfficeDocument doc = OfficeDocument.find(documentId);
                        if(values.get("BARCODE") != null){
                            if(!doc.getBarcode().equals(values.get("BARCODE"))){
                                String bar = (String) values.get("BARCODE");
                                List<OfficeDocument> barcode = OfficeDocument.findAllByBarcode(bar);
                                List list = OfficeFolder.findByBarcode(bar);
                                if (list != null && list.size()>0){
                                    throw new EdmException("Wprowadzony kod kreskowy = " + bar + " ju� istnieje w systemie !");
                                }
                                if(barcode.size() > 0){
                                    throw new EdmException("Wprowadzony kod kreskowy = " + bar + " ju� istnieje w systemie !");
                                }
                            }
                        }
                    }else{
                        documentKind.logic().validate(values, documentKind,
                                null);
                    }
                    List<Long> attachmentsToClone = new ArrayList<Long>(
                            copyAttachments.size());
                    List<Long> attachmentsToDelAfter = new ArrayList<Long>();
                    for (Iterator iter = copyAttachments.entrySet()
                            .iterator(); iter.hasNext();) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        Long attachmentId = new Long(entry.getKey()
                                .toString());
                        String copyOrMove = HttpUtils.valueOrNull(entry
                                .getValue());
                        if ("copy".equals(copyOrMove)
                                || "move".equals(copyOrMove))
                            attachmentsToClone.add(attachmentId);
                        if ("move".equals(copyOrMove))
                            attachmentsToDelAfter.add(attachmentId);
                    }

                    Document newDocument = document
                            .cloneObject((Long[]) attachmentsToClone
                                    .toArray(new Long[attachmentsToClone
                                            .size()]));
                    newDocument.setUnsafeCtime(new Date());


                    // w przypadku pism wychodzacych nie tworzony jest nowy
                    // sender, w przypadku pism wychodzacy/przychodzacych
                    // uzupelniane jest pole sendera w bazie pisma stareGuigo
                    //odbiorcy wprzypadku pisma przychodzacego maja zle id dokumentu(poprawiane jest tu)
                    if (document instanceof OfficeDocument
                            && ((OfficeDocument) document).getSenderId() != null) {

                        tmpSender.setDocumentId(document.getId());
                        tmpSender.setDictionaryGuid("rootdivision");
                        tmpSender.setDictionaryType("sender");

                        // ustawienie pol sendera dla in
                        if (newDocument instanceof InOfficeDocument) {

                            Person p = Person
                                    .find((long) ((OfficeDocument) newDocument)
                                            .getSenderId());
                            p.setDocumentId(newDocument.getId());
                            p.setDictionaryGuid("rootdivision");
                            p.setDictionaryType("sender");
                            //ustawienie dla recipient�w document_id
                            List<Recipient> lista = ((OfficeDocument)newDocument).getRecipients();
                            Iterator it = lista.iterator();
                            while (it.hasNext()) {
                                Recipient r = (Recipient) it.next();
                                r.setDocumentId(newDocument.getId());
                                r.setDocument(newDocument);
                            }

                        }
                        // swtorzenie nowego dla out
                        if (document instanceof OutOfficeDocument) {
                            Person p = Person
                                    .find((long) ((OfficeDocument) document)
                                            .getSenderId());
                            p.setDocumentId(newDocument.getId());
                            p.setDictionaryType("sender");

                            Sender tmpSender2 = new Sender();

                            if (document instanceof OfficeDocument) {
                                tmpSenderId = ((OfficeDocument) document)
                                        .getSenderId();
                                Person person = Person.find(tmpSenderId);
                                tmpSender2.fromMap(person.toMap());

                            }
                            tmpSender2.setDictionaryGuid(null);
                            tmpSender2.setDocument(newDocument);
                            tmpSender2.create();
                            ((OfficeDocument) newDocument)
                                    .setSender(tmpSender2);
                            ((OfficeDocument) newDocument)
                                    .setSenderId(tmpSender2.getId());
                        }

                    }



                    // wyrzucenie przeniesionych zalacznikow
                    for (Long id : attachmentsToDelAfter)
                        document.scrubAttachment(id);
                    //w przypadku gdy nie pobralo nadawcy(nie ma go) to ustawia sendera, aby sie nie wywalalo w documentuserdivision
                    if(values.containsKey("SENDER_HERE")){
                        if(!(values.get("SENDER_HERE")!=null))
                            values.put("SENDER_HERE", "");
                    }

						/* zapisywanie danych biznesowych */
                    newDocument.setPermissionsOn(false);
                    documentKind.set(newDocument.getId(), values);

                    documentKind.logic().archiveActions(newDocument,
                            DocumentKindsManager.getType(newDocument));

                    documentKind.logic().documentPermissions(newDocument);

						/*
						 * na wypadek gdyby oryginalny dokument by� podpisany
						 * podpisem elektronicznym
						 */
                    newDocument.setBlocked(false);

                    // ustawianie wersji
                    if (nowa_wersja) {
                        OfficeDocument doc = OfficeDocument.find(documentId);
                        List<OfficeDocument> docActive = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
                        for (OfficeDocument od : docActive) {
                            od.setCzyAktualny(false);
                        }
                        newDocument.setCzyAktualny(true);
                        newDocument.setCzyCzystopis(false);
                        newDocument.setVersionDesc("Zam�wienie klon");

                        if (document instanceof OfficeDocument) {
                            listaAudit.add(Audit.create(
                                    "newVersion",
                                    DSApi.context()
                                            .getPrincipalName(),
                                    sm.getString("UtworzonoNowaWersje")));
                            listaAudit
                                    .add(Audit.create(
                                            "Active",
                                            DSApi.context().getPrincipalName(),
                                            sm.getString("WersjaNieAktywna")
                                                    + " "
                                                    + document
                                                    .getVersion()));
                            listaAudit
                                    .add(Audit.create("Active",
                                            DSApi.context().getPrincipalName(),
                                            sm.getString("WersjaAktywna") + " "
                                                    + newDocument.getVersion()));

                        }

                    } else {
                        if (document instanceof OfficeDocument) {
                            listaAudit.add(Audit.create(
                                    "::nw_clone",
                                    DSApi.context()
                                            .getPrincipalName(),
                                    sm.getString("DokumentZostalSklonowany")));

                        }

                        newDocument.setCzyAktualny(true);
                        newDocument.setCzyCzystopis(false);
                        newDocument.setVersion(1);
                        newDocument.setVersionGuid((int) (long) newDocument
                                .getId());


                    }
                    if (newDocument instanceof InOfficeDocument)
                        ((InOfficeDocument) newDocument)
                                .setSummary(newDocument
                                        .getDescription());
                    if (newDocument instanceof OutOfficeDocument)
                        ((OutOfficeDocument) newDocument)
                                .setSummary(newDocument
                                        .getDescription());

                    //w set ja czysci wiec znow uzupelniam(wczesniej zapisana)
                    document.setWorkHistory(listaAudit);
                    folderId = newDocument.getFolderId();

                    // klonowanie historii pisma
                    if(nowa_wersja){
                        newDocument.setWorkHistory(document.getWorkHistory());
                        //int i=0;
                        for (Audit audit : document.getWorkHistory()) {
                            if (audit.getProperty() != null){
                                ((OfficeDocument) newDocument)
                                        .addWorkHistoryEntry(audit);
                            }

                        }

                    }
                    else{
                        for (Audit audit : document.getWorkHistory()) {
                            if (audit.getProperty() != null){
                                ((OfficeDocument) document)
                                        .addWorkHistoryEntry(audit);
                            }

                        }

                    }
                    if (newDocument instanceof OfficeDocument) {
                        // p4
                        if (AvailabilityManager
                                .isAvailable("p4CloneDocument")) {
                            Date entryDate = GlobalPreferences
                                    .getCurrentDay();
                            Journal journal = Journal.getMainIncoming();
                            Long journalId = journal.getId();
                            DSApi.context().session().flush();
                            Integer sequenceId = Journal.TX_newEntry2(
                                    journalId, newDocument.getId(),
                                    entryDate);
                            ((InOfficeDocument) newDocument).bindToJournal(
                                    journalId, sequenceId, event);

                            newDocument
                                    .getDocumentKind()
                                    .logic()
                                    .onStartProcess(
                                            (OfficeDocument) newDocument,
                                            event);
                        } else{
                            if(nowa_wersja){
                                //zamkniecie procesu dla starego dokumentu
                                OfficeDocument doc = OfficeDocument.find(documentId);
                                List<OfficeDocument> guid = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
                                for(OfficeDocument od:guid){
                                    if(JBPMTaskSnapshot.findByDocumentId(od.getId()) != null){
                                        List<JBPMTaskSnapshot> taskId = JBPMTaskSnapshot.findByDocumentId(od.getId());
                                        if(taskId.size() != 0){
                                            if(taskId.size()>1){
                                                for(int i=0;i<taskId.size()-1;i++){
                                                    WorkflowFactory wf = WorkflowFactory.getInstance();
                                                    Set<Long> docIds = new LinkedHashSet();

                                                    docIds.add(taskId.get(i).getDocumentId());

                                                    if(wf instanceof InternalWorkflowFactory){
                                                        for (Long docId : docIds) {
                                                            String activityId = wf.findManualTask(docId);
                                                            wf.manualFinish(activityId, false);
                                                        }
                                                    } else if(wf instanceof Jbpm4WorkflowFactory) {
                                                        for (Long docId : docIds) {
                                                            if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
                                                                String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
                                                                wf.manualFinish(firstActivityId, false);
                                                            }
                                                        }
                                                    } else {
                                                        throw new IllegalStateException("nieznany WorkflowFactory");
                                                    }
                                                }
                                            } else {
                                                for(int i=0;i<taskId.size();i++){
                                                    WorkflowFactory wf = WorkflowFactory.getInstance();
                                                    Set<Long> docIds = new LinkedHashSet();

                                                    docIds.add(taskId.get(i).getDocumentId());

                                                    if(wf instanceof InternalWorkflowFactory){
                                                        for (Long docId : docIds) {
                                                            String activityId = wf.findManualTask(docId);
                                                            wf.manualFinish(activityId, false);
                                                        }
                                                    } else if(wf instanceof Jbpm4WorkflowFactory) {
                                                        for (Long docId : docIds) {
                                                            if(Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null){
                                                                String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
                                                                wf.manualFinish(firstActivityId, false);
                                                            }
                                                        }
                                                    } else {
                                                        throw new IllegalStateException("nieznany WorkflowFactory");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //utworzenie procesu dla nowego dokumentu
                        newDocument.getDocumentKind().logic().onStartProcess(
                                (OfficeDocument) newDocument,event);
                    }
                    if (nowa_wersja) {
                        String sql = "delete from dso_document_asgn_history where document_id="
                                + document.getId();
                        PreparedStatement ps = DSApi.context()
                                .prepareStatement(sql);
                        ps.executeUpdate();
                    } else {
                        ((OfficeDocument) document).setAssignmentHistory(listaAHE);
                    }

                    DSApi.context().commit();
                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    // sklonowanie uprawnie� dla nowego dokumentu
                    // uprawnienia dla grupy
                    newDocument.getDocumentKind().logic()
                            .documentPermissions(newDocument);
                    DSApi.context().session().save(newDocument);
                    // uprawniania dla autora
                    java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

                    DSUser author = DSApi.context().getDSUser();
                    String user = author.getName();
                    String fullName = author.getLastname() + " "
                            + author.getFirstname();

                    perms.add(new PermissionBean(ObjectPermission.READ,
                            user, ObjectPermission.USER, fullName + " ("
                            + user + ")"));
                    perms.add(new PermissionBean(
                            ObjectPermission.READ_ATTACHMENTS, user,
                            ObjectPermission.USER, fullName + " (" + user
                            + ")"));
                    perms.add(new PermissionBean(ObjectPermission.MODIFY,
                            user, ObjectPermission.USER, fullName + " ("
                            + user + ")"));
                    perms.add(new PermissionBean(
                            ObjectPermission.MODIFY_ATTACHMENTS, user,
                            ObjectPermission.USER, fullName + " (" + user
                            + ")"));
                    perms.add(new PermissionBean(ObjectPermission.DELETE,
                            user, ObjectPermission.USER, fullName + " ("
                            + user + ")"));

                    Set<PermissionBean> documentPermissions = DSApi
                            .context().getDocumentPermissions(newDocument);
                    perms.addAll(documentPermissions);
                    ((AbstractDocumentLogic) newDocument.getDocumentKind()
                            .logic()).setUpPermission(newDocument, perms);

                    if (newDocument instanceof OfficeDocument) {
                        // flush i tak robi sie w
                        // updateDSApi.context().session().flush();
                        DSApi.context().begin();
                        if(!nowa_wersja){
                            ps = null;
                            rs = null;
                            if(newDocument instanceof InOfficeDocument) {
                                ps = DSApi
                                        .context()
                                        .prepareStatement(
                                                "SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=1");
                                ps.setLong(1, newDocument.getId());
                                rs = ps.executeQuery();
                                if(rs.next()){
                                    ((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
                                }
                            } else if( newDocument instanceof OutOfficeDocument) {
                                if(newDocument.getType() == DocumentType.INTERNAL){
                                    Date entryDate = GlobalPreferences.getCurrentDay();
                                    Long journalId = Journal.getMainInternal().getId();
                                    Integer sequenceId = Journal.TX_newEntry2(
                                            journalId, newDocument.getId(), entryDate);
                                    OutOfficeDocument doc = OutOfficeDocument
                                            .findOutOfficeDocument(newDocument.getId());
                                    doc.bindToJournal(journalId, sequenceId);
                                    ps = DSApi
                                            .context()
                                            .prepareStatement(
                                                    "SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=3");
                                    ps.setLong(1, newDocument.getId());
                                    rs = ps.executeQuery();
                                    if(rs.next()){
                                        ((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
                                    }

                                } else {
                                    Date entryDate = GlobalPreferences.getCurrentDay();
                                    Long journalId = Journal.getMainOutgoing().getId();
                                    Integer sequenceId = Journal.TX_newEntry2(
                                            journalId, newDocument.getId(), entryDate);
                                    OutOfficeDocument doc = OutOfficeDocument
                                            .findOutOfficeDocument(newDocument.getId());
                                    doc.bindToJournal(journalId, sequenceId);
                                    ps = DSApi
                                            .context()
                                            .prepareStatement(
                                                    "SELECT SEQUENCEID FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=2");
                                    ps.setLong(1, newDocument.getId());
                                    rs = ps.executeQuery();
                                    if(rs.next()){
                                        ((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
                                    }
                                }
                            }
                        }

                        TaskSnapshot.updateByDocumentId(
                                newDocument.getId(),
                                ((OfficeDocument) newDocument)
                                        .getStringType());

                        newDocument.setAuthor(DSApi.context().getDSUser().getName());
                        DSApi.context().commit();
                    }

                    Map<String, Object> toReloadMap =  document.getFieldsManager().getFieldValues();
                    String kod = (String) toReloadMap.get("NR_SPRAWY");
                    String[] czesci = kod.split("/");
                    String nowyKod = czesci[0] + "/" + czesci[1];
                    String query = "select count(1) from dsg_ifpan_zamowienia_umowa_pzp where nr_sprawy like('NZU/" + czesci[1] + "%/%');";
                    Statement stat = DSApi.context().createStatement();
                    ResultSet resSet = stat.executeQuery(query);
                    while(resSet.next())
                    {
                        int count = resSet.getInt(1);
                        if(count == 1) //pierwszy klon
                        {
                            nowyKod += "A/" + czesci[2];
                        }
                        else if(count > 1)//kolejny klon
                        {
                            String query2 = "select top 1 * from dsg_ifpan_zamowienia_umowa_pzp where nr_sprawy like('NZU/" + czesci[1] + "%/%') order by document_id desc;";
                            Statement stat2 = DSApi.context().createStatement();
                            ResultSet resSet2 = stat2.executeQuery(query2);
                            while(resSet2.next())
                            {
                                String staryKod = resSet2.getString("nr_sprawy");
                                String[] czesci2 = staryKod.split("/");
                                char literka = czesci2[1].charAt(czesci2[1].length()-1);
                                char kolejna = literka++;
                                nowyKod+= literka + "/" + czesci[2];
                            }

                        }
                    }
                    toReloadMap.put("NR_SPRAWY", nowyKod);

                    document.getDocumentKind().setOnly(newDocument.getId(), toReloadMap);
                        event.setResult("task-list");

                }
            }
            }
            else
            {
                addActionError("Nie mo�na klonowa� dokumentu, kt�ry zosta� ju� sklonowany z innego dokumentu");
            }

            }
         //   XesLog.isXesLogForThisAction(event, ServletActionContext.getRequest().getRequestURI());	   
        }
       
        catch(Exception e)
        {
           log.error(e.getMessage(), e);
        }
        }

    }

    public class PrintBarcodePool implements ActionListener
    {

    	@Override
        public void actionPerformed(ActionEvent event) {
            try
            {
                if(AvailabilityManager.isAvailable("print.barcode.pool")&& getDocumentId() != null)
                {
                    Document doc = getDocument();
                    FieldsManager fm = doc.getFieldsManager();

                    String printerName = (String) fm.getValue("CPP");
                    if(printerName==null) addActionError("Nie wybrano drukarki");
                    else if(printerName.contains("wybierz")) addActionError("Nie wybrano drukarki");
                    ZebraPrinterManager manager = new ZebraPrinterManager();
                    ZebraPrinter printer = ZebraPrinter.findByUserPrinterName(printerName);
                    try
    				{
                    printer.init();
                    
    					Map<String,String> fields = new LinkedHashMap<String,String>();
    					printer.setFields(fields);
    					
    					BarcodePool pula = (BarcodePool) DSApi.context().session().createCriteria(BarcodePool.class).add(Restrictions.eq("documentId", doc.getId())).list().get(0);
                        for(String barcode : pula.getBarcodes())
    					{
    						printer.setBarcodeValue(barcode);
    						printer.print();
    					}
    					
    					
    				} catch (EdmException e1)
    				{
    					log.error(e1.getMessage(), e1);
    				}
                }
            }
            catch(Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }
	
	/**
	 * Sprawdza czy dokument jest pismem wychodz?cym
	 * @return false je?li nie jest pismem wychodz?cym lub dokument jest null
	 */
	public boolean isOutOfficeDocument()
	{
		if (getDocument() == null)
			return false;
		return getDocument() instanceof OutOfficeDocument;
	}
	
	private class splitAndAssignOfficeNumber implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				splitAndAssignOfficeNumber = true;

			} catch (Exception e)
			{
				log.error("B??d wykonywania akcji " + getDockindEventValue(), e);
				addActionError(e.getMessage());
			}
		}
	}
	
	/**
	 * nadanie numeru ko pismu wychodzacemu z poziomu dokinda z weryfiakcja uprawnien  
	 * 
	 */
	private class AssignOfficeNumber implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{

			try
			{
				if (!DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO))
				{
					addActionError("NieMaszUprawnienDoNadawaniaNumeruKoPismuWychodzacemu");
				} else
				{

					try
					{
						DSApi.context().begin();
						if (getDocument() instanceof OutOfficeDocument)
						{
							Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), document.getId(), new Date());
							((OutOfficeDocument) document).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
						}
						DSApi.context().commit();
					} catch (Exception e)
					{	
						DSApi.context()._rollback();
						log.error("B??d wykonywania akcji " + getDockindEventValue(), e);
						addActionError(e.getMessage());
					}
				}
			} catch (EdmException e)
			{
				log.error("B??d wykonywania akcji " + getDockindEventValue(), e);
				addActionError(e.getMessage());
			}
		}
	}

	/**
	 *Dzielenie pisma z wielu odbiorcow na poszczegolnwe pisma 
	 */
	private class SplitDocument implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			if (AvailabilityManager.isAvailable("documentMain.SplitOutOfficeDocument") && getDocumentId() != null)
			{

				try
				{
					DSApi.context().begin();

				} catch (EdmException e)
				{
					log.error("", e);
				}
				Document doc = getDocument();
				values = getDockindKeys();
				if (!(doc instanceof OutOfficeDocument))
				{
					addActionError("DokumentNieJestPismemWychodzacym");
					return;
				}
				try
				{
					SplitOutOfficeDocument.splitOutOfficeDocument((OfficeDocument) doc, values, splitAndAssignOfficeNumber, event);
					try
					{
						DSApi.context().commit();
					} catch (EdmException e)
					{
						log.error("", e);
						DSApi.context().rollback();
					}
				} catch (EdmException e)
				{
					log.error("", e);

				} catch (SQLException e)
				{
					log.error("", e);
				}
			}
		}
	}
	
	/**
	 *Oznacza dokument jako czystopis(domyslnie brudnopis).
	 *Czystopis ma zablokowana edycje w pismach przychodzacych i wewnetrznych.
	 *Pismo wychodzace ma mo�liwo�� zmiany pola "otrzymano zwrotke".
	 */
	public class OznaczCzystopis implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

			try {
				if(AvailabilityManager.isAvailable("kancelaria.wlacz.oznaczJakoCzystopis")&& getDocumentId() != null)
					if(!DSApi.context().hasPermission(DSPermission.OZNACZ_JAKO_CZYSTOPIS)){
						addActionError(sm.getString("NieMaszUprawnienDoOznaczeniaJakoCzystopis"));
					}else{
				DSApi.context().begin();
				Document doc = Document.find(documentId);
				if(doc.getCzyAktualny()){
					doc.setCzyCzystopis(true);
					DSApi.context().commit();
					addActionMessage("Oznaczono dokument jako czystopis");
				}else{
					addActionError("dokument nie jest aktualny");
				}
					}
			} catch (DocumentNotFoundException e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			} catch (DocumentLockedException e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			} catch (AccessDeniedException e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
			}
			
		}
	}
	
	/**
	 *Podpisuje dokument, przy sprawdzeniu czy dany u�ytkownik ju� podpisa� dokument.
	 *Podpisany dokument staje si� czystopisem.
	 *<p>
	 *Dodanie informacji o tym kto podpisa� do historii pisma i dekretacji.
	 *
	 */
	private class InternalSign implements ActionListener 
	{
		public void actionPerformed(ActionEvent event)
		{
			if(!AvailabilityManager.isAvailable("kancelaria.wlacz.podpisWewnetrzny")){
				addActionMessage("Modu� jest wy��czony.");
			} else {
				try {
					
					DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
					long loginId=user.getId();
					
					setSign(true);
					DSApi.context().begin();
					OfficeDocument doc = OfficeDocument.find(documentId);
					
					List<DSUser> dsUsers = doc.getDocumentKind().logic().getUserWithAccessToDocument(documentId);
					for (DSUser dsUser : dsUsers) {
						DocumentMailHandler.createEvent(documentId, NormalLogic.MAILEVENTSIGN, dsUser.getEmail());
					}
					AssignmentHistoryEntry ahe=new AssignmentHistoryEntry(
							AssignmentHistoryEntry.JBPM4_INTERNALSIGN,
							DSApi.context().getPrincipalName(), 
							"x","x","x","x",
							false, 
							AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION
							);
			        String divisions = "";

			        for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
			        {
			            if (!divisions.equals(""))
			                divisions = divisions + " / " + a.getName();
			            else
			                divisions = a.getName();
			        }
					ahe.setSourceGuid(divisions);
					doc.addAssignmentHistoryEntry(ahe,true,null);
					
					doc.setCzyCzystopis(true);
					InternalSignature.getInstance().signDocument(loginId, documentId);
					DSApi.context().commit();
					InternalSignatureEntity internalSignatureEntity = InternalSignatureManager.findUserSignature(loginId);
					String userNamePlusDivision=" ("+DSDivision.find(doc.getCurrentAssignmentGuid()).getName()+")";
					doc.addWorkHistoryEntry(Audit.create("Sign",DSApi.context().getPrincipalName(),
							sm.getString("PodpisanoDokumentSygnatura",internalSignatureEntity.getSignature())));

				} catch (EdmException e) {
					log.error("Blad podpisywania pisma", e);
				}
			}
			
		}
	}
	
	/**
	 * Wymuszanie utworzenia nowej wersji przy zapisie zmian na dokumencie.
	 *
	 */
	private class SaveNewVersion implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Archive archive = new Archive();
			CloneNewVersion newVersion = new CloneNewVersion();
			try {
				OfficeDocument document = OfficeDocument.find(documentId);
				//mozliwosc archiwizacji dokumentu przy zmianie pola otrzymano zwrotke
				if(document.getDocumentKind().getCn().equals("normal_out")){
					OutOfficeDocument docOut = OutOfficeDocument.findOutOfficeDocument(documentId);	
					if(!docOut.isInternal() && document.getCzyCzystopis() && document.getOfficeNumber() != null){
						archive.actionPerformed(event);
					} else {
						if (AvailabilityManager.isAvailable("dwr"))
						{
							values = getDockindKeys();
						}
						newVersion.clone(documentId, event, values, nowyOpis);
					}
				} else {
					if (AvailabilityManager.isAvailable("dwr"))
					{
						values = getDockindKeys();
					}
					newVersion.clone(documentId, event, values, nowyOpis);
				}
			} catch (DocumentNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	public void setDocument(OfficeDocument document) {
		this.document = document;
	}

	public String getFolderPrettyPath() {
		return folderPrettyPath;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	public Long getCurrentBoxId() {
		return currentBoxId;
	}

	public String getCurrentBoxNumber() {
		return currentBoxNumber;
	}

	public Long getBoxId() {
		return boxId;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;
	}

	public String getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public boolean isCanClone() {
		return canClone;
	}

	public boolean isCanUpdate() {
		return canUpdate;
	}

	public boolean isNeedsNotBox() {
		return needsNotBox;
	}

	public boolean isBoxNumberReadonly() {
		return boxNumberReadonly;
	}

	public boolean isCanAddToRS() {
		return canAddToRS;
	}

	public List<DrsInfo> getDrsDocs() {
		return drsDocs;
	}

	public FinishOrAssignState getFinishOrAssignState() {
		return finishOrAssignState;
	}

	public boolean isDocumentFax() {
		return documentFax;
	}

	public void setDocumentFax(boolean documentFax) {
		this.documentFax = documentFax;
	}

	public boolean isCanReadDictionaries() {
		return canReadDictionaries;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isCanChangeDockind() {
		return canChangeDockind;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public Map<Long, String> getJbpmProcesses() {
		return jbpmProcesses;
	}

	public Long getJbpmProcessId() {
		return jbpmProcessId;
	}

	public String getJbpmLink() {
		return jbpmLink;
	}

	public String getInitJbpmLink() {
		return initJbpmLink;
	}

	public boolean isCanInitJbpmProcess() {
		return canInitJbpmProcess;
	}

	public FieldsManager getFm() {
		return fm;
	}

	public Map<String, Object> getValues() {
		return values;
	}

	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public Map<String, String> getDocumentKinds() {
		return documentKinds;
	}

	public void setAcceptanceCn(String acceptanceCn) {
		this.acceptanceCn = acceptanceCn;
	}

	public Map<Integer, String> getCentrumAcceptanceCn() {
		return centrumAcceptanceCn;
	}

	public void setCentrumAcceptanceCn(Map<Integer, String> centrumAcceptanceCn) {
		this.centrumAcceptanceCn = centrumAcceptanceCn;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getDockindAction() {
		return "edit";
	}

	public boolean isAcceptancesPage() {
		return true;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public boolean isCrm() {
		return documentKindCn.startsWith("crm");
	}

	public void setCrm(boolean crm) {
		this.crm = crm;
	}

	public boolean getIfFinAccepted() {
		return this.ifFinAccepted;
	}

	public Collection<Map> getAttachments() {
		return attachments;
	}

	public void setAttachments(Collection<Map> attachments) {
		this.attachments = attachments;
	}

	public boolean isFlagsPresent() {
		return flagsPresent;
	}

	public void setFlagsPresent(boolean flagsPresent) {
		this.flagsPresent = flagsPresent;
	}

	public List<Flags.Flag> getGlobalFlags() {
		return globalFlags;
	}

	public void setGlobalFlags(List<Flags.Flag> globalFlags) {
		this.globalFlags = globalFlags;
	}

	public List<Flags.Flag> getUserFlags() {
		return userFlags;
	}

	public void setUserFlags(List<Flags.Flag> userFlags) {
		this.userFlags = userFlags;
	}

	public Remark getLastRemark() {
		return lastRemark;
	}

	public void setLastRemark(Remark lastRemark) {
		this.lastRemark = lastRemark;
	}

	public String getLastRemarkAutchor() {
		return lastRemarkAutchor;
	}

	public void setLastRemarkAutchor(String lastRemarkAutchor) {
		this.lastRemarkAutchor = lastRemarkAutchor;
	}

	public String getNewNote() {
		return newNote;
	}

	public void setNewNote(String newNote) {
		this.newNote = newNote;
	}

	public List<Adnotation> getNoteList() {
		return noteList;
	}

	public void setNoteList(List<Adnotation> noteList) {
		this.noteList = noteList;
	}

	public Audit getLastHistory() {
		return lastHistory;
	}

	public void setLastHistory(Audit lastHistory) {
		this.lastHistory = lastHistory;
	}

	public String getLastHistoryAutchor() {
		return lastHistoryAutchor;
	}

	public void setLastHistoryAutchor(String lastHistoryAutchor) {
		this.lastHistoryAutchor = lastHistoryAutchor;
	}

	public Map<String, String> getSubstituted() {
		return substituted;
	}

	public void setSubstituted(Map<String, String> substituted) {
		this.substituted = substituted;
	}

	public Map<String, String> getAssignmentsMap() {
		return assignmentsMap;
	}

	public void setAssignmentsMap(Map<String, String> assignmentsMap) {
		this.assignmentsMap = assignmentsMap;
	}

	public Map<String, String> getWfProcesses() {
		return wfProcesses;
	}

	public void setWfProcesses(Map<String, String> wfProcesses) {
		this.wfProcesses = wfProcesses;
	}

	public List getObjectives() {
		return objectives;
	}

	public void setObjectives(List objectives) {
		this.objectives = objectives;
	}

	public boolean isShowSendToEva() {
		return showSendToEva;
	}

	public void setShowSendToEva(boolean showSendToEva) {
		this.showSendToEva = showSendToEva;
	}

	public Boolean getShowObjectives() {
		return showObjectives;
	}

	public void setShowObjectives(Boolean showObjectives) {
		this.showObjectives = showObjectives;
	}

	public String[] getAssignments() {
		return assignments;
	}

	public void setAssignments(String[] assignments) {
		this.assignments = assignments;
	}

	public Boolean getShowReject() {
		return showReject;
	}

	public void setShowReject(Boolean showReject) {
		this.showReject = showReject;
	}

	public Long[] getGlobalFlag() {
		return globalFlag;
	}

	public void setGlobalFlag(Long[] globalFlag) {
		this.globalFlag = globalFlag;
	}

	public Long[] getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(Long[] userFlag) {
		this.userFlag = userFlag;
	}

	public Boolean getShowLine() {
		return showLine;
	}

	public void setShowLine(Boolean showLine) {
		this.showLine = showLine;
	}

	public String getEvaMessage() {
		return evaMessage;
	}

	public void setEvaMessage(String evaMessage) {
		this.evaMessage = evaMessage;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReturnAcceptance() {
		return returnAcceptance;
	}

	public void setReturnAcceptance(String returnAcceptance) {
		this.returnAcceptance = returnAcceptance;
	}

	public Long getReturnAcceptanceObject() {
		return returnAcceptanceObject;
	}

	public void setReturnAcceptanceObject(Long returnAcceptanceObject) {
		this.returnAcceptanceObject = returnAcceptanceObject;
	}

	public Map<String, String> getUserToAcceptance() {
		return userToAcceptance;
	}

	public void setUserToAcceptance(Map<String, String> userToAcceptance) {
		this.userToAcceptance = userToAcceptance;
	}

	public String getUserAcceptance() {
		return userAcceptance;
	}

	public void setUserAcceptance(String userAcceptance) {
		this.userAcceptance = userAcceptance;
	}

	public Long getCentrumToAcceptanceId() {
		return centrumToAcceptanceId;
	}

	public void setCentrumToAcceptanceId(Long centrumToAcceptanceId) {
		this.centrumToAcceptanceId = centrumToAcceptanceId;
	}

	public Map<Long, Map<String, String>> getUserToNextAcceptance() {
		return userToNextAcceptance;
	}

	public void setUserToNextAcceptance(
			Map<Long, Map<String, String>> userToNextAcceptance) {
		this.userToNextAcceptance = userToNextAcceptance;

	}

	public Boolean getAcceptanceToUser() {
		return acceptanceToUser;
	}

	public void setAcceptanceToUser(Boolean acceptanceToUser) {
		this.acceptanceToUser = acceptanceToUser;
	}

	public Boolean getGeneralAcceptances() {
		return generalAcceptances;
	}

	public void setGeneralAcceptances(Boolean generalAcceptances) {
		this.generalAcceptances = generalAcceptances;
	}

	public Map<String, String> getUserToGeneralAcceptance() {
		return userToGeneralAcceptance;
	}

	public void setUserToGeneralAcceptance(
			Map<String, String> userToGeneralAcceptance) {
		this.userToGeneralAcceptance = userToGeneralAcceptance;
	}

	public Boolean getOpenViewer() {
		return openViewer;
	}

	public void setOpenViewer(Boolean openViewer) {
		this.openViewer = openViewer;
	}

	public Map<String, String> getAcceptancesUserList() {
		return acceptancesUserList;
	}

	public void setAcceptancesUserList(Map<String, String> acceptancesUserList) {
		this.acceptancesUserList = acceptancesUserList;
	}

	public Boolean getFinishOn() {
		return finishOn;
	}

	public void setFinishOn(Boolean finishOn) {
		this.finishOn = finishOn;
	}

	public Map<String, Acceptance> getGeneralAcceptancesToGo() {
		return generalAcceptancesToGo;
	}

	public void setGeneralAcceptancesToGo(
			Map<String, Acceptance> generalAcceptancesToGo) {
		this.generalAcceptancesToGo = generalAcceptancesToGo;
	}

	public Boolean getDiscardOn() {
		return discardOn;
	}

	public void setDiscardOn(Boolean discardOn) {
		this.discardOn = discardOn;
	}

	public Boolean getDoWiadomosci() {
		return doWiadomosci;
	}

	public void setDoWiadomosci(Boolean doWiadomosci) {
		this.doWiadomosci = doWiadomosci;
	}

	public void setCanAssignMe(boolean canAssignMe) {
		this.canAssignMe = canAssignMe;
	}

	public boolean isCanAssignMe() {
		return canAssignMe;
	}

	public Boolean getCanReopenWf() {
		return canReopenWf;
	}

	public void setCanReopenWf(Boolean canReopenWf) {
		this.canReopenWf = canReopenWf;
	}

	public void setLabel(Long[] label) {
		this.label = label;
	}

	public Long[] getLabel() {
		return label;
	}

	public void setHackedLabels(List<pair<Label, Boolean>> hackedLabels) {
		this.hackedLabels = hackedLabels;
	}

	public List<pair<Label, Boolean>> getHackedLabels() {
		return hackedLabels;
	}

	public void setNonModifiableLabels(List<Label> nonModifiableLabels) {
		this.nonModifiableLabels = nonModifiableLabels;
	}

	public List<Label> getNonModifiableLabels() {
		return nonModifiableLabels;
	}

	public void setRemarks(List<RemarkBean> remarks) {
		this.remarks = remarks;
	}

	public List<RemarkBean> getRemarks() {
		return remarks;
	}

	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}

	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}

	public String getDocumentAspectCn() {
		return documentAspectCn;
	}

	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}

	/**
	 * @return the jbpmAcceptances
	 */
	public boolean isJbpmAcceptances() {
		return jbpmAcceptances;
	}

	/**
	 * @return the acceptanceNames
	 */
	public Map<String, String> getAcceptanceNames() {
		return acceptanceNames;
	}

	/**
	 * @param acceptanceNames
	 *            the acceptanceNames to set
	 */
	public void setAcceptanceNames(Map<String, String> acceptanceNames) {
		this.acceptanceNames = acceptanceNames;
	}

	/**
	 * @return the centrum
	 */
	public CentrumKosztow getCentrum() {
		return centrum;
	}

	/**
	 * @param centrum
	 *            the centrum to set
	 */
	public void setCentrum(CentrumKosztow centrum) {
		this.centrum = centrum;
	}

	/**
	 * @return the cnsNeeded
	 */
	public Set<String> getCnsNeeded() {
		return cnsNeeded;
	}

	/**
	 * @param cnsNeeded
	 *            the cnsNeeded to set
	 */
	public void setCnsNeeded(Set<String> cnsNeeded) {
		this.cnsNeeded = cnsNeeded;
	}

    public void setCentra(List<CentrumKosztow> centra) {
		this.centra = centra;
	}

	public List<CentrumKosztow> getCentra() {
		return centra;
	}

	public void setCentraToRachunek(Map<Integer, String> centraToRachunek) {
		this.centraToRachunek = centraToRachunek;
	}

	public Map<Integer, String> getCentraToRachunek() {
		return centraToRachunek;
	}

	public void setSetInitialValues(Boolean setInitialValues) {
		this.setInitialValues = setInitialValues;
	}

	public Boolean getSetInitialValues() {
		return setInitialValues;
	}

	public boolean isPudloModify() {
		return pudloModify;
	}
	 


	public void setPudloModify(boolean pudloModify) {
		this.pudloModify = pudloModify;
	}
	
	public boolean isCanAssignOfficeNumber()
	{
		return canAssignOfficeNumber;
	}

	
	public void setCanAssignOfficeNumber(boolean canAssignOfficeNumber)
	{
		this.canAssignOfficeNumber = canAssignOfficeNumber;
	}
	public String getDiscardUserAcceptance() {
		return discardUserAcceptance;
	}

	public void setDiscardUserAcceptance(String discardUserAcceptance) {
		this.discardUserAcceptance = discardUserAcceptance;
	}

	public Map<String, String> getAcceptancesCnList() {
		return acceptancesCnList;
	}

	public void setAcceptancesCnList(Map<String, String> acceptancesCnList) {
		this.acceptancesCnList = acceptancesCnList;
	}

	public String getDiscardCnAcceptance() {
		return discardCnAcceptance;
	}

	public void setDiscardCnAcceptance(String discardCnAcceptance) {
		this.discardCnAcceptance = discardCnAcceptance;
	}

	public boolean isAcceptanceButtonsForCurrentUserVisible() {
		return acceptanceButtonsForCurrentUserVisible;
	}

	public void setAcceptanceButtonsForCurrentUserVisible(boolean acceptanceButtonsForCurrentUserVisible) {
		this.acceptanceButtonsForCurrentUserVisible = acceptanceButtonsForCurrentUserVisible;
	}

	public boolean isFastAssignment() {
		return fastAssignment;
	}

	public void setFastAssignment(boolean fastAssignment) {
		this.fastAssignment = fastAssignment;
	}

	public DSPartsManager getPartsManager(){
		return DSPartsConfig.get().provideManager(null);
	}

	public String getDockindEventValue() 
	{
		return dockindEventValue;
	}

	public String getPlace() 
	{
		return "documentArchive";
	}

	public void setDockindEventValue(String dockindEventValue) 
	{
		this.dockindEventValue = dockindEventValue;
	}

	public String[] getActivityIds() {
		return getActivity() != null ? new String[]{getActivity()} : null;
	}

	public Long[] getDocumentIds() 
	{
		return getDocumentId() != null ? new Long[]{getDocumentId()} : null;
	}
	public void setBoxNumberReadonly(boolean boxNumberReadonly) {
		this.boxNumberReadonly = boxNumberReadonly;		
	}

	public void setCurrentBoxId(Long boxId) {
		this.currentBoxId = boxId;		
	}

	public void setCurrentBoxNumber(String currentBoxNumber) {
		this.currentBoxNumber = currentBoxNumber;		
	}

	public void setNeedsNotBox(Boolean isneed) {
		this.needsNotBox = isneed;		
	}

    public Collection<ProcessDefinition> getProcessesManuallyStarted() {
        return processesManuallyStarted;
    }

    public void setProcessesManuallyStarted(Collection<ProcessDefinition> processesManuallyStarted) {
        this.processesManuallyStarted = processesManuallyStarted;
    }

    public String getStartingProcessName() {
        return startingProcessName;
    }

    public void setStartingProcessName(String startingProcessName) {
        this.startingProcessName = startingProcessName;
    }

    public boolean isBoxActionAvailable() {
        return AvailabilityManager.isAvailable("archiwizacja.pudlo", getDocumentKindCn()) || 
        	   AvailabilityManager.isAvailable("archiwizacja.wybierzPudlo", getDocumentKindCn());
    }

    public String getDroolsRuleCn() {
        return droolsRuleCn;
    }

    public void setDroolsRuleCn(String droolsRuleCn) {
        this.droolsRuleCn = droolsRuleCn;
    }

    
    /* DLA WNIOSK�W URLOPOWYCH */
	public Map<String, String> getAcceptancesMap() {
		return acceptancesMap;
	}
	
	public void setAcceptancesMap(Map<String, String> acceptancesMap) {
		this.acceptancesMap = acceptancesMap;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public List<FreeDay> getFreeDays()
	{
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getFreeDays();
	}

	/* END / DLA WNIOSK�W URLOPOWYCH */
	
	
	/* DLA FAKTURY PTE */
	/**
	 * Okre�la czy przysic 'dekretuj...' jest w��czony
	 */
	public Boolean getSendDecretation()
	{
		DocumentLogic logic = fm.getDocumentKind().logic();
		if (logic instanceof Acceptable)
		{
			try 
			{
				boolean ret = ((Acceptable) logic).canSendDecretation(fm);
				return ret;
			} catch (EdmException e) 
			{
				log.error(e.getMessage(), e);
			}
		}
		return true;
	}
	/* END / DLA FAKTURY PTE */
	

	private boolean isReturnToTasklist() {
		return returnToTasklist;
	}

	private void setReturnToTasklist(boolean returnToTasklist) {
		this.returnToTasklist = returnToTasklist;
	}
	
	/**
	 * Zwraca na formatk� dokumenty powi�zane
	 * 
	 * @return
	 */
	public List<RelatedDocumentBean> getRelatedDocumentBeans()
	{
		try
		{
			DocumentLogic logic = fm.getDocumentKind().logic();
			return logic.getRelatedDocuments(fm);
		} catch (EdmException ex)
		{
			log.error(ex.getMessage(), ex);
		}
		
		return null;
	}
        
        /** funkcja sprawdza czy mo?e odrzucic dokument*/
        public boolean getCanRemarkDocument()
        {
            try{
                return fm.getDocumentKind().logic().canRemarkDocument(fm, document);
            } catch (EdmException ex)
            {
                log.error(ex.getMessage(), ex);
            }
            
            return false;
        }
	
	/**
	 * Zwraca na formatk? list? dost?pnych pude? dla danego dokumentu
	 * 
	 * @return
	 */
	public Map<Long, String> getAvailableArchiveBoxes()
	{
		try
		{	
			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
	    	
	    	Map<Long, String> boxes = new LinkedHashMap<Long, String>();
	    	boxes.put(0L, "wybierz pud?o archiwalne ...");
	    	
	    	String boxLine = (String) fm.getDocumentKind().getDockindInfo()
	    		.getProperties().get("box-line");
	    	
	    	if (boxLine == null || boxLine.equals(""))
	    		return boxes;
	    	
	    	@SuppressWarnings("unchecked")
	    	List<Object[]> res = DSApi.context().session().createQuery(
	    			"select b.id, b.name from " + Box.class.getName() + " b where b.line = :line")
	    			.setParameter("line", boxLine)
	    		.list();
	    	
	    	for (Object[] row : res)
	    		boxes.put((Long) row[0], row[1].toString());
	    	
	    	DSApi.close();
	    	return boxes;
		}
		catch (EdmException ex)
		{
			log.error(ex.getMessage(), ex);
		}
		
		return null;
	}
	
	public void setBeforeUpdate(Map<String, Object> dockindKeys)  throws EdmException  
	{
		if(DocumentLogic.TYPE_IN_OFFICE == getDocType())
			dwrDocumentHelper.setBeforeUpdateIn(DocumentArchiveTabAction.this); 
		if(DocumentLogic.TYPE_OUT_OFFICE == getDocType())
			dwrDocumentHelper.setBeforeUpdateOut(DocumentArchiveTabAction.this); 
		if(DocumentLogic.TYPE_INTERNAL_OFFICE == getDocType())
			dwrDocumentHelper.setBeforeUpdateInt(DocumentArchiveTabAction.this); 
		if(DocumentLogic.TYPE_ARCHIVE == getDocType())
			dwrDocumentHelper.setBeforeUpdateIn(DocumentArchiveTabAction.this);
	}
	
	public void setBeforeCreate(OfficeDocument document) throws EdmException 
	{
		throw new EdmException("W archiwizacji nie tworzy si? dokument?w");
	}
	public void bindToJournal(OfficeDocument document, ActionEvent event)
			throws EdmException {
		throw new EdmException("Brak obs?ugi");
		
	}
	
	public boolean isStartProcess() throws EdmException {
		return false;
	}

	public InOfficeDocumentKind getDefaultKind() {
		return this.defaultKind;
	}

	public void setDefaultKind(InOfficeDocumentKind defaultKind) {
		this.defaultKind = defaultKind;
		
	}

	public void setAfterCreate(OfficeDocument document) throws EdmException {
		throw new EdmException("W archiwizacji nie tworzy si? dokument?w");
		
	}

	public void setDocumentKinds(Map<String, String> documentKinds) {
		this.documentKinds = documentKinds;
		
	}

	public Map<String, Object> getDockindKeys() {
		return this.dockindKeys;
	}

	public void setDockindKeys(Map<String, Object> dockindKeys) {
		this.dockindKeys = dockindKeys;
		
	}

	public Logger getLog() {
		return log;
	}

    public Collection<RenderBean> getProcessRenderBeans() {
		return processRenderBeans;
	}

	/**
	 * @return the manualToCoordinator
	 */
	public boolean isManualToCoordinator() {
		return manualToCoordinator;
	}

    public boolean isElectronicSignatureAvailable() {
        return electronicSignatureAvailable;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public boolean isForceSaveButton() {
        return forceSaveButton;
    }

    public void setForceSaveButton(boolean forceSaveButton) {
        this.forceSaveButton = forceSaveButton;
    }
    /**        ---------- FUNKCJA DO ODFILTROWANIA UZYTKOWNIKOW W RAZIE POTRZEBY
     * potrzebny <available name="archiwizacja.dokument.updatujListeUzytkownika" value="true"/> w xml
     */
        
   /**
     * funkcja potrzebna do modyfikowania listy uzytkownik?w
     * @return
     * @throws EdmException 
     */
   public List<DSUser> getUsersByDocumentToUpdate() throws EdmException {
       try {
           DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

           if (documentKindCn.equals("overtime_app"))
               usersByDocumentToUpdate = DSUser.listAcceptanceSubordinates(document.getAuthor());

       } catch (EdmException e) {
           log.error(e.getMessage(), e);
           addActionError(e.getMessage());
       } finally {
           DSApi._close();
       }

       return usersByDocumentToUpdate;
   }
    
    public String getUsersDockindFieldUpdate()
    {
        if(documentKindCn.equals("overtime_app"))
            return "DS_USER";
        
        return "DS_USER";
    }

    public boolean isShowFullAccount() {
        return showFullAccount;
    }

	public boolean isArchiveWithoutLabel() {
		return archiveWithoutLabel;
	}

	public void setArchiveWithoutLabel(boolean archiveWithoutLabel) {
		this.archiveWithoutLabel = archiveWithoutLabel;
	}

	public boolean isShowProcessActions()
	{
		return showProcessActions;
	}

    public boolean isNoScheme() {
        return noScheme;
    }

    public void setNoScheme(boolean noScheme) {
        this.noScheme = noScheme;
    }

    public boolean isRejectObjectAcceptanceAvailable() {
        return rejectObjectAcceptanceAvailable;
    }

	public boolean isMailNotification() {
		return mailNotification;
	}

	public void setMailNotification(boolean mailNotification) {
		this.mailNotification = mailNotification;
	}

	public ArrayList<EnumItem> getAvalilabeTypes() {
		return avalilabeTypes;
	}

	public void setAvalilabeTypes(ArrayList<EnumItem> avalilabeTypes) {
		this.avalilabeTypes = avalilabeTypes;
	}
	
	public String getCzystopis() {
		return czystopis;
	}
	
	public void setCzystopis(String czystopis) {
		this.czystopis = czystopis;
	}
	
	public boolean getSave() {
		return save;
	}
	
	public void setSave(boolean save) {
		this.save=save;
	}
	public boolean isDocumentHaveOfficeNumber()
	{
		return documentHaveOfficeNumber;
	}

	
	public void setDocumentHaveOfficeNumber(boolean documentHaveOfficeNumber)
	{
		this.documentHaveOfficeNumber = documentHaveOfficeNumber;
	}
	
	public boolean getSign() {
		return sign;
	}
	
	public void setSign(boolean sign) {
		this.sign=sign;
	}
	
	public boolean getCzyCzystopis() {
		return czyCzystopis;
	}
	
	public void setCzyCzystopis(boolean czyCzystopis) {
		this.czyCzystopis=czyCzystopis;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location=location;
	}
	
	public String getNowyOpis() {
		return nowyOpis;
	}
	
	public void setNowyOpis(String nowyOpis) {
		this.nowyOpis=nowyOpis;
	}
	
	public String getCaseName() {
		return caseName;
	}
	
	public void setCaseName(String caseName) {
		this.caseName=caseName;
	}
	
	public boolean getNewVersion() {
		return newVersion;
	}
	
	public void setNewVersion(boolean newVersion) {
		this.newVersion=newVersion;
	}
	
	public boolean getCzyUkryc() {
		return czyUkryc;
	}
	
	public void setCzyUkryc(boolean czyUkryc) {
		this.czyUkryc=czyUkryc;
	}

    public boolean isSendToExternalRepo() {
        return sendToExternalRepo;
    }

    public void setSendToExternalRepo(boolean sendToExternalRepo) {
        this.sendToExternalRepo = sendToExternalRepo;
    }

    public String getPropUuid() {
        return propUuid;
    }

    public void setPropUuid(String propUuid) {
        this.propUuid = propUuid;
    }

	public boolean isMaPrawaDoOznaczaniaJakoCzystopis() {
		return maPrawaDoOznaczaniaJakoCzystopis;
	}

	public void setMaPrawaDoOznaczaniaJakoCzystopis(
			boolean maPrawaDoOznaczaniaJakoCzystopis) {
		this.maPrawaDoOznaczaniaJakoCzystopis = maPrawaDoOznaczaniaJakoCzystopis;
	}

	public boolean isMaPrawaDoPodpisywania() {
		return maPrawaDoPodpisywania;
	}

	public void setMaPrawaDoPodpisywania(boolean maPrawaDoPodpisywania) {
		this.maPrawaDoPodpisywania = maPrawaDoPodpisywania;
	}

    public String getMobileReferrer() {
        return mobileReferrer;
    }

    public void setMobileReferrer(String mobileReferrer) {
        this.mobileReferrer = mobileReferrer;
    }

    public boolean isHasOnlyUpdateFields() {
		boolean wynik = false;
		
		try {
			for(pl.compan.docusafe.core.dockinds.field.Field field: fm.getFields()){
				if(field.getType().equals(Field.DOCUMENT_JOURNAL)){
					wynik = true;
					break;
				}
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		return wynik;
	}

    private TemplateServiceImpl getTemplateService() {
        if(AvailabilityManager.isAvailable("kancelaria.szablony.simple.asPdfWithBarcode")) {
//            return Docusafe.getBean(TemplateServicePgImpl.class);
            RestTemplate restTemplate = new RestTemplate();
            return new TemplateServicePgImpl(restTemplate, new DocumentConverter(restTemplate));
        } else {
            return Docusafe.getBean(TemplateServiceImpl.class);
        }
    }
}