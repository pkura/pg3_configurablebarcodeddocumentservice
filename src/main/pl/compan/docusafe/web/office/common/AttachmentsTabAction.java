package pl.compan.docusafe.web.office.common;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.*;
import org.apache.lucene.util.Version;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.util.encoders.Base64;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.xes.XesLogVerification;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.general.encryption.AESEncryptor;
import pl.compan.docusafe.logic.AttachmentsManager;
import pl.compan.docusafe.logic.DocumentsManager;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.nationwide.icrThread;
import pl.compan.docusafe.parametrization.pg.PgDocInLogic;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignature;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;








import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import jtpkg.at;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentsTabAction.java,v 1.64 2010/07/14 13:08:48 pecet5 Exp $
 */
public abstract class AttachmentsTabAction extends BaseTabsAction
{
	private static final Logger log = LoggerFactory.getLogger(AttachmentsTabAction.class);
    // @EXPORT
    private List attachments = new LinkedList();
    private Document document;
    private Attachment attachment;
    private boolean readOnly;
    private boolean useScanner;
    private boolean useSystemJTwain;
    private boolean useSignature;
    private boolean canSign;
    private boolean editorOn;
    private boolean showAddAttachments;
    private boolean isdoIcr = false;
    /** czy dokument jest zablowany podpisem - wowczas nie mozna dodac zalacznika */
    private boolean blocked;            
    private boolean showBtnMarkAttachments;
    private boolean canCreateDocFromAtt;
    private boolean canDeleteAttachments;
    private boolean czyJestPodpisany = false;
    private Collection<Map> revisionsReversed;
    private Collection<Map> attachmentBeans;
    private String newDocumentLink;
    private int attachmentMaxSize;
    
    // @IMPORT
    private Long[] attachmentIds;
    private String title;
    private String barcode;
    private String content;
    private Long attachmentId;
    private Long revisionId;
    private Map markAttachment = new HashMap();
    private static String powodUsuniecia;
    private  String decryptKey;
    private boolean encryption;
	private String publicKey;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(AttachmentsTabAction.class.getPackage().getName(),null);
    
    private FormFile file;

    private static final String EV_ADD = "add";

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new Add())
            .append(new XesLogVerification()).
            append(new IndexDocument()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doInitEdit").
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            append(new InitEdit()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doEdit").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new Edit()).
            append(new XesLogVerification()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doInitAddWithEditor").
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            append(new InitAddWithEditor()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddWithEditor").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateAdd()).
            append(EV_ADD, new AddWithEditor()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddRevision").
            append(OpenHibernateSession.INSTANCE).
            append(EV_ADD, new AddRevision()).
            append(new XesLogVerification()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new XesLogVerification()).
            append(new IndexDocument()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doMarkAttachment").
            append(OpenHibernateSession.INSTANCE).
            append(new MarkAttachment()).
            append(new XesLogVerification()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSplitAttachment").
            append(OpenHibernateSession.INSTANCE).
            append(new SplitAttachment()).
            append(new XesLogVerification()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        // tworzenie nowego dokumentu dla za??cznika utworzonego z po??czenia zaznaczonych za??cznik?w
        registerListener("doCreateNewDocument").
            append(OpenHibernateSession.INSTANCE).
            append(new CreateNewDocument()).
            append(new XesLogVerification()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
			registerListener("doIcrThread").
	        append(OpenHibernateSession.INSTANCE).
	        append(new IcrThread()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
       
			 registerListener("doDecrypt").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DecryptAttachment()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
   
    protected abstract List prepareTabs();

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());

                if(getDocumentId()==null)
                    throw new EdmException(sm.getString("NieMoznaZnalezcDokumentu")); 
                document = Document.find(getDocumentId());
                DocumentLogic logic = null;
                if(document != null && document.getDocumentKind() != null)
                	logic = document.getDocumentKind().logic();

                useScanner = DSApi.context().userPreferences().node("scanner").getBoolean("use", false);
                useSystemJTwain = DSApi.context().userPreferences().node("scanner").getBoolean("useSystemJTwain", false);
                useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
                canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);
                canCreateDocFromAtt = DSApi.context().hasPermission(DSPermission.PISMO_TWORZENIE_PISMA_Z_ZALACZNIKOW);
                blocked = (document.getBlocked() != null && document.getBlocked());

                editorOn = Configuration.additionAvailable(Configuration.ADDITION_EDITOR);
                
                readOnly = DocumentHelper.cannotUpdate(document);

                if(AvailabilityManager.isAvailable("attachment.validate.size")) {
                    attachmentMaxSize = AdditionManager.getIntProperty("attachment.size.limit");
                }


                if(AvailabilityManager.isAvailable("podpisWewnetrzny.ZablokujEdycjeZalacznikow"))czyJestPodpisany=InternalSignature.getInstance().isSignByUser(DSApi.context().getDSUser().getId(), getDocumentId());
                //ta sama klasa jest do obs?ugi za??cznik?w w umowach i tam nie ma logiki
                if(logic != null)
                	canDeleteAttachments = logic.canDeleteAttachments(document);
                
                // wybrano do podgl?du konkretny za??cznik
                if (attachmentId != null)
                {
                    attachment = document.getAttachment(attachmentId);

                    final int count = 10;

                    revisionsReversed = fun.map(
                        fun.slice(fun.reverse(attachment.getRevisions()), 0, count),
                        new lambda<AttachmentRevision, Map>()
                        {
                            public Map act(AttachmentRevision attachmentRevision)
                            {
                                BeanBackedMap result = new BeanBackedMap(attachmentRevision,
                                    "id", "size", "ctime", "revision");
                                try
                                {
                                    result.put("author", DSUser.findByUsername(attachmentRevision.getAuthor()));
                                    if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                                        result.put("signatures", AttachmentRevisionSignature.findByAttachmentRevisionId(attachmentRevision.getId()));
										
                                    result.put("encrypted", (attachment.isEncrypted() ? "TAK": "NIE "));
										
										

                                }
                                catch (EdmException e)
                                {
                                }
                                return result;
                            }
                        });
                }
                else
                {
                    class mapper implements lambda<Attachment, Map>
                    {
                        public boolean anyNullAttachments = false;

                        public Map act(Attachment attachment)
                        {
                            BeanBackedMap result = new BeanBackedMap(attachment,
                                "id", "mostRecentRevision", "document", "title", "barcode", "lparam", "cn", "wparam", "editable","encrypted");
                            try
                            {
                            	log.error("anonymous url: " + AttachmentUtils.getAnonymousUrl(attachment.getId()));
                                result.put("author", DSUser.findByUsername(attachment.getAuthor()));
                                result.put("attCtime", attachment.getCtime());
	                            result.put("editable", attachment.isEditable());
                                result.put("canSplit", AttachmentsManager.canSplit(attachment));
                                result.put("encrypted",(attachment.isEncrypted()));
                            }
                            catch (EdmException e)
                            {
                            }
                            
                            if (attachment.getRevisions().isEmpty())
                                anyNullAttachments = true;

                            return result;
                        }

                        public boolean isAnyNullAttachments()
                        {
                            return anyNullAttachments;
                        }
                    }

                    final mapper mapper = new mapper();
                    attachmentBeans = fun.map(document.getAttachments(), mapper);
                    showBtnMarkAttachments = mapper.anyNullAttachments;
                    showAddAttachments = !isdoIcr;
                 
                    if(AvailabilityManager.isAvailable("podpisWewnetrzny.ZablokujEdycjeZalacznikow")&&czyJestPodpisany)showAddAttachments=false;
                    
                    
                    if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin()){
                    	blocked = !PermissionManager.isDocumentInTaskList(document);
                    	if(showAddAttachments)
                    		showAddAttachments = !blocked;
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
   
    private class DecryptAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	 try
             {
        		 DSApi.context().begin();
        		 String a = decryptKey;
        		 if (attachmentIds==null){
        			 addActionError(sm.getString("Nie wybrano za�acznika"));
        		 }else 
        			 if (attachmentIds.length > 1) {
					addActionError(sm.getString("Wybrano za du�o za�acznik�w"));
				} else {
					Attachment atta = Attachment.find(attachmentIds[0]);
					if (!atta.isEncrypted())
						addActionError(sm.getString("Podany plik nie jest zaszyfrowany"));
				}

    		if (hasActionErrors())
    			return;

            	     Attachment atta = Attachment.find(attachmentIds[0]);
            	     AttachmentRevision revision =  atta.getMostRecentRevision();

  					AESEncryptor encryption = new AESEncryptor();
  					encryption.setUpKey(decryptKey);
  					InputStream  is = revision.getAttachmentStream();
  					File decryptFile = new File(prepareFilePatch(atta,null ,false));
  					OutputStream os = new FileOutputStream(decryptFile.getAbsolutePath());
  					encryption.decrypt(is, os);
  					streamFile(decryptFile ,revision.getMime());
  					decryptKey = null;
  					
  				
              }
             catch (InvalidCipherTextException e ){
            	 addActionError(sm.getString("Niepoprawny Klucz"));
            	 DSApi.context().setRollbackOnly();
             }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (file != null && file.getFile() != null)
                    file.getFile().delete();
            }       	
        }
    }
	private void streamFile(File sFile ,String  mime)
	{
		  try
        {
            ServletUtils.streamFile(ServletActionContext.getResponse(), sFile, mime, "Content-Disposition: attachment; filename=\""+sFile.getName()+"\"");
        }
        catch (IOException e)
        {
        	log.error("", e);
        }
		
	}
	 public String prepareFilePatch(Attachment attachment , String fileName, boolean encrypted) {
	    	StringBuilder sb = new StringBuilder(System.getProperty("java.io.tmpdir"));
	    	sb.append("//");
	    	//szyfrowany
	    	if(encrypted){
	    		
	    		sb.append(fileName.replaceAll("\\.", "_"));
	        	sb.append(".krypt");
	    	} //odrzyfrowywany
	    	else if(attachment!=null) {
	    		sb.append(attachment.getTitle());		
	    		sb.append(attachment.getFileExtension());
	    	}
	    	else{
	    		sb.append(getRandomName());
	    	}
	    	
			return sb.toString();
		}
	 private String getRandomName()
		{
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 20; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
		}
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
                addActionError(sm.getString("NiePodanoTytuluZalacznika"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId(),false);

                if (DocumentHelper.cannotUpdate(document))
                {
                	return;
                }

                Attachment attachment = document.getAttachment(attachmentId);

                String newTitle = TextUtils.trimmedStringOrNull(title, 254);
                if (attachment.getTitle() != null && !attachment.getTitle().equals(newTitle))
                    addActionMessage(sm.getString("ZmienionoTytulZalacznika"));

                attachment.setTitle(newTitle);

                String newBarcode = TextUtils.trimmedStringOrNull(barcode, 25);
                if (attachment.getBarcode() != null && !attachment.getBarcode().equals(newBarcode))
                    addActionMessage(sm.getString("ZmienionoKodKreskowy"));

                attachment.setBarcode(newBarcode);

                //tworzenie pliku XML dla dokumentu z danymi dokumentu i za??cznikiem w postaci BASE64
                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(documentId);
                }

                DSApi.context().commit();
                addActionMessage(sm.getString("ZapisanoZmiany"));
                document.changelog(DocumentChangelog.ADD_ATTACHMENT_REVISION);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateAdd implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
            {
                addActionError(sm.getString("NiePodanoTytuluZalacznika"));
            }

            if (!StringUtils.isEmpty(barcode) && barcode.trim().length() > 25)
            {
                addActionError(sm.getString("ZbytDlugiKodKreskowyMaksymalnieZnakow",25));
            }

            if (hasActionErrors())
            {
                //event.skip(EV_HANDLE_UPLOAD);
                event.skip(EV_ADD);
                return;
            }
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            if (file == null || !file.sensible())
            {                
                addActionError(sm.getString("PodanoPlikJestPustyLubNiepoprawny"));
                return;
            }
            
            File attachment = null;
            
            try
            {
            	 DSApi.context().begin();
            	 
            	 if((StringUtils.isNotBlank(publicKey)) && encryption) {
               		attachment = file.getFile();
   					AESEncryptor encryptor = new AESEncryptor();
   					encryptor.setUpKey(publicKey);
   					InputStream is = new FileInputStream(attachment.getAbsolutePath());
   					File encFile = new File(prepareFilePatch(null,file.getName() ,true));
   					//File enctemp = File.createTempFile(attachment.getName().replaceAll("\\.", "_"), ".kryp");
   					OutputStream os = new FileOutputStream(encFile.getAbsolutePath());
   					encryptor.encrypt(is, os);
   					createAttachment(encFile, documentId, title, barcode, file.getFileExtension() , true );
   					attachment.delete();
   					publicKey = null;
   					encryption=false;
   					title=null;
   					barcode=null;
  				
              }
              else if (!createAttachment(file, documentId, title, barcode)) return;

                DSApi.context().commit();

                OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);
                event.setAttribute("documentId", documentId);
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (file != null && file.getFile() != null)
                    file.getFile().delete();
                	
            }
        }
    }

    public static boolean createAttachment(FormFile file, Long documentId, String title, String barcode) throws EdmException {
        AttachmentHelper.createAttachment(documentId, file.getFile(), file.getName(), title, barcode);
        return DocumentHelper.createDocumentMetadataFileEvent(documentId);
    }
    
    public void testDecrypt(File enctemp) {
    	
    	AESEncryptor encryption = new AESEncryptor();
    //	encryption.decrypt(enctemp);
	}

    public static boolean createAttachment(File file, Long documentId, String title, String barcode ,String fileExtension,Boolean encrypted) throws EdmException {
        AttachmentHelper.createAttachment(documentId, file, file.getName(), title, barcode,fileExtension,encrypted);
        return DocumentHelper.createDocumentMetadataFileEvent(documentId);
    }

	public static boolean createAttachment(File file, Long documentId, String title, String barcode) throws EdmException {
        AttachmentHelper.createAttachment(documentId, file, file.getName(), title, barcode);
        return DocumentHelper.createDocumentMetadataFileEvent(documentId);
    }

    private class IndexDocument implements ActionListener
    {
    	IndexWriter indexWriter = null;
    	StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);
    	IndexHelper ih = new IndexHelper();
    	
        public void actionPerformed(ActionEvent event)
        {
        	// je?eli lucynka nie jest w??czona to odpuszczamy
        	if (!AvailabilityManager.isAvailable("menu.left.repository.fulltextSearch"))
        		return;
            
            try
            {
               DSApi.beginTransacionSafely();
               boolean opened =  DSApi.openContextIfNeeded();
                
                Document document = Document.find(getDocumentId(),false);

                IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_46, analyzer);
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
                indexWriter = new IndexWriter(Docusafe.getIndexes(), iwc);

                indexWriter.deleteDocuments(new Term(IndexHelper.DOCUMENT_ID_FIELD, document.getId().toString()));

				// przeindeksowanie za??cznik?w w dokumencie
				ih.IndexDocument(document.getId(), indexWriter);

				indexWriter.close();
                
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(opened);
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }

    private class InitAddWithEditor implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("editor");
        }
    }


    private class AddWithEditor implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId());
                
                if (DocumentHelper.cannotUpdate(document))
                {
                	return;
                }
                
                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                document.createAttachment(attachment);
                if(document instanceof InOfficeDocument){
                	((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
                }
                

                byte[] buf = content.getBytes();
                AttachmentRevision revision = attachment.createRevision(new ByteArrayInputStream(buf), content.length(), title+".html");//.setOriginalFilename(title+".html");
                revision.setEditable(Boolean.TRUE);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class InitEdit implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            InputStream stream = null;
            try
            {           
            	//@TODO - do poprawy, usuniecie ifa/calosci moze powodowac bledy w r?nych systemach
            	if(!DSApi.context().isTransactionOpen()){
            		DSApi.context().begin();
            	}

                AttachmentRevision revision = AttachmentRevision.find(revisionId);

                title = revision.getAttachment().getTitle();
                barcode = revision.getAttachment().getBarcode();

                stream = revision.getBinaryStream();
                if (stream != null)
                {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }

                    content = os.toString();

                    os.close();
                    stream.close();
                    stream = null;
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            catch (IOException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }

            event.setResult("editor");
        }
    }


    private class Edit implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (barcode != null)
                barcode = barcode.trim();

            try
            {
                DSApi.context().begin();

                AttachmentRevision revision = AttachmentRevision.find(revisionId);


                Attachment attachment = revision.getAttachment();
                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
                attachment.setTitle(TextUtils.trimmedStringOrNull(title, 254));

                byte[] buf = content.getBytes();
                if (DSApi.context().getPrincipalName().equals(revision.getAuthor()))
                {
                    revision.updateBinaryStream(new ByteArrayInputStream(buf), content.length());
                    revision.setSize(content.length());
                    addActionMessage(sm.getString("ZmianyZapisanoWnajnowszejIstniejacejWersjiZalacznika"));
                }
                else
                {
                    AttachmentRevision newRevision = attachment.createRevision(new ByteArrayInputStream(buf), content.length(), title+".html");//.setOriginalFilename(title+".html");
                    newRevision.setEditable(Boolean.TRUE);
                    addActionMessage(sm.getString("UtworzonoNowaWersjeZalacznika"));
                }
                DSApi.context().commit();
                event.setAttribute("documentId", documentId);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class AddRevision implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (file == null || !file.sensible())
            {                
                addActionError(sm.getString("PodanyPlikJestPustyLubNiepoprawny"));
                return;
            }
            
            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId());

                if (DocumentHelper.cannotUpdate(document))
                {
                	return;
                }

                document.getAttachment(attachmentId).createRevision(file.getFile());
                document.getDocumentKind().logic().onAddRevision(document);

                //tworzenie pliku XML dla dokumentu z danymi dokumentu i za??cznikiem w postaci BASE64
                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(documentId);
                }

                DSApi.context().commit();
                addActionMessage(sm.getString("ZaktualizowanoTrescZalacznika"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (file != null && file.getFile() != null)
                    file.getFile().delete();
            }
        }
    }

    private class Delete implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		String powod = powodUsuniecia;
    		setPowodUsuniecia(powod);
    		if (attachmentIds == null || attachmentIds.length == 0)
    			addActionError(sm.getString("NieZaznaczonoZalacznikowDoUsuniecia"));

    		boolean isEnable =AvailabilityManager.isAvailable("deleteReason");
    		if(isEnable){
    			if(powodUsuniecia == null )
    				addActionError(sm.getString("Nie podano powodu usuniecia"));
    		}
    		if (hasActionErrors())
    			return;

    		try
    		{
    			DSApi.context().begin();

                AttachmentHelper.removeAttachments(getDocumentId(), attachmentIds);
    			
    			attachmentIds = null;
    			document = null;

                OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);

                //tworzenie pliku XML dla dokumentu z danymi dokumentu i za??cznikiem w postaci BASE64
                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(documentId);
                }
    			DSApi.context().commit();
    			event.setAttribute("documentId", documentId);
    		}
    		catch (Exception e)
    		{
    			DSApi.context().setRollbackOnly();
    			addActionError(e.getMessage());
    		}
    	}
    }

    private class MarkAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // markAttachment: String -> String[]
            // klucze to napisowe reprezentacji id za??cznik?w,
            // warto?ci to pozycje wybrane z listy

            // sprawdzam, przy ilu za??cznikach wybrano jak?? warto??
            // z listy rozwijanej
            int selectedCount = 0;
            for (Iterator iter=markAttachment.entrySet().iterator(); iter.hasNext(); )
            {
                // kluczami tablicy s? identyfikatory za??cznik?w (jako napisy),
                // warto?ciami jednoelementowe tablice napis?w odpowiadaj?ce
                // warto?ciom wybranym z rozwijanej listy przy danym za??czniku
              
                Map.Entry entry = (Map.Entry) iter.next();
                if (!(entry.getKey() instanceof String) ||
                    !(entry.getValue() instanceof String || entry.getValue() instanceof String[]))
                    continue;
                try
                {
                    
                    Long.valueOf((String) entry.getKey());
                    String[] values;
                    if (entry.getValue() instanceof String) {
                        values = new String[1];
                        values[0] = (String) entry.getValue();
                    }
                    else
                        values = (String[]) entry.getValue();
                    
                    if (values.length > 0 && values[0].trim().length() > 0)
                        selectedCount++;
                }
                catch (NumberFormatException e)
                {
                    continue;
                }
            }

            if (selectedCount == 0)
                addActionError(sm.getString("NieWybranoZalacznikowDoOznaczenia"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Document document = Document.find(getDocumentId());

                if (DocumentHelper.cannotUpdate(document))
                {
                	return;
                }

                for (Iterator iter=markAttachment.entrySet().iterator(); iter.hasNext(); )
                {
                    Map.Entry entry = (Map.Entry) iter.next();
                    if (!(entry.getKey() instanceof String) ||
                        !(entry.getValue() instanceof String || entry.getValue() instanceof String[]))
                        continue;
                    try
                    {
                        String[] values;
                        if (entry.getValue() instanceof String) {
                            values = new String[1];
                            values[0] = (String) entry.getValue();
                        }
                        else
                            values = (String[]) entry.getValue();
                        if (values.length > 0 && values[0].trim().length() > 0 &&
                            (InOfficeDocument.ATTACHMENT_FILED.equals(values[0]) ||
                             InOfficeDocument.ATTACHMENT_INSPE.equals(values[0]) ||
                             InOfficeDocument.ATTACHMENT_MISSING.equals(values[0]) ||
                             InOfficeDocument.ATTACHMENT_SEEN.equals(values[0]) ||
                             "*".equals(values[0])))
                        {
                            Attachment attachment = document.getAttachment(Long.valueOf((String) entry.getKey()));
                            if ("*".equals(values[0]) && attachment.getLparam() != null)
                            {
                                ((OfficeDocument) document).addWorkHistoryEntry(
                                    Audit.create("attachments", DSApi.context().getPrincipalName(),
                                        sm.getString("UsunietoOznaczenieZzalacznika",InOfficeDocument.getAttachmentLparamDescription(attachment.getLparam()),attachment.getTitle()),
                                        "LPARAM", attachment.getId()));
                                attachment.setLparam(null);
                            }
                            else if (!values[0].equals(attachment.getLparam()))
                            {
                                attachment.setLparam(values[0]);
                                ((OfficeDocument) document).addWorkHistoryEntry(
                                    Audit.create("attachments", DSApi.context().getPrincipalName(),
                                        sm.getString("ZalacznikOznaczonoJako",attachment.getTitle(),InOfficeDocument.getAttachmentLparamDescription(values[0])),
                                        "LPARAM", attachment.getId()));
                            }
                        }
                    }
                    catch (NumberFormatException e)
                    {
                        continue;
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class SplitAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                Document document = Document.find(getDocumentId());
                Attachment attachment = document.getAttachment(attachmentId);                
                AttachmentsManager.splitAttachment(attachment);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            
            // zeruje, aby zosta?a wy?wietlona strona g??wna za??cznik?w, a nie szczeg?owa tego za??cznika
            attachmentId = null;
        }
    }
    
    private class CreateNewDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (attachmentIds == null || attachmentIds.length == 0)
            {
                addActionError(sm.getString("NieZaznaczonoZalacznikow"));
                return;
            }
            
            try
            {                               
                DSApi.context().begin();
                
                OfficeDocument doc = DocumentsManager.createDocument(OfficeDocument.findOfficeDocument(getDocumentId()), attachmentIds);
                
                if (doc.getType() == DocumentType.INCOMING)
                {
                    newDocumentLink = "/office/incoming/summary.action?";
                }
                else if (doc.getType() == DocumentType.OUTGOING)
                {
                    newDocumentLink = "/office/outgoing/summary.action?";
                }
                else if (doc.getType() == DocumentType.INTERNAL)
                {
                    newDocumentLink = "/office/internal/summary.action?";
                }

                newDocumentLink += "documentId="+doc.getId();//+"&activity="+getActivity();
                
                DSApi.context().commit();
                if (doc.getOfficeNumber() != null)
                    addActionMessage(sm.getString("UtworzonoNowyDokumentOnumerzeProszeZweryfikowacPoprawnoscJegoAtrybutow",doc.getOfficeNumber()));
                else
                    addActionMessage(sm.getString("UtworzonoNowyDokumentOIDProszeZweryfikowacPoprawnoscJegoAtrybutow",doc.getId()));
               
                event.setResult("doc-from-att-created");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class IcrThread implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	isdoIcr = true;        	
        	Thread thread = new Thread(new icrThread(getDocumentId(),getAttachmentId()));
    		thread.start();
        	addActionMessage("Uruchomiono proces ciecia dla zalacznika");           	
	    }
    }
    
    public List getAttachments()
    {
        return attachments;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setAttachmentIds(Long[] attachmentIds)
    {
        this.attachmentIds = attachmentIds;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Long getAttachmentId()
    {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId)
    {
        this.attachmentId = attachmentId;
    }

    public Long getRevisionId()
    {
        return revisionId;
    }

    public void setRevisionId(Long revisionId)
    {
        this.revisionId = revisionId;
    }

    public Attachment getAttachment()
    {
        return attachment;
    }

    public Document getDocument()
    {
        return document;
    }

    public Map getMarkAttachment()
    {
        return markAttachment;
    }

    public boolean isReadOnly()
    {
        return readOnly;
    }

    public boolean isUseScanner()
    {
        return useScanner;
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public boolean isCanSign()
    {
    	return canSign;
    }
    
    public boolean isBlocked()
    {
        return blocked;
    }

    public boolean isEditorOn()
    {
        return editorOn;
    }

    public boolean isShowBtnMarkAttachments()
    {
        return showBtnMarkAttachments;
    }
    
    public boolean isCanDeleteAttachments()
	{
		return canDeleteAttachments;
	}

    public Collection<Map> getRevisionsReversed()
    {
        return revisionsReversed;
    }

    public Collection<Map> getAttachmentBeans()
    {
        return attachmentBeans;
    }

    public boolean isUseSystemJTwain()
    {
        return useSystemJTwain;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public FormFile getFile()
    {
        return file;
    }

    public void setRevisionFile(FormFile file)
    {
        this.file = file;
    }

    public FormFile getRevisionFile()
    {
        return file;
    }

    public boolean isCanCreateDocFromAtt()
    {
        return canCreateDocFromAtt;
    }

    public String getNewDocumentLink()
    {
        return newDocumentLink;
    }

	public void setShowAddAttachments(boolean showAddAttachments)
	{
		this.showAddAttachments = showAddAttachments;
	}

	public boolean isShowAddAttachments()
	{
		return showAddAttachments;
	}
	
	public String getPowodUsuniecia()
	{
		return powodUsuniecia;
	}

	public void setPowodUsuniecia(String powodUsuniecia)
	{
		this.powodUsuniecia=powodUsuniecia;
	}

	public boolean isczyJestPodpisany() {
		return czyJestPodpisany;
	}

	public void setczyJestPodpisany(boolean czyJestPodpisany) {
		this.czyJestPodpisany = czyJestPodpisany;
	}
	
	public boolean getEncryption() {
		return encryption;
	}
	
	public void setEncryption(boolean encryption) {
		this.encryption = encryption;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

    public int getAttachmentMaxSize() {
        return attachmentMaxSize;
    }

    public void setAttachmentMaxSize(int attachmentMaxSize) {
        this.attachmentMaxSize = attachmentMaxSize;
    }

	public String getDecryptKey() {
		return decryptKey;
	}

	public void setDecryptKey(String decryptKey) {
		this.decryptKey = decryptKey;
	}
}
