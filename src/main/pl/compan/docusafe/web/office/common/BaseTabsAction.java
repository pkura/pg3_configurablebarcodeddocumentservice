package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: BaseTabsAction.java,v 1.18 2010/06/29 13:02:25 pecet1 Exp $
 */
public abstract class BaseTabsAction extends EventActionSupport implements DocumentTabAction
{
    private static final Logger log = LoggerFactory.getLogger(BaseTabsAction.class);
    //ustawienia indiwidualne w HOME
    private static String individualJavaScript;
    
    protected Long documentId;
	private Document document;
	
    protected List<Tab> tabs;
    private String activity;
    private String process;
    private boolean externalWorkflow;    
    
    protected String getTaskListResult(DocumentType documentType)
    {
        if (documentType == DocumentType.INTERNAL)
        {
            return "task-list-int";
        }
        else if (documentType == DocumentType.OUTGOING)
        {
            return "task-list-out";
        }
        else if (documentType == DocumentType.ORDER)
        {
            return "task-list-order";
        }
        else
        {
            return "task-list";
        }
    }

	 /**
     * Rodzaj dokumentu - u�ywane w plikach JSP wsp�lnych dla dokument�w
     * przychodz?cych, wychodz?cych i innych.
     * @return 'in' lub 'out'.
     */
    public abstract String getDocumentType();

    /**
     * Podaje wynik akcji, przekierowujacy do danej listy zadan w zaleznosci od typu "aktualnie przetwarzanego dokumentu".
     * Do tego celu uzywana jest funkcja getDocumentType.
     * @return napis reprezentujacy wynik akcji przekierowujacy do listy zadan
     */
    protected String getTaskListResult()
    {
        if (OutOfficeDocument.INTERNAL_TYPE.equals(getDocumentType()))
        {
            return "task-list-int";
        }
        else if (OutOfficeDocument.TYPE.equals(getDocumentType()))
        {
            return "task-list-out";
        }
        else if (OfficeOrder.TYPE.equals(getDocumentType()))
        {
            return "task-list-order";
        }
        else
        {
            return "task-list";
        }
    }

    public final String getActivity()
    {
        //z jakich� wzgl�d�w do activiti dodaje si� spacja i kolejne warto�ci
        // ten kod sprawdza to i ewentualnie usuwa niepotrzebne fragmenty
        if(activity != null && activity.indexOf(' ') >= 0){
            //ignoruj ci�g znak�w do pierwszej spacji
            activity = activity.substring(0, activity.indexOf(' ') - 1);
        }
        return activity;
    }
    
    public final Long getDocumentId()
    {
        return documentId;
    }

    public final void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public final List<Tab> getTabs()
    {
        return tabs;
    }

    public final void setTabs(List<Tab> tabs)
    {
        this.tabs = tabs;
    }


    public final void setActivity(String activity)
    {
        this.activity = activity;
    }

    public String getProcess()
    {
        return process;
    }

    public void setProcess(String process)
    {
        this.process = process;
    }

    public boolean isExternalWorkflow()
    {
        return externalWorkflow;
    }

    public void setExternalWorkflow(boolean externalWorkflow)
    {
        this.externalWorkflow = externalWorkflow;
    }

	public Document getDocument() {
        if(document == null && documentId != null) {
            try {
                document = OfficeDocument.find(documentId);
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	/**
	 * Inicjalizuje pole document je�li documentId != null
	 */
	public class SimpleDocumentInit extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			if(documentId != null){
				document = Document.find(documentId);
			}
		}

		@Override
		public Logger getLogger() {
			return _SDI_LOG;
		}
	}
	private static final Logger _SDI_LOG = LoggerFactory.getLogger(SimpleDocumentInit.class);

	public class DockindDocumentInit extends SimpleDocumentInit{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			super.actionPerformed(event, log);
		}

		@Override
		public Logger getLogger() {
			return _DDI_LOG;
		}
	}
	private static final Logger _DDI_LOG = LoggerFactory.getLogger(SimpleDocumentInit.class);

     /**
     * Zwraca uproszczony encodowany url do tej strony - tylko z documentId i activity
     * @return
     */
    public final String getSimpleReturnUrl(){
        String returnUrl;
        returnUrl = ServletActionContext.getRequest().getRequestURL().toString();
        returnUrl += String.format("?documentId=%d&activity=%s", getDocumentId(), StringUtils.trimToEmpty(getActivity()));
        returnUrl = HttpUtils.urlEncode(returnUrl);
        log.info("returnUrl '{}'", returnUrl);
        return returnUrl;
    }
    
    
    public String getIndividualJavaScript()
    {
    	try
    	{
    		if(individualJavaScript == null)
    		{
	    		File htmlHelpF = new File(Docusafe.getHome().getAbsolutePath()+"/individualJavaScript.js");
	            individualJavaScript = FileUtils.readFileToString(htmlHelpF);
    		}
    	}catch(Exception e){
    		log.error("", e);
    	}
    	
    	return individualJavaScript;
    }
}
