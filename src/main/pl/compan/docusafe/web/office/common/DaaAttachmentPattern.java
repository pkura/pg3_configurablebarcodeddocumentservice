package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.boot.Docusafe;

import java.util.List;
import java.util.ArrayList;
import java.io.*;
/* User: Administrator, Date: 2006-06-26 11:57:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DaaAttachmentPattern.java,v 1.4 2009/10/08 11:44:46 tomekl Exp $
 */
public class DaaAttachmentPattern
{
    static private String PATTERNS_DIRECTORY = "DAA-wzory_umow";
    static private String PATTERNS_CONFIG_FILE = "DAA-wzory_umow.config";

    Long id;
    Integer typeId;
    String filename;

    public DaaAttachmentPattern(Long id, Integer typeId, String filename)
    {
        this.id = id;
        this.typeId = typeId;
        this.filename = filename;
    }

    public String getFilename()
    {
        return filename;
    }

    public Long getId()
    {
        return id;
    }

    public Integer getTypeId()
    {
        return typeId;
    }

    public File getFile()
    {
        /*File file = new File(Docusafe.getHome().getAbsolutePath()
				+ File.pathSeparator + PATTERNS_DIRECTORY + File.pathSeparator
				+ filename);*/
    	File file = new File(Docusafe.getHome().getAbsolutePath() + "/" + PATTERNS_DIRECTORY + "/" + filename);
        return file;
    }

    /*
    static public List<DaaAttachmentPattern> getAttachmentPatterns()
    {
        File directory = new File(Docusafe.getHome().getAbsolutePath()+"\\"+PATTERNS_DIRECTORY);
        String[] children = directory.list();
        List<DaaAttachmentPattern> patterns = new ArrayList<DaaAttachmentPattern>();
        for (int i=0; i < children.length; i++)
        {
            patterns.add(new DaaAttachmentPattern(new Long(i+1), null, children[i]));
        }
        return patterns;
    }
     */

    static public List<DaaAttachmentPattern> getAttachmentPatternsFromConfig()
    {
        File configFile = new File(Docusafe.getHome(), PATTERNS_CONFIG_FILE);
        List<DaaAttachmentPattern> patterns = new ArrayList<DaaAttachmentPattern>();
        if (configFile.exists())
        {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile)));
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null)
                {
                    if (line.trim().startsWith("#"))
                        continue;
                    String[] fields = line.split("\\s*,\\s*");
                    try {
                        if (fields.length == 2) {
                            Integer typeId = new Integer(fields[0]);
                            i++;
                            patterns.add(new DaaAttachmentPattern(new Long(i), typeId, fields[1]));
                        }
                    }
                    catch (NumberFormatException e)
                    {
                    }
                }
            }
            catch (IOException e)
            {
            }
        }

        return patterns;
    }
}
