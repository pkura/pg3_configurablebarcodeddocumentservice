package pl.compan.docusafe.web.office.common;

import org.codehaus.jackson.map.ObjectMapper;
import pl.compan.docusafe.core.jackrabbit.JackrabbitDocumentVersion;
import pl.compan.docusafe.core.jackrabbit.entities.DocumentVersionEntity;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.docusafe.utils.JsonUtils;

import java.util.List;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public abstract class DocumentVersionJcrTabAction extends BaseTabsAction {

    public final static Logger log = LoggerFactory.getLogger(DocumentVersionJcrTabAction.class);

    protected abstract List prepareTabs();

    List<DocumentVersionEntity> versions;

    @Override
    protected void setup() {

        registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) throws Exception {
            try {
                setTabs(prepareTabs());
                versions =  JackrabbitDocumentVersion.instance().getVersionList(documentId);

            }catch (Exception e) {
                log.error("", e);
            }

        }
    }

    public List<DocumentVersionEntity> getVersions() {
        return versions;
    }

    public void setVersions(List<DocumentVersionEntity> versions) {
        this.versions = versions;
    }
}
