package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentSignature;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.base.Flags.FlagBean;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.dockinds.logic.DaaLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.graphs.GraphGenerator;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.core.xes.XesLogAction;
import pl.compan.docusafe.core.xes.XesXmlFactory;
import pl.compan.docusafe.nationwide.CompileAttachmentsManager;
import pl.compan.docusafe.nationwide.CompileAttachmentsManager.CompilationStatus;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Daa;
import pl.compan.docusafe.web.certificates.SignXmlAction;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.commons.BoxAction;
import pl.compan.docusafe.web.commons.OfficeDocumentHelper;


import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import std.fun;
import std.lambda;

import com.lowagie.text.Cell;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2006-03-15 09:47:03 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SummaryTabAction.java,v 1.142 2010/08/13 10:30:28 pecet5 Exp $
 */
public abstract class SummaryTabAction extends ProcessBaseTabsAction implements BoxAction
{
    public static final Logger log = LoggerFactory.getLogger(SummaryTabAction.class);

    private StringManager smG = GlobalPreferences.loadPropertiesFile("",null);;

    private boolean enableCollectiveAssignments;
    private Map<String, String> wfProcesses;
    private Map<String, String> assignmentsMap;
    private Collection<Map> attachments;
    private Long mostRecentRevisionId;
    private DocumentSignature recentSignature;
    private Map recentRemark;
    private boolean canReopenWf;
    private boolean canAssignMe;
    private boolean canFinish;
    private FinishOrAssignState finishOrAssignState;
    private boolean graphML ;
    private boolean graphDot ;
    private boolean canUpdate;
    private boolean multiRealizationAssignment;
    private boolean doWiadomosci;

//    private boolean canFinish;
//    private String finishWarning;
    private String creatingUser;
    private String assignedDivision;
    private String caseDocumentId;

    /** czy dokument jest zablokowany podpisem */
    private boolean blocked;
    /** dodatkowa tre�� dokumentu dla Daa */
    private String daaSummary;
    /** true <=> nieprzypisany do agenta ani agencji (tylko dla dokumentow DAA) */
    private boolean daaUnassigned;

    private String abstrakt;
    private boolean useSignature;
    private boolean canSign;
    private boolean canAccept;
    private Map<String,String> substituted = Collections.emptyMap();
    private Map<String,String> substitutedNames = Collections.emptyMap();
    /** info o mo�liwo�ci wykonania operacji "kompiluj pliki sprawy" */
    private CompilationStatus compilationStatus;
    private boolean canGenerateDocumentView;
    private boolean canGenerateCsvDocument;

    private Map<Long,String> jbpmProcesses;
    private Long jbpmProcessId;
    private String jbpmLink;
    private String initJbpmLink;
    private boolean canInitJbpmProcess;
    private String boxNumber;
    private boolean showNotInBoxMessage;
    private List<JbpmManager.ProcessSummary> jbpmSummary;

    protected boolean canAddToWatched;
    protected boolean canAssignToCoordinator;

    // na potrzeby akceptacji dokumentu
    private boolean acceptancesEnabled;
    private FieldsManager fm;
    private String acceptanceCn;
    private Map<Integer,String> centrumAcceptanceCn = new LinkedHashMap<Integer, String>();
    /** identyfikator obiektu, dla kt�rego wycofa� akceptacj� */
    private Long objectId;

    private List objectives;
    private String objectiveSel;
    private boolean showObjectives;

    public boolean showSendToEva;
    private String evaMessage;
    protected List<Label> modifiableLabels;
    private List<Label> nonModifiableLabels;
    private Long[] labelsIds;

    private StringManager sm = GlobalPreferences.loadPropertiesFile(SummaryTabAction.class.getPackage().getName(),null);

    private Boolean openViewer;
    private Boolean viewQc;

	private boolean pudloModify;

	private boolean needsNotBox;

	private String currentBoxNumber;

	private Long currentBoxId;

	private boolean boxNumberReadonly;
	
	private String documentForm;

	private Long boxId;
	protected static OfficeDocumentHelper officeDocumentHelper = new OfficeDocumentHelper();

    private static final String EV_MANUAL_FINISH = "evManualFinish";
    private static final String EV_FILL = "fill";

    public Boolean getViewQc() {
        return viewQc;
	}

	public void setViewQc(Boolean viewQc) {
		this.viewQc = viewQc;
	}

    public boolean isBoxActionAvailable() {
           //return AvailabilityManager.isAvailable("summary.pudlo", getDocumentKindCn());
           return true;
    }

	protected String preExecute()
    {
        if (getDocumentId() == null)
        {
            if (this instanceof pl.compan.docusafe.web.office.out.SummaryAction)
            {
                return "task-list-out";
            }
            else if (this instanceof pl.compan.docusafe.web.office.internal.SummaryAction)
            {
                return "task-list-int";
            }
            else if (this instanceof pl.compan.docusafe.web.office.order.SummaryAction)
            {
                return "task-list-order";
            }
            else
            {
                return "task-list";
            }
        }

        return null;
    }

    public class UpdateBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			try 
			{
				DSApi.context().begin();
				officeDocumentHelper.updateBox(SummaryTabAction.this);
				DSApi.context().commit();
	            addActionMessage(sm.getString("ZapisanoNumerPudlaArchiwalnego"));
			}
			catch (EdmException e) {
				log.error(e.getMessage(), e);
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
        }
    }

    protected void prepareLabels() throws EdmException
    {
    	//modifiableLabels = LabelsManager.getWriteLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
    	//nonModifiableLabels = LabelsManager.getNonModifiableLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
    	//modifiableLabels = LabelsManager.getWriteLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
    	modifiableLabels = LabelsManager.getDocumentLabelsForUser(DSApi.context().getPrincipalName(), getDocumentId(), Label.LABEL_TYPE);
    	nonModifiableLabels = LabelsManager.getLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE, true, false, false);
//    	nonModifiableLabels = LabelsManager.getReadLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
//    	nonModifiableLabels.removeAll(LabelsManager.getWriteLabelsForDocument(getDocumentId(), DSApi.context().getPrincipalName(), Label.LABEL_TYPE));
    }
    // wym�g utworzenia dw�ch tablic jeszcze zanim jest wiadomo, czy zostan� u�yte
    // mo�e si� wydawa� ma�o wydajny, ale w wi�kszo�ci wdro�e� zbiorcza dekretacja
    // JEST u�ywana, wi�c tablice przewa�nie s� u�ywane
    protected void prepareCollectiveAssignments() throws EdmException
    {
        //smG = GlobalPreferences.loadPropertiesFile("",null);
        boolean collectiveAssignmentPossible =
            (Configuration.officeAvailable() || Configuration.faxAvailable())
                && !StringUtils.isEmpty(getActivity())
                && (DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_ZBIORCZA) || DSApi.context().hasPermission(DSPermission.WWF_DEKRETACJA_DOWOLNA));

        if (!collectiveAssignmentPossible)
            return;

        Map<String, Map<String, String>> assignmentMap = WorkflowFactory.getInstance().prepareCollectiveAssignments(getActivity());

        if (assignmentMap == null) {
        	enableCollectiveAssignments = false;
        } else {
			enableCollectiveAssignments = true;
        	assignmentsMap = assignmentMap.get("assignmentsMap");
        	wfProcesses = assignmentMap.get("wfProcesses");
        	substituted = assignmentMap.get("substituted");
        	substitutedNames = assignmentMap.get("substitutedNames");
        }

        showObjectives = false;
        if (DSApi.context().userPreferences().node("other").getBoolean("multiAssignmentObjective", false))
        {
            objectives = AssignmentObjective.list();

            // Business: domy�lnie wybrany jest pierwszy cel dekretacji
            if (Configuration.hasExtra("business"))
            {
                if (objectives.size() > 0)
                    objectiveSel = ((AssignmentObjective) objectives.get(0)).getName();
            }

            showObjectives = true;
        }
    }

    protected boolean collectiveAssignment(final String[] assignments,
            final String activityId, ActionEvent event) throws EdmException
    {

    	 String[] activityIds = new String[] { activityId };
    	 Set<String> paSet = new HashSet<String>();
    	 for (int i=0; i < assignments.length; i++)
         {
             String processDesc =  ServletActionContext.getRequest().getParameter("assignmentProcess_"+assignments[i]);
    		 String[] tmp = processDesc.split("::");
    		 if (tmp.length < 2)
    			 continue;
    		 String objective;
    		 if (tmp[1].equals(WorkflowFactory.wfManualName())){
    			 objective = smG.getString("doRealizacji");
    		 } else {
    			 objective = smG.getString("doWiadomosci");
    		 }
    		 String pa = WorkflowFactory.getPreparedAssignment(assignments[i], tmp[1], objective);
    		 paSet.add(pa);
         }
    	 Document document = Document.find(WorkflowFactory.getDocumentId(activityId));
    	 String[] paArray = paSet.toArray(new String[paSet.size()]);

    	 WorkflowFactory.multiAssignment(
				 activityIds,
				 DSApi.context().getPrincipalName(),
				 paArray,
				 null,
				 event);

		 TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());
    	 return true;
    }

    protected void prepareAttachments(OfficeDocument document) throws EdmException
    {
    	List<Attachment> l = document.getAttachments();
    	if(l != null && l.size() > 0)
    	{
    		for (Attachment attachment : l) 
    		{
        		AttachmentRevision r = attachment.getMostRecentRevision();
        		if(r != null)
        		{
        			mostRecentRevisionId = r.getId();
        			break;
        		}
			}
    	}
        attachments = fun.map(l, new lambda<Attachment, Map>()
        {
            public Map act(Attachment attachment)
            {
                BeanBackedMap map = new BeanBackedMap(attachment,
                    "id", "title", "barcode", "lparam", "mostRecentRevision", "cn");
                try
                {
                    map.put("userSummary", DSUser.safeToFirstnameLastname(attachment.getAuthor()));
                    map.put("attCtime", attachment.getCtime());
                    map.put("id", attachment.getId());
                }
                catch (EdmException e)
                {
                }
                return map;
            }
        });
        
    }

    protected void updateRecentRemark(OfficeDocument document) throws EdmException
    {
        if (document.getRemarks().size() > 0)
        {
            recentRemark = new BeanBackedMap(document.getRemarks().size()-1, "content");
            recentRemark.put("author", DSUser.safeToFirstnameLastname(document.getRemarks().get(document.getRemarks().size()-1).getAuthor()));
            recentRemark.put("content", document.getRemarks().get(document.getRemarks().size()-1).getContent());
        }
    }

    protected abstract List prepareTabs();

    protected abstract String getExternalWorkflowBaseLink();

    protected void fillForm(OfficeDocument document, WorkflowActivity activity) throws EdmException
    {
    	viewQc = false;

        if (document.getDocumentKind() != null)
        {
            document.getDocumentKind().initialize();
            // czy mo�liwe jest wygenerowanie obrazu w poostaci PDF dla tego dokumentu
            canGenerateDocumentView = document.getDocumentKind().logic().canGenerateDocumentView();
            canGenerateCsvDocument = document.getDocumentKind().logic().canGenerateCsvView();

            Map<String, String> properties = document.getDocumentKind().getProperties();
            viewQc = DSApi.context().hasPermission(DSPermission.NW_QC);
            viewQc = properties.get("hotIcrFolder") != null ? viewQc : false;
        }

        //SummaryTabAction.log.debug(viewQc.toString());

        setTabs(prepareTabs());
        setExternalWorkflow(false);
        DSApi.initializeProxy(document.getRecipients());
        prepareAttachments(document);
        updateRecentRemark(document);

        // czy w��czona jest dekretacja na wiele os�b do realizacji
        multiRealizationAssignment = Configuration.isAdditionOn(Configuration.ADDITION_MULTI_REALIZATION_ASSIGNMENT);

        officeDocumentHelper.initBox(SummaryTabAction.this);
        canUpdate = document.canModify();
        
        // sprawdzanie czy mozliwa kompilacja plikow sprawy dla tego dokumentu
        compilationStatus = CompileAttachmentsManager.checkCompilationStatus(document);

        // inicjalizacja na potrzeaby zewnetrzengo workflow JBPM
        canInitJbpmProcess = JbpmManager.canInitJbpmProcess(document, getActivity());
        showNotInBoxMessage = JbpmManager.showNotInBoxMessage(document);

        showSendToEva = false;
        // czy wlaczyc funkcje wysylania do interfejsu EVA
        if (document.getDocumentKind() != null &&
        		document.getDocumentKind().getCn() != null &&
        		document.getDocumentKind().getCn().equals(DocumentLogicLoader.ROCKWELL_KIND))
        {
        	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        	Integer tmp = (Integer)fm.getKey(RockwellLogic.EVA_STATUS_FIELD_CN);
        	if(tmp ==0)
        	{
        		showSendToEva = true;
        	}
        	else
        	{
        		evaMessage = "Document was sent to Eva";
        	}
        }

        if (StringUtils.isNotEmpty(getActivity()) && !WorkflowFactory.jbpm) {
        	doWiadomosci = !WorkflowFactory.getInstance().isManual(getActivity());
        } else {
        	doWiadomosci = false;
        }
        if (WorkflowFactory.getInstance().allowAssignment(activity)) {
        	prepareCollectiveAssignments();
        }
        canReopenWf = !WorkflowFactory.jbpm && !WorkflowFactory.documentHasTasks(getDocumentId());
        canAssignMe = !WorkflowFactory.jbpm && WorkflowFactory.getInstance().checkCanAssignMe(getDocumentId(), getActivity());
        canFinish =   !WorkflowFactory.jbpm && StringUtils.isNotEmpty(getActivity());
        if (!WorkflowFactory.jbpm)
     	   finishOrAssignState = WorkflowFactory.getFinishOrAssignState(document, activity);

        caseDocumentId = document.getCaseDocumentId();
        assignedDivision = DSDivision.find(document.getAssignedDivision()).getPrettyPath();
        creatingUser = DSUser.safeToFirstnameLastname(document.getCreatingUser());
        if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
        {
            blocked = (document.getBlocked() != null && document.getBlocked());
            recentSignature = DocumentSignature.findMostRecentSignature(document.getId()/*, true*/);
            useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
            canSign = DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE);
            canAccept = DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA);
        }
        
        fm = document.getDocumentKind().getFieldsManager(document.getId());
        fm.initialize();

        if(document.getAbstractDescription() != null) {
            abstrakt = document.getAbstractDescription();
        }

        // inicjalizacja na potrzeby akceptacji dokumentu
        if (document.getDocumentKind() != null && document.getDocumentKind().getAcceptancesDefinition() != null
          		&& "true".equals(document.getDocumentKind().getProperties().get(DocumentKind.ACCEPTANCES_IN_SUMMARY)))
	    {
			acceptancesEnabled = true;
			fm.initializeAcceptances();
	    }
        // tresc dla dokumentu archiwum agenta
        if (Docusafe.hasExtra("daa") && (document.getDoctype() != null) && ("daa".equals(document.getDoctype().getCn())))
        {
        	daaSummary = Daa.DEFAULT_TITLE;
        	EnumItem typItem = fm.getEnumItem(DaaLogic.TYP_DOKUMENTU_FIELD_CN);
        	EnumItem siecItem = fm.getEnumItem(DaaLogic.RODZAJ_SIECI_FIELD_CN);
        	DaaAgencja agencja = (DaaAgencja) fm.getValue(DaaLogic.AGENCJA_FIELD_CN);
        	DaaAgent agent = (DaaAgent) fm.getValue(DaaLogic.AGENT_FIELD_CN);
        	
        	if (DaaLogic.AGENCJA_CN.equals(typItem.getArg(1)))
        	{
        		if (agencja != null && (agencja.getNumer() != null) || (agencja.getNip() != null))
        			daaSummary += ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
        		daaSummary += " / " + siecItem.getTitle();
        	}
        	else if (DaaLogic.AGENT_CN.equals(typItem.getArg(1)) || DaaLogic.INNE_CN.equals(typItem.getArg(1)))
        	{	
        		if (agent != null && agent.getNumer() != null)
        			daaSummary += " / " + agent.getNumer();
        		if (agencja != null && (agencja.getNumer() != null) || (agencja.getNip() != null))
        			daaSummary += ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
        		daaSummary += " / " + siecItem.getTitle();
        	}
        	else if (DaaLogic.RAPORTY_CN.equals(typItem.getArg(1)))
        	{
        		daaSummary += " / " + fm.getEnumItem(DaaLogic.KLASA_RAPORTU_FIELD_CN).getTitle();
                if (agencja != null)
                {
                    if (agencja.getNumer() != null || agencja.getNip() != null)
                        daaSummary += ((agencja.getNumer() != null) ? (" / " + agencja.getNumer()) : "") + ((agencja.getNip() != null) ? (" / " + agencja.getNip()) : "");
                    daaSummary += " / " + siecItem.getTitle();
                }
        	}
        	
        	if (agent == null && agencja == null && !(DaaLogic.RAPORTY_CN.equals(typItem.getArg(1))))
                daaUnassigned = true;
        }
        
        if (AvailabilityManager.isAvailable("podsumowanie.formaDokumentu", fm.getDocumentKind().getCn()))
        	documentForm = fm.getValue("DOCUMENT_FORM").toString();
    }

    public class Watch implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                WfActivity activity = getActivity() != null
                    ? WorkflowFactory.getWfActivity(getActivity())
                    : null;
                setExternalWorkflow(false);
                if (getActivity() != null && !WorkflowFactory.jbpm) {
                   DSApi.context().watch(URN.create(activity));
                } else {
                    DSApi.context().watch(URN.create(Document.find(getDocumentId())));
                }
                DSApi.context().commit();

                addActionMessage(sm.getString("DodanoDoObserwowanych"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                try {DSApi.context().rollback(); } catch (EdmException f) {};
            }
        }
    }
    
  
    /**
     * Klasa umozliwia wygenerowanie xes loga dokumentu 
     *   
     */
    public class XesLogFromDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
               XesXmlFactory xf = new XesXmlFactory();
               XesLog xld = XesLog.getXesLogByDocumentId(getDocumentId());
               if(xld == null){
            	   addActionMessage("Nie istnieje logu dla dokumentu");
               }else 
            	   
               xf.generateXesXml(xld, xld.getId(), true, false);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                try {DSApi.context().rollback(); } catch (EdmException f) {};
            } catch (Exception e)
			{
            	 addActionError(e.getMessage());
				log.error("Brak xesLog dla dokumentu o ID :" +getDocumentId(), e);
			}
        }
    }
    /**
     * Klasa umozliwia wygenerowanie Grafu Przeplywu zdazen  dokumentu 
     *   
     */
	public class Graph implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				GraphGenerator generator = new GraphGenerator(OfficeDocument.find(documentId));
				generator.generateGraph(isGraphDot(), isGraphML());
				
			} catch (Exception e)
			{
				addActionError(e.getMessage());
				log.error("Brak xesLog dla dokumentu o ID :" + getDocumentId(), e);
			}
		}
	}

    public class PrintCard implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                Map<String, Object> params = Maps.newHashMap();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                OfficeDocument document = OfficeDocument.find(documentId);
                FieldsManager fm = document.getFieldsManager();
                fm.initialize();

                String templateName = "kartazastepcza.rtf";
                String special = "true";
                ServletActionContext.getResponse().setContentType("text/rtf");
                DocFacade df = Documents.document(documentId);
                params.put("fields", df.getFields());
                params.put("DOC_NUMBER", documentId);
                params.put("AUTHOR", document.getAuthorFirstLastName());

                try {
                    if (document.getDocumentDate() != null) {
                        params.put("DATA",dateFormat.format(document.getDocumentDate()));
                    } else if(document.getCtime() != null){
                        params.put("DATA", dateFormat.format(document.getCtime()));
                    } else {
                        params.put("DATA", " dokument bez daty");
                    }
                } catch (Exception e) {

                }

                // numer KO
                params.put("KO", document.getOfficeNumber() != null ? document.getOfficeNumber().toString(): "...........");
                params.put("BARCODE", document.getBarcode() != null ? document.getBarcode() : "Brak numeru Barcode");

                Map<Class, Format> defaultFormatters = Maps.newLinkedHashMap();
                ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + templateName + "\"");
                Templating.rtfTemplate(templateName, params, ServletActionContext.getResponse().getOutputStream(), defaultFormatters, special);
            } catch (IOException e) {
                LOG.error("", e);
            } catch (Exception e) {
                LOG.error("", e);
            }
        }
    }

    public class DocumentMetrics implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	  {
    		try {
    	//	Long a =(long) 1;
    		 OfficeDocument doc = OfficeDocument.find(documentId);
    		 String filename;
             Date now = new Date();
             filename = DateUtils.formatDateWithHoursWithoutDash(now)+"_ID"+documentId;
    		
    		 ServletUtils.streamFile(ServletActionContext.getResponse(), pdfList(doc), "application/pdf",
				"Content-Disposition: attachment; filename=\""+filename+".pdf\"");
    		
    		}catch (Exception e) {
    			log.error("", e);
    		}
    	  }
    		 
        class HistoriaWpis
        {
        	Date data;
        	String osoba;
        	String czynnosc;
        	String identyfikator;
        	String className;
        
        	HistoriaWpis(Date data, String osoba, String czynnosc, String identyfikator){
        		this.data=data;
        		this.osoba=osoba;
        		this.czynnosc=czynnosc;
        		this.identyfikator=identyfikator;
        	}
        	
        	HistoriaWpis(Date data, String osoba, String czynnosc, String identyfikator, String className){
        		this.data=data;
        		this.osoba=osoba;
        		this.czynnosc=czynnosc;
        		this.identyfikator=identyfikator;
        		this.className = className;
        	}
        }
       
        private File pdfList(OfficeDocument document) throws EdmException
		{
        	
         	List<HistoriaWpis> historia = getData(document);
        
         	// sortowanie po dacie dokumentu
			Collections.sort(historia, new Comparator<HistoriaWpis>() {
				public int compare(HistoriaWpis document1,HistoriaWpis document2) {
					long value = document1.data.getTime() - document2.data.getTime();
					if(value < 0 ){
						return -1;
					}else{
						return 1;
					}
				}
			});
        	
        	File pdfFile= null;
			try {
				// font
				File fontDir= new File(Docusafe.getHome(), "fonts");
				File arial= new File(fontDir, "arial.ttf");
				BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

				Font fontSuperSmall= new Font(baseFont, (float) 6);
				Font fontSmall= new Font(baseFont, (float) 8);
				Font fontNormal= new Font(baseFont, (float) 12);
				Font fontNormal_Large= new Font(baseFont, (float) 14);
				Font fontLarge= new Font(baseFont, (float) 16);

				// file
				pdfFile= File.createTempFile("docusafe_", "_tmp");
				pdfFile.deleteOnExit();
				com.lowagie.text.Document pdfDoc= new com.lowagie.text.Document(PageSize.A4);
				PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

				pdfDoc.open();

				// content

				/* ********************************** */
				Table topTable= new Table(2);
				topTable.setCellsFitPage(true);
				topTable.setWidth(100);
				topTable.setPadding(3);
				topTable.setBorder(1);
				topTable.setWidths( new int[]{25,75});
				Cell oznaczenieSprawyLabel;
				if(AvailabilityManager.isAvailable("ins.wlacz.parametryzacjaMetrykiDokumentu")){
					 oznaczenieSprawyLabel = new Cell(new Paragraph("KO dokumentu", fontNormal));
				} else {
					 oznaczenieSprawyLabel = new Cell(new Paragraph("Oznaczenie sprawy", fontNormal));
				}
				Cell oznaczenieSprawy ;
				if (AvailabilityManager.isAvailable("PAA")){
					 String ZnakPisma = "Nie nadano"; 
					 String numerKo = "Brak";
					 
					 if (document.getOfficeNumber()!= null )
							numerKo = document.getOfficeNumber().toString(); 
						if (document.getFieldsManager().getStringKey("znak_pisma")!=null)			
						ZnakPisma = document.getFieldsManager().getStringKey("znak_pisma");
						
						String wpis = "Numer KO : "+numerKo+" , Znak pisma : "+ZnakPisma+"";
						
						 oznaczenieSprawy= new Cell(new Paragraph(wpis, fontNormal));
				} else{
                    Integer officeNumber = document.getOfficeNumber();
					oznaczenieSprawy= new Cell(new Paragraph("Numer KO : "+ (officeNumber != null ? officeNumber.toString() : "Brak"), fontNormal));
				}
				Cell tytulSprawyLabel;
				if(AvailabilityManager.isAvailable("ins.wlacz.parametryzacjaMetrykiDokumentu")){
					 tytulSprawyLabel = new Cell(new Paragraph("Opis/dotyczy", fontNormal));
				} else {
					 tytulSprawyLabel = new Cell(new Paragraph("Tytu� sprawy", fontNormal));
				}
				Cell tytulSprawy= new Cell(new Paragraph(document.getTitle(), fontNormal));
				
				topTable.addCell(oznaczenieSprawyLabel);
				topTable.addCell(oznaczenieSprawy);
				topTable.addCell(tytulSprawyLabel);
				topTable.addCell(tytulSprawy);
				
				pdfDoc.add(topTable);
				
				// Tabela z wlasciwymi danymi
				Table contentTable= new Table(5);
				contentTable.setCellsFitPage(true);
				contentTable.setWidth(100);
				contentTable.setPadding(3);
				contentTable.setBorder(1);
				contentTable.setWidths( new int[]{10,15,20,35,20});
				
				// Naglowki
				contentTable.addCell( new Cell(new Paragraph("Lp.", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Data podj�tej czynno�ci", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Oznaczenie osoby podejmuj�cej dan� czynno��", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Okre�lenie podejmowanej czynno�ci", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Wskazanie identyfikatora w aktach sprawy, do kt�rego odnosi si� dana czynno��", fontNormal)));
				
				Integer lp_index = 1;
				// Tresc tabeli
				for( HistoriaWpis wpis : historia)
				{
					Cell lp = new Cell(new Paragraph(lp_index.toString(), fontNormal));
					Cell data = new Cell(new Paragraph(wpis.data.toLocaleString(), fontNormal));
                    Cell osoba;

                    if (!AvailabilityManager.isAvailable("pg.externalname.metrics")) {
                        osoba = new Cell(new Paragraph(wpis.osoba, fontNormal));
                    } else {
                        String[] splited = DSUser.getUserByFirstnameLastnameBySplit(wpis.osoba, " ");
                        if(splited != null) {

                            DSDivision[] dzialy = DSUser.findByFirstnameLastname(splited[0], splited[1]).getDivisionsWithoutGroup();
                            String dzial = " ";
                            for(int i = 0; i < dzialy.length; i++) {
                                dzial = dzial + dzialy[i].getName() + " ";
                            }

                            osoba = new Cell(new Paragraph(wpis.osoba + " (" + DSUser.findByFirstnameLastname(splited[0], splited[1]).getExternalName() + ") " + dzial, fontNormal));
                        } else {
                            osoba = new Cell(new Paragraph(wpis.osoba, fontNormal));
                        }
                    }
					Cell czynnosc = new Cell(new Paragraph(wpis.czynnosc, fontNormal));
					Cell identyfikator = new Cell(new Paragraph("Id Dokumentu \n" +wpis.identyfikator, fontNormal));
					
					contentTable.addCell(lp);
					contentTable.addCell(data);
					contentTable.addCell(osoba);
					contentTable.addCell(czynnosc);
					contentTable.addCell(identyfikator);
					
					++lp_index;
				}
			
				pdfDoc.add(contentTable);
				
				
				pdfDoc.close();
			} catch (Exception e) {
				throw new EdmException(e);
			}

			return pdfFile;
        	
		}

		private List<HistoriaWpis> getData(OfficeDocument doc) throws EdmException
		{
			List<HistoriaWpis> historia = new ArrayList<DocumentMetrics.HistoriaWpis>();
        	
        	{
        		List<AssignmentHistoryEntry> historiaDekretacji = doc.getAssignmentHistory();
        		List<Audit> historiaPisma = doc.getWorkHistory();
        		
        		String docId = doc.getId().toString();
        		
        		for( AssignmentHistoryEntry ashistory : historiaDekretacji)
        		{
        			AssignmentHistoryTabAction.AssignmentHistoryBean ashistoryBean = new AssignmentHistoryTabAction.AssignmentHistoryBean(ashistory);
        			historia.add(new HistoriaWpis(
        				ashistory.getCtime(),	// data
        				DSUser.safeToFirstnameLastname(ashistory.getSourceUser()),	// osoba
        				ashistoryBean.getContent(),	// czynnosc
        				docId,	// identyfikator
        				ashistory.getClass().toString()   // nazwa klasy
        				));
        		}
        		
        		for( Audit audit : historiaPisma)
        			historia.add(new HistoriaWpis(
        				audit.getCtime(),	// data
        				DSUser.safeToFirstnameLastname(audit.getUsername()),	// osoba
        				audit.getDescription(),	// czynnosc
        				docId,	// identyfikator
        				audit.getClass().toString()   // nazwa klasy
        				));
    			// usuwanie duplikat�w
    			for (int a = 0; a < historia.size(); a++) {
    				for (int b = a + 1; b < historia.size(); b++) {
    					String h1 = historia.get(a).data.toString().substring(0, 20);
    					String h2 = historia.get(b).data.toString().substring(0, 20);
    					if (h1.equals(h2) && historia.get(a).czynnosc.equals(historia.get(b).czynnosc)
    							&& historia.get(a).osoba.equals(historia.get(b).osoba)
    							&& historia.get(a).identyfikator.equals(historia.get(b).identyfikator)){
    						if (h1.equals(h2) && historia.get(a).czynnosc.equals(historia.get(b).czynnosc)
        							&& historia.get(a).osoba.equals(historia.get(b).osoba)
        							&& historia.get(a).identyfikator.equals(historia.get(b).identyfikator))
        						
        						historia.remove(historia.get(b));
    					}
    				}
    			}
        	}
        	return historia;
		}
    }
    
    

    public class CompileAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                CompileAttachmentsManager.compile(document);

                DSApi.context().commit();

                addActionMessage(sm.getString("SkompilowanoWszystkiePlikiSprawyDoZalacznika"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                try {DSApi.context().rollback(); } catch (EdmException f) {};
            }
        }
    }

    public class hotIcr implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        	Boolean blad = false;

        	try
        	{
        		DSApi.context().begin();
        		Document doc = Document.find(getDocumentId());
        		doc.getDocumentKind().initialize();


        		FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
        		fm.initialize();
        		Map<String, Object> values = fm.getFieldValues();
    			//doc.getDocumentKind().setOnly(doc.getId(), values);
    			doc.getDocumentKind().setWithHistory(doc.getId(),values,false);

        		Map<String, String> properties = doc.getDocumentKind().getProperties();
	        	for(FlagBean flb : Flags.listFlags(false))
	        	{
	        		if(flb.getC().equalsIgnoreCase("ICR"))
	        		{
	        			doc.getFlags().setFl(flb.getId(), true);
	        		}
	        	}



	        	List<Attachment> ats = doc.getAttachments();
	            Collections.sort(ats, new Attachment.AttacgnebtComparatorByDate());
	            Attachment lastAttachment = null;
	            for(Attachment at : ats)
	            {
	            	if("Skan".equalsIgnoreCase(at.getTitle()))
	            	{
	            		lastAttachment = at;
	            	}
	            }

	            InputStream biss = lastAttachment.getMostRecentRevision().getBinaryStream();
				File tempFile = FileUtils.fileFromBlobInputStream(biss);
				File dir = new File(properties.get("hotIcrFolder"));
				if(!dir.exists())
					dir.mkdir();

				File retFile = new File(dir, doc.getId()+".tif");
				FileUtils.copyFileToFile(tempFile, retFile);
        	}
        	catch (EdmException e)
        	{
        		blad = true;
        		event.skip(EV_MANUAL_FINISH);
        		addActionError("Blad w czasie zapisywania pliku: "+e.getMessage());
        		DSApi.context().setRollbackOnly();
        		log.error("", e);
			}
        	catch (Exception e)
        	{
        		blad = true;
        		event.skip(EV_MANUAL_FINISH);
        		addActionError("Blad w czasie zapisywania pliku.");
        		DSApi.context().setRollbackOnly();
        		log.error("", e);
			}
        	finally
        	{
        		try
        		{
        			DSApi.context().commit();
        		}
        		catch (Exception ex)
        		{
        			log.error("",ex);
        		}
        	}
        	if(!blad)
        		event.skip(EV_FILL);
        }
    }

    public class GiveAcceptance implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();
                
                Document document = Document.find(getDocumentId());
                DocumentLogic documentLogic = document.getDocumentKind().logic();

                if (acceptanceCn != null)
                {
                	AcceptancesManager.giveAcceptance(acceptanceCn, getDocumentId());
                	documentLogic.onGiveAcceptance(document, acceptanceCn);
                }
                else
                    AcceptancesManager.giveCentrumAcceptance(centrumAcceptanceCn, getDocumentId());
               
                
                if(documentLogic instanceof Acceptable)
				{
                	try
                	{
                		Acceptable logic = (Acceptable) documentLogic;
                		logic.onAccept(acceptanceCn, getDocumentId());
                	}
                	catch (EdmException e) 
					{
                		if(AvailabilityManager.isAvailable("skipMerytorycznaCheck", document.getDocumentKind().getCn()))
                		{
                			//addActionError(e.getMessage());
                		}
                		else
                		{
                			throw e;
                		}						
					}
				}
                
                DSApi.context().commit();
                addActionMessage(sm.getString("WykonanoAkceptacjeDokumentu"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public class WithdrawAcceptance implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();
                
                Document doc = Document.find(getDocumentId());
				DocumentLogic logic = doc.getDocumentKind().logic();
				
				logic.canWithdrawAcceptance(doc, acceptanceCn);

                if (objectId != null){
                    AcceptancesManager.withdrawCentrumAcceptanceAndAssign(
                    		Document.find(getDocumentId()), objectId, getActivity());
				} else {
                    AcceptancesManager.withdrawAcceptanceAndAssign(
                    		Document.find(getDocumentId()), acceptanceCn, getActivity());
				}
                
                doc.getDocumentKind().logic().onWithdrawAcceptance(doc, acceptanceCn);

                if(logic instanceof Acceptable)
				{
					Acceptable acceptable = (Acceptable) logic;
					acceptable.onWithdraw(acceptanceCn, getDocumentId());
				}
                
                DSApi.context().commit();
                addActionMessage(sm.getString("WycofanoAkceptacjeDokumentu"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public class GenerateDocumentView implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File pdf = null;
            try
            {
                Document document = Document.find(getDocumentId());
                pdf = document.getDocumentKind().logic().generateDocumentView(document);

                if (pdf != null && pdf.exists())
                {
                    if (log.isDebugEnabled())
                        log.debug("rozmiar pliku (obrazu dokumentu) "+pdf+": "+pdf.length());
                    try
                    {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdf),"application/pdf", (int) pdf.length(),"Content-Disposition: inline; filename=\"obraz_dokumentu.pdf\"");
                    }
                    catch (IOException e)
                    {
                        log.error("", e);
                    }
                    finally
                    {
                        pdf.delete();
                    }
                }
                event.setResult(NONE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    public class GenerateCsvDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File csv = null;
            try
            {
                Document document = Document.find(getDocumentId());
                csv = document.getDocumentKind().logic().generateCsvDocumentView(document);

                if (csv != null && csv.exists())
                {
                    if (log.isDebugEnabled())
                        log.debug("rozmiar pliku (obrazu dokumentu) "+csv+": "+csv.length());
                    try
                    {
                        ServletUtils.streamFile(ServletActionContext.getResponse(), csv, "text/plain", "Content-Disposition: attachment; filename=\"invoicePTE.csv\"");
                    }
                    catch (IOException e)
                    {
                        log.error("", e);
                    }
                    finally
                    {
                    	csv.delete();
                    }
                }
                event.setResult(NONE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     *
     * @author wkutyla
     * FIXME - zrobic na evencie Dockinda
     */
    @Deprecated
    public class SendToEva implements ActionListener
    {

    	public void actionPerformed(ActionEvent event){
    		try
    		{
    			Map<String,Object> statusMap = new HashMap<String,Object>();
                statusMap.put(RockwellLogic.EVA_STATUS_FIELD_CN,1);
                statusMap.put(RockwellLogic.SUBMITTED_BY_CN,DSApi.context().getPrincipalName());
                statusMap.put(RockwellLogic.SUBMIT_DATE_FIELD_CN,new Date());
                statusMap.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN,RockwellLogic.STAT_PROCESSED_EVA);

                Document doc = Document.find(getDocumentId());
                if(doc != null)
                {
                	doc.getDocumentKind().setWithHistory(doc.getId(),statusMap,false);

                	DSApi.context().begin();

                    OfficeDocument document = (OfficeDocument)doc;
                    WorkflowFactory.getInstance().manualFinish(getActivity(), true);
                   	DSApi.context().commit();

                  //powiadamiany EVA
                   	pl.compan.docusafe.parametrization.ra.EvaNotifier.notifyEva();

                    event.setResult(getTaskListResult(document.getType()));
                }

    		}
    		catch(Exception e)
    		{
    			log.error("", e);
    			addActionError(e.getMessage());
    		}
    	}

    }
    public class removeLabels implements ActionListener
    {

    	public void actionPerformed(ActionEvent event)
    	{
    		if(labelsIds==null || labelsIds.length==0){
    			addActionError(sm.getString("NieWybranoZadnychEtykiet"));
    			return;
    		}
    		try
    		{
    			DSApi.context().begin();
	    		for(int i = 0; i<labelsIds.length; i++)
	    		{
	    			LabelsManager.removeLabel(getDocumentId(), labelsIds[i],DSApi.context().getPrincipalName());
	    		}
    			DSApi.context().commit();
    		}
    		catch(EdmException e)
    		{
    			addActionError(sm.getString("NieUdaloSieUsunacEtykiety"));
    			DSApi.context().setRollbackOnly();
    		}
    	}
    }

    public final boolean isEnableCollectiveAssignments()
    {
        return enableCollectiveAssignments;
    }

    public final Map<String, String> getWfProcesses()
    {
        return wfProcesses;
    }

    public final Map<String, String> getSubstituted()
    {
        return substituted;
    }

    public final Map<String, String> getAssignmentsMap()
    {
        return assignmentsMap;
    }

    public final Collection<Map> getAttachments()
    {
        return attachments;
    }

    public final DocumentSignature getRecentSignature()
    {
        return recentSignature;
    }

    public final Map getRecentRemark()
    {
        return recentRemark;
    }

    public final boolean isCanReopenWf()
    {
        return canReopenWf;
    }

    public final String getCreatingUser()
    {
        return creatingUser;
    }

    public final String getAssignedDivision()
    {
        return assignedDivision;
    }

    public final String getCaseDocumentId()
    {
        return caseDocumentId;
    }

    public FinishOrAssignState getFinishOrAssignState()
    {
        return finishOrAssignState;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public boolean isCanSign()
    {
    	return canSign;
    }

    public String getDaaSummary()
    {
        return daaSummary;
    }

    public boolean isDaaUnassigned()
    {
        return daaUnassigned;
    }

    public Map<Long, String> getJbpmProcesses()
    {
        return jbpmProcesses;
    }

    public Long getJbpmProcessId()
    {
        return jbpmProcessId;
    }

    public String getJbpmLink()
    {
        return jbpmLink;
    }

    public String getInitJbpmLink()
    {
        return initJbpmLink;
    }

    public boolean isCanInitJbpmProcess()
    {
        return canInitJbpmProcess;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }

    public boolean isShowNotInBoxMessage()
    {
        return showNotInBoxMessage;
    }

    public List<JbpmManager.ProcessSummary> getJbpmSummary()
    {
        return jbpmSummary;
    }

    public boolean isAcceptancesEnabled()
    {
        return acceptancesEnabled;
    }

    public boolean isCanGenerateDocumentView()
    {
        return canGenerateDocumentView;
    }
    
    public boolean isCanGenerateCsvDocument() {
    	return canGenerateCsvDocument;
    }

    public FieldsManager getFm()
    {
        return fm;
    }

    public void setAcceptanceCn(String acceptanceCn)
    {
        this.acceptanceCn = acceptanceCn;
    }

    public Map<Integer, String> getCentrumAcceptanceCn()
    {
        return centrumAcceptanceCn;
    }

    public void setCentrumAcceptanceCn(Map<Integer, String> centrumAcceptanceCn)
    {
        this.centrumAcceptanceCn = centrumAcceptanceCn;
    }

    public void setObjectId(Long objectId)
    {
        this.objectId = objectId;
    }

    public CompilationStatus getCompilationStatus()
    {
        return compilationStatus;
    }

    public boolean isMultiRealizationAssignment()
    {
        return multiRealizationAssignment;
    }

    public List getObjectives()
    {
        return objectives;
    }

    public String getObjectiveSel()
    {
        return objectiveSel;
    }

    public boolean isShowObjectives()
    {
        return showObjectives;
    }

    public String getDockindAction()
    {
        return "summary";
    }

	public String getEvaMessage() {
		return evaMessage;
	}

	public void setEvaMessage(String evaMessage) {
		this.evaMessage = evaMessage;
	}

	public Boolean getOpenViewer() {
		return openViewer;
	}

	public void setOpenViewer(Boolean openViewer) {
		this.openViewer = openViewer;
	}

	public List<Label> getModifiableLabels() {
		return modifiableLabels;
	}

	public List<Label> getNonModifiableLabels() {
		return nonModifiableLabels;
	}

	public void setLabelsIds(Long[] labelsIds) {
		this.labelsIds = labelsIds;
	}

	public boolean isDoWiadomosci() {
		return doWiadomosci;
	}

	public void setDoWiadomosci(boolean doWiadomosci) {
		this.doWiadomosci = doWiadomosci;
	}

	public void setCanAssignMe(boolean canAssignMe) {
		this.canAssignMe = canAssignMe;
	}

	public boolean isCanAssignMe() {
		return canAssignMe;
	}

	public void setCanFinish(boolean canFinish) {
		this.canFinish = canFinish;
	}

	public boolean isCanFinish() {
		return canFinish;
	}

	public void setCanAccept(boolean canAccept)
	{
		this.canAccept = canAccept;
	}

	public boolean isCanAccept()
	{
		return canAccept;
	}

    public Map<String, String> getSubstitutedNames()
    {
        return substitutedNames;
    }

	public Long getMostRecentRevisionId() {
		return mostRecentRevisionId;
	}

	public void setMostRecentRevisionId(Long mostRecentRevisionId) {
		this.mostRecentRevisionId = mostRecentRevisionId;
	}
	
	public Long getBoxId() {
		return this.boxId;
	}

	public Long getCurrentBoxId() {
		return this.currentBoxId;
	}

	public String getCurrentBoxNumber() {
		return this.currentBoxNumber;
	}

	public boolean isBoxNumberReadonly() {
		return this.boxNumberReadonly;
	}

	public boolean isNeedsNotBox() {
		return this.needsNotBox;
	}

	public boolean isPudloModify() {
		return this.pudloModify;
	}

	public void setBoxId(Long boxId) {
		this.boxId = boxId;		
	}

	public void setBoxNumber(String boxNumber) 
	{
		this.boxNumber = boxNumber;		
	}

	public void setBoxNumberReadonly(boolean boxNumberReadonly) 
	{
		this.boxNumberReadonly = boxNumberReadonly;		
	}

	public void setCurrentBoxId(Long boxId) 
	{
		this.currentBoxId = boxId;		
	}

	public void setCurrentBoxNumber(String currentBoxNumber) 
	{
		this.currentBoxNumber = currentBoxNumber;		
	}

	public void setNeedsNotBox(Boolean isneed) {
		this.needsNotBox = isneed;		
	}

	public void setPudloModify(boolean pudloModify) {
		this.pudloModify = pudloModify;		
	}

    public boolean isCanAddToWatched() {
        return canAddToWatched;
    }

    public void setCanAddToWatched(boolean canAddToWatched) {
        this.canAddToWatched = canAddToWatched;
    }
    
    /**
	 * @return the documentForm
	 */
	public String getDocumentForm()
	{
		return documentForm;
	}
	

    public boolean isCanUpdate()
	{
		return canUpdate;
	}

	public void setCanUpdate(boolean canUpdate)
	{
		this.canUpdate = canUpdate;
	}

	/* DLA FAKTURY PTE */
	/**
	 * Okre�la czy przysic 'dekretuj...' jest w��czony
	 */
	public Boolean getSendDecretation()
	{
		DocumentLogic logic = fm.getDocumentKind().logic();
		if (logic instanceof Acceptable)
		{
			try 
			{
				boolean ret = ((Acceptable) logic).canSendDecretation(fm);
				return ret;
			} catch (EdmException e) 
			{
				log.error(e.getMessage(), e);
			}
		}
		return true;
	}
	/* END / DLA FAKTURY PTE */

	
	public boolean isGraphML()
	{
		return graphML;
	}

	
	public void setGraphML(boolean graphML)
	{
		this.graphML = graphML;
	}

	
	public boolean isGraphDot()
	{
		return graphDot;
	}

	
	public void setGraphDot(boolean graphDot)
	{
		this.graphDot = graphDot;
	}

    public String getAbstrakt() {
        return abstrakt;
    }

    public void setAbstrakt(String abstrakt) {
        this.abstrakt = abstrakt;
    }

}
