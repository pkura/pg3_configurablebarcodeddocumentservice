package pl.compan.docusafe.web.office.common;

import java.util.List;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface DocumentTabAction {

	String getActivity();

	Long getDocumentId();

	String getProcess();

	List getTabs();
}
