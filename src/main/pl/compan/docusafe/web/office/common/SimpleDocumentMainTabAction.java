package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.IntegrityVerificationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.ActionContext;
import pl.compan.docusafe.core.cfg.ActionContext.Action;
import pl.compan.docusafe.core.cfg.part.DSPartsConfig;
import pl.compan.docusafe.core.cfg.part.DSPartsManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public abstract class SimpleDocumentMainTabAction extends EventActionSupport implements DocumentTabAction
{
	protected StringManager sm = GlobalPreferences.loadPropertiesFile(SimpleDocumentMainTabAction.class.getPackage().getName(),null);
	protected static Logger log = LoggerFactory.getLogger(SimpleDocumentMainTabAction.class);

    private static InOfficeDocumentKind  defaultKind;

    protected boolean personAnonymous;
    protected String personTitle;
    protected String personFirstname;
    protected String personLastname;
    protected String personOrganization;
    protected String personStreetaddress;
    protected String personZip;
    protected String personLocation;
    protected String personCountry;
    protected String personEmail;
    protected String personFax;
    protected String personPesel;
    protected String personNip;
    protected String personRegon;
    protected String summary;
    protected Boolean original; 
    protected boolean currentDayWarning;
	protected Long boxId;
    protected String boxName;
    protected String documentDate;
    protected String postalRegNumber;
    protected Integer deliveryId;
    protected String fastAssignmentSelect;
    protected List<DSDivision> fastAssignmentDivision;
    protected List<DSUser> fastAssignmentUser;
    protected String fastAssignmentSelectUser;
	protected String fastAssignmentSelectDivision;
	/**Jesli nie ma fastAssignmentSelectDivision w selekcie to mamy zapasowa zmienna przekazujaca*/
	protected String reserveFastAssignmentSelectDivision;
	protected String userDivision;
	protected String userLastname;
	protected String userFirstname;
	protected boolean createPerson;
	protected OfficeDocument doc;

	private Long documentId = null;

    private String dockindCn;
    private List<DocumentKind> documentKinds;
    /** MAPA cn -> key */
    private Map<String, Object> dockindKeys;

	private Action actionType;
	private String activityId;
	private List tabs;

    protected Date entryDate = null;
    protected Long journalId = null;
	protected OfficeDocument document;
	
	protected Map<String,String> loadFiles;
    protected String[] returnLoadFiles;
	protected List<FormFile> multiFiles = new ArrayList<FormFile>();
	protected String filestoken;
	private Long revisionId;
	private Boolean openViewer;

    protected static final String EV_FILL_CREATE = "fillCreate";
    protected static final String EV_CREATE = "create";


    private void initDefaultKind()
    {
    	try
    	{
    		setDefaultKind(InOfficeDocumentKind.list().get(0));
    	}
    	catch (Exception e) 
    	{
			log.error("Nie udalo si� zainicjowa� domy�lnego InOfficeDocumentKind ");
		}
    }

//    public abstract String getDocumentType();
    public abstract OfficeDocument getOfficeDocument();
    public abstract void setBeforeCreate(OfficeDocument document) throws EdmException;
    public abstract void setAfterCreate(OfficeDocument document) throws EdmException;
    public abstract void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException;
    public abstract String getBaseUrl();
	public abstract DocumentType getDocumentType();
    public abstract void loadDelivery() throws EdmException;
	public abstract void loadTabs() throws EdmException;

	public abstract void setSender(Person person) throws EdmException;
	public abstract boolean isStartProcess() throws EdmException;
	public abstract void setReceiver(Person person) throws EdmException;
	public abstract void setDocument(OfficeDocument document) throws EdmException;

	public abstract void updateSender(Person sender)throws EdmException;
	public abstract void updateReceiver(Person receiver)throws EdmException;
    public abstract void loadFillForm() throws EdmException;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(CloseHibernateSession.INSTANCE).
            append(OpenHibernateSession.INSTANCE).
            append( new BindToJournal()).
            appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doOldUser").
		 	append(OpenHibernateSession.INSTANCE).
			append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(CloseHibernateSession.INSTANCE).
            append(OpenHibernateSession.INSTANCE).
			append( new BindToJournal()).
			append(new RedoNewUser()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doNewUser").
			append(OpenHibernateSession.INSTANCE).
			append(new ValidateCreate()).
	        append(EV_CREATE, new Create()).
            append(CloseHibernateSession.INSTANCE).
            append(OpenHibernateSession.INSTANCE).
	        append( new BindToJournal()).
			append(new RedoOldUser()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
	        append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
	public FormFile getMultiFiles() 
	{
		if(this.multiFiles != null && this.multiFiles.size() > 0 )
			return this.multiFiles.get(0);
		else
			return null;
	}
    
	public void setMultiFiles(FormFile file) {
		this.multiFiles.add(file);
	}


	private class Update extends TransactionalActionListener{

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			document = OfficeDocument.find(documentId);

			updateSender(document.getSender());
			updateReceiver(document.getRecipients().get(0));

			if (document instanceof OutOfficeDocument) {
				((OutOfficeDocument) document).setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
			} else if (document instanceof InOfficeDocument) {
				((InOfficeDocument) document).setPostalRegNumber(TextUtils.trimmedStringOrNull(postalRegNumber));
			}

			if (documentDate != null) {
				if (document instanceof OutOfficeDocument) {
					((OutOfficeDocument) document).setDocumentDate(DateUtils.parseJsDate(documentDate));
				} else if (document instanceof InOfficeDocument) {
					((InOfficeDocument) document).setDocumentDate(DateUtils.parseJsDate(documentDate));
				}
			} else {
				if (document instanceof OutOfficeDocument) {
					((OutOfficeDocument) document).setDocumentDate(null);
				} else if (document instanceof InOfficeDocument) {
					((InOfficeDocument) document).setDocumentDate(null);
				}
			}
			
			if(deliveryId != null && document instanceof InOfficeDocument) {
				InOfficeDocumentDelivery delivery = InOfficeDocumentDelivery.find(deliveryId);
				((InOfficeDocument) document).setDelivery(delivery);
			}
			
			if(document.getDocumentKind() != null)
			{
				document.getDocumentKind() .logic().archiveActions(document,0);
			}
			document.setSummary(summary != null ? summary : "");
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	}


	private class RedoNewUser extends LoggedActionListener
	{
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			userDivision = null;
			userFirstname = null;
			userLastname = null;
			fastAssignmentSelectDivision = null;
			fastAssignmentSelectUser = null;
			reserveFastAssignmentSelectDivision = null;
			personTitle = null;
			personFirstname = null;
			personLastname = null;
			personOrganization = null;
			personStreetaddress = null;
			personZip = null;
			personLocation = null;
			personCountry = null;
			personEmail = null;
			personFax = null;
			event.setResult("redo");
		}

		public Logger getLogger()
		{
			return log;
		}
	}

	private class RedoOldUser extends LoggedActionListener
	{
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			personTitle = null;
			personFirstname = null;
			personLastname = null;
			personOrganization = null;
			personStreetaddress = null;
			personZip = null;
			personLocation = null;
			personCountry = null;
			personEmail = null;
			personFax = null;
			event.setResult("redo");
		}

		public Logger getLogger()
		{
			return log;
		}
	}

	private void initDocumentAttributes() throws EdmException{
		if(document == null){
			document = OfficeDocument.find(documentId);
		}
		if (document instanceof OutOfficeDocument) {
			postalRegNumber = ((OutOfficeDocument) document).getPostalRegNumber();
			OutOfficeDocumentDelivery delivery = ((OutOfficeDocument) document).getDelivery();
			if(delivery != null)
				setDeliveryId(delivery.getId());
			if(((OutOfficeDocument) document).getDocumentDate() != null)
				documentDate = DateUtils.formatJsDate(((OutOfficeDocument) document).getDocumentDate());
		} else if (document instanceof InOfficeDocument) {
			postalRegNumber = ((InOfficeDocument) document).getPostalRegNumber();
			InOfficeDocumentDelivery delivery = ((InOfficeDocument) document).getDelivery();
			if(delivery != null)
				setDeliveryId(delivery.getId());
			if(((InOfficeDocument) document).getDocumentDate() != null)
				documentDate = DateUtils.formatJsDate(((InOfficeDocument) document).getDocumentDate());
		}
		setSender(document.getSender());
		if(!document.getRecipients().isEmpty())
		{		
			setReceiver(document.getRecipients().get(0));
		}
		setSummary(document.getSummary());
		
		if(document.getAttachments() != null && document.getAttachments().size() > 0)
		{
			revisionId = document.getAttachments().get(document.getAttachments().size()-1).getMostRecentRevision().getId();
		}
		
	}

    private class FillForm extends LoggedActionListener {
    
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			if(documentId != null){
				initDocumentAttributes();
				actionType = Action.DOCUMENT_SUMMARY;
				loadTabs();
				
			} else {
				actionType = Action.NEW_DOCUMENT;
				loadFillForm();
			}
            documentKinds = DocumentKind.listForEdit();
            if(dockindCn == null){
                dockindCn = DocumentKind.getDefaultKind();
            }

			loadDelivery();
            currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
            fastAssignmentUser = Arrays.asList(UserFactory.getInstance().searchUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true, null, null, false, false,false).results()); 
            	//DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
            initFastAssignmentDivision(); 
            
            if(AvailabilityManager.isAvailable("p4HandWrittenSignature")) {
            	if(document != null) {
            		List<Attachment> atts = document.getAttachments();
            		if(atts.size() > 0) {
            			try {
            				atts.get(0).getMostRecentRevision().verifyIntegrity();
            			} catch(IntegrityVerificationException e) {
            				addActionError("Nieprawid�owa suma kontrolna podpisu");
            			}
            		}
            	}
            }
		}

		@Override
		public Logger getLogger() {
			return log;
		}
    }

    private class ValidateCreate extends LoggedActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {

        	if(fastAssignmentSelectDivision == null )
        		fastAssignmentSelectDivision = reserveFastAssignmentSelectDivision;
            if( documentDate != null && DateUtils.parseJsDate(documentDate) == null)
            {
            	addActionError(sm.getString("NiepoprawnaDataPisma"));
            }

            if(AvailabilityManager.isAvailable("dwr.fields")){
                log.info("loading dwr fields");
                dockindKeys = RequestLoader.loadKeysFromWebWorkRequest();
            }
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class Create implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
		{
			if(hasActionErrors())
			{
				saveFile();
				return;
			}
           
            try
            {
            	DSApi.context().begin();
	            doc = getOfficeDocument();
	            setBeforeCreate(doc);
	            
	            doc.setCurrentAssignmentGuid(fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision :DSDivision.ROOT_GUID);
	            doc.setDivisionGuid(fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision :DSDivision.ROOT_GUID);
	            doc.setCurrentAssignmentAccepted(Boolean.FALSE);
                if (!DSApi.context().hasPermission(DSPermission.PISMA_KO_PRZYJECIE))
                    throw new EdmException(sm.getString("BrakUprawnieniaDoPrzyjmowaniaPismWKO"));
                
                DocumentKind documentKind = DocumentKind.findByCn(Docusafe.getAdditionProperty("simple.document.default.dockind"));
                doc.setSummary(summary != null ? summary : documentKind.getName());
                doc.setAssignedDivision(DSDivision.ROOT_GUID);
                doc.setDocumentKind(documentKind);
                
//                if(deliveryId != null && InOfficeDocumentDelivery.find(deliveryId) != null && InOfficeDocumentDelivery.find(deliveryId).getName().equalsIgnoreCase("poczta"))
//                {
//                	doc.setPostalRegNumber(cutPostalRegNumber(postalRegNumber));
//                }
//                else
//                {
                	doc.setPostalRegNumber(postalRegNumber);
//                }
                
                doc.create();	
                setAfterCreate(doc);                
                Long newDocumentId = doc.getId();
                Map<String,Object> values = new HashMap<String, Object>();
                documentKind.set(newDocumentId, values);
                log.trace("1.!!!!doc.id: {},  doc.getSummar(): {}", doc.getId(), doc.getSummary());
                doc.getDocumentKind().logic().archiveActions(doc, 0);
                DSApi.context().session().save(doc); 
                log.trace("2.!!!!doc.id: {},  doc.getSummar(): {}", doc.getId(), doc.getSummary());
                log.trace("!!!!doc.id: {},  fastAssignmentSelectUser: {}, fastAssignmentSelectDivision: {}", doc.getId(), fastAssignmentSelectUser, fastAssignmentSelectDivision);
                if((fastAssignmentSelectUser != null || fastAssignmentSelectDivision != null) && isStartProcess())
                {
                	WorkflowFactory.createNewProcess(doc, true, sm.getString("task.list.PismoPrzyjete"),
                			DSApi.context().getPrincipalName(), (fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision : DSDivision.ROOT_GUID), fastAssignmentSelectUser);
                	DSApi.context().session().flush();
                	DSApi.context().commit();
                	DSApi.context().begin();
                	doc.getDocumentKind().logic().onStartProcess(doc, event);
                	log.trace("!!!!p4.logic: {}", doc.getDocumentKind().logic().getClass());
                }
                addFiles(doc);
                DSApi.context().commit();
                addActionMessage("Przyj�to pismo o numerze id "+doc.getId());
                postalRegNumber = null;
                event.setResult("task-list");
            }
            catch (EdmException e)
            {
            	saveFile();
            	addActionError(e.getMessage());
				log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
	private class BindToJournal extends LoggedActionListener
	{
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			try
			{
				DSApi.context().begin();
				doc = OfficeDocument.find(doc.getId());
	            bindToJournal(doc,event);
	            TaskSnapshot.updateByDocument(doc);
	            DSApi.context().commit();
	        }
	        catch (EdmException e)
	        {
	        	addActionError(e.getMessage());
				log.error(e.getMessage(), e);
	            DSApi.context().setRollbackOnly();
	        }
		}

		public Logger getLogger()
		{
			return log;
		}
	}
    
    /**Metoda zapoisuje pobrane zalaczniki to katalogu tymczasowego i dodaje wpis do loadFiles
     * Jest to robione na wypadek wystapienia bledu i powrotu jeszcze raz na formularz dodani dokumenu.
     * Zalaczniki sa pamietane i nie trzeba ich jeszcze raz przesylac */
    protected void saveFile()
    {
    	try
    	{
    		if(filestoken == null || filestoken.length() < 1)
    			filestoken = ServletActionContext.getRequest().getRequestedSessionId()+ new Date().getTime();
    		if(loadFiles == null)
    			loadFiles = new HashMap<String, String>();
	    	if(multiFiles != null && multiFiles.size() > 0)
	        {
	        	for (FormFile formFile : multiFiles)
				{   		
					File tmp = new File(Docusafe.getTemp(),filestoken+"_"+formFile.getFile().getName());
					FileOutputStream os = new FileOutputStream(tmp);
					FileInputStream stream = new FileInputStream(formFile.getFile());
					byte[] buf = new byte[2048];
					int count;
					while ((count = stream.read(buf)) > 0)
					{
					    os.write(buf, 0, count);
					}
					os.close();
					stream.close();
					loadFiles.put(tmp.getAbsolutePath(), formFile.getFile().getName());
				}
	        }
	    	if(returnLoadFiles != null && returnLoadFiles.length > 0)
	    	{
	    		for (String path : returnLoadFiles)
				{
	    			String[] tab = path.split("\\\\");
	    			String fileName = tab[tab.length -1].replace(filestoken+"_", "");
	    			loadFiles.put(path,fileName);
				}
	    	}
    	}
    	catch (Exception e) 
    	{
    		LOG.error("",e);
			addActionError("B��d zapisu plik�w "+e.getMessage());
		}
    }

	public void initFastAssignmentDivision() throws EdmException 
    {
    	if(StringUtils.isNotEmpty(Docusafe.getAdditionProperty("in.simple.FastAssignmentDivision")))
    	{
    		fastAssignmentDivision = new ArrayList<DSDivision>();
    		String[] divs = Docusafe.getAdditionProperty("in.simple.FastAssignmentDivision").split(";");
    		for (int i = 0; i < divs.length; i++) 
    		{
    			fastAssignmentDivision.add(DSDivision.find(divs[i]));
			}
    	}
    	else
    		setFastAssignmentDivision(Arrays.asList(DSDivision.getOnlyDivisionsAndPositions(false)));
		
	}

	/**
     * Metoda dodaj�ca za��czniki do dokumentu, pobiera zalaczniki z listy multiFiles zawierajacej aktualnie wczytane zalaczniki
     * oraz z tablicy returnLoadFiles zawierajace zalaczniki wczytane przed przeladowanie formularza (jesli takie nastapilo)
     * @param doc
     * @throws AccessDeniedException
     * @throws EdmException
     */
    protected void addFiles(OfficeDocument doc) throws AccessDeniedException, EdmException
    {
    	doc.setPermissionsOn(false);
    	if(multiFiles != null && multiFiles.size() > 0)
        {
        	for (FormFile formFile : multiFiles)
			{
        		 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			}            	
        }
        if( returnLoadFiles != null && returnLoadFiles.length > 0 )
        {
        	for (String fileKey : returnLoadFiles)
			{
        		String path = fileKey.replace("\\","/");
				File f = new File(path);
				 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(f).setOriginalFilename(f.getName().replace(filestoken+"_", ""));
			}
        }
    }
    
    private String cutPostalRegNumber(String prn)
    {
    	if(prn != null && prn.length() > 9)
    	{
    		return prn.substring(prn.length() - 10, prn.length() - 1);
    	}
    	return prn;
    }

	protected void setPersonValue(Person person)
	{
		person.setTitle(TextUtils.trimmedStringOrNull(personTitle, 30));
		person.setFirstname(TextUtils.trimmedStringOrNull(personFirstname, 50));
		person.setLastname(TextUtils.trimmedStringOrNull(personLastname, 50));
		person.setOrganization(TextUtils.trimmedStringOrNull(personOrganization, 50));
		person.setStreet(TextUtils.trimmedStringOrNull(TextUtils.replaceBackslash(personStreetaddress), 50));
		person.setZip(TextUtils.trimmedStringOrNull(personZip, 14));
		person.setLocation(TextUtils.trimmedStringOrNull(personLocation, 50));
		person.setCountry(TextUtils.trimmedStringOrNull(personCountry, 3));
		person.setEmail(TextUtils.trimmedStringOrNull(personEmail, 50));
		person.setFax(TextUtils.trimmedStringOrNull(personFax, 30));
	}

	protected void setPerson(Person person)
	{
		personAnonymous = person.isAnonymous();
		personTitle = person.getTitle();
		personFirstname = person.getFirstname();
		personLastname = person.getLastname();
		personOrganization = person.getOrganization();
		personStreetaddress = person.getStreet();
		personZip = person.getZip();
		personLocation = person.getLocation();
		personCountry = person.getCountry();
		personEmail = person.getEmail();
		personFax = person.getFax();
	}

	protected void setUser(Person person)
	{
		userFirstname = person.getFirstname();
		userLastname = person.getLastname();
		userDivision = person.getOrganization();
	}

    public boolean isPersonAnonymous()
	{
		return personAnonymous;
	}
	public String getPersonTitle()
	{
		return personTitle;
	}
	public String getPersonFirstname()
	{
		return personFirstname;
	}
	public String getPersonLastname()
	{
		return personLastname;
	}
	public String getPersonOrganization()
	{
		return personOrganization;
	}
	public String getPersonStreetAddress()
	{
		return personStreetaddress;
	}
	public String getPersonZip()
	{
		return personZip;
	}
	public String getPersonLocation()
	{
		return personLocation;
	}
	public String getPersonCountry()
	{
		return personCountry;
	}
	public String getPersonEmail()
	{
		return personEmail;
	}
	public String getPersonFax()
	{
		return personFax;
	}
	public String getPersonPesel()
	{
		return personPesel;
	}
	public String getPersonNip()
	{
		return personNip;
	}
	public String getPersonRegon()
	{
		return personRegon;
	}
	public Boolean getOriginal()
	{
		return original;
	}

	public boolean isCurrentDayWarning()
	{
		return currentDayWarning;
	}
	public Long getBoxId()
	{
		return boxId;
	}
	public String getBoxName()
	{
		return boxName;
	}
	public String getDocumentDate()
	{
		return documentDate;
	}
	public String getPostalRegNumber()
	{
		return postalRegNumber;
	}
	public Integer getDeliveryId()
	{
		return deliveryId;
	}
	public String getFastAssignmentSelect()
	{
		return fastAssignmentSelect;
	}
	public List<DSDivision> getFastAssignmentDivision()
	{
		return fastAssignmentDivision;
	}
	public List<DSUser> getFastAssignmentUser()
	{
		return fastAssignmentUser;
	}
	public String getFastAssignmentSelectUser()
	{
		return fastAssignmentSelectUser;
	}
	public String getFastAssignmentSelectDivision()
	{
		return fastAssignmentSelectDivision;
	}
	public String getUserDivision()
	{
		return userDivision;
	}
	public String getUserLastname()
	{
		return userLastname;
	}
	public String getUserFirstname()
	{
		return userFirstname;
	}
	public Date getEntryDate()
	{
		return entryDate;
	}
	public Long getJournalId()
	{
		return journalId;
	}
	public void setPersonAnonymous(boolean personAnonymous)
	{
		this.personAnonymous = personAnonymous;
	}
	public void setPersonTitle(String personTitle)
	{
		this.personTitle = personTitle;
	}
	public void setPersonFirstname(String personFirstname)
	{
		this.personFirstname = personFirstname;
	}
	public void setPersonLastname(String personLastname)
	{
		this.personLastname = personLastname;
	}
	public void setPersonOrganization(String personOrganization)
	{
		this.personOrganization = personOrganization;
	}
	public void setPersonStreetAddress(String personStreetaddress)
	{
		this.personStreetaddress = personStreetaddress;
	}
	public void setPersonZip(String personZip)
	{
		this.personZip = personZip;
	}
	public void setPersonLocation(String personLocation)
	{
		this.personLocation = personLocation;
	}
	public void setPersonCountry(String personCountry)
	{
		this.personCountry = personCountry;
	}
	public void setPersonEmail(String personEmail)
	{
		this.personEmail = personEmail;
	}
	public void setPersonFax(String personFax)
	{
		this.personFax = personFax;
	}
	public void setPersonPesel(String personPesel)
	{
		this.personPesel = personPesel;
	}
	public void setPersonNip(String personNip)
	{
		this.personNip = personNip;
	}
	public void setPersonRegon(String personRegon)
	{
		this.personRegon = personRegon;
	}
	public void setOriginal(Boolean original)
	{
		this.original = original;
	}
	public void setCurrentDayWarning(boolean currentDayWarning)
	{
		this.currentDayWarning = currentDayWarning;
	}
	public void setBoxId(Long boxId)
	{
		this.boxId = boxId;
	}
	public void setBoxName(String boxName)
	{
		this.boxName = boxName;
	}
	public void setDocumentDate(String documentDate)
	{
		this.documentDate = documentDate;
	}
	public void setPostalRegNumber(String postalRegNumber)
	{
		this.postalRegNumber = postalRegNumber;
	}
	public void setDeliveryId(Integer deliveryId)
	{
		this.deliveryId = deliveryId;
	}
	public void setFastAssignmentSelect(String fastAssignmentSelect)
	{
		this.fastAssignmentSelect = fastAssignmentSelect;
	}
	public void setFastAssignmentDivision(List<DSDivision> fastAssignmentDivision)
	{
		this.fastAssignmentDivision = fastAssignmentDivision;
	}
	public void setFastAssignmentUser(List<DSUser> fastAssignmentUser)
	{
		this.fastAssignmentUser = fastAssignmentUser;
	}
	public void setFastAssignmentSelectUser(String fastAssignmentSelectUser)
	{
		this.fastAssignmentSelectUser = fastAssignmentSelectUser;
	}
	public void setFastAssignmentSelectDivision(String fastAssignmentSelectDivision)
	{
		this.fastAssignmentSelectDivision = fastAssignmentSelectDivision;
	}
	public void setUserDivision(String userDivision)
	{
		this.userDivision = userDivision;
	}
	public void setUserLastname(String userLastname)
	{
		this.userLastname = userLastname;
	}
	public void setUserFirstname(String userFirstname)
	{
		this.userFirstname = userFirstname;
	}
	public void setEntryDate(Date entryDate)
	{
		this.entryDate = entryDate;
	}
	public void setJournalId(Long journalId)
	{
		this.journalId = journalId;
	}

	public void setDefaultKind(InOfficeDocumentKind defaultKind)
	{
		SimpleDocumentMainTabAction.defaultKind = defaultKind;
	}

	public InOfficeDocumentKind getDefaultKind()
	{
		if(defaultKind == null)
			initDefaultKind();
		return defaultKind;
	}

	public void setCreatePerson(boolean createPerson)
	{
		this.createPerson = createPerson;
	}

	public boolean isCreatePerson()
	{
		return createPerson;
	}

	public Long getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public DSPartsManager getPartsManager(){
		return DSPartsConfig.get().provideManager(
			ActionContext
				.action(actionType)
				.type(getDocumentType()));
	}

	public String getActivity() {
		return activityId;
	}

	public void setActivity(String activityId) {
		this.activityId = activityId;
	}

	public List getTabs() {
		return tabs;
	}

	public void setTabs(List tabs) {
		this.tabs = tabs;
	}

	public String getActionTypeString() {
		return actionType.toString();
	}

	public Map<String, String> getLoadFiles() {
		return loadFiles;
	}

	public void setLoadFiles(Map<String, String> loadFiles) {
		this.loadFiles = loadFiles;
	}

	public String[] getReturnLoadFiles() {
		return returnLoadFiles;
	}

	public void setReturnLoadFiles(String[] returnLoadFiles) {
		this.returnLoadFiles = returnLoadFiles;
	}

	public String getFilestoken() {
		return filestoken;
	}

	public void setFilestoken(String filestoken) {
		this.filestoken = filestoken;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getReserveFastAssignmentSelectDivision() {
		return reserveFastAssignmentSelectDivision;
	}

	public void setReserveFastAssignmentSelectDivision(
			String reserveFastAssignmentSelectDivision) {
		this.reserveFastAssignmentSelectDivision = reserveFastAssignmentSelectDivision;
	}

	public Long getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Long revisionId) {
		this.revisionId = revisionId;
	}

	public Boolean getOpenViewer() {
		return openViewer;
	}

	public void setOpenViewer(Boolean openViewer) {
		this.openViewer = openViewer;
	}

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    public List<DocumentKind> getDocumentKinds() {
        return documentKinds;
    }
}
