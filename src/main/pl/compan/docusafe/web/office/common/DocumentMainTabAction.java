package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.Aspect;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.nationwide.icrThread;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.parametrization.aegon.overtime.OvertimeAppLogic;
import pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn;
import pl.compan.docusafe.parametrization.aegon.overtime.OvertimeReceiveLogic;
import pl.compan.docusafe.parametrization.polcz.WniosekUrlopLogic;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public abstract class DocumentMainTabAction extends BaseTabsAction
{
    private final static Logger log = LoggerFactory.getLogger(DocumentMainTabAction.class);

    protected DocumentKind documentKind = null;
    
    // @EXPORT
    private FieldsManager fm;
    private List<DocumentKind> documentKinds;
    private boolean canAddToRS;    
    private List<DaaAttachmentPattern> patterns;

    // @IMPORT
    protected Map<String,Object> values = new HashMap<String,Object>();

    // @EXPORT/@IMPORT
    protected String documentKindCn;
    protected String NR_SZKODY;
    private Long attachmentPattern;  

    private List<DSUser> usersByDocumentToUpdate;
    //aspekty
    private List<Aspect> documentAspects;
    private String documentAspectCn;
    private boolean isNewDocumentAction = true;
    protected List<FormFile> multiFiles = new ArrayList<FormFile>();
    protected Map<String,String> loadFiles;
    protected String[] returnLoadFiles;
    protected String filestoken;
    public abstract void setDefaultDelivery() throws EdmException;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected boolean createNextDocument;
    protected boolean showCreateNext;
    
    public boolean isNormalKind()
    {
        return DocumentKind.NORMAL_KIND.equals(documentKindCn);
    }

    protected void fillForm(int documentType) throws EdmException
    {		
        documentKinds = DocumentKind.listForCreate(documentType);
        setDefaultDelivery();
        if (documentKindCn == null)
        {
            documentKind = DocumentKind.findByCn(DocumentKind.getDefaultKind());
            //jesli tu sa nullpointery to przeczytaj komentarz w webwork-coreoffice przy klasie pl.compan.docusafe.webwork.NationwideRedirectAction
            documentKindCn = documentKind.getCn();
        }
        else
            documentKind = DocumentKind.findByCn(documentKindCn);

        // gdy getDocumentId() == null to po prostu dostaniemy wyzerowane dane
        fm = documentKind.getFieldsManager(getDocumentId()).withActivityId(getActivity());
        fm.initialize();

        showCreateNext = "true".equalsIgnoreCase(documentKind.getProperties().get("createNext"));
        
        documentAspects = documentKind.getAspectsForUser(DSApi.context().getPrincipalName());
        if(documentAspects == null)
        {
        	documentAspectCn = null;
        }
        else if(documentAspects != null && documentAspectCn == null)
        {
        	documentAspectCn = documentAspects.isEmpty() ? null : documentAspects.get(0).getCn();
        }
        
        if (getDocumentId() == null)
            documentKind.logic().setInitialValues(fm, documentType);

        // jak byl blad, to powracamy na ten sam ekran, wiec dobrze zeby pokazaly sie te wartosci w polach, ktore wpisal uzytkownik
        if (hasActionErrors() || createNextDocument)
            fm.reloadValues(values);

        // dodawanie do DRS - tylko na potrzeby rodzaju 'nationwide'
        canAddToRS = documentKind.logic().canAddToRS();
        
        //canReadDAASlowniki = DSApi.context().hasPermission(DSPermission.DAA_SLOWNIK_AGENTA_ODCZYTYWANIE);

        if (DocumentLogicLoader.DAA_KIND.equals(documentKindCn))
            patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();
    }
    
    /**
     * dodaktowe rzeczy wykonywane podczas tworzenia dokumentu zale�ne od wybranego rodzaju dokumentu
     */
    
    protected void specificAdditions(OfficeDocument doc) throws EdmException
    {
        Long docId = doc.getId();
        FieldsManager localFm = doc.getDocumentKind().getFieldsManager(docId).withActivityId(getActivity());
        
        if (DocumentLogicLoader.DAA_KIND.equals(documentKindCn))
        {
            /* dodanie wzoru umowy */
            patterns = DaaAttachmentPattern.getAttachmentPatternsFromConfig();
            if ((attachmentPattern != null) && (patterns.get(attachmentPattern.intValue() - 1).getFilename() != null))
            {
                File patternFile = patterns.get(attachmentPattern.intValue() - 1).getFile();

                if (patternFile != null)
                {
                    Attachment attachment = new Attachment(sm.getString("WzorUmowy"));
                    doc.createAttachment(attachment);
                    attachment.createRevision(patternFile).setOriginalFilename(patternFile.getName());
                }
                else
                    throw new EdmException(sm.getString("BladPodczasPobieraniaPlikuZeWzoremUmowy"));
            }
        }
        
        if (documentKindCn.startsWith("crm"))
        {
            doc.setSource("crm");
        }
        
        //if("true".equals(Docusafe.getAdditionProperty("icrOnLoad")) && DocumentKind.NATIONWIDE_KIND.equals(documentKindCn))
        if(DocumentKind.findByCn(documentKindCn) != null)
        {
        	Map<String, String> properties = DocumentKind.findByCn(documentKindCn).getProperties();
        	if(properties.get("hotIcrFolder") != null)
        	{
        		Thread thread = new Thread(new icrThread(doc)); 
            	thread.start(); 
        	}
        }
        
        //overtime_app dla dokument�w: "Zgloszenie nadgodzin"
        if(documentKindCn.equals(OvertimeCn.DOCKIND_NAME))
        {
            Date overtimeDate = (Date) localFm.getValue(OvertimeCn.OVERTIME_DATE_CN);
            OvertimeAppLogic.needAttachments(docId, overtimeDate);
        }
    }
    
    /**Metoda zapoisuje pobrane zalaczniki to katalogu tymczasowego i dodaje wpis do loadFiles
     * Jest to robione na wypadek wystapienia bledu i powrotu jeszcze raz na formularz dodani dokumenu.
     * Zalaczniki sa pamietane i nie trzeba ich jeszcze raz przesylac */
    protected void saveFile()
    {
    	try
    	{
    		if(filestoken == null || filestoken.length() < 1)
    			filestoken = ServletActionContext.getRequest().getRequestedSessionId()+ new Date().getTime();
    		if(loadFiles == null)
    			loadFiles = new HashMap<String, String>();
	    	if(multiFiles != null && multiFiles.size() > 0)
	        {
	        	for (FormFile formFile : multiFiles)
				{   		
					File tmp = new File(Docusafe.getTemp(),filestoken+"_"+formFile.getFile().getName());
					FileOutputStream os = new FileOutputStream(tmp);
					FileInputStream stream = new FileInputStream(formFile.getFile());
					byte[] buf = new byte[2048];
					int count;
					while ((count = stream.read(buf)) > 0)
					{
					    os.write(buf, 0, count);
					}
					os.close();
					stream.close();
					loadFiles.put(tmp.getAbsolutePath(), formFile.getFile().getName());
				}
	        }
	    	if(returnLoadFiles != null && returnLoadFiles.length > 0)
	    	{
	    		for (String path : returnLoadFiles)
				{
	    			String[] tab = path.split("\\\\");
	    			String fileName = tab[tab.length -1].replace(filestoken+"_", "");
	    			loadFiles.put(path,fileName);
				}
	    	}
    	}
    	catch (Exception e) 
    	{
    		log.error("",e);
			addActionError("B��d zapisu plik�w "+e.getMessage());
		}
    }
    
    /**
     * Metoda dodaj�ca za��czniki do dokumentu, pobiera zalaczniki z listy multiFiles zawierajacej aktualnie wczytane zalaczniki
     * oraz z tablicy returnLoadFiles zawierajace zalaczniki wczytane przed przeladowanie formularza (jesli takie nastapilo)
     * @param doc
     * @throws AccessDeniedException
     * @throws EdmException
     */
    protected void addFiles(OfficeDocument doc) throws AccessDeniedException, EdmException
    {
    	doc.setPermissionsOn(false);
    	if(multiFiles != null && multiFiles.size() > 0)
        {
        	for (FormFile formFile : multiFiles)
			{
        		 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			}            	
        }
        if( returnLoadFiles != null && returnLoadFiles.length > 0 )
        {
        	for (String fileKey : returnLoadFiles)
			{
        		String path = fileKey.replace("\\","/");
				File f = new File(path);
				 Attachment attachment = new Attachment("Skan");
                 doc.createAttachment(attachment);
                 attachment.createRevision(f).setOriginalFilename(f.getName().replace(filestoken+"_", ""));
			}
        }
    }
    
	public Map<String, String> getLoadFiles()
	{
		return loadFiles;
	}

	public String[] getReturnLoadFiles()
	{
		return returnLoadFiles;
	}

	public void setLoadFiles(Map<String, String> loadFiles)
	{
		this.loadFiles = loadFiles;
	}

	public void setReturnLoadFiles(String[] returnLoadFiles)
	{
		this.returnLoadFiles = returnLoadFiles;
	}

	public FormFile getMultiFiles() 
	{
		if(this.multiFiles != null && this.multiFiles.size() > 0 )
			return this.multiFiles.get(0);
		else
			return null;
	}
    
	public void setMultiFiles(FormFile file) {
		this.multiFiles.add(file);
	}

    public FieldsManager getFm()
    {
        return fm;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public List<DocumentKind> getDocumentKinds()
    {
        return documentKinds;
    }

    public boolean isCanAddToRS()
    {
        return canAddToRS;
    }
    
    public String getNR_SZKODY()
    {
        return NR_SZKODY;
    }

    public void setNR_SZKODY(String NR_SZKODY)
    {
        this.NR_SZKODY = NR_SZKODY;
    }
    
    public void setAttachmentPattern(Long attachmentPattern)
    {
        this.attachmentPattern = attachmentPattern;
    }

    public Long getAttachmentPattern()
    {
        return attachmentPattern;
    }

    public List<DaaAttachmentPattern> getPatterns()
    {
        return patterns;
    }    
    
    public String getDockindAction()
    {
        return "create";
    }

	public List<Aspect> getDocumentAspects() {
		return documentAspects;
	}

	public void setDocumentAspects(List<Aspect> documentAspects) {
		this.documentAspects = documentAspects;
	}

	public String getDocumentAspectCn() {
		return documentAspectCn;
	}

	public void setDocumentAspectCn(String documentAspectCn) {
		this.documentAspectCn = documentAspectCn;
	}

	public void setIsNewDocumentAction(boolean isNewDocumentAction)
	{
		this.isNewDocumentAction = isNewDocumentAction;
	}

	public boolean isNewDocumentAction()
	{
		return true;
	}

	public void setFilestoken(String filestoken)
	{
		this.filestoken = filestoken;
	}

	public String getFilestoken()
	{
		return filestoken;
	}

	public boolean isCreateNextDocument() {
		return createNextDocument;
	}

	public void setCreateNextDocument(boolean createNextDocument) {
		this.createNextDocument = createNextDocument;
	}

	public boolean isShowCreateNext() {
		return showCreateNext;
	}

	public void setShowCreateNext(boolean showCreateNext) {
		this.showCreateNext = showCreateNext;
	}
	
	
        // ----------PONIZEJ METODY DLA KART NADGODZIN
        /**
         * Zwraca kart� nadgodzin u�ytkownika w danym roku
         * @return List<EmployeeOvertimeEntity>
         */
        public List<EmployeeOvertimeEntity> getEmpOvertimes()
        {
            return ((OvertimeReceiveLogic) fm.getDocumentKind().logic()).getEmpOvertimes();
        }
        
        public String getEmpOvertimesAllHour()
        {
            return ((OvertimeReceiveLogic) fm.getDocumentKind().logic()).getAllHour();
        }
        
        public String getEmpOvertimesAllHourReceive()
        {
            return ((OvertimeReceiveLogic) fm.getDocumentKind().logic()).getAllHourReceive();
        }
        
        public String getEmpOvertimesAllHourLeft()
        {
            return ((OvertimeReceiveLogic) fm.getDocumentKind().logic()).getAllHourLeft();
        }
        
        
	// PONI�EJ METODY DLA WNIOSKU URLOPOWEGO AEGON
	
	/**
	 * Zwraca list� urlop�w pracownika w bierzacym roku, dla widoku
	 * 
	 * @return
	 */
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getEmplAbsences();
	}
	
	public List<FreeDay> getFreeDays()
	{
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getFreeDays();
	}
	
	public int getCurrentYear()
	{
		return GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
	}
	
	public String getEmpCardName()
	{
		DocumentLogic logic = fm.getDocumentKind().logic();
		EmployeeCard card = null;
		if (logic instanceof AbsenceLogic)
			card = ((AbsenceLogic) logic).getEmployeeCard();
		else if (logic instanceof OvertimeReceiveLogic)
			card = ((OvertimeReceiveLogic) logic).getEmpCard();
		else if (logic instanceof WniosekUrlopLogic)
			card = ((WniosekUrlopLogic) logic).getEmployeeCard();
		if (card != null)
			return card.getExternalUser();
		return null;
	}
        
    /**        ---------- FUNKCJA DO ODFILTROWANIA UZYTKOWNIKOW W RAZIE POTRZEBY
     * potrzebny <available name="archiwizacja.dokument.updatujListeUzytkownika" value="true"/> w xml
     */
        
   /**
     * funkcja potrzebna do modyfikowania listy uzytkownik�w
     * @return
     * @throws EdmException 
     */
    public List<DSUser> getUsersByDocumentToUpdate() throws EdmException
    {
        try
        {
//            usersByDocumentToUpdate = ArrayList<DSUser>();
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            
            if(documentKindCn.equals("overtime_app"))
                usersByDocumentToUpdate = DSUser.listAcceptanceSubordinates(DSApi.context().getPrincipalName());
            
        } catch (EdmException e){
            log.error("",e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();

        } finally{
            try{
                DSApi.close();
            } catch (EdmException e){}
        }

        return usersByDocumentToUpdate;
    }
    
    public String getUsersDockindFieldUpdate()
    {
        log.error(documentKindCn);
        
        if(documentKindCn.equals("overtime_app"))
            return "DS_USER";
        
        return "DS_USER";
    }
}
