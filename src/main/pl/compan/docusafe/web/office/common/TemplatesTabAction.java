package pl.compan.docusafe.web.office.common;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.google.gson.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.repository.SimpleFileSystemRepository;
import pl.compan.docusafe.core.templating.DocumentSnapshot;
import pl.compan.docusafe.core.templating.DocumentSnapshotSerializer;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.spring.user.office.TemplateServiceImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.*;
import java.net.ConnectException;
import java.text.Format;
import java.util.*;


public abstract class TemplatesTabAction extends BaseTabsAction
{
    private static final Logger log = LoggerFactory.getLogger(TemplatesTabAction.class);

    private FormFile file;

    @Override
    protected void setup() {

        registerListener(DEFAULT_ACTION)
                .append(OpenHibernateSession.INSTANCE)
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("test")
                .append(OpenHibernateSession.INSTANCE)
                .append(new TestAction())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("createSnapshot")
                .append(OpenHibernateSession.INSTANCE)
                .append(new CreateSnapshot())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("getSnapshot")
                .append(OpenHibernateSession.INSTANCE)
                .append(new GetSnapshot())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("getSnapshots")
                .append(OpenHibernateSession.INSTANCE)
                .append(new GetSnapshots())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("updateSnapshot")
                .append(OpenHibernateSession.INSTANCE)
                .append(new UpdateSnapshot())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("deleteSnapshot")
                .append(OpenHibernateSession.INSTANCE)
                .append(new DeleteSnapshot())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("createFileDocument")
                .append(OpenHibernateSession.INSTANCE)
                .append(new CreateFileDocument())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("deleteFileDocument")
                .append(OpenHibernateSession.INSTANCE)
                .append(new DeleteFileDocument())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("uploadFileDocument")
                .append(OpenHibernateSession.INSTANCE)
                .append(new UploadFileDocument())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("getFileDocument")
                .append(OpenHibernateSession.INSTANCE)
                .append(new GetFileDocument())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("createPdf")
                .append(OpenHibernateSession.INSTANCE)
                .append(new CreatePdf())
                .appendFinally(CloseHibernateSession.INSTANCE);
    }

    abstract protected List prepareTabs();

    private class FillForm extends RichActionListener {

        @Override
        public void action(ActionEvent event) throws Exception {
            log.info("prepareTabs()");
            setTabs(prepareTabs());
            log.info("tabs.length = {}", tabs.size());
        }

    }

    private class TestAction extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) {
            event.addActionError("fatal error!!");
            event.addActionMessage("awesome!");
            event.addActionMessage("ok!");

            markAsFailed();

            return new JsonPrimitive("asd");
        }
    }

    private class GetSnapshot extends AjaxActionListener {



        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long id = getLongParameterOrError("id");

            try{
                return getTemplateService().getSnapshot(id);
            }catch(Exception e){
                markAsFailed();
            }

            return new JsonNull();
        }
    }

    private class GetSnapshots extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long documentId = getLongParameterOrError("documentId");

            return getTemplateService().getSnapshots(documentId);
        }
    }

    private class UpdateSnapshot extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(final ActionEvent event) throws Exception {
            final Long snapshotId = getLongParameterOrError("id");
            final String values = getParameterOrError("values");
            final String title = getParameterOrError("title");

            new RichEventActionSupport.ActionTransaction(event).execute(new DSTransaction.Function<Object>() {
                @Override
                public Object apply() throws Exception {
                   return getTemplateService().updateSnapshot(snapshotId, values, title);
                }

                public void success() {
                    addActionMessage("Zapisano!");
                }
            });


            return null;
        }
    }

    private class CreateSnapshot extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            final Long documentId = getLongParameterOrError("documentId");
            // opcjonalny json warto¶ci
            final String values = getParameter("values");

            Optional<DocumentSnapshot> snapshot = new RichEventActionSupport.ActionTransaction(event).execute(new DSTransaction.Function<DocumentSnapshot>() {

                @Override
                public DocumentSnapshot apply() throws Exception {
                    return getTemplateService().createSnapshot(documentId, values);
                }

            });

            if(snapshot.isPresent())
            {
                log.debug("ctime class = {}", snapshot.get().getCtime().getClass());
                return snapshot.get().getJsonView();
            }
            else
            {
                markAsFailed();
                return null;
            }
        }
    }

    private class DeleteSnapshot extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(final ActionEvent event) throws Exception {

            final Long snapshotId = getLongParameterOrError("id");

            new RichEventActionSupport.ActionTransaction(event).execute(new DSTransaction.Function<DocumentSnapshot>() {

                private Long documentId;

                @Override
                public DocumentSnapshot apply() throws Exception {
                   getTemplateService().deleteSnapshot(snapshotId);
                   return null;
                }

                public void success() {
                    addActionMessage("Usuni�to snapshot o id "+snapshotId);
                }
            });

            return null;
        }
    }

    private class CreateFileDocument extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            Long snapshotId = getLongParameterOrError("id");
            Long documentId = getLongParameterOrError("documentId");
            String templateName = getParameterOrError("templateName");
            String dockind = getParameterOrError("dockind");
            final String t = getParameter("isUserTemplate");
            boolean isUserTemplate = Boolean.valueOf(t);

            return getTemplateService().createFile(snapshotId, documentId, templateName, dockind, isUserTemplate);
        }
    }

    private class DeleteFileDocument extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long snapshotId = getLongParameterOrError("id");
            Long documentId = getLongParameterOrError("documentId");
            String documentName = getParameterOrError("documentName");

           return getTemplateService().deleteFile(snapshotId, documentId, documentName);
        }
    }

    private class GetFileDocument extends FileActionListener {

        @Override
        public FileInfo fileAction(ActionEvent event) throws Exception {
            Long snapshotId = getLongParameterOrError("id");
            Long documentId = getLongParameterOrError("documentId");
            String documentName = getParameterOrError("documentName");

            return getTemplateService().getFile(snapshotId, documentId, documentName);
        }
    }

    private class UploadFileDocument extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            Long snapshotId = getLongParameterOrError("id");
            Long documentId = getLongParameterOrError("documentId");
            String documentName = getParameterOrError("documentName");

            if(file == null) {
                throw new EdmException("Brak pliku do wgrania");
            }

            return getTemplateService().uploadFile(snapshotId, documentId, documentName, file.getFile());
        }
    }

    private class CreatePdf extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            final Long snapshotId = getLongParameterOrError("id");
            final Long documentId = getLongParameterOrError("documentId");
            final String documentName = getParameterOrError("documentName");

            return getTemplateService().createPdf(snapshotId, documentId, documentName);
        }

//        private void createAttachment(Long documentId, String filename, FileInputStream pdfInputStream) {
//            Add
//        }
    }

    private TemplateServiceImpl getTemplateService() {
        return Docusafe.getBean(TemplateServiceImpl.class);
    }


    public void setFile(FormFile file) {
        this.file = file;
    }

    public FormFile getFile() {
        return file;
    }
}
