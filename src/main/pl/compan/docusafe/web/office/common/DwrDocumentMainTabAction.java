package pl.compan.docusafe.web.office.common;

import com.google.gson.Gson;
import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.Collections;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.RequestLoader;
import pl.compan.docusafe.core.dockinds.field.EmailMessageField;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MessageAttachment;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.xes.XesLogVerification;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.parametrization.p4.P4Helper;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;
import pl.compan.docusafe.web.office.CurrentDayAction;
import pl.compan.docusafe.web.office.out.SplitOutOfficeDocument;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.*;


//import org.jivesoftware.smack.packet.Session;
//import org.jivesoftware.smack.packet.Session;

public abstract class DwrDocumentMainTabAction extends BaseTabsAction implements DwrOfficeDocument
{

    public static final String FAST_ASSIGNMENT_SELECT_USER = "fastAssignmentSelectUser";
    public static final String FAST_ASSIGNMENT_SELECT_DIVISION = "fastAssignmentSelectDivision";
    public static final String DOC_DELIVERY = "DOC_DELIVERY";
    public static final String DELIVERY_TYPE = "DELIVERY_TYPE";

    public Long getNewDocumentId() {
		return newDocumentId;
	}

	public void setNewDocumentId(Long newDocumentId) {
		this.newDocumentId = newDocumentId;
	}

	private static final long serialVersionUID = 1L;
	protected StringManager sm = GlobalPreferences.loadPropertiesFile(DwrDocumentMainTabAction.class.getPackage().getName(), null);
	protected Logger log = LoggerFactory.getLogger(DwrDocumentMainTabAction.class);

	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();

	protected static final String EV_FILL = "fillForm";
	protected static final String EV_CREATE = "create";
	protected static final String EV_CREATE_GOTO_DOCUMENT = "createGoToDocument";
	protected static final String EV_UPDATE = "update";
	protected static final String EV_CREATE_ADDTO_CASE = "createAddToCase";
	private static InOfficeDocumentKind defaultKind;
	private Map<String, String> documentKinds;

    protected boolean currentDayWarning;
	protected static final String EV_REMOVEFM = "reloadfm";
	/** MAPA cn -> key */
	protected Map<String, Object> dockindKeys;
	protected String fastAssignmentSelectUser;
	protected String fastAssignmentSelectDivision;
	protected Boolean goToDocument;
	protected Long newDocumentId;
	protected Boolean addToCase;
	protected Boolean splitNewDocument = false;
	protected boolean splitAndAssignOfficeNumber = false;
	private boolean canAssignOfficeNumber=false; 
	private boolean assignOfficeNumber=false; 
	protected String documentType;
	protected String splitIndex = ",";
	private static final String inDocument = "incoming";
	private static final String outDocument = "outgoing";
	protected Long createdDocumentId;
	protected boolean outOfficeDocument;
	
	

	protected boolean createNextDoc;
    protected boolean createNextDocForThisUser;

	private String documentKindCn;
	private Map<String, Object> initalValues;

	private FieldsManager fm;
	
	protected Long inDocumentId;
	protected String messageId;
	protected String summary;
	protected String documentDate;
	protected Long journalId = null;
	protected Date entryDate = null;

	protected Map<String, String> loadFiles;
	protected String[] returnLoadFiles;
	protected List<FormFile> multiFiles = new ArrayList<FormFile>();
	protected String filestoken;

	private String dependsDocsJson;
	//pole dla przekazywania wartosci 
	private Long caseId;
    /** @var dodatkowa w�a�ciwo�� przekazywana w requescie. W przypadku, gdy odno�nik
     * prowadzi z innej formatki i ma zostac przekazany dalej */
    private String prop;
	private String empCardName;
	
	private Long attId;
	
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreateGoToDocument")
                .append(OpenHibernateSession.INSTANCE)
                .append(new ValidateCreate())
                .append(EV_CREATE_GOTO_DOCUMENT, new GoToDocument())
                .append(EV_CREATE, new Create())
                .append(new XesLogVerification())
                .append(EV_FILL, fillForm)
                .append(EV_REMOVEFM, new RemoveFMSessionAttribute())
                .appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doCreate")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ValidateCreate())
			.append(EV_CREATE, new Create())
			.append(new XesLogVerification())
			.append(EV_FILL, fillForm)
			.append(EV_REMOVEFM, new RemoveFMSessionAttribute())
			.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ValidateCreate())
			.append(EV_UPDATE, new Update())
			 .append(new XesLogVerification())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		 registerListener("doCreateAddToCase")
         .append(OpenHibernateSession.INSTANCE)
         .append(new ValidateCreate())
         .append(EV_CREATE_ADDTO_CASE, new addToCase())
         .append(EV_CREATE, new Create())
         .append(new XesLogVerification())
         .append(EV_FILL, fillForm)
         .append(EV_REMOVEFM, new RemoveFMSessionAttribute())
         .appendFinally(CloseHibernateSession.INSTANCE);
		
		 registerListener("doSplitDocument")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ValidateCreate())
			.append(new SplitNewDocument())
			.append(EV_CREATE, new Create())
			.append(new XesLogVerification())
			.append(EV_FILL, fillForm)
			.append(EV_REMOVEFM, new RemoveFMSessionAttribute())
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		 registerListener("doSplitAndAssingOficeNumber")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ValidateCreate())
			.append(new SplitAndAssingOficeNumber())
			.append(new SplitNewDocument())
			.append(new XesLogVerification())
			.append(EV_CREATE, new Create())
			.append(EV_FILL, fillForm)
			.append(EV_REMOVEFM, new RemoveFMSessionAttribute())
			.appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doCreateAndAssignOfficeNumber")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ValidateCreate())
			.append(new AssignOfficeNumber())
			.append(new XesLogVerification())
			.append(EV_CREATE, new Create())
			.append(EV_FILL, fillForm)
			.append(EV_REMOVEFM, new RemoveFMSessionAttribute())
			.appendFinally(CloseHibernateSession.INSTANCE);
		 
	}
	
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getEmplAbsences();
	}
	public class FillForm extends LoggedActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			try
			{
				isPrinterDefined();
				
				setCaseIdInProperty(caseId);
                setPropInProperty(prop);
                currentDayWarning = !DateUtils.datesEqual(new Date(), GlobalPreferences.getCurrentDay());
				LoggerFactory.getLogger("tomekl").debug("test!!! {}",attId);

				Map<String, String> dependsDocs = new HashMap<String, String>();

				//edycja dokumentu przychodzacego
				if (inDocumentId != null) {
					if (DocumentKind.findByCn("normal_out")!=null){
					    dependsDocs = DocumentKind.findByCn("normal_out").logic().setInitialValuesForReplies(inDocumentId);
					}
					else {
						dependsDocs = DocumentKind.findByCn("paadocout").logic().setInitialValuesForReplies(inDocumentId);
					}
				}
				
				if (messageId != null && !messageId.isEmpty())
                {
                    MailMessage mailMessage = MailMessage.find(Long.parseLong(messageId));
                    String content = EmailUtils.getEmailContent(mailMessage);
					dependsDocs.put("DWR_DOC_DESCRIPTION", content);
					dependsDocs.put(EmailMessageField.MESSAGE_ID_COLUMN, String.valueOf(messageId));
				}
				//za�adownie do jsona danych przygotowanych wczesniej np  z maila
				if(dependsDocs.size() > 0)
				{
					Gson gson = new Gson();
					String id = gson.toJson(dependsDocs);
					String encodedString = null;
					try
					{
						encodedString = URLEncoder.encode(id, "UTF-8");
					}
					catch (UnsupportedEncodingException e)
					{
						log.error(e.getMessage(),e);
					}
					setDependsDocsJson(encodedString);
				}
				
				dwrDocumentHelper.fillForm(DwrDocumentMainTabAction.this);
				
				DocumentKind documentKind = DocumentKind.findByCn(getDocumentKindCn());
			
				fm = documentKind.getFieldsManager(getDocumentId()).withActivityId(getActivity());
		        fm.initialize();
		        if(AvailabilityManager.isAvailable("documentMain.SplitOutOfficeDocument")){
		        	outOfficeDocument = checkDocumentKind(documentKind);
		        }
		        /*if (messageId != null && !messageId.isEmpty()) {
                    MailMessage mailMessage = MailMessage.find(Long.parseLong(messageId));
                    String content = EmailUtils.getEmailContent(mailMessage);
		            documentKind.logic().setInitialValuesFromEmail(fm, mailMessage ,content);
                }*/
		        canAssignOfficeNumber = DSApi.context().hasPermission(DSPermission.PISMO_WYCH_PRZYJECIE_KO);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				addActionError(sm.getString("blad") + e.getMessage());
			}
		}

	
		@Override
		public Logger getLogger()
		{
			return log;
		}
	}


    public class RemoveFMSessionAttribute extends LoggedActionListener
	{

        @Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			try
			{
				HttpSession session = ServletActionContext.getRequest().getSession();

				Map<String,Object> map = (Map<String, Object>) session.getAttribute(DwrFacade.DWR_SESSION_NAME);
				if(map != null)
				{
					 Map<String,String> fv = (Map<String, String>) map.get("FIELDS_VALUES");
					 if(fv != null)
						 fv.remove("SENDER");
				}

                if (createNextDocForThisUser)
                {
                       session.setAttribute(FAST_ASSIGNMENT_SELECT_USER, fastAssignmentSelectUser);
                       session.setAttribute(FAST_ASSIGNMENT_SELECT_DIVISION, fastAssignmentSelectDivision);
                    if (dockindKeys != null && dockindKeys.containsKey(DOC_DELIVERY))
                    {
                        Integer docDeliveryId = (Integer) dockindKeys.get(DOC_DELIVERY);
                        session.setAttribute(DELIVERY_TYPE, docDeliveryId);
                    }
                }
                else
                {
                    session.removeAttribute(FAST_ASSIGNMENT_SELECT_USER);
                    session.removeAttribute(FAST_ASSIGNMENT_SELECT_DIVISION);
                    session.removeAttribute(DELIVERY_TYPE);
                }

				if((createNextDoc) && AvailabilityManager.isAvailable("dwr.notremove.FMSession.after.create"))
					return;
				
				session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
				
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				addActionError("B��d :" + e.getMessage());
			}
		}

		@Override
		public Logger getLogger()
		{
			return log;
		}
	}

    public class ValidateCreate extends LoggedActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			log.info("loading dwr fields");
			setDockindKeys(RequestLoader.loadKeysFromWebWorkRequest());
		}

		@Override
		public Logger getLogger()
		{
			return log;
		}
	}
	
	private boolean checkDocumentKind(DocumentKind documentKind)
	{
		 Set<DocumentType> dt = documentKind.getDockindInfo().getDocumentTypes();
	        for (DocumentType  d : dt){
	        	if ("out".contains(d.getName()))
	        		return true;
	        }
	        
	        	return false;
	   
	}
	
	public void isPrinterDefined() {
		// TODO Auto-generated method stub
		
	}

	public Document getNewDocument()
	{
		return dwrDocumentHelper.getNewDocument(DwrDocumentMainTabAction.this);
	}

    private class GoToDocument implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            DwrDocumentMainTabAction.this.goToDocument = true;
        }
    }
  
    private class addToCase implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            DwrDocumentMainTabAction.this.addToCase = true;
        }
    }
    
    
    private class SplitAndAssingOficeNumber implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            DwrDocumentMainTabAction.this.splitAndAssignOfficeNumber = true;
        }
    }
    private class SplitNewDocument implements ActionListener 	{
	
		@Override
        public void actionPerformed(ActionEvent event) {
            DwrDocumentMainTabAction.this.splitNewDocument = true;
        }
		
	}
    private class AssignOfficeNumber implements ActionListener 	{
    	
		@Override
        public void actionPerformed(ActionEvent event) {
            DwrDocumentMainTabAction.this.assignOfficeNumber = true;
        }
		
	}
    
    private void splitDocument(OfficeDocument doc, ActionEvent event) {
           
           if(AvailabilityManager.isAvailable("documentMain.SplitOutOfficeDocument")&& doc != null)
			{
				
				if (!(doc instanceof OutOfficeDocument))
				{
					addActionError("DokumentNieJestPismemWychodzacym");
					return;
				}
				try
				{ 
					
					SplitOutOfficeDocument.splitOutOfficeDocument((OfficeDocument)doc, dockindKeys,splitAndAssignOfficeNumber ,event);
				} catch (EdmException e)
				{
					log.error("", e);
				} catch (SQLException e)
				{
					log.error("", e);
				}
			}
		}
	

	public class Create implements ActionListener
	{
		
		public void actionPerformed(ActionEvent event){
            create(event);
//			System.out.println("tworze dsadas");
            if (hasActionErrors()){
                saveFile();
                return;
            }
        }

        public void create(ActionEvent event){
			OfficeDocument doc = null;
			try
			{
				DSApi.context().begin();
				setDocument(getNewDocument());
				doc = (OfficeDocument) getDocument();
				setBeforeCreate(doc);
				

				doc.setCurrentAssignmentGuid(fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision : DSDivision.ROOT_GUID);
				doc.setDivisionGuid(fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision : DSDivision.ROOT_GUID);
				DocumentKind.findByCn(getDocumentKindCn()).logic().revaluateFastAssignmentSelectetValuesOnDocument((OfficeDocument)doc , fastAssignmentSelectUser, fastAssignmentSelectDivision);
				doc.setCurrentAssignmentAccepted(Boolean.FALSE);

				DocumentKind documentKind = DocumentKind.findByCn(getDocumentKindCn());
				String newDocKindCn = documentKind.getProperties().get("zmien.na.dockind");
				
				if(newDocKindCn !=null)
				{
					documentKind = DocumentKind.findByCn(newDocKindCn);
					documentKindCn = newDocKindCn;
				}
				
				documentKind.logic().validate(dockindKeys, documentKind, null);
				
				if (dockindKeys.get("DOC_DESCRIPTION") != null && !dockindKeys.get("DOC_DESCRIPTION").equals(""))
					summary = (String) dockindKeys.get("DOC_DESCRIPTION");
				
				doc.setSummary(summary != null ? summary : documentKind.getName());
				doc.setAssignedDivision(DSDivision.ROOT_GUID);
				doc.setDocumentKind(documentKind);
				if (doc instanceof OutOfficeDocument){
					if(((OutOfficeDocument) doc).getPriority() ==null)
						((OutOfficeDocument) doc).setPriority(false);
					if(((OutOfficeDocument) doc).getCountryLetter() ==null)
						((OutOfficeDocument) doc).setCountryLetter(true);
					}
				doc.setInPackage(false);
				
				
				
				doc.create();
				updateTaskSnapshotOnAnswerCounter();
				
				
				if(splitNewDocument)
					splitDocument(doc,event);
				
				
				setAfterCreate(doc);
				
				Long newDocumentId = doc.getId();
				documentKind.logic().setAfterCreateBeforeSaveDokindValues(documentKind ,newDocumentId, dockindKeys);
				documentKind.saveDictionaryValues(doc, dockindKeys);
				documentKind.saveMultiDictionaryValues(doc, dockindKeys);

				doc.getDocumentKind().logic().removeValuesBeforeSet(dockindKeys);
				
				documentKind.set(newDocumentId, dockindKeys);

				doc.getDocumentKind().logic().archiveActions(doc, 0);
				doc.getDocumentKind().logic().documentPermissions(doc);
				if(doc.getBarcode() != null){
					Document docBase = (Document) doc;
					docBase.setDocumentBarcode(doc.getBarcode());
				}
				DSApi.context().session().save(doc);
				
				if(AvailabilityManager.isAvailable("kancelaria.wlacz.walidacjaZalacznikaPrzyTworzeniuPisma")){
					if(ServletActionContext.getRequest().getAttribute("pustyPlik") != null){
						boolean b = (Boolean) ServletActionContext.getRequest().getAttribute("pustyPlik");
						if(b)
							throw new EdmException(sm.getString("PodanoPlikJestPustyLubNiepoprawny"));
					}
				}
				
				bindToJournal(doc, event);
				
				if (assignOfficeNumber && doc instanceof OutOfficeDocument && (doc.getOfficeNumber()==null))
					assignOfficeNumber((OutOfficeDocument)doc);
				

                if (CurrentDayAction.P_4_CURRENT_DAY)
                {
                    Long journalId = P4Helper.getJournalId(doc);
                    Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay(P4Helper.journaGuidOwner()));
                    doc.setJournal(Journal.find(journalId));
                    if (doc instanceof InOfficeDocument)
                        ((InOfficeDocument)doc).bindToJournal(journalId, sequenceId, event);
                }
//                Niestety ale nie t�dy droga, PIG zrezygnowa� za du�o to rozwala�o dzia�anie  
                else if (AvailabilityManager.isAvailable("journal.setOfficeNumberFrom") && doc instanceof pl.compan.docusafe.core.office.OfficeDocument && ((pl.compan.docusafe.core.office.OfficeDocument) doc).getJournal() != null && !((pl.compan.docusafe.core.office.OfficeDocument) doc).getJournal().isMain())
                {
                    pl.compan.docusafe.core.office.Journal journal = ((pl.compan.docusafe.core.office.OfficeDocument) doc).getJournal();
                    if (journal != null)
                    {
                    	pl.compan.docusafe.core.office.JournalEntry entry = journal.findDocumentEntry(doc.getId());
           
                    	if (entry != null)
                        {	
                            Integer sequenceId = journal.findDocumentEntry(doc.getId()).getSequenceId();
                            ((pl.compan.docusafe.core.office.OfficeDocument) doc).setOfficeNumber(sequenceId);
                        }
                    }
                }
				if (fastAssignmentSelectUser != null || fastAssignmentSelectDivision != null)
					event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, fastAssignmentSelectUser);

				if(fastAssignmentSelectDivision == null && fastAssignmentSelectUser !=null)
				{
					DSUser user = DSUser.findByUsername(fastAssignmentSelectUser);
					String[] divs = user.getDivisionsGuidWithoutGroup();
					event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, divs.length > 0 ?  divs[0] : DSDivision.ROOT_GUID);
				}
				else
				{
					event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, fastAssignmentSelectDivision);
				}

				event.setAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, fastAssignmentSelectDivision != null ? fastAssignmentSelectDivision : DSDivision.ROOT_GUID);
				
				if (messageId != null && !messageId.isEmpty())
                {
                    MailMessage message = MailMessage.find(Long.parseLong(messageId));
                    
                    for (MessageAttachment messageAttachment : message.getAttachments()) 
                    {
                    	Attachment attachment = new Attachment(messageAttachment.getFilename());
                    	attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
                        attachment.setWparam(Long.parseLong(messageId));
        				doc.createAttachment(attachment);
        				attachment.createRevision(messageAttachment.getBinaryStream(), messageAttachment.getSize(), messageAttachment.getFilename());
					}
                    
                    Attachment attachment = new Attachment("Mail");
                	attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
                    attachment.setWparam(Long.parseLong(messageId));
    				doc.createAttachment(attachment);
    				attachment.createRevision(message.getBinaryStream(),message.getBinaryStream().available(),message.getSubject()+".eml");
                    message.setProcessed(Boolean.TRUE);
                    //	wiazemy dokument z emailem (message_id)
                    if(doc instanceof InOfficeDocument){
                    	((InOfficeDocument)doc).setMessageId(Long.parseLong(messageId));
                    }
                }
				
				addFiles(doc);
				if (!DSApi.context().inTransaction())
					DSApi.context().begin();
				doc.getDocumentKind().logic().onStartProcess(doc, event);
				//Dodaje poprawne wpisy na dokumencie w kolumnach currentAssignedGuid i jesli jest username  - w przyapdku gdy na dokindzie wystepuje field document-user-division-second a nie chcemy aby pismo trafilo do odbiorcy okreslonym na dokindzie bo w metodzie setBeforeCreate(doc) ustawiane sa  domyslnie wartosci zczytywane z fielda (division i user) 
				if (AvailabilityManager.isAvailable("setupDocumentAssignmentsFromOnStartProcesParams"))
				{
					if (event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM) != null
							&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM)).isEmpty())
					{
						doc.setDivisionGuid((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM));
						doc.setAssignedDivision((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM));
						doc.setCurrentAssignmentGuid((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM));
						
					}
					if (event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM) != null
							&& !((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)).isEmpty())
					{
						doc.setCurrentAssignmentUsername((String) event.getAttribute(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM));

					}

				}
				
				
				ustawWersje(doc);
				
				DSApi.context().commit();

                event.addActionMessage(doc.getDocumentKind().logic().getActionMessage(ActionType.OFFICE_CREATE_DOCUMENT, doc));

               

				DSApi.context().begin();
                try
                {
                    doc.getDocumentKind().logic().printLabel(doc, getDocType(), dockindKeys);
                }
                catch (Exception e)
                {
                    event.addActionError(e.getMessage());
                }
				TaskSnapshot.updateByDocument(doc);
				
				if (dockindKeys.containsKey("RECIPIENT_PARAM") && 
						(!dockindKeys.containsKey("RECIPIENT") || (dockindKeys.containsKey("RECIPIENT") && dockindKeys.get("RECIPIENT") == null) ))
		    	{
					if(DwrDictionaryFacade.getDwrDictionary(getDocumentKindCn(), "RECIPIENT").isOnlyPopUpCreate())
					{
						throw new EdmException(sm.getString("OnlyPopupCreate"));
					}
		    		Person person = new Person();
		    		person.fromMapUpper((Map)dockindKeys.get("RECIPIENT_PARAM"));
		    		person.setDictionaryGuid("rootdivision");
			        person.create();
			        Recipient recipient = new Recipient();
			        recipient.fromMap(person.toMap());
			        recipient.setDictionaryGuid("rootdivision");
			        recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
			        recipient.setBasePersonId(person.getId());
			        recipient.setDocumentId(doc.getId());
			        recipient.setDocument(doc);
			        recipient.create();
			        doc.addRecipient(recipient);
		    	}
				
				
				
				for (Recipient rec : doc.getRecipients()) {
					rec.setDocumentId(doc.getId());
					rec.setDocument(doc);
				}
				
                if(inDocumentId != null){
                	OfficeDocument inDocument = OfficeDocument.find(inDocumentId);
                	//nie ka�de pismo na ktore tworzymy odpowied� jest w sprawie wiec musi byc notnulem bo sypie nulpointerem
                	if (inDocument.getContainingCase()!=null){
                	doc.setContainingCase(inDocument.getContainingCase(), true);
                	doc.setContainingCaseId(inDocument.getContainingCase().getId());
                	}
                }
                
				DSApi.context().commit();
	            if(AvailabilityManager.isAvailable("redirectToTasklistAfterCreate") && !createNextDoc)
	            {
	            	event.setResult("task-list");
	            }
	            if(AvailabilityManager.isAvailable("redirectToTasklistAfterCreateRepilesDoc") && inDocumentId!=null)
	            {
	            	event.setResult("task-list");
	            }
	            if(AvailabilityManager.isAvailable("redirectToTasklistDocumentTypeAfterCreate")){
	            	event.setResult(getTaskListResult(doc.getType()));
	            }
	            messageId = null;
                if(goToDocument != null && goToDocument)
                {
    				// nie wime jak to lepiej zrobic
                    TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);                
    				ArrayList<Task> officeTask = new ArrayList<Task>(taskList.getTasks(DSApi.context().getDSUser(),newDocumentId));			
    				if (officeTask.size() > 0)
                    {
                        // TODO - IndexOutOfBoundException
                    Task task = officeTask.get(0);
    				setActivity(WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey()));
                	event.setResult("go-to-document");
                    }
                }
                if(addToCase!=null && addToCase) {
                    createdDocumentId = doc.getId();
                	if (doc instanceof InOfficeDocument) {
                        documentType = inDocument;
                    } else {
                        documentType= outDocument;
                    }
                    event.setResult("case");
                }

                // propagacja XML do jackrabbita
                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(doc.getId());
                }
                
             //   XesLog.isXesLogForThisAction(event, ServletActionContext.getRequest().getRequestURI());
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				event.addActionError(e.getMessage());
				event.skip(EV_REMOVEFM);

				DSApi.context().setRollbackOnly();

                try { // zamkni�cie kontekstu przez commit - uwzgl�dnia setRollbackOnly
                    DSApi.context().commit();
                } catch (EdmException e1) {
                    log.error(e1.getMessage(), e1);
                }
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				try
				{
					event.skip(EV_REMOVEFM); 
					DSApi.context()._rollback();
				}
				catch (Exception e2)
				{
					log.error(e.getMessage(), e);
					// TODO: handle exception
				}
				event.addActionError(e.getMessage());
			}
		}

        /**
         * Method is used to update answerCounter on JBPMTaskSnapshot object when document out is creted
         * if is set up inDoucumentId
         */
		private void updateTaskSnapshotOnAnswerCounter() {
			if (getInDocumentId() != null) {				
				boolean contextOpened = false;
				Criteria c;
				try {
					contextOpened = DSApi.openContextIfNeeded();
					InOfficeDocument document = InOfficeDocument.findInOfficeDocument(getInDocumentId());
					Set<OutOfficeDocument> outDocuments = document.getOutDocuments();
					int sizeOfReplies = outDocuments.size();
												            
					Session session = DSApi.context().session();
			        c = session.createCriteria(pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot.class);
			        c.add(Restrictions.eq("documentId", inDocumentId));
			        c.add(Restrictions.eq("documentType", "in"));

			        JBPMTaskSnapshot jBPMTaskSnapshot = (JBPMTaskSnapshot)c.uniqueResult();
			        jBPMTaskSnapshot.setAnswerCounter(new Integer(sizeOfReplies));
			        session.saveOrUpdate(jBPMTaskSnapshot);	
					
				} catch (EdmException e) {
					LOG.error(
							"exception connected with open context for TaskSnapshot class {}",e);
				} finally {
					DSApi.closeContextIfNeeded(contextOpened);
					LOG.info("close context for " + TaskSnapshot.class
							+ " used on " + DwrDocumentMainTabAction.class);
				}
			}
		}
	}
	
	
	//ustawia pola wersjonowania przy tworzeniu dokumentu
	private void ustawWersje(Document document) {
			document.setCzyAktualny(true);
			document.setCzyCzystopis(false);
			document.setVersion(1);
			document.setVersionGuid(document.getId());
	}
	private void assignOfficeNumber(OutOfficeDocument document) throws EdmException
	{
		Date entryDate = new Date();
		Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), document.getId(), entryDate);
		document.bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
		
	}
	
	private class Update extends TransactionalActionListener
	{
		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception
		{
			dwrDocumentHelper.update(DwrDocumentMainTabAction.this);
		}

		@Override
		public Logger getLogger()
		{
			return log;
		}
	}

	/**
	 * Metoda dodaj�ca za��czniki do dokumentu, pobiera zalaczniki z listy
	 * multiFiles zawierajacej aktualnie wczytane zalaczniki oraz z tablicy
	 * returnLoadFiles zawierajace zalaczniki wczytane przed przeladowanie
	 * formularza (jesli takie nastapilo)
	 * 
	 * @param doc
	 * @throws AccessDeniedException
	 * @throws EdmException
	 */
	protected void addFiles(OfficeDocument doc) throws AccessDeniedException, EdmException
	{
		log.debug("addFiels: {} ", multiFiles.size());
		doc.setPermissionsOn(false);
		if (multiFiles != null && multiFiles.size() > 0)
		{
			log.info("mulitfiles: {}", multiFiles.size());
			for (FormFile formFile : multiFiles)
			{
				Attachment attachment = new Attachment(formFile.getNameWithoutExtension());
				doc.createAttachment(attachment);
				attachment.createRevision(formFile.getFile()).setOriginalFilename(formFile.getName());
			}
		}
		if (returnLoadFiles != null && returnLoadFiles.length > 0)
		{
			for (String fileKey : returnLoadFiles)
			{
				String path = fileKey.replace("\\", "/");
				File f = new File(path);
				Attachment attachment = new Attachment("Skan");
				doc.createAttachment(attachment);
				attachment.createRevision(f).setOriginalFilename(f.getName().replace(filestoken + "_", ""));
			}
		}
		
		LoggerFactory.getLogger("tomekl").debug("attId check");
		if(attId != null)
		{
			LoggerFactory.getLogger("tomekl").debug("attId != null");
			AttachmentRevision rev = AttachmentRevision.find(attId);
			rev.setLparam("DODANY");
			Attachment attachment = new Attachment("Skan");
			doc.createAttachment(attachment);
			attachment.createRevision(rev.getBinaryStream(), rev.getSize(), rev.getOriginalFilename());
		}
		
        if(AvailabilityManager.isAvailable("zalaczniki.odswiezajPoleLiczbaZalacznikowNaPismiePrzychodzacym")&&doc instanceof InOfficeDocument){
            ((InOfficeDocument)doc).setNumAttachments(doc.getAttachments().size());
        }
	}

	

	/**
	 * Metoda zapoisuje pobrane zalaczniki to katalogu tymczasowego i dodaje
	 * wpis do loadFiles Jest to robione na wypadek wystapienia bledu i powrotu
	 * jeszcze raz na formularz dodani dokumenu. Zalaczniki sa pamietane i nie
	 * trzeba ich jeszcze raz przesylac
	 */
	protected void saveFile()
	{
		try
		{
			if (filestoken == null || filestoken.length() < 1)
				filestoken = ServletActionContext.getRequest().getRequestedSessionId() + new Date().getTime();
			if (loadFiles == null)
				loadFiles = new HashMap<String, String>();
			if (multiFiles != null && multiFiles.size() > 0)
			{
				for (FormFile formFile : multiFiles)
				{
					File tmp = new File(Docusafe.getTemp(), filestoken + "_" + formFile.getFile().getName());
					FileOutputStream os = new FileOutputStream(tmp);
					FileInputStream stream = new FileInputStream(formFile.getFile());
					byte[] buf = new byte[2048];
					int count;
					while ((count = stream.read(buf)) > 0)
					{
						os.write(buf, 0, count);
					}
					os.close();
					stream.close();
					loadFiles.put(tmp.getAbsolutePath(), formFile.getFile().getName());
				}
			}
			if (returnLoadFiles != null && returnLoadFiles.length > 0)
			{
				for (String path : returnLoadFiles)
				{
					String[] tab = path.split("\\\\");
					String fileName = tab[tab.length - 1].replace(filestoken + "_", "");
					loadFiles.put(path, fileName);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			saveFile();
			addActionError("B��d zapisu plik�w " + e.getMessage());
		}
	}

    @Override
    public void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException
    {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public InOfficeDocumentKind getDefaultKind()
	{
		if (defaultKind == null)
			dwrDocumentHelper.initDefaultKind(this);
		return defaultKind;
	}

	public void setDefaultKind(InOfficeDocumentKind defaultKind)
	{
		DwrDocumentMainTabAction.defaultKind = defaultKind;
	}
	
	public boolean isCanChangeDockind() {
        DocumentKind dockind = getDocument().getDocumentKind();
        if(dockind != null && dockind.logic() != null) {
            try {
                return dockind.logic().canChangeDockind(getDocument());
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }

		return true;
	}
	public boolean isOutOfficeDocument()
	{
		return outOfficeDocument;
	}
	public void setOutOfficeDocument(boolean outOfficeDocument)
	{
		this.outOfficeDocument = outOfficeDocument;
	}
	public String getDocumentDate()
	{
		return documentDate;
	}

	public void setDocumentDate(String documentDate)
	{
		this.documentDate = documentDate;
	}

	public Long getJournalId()
	{
		return journalId;
	}

	public void setJournalId(Long journalId)
	{
		this.journalId = journalId;
	}

	public Date getEntryDate()
	{
		return entryDate;
	}

	public void setEntryDate(Date entryDate)
	{
		this.entryDate = entryDate;
	}

	public String getFastAssignmentSelectUser()
	{
		return fastAssignmentSelectUser;
	}

	public void setFastAssignmentSelectUser(String fastAssignmentSelectUser)
	{
		this.fastAssignmentSelectUser = fastAssignmentSelectUser;
	}

	public String getFastAssignmentSelectDivision()
	{
		return fastAssignmentSelectDivision;
	}

	public void setFastAssignmentSelectDivision(String fastAssignmentSelectDivision)
	{
		this.fastAssignmentSelectDivision = fastAssignmentSelectDivision;
	}

	public Map<String, String> getLoadFiles()
	{
		return loadFiles;
	}

	public void setLoadFiles(Map<String, String> loadFiles)
	{
		this.loadFiles = loadFiles;
	}

	public String[] getReturnLoadFiles()
	{
		return returnLoadFiles;
	}

	public void setReturnLoadFiles(String[] returnLoadFiles)
	{
		this.returnLoadFiles = returnLoadFiles;
	}

	public String getFilestoken()
	{
		return filestoken;
	}

	public void setFilestoken(String filestoken)
	{
		this.filestoken = filestoken;
	}

	public FormFile getMultiFiles()
	{
		if (this.multiFiles != null && this.multiFiles.size() > 0)
			return this.multiFiles.get(0);
		else
			return null;
	}

	public void setMultiFiles(FormFile file)
	{
		this.multiFiles.add(file);
	}

	public Map<String, String> getDocumentKinds()
	{
		
		return sortByComparator(this.documentKinds);
	}
	
	public String getEmpCardName()
	{
		log.error("fieldsmanager" + fm);
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getEmployeeCard().getExternalUser();
		
	}
	
	public List<FreeDay> getFreeDays()
	{
		return ((AbsenceLogic) fm.getDocumentKind().logic()).getFreeDays();
	}
	
	public int getCurrentYear()
	{
		return GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
	}
	
	public void setDocumentKinds(Map<String, String> documentKinds)
	{
		this.documentKinds = documentKinds;
	}

	public Map<String, Object> getDockindKeys()
	{
		return dockindKeys;
	}

	public void setDockindKeys(Map<String, Object> dockindKeys)
	{
		this.dockindKeys = dockindKeys;
	}

	public Long getInDocumentId()
	{
		return inDocumentId;
	}

	public void setInDocumentId(Long inDocumentId)
	{
		this.inDocumentId = inDocumentId;
	}

	public final String getDocumentKindCn()
	{
		return documentKindCn;
	}

	public final void setDocumentKindCn(String documentKindCn)
	{
		this.documentKindCn = documentKindCn;
	}

	public String getDependsDocsJson()
	{
		return dependsDocsJson;
	}

	public void setDependsDocsJson(String dependsDocsJson)
	{
		this.dependsDocsJson = dependsDocsJson;
	}
	
	public void setCaseId(Long caseId)
	{
		if(caseId != null)
		{
			log.info("ustawiam caseId w dwr:"+ caseId);
			this.caseId = caseId;
		}
		else
			this.caseId = -1L;
	}
	
	public void setCaseIdInProperty(Long caseId)
	{
		if(caseId != null && !caseId.toString().equals("-1"))
		{
			log.info("ustawiam properties dla caseId" + caseId);
			System.clearProperty("caseId_" + DSApi.context().getPrincipalName());
			System.setProperty("caseId_" + DSApi.context().getPrincipalName(), caseId.toString());
		}
	}
	
	
	public Long getCaseId()
	{
		return this.caseId;
	}

    public String getProp() {
        return prop;
    }

    private void setPropInProperty(String prop){
        if(StringUtils.isNotEmpty(prop)){
            log.info("ustawiam property {}", prop);
            System.clearProperty("property_" + DSApi.context().getPrincipalName());
            System.setProperty("property_" + DSApi.context().getPrincipalName(), prop.toString());
        }
    }

    public void setProp(String property) {
        this.prop = property;

    }
   

    private Map sortByComparator(Map unsortMap) {
		 
	        List list = new LinkedList(unsortMap.entrySet());
	 
	        //sort list based on comparator
	        Collections.sort(list, new Comparator() {
	             public int compare(Object o1, Object o2) {
		           return ((Comparable) ((Map.Entry) (o1)).getKey())
		           .compareTo(((Map.Entry) (o2)).getKey());
	             }
		});
	 
	        //put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
		     Map.Entry entry = (Map.Entry)it.next();
		     sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;

	   }

	public Long getAttId() {
		return attId;
	}

	public void setAttId(Long attId) {
		this.attId = attId;
	}	
	   

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public boolean isCreateNextDoc() {
		return createNextDoc;
	}

	public void setCreateNextDoc(boolean createNextDoc) {
		this.createNextDoc = createNextDoc;
	}

    public boolean isCreateNextDocForThisUser()
    {
        return createNextDocForThisUser;
    }

    public void setCreateNextDocForThisUser(boolean createNextDocForThisUser)
    {
        this.createNextDocForThisUser = createNextDocForThisUser;
    }

    public boolean isCurrentDayWarning()
    {
        return currentDayWarning;
    }
    public String getDocumentType()
	{
		return documentType;
	}

	
	public void setDocumentType(String documentType)
	{
		this.documentType = documentType;
	}

	
	public Long getCreatedDocumentId()
	{
		return createdDocumentId;
	}

	
	public void setCreatedDocumentId(Long createdDocumentId)
	{
		this.createdDocumentId = createdDocumentId;
	}
	public boolean isCanAssignOfficeNumber()
	{
		return canAssignOfficeNumber;
	}

	public void setCanAssignOfficeNumber(boolean canAssignOfficeNumber)
	{
		this.canAssignOfficeNumber = canAssignOfficeNumber;
	}
}

