package pl.compan.docusafe.web.office.common;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.FinishOrAssignState;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.PlannedAssignment;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
//import pl.compan.docusafe.util.HtmlTree; - old tree
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.tree.HtmlTree;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowTabAction.java,v 1.163 2010/05/24 11:21:07 pecet1 Exp $
 */

public abstract class WorkflowTabAction extends BaseTabsAction
{
	private static final Logger log = LoggerFactory.getLogger(WorkflowTabAction.class);

    private static StringManager sm
        = GlobalPreferences.loadPropertiesFile(WorkflowTabAction.class.getPackage().getName(),null);
   // private static StringManager smL;
    // @EXPORT
    private OfficeDocument document;
    private String objective;
    private String treeHtml;
    private List<Map<String, Object>> users;
    private DSDivision targetDivision;
    private String divisionPrettyPath;
    private List<Map> inputFields;
    private String activityName;
    private String activityDescription;
    private boolean requireAcknowledge;
    private boolean canReturnAssignment;
    private boolean allowAssignment;
    private boolean canFinish;
    private String sourceUser;
    private boolean hasManualPush;

	private List<DSUser> allUsers;
    /**
     * Lista obiekt�w {@link PlannedAssignment} u�ywana w JSP.
     */
    private List<PlannedAssignment> plannedAssignmentBeans;
    /**
     * Flaga okre�laj�ca, �e na li�cie przygotowanych dekretacji znajduje
     * si� ju� dekretacja "do realizacji", wi�c dodanie kolejnej tego typu
     * spowoduje usuni�cie poprzedniej.
     */
    private boolean hasExecutePlanned;
    private boolean canChangeDeadline;
    private FinishOrAssignState finishOrAssignState;

    // @EXPORT/@IMPORT
    private String divisionGuid;
    /**
     * Lista napis�w (kt�re mog� by� u�yte jako parametr id w konstruktorze
     * {@link PlannedAssignment} okre�laj�cych list� planowanych dekretacji.
     * Lista u�ywana do utworzenia listy p�l input type=hidden w JSP.
     */
    private String[] plannedAssignments;


    // @IMPORT
    private String newPlannedAssignment;
    /**
     * Lista identyfikator�w planowanych dekretacji odpowiadaj�cych
     * zaznaczonym checkboksom przy dekretacjach, u�ywana np. do usuwania
     * dekretacji z listy.
     */
    private String[] plannedAssignmentIds;
    /**
     * Jedna zaplanowana dekretacja - je�eli warto�� tego pola jest obecna,
     * klasa ManualPush ignoruje tablic� plannedAssignments.
     */
    private String onePlannedAssignment;
    private String deadlineTime;
    private String deadlineRemark;
    private Map<String,String> substituted;

    // rzeczy zwiazane z zewnetrznym procesem Workflow JBPM
    private Map<Long,String> jbpmProcesses;
    private Long jbpmProcessId;
    private String initJbpmLink;
    private boolean canInitJbpmProcess;    
    private boolean pushToCoor;
    private static final String EV_FILL = "fill";

    public String getBaseLink()
    {
        return "/office/incoming/workflow.action";
    }

    protected void setup()
    {
        // Return - akcja uniwersalna
        // ManualPush - akcja uniwersalna
        // ManualFinish - akcja uniwersalna
        // AcknowledgeCc - akcja uniwersalna
        // PlanAssignment - akcja lokalna
        // Delete - akcja lokalna

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReturn").
            append(OpenHibernateSession.INSTANCE).
            append(new Return()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualPush").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualPush()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doManualFinish").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualFinish()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAcknowledgeCc").
            append(OpenHibernateSession.INSTANCE).
            append(new ManualFinish()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPlanAssignment").
            append(OpenHibernateSession.INSTANCE).
            append(new PlanAssignment()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doWatch").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Watch()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doManualPushToCoor").
	        append(OpenHibernateSession.INSTANCE).
	        append(new ManualPushToCoor()).
	        append(EV_FILL, new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected abstract List prepareTabs();

    protected abstract String getExternalWorkflowBaseLink();

	public List<DSUser> getAllUsers() {
		return allUsers;
	}

	public void setAllUsers(List<DSUser> allUsers) {
		this.allUsers = allUsers;
	}

    @Unchecked
    /**Odes�anie zadania do koordynatora*/
    private class ManualPushToCoor implements ActionListener
    {
		public void actionPerformed(ActionEvent event) 
		{
			
			LoggerFactory.getLogger("tomekl").debug("ManualPushToCoor");
			try
			{
				DSApi.context().begin();
				
				WorkflowFactory.forwardToCoordinator(getActivity(), event);
				
				DSApi.context().commit();
				event.skip(EV_FILL);
				event.setResult("task-list");
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
				DSApi.context()._rollback();
				log.debug(e.getLocalizedMessage(),e);
			}
			
			/*try {
				document = OfficeDocument.findOfficeDocument(getDocumentId());
				WorkflowFactory.forwardToCoordinator(event, new String[] { getActivity()}, document);
				event.setResult("task-list");
			} catch (EdmException e) {
				event.addActionError(e.getMessage());
				log.error("", e);
			}*/
		}
    }
    
    /**
     * Odes�anie dekretacji do nadawcy.
     */
    private class Return implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
           
                WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());
                
                String sourceGuid = activity.getLastDivision();
                String sourceUser = activity.getLastUser();
                
                if (DSApi.context().getPrincipalName().equals(sourceUser))
                    throw new EdmException(sm.getString("NieMoznaOdeslacDekretacjiDoSamegoSiebie"));
                
                final boolean isCc = !WorkflowFactory.getInstance().isManual(activity);
                final Long documentId = activity.getDocumentId();
                final Document document = Document.find(documentId);
                
                if(isCc) {
                    if (documentId != null)
                    {
                        if (document instanceof OfficeDocument)
                        {
                            AssignmentHistoryEntry ahe = new AssignmentHistoryEntry(
                                    DSApi.context().getPrincipalName(),
                                    WorkflowFactory.getInstance().isManual(getActivity()) ? sm.getString("doRealizacji") : sm.getString("doWiadomosci"),
                                    WorkflowFactory.getInstance().isManual(getActivity()) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC);
                            ahe.setStatus("returned");

                            ((OfficeDocument) document).addAssignmentHistoryEntry(ahe);
                        }
                    }
                }
                
                WorkflowFactory.returnAssignment(activity, event);
                
                
                ((OfficeDocument) document).addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName()
                        ,DSApi.context().getPrincipalName(),sourceUser,sourceGuid,sm.getString("ZwrotDekretacji")
                        ,sm.getString("ZwrotDekretacji"),false,AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION ));
                       
                if (documentId != null)
                    TaskSnapshot.updateByDocumentId(documentId, OfficeDocument.findOfficeDocument(documentId).getStringType());

                DSApi.context().commit();
                event.setResult("task-list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Dodawanie opisu nowej dekretacji do listy przygotowanych
     * dekretacji.
     */
    private class PlanAssignment implements ActionListener
    {
        
        public void actionPerformed(ActionEvent event)
        {            
        	substituted = new LinkedHashMap<String, String>();
        	Date now = new Date();
            List<String> plan = new ArrayList<String>();
            if (plannedAssignments != null)
            {
                for (String pa : plannedAssignments)
                {
                    plan.add(pa);
                }
            }

            if (!StringUtils.isEmpty(newPlannedAssignment) && !plan.contains(newPlannedAssignment))
            {
                try
                {
                    PlannedAssignment npa = new PlannedAssignment(newPlannedAssignment);

                    // je�eli dodano dekretacj� "do wykonania", nale�y
                    // usun�� z listy wszystkie inne dekretacje tego typu
                    if (npa.getKind().equals(PlannedAssignment.KIND_EXECUTE))
                    {
                        for (Iterator iter=plan.iterator(); iter.hasNext(); )
                        {
                            PlannedAssignment pa = new PlannedAssignment((String) iter.next());
                            if (pa.getKind().equals(PlannedAssignment.KIND_EXECUTE))
                            {
                                iter.remove();
                                // na li�cie mo�e by� tylko jedna
                                // dekretacja "do realizacji"
                                if(pa.getUser() != null)
                                    addActionMessage(sm.getString("ZlistyDekretacjiUsunietoDekretacjeNaUzytkownikaPoniewazNaLiscieMozeZnalezcSieTylkoJednaDekretacja",
                                        pa.getUser().asFirstnameLastname(),(pa.getDivision() != null ? pa.getDivision().getPrettyPath() : ""),pa.getKindDescription()));
                                    else
                                        addActionMessage(sm.getString("ZlistyDekretacjiUsunietoDekretacjeNaDzialPoniewazNaLiscieMozeZnalezcSieTylkoJednaDekretacja",
                                            pa.getDivision().getPrettyPath())); 
                            }

                        }

                    }

                    plan.add(newPlannedAssignment);
                    for (Iterator iter=plan.iterator(); iter.hasNext(); )
                    {
                    	PlannedAssignment pa = new PlannedAssignment((String) iter.next());
                    	DSUser temp=pa.getUser();
                        if (temp != null)
                        {
                            if(temp.getSubstituteUser()!=null && temp.getSubstitutedFrom().before(now))
                    		    substituted.put(temp.asFirstnameLastname(),temp.getSubstituteUser().asFirstnameLastname());
                    	    else substituted.put(temp.asFirstnameLastname(),"");
                        }
                    }
                    plannedAssignments = (String[]) plan.toArray(new String[plan.size()]);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    /**
     * Usuwanie zaplanowanych dekretacji.
     */
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	substituted = new LinkedHashMap<String, String>();
        	Date now = new Date();
            if (plannedAssignmentIds != null && plannedAssignmentIds.length > 0)
            {
                List<String> plan;
                if (plannedAssignments != null)
                {
                    plan = new ArrayList<String>(Arrays.asList(plannedAssignments));
                }
                else
                {
                    plan = Collections.emptyList();
                }

                plan.removeAll(Arrays.asList(plannedAssignmentIds));
                try{
                	plannedAssignments = plan.toArray(new String[plan.size()]);
                	for (Iterator iter=plan.iterator(); iter.hasNext(); )
                	{
                		PlannedAssignment pa = new PlannedAssignment((String) iter.next());
                		DSUser temp = pa.getUser();
                		if(temp != null)
                		{
	                		if(temp.getSubstituteUser() != null && temp.getSubstitutedFrom() != null && temp.getSubstitutedFrom().before(now))
	                			substituted.put(temp.asFirstnameLastname(),temp.getSubstituteUser().asFirstnameLastname());
	                		else substituted.put(temp.asFirstnameLastname(),"");
                		}
                	}
                }
                catch(EdmException e)
                {
                	e.printStackTrace();
                }
            }
            else
            {
                addActionError(sm.getString("NieZaznaczonoDekretacjiDoUsuniecia"));
            }
        }
    }

    

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());
				allUsers = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                document = OfficeDocument.findOfficeDocument(getDocumentId());

                if(document == null || document.getDocumentKind() == null)
				{
					pushToCoor = false;
				}
                else
                {
                	//zamienilem metody
                	pushToCoor = document.getDocumentKind().logic().isProcessCoordinator(document);                	
                }
                
                // inicjalizacja na potrzeaby zewnetrzengo workflow JBPM
                canInitJbpmProcess = false; 
                if (plannedAssignments != null)
                {
                    // tworz� list�, aby usuwa� elementy powoduj�ce problemy
                    plannedAssignmentBeans = new ArrayList<PlannedAssignment>(plannedAssignments.length);
                    List<String> plan = new ArrayList<String>(Arrays.asList(plannedAssignments));

                    for (Iterator iter=plan.iterator(); iter.hasNext(); )
                    {
                        String id = ((String) iter.next()).trim();
                        try
                        {
                            PlannedAssignment pa = new PlannedAssignment(id);
                            plannedAssignmentBeans.add(pa);

                            if (PlannedAssignment.KIND_EXECUTE.equals(pa.getKind()))
                                hasExecutePlanned = true;
                        } catch (EdmHibernateException e) {
                            addActionError(e.getMessage());
                            log.error(e.getMessage(), e);
                        } catch (EdmException e) {
                            addActionError(e.getMessage());
                            log.debug(e.getMessage(), e);
                        }
                    }

                    plannedAssignments = plan.toArray(new String[plan.size()]);
                }

                WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());
                //canReturnAssignment = WorkflowUtils.isManual(activity);
                canReturnAssignment = true;
                
                canFinish = (document.getType() != DocumentType.ORDER) || (DSApi.context().getPrincipalName().equals(document.getCreatingUser()));
                finishOrAssignState = WorkflowFactory.getFinishOrAssignState(document, activity);
                
                //hot fix - mechanizm drzewa uzywa atrybutu canAssign do sprawdzenia czy wyswietli sie opcja recznej rekretacji zadania,
                // jednak w przypadku dekretowalnych zadan CC ta opcja nie powinna sie pojawiac 
                hasManualPush = WorkflowFactory.getInstance().isManual(activity);
                
                finishOrAssignState = new FinishOrAssignState(
                		finishOrAssignState.isCanFinish(),
                		hasManualPush,
                		finishOrAssignState.getFinishWarnings(),
                		finishOrAssignState.getAssignWarnings(),
                		finishOrAssignState.getCantFinishReasons(),
                		finishOrAssignState.getCantAssignReasons());
                
                //czy pozwolic na dekretacj�
                allowAssignment = WorkflowFactory.getInstance().allowAssignment(activity);
                requireAcknowledge = !WorkflowFactory.getInstance().isManual(activity);
                objective = activity.getObjective();
                
                if(AvailabilityManager.isAvailable("blockAtributesIfCC") && !hasManualPush)
                {
                	canReturnAssignment = false;
                }
                
                String sourceUsername = activity.getLastUser();

                if (sourceUsername != null)
                {
                    sourceUser = DSUser.safeToFirstnameLastname(sourceUsername);
                }

                // drzewo struktury organizacyjnej
                if (!StringUtils.isEmpty(divisionGuid))
                {
                    try
                    {
                        targetDivision = DSDivision.find(divisionGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                // przekierowanie do dzia�u, w kt�rym jest u�ytkownik
                if (targetDivision == null)
                {
                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                        if (!division.isGroup() && !division.isSwimlane())
                        {
                            targetDivision = division;
                            break;
                        }
                    }
                }

                // domy�lnie - dzia� g��wny
                if (targetDivision == null)
                {
                    targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }
                
                final OrganizationUrlProvider provider = new OrganizationUrlProvider() {

					public String getUrl(DSDivision div, DSUser user) {
						try {
							StringBuilder url = new StringBuilder(
									ServletActionContext.getRequest()
											.getContextPath()
											+ getBaseLink()
											+ "?documentId=" + getDocumentId()
											+ "&activity=" + getActivity()
											+ "&divisionGuid=" + (div).getGuid());
							if (plannedAssignments != null) {
								for (int i = 0; i < plannedAssignments.length; i++) {
									url.append("&plannedAssignments="
											+ URLEncoder.encode(
													plannedAssignments[i],
													"iso-8859-1"));
								}
							}
							return url.toString();
						} catch (Exception e) {
                            addActionError(e.getMessage());
							log.error(e.getMessage(), e);
							return null;
						}
					}
				};

				final HtmlTree tree = ExtendedOrganizationTree.
					createAssignmentTree(targetDivision,
							null, 
							hasExecutePlanned,
							finishOrAssignState,
							provider, 
							ServletActionContext.getRequest().getContextPath());

                treeHtml = tree.generateTree();   
				
                // lista u�ytkownik�w w dziale
                if (targetDivision != null && !targetDivision.isRoot())
                {
                    // final User[] divisionUsers =
					// DSApi.context().getOrganizationHelper().getUsersInDivision(divisionGuid);
                    final DSUser[] divisionUsers = targetDivision.getUsers();

                    users = new ArrayList<Map<String, Object>>(divisionUsers.length);

                    for (int i=0, n=divisionUsers.length; i < n; i++)
                    {
                        if (divisionUsers[i].isDeleted())
                            continue;

                        Map<String, Object> bean = new HashMap<String, Object>();

                        bean.put("description", divisionUsers[i].asFirstnameLastname());
                        bean.put("name", divisionUsers[i].getName());
                        if (DSApi.context().hasPermission(DSPermission.PODGLAD_LICZBY_ZADAN_WSZEDZIE) ||
                            (DSApi.context().hasPermission(DSPermission.PODGLAD_LICZBY_ZADAN_KOMORKA) &&
                             DSApi.context().getDSUser().sharesDivision(divisionUsers[i])))
                        {
                            int[] taskCount =
                                ((TaskList) ServiceManager.getService(TaskList.NAME)).
                                    getTaskCount(divisionUsers[i]);
                            bean.put("workingTasks", new Integer(taskCount[0]));
                            bean.put("awaitingTasks", new Integer(taskCount[1]));
                            DSApi.context().session().flush();
                            bean.put("assignedCases", new Integer(OfficeCase.countUserCases(divisionUsers[i].getName(), true)));
                            DSApi.context().session().flush();

                            bean.put("canShowTasks", Boolean.TRUE);
                        }

                        users.add(bean);
                    }

                    divisionPrettyPath = targetDivision.getPrettyPath();
                }

                // deadline
                Date deadlineDate = activity.getDeadlineTime();
                if (deadlineDate != null) {
                	deadlineTime = DateUtils.formatJsDate(activity.getDeadlineTime());
                }
                deadlineRemark = activity.getDeadlineRemark();
                String deadlineAuthor = activity.getDeadlineAuthor();
                canChangeDeadline = DSApi.context().getPrincipalName().equals(deadlineAuthor);
                	
            } catch (EdmHibernateException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            } catch (EdmException e) {
                addActionError(e.getMessage());
                log.debug(e.getMessage(), e);
            }
        }
    }

    private class ManualPush implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
        	if ((plannedAssignments == null || plannedAssignments.length == 0) &&
                StringUtils.isEmpty(onePlannedAssignment))
                addActionError(sm.getString("NieZaplanowanoZadnejDekretacji"));

            Date deadline = DateUtils.nullSafeParseJsDate(deadlineTime);

            if (deadline != null)
            {
                deadline = DateUtils.midnight(deadline, 1);
                if (deadline.before(DateUtils.midnight(new Date(), 0)))
                    addActionError(sm.getString("PodanoNieprawidlowaDateKoncaPracyDzienWprzeszlosci)"));
            }

            if (deadline != null && deadlineRemark == null)
                addActionError(sm.getString("NiePodanoPowoduWyznaczeniaTerminuZakonczeniaPracy"));

            if (hasActionErrors())
                return;

            
            try
            {
                DSApi.context().begin();
                
                document = OfficeDocument.findOfficeDocument(getDocumentId());
                
                
                if (WorkflowFactory.addDeadLine(getActivity(), DSApi.context().getPrincipalName(), deadline, deadlineRemark)) 
                {
                    document.addWorkHistoryEntry(Audit.create("assignment", DSApi.context().getPrincipalName(),
                        sm.getString("WyznaczonoTerminRealizacjiZadaniaNa",DateUtils.formatCommonDate(deadline)),
                        null, null));
                }
                
                String[] activityIds = new String[] { getActivity() };
                
                WorkflowFactory.multiAssignment(
                		activityIds , 
                		DSApi.context().getPrincipalName(), 
                		plannedAssignments, 
                		onePlannedAssignment, 
                		event);
                
                TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());
                
                DSApi.context().commit();

                //addActionMessage("Pismo zosta�o zadekretowane");
                event.setResult("confirm");
                event.skip(EV_FILL);

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

    }

    private class ManualFinish implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                
                //KAWALEK DO UPORZADKOWANIA
                // --- WPIS DO HISTORII - tomek uporzadkuj to
                WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());
                if (document instanceof OfficeDocument)
                {
                    ((OfficeDocument) document).addAssignmentHistoryEntry(
                        new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), activity.getProcessDesc(),
                            WorkflowFactory.getInstance().isManual(activity) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC));

                    ((OfficeDocument) document).setCurrentAssignmentGuid(null);
                    ((OfficeDocument) document).setCurrentAssignmentUsername(null);
                    ((OfficeDocument) document).setCurrentAssignmentAccepted(null);
                }
                // -------------- EOT
                
                boolean finish;
                if (StringUtils.isNotEmpty(getActivity())) {
                	finish = !WorkflowFactory.getInstance().isManual(getActivity());
                } else {
                	finish = false;
                }
                                
                WorkflowFactory.getInstance().manualFinish(getActivity(), !finish);

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

                TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());

                DSApi.context().commit();

                if (document.getType() == DocumentType.INCOMING)
                {
                    event.setResult("task-list");
                }
                else if (document.getType() == DocumentType.OUTGOING)
                {
                    event.setResult("task-list-out");
                }
                else if (document.getType() == DocumentType.ORDER) {
                    event.setResult("task-list-order");
                }
                else
                {
                    event.setResult("task-list-int");
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Watch implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
            {
                DSApi.context().begin();
                WorkflowActivity activity = getActivity() != null
                    ? WorkflowFactory.getWfActivity(getActivity())
                    : null;
                setExternalWorkflow(false);
                if (getActivity() != null)
                {
                    DSApi.context().watch(URN.create(activity));
                }
                else
                {
                    DSApi.context().watch(URN.create(Document.find(getDocumentId())));
                }
                DSApi.context().commit();

                addActionMessage(sm.getString("DodanoDoObserwowanych"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
				log.error(e.getMessage(), e);
                try {DSApi.context().rollback(); } catch (EdmException f) {}
            }
    	}
    }
    
    /** FIXME jeszcze nie dzia�a dla jbpm */
    private class AcknowledgeCc implements ActionListener
    {
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                DSApi.context().begin();

/*
                if (!DSApi.context().hasPermission(Permissions.ZADANIE_KONCZENIE))
                    throw new EdmException("Brak uprawnienia do ko�czenia procesu workflow");
*/

                WorkflowActivity activity = WorkflowFactory.getWfActivity(getActivity());

                // id procesu dekretacji r�cznej
                final boolean isCc = !WorkflowFactory.getInstance().isManual(activity);
                    

                if (!isCc)
                    throw new EdmException(sm.getString("WymaganyJestProcesDoWiadomosci"));

                // odczytanie identyfikatora dokumentu zwi�zanego z zadaniem,
                final Long documentId = activity.getDocumentId();
                   
                Document document = null;

                if (documentId != null)
                {
                    document = Document.find(documentId);
                    
                    if (document instanceof OfficeDocument)
                    {
/*
                        NameValue[] nv = activity.container().process_context();

                        NameValue sourceUser = WorkflowUtils.findParameter(nv, "ds_source_user");
                        NameValue sourceGuid = WorkflowUtils.findParameter(nv, "ds_source_division");
                        NameValue targetGuid = WorkflowUtils.findParameter(nv, "ds_assigned_division");
                        //NameValue objective = WorkflowUtils.findParameter(nv, "ds_objective");

                        String sSourceUser = sourceUser != null && sourceUser.value() != null && sourceUser.value().value() != null ? (String) sourceUser.value().value() : null;
                        String sSourceGuid = sourceGuid != null && sourceGuid.value() != null && sourceGuid.value().value() != null ? (String) sourceGuid.value().value() : null;
                        String sTargetGuid = targetGuid != null && targetGuid.value() != null && targetGuid.value().value() != null ? (String) targetGuid.value().value() : null;

                        ((OfficeDocument) document).getAssignmentHistory().
                            add(new AssignmentHistoryEntry(
                                sSourceUser != null ? sSourceUser : DSApi.context().getDSUser().getName(),
                                sSourceGuid != null ? sSourceGuid : DSDivision.ROOT_GUID,
                                DSApi.context().getDSUser().getName(),
                                sTargetGuid != null ? sTargetGuid : DSDivision.ROOT_GUID,
                                "", false));
*/
                        ((OfficeDocument) document).addAssignmentHistoryEntry(
                            new AssignmentHistoryEntry(
                                DSApi.context().getPrincipalName(),
                                WorkflowFactory.getInstance().isManual(getActivity()) ? sm.getString("doRealizacji") : sm.getString("doWiadomosci"),
                                WorkflowFactory.getInstance().isManual(getActivity()) ? AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION : AssignmentHistoryEntry.PROCESS_TYPE_CC)
                        );

                    }
                }

                activity.getActivity().complete();

                try
                {
                    DSApi.context().session().flush();
                }
                catch(HibernateException e)
                {
                    throw new EdmException(e);
                }

                if (document != null)
                    TaskSnapshot.updateByDocumentId(document.getId(), document.getStringType());

                DSApi.context().commit();

                //event.setResult("confirm");

                if (document.getType() == DocumentType.INCOMING)
                {
                    event.setResult("task-list");
                }
                else if (document.getType() == DocumentType.OUTGOING)
                {
                    event.setResult("task-list-out");
                }
                else if (document.getType() == DocumentType.ORDER) {
                    event.setResult("task-list-order");
                }
                else
                {
                    event.setResult("task-list-int");
                }

                event.skip(EV_FILL);
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getObjective()
    {
        return objective;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public DSDivision getTargetDivision()
    {
        return targetDivision;
    }

    public String getDivisionPrettyPath()
    {
        return divisionPrettyPath;
    }

    public List getInputFields()
    {
        return inputFields;
    }

    public String getActivityName()
    {
        return activityName;
    }

    public String getActivityDescription()
    {
        return activityDescription;
    }

    public List getUsers()
    {
        return users;
    }

    public boolean isAllowAssignment()
    {
        return allowAssignment;
    }

    public String getSourceUser()
    {
        return sourceUser;
    }

    public String[] getPlannedAssignments()
    {
        return plannedAssignments;
    }

    public void setPlannedAssignments(String[] plannedAssignments)
    {
        this.plannedAssignments = plannedAssignments;
    }

    public void setNewPlannedAssignment(String newPlannedAssignment)
    {
        this.newPlannedAssignment = newPlannedAssignment;
    }

    public List getPlannedAssignmentBeans()
    {
        return plannedAssignmentBeans;
    }

    public void setPlannedAssignmentIds(String[] plannedAssignmentIds)
    {
        this.plannedAssignmentIds = plannedAssignmentIds;
    }

    public boolean isRequireAcknowledge()
    {
        return requireAcknowledge;
    }

    public boolean isHasExecutePlanned()
    {
        return hasExecutePlanned;
    }

    public OfficeDocument getDocument()
    {
        return document;
    }

    public String getOnePlannedAssignment()
    {
        return onePlannedAssignment;
    }

    public void setOnePlannedAssignment(String onePlannedAssignment)
    {
        this.onePlannedAssignment = onePlannedAssignment;
    }

    public boolean isCanReturnAssignment()
    {
        return canReturnAssignment;
    }

    public boolean isCanFinish()
    {
        return canFinish;
    }

    public boolean isCanChangeDeadline()
    {
        return canChangeDeadline;
    }

    public String getDeadlineTime()
    {
        return deadlineTime;
    }

    public void setDeadlineTime(String deadlineTime)
    {
        this.deadlineTime = deadlineTime;
    }

    public String getDeadlineRemark()
    {
        return deadlineRemark;
    }

    public void setDeadlineRemark(String deadlineRemark)
    {
        this.deadlineRemark = deadlineRemark;
    }

    public FinishOrAssignState getFinishOrAssignState()
    {
        return finishOrAssignState;
    }
    public Map getSubstituted()
    {
    	return substituted;
    }

    public Map<Long, String> getJbpmProcesses()
    {
        return jbpmProcesses;
    }

    public Long getJbpmProcessId()
    {
        return jbpmProcessId;
    }

    public String getInitJbpmLink()
    {
        return initJbpmLink;
    }

    public boolean isCanInitJbpmProcess()
    {
        return canInitJbpmProcess;
    }

	public boolean isPushToCoor() {
		return pushToCoor;
	}

	public void setPushToCoor(boolean pushToCoor) {
		this.pushToCoor = pushToCoor;
	}

	public void setHasManualPush(boolean hasManualPush) {
		this.hasManualPush = hasManualPush;
	}

	public boolean isHasManualPush() {
		return hasManualPush;
	}
}
