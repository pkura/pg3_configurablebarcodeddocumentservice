package pl.compan.docusafe.web.office.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.api.user.office.WorkHistoryBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;

import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkHistoryTabAction.java,v 1.11 2009/03/05 08:14:53 mariuszk Exp $
 */
public abstract class WorkHistoryTabAction extends BaseTabsAction
{
    private static final Logger log = LoggerFactory.getLogger(WorkHistoryTabAction.class);
    private Collection<WorkHistoryBean> workHistory;
    protected abstract List prepareTabs();
    private boolean showAll;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());
                Document doc = Document.find(getDocumentId());
                List<Audit>  listAudit = new ArrayList<Audit>();
                if(doc instanceof InOfficeDocument && ((InOfficeDocument) doc).getMasterDocumentId()!=null){
               
                	listAudit.addAll(((InOfficeDocument )doc ).getMasterDocument().getWorkHistory());
                }
                else if (doc instanceof OutOfficeDocument && ((OutOfficeDocument) doc).getMasterDocument()!=null){
                	
                	listAudit.addAll(((OutOfficeDocument )doc ).getMasterDocument().getWorkHistory());
                }
                listAudit.addAll(doc.getWorkHistory());
  
                workHistory = fun.reverse(fun.map(listAudit,
                    new lambda<Audit, WorkHistoryBean>()
                    {
                        public WorkHistoryBean act(Audit audit)
                        {
                            try
                            {
                                if (!showAll && audit.getPriority() != null && audit.getPriority() > Audit.HIGHEST_PRIORITY)
                                    throw new NoSuchElementException();
                                return new WorkHistoryBean(audit);
                            }
                            catch (EdmException e)
                            {
                                throw new NoSuchElementException();
                            }
                        }
                    }));
                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public Collection<WorkHistoryBean> getWorkHistory()
    {
        return workHistory;
    }

    public void setShowAll(boolean showAll)
    {
        this.showAll = showAll;
    }

    public boolean isShowAll()
    {
        return showAll;
    }
}
