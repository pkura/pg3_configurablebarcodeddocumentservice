package pl.compan.docusafe.web.office.common;

import com.opensymphony.webwork.ServletActionContext;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.bpmn.diagram.ProcessDiagramGenerator;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.util.FileCopyUtils;
import pl.compan.docusafe.activiti.ActivitiDiagramRenderer;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.process.DocuSafeProcessLogic;
import pl.compan.docusafe.rest.exceptions.NotFoundException;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: Tomasz
 * Date: 27.05.14
 * Time: 22:53
 */
public abstract class ProcessDiagramTabAction extends BaseTabsAction {

    private Set<Diagram> diagrams;

    protected abstract List prepareTabs();

    @Override
    public String getDocumentType() {
        return null;
    }

    @Override
    protected void setup() {
        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(new FillForm())
            .appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Diagram {
        private String imgLink;
        private String name;
        private String info;

        public Diagram() {}

        public Diagram(String name, String imgLink, String info) {
            this.imgLink = imgLink;
            this.name = name;
            this.info = info;
        }

        public String getImgLink() {
            return imgLink;
        }

        public String getName() {
            return name;
        }

        public String getInfo() {
            return info;
        }

    }

    private class FillForm implements ActionListener {

        private byte[] renderForInstance(RuntimeService runtimeService, HistoricProcessInstance hpi) throws EdmException {

            List<String> activitiIds = new ArrayList<String>();

            try {
                activitiIds.addAll(runtimeService.getActiveActivityIds(hpi.getId()));
            } catch(Exception e) {
                    throw new EdmException("Proces zakończony");
            }


            List<HistoricTaskInstance> history = ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricTaskInstanceQuery().
                    finished().processInstanceId(hpi.getId()).
                    list();
            for(HistoricTaskInstance hti : history) {
                activitiIds.add(hti.getTaskDefinitionKey());
            }
            BpmnModel model = ProcessEngines.getDefaultProcessEngine().getRepositoryService().getBpmnModel(hpi.getProcessDefinitionId());

            InputStream is = ProcessDiagramGenerator.generateDiagram(model, "png", activitiIds);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            try {
                FileCopyUtils.copy(is, bos);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return bos.toByteArray();
        }

        public void actionPerformed(ActionEvent event) {
            setTabs(prepareTabs());

            ActivitiDiagramRenderer renderer = Docusafe.getBean(ActivitiDiagramRenderer.class);
            RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
            RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
            List<HistoricProcessInstance> cpis = ProcessEngines.getDefaultProcessEngine()
                    .getHistoryService()
                    .createHistoricProcessInstanceQuery()
                    .variableValueEquals(DocuSafeProcessLogic.DOCUMENT_ID_PARAM_OTHER,documentId)
                    .list();

            diagrams = new HashSet<Diagram>();
            try {
                String imgLoc;
                for(HistoricProcessInstance hpi : cpis) {
                    File tmpFile = File.createTempFile("diagram",".png", new File(Docusafe.getHome().getAbsolutePath()+"//"+Docusafe.TEMP_FOLDER_NAME_FOR_PNG_FILES));
                    FileOutputStream fos = new FileOutputStream(tmpFile);

                    fos.write(renderForInstance(runtimeService, hpi));
                    imgLoc = ServletActionContext.getRequest().getContextPath().concat("/".concat(Docusafe.TEMP_FOLDER_NAME_FOR_PNG_FILES.concat("/").concat(tmpFile.getName())));
                    String name = repositoryService.createProcessDefinitionQuery().processDefinitionId(hpi.getProcessDefinitionId()).singleResult().getName();
                    diagrams.add(new Diagram(name, imgLoc, hpi.getEndTime() != null ? "archiwalny" : "aktywny"));

                    fos.close();
                }
            } catch (Exception e) {
                addActionError(e.getMessage());
                LOG.debug(e.getMessage(),e);
            }
        }
    }

    public Set<Diagram> getDiagrams() {
        return diagrams;
    }

    public boolean hasDiagram() { return !diagrams.isEmpty(); }
}
