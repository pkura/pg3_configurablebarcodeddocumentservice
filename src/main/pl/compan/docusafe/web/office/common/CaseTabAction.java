package pl.compan.docusafe.web.office.common;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import pl.compan.docusafe.api.user.office.OfficeCaseService;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.core.regtype.RegistryEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.spring.user.office.OfficeCaseServiceImpl;
import pl.compan.docusafe.core.xes.XesLogVerification;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CaseTabAction.java,v 1.99 2010/08/17 10:56:54 mariuszk Exp $
 */
public abstract class CaseTabAction extends BaseTabsAction
{
	private static Logger log = LoggerFactory.getLogger(CaseTabAction.class);
    // @EXPORT
    protected OfficeDocument document;
    protected boolean canCreateCase;
    protected List priorities;
    protected List<DSUser> clerks;
    protected boolean canChooseClerk;
    protected boolean canSetArbitraryCaseId;
    protected String documentIncomingDate;
    protected String caseLink;
    /**
     * Je�eli true, pismo musi by� najpierw przyj�te w jednym z dzia��w.
     */
    protected Map incomingDivisions;
    protected boolean intoJournalPermission;
    private boolean internal;
    private Map substitutedUsers;
    private GrantRequest grantRequest;
    private Long grantRequestId;
    private boolean canCreateGrantRequest;
    private String targetDivision;   // wykorzystywane podczas wybierania teczki (pick-portfolio.action)
    
    private StringManager sm = GlobalPreferences.loadPropertiesFile(CaseTabAction.class.getPackage().getName(),null);
    
//    static
//    {
//        for (int i=0; i < Case.PRIORITIES.length; i++)
//        {
//            priorities.put(Case.PRIORITIES[i], Case.getPriorityDescription(Case.PRIORITIES[i]));
//        }
//    }

    // @IMPORT
    private Long caseId;
    private int record;
    private String caseRwa;

    // @EXPORT/@IMPORT
    private Integer priority;
    protected String caseFinishDate;
    //private Integer rwaCategoryId;
    private String prettyRwaCategory;
    private String portfolioTxt;
    private Long portfolioId;
    protected String clerk;
    private String arbitraryCaseId;
    private boolean confirmCreate;

    private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;

    private String suggestedOfficeId;
    private Integer customSequenceId;
    private Integer suggestedSequenceId;
    private List<AssociativeDocumentStore.AssociativeDocumentBean> associatedDocumentsIn;
    private List<AssociativeDocumentStore.AssociativeDocumentBean> associatedDocumentsOut;

    private Boolean customOfficeId;
    private boolean canView;
    private boolean caseReopen;
    private boolean blocked;
    
    private String caseTitle;

    private Map<String, String> inRecords;
    private Map<RegistryBean, String> availableRecords;
    private String description2;
    private String freeCaseNumbers = null;
    private String freeCaseNumbersText = null;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDefault").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(new XesLogVerification()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doFullCreate").
                append(OpenHibernateSession.INSTANCE).
                append(new PreCreate()).
                append(new Create()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddToCase").
            append(OpenHibernateSession.INSTANCE).
            append(new AddToCase()).
            append(new XesLogVerification()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddToNextCase").
	        append(OpenHibernateSession.INSTANCE).
	        append(new AddToNextCase()).
	        append(new XesLogVerification()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRemoveFromCase").
	        append(OpenHibernateSession.INSTANCE).
	        append(new RemoveFromCase()).
	        append(new XesLogVerification()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

/*
        registerListener("doCreateGrantRequest").
            append(OpenHibernateSession.INSTANCE).
            append(new CreateGrantRequest()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
*/

/*
        registerListener("doIntoJournal").
            append(OpenHibernateSession.INSTANCE).
            append(new IntoJournal()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
*/
    }

    protected abstract List prepareTabs();

    /**
     * Metoda wywo�ywana po utworzeniu sprawy. Powinna doda�
     * dokument do sprawy.
     * @return true, je�eli sklonowano dokument
     */
    protected abstract boolean setCase(OfficeDocument document, OfficeCase c)
        throws EdmException;
    
    private class RegistryBean
    {
        private String url;
        private String requiredRwa;
        
        public RegistryBean(String url, String requiredRwa)
        {
            this.url = url;
            this.requiredRwa = (requiredRwa != null) ? requiredRwa : "";
        }
        
        public void setUrl(String url)
        {
            this.url = url;
        }
        
        public String getUrl()
        {
            return url;
        }

        public void setRequiredRwa(String requiredRwa)
        {
            this.requiredRwa = requiredRwa;
        }

        public String getRequiredRwa()
        {
            return requiredRwa;
        }
        
        public String toString()
        {
            return url + ";" + requiredRwa;
        }
    }    
    
    @SuppressWarnings("unchecked")
	protected void fillForm() throws EdmException
    {
        document = OfficeDocument.findOfficeDocument(getDocumentId());
            associatedDocumentsOut = new ArrayList<AssociativeDocumentStore.AssociativeDocumentBean>();
            associatedDocumentsIn  = new ArrayList<AssociativeDocumentStore.AssociativeDocumentBean>();
            if(getDocumentType().equals(InOfficeDocument.TYPE)) {
                OfficeDocument master = ((InOfficeDocument)document).getMasterDocument();
                if(master != null) {
                    associatedDocumentsIn.addAll(AssociativeDocumentStore.getAssociatedDocumentsIn(master.getId()));
                    associatedDocumentsIn.add(AssociativeDocumentStore.fromOfficeDocument(master));
                } else {
                    associatedDocumentsIn.addAll(AssociativeDocumentStore.getAssociatedDocumentsIn(getDocumentId()));
                }
            } else {
                OfficeDocument master = ((OutOfficeDocument)document).getMasterDocument();
                if(master != null) {
                    associatedDocumentsOut.addAll(AssociativeDocumentStore.getAssociatedDocumentsOut(master.getId()));
                    associatedDocumentsOut.add(AssociativeDocumentStore.fromOfficeDocument(master));
                } else {
                    associatedDocumentsOut.addAll(AssociativeDocumentStore.getAssociatedDocumentsOut(getDocumentId()));
                }
            }
            internal = document instanceof OutOfficeDocument && ((OutOfficeDocument) document).isInternal();

            if (document.getType() == DocumentType.INCOMING) {
                Date date = ((InOfficeDocument) document).getIncomingDate();
                if (date != null)
                    documentIncomingDate = DateUtils.formatJsDate(date);
            }else if(documentIncomingDate==null){
                documentIncomingDate=DateUtils.jsDateFormat.format(new Date());
            }
            if(document.getContainingCase()!=null)
                caseLink = "/office/edit-case.do?id="+document.getContainingCase().getId()+"&tabId=summary";
            // lazy initialization
            if (document.getContainingCase() != null)
            {
                DSApi.initializeProxy(document.getContainingCase());
                DSApi.initializeProxy(document.getContainingCase().getPriority());

                caseRwa = document.getContainingCase().getRwa().getRwa();
            }

            inRecords = new LinkedHashMap<String, String>();
            availableRecords = new LinkedHashMap<RegistryBean, String>();

            // rejestry "r�cznie tworzone" (takie jak Architektura i Stypendia)
            if (document.getContainingCase() != null && Docusafe.moduleAvailable(Modules.MODULE_REGISTRIES))
            {
                if (Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
                {
                    if (document.getContainingCase().isInRegistry(Records.GRANTS))
                        inRecords.put("/record/grants/grant-request.action?caseId="+document.getContainingCase().getId(), "Stypendia");
                    else
                        availableRecords.put(new RegistryBean(ServletActionContext.getRequest().getContextPath()+
                                "/record/grants/create-grant-request.action", ""),
                                "Stypendia");
                }

                if (Docusafe.moduleAvailable(Modules.MODULE_CONSTRUCTION) && DSApi.context().hasPermission(DSPermission.CNS_DOSTEP_OGOLNY))
                {
                    if (document.getContainingCase().isInRegistry(Records.CONSTRUCTION))
                        inRecords.put("/record/construction/construction-application.action?caseId="+document.getContainingCase().getId(), "Wnioski o pozwolenie na budow?");
                    else {
                        availableRecords.put(new RegistryBean(ServletActionContext.getRequest().getContextPath()+
                                "/record/construction/create-construction-application.action", "7351"),
                                "Wnioski o pozwolenie na budow�");
                    }
                }
            }

            // rejestry tworzone automatycznie z plik�w XML
            if (Configuration.additionAvailable(Configuration.ADDITION_REGISTRIES))
            {
                List<Registry> registries = Registry.list(true);
                for (Registry registry : registries)
                {
                    RegistryEntry regEntry = null;
                    if (Registry.CASE.equals(registry.getRegType()))
                    {
                        if (document.getContainingCase() == null)
                            continue;
                        Set<RegistryEntry> entries = document.getContainingCase().getRegistryEntries();
                        for (RegistryEntry entry : entries)
                            if (registry.equals(entry.getRegistry()))
                            {
                                regEntry = entry;
                                break;
                            }
                    }
                    else if (Registry.INDOCUMENT.equals(registry.getRegType()))
                    {
                        regEntry = RegistryEntry.find(registry.getId(), document.getId());
                    }
                    else
                    {
                        // jak rejestr bez powi�zania ze spraw�/pismem to nie mo�na doda� wpisu z zak�adki pisma "Sprawa"
                        continue;
                    }

                    if (regEntry != null)
                        inRecords.put("/office/registry/entry-main.action?entryId="+regEntry.getId(), registry.getName());
                    else
                        availableRecords.put(new RegistryBean(ServletActionContext.getRequest().getContextPath()+
                                "/office/registry/new-entry.action?registryId="+registry.getId()
                                //  +(Registry.INDOCUMENT.equals(registry.getRegType()) ? "&inDocumentId="+document.getId() : "")
                                , null), registry.getName());
                }
            }


            canCreateCase =  DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE);

            canSetArbitraryCaseId = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_DOWOLNY_SYMBOL);

            if(AvailabilityManager.isAvailable("INS.wlacz.wyczyscDateNaSprawie")){
                caseFinishDate = TextUtils.trimmedStringOrNull(caseFinishDate);
            } else {
                caseFinishDate = TextUtils.trimmedStringOrNull(caseFinishDate);

                if (document.getType() == DocumentType.INCOMING && caseFinishDate == null)
                {
                    final Calendar cal = new GregorianCalendar();
                    cal.setTime(((InOfficeDocument) document).getIncomingDate());
                    cal.add(Calendar.DATE, ((InOfficeDocument) document).getKind().getDays());
                    caseFinishDate = DateUtils.formatJsDate(cal.getTime());
                }
            }

            priorities = CasePriority.list();

            DSUser[] substUsers = DSApi.context().getDSUser().getSubstituted();

            // na po�rednim ekranie warto�ci p�l clerk i caseFinishDate s�
            // wype�nione i w elementach hidden, nie nale�y ich nadpisywa�

            clerk = TextUtils.trimmedStringOrNull(clerk);

            // wyb�r innego referenta jest mo�liwy, gdy u�ytkownik ma takie
            // uprawnienie, lub gdy zast�puje innego u�ytkownika i tworzy
            // spraw� w jego imieniu
            if (DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA))
            {
                canChooseClerk = true;
                clerks = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                if (clerk == null)
                clerk = DSApi.context().getPrincipalName();
        }
        else if (substUsers.length > 0)
        {
            canChooseClerk = true;
            clerks = new LinkedList<DSUser>(Arrays.asList(substUsers));
            clerks.add(DSApi.context().getDSUser());
            // na po�rednim ekranie nie nale�y ju� nadpisywa� warto�ci pola clerk
            if (clerk == null)
                clerk = substUsers[0].getName();
        }
        else
        {
            if (clerk == null)
                clerk = DSApi.context().getPrincipalName();
        }
            
            
            if(AvailabilityManager.isAvailable("blockSetingClerkOnCreatingCase")){
            	clerk=DSApi.context().getPrincipalName();
            	 clerks = new LinkedList<DSUser>();
            	clerks.add(DSApi.context().getDSUser());
            }

        intoJournalPermission = DSApi.context().hasPermission(DSPermission.PISMA_KOMORKA_PRZYJECIE);

        canCreateGrantRequest = document.getType() == DocumentType.INCOMING;

        DSUser user = DSApi.context().getDSUser();
        DSDivision[] divisions = user.getDivisions();
        if (divisions.length > 0)
            targetDivision = divisions[0].getGuid();
        else
            targetDivision = document.getAssignedDivision();
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());
                document = OfficeDocument.findOfficeDocument(getDocumentId());
                
                blocked = false;
                
                if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin())
                	blocked = !PermissionManager.isDocumentInTaskList(document);
                
                canView = DSApi.context().hasPermission(DSPermission.SPRAWA_WSZYSTKIE_PODGLAD) || DSApi.context().isAdmin() || PermissionManager.isDocumentInTaskList(document);
                if (!canView)
                {
	                	DSUser user = DSApi.context().getDSUser();

		                if (DSApi.context().hasPermission(DSPermission.SPRAWA_KOMORKA_PODGLAD))
		                {
		                    DSDivision[] divisions = user.getDivisions();
		                    for (int i=0; i < divisions.length; i++)
		                    {
		                    	if (document.getDivisionGuid().equals(divisions[i].getGuid()))
		                    	{
		                    		canView = true;
		                    		break;
		                    	}
		                    	if (divisions[i].isPosition())
		                    	{
		                    		DSDivision parent = divisions[i].getParent();
		                            if ((parent != null) && (document.getDivisionGuid().equals(parent.getGuid())))
		                    		{
		                    			canView = true;
		                        		break;
		                    		}
		                    	}
		                    }
	                    }
	                    if ((document.getClerk() != null &&document.getClerk().equals(user.getName())) || document.getAuthor().equals(user.getName()))
	                    {
	                           	canView = true;
	                    }
                    }                   
                
                if(!canView && hasPermmisionFromListBriefcase() )
                { 
                	canView=true;
                }
                
                if(!canView)
                { 
            		event.addActionError("Nie masz uprawnie� do ogl�dania dokumentu");
            		return;
            	}
                fillForm();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }

		
    }
    
    
    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	freeCaseNumbers = null;
        	freeCaseNumbersText = provideFreeCaseNumbers(portfolioId);
           
            Date finishDate = DateUtils.nullSafeParseJsDate(caseFinishDate);
            if(description2!=null && description2.length()>0)
                if(description2.length()>200)
                {
                    event.addActionMessage("Opis sprawy mo�e mie� maksymalnie 200 znak�w");
                    return;
                }
            if (finishDate == null)
                addActionError("B��dna data zako�czenia sprawy");
            
            try
            {
            	finishDate.setHours(23);
            	finishDate.setMinutes(59);
                if (finishDate.before(GlobalPreferences.getCurrentDay()))
                    addActionError("Data zako�czenia sprawy jest wcze�niejsza od obecnej");
            }
            catch (Exception e)
            {                
            }

            //if (rwaCategoryId == null)
            if (portfolioId == null)
                addActionError("Nie wybrano teczki");

            if (hasActionErrors())
                return;

            event.setResult("pre-create");

            try
            {
                OfficeFolder portfolio = OfficeFolder.find(portfolioId);
                DSUser user = DSApi.context().getDSUser();
                boolean sameDiv = false ;
                
                
                for(DSDivision div :user.getOriginalDivisions()){
                	if (div.getGuid().contains(portfolio.getDivisionGuid()))
                		sameDiv=true;
                }
                if(sameDiv &&  !DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE)){
            		sameDiv=false;
            	} 
                if (DSApi.context().isAdmin())
                	sameDiv = true ;
                	
                if (!sameDiv && !canCreateCaseFromListBriefcase(portfolio)){
            	   throw new EdmException("Nie masz uprawnien do zak�adania sprawy w tej teczce");
               }
              
                canChooseClerk = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA);
                canSetArbitraryCaseId = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_DOWOLNY_SYMBOL);

                OfficeCase officeCase = new OfficeCase(portfolio);

                suggestedSequenceId = customSequenceId = officeCase.suggestSequenceId();

                String[] breakdown = officeCase.suggestOfficeId();
                suggestedOfficeId = StringUtils.join(breakdown, "");
                customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                customOfficeIdPrefixSeparator = breakdown[1];
                customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                customOfficeIdSuffixSeparator = breakdown[3];
                customOfficeIdSuffix = officeIdSuffix = breakdown[4];

                event.setResult("pre-create");
            }
            catch (EdmException e)
            {
                event.setResult(SUCCESS);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class RemoveFromCase implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                Docusafe.getBean(OfficeCaseService.class).removeDocumentFromOfficeCase(getDocumentId().toString(), caseId);
                OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);
                DSApi.context().commit();
            }
            catch(Exception e)
            {
                DSApi.context()._rollback();
            	log.error(e.getMessage(), e);
                addActionError("Nie uda�o si� usun�� pisma ze sprawy.");
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        { 
            Date finishDate = DateUtils.nullSafeParseJsDate(caseFinishDate);

            if (finishDate == null)
            {
                addActionError("B��dna data zako�czenia sprawy");
                return;
            }

            //if (rwaCategoryId == null)
            if (portfolioId == null)
            {
                addActionError("Nie wybrano teczki");
                return;
            }
            
            if(caseTitle==null || caseTitle.length()==0){
            	portfolioId=null;
            	addActionError("Nie podano tytu�u sprawy");
            	return;
            }
           
            // wyj�tek pozwalaj�cy na wyj�cie z try-catch bez wypisywania
            // komunikatu b��du
            //class DummyException extends Exception { };

            try
            {
                
                DSApi.context().begin();

                document = OfficeDocument.findOfficeDocument(getDocumentId());

                OfficeFolder portfolio = OfficeFolder.find(portfolioId);

                // je�eli dokument jest klonem i zosta� ju� przypisany do
                // sprawy - b��d
                if (document.getType() == DocumentType.INCOMING &&
                    ((InOfficeDocument) document).getMasterDocument() != null &&
                    document.getContainingCase() != null)
                    throw new EdmException("Nie mo�na utworzy� nowej sprawy dla " +
                        "dokumentu zale�nego. Przejd� do g��wnego dokumentu.");

                if (portfolio.getRwa().getAcHome() == null)
                    throw new EdmException("Nie mo�na za�o�y� sprawy w RWA "+
                        "nie posiadaj�cym kategorii archiwalnej");

                canChooseClerk = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_WYBOR_REFERENTA);
                canSetArbitraryCaseId = DSApi.context().hasPermission(DSPermission.SPRAWA_TWORZENIE_DOWOLNY_SYMBOL);

                OfficeCase officeCase = new OfficeCase(portfolio);                
                
                officeCase.setStatus(CaseStatus.find(CaseStatus.ID_OPEN));
                officeCase.setPriority(CasePriority.find(priority));
                officeCase.setFinishDate(finishDate);
                officeCase.setDescription(description2);
                officeCase.setTitle(caseTitle);
                

                //if (canChooseClerk)
                if ((canChooseClerk || DSApi.context().getDSUser().getSubstituted().length > 0)
                    && clerk != null)
                {
                    officeCase.setClerk(DSUser.findByUsername(clerk).getName());
                }
                else
                {
                    officeCase.setClerk(DSApi.context().getPrincipalName());
                }

                if (customOfficeId != null && customOfficeId.booleanValue())
                {
                    if (customSequenceId == null)
                        throw new EdmException("Nie podano numeru kolejnego sprawy lub " +
                            "wpisano niepoprawn� liczb�");

                    if (customSequenceId.intValue() <= 0)
                        throw new EdmException("Numer kolejny sprawy musi by� wi�kszy od zera");

                    if (!officeCase.sequenceIdAvailable(customSequenceId))
                        throw new EdmException("Wybrany numer kolejny ("+customSequenceId+") jest niedost�pny");

                    officeCase.setSequenceId(customSequenceId);

                    // zmodyfikowano numer kolejny sprawy, ponowne wygenerowanie
                    // ca�ego numeru
                    if (suggestedSequenceId.intValue() != customSequenceId.intValue())
                    {
                        suggestedSequenceId = customSequenceId;
                        officeCase.setSequenceId(customSequenceId);
                        String[] breakdown = officeCase.suggestOfficeId();
                        suggestedOfficeId = StringUtils.join(breakdown, "");
                        customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                        customOfficeIdPrefixSeparator = breakdown[1];
                        customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                        customOfficeIdSuffixSeparator = breakdown[3];
                        customOfficeIdSuffix = officeIdSuffix = breakdown[4];
                        addActionMessage("Wygenerowano nowy znak sprawy dla zmienionego numeru porz�dkowego");
                        event.setResult("pre-create");
                        return;
                    }

                    if(freeCaseNumbers == null && customSequenceId != null && customSequenceId>officeCase.suggestSequenceId()){
                    	freeCaseNumbers = provideFreeCaseNumbers(portfolioId);
                    	
                    	event.setResult("pre-create");
                        return;
                    }
                    
                    String oid = StringUtils.join(new String[] {
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
                    }, "");

                    officeCase.create(oid,
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");
                }
                else
                {
                    officeCase.setSequenceId(suggestedSequenceId);
                    officeCase.create(suggestedOfficeId,
                        officeIdPrefix, officeIdMiddle, officeIdSuffix);
                }

                //document.setContainingCase(c);
                boolean cloned = setCase(document, officeCase);
    
                // TODO: je�eli w sprawie ustawiany jest bit obecno�ci
                // w rejestrze, nale�y od razu utworzy� odpowiedni wpis
                // w rejestrze

                TaskSnapshot.updateAllTasksByDocumentId(getDocumentId(),document.getStringType());

                OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);

                DSApi.context().commit();

                if (cloned)
                {
                    addActionMessage(sm.getString("UtworzonoSpraweIUmieszczonoKopieDokumentu", officeCase.getOfficeId()));
                }
                else
                {
                	addActionMessage(sm.getString("UtworzonoSpraweIUmieszczonoDokument", officeCase.getOfficeId()));
                }
                setCaseId(officeCase.getId());
            }
//            catch (DummyException e)
//            {
//                DSApi.context().setRollbackOnly();
//            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
            	log.error(e.getMessage(),e);
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			} catch (Exception e) {
                log.error(e.getMessage(),e);
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            caseFinishDate = null;
            
            if(document instanceof InOfficeDocument)
            {
	            InOfficeDocument indoc = (InOfficeDocument)document;
	            if (indoc.isSubmitToBip())
	            {
	            	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_DOCUMENT_INFO).documentId(indoc.getId());
	            }
            }
            /*
            try
            {
                Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                bip.submit((InOfficeDocument)document);
            }
            catch (Exception e)
            {
            }*/
        }
        
        
    }
    
    private class AddToNextCase implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if(caseId == null)
        			throw new EdmException("Nie wybrano sprawy");
        		DSApi.context().begin();
        		document = OfficeDocument.findOfficeDocument(getDocumentId());
        		OfficeCase officeCase = OfficeCase.find(caseId);
        		
                if (document.getContainingCase() != null && officeCase.getOfficeId().equals(document.getContainingCase().getOfficeId()))
                	throw new EdmException(sm.getString("DokumentZnajdujeSieJuzWSprawie", officeCase.getOfficeId()));
                
        		  if(officeCase.getStatus().isClosingStatus() && caseReopen)
                  {
                  	officeCase.setStatus(CaseStatus.find(CaseStatus.ID_REOPEN));
                  }
	        	 setCase(document, officeCase);
                 addActionMessage("Kopie dokumentu umieszczono w sprawie "+ officeCase.getOfficeId());

                 OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
                 doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);

                DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
				log.error("",e);
				addActionError(e.getMessage());
				DSApi.context()._rollback();
			}
        }
    }

    private class AddToCase implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
               Docusafe.getBean(OfficeCaseService.class).addDocumentToOfficeCase(getDocumentId().toString(), caseId, caseReopen);
               DSApi.context().begin();
               OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
               doc.getDocumentKind().logic().updateDocumentBarcodeVersion(doc);
               DSApi.context().commit();
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            
        }
    }
   
    private boolean hasPermmisionFromListBriefcase() throws UserNotFoundException, EdmException
	{
    	if (!AvailabilityManager.isAvailable("menu.left.user.to.briefcase"))
    		return false;
    	else
    	return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.hasPermisionToCase(DSApi.context().getDSUser(),(Document)document);
    	
		
	}
    private boolean canCreateCaseFromListBriefcase(OfficeFolder portfolio) throws UserNotFoundException, EdmException
	{
    	if (!AvailabilityManager.isAvailable("menu.left.user.to.briefcase"))
    		return true;
    	else
    	return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.canCreateCaseFromListBriefcase(portfolio ,DSApi.context().getDSUser() );
    	
		
	}
    
    private String provideFreeCaseNumbers(Long portfolioId){
    	String result = null;
    	StringBuilder sb = new StringBuilder();
    	
    	boolean contextOpened = false;
    	
    	try{
    		contextOpened = DSApi.openContextIfNeeded();
    		
    		Query query = DSApi.context().session().createQuery("SELECT c.sequenceId FROM OfficeCase c WHERE c.parent=:parentId ORDER BY c.sequenceId");
    		
    		query.setLong("parentId", portfolioId);
    		
    		List<Integer> queryResult = query.list();
    		if(customSequenceId != null)
    			queryResult.add(customSequenceId);
    		
    		int sequenceNumber = 1;
    		int liczbaCyfrWolnych = 0;
    		
    		for(Integer sequenceId: queryResult){
    			liczbaCyfrWolnych = sequenceId - sequenceNumber;
    			
    			if(liczbaCyfrWolnych == 1){
    				sb.append(sequenceNumber);
    				sb.append(",");
    			}
    			else if(liczbaCyfrWolnych > 1){
    				sb.append(sequenceNumber);
    				sb.append("-");
    				sb.append(sequenceId-1);
    				sb.append(",");
    			}
    			
    			sequenceNumber = sequenceId+1;
    		}
    		if(sb.length() > 0){
    			sb.deleteCharAt(sb.length()-1);
    		}
    		
    		if(sb.length() != 0){
    			result = sb.toString();
    		}
    	}
    	catch(EdmException e){}
    	finally{
    		DSApi.closeContextIfNeeded(contextOpened);
    	}
    	
    	return result;
    }
    
    private String getDocumentTypeAsString(OfficeDocument document) {
		if (document.getType() == DocumentType.INCOMING)
			return " przychodz�ce ";
		if (document.getType() == DocumentType.OUTGOING)
			return " wychodz�ce ";
		if (document.getType() == DocumentType.INTERNAL)
		    return " wewn�trzne ";
		return "";
	}
  /*  private class IntoRegistry implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
                return;

            if (record == 0)
            {
                addActionError("Nie wybrano rejestru");
                return;
            }

            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());
                OfficeCase officeCase = document.getContainingCase();

                if (officeCase == null)
                    throw new EdmException("Dokument nie znajduje si� w sprawie, "+
                        "nie mo�na utworzy� wpisu w rejestrze");

                if (record == Records.GRANTS)
                {
                    try
                    {
                        GrantRequest gr = GrantRequest.findByOfficeCase(officeCase);

                        throw new EdmException("Ze spraw� jest ju� zwi�zany wniosek " +
                            "o numerze "+gr.getSequenceId());
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                    GrantRequest grantRequest = new GrantRequest();
                    grantRequest.create(officeCase);

                    grantRequestId = grantRequest.getId();
                    event.setResult("grant-request");
                }
                else if (record == Records.CONSTRUCTION)
                {
                    try
                    {
                        ConstructionApplication ca = ConstructionApplication.findByOfficeCase(officeCase);
                        throw new EdmException("Ze spraw� jest ju� zwi�zany wniosek o zezwolenie "+
                            "na budow�");
                    }
                    catch (EntityNotFoundException e)
                    {
                    }

                    ConstructionApplication ca = new ConstructionApplication();
                    ca.create(officeCase);
                }

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
*/
/*
    private class CreateGrantRequest implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
                return;

            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                OfficeCase officeCase = document.getContainingCase();

                if (officeCase == null)
                    throw new EdmException("Dokument nie znajduje si� w sprawie, "+
                        "nie mo�na utworzy� wniosku o stypendium");

                try
                {
                    GrantRequest gr = GrantRequest.findByOfficeCase(officeCase);

                    throw new EdmException("Ze spraw� jest ju� zwi�zany wniosek " +
                        "o numerze "+gr.getSequenceId());
                }
                catch (EntityNotFoundException e)
                {
                }
                GrantRequest grantRequest = new GrantRequest();
                grantRequest.create(officeCase);

                grantRequestId = grantRequest.getId();

                DSApi.context().commit();

                event.setResult("grant-request");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
*/

   public Long  getCaseId()
   {
       return caseId;
   }

    public final void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public Integer getPriority()
    {
        return priority;
    }

    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public String getCaseFinishDate()
    {
        return caseFinishDate;
    }

    public void setCaseFinishDate(String caseFinishDate)
    {
        this.caseFinishDate = caseFinishDate;
    }

    public String getPrettyRwaCategory()
    {
        return prettyRwaCategory;
    }

    public void setPrettyRwaCategory(String prettyRwaCategory)
    {
        this.prettyRwaCategory = prettyRwaCategory;
    }

    public OfficeDocument getDocument()
    {
        return document;
    }

    public List getPriorities()
    {
        return priorities;
    }

    public boolean isCanCreateCase()
    {
        return canCreateCase;
    }

    public void setPortfolioId(Long portfolioId)
    {
        this.portfolioId = portfolioId;
    }

    public String getPortfolioTxt()
    {
        return portfolioTxt;
    }

    public void setPortfolioTxt(String portfolioTxt)
    {
        this.portfolioTxt = portfolioTxt;
    }

    public Long getPortfolioId()
    {
        return portfolioId;
    }

    public List getClerks()
    {
        return clerks;
    }

    public boolean isCanChooseClerk()
    {
        return canChooseClerk;
    }

    public String getClerk()
    {
        return clerk;
    }

    public void setClerk(String clerk)
    {
        this.clerk = clerk;
    }

    public boolean isCanSetArbitraryCaseId()
    {
        return canSetArbitraryCaseId;
    }

    public String getArbitraryCaseId()
    {
        return arbitraryCaseId;
    }

    public void setArbitraryCaseId(String arbitraryCaseId)
    {
        this.arbitraryCaseId = arbitraryCaseId;
    }

    public Map getIncomingDivisions()
    {
        return incomingDivisions;
    }

    public boolean isIntoJournalPermission()
    {
        return intoJournalPermission;
    }

    public boolean isConfirmCreate()
    {
        return confirmCreate;
    }

    public void setConfirmCreate(boolean confirmCreate)
    {
        this.confirmCreate = confirmCreate;
    }

    public String getCustomOfficeIdPrefix()
    {
        return customOfficeIdPrefix;
    }

    public void setCustomOfficeIdPrefix(String customOfficeIdPrefix)
    {
        this.customOfficeIdPrefix = customOfficeIdPrefix;
    }

    public String getCustomOfficeIdPrefixSeparator()
    {
        return customOfficeIdPrefixSeparator;
    }

    public void setCustomOfficeIdPrefixSeparator(String customOfficeIdPrefixSeparator)
    {
        this.customOfficeIdPrefixSeparator = customOfficeIdPrefixSeparator;
    }

    public String getCustomOfficeIdMiddle()
    {
        return customOfficeIdMiddle;
    }

    public void setCustomOfficeIdMiddle(String customOfficeIdMiddle)
    {
        this.customOfficeIdMiddle = customOfficeIdMiddle;
    }

    public String getCustomOfficeIdSuffixSeparator()
    {
        return customOfficeIdSuffixSeparator;
    }

    public void setCustomOfficeIdSuffixSeparator(String customOfficeIdSuffixSeparator)
    {
        this.customOfficeIdSuffixSeparator = customOfficeIdSuffixSeparator;
    }

    public String getCustomOfficeIdSuffix()
    {
        return customOfficeIdSuffix;
    }

    public void setCustomOfficeIdSuffix(String customOfficeIdSuffix)
    {
        this.customOfficeIdSuffix = customOfficeIdSuffix;
    }

    public String getOfficeIdPrefix()
    {
        return officeIdPrefix;
    }

    public void setOfficeIdPrefix(String officeIdPrefix)
    {
        this.officeIdPrefix = officeIdPrefix;
    }

    public String getOfficeIdSuffix()
    {
        return officeIdSuffix;
    }

    public void setOfficeIdSuffix(String officeIdSuffix)
    {
        this.officeIdSuffix = officeIdSuffix;
    }

    public String getOfficeIdMiddle()
    {
        return officeIdMiddle;
    }

    public void setOfficeIdMiddle(String officeIdMiddle)
    {
        this.officeIdMiddle = officeIdMiddle;
    }

    public String getSuggestedOfficeId()
    {
        return suggestedOfficeId;
    }

    public void setSuggestedOfficeId(String suggestedOfficeId)
    {
        this.suggestedOfficeId = suggestedOfficeId;
    }

    public Integer getCustomSequenceId()
    {
        return customSequenceId;
    }

    public void setCustomSequenceId(Integer customSequenceId)
    {
        this.customSequenceId = customSequenceId;
    }

    public Integer getSuggestedSequenceId()
    {
        return suggestedSequenceId;
    }

    public void setSuggestedSequenceId(Integer suggestedSequenceId)
    {
        this.suggestedSequenceId = suggestedSequenceId;
    }

    public Boolean getCustomOfficeId()
    {
        return customOfficeId;
    }

    public void setCustomOfficeId(Boolean customOfficeId)
    {
        this.customOfficeId = customOfficeId;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public int getRecord()
    {
        return record;
    }

    public void setRecord(int record)
    {
        this.record = record;
    }

    public GrantRequest getGrantRequest()
    {
        return grantRequest;
    }

    public Long getGrantRequestId()
    {
        return grantRequestId;
    }

    public boolean isCanCreateGrantRequest()
    {
        return canCreateGrantRequest;
    }

    public Map getInRecords()
    {
        return inRecords;
    }

    public Map getAvailableRecords()
    {
        return availableRecords;
    }
    
    public String getDocumentIncomingDate()
    {
        return documentIncomingDate;
    }
    
    public void setDocumentIncomingDate(String documentIncomingDate)
    {
        this.documentIncomingDate = documentIncomingDate;
    }

    public String getTargetDivision()
    {
        return targetDivision;
    }

	public String getCaseLink() {
		return caseLink;
	}

	public void setCaseLink(String caseLink) {
		this.caseLink = caseLink;
	}

    public String getCaseRwa()
    {
        return caseRwa;
    }

    public String getDescription2()
    {
        return description2;
    }

    public void setDescription2(String description2)
    {
        this.description2 = description2;
        
    }

    public List<AssociativeDocumentStore.AssociativeDocumentBean> getAssociatedDocumentsIn() {
        return associatedDocumentsIn;
    }

    public void setAssociatedDocumentsIn(List<AssociativeDocumentStore.AssociativeDocumentBean> associatedDocumentsIn) {
        this.associatedDocumentsIn = associatedDocumentsIn;
    }

    public List<AssociativeDocumentStore.AssociativeDocumentBean> getAssociatedDocumentsOut() {
        return associatedDocumentsOut;
    }

    public void setAssociatedDocumentsOut(List<AssociativeDocumentStore.AssociativeDocumentBean> associatedDocumentsOut) {
        this.associatedDocumentsOut = associatedDocumentsOut;
    }

    public boolean isCanView() {
		return canView;
	}

	public void setCanView(boolean canView) {
		this.canView = canView;
	}

	public boolean isCaseReopen() {
		return caseReopen;
	}

	public void setCaseReopen(boolean caseReopen) {
		this.caseReopen = caseReopen;
	}

	public String getCaseTitle()
	{
		return caseTitle;
	}

	public void setCaseTitle(String caseTitle)
	{
		this.caseTitle= caseTitle;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public String getFreeCaseNumbers() {
		return freeCaseNumbers;
	}

	public void setFreeCaseNumbers(String freeCaseNumbers) {
		this.freeCaseNumbers = freeCaseNumbers;
	}

	public String getFreeCaseNumbersText() {
		return freeCaseNumbersText;
	}

	public void setFreeCaseNumbersText(String freeCaseNumbersText) {
		this.freeCaseNumbersText = freeCaseNumbersText;
	}
}