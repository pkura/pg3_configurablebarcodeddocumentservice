package pl.compan.docusafe.web.office.common;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 *
 * @author Micha� Sankowski
 */
public class RecipientLookupAction extends EventActionSupport{
	private final static int RESULTS_COUNT = 10;
	private final static Logger LOG = LoggerFactory.getLogger(RecipientLookupAction.class);

	private List<DSUser> users;
	private String lastname;
	private String division;

	protected void setup() {
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			try
			{
				Criteria crit = DSApi.context().session().createCriteria(DSUser.class);
				if (!StringUtils.isEmpty(lastname))
				{
					crit.add(Restrictions.like("lastname", lastname + "%").ignoreCase());
					crit.add(Restrictions.not(Restrictions.eq("deleted", true)));
				} 
				crit.setMaxResults(RESULTS_COUNT);
				setUsers(crit.list());
				for (DSUser user :users) 
				{
					DSDivision[] divisions = user.getDivisionsWithoutGroupPosition();
					if(divisions != null && divisions.length > 0)
					{
						divisions[0].getGuid();
						divisions[0].getName();
					}
				}
			}
			catch (Exception e) 
			{
				log.error("",e);
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setUsers(List<DSUser> users)
	{
		this.users = users;
	}

	public List<DSUser> getUsers()
	{
		return users;
	}
}
