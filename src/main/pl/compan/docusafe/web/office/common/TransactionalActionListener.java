package pl.compan.docusafe.web.office.common;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;

/**
 * Klasa wykonuj�ca jak�� czynno�� w transakcji.
 * Wszystkie wyj�tki s� logowane i rejestrowane (tak jak w LoggedActionListener).
 *
 * @author Micha� Sankowski
 */
public abstract class TransactionalActionListener extends LoggedActionListener{
	@Override
	public final void actionPerformed(ActionEvent event, Logger log) throws Exception {
		beforeTransaction(event, log);
		DSApi.context().begin();
		transaction(event, log);
		DSApi.context().commit();
		afterTransaction(event, log);
	}

	/** Czynno�� wykonywana w transakcji - jest rollbackowana gdy wyrzucony zostanie wyj�tek */
	public abstract void transaction(ActionEvent event, Logger log) throws Exception;

	/** Czynno�� wykonywana po udanym zako�czeniu transakcji */
	public void afterTransaction(ActionEvent event, Logger log) throws Exception {};

	/** Czynno�� wykonywana przed transakcj� - je�li wyrzuci wyj�tek transakcja nie jest wykonywana*/
	public void beforeTransaction(ActionEvent event, Logger log) throws Exception {};
}
