package pl.compan.docusafe.web.office.common;

import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.process.DocuSafeProcessDefinition;
import pl.compan.docusafe.process.ProcessEngineType;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.List;

/**
 * Wydzielona klasa dla akcji wspieraj�cych czynno�ci procesowe
 *
 * po stronie jsp ta klasa wykorzystuje process-actions.jsp
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class ProcessBaseTabsAction extends BaseTabsAction {
    private String processName;
    private String processId;
    private String processAction;
    private String processActionCorrectionInfo;
    private boolean processActionStored;

    /**
     * Change template layout to mobileView
     */
    protected boolean mobileView = false;

    protected String mobileRedirect;

    private final static Logger _PA_LOG = LoggerFactory
            .getLogger(ProcessAction.class);

    /**
     * Klasa wykonuje akcj� przekazan� ze strony
     */
    protected class ProcessAction extends LoggedActionListener {
        private String fillEventName;

        public ProcessAction(){}
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {
            if (log.isTraceEnabled()) {
                log.trace("processId = " + getProcessId());
                log.trace("processName = " + getProcessName());
                log.trace("processAction = " + getProcessAction());
                log.trace("processActionCorrectionInfo = " + getProcessActionCorrectionInfo());
            }

            OfficeDocument doc = OfficeDocument.find(getDocumentId());
            ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
                    .getProcessesDeclarations().getProcess(getProcessName());
            ProcessInstance pi = pdef.getLocator().lookupForId(getProcessId());

            String processActionCorrectionInfo = getProcessActionCorrectionInfo();
            //todo dla dzia�u R&D przygotowanie implementacji aby dzia�a�a dla dowolnego ProcessEngine'a
            if(pdef instanceof DocuSafeProcessDefinition){
                if( ProcessEngineType.ACTIVITI.getKey().equals(((DocuSafeProcessDefinition) pdef).getProcessEngineType().getKey())){
                    activitiActionCorrection(log,pi,processActionCorrectionInfo);
                }else{
                    jbpmActionCorrection(log, pi, processActionCorrectionInfo);
                }
            }else{
                jbpmActionCorrection(log, pi, processActionCorrectionInfo);
            }

            //log.info("begin context"); 
            DSApi.context().begin();
            pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
            pdef.getLogic().process(
                    pi,
                    pa = ProcessActionContext
                            .action(getProcessAction())
                            .document(doc)
                            .stored(processActionStored)
                            .activityId(getActivity()));

            DSApi.context().commit();
            //log.info("commited");

            if(pa.isReassigned()){
                String taskListResult = getTaskListResult();
                if(!mobileView) {
                    event.setResult(taskListResult);
                    if(fillEventName != null){
                        event.skip(fillEventName);
                    }
                } else {
                    mobileRedirect = taskListResult;
                }
            }
        }

        @Override
        public Logger getLogger() {
            return _PA_LOG;
        }

        /**
         * Ustal nazw� listenera, kt�ry ma zosta� omini�ty w przypadku gdy u�ytkowni zostanie przekierowany na
         * list� zada� (getTaskListResult()).
         * @param eventName nazwa zdarzenia
         * @return this
         */
        public ProcessAction skipListener(String eventName){
            this.fillEventName = eventName;
            return this;
        }
    }

    /**
     * Ustala czy by�o przekazanie do korekcji, je�li tak to ustawia zmienne procesowe activiti
     * cUser = username u�ytkownika do kt�rego przekazano do poprawy,
     * cState = status do kt�rego przekazano do poprawy
     * accId = ???
     * @param log
     * @param pi
     * @param processActionCorrectionInfo
     */
    private void activitiActionCorrection(Logger log, ProcessInstance pi, String processActionCorrectionInfo) {
        String processActionCorrectionUser,processActionCorrectionState ;
        Long acceptanceId = null;

        if (processActionCorrectionInfo != null && processActionCorrectionInfo.split("/").length==2) {
            processActionCorrectionUser = processActionCorrectionInfo.split("/")[0];
            processActionCorrectionState = processActionCorrectionInfo.split("/")[1];
        } else if (processActionCorrectionInfo != null && processActionCorrectionInfo.split("/").length>2) {
            try {
                acceptanceId = Long.valueOf(processActionCorrectionInfo.split("/")[0]);
            } catch (NumberFormatException e) {
                log.error("Nie mo�na przeparsowa� document_acceptance id: "+processActionCorrectionInfo.split("/")[0]);
            }
            processActionCorrectionUser = processActionCorrectionInfo.split("/")[1];
            processActionCorrectionState = processActionCorrectionInfo.split("/")[2];
        }else{
            return;
        }
        setActivitiProcessCorrectionVariables(pi, processActionCorrectionUser, processActionCorrectionState, acceptanceId);
    }

    /**
     * Ustawia zmienne procesowe activiti
     * cUser = username u�ytkownika do kt�rego przekazano do poprawy,
     * cState = status do kt�rego przekazano do poprawy
     * accId = ???
     * @param pi
     * @param processActionCorrectionUser
     * @param processActionCorrectionState
     * @param acceptanceId
     */
    private void setActivitiProcessCorrectionVariables(ProcessInstance pi, String processActionCorrectionUser, String processActionCorrectionState, Long acceptanceId) {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        List<Execution> executions = runtimeService.createExecutionQuery().processInstanceId(pi.getId().toString()).list();
        if(executions != null && !executions.isEmpty()){
            Execution execution = executions.get(0);
            if(acceptanceId != null){
                runtimeService.setVariable(execution.getId(),"acceptanceId",acceptanceId);
            }
            if (processActionCorrectionState != null) {
                runtimeService.setVariable(execution.getId(),"correctionState",processActionCorrectionState);
            }
            if (processActionCorrectionUser != null) {
                runtimeService.setVariable(execution.getId(),"correctionUserKey",processActionCorrectionUser);
            }
        }
    }

    private void jbpmActionCorrection(Logger log, ProcessInstance pi, String processActionCorrectionInfo) {
        if (processActionCorrectionInfo != null && processActionCorrectionInfo.split("/").length==2) {
            String processActionCorrectionUser = processActionCorrectionInfo.split("/")[0];
            String processActionCorrectionState = processActionCorrectionInfo.split("/")[1];
            Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId().toString(), "cState", processActionCorrectionState);
            Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId().toString(), "user", processActionCorrectionUser);
        } else if (processActionCorrectionInfo != null && processActionCorrectionInfo.split("/").length>2) {
            try {
                Long acceptanceId = Long.valueOf(processActionCorrectionInfo.split("/")[0]);
                Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId().toString(), "accId", acceptanceId);
            } catch (NumberFormatException e) {
                log.error("Nie mo�na przeparsowa� document_acceptance id: "+processActionCorrectionInfo.split("/")[0]);
            }

            String processActionCorrectionUser = processActionCorrectionInfo.split("/")[1];
            String processActionCorrectionState = processActionCorrectionInfo.split("/")[2];

            Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId().toString(), "cState", processActionCorrectionState);
            Jbpm4Provider.getInstance().getExecutionService().setVariable(pi.getId().toString(), "user", processActionCorrectionUser);
        }
    }

    public final String getProcessName() {
        return processName;
    }

    public final void setProcessName(String processName) {
        this.processName = processName;
    }

    public final String getProcessId() {
        return processId;
    }

    public final void setProcessId(String processId) {
        this.processId = processId;
    }

    public final String getProcessAction() {
        return processAction;
    }

    public final void setProcessAction(String processAction) {
        this.processAction = processAction;
    }

    public final boolean isProcessActionStored() {
        return processActionStored;
    }

    public final void setProcessActionStored(boolean processActionStored) {
        this.processActionStored = processActionStored;
    }

    public final String getProcessActionCorrectionInfo() {
        return processActionCorrectionInfo;
    }

    public final void setProcessActionCorrectionInfo(
            String processActionCorrectionInfo) {
        this.processActionCorrectionInfo = processActionCorrectionInfo;
    }

    public boolean isMobileView() {
        return mobileView;
    }

    public void setMobileView(boolean mobileView) {
        this.mobileView = mobileView;
    }

    public String getMobileRedirect() {
        return mobileRedirect;
    }

    public void setMobileRedirect(String mobileRedirect) {
        this.mobileRedirect = mobileRedirect;
    }
}