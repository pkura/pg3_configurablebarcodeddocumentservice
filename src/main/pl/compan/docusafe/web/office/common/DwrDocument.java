package pl.compan.docusafe.web.office.common;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;

public interface DwrDocument
{
    public Long getDocumentId();
    public void setDocumentId(Long documentId);
    public String getDocumentKindCn();
	/***
	 * Typ dokumentu (in,out,int,archive)
	 * @return
	 */
	public int getDocType();
    public void setDocumentKindCn(String documentKindCn);
    public Map<String, String> getDocumentKinds();
    public void setDocumentKinds(Map<String,String> documentKinds);
	public Map<String, Object> getDockindKeys();
	public void setDockindKeys(Map<String, Object> dockindKeys);
	public void setDocument(Document find);
	public Document getDocument();
    public void setBeforeUpdate(Map<String, Object> dockindKeys) throws EdmException ;
    public Logger getLog();
    public Document getNewDocument();
}
