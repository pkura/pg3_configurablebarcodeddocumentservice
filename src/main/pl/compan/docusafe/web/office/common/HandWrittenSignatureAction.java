package pl.compan.docusafe.web.office.common;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.ajax.AjaxJsonResult;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.p4.CometBarcodeMessage;
import pl.compan.docusafe.parametrization.p4.HandWrittenSignatureHelper;
import pl.compan.docusafe.parametrization.p4.CometBarcodeMessage.BarcodeOperation;
import pl.compan.docusafe.parametrization.p4.CometSignatureMessage;
import pl.compan.docusafe.parametrization.p4.CometSignatureMessage.MessageStatus;
import pl.compan.docusafe.parametrization.p4.SignatureUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.comet.CometMessage;
import pl.compan.docusafe.web.comet.CometMessage.MessageKind;
import pl.compan.docusafe.web.comet.CometMessageManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.lowagie.text.Image;
import com.opensymphony.webwork.ServletActionContext;

public class HandWrittenSignatureAction extends EventActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(HandWrittenSignatureAction.class);
	
	private String json;
	private String login;
	private String barcode;
	
	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("registerRecipient")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new RegisterRecipient())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("removeBarcode")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new RemoveBarcode())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("confirmReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new ConfirmReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("acceptReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new AcceptReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("saveReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new SaveReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("resetToken")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new ResetToken())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("correctReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new CorrectReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("cancelReception")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new CancelReception())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("getUserList")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new GetUserList())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("checkSession")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new CheckSession())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			// TODO Auto-generated method stub

		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	
	
	
	/**
	 * Rejestruje nowe nazwisko odbiorcy.
	 * Zwraca zarejestrowane nazwisko lub b��d (w jsonie)
	 * @author Kamil
	 *
	 */
	private class RegisterRecipient extends LoggedActionListener {
		private class RegisteredRecipient extends AjaxJsonResult {
			String lastname;
			String firstname;
			String token;
		}
		
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			RegisteredRecipient recipient = new RegisteredRecipient();
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			
			if(StringUtils.isEmpty(login)) {
				recipient.success = false;
				recipient.errorMessage = "Lastname is empty";
			} else {
				try {
					DSUser user = DSUser.findByUsername(login);
					CometMessage msgq = new CometSignatureMessage(user, MessageStatus.BEGIN);
					manager.addCometMessage(SignatureUtils.WYDAWCA_USER, msgq);
					recipient.lastname = user.getLastname();
					recipient.firstname = user.getFirstname();
					
					// Token will be validated in confirmReception action
					String uuid = UUID.randomUUID().toString();
					recipient.token = uuid;
					ServletActionContext.getRequest().getSession().setAttribute("hand-written-signature-token", uuid);
					
					// Initialize barcodes list in helper
					HandWrittenSignatureHelper.getInstance().clear();
					
				} catch(EdmException e) {
					LOG.error(e.getMessage(), e);
					recipient.success = false;
					recipient.errorMessage = e.getLocalizedMessage();
				}
			}
			
			out.write(new Gson().toJson(recipient));
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class ResetToken extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			String uuid = UUID.randomUUID().toString();
			ServletActionContext.getRequest().getSession().setAttribute("hand-written-signature-token", uuid);
			out.write(uuid);
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
		
	}
	
	private class RemoveBarcode extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			AjaxBarcode ajaxBarcode = new AjaxBarcode();
			
			if(StringUtils.isEmpty(barcode)) {
				ajaxBarcode.success = false;
				ajaxBarcode.errorMessage = "Nie podano barkodu dokumentu";
			} else {
				ajaxBarcode.barcode = barcode;
				CometMessage msg = new CometBarcodeMessage(MessageKind.BARCODE_OFFICE, barcode, 
						BarcodeOperation.REMOVE, null);
				manager.addCometMessage(SignatureUtils.WYDAWCA_USER, msg);
				
				HandWrittenSignatureHelper.getInstance().removeByBarcode(barcode);
			}

			out.write(ajaxBarcode.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class AjaxBarcode extends AjaxJsonResult {
		String barcode;
	}
	
	private static class AjaxReception {
		String login;
		String base64img;
//		List<String> barcodes;
//		List<String> documentIds;
		String token;
	}
	
	private class CorrectReception extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			
			
			DSUser reci = DSUser.findByUsername(login);
			CometMessageManager manager = CometMessageManager.getInstance();
			CometSignatureMessage msg = new CometSignatureMessage(reci, MessageStatus.CORRECT);
			String token = msg.getToken();
			manager.addCometMessage(SignatureUtils.MONITOR_DOTYKOWY_USER, msg);
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class ConfirmReception extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			AjaxJsonResult result = new AjaxJsonResult();
			StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
			
			if(StringUtils.isEmpty(json)) {
				result.success = false;
				result.errorMessage = "Nieprawid�owy JSON";
			} else {
				Gson gson = new Gson();
				AjaxReception reception = null;
				try {
					reception = gson.fromJson(json, AjaxReception.class);
				} catch(JsonParseException e) {
					LOG.error(e.getMessage(), e);
					result.success = false;
					result.errorMessage = e.getLocalizedMessage();
				}
				
				if(reception != null) {
					try {
						// validate token
						if(reception.token == null || reception.token.equals(getToken()) == false)
						{
							throw new EdmException("invalidToken");
						}
						else
						{
							// token OK, invalidate to prevent next actions.
							removeToken();
						}
						
						if(StringUtils.isEmpty(reception.base64img)) {
							throw new EdmException("Base64 image data is null or empty");
						}
						DSUser recipient = DSUser.findByUsername(reception.login);
						CometMessage msg = new CometSignatureMessage(recipient, reception.base64img, MessageStatus.CONFIRM);
						manager.addCometMessage(SignatureUtils.WYDAWCA_USER, msg);
						
//						List<InOfficeDocument> docs = InOfficeDocument.findByIds(HandWrittenSignatureHelper.getInstance().getDocumentIds());
//						
//						String recipientLogin = recipient.getName();
//						
//						DSApi.context().begin();
//						InOfficeDocument signatureDoc = createSignatureDocument(reception);
//						
//						Attachment attTemp = signatureDoc.getAttachmentByCn(SignatureUtils.SIGNATURE_ATTACHMENT);
//						AttachmentRevision attTempRev = attTemp.getMostRecentRevision();
//						Image img = attTempRev.getItextImage(0);
//						File pdf = SignatureUtils.createSignaturePdf(recipient, docs, img, sm);
//						
//						for(InOfficeDocument doc : docs) {
//							doc.setMasterDocument(signatureDoc);
//							doc.setRecipientUser(recipientLogin);
//							Attachment att = new Attachment("podpis");
//							doc.setPermissionsOn(false);
//							doc.createAttachment(att);
//							AttachmentRevision rev = att.createRevision(pdf);
//							rev.verifyIntegrity();
//						}
//						DSApi.context().commit();
//						
//						HandWrittenSignatureHelper.getInstance().clear();
					} catch(EdmException e) {
						LOG.error(e.getMessage(), e);
						result.success = false;
						result.errorMessage = e.getLocalizedMessage();
					}
				}
			}
			
			out.write(result.toJson());
			out.flush();
			out.close();
		}
		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class SaveReception extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			AjaxJsonResult result = new AjaxJsonResult();
			StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
			
			if(StringUtils.isEmpty(json)) {
				result.success = false;
				result.errorMessage = "Nieprawid�owy JSON";
			} else {
				Gson gson = new Gson();
				AjaxReception reception = null;
				try {
					reception = gson.fromJson(json, AjaxReception.class);
				} catch(JsonParseException e) {
					LOG.error(e.getMessage(), e);
					result.success = false;
					result.errorMessage = e.getLocalizedMessage();
				}
				
				if(reception != null) {
					try {
						
						if(StringUtils.isEmpty(reception.base64img)) {
							throw new EdmException("Base64 image data is null or empty");
						}
						DSUser recipient = DSUser.findByUsername(reception.login);
						CometMessage msg = new CometSignatureMessage(recipient, reception.base64img, MessageStatus.FINISH);
						manager.addCometMessage(SignatureUtils.WYDAWCA_USER, msg);
						
						List<InOfficeDocument> docs = InOfficeDocument.findByIds(HandWrittenSignatureHelper.getInstance().getDocumentIds());
						
						String recipientLogin = recipient.getName();
						
						DSApi.context().begin();
						InOfficeDocument signatureDoc = createSignatureDocument(reception);
						
						Attachment attTemp = signatureDoc.getAttachmentByCn(SignatureUtils.SIGNATURE_ATTACHMENT);
						AttachmentRevision attTempRev = attTemp.getMostRecentRevision();
						Image img = attTempRev.getItextImage(0);
						File pdf = SignatureUtils.createSignaturePdf(recipient, docs, img, sm);
						
						for(InOfficeDocument doc : docs) {
							doc.setMasterDocument(signatureDoc);
							doc.setRecipientUser(recipientLogin);
							Attachment att = new Attachment("podpis");
							doc.setPermissionsOn(false);
							doc.createAttachment(att);
							AttachmentRevision rev = att.createRevision(pdf);
							rev.verifyIntegrity();
							DSApi.context().session().createSQLQuery("delete from ds_p4_report_task where document_id=? and username= ?").setString(1, recipientLogin).setLong(0, doc.getId()).executeUpdate();
						}
						DSApi.context().commit();
						
						HandWrittenSignatureHelper.getInstance().clear();
						
						
						
						
						
						
					}catch(Exception e){
						LOG.error(e.getMessage(), e);
						result.success = false;
						result.errorMessage = e.getLocalizedMessage();
					}
				}
			}
			out.write(result.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class AcceptReception extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			DSUser reci = DSUser.findByUsername(login);
			CometMessageManager manager = CometMessageManager.getInstance();
			CometMessage msg = new CometSignatureMessage(reci, MessageStatus.ACCEPT);
			manager.addCometMessage(SignatureUtils.MONITOR_DOTYKOWY_USER, msg);
			
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class CancelReception extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			CometMessageManager manager = CometMessageManager.getInstance();
			AjaxJsonResult result = new AjaxJsonResult();
			
			if(StringUtils.isEmpty(login)) {
				result.success = false;
				result.errorMessage = "Nie podano loginu"; 
			} else {
				try {
					DSUser recipient = DSUser.findByUsername(login);
//					DSUser loggedUser = DSApi.context().getDSUser();
					CometMessage msg = new CometSignatureMessage(recipient, MessageStatus.CANCEL);
					manager.addCometMessage(SignatureUtils.WYDAWCA_USER, msg);
					
					HandWrittenSignatureHelper.getInstance().clear();
				} catch(EdmException e) {
					LOG.error(e.getMessage(), e);
					result.success = false;
					result.errorMessage = e.getLocalizedMessage();
				}
			}
			
			out.write(result.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class GetUserList extends LoggedActionListener {
		private class UserInfo {
			String login;
			String firstname;
			String lastname;
			String normalizedFirstname;
			String normalizedLastname;
			
			UserInfo(String login, String firstname, String lastname) {
				this.login = login;
				this.firstname = firstname;
				this.lastname = lastname;
				this.normalizedFirstname = TextUtils.normalizeString(firstname);
				this.normalizedLastname = TextUtils.normalizeString(lastname);
			}
		}
		
		private class AjaxUserList extends AjaxJsonResult {
			List<UserInfo> users = new ArrayList<UserInfo>();
		}
		
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			AjaxUserList ajaxUserList = new AjaxUserList();
			
			try {
				List<DSUser> users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				for(DSUser usr : users) {
					ajaxUserList.users.add(new UserInfo(usr.getName(), usr.getFirstname(), usr.getLastname()));
				}
			} catch(EdmException e) {
				LOG.error(e.getMessage(), e);
				ajaxUserList.success = false;
				ajaxUserList.errorMessage = e.getLocalizedMessage();
			}
			
			out.write(ajaxUserList.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class CheckSession extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter();
			
			out.write("OK");
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private InOfficeDocument createSignatureDocument(AjaxReception reception) throws EdmException {
		
		InOfficeDocument doc = new InOfficeDocument();
		doc.setSummaryWithoutHistory("");
		doc.setAuthor(reception.login);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision("");
		doc.setCreatingUser(reception.login);
		doc.setArchived(false);
		
		String base64only = reception.base64img.replaceFirst("data:image/(png|jpeg);base64,", "");
		byte[] decodedImage = Base64.decodeBase64(base64only);
		InputStream in = new ByteArrayInputStream(decodedImage);
		
		Attachment att = new Attachment(reception.login);
		att.setCn(SignatureUtils.SIGNATURE_ATTACHMENT);
		
		doc.create();
		doc.setPermissionsOn(false);
		doc.createAttachment(att);
		AttachmentRevision rev = att.createRevision(in, decodedImage.length, "podpis.png");

		return doc;
	}
	
	private PrintWriter getOutPrintWriter() throws IOException {
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html; charset=UTF-8");
		return resp.getWriter();
	}
	
	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 */
	public String getToken()
	{
		return (String)ServletActionContext.getRequest().getSession().getAttribute("hand-written-signature-token");
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 */
	public void removeToken()
	{
		ServletActionContext.getRequest().getSession().setAttribute("hand-written-signature-token", null);
	}

}
