package pl.compan.docusafe.web.office.common;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import std.fun;

/**
 *
 * @author Micha� Sankowski
 */
public class SenderLookupAction extends EventActionSupport{
	private final static Logger LOG = LoggerFactory.getLogger(SenderLookupAction.class);
	private final static int RESULTS_COUNT = 200;

	private String lastname;
	private String organization;
	private String dictionaryType;
	private String title;
	private String firstname;
	private String streetAddress;
	private String zip;
	private String email;
	private String fax;
	private String location;

	private List<? extends Person>  senders;

	protected void setup() {
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private static String isoToUtfHack(String string) throws Exception{
		return string; //new String(string.getBytes("ISO-8859-2"),"utf-8");
	}

	private class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{
			//log.error(ServletActionContext.getRequest().getContentType() + " lastname = " + lastname + "/" + new String(lastname.getBytes("ISO-8859-2"),"utf-8"));


			 QueryForm form = new QueryForm(0, RESULTS_COUNT);
				if (!StringUtils.isEmpty(firstname))
					form.addProperty("firstname", isoToUtfHack(firstname.trim()));
				if (!StringUtils.isEmpty(lastname))
					form.addProperty("lastname", isoToUtfHack(lastname.trim()));
				if (!StringUtils.isEmpty(organization))
					form.addProperty("organization", isoToUtfHack(organization.trim()));
				if (!StringUtils.isEmpty(streetAddress))
					form.addProperty("street", isoToUtfHack(streetAddress.trim()));
				if (!StringUtils.isEmpty(zip))
					form.addProperty("zip", isoToUtfHack(zip.trim()));
				if (!StringUtils.isEmpty(location))
					form.addProperty("location", isoToUtfHack(location.trim()));
				if (!StringUtils.isEmpty(email))
					form.addProperty("email", isoToUtfHack(email.trim()));
				if (!StringUtils.isEmpty(fax))
					form.addProperty("fax", isoToUtfHack(fax.trim()));
				if (!StringUtils.isEmpty(dictionaryType))
					form.addProperty("dictionaryType", dictionaryType);
				form.addProperty("dictionaryGuid", DSDivision.ROOT_GUID);
	            form.addProperty("DISCRIMINATOR", "PERSON");
	            SearchResults<? extends Person> results = Person.search(form);
	            setSenders(fun.list(results));
		}

		@Override
		public Logger getLogger()
		{
			return LOG;
		}
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSenderEmail() {
		return email;
	}

	public void setSenderEmail(String senderEmail) {
		this.email = senderEmail;
	}

	public String getSenderFax() {
		return fax;
	}

	public void setSenderFax(String senderFax) {
		this.fax = senderFax;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getDictionaryType() {
		return dictionaryType;
	}

	public void setDictionaryType(String dictionaryType) {
		this.dictionaryType = dictionaryType;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setSenders(List<? extends Person> senders)
	{
		this.senders = senders;
	}

	public List<? extends Person> getSenders()
	{
		return senders;
	}

	public void setOrganization(String organization)
	{
		this.organization = organization;
	}

	public String getOrganization()
	{
		return organization;
	}
}
