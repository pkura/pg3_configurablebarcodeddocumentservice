package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-12-21 16:38:51 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentsAppletAction.java,v 1.3 2006/02/20 15:42:41 lk Exp $
 */
public class AttachmentsAppletAction extends EventActionSupport
{
    private String openerUrl;
    private Long documentId;
    private String activity;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    public String getOpenerUrl()
    {
        return openerUrl;
    }

    public void setOpenerUrl(String openerUrl)
    {
        this.openerUrl = openerUrl;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getActivity()
    {
        return activity;
    }

    public void setActivity(String activity)
    {
        this.activity = activity;
    }
}
