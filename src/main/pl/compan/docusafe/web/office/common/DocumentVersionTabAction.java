package pl.compan.docusafe.web.office.common;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import com.google.common.collect.Sets;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;

public abstract class DocumentVersionTabAction extends BaseTabsAction {

	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			DocumentVersionTabAction.class.getPackage().getName(), null);
	public final static Logger log = LoggerFactory
			.getLogger(DocumentVersionTabAction.class);

	private List documentVersion;

	protected abstract List prepareTabs();

	private Long docId;
	private Boolean procesAktywny = true;
	 
	@Override
	protected void setup() {

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
		.append(new Active()).append(new FillForm())
		.appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				setTabs(prepareTabs());
				OfficeDocument docType = OfficeDocument.find(getDocumentId());
				long id = docType.getVersionGuid();
				List<OfficeDocument> doc = OfficeDocument
						.findAllByVersionGuid(id);
				
				if (docType instanceof InOfficeDocument) {

					int keys = doc.size();
					documentVersion = new ArrayList(keys);
					
					for (int i = 0; i < keys; i++) {
						Map bean = new HashMap();

						bean.put("lp", i + 1);
						bean.put("key", doc.get(i).getId());
						bean.put("opis", doc.get(i).getVersionDesc());
						bean.put("link",  "/office/incoming/document-archive.action?documentId="+doc.get(i).getId());
						bean.put("active", "/office/incoming/document-version.action?documentId="+doc.get(i).getId()+"&docId="+doc.get(i).getId());
						bean.put("wersja", doc.get(i).getVersion());
						if (doc.get(i).getCzyAktualny()) {
							bean.put("aktywny", "Tak");
						} else {
							bean.put("aktywny", "Nie");
						}
						bean.put("data", doc.get(i).getCtime());
						bean.put("czyAktywny", doc.get(i).getCzyAktualny());
						documentVersion.add(bean);
					}
				} else if (docType instanceof OutOfficeDocument) {
					OutOfficeDocument OutDoc = OutOfficeDocument
							.findOutOfficeDocument(getDocumentId());
					int keys = doc.size();
					documentVersion = new ArrayList(keys);
					if (OutDoc.isInternal()) {
						for (int i = 0; i < keys; i++) {
							Map bean = new HashMap();

							bean.put("lp", i + 1);
							bean.put("key", doc.get(i).getId());
							bean.put("opis", doc.get(i).getVersionDesc());
							bean.put("link",  "/office/internal/document-archive.action?documentId="+doc.get(i).getId());
							bean.put("active", "/office/internal/document-version.action?documentId="+doc.get(i).getId()+"&docId="+doc.get(i).getId());
							bean.put("wersja", doc.get(i).getVersion());
							if (doc.get(i).getCzyAktualny()) {
								bean.put("aktywny", "Tak");
							} else {
								bean.put("aktywny", "Nie");
							}
							bean.put("data", doc.get(i).getCtime());
							bean.put("czyAktywny", doc.get(i).getCzyAktualny());
							documentVersion.add(bean);
						}
					} else {
						for (int i = 0; i < keys; i++) {
							Map bean = new HashMap();

							bean.put("lp", i + 1);
							bean.put("key", doc.get(i).getId());
							bean.put("opis", doc.get(i).getVersionDesc());
							bean.put("link",  "/office/outgoing/document-archive.action?documentId="+doc.get(i).getId());
							bean.put("active", "/office/outgoing/document-version.action?documentId="+doc.get(i).getId()+"&docId="+doc.get(i).getId());
							bean.put("wersja", doc.get(i).getVersion());
							if (doc.get(i).getCzyAktualny()) {
								bean.put("aktywny", "Tak");
							} else {
								bean.put("aktywny", "Nie");
							}
							bean.put("data", doc.get(i).getCtime());
							bean.put("czyAktywny", doc.get(i).getCzyAktualny());
							documentVersion.add(bean);
						}
					}
				}
			} catch (EdmException e) {
				addActionError(e.toString());
				log.error(e + " - " + e.getMessage(), e);
			}

		}
	}

	public class Active implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			Long oldVersionId = null;
			if (docId != null)
			{
				String caseDocumentId;
				Long caseId;

				try
				{

					DSApi.context().begin();
					OfficeDocument doc = OfficeDocument.find(documentId);
					if(doc.getCzyAktualny())
					{
						addActionError(sm.getString("ProbujeszAktywowacWersjeJuzAktywna"));		
						return;
					}
					oldVersionId = findOldVersion(doc.getVersionGuid());
					List<OfficeDocument> docActive = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
					doc.getCzyAktualny();
					int rozmiar = docActive.size();
					for (int i = 0; i < rozmiar; i++)
					{
						if (docActive.get(i).getCzyAktualny())
						{
							long id = docActive.get(i).getId();

							if (id != documentId)
							{
								caseId = docActive.get(i).getContainingCaseId();
								caseDocumentId = docActive.get(i).getCaseDocumentId();
								doc.setCaseDocumentId(caseDocumentId);
								doc.setContainingCaseId(caseId);
								docActive.get(i).setCzyAktualny(false);
								docActive.get(i).setCaseDocumentId(null);
								docActive.get(i).setContainingCaseId(null);

							}

							if (doc instanceof InOfficeDocument)
							{
								doc.setSummary(doc.getDescription());
							} else if (doc instanceof OutOfficeDocument)
							{
								doc.setSummary(doc.getDescription());
							}
						}

						if (JBPMTaskSnapshot.findByDocumentId(docActive.get(i).getId()) != null && procesAktywny)
						{
							List<OfficeDocument> guid = OfficeDocument.findAllByVersionGuid(doc.getVersionGuid());
							for (OfficeDocument od : guid)
							{
								if (JBPMTaskSnapshot.findByDocumentId(od.getId()) != null)
								{
									List<JBPMTaskSnapshot> taskId = JBPMTaskSnapshot.findByDocumentId(od.getId());
									if (taskId.size() != 0)
									{
										if (taskId.size() > 1)
										{
											for (int k = 0; k < taskId.size() - 1; k++)
											{
												WorkflowFactory wf = WorkflowFactory.getInstance();
												Set<Long> docIds = Sets.newLinkedHashSet();

												docIds.add(taskId.get(k).getDocumentId());

												if (wf instanceof InternalWorkflowFactory)
												{
													for (Long docId : docIds)
													{
														String activityId = wf.findManualTask(docId);
														wf.manualFinish(activityId, false);
													}
												} else if (wf instanceof Jbpm4WorkflowFactory)
												{
													for (Long docId : docIds)
													{
														if (Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null)
														{
															String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
															wf.manualFinish(firstActivityId, false);
														}
													}
												} else
												{
													throw new IllegalStateException("nieznany WorkflowFactory");
												}
											}
										} else
										{
											for (int j = 0; j < taskId.size(); j++)
											{
												WorkflowFactory wf = WorkflowFactory.getInstance();
												Set<Long> docIds = Sets.newLinkedHashSet();

												docIds.add(taskId.get(j).getDocumentId());

												if (wf instanceof InternalWorkflowFactory)
												{
													for (Long docId : docIds)
													{
														String activityId = wf.findManualTask(docId);
														wf.manualFinish(activityId, false);
													}
												} else if (wf instanceof Jbpm4WorkflowFactory)
												{
													for (Long docId : docIds)
													{
														if (Jbpm4ProcessLocator.taskIds(docId).iterator().next() != null)
														{
															String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
															wf.manualFinish(firstActivityId, false);
															setProcesAktywny(false);
														}
													}
												} else
												{
													throw new IllegalStateException("nieznany WorkflowFactory");
												}
											}
										}
									}
								}
							}
						}
					}

					doc.setCzyAktualny(true);
					Document document = Document.find(documentId);

					document.getDocumentKind().logic().onStartProcess((OfficeDocument) document, event);

					TaskSnapshot.updateByDocumentId(document.getId(), ((OfficeDocument) document).getStringType());

					if (oldVersionId != null)
					{
						if (Document.find(oldVersionId) instanceof OfficeDocument)
						{
							((OfficeDocument) Document.find(oldVersionId)).addWorkHistoryEntry(Audit.create("Active", DSApi.context().getPrincipalName(),
									sm.getString("WersjaNieAktywna") + " " + Document.find(oldVersionId).getVersion()));
							((OfficeDocument) Document.find(oldVersionId)).addWorkHistoryEntry(Audit.create("Active", DSApi.context().getPrincipalName(),
									sm.getString("WersjaAktywna") + " " + doc.getVersion()));

						}
						if (AvailabilityManager.isAvailable("dokument.usuwaStareWpisyZHistoriiZanimUstawiaNowa"))
						{
							try
							{
								PreparedStatement ps = DSApi.context().prepareStatement("delete from dso_document_audit where document_id=" + oldVersionId);
								ps.executeUpdate();
							} catch (SQLException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (EdmException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
						doc.setWorkHistory(Document.find(oldVersionId).getWorkHistory());
						for (Audit audit : Document.find(oldVersionId).getWorkHistory())
						{
							if (audit.getProperty() != null)
								((OfficeDocument) doc).addWorkHistoryEntry(audit);
						}
						//usuwanie starej historii dekretacji
						String sql = "delete from dso_document_asgn_history where document_id=" + oldVersionId;
						PreparedStatement ps;
						try
						{
							ps = DSApi.context().prepareStatement(sql);
							ps.executeUpdate();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					if (AvailabilityManager.isAvailable("utp.aktualizujDokumentyPowiazanePrzyZmianieWersji"))
					{
						if (oldVersionId != doc.getId())
						{
							String tabela_multi = doc.getDocumentKind().getMultipleTableName();
							String sql = "update " + tabela_multi + " set document_id='" + doc.getId() + "' where document_id=" + oldVersionId.toString()
									+ " and field_cn='LINKS'";
							PreparedStatement ps2;
							try
							{
								ps2 = DSApi.context().prepareStatement(sql);
								ps2.executeUpdate();
								sql = "update " + tabela_multi + " set field_val='" + doc.getId() + "' where field_val=" + oldVersionId.toString()
										+ " and field_cn='LINKS'";
								ps2 = DSApi.context().prepareStatement(sql);
								ps2.executeUpdate();
							} catch (SQLException e)
							{
								e.printStackTrace();
							}
						}
					}
					if (AvailabilityManager.isAvailable("utp.usuwajDokumentyPowiazanePrzyZmianieWersji"))
					{
						String tabela_multi = doc.getDocumentKind().getMultipleTableName();
						String sql = "update " + tabela_multi + " set document_id='" + doc.getId() + "' where document_id=" + oldVersionId.toString()
								+ " and field_cn='LINKS'";
						PreparedStatement ps2;
						try
						{
							ps2 = DSApi.context().prepareStatement(sql);
							ps2.executeUpdate();
							sql = "update " + tabela_multi + " set field_val='" + doc.getId() + "' where field_val=" + oldVersionId.toString()
									+ " and field_cn='LINKS'";
							ps2 = DSApi.context().prepareStatement(sql);
							ps2.executeUpdate();
						} catch (SQLException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						DSApi.context().commit();
					}
				} catch (DocumentNotFoundException e)
				{
					addActionError(e.toString());
					log.error(e + " - " + e.getMessage(), e);
				} catch (EdmException e)
				{
					addActionError(e.toString());
					log.error(e + " - " + e.getMessage(), e);
				}

			} else
			{

			}
		}

		// zwraca aktywna id aktywnej wersji dla danego version_guid
		private long findOldVersion(long i) {
			try {
				PreparedStatement ps;

				ps = DSApi.context().prepareStatement("SELECT id from ds_document where version_guid=? and czy_aktualny=1");
				ps.setLong(1, i);
				ResultSet rs = ps.executeQuery();
				if (rs.next())
				{
					return rs.getLong("id");
				}
				return 0;
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EdmException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;

		}

	}

	@Override
	public String getDocumentType() {
		// TODO Auto-generated method stub
		return null;
	}

	public List getDocumentVersion() {
		return documentVersion;
	}

	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}
	
	public boolean getProcesAktywny() {
		return procesAktywny;
	}
	
	public void setProcesAktywny(boolean procesAktywny) {
		this.procesAktywny=procesAktywny;
	}

}
