package pl.compan.docusafe.web.office.common;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.jbpm.api.ProcessDefinition;
import org.jbpm.api.ProcessDefinitionQuery;
import org.xml.sax.SAXException;

import pl.compan.docusafe.ajax.AjaxJsonResult;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.dockinds.DockindXMLUtils;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.process.DocuSafeProcessDefinition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;

public class DwrEditorAction extends EventActionSupport {
	private final static Logger LOG = LoggerFactory.getLogger(DwrEditorAction.class);
	private boolean saveXML;
	private boolean loadXML;
	private String dockindCn;
	private String processName;
	/**
	 * XML edytowany przez u�ytkownika przesy�amy do serwera, a potem pobieramy,
	 * poniewa� �adna przegl�darka (opr�cz Google Chrome) nie pozwala na pobranie przez u�ytkownika
	 * pliku z innej lokacji ni� odpowied� servera.
	 */
	private FormFile xml;
	private String xmlString;
	/**
	 * Odebrane dane w formacie JSON
	 */
	private String json;
	
	static class DockindProperties {
		String cn;
		String name;
		String tableName;
        boolean updateDb = false;
	}
	
	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindList")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindList())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindUpdate")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindUpdate())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindXML")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindXML())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("pingBackXML")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new PingBackXML())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("pingBackXMLFile")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new PingBackXMLFile())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindSchema")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindSchema())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindFields")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindFields())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("dockindTemplate")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new DockindTemplate())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("processList")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new ProcessList())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("processFields")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new ProcessFields())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private PrintWriter getOutPrintWriter(String contentType) {
    	try {
	    	HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType(contentType);
			return resp.getWriter();
    	} catch(IOException e) {
    		LOG.error(e.getMessage(), e);
    		return null;
    	}
    }
	
	private class DockindList extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			Gson gson = new Gson();
			PrintWriter out = getOutPrintWriter("text/json; charset=utf-8");
			List<Map<String, Object>> dockinds = new ArrayList<Map<String, Object>>();
			
			try {
				List<DocumentKind> documentKinds = DocumentKind.list();
				for(DocumentKind dockind : documentKinds) {
					Map<String, Object> m = new HashMap<String, Object>();
					m.put("cn", dockind.getCn());
					m.put("type", dockind.getDockindInfo().getDocumentTypes());
					m.put("name", dockind.getName());
					m.put("tableName", dockind.getTablename());
					m.put("addDate", dockind.getAddDate().getTime());
					dockinds.add(m);
				}
				out.write(gson.toJson(dockinds));
			} catch(EdmException e) {
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private void validateXML(File file) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		URL url = Docusafe.getServletContext().getResource("/WEB-INF/DockindSchema.xsd");
		Schema schema = factory.newSchema(url);
	
		schema.newValidator().validate(new StreamSource(file));
	}
	
	private class DockindUpdate extends LoggedActionListener {
		private class AjaxDockind extends AjaxJsonResult {
			String xmlString;
			DockindProperties props;
		}
		
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter("text/json; charset=utf-8");
			Gson gson = new Gson();
			AjaxDockind result = new AjaxDockind();
			
			try {
				String xmlString = FileUtils.readFileToString(xml.getFile(), "UTF-8");
				DockindProperties dockindProp = gson.fromJson(json, new TypeToken<DockindProperties>() {}.getType());
				dockindProp.name = new String(dockindProp.name.getBytes("ISO-8859-2"), "UTF-8");
				
				validateXML(xml.getFile());
				DSApi.context().begin();
				DocumentKind dockind = createOrUpdate(dockindProp, xml.getFile(), dockindProp.updateDb);
				DSApi.context().commit();
				
				DocumentKind.resetDockindInfo(dockindProp.cn);
				
				result.xmlString = xmlString;
				result.props = dockindProp;
				result.success = true;
			} catch(Exception e) {
				DSApi.context().setRollbackOnly();
				result.success = false;
				result.errorMessage = e.getLocalizedMessage();
				log.error(e.getMessage(), e);
			} finally {
				out.write(result.toJson());
				out.flush();
				out.close();
			}
			
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private DocumentKind createOrUpdate(DockindProperties props, File file) throws EdmException
	{
		return createOrUpdate(props, file, false);
	}
	
	private DocumentKind createOrUpdate(DockindProperties props, File file, boolean createOrUpdateTable) throws EdmException
    {
        DocumentKind documentKind = DocumentKind.findByCn(props.cn);
        
        if (documentKind == null)
        {
        	if (StringUtils.isEmpty(props.name))
                throw new EdmException("Nie podano nazwy tworzonego typu");
            if (StringUtils.isEmpty(props.tableName))
                throw new EdmException("Nie podano nazwy tabeli w bazie dla tworzonego rodzaju");
            
            documentKind = new DocumentKind();
            documentKind.setCn(props.cn);
            documentKind.setTablename(props.tableName);
            documentKind.setName(props.name);
            documentKind.setEnabled(true);
            documentKind.setAddDate(new Date());
            Persister.create(documentKind);
        }
        else
        {
        	documentKind.setTablename(props.tableName != null ? props.tableName : documentKind.getTablename());
            documentKind.setName(props.name != null ? props.name : documentKind.getName());
            documentKind.setAddDate(new Date());
        }
        
        documentKind.saveContent(file);
        if(createOrUpdateTable)
        {
        	documentKind.createOrUpdateTable();
        }
        documentKind.loadXmlData(); //dzi�ki temu wywala je�li dockind b�dzie nieprawid�owy
        
        return documentKind;
    }
	
	private class DockindXML extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			if(loadXML) {
				Document xml;
				try {
					xml = DocumentKind.findByCn(dockindCn).getXML();
					out.write(xml.asXML());
				} catch(EdmException e)  {
					out.write("-1");
				} finally {
					out.flush();
					out.close();
				}
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class PingBackXML extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
				
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			try 
			{
				String xmlString = FileUtils.readFileToString(xml.getFile(), "UTF-8");
				out.write(xmlString);
			} catch(IOException e) {
				out.write("-1");
			} catch(NullPointerException e) {
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class PingBackXMLFile extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			
			ServletActionContext.getResponse().setHeader("Content-disposition", "attachment; filename=\"" + "plik" + ".xml\"");
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			try {
				out.write(URLDecoder.decode(xmlString, "UTF-8"));
			} catch(NullPointerException e) {
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class DockindSchema extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			SAXReader reader = new SAXReader();
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			try {
				InputStream stream = Docusafe.getServletContext().getResourceAsStream("/WEB-INF/DockindSchema.xsd");
				Document schema = reader.read(stream);
				out.write(schema.asXML());
			} catch(Exception e) {
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class DockindFields extends LoggedActionListener {

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			SAXReader reader = new SAXReader();
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			try {
				InputStream stream = Docusafe.getServletContext().getResourceAsStream("/WEB-INF/Fields.xml");
				Document xml = reader.read(stream);
				out.write(xml.asXML());
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	private class DockindTemplate extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			SAXReader reader = new SAXReader();
			PrintWriter out = getOutPrintWriter("text/xml; charset=utf-8");
			
			try {
				InputStream stream = Docusafe.getServletContext().getResourceAsStream("/WEB-INF/Template.xml");
				Document xml = reader.read(stream);
				out.write(xml.asXML());
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
				out.write("-1");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class ProcessFields extends LoggedActionListener {
		private class AjaxRequiredFields extends AjaxJsonResult {
			 Map<String, Map<String, Set<String>>> fields;
		}
		
		public void actionPerformed(ActionEvent event, Logger log) throws Exception 
		{
			PrintWriter out = getOutPrintWriter("text/json; charset=utf-8");
			AjaxRequiredFields ajaxResult = new AjaxRequiredFields();
			
			if(processName == null || processName.isEmpty()) {
				ajaxResult.success = false;
				ajaxResult.errorMessage = "Nie podano nazwy procesu";
			} else {
				ajaxResult.fields = DockindXMLUtils.getRequiredFieldsValuesFromProcess(Jbpm4Utils.getProcessResourceAsDocument(processName, Jbpm4Utils.JPDL));
			}
			
			out.write(ajaxResult.toJson());
			out.flush();
			out.close();
		}

		public Logger getLogger() 
		{
			return LOG;
		}
	}
	
	private class ProcessList extends LoggedActionListener {
		private class AjaxProcesses extends AjaxJsonResult {
			private class ProcessInfo {
				String name;
				String desc;
			}
			
			List<ProcessInfo> processes = new ArrayList<ProcessInfo>();
			
			void addProcess(ProcessDefinition procDef) {
				ProcessInfo procInfo = new ProcessInfo();
				procInfo.name = procDef.getName();
				procInfo.desc = procDef.getDescription();
				this.processes.add(procInfo);
			}

            void addActivitiProcess(String name) {
                ProcessInfo procInfo = new ProcessInfo();
                procInfo.name = name;
                procInfo.desc = name;
                this.processes.add(procInfo);
            }
			
		}
		
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			PrintWriter out = getOutPrintWriter("text/json; charset=utf-8");
			AjaxProcesses processList = new AjaxProcesses();

            /*List<ProcessDefinition> allProcs = Jbpm4Provider.getInstance().getRepositoryService()
					.createProcessDefinitionQuery().orderAsc(ProcessDefinitionQuery.PROPERTY_NAME)
					.list();
			SortedMap<String, ProcessDefinition> highestVersionProcs = new TreeMap<String, ProcessDefinition>(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					return o1.compareToIgnoreCase(o2);
				}
			});
			
			for(ProcessDefinition def : allProcs) {
				boolean put2map = false;
				
				if(highestVersionProcs.containsKey(def.getName())) {
					if(def.getVersion() > highestVersionProcs.get(def.getName()).getVersion()) {
						put2map = true;
					}
				} else {
					put2map = true;
				}
				
				if(put2map)
					highestVersionProcs.put(def.getName(), def);
			}
			for(Map.Entry<String, ProcessDefinition> entry : highestVersionProcs.entrySet()) {
				processList.addProcess(entry.getValue());
			}*/

            String [] names = Docusafe.getBeanNamesForType(DocuSafeProcessDefinition.class);
            for(String name : names) {
                LoggerFactory.getLogger("tomekl").debug("name : {}",name);
                processList.addActivitiProcess(name);
            }
			
			out.write(processList.toJson());
			out.flush();
			out.close();
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	public void setSaveXML(boolean saveXML) {
		this.saveXML = saveXML;
	}

	public boolean isSaveXML() {
		return saveXML;
	}

	public void setLoadXML(boolean loadXML) {
		this.loadXML = loadXML;
	}

	public boolean isLoadXML() {
		return loadXML;
	}

	public void setDockindCn(String dockindCn) {
		this.dockindCn = dockindCn;
	}

	public String getDockindCn() {
		return dockindCn;
	}

	public void setXml(FormFile xml) {
		this.xml = xml;
	}

	public FormFile getXml() {
		return xml;
	}

	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	public String getXmlString() {
		return xmlString;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}
}
