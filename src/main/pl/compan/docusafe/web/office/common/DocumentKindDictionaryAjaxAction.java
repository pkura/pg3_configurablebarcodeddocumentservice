package pl.compan.docusafe.web.office.common;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.DocumentKindDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.google.gson.Gson;
import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author Mariusz Kilajnczyk
 */
public class DocumentKindDictionaryAjaxAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(DocumentKindDictionaryAjaxAction.class);
	
	private String className;
	private String params;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{ 
			try
			{	
				if(params == null || params.length() < 3)
					return;
				
				List<Map<String,String>> result = null;				
				Class c = Class.forName(className);
				try 
				{
					Object o = c.getConstructor(new Class[0]).newInstance(new Object[0]);
					if(DocumentKindDictionary.class.isInstance(o))
					{
						Object[] args = params.split(",");
						result =  ((DocumentKindDictionary)o).search(args);
					}
				} 
				catch (Exception e)
				{
					throw new EdmException("Nie znaleziono definicji klasy " + className, e);
				}
				if(result == null || result.size() < 1) 
					return ;

				HttpServletResponse response = 	ServletActionContext.getResponse();
				response.setContentType("text/html; charset=iso-8859-2");				
		    	PrintWriter out = response.getWriter();	    	
		    	out.write(new Gson().toJson(result));
		    	out.flush();
		    	out.close(); 
			}
			catch (Exception e) {
				log.error("",e);
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private String toJSON(List<Map<String,String>> result)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Map<String, String> map : result) 
		{
			Set<String> keys = map.keySet();
			sb.append("{");
			for (String  key: keys) 
			{
				sb.append("\"").append(key).append(("\":\"")).append(map.get(key)).append("\",");
			}
			sb.delete(sb.length()-1, sb.length());
			sb.append("},");
		}
		sb.delete(sb.length()-1, sb.length());
		sb.append("]");
		return sb.toString();
	}
}
