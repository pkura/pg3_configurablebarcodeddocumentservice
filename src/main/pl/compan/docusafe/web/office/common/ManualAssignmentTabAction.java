package pl.compan.docusafe.web.office.common;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opensymphony.oscache.util.StringUtil;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.dockinds.process.WorkflowUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.HtmlUtils;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationTreeNode;
import pl.compan.docusafe.web.tree.organization.OrganizationTreeView;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Predicates.*;

/**
 * Klasa odpowiedzialna za r�czn� dekretacj� w procesach jbpm'owych, WorkflowAction by�o nie do odratowania
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class ManualAssignmentTabAction extends BaseTabsAction {
    private static final Logger LOG = LoggerFactory.getLogger(ManualAssignmentTabAction.class);

    //EXPORT
    private List<DSUser> users;
    private List<DSDivision> divisions;

    private String treeHtml = "";

    /** Czy mo�na utworzy� proces do wiadomo�ci */
    private boolean canCreateReadOnly;

    //IMPORT
    private String[] targetUsers = new String[0];
    private String[] targetGuids = new String[0];
    private String selectedGuid;
    private String[] activityIds;

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignment").
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
            append(new ManualAssignment()).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreateReadOnly").
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
            append(new CreateReadOnly()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    protected abstract List prepareTabs();

    private class FillForm extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            log.info("ManualAssignment : " + getDocumentType());
            setTabs(prepareTabs());
            users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);

            if(selectedGuid != null){
                DSDivision selectedDivision = DSDivision.find(selectedGuid);
                treeHtml = ExtendedOrganizationTree.createOrganizationTree(
                            selectedDivision,
                            null,
                            new ExpandUrlProvider(),
                            new SelectOrganizationTreeView(
                                        new SelectUrlProvider(),
                                        ServletActionContext.getRequest().getContextPath(),
                                        selectedDivision,
                                        null),
                            ServletActionContext.getRequest().getContextPath())
                        .generateTree();
            } else {
                treeHtml = ExtendedOrganizationTree.createOrganizationTree(
                            new ExpandUrlProvider(),
                            new SelectOrganizationTreeView(
                                        new SelectUrlProvider(),
                                        ServletActionContext.getRequest().getContextPath(),
                                        null,
                                        null),
                            ServletActionContext.getRequest().getContextPath())
                        .generateTree();
            }

            canCreateReadOnly = true;
            if(activityIds != null && activityIds.length > 0){
                activityIds = Sets.newHashSet( //usuwa duplikaty i nulle
                        Iterables.filter(
                                Arrays.asList(activityIds),
                                not(or(equalTo("null"), isNull()))))
                            .toArray(new String[0]);
                log.debug("activities = {}", Joiner.on('|').join(activityIds));
                DSApi.context().begin();
                for(String activityId : activityIds){
                    Jbpm4WorkflowFactory.acceptTaskIfNotAccepted(activityId, event);
                }
                DSApi.context().commit();
                canCreateReadOnly = false;
            } else {
                log.debug("activity = {}", getActivity());
                canCreateReadOnly = WorkflowUtils.canSpawnReadOnlyProcess(getActivity(),
                        Documents.officeDocument(getDocumentId(), getActivity()).getDocument().getDocumentKind());
            }
            
            divisions = Arrays.asList(DSDivision.getOnlyDivisions(false));//Arrays.asList(DSDivision.get);
        }
        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    private static class SelectOrganizationTreeView extends OrganizationTreeView {
        public SelectOrganizationTreeView(OrganizationUrlProvider urlProvider, String contextPath, DSDivision selectedDivision, DSUser selectedUser) {
            super(urlProvider, contextPath, selectedDivision, selectedUser);
        }

        @Override
        public String getUserHtml(OrganizationTreeNode node) {
            String substituted;
            try {
                substituted = node.getUser().getSubstitute() != null ?
                        " ("
                                + sm.getString("zastepowanyPrzez")
                                + " "
                                + node.getUser().getSubstitute().asLastnameFirstname()
                                + ")"
                        : "";
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                substituted = "";
            }

            if (!node.isBackup()) {
                return HtmlUtils.toHtmlAnchor(urlProvider.getUrl(null,node.getUser()), node.getUser().asLastnameFirstname())
                        + substituted;
            } else {
                return HtmlUtils.toHtmlAnchor(urlProvider.getUrl(null,node.getUser()), node.getUser().asLastnameFirstname(), "backup")
                        + substituted;
            }
        }
    }

    private static class SelectUrlProvider implements OrganizationUrlProvider{
        public String getUrl(DSDivision div, DSUser user) {
            if(div == null){
                return "javascript:selectUser('"+user.getName()+"');";
            } else {
                return "javascript:selectDivision('"+div.getGuid()+"');";
            }
        }
    }
    private static class ExpandUrlProvider implements OrganizationUrlProvider{
        public String getUrl(DSDivision div, DSUser user) {
            if(div == null){
                return "#";
            } else {
                return "javascript:expandDivision('"+div.getGuid()+"');";
            }
        }
    }

    private class CreateReadOnly extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            OfficeDocument od = OfficeDocument.find(getDocumentId());

            Map<String, Object> params = Maps.newHashMap();
            params.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES, Arrays.asList(getTargetUsers()));
            params.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS,     Arrays.asList(getTargetGuids()));
            
           
            	// aby mo�na by�o dekretowac recznie  do wiadomosci dla dzialow  trzeba ustawic -> readOnlyForMany = false
            if (AvailabilityManager.isAvailable("readOnlyForMany"))
            {
            	for (String user : getTargetUsers())
            	{
            		od.getDocumentKind()
            		.getDockindInfo().getProcessesDeclarations()
            		.getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME)
            		.getLogic().startProcess(od, Collections.<String, Object>singletonMap( Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,user));
            	}
            }
            else
            {
            	 od.getDocumentKind()
                 .getDockindInfo()
                 .getProcessesDeclarations()
                 .getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME)
                 .getLogic()
                 .startProcess(od,params);
            }
            
            TaskSnapshot.updateByDocumentId(od.getId(), "anyType");
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            event.setResult("after");
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    private class ManualAssignment extends TransactionalActionListener {

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
//            log.info("ManualAssignment.assign: " + getDocumentType() + "targetUsers: " + Arrays.toString(targetUsers));

            if (targetUsers.length == 0 && targetGuids.length == 0) {
                addActionError("Nie wybrano celu dekretacji.");
                return;
            }

            if (activityIds != null) { //dekretacja wielu task�w
                if (AvailabilityManager.isAvailable("ifpan") && activityIds.length > 1 && activityIds[0].equals(activityIds[1])) {
                    assign(activityIds[0], null);
                } else {
                    for (String activity : activityIds) {
                        //taskSnaphot = TaskSnapshot.getInstance().findByActivityKey(activity.replaceAll("[^,]*,","")).setAccepted(0);
//                    	log.error();
                    	
                    	if(AvailabilityManager.isAvailable("mosty")){
                    		try{
                    		boolean prezes = false;
                    		boolean ofertowy = false;
                    		String chairman_guid = Docusafe.getAdditionProperty("prezes_GUID");
                    		String offer_guid = Docusafe.getAdditionProperty("dzial_ofertowy_GUID");
                    		if(targetUsers.length > 0){
                    			for(String xxx: targetUsers){
                    				DSUser temp_u = DSUser.findByUsername(xxx);
                    				for(DSDivision temp_div: temp_u.getDivisions())
                    				{
                    					String temp_guid = temp_div.getGuid();
                    					boolean test = temp_guid.equalsIgnoreCase(chairman_guid);
                    					if(temp_div.getGuid().equalsIgnoreCase(chairman_guid)) prezes=true; //czy ma isc do prezesa
                    				}
                    			}
                    		}
                    		DSUser dsu = DSApi.context().getDSUser();
                    		for(DSDivision ddd : dsu.getDivisions()){
                    			String gd = ddd.getGuid();
                    			if(gd.equalsIgnoreCase(offer_guid)) ofertowy=true; //czy user jest z dzialu ofertowego
                    			
                    		}
                    		if(ofertowy && prezes){
                    		String cleanAct = activity.replaceAll("[^,]*,","");
                    		Long dId = Jbpm4ProcessLocator.documentIdForActivity(cleanAct);
                    		OfficeDocument od = OfficeDocument.find(dId);
                    		Map<String, Object> params = Maps.newHashMap();
                    		ArrayList<String> divy = new ArrayList<String>();
                    		divy.add(offer_guid);
                    		params.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, divy);
                    		od.getDocumentKind()
                    		.getDockindInfo()
                    		.getProcessesDeclarations()
                    		.getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME)
                    		.getLogic()
                    		.startProcess(od,params);
                    		}
                    		}catch(Exception e){
                    			log.error(e.getMessage(), e);
                    		}
                    	}
                        
                        assign(activity, null);

                        try{
                            Long docId = Jbpm4ProcessLocator.documentIdForActivity(activity.replaceAll("[^,]*,",""));
                            setDocumentId(docId);
                        }catch(EdmException e){
                            log.info("Nie powiod�o si� odnalezienie id dokumentu o activity " + activity + ":" + e.getMessage(), e);
                            throw e;
                        }
                    }
                }
            } else {
                assign(getActivity(), getDocumentId());

                if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                    JackrabbitXmlSynchronizer.createEvent(getDocumentId());
                }
            }
        }

	    private void assign(String activity, Long documentId) throws EdmException, DocumentLockedException {
            WorkflowFactory.getInstance().reassign(activity, DSApi.context().getPrincipalName(), ImmutableSet.copyOf(targetUsers), ImmutableSet.copyOf(targetGuids));
            if(documentId != null){
                TaskSnapshot.updateByDocumentId(documentId, getDocumentType());
            }
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
			if (getDocumentId() != null) {
                String assignee = "";
                DSDivision division = null;

                if(getTargetGuids() != null && getTargetGuids().length > 0){
                    for(String guid : getTargetGuids()){
                        division = DSDivision.find(guid);
                        assignee += division.getName() + " (";
                        for(DSUser user : division.getUsers(true)){
                            assignee += user.getWholeName() + ", ";
                        }
                        assignee = assignee.substring(0, assignee.length()-2) + "), ";
                    }
                }

                if(getTargetUsers() != null && getTargetUsers().length > 0){
                    for(String username : getTargetUsers()){
                        assignee += DSUser.findByUsername(username).getWholeName() + ", ";
                    }
                }

				addActionMessage("Przekazano zadanie dla dokumentu ID = " + getDocumentId() + ", numer KO: "
						+ OfficeDocument.find(getDocumentId()).getOfficeNumber() + " do: \n \n"
						+ assignee.substring(0, assignee.length()-2));
			}
			event.setResult("after");
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }



    public String[] getTargetUsers() {
        return targetUsers;
    }

    public void setTargetUsers(String[] targetUsers) {
        this.targetUsers = targetUsers;
    }

    public List<DSUser> getUsers() {
        return users;
    }

    public void setUsers(List<DSUser> users) {
        this.users = users;
    }

    public List<DSDivision> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<DSDivision> divisions) {
        this.divisions = divisions;
    }

    public String[] getTargetGuids() {
        return targetGuids;
    }

    public void setTargetGuids(String[] targetGuids) {
        this.targetGuids = targetGuids;
    }

    public String getTreeHtml() {
        return treeHtml;
    }

    public void setTreeHtml(String treeHtml) {
        this.treeHtml = treeHtml;
    }

    public String getSelectedGuid() {
        return selectedGuid;
    }

    public void setSelectedGuid(String selectedGuid) {
        this.selectedGuid = selectedGuid;
    }

    public String[] getActivityIds() {
        return activityIds;
    }

    public void setActivityIds(String[] activityIds) {
        this.activityIds = activityIds;
    }

    public boolean isCanCreateReadOnly() {
        return canCreateReadOnly;
    }

    public void setCanCreateReadOnly(boolean canCreateReadOnly) {
        this.canCreateReadOnly = canCreateReadOnly;
    }
}
