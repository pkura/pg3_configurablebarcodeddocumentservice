package pl.compan.docusafe.web.office.common;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.massimport.DocumentsFileReader;
import pl.compan.docusafe.web.office.massimport.FieldDataProvider;
import pl.compan.docusafe.web.office.massimport.FieldsHandler;
import pl.compan.docusafe.web.office.massimport.xls.XlsDocumentsFileReader;
import pl.compan.docusafe.web.office.out.DwrDocumentMainAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class ImportMainTabAction extends DwrDocumentMainAction {

    protected Logger log = LoggerFactory.getLogger(ImportMainTabAction.class);
    protected FieldDataProvider fieldDataProvider;
    //protected DocumentsFileReader documentsFileReader;
    protected Map dockindKeysBackup = new HashMap();
    protected String file;

    @Override
    public void setBeforeCreate(OfficeDocument document) throws Exception {
        super.setBeforeCreate(document);
        try {
            DocumentKind documentKind = DocumentKind.findByCn(getDocumentKindCn());
            DocumentLogic logic = documentKind.logic();
            List<Field> dockindFields = documentKind.getFields();
            //FieldsManager fieldsManager = document.getFieldsManager(); // String fm = documentKind.getFieldsManager(inDocumentId);
            FieldsHandler.Provider provider = logic instanceof FieldsHandler.Provider ? ((FieldsHandler.Provider) logic) : null;
            FieldsHandler fieldsHandler = FieldsHandler.getFildsParser(provider, getDefaultFieldsParser());

            //wstawic wartosci do dockindKeys, lub na dokument - z dobrym typem
            Set<String> keys = fieldDataProvider.getFieldsCns(fieldsHandler.getDictToFieldLinker());//new HashSet<String>(dockindKeys.keySet());
            for (String cn : keys) {
                Field field = documentKind.getFieldByCn(cn);
                fieldsHandler.update(field, document, dockindKeys, fieldDataProvider, cn);
            }
            //        Map<String, Object> values = fieldDataProvider.getValues();
            //        for (String cn : values.keySet()) {
            //            if (dockindKeys.containsKey(cn) && ((dockindKeys.get(cn) == null) || StringUtils.isBlank(dockindKeys.get(cn).toString()))) {
            //                Field field = documentKind.getFieldByCn(cn);
            //                fieldsHandler.update(field, document, dockindKeys, cn, values.get(cn));
            //            } else {
            //                documentsFileReader.addErrorNoColumnInDockind(cn);
            //            }
            //        }

            //        if (handler != null)
            //            handler.importDocument(document, dockindKeys, fieldDataProvider.getColumns(),fieldDataProvider.getRow());

            //        todo sprawdzenie wymagalnosci pol - to raczej nie jest po droolsach
            //        todo jezeli nowy wpis w slowniku, to nie bedzie np "RECIPIENT" ale b�da "RECIPIENT_PARAM", w metodzie bazowej zostsanie utworzony, ale i tak nie dodany do "dockindKeys"
            List<String> errorNoValues = new ArrayList<String>();
            for (Field field : dockindFields) {
                if (fieldsHandler.isRequired(field, dockindKeys))
                    errorNoValues.add(field.getName());
            }
            if (!errorNoValues.isEmpty())
                throw new EdmException("Brak warto�ci dla p�l: " + StringUtils.join(errorNoValues.toArray(new String[errorNoValues.size()]), ","));
        } catch (Exception e) {
            throw new Exception("Nie utworzono dokumentu: " + fieldDataProvider.getIdentifierDocumentRow() + ". " + e.getMessage(), e.getCause());
        }

    }

    protected abstract FieldsHandler getDefaultFieldsParser();

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public class Create extends DwrDocumentMainAction.Create {
        @Override
        public void create(ActionEvent event) {
            if (multiFiles.isEmpty()) {
                event.addActionError("Nie wybrano pliku z importowanymi dokumentami.");
            } else {
                try {
                    File file = multiFiles.get(0).getFile();
                    multiFiles.clear();

                    DocumentsFileReader documentsFileReader = XlsDocumentsFileReader.create(file, null);
                    backupDockindKeys();
                    while (documentsFileReader.hasNext()) {
                        fieldDataProvider = documentsFileReader.next();
                        super.create(event);
                        restoreDockindKeys();
                    }
                    documentsFileReader.close();
                } catch (IOException e) {
                    event.addActionError(e.getMessage());
                    log.error(e.getMessage(), e);
                }
            }
        }

        public void restoreDockindKeys() {
            dockindKeys.clear();
            dockindKeys.putAll(dockindKeysBackup);
        }

        public void backupDockindKeys() {
            dockindKeysBackup.clear();
            dockindKeysBackup.putAll(dockindKeys);
        }


//        public void createMultiDocuments(ActionEvent event) throws IOException {
//            if (StringUtils.isBlank(file)) {
//                event.addActionError("NoSpecialAttachment");
//
//
//                file = "C:\\Users\\Gosia\\Desktop\\dokumenty.xls";//todo - usunac, poprawic
//                //return;
//            }
//            java.io.InputStream fis = null;
//            try {
//                fis = new FileInputStream(file);
//                HSSFWorkbook workbook = new HSSFWorkbook(fis); //XSSFWorkbook
//
//                createMultiDocuments(event, workbook);
//
//                if (fieldDataProvider.isErrorColumns()) {
//                    DocumentKind documentKind = DocumentKind.findByCn(getDocumentKindCn());
//                    event.addActionError(documentKind.getName() + " nie ma pol " + StringUtils.join(fieldDataProvider.getErrorNoColumnsInDockind(), ", "));
//                }
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//            } finally {
//                if (fis != null)
//                    fis.close();
//            }
//        }
//        public void createMultiDocuments(ActionEvent event, HSSFWorkbook workbook) throws IOException {
//            for (int i = 0; i < workbook.getNumberOfSheets(); ++i) {
//                HSSFSheet sheet = workbook.getSheetAt(i);
//                createMultiDocuments(event, sheet);
//                fieldDataProvider.cleanErrorColumns();
//            }
//        }
//
//        public void createMultiDocuments(ActionEvent event, HSSFSheet sheet) throws IOException {
//            Iterator<Row> rowIterator = sheet.iterator();
//            Map<String, Integer> columns = XlsFieldDataProvider.loadColumnsHeader(rowIterator);
//
//            if (fieldDataProvider.prepareColumns(columns)) {
//                while (rowIterator.hasNext()) {
//                    Row row = rowIterator.next();
//                    if (fieldDataProvider.prepare(row)) {
//                        restoreDockindKeys();
//                        super.create(event);
//                    }
//                }
//            }
//
//            fieldDataProvider.resetData();
//        }
//
    }

}
