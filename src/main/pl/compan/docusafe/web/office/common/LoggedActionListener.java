package pl.compan.docusafe.web.office.common;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

/**
 * Prosty listener reaguj�cy na wyskakuj�ce wyj�tki. W przypadku nieobs�u�onego wyj�tku przerywa transakcj� (setRollbackOnly).
 * @author Micha� Sankowski
 */
public abstract class LoggedActionListener implements ActionListener{
	public final void actionPerformed(ActionEvent event) {
		try {
			actionPerformed(event, getLogger());
		} catch (Exception e) {
			handleException(e, event);
		}
	}

	/**
	 * @param event ActionEvent danej akcji
	 * @param log, Logger zwracany przez getLogger()
	 * @throws java.lang.Exception 
	 */
	public abstract void actionPerformed(ActionEvent event, Logger log) throws Exception;

	/**
	 * Logger wykorzystywany przez otoczk� w LoggedActionListener
	 * @return
	 */
	public abstract Logger getLogger();

	/**
	 * Domy�lna obs�uga wyj�tku (addActionError, log.error, setRollbackOnly). 
	 * @param e
	 * @param event
	 */
	public void handleException(Exception e, ActionEvent event){
		getLogger().error(e.getLocalizedMessage(), e);
		if(e.getCause() instanceof EdmException) {
			event.addActionError(e.getCause().getLocalizedMessage());
        } else {
			event.addActionError(e.getLocalizedMessage());
        }
		DSApi.context().setRollbackOnly();
	}
}
