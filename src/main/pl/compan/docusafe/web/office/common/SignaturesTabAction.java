package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentSignature;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.web.common.BeanBackedMap;
import std.fun;
import std.lambda;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * Akcja wyświetlająca informacje o podpisach złożonych na danym dokumencie.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class SignaturesTabAction extends BaseTabsAction
{
    private Collection<Map> signatureBeans;
    
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    protected abstract List prepareTabs();

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());
             
                List<DocumentSignature> signatures = DocumentSignature.findByDocumentId(getDocumentId());
                
                class mapper implements lambda<DocumentSignature, Map>
                {
                    public Map act(DocumentSignature signature)
                    {
                        BeanBackedMap result = new BeanBackedMap(signature,
                            "id", "ctime", "correctlyVerified","type","typeAsString");
                        try
                        {
                            result.put("author", DSUser.findByUsername(signature.getAuthor()));
                            result.put("typeAsString", signature.getTypeAsString());
                        }
                        catch (EdmException e)
                        {
                        }

                        return result;
                    }
                }

                final mapper mapper = new mapper();
                signatureBeans = fun.map(signatures, mapper);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    public Collection<Map> getSignatureBeans()
    {
        return signatureBeans;
    }
    
}
