package pl.compan.docusafe.web.office.common;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskPositionBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author Mariusz Kilajnczyk
 */
public class TaskPositionAjxAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;

	private final static Logger LOG = LoggerFactory.getLogger(TaskPositionAjxAction.class);
	
	private String activityKey;

	public String getActivityKey() {
		return activityKey;
	}

	public void setActivityKey(String activityKey) { 
		this.activityKey = activityKey;
	}

	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm extends LoggedActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
		{ 
			try
			{
				if(activityKey == null)
					throw new EdmException("Brak activityKey");
				
				TaskPositionBean bean = SnapshotUtils.getTaskPosition(activityKey);
				HttpServletResponse response = 	ServletActionContext.getResponse();
				response.setContentType("text/html; charset=iso-8859-2");				
		    	PrintWriter out = response.getWriter();	    	
		    	if(bean != null)
		    		out.write(bean.getStringToAjx());
		    	else
		    		out.write("Brak danych");
		    	out.flush();
		    	out.close();
			}
			catch (Exception e) {
				log.error("",e);
			}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
}
