package pl.compan.docusafe.web.office.common;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.LogFactory;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.util.encoders.Base64;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.base.DocumentSignature;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.swig.verification.verification;
import pl.compan.docusafe.core.swig.verification.verificationConstants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

import com.lowagie.text.Chapter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfReader;
import com.opensymphony.webwork.ServletActionContext;



/**
 * User: Michal Manski mailto:michal.manski@com-pan.pl
 * Date: 2006-05-17
 */
public class DocumentSignatureAction extends EventActionSupport
{
    private Long id;    /* id dokumentu zwiazanego z podpisem */
    private Long signatureId;  
    private String inFile;
    private String outFile;
    private boolean doSign;
    private boolean accept;
   // private boolean isOffice;

    // @EXPORT
    private String verificationDescription;
    private List<String> verificationWarnings;
    private int verificationResult;
    private String verificationInfo;
    private String signatureAuthor;
    private String signatureCtime;
    private Collection<Map> signatureBeans;

    // ALERT
    //private FormFile signatureFile;

    private Boolean useSignatureDebug = false;
    
    public Boolean getUseSignatureDebug() {
		return useSignatureDebug;
	}

	public void setUseSignatureDebug(Boolean useSignatureDebug) {
		this.useSignatureDebug = useSignatureDebug;
	}

	private String xml;
    
	private static final Logger log = LoggerFactory.getLogger(DocumentSignatureAction.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    protected void setup()
    {    
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateXMLDocument()).
            append(new GenerateDocument(true)).
            append(new SignDocument()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new SaveSignature()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("getSig").
            append(OpenHibernateSession.INSTANCE).
            append(new View(true)).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("getDoc").
            append(OpenHibernateSession.INSTANCE).
            append(new View(false)).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("showDoc").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateXMLDocument()).
            append(new GenerateDocument(true)).
            append(new ShowDocument()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doVerify").
            append(OpenHibernateSession.INSTANCE).
            append(new GenerateXMLDocument()).
            append(new VerifyXML()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDownload").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DownloadDocument()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAcceptFillForm").
	        append(OpenHibernateSession.INSTANCE).
	        append(new AcceptFillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doAccept").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GenerateDocument(false)).
	        append(new Accept()).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
//klasy od akceptacji
    private class AcceptFillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE)) 
                {                	
                    List<DocumentSignature> signatures = DocumentSignature.findByDocumentId(id);
                    
                    class mapper implements lambda<DocumentSignature, Map>
                    {
                        public Map act(DocumentSignature signature)
                        {
                            BeanBackedMap result = new BeanBackedMap(signature,
                                "id", "ctime", "correctlyVerified","type","typeAsString");
                            try
                            {
                                result.put("author", DSUser.findByUsername(signature.getAuthor()));
                                result.put("typeAsString", signature.getTypeAsString());
                            }
                            catch (EdmException e)
                            {
                            }

                            return result;
                        }
                    }

                    final mapper mapper = new mapper();
                    setSignatureBeans(fun.map(signatures, mapper));
                	
                	
                	if(!(DSApi.context().hasPermission(DSPermission.PISMO_PODPISANIE) && DSApi.context().hasPermission(DSPermission.PISMO_AKCEPTACJA)))
                	{
                		addActionError("BrakUprawnienDoAkceptacji");
                		accept = true;
                	}
                }
                else
                {
                	addActionError("BrakModuluCertyfikaty");
                	accept = true;
                }
            }
            catch (Exception e) 
            {
            	log.error("",e);
            	addActionError(e.getMessage());
			}
        }
    }
    
    private class Accept implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	File in = new File(inFile);
            try
            {
                if (hasActionErrors()) 
                {
                    event.setResult(ERROR);
                    return;
                }
                try 
                {
                    DSApi.context().begin();
                    Document document = Document.find(id);
                    DocumentSignature signature = new DocumentSignature();
                    signature.setDocument(document);
                    signature.setType(DocumentSignature.TYPE_ACCEPT);
                    signature.create();
                    signature.saveContent(in, DocumentSignature.BLOB_PDF);
                    //przy akceptacji nie generujemy xml  oraz nie posiadamy podpisu
                  //  signature.saveContent(out, DocumentSignature.BLOB_SIG);
                  //  signature.saveContent(xml, DocumentSignature.BLOB_XML);

                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.create("accept",
                                DSApi.context().getPrincipalName(),
                                sm.getString("ZaakceptowanoDokument"),
                                "ACCEPT", signature.getId()));
                    }

                    document.setBlocked(Boolean.valueOf(true));
                    document.changelog(DocumentChangelog.ACCEPT_DOCUMENT);
                    DSApi.context().commit();
                    accept = true;
                    addActionMessage(sm.getString("ZaakceptowanoDokument"));
                }
                catch (EdmException e)
                {
                	log.error("",e);
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
                finally
                {
                    in.delete();
                }
                if (hasActionErrors())
                    event.setResult(ERROR);
            }
            catch (Exception e) 
            {
            	log.error("",e);
            	addActionError(e.getMessage());
			}
        }
    }
//koniecklas do akceptacji
    
    private class GenerateXMLDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);
                OfficeDocument officeDocument = null;
                if (document instanceof OfficeDocument)
                    officeDocument = (OfficeDocument) document;

                xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<document><fields>";
                xml += "<id>" + document.getId() + "</id>";
                if (officeDocument != null)
                    xml += "<officeNumber>" + officeDocument.getOfficeNumber() + "</officeNumber>";

                if (officeDocument != null && officeDocument.getType() == DocumentType.INCOMING)
                    xml += "<incomingDate>"+DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate())+"</incomingDate>";
                xml += "<ctime>"+DateUtils.formatCommonDate(document.getCtime())+"</ctime>";
                xml += "<mtime>"+DateUtils.formatCommonDate(document.getMtime())+"</mtime>";
                xml += "<description>"+document.getDescription()+"</description>";
                xml += "<title>"+document.getTitle()+"</title>";

                if (officeDocument != null &&
                (officeDocument.getType() == DocumentType.INCOMING || officeDocument.getType() == DocumentType.OUTGOING))
                {
                    xml += "<sender>";
                    if (officeDocument instanceof InOfficeDocument)
                        xml += ((InOfficeDocument) officeDocument).getSender().getSummary();
                    else
                        xml += ((OutOfficeDocument) officeDocument).getSender().getSummary();
                    xml += "</sender><recipients>";
                    for (Iterator iter = officeDocument.getRecipients().iterator(); iter.hasNext();)
                    {
                        Recipient recipient = (Recipient) iter.next();
                        xml += "<recipient>" + recipient.getSummary() + "</recipient>";
                    }
                    xml += "</recipients>";
                }

              /*  List lstRemarks = officeDocument.getRemarks();
                xml += "<remarks>";
                for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
                {
                    Remark remark = (Remark) iter.next();
                    xml += "<remark>"+remark.getContent()+"</remark>";
                }
                xml += "</remarks>";
              */
                xml += "<attachments>";
                List<Attachment> attachments = document.getAttachments();
                for (int i=0; i < attachments.size(); i++)
                {
                    Attachment attachment = attachments.get(i);
                    xml += "<attachment><id>" + attachment.getId() + "</id><ctime>" +
                        DateUtils.formatCommonDate(attachment.getCtime())+ "</ctime>";
                    xml += "<version>" + attachment.getRevisions().size() + "</version>";

                    /* liczenie skrotu zawartosci zalacznika */
                    AttachmentRevision revision = attachment.getMostRecentRevision();
                    if (revision != null)
                    {
                        InputStream stream = revision.getBinaryStream();
                        ByteArrayOutputStream os = null;
                        if (stream != null)
                        {
                            os = new ByteArrayOutputStream();
                            byte[] buffer = new byte[8192];
                            int count;
                            while ((count = stream.read(buffer)) > 0)
                            {
                                os.write(buffer, 0, count);
                            }

                            os.close();
                            stream.close();
                            stream = null;
                        }
                        if (os != null)
                            xml += "<digest>"+getDigest(os.toByteArray())+"</digest>";
                    }
                    xml += "</attachment>";
                }
                xml += "</attachments>";

                xml += "</fields></document>";



                DSApi.context().commit();
            }
            catch (Exception e)
            {
                event.getLog().error(e.getMessage(), e);
                event.setResult(ERROR);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }



    private class GenerateDocument implements ActionListener
    {
    	/***
    	 * Czy podpis
    	 */
    	private boolean isSign = true;
    	
    	public GenerateDocument(boolean isSign)
    	{
    		this.isSign = isSign;
    	}
        public void actionPerformed(ActionEvent event)
        {
            Long[] documentIds = {id};

            File pdf = null;

            try
            {
                DSApi.context().begin();

                Document document = Document.find(id);

                // sprawdzam czy jest to dokument zawierający "załącznik pokompilacyjny", bo jeśli tak
                // to wówczas wygenerowany ma być troszkę inny
                
                pdf = File.createTempFile("docusafe_tmp_", ".pdf");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));
                String xmlDigest = "";
                if(isSign)
                	xmlDigest = getDigest(xml.getBytes());

                inFile = pdf.getCanonicalPath().replaceAll("\\\\", "\\\\\\\\");
    
                DSApi.context().commit();
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
                event.setResult(ERROR);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
                if (pdf != null) pdf.delete();
                pdf = null;
            }

        }
    }

    /**
     * Generuje początek pliku PDF ("postaci dokumentu do podpisu") dla dokumentów archiwalnych.
     * 
     * @param compiledDocument  true <=> jest to dokument zawierający "załącznik pokompilacyjny"
     */
    private Element createDocumentBeginning(boolean compiledDocument) throws EdmException
    {
        try
        {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 18);

            Document document = Document.find(id);

            Paragraph paragraph = new Paragraph(sm.getString("PismoOidentyfikatorze")+document.getId(), font);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingBefore(20);
            paragraph.setSpacingAfter(30);
            Chapter beginning = new Chapter(paragraph, 2);
            beginning.setNumberDepth(0);

            if (compiledDocument)
                addCompiledDocumentAttributes(beginning, document, font);
            else
            {
                beginning.add(new Paragraph(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(document.getCtime()), font));
    
                beginning.add(new Paragraph(sm.getString("DataOstatniejModyfikacji")+": "+DateUtils.formatCommonDate(document.getMtime()), font));
    
                beginning.add(new Paragraph(sm.getString("Tytul")+": "+document.getTitle(), font));
    
                beginning.add(new Paragraph(sm.getString("Opis")+": "+document.getDescription(), font));

            }
            
            if (compiledDocument)
            {
                Font smallFont = new Font(baseFont, 14);            
                addSignaturesInfo(beginning, document, smallFont);
            }
            return beginning;
        }
        catch (DocumentException e)
        {
        }
        catch (IOException e)
        {
        }
        return null;
    }

    /**
     * Generuje początek pliku PDF ("postaci dokumentu do podpisu") dla dokumentów kancelaryjnych.
     * 
     * @param compiledDocument  true <=> jest to dokument zawierający "załącznik pokompilacyjny"
     */
    private Element createOfficeDocumentBeginning(boolean compiledDocument) throws EdmException
    {

        try
        {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 18);
            Font smallFont = new Font(baseFont, 14);

            OfficeDocument officeDocument = OfficeDocument.findOfficeDocument(id);
            Paragraph paragraph = null;

            if (officeDocument.getType() == DocumentType.INCOMING)
                paragraph = new Paragraph(sm.getString("PismoPrzychodzace"), font);
            else if (officeDocument.getType() == DocumentType.OUTGOING)
                paragraph = new Paragraph(sm.getString("PismoWychodzace"), font);
            else if (officeDocument.getType() == DocumentType.INTERNAL)
                paragraph = new Paragraph(sm.getString("PismoWewnetrzne"), font);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingBefore(15);
            Chapter beginning = new Chapter(paragraph, 2);
            beginning.setNumberDepth(0);

            if (officeDocument.getOfficeNumber() != null) 
            {
                paragraph = new Paragraph(sm.getString("NumerKancelaryjny")+": "+officeDocument.getOfficeNumber()+"\n\n", font);
            }
            else 
            {
                paragraph = new Paragraph(sm.getString("Identyfikator")+": "+officeDocument.getId()+"\n\n", font);
            }
            paragraph.setAlignment(Element.ALIGN_CENTER);
            beginning.add(paragraph);

            if (compiledDocument)
                addCompiledDocumentAttributes(beginning, officeDocument, font);
            else
            {

                if (officeDocument.getType() == DocumentType.INCOMING)
                    beginning.add(new Paragraph(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate()), font));
                else
                    beginning.add(new Paragraph(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(officeDocument.getCtime()), font));
    
                beginning.add(new Paragraph(sm.getString("DataOstatniejModyfikacji")+": "+DateUtils.formatCommonDate(officeDocument.getMtime()), font));
    
                beginning.add(new Paragraph(sm.getString("Tytul")+": "+officeDocument.getSummary(), font));

                if (officeDocument.getType() == DocumentType.INCOMING || officeDocument.getType() == DocumentType.OUTGOING)
                {
                    beginning.add(new Paragraph(sm.getString("Nadawca")+": ", font));
                    if (officeDocument instanceof InOfficeDocument)
                        beginning.add(new Paragraph("    "+((InOfficeDocument) officeDocument).getSender().getSummary(), font));
                    else
                        beginning.add(new Paragraph("    "+((OutOfficeDocument) officeDocument).getSender().getSummary(), font));
                    beginning.add(new Paragraph("Odbiorcy: ", font));
                    for (Iterator iter = officeDocument.getRecipients().iterator(); iter.hasNext();)
                    {
                        Recipient recipient = (Recipient) iter.next();
                        beginning.add(new Paragraph("    "+recipient.getSummary(), font));
                    }
                }
            }

            List lstRemarks = officeDocument.getRemarks();
            com.lowagie.text.List remarks = new com.lowagie.text.List(false,10);
            boolean done = false;
            for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
            {
                if (!done) 
                {
                    paragraph = new Paragraph(sm.getString("Uwagi")+" :", smallFont);
                    paragraph.setSpacingBefore(20);
                    paragraph.setAlignment(Element.ALIGN_CENTER);
                    beginning.add(paragraph);
                    done = true;
                }
                Remark remark = (Remark) iter.next();
                remarks.add(new ListItem(remark.getContent(), smallFont));
            }
            beginning.add(remarks);

            if (compiledDocument)
                addSignaturesInfo(beginning, officeDocument, smallFont);
            return beginning;
        }
        catch (DocumentException e)
        {
        }
        catch (IOException e)
        {
        }
        return null;
    }

    /**
     * Generuje kawałek dokumentu do podpisu specyficzny  dla dokumentów zawierających "załącznik pokompilacyjny".
     */
    private void addCompiledDocumentAttributes(Chapter beginning, Document document, Font font) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        beginning.add(new Paragraph(sm.getString("DataOstatniejModyfikacji")+": "+DateUtils.formatCommonDate(document.getMtime()), font));
    }

    /**
     * Dodaje do "dokumentu do podpisu" kawałek zawierający informacje o wcześniej dokonanych podpisach elektronicznych.
     */
    private void addSignaturesInfo(Chapter beginning, Document document, Font font) throws EdmException
    {
        List<DocumentSignature> signatures = DocumentSignature.findByDocumentId(document.getId());
        boolean done = false;
        com.lowagie.text.List sigList = new com.lowagie.text.List(false,10);
        for (DocumentSignature signature : signatures)
        {
            if (!done) 
            {
                Paragraph paragraph = new Paragraph(sm.getString("PodpisElektroniczny")+":", font);
                paragraph.setSpacingBefore(20);
                paragraph.setAlignment(Element.ALIGN_CENTER);
                beginning.add(paragraph);
                done = true;
            }
            sigList.add(new ListItem(DSUser.safeToFirstnameLastname(signature.getAuthor())+"; "+DateUtils.formatJsDateTime(signature.getCtime())));
        }
        beginning.add(sigList);
    }

    private class SignDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            if (hasActionErrors()) 
            {
                event.setResult(ERROR);
                return;
            }

            doSign = true;
            //inFile = "E:\\\\test\\\\a.txt";
            outFile = inFile+DocumentSignature.SIG_EXTENSION;
        }
    }

    private class SaveSignature implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	LogFactory.getLog("tomekl").debug("Save signature");
            doSign = false;


            if (hasActionErrors()) 
            {
                event.setResult(ERROR);
                return;
            }

            File in = new File(inFile);
            LogFactory.getLog("tomekl").debug("infile "+inFile);
            LogFactory.getLog("tomekl").debug(""+in.getAbsolutePath());
            
            File out = new File(outFile);
            LogFactory.getLog("tomekl").debug("outfile"+outFile);
            LogFactory.getLog("tomekl").debug(""+out.getAbsolutePath());
            

            try {
                DSApi.context().begin();

                /* BEGIN ALERT
                if (!(signatureFile != null && signatureFile.sensible()))
                    throw new EdmException("Nie podano pliku z wygenerowanym podpisem");
                out = signatureFile.getFile();
                 END ALERT  */

                Document document = Document.find(id);

                DocumentSignature signature = new DocumentSignature();
                signature.setDocument(document);
                //signature.setIsOffice(Boolean.valueOf(document instanceof OfficeDocument));
                signature.setType(DocumentSignature.TYPE_SIG);

                signature.create();

                signature.saveContent(in, DocumentSignature.BLOB_PDF);
                signature.saveContent(out, DocumentSignature.BLOB_SIG);
                signature.saveContent(xml, DocumentSignature.BLOB_XML);

                if (document instanceof OfficeDocument)
                {
                    ((OfficeDocument) document).addWorkHistoryEntry(
                        Audit.create("signatures",
                            DSApi.context().getPrincipalName(),
                            sm.getString("PodpisanoDokument"),
                            "SIGN", signature.getId()));
                }

                document.setBlocked(Boolean.valueOf(true));

                document.changelog(DocumentChangelog.SIGN_DOCUMENT);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                /* usuwam pliku tymczasowe */
                in.delete();
                out.delete();
            }

            if (hasActionErrors())
                event.setResult(ERROR);
        }
    }

    /* private class VerifyPdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // jak byly bledy podczas generowania pdf - przerywam
            if (hasActionErrors()) {
                event.setResult(ERROR);
                return;
            }

            event.setResult("verification");

            File generatedPdf = new File(inFile);

            // wczytanie z bazy pliku podpisywanego na content
            // (w inFile mam juz wygenerowany pdf z aktualna zawartoscia)

            File content = null;

            BlobInputStream stream = null;
            try
            {
                // do odczytu bloba trzeba otworzyć transakcję
                DSApi.context().begin();

                DocumentSignature signature = DocumentSignature.find(signatureId);

                stream = signature.getBinaryStream(false);
                if (stream != null)
                {
                    content = File.createTempFile("docusafe_doc_", ".pdf");

                    FileOutputStream os = new FileOutputStream(content);
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }

                    os.close();
                    stream.close();
                    stream = null;

                }

                correctlyVerified = true;
                // porownanie plikow

                String generatedPdfDigest = getDigest(generatedPdf);
                String contentDigest = getDigest(content);

                correctlyVerified = generatedPdfDigest.equals(contentDigest);



                // weryfikacja podpisu z orginalnym plikiem


                // zapisanie wyniku
                signature.setCorrectlyVerified(correctlyVerified);

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }

        }
    }   */

    /**
     * Weryfikacja podpisu dwuczesciowo - najpierw sprawdzenie zgodnosci stanu w podpisywanym dokumencie
     * z aktualnym stanem aplikacji, a potem weryfikacja poprawnosci podpisu (korzystajac z biblioteki
     * verification.dll - ktora dokonuje weryfikacji za pomocy pemheart.dll).
     */
    private class VerifyXML implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            verificationResult = 0;

            // jak byly bledy podczas generowania xml - przerywam
            if (hasActionErrors()) {
                event.setResult(ERROR);
                return;
            }

            event.setResult("verification");

            BlobInputStream stream = null;
            //ByteArrayOutputStream os = null;
            File pdfFile = null;
            File sigFile = null;

            try
            {
                // do odczytu bloba trzeba otworzyć transakcję
                DSApi.context().begin();

                DocumentSignature signature = DocumentSignature.find(signatureId);
                Document document = signature.getDocument();

                verificationInfo = sm.getString("WeryfikacjaPodpisuZlozonegoPodDokumentemOID",document.getId());
                signatureCtime = DateUtils.formatJsDateTime(signature.getCtime());
                signatureAuthor = DSUser.findByUsername(signature.getAuthor()).asFirstnameLastname();

             /*   stream = signature.getBinaryStream(DocumentSignature.BLOB_XML);
                if (stream != null)
                {

                    os = new ByteArrayOutputStream();
                    //content = File.createTempFile("docusafe_doc_", ".xml");
                    //FileOutputStream os = new FileOutputStream(content);
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }

                    os.close();
                    stream.close();
                    stream = null;

                } */

                boolean correctlyVerified = true;

                // weryfikacja podpisu z orginalnym plikiem

                if (Docusafe.getVerificationLibraryLoaded())
                {

                    /* zapisujemy na dysk orginalny plik pdf */
                   /* stream = signature.getBinaryStream(DocumentSignature.BLOB_PDF);
                    if (stream != null)
                    {
                        pdfFile = File.createTempFile("docusafe_doc_", ".pdf");
                        FileOutputStream os = new FileOutputStream(pdfFile);
                        byte[] buffer = new byte[8192];
                        int count;
                        while ((count = stream.read(buffer)) > 0)
                        {
                            os.write(buffer, 0, count);
                        }
                        os.close();
                        stream.close();
                        stream = null;
                    }*/

                    /* zapisujemy na dysk podpisany plik (.pem) */
                    stream = signature.getBinaryStream(DocumentSignature.BLOB_SIG);
                    if (stream != null)
                    {
                        sigFile = File.createTempFile("docusafe_doc_", DocumentSignature.SIG_EXTENSION);
                        FileOutputStream os = new FileOutputStream(sigFile);
                        byte[] buffer = new byte[8192];
                        int count;
                        while ((count = stream.read(buffer)) > 0)
                        {
                            os.write(buffer, 0, count);
                        }
                        os.close();
                        stream.close();
                        stream = null;
                    }
                    /* tworzymy plik, na ktory zapiszemy wynik */
                    pdfFile = File.createTempFile("docusafe_doc_", ".pdf");
                    String profileName = Configuration.getProperty("signature.verification.pemheart_profile");
                    String pin = Configuration.getProperty("signature.verification.pin");
                    if (profileName == null || pin == null)
                        throw new EdmException(sm.getString("NiezdefiniowanoWdocusafe.propertiesNazwyProfiluLubPinuDlaPEMHEARTa"));
                    verificationResult = verification.verifyNormalSignature(profileName, pin, sigFile.getAbsolutePath(), pdfFile.getAbsolutePath());

                    if (verificationResult == verificationConstants.VERIFY_ERROR)
                        throw new EdmException(verification.getDescription());

                  
                    correctlyVerified = !(verificationResult == verificationConstants.VERIFY_NEG || verificationResult == verificationConstants.VERIFY_NOT_VER);
                    verificationDescription = verification.getDescription();
   
                   // String tmpverificationDescription = verificationDescription;
                  /*  tmpverificationDescription += "\n"+("1"+new String(verificationDescription.getBytes("ISO-8859-2")));
                    tmpverificationDescription += "\n"+("2"+new String(verificationDescription.getBytes("iso-8859-2")));
                    tmpverificationDescription += "\n"+("3"+new String(verificationDescription.getBytes("cp1250")));
                    tmpverificationDescription += "\n"+("4"+new String(verificationDescription.getBytes(),"iso-8859-2"));
                    tmpverificationDescription += "\n"+("5"+new String(verificationDescription.getBytes(),"cp1250"));
                    tmpverificationDescription += "\n"+("6"+new String(verificationDescription.getBytes("UTF-8")));
                    tmpverificationDescription += "\n"+("7"+new String(verificationDescription.getBytes(),"UTF-8"));
                    tmpverificationDescription += "\n"+("8"+new String(verificationDescription.getBytes("cp1250"),"iso-8859-2"));
                    tmpverificationDescription += "\n"+("9"+new String(verificationDescription.getBytes("iso-8859-2"),"iso-8859-2"));
                    tmpverificationDescription += "\n"+("10"+new String(verificationDescription.getBytes("iso-8859-2"),"cp1250"));
                    tmpverificationDescription += "\n"+("11"+new String(verificationDescription.getBytes("cp1250"),"iso-8859-2"));
                    tmpverificationDescription += "\n"+("12"+new String((new String(verificationDescription.getBytes(),"cp1250")).getBytes("iso-8859-2")));
                  */

                   /* tmpverificationDescription += "\n"+(new String(new String(verificationDescription.getBytes(),"utf-8").getBytes("iso-8859-2")));

                    tmpverificationDescription += "\n"+(new String(new String(verificationDescription.getBytes(),"cp1250").getBytes("iso-8859-2")));
                    tmpverificationDescription += "\n"+(new String(new String(verificationDescription.getBytes(),"cp852").getBytes("iso-8859-2")));
                    tmpverificationDescription += "\n"+(new String(new String(verificationDescription.getBytes(),"cp1252").getBytes("iso-8859-2")));

                    tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes(),"utf-8"));
                     tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes("utf-8")));
                     tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes("cp1250"), "iso-8859-2"));
                     tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes(),"utf-8").getBytes("iso-8859-2"));
                     tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes(),"cp1250").getBytes("iso-8859-2"));
                    //tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes(""),"cp1250"));
                    tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes("utf-8"),"iso-8859-2"));
                    //tmpverificationDescription += "\n"+(new String(verificationDescription.getBytes(),"Mazovia"));
                    verificationDescription = tmpverificationDescription;*/
                    int warningsCount = verification.getWarningsCount();

                    verificationWarnings = new ArrayList<String>();
                    for (int i=0; i < warningsCount; i++)
                    {
                        String warning = verification.getWarning(i);
                        verificationWarnings.add(warning);
                    }

                    if (document instanceof OfficeDocument)
                    {
                        String str;
                        if (verificationResult == verificationConstants.VERIFY_OK)
                            str = "poprawnie";
                        else if (verificationResult == verificationConstants.VERIFY_PART_OK)
                            str = "niekompletnie";
                        else
                            str = "negatywnie";

                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.create("signatures",
                                DSApi.context().getPrincipalName(),
                                sm.getString("PodpisZlozonyPodDokumentemPrzezDniaZweryfikowano",DSUser.findByUsername(signature.getAuthor()).asFirstnameLastname(),DateUtils.formatJsDateTime(signature.getCtime()),str),
                                "VERIFY", signature.getId()));
                    }
                }
                else
                {
                    verificationResult = verificationConstants.VERIFY_ERROR;
                    throw new EdmException(sm.getString("NiePowiodloSieWczytanieBibliotekiWeryfikującej")+" - verification.dll");
                }

                // sprawdzenie czy zawartosc ulegla zmianie
                if (correctlyVerified)
                {
                    stream = signature.getBinaryStream(DocumentSignature.BLOB_PDF);

                    String generatedXMLDigest = getDigest(xml.getBytes());

                    InputStream inStream = new FileInputStream(pdfFile);
                    PdfReader pdfReader = new PdfReader(inStream);
                    Object object = pdfReader.getInfo().get("Keywords");
                    String contentDigest = null;
                    if (object instanceof String)
                        contentDigest = (String) object;

                    correctlyVerified = generatedXMLDigest.equals(contentDigest);

                }

                // zapisanie wyniku
                signature.setCorrectlyVerified(correctlyVerified);

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                //if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                //if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                if (sigFile != null) sigFile.delete();
                if (pdfFile != null) pdfFile.delete();
                //DSApi._close();
            }

        }
    }


    private class View implements ActionListener
    {
        private boolean viewSignature;

        public View(boolean viewSignature)
        {
            this.viewSignature = viewSignature;
        }

        public void actionPerformed(ActionEvent event)
        {
            event.setResult("view");
            doSign = false;

            if (signatureId == null)
                return;

            File content = null;
            String filename = null;
            boolean success = false;

            BlobInputStream stream = null;
            try
            {
                // do odczytu bloba trzeba otworzyć transakcję
                DSApi.context().begin();

                DocumentSignature signature = DocumentSignature.find(signatureId);
                Document document = signature.getDocument();

                filename = "document"+document.getId()+".pdf";
                if (viewSignature)
                    filename = filename + DocumentSignature.SIG_EXTENSION;

                stream = signature.getBinaryStream(viewSignature ? DocumentSignature.BLOB_SIG : DocumentSignature.BLOB_PDF);
                if (stream != null)
                {
                    content = File.createTempFile("docusafe_doc_sig", ".tmp");

                    FileOutputStream os = new FileOutputStream(content);
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }
                    os.close();
                    stream.close();
                    stream = null;
                }

                DSApi.context().commit();

                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }

            if (success && content != null)
            {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
                //response.setHeader("Content-Disposition", "inline");
                response.setContentLength((int) content.length());
                try
                {
                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(content);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
                finally
                {
                    content.delete();
                }
            }
        }
    }

    /**
     * akcja sciagajaca plik o nazwie inFile z serwera do klienta (wykorzystywane tez podczas podpisywania zalacznikow)
     */
    private class DownloadDocument implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {

            event.setResult("view");
            doSign = false;

            File content = new File(inFile);
            String filename = content.getName();//"document.pdf";
            if (content != null)
            {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
                //response.setHeader("Content-Disposition", "inline");
                response.setContentLength((int) content.length());
                try
                {
                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(content);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
            }
        }
    }
    
    public class ShowDocument implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            xml = null;        // bo juz niepotrzebny dalej
            File file = null;
            try
            {
                file = new File(inFile);
                ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(file),
                    "application/pdf", (int) file.length());
            }
            catch (IOException e)
            {
                event.getLog().error("", e);
            }
            finally
            {
                file.delete();
            }
        }
    }
    
  /*  static public String getDigest(String filename)
    {
    	File datafile = new File(filename);
    	return getDigest(filename);
    }
    */

    static public String getDigest(File datafile) throws Exception
    {
        FileInputStream in = new FileInputStream(datafile);
		byte[] raw_data_bytes = new byte[(int) datafile.length()];
		in.read(raw_data_bytes);
        return getDigest(raw_data_bytes);
    }

    static public String getDigest(byte[] raw_data_bytes) throws Exception
    {
		SHA1Digest digEng = new SHA1Digest();
		digEng.update( raw_data_bytes, 0, raw_data_bytes.length );
		byte [] digest = new byte[digEng.getDigestSize()];
		digEng.doFinal(digest, 0);

		// Encode the digest into ASCII format for XML
		return (new String(Base64.encode(digest)));
	}
    
    public Long getId()
    {
        return id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getSignatureId()
    {
        return signatureId;
    }
    
    public void setSignatureId(Long signatureId)
    {
        this.signatureId = signatureId;
    }
    
    public String getInFile()
    {
        return inFile;
    }
    
    public void setInFile(String inFile)
    {
        this.inFile = inFile;
    }
    
    public String getOutFile()
    {
        return outFile;
    }
    
    public void setOutFile(String outFile)
    {
        this.outFile = outFile;
    }
    
    public boolean isDoSign()
    {
        return doSign;
    }
    
    public void setDoSign(boolean doSign)
    {
        this.doSign = doSign;
    }

/*    public void setIsOffice(boolean isOffice)
    {
        this.isOffice = isOffice;
    }

    public boolean isIsOffice()
    {
        return isOffice;
    }*/

    public String getXml()
    {
        return xml;
    }

    public void setXml(String xml)
    {
        this.xml = xml;
    }

    public String getVerificationDescription()
    {
        return verificationDescription;
    }

    public List<String> getVerificationWarnings()
    {
        return verificationWarnings;
    }

    public int getVerificationResult()
    {
        return verificationResult;
    }

    public int getVERIFY_OK()
    {
        return verificationConstants.VERIFY_OK;
    }

    public int getVERIFY_NOT_VER()
    {
        return verificationConstants.VERIFY_NOT_VER;
    }

    public int getVERIFY_NEG()
    {
        return verificationConstants.VERIFY_NEG;
    }

    public int getVERIFY_PART_OK()
    {
        return verificationConstants.VERIFY_PART_OK;
    }

    public int getVERIFY_ERROR()
    {
        return verificationConstants.VERIFY_ERROR;
    }

    public String getVerificationInfo()
    {
        return verificationInfo;
    }

    public String getSignatureCtime()
    {
        return signatureCtime;
    }

    public String getSignatureAuthor()
    {
        return signatureAuthor;
    }
    /* ALERT
    public FormFile getSignatureFile()
    {
        return signatureFile;
    }

    public void setSignatureFile(FormFile signatureFile)
    {
        this.signatureFile = signatureFile;
    } */

	public void setAccept(boolean accept)
	{
		this.accept = accept;
	}

	public boolean isAccept()
	{
		return accept;
	}

	public void setSignatureBeans(Collection<Map> signatureBeans)
	{
		this.signatureBeans = signatureBeans;
	}

	public Collection<Map> getSignatureBeans()
	{
		return signatureBeans;
	}
}

