package pl.compan.docusafe.web.office.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry.EntryType;
import pl.compan.docusafe.core.office.AssignmentHistoryEntryTargets;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.common.collect.Lists;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentHistoryTabAction.java,v 1.40 2009/03/27 08:34:15 mariuszk Exp $
 */
public abstract class AssignmentHistoryTabAction extends BaseTabsAction
{
    private static final Logger log = LoggerFactory.getLogger(AssignmentHistoryTabAction.class);
    private static StringManager sm = GlobalPreferences.loadPropertiesFile(AssignmentHistoryTabAction.class.getPackage().getName(), null);
    
    private String currentAssignmentUser;
    private String currentAssignmentDivision;
    private String participantsDescription;
    private Boolean currentAssignmentAccepted;
    private Boolean currentCoor;
    private List<AssignmentHistoryBean> assignmentHistory;

    private static StringManager smL;

    private boolean internal;

    protected void setup(){
        if (WorkflowFactory.jbpm) {
            registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillFormJbpm4()).
                appendFinally(CloseHibernateSession.INSTANCE);
        } else {
            registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillFormInternal()).
                appendFinally(CloseHibernateSession.INSTANCE);
        }
    }

    /**
     * Implementacja metody będzie wywoływana w ramach otwartego
     * kontektu Hibernate.
     */
    protected abstract List prepareTabs();


    private class FillFormJbpm4 extends LoggedActionListener {

        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {

            LoggerFactory.getLogger("tomekl").debug("FillFormJbpm4");

        	setTabs(prepareTabs());
            log.info("activityId = {}", getActivity());
            //przerabiam na adsy dla itwla tylko taka historia dekretacj przy nowej zakłądce manualmulti assignment
            //assignmentHistory = !AvailabilityManager.isAvailable("manual-multi-assignment") ? fromJbpm4History(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity())) : manualMultiAssignmentHistory(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity()));
            if (AvailabilityManager.isAvailable("PAA")){
                assignmentHistory= fromJbpm4History(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity()));
            }
            else  if (AvailabilityManager.isAvailable("assignmentHistoryFromJbpm4"))
            {
                assignmentHistory = fromJbpm4History(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity()));
            }
            else
            {
                assignmentHistory = !AvailabilityManager.isAvailable("manual-multi-assignment") ?
                        fromJbpm4History(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity())) :
                        manualMultiAssignmentHistory(Jbpm4WorkflowFactory.provideHistory(getDocumentId(), getActivity()));
            }
            
            
            if(getActivity() != null)
            {
                participantsDescription = Jbpm4WorkflowFactory.getParticipantsDescription(getActivity());
            }
            else
            {
            	StringBuilder sb = new StringBuilder();
            	List<String>  list = (List<String>) Jbpm4ProcessLocator.taskIds(getDocumentId());
            	for (String taskIdString : list) {
					sb.append(Jbpm4WorkflowFactory.getParticipantsDescription(taskIdString));
				}
            	participantsDescription = sb.toString();
            }
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class FillFormInternal implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
            try
            {
                setTabs(prepareTabs());
                OfficeDocument document = OfficeDocument.findOfficeDocument(getDocumentId());

                internal = document instanceof OutOfficeDocument && ((OutOfficeDocument) document).isInternal();

                if (document.getAssignmentHistory() != null) {
                    assignmentHistory = new ArrayList<AssignmentHistoryBean>(document.getAssignmentHistory().size());
                    List<AssignmentHistoryEntry> assignmentHistoryCopy = new ArrayList<AssignmentHistoryEntry>(document.getAssignmentHistory());
                    Collections.reverse(assignmentHistoryCopy);
                    for (Iterator<AssignmentHistoryEntry> iter=assignmentHistoryCopy.iterator(); iter.hasNext(); )
                    {
                        assignmentHistory.add(new AssignmentHistoryBean(iter.next()));
                    }
                }

                WorkflowActivity wa = null;
                String currUser = null;
                String currDiv = null;
                Boolean currAccept = null;
                ProcessCoordinator pc = null;
                if (document.getDocumentKind() != null &&  document.getDocumentKind().logic() != null) {
                	pc = document.getDocumentKind().logic().getProcessCoordinator(document);
                }

            	if (getActivity() != null) {
            		wa = WorkflowFactory.getWfActivity(getActivity());
            		currDiv = wa.getCurrentGuid();
            		currUser = wa.getCurrentUser();
            		currAccept = wa.getAccepted();
        			if (pc != null) {
        				if (currUser != null) {
        					currentCoor = currUser.equals(pc.getUsername());
        				} else {
        					currentCoor = currDiv.equals(pc.getGuid());
        				}
        			}
            	}
                try
                {
                	if (currUser == null && getActivity() == null) {
                		String activityId = WorkflowFactory.getInstance().findManualTask(getDocumentId());
                		if (activityId != null) {
                			currUser = WorkflowFactory.getInstance().getWfActivity(activityId).getCurrentUser();
                		}
                		if (currUser == null || currUser.length() == 0) {
                			currUser = document.getCurrentAssignmentUsername();
                		}
                		if (pc != null) {
            				if (currUser != null) {
            					currentCoor = currUser.equals(pc.getUsername());
            				}
            			}
                	}
	                if (currUser != null)       
	                    currentAssignmentUser = DSUser.safeToFirstnameLastnameName(currUser);                    
                 //   else if (document.getAuthor() != null)
                 //       currentAssignmentUser = DSUser.safeToFirstnameLastnameName(document.getAuthor());
                }
                catch (UserNotFoundException e)
                {
                    currentAssignmentUser = currUser;
                }

                try
                {
                	if (currDiv == null) {
                		String activityId = WorkflowFactory.getInstance().findManualTask(getDocumentId());
                		if (activityId != null) {
                			currDiv = WorkflowFactory.getInstance().getWfActivity(activityId).getCurrentGuid();
                		} else {
                			currDiv = document.getCurrentAssignmentGuid();
                		}
                		if (pc != null) {
            				if (currDiv != null) {
            					currentCoor = DSDivision.find(currDiv).getGuid().equals(pc.getGuid());
            				}
            			}
                	}
                    if (currDiv != null)
                        currentAssignmentDivision = DSDivision.find(currDiv).getPrettyPath();
                }
                catch (DivisionNotFoundException e)
                {
                    if (currDiv != null)
                        currentAssignmentDivision = currDiv+ " [ "+ smL.getString("usunieta") +"]";
                    else
                        currentAssignmentDivision = "";
                }

                if (currAccept == null) {
                	currAccept = document.isCurrentAssignmentAccepted();
                }
                currentAssignmentAccepted = currAccept;
              
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    //manual-multi-assignment
    public static List<AssignmentHistoryBean> manualMultiAssignmentHistory(List<AssignmentHistoryEntry> historyList) throws EdmException {
        LoggerFactory.getLogger("tomekl").debug("manualMultiAssignmentHistory");
        List<AssignmentHistoryBean> history = Lists.newArrayList();
        
        AssignmentHistoryBean bean = null;
        boolean initDone = false;
        for(AssignmentHistoryEntry entry : historyList) {
            bean = new AssignmentHistoryBean();
            bean.user = DSUser.findByUsername(entry.getSourceUser()).asLastnameFirstname();
            bean.date = entry.getCtime();
            bean.accepted = false;

            if(entry.getSourceGuid() != null && !entry.getSourceGuid().equals("x")) {
                bean.divisions = " (" + entry.getSourceGuid() +")";
            }

            if(entry.getType() == null) {
                continue;
            }

            StringTemplate template = null;
            if(EntryType.JBPM_INIT.equals(entry.getType()) && !initDone) {
                template = new StringTemplate(sm.getString("mma.JBPM_INIT"));
                initDone = true;
            }
            else if(EntryType.JBPM_INIT.equals(entry.getType())) {
                continue;
            } else if(EntryType.JBPM4_REASSIGN_SINGLE_USER.equals(entry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_SINGLE_USER"));
            } else if(EntryType.JBPM4_REASSIGN_DIVISION.equals(entry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_DIVISION"));
            } else if(EntryType.JBPM4_ACCEPT_TASK.equals(entry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_ACCEPT_TASK"));
            } else {
                template = new StringTemplate(sm.getString("jbpm4."+entry.getType()));
            }

            if(template != null && template.toString().length() > 0) { 
            	String processName = entry.getProcessName();
            	// \\d Any digit, short for [0-9]
            	processName = processName.split("-\\d")[0];
                template.setAttribute("type",("mma."+ processName).equals(sm.getString("mma."+ processName)) ? null : sm.getString("mma."+ processName));
                template.setAttribute("targetUser",entry.getTargetUser() != null && !entry.getTargetUser().equals("x") ? DSUser.findByUsername(entry.getTargetUser()).asFirstnameLastname() : null);
                template.setAttribute("targetDivision",entry.getTargetGuid() != null && !entry.getTargetGuid().equals("x") ? DSDivision.find(entry.getTargetGuid()).getName() : null);
                template.setAttribute("clerk",entry.getClerk() != null ? DSUser.findByUsername(entry.getClerk()).asFirstnameLastname() : null);
                template.setAttribute("deadline",String.valueOf("BRAK").equals(entry.getDeadline()) ? null : entry.getDeadline());
                template.setAttribute("objective","x".equals(entry.getObjective()) ? null : entry.getObjective());
                bean.content = template.toString();
            } else {
                bean.content = String.valueOf(entry.getType());
            }

            history.add(bean);
        }

        return history;
    }

    public static List<AssignmentHistoryBean> fromJbpm4History(List<AssignmentHistoryEntry> history) throws EdmException {
    	  
    	Collections.reverse(history);
    	List<AssignmentHistoryBean> ret = Lists.newArrayListWithExpectedSize(history.size());
        boolean isStarted = false;
        String author = null;
      
        for(AssignmentHistoryEntry entry : history){
        	//jesli ze starego workflow
        	if(entry.getType() == null)
        	{
        		ret.add(new AssignmentHistoryBean(entry));
        		continue;
        	}
			if (isStarted && "start-process".equals(entry.getObjective()))
				continue;
			if ("start-process".equals(entry.getObjective()))
			{
				isStarted = true;
				author = entry.getSourceUser();
			}
            AssignmentHistoryBean bean = new AssignmentHistoryBean();
            bean.user = DSUser.findByUsername(entry.getSourceUser()).asLastnameFirstname();

            bean.substitute = getSubstituteDesc(entry);
            bean.date = entry.getCtime();
            bean.accepted = false;
            if(entry.getStatus() != null && !entry.getStatus().equals("x") && StringUtils.isNotEmpty(entry.getStatus()))
            {
          	    if (entry.getType().equals(EntryType.JBPM4_REASSIGN) ||
          			  entry.getType().equals(EntryType.JBPM4_REASSIGN_DIVISION) ||
          			  entry.getType().equals(EntryType.JBPM4_REASSIGN_ME) ||
          			  entry.getType().equals(EntryType.JBPM4_REASSIGN_SINGLE_USER))

                    bean.status = " do etapu " + entry.getStatus();
          	    else
          	        bean.status = " na etapie " + entry.getStatus();
          		  
            }
            if(entry.getSourceGuid() != null && !entry.getSourceGuid().equals("x"))
            {
            	bean.divisions = " (" + entry.getSourceGuid() +")";
            }
            StringTemplate template = null;
			if (entry.getType() == AssignmentHistoryEntry.EntryType.JBPM4_PROCESS_ACTION) {
				template = new StringTemplate(sm.getString("jbpm4." + entry.getProcessName() + "." + entry.getObjective().replaceAll("\\s", "")));
			} 
			else if (entry.getType() == AssignmentHistoryEntry.EntryType.JBPM4_CONFIRMED && !entry.getSourceUser().equals(author))
			{
                template = new StringTemplate(sm.getString("jbpm4.JBPM4_CONFIRMED"));
			}
			else if(entry.getType() == AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN)
			{
                LoggerFactory.getLogger("tomekl").debug("JBPM4_REASSIGN 1");
				StringBuffer users = new StringBuffer();
				StringBuffer divisions = new StringBuffer();
				
				int divisionSize = 0;
				
				for(AssignmentHistoryEntryTargets t : entry.getAssignmentHistoryTargets())
				{
					if(AssignmentHistoryEntryTargets.USER == t.getType().intValue())
					{
						users.append(t.toPrettyString());
						users.append(" ,");
					}
					else
					{
						divisions.append(t.toPrettyString());
						divisions.append(" ,");
						divisionSize++;
					}
				}
				
				
				users.setLength(users.length() > 0 ? users.length()-1 : users.length());
				divisions.setLength(divisions.length() > 0 ? divisions.length()-1 : divisions.length());

                if(users.length() > 0 && divisions.length() > 0)
				{
                    if(!AvailabilityManager.isAvailable("manual-multi-assignment")) {
					    template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_USR_DIV",users.toString(),divisions.toString()));
                    } else {
                        template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_DIV_WHY_WHEN",users.toString(),divisions.toString(),entry.getObjective(),entry.getDeadline()));
                    }
				}
				else if(users.length() > 0)
				{
                    if(!AvailabilityManager.isAvailable("manual-multi-assignment")) {
					    template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_USR",users.toString()));
                    } else {
                        template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_USR_WHY_WHEN",users.toString(),entry.getObjective(),entry.getDeadline()));
                    }
				}
				else if(divisions.length() > 0)
				{
                    if(!AvailabilityManager.isAvailable("manual-multi-assignment")) {
					    template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_DIV",divisions.toString()));
                    } else {
                    	if(divisionSize==1)
                    		template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_TO_ONE_DIV_WHY_WHEN",divisions.toString(),entry.getObjective(),entry.getDeadline()));
                    	else
                    		template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_DIV_WHY_WHEN",divisions.toString(),entry.getObjective(),entry.getDeadline()));
                    }
				}
				else
				{
                    if(!AvailabilityManager.isAvailable("manual-multi-assignment")) {
					    template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN"));
                    } else {
                        template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_WHY_WHEN",entry.getObjective(),entry.getDeadline()));
                    }
				}
			}
			else 
			{
                if(!AvailabilityManager.isAvailable("manual-multi-assignment")) {
                    template = new StringTemplate(sm.getString("jbpm4." + entry.getType(), entry.getProcessName().contains("read") &&  entry.getProcessName().contains("only") ? sm.getString("doWiadomosci") : sm.getString("doRealizacji"),entry.getProcessName()));
                } else {
                    if(EntryType.JBPM4_REASSIGN_SINGLE_USER.equals(entry.getType()) && entry.getObjective() != null && !entry.getObjective().equals("x")) {
                        String lastFirstName = DSUser.findByUsername(entry.getTargetUser()).getLastnameFirstnameName();
                        template = new StringTemplate(sm.getString("jbpm4.JBPM4_REASSIGN_SINGLE_USER_WHY_WHEN", lastFirstName, entry.getObjective(), entry.getDeadline()));
                    } else if(EntryType.JBPM_INIT.equals(entry.getType())) {
                        template = new StringTemplate(sm.getString("mma.JBPM_INIT"));
                        //template = new StringTemplate(sm.getString("jbpm4.JBPM_INIT_WHY_WHEN",entry.getObjective(),entry.getDeadline()));
                    } else {
                        template = new StringTemplate(sm.getString("jbpm4." + entry.getType(), entry.getProcessName().contains("read") &&  entry.getProcessName().contains("only") ? sm.getString("doWiadomosci") : sm.getString("doRealizacji"),entry.getProcessName()));
                    }
                }
			}
            //TODO dodać więcej atrybutów w miare potrzeby
            template.setAttribute("sourceUser", bean.user);
            if(entry.getTargetUser() != null && !entry.getTargetUser().equals("x")){
                template.setAttribute("targetUser", DSUser.findByUsername(entry.getTargetUser()).asLastnameFirstname());
            }
            if(entry.getTargetGuid() != null && !entry.getTargetGuid().equals("x")){
                template.setAttribute("targetDivision", DSDivision.find(entry.getTargetGuid()).getName());
            }
            bean.content = template.toString() + " "; 
            ret.add(bean);
        }
        return ret;
    }

    private static String getSubstituteDesc(AssignmentHistoryEntry entry) throws EdmException {
        String result = null;
        String subst = entry.getSubstituteUser();
        if (subst != null)
        {
            result = " - " + sm.getString("wZastepstwieZa") + ": " + DSUser.findByUsername(entry.getSubstituteUser()).asLastnameFirstname();
        }
        return result;
    }

    public static class AssignmentHistoryBean
    {
        private Date date;
        private String content;
        private boolean accepted;
        private boolean finished;
        private String status;
        private String user;
        private String substitute;
        private String divisions;

        public String getUser()
        {
            return user;
        }

        private AssignmentHistoryBean(){}

        private String convertProcessName(AssignmentHistoryEntry entry){
        	String processName = entry.getProcessName();
        	if(processName.contains("manual")){
        		processName = "do realizacji";
        	}else if(processName.contains("read-only") || processName.contains("read_only")){
        		processName = "do wiadomości";
        	}
        	return processName;
        }
        
        public AssignmentHistoryBean(AssignmentHistoryEntry entry)
            throws EdmException
        {
        	if(AvailabilityManager.isAvailable("manual-multi-assignment") && !AvailabilityManager.isAvailable("oldHistoryDecretationOnManualMultiAssign")){
            	boolean initDone = false;
                this.user = DSUser.findByUsername(entry.getSourceUser()).asLastnameFirstname();
                this.date = entry.getCtime();
                this.accepted = false;

                if(entry.getSourceGuid() != null && !entry.getSourceGuid().equals("x")) {
                    this.divisions = " (" + entry.getSourceGuid() +")";
                }

                StringTemplate template = null;
                if(EntryType.JBPM_INIT.equals(entry.getType()) && !initDone) {
                    template = new StringTemplate(sm.getString("mma.JBPM_INIT"));
                    initDone = true;
                }
                 else if(EntryType.JBPM4_REASSIGN_SINGLE_USER.equals(entry.getType())) {
                    template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_SINGLE_USER"));
                } else if(EntryType.JBPM4_REASSIGN_DIVISION.equals(entry.getType())) {
                    template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_DIVISION"));
                } else if(EntryType.JBPM4_ACCEPT_TASK.equals(entry.getType())) {
                    template = new StringTemplate(sm.getString("mma.JBPM4_ACCEPT_TASK"));
                } else {
                    template = new StringTemplate(sm.getString("jbpm4."+entry.getType()));
                }

                if(template != null && template.toString().length() > 0) { 
                	String processName = entry.getProcessName();
                	// \\d Any digit, short for [0-9]
                	processName = processName.split("-\\d")[0];
                    template.setAttribute("type",("mma."+ processName).equals(sm.getString("mma."+ processName)) ? null : sm.getString("mma."+ processName));
                    template.setAttribute("targetUser",entry.getTargetUser() != null && !entry.getTargetUser().equals("x") ? DSUser.findByUsername(entry.getTargetUser()).asFirstnameLastname() : null);
                    template.setAttribute("targetDivision",entry.getTargetGuid() != null && !entry.getTargetGuid().equals("x") ? DSDivision.find(entry.getTargetGuid()).getName() : null);
                    template.setAttribute("clerk",entry.getClerk() != null ? DSUser.findByUsername(entry.getClerk()).asFirstnameLastname() : null);
                    template.setAttribute("deadline",String.valueOf("BRAK").equals(entry.getDeadline()) ? null : entry.getDeadline());
                    template.setAttribute("objective","x".equals(entry.getObjective()) ? null : entry.getObjective());
                    this.content = template.toString();
                } else {
                    this.content = String.valueOf(entry.getType());
                }
    	} else{
        	
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
            this.date = entry.getCtime();
            this.accepted = entry.isAccepted();
            this.status = entry.getStatus();
            if (entry.getType() == AssignmentHistoryEntry.IMPORT)
            {
            	this.content = "Dokument zaimportowany";
            	this.user = sm.getString("System");
            	return;
            }
            if(!StringUtils.isEmpty(entry.getSourceUser()))
            {
                this.user = DSUser.safeToFirstnameLastname(entry.getSourceUser());
            }

            String entryObjective = entry.getObjective();
            if (isUnsetObjective(entryObjective))
                entryObjective = "";

            if (entry.getType() != null)
            {
                
                // wpisy dotyczace zewnetrzengo Workflow JBPM
                if (AssignmentHistoryEntry.JBPM_INIT.equals(entry.getType()))
                {
                    this.user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                    this.content = smL.getString("ZainicjowalProcesWorkflow",convertProcessName(entry));
                }
                else if (AssignmentHistoryEntry.JBPM_END_TASK.equals(entry.getType()))
                {
                    this.user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                    this.content = smL.getString("ZakonczylZadanieWprocesieWorkflow",entryObjective,convertProcessName(entry));
                }
                else if (AssignmentHistoryEntry.JBPM_FINISH.equals(entry.getType()))
                {
                    this.user = null;
                    this.content = 
                    smL.getString("ProcesWorkflowZostalZakonczony",convertProcessName(entry));
                }
                else if (AssignmentHistoryEntry.JBPM_CANCELLED.equals(entry.getType()))
                {
                    this.user = null;
                    this.content = 
                    smL.getString("ProcesWorkflowZostalPrzerwany",convertProcessName(entry));
                }
                else if (AssignmentHistoryEntry.JBPM_NORMAL_DIVISION.equals(entry.getType()))
                {
//                  this.user = smL.getString("Dzial")+" "+DSDivision.safeGetName(entry.getTargetGuid());
                    this.user = smL.getString("Dzial")+" "+DSDivision.safeGetName(entry.getTargetGuid());
                    this.content = smL.getString("OtrzymaZadanieWprocesieWorkflow",entryObjective,convertProcessName(entry));
                }
                else if (AssignmentHistoryEntry.JBPM_FORWARD.equals(entry.getType()))
                {
                    this.user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                    this.content = smL.getString("PrzekazalZadanieWprocesieWorkflow",entryObjective,convertProcessName(entry));
                }

                //Z powodu calkowicie blednej konstrukcji histori jestem zmuszony zrobic to w ten sposob
                else if(AssignmentHistoryEntry.REOPEN_WF.equals(entry.getType()))
                {
                	 this.user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                     this.content = smL.getString("PrzywrocilZadanie");
                }
                else if (AssignmentHistoryEntry.JBPM_EXPORT_FINISH .equals(entry.getType()))
                {
                    this.user = null;
                    this.content = smL.getString("jbpm4.JBPM_EXPORT_FINISH");
                }
                else // AssignmentHistoryEntry.JBPM_NORMAL.equals(entry.getType())
                {
                    this.user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                    if (entry.isAccepted())
                        this.content = smL.getString("PrzyjalZadanieWprocesieWorkflow",entryObjective,convertProcessName(entry));
                    else
                        this.content = smL.getString("OtrzymaZadanieWprocesieWorkflow",entryObjective,convertProcessName(entry));
                }
                return;
            }

            if (entry.isFinished())
            {
            	
                //if (entry.getProcessName().equals("do wiadomo¶ci") || entry.getProcessName().equals("CC"))
            	//if ("do wiadomo¶ci".equals(entry.getProcessName()) || "CC".equals(entry.getProcessName()))
            	if (entry.getProcessType().equals(AssignmentHistoryEntry.PROCESS_TYPE_CC))
                {
                    if(!StringUtils.isEmpty(entry.getStatus()) && entry.getStatus().equals("returned"))
                    {
                        this.content = smL.getString("ZwrocilDokumentWprocesieDoWiadomosciUzytkownikowiDzial",DSUser.safeToFirstnameLastname(entry.getTargetUser()),DSDivision.safeGetName(entry.getTargetGuid()));
                        return;
                    }
                    else
                    {
                        this.content = smL.getString("PotwierdzilPrzeczytanieDokumentuWprocesieDoWiadomosci.");
                        return;
                
                    }
                }
                else
                {
                    //this.content = smL.getString("ZakonczylPraceZdokumentemWprocesie",entry.getProcessName());
                	this.content = smL.getString("ZakonczylPraceZdokumentemWprocesie",entry.getProcessType().equals(AssignmentHistoryEntry.PROCESS_TYPE_CC) ? smL.getString("doWiadomosci") : smL.getString("doRealizacji"));
                    this.finished = true;
                    return;
                }
            }

            try
            {
                if (entry.getTargetUser() != null)
                {
                        if (entry.isAccepted())
                        {
                            user = DSUser.safeToFirstnameLastname(entry.getTargetUser());
                            this.content =  smL.getString("PrzyjalPismoWcelu",
                                    (entry.getProcessName() != null ? entry.getProcessName() : ""),
                                    entryObjective);
                        }
                        else
                        {                       
                            this.content = 
                                smL.getString("DokonalDekretacjiNaUzytkownikaDzial",DSUser.safeToFirstnameLastname(entry.getTargetUser()),
                                		DSDivision.safeGetName(entry.getTargetGuid()),
                                		(entry.getProcessName() != null ? entry.getProcessName() : " "))   
                                //je¶li zastępstwo:
                                + ((entry.getSubstituteUser() == null) ? "" : 
                                	entry.getSubstituteGuid()!=null ? 
                                			" " + smL.getString("zastepowanegoPrzezUserDzial",entry.getSubstituteUser(),DSDivision.safeGetName(entry.getSubstituteGuid()))
                                			: smL.getString("zastepowanegoPrzezUser",entry.getSubstituteUser()))
                                + (!isUnsetObjective(entry.getObjective()) ? " " + smL.getString("wCelu") + entry.getObjective() : " ");
                        }
                }
                else if (entry.getSourceGuid().equals("x"))
                {
                	this.content = smL.getString("ZarejestrowalPismoWsystemie");
                    return;
                }
                //else if (entry.getProcessName().equals("rejestracja pisma"))
                else if ("rejestracja pisma".equals(entry.getProcessName()))
                {
                    this.content = smL.getString("ZarejestrowalPismoWsystemie");
                    return;
                }
                else
                {
                    this.content = smL.getString("DekretacjaNaDzialWcelu",DSDivision.safeGetName(entry.getTargetGuid()),entryObjective);
                }
            }
            catch (EdmException e)
            {
                log.warn(e.getMessage(), e);
            }
    	}
        }

        private boolean isUnsetObjective (String objective){
            return objective==null || objective.equalsIgnoreCase("x");
        }

        public Date getDate()
        {
            return date;
        }

        public String getContent()
        {
            return content;
        }

        public boolean isAccepted()
        {
            return accepted;
        }

        public boolean isFinished()
        {
            return finished;
        }
        
        public String getStatus()
        {
            return status;
        }

		public String getDivisions() {
			return divisions;
		}

		public void setDivisions(String divisions) {
			this.divisions = divisions;
		}

        public String getSubstitute() {
            return substitute;
        }

        public void setSubstitute(String substitute) {
            this.substitute = substitute;
        }
    }

    public List<AssignmentHistoryBean> getAssignmentHistory()
    {
        return assignmentHistory;
    }


    public String getCurrentAssignmentUser()
    {
        return currentAssignmentUser;
    }

    public String getCurrentAssignmentDivision()
    {
        return currentAssignmentDivision;
    }

    public Boolean isCurrentAssignmentAccepted()
    {
        return currentAssignmentAccepted;
    }

    public boolean isInternal()
    {
        return internal;
    }

	public void setCurrentCoor(Boolean currentCoor) {
		this.currentCoor = currentCoor;
	}

	public Boolean getCurrentCoor() {
		return currentCoor;
	}

    public String getParticipantsDescription() {
        return participantsDescription;
    }

    public void setParticipantsDescription(String participantsDescription) {
        this.participantsDescription = participantsDescription;
    }
}
