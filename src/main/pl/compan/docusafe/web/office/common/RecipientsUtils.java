package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.util.Collection;

import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.reports.tools.CsvDumper;

public class RecipientsUtils
{
	
	
	
	public static void createCSVFromRecipients(Collection<Recipient> recipients, File file) throws Exception
	{
		//ByteArrayOutputStream baos = new ByteArrayOutputStream();
		//BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(baos));
		CsvDumper dumper = new CsvDumper();
		dumper.openFile(file);

		dumper.newLine();
		dumper.addText("Tytul");
		dumper.addText("Imie");
		dumper.addText("Nazwisko");
		dumper.addText("Organizacja");
		dumper.addText("Dzial");
		dumper.addText("Ulica");
		dumper.addText("Kod pocztowy");
		dumper.addText("Miasto");
		dumper.addText("Kraj");
		dumper.dumpLine();
		for(Recipient r:recipients)
		{
			dumper.newLine();
			dumper.addText(r.getTitle());
			dumper.addText(r.getFirstname());
			dumper.addText(r.getLastname());
			dumper.addText(r.getOrganization());
			dumper.addText(r.getOrganizationDivision());
			dumper.addText(r.getStreet());
			dumper.addText(r.getZip());
			dumper.addText(r.getLocation());
			dumper.addText(r.getCountry());
			dumper.dumpLine();
		}
		dumper.closeFile();
		
		
	}

}
