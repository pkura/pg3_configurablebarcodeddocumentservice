package pl.compan.docusafe.web.office.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.office.Adnotation;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RemarksTabAction.java,v 1.29 2010/06/22 11:45:16 tomekl Exp $
 */
public abstract class RemarksTabAction extends BaseTabsAction
{
    private List remarks;
    private String content;
    private String contentSmall;
    private boolean canDelete;
    private boolean internal;

    private Long deleteId;

    private List<Adnotation> noteList;

    private StringManager sm = GlobalPreferences.loadPropertiesFile(RemarksTabAction.class.getPackage().getName(),null);

    /** czy dokument jest zablokowany podpisem */
    private boolean blocked;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected abstract List prepareTabs();

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	 try
             {
	            if (content == null && contentSmall == null)
	                throw new EdmException(sm.getString("NieWpisanoTresciUwagi"));

                DSApi.context().begin();
                addRemark(getDocumentId(), content, contentSmall, DSApi.context().getPrincipalName());
                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

    }

    public static void addRemark(Long documentId, String content) throws EdmException {
        String principalName = DSApi.context().getPrincipalName();
        addRemark(documentId, content, principalName);
    }

    public static void addRemark(Long documentId, String content, String contentSmall, String principalName) throws EdmException {
        addRemark(documentId, content != null ? content : contentSmall, principalName);
    }

    public static void addRemark(Long documentId, String content, String principalName) throws EdmException {
        OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId);
        doc.addRemark(new Remark(StringUtils.left(content, 4000), principalName));
    }

    public static void addRemark(Document document, String content) throws EdmException {
        String principalName = DSApi.context().getPrincipalName();
        addRemark(document, content, principalName);
    }

    public static void addRemark(Document document, String content, String contentSmall, String principalName) throws EdmException {
        addRemark(document, content != null ? content : contentSmall, principalName);
    }

    public static void addRemark(Document document, String content, String principalName) throws EdmException {
        document.addRemark(new Remark(StringUtils.left(content, 4000), principalName));
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
            {
        		if (deleteId == null)
        			throw new EdmException(sm.getString("NieWybranoUwagiDoUsuniecia"));

                DSApi.context().begin();
                canDelete = DSApi.context().hasPermission(DSPermission.PISMO_USUWANIE_UWAGI) && !blocked;
                if (!canDelete && !DSApi.context().isAdmin())
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaKomentarzy"));

                Remark deletedRemark = Remark.find(deleteId);

                if (deletedRemark != null)
                {
                    OfficeDocument doc = OfficeDocument.findOfficeDocument(getDocumentId());
                	doc.deleteRemark(deletedRemark);
                	
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	LOG.error("",e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                setTabs(prepareTabs());
                OfficeDocument doc = OfficeDocument.findOfficeDocument(getDocumentId());
                internal = doc instanceof OutOfficeDocument && ((OutOfficeDocument) doc).isInternal();
                noteList = Adnotation.list();
                if (doc.getRemarks() != null)
                {
                    remarks = new ArrayList(doc.getRemarks().size());
                    List remarksCopy = new ArrayList(doc.getRemarks());
                    Collections.reverse(remarksCopy);
                    int posn = doc.getRemarks().size() - 1;
                    for (Iterator iter=remarksCopy.iterator(); iter.hasNext(); )
                    {
                        remarks.add(new RemarkBean((Remark) iter.next(), posn--));
                    }
                }
                if (Docusafe.moduleAvailable(Modules.MODULE_CERTIFICATE))
                {
                    blocked = (doc.getBlocked() != null && doc.getBlocked());
                }
                else
                    blocked = false;
                
                if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin())
                	blocked = !PermissionManager.isDocumentInTaskList(doc);

                canDelete = DSApi.context().hasPermission(DSPermission.PISMO_USUWANIE_UWAGI) && !blocked;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public static class RemarkBean
    {
        private Date date;
        private String author;
        private String content;
        private Long remarkId;

		public RemarkBean(Remark remark, int posn) throws EdmException
        {
            this.date = remark.getCtime();
            this.author = DSUser.safeToFirstnameLastname(remark.getAuthor());
            this.content = remark.getContent();
            this.remarkId = remark.getId();
        }

        public Long getRemarkId() {
			return remarkId;
		}

        public Date getDate()
        {
            return date;
        }

        public String getAuthor()
        {
            return author;
        }

        public String getContent()
        {
            return content;
        }
    }

    public List getRemarks()
    {
        return remarks;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

	public List<Adnotation> getNoteList() {
		return noteList;
	}

	public void setNoteList(List<Adnotation> noteList) {
		this.noteList = noteList;
	}

	public String getContentSmall() {
		return contentSmall;
	}

	public void setContentSmall(String contentSmall) {
		this.contentSmall = contentSmall;
	}

	public Long getDeleteId() {
		return deleteId;
	}

	public void setDeleteId(Long deleteId) {
		this.deleteId = deleteId;
	}
}
