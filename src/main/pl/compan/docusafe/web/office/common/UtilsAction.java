package pl.compan.docusafe.web.office.common;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.PersonNotFoundException;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganisationalUnits;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganisationalUnitsResponse;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.OrganizationalUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UtilsAction extends EventActionSupport {
    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(UtilsAction.class);
    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(UtilsAction.class.getPackage().getName(), null);

    @Override
    protected void setup() {
        registerListener(DEFAULT_ACTION)
                .append(OpenHibernateSession.INSTANCE)
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doImport").
                append(OpenHibernateSession.INSTANCE).
                append(null, new Import()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doImportContractors").
                append(OpenHibernateSession.INSTANCE).
                append(null, new ImportContractors()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doImportIMGW").
                append(OpenHibernateSession.INSTANCE).
                append(null, new ImportIMGW()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {


        }

        @Override
        public Logger getLogger() {
            return LOG;
        }

    }

    private class Import implements ActionListener {
        private Logger log = LoggerFactory.getLogger(Import.class);

        public void actionPerformed(ActionEvent event) {
            try {
                File file = new File(Docusafe.getHome().getAbsolutePath() + "/" + Docusafe.getAdditionProperty("ImportFileName"));
                InputStream inp = new FileInputStream(file);
                Workbook wb = WorkbookFactory.create(inp);
                Sheet sheet = wb.getSheetAt(0);
                int rowStart = sheet.getFirstRowNum();

                createRootDivision(sheet.getRow(rowStart + 1), 7);
                createDivision(sheet.getRow(rowStart + 1), 8, null);
                for (Row row : sheet) {
                    int num = row.getRowNum();
                    if (num == rowStart) {
                        continue;
                    }

                    String extension = (String) getValueFromCell(row.getCell(0));
                    String externalName = (String) getValueFromCell(row.getCell(3));
                    String firstname = (String) getValueFromCell(row.getCell(1));
                    String lastname = (String) getValueFromCell(row.getCell(2));
                    String username = (String) getValueFromCell(row.getCell(17));
                    String email = (String) getValueFromCell(row.getCell(14));

                    DSUser user = null;
                    if (username != null && !username.equals("")) {
                        try {
                            user = UserFactory.getInstance().createUser(username, firstname, lastname);
                            try {
                                DSApi.context().begin();
                                user.setEmail(email);
                                user.setExternalName(externalName);
                                user.setExtension(extension);
                                user.setPassword("docusafe", true);
                                Role.findByName("Pracownik kancelarii").addUser(user.getName());
                                DSApi.context().commit();
                            } catch (Exception e) {
                                log.error(e.getMessage(), e);
                                DSApi.context().rollback();
                            }
                        } catch (DuplicateNameException dne) {
                            log.error(sm.getString("istniejeUzytkownik", username));
                            user = DSUser.findByUsername(username);
                        }
                    }

                    boolean stop = false;
                    for (int i = 9; i < 14; i++) {
                        if (!stop)
                            stop = createDivision(row, i, user);
                    }

                }

            } catch (Exception e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }

        private void createRootDivision(Row row, int cellNum) throws EdmException, DivisionNotFoundException {
            String divName = (String) getValueFromCell(row.getCell(cellNum));
            try {
                if (divName != null) {
                    DSDivision.findByName(divName);
                }

            } catch (DivisionNotFoundException dnf) {
                try {
                    DSApi.context().begin();
                    DSDivision.find(DSDivision.ROOT_GUID).setName(divName);
                    DSApi.context().commit();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    DSApi.context().rollback();
                }
            }
        }

        private boolean createDivision(Row row, int cellNum, DSUser user) throws EdmException, DivisionNotFoundException {
            String divName = (String) getValueFromCell(row.getCell(cellNum));
            try {
                if ((divName == null || divName.equals(""))) {
                    DSDivision div = DSDivision.findByName((String) getValueFromCell(row.getCell(cellNum - 1)));
                    DSApi.context().begin();
                    div.setCode((String) getValueFromCell(row.getCell(5)));
                    if (user != null) {
                        div.addUser(user);
                    }
                    DSApi.context().commit();
                    return true;
                } else {
                    DSDivision.findByName(divName);
                }

            } catch (DivisionNotFoundException e) {
                String parentName = (String) getValueFromCell(row.getCell(cellNum - 1));
                DSDivision parent = DSDivision.findByName(parentName);
                try {
                    DSApi.context().begin();
                    parent.createDivision(divName, null);
                    DSApi.context().commit();
                } catch (Exception e1) {
                    log.error(e1.getMessage(), e1);
                    DSApi.context().rollback();
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                DSApi.context().rollback();
            }
            return false;
        }

        private Object getValueFromCell(Cell cell) {
            try {
                if (cell == null)
                    return null;
                if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                    return null;
                } else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
                    return cell.getRichStringCellValue().getString();
                else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    if (DateUtil.isCellDateFormatted(cell)) {
                        return cell.getDateCellValue();
                    } else {
                        return cell.getNumericCellValue();
                    }
                } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                    return cell.getBooleanCellValue();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
            return null;
        }
    }


    private class ImportContractors implements ActionListener {
        private Logger log = LoggerFactory.getLogger(Import.class);

        public void actionPerformed(ActionEvent event) {
            try {
                File file = new File(Docusafe.getHome().getAbsolutePath() + "/" + Docusafe.getAdditionProperty("ImportContractorFileName"));
                InputStream inp = new FileInputStream(file);
                Workbook wb = WorkbookFactory.create(inp);
                Sheet sheet = wb.getSheetAt(0);
                Sheet bankSheet = wb.getSheetAt(1);
                int rowStart = sheet.getFirstRowNum();

                for (Row row : sheet) {
                    int num = row.getRowNum();
                    if (num == rowStart) {
                        continue;
                    }

                    String nazwa = (String) getValueFromCell(row.getCell(0)) == null ? "" : (String) getValueFromCell(row.getCell(0));
                    String ulica = (String) getValueFromCell(row.getCell(1)) == null ? "" : (String) getValueFromCell(row.getCell(1));
                    String kod = (String) getValueFromCell(row.getCell(2)) == null ? "" : (String) getValueFromCell(row.getCell(2));
                    String miejscowosc = (String) getValueFromCell(row.getCell(3)) == null ? "" : (String) getValueFromCell(row.getCell(3));
                    String nip = (String) getValueFromCell(row.getCell(4)) == null ? "" : (String) getValueFromCell(row.getCell(4));
                    String regon = (String) getValueFromCell(row.getCell(5)) == null ? "" : (String) getValueFromCell(row.getCell(5));
                    String nrKontrahenta = String.valueOf((Long) getValueFromCell(row.getCell(6))) == null ? "" : String.valueOf((Long) getValueFromCell(row.getCell(6)));
                    Long idKonta = (Long) getValueFromCell(row.getCell(7));
                    String krajKod = (String) getValueFromCell(row.getCell(8));
                    String kraj = "";
                    DataBaseEnumField dsr_country = DataBaseEnumField.getEnumFiledForTable("dsr_country");
                    if (StringUtils.isNotEmpty(krajKod)) {
                        EnumItem enumItemByCn = dsr_country.getEnumItemByCn(krajKod);
                        kraj = enumItemByCn.getId().toString();
                        if (enumItemByCn.getId().compareTo(2) == 0 && kod.length() > 6) {
                            kod = kod.substring(0, 6);
                        }
                    } else if (kod.length() > 14) {
                        kod = kod.substring(0, 15);
                    }
                    Long idSiedziby = (Long) getValueFromCell(row.getCell(9));

                    try {
                        Person person = Person.findPersonByExternalIdAndLparam(nrKontrahenta, String.valueOf(idSiedziby));
                        person.setOrganization(nazwa);
                        person.setStreet(ulica);
                        person.setZip(kod);
                        person.setLocation(miejscowosc);
                        person.setNip(nip);
                        person.setRegon(regon);
                        person.setCountry(kraj);
                        person.setWparam(person.getId());
                        person.setLparam(String.valueOf(idSiedziby));

//                        if (person.getId().compareTo(7628L) == 0) {
//                            log.error(nazwa + nazwa.length());
//                            log.error(ulica + ulica.length());
//                            log.error(kod + kod.length());
//                            log.error(miejscowosc + miejscowosc.length());
//                            log.error(nip + nip.length());
//                            log.error(regon + regon.length());
//                            log.error(kraj + kraj.length());
//                            log.error(String.valueOf(person.getId()));
//                            log.error(String.valueOf(idSiedziby) + String.valueOf(idSiedziby).length());
//                        }


                        DSApi.context().begin();
                        DSApi.context().session().save(person);
                        DSApi.context().commit();

                        assignAccountNumber(bankSheet, idKonta, person.getId());
                    } catch (PersonNotFoundException e) {
                        Person newPerson = new Person();
                        newPerson.setOrganization(nazwa);
                        newPerson.setStreet(ulica);
                        newPerson.setZip(kod);
                        newPerson.setLocation(miejscowosc);
                        newPerson.setNip(nip);
                        newPerson.setRegon(regon);
                        newPerson.setExternalID(nrKontrahenta);
                        newPerson.setCountry(kraj);
                        newPerson.setDictionaryGuid("rootdivision");
                        newPerson.setLparam(String.valueOf(idSiedziby));
                        newPerson.create();
                        Long personId = newPerson.getId();
                        newPerson.setWparam(personId);

                        DSApi.context().begin();
                        DSApi.context().session().save(newPerson);
                        DSApi.context().commit();

                        assignAccountNumber(bankSheet, idKonta, personId);
                    }
                }
            } catch (Exception e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }

        private void assignAccountNumber(Sheet bankSheet, Long idKonta, Long personId) throws SQLException, EdmException {
            if (idKonta != null) {
                PreparedStatement ps = null;
                ResultSet rs = null;
                ps = DSApi.context().prepareStatement("SELECT * FROM dsg_contractor_account_ext WHERE id_konta = ? AND personID = ?");
                ps.setLong(1, idKonta);
                ps.setLong(2, personId);
                rs = ps.executeQuery();
                if (!rs.next()) {
                    for (Row bankRow : bankSheet) {
                        int rowStart2 = bankSheet.getFirstRowNum();
                        int rowNum = bankRow.getRowNum();
                        if (rowNum == rowStart2) {
                            continue;
                        }
                        if (idKonta.compareTo((Long) getValueFromCell(bankRow.getCell(0))) == 0) {
                            ps = DSApi.context().prepareStatement("INSERT INTO dsg_contractor_account_ext VALUES (?,?,?,?)");
                            ps.setLong(1, Long.valueOf(idKonta));
                            ps.setString(2, (String) getValueFromCell(bankRow.getCell(1)));
                            ps.setString(3, (String) getValueFromCell(bankRow.getCell(2)));
                            ps.setLong(4, personId);
                            ps.executeUpdate();

                        }
                    }
                }
                ps.close();
                rs.close();
            }
        }

        private Object getValueFromCell(Cell cell) {
            try {
                if (cell == null)
                    return null;
                if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                    return null;
                } else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
                    return cell.getRichStringCellValue().getString();
                else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    if (DateUtil.isCellDateFormatted(cell)) {
                        return cell.getDateCellValue();
                    } else {
                        return (long) cell.getNumericCellValue();
                    }
                } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                    return cell.getBooleanCellValue();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
            return null;
        }
    }

    private class ImportIMGW implements ActionListener {

        private static final String SERVICE_PATH = "/services/DictionaryServicesOrganizationalUnit";

        DictionaryServicesOrganizationalUnitStub stub;
        private StringBuilder message;
        OrganizationalUnit[] items;
        int itemPointer;


        public void initConfiguration() throws Exception {
            AxisClientConfigurator conf = new AxisClientConfigurator();
            stub = new DictionaryServicesOrganizationalUnitStub();
            conf.setUpHttpParameters(stub, SERVICE_PATH);
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                initConfiguration();
                message = new StringBuilder();
                if (stub != null) {
                    GetOrganisationalUnits params = new GetOrganisationalUnits();
                    GetOrganisationalUnitsResponse response;
                    if (params != null && (response = stub.getOrganisationalUnits(params)) != null) {
                        items = response.get_return();
                        if (items != null) {
                            message.append("Pobrano kontrahent�w do przetworzenia: " + items.length);
                        } else {
                            message.append("Nie pobrano, serwis zwraca pust� tablic�");
                            return;
                        }
                    }

                    for (OrganizationalUnit item : items) {
                        message.append("Przetwarzam pozycj� erpId: " + item.getKomorka_id() + ", name: " + item.getNazwa() +
                                ", idn:" + item.getKomorka_idn() + ", parentName: " + item.getKomorka_nad_nazwa() + ", parentIdn: " + item.getKomorka_nad_idn());

                    }

//                    List<Person> found = Person.findByGivenFieldValue("wparam", personFromErp.getDostawcaId());
//                    boolean createBasePerson = true;
//
//                    if (!found.isEmpty()) {
//                        message.append(" Aktualizuj� istniej�cych kontrahent�w: " + found.size());
//                        for (Person foundPerson : found) {
//
//                            if (!(foundPerson instanceof Sender || foundPerson instanceof Recipient)) {
//                                createBasePerson = false;
//                            }
//
//                            foundPerson.setZip(personFromErp.getDostawcaKodPoczt() != null ? StringUtils.left(personFromErp.getDostawcaKodPoczt().trim(), 14) : null);
//                            foundPerson.setLocation(personFromErp.getDostawcaMiasto() != null ? StringUtils.left(personFromErp.getDostawcaMiasto().trim(), 50) : null);
//                            foundPerson.setOrganization(personFromErp.getDostawcaNazwa() != null ? StringUtils.left(personFromErp.getDostawcaNazwa().trim(), 128) : null);
//                            foundPerson.setNip(personFromErp.getDostawcaNip() != null ? StringUtils.left(personFromErp.getDostawcaNip().trim(), 15) : null);
//                            foundPerson.setStreet(streetBuilder);
//                            foundPerson.update();
//                        }
//                    }
//
//                    if (createBasePerson) {
//                        Person newPerson = new Person();
//                        newPerson.setWparam(personFromErp.getDostawcaId());
//                        newPerson.setZip(personFromErp.getDostawcaKodPoczt() != null ? StringUtils.left(personFromErp.getDostawcaKodPoczt().trim(), 14) : null);
//                        newPerson.setLocation(personFromErp.getDostawcaMiasto() != null ? StringUtils.left(personFromErp.getDostawcaMiasto().trim(), 50) : null);
//                        newPerson.setOrganization(personFromErp.getDostawcaNazwa() != null ? StringUtils.left(personFromErp.getDostawcaNazwa().trim(), 128) : null);
//                        newPerson.setNip(personFromErp.getDostawcaNip() != null ? StringUtils.left(personFromErp.getDostawcaNip().trim(), 15) : null);
//                        newPerson.setStreet(streetBuilder);
//                        log.debug(newPerson.toString());
//                        newPerson.create();
//                        message.append(" Utworzono nowego kontrahenta, id: " + newPerson.getId());
//                    }
                    printMessage();
                } else {
                    message.append("B��d konfiguracji DictionaryServicesOrganizationalUnitStub");
                    printMessage();
                    return;
                }

            } catch (RemoteException e) {
                LOG.error(e.getMessage(), e);  //To change body of catch statement use File | Settings | File Templates.
//            } catch (EdmException e) {
//                LOG.error(e.getMessage(), e);  //To change body of catch statement use File | Settings | File Templates.
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        public void printMessage() {
            LOG.error(message.toString());
        }

    }
}
