package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.List;

public interface DwrOfficeDocument extends DwrDocument
{
    public abstract void bindToJournal(OfficeDocument document, ActionEvent event) throws EdmException;
	public abstract boolean isStartProcess() throws EdmException;
    public abstract List<Tab> prepareTabs();
	public InOfficeDocumentKind getDefaultKind();	
	public void setDefaultKind(InOfficeDocumentKind defaultKind);
	public void setTabs(List<Tab> prepareTabs);
    public void setBeforeCreate(OfficeDocument document) throws EdmException, Exception;
    public void setAfterCreate(OfficeDocument document) throws EdmException;
  //  public void setBeforeCreate(OfficeDocument document ,ActionEvent event) throws EdmException, Exception;
}
