package pl.compan.docusafe.web.office.common;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.AssignmentPermission;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WorkflowAssignmentAction.java,v 1.17 2010/08/05 14:03:48 pecet1 Exp $
 */
public class WorkflowAssignmentAction extends EventActionSupport
{

    private final static Logger log = LoggerFactory.getLogger(WorkflowAssignmentAction.class);
    // @EXPORT
    private DSDivision division;
    private DSUser user;
    private List objectives;
    // @EXPORT/@IMPORT
    private String divisionGuid;
    private String username;
    /**
     * Warto�� nadawana przy wywo�aniu z okna macierzystego (workflow.jsp),
     * je�eli na li�cie istnieje ju� dekretacja "do realizacji".
     */
    private boolean hasExecutePlanned;
    /**
     * Warto�� nadawana przy wywo�aniu z okna macierzystego (workflow.jsp),
     * je�eli mo�liwa jest dekretacja r�czna.
     */
    private boolean hasManualPush;
    private boolean hasCloneAndConsult;
    private boolean hasCC;
    // @IMPORT
    private String kind;
    private String objective;
    private String scope;
    private Long documentId;
    // @EXPORT
    private String objectiveSel;
    private boolean blocked;// = false;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
                //je�li nie ma wybranego dzia�u a jest wybrany u�ytkownik to wybierz dowolny jego dzia�
                if (!StringUtils.isEmpty(username))
                {
                    user = DSUser.findByUsername(username);

                    //if(StringUtils.isEmpty(divisionGuid) || divisionGuid.equals(null+""))
                    if (divisionGuid == null || StringUtils.isEmpty(divisionGuid) || divisionGuid.equalsIgnoreCase("null"))
                    {
                        DSDivision divs[] = user.getDivisions();
                        if (divs.length > 0)
                        {
                            division = divs[0];
                        } else
                        {
                            division = DSDivision.find(DSDivision.ROOT_GUID);
                        }
                    }

                    //TODO: zavailablowac to?
                    if (user.getRoles().contains("archive_only"))
                    {
                        blocked = true;
                        throw new EdmException("Blokada dekretacji na uzytkownika archive_only");
                    }
                }

                if (division == null)
                {
                    division = DSDivision.find(divisionGuid);
                }

                division.getPrettyPath();

                objectives = AssignmentObjective.list();

                // Business: domy�lnie wybrany jest pierwszy cel dekretacji
                if (Configuration.hasExtra("business"))
                {
                    if (objectives.size() > 0)
                    {
                        objectiveSel = ((AssignmentObjective) objectives.get(0)).getName();
                    }
                }
                OfficeDocument doc = null;
                if (documentId != null)
                {
                    doc = OfficeDocument.find(documentId);
                }

                hasCloneAndConsult = !WorkflowFactory.jbpm && (doc == null ? true : AssignmentPermission.getInstance().checkDockindAllows(doc, "konsultacje"));
                hasCC = (doc == null ? true : AssignmentPermission.getInstance().checkDockindAllows(doc, "do_wiadomosci"));
            } catch (EdmException e)
            {
                log.error(e.getLocalizedMessage(), e);
                addActionError(e.getMessage());
            }
        }
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public DSDivision getDivision()
    {
        return division;
    }

    public DSUser getUser()
    {
        return user;
    }

    public List getObjectives()
    {
        return objectives;
    }

    public void setKind(String kind)
    {
        this.kind = kind;
    }

    public void setObjective(String objective)
    {
        this.objective = objective;
    }

    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public String getUsername()
    {
        return username;
    }

    public boolean isHasExecutePlanned()
    {
        return hasExecutePlanned;
    }

    public void setHasExecutePlanned(boolean hasExecutePlanned)
    {
        this.hasExecutePlanned = hasExecutePlanned;
    }

    public boolean isHasManualPush()
    {
        return hasManualPush;
    }

    public void setHasManualPush(boolean hasManualPush)
    {
        this.hasManualPush = hasManualPush;
    }

    public String getObjectiveSel()
    {
        return objectiveSel;
    }

    public void setHasCloneAndConsult(boolean hasCloneAndConsult)
    {
        this.hasCloneAndConsult = hasCloneAndConsult;
    }

    public boolean isHasCloneAndConsult()
    {
        return hasCloneAndConsult;
    }

    public boolean isBlocked()
    {
        return blocked;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public boolean isHasCC()
    {
        return hasCC;
    }

    public void setHasCC(boolean hasCC)
    {
        this.hasCC = hasCC;
    }
}
