package pl.compan.docusafe.web.office.common;
import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 *
 * @author Micha� Sankowski extended by Maciej Starosz
 */
public class RecipientExtensionLookupAction extends EventActionSupport{
	private final static int RESULTS_COUNT = 20;
    private final static Logger LOG = LoggerFactory.getLogger(RecipientExtensionLookupAction.class);

	private ArrayList<RecipientExtensionLookupAction.SearchItem> usersList = new ArrayList<RecipientExtensionLookupAction.SearchItem>();
	private List<DSUser> users;
	private String lastname;
	private String division;
	private String firstname;

	

	protected void setup() {
		registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new FillForm()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
	private class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception
        {
                if(StringUtils.isEmpty(division)) {
                    getUsersWithFirstnameLastname(firstname ,lastname);
                }
                else {
                	getUsersFromDivision(firstname,lastname, division);
                }
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}

        private void getUsersWithFirstnameLastname(String firstname, String lastname) throws EdmException {

                Criteria crit = DSApi.context().session().createCriteria(DSUser.class);
                if (!StringUtils.isEmpty(lastname)) {
                    crit.add(Restrictions.like("lastname", lastname + "%").ignoreCase());
                }
                if (!StringUtils.isEmpty(firstname)){   
             	   crit.add(Restrictions.like("firstname", firstname + "%").ignoreCase());
                }
                    crit.add(Restrictions.not(Restrictions.eq("deleted", true)));
                
                crit.setMaxResults(RESULTS_COUNT);
                setUsers(crit.list());
                for (DSUser user : users) {
                    DSDivision[] divisions = user.getDivisionsWithoutGroupPosition();
                    for (DSDivision div : divisions) {
                        usersList.add(new SearchItem(user.getName(), user.getLastname(), user.getFirstname(), div.getName(), div.getGuid()));
                    }
                }
        }

		private void getUsersFromDivision(String firstname, String lastname, String division) throws EdmException
		{

			Criteria crit = DSApi.context().session().createCriteria(DSDivision.class);

			if (!StringUtils.isEmpty(lastname))
			{
				crit.add(Restrictions.like("name", division + "%"));
			}
			crit.setMaxResults(RESULTS_COUNT);
			List<DSDivision> divs = crit.list();
			for (DSDivision div : divs)
			{
				DSUser[] users = div.getUsers();
				for (DSUser user : users)
				{

					if (!StringUtils.isEmpty(firstname) && !StringUtils.isEmpty(lastname) && user.getLastname().contains(lastname)
							&& (user.getFirstname().contains(firstname)))
					{
						usersList.add(new SearchItem(user.getName(), user.getLastname(), user.getFirstname(), div.getName(), div.getGuid()));

					} else if (!StringUtils.isEmpty(lastname) && (user.getLastname().contains(lastname)))
					{
						usersList.add(new SearchItem(user.getName(), user.getLastname(), user.getFirstname(), div.getName(), div.getGuid()));

					} else
					{
						if (!StringUtils.isEmpty(firstname) && (user.getFirstname().contains(firstname)))
						{
							usersList.add(new SearchItem(user.getName(), user.getLastname(), user.getFirstname(), div.getName(), div.getGuid()));
						}
					}
				}
			}

		}
	}

	private class SearchItem {
		public SearchItem(String userName, String userLastName,
				String userFirstName, String userDivisionName,
				String userDivisionGuid) {
			super();
			this.userName = userName;
			this.userLastName = userLastName;
			this.userFirstName = userFirstName;
			this.userDivisionName = userDivisionName;
			this.userDivisionGuid = userDivisionGuid;
		}
		private String userName;
		private String userLastName;
		private String userFirstName;
		private String userDivisionName;
		private String userDivisionGuid;
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getUserLastName() {
			return userLastName;
		}
		public void setUserLastName(String userLastName) {
			this.userLastName = userLastName;
		}
		public String getUserFirstName() {
			return userFirstName;
		}
		public void setUserFirstName(String userFirstName) {
			this.userFirstName = userFirstName;
		}
		public String getUserDivisionName() {
			return userDivisionName;
		}
		public void setUserDivisionName(String userDivisionName) {
			this.userDivisionName = userDivisionName;
		}
		public String getUserDivisionGuid() {
			return userDivisionGuid;
		}
		public void setUserDivisionGuid(String userDivisionGuid) {
			this.userDivisionGuid = userDivisionGuid;
		}
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setUsers(List<DSUser> users)
	{
		this.users = users;
	}

	public List<DSUser> getUsers()
	{
		return users;
	}

	public String getFirstname()
	{
		return firstname;
	}

	
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}
	public ArrayList<SearchItem> getUsersList() {
		return usersList;
	}

	public void setUsersList(ArrayList<SearchItem> usersList) {
		this.usersList = usersList;
	}
}
