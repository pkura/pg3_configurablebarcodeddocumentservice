package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.encryption.EncryptedAttachmentRevision;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.tiff.Tiff;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.asprise.util.tiff.TIFFWriter;

/* User: Administrator, Date: 2005-12-09 13:34:22 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AttachmentsToTiffTabAction.java,v 1.10 2009/08/26 08:50:31 tomekl Exp $
 */
public abstract class AttachmentsToTiffTabAction extends BaseTabsAction
{
	private static final Logger log = LoggerFactory.getLogger(AttachmentsToTiffTabAction.class);
    private Map<Long,String> attachmentsMap = new LinkedHashMap<Long, String>();

    private boolean deleteSources;
    private Long[] revisionIds;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doConcatenate").
            append(OpenHibernateSession.INSTANCE).
            append(new Concatenate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected abstract List prepareTabs();

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(prepareTabs());

            try
            {
                Document document = Document.find(getDocumentId());
                List<Attachment> attachments = document.listAttachments();
                for (Iterator iter=attachments.iterator(); iter.hasNext(); )
                {
                    Attachment attachment = (Attachment) iter.next();
                    AttachmentRevision revision = attachment.getMostRecentRevision();

                    if (revision != null && ImageUtils.canConvertToTiff(revision.getMime()))
                    {
                        attachmentsMap.put(revision.getId(),
                            attachment.getTitle() + " ("+revision.getOriginalFilename()+")");
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Concatenate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (revisionIds == null || revisionIds.length == 0)
            {
                addActionError(sm.getString("ListaZalacznikowJestPusta"));
                return;
            }

            try
            {
                DSApi.context().begin();
                Document document = Document.find(getDocumentId());

                Tiff tiff = new Tiff();

                for (int i=0; i < revisionIds.length; i++)
                {
                    AttachmentRevision revision = AttachmentRevision.find(revisionIds[i]);
                    File tmp = File.createTempFile("docusafe_", ".tmp");
                    //"."+(revision.getMime().substring(revision.getMime().indexOf('/')+1)));
                    
                    if(revision instanceof EncryptedAttachmentRevision)
                    {
                    	throw new EdmException(sm.getString("BrakMozliwosciScalaniaZaszyfrowanychZalacznikow"));
                    }
                    
                    revision.saveToFile(tmp);
                    
                    /*if (event.getLog().isDebugEnabled())
                        event.getLog().debug("utworzono plik "+tmp+" typu "+
                            revision.getMime());*/
                    
                    tiff.add(tmp, revision.getMime());

                    if (deleteSources && revision.getAttachment().isDocumentType())
                    {
                        revision.getAttachment().getDocument().scrubAttachment(
                            revision.getAttachment().getId());
                    }
                }
                File output = tiff.write(
                    TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
                    TIFFWriter.TIFF_COMPRESSION_GROUP4);

                Attachment attachment = new Attachment(sm.getString("Zalacznik"), sm.getString("ZalacznikPolaczonePliki"));
                document.createAttachment(attachment);

                attachment.createRevision(output);

                DSApi.context().commit();
                event.setResult("attachments");
            }
            catch (IOException e)
            {
                addActionError(e.getMessage());
                log.error("",e);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }

    public Map<Long,String> getAttachmentsMap()
    {
        return attachmentsMap;
    }

    public void setDeleteSources(boolean deleteSources)
    {
        this.deleteSources = deleteSources;
    }

    public void setRevisionIds(Long[] revisionIds)
    {
        this.revisionIds = revisionIds;
    }
}
