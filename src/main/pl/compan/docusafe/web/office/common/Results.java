package pl.compan.docusafe.web.office.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections.IteratorUtils;
import pl.compan.docusafe.web.office.common.Results.ResultEntry;

/**
 * Klasa pomocnicza do zwracania wynik�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class Results implements Iterable<ResultEntry> {

	String[] header = null;
	List<ResultEntry> results = new ArrayList<ResultEntry>();
	Integer columns = null;

	public Results header(String... header){
		if(columns != null) {
			throw new IllegalStateException("wywo�ano ju� metod� header lub entry");
		}
		this.header = header;
		columns = header.length;
		return this;
	}

	public Results entry(String hyperlink, String queryString, String... entry){
		if(columns == null){
			columns = entry.length;
		}
		if(columns != entry.length){
			throw new IllegalArgumentException("Z�y rozmiar entry");
		}
		results.add(new ResultEntry(hyperlink, queryString, entry));
		return this;
	}

	public Iterator<ResultEntry> iterator() {
		return results.iterator();
	}

	public String[] getHeader(){
		return header;
	}

	public boolean getHasHeader(){
		return header != null;
	}

	public boolean isEmpty(){
		return results.isEmpty();
	}

	public static class ResultEntry implements Iterable<String>{
		String hyperlink;
		String queryString;
		String values[];

		public ResultEntry(String hyperlink, String queryString, String[] values){
			this.queryString = queryString;
			this.hyperlink = hyperlink;
			this.values = values;
		}

		public Iterator<String> iterator() {
			return Arrays.asList(values).iterator();
		}

		public String getHyperlink(){
			return hyperlink;
		}

		public String getQueryString(){
			return queryString;
		}
	}
}
