package pl.compan.docusafe.web.office.common;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.parametrization.pg.PgPackageLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import javax.annotation.Nullable;
import javax.servlet.http.HttpSession;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class DwrDocumentHelper
{
	protected static Logger log = LoggerFactory.getLogger(DwrDocumentHelper.class);
	static final StringManager sm = GlobalPreferences.loadPropertiesFile(DwrDocumentHelper.class.getPackage().getName(),null);
	
	
	public void initDefaultKind(DwrOfficeDocument dwrOfficeDocument)
	{
		try
		{
			dwrOfficeDocument.setDefaultKind(InOfficeDocumentKind.list().get(0));
		}
		catch (Exception e)
		{
			dwrOfficeDocument.getLog().error("Nie udalo si� zainicjowa� domy�lnego InOfficeDocumentKind ", e);
		}
	}

	public void fillForm(DwrDocument dwrDocument) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		if (!AvailabilityManager.isAvailable("dwr"))
		{
			return;
		}
		if (dwrDocument.getDocumentKindCn() == null)
		{
			
			if (dwrDocument.getDocumentId() != null)
			{
				dwrDocument.setDocumentKindCn(Document.find(dwrDocument.getDocumentId()).getDocumentKind().getCn());
			}
			else  if(dwrDocument.getDocumentKindCn() == null && isPackageRedirectAvailable()){
				dwrDocument.setDocumentKindCn(getPackageKind(dwrDocument));
			}
			else
			{
				// ka�dy dokument musi mie� dockind w przypadku dwr
				dwrDocument.setDocumentKindCn(DocumentKind.getDefaultKind(dwrDocument.getDocType()));
			}
		
		}
			
//		List<DocumentKind> list = DocumentKindProvider.get().visible().forManualCreate().type(getDocumentTypeAsType(dwrDocument)).list();
        List<DocumentKind> list = null;
        list = DocumentKindProvider.get().visible().forManualCreate().canCreate(dwrDocument.getDocumentKindCn(),
                dwrDocument.getDocumentId() == null ? true:false)
                .type(getDocumentTypeAsType(dwrDocument)).list();

        Map<String, String> documentKinds = new LinkedHashMap<String, String>();
		for (DocumentKind documentKind : list)
		{
           documentKinds.put(documentKind.getCn(), documentKind.getName());
		}

        Iterable<String> allDocumentKind = Iterables.transform(DocumentKindProvider.get().forManualCreate().type(getDocumentTypeAsType(dwrDocument)).list(), new Function<DocumentKind, String>() {
            public String apply( DocumentKind documentKind) {
                return documentKind.getCn();
            }
        });
        if (!Iterables.contains(allDocumentKind, dwrDocument.getDocumentKindCn()) && !getDocumentTypeAsType(dwrDocument).equals(DocumentType.ARCHIVE))
        {
            dwrDocument.setDocumentKindCn(documentKinds.keySet().iterator().next());
        }
		dwrDocument.setDocumentKinds(documentKinds);

		if (dwrDocument instanceof DwrOfficeDocument)
		{
			DwrOfficeDocument dwrOfficeDocument = (DwrOfficeDocument) dwrDocument;
			dwrOfficeDocument.setTabs(dwrOfficeDocument.prepareTabs());
		}
		

	}

	public void update(DwrDocument dwrDocument) throws DocumentNotFoundException, EdmException
	{
		
		log.info("START UPDATE");
		Document doc = Document.find(dwrDocument.getDocumentId());

		dwrDocument.setDocument(doc);

		if (dwrDocument.getDocument().getDocumentKind() != null)
		{
			dwrDocument.setBeforeUpdate(dwrDocument.getDockindKeys());
			dwrDocument.getDocument().getDocumentKind().logic().validate(dwrDocument.getDockindKeys(), dwrDocument.getDocument().getDocumentKind());
			if (dwrDocument.getDocument() instanceof OfficeDocument)
			{
				((OfficeDocument) dwrDocument.getDocument()).setSummary(dwrDocument.getDockindKeys().get("DOC_DESCRIPTION") != null ? (String) dwrDocument.getDockindKeys().get("DOC_DESCRIPTION") : dwrDocument.getDocument().getDocumentKind().getName());
			}
			log.info("UPDATE: {}", dwrDocument.getDockindKeys().toString());
			dwrDocument.getDocument().getDocumentKind().setWithHistory(dwrDocument.getDocumentId(), dwrDocument.getDockindKeys(), false);
			dwrDocument.getDocument().getDocumentKind().logic().archiveActions(dwrDocument.getDocument(),  dwrDocument.getDocType());
			dwrDocument.getDocument().getDocumentKind().logic().documentPermissions(dwrDocument.getDocument());   
		//	dwrDocument.getDocument().getDocumentKind().logic().archiveActions(dwrDocument.getDocument(), -1);
		}
		HttpSession session = ServletActionContext.getRequest().getSession();
		session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
		TaskSnapshot.updateByDocument(dwrDocument.getDocument());
		
		
	}

	public Document getNewDocument(DwrDocument dwrDocument)
	{
		if (DocumentLogic.TYPE_IN_OFFICE == dwrDocument.getDocType())
			return new InOfficeDocument();
		if (DocumentLogic.TYPE_OUT_OFFICE == dwrDocument.getDocType())
			return new OutOfficeDocument();
		if (DocumentLogic.TYPE_INTERNAL_OFFICE == dwrDocument.getDocType())
			return new OutOfficeDocument();
		if (DocumentLogic.TYPE_ARCHIVE == dwrDocument.getDocType())
			return new Document("Opis", "Opis");
		return new Document("Opis", "Opis");
	}

	/** Ch�opcy pomylili klasy po kt�rych trzeba dziedziczy� */
	public DocumentType getDocumentTypeAsType(DwrDocument dwrDocument)
	{
		if (DocumentLogic.TYPE_IN_OFFICE == dwrDocument.getDocType())
			return DocumentType.INCOMING;
		if (DocumentLogic.TYPE_OUT_OFFICE == dwrDocument.getDocType())
			return DocumentType.OUTGOING;
		if (DocumentLogic.TYPE_INTERNAL_OFFICE == dwrDocument.getDocType())
			return DocumentType.INTERNAL;
		if (DocumentLogic.TYPE_ARCHIVE == dwrDocument.getDocType())
			return DocumentType.ARCHIVE;
		return DocumentType.INCOMING;
	}

    /**
     * Kowertuje type dokumentu z <code>DocumentType</code> na sta�� z <code>DocumentLogic</code>
     * @param docType Typ do konwersji
     * @return Skonwertowany typ do <code>DocumentLogic</code>
     * @throws EdmException Nieznany typ dokumentu - nie ma odpowiednika w <code>DocumentLogic</code>
     */
    public static int getDocumentTypeAsDocumentLogicType(DocumentType docType) throws EdmException {
        checkNotNull(docType);
        switch (docType) {
            case INCOMING:
                return DocumentLogic.TYPE_IN_OFFICE;
            case OUTGOING:
                return DocumentLogic.TYPE_OUT_OFFICE;
            case INTERNAL:
                return DocumentLogic.TYPE_INTERNAL_OFFICE;
            case ARCHIVE:
                return DocumentLogic.TYPE_ARCHIVE;
            case PLAIN:
            case ORDER:
            default:
                StringBuilder msg = new StringBuilder(sm.getString("NapotkanoNieznanyTypDokumentuKancelaryjnego"));
                msg.append(": ").append(docType.getName());
                throw new EdmException(msg.toString());
        }
    }

	public void setBeforeUpdate(DwrDocument dwrDocument) throws EdmException
	{
		if (DocumentLogic.TYPE_IN_OFFICE == dwrDocument.getDocType())
			setBeforeUpdateIn(dwrDocument);
		if (DocumentLogic.TYPE_OUT_OFFICE == dwrDocument.getDocType())
			setBeforeUpdateOut(dwrDocument);
		if (DocumentLogic.TYPE_INTERNAL_OFFICE == dwrDocument.getDocType())
			setBeforeUpdateInt(dwrDocument);
		if (DocumentLogic.TYPE_ARCHIVE == dwrDocument.getDocType())
			setBeforeUpdateIn(dwrDocument);
	}
	
	public void setBeforeUpdateSender(DwrDocument dwrDocument) throws EdmException
	{
		log.debug("dwrDocument.getDockindKeys() is null ? :" + (dwrDocument.getDockindKeys() == null));
		OfficeDocument doc = (OfficeDocument) Document.find(dwrDocument.getDocumentId());
		if (dwrDocument.getDockindKeys() != null && dwrDocument.getDockindKeys().containsKey("RECIPIENT_PARAM") 
				&& (!dwrDocument.getDockindKeys().containsKey("RECIPIENT") || ( dwrDocument.getDockindKeys().containsKey("RECIPIENT") && dwrDocument.getDockindKeys().get("RECIPIENT") == null) ))
//		if (dwrDocument.getDockindKeys() != null && (dwrDocument.getDockindKeys().containsKey("RECIPIENT_PARAM") && 
//				(!dwrDocument.getDockindKeys().containsKey("RECIPIENT") || (dwrDocument.getDockindKeys().containsKey("RECIPIENT") && dwrDocument.getDockindKeys().get("RECIPIENT") == null) ) && doc.getRecipients().isEmpty()))
		{
			if (DwrDictionaryFacade.getDwrDictionary(dwrDocument.getDocumentKindCn(), "RECIPIENT").isOnlyPopUpCreate())
			{
				throw new EdmException(sm.getString("OnlyPopupCreate"));
			}
			Person person = new Person();
			try
			{
				person.fromMapUpper((Map) dwrDocument.getDockindKeys().get("RECIPIENT_PARAM"));
				person.setDictionaryGuid("rootdivision");
				person.create();
				Recipient recipient = new Recipient();
				recipient.fromMap(person.toMap());
				recipient.setDictionaryGuid("rootdivision");
				recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
				recipient.setBasePersonId(person.getId());
				recipient.create();
				dwrDocument.getDockindKeys().put("RECIPIENT", recipient.getId());
				
				if (dwrDocument.getDocument() instanceof InOfficeDocument)
				{
					((InOfficeDocument) dwrDocument.getDocument()).setRecipients();
					((InOfficeDocument) dwrDocument.getDocument()).addRecipient(recipient);
				}
				else if (dwrDocument.getDocument() instanceof OutOfficeDocument)
				{
					((OutOfficeDocument) dwrDocument.getDocument()).setRecipients();
					((OutOfficeDocument) dwrDocument.getDocument()).addRecipient(recipient);
				}
			}
			catch (EdmException e)
			{
				log.error(e.getMessage());
				throw new EdmException(e.getMessage());
			}
		}
		if (dwrDocument.getDockindKeys() != null &&dwrDocument.getDockindKeys().containsKey("SENDER_PARAM") && (!dwrDocument.getDockindKeys().containsKey("SENDER") || dwrDocument.getDockindKeys().get("SENDER") == null))
		{
			log.debug("SENDER_PARAM i !SENDER");
			if (DwrDictionaryFacade.getDwrDictionary(dwrDocument.getDocumentKindCn(), "SENDER") != null && DwrDictionaryFacade.getDwrDictionary(dwrDocument.getDocumentKindCn(), "SENDER").isOnlyPopUpCreate())
			{
				throw new EdmException(sm.getString("OnlyPopupCreate"));
			}
			Person person = new Person();
			try
			{
				person.fromMapUpper((Map) dwrDocument.getDockindKeys().get("SENDER_PARAM"));
				person.setDictionaryGuid("rootdivision");
				person.create();
				Sender sender = new Sender();
				sender.fromMap(person.toMap());
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(person.getId());
				sender.create();
				dwrDocument.getDockindKeys().put("SENDER", sender.getId());
				if (dwrDocument.getDocument() instanceof InOfficeDocument)
				{
					((InOfficeDocument) dwrDocument.getDocument()).setSender(sender);
				}
				else if (dwrDocument.getDocument() instanceof OutOfficeDocument)
				{
					((OutOfficeDocument) dwrDocument.getDocument()).setSender(sender);
				}
			}
			catch (EdmException e)
			{
				log.error(e.getMessage());
			}
		}
	}
	private boolean isPackageRedirectAvailable() throws EdmException
	{
	
		return PgPackageLogic.isPackageRedirectAvailable();
	}

	private String getPackageKind(DwrDocument dwrDocument) throws EdmException
	{
		DocumentKind kind = null;
			kind =  PgPackageLogic.setKindFromRedirectAction();
			 if(kind!=null)
				 return kind.getCn();
			 else
				return DocumentKind.getDefaultKind(dwrDocument.getDocType());
	}

		

	public void setBeforeUpdateIn(DwrDocument dwrDocument) throws EdmException
	{
		setBeforeUpdateSender(dwrDocument);
	}

	public void setBeforeUpdateOut(DwrDocument dwrDocument) throws EdmException
	{
		setBeforeUpdateSender(dwrDocument);
	}

	public void setBeforeUpdateInt(DwrDocument dwrDocument) throws EdmException
	{
		setBeforeUpdateSender(dwrDocument);
	}
}
