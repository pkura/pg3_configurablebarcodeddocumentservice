package pl.compan.docusafe.web.office.common;

import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.core.certificates.ElectronicSignatureBean;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.List;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class ElectronicSignaturesTabAction extends BaseTabsAction {
    private final static Logger LOG = LoggerFactory.getLogger(ElectronicSignaturesTabAction.class);

    private List<ElectronicSignatureBean> signatures;
    private String singerData;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected abstract List<Tab> prepareTabs();

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {
            LOG.info("ElectronicSignaturesTabAction " + getDocumentType());
            setTabs(prepareTabs());

            DSUser singer = null;
            signatures = ElectronicSignatureStore.getInstance().findSignaturesForDocument(getDocumentId());
            
            
           
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public List<ElectronicSignatureBean> getSignatures() {
        return signatures;
    }

	
	public String getSingerData()
	{
		return singerData;
	}

	
	public void setSingerData(String singerData)
	{
		this.singerData = singerData;
	}
}