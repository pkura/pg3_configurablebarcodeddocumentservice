package pl.compan.docusafe.web.office.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.AttachmentRevisionSignature;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.swig.verification.verification;
import pl.compan.docusafe.core.swig.verification.verificationConstants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * User: Michal Manski
 * Date: 2006-05-09
 */
public class AttachmentSignatureAction extends EventActionSupport
{
    
    private Long id;    /* id wersji załacznika zwiazanego z podpisem */
    private Long signatureId;  
    private String inFile;
    private String outFile;
    private boolean doSign;

    // @EXPORT
    private String verificationDescription;
    private List<String> verificationWarnings;
    private int verificationResult;
    private String verificationInfo;
    private String signatureAuthor;
    private String signatureCtime;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

    
    protected void setup()
    {    
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SignAttachment()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new SaveSignature()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doView").
            append(OpenHibernateSession.INSTANCE).
            append(new ViewSignature()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doVerify").
            append(OpenHibernateSession.INSTANCE).
            append(new Verify()).
            appendFinally(CloseHibernateSession.INSTANCE);
        }
    
    private class SignAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            doSign = true;
            
            File content = null;

            InputStream stream = null;
            try
            {
                // do odczytu bloba trzeba otworzyć transakcję
                DSApi.context().begin();

                AttachmentRevision revision = AttachmentRevision.find(id);

                stream = revision.getBinaryStream();
                if (stream != null)
                {
                    int index = revision.getOriginalFilename().lastIndexOf(".");
                    String fileExtension;
                    if (index < 0)
                        fileExtension = ".tmp";
                    else
                        fileExtension = revision.getOriginalFilename().substring(index);
                    
                    content = File.createTempFile("docusafe_att_rev", fileExtension);
                    
                    inFile = content.getCanonicalPath().replaceAll("\\\\", "\\\\\\\\");
                    outFile = (content.getCanonicalPath()+AttachmentRevisionSignature.SIG_EXTENSION).replaceAll("\\\\", "\\\\\\\\");
                    
                    
                    
                    FileOutputStream os = new FileOutputStream(content);
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }
                    os.close();
                    stream.close();
                    stream = null;
                }

                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());  
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }
            
            if (hasActionErrors())
                event.setResult(ERROR);
        }
    }
    
    private class SaveSignature implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            doSign = false;
                        
            File in = new File(inFile);
            File out = new File(outFile);
            
            try {
                DSApi.context().begin();
                
                AttachmentRevision revision = AttachmentRevision.find(id);
                
                AttachmentRevisionSignature signature = new AttachmentRevisionSignature();
                signature.setAttachmentRevision(revision);
 
                signature.create();
          
                signature.saveContent(out);

                Attachment attachment = revision.getAttachment();
                if (attachment.isDocumentType())
                {
                    Document document = attachment.getDocument();
                    if (document instanceof OfficeDocument)
                    {
                        ((OfficeDocument) document).addWorkHistoryEntry(
                            Audit.create("attachments", DSApi.context().getPrincipalName(), sm.getString("PodpisanoZalacznik",attachment.getTitle()),
                                "SIGN", attachment.getId()));
                    }
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally 
            {
                /* usuwam pliku tymczasowe */
                in.delete();
                out.delete();
            }
              
            if (hasActionErrors())
                event.setResult(ERROR);
        }
    }
    
    private class ViewSignature implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("view");
            doSign = false;
            
            if (signatureId == null)
                return;
            
            File content = null;
            String filename = null;
            boolean success = false;
            
            BlobInputStream stream = null;
            try
            {
                // do odczytu bloba trzeba otworzyć transakcję
                DSApi.context().begin();

                AttachmentRevisionSignature signature = AttachmentRevisionSignature.find(signatureId);
                AttachmentRevision revision = signature.getAttachmentRevision();

                filename = revision.getOriginalFilename()+AttachmentRevisionSignature.SIG_EXTENSION;
     
                stream = signature.getBinaryStream();
                if (stream != null)
                {
                    content = File.createTempFile("docusafe_att_sig", ".tmp");
  
                    FileOutputStream os = new FileOutputStream(content);
                    byte[] buffer = new byte[8192];
                    int count;
                    while ((count = stream.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }
                    os.close();
                    stream.close();
                    stream = null;
                }

                DSApi.context().commit();
                
                success = true;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                if (content != null) content.delete();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                //DSApi._close();
            }
            
            if (success && content != null)
            {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
                //response.setHeader("Content-Disposition", "inline");
                response.setContentLength((int) content.length());
                try
                {
                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(content);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
                finally
                {
                    content.delete();
                }
            }
        }
    }
    
    private class Verify implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("verification");

            BlobInputStream stream = null;
            File attFile = null;
            File sigFile = null;

            try
            {
                DSApi.context().begin();

                AttachmentRevisionSignature signature = AttachmentRevisionSignature.find(signatureId);
                Attachment attachment = signature.getAttachmentRevision().getAttachment();
                
                verificationInfo = sm.getString("WeryfikacjaPodpisuZlozonegoPodZalacznikiemZdokumentuOID",attachment.getTitle(),attachment.getDocument().getId());
                signatureCtime = DateUtils.formatJsDateTime(signature.getCtime());
                signatureAuthor = DSUser.findByUsername(signature.getAuthor()).asFirstnameLastname();

                boolean correctlyVerified = true;

                // weryfikacja podpisu z z podpisywanym zalacznikiem
                if (Docusafe.getVerificationLibraryLoaded())
                {
                    /* zapisujemy na dysk orginalny zalacznik */
                    /*stream = signature.getAttachmentRevision().getBinaryStream();
                    if (stream != null)
                    {
                        pdfFile = File.createTempFile("docusafe_att_", ".tmp");
                        FileOutputStream os = new FileOutputStream(pdfFile);
                        byte[] buffer = new byte[8192];
                        int count;
                        while ((count = stream.read(buffer)) > 0)
                        {
                            os.write(buffer, 0, count);
                        }
                        os.close();
                        stream.close();
                        stream = null;
                    } */
                    /* zapisujemy na dysk podpisany plik (.pem) */
                    stream = signature.getBinaryStream();
                    if (stream != null)
                    {
                        sigFile = File.createTempFile("docusafe_att_", AttachmentRevisionSignature.SIG_EXTENSION);
                        FileOutputStream os = new FileOutputStream(sigFile);
                        byte[] buffer = new byte[8192];
                        int count;
                        while ((count = stream.read(buffer)) > 0)
                        {
                            os.write(buffer, 0, count);
                        }
                        os.close();
                        stream.close();
                        stream = null;
                    }
                    /* tworzymy plik, na ktory zapiszemy wynik */
                    attFile = File.createTempFile("docusafe_att_", ".tmp");

                    String profileName = Configuration.getProperty("signature.verification.pemheart_profile");
                    String pin = Configuration.getProperty("signature.verification.pin");
                    if (profileName == null || pin == null)
                        throw new EdmException(sm.getString("NiezdefiniowanoWdocusafe.propertiesNazwyProfiluLubPinuDlaPEMHEARTa"));
                    verificationResult = verification.verifyNormalSignature(profileName, pin, sigFile.getAbsolutePath(), attFile.getAbsolutePath());

                    if (verificationResult == verificationConstants.VERIFY_ERROR)
                        throw new EdmException(verification.getDescription());

                    correctlyVerified = !(verificationResult == verificationConstants.VERIFY_NEG || verificationResult == verificationConstants.VERIFY_NOT_VER);
                    verificationDescription = verification.getDescription();

                    int warningsCount = verification.getWarningsCount();
                    verificationWarnings = new ArrayList<String>();
                    for (int i=0; i < warningsCount; i++)
                    {
                        String warning = verification.getWarning(i);
                        verificationWarnings.add(warning);
                    }
                }
                else
                {
                    verificationResult = verificationConstants.VERIFY_NOT_VER;
                    throw new EdmException(sm.getString("NiePowiodloSieWczytanieBibliotekiWeryfikujacej")+"verification.dll");
                }
                                
                
                // zapisanie wyniku
                signature.setCorrectlyVerified(correctlyVerified);
                
                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());  
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally
            {
                if (stream != null) try { stream.close(); } catch (Exception e) { }
                if (sigFile != null) sigFile.delete();
                if (attFile != null) attFile.delete();
            }
        }
    }
    
    public Long getId()
    {
        return id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getSignatureId()
    {
        return signatureId;
    }
    
    public void setSignatureId(Long signatureId)
    {
        this.signatureId = signatureId;
    }
    
    public String getInFile()
    {
        return inFile;
    }
    
    public void setInFile(String inFile)
    {
        this.inFile = inFile;
    }
    
    public String getOutFile()
    {
        return outFile;
    }
    
    public void setOutFile(String outFile)
    {
        this.outFile = outFile;
    }
    
    public boolean isDoSign()
    {
        return doSign;
    }
    
    public void setDoSign(boolean doSign)
    {
        this.doSign = doSign;
    }
    
    public String getVerificationDescription()
    {
        return verificationDescription;
    }

    public List<String> getVerificationWarnings()
    {
        return verificationWarnings;
    }

    public int getVerificationResult()
    {
        return verificationResult;
    }

    public int getVERIFY_OK()
    {
        return verificationConstants.VERIFY_OK;
    }

    public int getVERIFY_NOT_VER()
    {
        return verificationConstants.VERIFY_NOT_VER;
    }

    public int getVERIFY_NEG()
    {
        return verificationConstants.VERIFY_NEG;
    }

    public int getVERIFY_PART_OK()
    {
        return verificationConstants.VERIFY_PART_OK;
    }

    public int getVERIFY_ERROR()
    {
        return verificationConstants.VERIFY_ERROR;
    }

    public String getVerificationInfo()
    {
        return verificationInfo;
    }

    public String getSignatureCtime()
    {
        return signatureCtime;
    }

    public String getSignatureAuthor()
    {
        return signatureAuthor;
    }
}
