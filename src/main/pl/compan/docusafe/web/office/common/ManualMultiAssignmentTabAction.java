package pl.compan.docusafe.web.office.common;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.process.manual.ManualMultiAssignmentManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.xes.XesLogVerification;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.common.HtmlUtils;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationTreeNode;
import pl.compan.docusafe.web.tree.organization.OrganizationTreeView;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;
import pl.compan.docusafe.webwork.event.*;

import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Predicates.*;

/**
 * User: Tomasz
 * Date: 18.02.13
 * Time: 13:11
 * 
 * Konfiguracja dla poprawnie dzialajacej dekretacji recznej do wielu realizatorow
 * manual-multi-assignment=true
 * manual-multi-assignment.multi-exec=true
 * mma.startNewProcess=true
 * mma.makeForkPush=true
 * mma.LastMultiExecDecretationAsPush=true
 * 
 * searchInManualMultiAssignment=true --> wyszukiwarka (opcjonalna)
 *  
 * TO DO
 */
public abstract class ManualMultiAssignmentTabAction extends BaseTabsAction
{
    private static final Logger log = LoggerFactory.getLogger(ManualMultiAssignmentTabAction.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(ManualMultiAssignmentTabAction.class.getPackage().getName(), null);

    public static final String KIND_EXECUTE = "EXECUTE";
    public static final String KIND_CC = "CC";
    public static final String KIND_CCK = "CCK";
    public static final String KIND_EXECUTE_W = "EXECUTE_W";
    public static final String TO_DIVISION = "D";
    public static final String TO_USER = "U";

    private String treeHtml;
    private String selectedGuid;
    private String targets[];
    private String kinds[];
    private String objectives[];
    private String clerks[];
	private String targetsKind[];
    private String deadlines[];

    private String activityIds[];
    private List<AssignmentObjective> objectivesList;
    private List<PlanedAssignmentBean> planedAssignments;
    private List<DSUser> clerksList;
    private List<DSUser> usersList;
    private String selectedUsers[];
    
    private boolean canAssignToExecute;
    private boolean canSetDeadline;
    private boolean canSetClerk;
    private boolean canMultiExec;

    private String clerk;
    private String powodUsuniecia;
    private boolean lastone = false;
    private Boolean startNewProcess;

    private String savedPlanedAssignmentName;
    private Set<String> savedPLanedAssignments;
    private String choosenPlanedAssignmentName;
    private String selectedDivisionOnTree;

    public static Map<String,String> kindLang = ImmutableMap.<String, String>builder().
                put(KIND_EXECUTE, SM.getString("doRealizacji")).
                put(KIND_CC,SM.getString("doWiadomosci")).
                put(KIND_CCK,SM.getString("doKonsultacji")).
                put(KIND_EXECUTE_W,SM.getString("doRealizacjiWiodacej")).
                build();

    public static Map<String,Map<String,Boolean>> availabilityMaps = ImmutableMap.<String,Map<String,Boolean>>builder().
                put(KIND_CC, ImmutableMap.<String, Boolean>builder().
                        put("archiwizacja.klon",false).
                        put("kancelaria.dekretacja", false).
                        put("kancelaria.zalaczniki", false).
                        put("blockAtributesIfCC",true).
                        put("kancelaria.wlacz.oznaczJakoCzystopis",false).
                        put("kancelaria.wlacz.podpisWewnetrzny",false).
                        put("kancelaria.wlacz.wersjeDokumentu",false).
                        put("assignment.block",true).
                        build()).
                put(KIND_CCK, ImmutableMap.<String, Boolean>builder().
                        put("archiwizacja.klon",false).
                        put("kancelaria.dekretacja", false).
                        put("kancelaria.zalaczniki", true).
                        put("blockAtributesIfCC",true).
                        put("kancelaria.wlacz.oznaczJakoCzystopis",false).
                        put("kancelaria.wlacz.podpisWewnetrzny",false).
                        put("kancelaria.wlacz.wersjeDokumentu",false).
                        put("assignment.block",true).
                        build()).
                build();

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssign").
                append(OpenHibernateSession.INSTANCE).
                append(new CleanUp()).
                append(new Assign()).
                append(new XesLogVerification()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAssignJson").
                append(OpenHibernateSession.INSTANCE).
                append(new JsonAssignDataInterceptor()).
                append(new CleanUp()).
                append(new Assign()).
                append(fillForm).
                append(new JsonView()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doLastone").
                append(OpenHibernateSession.INSTANCE).
                append(new CleanUp()).
                append(new Lastone()).
                append(new Assign()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
                append(OpenHibernateSession.INSTANCE).
                append(new Save()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    protected abstract List prepareTabs();

    private class FillForm extends LoggedActionListener {
        public void actionPerformed(ActionEvent event, Logger log) throws Exception
        {
            setTabs(prepareTabs());
            objectivesList = AssignmentObjective.list();
            clerksList = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
            usersList = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
            canAssignToExecute = true;
            if (!AvailabilityManager.isAvailable("manual-multi-assignment.checkIfCanAssignToExecute")) {
            } else {
                WorkflowFactory workflow = WorkflowFactory.getInstance();
                for (String activityId : Objects.firstNonNull(getActivityIds(), new String[]{getActivity()})) {
                    if (workflow.getProcessName(activityId).equals(Jbpm4Constants.READ_ONLY_PROCESS_NAME)) {
                        canAssignToExecute = false;
                    }
                }
            }

            canSetDeadline = DSApi.context().hasPermission(DSPermission.PROCES_WYZNACZENIE_TERMINU);
            canSetClerk = DSApi.context().hasPermission(DSPermission.PISMO_ZMIANA_REFERENTA);
            canMultiExec = !AvailabilityManager.isAvailable("manual-multi-assignment.multi-exec");


            String namesAsString = DSApi.context().userPreferences().node("assignment").get("names", null);
            if(namesAsString == null) {
                savedPLanedAssignments = Sets.newHashSet();
            } else {
                savedPLanedAssignments = Sets.newHashSet(Splitter.on(';').split(namesAsString));
            }
            if(choosenPlanedAssignmentName != null && choosenPlanedAssignmentName.length() > 0) {
                String planedAssignmentsStr = DSApi.context().userPreferences().node("assignment").node(choosenPlanedAssignmentName).get("planedAssignmentsStr",null);
                if(planedAssignmentsStr != null) {
                    ByteArrayInputStream b = null;
                    ObjectInputStream o = null;

                    b = new ByteArrayInputStream(Base64.decodeBase64(planedAssignmentsStr.getBytes()));
                    o = new ObjectInputStream(b);
                    planedAssignments = (List<ManualMultiAssignmentTabAction.PlanedAssignmentBean>) o.readObject();
                    o.close();
                    b.close();
                }
            }
            savedPlanedAssignmentName = null; //choosenPlanedAssignmentName;
            choosenPlanedAssignmentName = null;

            if(activityIds != null && activityIds.length > 0){
                Set<String> activitiIdsSet = Sets.newHashSet(Iterables.filter(Arrays.asList(activityIds),not(or(equalTo("null"), isNull()))));
                Set<String> toRemove = Sets.newHashSet();
                for(String activityId : activitiIdsSet) {
                    if(AvailabilityManager.isAvailable("assignment.block", null, activityId)) {
                        toRemove.add(activityId);
                        addActionError(SM.getString("DekredacjaZablokowanaDla",Jbpm4ProcessLocator.documentIdForActivity(activityId.split(",")[1])));
                    }
                }
                activitiIdsSet.removeAll(toRemove);
                activityIds = activitiIdsSet.toArray(new String[0]);
            }


            if((activityIds == null || activityIds.length < 1) && getActivity() == null) {
                addActionError(SM.getString("BrakPismDoDekretacji"));
                event.setResult("after");
            }

            if(targets != null && targets.length > 0 && planedAssignments == null) {
                planedAssignments = computePlanedAssignments();
            }

            if(selectedGuid != null) {
                DSDivision selectedDivision = DSDivision.find(selectedGuid);
                selectedDivisionOnTree = selectedDivision.getName();
                treeHtml = ExtendedOrganizationTree.createOrganizationTree(
                        selectedDivision,
                        null,
                        new ExpandUrlProvider(),
                        new SelectOrganizationTreeView(
                                new ExpandUrlProvider(),
                                ServletActionContext.getRequest().getContextPath(),
                                selectedDivision,
                                null,
                                new SelectUrlProvider()),
                        ServletActionContext.getRequest().getContextPath())
                        .generateTree();
            } else {
                treeHtml = ExtendedOrganizationTree.createOrganizationTree(
                        new ExpandUrlProvider(),
                        new SelectOrganizationTreeView(
                                new ExpandUrlProvider(),
                                ServletActionContext.getRequest().getContextPath(),
                                null,
                                null,
                                new SelectUrlProvider()),
                        ServletActionContext.getRequest().getContextPath())
                        .generateTree();
            }
        }

        public Logger getLogger() {
            return log;
        }
    }

    private class JsonAssignDataInterceptor extends RichActionListener {

        public class AssignRequest {
            public String[] activityIds;
            public String[] targetsKind;
            public String[] targets;
            public String[] kinds;
            public String[] deadlines;
            public String[] objectives;
            public String[] clerks;

            public void copyProperties(ManualMultiAssignmentTabAction to) {
                to.setActivityIds(activityIds);
                to.setTargetsKind(targetsKind);
                to.setTargets(targets);
                to.setKinds(kinds);
                to.setDeadlines(deadlines);
                to.setObjectives(objectives);
                to.setClerks(clerks);
            }
        }

        @Override
        public void action(ActionEvent event) throws Exception {

            HttpServletRequest request = ServletActionContext.getRequest();

            InputStream body = null;
            try {
                body = request.getInputStream();

                StringWriter writer = new StringWriter();
                IOUtils.copy(body, writer, "UTF-8");
                String jsonString = writer.toString();

                AssignRequest assignRequest = new Gson().fromJson(jsonString, AssignRequest.class);

                assignRequest.copyProperties(ManualMultiAssignmentTabAction.this);
            } catch(Exception ex) {
                log.error("[JsonAssignDataInterceptor] cannot process data from request");
                if(body != null) {
                    body.close();
                }
                throw new Exception(ex);
            }
        }
    }

    /**
     * <p>Placeholder po to, �eby odzyska� messages i errors w postaci json.</p>
     */
    private class JsonView extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            return null;
        }
    }

    private List<PlanedAssignmentBean> computePlanedAssignments() throws EdmException {
        List<PlanedAssignmentBean> list = new LinkedList<PlanedAssignmentBean>();
        PlanedAssignmentBean bean;

        for(int i=0;i<targets.length;i++) {
            bean = new PlanedAssignmentBean();
            bean.setTarget(targets[i]);
            bean.setKind(kinds[i]);
            bean.setObjective(objectives != null ? objectives[i] : null);
            bean.setClerk(clerks != null ? clerks[i] : null);
            bean.setTargetKind(targetsKind[i]);
            bean.setTargetName("U".equals(targetsKind[i]) ? DSUser.findByUsername(targets[i]).asFirstnameLastname() : DSDivision.find(targets[i]).getName());
            bean.setKindName(kindLang.get(kinds[i]));
            bean.setDeadline(deadlines != null ? deadlines[i] : null);
            list.add(bean);
        }
        return list;
    }

    private static class SelectOrganizationTreeView extends OrganizationTreeView {
        private OrganizationUrlProvider actionProvider;

        public SelectOrganizationTreeView(OrganizationUrlProvider urlProvider, String contextPath, DSDivision selectedDivision, DSUser selectedUser, OrganizationUrlProvider actionProvider) {
            super(urlProvider, contextPath, selectedDivision, selectedUser);
            this.actionProvider = actionProvider;
        }

        public String getUserHtml(OrganizationTreeNode node) {
            String substituted;
            try {
                substituted = node.getUser().getSubstitute() != null ?
                        " ("
                                + sm.getString("zastepowanyPrzez")
                                + " "
                                + node.getUser().getSubstitute().asLastnameFirstname()
                                + ") "
                        : " ";

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                substituted = " ";
            }

            if (!node.isBackup()) {
                return HtmlUtils.toHtmlAnchor(actionProvider.getUrl(null,node.getUser()), node.getUser().asLastnameFirstname())
                       + substituted + addTaskCountIfAvaialbe(node)
                        + HtmlUtils.toHtmlAnchor(actionProvider.getUrl(null,node.getUser()),
                        HtmlUtils.toImg(new HtmlImage(Docusafe.getBaseUrl()+"/img/document-move.gif", 17, 17),"dekretuj","dekretuj"));
            } else {
                return HtmlUtils.toHtmlAnchor(actionProvider.getUrl(null,node.getUser()), node.getUser().asLastnameFirstname(), "backup")
                		 + substituted+ addTaskCountIfAvaialbe(node)
                        + HtmlUtils.toHtmlAnchor(actionProvider.getUrl(null,node.getUser()),
                        HtmlUtils.toImg(new HtmlImage(Docusafe.getBaseUrl()+"/img/document-move.gif", 17, 17),"dekretuj","dekretuj"));
            }
        }


		@Override
        public String getDivisionHtml(OrganizationTreeNode node) {
            return super.getDivisionHtml(node)
                    + " "
                    + HtmlUtils.toHtmlAnchor(actionProvider.getUrl(node.getDivision(),null),
                    HtmlUtils.toImg(new HtmlImage(Docusafe.getBaseUrl()+"/img/document-move.gif", 17, 17),"dekretuj","dekretuj"));
        }
    }
      private static class SelectUrlProvider implements OrganizationUrlProvider{
        public String getUrl(DSDivision div, DSUser user) {
            if(user != null) {
                return "javascript:openDialog('"+user.asFirstnameLastname()+"','"+user.getName()+"','U');";
            }
            else {
                return "javascript:openDialog('"+div.getName()+"','"+div.getGuid()+"','D');";
            }
        }
    }

    private static class ExpandUrlProvider implements OrganizationUrlProvider{
        public String getUrl(DSDivision div, DSUser user) {
            if(div == null) {
                return "#";
            } else {
                return "javascript:expandDivision('"+div.getGuid()+"');";
            }
        }
    }
    private static String addTaskCountIfAvaialbe(OrganizationTreeNode node)
	{
    	String taskCount="";
    	 if (AvailabilityManager.isAvailable("mma.HtmlTreeShowTaskCountForUser"))
 		{
 			TaskListCounterManager t = new TaskListCounterManager(false);
 			try
 			{
 				taskCount = t.getTaskListUserCount(node.getUser().getName(), false);
 			} catch (EdmException e)
 			{
 				log.error(e.getMessage(), e);
 			}
 		}
		return taskCount;

	}
    public static class PlanedAssignmentBean implements Serializable {

        private String target;
        private String kind;
        private String objective;
        private String clerk;
        private String targetKind;
        private String targetName;
        private String kindName;
        private String deadline;

        public PlanedAssignmentBean() {}

        public String getTarget() {
            return target;
        }

        public String getKind() {
            return kind;
        }

        public String getObjective() {
            return objective;
        }

        public String getTargetKind() {
            return targetKind;
        }

        public String getTargetName() {
            return targetName;
        }

        public String getKindName() {
            return kindName;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public void setObjective(String objective) {
            this.objective = objective;
        }

        public void setTargetKind(String targetKind) {
            this.targetKind = targetKind;
        }

        public void setTargetName(String targetName) {
            this.targetName = targetName;
        }

        public void setKindName(String kindName) {
            this.kindName = kindName;
        }

        public String getDeadline() {
            return deadline;
        }

        public void setDeadline(String deadline) {
            this.deadline = deadline;
        }

		public void setClerk(String clerk) {
			this.clerk = clerk;
		}

		public String getClerk() {
			return clerk;
		}
    }

	private  void dekretujDoOdbiorcy( String activityId, Map<String, Object> params) throws EdmException
	{
		Jbpm4WorkflowFactory.reassign(
                activityId,
                documentId,
                DSApi.context().getPrincipalName(),
                (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
                params);

	}



    private class Assign extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            ManualMultiAssignmentTabAction.log.info("ManualMultiAssignmentTabAction -> Assing");

            if (targets == null || targets.length == 0)
            {
            	if(AvailabilityManager.isAvailable("showMessageFromMMA.PIG")){
            		addActionMessage(SM.getString("nieZostalawybranaOsobaDzial"));
            	}else{
            		addActionMessage(SM.getString("nieWybranoUzytkownikaDzialu"));
            	}
                event.setResult("after");
                return;
            }

            ManualMultiAssignmentManager manager = new ManualMultiAssignmentManager();
            manager.setTargets(targets);
            manager.setKinds(kinds);
            manager.setTargetsKind(targetsKind);
            manager.setDeadlines(deadlines);
            manager.setLastone(lastone);
            manager.setDocumentId(documentId);
            manager.setActivityIds(activityIds);
            manager.setObjectives(objectives);
            manager.setActivity(getActivity());
            manager.setStartNewProcess(startNewProcess);
            manager.setClerk(clerk);

            manager.manualAssign();

            event.setResult("after");

            if(getDocumentId() != null)
                addActionMessage(SM.getString("PrzekazanoZadanieIDKO",getDocumentId(),OfficeDocument.find(getDocumentId()).getOfficeNumber()));
            else
                addActionMessage(SM.getString("PrzekazanoZadania"));
    }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class CleanUp extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(activityIds != null && activityIds.length > 0){
                activityIds = Sets.newHashSet( //usuwa duplikaty i nulle
                        Iterables.filter(
                                Arrays.asList(activityIds),
                                not(or(equalTo("null"), isNull()))))
                        .toArray(new String[0]);
                log.info("activities = {}", Joiner.on('|').join(activityIds));

                DSApi.context().begin();
                for(String activityId : activityIds){
                    Jbpm4WorkflowFactory.acceptTaskIfNotAccepted(activityId, event);
                }
                DSApi.context().commit();
            }
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class Lastone extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            ManualMultiAssignmentTabAction.log.info("ManualMultiAssignmentTabAction -> Lastone");
            log.debug("lastone");
            lastone = true;
        }

        @Override
        public Logger getLogger()
        {
            return log;
        }
    }

    private class Save extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            LoggerFactory.getLogger("tomekl").debug("ManualMultiAssignmentTabAction -> Save");

            try {
                DSApi.context().begin();

                if(targets == null || targets.length < 1) {
                    throw new EdmException(SM.getString("BrakDanychDoZapisania"));
                }

                if(savedPlanedAssignmentName == null || savedPlanedAssignmentName.length() < 1) {
                    throw new EdmException(SM.getString("NiePodanoNazwyDekretacji"));
                }

                List<ManualMultiAssignmentTabAction.PlanedAssignmentBean> planedAssignmentsToPersist = computePlanedAssignments();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(planedAssignmentsToPersist);
                oos.close();
                baos.close();
                String planedAssignmentsStr = org.apache.commons.codec.binary.Base64.encodeBase64String(baos.toByteArray());

                String namesAsString = DSApi.context().userPreferences().node("assignment").get("names", null);
                Set names = namesAsString == null ? Sets.newHashSet() : Sets.newHashSet(Splitter.on(';').split(DSApi.context().userPreferences().node("assignment").get("names", null)));

                names.add(savedPlanedAssignmentName);

                DSApi.context().userPreferences().node("assignment").put("names", Joiner.on(';').skipNulls().join(names));
                DSApi.context().userPreferences().node("assignment").node(savedPlanedAssignmentName).put("planedAssignmentsStr",planedAssignmentsStr);

                DSApi.context().commit();

            } catch (Exception e) {
                e.printStackTrace();
                getLogger().debug(e.getMessage(), e);
                DSApi.context()._rollback();
                addActionError(e.getMessage());
            }
        }

        @Override
        public Logger getLogger()
        {
            return log;
        }
    }

    public String getSavedPlanedAssignmentName() {
        return savedPlanedAssignmentName;
    }

    public void setSavedPlanedAssignmentName(String savedPlanedAssignmentName) {
        this.savedPlanedAssignmentName = savedPlanedAssignmentName;
    }

    public Set<String> getSavedPLanedAssignments() {
        return savedPLanedAssignments;
    }

    public void setSavedPLanedAssignments(Set<String> savedPLanedAssignments) {
        this.savedPLanedAssignments = savedPLanedAssignments;
    }

    public String getTreeHtml() {
        return treeHtml;
    }

    public String getSelectedGuid() {
        return selectedGuid;
    }

    public void setSelectedGuid(String selectedGuid) {
        this.selectedGuid = selectedGuid;
    }

    public List<AssignmentObjective> getObjectivesList() {
        return objectivesList;
    }

    public void setTargets(String[] targets) {
        this.targets = targets;
    }

    public void setKinds(String[] kinds) {
        this.kinds = kinds;
    }

    public void setObjectives(String[] objectives) {
        this.objectives = objectives;
    }

    public void setTargetsKind(String[] targetsKind) {
        this.targetsKind = targetsKind;
    }

    public List<PlanedAssignmentBean> getPlanedAssignments() {
        return planedAssignments;
    }

    public void setDeadlines(String[] deadlines) {
        this.deadlines = deadlines;
    }

    public boolean isCanSetDeadline() {
        return canSetDeadline;
    }

    public boolean isCanAssignToExecute() {
        return canAssignToExecute;
    }

    public String[] getActivityIds() {
        return activityIds;
    }

    public void setActivityIds(String[] activityIds) {
        this.activityIds = activityIds;
    }

    public boolean isCanSetClerk() {
        return canSetClerk;
    }

    public List<DSUser> getClerksList() {
        return clerksList;
    }

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public boolean isCanMultiExec() {
        return canMultiExec;
    }

    public String getPowodUsuniecia(){
    	return powodUsuniecia;
    }

    public void setPowodUsuniecia(String powodUsuniecia){
    	this.powodUsuniecia=powodUsuniecia;
    }

    public String[] getClerks() {
        return clerks;
    }

    public void setClerks(String[] clerks) {
        this.clerks = clerks;
    }

    public Boolean isStartNewProcess()
    {
        return startNewProcess;
    }

    public void setStartNewProcess(Boolean startNewProcess)
    {
        this.startNewProcess = startNewProcess;
    }

    public String getChoosenPlanedAssignmentName() {
        return choosenPlanedAssignmentName;
    }

    public void setChoosenPlanedAssignmentName(String choosenPlanedAssignmentName) {
        this.choosenPlanedAssignmentName = choosenPlanedAssignmentName;
    }

	public List<DSUser> getUsersList()
	{
		return usersList;
	}

	public void setUsersList(List<DSUser> usersList)
	{
		this.usersList = usersList;
	}

    public String getSelectedDivisionOnTree() {
        return selectedDivisionOnTree;
    }

    public void setSelectedDivisionOnTree(String selectedDivisionOnTree) {
        this.selectedDivisionOnTree = selectedDivisionOnTree;
    }
}
