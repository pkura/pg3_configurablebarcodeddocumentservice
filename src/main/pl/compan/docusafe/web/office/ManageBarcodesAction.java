package pl.compan.docusafe.web.office;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeManager;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeRange;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ManageBarcodesAction extends EventActionSupport
{
	
	private static final Logger log  = LoggerFactory.getLogger(ManageBarcodesAction.class);
	private List<BarcodeRange> ranges;
	private Long rangeId;
	private String percentage;
	private Long usedBarcodes;
	private Long leftBarcodes;
	private Integer showstatus;
	private Map<String,Integer> statuses;
	private StringManager sm = StringManager.getManager(BarcodesOrderAction.class.getPackage().getName());
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAccept").
        	append(OpenHibernateSession.INSTANCE).
        	append(new AcceptRange()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doReject").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new RejectRange()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doPrint").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new PrintRange()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSend").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new SendRange()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if(showstatus==null)
        		{
        			showstatus = BarcodeManager.RANGE_STATUS_NEW;
        		}
        		if(showstatus<0)
        		{
        			ranges = BarcodeManager.getAllRanges();
        		}
        		else
        		{
            		ranges = BarcodeManager.getRangeByStatus(showstatus);
        		}
        		for(BarcodeRange br: ranges)
        		{
        			try
        			{
        			LoggerFactory.getLogger("kubaw").debug(br.getNextBarcodeAsStringOrNullOwnTransaction());
        			}catch(Exception e)
        			{
        				LoggerFactory.getLogger("kuba").debug("Zakres nieaktywny");
        			}
        		}
        		prepareStatuses();
        		Double perc = new Double(new Double(BarcodeManager.getActivePool().getCurrentPointer()-BarcodeManager.getActivePool().getStartBarcode())/
        				new Double(BarcodeManager.getActivePool().getEndBarcode()-BarcodeManager.getActivePool().getStartBarcode()));
        		NumberFormat nf = new DecimalFormat("#0.00");
        		percentage = nf.format(perc*100)+"%";
        		usedBarcodes = BarcodeManager.getActivePool().getCurrentPointer()-BarcodeManager.getActivePool().getStartBarcode();
        		leftBarcodes = BarcodeManager.getActivePool().getEndBarcode()-BarcodeManager.getActivePool().getCurrentPointer();
        	}
        	catch(EdmException e)
        	{
        		log.error(sm.getString("NieUdaloSieZaladowacListyZakresow"), e);
        		addActionError(sm.getString("NieUdaloSieZaladowacListyZakresow"));
        	}
        }
    }
	
	private class AcceptRange implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
        	{
				BarcodeManager.acceptBarcodeRange(rangeId, null);
        	}
        	catch(EdmException e)
        	{
        		log.error(e.getMessage(), e);
        		addActionError(sm.getString("NieUdaloSieZaakceptowacZamowieniaNaBarkody"));
        	}
        }
	}

	private class RejectRange implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		BarcodeManager.setStatusBarcodeRange(rangeId, BarcodeManager.RANGE_STATUS_REJECTED);
        	}
        	catch(EdmException e)
        	{
        		log.error(e.getMessage(), e);
        		addActionError(sm.getString("NieUdaloSieZmienicStatusuZamowienia"));
        	}
        	
        }
    }
	
	private class PrintRange implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		BarcodeManager.setStatusBarcodeRange(rangeId, BarcodeManager.RANGE_STATUS_PRINTED);
        	}
        	catch(EdmException e)
        	{
        		log.error(e.getMessage(), e);
        		addActionError(sm.getString("NieUdaloSieZmienicStatusuZamowienia"));
        	}
        	
        }
    }
	
	private class SendRange implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		BarcodeManager.setStatusBarcodeRange(rangeId, BarcodeManager.RANGE_STATUS_SENT);
        	}
        	catch(EdmException e)
        	{
        		log.error(e.getMessage(), e);
        		addActionError(sm.getString("NieUdaloSieZmienicStatusuZamowienia"));
        	}
        	
        }
    }
	
	private void prepareStatuses()
	{
		statuses = new HashMap<String, Integer>();
		statuses.put(sm.getString("Nowy"), BarcodeManager.RANGE_STATUS_NEW);
		statuses.put(sm.getString("Zaakceptowany"), BarcodeManager.RANGE_STATUS_ACCEPTED);
		statuses.put(sm.getString("Aktywny"), BarcodeManager.RANGE_STATUS_INUSE);
		statuses.put(sm.getString("Drukowany"), BarcodeManager.RANGE_STATUS_PRINTED);
		statuses.put(sm.getString("Odrzucony"), BarcodeManager.RANGE_STATUS_REJECTED);
		statuses.put(sm.getString("Wyslany"), BarcodeManager.RANGE_STATUS_SENT);
		statuses.put(sm.getString("Zuzyty"), BarcodeManager.RANGE_STATUS_USED);
		statuses.put(sm.getString("Anulowany"), BarcodeManager.RANGE_STATUS_CANCELED);
		statuses.put(sm.getString("Zareklamowany"), BarcodeManager.RANGE_STATUS_ADVERTISED);
		statuses.put(sm.getString("Wszystkie"), -1);
	}
	public List<BarcodeRange> getRanges() {
		return ranges;
	}

	public void setRanges(List<BarcodeRange> ranges) {
		this.ranges = ranges;
	}

	public Long getRangeId() {
		return rangeId;
	}

	public void setRangeId(Long rangeId) {
		this.rangeId = rangeId;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public Long getUsedBarcodes() {
		return usedBarcodes;
	}

	public void setUsedBarcodes(Long usedBarcodes) {
		this.usedBarcodes = usedBarcodes;
	}

	public Long getLeftBarcodes() {
		return leftBarcodes;
	}

	public void setLeftBarcodes(Long leftBarcodes) {
		this.leftBarcodes = leftBarcodes;
	}

	
	public Integer getShowstatus() {
		return showstatus;
	}
	public void setShowstatus(Integer showstatus) {
		this.showstatus = showstatus;
	}
	public Map<String, Integer> getStatuses() {
		return statuses;
	}
	public void setStatuses(Map<String, Integer> statuses) {
		this.statuses = statuses;
	}


}
