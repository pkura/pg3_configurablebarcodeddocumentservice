package pl.compan.docusafe.web.nationwide;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;
import std.lambda;

/* User: Administrator, Date: 2006-01-23 12:53:10 */

/**
 * Akcja tworz�ca dokument - wpis do Rejestru Szk�d.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CreateDRSAction.java,v 1.15 2008/09/16 12:47:47 tomekl Exp $
 */
public class CreateDRSAction extends EventActionSupport
{
    private Long sourceId;
    private String NR_SZKODY;
    private Integer ROK_WPISU;
    private String NR_POLISY;
    private String RODZAJ;
    private Date DATA;
    private String sender;
    private Boolean closeTasks;
    
    
    // @EXPORT
    private Long documentId;
    private Integer NR_KOLEJNY;
    private String sourceLink;
    private Collection<Map> attachments;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doCancel").
            append(new Cancel());
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (sourceId == null)
            {
                addActionError("Nie podano ID dokumentu �r�d�owego");
                return;
            }

            try
            {
                Document nwDocument = Document.find(sourceId);

                Doctype doctype = nwDocument.getDoctype();
                Map values = doctype.getValueMapByCn(nwDocument.getId());
                NR_POLISY = (String) values.get("NR_POLISY");
                RODZAJ = doctype.getFieldByCn("RODZAJ").getEnumItem((Integer) values.get("RODZAJ")).getTitle();
                DATA = (Date) values.get("DATA_WPLYWU");
                if (nwDocument.getType() == DocumentType.INCOMING)
                {
                    sender = ((InOfficeDocument) nwDocument).getSender().getSummary();
                }

                attachments = fun.map(nwDocument.listAttachments(),
                    new lambda<Attachment, Map>()
                    {
                        public Map act(Attachment attachment)
                        {
                            BeanBackedMap result = new BeanBackedMap(attachment,
                                "id", "mostRecentRevision", "document", "title", "barcode", "lparam");
                            try
                            {
                                result.put("author", DSUser.findByUsername(attachment.getAuthor()));
                            }
                            catch (EdmException e)
                            {
                            }
                            return result;
                        }
                    });
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (NR_SZKODY == null)
            {
                addActionError("Nie podano numeru szkody");
                return;
            }

                event.setResult("finish");

        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (sourceId == null)
            {
                addActionError("Nie podano ID dokumentu �r�d�owego");
                return;
            }

            if (NR_SZKODY == null)
            {
                addActionError("Nie podano numeru szkody");
                return;
            }

            if (ROK_WPISU == null)
            {
                addActionError("Nie podano roku wpisu");
                return;
            }

            try
            {
            	
                DSApi.context().begin();
                
                Document nwDocument = Document.find(sourceId);

                if (nwDocument.getType() == DocumentType.INCOMING)
                    sourceLink = "/office/incoming/summary.action?documentId=" + nwDocument.getId();
                else
                    sourceLink = "/repository/edit-document.action?id="+nwDocument.getId();
                
                
                if(closeTasks != null && closeTasks)
                	WorkflowFactory.manualFinish(nwDocument.getId(),false);
                
                DSApi.context().commit();

                event.setResult("finish2");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            /*finally
            {
            	DSApi._close();
            }*/
        }
    }

    private class Cancel implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("cancel");
        }
    }

    public Long getSourceId()
    {
        return sourceId;
    }

    public void setSourceId(Long sourceId)
    {
        this.sourceId = sourceId;
    }

    public String getNR_SZKODY()
    {
        return NR_SZKODY;
    }

    public void setNR_SZKODY(String NR_SZKODY)
    {
        this.NR_SZKODY = NR_SZKODY;
    }

    public Integer getROK_WPISU()
    {
        return ROK_WPISU;
    }

    public void setROK_WPISU(Integer ROK_WPISU)
    {
        this.ROK_WPISU = ROK_WPISU;
    }

    public String getNR_POLISY()
    {
        return NR_POLISY;
    }

    public String getRODZAJ()
    {
        return RODZAJ;
    }

    public Date getDATA()
    {
        return DATA;
    }

    public String getSender()
    {
        return sender;
    }

    public void setNR_POLISY(String NR_POLISY)
    {
        this.NR_POLISY = NR_POLISY;
    }

    public void setRODZAJ(String RODZAJ)
    {
        this.RODZAJ = RODZAJ;
    }

    public void setDATA(Date DATA)
    {
        this.DATA = DATA;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public Integer getNR_KOLEJNY()
    {
        return NR_KOLEJNY;
    }

    public String getSourceLink()
    {
        return sourceLink;
    }

	public Boolean getCloseTasks() {
		return closeTasks;
	}

	public void setCloseTasks(Boolean closeTasks) {
		this.closeTasks = closeTasks;
	}
}
