package pl.compan.docusafe.web.nationwide;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.util.Date;

/* User: Administrator, Date: 2005-08-11 12:50:48 */

/**
 * Umo�liwia seryjne umieszczenie dokument�w w pude�kach archiwalnych.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SerialBoxingAction.java,v 1.7 2007/06/20 06:55:36 lk Exp $
 */
public class SerialBoxingAction extends EventActionSupport
{
    private String boxName;
    private String startId;
    private String endId;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doBox").
            append(OpenHibernateSession.INSTANCE).
            append(new DoBox()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class DoBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            startId = TextUtils.trimmedStringOrNull(startId);
            endId = TextUtils.trimmedStringOrNull(endId);

            if (startId == null)
                addActionError("Nie podano pocz�tkowego numeru dokumentu");
            if (endId == null)
                addActionError("Nie podano ko�cowego numeru dokumentu");

            if (hasActionErrors())
                return;

            startId = startId.toUpperCase();
            endId = endId.toUpperCase();

            try
            {
                DSApi.context().begin();

                try
                {
                    Box.findByName(null,boxName);
                    throw new EdmException("Istnieje ju� pud�o o numerze "+boxName);
                }
                catch (EntityNotFoundException e)
                {
                }




                // walidacja - czy wszystkie dokumenty maj� ten sam typ
                Integer idRODZAJ = null;

                Box box = new Box(boxName);
                box.create();
                box.setOpenTime(new Date());
                box.setCloseTime(new Date());

                // umieszczanie dokument�w w pudle
                int count = 0;

                DSApi.context().commit();

                addActionMessage("W pudle "+box.getName()+" umieszczono "+count+" dokument�w");
            }
            catch (HibernateException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError("Wyst�pi� b��d odczytu dokumentu");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getBoxName()
    {
        return boxName;
    }

    public void setBoxName(String boxName)
    {
        this.boxName = boxName;
    }

    public String getStartId()
    {
        return startId;
    }

    public void setStartId(String startId)
    {
        this.startId = startId;
    }

    public String getEndId()
    {
        return endId;
    }

    public void setEndId(String endId)
    {
        this.endId = endId;
    }
}
