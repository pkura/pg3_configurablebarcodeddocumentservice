package pl.compan.docusafe.web.nationwide;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/* User: Administrator, Date: 2005-08-25 15:57:47 */

/**
 * Wy�wietla wszystkie za��czniki graficzne z przekazanej listy
 * dokument�w po��czone w jeden plik PDF.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewAttachmentsAsPdfAction.java,v 1.18 2008/10/06 11:30:35 pecet4 Exp $
 */


//W przypadku konieczno��i usuni�cia tej klasy prosz� o wcze�niejszy kontakt - Maciej Starosz

public class ViewAttachmentsAsPdfAction extends EventActionSupport
{
	private StringManager sm=
        GlobalPreferences.loadPropertiesFile(ViewAttachmentsAsPdfAction.class.getPackage().getName(),null);
    private Long[] documentIds;
    private boolean remarksLayer;
    private boolean czyUwagi;
    private boolean czyDekretacja;
    private boolean czyHistoria;
    private final static float iwidth = 550, iheight = 700;
    private final static float absX = 20, absY = 20;

    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           // ArrayList<Long> list = new ArrayList<Long>();
           // list.add(new Long(3203));
           // documentIds = list.toArray();
           // Long[] ids = {new Long(3203)};
            
            if (documentIds == null || documentIds.length == 0)
                addActionError(sm.getString("NiePodanoIdentyfikatorowDokumentow"));

            if (hasActionErrors())
            {
                event.setResult(ERROR);
                return;
            }

            File pdf = null;

            try
            {
                DSApi.context().begin();

                pdf = File.createTempFile("docusafe_tmp_", ".pdf");
                OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));
                if(isCzyUwagi() != false || isCzyHistoria() != false || isCzyDekretacja() != false){
                	PdfUtils.attachmentsAsPdfWith(documentIds, remarksLayer, os, isCzyUwagi(), isCzyHistoria(), isCzyDekretacja(), null, null, null, false);
                }else{
                	PdfUtils.attachmentsAsPdf(documentIds, remarksLayer, os, null, null, null, false);
                }
                
                
                os.close();

                DSApi.context().rollback();
            }
            catch (Exception e)
            {
                event.getLog().error(e.getMessage(), e);
                event.setResult(ERROR);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
                if (pdf != null) pdf.delete();
                pdf = null;
            }

            if (pdf != null && pdf.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("rozmiar pliku "+pdf+": "+pdf.length());
                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdf),
                        "application/pdf", (int) pdf.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    pdf.delete();
                }
            }
        }
    }

    public void setDocumentIds(Long[] documentIds)
    {
        this.documentIds = documentIds;
    }

    public void setRemarksLayer(boolean remarksLayer)
    {
        this.remarksLayer = remarksLayer;
    }

	public boolean isCzyUwagi() {
		return czyUwagi;
	}

	public void setCzyUwagi(boolean czyUwagi) {
		this.czyUwagi = czyUwagi;
	}

	public boolean isCzyDekretacja() {
		return czyDekretacja;
	}

	public void setCzyDekretacja(boolean czyDekretacja) {
		this.czyDekretacja = czyDekretacja;
	}

	public boolean isCzyHistoria() {
		return czyHistoria;
	}

	public void setCzyHistoria(boolean czyHistoria) {
		this.czyHistoria = czyHistoria;
	}
}
