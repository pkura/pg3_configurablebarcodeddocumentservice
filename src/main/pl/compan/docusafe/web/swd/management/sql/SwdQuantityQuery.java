package pl.compan.docusafe.web.swd.management.sql;

import org.apache.commons.lang.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.zbp.users.OrganisationUnit;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdQuantityQuery implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(SwdQuantityQuery.class);
    private Integer id;
    private String cn;
    private String title;
    private String sqlQuery;
    /**
     * Je�eli zapytanie zwraca wiele warto�ci, w pierwszej kolumnie musi by� liczba, w drugiej nazwa (do��czana do tytu�u)
     */
    private Boolean multiResults;

    public static List<Map.Entry<String,Integer>> findQuantities(ObjectQuantityCns type, Object obj){
        List<SwdQuantityQuery> queries = searchQueries(type);
        List<Map.Entry<String,Integer>> quantities = executeQueries(queries, obj);
        return quantities;
    }

    public static List<SwdQuantityQuery> searchQueries(ObjectQuantityCns type) {
        List<SwdQuantityQuery> queries = new ArrayList<SwdQuantityQuery>();
        for (QuantityCn quantityCn : type.quantityCn)
            queries.addAll(searchQueries(quantityCn));
        return queries;
    }

    public static List<SwdQuantityQuery> searchQueries(QuantityCn type) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(SwdQuantityQuery.class);

            criteria.add(Restrictions.eq("cn", type.getCn()));

            return criteria.list();
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return new ArrayList<SwdQuantityQuery>();
    }

    public static List<Map.Entry<String, Integer>> executeQueries(List<SwdQuantityQuery> queries, Object obj) {
        List<Map.Entry<String, Integer>> results = new ArrayList<Map.Entry<String, Integer>>();

        for (SwdQuantityQuery query : queries)
            results.addAll(executeQuery(query, obj));
        return results;
    }

    public static List<Map.Entry<String, Integer>> executeQuery(SwdQuantityQuery query, Object obj) {
        List<Map.Entry<String, Integer>> results = new ArrayList<Map.Entry<String, Integer>>();

        DSContextOpener opener = null;
        PreparedStatement ps = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();

            ps = DSApi.context().prepareStatement(query.getSqlQuery());
            QuantityCn.valueOf(query.getCn()).setParams(ps, obj);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Integer quantity = rs.getInt(1);
                String endTitle = query.isMultiResults() ? rs.getString(2) : "";

                results.add(new AbstractMap.SimpleEntry<String, Integer>(toTitle(query.getTitle(), endTitle), quantity));
            }

        } catch (EdmException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (opener != null)
                opener.closeIfNeed();
        }
        return results;
    }

    private static String toTitle(String title, String endTitle) {
        return title + endTitle;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    public boolean isMultiResults() {
        return multiResults != null && multiResults;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getMultiResults() {
        return multiResults;
    }

    public void setMultiResults(Boolean multiResults) {
        this.multiResults = multiResults;
    }

    public enum ObjectQuantityCns {
        ORGANISATION_UNIT(QuantityCn.ORGANISATION_UNIT_ID);
        private final QuantityCn[] quantityCn;

        ObjectQuantityCns(QuantityCn... quantityCn) {
            this.quantityCn = quantityCn;
        }
    }

    public enum QuantityCn {
        /**
         * W PreparedStatement nalezy ustawi� organisation unit id
         */
        ORGANISATION_UNIT_ID;
        private final String cn;

        QuantityCn() {
            this(null);
        }

        QuantityCn(String cn) {
            this.cn = cn;
        }

        public String getCn() {
            return StringUtils.isEmpty(this.cn) ? this.name() : this.cn;
        }

        public void setParams(PreparedStatement ps, Object obj) throws SQLException {
            switch (this) {
                case ORGANISATION_UNIT_ID:
                    OrganisationUnit ou = (OrganisationUnit) obj;
                    ps.setInt(1, ou.getKlientId().intValue());
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }
}