package pl.compan.docusafe.web.swd.management;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.sql.SwdPassedPassword;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;
import pl.compan.docusafe.web.swd.management.utils.PasswdGenerator;
import pl.compan.docusafe.web.swd.management.utils.SwdUsersFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.support.cli.IllegalCommandArgumentsException;
import pl.compan.docusafe.web.swd.management.jsp.attributes.UserAttributes;
import pl.compan.docusafe.webwork.event.*;
import pl.zbp.users.User;

import java.util.Calendar;
import java.util.Date;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class PassedPasswordAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger(PassedPasswordAction.class);
    protected String id;
    protected String password;
    protected String onDate;
    protected Boolean onDateValid;
    protected User user;
    protected UserAttributes jspAttributes = new UserAttributes("Dane u�ytkownika");

    protected JspPermissionManager.DocumentStatus jspPermissionManager = new SwdJspPermissionManager(PassedPasswordListAction.class.getName(),true);

    public static String createLink(SwdPassedPassword passedPassword, JspComponentTable.Layout parentLayout) {
        return new LinkCreator("/swd/management/passwd.action")
                .setLayout(parentLayout)
                .add("id",passedPassword.getUserId())
                .create();
    }

    public static String generatePassword() {
        PasswdGenerator rpg = new PasswdGenerator(8, 10);
        rpg.setRestriction(PasswdGenerator.CharsGroup.ALPHA, 2);
        rpg.setRestriction(PasswdGenerator.CharsGroup.ALPHA_CAPS, 2);
        rpg.setRestriction(PasswdGenerator.CharsGroup.NUM, 2);
        return rpg.generate();
    }

    public String getLinkToUser() {
        return UserViewAction.createLink(id, null);
    }

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("PassToSWD").
                append(OpenHibernateSession.INSTANCE).
                append(new PassToSWDListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("PassOnAnHour").
                append(OpenHibernateSession.INSTANCE).
                append(new PassOnAnHourListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("PassTommorowAt9").
                append(OpenHibernateSession.INSTANCE).
                append(new PassTomorrowAt9Listener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("PassOn").
                append(OpenHibernateSession.INSTANCE).
                append(new PassOnListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("CancelPassing").
                append(OpenHibernateSession.INSTANCE).
                append(new CancelPassingListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private String getBaseLink(String baseLink, String id) {
        return baseLink + "?id=" + id;
    }

    public User findUser() {
        Integer userId = parseUserId();
        return SwdUsersFactory.getUserById(userId);
    }

    public Integer parseUserId() {
        return Integer.parseInt(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserAttributes getJspAttributes() {
        return jspAttributes;
    }

    public void setJspAttributes(UserAttributes jspAttributes) {
        this.jspAttributes = jspAttributes;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public Boolean getOnDateValid() {
        return onDateValid;
    }

    public void setOnDateValid(Boolean onDateValid) {
        this.onDateValid = onDateValid;
    }

    public String getNextPassedTime() {
        Calendar now = Calendar.getInstance();

        int weekday = now.get(Calendar.DAY_OF_WEEK);
        if (weekday != Calendar.MONDAY) {
            // calculate how much to add
            // the 2 is the difference between Saturday and Monday
            int days = (Calendar.SATURDAY - weekday + 2) % 7;
            now.add(Calendar.DAY_OF_YEAR, days);
        } else {
            now.add(Calendar.DAY_OF_YEAR, 7);
        }
        Date startDay = DateUtils.startOfDay(now.getTime());
        now.setTime(startDay);

        now.add(Calendar.HOUR_OF_DAY, 9);

        return DateUtils.formatJsDateTime(now.getTime());
    }

    private class FillForm implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            user = findUser();
            if (user == null)
                throw new IllegalArgumentException("Nie odnaleziono u�ytkownika o id = " + id);

            if (StringUtils.isEmpty(password))
                password = generatePassword();

            jspAttributes.setJspData(user);
        }
    }

    private class PassToSWDListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {


            System.out.println("pass to swd"); //todo - and remove it

            // TODO - przekaza� has�o
            throw new IllegalCommandArgumentsException("TODO - PRZEKAZYWANIE DO SWD");

//            SwdPassedPassword passwd = SwdPassedPassword.findByUserId(parseUserId());
//            passwd.deleteSafely();
        }
    }

    private class PassOnAnHourListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            Calendar inCalendar = Calendar.getInstance();
            inCalendar.add(Calendar.HOUR_OF_DAY, 1);
            Date inDate = inCalendar.getTime();

            SwdPassedPassword passwd = SwdPassedPassword.findNotPassByUserId(parseUserId());
            passwd.setToPassAfter(inDate);
            passwd.updateSafely();

            event.setResult("returnToList");
        }
    }

    private class PassTomorrowAt9Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            Calendar inCalendar = Calendar.getInstance();
            inCalendar.setTime(DateUtils.startOfDay(new Date()));
            inCalendar.add(Calendar.HOUR_OF_DAY, 24 + 9);
            Date inDate = inCalendar.getTime();

            SwdPassedPassword passwd = SwdPassedPassword.findNotPassByUserId(parseUserId());
            passwd.setToPassAfter(inDate);
            passwd.updateSafely();

            event.setResult("returnToList");
        }
    }

    private class PassOnListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            Date inDate = DateUtils.parseJsDateWithOptionalTime(onDate);//todo format

            if (inDate == null) {
                onDateValid = false;
                return;
            }
            onDateValid = true;

            SwdPassedPassword passwd = SwdPassedPassword.findNotPassByUserId(parseUserId());
            passwd.setToPassAfter(inDate);
            passwd.updateSafely();

            event.setResult("returnToList");
        }
    }

    private class CancelPassingListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            SwdPassedPassword passwd = SwdPassedPassword.findNotPassByUserId(parseUserId());
            passwd.deleteSafely();

            event.setResult("returnToList");
        }
    }

    public JspPermissionManager.DocumentStatus getJspPermissionManager() {
        return jspPermissionManager;
    }

    public void setJspPermissionManager(JspPermissionManager.DocumentStatus jspPermissionManager) {
        this.jspPermissionManager = jspPermissionManager;
    }
}
