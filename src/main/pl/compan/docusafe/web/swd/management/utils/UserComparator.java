package pl.compan.docusafe.web.swd.management.utils;


import pl.compan.docusafe.web.swd.management.SwdUser;
import system.NotImplementedException;

import java.util.Comparator;

/**
 * Created by Damian on 19.12.13.
 */
public class UserComparator {

    public static Comparator<SwdUser> getComparator(String sortField, boolean asc){

            if(UserColumn.IMIE.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareFirstNameAsc();
                }else{
                    return new CompareFirstNameDesc();
                }
            }
            else if(UserColumn.NAZWISKO.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareLastNameAsc();
                }else{
                    return new CompareLastNameDesc();
                }
            }
            else if(UserColumn.LOGIN.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareLoginAsc();
                }else{
                    return new CompareLoginDesc();
                }
            }
            else if(UserColumn.KLIENT.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareClientNameAsc();
                }else{
                    return new CompareClientNameDesc();
                }
            }
            else if(UserColumn.USERID.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareUserIDAsc();
                }else{
                    return new CompareUserIDDesc();
                }
            }else if(UserColumn.AKT.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareAktAsc();
                }else{
                    return new CompareAktDesc();
                }
            }else if(UserColumn.ZABL.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareZablAsc();
                }else{
                    return new CompareZablDesc();
                }
            }else if(UserColumn.LOGOWANIE.proprety.equalsIgnoreCase(sortField)){
                if(asc){
                    return new CompareLastLoginAsc();
                }else{
                    return new CompareLastLoginDesc();
                }
            }
            else{
                throw new NotImplementedException("No comparator for field name: " + sortField);
            }
    }
    private static class CompareFirstNameAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getFirstName().compareTo(o2.getFirstName());
            }else{
                return -1;
            }

        }
    }

    private static class CompareFirstNameDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getFirstName().compareTo(o1.getFirstName());
            }else{
                return 1;
            }

        }
    }

    private static class CompareLastNameAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getLastName().compareTo(o2.getLastName());
            }else{
                return -1;
            }

        }
    }

    private static class CompareLastNameDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getLastName().compareTo(o1.getLastName());
            }else{
                return 1;
            }

        }
    }
    private static class CompareLoginAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getLogin().compareTo(o2.getLogin());
            }else{
                return -1;
            }

        }
    }

    private static class CompareLoginDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getLogin().compareTo(o1.getLogin());
            }else{
                return 1;
            }

        }
    }

    private static class CompareClientNameAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getClientName().compareTo(o2.getClientName());
            }else{
                return -1;
            }

        }
    }

    private static class CompareClientNameDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getClientName().compareTo(o1.getClientName());
            }else{
                return 1;
            }

        }
    }

    private static class CompareUserIDAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getUserID().compareTo(o2.getUserID());
            }else{
                return -1;
            }

        }
    }

    private static class CompareUserIDDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getUserID().compareTo(o1.getUserID());
            }else{
                return 1;
            }

        }
    }
    private static class CompareZablDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getBlocked().compareTo(o2.getBlocked());
            }else{
                return -1;
            }

        }
    }

    private static class CompareZablAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getBlocked().compareTo(o1.getBlocked());
            }else{
                return 1;
            }

        }
    }

    private static class CompareAktDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getActiveAsString().compareTo(o2.getActiveAsString());
            }else{
                return -1;
            }

        }
    }

    private static class CompareAktAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getActiveAsString().compareTo(o1.getActiveAsString());
            }else{
                return 1;
            }

        }
    }
    private static class CompareLastLoginAsc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o1.getLastLoginTime().compareTo(o2.getLastLoginTime());
            }else{
                return -1;
            }

        }
    }

    private static class CompareLastLoginDesc implements Comparator<SwdUser>{
        @Override
        public int compare(SwdUser o1, SwdUser o2) {
            if(o1 != null && o2 != null){
                return o2.getLastLoginTime().compareTo(o1.getLastLoginTime());
            }else{
                return 1;
            }

        }
    }
}
