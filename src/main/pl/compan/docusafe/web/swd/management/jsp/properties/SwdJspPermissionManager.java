package pl.compan.docusafe.web.swd.management.jsp.properties;

import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;
import pl.compan.docusafe.web.swd.management.utils.SwdProfile;

import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdJspPermissionManager extends JspPermissionManager.DocumentStatus {
    private static Logger log = LoggerFactory.getLogger(SwdJspPermissionManager.class);
    protected final String jspName;
    private final boolean adminAlwaysAll = false;//JspPermissionManager.isAdmin() && JspPermissionManager.isAddsTrue("swd.permissions.admin.always.all");
    private final boolean dbaAlwaysAll = false;//JspPermissionManager.hasProfileName("DBA");// && JspPermissionManager.isAddsTrue("swd.permissions.dba.always.all"); //        private final boolean dbaAlwaysAll = JspPermissionManager.hasRoles("DBA") && JspPermissionManager.isAddsTrue("swd.permissions.rolesNames.always.all");
    protected boolean permissionByProfileIfNoDocument = true; // todo zmieni� na false

    public SwdJspPermissionManager(String jspName, boolean cacheEnabled) {
        super(true, true);
        setCacheEnabled(cacheEnabled);
        this.jspName = jspName;
    }

    public SwdJspPermissionManager setPermissionByProfileIfNoDocument(boolean permissionByProfileIfNoDocument) {
        this.permissionByProfileIfNoDocument = permissionByProfileIfNoDocument;
        return this;
    }

    @Override
    public Boolean isFieldDisabledBody(String fieldName) {
        if (adminAlwaysAll || dbaAlwaysAll) return false;
        return super.isDisabled(JspFieldKind.FIELD, fieldName);
    }

    @Override
    public Boolean isFieldHiddenBody(String fieldName) {
        if (adminAlwaysAll || dbaAlwaysAll) return false;
        return (super.isHidden(JspFieldKind.FIELD, fieldName));
    }

    @Override
    public Boolean isComponentDisabledBody(String componentName) {
        if (adminAlwaysAll || dbaAlwaysAll) return false;
        return super.isDisabled(JspFieldKind.COMPONENT, componentName);
    }

    @Override
    public Boolean isComponentHiddenBody(String componentName) {
        if (adminAlwaysAll || dbaAlwaysAll) return false;
        return super.isHidden(JspFieldKind.COMPONENT, componentName);
    }

    @Override
    protected JspStatusFieldsPropertiesSet getFieldsProperties() {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsUserIfNeed();
            Document document = getDocument();

            if (document == null && permissionByProfileIfNoDocument) {
                JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();
                Set<SwdProfile> profiles = SwdProfile.getUserProfiles();
                for (SwdProfile prof : profiles)
                    properties.setFieldModificatorsByPermissions(prof.permissions);
                return properties;
            }

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }

        return super.getFieldsProperties();
    }

    @Override
    protected JspStatusFieldsPropertiesSet getFieldsProperties(DocumentLogic logic, String status) {
        if (logic instanceof FieldPropertiesGetter)
            return ((FieldPropertiesGetter) logic).getFieldsProperties(status, jspName);
        return null;
    }

    public interface FieldPropertiesGetter {
        JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName);
        JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName, String dictCn);
    }

}