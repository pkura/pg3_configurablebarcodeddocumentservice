package pl.compan.docusafe.web.swd.management.sql;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.utils.ReportDataType;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportSelectColumn implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(ReportSelectColumn.class);
    private String columnName;
    private Integer idReport;
    private String type;
    private String title;

    public static List<ReportSelectColumn> search(int reportId) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(ReportSelectColumn.class);

            criteria.add(Restrictions.eq("idReport", reportId));

            return criteria.list();
//            int totalCount = results.size();
//            return new SearchResultsAdapter<ReportSelectColumn>(results, 0, totalCount, ReportSelectColumn.class);
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public String getColumnName() {
        return columnName;
    }

    public String getColumnCode() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getIdReport() {
        return idReport;
    }

    public void setIdReport(Integer idReport) {
        this.idReport = idReport;
    }

    public String getType() {
        return type;
    }
    public ReportDataType getTypeEnum() {
        return ReportDataType.valueOf(getType());
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
