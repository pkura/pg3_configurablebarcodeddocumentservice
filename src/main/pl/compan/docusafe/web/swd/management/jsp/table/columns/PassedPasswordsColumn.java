package pl.compan.docusafe.web.swd.management.jsp.table.columns;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.sql.SwdPassedPassword;
import pl.compan.docusafe.web.swd.management.utils.SwdUsersFactory;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;

import javax.validation.UnexpectedTypeException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum PassedPasswordsColumn {
    USER_ID("Id u�ytkownika", SwdPassedPassword.HibernateColumn.USER_ID),
    LAST_LOGIN_ATTEMPT("Ostatnia pr�ba logowania", SwdPassedPassword.HibernateColumn.LAST_PASS_ATTEMPT),
    TO_PASS_AFTER("Przekaza� po", SwdPassedPassword.HibernateColumn.TO_PASS_AFTER),
    ORGANISATION_UNIT_NAME("Klient"),
    PHONE("Telefon"),
    FIRST_NAME("Imi�"),
    LAST_NAME("Nazwisko");

    public final String title;
    public final Integer width;
    public final boolean sortEnabled;
    public final SwdPassedPassword.HibernateColumn hibernateColumn;

    PassedPasswordsColumn(String title) {
        this(title, JspTableColumn.DEFAULT_SMALL_WIDTH, true, null);
    }

    PassedPasswordsColumn(String title, SwdPassedPassword.HibernateColumn hibernateColumn) {
        this(title, JspTableColumn.DEFAULT_SMALL_WIDTH, true, hibernateColumn);
    }

    PassedPasswordsColumn(String title, Integer width, boolean sortEnabled, SwdPassedPassword.HibernateColumn hibernateColumn) {
        this.title = title;
        this.width = width;
        this.sortEnabled = sortEnabled;
        this.hibernateColumn = hibernateColumn;
    }


    public Object getData(SwdPassedPassword passedPassword) {
        switch (this) {
            case USER_ID:
                return passedPassword.getUserId();
            case LAST_LOGIN_ATTEMPT:
                return passedPassword.getLastPassAttempt();
            case TO_PASS_AFTER:
                return passedPassword.getToPassAfter();
            case ORGANISATION_UNIT_NAME:{
                OrganisationUnit oUnit = passedPassword.getOrganisationUnit(passedPassword.getUserId());
                return oUnit == null ? null : oUnit.getName();
            }
            case PHONE:{
//                OrganisationUnit oUnit = SwdOrganisationUnitFactory.getOrganisationUnitsById(passedPassword.getOrganisationUnitId());
//                return oUnit.getPhone();
                User user = SwdUsersFactory.getUserById(passedPassword.getUserId());
                return user == null ? null : user.getPhone();
            }
            case FIRST_NAME:{
                User user = SwdUsersFactory.getUserById(passedPassword.getUserId());
                return user == null ? null :user.getFirstName();
            }
            case LAST_NAME:{
                User user = SwdUsersFactory.getUserById(passedPassword.getUserId());
                return user == null ? null :user.getLastName();
            }
            default:
                throw new UnexpectedTypeException("Brak metody do pobrania danych dla UsersSimpleColumn: " + this.name());
        }
    }

    public String getTitle() {
        return title;
    }

    public Integer getWidth() {
        return width;
    }

    public boolean isSortEnabled() {
        return sortEnabled;
    }

    public static PassedPasswordsColumn findByName(String orderBy) {
        if (StringUtils.isNotEmpty(orderBy))
            return PassedPasswordsColumn.valueOf(orderBy);
        return null;
    }
}
