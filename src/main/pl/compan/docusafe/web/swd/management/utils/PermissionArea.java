package pl.compan.docusafe.web.swd.management.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Damian on 21.01.14.
 */
public enum PermissionArea {
    USER_SAVE(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE),

    USER_PROFILE(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),

    USER_ACCESS_DATA(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),

    USER_ROLES(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),

    USER_RIGHTS(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),

    USER_CERTS(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,Boolean.TRUE),

    USER_MAIL(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE,Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),

    USER_DOCUMNETS(Boolean.TRUE,Boolean.TRUE,Boolean.FALSE);

    //Permission for profiles
    private Boolean permissionDba;
    private Boolean permissionOperator;
    private Boolean permissionKsiegowy;
    private Boolean visibleDba;
    private Boolean visibleOperator;
    private Boolean visibleKsiegowy;

    PermissionArea(Boolean permissionDba, Boolean permissionOperator, Boolean permissionKsiegowy) {
        this.permissionDba = permissionDba;
        this.permissionOperator = permissionOperator;
        this.permissionKsiegowy = permissionKsiegowy;
    }

    PermissionArea(Boolean permissionDba, Boolean permissionOperator, Boolean permissionKsiegowy, Boolean visibleDba, Boolean visibleOperator, Boolean visibleKsiegowy) {
        this.permissionDba = permissionDba;
        this.permissionOperator = permissionOperator;
        this.permissionKsiegowy = permissionKsiegowy;
        this.visibleDba = visibleDba;
        this.visibleOperator = visibleOperator;
        this.visibleKsiegowy = visibleKsiegowy;
    }

    /**
     * Returns PermissionArea with given name(case insensitive)
     * @param name of PermissionArea witch should be returned (case doesn't matter)
     * @return Returns PermissionArea with given name(case insensitive)
     * @throws IllegalArgumentException if there isn't any PermissionArea with given name
     */
    public static PermissionArea getPermissionAreaForName(String name){
        for(PermissionArea area : PermissionArea.values()){
            if(area.name().equalsIgnoreCase(name)){
                return area;
            }
        }
        throw new IllegalArgumentException("Brak PermissionArea o nazwie: " + name);
    }




    //___________GETTERS_____________
    public Boolean getPermissionDba() {
        return permissionDba;
    }

    public Boolean getPermissionOperator() {
        return permissionOperator;
    }

    public Boolean getPermissionKsiegowy() {
        return permissionKsiegowy;
    }

    public Boolean getVisibleDba() {
        return visibleDba;
    }

    public Boolean getVisibleOperator() {
        return visibleOperator;
    }

    public Boolean getVisibleKsiegowy() {
        return visibleKsiegowy;
    }
}
