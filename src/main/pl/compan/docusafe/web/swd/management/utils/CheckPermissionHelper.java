package pl.compan.docusafe.web.swd.management.utils;

import com.google.common.collect.Sets;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.jsp.component.permission.JspFieldProperties;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by Damian on 21.01.14.
 */
public class CheckPermissionHelper {
    private static final Logger log = LoggerFactory.getLogger("SWD: " + CheckPermissionHelper.class.getName());

    private static final String ADMIN = "Administrator";
    private static final String DBA = "DBA";
    private static final String OPERATOR = "Operator";
    private static final String KSIEGOWY= "Ksi�gowy";

    /**
     * Check if current user has one of SWD profiles and based on those profiles returns his permission to PermissionArea
     * @param area instance of PermissionArea
     * @return
     */
    public static Boolean hasPermissionCurrentProfile(PermissionArea area){
        List<String> profiles = getCurrentProfiles();

        if(profiles == null || profiles.isEmpty()){
            return Boolean.FALSE;
        }


        if(profiles.contains(ADMIN)){
            return Boolean.TRUE;
        }else if(profiles.contains(DBA)){
            return area.getPermissionDba();
        }else if(profiles.contains(OPERATOR)){
            return area.getPermissionOperator();
        }else if(profiles.contains(KSIEGOWY)){
            return area.getPermissionKsiegowy();
        }else{
            return Boolean.FALSE;
        }
    }

    /**
     * Check if current user has one of SWD profiles and based on those profiles returns if area is visible
     * @param area instance of PermissionArea
     * @return
     */
    public static Boolean isVisibleForCurrentProfile(PermissionArea area){
        List<String> profiles = getCurrentProfiles();

        if(profiles == null || profiles.isEmpty()){
            return Boolean.FALSE;
        }


        if(profiles.contains(ADMIN)){
            return Boolean.TRUE;
        }else if(profiles.contains(DBA)){
            return area.getVisibleDba();
        }else if(profiles.contains(OPERATOR)){
            return area.getVisibleOperator();
        }else if(profiles.contains(KSIEGOWY)){
            return area.getVisibleKsiegowy();
        }else{
            return Boolean.FALSE;
        }
    }

    /**
     * Returns current user profiles names list
     * @return
     */
    public static List<String> getCurrentProfiles(){
        List<String> profiles = null;
        try{
            if(DSApi.isContextOpen()){
                DSApi._close();
            }
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            DSUser currentUser = DSApi.context().getDSUser();
            profiles = Arrays.asList(currentUser.getProfileNames());
        }catch (EdmException e){
            log.error(e.getMessage(),e);
        }finally {
            if (DSApi.isContextOpen()){
                DSApi._close();
            }
        }
        return profiles;
    }



    /**
     * Check permission based on user profile and permision to field group from document logic
     * @param fieldGroup
     * @param docId
     * @return
     */
    public static Boolean hasPermissionByFieldGroup(String fieldGroup, Long docId,String jspName, String dictCn) {
        PermissionArea area = null;
        if(fieldGroup == null || jspName == null){
            return Boolean.TRUE;
        }else{
            area = PermissionArea.getPermissionAreaForName(fieldGroup);
        }

        if(hasPermissionToAreaByDocId(area, docId, jspName, dictCn) && hasPermissionCurrentProfile(area) ){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }



    /**
     * Returns permision to field group based on document logic
     *
     * @param area
     * @param docId
     * @return True if there is permision to field group based on document logic or documentId == null
     */
    private static Boolean hasPermissionToAreaByDocId(PermissionArea area, Long docId, String jspName, String dictCn) {
        if(docId == null){
            return Boolean.TRUE;
        }
        else{
            JspFieldProperties props = getAreaProperties(docId,area,jspName, dictCn);
            if(props != null){
                Set<String> permissions = Sets.newHashSet(props.getRequiredEnabledPermissionAsArray());
                if(permissions != null){
                    return permissions.contains(SwdPermission.PermissionType.EDIT.name());
                }
            }
            return Boolean.FALSE;
        }
    }

    /**
     * <h2>___NOT IMPLEMENTED YET____</h2><br/>
     * Returns permision to field  based on document logic
     *
     * @param field
     * @param docId
     * @param jspName
     * @return
     */
    public static Boolean hasPermissionToFieldByDocId(String field, Long docId, String jspName) {
        if(field == null || docId == null){
            return Boolean.TRUE;
        }
        else{
            return Boolean.TRUE;
        }
    }

    /**
     * Check visibility based on permission of user profile and visibility props from document logic
     *
     * @param fieldGroup
     * @param docId
     * @param jspName
     * @return
     */
    public static Boolean isVisibleFieldGroup(String fieldGroup, Long docId, String jspName, String dictCn) {
        PermissionArea area = null;
        if(fieldGroup == null || jspName == null){
            return Boolean.TRUE;
        }else{
            area = PermissionArea.getPermissionAreaForName(fieldGroup);
        }

        if(isVisibleAreaByDocId(area, docId, jspName, dictCn) && isVisibleForCurrentProfile(area)){
            return Boolean.TRUE;
        }else {
            return Boolean.FALSE;
        }
    }

    /**
     * Check visibility of given PermissionArea based on document logic
     *
     *
     * @param area
     * @param docId
     * @param jspName
     * @return If no document is given returns True,<br/>
     * otherwise returns True only if there is permission to READ
     */
    private static Boolean isVisibleAreaByDocId(PermissionArea area, Long docId, String jspName, String dictCn) {
        if(docId == null){
            return Boolean.TRUE;
        }
        else{
            JspFieldProperties props = getAreaProperties(docId,area,jspName, dictCn);
            if(props != null){
                Set<String> permissions = Sets.newHashSet(props.getRequiredReadPermissionAsArray());
                if(permissions != null){
                    return permissions.contains(SwdPermission.PermissionType.READ.name());
                }
            }
            return Boolean.FALSE;
        }
    }

    /**
     *  <h2>___NOT IMPLEMENTED YET____</h2><br/>
     * Returns visibility property from document
     * @param field
     * @param docId
     * @return True if any of parameters is null otherwise it check for visibility properties from document
     */
    public static Boolean isVisibleFieldByDocId(String field, Long docId, String jspName) {
        if(field == null || docId == null || jspName == null){
            return Boolean.TRUE;
        }
        else{
            return Boolean.TRUE;
        }
    }

    //_______________________METODY ADAPTUJ�CE DO WYKORZYSTANIA JspFieldProperties___________

    /**
     * Returns PermissionArea properties from document for given field
     * @param docId
     * @param area
     * @param jspName
     * @return
     */
    private static JspFieldProperties getAreaProperties(Long docId, PermissionArea area ,String jspName, String dictCn){
        JspStatusFieldsPropertiesSet fieldsPropertiesSet = getFieldPropertiesSet(docId,jspName,dictCn);
        return fieldsPropertiesSet.getFieldProperties(JspPermissionManager.JspFieldKind.COMPONENT, area.name());
    }

    /**
     * Returns field properties from document for given field
     * @param docId
     * @param fieldName
     * @param jspName
     * @return
     */
    private static JspFieldProperties getFieldProperties(Long docId, String fieldName ,String jspName, String dictCn){
        JspStatusFieldsPropertiesSet fieldsPropertiesSet = getFieldPropertiesSet(docId,jspName, dictCn);
        return fieldsPropertiesSet.getFieldProperties(JspPermissionManager.JspFieldKind.FIELD, fieldName);
    }

    /**
     * Returns JspStatusFieldsPropertiesSet from document of given id
     * @param docId
     * @param jspName
     * @return JspStatusFieldsPropertiesSet or null if there is no FieldsPropertiesSet
     */
    private static JspStatusFieldsPropertiesSet getFieldPropertiesSet(Long docId, String jspName, String dictCn){
        JspStatusFieldsPropertiesSet propertiesSet = null;
        boolean wasOpened = true;
        try {
            wasOpened = DSApi.isContextOpen();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return propertiesSet;
        }
        try{
            if (!wasOpened)
                DSApi.openAdmin();
            Document document = Document.find(docId);
            SwdJspPermissionManager.FieldPropertiesGetter fieldPropertiesGetter = getFieldPropertiesGetter(document);
            String status = getDocumentStatusCn(document);
            if(fieldPropertiesGetter != null){
                if(dictCn != null){
                    propertiesSet = fieldPropertiesGetter.getFieldsProperties(status,jspName,dictCn);
                }else{
                    propertiesSet = fieldPropertiesGetter.getFieldsProperties(status,jspName);
                }
            }
        }catch (EdmException e){
            log.error(e.getMessage(),e);
        }finally {
        if (DSApi.isContextOpen() && !wasOpened)
            DSApi._close();
        }
        return propertiesSet;
    }


    /**
     * Finds logic from document
     * and returns this logic if it is instance of SwdJspPermissionManager.FieldPropertiesGetter
     * @param document
     * @return SwdJspPermissionManager.FieldPropertiesGetter
     * or null if document logic doesn't implement SwdJspPermissionManager.FieldPropertiesGetter interface
     */
    private static SwdJspPermissionManager.FieldPropertiesGetter getFieldPropertiesGetter(Document document){
        DocumentLogic logic =  document.getDocumentKind().logic();


        if(logic instanceof SwdJspPermissionManager.FieldPropertiesGetter){
            return (SwdJspPermissionManager.FieldPropertiesGetter) logic;
        }else {
            return null;
        }
    }


    /**
     * Returns StatusCn from given document
     * @param document
     * @return Returns StatusCn from given document
     * @throws EdmException
     */
    public static String getDocumentStatusCn(Document document) throws EdmException {
        return document.getFieldsManager().getEnumItemCn("STATUS");
    }

    public static String getJspNameFromClass(Class actionClass) {
        String name = null;
        try{
            name = SwdPermission.JspType.getJspType(actionClass.getName()).name();
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        return name;
    }


}


