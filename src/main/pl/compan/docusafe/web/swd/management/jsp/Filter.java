package pl.compan.docusafe.web.swd.management.jsp;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class Filter<T> {

    public static <T> List<T> filter(List<Filter> filters, List<T> fieldsValues) {
        if (filters == null)
            return fieldsValues;

        List<T> filteredValues = new ArrayList<T>();
        for (T value : fieldsValues)
            if (Filter.filter(filters, value))
                filteredValues.add(value);
        return filteredValues;
    }

    public static <T> boolean filter(List<Filter> filters, T fieldValue) {
        for (Filter f : filters)
            if (!f.filter(fieldValue))
                return false;
        return true;
    }

    protected Object filter;

    public Filter(Object filter) {
        this.filter = filter;
    }

    public final boolean isActive() {
        return filter != null;
    }

    private final boolean check(Object value) {
        if (!isActive())
            return true;
        if (value == null)
            return checkIfNullValue();
        return checkFieldValue(value);
    }

    protected boolean checkIfNullValue (){
        return false;
    }

    protected boolean checkFieldValue(Object value) {
//        if (filter instanceof String || value instanceof String || filter instanceof Number || value instanceof Number)
//            return filter.toString().equalsIgnoreCase(value.toString());
        return filter.equals(value);
    }

    protected abstract Object getFieldValue(T fieldValue);

    public final boolean filter(T fieldValue) {
        Object value = getFieldValue(fieldValue);
        return check(value);
    }

    public Object getFilterValue() {
        return filter;
    }


    /**
     * Sprawdza czy osobne s�owa zawarte w podanym tek�cie pasuj� do sprawdzanej warto�ci.
     * Znaczenie ma kolejno��. Nie ma znaczenia wielko�� liter.
     */
    public static abstract class Text<T> extends Filter<T> {

        private static String ANY = ".*";
        private static String BEGINLINE = "^";
        private static String ENDLINE = "$";

//        public <T> Text(Object filter) {
//            this(filter, true);
//        }

        public <T> Text(Object filter, boolean isInsideAdmissible) {
            super(getParts(filter.toString(), isInsideAdmissible));
        }

        private static Object getParts(String filter, boolean isInsideAdmissible) {
            if (StringUtils.isEmpty(filter))
                return null;

            String[] parts = (filter.toLowerCase()).split(" ");
            String inside = StringUtils.join(parts, ANY);
            return isInsideAdmissible ? Pattern.compile(ANY + inside + ANY) : Pattern.compile(BEGINLINE + inside + ENDLINE);
        }

        @Override
        protected boolean checkFieldValue(Object value) {
            String txtValue = value.toString();

            if (StringUtils.isEmpty(txtValue))
                return false;

            return ((Pattern) filter).matcher((txtValue).toLowerCase()).find();
        }
    }
}
