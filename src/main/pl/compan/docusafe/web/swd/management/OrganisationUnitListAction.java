package pl.compan.docusafe.web.swd.management;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.jsp.table.OrganisationUnitsFilterTable;
import pl.compan.docusafe.web.swd.management.jsp.table.OrganisationUnitsTable;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.CustomerColumn;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;
import pl.compan.docusafe.web.swd.management.utils.SimpleGenerateCsvActionListener;
import pl.compan.docusafe.web.swd.management.utils.SwdOrganisationUnitFactory;
import pl.compan.docusafe.webwork.event.*;
import pl.zbp.users.OrganisationUnit;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OrganisationUnitListAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger(OrganisationUnitListAction.class);
    private static final int DEFAULT_THE_MOST_COMMON_USED_CUSTOMERS_LIMIT = 16;
    private static final String LINK_BASE = "/swd/management/organisation-unit-list.action";
    public String documentId;
    public String ascending;
    public String orderBy;
    public String layout;
    private StringManager sm = GlobalPreferences.loadPropertiesFile(OrganisationUnitListAction.class.getPackage().getName(), null);
    private OrganisationUnitsTable jspComponentTable1;
    private OrganisationUnitsFilterTable jspComponentTable2;
    private OrganisationUnitsTable.ColumnSortLinkCreator columnSortLinkCreator;

    protected SwdJspPermissionManager jspPermissionManager;

    private static int getTheMostCommonCustomersLimit() {
        return Docusafe.getIntAdditionPropertyOrDefault("swd.customers.theMostCommonUsedLimit", DEFAULT_THE_MOST_COMMON_USED_CUSTOMERS_LIMIT);
    }

    protected void setup() {
        FillForm fillForm = new FillForm();
        initAll();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doFilter").
                append(OpenHibernateSession.INSTANCE).
                append(new FilterListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doExportCSV").
                append(OpenHibernateSession.INSTANCE).
                append(new FilterListener()).
                append(new GenerateCSV()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private void initAll() {
        jspComponentTable1 = new OrganisationUnitsTable("Klienci najcz�ciej odwiedzani") {
            @Override
            public boolean isOrderBy(JspTableColumn column, Boolean ascending) {
                return column.getColumnName().equalsIgnoreCase(orderBy) && ascending.toString().equalsIgnoreCase(OrganisationUnitListAction.this.ascending);
            }
        };
        jspComponentTable2 = new OrganisationUnitsFilterTable("Lista klient�w") {
            @Override
            public boolean isOrderBy(JspTableColumn column, Boolean ascending) {
                return column.getColumnName().equalsIgnoreCase(orderBy) && ascending.toString().equalsIgnoreCase(OrganisationUnitListAction.this.ascending);
            }
        };
        columnSortLinkCreator = new OrganisationUnitsTable.ColumnSortLinkCreator () {
            @Override
            public String getLink(CustomerColumn column, boolean ascending) {
                return column == null ? "" : new LinkCreator(LINK_BASE)
                        .setLayout(JspComponentTable.Layout.getLayout(getLayout()))
                        .add("orderBy", column.name())
                        .add("ascending",ascending)
                        .create();
            }
        };
        jspPermissionManager  = new SwdJspPermissionManager(OrganisationUnitListAction.class.getName(),true);
    }

    public List<OrganisationUnit> getFilterOrganisationUnits() {
        String customFilter = jspComponentTable2.getCustomFilter(); // todo doda� do wyszukiwania
        return SwdOrganisationUnitFactory.getAllCustomers(jspComponentTable2.getFilters());
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getAscending() {
        return ascending;
    }

    public void setAscending(String ascending) {
        this.ascending = ascending;
    }

    public OrganisationUnitsFilterTable getJspComponentTable2() {
        return jspComponentTable2;
    }

    public void setJspComponentTable2(OrganisationUnitsFilterTable jspComponentTable2) {
        this.jspComponentTable2 = jspComponentTable2;
    }

    public OrganisationUnitsTable getJspComponentTable1() {
        return jspComponentTable1;
    }

    public void setJspComponentTable1(OrganisationUnitsTable jspComponentTable1) {
        this.jspComponentTable1 = jspComponentTable1;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    private boolean isPopup() {
        return JspComponentTable.Layout.getLayout(layout) == JspComponentTable.Layout.POPUP;
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            List<OrganisationUnit> customers = SwdOrganisationUnitFactory.getLimitedOrganisationUnits(getTheMostCommonCustomersLimit());
            OrganisationUnitsTable.init(jspComponentTable1, columnSortLinkCreator)
                    .setJspData(customers, orderBy, ascending)
                    .setJspFilterColumnWidth(JspTableColumn.DEFAULT_SMALL_WIDTH);

            List<OrganisationUnit> filterCustomers = getFilterOrganisationUnits();
            OrganisationUnitsFilterTable.init(jspComponentTable2, true, columnSortLinkCreator)
                    .setJspData(filterCustomers, orderBy, ascending)
                    .setJspFilterColumnWidth(JspTableColumn.DEFAULT_SMALL_WIDTH);

            Long documentId = parseDocumentId();
            jspPermissionManager.setDocumentId(documentId);

            if (isPopup()) {
                jspComponentTable1.setParentLayout(JspComponentTable.Layout.POPUP);
                jspComponentTable2.setParentLayout(JspComponentTable.Layout.POPUP);
                jspComponentTable2.initChosenItemGetter();
                event.setResult("popup");
            }
        }

    }

    private Long parseDocumentId() {
        if (StringUtils.isBlank(documentId) || !Pattern.compile("\\d+").matcher(documentId).find())
            return null;
        return Long.parseLong(documentId);
    }

    private class FilterListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            jspComponentTable2.updateFilters();
        }
    }

    private class GenerateCSV extends SimpleGenerateCsvActionListener<OrganisationUnit> {

        @Override
        public String getTitle() {
            StringBuilder title = new StringBuilder("Klienci");

            title.append(getFilterTitlePart(jspComponentTable2.getFilters()));

            return title.toString();
        }

        @Override
        public EventActionSupport getEventActionSupport() {
            return OrganisationUnitListAction.this;
        }

        @Override
        public Logger getLogger() {
            return log;
        }

        @Override
        public JspComponentTable<OrganisationUnit> getJspComponentTable() {
            List<OrganisationUnit> filterCustomers = getFilterOrganisationUnits();
            OrganisationUnitsFilterTable.init(jspComponentTable2, true, columnSortLinkCreator)
                    .setJspData(filterCustomers, orderBy, ascending);
            return jspComponentTable2;
        }
    }

    public SwdJspPermissionManager getJspPermissionManager() {
        return jspPermissionManager;
    }

    public void setJspPermissionManager(SwdJspPermissionManager jspPermissionManager) {
        this.jspPermissionManager = jspPermissionManager;
    }
    //    private class ColumnSortLinkCreator implements OrganisationUnitsTable.ColumnSortLinkCreator {
//
//        @Override
//        public String getLink(CustomerColumn column, boolean ascending) {
////            return column == null ? "" : getSortLink(column, ascending);
//            return column == null ? "" : new LinkCreator(LINK_BASE)
//                    .setLayout(JspComponentTable.Layout.getLayout(getLayout()))
//                    .add("orderBy", column.name())
//                    .add("ascending",ascending)
//                    .create();
//        }

//        protected String getSortLink(CustomerColumn column, boolean ascending) {
//            return getSortLink(column.name(), ascending);
//        }
//
//        protected String getSortLink(String column, boolean ascending) {
//
//            return getLinkBase() +
//                    "?" + (isPopup() ? "pop=true&" : "") + "orderBy=" + column +
//                    "&ascending=" + Boolean.toString(ascending);/* +
//                    "&withBackup=" + withBackup +
//                    "&filterBy=" + filterBy +
//                    "&orderTab=" + orderTab +
//                    "&filterName=" + filterName;*/
//        }
//
//    }
}
