package pl.compan.docusafe.web.swd.management.sql;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.zbp.users.OrganisationUnit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdOrganisationUnitAccess {

    private List<Map.Entry<String, Boolean>> accessMethods = new ArrayList<Map.Entry<String, Boolean>>();

    public SwdOrganisationUnitAccess() {
        this(searchOrganisationUnitAccess());
    }

    public SwdOrganisationUnitAccess(OrganisationUnit organisationUnit) {
        this(searchOrganisationUnitAccess(organisationUnit.getKlientId()));
    }

    public static List<Map.Entry<String, Boolean>> searchOrganisationUnitAccess(Integer customerId) {
        List<Map.Entry<String, Boolean>> organisationUnitAccess = new ArrayList<Map.Entry<String, Boolean>>();
        DSContextOpener opener = null;
        PreparedStatement ps = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();

            ps = DSApi.context().prepareStatement(" SELECT up.ACCESS_TYPE, case when oa.ORGANISATION_UNIT_ID is null then 0 else 1 end " +
                    " FROM ZBPAT_UPRAWNIENIE up" +
                    " LEFT JOIN ZBPAT_SWD_ORGANISATION_UNIT_ACCESS oa on up.ACCESS_TYPE = oa.ACCESS_TYPE and oa.ORGANISATION_UNIT_ID = ? " +
                    " GROUP BY up.ACCESS_TYPE, oa.ORGANISATION_UNIT_ID ");
            ps.setInt(1, customerId);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String method = rs.getString(1);
                boolean access = rs.getInt(2) > 0;
                if (StringUtils.isNotEmpty(method))
                    organisationUnitAccess.add(new AbstractMap.SimpleEntry<String, Boolean>(method, access));
            }

        } catch (EdmException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (opener != null)
                opener.closeIfNeed();
        }
        return organisationUnitAccess;
    }

    public static List<Map.Entry<String, Boolean>> searchOrganisationUnitAccess() {
        List<String> methods = searchAccessMethods();

        List<Map.Entry<String, Boolean>> accessMethods = new ArrayList<Map.Entry<String, Boolean>>();
        for (String m : methods)
            accessMethods.add(new AbstractMap.SimpleEntry<String, Boolean>(m, false));
        return accessMethods;
    }

    public static List<String> searchAccessMethods() {
        List<String> accessMethods = new ArrayList<String>();
        DSContextOpener opener = null;
        PreparedStatement ps = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();

            ps = DSApi.context().prepareStatement(" SELECT DISTINCT ACCESS_TYPE FROM ZBPAT_UPRAWNIENIE ");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String method = rs.getString(1);
                if (StringUtils.isNotEmpty(method))
                    accessMethods.add(method);
            }

        } catch (EdmException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (opener != null)
                opener.closeIfNeed();
        }
        return accessMethods;
    }

    private SwdOrganisationUnitAccess(List<Map.Entry<String, Boolean>> accessMethods) {
        this.accessMethods = accessMethods;
    }

    public List<Map.Entry<String, Boolean>> getAccessMethods() {
        return accessMethods;
    }

    public void setAccessMethods(List<Map.Entry<String, Boolean>> accessMethods) {
        this.accessMethods = accessMethods;
    }
}
