package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.swd.management.jsp.Filter;
import pl.zbp.users.OrganisationUnit;

import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdOrganisationUnitFactory {

    private static final Logger log = LoggerFactory.getLogger(SwdOrganisationUnitFactory.class);


    public static JspAttribute.SelectField getAllKindsFieldSelect(String defaultItemName) {
        return new JspAttribute.SelectField(
                JspAttribute.SelectField.addDefaultItem(defaultItemName, SwdOrganisationUnitFactory.getAllKinds()))
                .setDefaultSelectedId();
    }

    public static List<OrganisationUnit> getAllCustomers() {

        List<OrganisationUnit> allUnits = new ArrayList<OrganisationUnit>();
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            allUnits.addAll(factory.getOUList(null));

        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return allUnits;
    }

    public static List<JspAttribute.SelectItem> getAllKinds() {
        Set<String> allKinds = new HashSet<String>();
        List<JspAttribute.SelectItem> selectItems = new ArrayList<JspAttribute.SelectItem>();
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            Collection<OrganisationUnit> allUnits = factory.getOUList(null);
            for (OrganisationUnit unit : allUnits)
                if (allKinds.add(unit.getKind()))
                    selectItems.add(new JspAttribute.SelectItem(unit.getKind(), unit.getKind()));

        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return selectItems;
    }

    public static List<OrganisationUnit> getAllCustomers(List<Filter> filters) {
        List<OrganisationUnit> allUnits = getAllCustomers();
        allUnits = Filter.filter(filters, allUnits);
        return allUnits;
    }

    public static OrganisationUnit getOrganisationUnitsById(Integer id) {

        OrganisationUnit organisationUnit = null;
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            organisationUnit  = factory.getOrganisationUnit(id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return organisationUnit;
    }

    public static List<OrganisationUnit> getOrganisationUnitsByIdList(List<Integer> customersIds) {

        List<OrganisationUnit> allUnits = new ArrayList<OrganisationUnit>();
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            for (Integer id : customersIds) {
                OrganisationUnit organisationUnit = factory.getOrganisationUnit(id);
//                Collection<OrganisationUnit> ouList = factory.getOUList(organisationUnit);
//                allUnits.addAll(ouList);
                allUnits.add(organisationUnit);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return allUnits;
    }

    public static List<OrganisationUnit> getLimitedOrganisationUnits(int limit) {
        List<Integer> topCustomers = SwdCustomersStats.getTopCustomers(limit);
        return getOrganisationUnitsByIdList(topCustomers);
    }
}
