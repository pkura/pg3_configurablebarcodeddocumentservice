package pl.compan.docusafe.web.swd.management.jsp.table;

import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.PassedPasswordAction;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.PassedPasswordsColumn;
import pl.compan.docusafe.web.swd.management.sql.SwdPassedPassword;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class PassedPasswordsTable extends JspComponentTable<SwdPassedPassword> {

    public PassedPasswordsTable(String title) {
        super(title);
    }

    @Override
    public String getData(SwdPassedPassword passedPassword, JspTableColumn column) {
        PassedPasswordsColumn usersColumn = PassedPasswordsColumn.valueOf(column.getColumnName());

        Object data = usersColumn.getData(passedPassword);
        return data != null ? data.toString() : "";
    }

    @Override
    public String getLink(SwdPassedPassword passedPassword, JspTableColumn column) {
        return PassedPasswordAction.createLink(passedPassword, parentLayout);
    }

    public PassedPasswordsTable init(ColumnSortLinkCreator columnSortLinkCreator) {
        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (PassedPasswordsColumn col : PassedPasswordsColumn.values()) {
            columns.add(new JspTableColumn(
                    col.width,
                    null,
                    col.title,
                    columnSortLinkCreator.getLink(col, false),
                    columnSortLinkCreator.getLink(col, true),
                    col.name()));
        }

        setJspColumns(columns);
        return this;
    }

    public PassedPasswordsTable setJspData(String ascending, String orderBy, SwdPassedPassword.PassTime passTime) {
        PassedPasswordsColumn orderByColumn = PassedPasswordsColumn.findByName(orderBy);
        if (orderByColumn != null && orderByColumn.hibernateColumn != null) {
            setJspData(SwdPassedPassword.searchToPassToList(
                    new SwdPassedPassword.Query(passTime)
                            .setAscending(Boolean.parseBoolean(ascending))
                            .setOrderBy(orderByColumn.hibernateColumn)));
        } else {
            setJspData(SwdPassedPassword.searchToPassToList(
                    new SwdPassedPassword.Query(passTime)),
                    orderByColumn,
                    ascending);
        }
        return this;
    }

    private PassedPasswordsTable setJspData(List<SwdPassedPassword> passedPasswords, final PassedPasswordsColumn orderByColumn, final String sortAscending) {
        if (orderByColumn != null) {
            boolean ascending = sortAscending == null || Boolean.parseBoolean(sortAscending);
            DataGetter dataGetter = new DataGetter<PassedPasswordsColumn, SwdPassedPassword>(orderByColumn) {
                @Override
                public String getData(SwdPassedPassword obj) {
                    Object data = column.getData(obj);
                    return data != null ? data.toString() : null;
                }
            };
            sortByColumn(passedPasswords, dataGetter, ascending);
        }
        this.jspData = passedPasswords;
        return this;
    }

    public interface ColumnSortLinkCreator {
        public String getLink(PassedPasswordsColumn column, boolean ascending);
    }
}