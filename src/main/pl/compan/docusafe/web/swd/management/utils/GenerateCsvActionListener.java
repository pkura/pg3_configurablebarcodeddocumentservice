package pl.compan.docusafe.web.swd.management.utils;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.io.File;
import java.io.IOException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class GenerateCsvActionListener implements ActionListener {

    public GenerateCsvActionListener() {
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String title = getTitle();
        title = TextUtils.normalizeString(title);
        title = title.replaceAll("[^a-zA-Z0-9.-]", "_");

        try {
            File file = generateCsvForTableData(title);
            ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2",
                    "Content-Disposition: attachment; filename=\"" + title + ".csv\"");
        } catch (IOException ioe) {
            EventActionSupport eventActionSupport = getEventActionSupport();
            if (eventActionSupport != null)
                eventActionSupport.addActionError(ioe.getMessage());
            Logger logger = getLogger();
            if (logger != null)
                logger.debug(ioe.getMessage(), ioe);
        }
    }

    protected abstract File generateCsvForTableData(String title) throws IOException;
//
//    public File generateCsvForTableData(String title) throws IOException {
//        File file = File.createTempFile(title, ".csv");
//        CsvDumper dumper = new CsvDumper();
//        dumper.setFieldSeparator(",");
//        dumper.openFile(file);
//
//        JspComponentTable<T> table = getJspComponentTable();
//
//        dumper.newLine();
//
//        for (JspTableColumn c : table.getJspColumns()) {
//            dumper.addText(c.getTitle());
//        }
//
//        dumper.dumpLine();
//
//        for (T u : table.getJspData()) {
//
//            dumper.newLine();
//
//            for (JspTableColumn c : table.getJspColumns())
//                dumper.addText(table.getData(u, c));
//            dumper.dumpLine();
//        }
//
//        dumper.closeFile();
//
//        return file;
//    }

    public abstract String getTitle();

    public abstract EventActionSupport getEventActionSupport();

    public abstract Logger getLogger();
}