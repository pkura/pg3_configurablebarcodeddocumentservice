package pl.compan.docusafe.web.swd.management.jsp.table;

import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.OrganisationUnitAction;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.CustomerColumn;
import pl.compan.docusafe.web.swd.management.utils.AlphanumComparator;
import pl.zbp.users.OrganisationUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OrganisationUnitsTable extends JspComponentTable<OrganisationUnit> {

    public OrganisationUnitsTable(String title) {
        super(title);
    }

    public Object getDataItemId(OrganisationUnit item) {
        return item==null ? null : item.getKlientId();
    }

    @Override
    public String getData(OrganisationUnit customer, JspTableColumn column) {
        return getTableData(jspData, customer, column);
    }

    @Override
    public String getLink(OrganisationUnit customer, JspTableColumn column) {
        if (customer != null && customer instanceof OrganisationUnit) {
            return OrganisationUnitAction.createLink(customer, getParentLayout());
        }
        throw new IllegalArgumentException();
    }

    public String getTableData(List<?> tableData, OrganisationUnit customer, JspTableColumn column) {
//        if (customer instanceof OrganisationUnit /*&& StringUtils.isNotEmpty(column)*/) {
            CustomerColumn customerColumn = CustomerColumn.valueOf(column.getColumnName());//findByTitle(column);
            switch (customerColumn) {
                case LP:
                    return Integer.toString(tableData.indexOf(customer) + 1);
                default:
                    Object data = customerColumn.getData(customer);
                    return data != null ? data.toString() : null;
            }
//        }
//        throw new IllegalArgumentException();
    }

    public static OrganisationUnitsTable init (OrganisationUnitsTable table, ColumnSortLinkCreator columnSortLinkCreator){
        return table.initOrganisationUnitsTable(columnSortLinkCreator);
    }
    private OrganisationUnitsTable initOrganisationUnitsTable(ColumnSortLinkCreator columnSortLinkCreator) {

        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (CustomerColumn col : CustomerColumn.values())
            columns.add(new JspTableColumn(
                    col.width,
                    null,
                    col.title,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col, false) : null,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col, true) : null,
                    col.name()));

        setJspColumns(columns);
        return this;
    }

    public interface ColumnSortLinkCreator {
        public String getLink(CustomerColumn column, boolean ascending);
    }

//    protected String getSortLink(String baseLink, CustomerColumn column, boolean ascending, boolean popup) {
//        return getSortLink(baseLink, column.name(), ascending, popup);
//    }
//    protected String getSortLink(String baseLink, String column, boolean ascending, boolean popup) {
//
//        return baseLink +
//                "?" + (popup ? "pop=true&" : "") + "orderBy=" + column +
//                "&ascending=" + Boolean.toString(ascending);/* +
//                "&withBackup=" + withBackup +
//                "&filterBy=" + filterBy +
//                "&orderTab=" + orderTab +
//                "&filterName=" + filterName;*/
//    }

    public JspComponentTable setJspData(List<OrganisationUnit> data, String sortField, Object sortAscending) {

        CustomerColumn orderColumn = CustomerColumn.findByName(sortField, CustomerColumn.NAZWA);
        boolean ascending = sortAscending == null ? true
                : sortAscending instanceof Boolean ? (Boolean)sortAscending
                : sortAscending instanceof String ? Boolean.parseBoolean((String)sortAscending)
                : true;
        sortCustomersByColumn(data, orderColumn, ascending);
        this.jspData = data;
        return this;
    }

    private static void sortCustomersByColumn(List<OrganisationUnit> customers, final CustomerColumn column, final boolean ascending) {
        final AlphanumComparator alphanumComparator = new AlphanumComparator();
        Collections.sort(customers,
                new Comparator<Object>() {

                    @Override
                    public int compare(Object o1, Object o2) {

                        Object o1Name = column.getData((OrganisationUnit) o1);
                        Object o2Name = column.getData((OrganisationUnit) o2);

                        if (o1Name == null && o2Name == null)
                            return 0;
                        if (o1Name == null)
                            o1Name = "";
                        if (o2Name == null)
                            o2Name = "";

                        return ascending(alphanumComparator.compare(o1Name.toString(), o2Name.toString()));
                    }

                    private int ascending(int winner) {
                        return ascending ? winner : -winner;
                    }
                }
        );
    }
}