package pl.compan.docusafe.web.swd.management.utils;

import com.opensymphony.webwork.ServletActionContext;
import pl.zbp.users.OrganisationUnit;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdCustomersStats {

    private static final String SESSION_STATS = "SWD_CUSTOMERS_STATS_SESSION";

    public static void notifyUse(OrganisationUnit organisationUnit) {
        notifyUse(organisationUnit.getKlientId());
    }

    public static void notifyUse(Integer organisationUnit) {
        try {
            HttpSession session = getSession();
            Map<Integer, Integer> sessionStats = getSessionStats(session);
            updateStats(sessionStats, organisationUnit);
            session.setAttribute(SESSION_STATS, sessionStats);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<Integer, Integer> getSessionStats() {
        HttpSession session = getSession();
        return getSessionStats(session);
    }

    private static HttpSession getSession() {
        return ServletActionContext.getRequest().getSession();
        //return WebContextFactory.get().getSession();
    }

    private static Map<Integer, Integer> getSessionStats(HttpSession session) {
        if (session.getAttribute(SESSION_STATS) != null) {
            return (Map<Integer, Integer>) session.getAttribute(SESSION_STATS);
        } else {
            return new HashMap<Integer, Integer>();
        }
    }

    private static void updateStats(Map<Integer, Integer> customerUseStats, Integer organisationUnit) {
        Integer useCounter = customerUseStats.get(organisationUnit);
        customerUseStats.put(organisationUnit, useCounter == null ? 1 : useCounter + 1);
    }

    public static List<Integer> getTopCustomers(int limit) {
        if (limit <= 0)
            return new ArrayList<Integer>();
        List<Integer> customers = sortCustomersByUses();
        if (customers.size() == 0 || customers.size() <= limit)
            return customers;
        return customers.subList(0, limit);
    }

    public static List<Integer> sortCustomersByUses() {
        Map<Integer, Integer> sessionStats = getSessionStats();
        List<Map.Entry<Integer,Integer>> sortedStats = new ArrayList<Map.Entry<Integer, Integer>>(sessionStats.entrySet());

        Collections.sort(sortedStats,
                new Comparator<Object>() {
                    private static final int winner1 = -1;
                    private static final int winner2 = 1;

                    @Override
                    public int compare(Object o1, Object o2) {
                        if (o1 == null && o2 == null)
                            return 0;
                        if (o1 == null)
                            return winner2;
                        if (o2 == null)
                            return winner1;
                        if (!(o1 instanceof Map.Entry) || !(o2 instanceof Map.Entry))
                            return 0;

                        Integer o1Counter = (Integer) ((Map.Entry) o1).getValue();
                        Integer o2Counter = (Integer) ((Map.Entry) o2).getValue();

                        if (o1Counter == null && o2Counter == null)
                            return 0;
                        if (o1Counter == null)
                            return winner2;
                        if (o2Counter == null)
                            return winner1;

                        return o1Counter > o2Counter ? winner1 : winner2;
                    }
                }
        );

        List<Integer> sortedCustomers = new ArrayList<Integer>();
        for (Map.Entry<Integer,Integer> customer : sortedStats)
            sortedCustomers.add(customer.getKey());
        return sortedCustomers;
    }

    public static void notifyTest() {
        List<OrganisationUnit> allCustomers = SwdOrganisationUnitFactory.getAllCustomers();
        if (allCustomers.size() == 0)
            return;

        Random random = new Random();
        int value = random.nextInt(allCustomers.size() - 1);

        OrganisationUnit customer = allCustomers.get(value);
        notifyUse(customer);
    }
}
