package pl.compan.docusafe.web.swd.management;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.sql.SwdPassedPassword;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.PassedPasswordsColumn;
import pl.compan.docusafe.web.swd.management.jsp.table.PassedPasswordsTable;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class PassedPasswordListAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger(PassedPasswordListAction.class);

    private StringManager sm = GlobalPreferences.loadPropertiesFile(OrganisationUnitListAction.class.getPackage().getName(), null);

    private PassedPasswordsTable jspImmediatePassedPasswords = new PassedPasswordsTable("Has�a do natychmiastowego przekazania"){
                @Override
                public boolean isOrderBy (JspTableColumn column, Boolean ascending){
                    return column.getColumnName().equalsIgnoreCase(itOrderBy) && ascending.toString().equalsIgnoreCase(PassedPasswordListAction.this.itAscending);
                }
            };
    private PassedPasswordsTable jspPasswords = new PassedPasswordsTable("Has�a do przekazania w przysz�o�ci"){
                @Override
                public boolean isOrderBy (JspTableColumn column, Boolean ascending){
                    return column.getColumnName().equalsIgnoreCase(orderBy) && ascending.toString().equalsIgnoreCase(PassedPasswordListAction.this.ascending);
                }
            };

    public String itAscending;
    public String itOrderBy;
    public String ascending;
    public String orderBy;

    protected JspPermissionManager.DocumentStatus jspPermissionManager = new SwdJspPermissionManager(PassedPasswordListAction.class.getName(),true);

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            jspPasswords.init(new PassedPasswordsTable.ColumnSortLinkCreator() {
                @Override
                public String getLink(PassedPasswordsColumn column, boolean ascending) {
                    String orderBy = column.name();
                    String asc = Boolean.toString(ascending);
                    return createOrderLink(asc, orderBy, PassedPasswordListAction.this.itAscending, itOrderBy);
                }
            });
            jspPasswords.setJspData(ascending, orderBy, SwdPassedPassword.PassTime.LATER);


            jspImmediatePassedPasswords.init(new PassedPasswordsTable.ColumnSortLinkCreator() {
                @Override
                public String getLink(PassedPasswordsColumn column, boolean itAscending) {
                    String orderBy = column.name();
                    String asc = Boolean.toString(itAscending);
                    return createOrderLink(PassedPasswordListAction.this.itAscending, PassedPasswordListAction.this.orderBy, asc, orderBy);
                }
            });
            jspImmediatePassedPasswords.setJspData(itAscending, itOrderBy, SwdPassedPassword.PassTime.IMMEDIATELY);
        }
    }

    private String createOrderLink(String ascending, String orderBy, String itAscending, String itOrderBy) {
        String link = "/swd/management/passwds-list.action?" +
                (StringUtils.isNotEmpty(ascending) ? "ascending=" + ascending + "&" : "") +
                (StringUtils.isNotEmpty(orderBy) ? "orderBy=" + orderBy + "&" : "") +
                (StringUtils.isNotEmpty(itAscending) ? "itAscending=" + itAscending + "&" : "") +
                (StringUtils.isNotEmpty(itOrderBy) ? "itOrderBy=" + itOrderBy + "&" : "");
        return link.substring(0, link.length() - 1);
    }


    public PassedPasswordsTable getJspImmediatePassedPasswords() {
        return jspImmediatePassedPasswords;
    }

    public PassedPasswordsTable getJspPasswords() {
        return jspPasswords;
    }

    public String getItAscending() {
        return itAscending;
    }

    public String getItOrderBy() {
        return itOrderBy;
    }

    public String getAscending() {
        return ascending;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setJspImmediatePassedPasswords(PassedPasswordsTable jspImmediatePassedPasswords) {
        this.jspImmediatePassedPasswords = jspImmediatePassedPasswords;
    }

    public void setJspPasswords(PassedPasswordsTable jspPasswords) {
        this.jspPasswords = jspPasswords;
    }

    public void setItAscending(String itAscending) {
        this.itAscending = itAscending;
    }

    public void setItOrderBy(String itOrderBy) {
        this.itOrderBy = itOrderBy;
    }

    public void setAscending(String ascending) {
        this.ascending = ascending;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public JspPermissionManager.DocumentStatus getJspPermissionManager() {
        return jspPermissionManager;
    }

    public void setJspPermissionManager(JspPermissionManager.DocumentStatus jspPermissionManager) {
        this.jspPermissionManager = jspPermissionManager;
    }
}
