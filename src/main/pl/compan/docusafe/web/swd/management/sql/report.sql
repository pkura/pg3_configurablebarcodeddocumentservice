CREATE TABLE ZBPAT_SWD_REPORT (
      ID               NUMERIC(18,0),
      TITLE            VARCHAR(255) NOT NULL,
      
      SELECT_CLAUSE    VARCHAR(255) NOT NULL,
      WHERE_CLAUSE     VARCHAR(255),
      GROUP_BY_CLAUSE  VARCHAR(255),
      
      PRIMARY KEY (ID)
  );

CREATE TABLE ZBPAT_SWD_REPORT_WHERE_COLUMN (
      ID_REPORT        NUMERIC(18,0) NOT NULL,
      ALIAS            VARCHAR(255) NOT NULL,
      TYPE             VARCHAR(50) NOT NULL,

      PRIMARY KEY (ID_REPORT, ALIAS)
  );

CREATE TABLE ZBPAT_SWD_REPORT_SELECT_COLUMN (
      ID_REPORT        NUMERIC(18,0) NOT NULL,
      COLUMN_NAME      VARCHAR(50) NOT NULL,
      TYPE             VARCHAR(50) NOT NULL,
      TITLE            VARCHAR(100),

      PRIMARY KEY (ID_REPORT, COLUMN_NAME)
  );


--insert into ZBPAT_SWD_REPORT values (1, 'Zestawienie testowe 1', 'SELECT name, firstname, lastname, lastsuccessfullogin from DS_USER','ID > 1',null);
--insert into ZBPAT_SWD_REPORT_WHERE_COLUMN values (1, 'firstname', 'TEXT');
--insert into ZBPAT_SWD_REPORT_WHERE_COLUMN values (1, 'lastname', 'TEXT');
--insert into ZBPAT_SWD_REPORT_WHERE_COLUMN values (1, 'lastsuccessfullogin', 'DATE');
--insert into ZBPAT_SWD_REPORT_SELECT_COLUMN values (1, 'name', 'TEXT','Login');
--insert into ZBPAT_SWD_REPORT_SELECT_COLUMN values (1, 'firstname', 'TEXT','Imi�');
--insert into ZBPAT_SWD_REPORT_SELECT_COLUMN values (1, 'lastname', 'TEXT','Nazwisko');
--insert into ZBPAT_SWD_REPORT_SELECT_COLUMN values (1, 'lastsuccessfullogin', 'TEXT','Ostatnie logowanie');