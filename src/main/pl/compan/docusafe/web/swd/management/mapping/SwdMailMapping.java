package pl.compan.docusafe.web.swd.management.mapping;

import org.mvel2.util.Make;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.SwdUser;
import pl.compan.docusafe.web.swd.management.sql.SwdMailParts;
import pl.zbp.users.User;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Damian on 08.01.14.
 */
public class SwdMailMapping {
    private static final Logger log = LoggerFactory.getLogger("SWD: " + SwdMailMapping.class.getName());

    private String title;
    private String body;
    private String toMail;
    private String toPersonal;
    private Map<String,String> escape;
    private List<SwdMailParts> titleFields;
    private List<SwdMailParts> msgFields;

    private Map<Integer,String> selected = new HashMap<Integer, String>();


    //__________MAIL MAPPING METHODS_____________

    public void setupByUser(UpdateUserMapping user){
        setEscape(user);
        setToMail(user.getEmail());
        setToPersonal(user.getFirstName() + " " + user.getLastName());
    }

    public Boolean IsReadyToSend() {
        if(toMail != null){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public Boolean IsEmpty() {
        if(selected.isEmpty()){
            return Boolean.TRUE;
        }else if(toMail == null && title == null){
            return Boolean.TRUE;
        }else if("".equals(toMail) && "".equals(title)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public Collection<Mailer.Attachment> getAttachments() throws EdmException, IOException {
        Collection<Mailer.Attachment> attachments = null;

        if(selected != null && !selected.isEmpty()){
            attachments = new ArrayList<Mailer.Attachment>();
            Set<Long> docIds = new HashSet<Long>();

            for(String s: selected.values()){
                if(s != null && !"".equals(s)){
                    docIds.add(Long.valueOf(s));
                }
            }

            for(Long docId: docIds){
               if(docId != null){

                   OfficeDocument doc = OfficeDocument.find(docId);
                   if(doc != null){
                       Collection<Mailer.Attachment> attas= getMailerAttachments(doc);
                       if(attas != null){
                           attachments.addAll(attas);
                       }
                   }
               }
            }
        }
        return attachments;
    }

    private String escapeString(String msg) {
        if(escape != null){
            for(Map.Entry<String,String> entry : escape.entrySet()){
                msg = msg.replaceAll(entry.getKey(), entry.getValue());
            }
        }else{
            log.debug("Mail Escape not initialized");
        }
        return msg;
    }



    //________STATIC METHODS______________


    /**
     * Returns Mailer.Attachment for given Document
     * @param doc pl.compan.docusafe.core.base.Document
     * @return collection of Mailer.Attachment for given Document
     * @throws EdmException
     * @throws IOException
     */
    public static Collection<Mailer.Attachment> getMailerAttachments(Document doc) throws EdmException, IOException
    {
        //Dodaje zalaczniki
        List<Attachment> docAtta = doc.getAttachments();
        Collection<Mailer.Attachment> attachments = null;

        if(docAtta != null && !docAtta.isEmpty())
        {
            attachments = new ArrayList<Mailer.Attachment>();

            for(Attachment att : docAtta)
            {
                if(att.isDeleted()) continue;
                AttachmentRevision ar = att.getMostRecentRevision();
                File tempAr = ar.saveToTempFile();
                if(ar.getSize() > 0)
                {
                    attachments.add(new Mailer.Attachment(ar.getOriginalFilename(), tempAr.toURI().toURL()));
                }
            }
        }

        return attachments;
    }



    //________GETTERS AND SETTERS______________

    public void setEscape(Map<String,String> nameToValue){
        escape = new HashMap<String,String>();
        for(Map.Entry<String,String> entry : nameToValue.entrySet()){
            String regex = "(?i)<esc:\\s*"+entry.getKey()+"\\s*/\\s*>";
            escape.put(regex, entry.getValue());
        }
    }

    public void setEscape(UpdateUserMapping user){
        Map<String,String> nameToValue = new HashMap<String, String>();
        nameToValue.put("LOGIN", user.getLogin());
        nameToValue.put("LAST_NAME", user.getLastName());
        nameToValue.put("FIRST_NAME", user.getFirstName());
        nameToValue.put("EMAIL", user.getEmail());
        nameToValue.put("PHONE", user.getPhone());
        setEscape(nameToValue);
    }

    public void setEscape(SwdUser user){
        setEscape(user.getUser());
    }

    public void setEscape(User user){
        Map<String,String> nameToValue = new HashMap<String, String>();
        nameToValue.put("LOGIN", user.getLogin());
        nameToValue.put("LAST_NAME", user.getLastName());
        nameToValue.put("FIRST_NAME", user.getFirstName());
        nameToValue.put("EMAIL", user.getEMail());
        nameToValue.put("PHONE", user.getPhone());
        setEscape(nameToValue);
    }



    public String getTitle() {
        return escapeString(title);

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return escapeString(body);
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getToMail() {
        return toMail;
    }

    public void setToMail(String toMail) {
        this.toMail = toMail;
    }

    public String getToPersonal() {
        return toPersonal;
    }

    public void setToPersonal(String toPersonal) {
        this.toPersonal = toPersonal;
    }

    public Map<Integer, String> getSelected() {
        return selected;
    }

    public void setSelected(Map<Integer, String> selected) {
        for(Map.Entry<Integer,String> entry:selected.entrySet()){
            this.selected.put(entry.getKey(),entry.getValue());
        }
    }

    public List<SwdMailParts> getTitleFields() {
        return titleFields;
    }

    public void setTitleFields(List<SwdMailParts> titleFields) {
        this.titleFields = titleFields;
    }

    public List<SwdMailParts> getMsgFields() {
        return msgFields;
    }

    public void setMsgFields(List<SwdMailParts> msgFields) {
        this.msgFields = msgFields;
    }
}
