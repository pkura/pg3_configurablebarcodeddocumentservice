package pl.compan.docusafe.web.swd.management;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.web.swd.management.utils.SwdUserManagementFactory;
import pl.compan.docusafe.web.swd.management.sql.SwdRole;
import pl.compan.docusafe.web.swd.management.sql.SwdSystems;
import pl.compan.docusafe.web.swd.management.utils.SwdUtils;
import pl.zbp.users.Right;
import pl.zbp.users.User;

import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by Damian on 20.12.13.
 */
public class SwdUser{

    private static final Logger log = LoggerFactory.getLogger(SwdUserManagementFactory.class);
    private User user;
    private String clientName;


    /**
     * Initialize SwdUser with given user
     * and sets additional fields from db
     * @param user
     */
    public SwdUser(User user) {
        this.user = user;
        this.clientName = SwdUtils.getClientNameForUser(this);
    }


    /**
     * Returns Map of Systems and Role in the system for current user
     * @return Map SwdSystems, SwdRole in that system
     */
    public Map<SwdSystems,SwdRole> getSystemsRolesMap(){
        Map<SwdSystems,SwdRole> systemsRoles = new HashMap<SwdSystems, SwdRole>();

/*        for(SwdUserRole ur : SwdUtils.getSwdUserRoleList(this)){
            systemsRoles.put(ur.getSystem(),ur.getRole());
        }*/

        return systemsRoles;
    }

    /**
     * Returns the latest expiration date from all user certificates
     * @return max expiration date from user certificates
     */
    public Date getMaxCertExpDate(){
        List<Date> dateList = new LinkedList<Date>();

        for(X509Certificate cert : user.getCertificates().values()){
            dateList.add(cert.getNotAfter());
        }
        Collections.sort(dateList);
        if(dateList.isEmpty()){
            return null;
        }else{
            return dateList.get(dateList.size()-1);
        }
    }

    public Integer getUserID() {
        return user.getUserID();
    }

    public String getLastName() {
        return user.getLastName();
    }

    public String getFirstName() {
        return user.getFirstName();
    }

    public String getLogin() {
        return user.getLogin();
    }

    public Boolean getActive() {
        return user.getActive();
    }

    public String getActiveAsString() {
        return user.getActiveAsString();
    }

    public Boolean getBlocked() {
        return user.isBlocked();
    }

    public Integer getKlientID() {
        return user.getKlientID();
    }

    public String getClientName() {
        return clientName;
    }

    public User getUser() {
        return user;
    }

    public Date getLastLoginTime() {
        return user.getLastLoginTime();
    }

    public Date getLastInvalidLoginTime() {
        return user.getLastInvalidLoginTime();
    }

    public String getPosition() {
        return user.getPosition();
    }

    public String getEMail() {
        return user.getEMail();
    }

    public String getPhone() {
        return user.getPhone();
    }

    public String getFax() {
        return user.getFax();
    }

    public Boolean getExpiredPassword() {
        return user.getExpiredPassword();
    }

    public Map<String, Right> getGrants() {
        return user.getGrants();
    }

    public Map<BigInteger, X509Certificate> getCertificates() {
        return user.getCertificates();
    }

    public void setKoordynator(Boolean koordynator){

    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }


    public void setFirstName(String firstName) {
        user.setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        user.setLastName(lastName);
    }

    public void setLogin(String login) {
        user.setLogin(login);
    }

    public void setActive(Boolean active) {
        user.setActive(active);
    }

    public void setBlocked(Boolean blocked) {
        user.setBlocked(blocked);
    }

    public void setLastLoginTime(Date lastLoginTime) {
        user.setLastLoginTime(lastLoginTime);
    }

    public void setLastInvalidLoginTime(Date lastInvalidLoginTime) {
        user.setLastInvalidLoginTime(lastInvalidLoginTime);
    }

    public void setKlientID(Integer klientID) {
        user.setKlientID(klientID);
    }

    public void setPosition(String position){
        user.setPosition(position);
    }

    public void setEMail(String eMail) {
        user.setEMail(eMail);
    }

    public void setPhone(String phone) {
        user.setPhone(phone);
    }

    public void setFax(String fax) {
        user.setFax(fax);
    }

    public void setExpiredPassword(Boolean expiredPassword) {
        user.setExpiredPassword(expiredPassword);
    }

    public void setGrants(Map<String, Right> grants) {
        user.setGrants(grants);
    }

    public void setCertificates(Map<BigInteger, X509Certificate> certificates) {
        user.setCertificates(certificates);
    }

}
