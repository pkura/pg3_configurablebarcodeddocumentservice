CREATE TABLE [dbo].[swd_rejestr](
	[REG_CODE] [nvarchar](16) NOT NULL,
	[NAZWA_REJ] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_SWD_REJESTR] PRIMARY KEY CLUSTERED 
(
	[REG_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'AB', N'Orzeczenia arbitra bankowego')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'AT', N'Agent Transferowy - dane wspolne')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'BIG', N'Rejestr pomocniczy dla  Biur Informacji Gospodarczej')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'BR', N'Bankowy rejestr')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'BR_MERCHANT', N'Rejestr punktow handlowych nie wywiazujacych sie ze zobowiazan wobec Centrow Kartowych')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'BRP', N'MIG Posiadacze')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'DEVELOPMENT', N'Rejestr Testowy AT')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'GIODO', N'System raportowania dla GIODO')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'LOPLG', N'Lista os??b poszukiwanych listami go?�czymi')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'MIGDZ', N'Rejestr Dokument??w Zastrze??onych')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'POJAZDY', N'Baza skradzionych pojazd??w')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'REG_CODE', N'NAZWA_REJ')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'RZS', N'Rejestr Zastawow Sadowych')
--INSERT [dbo].[swd_rejestr] ([REG_CODE], [NAZWA_REJ]) VALUES (N'ZORO', N'Zarz?�dzanie ryzykiem operacyjnym')

