package pl.compan.docusafe.web.swd.management.utils;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum ReportDataType {
    TEXT("Tekst", ReportRestrictionType.EQ,ReportRestrictionType.LIKE),
    INTEGER("Liczba ca�kowita", ReportRestrictionType.GT,ReportRestrictionType.GE,ReportRestrictionType.EQ,ReportRestrictionType.LE,ReportRestrictionType.LT),
    DATE("Data", ReportRestrictionType.GT,ReportRestrictionType.GE,ReportRestrictionType.EQ,ReportRestrictionType.LE,ReportRestrictionType.LT);

    private final String title;
    private final ReportRestrictionType [] restrictionTypes;

    ReportDataType(String title, ReportRestrictionType... restrictionTypes) {
        this.title = title;
        this.restrictionTypes = restrictionTypes;
    }


    public String getCn(){
        return this.name();
    }


    public void setRestriction(PreparedStatement ps, int idx, String value) throws SQLException {
        switch(this){
            case TEXT:
                ps.setString(idx,value);
                break;
            case INTEGER:
                ps.setInt(idx, Integer.parseInt(value));
                break;
            case DATE:
                Date sqlDate = new Date(pl.compan.docusafe.util.DateUtils.parseJsDateWithOptionalTime(value).getTime());
                ps.setDate(idx, sqlDate);
                break;
            default:
                throw new IllegalArgumentException("Nie mozna doda� warto�ci dla typu="+this.name());
        }
    }

    public Object getData(ResultSet rs, String colName) throws SQLException {
        switch(this){
            case TEXT:
                return rs.getString(colName);
            case INTEGER:
                return rs.getInt(colName);
            case DATE:
                return rs.getDate(colName);
            default:
                throw new IllegalArgumentException("Nie mozna doda� warto�ci dla typu="+this.name());
        }
    }
}
