package pl.compan.docusafe.web.swd.management.jsp.table.columns;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.zbp.users.User;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum UsersColumn {
    LP("Lp.", JspTableColumn.DEFAULT_SMALL_WIDTH, false),
    NAZWISKO("Nazwisko", JspTableColumn.DEFAULT_SMALL_WIDTH),
    IMIE("Imi�", JspTableColumn.DEFAULT_SMALL_WIDTH),
    LOGIN("Login", JspTableColumn.DEFAULT_SMALL_WIDTH),
    AKTYWNY("Akt", JspTableColumn.DEFAULT_SMALL_WIDTH),
    ZABLOKOWANY("Zabl", JspTableColumn.DEFAULT_SMALL_WIDTH),
    OST_NIEUDANE_LOGOWANIE("Ostatnie nieudane logowanie", JspTableColumn.DEFAULT_SMALL_WIDTH),
    POWOD("Pow�d", JspTableColumn.DEFAULT_SMALL_WIDTH),
    ID("Id u�ytkownika", JspTableColumn.DEFAULT_SMALL_WIDTH),
    OST_LOGOWANIE("Ostatnie logowanie", JspTableColumn.DEFAULT_SMALL_WIDTH);


    public final String title;
    public final Integer width;
    public final boolean sortEnabled;

    UsersColumn(String title, Integer width) {
        this(title, width,true);
    }

    UsersColumn(String title, Integer width, boolean sortEnabled) {
        this.title = title;
        this.width = width;
        this.sortEnabled = sortEnabled;
    }

    public Object getData(User user) {
        switch(this){
            case NAZWISKO:
                return user.getLastName();
            case IMIE:
                return user.getFirstName();
            case LOGIN:
                return user.getLogin();
            case AKTYWNY:
                return user.getActive();
            case ZABLOKOWANY:
                return user.isBlocked();
            case OST_NIEUDANE_LOGOWANIE:
                return user.getLastInvalidLoginTime();
            case POWOD:
                return "? ? ? todo ? ? ?";
            case ID:
                return user.getUserID();
            case OST_LOGOWANIE:
                return user.getLastLoginTime();

            case LP:
            default:
                throw new IllegalArgumentException("Brak metody do pobrania danych dla CustomerColumn: " + this.name());
        }
    }

    public String getTitle() {
        return title;
    }

    public Integer getWidth() {
        return width;
    }

    public boolean isSortEnabled() {
        return sortEnabled;
    }

    public static UsersColumn findByName(String name, UsersColumn defaultColumn) {
        if (StringUtils.isNotEmpty(name))
            return UsersColumn.valueOf(name);
//            for (CustomerColumn cc : CustomerColumn.values())
//                if (cc.name().equalsIgnoreCase(name))
//                    return cc;
        return defaultColumn;
    }
}
