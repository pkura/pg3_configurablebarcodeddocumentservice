package pl.compan.docusafe.web.swd.management.utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.zbp.users.User;
import pl.zbp.users.event.EventLoginFail;
import pl.zbp.users.sql.SQLUserManagementFactory;

import java.util.Collection;

/**
 * Fabryka uzytkownikow dostosowana do SWD
 *
 * @author wkutyla
 */
public class SwdUserManagementFactory extends SQLUserManagementFactory {
    private static final Logger log = LoggerFactory.getLogger(SwdUserManagementFactory.class);

    public SwdUserManagementFactory() {
        DatabaseMode = ORACLE_MODE;
    }

    @Override
    public Session currentSession() {
        try {
            return SwdSessionFactory.session();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Nie zamykamy sesji
     */
    @Override
    public void closeSession() {
        //AtContext.forceClose();
    }

    @Override
    protected void markEventLoginFail(EventLoginFail event) {
        LoggerFactory.getLogger(SwdUserManagementFactory.class).error(event.getSwdRegister());
    }

    private final static Long DEFAULT_MAX_LOGIN_FAIL = 10l;

    @Override
    public long getMaxLoginFail() {
        return 10L;
    }


    /**Uwaga: Nie otwiera i nie zamyka sesji*/
    public Collection<User> getAllUsers(){
        Criteria criteria = currentSession().createCriteria(User.class);
        return criteria.list();
    }
}