package pl.compan.docusafe.web.swd.management;

import com.opensymphony.webwork.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.parametrization.swd.SwdDocumentLogicNotifier;
import pl.compan.docusafe.web.swd.management.mapping.DocumentUserChangeMapping;
import pl.compan.docusafe.web.swd.management.utils.*;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.swd.management.mapping.SwdMailMapping;
import pl.compan.docusafe.web.swd.management.mapping.UpdateUserMapping;
import pl.compan.docusafe.web.swd.management.sql.*;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;
import pl.zbp.users.UserManagementException;
import pl.zbp.users.UserSearchConditions;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class UserViewAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger("SWD: " + UserViewAction.class.getName());

    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(UserViewAction.class.getPackage().getName(), null);


    private Boolean pop;
    private Boolean readonly = Boolean.FALSE;
    private Long documentId;
    private String dictCn;

    private Boolean isCreatedNewUser;
    private Integer userId;
    private Integer organisationUnit;
    private String title;

    private Boolean formValid = Boolean.TRUE;

    private UpdateUserMapping upUser = new UpdateUserMapping();
    private FormFile multiFiles;
    private String certToFile;
    private String fileType;
    private SwdMailMapping mailMsg = new SwdMailMapping();
    private List<SwdSystems> systemsList;
    private Set<String> groups;
    private Map<String, List<SwdUprawnienie>> uprawnienia = new HashMap<String, List<SwdUprawnienie>>();



    private List<SwdRole> roleList;



    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave")
                .append(OpenHibernateSession.INSTANCE)
                .append(new Validate())
                .append(new Update())
                .append(new sendMail())
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGenCertFile")
                .append(OpenHibernateSession.INSTANCE)
                .append(new GenerateCertFile())
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);


    }


    //__________ACTION LISTENERY______________

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            if(userId == null && upUser.getId() == null){
                title = sm.getString("newUser.h1");
                upUser.setClientId(organisationUnit);
                upUser.setClientNameById(organisationUnit);
            }else {
                title = sm.getString("KartaUzytkownika");
                SwdUser user = SwdUtils.getSwdUserById(userId);
                if(formValid){
                    upUser = new UpdateUserMapping(user);
                }
                upUser.setCertList(SwdUtils.getCertList(user));
            }
            DSContext context = DSApi.context();
            if(context != null){
                systemsList = SwdUtils.getAllSytems(context);
                uprawnienia = SwdUtils.getAllUprawnieniaMap(context);
                mailMsg.setTitleFields(SwdUtils.getAllTitleParts(context));
                mailMsg.setMsgFields(SwdUtils.getAllMsgParts(context));
            }
            groups = uprawnienia.keySet();

            if(pop != null && pop.equals(Boolean.TRUE)){
                event.setResult("popup");
            }
        }

    }

    private class Validate implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if(upUser.getId() == null && organisationUnit == null && upUser.getClientId() == null){
                addActionError("Nie stworzy� u�ytkownika nie przypisanego do klienta.");
                formValid = Boolean.FALSE;
            }

            if(!validateLogin()){
                formValid = Boolean.FALSE;

            }

            if(!validateCertificateFile()){
                formValid = Boolean.FALSE;
            }
        }

        /**
         * Adds valid X509Certificate to upUser
         * Returns true if no certificate file was loaded to multifiles or
         * if it could parse file to X509Certificate and add to upUser
         * @return
         */
        private boolean validateCertificateFile() {
            if(multiFiles != null){
                X509Certificate cert = getValidCertificateFromFile(multiFiles.getFile());
                if(cert != null){
                    upUser.addCert(cert);
                }else{
                    return false;
                }
            }
            return true;

        }

        public X509Certificate getValidCertificateFromFile(File file){
            X509Certificate cert = null;

            try {
                cert=SwdUtils.getCertificateFromFile(file);
                if(cert != null){
                    cert.checkValidity();
                }
            }catch(CertificateException ce){
                Log.error(ce.getMessage(), ce);
                String erMsg = sm.getString("BladnyPlikCertyfikatu");
                addActionError(erMsg);
            }catch(IOException ioe){
                Log.error(ioe.getMessage(), ioe);
                String erMsg = sm.getString("BladPodczasCzytaniaPliku");
                addActionError(erMsg);
            }finally {
                return cert;
            }
        }

        /**
         * Checks the login constraints
         * @return
         */
        private Boolean validateLogin(){

            if((upUser.getGrantsCodes() != null && !upUser.getGrantsCodes().isEmpty())) {
                if (!validateLoginIsNotEqUserId()) {
                    addActionError("Nie mo�na wys�a� wiadomo�ci je�li Login jest taki jak ID");
                    return Boolean.FALSE;
                }
            }
            if(!mailMsg.IsEmpty()){
                if (!validateLoginIsNotEqUserId()) {
                    addActionError("Nie mo�na wys�a� wiadomo�ci je�li Login jest taki jak ID");
                    return Boolean.FALSE;
                }
            }
            if(validateLoginUniqueness()){
                return Boolean.TRUE;
            }else{
                addActionError("Login nie jest unikalny");
                return Boolean.FALSE;
            }

        }
        private Boolean validateLoginIsNotEqUserId(){
            if(upUser.getLogin() != null && upUser.getId() != null && upUser.getLogin().equals(upUser.getId().toString())){
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        private Boolean validateLoginUniqueness(){
            SwdUserManagementFactory factory = new SwdUserManagementFactory();
            UserSearchConditions conditions = factory.getUserSearchCondition();
            conditions.setLogin(upUser.getLogin());
            Collection<User> users = null;
            try {
                users = factory.findUsers(conditions);
            } catch (Exception e) {
                log.error(e.getMessage(),e);
            } finally {
                SwdSessionFactory.closeSessionQuietly();
            }

            //because findUsets(conditions) returns 2 instances of one user
            Set<User> userSet = new HashSet<User>(users);
            if(userSet.isEmpty()){
                return true;
            }else if(userSet.size() == 1){
                for(User u: userSet){
                    if(u.getUserID().equals(upUser.getId())){
                        return true;
                    }
                }
            }
            return false;
        }
    }


    private class Update implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if(formValid){

                try {
                    if(upUser.getId() == null){
                        createUser();
                        isCreatedNewUser = Boolean.TRUE;
                    }

                    updateUserInDb();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    addActionError(e.getMessage());
                }

                if(getActionErrors().isEmpty()){
                    String msg = sm.getString("PoprawnieZapisanoZmiany");
                    addActionMessage(msg);
                }
            }

            if(upUser.getSentPass()){
                if(!sentPass(userId)){
                    addActionError("Nie dodano has�a do przekazania");
                }
            }
        }

        private void createUser() throws UserManagementException {
            Integer ouId = null;
            if(organisationUnit != null || upUser.getClientId() != null){
                ouId = (organisationUnit != null?organisationUnit:upUser.getClientId());
            }
            if( ouId != null && upUser.getLogin() != null){
                try{
                    SwdUserManagementFactory factory = new SwdUserManagementFactory();
                    OrganisationUnit ou = factory.getOrganisationUnit(ouId);
                    if(ou == null){
                        throw new UserManagementException("Nie mo�na stworzy� u�ytkownika bez przypisanego klienta");
                    }

                    Integer newId = tempGetAvailableUserId(factory);
                    User user = factory.createUser(newId,upUser.getLogin(),ou);
                    if(user == null){
                        throw new UserManagementException("B��d przy tworzeniu u�ytkownika");
                    }
                    upUser.setId(user.getUserID());
                    userId = user.getUserID();
                    upUser.setClientId(user.getKlientID());
                }finally {
                    SwdSessionFactory.closeSessionQuietly();
                }
            }else if(ouId == null){
                throw new UserManagementException("Nie mo�na stworzy� u�ytkownika bez przypisanego klienta");
            }else if(upUser.getLogin() == null){
                throw new UserManagementException("Nie mo�na stworzy� u�ytkownika bez loginu");
            }
        }

        /**
         * TO DELETE - jak b�dzie mo�liwo�� zapisywania usera bez podanego id
         * @param factory
         * @return
         */
        private Integer tempGetAvailableUserId(SwdUserManagementFactory factory){
            Criteria criteria = factory.currentSession().createCriteria(User.class);
            criteria.addOrder(Order.desc("id"));
            criteria.setMaxResults(1);
            User user = (User) criteria.uniqueResult();
            return user.getUserID() + 1;
        }

        private void updateUserInDb() throws Exception {

            SwdUserManagementFactory factory = new SwdUserManagementFactory();
            Transaction transaction = null;
            User userFromDb = null;
            try {
                transaction = factory.currentSession().getTransaction();
                if(transaction != null && !transaction.isActive()){
                    transaction.begin();
                }else{
                    throw new TransactionException("Problem z zainicjalizowaniem transakcji");
                }
                userFromDb = factory.getUser(upUser.getId());
                if(userFromDb == null){
                    throw new UserManagementException("Problem z zapisem u�ytkownika");
                }
                DocumentUserChangeMapping userChanges = null;
                if(documentId != null){
                    userChanges = new DocumentUserChangeMapping(documentId, upUser.getId());
                }

                //SET GRANTS
                if(userChanges != null){
                    userChanges.getBefore().setGrantsCodes(userFromDb.getGrants().keySet());
                    userChanges.getAfter().setGrantsCodes(upUser.getGrantsCodes());
                }
                SwdUtils.setUserGrants(factory, userFromDb,upUser.getGrantsCodes(),documentId);


                //SET CERTS
                if(userChanges != null){
                    userChanges.getBefore().setCertSerials(userFromDb.getCertificates().keySet());
                }
                SwdUtils.setUserCerts(factory,userFromDb,upUser.getCertToRem(),upUser.getCertsToAdd(),documentId);


                //SET USER DATA
                updateUserFromForm(userFromDb, upUser, userChanges);
                factory.store(userFromDb);
                if(userChanges != null){
                    userChanges.store();
                }

                if(documentId != null && dictCn != null ){
                    SwdDocumentLogicNotifier.notifyUserModified(documentId, userId.longValue(), dictCn);
                }

            }catch (Exception e){
                transaction.rollback();
                throw e;
            }
            finally {
                SwdSessionFactory.closeSessionQuietly();
            }

        }


        private void updateUserFromForm(User user, UpdateUserMapping upUser, DocumentUserChangeMapping userChanges) {
            if(user.getLogin() == null || !user.getLogin().equals(upUser.getLogin())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setLogin(user.getLogin());
                    userChanges.getAfter().setLogin(upUser.getLogin());
                }
                user.setLogin(upUser.getLogin());
            }

            if(user.getFirstName() == null || !user.getFirstName().equals(upUser.getFirstName())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setFirstName(user.getFirstName());
                    userChanges.getAfter().setFirstName(upUser.getFirstName());
                }
                user.setFirstName(upUser.getFirstName());
            }

            if(user.getLastName() == null || !user.getLastName().equals(upUser.getLastName())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setLastName(user.getLastName());
                    userChanges.getAfter().setLastName(upUser.getLastName());
                }
                user.setLastName(upUser.getLastName());
            }

            if(user.getPosition() == null || !user.getPosition().equals(upUser.getPosition())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setPosition(user.getPosition());
                    userChanges.getAfter().setPosition(upUser.getPosition());
                }
                user.setPosition(upUser.getPosition());
            }
            if(user.getEMail() == null || !user.getEMail().equals(upUser.getEmail())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setEmail(user.getEMail());
                    userChanges.getAfter().setEmail(upUser.getEmail());
                }
                user.setEMail(upUser.getEmail());
            }
            if(user.getPhone() == null || !user.getPhone().equals(upUser.getPhone())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setPhone(user.getPhone());
                    userChanges.getAfter().setPhone(upUser.getPhone());
                }
                user.setPhone(upUser.getPhone());
            }
            if(user.getFax() == null || !user.getFax().equals(upUser.getFax())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setFax(user.getFax());
                    userChanges.getAfter().setFax(upUser.getFax());
                }
                user.setFax(upUser.getFax());
            }
            if(user.getActive() == null || !user.getActive().equals(upUser.getActive())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setActive(user.getActive());
                    userChanges.getAfter().setActive(upUser.getActive());
                }
                user.setActive(upUser.getActive());
            }
            if(user.isBlocked() == null || !user.isBlocked().equals(upUser.getBlocked())){
                if(userChanges != null && userChanges.getBefore() != null && userChanges.getAfter() != null){
                    userChanges.getBefore().setBlocked(user.isBlocked());
                    userChanges.getAfter().setBlocked(upUser.getBlocked());
                }
                user.setBlocked(upUser.getBlocked());
            }

        }


    }


    private class sendMail implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {

            if(formValid && !mailMsg.IsEmpty()){
                //setup mailMsg
                mailMsg.setupByUser(upUser);

                    try {
                        if(!mailMsg.IsReadyToSend()){
                            throw new EdmException("Mail not ready to send");
                        }
                        MailerDriver mailer = (MailerDriver) ServiceManager.getService(Mailer.NAME);
                        mailer.send(mailMsg.getToMail(),mailMsg.getToPersonal(),mailMsg.getTitle(),mailMsg.getBody(), (Iterable<Mailer.Attachment>) mailMsg.getAttachments());
                    } catch (Exception se) {
                        log.error(se.getMessage());
                        String errMsg = sm.getString("BladWysylkiMaila");
                        addActionError(errMsg);
                        addActionError(se.getMessage());
                    }finally{
                        mailMsg = new SwdMailMapping();
                    }
            }


        }
    }

    private class GenerateCertFile implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            formValid = Boolean.FALSE;
            User user = SwdUtils.getUserByID(userId);
            File cerFile =null;
            try{
                BigInteger serialNumber = new BigInteger(certToFile);
                X509Certificate cert = user.getCertificates().get(serialNumber);

                String title = "Certificate_" + cert.getSerialNumber();
                String fileExtension = "." + fileType.toLowerCase(Locale.ENGLISH);

                if("CER".equalsIgnoreCase(fileType)){
                    cerFile = SwdUtils.generateCERFile(cert, title);
                } else if("DER".equalsIgnoreCase(fileType)){
                    cerFile = SwdUtils.generateDERFile(cert, title);
                } else {
                    throw new IllegalArgumentException("No such file type: "+fileType);
                }
                ServletUtils.streamFile(ServletActionContext.getResponse(), cerFile, "application/x-x509-ca-cert", "Accept-Charset: iso-8859-2",
                        "Content-Disposition: attachment; filename=\"" + title + fileExtension + "\"");
            }catch(Exception e){
                Log.error(e.getMessage() +"file type: " + fileType,e);
                String errMsg = sm.getString("BladGenerowaniaPliku") + " " + fileType;
                addActionError(errMsg);
            }finally {
                fileType = null;
                certToFile = null;
            }

        }
    }


    //__________ACTION METHODS______________

    public static boolean sentPass(Integer userId){
            try{
                if(!SwdPassedPassword.isToPassForUser(userId)){
                    SwdPassedPassword swdPassedPassword = new SwdPassedPassword();
                    swdPassedPassword.setUserId(userId);
                    swdPassedPassword.setToPassAfter(new Date());
                    swdPassedPassword.save();
                    return true;
                }
            }catch(Exception e){
                log.error(e.getMessage(),e);
                return false;
            }
            return false;
    }

    public boolean isCreatedNewUser(){
        if(isCreatedNewUser != null){
            return isCreatedNewUser;
        }else{
            return false;
        }
    }

    public String hasRoleInSystem(Object system, Object role){
/*        if(system instanceof SwdSystems && role instanceof SwdRole){
            SwdRole userRole = user.getSystemsRolesMap().get(system);
            if(role.equals(userRole)){
                return "checked";
            }
        }*/
        return null;
    }

    public String hasGeneralRole(Object role){
/*        if(role instanceof SwdRole){
            if(role.equals(user.getGeneralRole())){
                return "checked";
            }
        }*/
        return null;
    }

    public String hasRight(Object grant){
        if(grant instanceof SwdUprawnienie){
            if(upUser.hasGrant((SwdUprawnienie)grant)){
                return "checked";
            }
        }
        return null;
    }

    public String prepareName(Object mailPart){
        String name = "";
        if(mailPart instanceof  SwdMailParts){
            SwdMailParts mp = (SwdMailParts)mailPart;
            Integer id = mp.getId();
            Integer doc = mp.getAttach();

            if(id != null){
                name=id.toString()+"_";
            }
            if(doc != null){
                name += doc.toString();
            }
        }
        return name;
    }

    public Boolean isMailPartSelected(Object mailPart){
        if(mailPart instanceof  SwdMailParts){
            SwdMailParts mp = (SwdMailParts)mailPart;
            return mailMsg.getSelected().containsKey(mp.getId());
        }
        return Boolean.FALSE;
    }

    public static String createLink(Integer userId, Object pop){
        return createLink(userId != null ? userId.toString() : null, pop);
    }

    public static String createLink(String userId, Object pop) {
        return createLink(userId, pop, null);
    }

    public static String createLink(String userId, Object pop, Object readonly){
        String link = "";

        link = "/swd/management/user-view.action?userId="+userId;
        if(pop instanceof Boolean && (Boolean) pop){
            link += "&amp;pop=true";
        }

        if(readonly instanceof Boolean && (Boolean)readonly){
            link += "&amp;readonly=true";
        }

        return link;
    }

    public static String getClientLink(Object user) {
        if (user!= null && user instanceof UpdateUserMapping) {

                return OrganisationUnitAction.createLink(((UpdateUserMapping) user).getClientId());
        }
        throw new IllegalArgumentException();
    }

    public static Boolean isReadonly(String fieldGroup, String field, Long documentId, Boolean readonly, String dictCn){
        if(readonly){
            return Boolean.TRUE;
        }
        String jspName = UserViewAction.class.getName();
        if(!CheckPermissionHelper.hasPermissionByFieldGroup(fieldGroup, documentId,jspName, dictCn) || !CheckPermissionHelper.hasPermissionToFieldByDocId(field, documentId,jspName)){
            return Boolean.TRUE;
        }else {
            return Boolean.FALSE;
        }

    }

    public static Boolean isReadonly(String fieldGroup, Long documentId, Boolean readonly, String dictCn){
        return isReadonly(fieldGroup,null,documentId,readonly, dictCn);
    }
    public static Boolean hasPermission(String fieldGroup, Long documentId, Boolean readonly, String dictCn){
        return !isReadonly(fieldGroup,null,documentId,readonly, dictCn);
    }

    public static String isVisible(String fieldGroup, String field, Long documentId, String dictCn){

        String jspName = UserViewAction.class.getName();
        if(!CheckPermissionHelper.isVisibleFieldGroup(fieldGroup, documentId,jspName, dictCn) || !CheckPermissionHelper.isVisibleFieldByDocId(field, documentId,jspName)){
            return "hiddenArea";
        }else {
            return null;
        }
    }

    public static String isVisible(String fieldGroup, Long documentId, String dictCn){
        return isVisible(fieldGroup,null,documentId,dictCn);
    }


    //_________GETTERS & SETTERS________

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<SwdRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SwdRole> roleList) {
        this.roleList = roleList;
    }

    public List<SwdSystems> getSystemsList() {
        return systemsList;
    }

    public void setSystemsList(List<SwdSystems> systemsList) {
        this.systemsList = systemsList;
    }

    public UpdateUserMapping getUpUser() {
        return upUser;
    }

    public void setUpUser(UpdateUserMapping upUser) {
        this.upUser = upUser;
    }

    public FormFile getMultiFiles() {
        return multiFiles;
    }

    public void setMultiFiles(FormFile multiFiles) {
        this.multiFiles = multiFiles;
    }

    public String getCertToFile() {
        return certToFile;
    }

    public void setCertToFile(String certToFile) {
        this.certToFile = certToFile;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public SwdMailMapping getMailMsg() {
        return mailMsg;
    }

    public void setMailMsg(SwdMailMapping mailMsg) {
        this.mailMsg = mailMsg;
    }

    public Set<String> getGroups() {
        return groups;
    }

    public void setGroups(Set<String> groups) {
        this.groups = groups;
    }

    public Map<String, List<SwdUprawnienie>> getUprawnienia() {
        return uprawnienia;
    }

    public void setUprawnienia(Map<String, List<SwdUprawnienie>> uprawnienia) {
        this.uprawnienia = uprawnienia;
    }

    public List<SwdUprawnienie> getListUprawnien(String group){
        return uprawnienia.get(group);
    }

    public Boolean getPop() {
        return pop;
    }

    public void setPop(Boolean pop) {
        this.pop = pop;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }

    public Integer getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(Integer organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDictCn() {
        return dictCn;
    }

    public void setDictCn(String dictCn) {
        this.dictCn = dictCn;
    }
}
