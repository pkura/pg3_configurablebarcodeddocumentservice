package pl.compan.docusafe.web.swd.management.jsp.table;

import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.sql.ReportSelectColumn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportTable extends JspComponentTable<Map<String,Object>> {

    @Override
    public String getData(Map<String,Object> col, JspTableColumn column) {
        Object data = col.get(column.getColumnName());
        return data != null ? data.toString() : "";
    }

    public ReportTable(String title) {
        super(title);
    }

    public ReportTable init(List<ReportSelectColumn> selects) {
        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (ReportSelectColumn col : selects) {
            columns.add(new JspTableColumn(
                    null,
                    null,
                    col.getTitle(),
                    getSortLink(col, false),
                    getSortLink(col, true),
                    col.getColumnCode()));
        }

        setJspColumns(columns);
        return this;
    }

    private String getSortLink(ReportSelectColumn col, boolean ascending) {
        return "/swd/management/books-list.action" +
                "?orderBy=" + col.getColumnCode() +
                "&ascending=" + Boolean.toString(ascending);
    }

    public ReportTable setJspData(List<Map<String,Object>> data, final String orderByColumn, final String sortAscending) {
        if (orderByColumn != null){
            boolean ascending = sortAscending == null || Boolean.parseBoolean(sortAscending);
            DataGetter dataGetter = new DataGetter<String, Map<String,Object>>(orderByColumn) {
                @Override
                public String getData(Map<String,Object> obj) {
                    Object data = obj.get(column);
                    return data != null ? data.toString() : null;
                }
            };
            sortByColumn(data, dataGetter, ascending);
        }
        this.jspData = data;
        return this;
    }

}