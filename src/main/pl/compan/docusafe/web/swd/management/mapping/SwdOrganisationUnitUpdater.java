package pl.compan.docusafe.web.swd.management.mapping;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jfree.util.Log;
import pl.compan.docusafe.web.swd.management.jsp.attributes.types.OrganisationUnitAttr;
import pl.compan.docusafe.web.swd.management.utils.SwdSessionFactory;
import pl.compan.docusafe.web.swd.management.utils.SwdUserManagementFactory;
import pl.compan.docusafe.web.swd.management.utils.Validation;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.sql.SQLOrganisationUnit;

import javax.validation.UnexpectedTypeException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdOrganisationUnitUpdater {

    private Map<OrganisationUnitAttr, String> values = new HashMap<OrganisationUnitAttr, String>();

    public Map<OrganisationUnitAttr, Change> create() {
        OrganisationUnit organisationUnit = new SQLOrganisationUnit();
        organisationUnit.setKlientId(-1);
        return update(organisationUnit);
    }

    public Map<OrganisationUnitAttr, Change> update(OrganisationUnit currentOrganisationUnit) {
        if (currentOrganisationUnit == null)
            throw new IllegalArgumentException("Brak OrganisationUnit");

        Map<OrganisationUnitAttr, Change> changes = new HashMap<OrganisationUnitAttr, Change>();

        for (Map.Entry<OrganisationUnitAttr, String> entry : values.entrySet()) {
            OrganisationUnitAttr attrType = entry.getKey();
            String value = entry.getValue();
            switch (attrType) {
                case ID:
                    break;
                case NAME:
                    changes.put(attrType, new Change(currentOrganisationUnit.getName(), value));
                    currentOrganisationUnit.setName(value);
                    break;
                case UNIT_CODE:
                    changes.put(attrType, new Change(currentOrganisationUnit.getUnitCode(), value));
                    currentOrganisationUnit.setUnitCode(value);
                    break;
                case KIND:
                    changes.put(attrType, new Change(currentOrganisationUnit.getKind(), value));
                    currentOrganisationUnit.setKind(value);
                    break;
                case SHORT_NAME:
                    changes.put(attrType, new Change(currentOrganisationUnit.getShortName(), value));
                    currentOrganisationUnit.setShortName(value);
                    break;
                case STREET:
                    changes.put(attrType, new Change(currentOrganisationUnit.getStreet(), value));
                    currentOrganisationUnit.setStreet(value);
                    break;
                case ADDRESS:
                    changes.put(attrType, new Change(currentOrganisationUnit.getAddress(), value));
                    currentOrganisationUnit.setAddress(value);
                    break;
                case POSTAL_CODE:
                    changes.put(attrType, new Change(currentOrganisationUnit.getPostalCode(), value));
                    currentOrganisationUnit.setPostalCode(value);
                    break;
                case CONTACT_PERSON:
                    changes.put(attrType, new Change(currentOrganisationUnit.getContactPerson(), value));
                    currentOrganisationUnit.setContactPerson(value);
                    break;
                case EMAIL:
                    changes.put(attrType, new Change(currentOrganisationUnit.getEmail(), value));
                    currentOrganisationUnit.setEmail(value);
                    break;
                case NOTES:
                    changes.put(attrType, new Change(currentOrganisationUnit.getNotes(), value));
                    currentOrganisationUnit.setNotes(value);
                    break;
                default:
                    throw new UnexpectedTypeException("Brak rozpoatrywanego typu OrganisationUnitAttr");
            }
        }

        save(currentOrganisationUnit);

        return changes;
    }

    public String setValue(OrganisationUnitAttr attr, String sValue) {
        String errorMsg = validateValue(attr, sValue);
        //Object value = getRightObject(attr, sValue);
        //values.put(attr, value);
        values.put(attr, sValue);
        return errorMsg;
    }

    /**
     * @return error msg
     */
    public String validateValue(OrganisationUnitAttr attr, String value) {
        switch (attr) {
            case UNIT_CODE:
                if (!Validation.validNumber(value, 1, 8))
                    return "Nieprawidłowy kod jednostki: " + value;
                break;
            case POSTAL_CODE:
                if (!Validation.validPostalCode(value))
                    return "Nieprawidłowy kod pocztowy: " + value;
                break;
            case EMAIL:
                if (!Validation.validEmail(value))
                    return "Nieprawidłowy email: " + value;
                break;
            case ID:
            case NAME:
            case KIND:
            case SHORT_NAME:
            case STREET:
            case ADDRESS:
            case CONTACT_PERSON:
            case NOTES:
                break;
            default:
                throw new UnexpectedTypeException("Brak rozpoatrywanego typu OrganisationUnitAttr");
        }
        return "";
    }

    public Map<OrganisationUnitAttr, Change> execute(OrganisationUnit organisationUnit) {
        if (organisationUnit == null) {
            return create();
        } else {
            return update(organisationUnit);
        }
    }

    private void save(OrganisationUnit organisationUnit) {
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        try {
            Session session = factory.currentSession();
            Transaction tx = session.beginTransaction();
            session.saveOrUpdate(organisationUnit);
            tx.commit();
        } catch (HibernateException e) {
            Log.error(e.getMessage(), e);
        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }
    }


    public static class Change {
        Object before;
        Object after;

        public Change(Object after) {
            this(null, after);
        }

        public Change(Object before, Object after) {
            this.before = before;
            this.after = after;
        }
    }
}
