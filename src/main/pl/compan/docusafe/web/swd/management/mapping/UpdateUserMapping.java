package pl.compan.docusafe.web.swd.management.mapping;

import pl.compan.docusafe.web.swd.management.SwdUser;
import pl.compan.docusafe.web.swd.management.sql.SwdUprawnienie;
import pl.compan.docusafe.web.swd.management.utils.SwdUtils;
import pl.zbp.users.Right;

import javax.persistence.Column;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by Damian on 03.01.14.
 */
public class UpdateUserMapping {
    private Integer id;
    private Integer clientId;
    private String clientName;
    private String login;
    private String lastName;
    private String firstName;
    private String position;
    private String email;
    private String phone;
    private String fax;
    private Boolean active = Boolean.FALSE;
    private Boolean blocked = Boolean.FALSE;
    private Boolean expiredPassword;
    private String lastLoginTime;
    private String lastInvalidLoginTime;
    private Boolean isLastLoginInvalid;
    private String maxCertExpDate;
    private Set<BigInteger> certToRem = new HashSet<BigInteger>();
    private Boolean sentPass = Boolean.FALSE;
    private Collection<X509Certificate> certList;
    private Collection<BigInteger> certSerials;
    //private Map<String, Right> grants;
    private Set<String> grantsCodes = new HashSet<String>();
    private Set<X509Certificate> certsToAdd = new HashSet<X509Certificate>();
    private String certSerialToRem;
    private Map<String,String> role = new HashMap<String, String>();


    public UpdateUserMapping() {

    }

    public UpdateUserMapping(SwdUser user) {
        if(user != null){
            id=user.getUserID();
            clientId = user.getKlientID();
            clientName=user.getClientName();
            login=user.getLogin();
            lastName=user.getLastName();
            firstName=user.getFirstName();
            position=user.getPosition();
            email=user.getEMail();
            phone=user.getPhone();
            fax=user.getFax();
            active=user.getActive();
            blocked=user.getBlocked();
            expiredPassword=user.getExpiredPassword();
            if(user.getLastLoginTime() != null){
                lastLoginTime=user.getLastLoginTime().toLocaleString();
            }
            if(user.getLastInvalidLoginTime() != null){
                lastInvalidLoginTime=user.getLastInvalidLoginTime().toLocaleString();
            }
            //grants = user.getGrants();
            if(user.getGrants() != null){
                grantsCodes = user.getGrants().keySet();
            }

            Date expDate = user.getMaxCertExpDate();
            if (expDate != null) {
                maxCertExpDate= expDate.toLocaleString();
            }

            Date lastInvalidLoginTime = user.getLastInvalidLoginTime();
            Date lastLoginTime = user.getLastLoginTime();
            if(lastInvalidLoginTime != null){
                isLastLoginInvalid = lastLoginTime.before(lastInvalidLoginTime);
            }else{
                isLastLoginInvalid = Boolean.FALSE;
            }

            certList = SwdUtils.getCertList(user);
        }
    }

    public void setClientNameById(Integer organisationUnit) {
        if(organisationUnit != null){
            clientName = SwdUtils.getClientNameForId(organisationUnit);
        }
    }

    public Boolean hasGrant(SwdUprawnienie grant) {

        if(grantsCodes.contains(grant.getKodUpr())){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public void addCert(X509Certificate newCert){
        certsToAdd.add(newCert);
    }



    public String getCertSerialToRem() {
        return certSerialToRem;
    }

    public void setCertSerialToRem(String certSerialToRem) {
        BigInteger certSerial = new BigInteger(certSerialToRem);
                this.certToRem.add(certSerial);
    }

/*
    public void setGrants(Map<String, Right> grants) {
        this.grants = grants;
    }
*/

    public Boolean getSentPass() {
        return sentPass;
    }

    public void setSentPass(Boolean sentPass) {
        this.sentPass = sentPass;
    }

    public Boolean getExpiredPassword() {
        return expiredPassword;
    }

    public void setExpiredPassword(Boolean expiredPassword) {
        this.expiredPassword = expiredPassword;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastInvalidLoginTime() {
        return lastInvalidLoginTime;
    }

    public void setLastInvalidLoginTime(String lastInvalidLoginTime) {
        this.lastInvalidLoginTime = lastInvalidLoginTime;
    }

    public String getMaxCertExpDate() {
        return maxCertExpDate;
    }

    public void setMaxCertExpDate(String maxCertExpDate) {
        this.maxCertExpDate = maxCertExpDate;
    }


    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        if(active == null){
            this.active = Boolean.FALSE;
        }else{
            this.active = active;
        }
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        if (blocked != null) {
            this.blocked = blocked;
        }
    }



    public Boolean getIsLastLoginInvalid() {
        return isLastLoginInvalid;
    }

    public void setIsLastLoginInvalid(Boolean isLastLoginInvalid) {
        this.isLastLoginInvalid = isLastLoginInvalid;
    }

    public Collection<X509Certificate> getCertList() {
        return certList;
    }

    public void setCertList(Collection<X509Certificate> certList) {
        this.certList = certList;
    }

    public void setCertToRem(Set<BigInteger> certToRem) {
        this.certToRem = certToRem;
    }

    public Set<BigInteger> getCertToRem() {
        return certToRem;
    }

    public Set<X509Certificate> getCertsToAdd() {
        return certsToAdd;
    }

    public void setCertsToAdd(Set<X509Certificate> certsToAdd) {
        this.certsToAdd = certsToAdd;
    }

    public Set<String>  getGrantsCodes() {
        return grantsCodes;
    }

    public void setGrantsCodes(Set<String> grantsCodes) {
        if(grantsCodes != null){
            this.grantsCodes = grantsCodes;
            this.grantsCodes.remove("dummy_grant");
        }
    }

    public Map<String, String> getRole() {
        return role;
    }

    public void setRole(Map<String, String> role) {
        this.role = role;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Collection<BigInteger> getCertSerials() {
        return certSerials;
    }

    public void setCertSerials(Collection<BigInteger> certSerials) {
        this.certSerials = certSerials;
    }


}
