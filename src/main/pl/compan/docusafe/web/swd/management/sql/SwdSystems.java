package pl.compan.docusafe.web.swd.management.sql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Damian on 30.12.13.
 */

@Entity
@Table(name="swd_rejestr")
public class SwdSystems {


    String regCode;
    String name;


    @Id
    @Column(name="REG_CODE")
    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    @Column(name="NAZWA_REJ")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SwdSystems that = (SwdSystems) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (regCode != null ? !regCode.equals(that.regCode) : that.regCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = regCode != null ? regCode.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
