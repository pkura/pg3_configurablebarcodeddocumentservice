package pl.compan.docusafe.web.swd.management.utils;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.web.swd.management.SwdUser;
import pl.compan.docusafe.web.swd.management.sql.SwdMailParts;
import pl.compan.docusafe.web.swd.management.sql.SwdSystems;
import pl.compan.docusafe.web.swd.management.sql.SwdUprawnienie;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;
import pl.zbp.users.UserManagementException;

import java.io.*;
import java.math.BigInteger;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by Damian on 31.12.13.
 */
public class SwdUtils {

    private static final Logger log = LoggerFactory.getLogger(SwdUserManagementFactory.class);

    /**
     * Finds List of SwdUserRole which holds information about roles of given user in SWD systems
     * @param user
     * @return List of SwdUserRole for give user
     */
/*    public static List<SwdUserRole> getSwdUserRoleList(SwdUser user) {
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        List<SwdUserRole> results = null;

        try{
            Criteria criteria = factory.currentSession().createCriteria(SwdUserRole.class);
            criteria.add(Restrictions.eq("userId", user.getUserID()));
            results = criteria.list();


        }catch (Exception e) {

            SwdUtils.log.error(e.getMessage(),e);

        } finally {

            SwdSessionFactory.closeSessionQuietly();
        }
        return results;
    }*/

    /**
     * Finds user by id and other parameters needed for complete SwdUser
     * @param userID Integer
     * @return SwdUser for given ID
     */
    public static SwdUser getSwdUserById(Integer userID){
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        SwdUser user = null;

        try {

            user = new SwdUser(factory.getUser(userID));

        } catch (Exception e) {

            SwdUtils.log.error(e.getMessage(),e);

        } finally {

            SwdSessionFactory.closeSessionQuietly();
        }

        if (user != null) {
            user.setClientName(getClientNameForUser(user));
        }


        return user;
    }


    public static User getUserByID(Integer userID){
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        User user = null;
        try {
            user = factory.getUser(userID);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return user;
    }

    /**
     * Finds client for given user and returns client name
     * @param user SwdUser
     * @return String of client name, if client name == null then it returns empty string ("")
     */
    public static String getClientNameForUser(SwdUser user){
            if(user != null){
                return getClientNameForId(user.getKlientID());
            }else {
                return "";
            }

    }

    /**
     * Returns client name for given client id
     * @param clientId
     * @return if client name == null then it returns empty string ("")
     */
    public static String getClientNameForId(Integer clientId) {
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        OrganisationUnit organisationUnit = null;
        String clientName = "";
        try {
            if(clientId != null){
                organisationUnit = factory.getOrganisationUnit(clientId);
            }
        } catch (Exception e) {

            SwdUtils.log.error(e.getMessage(),e);

        } finally {

            SwdSessionFactory.closeSessionQuietly();
        }

        if (organisationUnit != null) {
            clientName = organisationUnit.getName();
        }

        return clientName;
    }

    public static void setUserGrants(SwdUserManagementFactory factory, User user, Set<String> codes, Long docId)throws UserManagementException {
        Set<String> userGrants = user.getGrants().keySet();
        Set<String> grantsToAdd = new HashSet<String>();
        Set<String> grantsToRem = new HashSet<String>();
        if(codes != null){
            for(String c: codes){
                if(!userGrants.contains(c)){
                    grantsToAdd.add(c);
                }
            }

            for(String ug : userGrants){
                if(!codes.contains(ug)){
                    grantsToRem.add(ug);
                }
            }

        }else{
            for(String ug : userGrants){
                grantsToRem.add(ug);
            }
        }

        for (String g : grantsToRem){
            factory.revoke(user, g);
        }
        for(String g : grantsToAdd){
            factory.grant(user, g);
        }

        if(docId != null){
            if(!grantsToAdd.isEmpty() || !grantsToRem.isEmpty()){

            }
        }

    }

    public static void setUserCerts(SwdUserManagementFactory factory, User userFromDb, Set<BigInteger> certsToRem, Set<X509Certificate> certsToAdd, Long docId) throws UserManagementException {
        if(certsToRem != null){
            for(X509Certificate c : userFromDb.getCertificates().values()){
                if(certsToRem.contains(c.getSerialNumber())){
                    factory.removeCertificate(userFromDb,c);
                }
            }
        }
        if(certsToAdd != null){
            for(X509Certificate c : certsToAdd){
                factory.addCertificate(userFromDb,c);
            }

            if(docId != null && !certsToAdd.isEmpty()){

            }
        }
    }

    /**
     * Returns SwdRole if user has the same role in all SWD systems
     * if user don't have the same role in all systems or he hasn't got any role in any system then it returns null
     * @param //rolesList
     * @return
     */
/*    public static SwdRole getGeneralRole(List<SwdUserRole> rolesList) {
        List<SwdSystems> systemsList = SwdUserManagementFactory.getAllSystems();
        Map<SwdSystems,SwdRole> systemsRoles = new HashMap<SwdSystems, SwdRole>();
        SwdRole generalRole = null;

        //prepare map systems , user roles in system
        for(SwdUserRole ur : rolesList){
            systemsRoles.put(ur.getSystem(),ur.getRole());
        }


        for(SwdSystems s: systemsList){
            if(systemsRoles.containsKey(s)){
                if(generalRole == null){
                    generalRole = systemsRoles.get(s);
                }else if(!generalRole.equals(systemsRoles.get(s))){
                    return null;
                }
            }else{
                return null;
            }
        }
        return generalRole;
        return null;
    }*/

    public static List<SwdSystems> getAllSytems(DSContext context){
        List<SwdSystems> results = null;

        try{
            Criteria criteria = context.session().createCriteria(SwdSystems.class);

            results = criteria.list();
        }catch (Exception e){
            Log.error(e.getMessage(),e);
        }
        return results;

    }

    public static Map<String, List<SwdUprawnienie>> getAllUprawnieniaMap(DSContext context){
        List<SwdUprawnienie> uprawnienieList = null;
        List<String> groups = null;
        Map<String, List<SwdUprawnienie>> resultMap = new HashMap<String, List<SwdUprawnienie>>();
        try {
            groups = context.session().createQuery("select distinct grupa from SwdUprawnienie uprawnienie").list();
            for(String g : groups){
                Criteria criteria = context.session().createCriteria(SwdUprawnienie.class);
                criteria.add(Restrictions.eq("grupa", g));
                uprawnienieList = criteria.list();
                resultMap.put(g,uprawnienieList);
            }
        } catch (EdmException e) {
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * Returns Collection of user certificates
     * If user is null or there is no certificates returns empty collection
     * @return
     */
    public static Collection<X509Certificate> getCertList(SwdUser user){

        if(user != null){
            Map<BigInteger, X509Certificate> certificatesMap = user.getCertificates();
            if(certificatesMap != null){
                return certificatesMap.values();
            }
        }
        return new ArrayList<X509Certificate>();
    }

    /**
     * Returns Certificate from given file
     * If certificate could not be parsed it throws CertificateException
     * If some problems occur with open file and read throws IOException
     * @param file
     * @return X509Certificate
     * @throws IOException
     * @throws CertificateException
     */
    public static X509Certificate getCertificateFromFile(File file) throws IOException,CertificateException {
        X509Certificate cert = null;
        BufferedInputStream bif = null;
        try{
            bif = new BufferedInputStream(new FileInputStream(file));
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            cert = (X509Certificate)cf.generateCertificate(bif);
            return cert;
        } finally {
            bif.close();
        }
    }

    /**
     * Returns CER file with given title from given x509 certyficate
     * @param cert
     * @param title
     * @return
     * @throws IOException
     * @throws CertificateEncodingException
     */
    public static File generateCERFile(X509Certificate cert, String title) throws IOException, CertificateEncodingException {
        File file = File.createTempFile(title,".cer");

        Base64 encoder = new Base64() ;
        String certBegin = "-----BEGIN CERTIFICATE-----\n";
        String endCert = "\n-----END CERTIFICATE-----";

        BufferedWriter bw = null;


            try {
                bw = new BufferedWriter(new FileWriter(file));
                String certBody = new String(encoder.encode(cert.getEncoded()));
                String pemCert = certBegin + certBody + endCert;
                bw.write(pemCert);

            } finally {
                bw.close();
            }


        return file;
    }

    /**
     * Returns DER file with given title from given x509 certyficate
     * @param cert
     * @param title
     * @return
     * @throws IOException
     * @throws CertificateEncodingException
     */
    public static File generateDERFile(X509Certificate cert, String title) throws IOException, CertificateEncodingException {
        File file = File.createTempFile(title,".der");
        BufferedOutputStream bos = null;

            try {
                bos = new BufferedOutputStream(new FileOutputStream(file));
                bos.write(cert.getEncoded());
            } finally {
                bos.close();
            }


        return file;
    }

    public static List<SwdMailParts> getAllTitleParts(DSContext context){
       List<SwdMailParts> partsList = new LinkedList<SwdMailParts>();
       try{
            Criteria criteria = context.session().createCriteria(SwdMailParts.class);
           criteria.add(Restrictions.ilike("type","T"));
            criteria.addOrder(Order.asc("id"));
            partsList = criteria.list();
        }catch(EdmException e){
           log.error(e.getMessage(),e);
       }
        return partsList;

    }

    public static List<SwdMailParts> getAllMsgParts(DSContext context){
               List<SwdMailParts> partsList = new LinkedList<SwdMailParts>();
       try{
            Criteria criteria = context.session().createCriteria(SwdMailParts.class);
           criteria.add(Restrictions.ilike("type","W"));
            criteria.addOrder(Order.asc("id"));
            partsList = criteria.list();
        }catch(EdmException e){
           log.error(e.getMessage(),e);
       }
        return partsList;
    }

    /**
     * returns List of SWDUsers based on List of Users
     * @param userList
     * @return
     */
    public static List<SwdUser> getSwdUsersFromUsers(List<User> userList){
        List<SwdUser> swdUserList = new ArrayList<SwdUser>();
        for(User u : userList){
            swdUserList.add(new SwdUser(u));
        }
        return swdUserList;
    }



}
