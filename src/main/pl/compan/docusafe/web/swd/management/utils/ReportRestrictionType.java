package pl.compan.docusafe.web.swd.management.utils;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum ReportRestrictionType {
    GT(">"),
    GE(">="),
    EQ("="),
    LT("<"),
    LE("<="),
    LIKE("like");
    
    private final String sqlComparator;

    ReportRestrictionType(String sqlComparator) {
        this.sqlComparator = " " + sqlComparator + " ? ";
    }

    public String getRightSqlComparator() {
        return sqlComparator;
    }
}
