package pl.compan.docusafe.web.swd.management.jsp.attributes;

import pl.compan.docusafe.web.swd.management.jsp.attributes.types.OrganisationUnitAttr;
import pl.compan.docusafe.web.swd.management.utils.SwdOrganisationUnitFactory;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspComponentAttributes;
import pl.zbp.users.OrganisationUnit;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OrganisationUnitAttributes extends JspComponentAttributes {

    protected String jspId;
    protected String jspName;
    protected String jspUnitCode;
    protected String jspKind;
    protected String jspShortName;
    protected String jspStreet;
    protected String jspAddress;
    protected String jspPostalCode;
    protected String jspContactPerson;
    protected String jspEmail;
    protected String jspNotes;

    public OrganisationUnitAttributes(String title) {
        setJspTitle(title);
        for (OrganisationUnitAttr attr : OrganisationUnitAttr.values())
            addAttribute(getJspAttribute(attr));
    }

    public void setFields(OrganisationUnit customer) {
        this.jspId = Integer.toString(customer.getKlientId());
        this.jspName = customer.getName();
        this.jspUnitCode = customer.getUnitCode();
        this.jspKind = customer.getKind();
        this.jspShortName = customer.getShortName();
        this.jspStreet = customer.getStreet();
        this.jspAddress = customer.getAddress();
        this.jspPostalCode = customer.getPostalCode();
        this.jspContactPerson = customer.getContactPerson();
        this.jspEmail = customer.getEmail();
        this.jspNotes = customer.getNotes();
    }

    public String getFieldName(OrganisationUnitAttr attr) {
        switch (attr) {
            case ID:
                return "jspId";
            case NAME:
                return "jspName";
            case UNIT_CODE:
                return "jspUnitCode";
            case KIND:
                return "jspKind";
            case SHORT_NAME:
                return "jspShortName";
            case STREET:
                return "jspStreet";
            case ADDRESS:
                return "jspAddress";
            case POSTAL_CODE:
                return "jspPostalCode";
            case CONTACT_PERSON:
                return "jspContactPerson";
            case EMAIL:
                return "jspEmail";
            case NOTES:
                return "jspNotes";
            default:
                throw new IllegalArgumentException("Brak nazwy pola dla OrganisationUnitAttr: " + attr.name());
        }
    }

    public OrganisationUnitAttr findOrganisationUnitAttrByFieldName(String fieldName) {
        for (OrganisationUnitAttr attr : OrganisationUnitAttr.values())
            if (getFieldName(attr).equalsIgnoreCase(fieldName))
                return attr;
        throw new IllegalArgumentException("Brak atrybutu dla nazwy pola: " + fieldName);
    }

    public String getFieldValue(OrganisationUnitAttr attr) {
        switch (attr) {
            case ID:
                return jspId;
            case NAME:
                return jspName;
            case UNIT_CODE:
                return jspUnitCode;
            case KIND:
                return jspKind;
            case SHORT_NAME:
                return jspShortName;
            case STREET:
                return jspStreet;
            case ADDRESS:
                return jspAddress;
            case POSTAL_CODE:
                return jspPostalCode;
            case CONTACT_PERSON:
                return jspContactPerson;
            case EMAIL:
                return jspEmail;
            case NOTES:
                return jspNotes;
            default:
                throw new IllegalArgumentException("Brak nazwy pola dla OrganisationUnitAttr: " + attr.name());
        }
    }

    public JspAttribute getJspAttribute(OrganisationUnitAttr attr) {
        switch (attr.type) {
            case TEXT:
                return JspAttribute.Factory.createTextField(getFieldName(attr),attr.title,attr.isEditable());
            case SELECT:
                return JspAttribute.Factory.createSelectField(getFieldName(attr), attr.title, attr.isEditable(), SwdOrganisationUnitFactory.getAllKindsFieldSelect(""));
            default:
                throw new IllegalArgumentException("Nie mozna stworzyc atrybutu dla pola rodzaju: " + attr.type.name());
        }
    }

    public String getJspId() {
        return jspId;
    }

    public String getJspName() {
        return jspName;
    }

    public String getJspUnitCode() {
        return jspUnitCode;
    }

    public String getJspKind() {
        return jspKind;
    }

    public String getJspShortName() {
        return jspShortName;
    }

    public String getJspStreet() {
        return jspStreet;
    }

    public String getJspAddress() {
        return jspAddress;
    }

    public String getJspPostalCode() {
        return jspPostalCode;
    }

    public String getJspContactPerson() {
        return jspContactPerson;
    }

    public String getJspEmail() {
        return jspEmail;
    }

    public String getJspNotes() {
        return jspNotes;
    }

    public void setJspId(String jspId) {
        this.jspId = jspId;
    }

    public void setJspName(String jspName) {
        this.jspName = jspName;
    }

    public void setJspUnitCode(String jspUnitCode) {
        this.jspUnitCode = jspUnitCode;
    }

    public void setJspKind(String jspKind) {
        this.jspKind = jspKind;
    }

    public void setJspShortName(String jspShortName) {
        this.jspShortName = jspShortName;
    }

    public void setJspStreet(String jspStreet) {
        this.jspStreet = jspStreet;
    }

    public void setJspAddress(String jspAddress) {
        this.jspAddress = jspAddress;
    }

    public void setJspPostalCode(String jspPostalCode) {
        this.jspPostalCode = jspPostalCode;
    }

    public void setJspContactPerson(String jspContactPerson) {
        this.jspContactPerson = jspContactPerson;
    }

    public void setJspEmail(String jspEmail) {
        this.jspEmail = jspEmail;
    }

    public void setJspNotes(String jspNotes) {
        this.jspNotes = jspNotes;
    }

    @Override
    public boolean isSelected(String fieldName, String select) {
        String selected = getFieldValue(findOrganisationUnitAttrByFieldName(fieldName));
        return selected != null && selected.equalsIgnoreCase(select);
    }

    public JspAttribute findJspAttribute(OrganisationUnitAttr attr) {
        String fieldName = getFieldName(attr);
        return findAttributeByFieldName(fieldName);
    }
}
