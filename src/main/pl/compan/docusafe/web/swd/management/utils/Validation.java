package pl.compan.docusafe.web.swd.management.utils;

import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class Validation {

    /**
     * Sprawdzenie kodu pocztowego: DD-DDD
     */
    public static boolean validPostalCode(String postalCode) {
        return Pattern.compile("^\\d{2}-\\d{3}$").matcher(postalCode).find();
    }

    /**
     * Sprawdzenie adresu email
     */
    public static boolean validEmail(String email) {
        return Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$").matcher(email).find();
    }

    /**
     * Sprawdzenie czy podany ci�g sk�ada si� tylko z cyfr i jest wymaganej d�ugo�ci
     */
    public static boolean validNumber(String number, int minLength, int maxLength) {
        return Pattern.compile("^\\d{" + Integer.toString(minLength) + "," + Integer.toString(maxLength) + "}$").matcher(number).find();
    }


}
