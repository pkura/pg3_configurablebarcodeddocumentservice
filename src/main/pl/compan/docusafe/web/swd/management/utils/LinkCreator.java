package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.web.jsp.component.JspComponentTable;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class LinkCreator {

    private final String baseLink;
    private boolean linkContainsAttrs;
    List<Map.Entry<String, String>> attrs = new ArrayList<Map.Entry<String, String>>();

    public LinkCreator(String baseLink) {
        this(baseLink, false);
    }

    public LinkCreator(String baseLink, boolean linkContainsAttrs) {
        this.baseLink = baseLink;
        this.linkContainsAttrs = linkContainsAttrs;
    }

    /**
     * Add to link "layout=lowCaseName" if layout != null and layout != MAIN
     */
    public LinkCreator setLayout(JspComponentTable.Layout layout) {
        if (layout != null && layout != JspComponentTable.Layout.MAIN)
            attrs.add(new AbstractMap.SimpleEntry<String, String>("layout", layout.name().toLowerCase()));
        return this;
    }

    /**
     * Add to link if value != null
     */
    public LinkCreator add(String name, String value) {
        if (value!=null)
            attrs.add(new AbstractMap.SimpleEntry<String, String>(name, value));
        return this;
    }
    /**
     * Add to link String of value if value != null
     */
    public LinkCreator add(String name, Object value) {
        if (value!=null)
            attrs.add(new AbstractMap.SimpleEntry<String, String>(name, value.toString()));
        return this;
    }

    public String create() {
        StringBuilder builder = new StringBuilder();
        builder.append(baseLink);

        for (Map.Entry<String, String> a : attrs) {
            builder.append(linkContainsAttrs ? '&' : '?').append(a.getKey())
                    .append('=')
                    .append(a.getValue());
            linkContainsAttrs = true;
        }

        return builder.toString();
    }
}
