package pl.compan.docusafe.web.swd.management.jsp.table;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.jsp.Filter;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.CustomerColumn;
import pl.compan.docusafe.web.swd.management.sql.SwdCustomFilter;
import pl.compan.docusafe.web.swd.management.utils.CustomFilterSelect;
import pl.compan.docusafe.web.swd.management.utils.SwdOrganisationUnitFactory;
import pl.zbp.users.OrganisationUnit;

import javax.validation.UnexpectedTypeException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OrganisationUnitsFilterTable extends OrganisationUnitsTable {

    protected String filterId;
    protected String filterSignature;
    protected String filterKind;
    protected String filterName;
    protected String customFilter;
    private List<Filter> filters = new ArrayList<Filter>();

    public OrganisationUnitsFilterTable(String title) {
        super(title);
    }

//    @Override
//    public String getFilterName(String column) {
//        CustomerColumn customerColumn = CustomerColumn.findByTitle(column);
//        return "filter" + customerColumn.code;
//    }

    public static OrganisationUnitsFilterTable init(OrganisationUnitsFilterTable table, boolean customFilterOn, ColumnSortLinkCreator columnSortLinkCreator) {
        return table.initOrganisationUnitsFilterTable(customFilterOn, columnSortLinkCreator);
    }

    private OrganisationUnitsFilterTable initOrganisationUnitsFilterTable(boolean customFilterOn, ColumnSortLinkCreator columnSortLinkCreator) {

        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (CustomerColumn col : CustomerColumn.values()) {
            JspAttribute filter = getFilterAttribute(col);
            columns.add(new JspTableColumn(
                    filter,
                    col.width,
                    null,
                    col.title,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col, false) : null,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col, true) : null,
                    col.name()
            ));
        }

        if (customFilterOn) {
            List<SwdCustomFilter> filters = SwdCustomFilter.searchAsList(SwdCustomFilter.FilterCN.ORGANISATION_UNIT);
            JspAttribute.SelectField customFilter = CustomFilterSelect.create("", filters.toArray(new SwdCustomFilter[filters.size()]));
            if (customFilter != null) {
                columns.add(new JspTableColumn(
                        JspAttribute.Factory.createSelectFilter("customFilter", customFilter),
                        JspTableColumn.DEFAULT_SMALL_WIDTH,
                        null,
                        "Filtr w�asny",
                        null,
                        null,
                        CustomFilterSelect.CUSTOM_FILTER));
            }
        }

        setJspColumns(columns);
        setJspFilterEnabled(true);
        return this;
    }

    private JspAttribute getFilterAttribute(CustomerColumn col) {
        switch (col) {
            case LP:
                return null;
            case ID:
                return JspAttribute.Factory.createTextFilter("filterId");
            case KOD:
                return JspAttribute.Factory.createTextFilter("filterSignature");
            case RODZAJ:
                return JspAttribute.Factory.createSelectFilter("filterKind", SwdOrganisationUnitFactory.getAllKindsFieldSelect("DOWOLNY"));
            case NAZWA:
                return JspAttribute.Factory.createTextFilter("filterName");
            default:
                throw new UnexpectedTypeException("Brak typu CustomerColumn dla nazwy: " + col.name());
        }
    }

    public void initChosenItemGetter() {
        if (chosenItemGetter == null)
            chosenItemGetter = new ChosenItemGetter<OrganisationUnit>() {
                @Override
                public Object getDataItemId(OrganisationUnit item) {
                    return getDataItemId(item);
                }
            };
    }

    public String getCustomFilter() {
        return customFilter;
    }

    @Override
    public String getTableData(List<?> tableData, OrganisationUnit customer, JspTableColumn column) {
        if (column.getColumnName().equalsIgnoreCase(CustomFilterSelect.CUSTOM_FILTER))
            return null;
        return super.getTableData(tableData, customer, column);
    }

    public void updateFilters() {
        filters.clear();

        if (StringUtils.isNotEmpty(getFilterId()))
            addFilter(new Filter.Text<OrganisationUnit>(getFilterId(), false) {
                @Override
                protected Object getFieldValue(OrganisationUnit fieldValue) {
                    return fieldValue.getKlientId();
                }
            });
        if (StringUtils.isNotEmpty(getFilterSignature()))
            addFilter(new Filter.Text<OrganisationUnit>(getFilterSignature(), false) {
                @Override
                protected Object getFieldValue(OrganisationUnit fieldValue) {
                    return fieldValue.getUnitCode();
                }
            });
        if (StringUtils.isNotEmpty(getFilterKind()))
            addFilter(new Filter.Text<OrganisationUnit>(getFilterKind(), true) {
                @Override
                protected Object getFieldValue(OrganisationUnit fieldValue) {
                    return fieldValue.getKind();
                }
            });
        if (StringUtils.isNotEmpty(getFilterName()))
            addFilter(new Filter.Text<OrganisationUnit>(getFilterName(), true) {
                @Override
                protected Object getFieldValue(OrganisationUnit fieldValue) {
                    return fieldValue.getName();
                }
            });
//        if (StringUtils.isNotEmpty(getCustomFilter()))
//            addFilter(new CustomFilterSelect.Filter(getFilterName(), true) {
//                @Override
//                protected Object getFieldValue(OrganisationUnit fieldValue) {
//                    return fieldValue.getName();
//                }
//            });
    }

    private void addFilter(Filter<OrganisationUnit> filter) {
        if (filter.isActive())
            filters.add(filter);
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterKind() {
        return filterKind;
    }

    public void setFilterKind(String filterKind) {
        this.filterKind = filterKind;
    }

    public String getFilterSignature() {
        return filterSignature;
    }

    public void setFilterSignature(String filterSignature) {
        this.filterSignature = filterSignature;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public boolean isSelected(JspTableColumn column, String select) {
        if (column.getColumnName().equalsIgnoreCase(CustomFilterSelect.CUSTOM_FILTER))
            return customFilter.equalsIgnoreCase(select);

        CustomerColumn col = CustomerColumn.valueOf(column.getColumnName());
        switch (col) {
            case RODZAJ:
                return this.filterKind != null && this.filterKind.equalsIgnoreCase(select);
            case LP:
            case ID:
            case KOD:
            case NAZWA:
                throw new UnexpectedTypeException("Kolumna nie jest typu select: " + col.name());
            default:
                throw new UnexpectedTypeException("Brak typu CustomerColumn dla nazwy: " + col.name());
        }
    }
}