package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.web.swd.management.sql.ReportWhereColumn;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportRestriction {

    private ReportWhereColumn reportWhereColumn;
    private ReportDataType dataType;
    private ReportRestrictionType restrictionType;
    private String value;

    public ReportRestriction(ReportWhereColumn reportWhereColumn, String value, ReportRestrictionType restrictionType) {
        this.reportWhereColumn = reportWhereColumn;
        this.dataType = ReportDataType.valueOf(reportWhereColumn.getType());
        this.value = value;
        this.restrictionType = restrictionType;
    }

    public ReportWhereColumn getReportWhereColumn() {
        return reportWhereColumn;
    }

    public ReportDataType getDataType() {
        return dataType;
    }

    public ReportRestrictionType getRestrictionType() {
        return restrictionType;
    }

    public String getValue() {
        return value;
    }

    public String getRestrictionSqlClause() {
        return " " + reportWhereColumn.getAlias() + restrictionType.getRightSqlComparator();
    }
}
