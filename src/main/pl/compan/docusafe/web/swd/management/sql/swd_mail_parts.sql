CREATE TABLE [dbo].[SWD_MAIL_PARTS](
	[ID] [numeric](19, 0) NOT NULL PRIMARY KEY CLUSTERED,
	[TYPE] [nchar](1) NOT NULL,
	[NAME] [nvarchar](64) NOT NULL,
	[TEXT] [nvarchar](max) NULL,
	[ATTACH] [numeric](19, 0) NULL,
)

ALTER TABLE [dbo].[SWD_MAIL_PARTS]  WITH CHECK ADD  CONSTRAINT [FK_SWD_MAIL_PARTS_DS_DOCUMENT] FOREIGN KEY([ATTACH])
REFERENCES [dbo].[DS_DOCUMENT] ([ID])
GO
ALTER TABLE [dbo].[SWD_MAIL_PARTS] CHECK CONSTRAINT [FK_SWD_MAIL_PARTS_DS_DOCUMENT]
GO
ALTER TABLE [dbo].[SWD_MAIL_PARTS]  WITH CHECK ADD  CONSTRAINT [CK_TYPE] CHECK  (([TYPE]='T' OR [TYPE]='W'))
GO
ALTER TABLE [dbo].[SWD_MAIL_PARTS] CHECK CONSTRAINT [CK_TYPE]
GO


--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(1 AS Numeric(19, 0)), N'T', N'Temat Dost�p do SWI
--', N'Dost�p do System�w Wymiany Informacji
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(2 AS Numeric(19, 0)), N'W', N'Witam przekazuj� dost�p
--', N'Witamy<newline>Niniejszym przekazujemy dostep do:
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(3 AS Numeric(19, 0)), N'W', N'Dost�p WWW-BR
--', N'Systemu Bankowy Rejestr (adres: https://swd.zbp.pl)
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(4 AS Numeric(19, 0)), N'W', N'Dost�p WWW-DZ
--', N'Centrum Certyfikacyjnego (adres: https://swd.zbp.pl)
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(5 AS Numeric(19, 0)), N'W', N'Pani login
--', N'Pani login: <ESC:LOGIN/>
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(6 AS Numeric(19, 0)), N'W', N'Pani login:
--', N'Pana login: <ESC:LOGIN/>
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(7 AS Numeric(19, 0)), N'W', N'Has�o telefonicznie
--', N'Has�o zostanie przekazane telefonicznie
--', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(8 AS Numeric(19, 0)), N'W', N'Z powa�aniem
--', N'Z powa�aniem,
--Zesp� System�w Wymiany Informacji
--tel: 22 4868422
--mail: swd@cpb.pl', NULL)
--insert [dbo].[SWD_MAIL_PARTS] ([ID], [TYPE], [NAME], [TEXT], [ATTACH]) VALUES (CAST(9 AS Numeric(19, 0)), N'T', N'Mail Testowy
--', N'To jest wiadomo�� testowa
--', NULL)
