package pl.compan.docusafe.web.swd.management.mapping;

/**
 * Created by Damian on 13.01.14.
 */
public class SwdCustomFilterSelect {
    private Integer id;
    private String whereClause;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }


    /**
     * Returns TRUE if any filter was selected (or typed)
     * if no filter was selected and where clause is empty (or null) it returns FALSE
     * @return
     */
    public Boolean isSelected(){
        if(id == null && (whereClause == null || "".equals(whereClause))){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
