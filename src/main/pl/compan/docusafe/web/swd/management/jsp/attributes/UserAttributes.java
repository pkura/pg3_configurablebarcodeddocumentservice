package pl.compan.docusafe.web.swd.management.jsp.attributes;

import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspComponentAttributes;
import pl.compan.docusafe.web.swd.management.jsp.attributes.types.UsersAttr;
import pl.zbp.users.User;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UserAttributes extends JspComponentAttributes {

    public UserAttributes(String title) {
        setJspTitle(title);
    }


    public JspAttribute getJspAttribute(UsersAttr attr, User user) {
        switch (attr.type) {
            case TEXT:
                return JspAttribute.Factory.createAttrAsField(attr.title, attr.getValueFrom(user));
            default:
                throw new IllegalArgumentException("Nie mozna stworzyc atrybutu dla pola rodzaju: " + attr.type.name());
        }
    }

    public void setJspData(User user) {
        for (UsersAttr attr : UsersAttr.values())
            addAttribute(getJspAttribute(attr, user));
    }
}
