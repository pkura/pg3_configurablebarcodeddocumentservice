package pl.compan.docusafe.web.swd.management.jsp.table;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.UsersListAction;
import pl.compan.docusafe.web.swd.management.jsp.Filter;
import pl.compan.docusafe.web.swd.management.jsp.LoginPeriod;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.UsersColumn;
import pl.compan.docusafe.web.swd.management.sql.SwdCustomFilter;
import pl.compan.docusafe.web.swd.management.utils.AlphanumComparator;
import pl.compan.docusafe.web.swd.management.utils.CustomFilterSelect;
import pl.zbp.users.User;

import javax.validation.UnexpectedTypeException;
import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UsersTable extends JspComponentTable<User> {

    protected String filterLastName;
    protected String filterFirstName;
    protected String filterLogin;
    protected String filterActive;
    protected String filterBlocked;
    protected String filterLastInvalidLoginTime;
    protected String filterId;
    protected String filterLastLoginTime;
    protected String customFilter;
    private List<Filter> filters = new ArrayList<Filter>();

    public UsersTable(String title, int filterColumnWidth) {
        super(title);
        setJspFilterColumnWidth(filterColumnWidth);
    }

    public UsersTable init(ColumnSortLinkCreator columnSortLinkCreator, boolean customFilterOn) {
        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (UsersColumn col : UsersColumn.values()) {
            JspAttribute filter = getFilterAttriubtes(col);
            columns.add(new JspTableColumn(
                    filter,
                    col.width,
                    null,
                    col.title,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col,false) : null,
                    col.sortEnabled ? columnSortLinkCreator.getLink(col,true) : null,
                    col.name()));
        }

        if (customFilterOn) {
            List<SwdCustomFilter> filters = SwdCustomFilter.searchAsList(SwdCustomFilter.FilterCN.USER);
            JspAttribute.SelectField customFilter = CustomFilterSelect.create("", filters.toArray(new SwdCustomFilter[filters.size()]));
            if (customFilter != null) {
                columns.add(new JspTableColumn(
                        JspAttribute.Factory.createSelectFilter("customFilter", customFilter),
                        JspTableColumn.DEFAULT_SMALL_WIDTH,
                        null,
                        "Filtr w�asny",
                        null,
                        null,
                        CustomFilterSelect.CUSTOM_FILTER));
            }
        }

        setJspColumns(columns);
        setJspFilterEnabled(true);
        return this;
    }

    public static String getSortLink(String baseLink, boolean isParamsAdded, UsersColumn column, boolean ascending) {
        return baseLink +
                (!isParamsAdded ? "?" : "&") +
                "orderBy=" + column.name() +
                "&ascending=" + Boolean.toString(ascending);/* +
                "&withBackup=" + withBackup +
                "&filterBy=" + filterBy +
                "&orderTab=" + orderTab +
                "&filterName=" + filterName;*/
    }

    public JspAttribute getFilterAttriubtes(UsersColumn col) {
        String name = getFilterFieldName(col);
        if (StringUtils.isEmpty(name))
            return null;

        switch (col) {
            case AKTYWNY:
                return JspAttribute.Factory.createSelectFilter(name, JspAttribute.SelectField.createBooleanSelectField("0", "1", "-"));
            case ZABLOKOWANY:
                return JspAttribute.Factory.createSelectFilter(name, JspAttribute.SelectField.createBooleanSelectField("0", "1", "-"));
            case OST_LOGOWANIE: {
                List<JspAttribute.SelectItem> loginPeriods = new ArrayList<JspAttribute.SelectItem>();
                for (LoginPeriod period : LoginPeriod.values())
                    loginPeriods.add(new JspAttribute.SelectItem(period.getId(), period.getTitle()));

                JspAttribute.SelectField attr = new JspAttribute.SelectField(
                        loginPeriods, LoginPeriod.WSZYSTKIE.getId());
                return JspAttribute.Factory.createSelectFilter(name, attr);
            }

            default:
                return JspAttribute.Factory.createTextFilter(name);
        }
    }

    public String getFilterFieldName(UsersColumn col) {
        switch (col) {
            case NAZWISKO:
                return "filterLastName";
            case IMIE:
                return "filterFirstName";
            case LOGIN:
                return "filterLogin";
            case AKTYWNY:
                return "filterActive";
            case ZABLOKOWANY:
                return "filterBlocked";
            case OST_NIEUDANE_LOGOWANIE:
                return "filterLastInvalidLoginTime";
            case ID:
                return "filterId";
            case OST_LOGOWANIE:
                return "filterLastLoginTime";

            case POWOD:
                return null;
            case LP:
                return null;
            default:
                // dla takich nazw nie powinny byc w og�le wywo�ywane te metody
                throw new UnexpectedTypeException("Brak rozpatrywanego typu UsersColumn: " + col.name());
        }
    }


//    public JspComponentTable setJspFilteredData(List<User> users, String orderBy, String ascending) {
//        users = filterUsers(users);
//        return setJspData(users, orderBy, ascending);
//    }

    public static List<User> filterUsers(UsersTable usersTable, List<User> users) {
        if (usersTable==null) return users;
        users = Filter.filter(usersTable.filters, users);
        return users;
    }
    public List<User> filterUsers(List<User> users) {
        users = Filter.filter(filters, users);
        return users;
    }

    public JspComponentTable setJspData(List<User> users, String sortField, String sortAscending) {
        UsersColumn usersColumn = UsersColumn.findByName(sortField, UsersColumn.NAZWISKO);
        boolean ascending = sortAscending == null || Boolean.parseBoolean(sortAscending);
        sortByColumn(users, usersColumn, ascending);
        this.jspData = users;
        return this;
    }

    private static void sortByColumn(List<User> users, final UsersColumn column, final boolean ascending) {
        final AlphanumComparator alphanumComparator = new AlphanumComparator();
        Collections.sort(users,
                new Comparator<Object>() {

                    @Override
                    public int compare(Object o1, Object o2) {

                        Object o1Name = column.getData((User) o1);
                        Object o2Name = column.getData((User) o2);

                        if (o1Name == null && o2Name == null)
                            return 0;
                        if (o1Name == null)
                            o1Name = "";
                        if (o2Name == null)
                            o2Name = "";

                        return ascending(alphanumComparator.compare(o1Name.toString(), o2Name.toString()));
                    }

                    private int ascending(int winner) {
                        return ascending ? winner : -winner;
                    }
                }
        );
    }

    public UsersColumn findUserColumnByFilterFieldName(String fieldName) {
        for (UsersColumn col : UsersColumn.values())
            if (getFilterFieldName(col).equalsIgnoreCase(fieldName))
                return col;
        // dla takich nazw nie powinny byc w og�le wywo�ywane te metody
        throw new IllegalArgumentException("Brak kolumny dla nazwy pola: " + fieldName);
    }

    public void updateFilters() {
        filters.clear();

        for (final UsersColumn col : UsersColumn.values()) {
            String value = getFilterFieldValue(col);
            if (StringUtils.isNotEmpty(value)) {
                addFilter(createFilter(col, value));
            }
        }
    }

    public String getFilterFieldValue(UsersColumn col) {
        switch (col) {
            case NAZWISKO:
                return filterLastName;
            case IMIE:
                return filterFirstName;
            case LOGIN:
                return filterLogin;
            case AKTYWNY:
                return filterActive;
            case ZABLOKOWANY:
                return filterBlocked;
            case OST_NIEUDANE_LOGOWANIE:
                return filterLastInvalidLoginTime;
            case ID:
                return filterId;
            case OST_LOGOWANIE:
                return filterLastLoginTime;

            case POWOD:
                return null;
            case LP:
                return null;
            default:
                throw new UnexpectedTypeException("Brak rozpatrywanego typu UsersColumn: " + col.name());
        }
    }

    private Filter<User> createFilter(final UsersColumn col, final String value) {
        switch (col) {
            case OST_LOGOWANIE:
                LoginPeriod loginPeriod = LoginPeriod.valueOf(value);
                return new LoginTimeFilter(loginPeriod);

            case AKTYWNY:
            case ZABLOKOWANY:
                return new Filter.Text<User>(value, true) {

                    @Override
                    protected boolean checkIfNullValue() {
                        return checkFieldValue(false);
                    }

                    @Override
                    protected Object getFieldValue(User fieldValue) {
                        return col.getData(fieldValue);
                    }
                };

            default:
                return new Filter.Text<User>(value, true) {
                    @Override
                    protected Object getFieldValue(User fieldValue) {
                        return col.getData(fieldValue);
                    }
                };
        }
    }

    private void addFilter(Filter<User> filter) {
        if (filter != null && filter.isActive())
            filters.add(filter);
    }

    public String getFilterLastName() {
        return filterLastName;
    }

    public void setFilterLastName(String filterLastName) {
        this.filterLastName = filterLastName;
    }

    public String getFilterFirstName() {
        return filterFirstName;
    }

    public void setFilterFirstName(String filterFirstName) {
        this.filterFirstName = filterFirstName;
    }

    public String getFilterLogin() {
        return filterLogin;
    }

    public void setFilterLogin(String filterLogin) {
        this.filterLogin = filterLogin;
    }

    public String getFilterActive() {
        return filterActive;
    }

    public void setFilterActive(String filterActive) {
        this.filterActive = filterActive;
    }

    public String getFilterBlocked() {
        return filterBlocked;
    }

    public void setFilterBlocked(String filterBlocked) {
        this.filterBlocked = filterBlocked;
    }

    public String getFilterLastInvalidLoginTime() {
        return filterLastInvalidLoginTime;
    }

    public void setFilterLastInvalidLoginTime(String filterLastInvalidLoginTime) {
        this.filterLastInvalidLoginTime = filterLastInvalidLoginTime;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterLastLoginTime() {
        return filterLastLoginTime;
    }

    public void setFilterLastLoginTime(String filterLastLoginTime) {
        this.filterLastLoginTime = filterLastLoginTime;
    }

    @Override
    public boolean isSelected(JspTableColumn column, String select) {
        if (column.getColumnName().equalsIgnoreCase(CustomFilterSelect.CUSTOM_FILTER))
            return customFilter.equalsIgnoreCase(select);

        UsersColumn col = UsersColumn.valueOf(column.getColumnName());
        String value = getFilterFieldValue(col);
        return value != null && value.equalsIgnoreCase(select);
//        switch(col){

//            case AKTYWNY:
//                return this.filterActive != null && this.filterActive.equalsIgnoreCase(select);
//            case ZABLOKOWANY:
//                return this.filterActive != null && this.filterActive.equalsIgnoreCase(select);
//            case OST_LOGOWANIE:
//                return this.filterActive != null && this.filterActive.equalsIgnoreCase(select);
//            default:
//                throw new UnexpectedTypeException("Brak typu CustomerColumn dla nazwy: " + col.name());
//                throw new UnexpectedTypeException("Kolumna nie jest typu select: " + col.name());
//        }
    }

    @Override
    public String getData(User user, JspTableColumn column) {
        if (column.getColumnName().equalsIgnoreCase(CustomFilterSelect.CUSTOM_FILTER))
            return null;

        UsersColumn usersColumn = UsersColumn.valueOf(column.getColumnName());
        switch (usersColumn) {
            case LP:
                return Integer.toString(jspData.indexOf(user) + 1);
            case AKTYWNY:
            case ZABLOKOWANY: {
                Boolean data = (Boolean) usersColumn.getData(user);
                return data != null && data ? "1" : "0";
            }
            default: {
                Object data = usersColumn.getData(user);
                return data != null ? data.toString() : "";
            }
        }
    }

    @Override
    public String getLink(User user, JspTableColumn column) {
        return UsersListAction.getTableDataLink(user, null, parentLayout==Layout.POPUP);
    }

    public String getCustomFilter() {
        return customFilter;
    }

    public void setCustomFilter(String customFilter) {
        this.customFilter = customFilter;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public static class LoginTimeFilter extends pl.compan.docusafe.web.swd.management.jsp.Filter<User> {

        public LoginTimeFilter(LoginPeriod loginPeriod) {
            super(new LoginTimeValidator(loginPeriod));
        }

        @Override
        protected boolean checkIfNullValue() {
            LoginTimeValidator loginTimeValidator = (LoginTimeValidator) this.filter;
            return loginTimeValidator.valid();
        }

        @Override
        protected boolean checkFieldValue(Object value) {
            Date lastLoginTime = (Date) value;
            LoginTimeValidator loginTimeValidator = (LoginTimeValidator) this.filter;
            return loginTimeValidator.valid(lastLoginTime);
        }

        @Override
        protected Object getFieldValue(User fieldValue) {
            return fieldValue.getLastLoginTime();
        }

        public static class LoginTimeValidator {

            //private LoginPeriod loginPeriod;
            //private Date current;
            private Date from;
            private Date to;


            public LoginTimeValidator(LoginPeriod loginPeriod) {
                //this.loginPeriod = loginPeriod;

                this.from = null;
                this.to = null;

                switch (loginPeriod) {
                    case WSZYSTKIE:
                        break;
                    case W_TYM_ROKU: {
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.YEAR, -1);
                        this.from = cal.getTime();
                        break;
                    }
                    case PONAD_ROK: {
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.YEAR, -1);
                        this.to = cal.getTime();
                        break;
                    }
                    default:
                        throw new UnexpectedTypeException("Brak zdefiniowanego filtru dla wybranego przedzia�u czasowego.");
                }

            }

            public boolean valid() {
                return this.to == null; //return this.from == null && this.to == null;
            }

            public boolean valid(Date lastLoginTime) {
                if (this.from != null && !this.from.before(lastLoginTime))
                    return false;
                if (this.to != null && !this.to.after(lastLoginTime))
                    return false;
                return true;
            }
        }
    }

    public interface ColumnSortLinkCreator {
        public String getLink(UsersColumn column, boolean ascending);
    }
}
