CREATE TABLE [dbo].[swd_uprawnienie](
	[KOD_UPR] [nvarchar](50) NOT NULL,
	[INT_ID] [nvarchar](20) NOT NULL,
	[REG_CODE] [nvarchar](16) NOT NULL,
	[OPIS] [nvarchar](1024) NOT NULL,
	[GRUPA] [nvarchar](50) NULL,
 CONSTRAINT [PK_swd_uprawnienie] PRIMARY KEY CLUSTERED 
(
	[KOD_UPR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[swd_uprawnienie]  WITH NOCHECK ADD  CONSTRAINT [FK_swd_uprawnienie_swd_rejestr1] FOREIGN KEY([REG_CODE])
REFERENCES [dbo].[swd_rejestr] ([REG_CODE])
GO

ALTER TABLE [dbo].[swd_uprawnienie] CHECK CONSTRAINT [FK_swd_uprawnienie_swd_rejestr1]
GO

--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'AB_ADMIN', N'WWW', N'AB', N'Baza AB - zarz�dzanie zawarto�ci�', N'AB')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'AB_READ', N'WWW', N'AB', N'Baza AB - przegl�danie', N'AB')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'AT_ADMIN', N'WWW', N'AT', N'Dostep do aplikacji administracyjnej', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BR_ADMIN', N'WWW', N'BR', N'Prawo do administracji rejestrem BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BR_EDITOR', N'WWW', N'BR', N'Prawo do modyfikacji rejestru BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BR_SEEKER', N'WWW', N'BR', N'Prawo do przegladania rejestru BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRF_EDITOR', N'WWW', N'ZORO', N'Modu� - F - edycja', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRF_SEEKER', N'WWW', N'ZORO', N'Modu� - F - szukanie', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRM_EDITOR', N'WWW', N'BR_MERCHANT', N'Prawo do edycji rejestru BR Merchant', N'BR_MERCHANT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRM_SEEKER', N'WWW', N'BR_MERCHANT', N'Prawo do przeszukiwania rejestru BR Merchant', N'BR_MERCHANT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRP_EDITOR', N'WWW', N'BRP', N'Modyfikowanie danych BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'BRP_SEEKER', N'WWW', N'BRP', N'Przeszukiwanie danych BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'CA_ADMIN', N'WWW', N'AT', N'Operator CA', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'CERT_REQ_ONLY', N'WWW', N'AT', N'Generowanie certyfikatu', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'GIODO_USER', N'WWW', N'GIODO', N'U�ytkownik aplikacji raportuj�cej dla GIODO', N'GIODO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'LOCAL_ADMIN', N'WWW', N'AT', N'Lokalny administrator', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'LOPLG_KGP_KOORDYNATY', N'WWW', N'LOPLG', N'Uprawnienia dla KGP ? dost�p do koordynat', N'LOPLG')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'LOPLG_KGP_PLIK', N'WWW', N'LOPLG', N'Uprawnienia dla operatora KGP ? zarz�dzanie plikiem', N'LOPLG')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'LOPLG_SEARCH', N'WWW', N'LOPLG', N'Przeszukiwanie bazy danych', N'LOPLG')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'loplgSearch', N'WWW', N'LOPLG', N'Dost�p do us�ugi sieciowej przeszukuj�cej baz�', N'LOPLG')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG2_READ', N'WWW', N'MIGDZ', N'Prawo do szukania w bazie MIGDZ z poziomu aplikacji MIG2', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG2_WRITE', N'WWW', N'MIGDZ', N'Prawo do modyfikacji MIGDZ z poziomu aplikacji MIG2', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_ADMIN', N'WWW', N'MIGDZ', N'Prawo do administrowania aplikacj� MIG3', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_BATCH', N'WWW', N'MIGDZ', N'Prawo do wykorzystywania formatek w interfejsie BATCH3', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_OEWIUDO', N'WWW', N'MIGDZ', N'OEWiUDO - dost�p z poziomu aplikacji', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_OEWIUDO_PESEL', N'WWW', N'MIGDZ', N'OEWiUDO lub PESEL - dost�p z poziomu aplikacji', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_OPERATOR', N'WWW', N'MIGDZ', N'PESEL - dost�p z poziomu aplikacji', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_PESEL', N'WWW', N'MIGDZ', N'PESEL - dost�p z poziomu aplikacji', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_READ', N'WWW', N'MIGDZ', N'Prawo do szukania w bazie MIGDZ', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_WRITE', N'WWW', N'MIGDZ', N'Prawo do modyfikacji MIGDZ', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_XML_QUERY', N'XML', N'MIGDZ', N'Prawo do wysy�ania pyta� XML w rejestrze MIGDZ', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_XML_READ', N'XML', N'MIGDZ', N'Prawo do odczytu plik�w XML w rejestrze MIGDZ', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'MIG3_XML_WRITE', N'XML', N'MIGDZ', N'Prawo do wysy�ania plik�w XML w rejestrze MIGDZ', N'MIGDZ')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'NO_CERT', N'WWW', N'AT', N'asss', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'POJ_ADMIN', N'WWW', N'POJAZDY', N'Administrator bazy policji', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'POJ_SEEKER', N'WWW', N'POJAZDY', N'Prawo do zapyta� do bazy Pojazd�w skradzionych', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'POLICJA', N'WWW', N'POJAZDY', N'Prawo do wysy�ania danych policyjnych', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'POLICJA_READ', N'WWW', N'POJAZDY', N'Prawo odbioru powiadomie� w rejestrze Pojazdy', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'REPL_XML_QUERY_BR', N'XML', N'BR', N'Pytania online do repliki rejestru BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'REPL_XML_QUERY_BRP', N'XML', N'BRP', N'Pytania online do repliki rejestru BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'REPL_XML_QUERY_POJAZDY', N'XML', N'POJAZDY', N'Pytania online do repliki rejestru pojazdy', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'RZS_SEEKER', N'WWW', N'RZS', N'Prawo do zadawania pytan do RZS', N'RZS')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'SWD_BOK', N'WWW', N'AT', N'Pracownik BOK', N'AT')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_FILE_BR', N'XML', N'BR', N'Prawo do odczytu plikow przyrostu BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_FILE_BRP', N'XML', N'BRP', N'Prawo do odczytu plik�w przyrostowych BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_QUERY_BR', N'XML', N'BR', N'Pytania wsadowe do rejestru BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_QUERY_BRP', N'XML', N'BRP', N'Pytania wsadowe do rejestru BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_QUERY_POJAZDY', N'XML', N'POJAZDY', N'Pytania wsadowe pojazdy', N'POJAZDY')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_REGISTERING_BR', N'XML', N'BR', N'Prawo do wysy�ania plik�w przyrostu BR', N'BR')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'XML_REGISTERING_BRP', N'XML', N'BRP', N'Prawo do wysylania plik�w XML BRP', N'BRP')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_ADMIN', N'WWW', N'ZORO', N'Administrator u�ytkownik�w systemu ZORO', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_GROUP', N'WWW', N'ZORO', N'ZORO - edycja grupy bank�w', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_MODIFY', N'WWW', N'ZORO', N'Modyfikacja danych systemu ZORO', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_READ', N'WWW', N'ZORO', N'Odczyt danych z systemu ZORO', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_XML_READ', N'XML', N'ZORO', N'Odczyt danych przyrostowych przez interfejs XML', N'ZORO')
--INSERT [dbo].[swd_uprawnienie] ([KOD_UPR], [INT_ID], [REG_CODE], [OPIS], [GRUPA]) VALUES (N'ZORO_XML_WRITE', N'XML', N'ZORO', N'Modyfikacje danych przez interfejs XML', N'ZORO')
