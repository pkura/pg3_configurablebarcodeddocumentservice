package pl.compan.docusafe.web.swd.management.sql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Damian on 09.01.14.
 */

@Entity
@Table(name = "SWD_MAIL_PARTS")
public class SwdMailParts implements Comparable{
    Integer id;
    String type;
    String name;
    String text;
    Integer attach;

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "TYPE" ,length = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "NAME" ,length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "TEXT")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "ATTACH")
    public Integer getAttach() {
        return attach;
    }

    public void setAttach(Integer attach) {
        this.attach = attach;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SwdMailParts that = (SwdMailParts) o;

        if (attach != null ? !attach.equals(that.attach) : that.attach != null) return false;
        if (!id.equals(that.id)) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (attach != null ? attach.hashCode() : 0);
        return result;
    }

    @Override
    /**
     * Order by type (string comparison) and next by id
     */
    public int compareTo(Object o) {
        if(o instanceof SwdMailParts){
            SwdMailParts other = (SwdMailParts) o;
            Integer orderByType = this.type.compareTo(other.type);

            if(orderByType == 0){
                return this.id.compareTo(other.id);
            }else{
                return orderByType;
            }

        }else{
            return 0;
        }

    }


}
