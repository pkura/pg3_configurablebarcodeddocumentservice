package pl.compan.docusafe.web.swd.management.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.io.File;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdSessionFactory {

    protected static final ThreadLocal<Session> sessions = new ThreadLocal<Session>();
    private static final String SWD_HIBERNATE_CONFIG = "swd.hibernate.cfg.xml";

    /**
     * To sobie trzeba pobrac z DSApi
     */
    protected static SessionFactory sessionFactory = null;
    protected static DataSource dataSource = null;

    public static void tryInit() throws Exception {
        if (sessionFactory == null) {
            synchronized (SwdSessionFactory.class) {
                if (sessionFactory == null) {
                    init();
                }
            }
        }
    }

    public static void init() throws Exception {
        javax.naming.Context initContext = new InitialContext();
        javax.naming.Context envContext = (javax.naming.Context) initContext.lookup("java:/comp/env");
        dataSource = (javax.sql.DataSource) envContext.lookup("jdbc/swd");

        Configuration cp = new Configuration();
        cp.configure(new File(Docusafe.getHome(), SWD_HIBERNATE_CONFIG));
        sessionFactory = cp.buildSessionFactory();
    }

    public static Session session() throws Exception {
        Session s = sessions.get();
        // Open a new Session, if this Thread has none yet
        if (s == null) {
            tryInit();
            if (sessionFactory == null)
                throw new Exception("SessionFactory==null");
            if (dataSource == null)
                throw new Exception("DataSource==null");
            s = sessionFactory.openSession(dataSource.getConnection());
            sessions.set(s);
        }
        return s;
    }

    public static void closeSession() throws Exception {
        Session s = sessions.get();
        if (s != null)
            s.close().close();
        sessions.set(null);
    }

    public static void closeSessionQuietly() {
        try {
            closeSession();
        } catch (Exception e) {
            LoggerFactory.getLogger(SwdSessionFactory.class).error(e.getMessage(), e);
        }
    }
}
