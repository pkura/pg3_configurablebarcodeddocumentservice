package pl.compan.docusafe.web.swd.management.sql;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportWhereColumn implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(ReportWhereColumn.class);
    private String alias;
    private Integer idReport;
    private String type;

    public static List<ReportWhereColumn> search(int reportId) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(ReportWhereColumn.class);

            criteria.add(Restrictions.eq("idReport", reportId));

            return criteria.list();
//            int totalCount = results.size();
//            return new SearchResultsAdapter<ReportWhereColumn>(results, 0, totalCount, ReportWhereColumn.class);
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getIdReport() {
        return idReport;
    }

    public void setIdReport(Integer idReport) {
        this.idReport = idReport;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
