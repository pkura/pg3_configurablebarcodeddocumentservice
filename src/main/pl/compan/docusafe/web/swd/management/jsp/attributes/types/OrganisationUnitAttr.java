package pl.compan.docusafe.web.swd.management.jsp.attributes.types;

import pl.compan.docusafe.web.jsp.component.JspFieldType;
import pl.zbp.users.OrganisationUnit;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum OrganisationUnitAttr {
    ID("Id klienta", JspFieldType.TEXT, false),
    NAME("Nazwa", JspFieldType.TEXT, true),
    UNIT_CODE("Kod instytucji", JspFieldType.TEXT),
    KIND("Rodzaj", JspFieldType.SELECT, true),
    SHORT_NAME("Nazwa skr�cona", JspFieldType.TEXT),
    STREET("Ulica", JspFieldType.TEXT),
    ADDRESS("Miasto", JspFieldType.TEXT),
    POSTAL_CODE("Kod pocztowy", JspFieldType.TEXT),
    CONTACT_PERSON("Osoba kontaktowa", JspFieldType.TEXT),
    EMAIL("Email", JspFieldType.TEXT),
    NOTES("Uwagi", JspFieldType.TEXT);

    public final String title;
    public final JspFieldType type;
    public final boolean editable;
    public final boolean required;

    public static final boolean DEFAULT_REQUIRED = false;
    public static final boolean DEFAULT_EDITABLE = true;

    OrganisationUnitAttr(String title, JspFieldType type) {
        this(title, type, DEFAULT_EDITABLE, DEFAULT_REQUIRED);
    }
    OrganisationUnitAttr(String title, JspFieldType type, boolean editable_required) {
        this(title, type, editable_required, editable_required);
    }
    OrganisationUnitAttr(String title, JspFieldType type, boolean editable, boolean required) {
        this.title = title;
        this.type = type;
        this.editable = editable;
        this.required = required;
    }

    public String getTitle() {
        return title;
    }

    public JspFieldType getType() {
        return type;
    }

    public boolean isEditable() {
        return editable;
    }

    public Object getValueFrom(OrganisationUnit unit) {
        switch (this) {
            case ID:
                return unit.getKlientId();
            case NAME:
                return unit.getName();
            case UNIT_CODE:
                return unit.getUnitCode();
            case KIND:
                return unit.getKind();
            case SHORT_NAME:
                return unit.getShortName();
            case STREET:
                return unit.getStreet();
            case ADDRESS:
                return unit.getAddress();
            case POSTAL_CODE:
                return unit.getPostalCode();
            case CONTACT_PERSON:
                return unit.getContactPerson();
            case EMAIL:
                return unit.getEmail();
            case NOTES:
                return unit.getNotes();
            default:
                throw new IllegalArgumentException("Brak nazwy pola dla OrganisationUnitAttr: " + this.name());
        }
    }
}
