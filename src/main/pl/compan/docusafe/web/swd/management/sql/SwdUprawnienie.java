package pl.compan.docusafe.web.swd.management.sql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Damian on 07.01.14.
 */

@Entity
@Table(name="swd_uprawnienie")
public class SwdUprawnienie {
    String kodUpr;
    String intID;
    String regCode;
    String opis;
    String grupa;

    @Id
    @Column(name="KOD_UPR")
    public String getKodUpr() {
        return kodUpr;
    }

    public void setKodUpr(String kodUpr) {
        this.kodUpr = kodUpr;
    }

    @Column(name="INT_ID")
    public String getIntID() {
        return intID;
    }


    public void setIntID(String intID) {
        this.intID = intID;
    }

    @Column(name="REG_CODE")
    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        regCode = regCode;
    }

    @Column(name="OPIS")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        opis = opis;
    }

    @Column(name="GRUPA")
    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SwdUprawnienie that = (SwdUprawnienie) o;

        if (grupa != null ? !grupa.equals(that.grupa) : that.grupa != null) return false;
        if (intID != null ? !intID.equals(that.intID) : that.intID != null) return false;
        if (kodUpr != null ? !kodUpr.equals(that.kodUpr) : that.kodUpr != null) return false;
        if (opis != null ? !opis.equals(that.opis) : that.opis != null) return false;
        if (regCode != null ? !regCode.equals(that.regCode) : that.regCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = kodUpr != null ? kodUpr.hashCode() : 0;
        result = 31 * result + (intID != null ? intID.hashCode() : 0);
        result = 31 * result + (regCode != null ? regCode.hashCode() : 0);
        result = 31 * result + (opis != null ? opis.hashCode() : 0);
        result = 31 * result + (grupa != null ? grupa.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return kodUpr ;
    }
}
