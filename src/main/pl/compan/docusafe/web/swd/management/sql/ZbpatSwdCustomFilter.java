package pl.compan.docusafe.web.swd.management.sql;

import javax.persistence.*;

/**
 * Created by Damian on 13.01.14.
 */

@Entity
@Table(name="SWD_CUSTOM_FILTER")
public class ZbpatSwdCustomFilter {


    private Integer id;
 //   private FilterCn cn;
    private String cn;
    private String title;
    private String whereClause;

    @Id
    @Column(name="ID")
    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="CN")
    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

  /*  @Enumerated(EnumType.STRING)
    public FilterCn getCn() {
        return cn;
    }

    public void setCn(FilterCn cn) {
        this.cn = cn;
    }
*/

    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "WHERE_CLAUSE")
    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }


}
