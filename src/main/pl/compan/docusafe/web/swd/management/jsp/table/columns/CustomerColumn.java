package pl.compan.docusafe.web.swd.management.jsp.table.columns;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspFieldType;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.zbp.users.OrganisationUnit;

import javax.validation.UnexpectedTypeException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum CustomerColumn {
    LP("Lp.", JspTableColumn.DEFAULT_SMALL_WIDTH, null, false),
    ID("Id", JspTableColumn.DEFAULT_SMALL_WIDTH, JspFieldType.TEXT),
    KOD("Kod", JspTableColumn.DEFAULT_SMALL_WIDTH, JspFieldType.TEXT),
    RODZAJ("Rodzaj", JspTableColumn.DEFAULT_SMALL_WIDTH, JspFieldType.TEXT),
    NAZWA("Nazwa", null, JspFieldType.TEXT);

    
    public final String title;
    public final Integer width;
//    public final JspFieldType filterType;
    public final boolean sortEnabled;

    private CustomerColumn(String title, Integer width, JspFieldType filterType) {
        this(title, width, filterType, true);
    }
    private CustomerColumn(String title, Integer width, JspFieldType filterType, boolean sortEnabled) {
        this.title = title;
        this.width = width;// == null ? "" : width.toString();
//        this.filterType = filterType;
        this.sortEnabled = sortEnabled;
    }

//    public static CustomerColumn findByTitle(String title) {
//        for (CustomerColumn cc : CustomerColumn.values())
//            if (cc.title.equalsIgnoreCase(title))
//                return cc;
//        throw new IllegalArgumentException("Nie ma typu CustomerColumn z tytu�em: " + title);
//    }
    public static CustomerColumn findByName(String name, CustomerColumn defaultColumn) {
        if (StringUtils.isNotEmpty(name))
            return CustomerColumn.valueOf(name);
//            for (CustomerColumn cc : CustomerColumn.values())
//                if (cc.name().equalsIgnoreCase(name))
//                    return cc;
        return defaultColumn;
    }

    public Object getData(OrganisationUnit customer) {
        switch (this) {
            case ID:
                return customer.getKlientId();
            case KOD:
                return customer.getUnitCode();
            case RODZAJ:
                return customer.getKind();
            case NAZWA:
                return customer.getName();
            case LP:
            default:
                throw new UnexpectedTypeException("Brak metody do pobrania danych dla CustomerColumn: " + this.name());
        }
    }

    public String getTitle() {
        return title;
    }

    public Integer getWidth() {
        return width;
    }

//    public JspFieldType getFilterType() {
//        return filterType;
//    }

    public boolean isSortEnabled() {
        return sortEnabled;
    }
}
