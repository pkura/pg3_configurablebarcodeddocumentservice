package pl.compan.docusafe.web.swd.management;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.swd.management.jsp.table.ReportTable;
import pl.compan.docusafe.web.swd.management.sql.ReportKind;
import pl.compan.docusafe.web.swd.management.sql.ReportSelectColumn;
import pl.compan.docusafe.web.swd.management.sql.ReportWhereColumn;
import pl.compan.docusafe.web.swd.management.utils.*;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class BooksListAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger(BooksListAction.class);
    public String ascending;
    public String orderBy;
    private ReportTable jspReport = new ReportTable("Wynik zestawienia:");
    private JspAttribute jspReportKinds;
    private String jspReportKindsField;

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String getAscending() {
        return ascending;
    }

    public void setAscending(String ascending) {
        this.ascending = ascending;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public ReportTable getJspReport() {
        return jspReport;
    }

    public void setJspReport(ReportTable jspReport) {
        this.jspReport = jspReport;
    }

    public boolean isReportKindSelect(String jspId) {
        return StringUtils.isNotEmpty(jspReportKindsField) && jspReportKindsField.equalsIgnoreCase(jspId);
    }

    public JspAttribute getJspReportKinds() {
        return jspReportKinds;
    }

    public void setJspReportKinds(JspAttribute jspReportKinds) {
        this.jspReportKinds = jspReportKinds;
    }

    public String getJspReportKindsField() {
        return jspReportKindsField;
    }

    public void setJspReportKindsField(String jspReportKindsField) {
        this.jspReportKindsField = jspReportKindsField;
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            List<ReportKind> kinds = ReportKind.search();
            List<ReportSelectColumn> selects = ReportSelectColumn.search(1);
            List<ReportWhereColumn> wheres = ReportWhereColumn.search(1);

            List<Map<String, Object>> results = ReportQueryMaker.search(kinds.get(0), selects, new ArrayList<ReportRestriction>());

            jspReportKinds = JspAttribute.Factory.createSelectField("jspReportKindsField", "Zestawienie:", true, getReportKindsSelectField(kinds));

            jspReport.init(selects);
            jspReport.setJspData(results, orderBy, ascending);

            System.out.println("BooksListAction - TEST");

        }

        private JspAttribute.SelectField getReportKindsSelectField(List<ReportKind> kinds) {
            List<JspAttribute.SelectItem> items = new ArrayList<JspAttribute.SelectItem>();
            items.add(new JspAttribute.SelectItem(JspAttribute.SelectField.DEFAULT_SELECTED_ID, ""));
            for (ReportKind kind : kinds) {
                JspAttribute.SelectItem item = new JspAttribute.SelectItem(kind.getId().toString(), kind.getTitle());
                items.add(item);
            }
            JspAttribute.SelectField selectField = new JspAttribute.SelectField(items);
            return selectField;
        }
    }
}
