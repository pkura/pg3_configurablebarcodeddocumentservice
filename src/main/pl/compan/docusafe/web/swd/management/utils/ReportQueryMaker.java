package pl.compan.docusafe.web.swd.management.utils;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.sql.ReportKind;
import pl.compan.docusafe.web.swd.management.sql.ReportSelectColumn;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportQueryMaker {

    private static final Logger log = LoggerFactory.getLogger(ReportQueryMaker.class);

    public static List<Map<String, Object>> search(ReportKind kind, List<ReportSelectColumn> columns, List<ReportRestriction> restrictions) {
        PreparedStatement ps = null;
        try {
            String sql = buildSql(kind, restrictions);
            ps = DSApi.context().prepareStatement(sql);

            int idx = 0;
            for (ReportRestriction r : restrictions) {
                ++idx;
                r.getDataType().setRestriction(ps, idx, r.getValue());
            }

            ResultSet rs = ps.executeQuery();
            List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

//            Map<Integer,String> columnIdxs = new HashMap<Integer,String>();
//            for (ReportSelectColumn col : columns){
//                //ReportDataType type = col.getTypeEnum();
//                int colIdx = rs.findColumn(col.getColumnName());
//                columnIdxs.put(colIdx,col.get);
//            }

            while (rs.next()) {
                Map<String, Object> bean = new HashMap<String, Object>();
                for (ReportSelectColumn col : columns)
                    bean.put(col.getColumnCode(), col.getTypeEnum().getData(rs, col.getColumnName()));
                results.add(bean);
            }

            rs.close();
            return results;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ArrayList<Map<String, Object>>();
        } finally {
            if (ps != null)
                DSApi.context().closeStatement(ps);
        }
    }

    private static String buildSql(ReportKind kind, List<ReportRestriction> restrictions) {
        StringBuilder builder = new StringBuilder();

        builder.append(kind.getSelectClause());

        if (isSqlEmpty(kind.getWhereClause()) || restrictions.size() > 0) {
            builder.append(" WHERE ");
            builder.append(kind.getWhereClause());

            for (ReportRestriction r : restrictions) {
                builder.append(r.getRestrictionSqlClause());
            }
        }

        if (isSqlEmpty(kind.getWhereClause())) {
            builder.append(" GROUP BY ");
            builder.append(kind.getGroupByClause());
        }

        return builder.toString();
    }

    private static boolean isSqlEmpty (String value){
        return StringUtils.isNotEmpty(value) && "null".equalsIgnoreCase(value);
    }

}
