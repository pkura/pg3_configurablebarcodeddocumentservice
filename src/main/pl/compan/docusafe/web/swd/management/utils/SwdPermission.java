package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.swd.management.*;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdPermission {

    public enum JspType {
        USERS_LIST("UL"),
        USER_VIEW("USER"),
        ORGANISATION_UNIT("OU"),
        ORGANISATION_UNIT_LIST("OULIST"),
        PASSED_PASSWORD("HDP"),
        PASSED_PASSWORD_LIST("HDPLIST");

        static {
            //zaleznosci: skad mozna przejsc na jakiego jsp
            USERS_LIST.setSubJsps(USER_VIEW);
            USER_VIEW.setSubJsps(ORGANISATION_UNIT);
            ORGANISATION_UNIT.setSubJsps(USER_VIEW);
            ORGANISATION_UNIT_LIST.setSubJsps(ORGANISATION_UNIT, USER_VIEW);
            PASSED_PASSWORD.setSubJsps(ORGANISATION_UNIT, USER_VIEW);
            PASSED_PASSWORD_LIST.setSubJsps(PASSED_PASSWORD);
        }

        public final String code;
        private JspType [] subJsps;

        private void setSubJsps(JspType... subJsps){
            this.subJsps = subJsps;
        }
        JspType(String code, JspType... subJsps) {
            this.code = code;
            this.subJsps = subJsps;
        }

        public static JspType getJspType(String className){

            if (OrganisationUnitAction.class.getName().equalsIgnoreCase(className)) {
                return ORGANISATION_UNIT;
            }
            else if (OrganisationUnitListAction.class.getName().equalsIgnoreCase(className)) {
                return ORGANISATION_UNIT_LIST;
            }
            else if (PassedPasswordAction.class.getName().equalsIgnoreCase(className)) {
                return PASSED_PASSWORD;
            }
            else if (PassedPasswordListAction.class.getName().equalsIgnoreCase(className)) {
                return PASSED_PASSWORD_LIST;
            }
            else if (UsersListAction.class.getName().equalsIgnoreCase(className)) {
                return USERS_LIST;
            }
            else if (UserViewAction.class.getName().equalsIgnoreCase(className)) {
                return USER_VIEW;
            }

            throw new IllegalArgumentException("Nie mo�na przyporz�dkowa� jsp dla wskazanej nazwy");
        }

//        /**W��cznie z podanym*/
//        public static Set<JspType> getRecursiveSubJsps(JspType parent){
//            Set<JspType> parentSubs = new HashSet<JspType>();
//            Set<JspType> subsToCheck = new HashSet<JspType>();
//            subsToCheck.addAll(Arrays.asList(parent.subJsps));
//            while (!subsToCheck.isEmpty()){
//                JspType sub = subsToCheck.iterator().next();
//                parentSubs.add(sub);
//                subsToCheck.remove(sub);
//
//                for (JspType subSub : sub.subJsps){
//                    if (!parentSubs.contains(subSub) && !subsToCheck.contains(subSub)){
//                        subsToCheck.add(subSub);
//                    }
//                }
//            }
//            return parentSubs;
//        }
    }

    /**
     * Nazwa musi si� zaczyna� od {@link pl.compan.docusafe.web.swd.management.utils.SwdPermission.JspType}.name()
     */
    public enum FieldsGroup {
        OULIST("jspComponentTable1", "jspComponentTable2"),
        OULIST_BUTTON_CHOOSE(JspPermissionManager.JspFieldKind.FIELD,"BUTTON_CHOOSE"),
        OULIST_BUTTON_CREATE_OUNIT(JspPermissionManager.JspFieldKind.FIELD,"BUTTON_CREATE_OUNIT"),

        OU_DANE_ORG("jspAttributes"),
        OU_SPOSOBY_DOSTEPU("jspCheckboxes"),
        OU_ILOSCI("jspQuantity"),
        OU_UZYTKOWNICY_WG_ROLI("jspTechAdminsTable", "jspCoordinatorsTable"),
        OU_UZYTKOWNICY("jspUsersTable"),
        OU_BUTTON_SAVE(JspPermissionManager.JspFieldKind.FIELD,"BUTTON_SAVE"),
        OU_BUTTON_DOCUMENTS(JspPermissionManager.JspFieldKind.FIELD,"BUTTON_DOCUMENTS"),
        OU_BUTTON_CREATE_USER(JspPermissionManager.JspFieldKind.FIELD,"BUTTON_CREATE_USER"),

        HDPLIST("jspPasswords", "jspImmediatePassedPasswords"),

        HDP_EDIT("EDIT"),

        USER_SAVE("USER_SAVE"),
        USER_PROFILE("USER_PROFILE"),
        USER_ACCESS_DATA("USER_ACCESS_DATA"),
        USER_ROLES("USER_ROLES"),
        USER_RIGHTS("USER_RIGHTS"),
        USER_CERTS("USER_CERTS"),
        USER_MAIL("USER_MAIL");

        public final JspPermissionManager.JspFieldKind fieldKind;
        public final String[] fieldsNames;

        FieldsGroup(String... fieldsNames) {
            this(JspPermissionManager.JspFieldKind.COMPONENT, fieldsNames);
        }
        FieldsGroup(JspPermissionManager.JspFieldKind fieldKind, String... fieldsNames) {
            this.fieldKind = fieldKind;
            this.fieldsNames = fieldsNames;
        }

        public static Set<FieldsGroup> getFieldsGroupsForJsp(JspType jspType) {
            Set<FieldsGroup> fieldsGroups = new HashSet<FieldsGroup>();
            Pattern pattern = Pattern.compile("^" + jspType.code + "_.*$");
            for (FieldsGroup fg : FieldsGroup.values()) {
                if (pattern.matcher(fg.name()).find())
                    fieldsGroups.add(fg);
            }
            return fieldsGroups;
        }
    }

    public enum PermissionType {
        READ, EDIT, SHOW;

        public static PermissionType getPermissionType(Permission permission) {
            String name = permission.name();
            for (PermissionType type : PermissionType.values()) {
                if (Pattern.compile("^.*_" + type.name() + "$").matcher(name).find())
                    return type;
            }
            throw new IllegalArgumentException("Uprawnienie nie konczy sie zadna znana koncowka okreslajaca typ");
        }
    }

    // ((.*)_[^_]*),
    // $1 (FieldsGroup.$2),

    /**
     * Nazwa musi si� ko�czy� na {@link pl.compan.docusafe.web.swd.management.utils.SwdPermission.PermissionType}.name()
     */
    public enum Permission {
        OULIST_READ(FieldsGroup.OULIST),
        OULIST_BUTTON_CHOOSE_READ(FieldsGroup.OULIST_BUTTON_CHOOSE),
        OULIST_BUTTON_CREATE_OUNIT_READ(FieldsGroup.OULIST_BUTTON_CREATE_OUNIT),
        OU_DANE_ORG_READ(FieldsGroup.OU_DANE_ORG),
        OU_SPOSOBY_DOSTEPU_READ(FieldsGroup.OU_SPOSOBY_DOSTEPU),
        OU_ILOSCI_READ(FieldsGroup.OU_ILOSCI),
        OU_UZYTKOWNICY_WG_ROLI_READ(FieldsGroup.OU_UZYTKOWNICY_WG_ROLI),
        OU_UZYTKOWNICY_READ(FieldsGroup.OU_UZYTKOWNICY),
        OU_BUTTON_SAVE_READ(FieldsGroup.OU_BUTTON_SAVE),
        OU_BUTTON_DOCUMENTS_READ(FieldsGroup.OU_BUTTON_DOCUMENTS),
        OU_BUTTON_CREATE_USER_READ(FieldsGroup.OU_BUTTON_CREATE_USER),
        HDPLIST_READ(FieldsGroup.HDPLIST),
        HDP_EDIT_READ(FieldsGroup.HDP_EDIT),

        OULIST_BUTTON_CHOOSE_EDIT(FieldsGroup.OULIST_BUTTON_CHOOSE),
        OULIST_BUTTON_CREATE_OUNIT_EDIT(FieldsGroup.OULIST_BUTTON_CREATE_OUNIT),
        OU_DANE_ORG_EDIT(FieldsGroup.OU_DANE_ORG),
        OU_SPOSOBY_DOSTEPU_EDIT(FieldsGroup.OU_SPOSOBY_DOSTEPU),
        OU_BUTTON_SAVE_EDIT(FieldsGroup.OU_BUTTON_SAVE),
        OU_BUTTON_DOCUMENTS_EDIT(FieldsGroup.OU_BUTTON_DOCUMENTS),
        OU_BUTTON_CREATE_USER_EDIT(FieldsGroup.OU_BUTTON_CREATE_USER),
        HDP_EDIT_EDIT(FieldsGroup.HDP_EDIT),

        //____________________________USER_________________________________________

        //_______USER SHOW___________
        //Brak implementacji dla tego typu uprawnie�

        //_______USER READ______
        USER_PROFILE_READ(FieldsGroup.USER_PROFILE),
        USER_ACCESS_DATA_READ(FieldsGroup.USER_ACCESS_DATA),
        USER_ROLES_READ(FieldsGroup.USER_ROLES),
        USER_RIGHTS_READ(FieldsGroup.USER_RIGHTS),
        USER_CERTS_READ(FieldsGroup.USER_CERTS),


        //_______USER EDIT______

        /**
         * USER_SAVE_EDIT - If not enabled then save button is hidden
         */
        USER_SAVE_EDIT(FieldsGroup.USER_SAVE),
        USER_PROFILE_EDIT(FieldsGroup.USER_PROFILE),
        USER_ACCESS_DATA_EDIT(FieldsGroup.USER_ACCESS_DATA),
        USER_ROLES_EDIT(FieldsGroup.USER_ROLES),
        USER_RIGHTS_EDIT(FieldsGroup.USER_RIGHTS),
        USER_CERTS_EDIT(FieldsGroup.USER_CERTS),
        /**
         * USER_MAIL_EDIT - show mail area
         */
        USER_MAIL_EDIT(FieldsGroup.USER_MAIL);
        //TODO DEAKTYWACJA





        public final FieldsGroup fieldsGroup;

        Permission(FieldsGroup fieldsGroup) {
            this.fieldsGroup = fieldsGroup;
        }

        public static Set<Permission> getPermissionByTypeForJsp(PermissionType permissionType, JspType jspType) {
            Set<Permission> permissions = new HashSet<Permission>();
            Pattern pattern = Pattern.compile("^" + jspType.code + "_.*_" + permissionType.name() + "$");
            for (Permission p : Permission.values()) {
                if (pattern.matcher(p.name()).find())
                    permissions.add(p);
            }
            return permissions;
        }
//        public static Set<Permission> getPermissionByTypeForRecursiveSubJsp(PermissionType permissionType, JspType parent) {
//            Set<Permission> permissions = new HashSet<Permission>();
//            Set<JspType> subJsps = JspType.getRecursiveSubJsps(parent);
//            for (JspType jsp : subJsps)
//                permissions.addAll(getPermissionByTypeForRecursiveSubJsp(permissionType, jsp));
//            return permissions;
//        }
    }
}
