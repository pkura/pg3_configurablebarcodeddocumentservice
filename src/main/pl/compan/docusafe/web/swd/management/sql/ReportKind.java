package pl.compan.docusafe.web.swd.management.sql;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ReportKind implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(ReportKind.class);
    private Integer id;
    private String title;
    private String selectClause;
    private String whereClause;
    private String groupByClause;

    public static List<ReportKind> search() {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(ReportKind.class);
            List<ReportKind> results = criteria.list();
            return results;
//            int totalCount = results.size();
//            return new SearchResultsAdapter<ReportKind>(results, 0, totalCount, ReportKind.class);
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSelectClause() {
        return selectClause;
    }

    public void setSelectClause(String selectClause) {
        this.selectClause = selectClause;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    public String getGroupByClause() {
        return groupByClause;
    }

    public void setGroupByClause(String groupByClause) {
        this.groupByClause = groupByClause;
    }
}
