package pl.compan.docusafe.web.swd.management.jsp.attributes.types;

import pl.compan.docusafe.web.jsp.component.JspFieldType;
import pl.zbp.users.User;

import javax.validation.UnexpectedTypeException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum UsersAttr {
    ID("Id u�ytkownika"),
    LOGIN("Login"),

    NAZWISKO("Nazwisko"),
    IMIE("Imi�"),

    STANOWISKO("Stanowisko"),

    ADRES_ULICA("Adres ulica"),
    ADRES_MIASTO("Adres miasto"),
    ADRES_KOD_POCZTOWY("Adres kod pocztowy"),

    EMAIL("Email"),
    TELEFON("Telefon"),
    FAX("Fax");
    
    public final String title;
    public final JspFieldType type;
    public final boolean editable;

    UsersAttr(String title) {
        this(title, JspFieldType.TEXT, false);
    }

    UsersAttr(String title, JspFieldType type, boolean editable) {
        this.title = title;
        this.type = type;
        this.editable = editable;
    }

    public String getTitle() {
        return title;
    }

    public JspFieldType getType() {
        return type;
    }

    public boolean isEditable() {
        return editable;
    }

    public Object getValueFrom(User user) {
        switch(this){
            case ID:
                return user.getUserID();
            case LOGIN:
                return user.getLogin();
            case NAZWISKO:
                return user.getLastName();
            case IMIE:
                return user.getFirstName();
            case STANOWISKO:
                return user.getPosition();
            case ADRES_ULICA:
                return user.getAddress(); // adres zawiera ca�o��: ulic�, miasto...
            case ADRES_MIASTO:
                return user.getCity();
            case ADRES_KOD_POCZTOWY:
                return user.getPostCode();
            case EMAIL:
                return user.getEMail();
            case TELEFON:
                return user.getPhone();
            case FAX:
                return user.getFax();
            default:
                throw new UnexpectedTypeException("Brak metody do pobrania danych dla CustomerColumn: " + this.name());
        }
    }
}
