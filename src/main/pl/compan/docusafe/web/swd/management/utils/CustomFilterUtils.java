package pl.compan.docusafe.web.swd.management.utils;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.sql.FilterCn;
import pl.compan.docusafe.web.swd.management.sql.ZbpatSwdCustomFilter;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;

import java.util.List;

/**
 * Created by Damian on 10.01.14.
 */
public class CustomFilterUtils {
    private static Logger log = LoggerFactory.getLogger(CustomFilterUtils.class);

    public static List<ZbpatSwdCustomFilter> getCustomFiltersForCn(FilterCn cn){
        List<ZbpatSwdCustomFilter> resultList = null;

        try {
            Criteria criteria = DSApi.context().session().createCriteria(ZbpatSwdCustomFilter.class);
            criteria.add(Restrictions.eq("cn",cn.name()));
            resultList = criteria.list();

        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }

        return resultList;
    }

    public static ZbpatSwdCustomFilter getFilterById(Integer id){
        ZbpatSwdCustomFilter result = null;

        try {
            Criteria criteria = DSApi.context().session().createCriteria(ZbpatSwdCustomFilter.class);
            criteria.add(Restrictions.idEq(id));
            result = (ZbpatSwdCustomFilter) criteria.uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }

        return result;
    }

    public static List<User> findUsersByCustomFilter(ZbpatSwdCustomFilter customFilter){
        return findUsersByCustomWhereClause(customFilter.getWhereClause());
    }

    public static List<User> findUsersByCustomWhereClause(String whereClause){
        List<User> users = null;
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        try {

            log.error(CustomFilterUtils.class.getName() + ": findUsersByCustomWhereClause: wyszukiwanie mo�e nie zadzia�ac");

            Criteria criteria = factory.currentSession().createCriteria(User.class);
            criteria.add(Restrictions.sqlRestriction(whereClause));
            users = criteria.list();
        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }
        return users;
    }

    public static List<OrganisationUnit> findOUByCustomFilter(ZbpatSwdCustomFilter customFilter){
        return findOUByCustomWhereClause(customFilter.getWhereClause());
    }

    public static List<OrganisationUnit> findOUByCustomWhereClause(String whereClause){
        List<OrganisationUnit> ou = null;
        SwdUserManagementFactory factory = new SwdUserManagementFactory();
        try {
            Criteria criteria = factory.currentSession().createCriteria(OrganisationUnit.class);
            criteria.add(Restrictions.sqlRestriction(whereClause));
            ou = criteria.list();
        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }
        return ou;
    }

}
