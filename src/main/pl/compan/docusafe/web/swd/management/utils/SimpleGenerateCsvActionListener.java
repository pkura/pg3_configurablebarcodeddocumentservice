package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.jsp.Filter;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class SimpleGenerateCsvActionListener<T> extends GenerateCsvActionListener {

    public SimpleGenerateCsvActionListener() {
    }


    public File generateCsvForTableData(String title) throws IOException {
        File file = File.createTempFile(title, ".csv");
        CsvDumper dumper = new CsvDumper();
        dumper.setFieldSeparator(",");
        dumper.openFile(file);

        JspComponentTable<T> table = getJspComponentTable();

        dumper.newLine();

        for (JspTableColumn c : table.getJspColumns()) {
            dumper.addText(c.getTitle());
        }

        dumper.dumpLine();

        for (T u : table.getJspData()) {

            dumper.newLine();

            for (JspTableColumn c : table.getJspColumns())
                dumper.addText(table.getData(u, c));
            dumper.dumpLine();
        }

        dumper.closeFile();

        return file;
    }

    public String getFilterTitlePart(List<Filter> filters){
        if (filters == null || filters.isEmpty())
            return "";
        StringBuilder builder = new StringBuilder();
        for (Filter filter : filters) {
            if (filter.isActive()) {
                Object filterValue = filter.getFilterValue();
                String name = filterValue != null ? filterValue.toString() : "";
                if (name.length() > 0)
                    builder.append("-").append(name);
            }
        }
        return builder.toString();
    }

    public abstract JspComponentTable<T> getJspComponentTable();
}