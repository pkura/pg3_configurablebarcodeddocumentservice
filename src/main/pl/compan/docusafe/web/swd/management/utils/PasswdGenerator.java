package pl.compan.docusafe.web.swd.management.utils;

import org.apache.commons.lang.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class PasswdGenerator {
    private Integer minLen;
    private Integer maxLen;
    private Map<CharsGroup, Integer> restrictions = new HashMap<CharsGroup, Integer>();

    public PasswdGenerator() {
        this(null, null);
    }

    public PasswdGenerator(Integer length) {
        this(length, length);
    }

    public PasswdGenerator(Integer minLen, Integer maxLen) {
        this.minLen = minLen;
        this.maxLen = maxLen;
    }

    public PasswdGenerator setRestriction(CharsGroup charsGroup, int count) {
        if (count < 0)
            throw new IllegalArgumentException("Liczba znak�w dla grupy znak�w " + charsGroup.name() + " musi by� > 0");
        restrictions.put(charsGroup, count);
        return this;
    }

    public String generate() {
        return generatePasswd(minLen, maxLen, restrictions);
    }

    public enum CharsGroup {
        ALPHA_CAPS("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        ALPHA("abcdefghijklmnopqrstuvwxyz"),
        NUM("0123456789"),
        SPL_CHARS("!@#$%^&*_=+-/");

        public final String chars;

        private CharsGroup(String chars) {
            this.chars = chars;
        }
    }

    public static String generatePasswd(Map<CharsGroup, Integer> passwdRestrictions) {
        return generatePasswd(null, null, passwdRestrictions);
    }

    public static String generatePasswd(Integer minLen, Integer maxLen, Map<CharsGroup, Integer> passwdRestrictions) {

        if (minLen == null){
            minLen = 0;
            for (Map.Entry<CharsGroup, Integer> restriction : passwdRestrictions.entrySet())
                minLen += restriction.getValue();
        }

        if (maxLen == null)
            maxLen = minLen;
        else if (minLen > maxLen)
            throw new IllegalArgumentException("Min. d�ugo�� > maks. d�ugo��!");

        Random rnd = new Random();
        int len = rnd.nextInt(maxLen - minLen + 1) + minLen;

        StringBuilder passwdChars = new StringBuilder();
        for (Map.Entry<CharsGroup, Integer> chars : passwdRestrictions.entrySet()) {
            String randomChars = RandomStringUtils.random(chars.getValue(), chars.getKey().chars);
            passwdChars.append(randomChars);
        }

        int lengthLeft = len - passwdChars.length();
        if (lengthLeft < 0)
            throw new IllegalArgumentException("D�ugo�� has�a jest mniejsza od minimalnej d�ugo�ci wg. kryteri�w");
        if (lengthLeft > 0) {
            StringBuilder allChars = new StringBuilder();
            for (CharsGroup pc : CharsGroup.values())
                allChars.append(pc.chars);
            String randomChars = RandomStringUtils.random(lengthLeft, allChars.toString());
            passwdChars.append(randomChars);
        }

        return mixChars(passwdChars);
    }

    private static String mixChars(StringBuilder chars) {

        StringBuilder passwdChars = new StringBuilder();

        Random rnd = new Random();
        int charsLength;
        while ((charsLength = chars.length()) > 0) {
            int idx = rnd.nextInt(charsLength);
            passwdChars.append(chars.charAt(idx));
            chars.deleteCharAt(idx);
        }

        return passwdChars.toString();
    }
}
