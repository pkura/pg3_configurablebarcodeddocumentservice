package pl.compan.docusafe.web.swd.management.mapping;

import pl.compan.docusafe.core.DSApi;

/**
 * Created by Damian on 20.01.14.
 */
public class DocumentUserChangeMapping {
    private UpdateUserMapping before;
    private UpdateUserMapping after;
    private Long documentId;
    private Integer userId;

    public DocumentUserChangeMapping(Long documentId, Integer userId) {
        before = new UpdateUserMapping();
        after = new UpdateUserMapping();
        this.documentId = documentId;
        this.userId = userId;
    }


    public void store() {

    }


    //_________GETTERS & SETTERS______________

    public UpdateUserMapping getBefore() {
        return before;
    }

    public void setBefore(UpdateUserMapping before) {
        this.before = before;
    }

    public UpdateUserMapping getAfter() {
        return after;
    }

    public void setAfter(UpdateUserMapping after) {
        this.after = after;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


}
