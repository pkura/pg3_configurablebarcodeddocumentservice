package pl.compan.docusafe.web.swd.management.jsp.table.columns;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.zbp.users.User;

import javax.validation.UnexpectedTypeException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum UsersSimpleColumn {
    NAZWISKO("Nazwisko", JspTableColumn.DEFAULT_SMALL_WIDTH),
    IMIE("Imi�", JspTableColumn.DEFAULT_SMALL_WIDTH),
    SYSTEMY_SWD("Systemy SWD",3 * JspTableColumn.DEFAULT_DOUBLE_SMALL_WIDTH);

    public final String title;
    public final Integer width;
    public final boolean sortEnabled;

    UsersSimpleColumn(String title, Integer width) {
        this(title, width, false);
    }

    UsersSimpleColumn(String title, Integer width, boolean sortEnabled) {
        this.title = title;
        this.width = width;
        this.sortEnabled = sortEnabled;
    }

    public Object getData(User user) {
        switch(this){
            case NAZWISKO:
                return user.getLastName();
            case IMIE:
                return user.getFirstName();
            case SYSTEMY_SWD:
                return "todo - systemy swd, w kt�rych u�. ma tak� role";
            default:
                throw new UnexpectedTypeException("Brak metody do pobrania danych dla UsersSimpleColumn: " + this.name());
        }
    }

    public String getTitle() {
        return title;
    }

    public Integer getWidth() {
        return width;
    }

    public boolean isSortEnabled() {
        return sortEnabled;
    }

    public static UsersSimpleColumn findByName(String name, UsersSimpleColumn defaultColumn) {
        if (StringUtils.isNotEmpty(name))
            return UsersSimpleColumn.valueOf(name);
//            for (CustomerColumn cc : CustomerColumn.values())
//                if (cc.name().equalsIgnoreCase(name))
//                    return cc;
        return defaultColumn;
    }
}
