package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdUsersFactory {

    private static final Logger log = LoggerFactory.getLogger(SwdUsersFactory.class);

    public static String getWholeName(User user) {
        return user.getFirstName() + " " + user.getLastName();
    }

    public static List<User> getAllUsers() {

        List<User> allUsers = new ArrayList<User>();
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            Collection<OrganisationUnit> allUnits = null;
            allUnits = factory.getOUList(null);

            for (OrganisationUnit unit : allUnits) {
                Collection<User> unitUsers = factory.getUserList(unit);
                allUsers.addAll(unitUsers);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return allUsers;
    }

    public static List<User> getUsers (OrganisationUnit organisationUnit){

        List<User> allUsers = new ArrayList<User>();
        SwdUserManagementFactory factory = new SwdUserManagementFactory();

        try {
            Collection<User> unitUsers = factory.getUserList(organisationUnit);
            allUsers.addAll(unitUsers);

        } catch (Exception e) {
            log.error(e.getMessage(), e);

        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }

        return allUsers;
    }

    public static User getUserById (Integer userId){
        User user = null;
        if (userId!= null){
            SwdUserManagementFactory factory = new SwdUserManagementFactory();

            try {
                user = factory.getUser(userId);

            } catch (Exception e) {
                log.error(e.getMessage(), e);

            } finally {
                SwdSessionFactory.closeSessionQuietly();
            }
        }
        return user;
    }
}
