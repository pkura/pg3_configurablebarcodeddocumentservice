package pl.compan.docusafe.web.swd.management.utils;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.swd.management.sql.SwdCustomFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class CustomFilterSelect {

    public static final String CUSTOM_FILTER = "CUSTOM_FILTER";

    public static JspAttribute.SelectField create(String defaultTitle, SwdCustomFilter... filters) {

        List<JspAttribute.SelectItem> items = new ArrayList<JspAttribute.SelectItem>();

        if (StringUtils.isNotEmpty(defaultTitle)) {
            items.add(new JspAttribute.SelectItem(JspAttribute.SelectField.DEFAULT_SELECTED_ID, defaultTitle));
        }

        for (SwdCustomFilter f : filters) {
            items.add(new JspAttribute.SelectItem(f.getId().toString(), f.getTitle()));
        }

        return new JspAttribute.SelectField(items);
    }

}
