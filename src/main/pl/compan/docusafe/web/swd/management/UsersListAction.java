package pl.compan.docusafe.web.swd.management;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.swd.management.mapping.SwdCustomFilterSelect;
import pl.compan.docusafe.web.swd.management.sql.FilterCn;
import pl.compan.docusafe.web.swd.management.sql.ZbpatSwdCustomFilter;
import pl.compan.docusafe.web.swd.management.utils.*;
import pl.compan.docusafe.webwork.event.*;
import pl.zbp.users.User;
import pl.zbp.users.UserSearchConditions;

import javax.validation.UnexpectedTypeException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class UsersListAction extends EventActionSupport {
    private static final Logger log = LoggerFactory.getLogger("SWD: " + UsersListAction.class.getName());

    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(UsersListAction.class.getPackage().getName(), null);

    private Boolean pop;
    private Boolean readonly;

    private String typ;

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }


    //Form Input
    private Integer clientID;
    private Integer userID;
    private String login;
    private String lastName;
    private String firstName;
    private Boolean active;
    private Boolean blocked;
    private String customFilterField;
    private String customFilterClause;


    private static Map<Boolean, String> selectActive = new HashMap<Boolean, String>();
    static
    {
        selectActive.put(null,"Wszyscy");
        selectActive.put(true,"Aktywny");
        selectActive.put(false, "Nie Aktywny");
    }

    private static Map<Boolean, String> selectBlocked = new HashMap<Boolean, String>();
    static
    {
        selectBlocked.put(null,"Wszyscy");
        selectBlocked.put(true,"Zablokowany");
        selectBlocked.put(false,"Nie Zablokowany");
    }
    private static Map<String, String> selectField = new HashMap<String, String>();
    static
    {
        selectField.put(null, "Filtruj");
        for (UserColumn col : UserColumn.values()){
            selectField.put(col.proprety, col.title);
        }
    }


    //Custom Filter
    private SwdCustomFilterSelect selectedFilter = new SwdCustomFilterSelect();
    private List<ZbpatSwdCustomFilter> filterList;


    //Form Fill
    private Boolean tableHidden;
    private String tableTitle;
    private List<TableColumn> tableColumns;
    private List<SwdUser> tableData;


    //Sort
    private String sortField = "lastName";
    private Boolean asc = true;





    protected void setup() {
        FillForm fillForm = new FillForm();
        Search search = new Search();

        registerListener(DEFAULT_ACTION)
                .append(OpenHibernateSession.INSTANCE)
                .append(new CustomFilterSearch())
                .append(search)
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);


        registerListener("doGenerateCSV")
                .append(OpenHibernateSession.INSTANCE)
                .append(new CustomFilterSearch())
                .append(search)
                .append(fillForm)
                .append(new GenerateCSV())
                .appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            tableTitle = "Odnalezieni Uzytkownicy";
            tableColumns = new ArrayList<TableColumn>();

            for (UserColumn col : UserColumn.values()){
                tableColumns.add(new TableColumn(col.proprety, col.title, "", ""));
            }
            if(tableData == null || tableData.isEmpty()){
                 tableHidden = true;
            }else{
                 tableHidden = false;
            }

            filterList = CustomFilterUtils.getCustomFiltersForCn(FilterCn.USER);

            if(pop != null && pop.equals(Boolean.TRUE)){
                event.setResult("popup");
            }
        }


    }

    private class CustomFilterSearch implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            if(selectedFilter != null && selectedFilter.isSelected()){
                List<User> userList = CustomFilterUtils.findUsersByCustomWhereClause(selectedFilter.getWhereClause());
                if(userList != null){
                    tableData = SwdUtils.getSwdUsersFromUsers(userList);
                }else{
                    addActionMessage("Brak wyniku dla danego filtra");
                }
            }
        }
    }

    private class Search implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent event) {
            if(selectedFilter == null || !selectedFilter.isSelected()){
                tableData = getUsers();
            }
            if(tableData != null){
                sortUsers();
            }
        }

        /**
         * Returns SwdUsers based on users from SWD database based on the conditions builded from Action fields
         *
         * @return SwdUsers List
         */
        private List<SwdUser> getUsers() {
            SwdUserManagementFactory factory = new SwdUserManagementFactory();
            Collection<User> users = null;
            Set<User> userSet = new HashSet<User>();


            UserSearchConditions conditions = getUserSearchCondition(factory);

            try {
                if(conditions != null){
                    users = factory.findUsers(conditions);
                }
                else{
                    users = factory.getAllUsers();
                }

            } catch (Exception e) {

                log.error(e.getMessage(),e);

            } finally {

                SwdSessionFactory.closeSessionQuietly();
            }
            //findUsers(conditions) zwraca po 2 instancje każdego usera
            if(users != null){
                for(User u : users){
                    //test-filter for blocked and active
                    if(blocked!=null || active!=null){
                        if( (blocked==null || blocked.equals(u.isBlocked())) && (active==null || active.equals(u.getActive()))){
                            userSet.add(u);
                        }
                    } else{
                        userSet.add(u);
                    }
                }
            }
            List<SwdUser> userList = new LinkedList<SwdUser>();
            for(User u : userSet){
                userList.add(new SwdUser(u));
            }
            return userList;

        }

        /**
         * Returns search conditions based on action fields for searching users in database.
         * If only blocked or active are set in action fields (other fields are null) returns condition
         * that returns all users
         *
         * @param  factory SwdUserManagementFactory
         * @return UserSearchConditions
         */
        private UserSearchConditions getUserSearchCondition(SwdUserManagementFactory factory){
            UserSearchConditions conditions = factory.getUserSearchCondition();

            //If filter is set only for blocked or active it must return conditions that will match all users
            if(clientID == null && userID == null && login==null && firstName == null && lastName == null)
                    //&& (blocked!=null || active!=null))
            {
                //conditions.setLogin("%");
                return null;
            }else{
                conditions.setKlientID(clientID);
                conditions.setUserID(userID);

                if(!"".equals(login) && login!=null){
                    conditions.setLogin("%" + login + "%");
                }
                if(!"".equals(lastName) && lastName!=null){
                    conditions.setLastName("%" + lastName + "%");
                }
                if(!"".equals(firstName) && firstName!=null){
                    conditions.setFirstName("%" + firstName + "%");
                }
            }

            return conditions;

        }


        private void sortUsers(){

            if(sortField != null && asc != null && !"".equals(sortField)){
                Comparator<SwdUser> comparator = UserComparator.getComparator(sortField, asc);

                Collections.sort(tableData, comparator);
            }
        }
    }

    private class GenerateCSV implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String title = "SWD_Users";

            try{
            File file = generateCsvForTableData(title);
            ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2",
                                    "Content-Disposition: attachment; filename=\"" + title +".csv\"");
            }catch (IOException ioe){
                addActionError(ioe.getMessage());
                log.debug(ioe.getMessage(),ioe);
            }
        }

        public File generateCsvForTableData(String title) throws IOException{

                File file = File.createTempFile(title,".csv");
                //CSVPrinter printer= new CSVPrinter(new FileOutputStream(file));
                CsvDumper dumper = new CsvDumper();
                dumper.setFieldSeparator(",");
                dumper.openFile(file);

                dumper.newLine();
                for(TableColumn c: tableColumns){
                    dumper.addText(c.getTitle());
                }
                dumper.dumpLine();

                for(SwdUser u: tableData){
                    dumper.newLine();
                    for(TableColumn c: tableColumns){
                        dumper.addText(getTableData(u,c.getTitle()));
                    }
                    dumper.dumpLine();

                }
                dumper.closeFile();

            return file;
        }
    }



    public String getTableData(Object user, String column) {
        if (user instanceof SwdUser && StringUtils.isNotEmpty(column)) {
            UserColumn userColumn = UserColumn.findByTitle(column);
            switch (userColumn) {
                case LP:
                    return Integer.toString(tableData.indexOf(user)+1);
                case USERID:
                    return ((SwdUser) user).getUserID().toString();
                case NAZWISKO:
                    return ((SwdUser) user).getLastName();
                case IMIE:
                    return ((SwdUser) user).getFirstName();
                case LOGIN:
                    return ((SwdUser) user).getLogin();
                case AKT:
                    return ((SwdUser) user).getActiveAsString();
                case ZABL:
                    if(((SwdUser) user).getBlocked()){
                        return "1";
                    }else if(((SwdUser) user).getBlocked() == null){
                        return "0";
                    }else{
                        return "0";
                    }
                case KLIENT:
                    return ((SwdUser)user).getClientName();
                case LOGOWANIE:
                    return ((SwdUser) user).getLastLoginTime().toString();
                default:
                    throw new UnexpectedTypeException("Brak typu CustomerColumn dla nazwy: " + column);
            }
        }
        throw new IllegalArgumentException();
    }

    public static String getTableDataLink(Object user, String column, Object pop) {
        return getTableDataLink(user,column,pop,null);
    }

    public static String getTableDataLink(Object user, String column, Object pop, Object readonly){
        if (user!= null && user instanceof SwdUser) {
            if(column != null && UserColumn.KLIENT.title.equals(column)){
                return OrganisationUnitAction.createLink(((SwdUser) user).getKlientID());
            }
            String userId = ((SwdUser) user).getUserID() != null ? ((SwdUser) user).getUserID().toString() : null;
            return UserViewAction.createLink(userId, pop, readonly);

        }
        if(user instanceof User){
            return UserViewAction.createLink(((User) user).getUserID(), pop);
        }

        throw new IllegalArgumentException();
    }



    public Boolean getTableHidden() {
        return  tableHidden;
    }

    public void setTableHidden(Boolean tableTitleHidden) {
        this. tableHidden = tableTitleHidden;
    }

    public String getTableTitle() {
        return tableTitle;
    }

    public void setTableTitle(String tableTitle) {
        this.tableTitle = tableTitle;
    }

    public List<TableColumn> getTableColumns() {
        return tableColumns;
    }

    public void setTableColumns(List<TableColumn> tableColumns) {
        this.tableColumns = tableColumns;
    }

    public List<SwdUser> getTableData() {
        return tableData;
    }

    public void setTableData(List<SwdUser> tableData) {
        this.tableData = tableData;
    }

    public Integer getClientID() {
        return clientID;
    }

    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public static Map<Boolean, String> getSelectBlocked() {
        return selectBlocked;
    }

    public static void setSelectBlocked(Map<Boolean, String> selectBlocked) {
        UsersListAction.selectBlocked = selectBlocked;
    }

    public static Map<Boolean, String> getSelectActive() {
        return selectActive;
    }

    public static void setSelectActive(Map<Boolean, String> selectActive) {
        UsersListAction.selectActive = selectActive;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public Boolean getAsc() {
        return asc;
    }

    public void setAsc(Boolean asc) {
        this.asc = asc;
    }

    public String getCustomFilterField() {
        return customFilterField;
    }

    public void setCustomFilterField(String customFilterField) {
        this.customFilterField = customFilterField;
    }

    public static Map<String, String> getSelectField() {
        return selectField;
    }

    public String getCustomFilterClause() {
        return customFilterClause;
    }

    public void setCustomFilterClause(String customFilterClause) {
        this.customFilterClause = customFilterClause;
    }

    public SwdCustomFilterSelect getSelectedFilter() {
        return selectedFilter;
    }

    public void setSelectedFilter(SwdCustomFilterSelect selectedFilter) {
        this.selectedFilter = selectedFilter;
    }

    public List<ZbpatSwdCustomFilter> getFilterList() {
        return filterList;
    }

    public void setFilterList(List<ZbpatSwdCustomFilter> filterList) {
        this.filterList = filterList;
    }

    public Boolean getPop() {
        return pop;
    }

    public void setPop(Boolean pop) {
        this.pop = pop;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }
}
