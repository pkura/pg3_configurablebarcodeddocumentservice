package pl.compan.docusafe.web.swd.management.jsp;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum LoginPeriod {
    WSZYSTKIE("Wszystkie"),
    W_TYM_ROKU("W tym roku"),
    PONAD_ROK("Ponad rok");

    public final String title;

    LoginPeriod(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return name();
    }
}
