package pl.compan.docusafe.web.swd.management.utils;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * Created by Damian on 17.12.13.
 */
public enum UserColumn {
    LP("lp","LP"),
    NAZWISKO("Nazwisko","lastName"),
    IMIE("Imie","firstName"),
    LOGIN("Login","login"),
    KLIENT("NazwaKlienta","clientName"),
    AKT("Active","akt"),
    ZABL("Blocked","zabl"),
    USERID("IdUser","userID"),
    LOGOWANIE("OstatnieUdaneLogowanie","lastLogin");

    private StringManager sm = GlobalPreferences.loadPropertiesFile(UserColumn.class.getPackage().getName(), null);

    public final String title;
    public final String proprety;

    private UserColumn(String title, String proprety) {
        this.title = sm.getString(title);
        this.proprety = proprety;
    }

    public static UserColumn findByTitle(String title) {
        for (UserColumn cc : UserColumn.values())
            if (cc.title.equals(title))
                return cc;
        throw new IllegalArgumentException("Nie ma typu CustomerColumn z tytu�em: " + title);
    }

    public String getTitle() {
        return title;
    }
}
