package pl.compan.docusafe.web.swd.management.sql;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.utils.SwdOrganisationUnitFactory;
import pl.compan.docusafe.web.swd.management.utils.SwdUsersFactory;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;

import javax.validation.UnexpectedTypeException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdPassedPassword implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(SwdPassedPassword.class);

    private Integer id;
    private Integer userId;
    private Date lastPassAttempt;
    private Date toPassAfter;
    private Date passedDate;

    private User user = null; //cache
    private OrganisationUnit organisationUnit = null; //cache

    public User getUser(Integer userId) {
        if (user == null)
            user = SwdUsersFactory.getUserById(userId);
        return user;
    }
    public OrganisationUnit getOrganisationUnit(Integer userId) {
        if (organisationUnit == null){
            User user = getUser(userId);
            organisationUnit = SwdOrganisationUnitFactory.getOrganisationUnitsById(user.getKlientID());
        }
        return organisationUnit;
    }

    public enum HibernateColumn {
        ID("id", "ID"),
        USER_ID("userId", "USER_ID"),
        LAST_PASS_ATTEMPT("lastPassAttempt", "LAST_PASS_ATTEMPT"),
        TO_PASS_AFTER("toPassAfter", "TO_PASS_AFTER"),
        PASSED_DATE("passedDate", "PASSED_DATE");

        public final String columnHibName;
        public final String columnDbName;

        private HibernateColumn(String columnHibName, String columnDbName) {
            this.columnHibName = columnHibName;
            this.columnDbName = columnDbName;
        }
    }

    public static List<SwdPassedPassword> searchToPassToList(Query query) {
        return searchToPassToList(search(query, false));
    }

    public static List<SwdPassedPassword> searchToPassToList(SearchResults<SwdPassedPassword> results) {
        SwdPassedPassword[] passwords = results.results();
        return Arrays.asList(passwords);
    }

    public static SearchResults<SwdPassedPassword> search(Query query, boolean passed) {

        try {
            Criteria criteria = DSApi.context().session().createCriteria(SwdPassedPassword.class);

            if (query.passTime != null) {
                switch (query.passTime) {
                    case IMMEDIATELY:
                        criteria.add(Restrictions.le(HibernateColumn.TO_PASS_AFTER.columnHibName, new Date()));
                        break;
                    case LATER:
                        criteria.add(Restrictions.gt(HibernateColumn.TO_PASS_AFTER.columnHibName, new Date()));
                        break;
                    default:
                        throw new UnexpectedTypeException("Brak obslugi typu: " + query.passTime.name());
                }
            }

            if (query.userId != null) {
                criteria.add(Restrictions.eq(HibernateColumn.USER_ID.columnHibName, query.userId));
            }

            if (passed) {
                criteria.add(Restrictions.isNotNull(HibernateColumn.PASSED_DATE.columnHibName));
            } else {
                criteria.add(Restrictions.isNull(HibernateColumn.PASSED_DATE.columnHibName));
            }

            criteria.addOrder(query.isAscending() ? Order.asc(query.getOrderBy().columnHibName) : Order.desc(query.getOrderBy().columnHibName));

            List<SwdPassedPassword> results = criteria.list();
            int totalCount = results.size();
            return new SearchResultsAdapter<SwdPassedPassword>(results, 0, totalCount, SwdPassedPassword.class);
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    public static SwdPassedPassword findNotPassByUserId(Integer userId) {
        List<SwdPassedPassword> result = SwdPassedPassword.searchToPassToList(new SwdPassedPassword.Query(userId));

        if (result.isEmpty())
            throw new IllegalArgumentException("Brak u�ytkownika o id=" + userId + ", kt�remu nale�y przekaza� has�o");
        if (result.size() > 1)
            throw new IllegalArgumentException("Niejednoznaczne id przekazywanego has�a dla u�ytkownika o id=" + userId);

        return result.get(0);
    }

    public static boolean isToPassForUser(Integer userId) {
        List<SwdPassedPassword> result = SwdPassedPassword.searchToPassToList(new SwdPassedPassword.Query(userId));

        if (result.isEmpty())
            return false;
        if (result.size() == 1)
            return true;

        throw new IllegalArgumentException("Niejednoznaczne id przekazywanego has�a dla u�ytkownika o id=" + userId);
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return true;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return true;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return true;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public void updateSafely() {
        try {
            update();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void update() throws EdmException {
        Session session = DSApi.context().session();
        Transaction tx = session.beginTransaction();
        session.update(this);
        tx.commit();
    }

    public void saveSafely() {
        try {
            save();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void save() throws EdmException {
        Session session = DSApi.context().session();
        Transaction tx = session.beginTransaction();
        session.save(this);
        tx.commit();
    }

    public void deleteSafely() {
        try {
            delete();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void delete() throws EdmException {
        Session session = DSApi.context().session();
        Transaction tx = session.beginTransaction();
        session.delete(this);
        tx.commit();
    }


    public enum PassTime {
        IMMEDIATELY,
        LATER
    }

    public static class Query {

        public final PassTime passTime;
        public final Integer userId;

        private Boolean ascending;
        private HibernateColumn orderBy;

        public Query(PassTime passTime) {
            this(passTime, null);
        }

        public Query(int userId) {
            this(null, userId);
        }

        private Query(PassTime passTime, Integer userId) {
            this.passTime = passTime;
            this.userId = userId;
        }

        public boolean isAscending() {
            return ascending == null ? false : ascending;
        }

        public HibernateColumn getOrderBy() {
            return orderBy == null ? HibernateColumn.TO_PASS_AFTER : orderBy;
        }

        public Query setAscending(boolean ascending) {
            this.ascending = ascending;
            return this;
        }

        public Query setOrderBy(HibernateColumn orderBy) {
            this.orderBy = orderBy;
            return this;
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public Date getLastPassAttempt() {
        return lastPassAttempt;
    }

    public Date getToPassAfter() {
        return toPassAfter;
    }

    public Integer getId() {
        return id;
    }

    public Date getPassedDate() {
        return passedDate;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setLastPassAttempt(Date lastPassAttempt) {
        this.lastPassAttempt = lastPassAttempt;
    }

    public void setToPassAfter(Date toPassAfter) {
        this.toPassAfter = toPassAfter;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPassedDate(Date passedDate) {
        this.passedDate = passedDate;
    }
}
