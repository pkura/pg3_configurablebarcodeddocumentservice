CREATE TABLE SWD_CUSTOM_FILTER (
                  ID               NUMERIC(18,0),
                  CN               VARCHAR(50),
                  TITLE            VARCHAR(50),
                  WHERE_CLAUSE     VARCHAR(255),
                  PRIMARY KEY (ID)
              );

--INSERT [SWD_CUSTOM_FILTER] ([ID], [CN], [TITLE], [WHERE_CLAUSE]) VALUES (11, N'USER', N'user_test_filter', N'LAST_INVALID_LOGIN_TIME > ''20130101'' and last_invalid_login_time < ''20140101')
--INSERT [SWD_CUSTOM_FILTER] ([ID], [CN], [TITLE], [WHERE_CLAUSE]) VALUES (12, N'ORGANISATION_UNIT', N'ou_test_filter', N'Kind like ''%ZORO%''')
--INSERT [SWD_CUSTOM_FILTER] ([ID], [CN], [TITLE], [WHERE_CLAUSE]) VALUES (13, N'USER', N'user_test_filter2', N'login like ''a%'' or login like ''b%''')
--INSERT [SWD_CUSTOM_FILTER] ([ID], [CN], [TITLE], [WHERE_CLAUSE]) VALUES (14, N'USER', N'user_test_filter3', N'LAST_INVALID_LOGIN_TIME >= ''20130101'' and last_invalid_login_time <= ''20140101'))