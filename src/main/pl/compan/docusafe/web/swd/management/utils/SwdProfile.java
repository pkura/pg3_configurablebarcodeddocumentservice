package pl.compan.docusafe.web.swd.management.utils;

import org.jfree.util.Log;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum SwdProfile {
    OPERATOR("Operator",
            SwdPermission.Permission.OULIST_READ,
            SwdPermission.Permission.OULIST_BUTTON_CHOOSE_READ,
            SwdPermission.Permission.OULIST_BUTTON_CREATE_OUNIT_READ,
            SwdPermission.Permission.OU_DANE_ORG_READ,
            SwdPermission.Permission.OU_SPOSOBY_DOSTEPU_READ,
            SwdPermission.Permission.OU_ILOSCI_READ,
            SwdPermission.Permission.OU_UZYTKOWNICY_WG_ROLI_READ,
            SwdPermission.Permission.OU_UZYTKOWNICY_READ,
            SwdPermission.Permission.OU_BUTTON_SAVE_READ,
            SwdPermission.Permission.OU_BUTTON_DOCUMENTS_READ,
            SwdPermission.Permission.OU_BUTTON_CREATE_USER_READ,
            SwdPermission.Permission.HDPLIST_READ,
            SwdPermission.Permission.HDP_EDIT_READ,

            SwdPermission.Permission.OULIST_BUTTON_CHOOSE_EDIT,
            SwdPermission.Permission.OULIST_BUTTON_CREATE_OUNIT_EDIT,
            SwdPermission.Permission.OU_DANE_ORG_EDIT,
            SwdPermission.Permission.OU_BUTTON_SAVE_EDIT,
            SwdPermission.Permission.OU_BUTTON_DOCUMENTS_EDIT,
            SwdPermission.Permission.OU_BUTTON_CREATE_USER_EDIT,
            SwdPermission.Permission.HDP_EDIT_EDIT),
    DBA("DBA", SwdPermission.Permission.values()),
    KSIEGOWOSC("Ksi�gowy",
            SwdPermission.Permission.OULIST_READ,
            SwdPermission.Permission.OU_DANE_ORG_READ,
            SwdPermission.Permission.OU_SPOSOBY_DOSTEPU_READ,
            SwdPermission.Permission.OU_ILOSCI_READ,
            SwdPermission.Permission.OU_UZYTKOWNICY_WG_ROLI_READ,
            SwdPermission.Permission.OU_UZYTKOWNICY_READ,
            SwdPermission.Permission.HDPLIST_READ,
            SwdPermission.Permission.HDP_EDIT_READ
    );
    public final SwdPermission.Permission[] permissions;
    private final String name;

    SwdProfile(String name, SwdPermission.Permission... permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public static String[] getProfilesNamesAsArrayFor(SwdPermission.Permission permission) {
        Set<String> profiles = getProfilesNameFor(permission);
        return profiles.toArray(new String[profiles.size()]);
    }

    public static Set<String> getProfilesNameFor(SwdPermission.Permission permission) {
        Set<String> matchedProfiles = new HashSet<String>();
        for (SwdProfile profile : SwdProfile.values()) {
            if (profile.contains(permission))
                matchedProfiles.add(profile.getName());
        }
        return matchedProfiles;
    }

    public String getName() {
        return name;
    }

    private boolean contains(SwdPermission.Permission permission) {
        for (SwdPermission.Permission profilePermission : this.permissions)
            if (profilePermission == permission)
                return true;
        return false;
    }

    public static Set<SwdProfile> getUserProfiles() {
        Set<SwdProfile> profiles = new HashSet<SwdProfile>();
        DSContextOpener opener = null;
        try{
            opener = DSContextOpener.openAsUserIfNeed();
            DSUser user = JspPermissionManager.getLoginUser();
            if (user != null) {
                String[] userProfileNames = user.getProfileNames();
                for (SwdProfile swdP : SwdProfile.values()) {
                    for (String upName : userProfileNames) {
                        if (swdP.getName().equalsIgnoreCase(upName)) {
                            profiles.add(swdP);
                            continue;
                        }
                    }
                }
            }
        } catch (EdmException e) {
            Log.error(e.getMessage(),e);
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
        return profiles;
    }
}
