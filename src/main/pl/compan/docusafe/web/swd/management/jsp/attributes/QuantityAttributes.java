package pl.compan.docusafe.web.swd.management.jsp.attributes;

import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspComponentAttributes;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class QuantityAttributes extends JspComponentAttributes {

    public QuantityAttributes(String title) {
        setJspTitle(title);
    }

    public void setQuantities(List<Map.Entry<String, Integer>> quantities) {
        for (Map.Entry<String, Integer> q : quantities) {
            JspAttribute attr = JspAttribute.Factory.createAttr(q.getKey(), q.getValue());
            addAttribute(attr);
        }
    }
}
