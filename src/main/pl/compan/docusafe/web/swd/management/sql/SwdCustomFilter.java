package pl.compan.docusafe.web.swd.management.sql;

import org.drools.util.StringUtils;
import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdCustomFilter implements Serializable, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(SwdCustomFilter.class);
    private Integer id;
    private String cn;
    private String title;
    private String whereClause;

    public static List<SwdCustomFilter> searchAsList(FilterCN filter) {
        return searchAsList(filter,null);
    }
    public static List<SwdCustomFilter> searchAsList(FilterCN filter, Boolean ascending) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(SwdCustomFilter.class);
            criteria.add(Restrictions.eq(HibernateColumn.CN.columnHibName, filter.getCn()));
            String orderColumn = HibernateColumn.TITLE.columnHibName;
            if (ascending!=null)
                criteria.addOrder(ascending ? Order.asc(orderColumn) : Order.desc(orderColumn));

            List<SwdCustomFilter> results = criteria.list();
            return results;
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    public static SearchResults<SwdCustomFilter> search(FilterCN filter, Boolean ascending) {
        List<SwdCustomFilter> results = searchAsList(filter,ascending);
        int totalCount = results.size();
        if (totalCount == 0)
            return null;
        return new SearchResultsAdapter<SwdCustomFilter>(results, 0, totalCount, SwdCustomFilter.class);

//        try {
//            Criteria criteria = DSApi.context().session().createCriteria(SwdCustomFilter.class);
//            criteria.add(Restrictions.eq(HibernateColumn.CN.columnHibName, filter.getCn()));
//            String orderColumn = HibernateColumn.TITLE.columnHibName;
//            if (ascending!=null)
//                criteria.addOrder(ascending ? Order.asc(orderColumn) : Order.desc(orderColumn));
//
//            List<SwdCustomFilter> results = criteria.list();
//            int totalCount = results.size();
//            return new SearchResultsAdapter<SwdCustomFilter>(results, 0, totalCount, SwdCustomFilter.class);
//        } catch (EdmException e) {
//            log.debug(e.getMessage(), e);
//        }
//        return null;
    }

    public static SwdCustomFilter findById(Integer id) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(SwdCustomFilter.class);
            criteria.add(Restrictions.eq(HibernateColumn.ID.columnHibName, id));

            List<SwdCustomFilter> results = criteria.list();
            int totalCount = results.size();

            if (totalCount == 0)
                throw new IllegalArgumentException("Brak filtru o id=" + id);
            if (totalCount > 1)
                throw new IllegalArgumentException("Niejednoznaczny filtr dla id=" + id);

            SearchResults<SwdCustomFilter> resultsCustomFilter = new SearchResultsAdapter<SwdCustomFilter>(results, 0, totalCount, SwdCustomFilter.class);
            return resultsCustomFilter.next();
        } catch (EdmException e) {
            log.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean onSave(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session session) throws CallbackException {
        return false;
    }

    @Override
    public boolean onDelete(Session session) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session session, Serializable serializable) {

    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public enum HibernateColumn {
        ID("id", "ID"),
        CN("cn", "CN"),
        TITLE("title", "TITLE"),
        WHERE_CLAUSE("whereClause", "WHERE_CLAUSE");
        public final String columnHibName;
        public final String columnDbName;

        private HibernateColumn(String columnHibName, String columnDbName) {
            this.columnHibName = columnHibName;
            this.columnDbName = columnDbName;
        }
    }

    public enum FilterCN {
        ORGANISATION_UNIT,
        USER;
        private final String cn;

        private FilterCN() {
            this(null);
        }

        private FilterCN(String cn) {
            this.cn = cn;
        }

        public String getCn() {
            return StringUtils.isEmpty(cn) ? name() : cn;
        }
    }
}
