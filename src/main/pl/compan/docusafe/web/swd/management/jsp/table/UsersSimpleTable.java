package pl.compan.docusafe.web.swd.management.jsp.table;

import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.UsersListAction;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.UsersSimpleColumn;
import pl.zbp.users.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UsersSimpleTable extends JspComponentTable<User> {

    public UsersSimpleTable(String title) {
        super(title);
        init();
    }

    private UsersSimpleTable init() {
        List<JspTableColumn> columns = new ArrayList<JspTableColumn>();
        for (UsersSimpleColumn col : UsersSimpleColumn.values()) {
            columns.add(new JspTableColumn(
                    col.width,
                    null,
                    col.title,
                    null,
                    null,
                    col.name()));
        }

        setJspColumns(columns);
        setJspHeaderDisabled(true);
        return this;
    }

    @Override
    public String getData(User user, JspTableColumn column) {
        UsersSimpleColumn usersColumn = UsersSimpleColumn.valueOf(column.getColumnName());
        Object data = usersColumn.getData(user);
        return data != null ? data.toString() : "";
    }

    @Override
    public String getLink(User user, JspTableColumn column) {
        return UsersListAction.getTableDataLink(user, null, parentLayout==Layout.POPUP);
    }

    public JspComponentTable setJspData(List<User> users) {
        return setJspData(users, null, null);
    }

    public JspComponentTable setJspData(List<User> users, String sortField, String sortAscending) {
        UsersSimpleColumn usersColumn = UsersSimpleColumn.findByName(sortField, UsersSimpleColumn.NAZWISKO);
        boolean ascending = sortAscending == null || Boolean.parseBoolean(sortAscending);
        DataGetter dataGetter = new DataGetter<UsersSimpleColumn, User>(usersColumn) {
            @Override
            public String getData(User obj) {
                Object data = column.getData(obj);
                return data != null ? data.toString() : null;
            }
        };
        sortByColumn(users, dataGetter, ascending);
        this.jspData = users;
        return this;
    }
}
