package pl.compan.docusafe.web.swd.management;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspAttribute;
import pl.compan.docusafe.web.jsp.component.JspComponentCheckboxesStringId;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.jsp.component.JspTableColumn;
import pl.compan.docusafe.web.swd.management.jsp.attributes.OrganisationUnitAttributes;
import pl.compan.docusafe.web.swd.management.jsp.attributes.QuantityAttributes;
import pl.compan.docusafe.web.swd.management.jsp.attributes.types.OrganisationUnitAttr;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.jsp.table.UsersSimpleTable;
import pl.compan.docusafe.web.swd.management.jsp.table.UsersTable;
import pl.compan.docusafe.web.swd.management.jsp.table.columns.UsersColumn;
import pl.compan.docusafe.web.swd.management.mapping.SwdOrganisationUnitUpdater;
import pl.compan.docusafe.web.swd.management.sql.SwdOrganisationUnitAccess;
import pl.compan.docusafe.web.swd.management.sql.SwdQuantityQuery;
import pl.compan.docusafe.web.swd.management.utils.*;
import pl.compan.docusafe.webwork.event.*;
import pl.zbp.users.OrganisationUnit;
import pl.zbp.users.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OrganisationUnitAction extends EventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(OrganisationUnitAction.class);
    private static final String LINK_BASE = "/swd/management/organisation-unit.action";

    public static String createLink(OrganisationUnit organisationUnit) {
        return createLink(organisationUnit.getKlientId(), JspComponentTable.Layout.MAIN);
    }

    public static String createLink(Integer id) {
        return createLink(id, JspComponentTable.Layout.MAIN);
    }

    public static String createLink(String id) {
        return createLink(id, JspComponentTable.Layout.MAIN);
    }


    public static String createLink(OrganisationUnit organisationUnit, JspComponentTable.Layout parentLayout) {
        return createLink(organisationUnit.getKlientId(), parentLayout);
    }

    public static String createLink(Object id, JspComponentTable.Layout parentLayout) {
        return new LinkCreator("/swd/management/organisation-unit.action")
                .setLayout(parentLayout)
                .add("id", id)
                .create();
    }


    protected String id;
    public String documentId;
    protected Boolean loaded;
    public String layout;
    protected OrganisationUnit jspOrganisationUnit;

    protected OrganisationUnitAttributes jspAttributes;
    protected QuantityAttributes jspQuantity;

    protected UsersSimpleTable jspCoordinatorsTable;
    protected UsersSimpleTable jspTechAdminsTable;
    protected UsersSimpleTable jspContactPersonsTable;

    private String orderBy;
    private String ascending;
    protected UsersTable jspUsersTable;

    protected JspComponentCheckboxesStringId jspCheckboxes;

    private UsersTable.ColumnSortLinkCreator columnSortLinkCreator;

    protected SwdJspPermissionManager jspPermissionManager;

    protected void setup() {
        FillForm fillForm = new FillForm();
        initAll();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDocuments").
                append(OpenHibernateSession.INSTANCE).
                append(new DocumentsListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
                append(OpenHibernateSession.INSTANCE).
                append(new SaveListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doFilter").
                append(OpenHibernateSession.INSTANCE).
                append(new FilterListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddUser").
                append(OpenHibernateSession.INSTANCE).
                append(new AddUserListener()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doExportCSV").
                append(OpenHibernateSession.INSTANCE).
                append(new FilterListener()).
                append(new GenerateCSV()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private void initAll() {
        jspAttributes = new OrganisationUnitAttributes("Dane organizacyjne");
        jspQuantity = new QuantityAttributes("Ilo�ci dla tego klienta");
        jspCoordinatorsTable  = new UsersSimpleTable("Koordynatorzy u�ytkownik�w");
        jspTechAdminsTable = new UsersSimpleTable("Administratorzy techniczni");
        jspContactPersonsTable = new UsersSimpleTable("Osoby kontaktowe");
        jspUsersTable = new UsersTable("U�ytkownicy", JspTableColumn.DEFAULT_SMALL_WIDTH) {
            @Override
            public boolean isOrderBy(JspTableColumn column, Boolean ascending) {
                return column.getColumnName().equalsIgnoreCase(orderBy) && ascending.toString().equalsIgnoreCase(OrganisationUnitAction.this.ascending);
            }
        };
        jspCheckboxes = new JspComponentCheckboxesStringId("Sposoby dost�pu");
        columnSortLinkCreator  = new UsersTable.ColumnSortLinkCreator() {
            @Override
            public String getLink(UsersColumn column, boolean ascending) {
                ;
                return column == null ? "" : new LinkCreator(LINK_BASE)
                        .setLayout(JspComponentTable.Layout.getLayout(getLayout()))
                        .add("id", id)
                        .add("orderBy", column.name())
                        .add("ascending", ascending)
                        .create();
            }
//    private String getBaseLink(String baseLink, String id) {
//        return baseLink + "?id=" + id;
//    }
        };
        jspPermissionManager = new SwdJspPermissionManager(OrganisationUnitAction.class.getName(), true);
    }


    public boolean isNewOrganisationUnit() {
        return jspOrganisationUnit == null;
    }

    private boolean isPopup() {
        return JspComponentTable.Layout.getLayout(layout) == JspComponentTable.Layout.POPUP;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    private Long parseDocumentId() {
        if (StringUtils.isBlank(documentId) || !Pattern.compile("\\d+").matcher(documentId).find())
            return null;
        return Long.parseLong(documentId);
    }

    private class FillForm implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            jspOrganisationUnit = getOrganisationUnit();

            if (isNewOrganisationUnit()) {

                SwdOrganisationUnitAccess access = new SwdOrganisationUnitAccess();
                for (Map.Entry<String, Boolean> a : access.getAccessMethods())
                    jspCheckboxes.add(a.getKey(), a.getKey(), !jspCheckboxes.isSet() ? a.getValue() : null, null);

            } else {

                // edytowalne pola - nie zmieniamy

                Long documentId = parseDocumentId();
                jspPermissionManager.setDocumentId(documentId);

                if (!jspAttributes.isSet())
                    jspAttributes.setFields(jspOrganisationUnit);

                SwdOrganisationUnitAccess access = new SwdOrganisationUnitAccess(jspOrganisationUnit);
                for (Map.Entry<String, Boolean> a : access.getAccessMethods())
                    jspCheckboxes.add(a.getKey(), a.getKey(), !jspCheckboxes.isSet() ? a.getValue() : null, null);

                // nieedytowalne pola - np. filtrujemy

                List<Map.Entry<String, Integer>> quantities = SwdQuantityQuery.findQuantities(SwdQuantityQuery.ObjectQuantityCns.ORGANISATION_UNIT, jspOrganisationUnit);
//                        new ArrayList<Map.Entry<String, Integer>>();
//                for (Map.Entry<String, Boolean> a : access.getAccessMethods())
//                    quantities.add(new AbstractMap.SimpleEntry<String, Integer>(a.getKey(),a.getValue() == true ? 1 : 0));
                jspQuantity.setQuantities(quantities);

                List<User> users = getFilterOrganisationUnits();
                jspUsersTable
                        .init(columnSortLinkCreator, true)
                        .setJspData(users, orderBy, ascending);

                jspCoordinatorsTable.setJspData(tmpTestGetUsers(3));
                jspTechAdminsTable.setJspData(tmpTestGetUsers(1));
                jspContactPersonsTable.setJspData(tmpTestGetUsers(2));

                //uaktualnij najcz�ciej odwiedzanych klient�w

                if (loaded == null || !loaded) {
                    loaded = true;
                    SwdCustomersStats.notifyUse(jspOrganisationUnit);
                }
            }

            if (isPopup()) {
                jspUsersTable.setParentLayout(JspComponentTable.Layout.POPUP);
                event.setResult("popup");
            }


        }

        private List<User> tmpTestGetUsers(int num) {
            List<User> allUsers = SwdUsersFactory.getAllUsers();
            List<User> users = new ArrayList<User>();

            Random random = new Random();
            while (num > 0 && allUsers.size() > 0) {
                int idx = random.nextInt(allUsers.size());
                User u = allUsers.get(idx);

                users.add(u);
                allUsers.remove(u);

                --num;
            }

            return users;
        }
    }


    public List<User> getFilterOrganisationUnits() {
        String customFilter = jspUsersTable.getCustomFilter(); // todo doda� do wyszukiwania
        List<User> users = SwdUsersFactory.getUsers(getOrganisationUnit());

        users = UsersTable.filterUsers(jspUsersTable, users);
        return users;
    }

    private abstract class ActionUpdater implements ActionListener {

        @Override
        public final void actionPerformed(ActionEvent event) {
            if (jspAttributes != null)
                jspAttributes.set();
            if (jspCheckboxes != null)
                jspCheckboxes.set();

            if (jspUsersTable != null)
                jspUsersTable.updateFilters();

            perform(event);
        }

        public abstract void perform(ActionEvent event);
    }

    private class GenerateCSV extends SimpleGenerateCsvActionListener<User> {

        @Override
        public String getTitle() {
            StringBuilder title = new StringBuilder();
            title.append("Profil klienta ")
                    .append(getOrganisationUnit().getName())
                    .append(" - u�ytkownicy");

            title.append(getFilterTitlePart(jspUsersTable.getFilters()));

            return title.toString();
        }

        @Override
        public EventActionSupport getEventActionSupport() {
            return OrganisationUnitAction.this;
        }

        @Override
        public Logger getLogger() {
            return log;
        }

        @Override
        public JspComponentTable<User> getJspComponentTable() {
            List<User> users = getFilterOrganisationUnits();
            jspUsersTable
                    .init(columnSortLinkCreator, true)
                    .setJspData(users, orderBy, ascending);
            return jspUsersTable;
        }
    }

    private class SaveListener extends ActionUpdater {
        public void perform(ActionEvent event) {
            System.out.println("save listener");

            StringBuilder msg = new StringBuilder();

            SwdOrganisationUnitUpdater updater = new SwdOrganisationUnitUpdater();

            if (jspAttributes != null) {
                for (OrganisationUnitAttr attr : OrganisationUnitAttr.values()) {
                    String value = jspAttributes.getFieldValue(attr);
                    JspAttribute jspAttr = jspAttributes.findJspAttribute(attr);
                    if (StringUtils.isEmpty(value)) {
                        if (isRequired(attr)) {
                            msg.append("Pole \"" + attr.getTitle() + "\" jest wymagane");

                            jspAttr.addCssClass(JspAttribute.CssClass.REQUIRED);
                        } else
                            jspAttr.clearCssClass();
                    } else {
                        String errorMsg = updater.setValue(attr, value);
                        appendMsg(msg, errorMsg);

                        if (errorMsg.length() > 0)
                            jspAttr.addCssClass(JspAttribute.CssClass.INVALID);
                        else
                            jspAttr.clearCssClass();
                    }
                }
            } else {
                msg.append("Brak zmian");
            }

            if (msg.length() == 0) {
                jspOrganisationUnit = getOrganisationUnit();
                updater.execute(jspOrganisationUnit);
            } else {
                addActionError(msg.toString());
            }
        }

        private void appendMsg(StringBuilder msg, String errorMsg) {
            if (msg.length() > 0)
                msg.append("; ");
            msg.append(errorMsg);
        }


        private boolean isRequired(OrganisationUnitAttr attr) {
            return attr.required;
        }

    }


    private class FilterListener extends ActionUpdater {
        public void perform(ActionEvent event) {
            System.out.println("filter listener");
//            //todo
        }
    }


    private class DocumentsListener extends ActionUpdater {
        public void perform(ActionEvent event) {
            System.out.println("documents listener");
//            //todo
        }
    }


    private class AddUserListener extends ActionUpdater {
        public void perform(ActionEvent event) {
            System.out.println("add user listener");
//            //todo
        }
    }


    public OrganisationUnit getOrganisationUnit() {
        if (jspOrganisationUnit != null)
            return jspOrganisationUnit;

        Integer ouId = getOrganisationId();
        if (ouId == null)
            return null;

        OrganisationUnit organisationUnit = null;
        try {
            SwdUserManagementFactory factory = new SwdUserManagementFactory();
            organisationUnit = factory.getOrganisationUnit(ouId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            SwdSessionFactory.closeSessionQuietly();
        }
        return organisationUnit;
    }

    public Integer getOrganisationId() {
        try {
            return StringUtils.isNotEmpty(id) ? ActionUtils.getIntegerId(id, true) : null;
        } catch (Exception e) {
            return null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////   GETTERS / -------   ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getId() {
        return id;
    }

    public OrganisationUnit getJspOrganisationUnit() {
        return jspOrganisationUnit;
    }

    public OrganisationUnitAttributes getJspAttributes() {
        return jspAttributes;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getAscending() {
        return ascending;
    }

    public UsersTable getJspUsersTable() {
        return jspUsersTable;
    }

    public JspComponentCheckboxesStringId getJspCheckboxes() {
        return jspCheckboxes;
    }

    public UsersSimpleTable getJspCoordinatorsTable() {
        return jspCoordinatorsTable;
    }

    public UsersSimpleTable getJspTechAdminsTable() {
        return jspTechAdminsTable;
    }

    public UsersSimpleTable getJspContactPersonsTable() {
        return jspContactPersonsTable;
    }

    public Boolean getLoaded() {
        return loaded;
    }

    public QuantityAttributes getJspQuantity() {
        return jspQuantity;
    }

    public String getLayout() {
        return layout;
    }

    public SwdJspPermissionManager getJspPermissionManager() {
        return jspPermissionManager;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////   ------- / SETTERS   ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void setId(String id) {
        this.id = id;
    }

    public void setJspOrganisationUnit(OrganisationUnit jspOrganisationUnit) {
        this.jspOrganisationUnit = jspOrganisationUnit;
    }

    public void setJspAttributes(OrganisationUnitAttributes jspAttributes) {
        this.jspAttributes = jspAttributes;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setAscending(String ascending) {
        this.ascending = ascending;
    }

    public void setJspUsersTable(UsersTable jspUsersTable) {
        this.jspUsersTable = jspUsersTable;
    }

    public void setJspCheckboxes(JspComponentCheckboxesStringId jspCheckboxes) {
        this.jspCheckboxes = jspCheckboxes;
    }

    public void setJspCoordinatorsTable(UsersSimpleTable jspCoordinatorsTable) {
        this.jspCoordinatorsTable = jspCoordinatorsTable;
    }

    public void setJspTechAdminsTable(UsersSimpleTable jspTechAdminsTable) {
        this.jspTechAdminsTable = jspTechAdminsTable;
    }

    public void setJspContactPersonsTable(UsersSimpleTable jspContactPersonsTable) {
        this.jspContactPersonsTable = jspContactPersonsTable;
    }

    public void setJspQuantity(QuantityAttributes jspQuantity) {
        this.jspQuantity = jspQuantity;
    }

    public void setLoaded(Boolean loaded) {
        this.loaded = loaded;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public void setJspPermissionManager(SwdJspPermissionManager jspPermissionManager) {
        this.jspPermissionManager = jspPermissionManager;
    }

}
