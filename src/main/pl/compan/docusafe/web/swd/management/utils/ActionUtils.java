package pl.compan.docusafe.web.swd.management.utils;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;

import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ActionUtils {

    public static int getIntId(String id) throws EdmException {
        return getIntegerId(id, false);
    }

    public static Integer getIntegerId(String id, boolean nullable) throws EdmException {
        if (StringUtils.isEmpty(id) || "null".equalsIgnoreCase(id)){
            if (nullable)
                return null;
            throw new EdmException("Brak id");
        }
        if (!Pattern.compile("\\d+").matcher(id).find())
            throw new EdmException("Nieprawidłowy format id");

        return Integer.parseInt(id);
    }
}
