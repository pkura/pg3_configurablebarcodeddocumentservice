package pl.compan.docusafe.web.comet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.comet.CometMessage.MessageKind;

public class CometMessageManager {
	private final static Logger kl = LoggerFactory.getLogger("kamil");
	private static CometMessageManager instance = new CometMessageManager();
	private Map<String, List<CometMessage>> messagesMap = new HashMap<String, List<CometMessage>>();
	
	public static CometMessageManager getInstance() {
		return instance;
	}
	
	public void addCometMessage(String recipient, CometMessage msg) {
		synchronized(messagesMap) {
			List<CometMessage> messages = messagesMap.get(recipient);
			if(messages == null) {
				messages = new ArrayList<CometMessage>();
				messagesMap.put(recipient, messages);
			}
			messages.add(msg);
			messagesMap.notifyAll();
			kl.debug("CometMessageManager - new message {} -> {}", recipient, msg.toJsonString());
		}
	}
	
	public List<CometMessage> getCometMessages(String recipient, MessageKind messageKind) {
		List<CometMessage> msgs = new ArrayList<CometMessage>();
		List<CometMessage> recipientMsgs = messagesMap.get(recipient);
		
		if(recipientMsgs != null) {
			for(CometMessage msg : recipientMsgs) {
				if(msg.getMessageKind().equals(messageKind)) {
					msgs.add(msg);
				}
			}
		}
		
		return msgs;
	}
	
	public Map<String, List<CometMessage>> getAllCometMessages() {
		synchronized(messagesMap) {
			if(kl.isDebugEnabled()) {
				kl.debug("get all messages:");
				for(Map.Entry<String, List<CometMessage>> entry : messagesMap.entrySet()) {
					kl.debug("{}", entry.getKey());
					for(CometMessage msg : entry.getValue()) {
						kl.debug(" ---> {}", msg.toJsonString());
					}
				}
			}
			return messagesMap;
		}
	}
}
