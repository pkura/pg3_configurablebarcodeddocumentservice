package pl.compan.docusafe.web.comet;

public abstract class CometMessage {
	public static enum MessageKind {
		/**
		 * Rozpocz�cie odbioru przesy�ki
		 */
		SIGNATURE_REQUEST,
		/**
		 * Odbi�r potwierdzony
		 */
		SIGNATURE_COMPLETE,
		/**
		 * Odrzucenie rozpocz�cia odbioru
		 */
		SIGNATURE_REQUEST_REJECT,
		/**
		 * Operacja na barkodzie ma by� odczytana przez pracownika kancelarii
		 */
		BARCODE_OFFICE,
		/**
		 * Operacja na barkodzie ma by� odczytana przez odbiorc�
		 */
		BARCODE_RECIPIENT
	}
	
	public abstract MessageKind getMessageKind();
	
	public abstract String toJsonString();
}
