package pl.compan.docusafe.web.record.projects;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.projects.AccountingCostKind;
import pl.compan.docusafe.core.record.projects.CostKind;
import pl.compan.docusafe.core.record.projects.FinancingInstitutionKind;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.repository.EditAttachmentAction;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class EditMainAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(EditMainAction.class);
	
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(EditMainAction.class.getPackage().getName(), null);

	// Export
	private List<DSUser> users;
	private List<DSUser> dpirUsers;
	private Map<String, String> entryTypeList;
	private List<CostKind> costKinds;
	private Map<String, BigDecimal> values;

	// Import
	private String projectName;
	private String nrIFPAN;
	private String startDate;
	private String finishDate;
	private String projectManager;
	private String responsiblePerson;
	private Project project;
	private Long projectId;
	private SearchResults<ProjectEntry> entries;
	private Long entryId;
	private String balanceCurr;

	// ProjectEntry
	private String dateEntry;
	private String dateEntryFrom;
	private String dateEntryTo;
	private String expenditureDate;
	private String costDate;
	private String costName;
	private Integer costKindId;
	private String gross;
	private String net;
	private String vat;
	private String entryType;
	private String docNr;
	private String demandNr;
	private String description;
	private String projectType;
	private String settlementKind;
	private String plnCost;
	private Integer accountingCostKind;
	public Set<String> currencyExchangeMsgSet = new TreeSet<String>();
	
	public List<String> getSettlementKindsAsList()
	{
		return Arrays.asList(ProjectEntry.settlementKinds);
	}

	private String currency;

	public List<String> getCurrencyKinds()
	{
		return Arrays.asList(ProjectEntry.currencyKinds);
	}

	private String rate;
	private Map<String, KostKindCounter> costCounterMap;

	private BigDecimal blocadeSummary;
	private BigDecimal costSummary;
	private BigDecimal impactSummary;
	private BigDecimal planSummary;
	private BigDecimal toSpend;
	private BigDecimal firstPercent;
	private BigDecimal lastPercent;
	private BigDecimal aparaturaPlan = new BigDecimal(0);
	private BigDecimal aparaturaKoszt = new BigDecimal(0);
	private BigDecimal aparaturaBlokada = new BigDecimal(0);
	private BigDecimal materialyPlan = new BigDecimal(0);
	private BigDecimal materialyKoszt = new BigDecimal(0);
	private BigDecimal materialyBlokada = new BigDecimal(0);
	private BigDecimal uslugiPlan = new BigDecimal(0);
	private BigDecimal uslugiKoszt = new BigDecimal(0);
	private BigDecimal uslugiBlokada = new BigDecimal(0);
	private BigDecimal innePlan = new BigDecimal(0);
	private BigDecimal inneKoszt = new BigDecimal(0);
	private BigDecimal inneBlokada = new BigDecimal(0);
	private BigDecimal podrozePlan = new BigDecimal(0);
	private BigDecimal podrozeKoszt = new BigDecimal(0);
	private BigDecimal podrozeBlokada = new BigDecimal(0);

	private static final String EV_CREATE = "create";

	private Map<String, String> columns = SearchEntryAction.columns;
	private String sortField;
	private boolean ascending;
	private Pager pager;
	private int offset;

	private BigDecimal wynagrodzenieBlokada;
	private BigDecimal wynagrodzeniePlan;
	private BigDecimal wynagrodzenieWplyw;
	private BigDecimal wynagrodzenieKoszt;

	private BigDecimal posrednieBlokada;
	private BigDecimal posredniePlan;
	private BigDecimal posrednieWplyw;
	private BigDecimal posrednieKoszt;

	private String contractNumber;
	private String remarks;
	private String fullGross;
	private String defaultCurrency;

	private FormFile file;
	private AttachmentRevision mostRecentRevision;
	private List<Map<String, Object>> attachments;
	private Long zalacznik_id;
	private List<AccountingCostKind> accountingCostKindsList;
	private String percent;
	private Integer financingInstitutionKind;
	private List<FinancingInstitutionKind> financingInstitutionKinds;
	private String applicationNumber;
	private Boolean clearance;
	private Boolean finalReport;
	private Boolean active;
	private String attName;

	private static final int LIMIT = 10;

	protected void setup()
	{
	    
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doCreate").append(OpenHibernateSession.INSTANCE).append(new ValidateCreate()).append(EV_CREATE, new Create()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").append(OpenHibernateSession.INSTANCE).append(new ValidateCreate()).append(EV_CREATE, new Update()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDelete").append(OpenHibernateSession.INSTANCE).append(new ValidateCreate()).append(EV_CREATE, new Delete()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("addEntry").append(OpenHibernateSession.INSTANCE).append(new ValidateEntry()).append(EV_CREATE, new AddEntry()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDeleteEntry").append(OpenHibernateSession.INSTANCE).append(new ValidateEntry()).append(EV_CREATE, new DeleteEntry()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAddAtta").append(OpenHibernateSession.INSTANCE).append(new AddAtta()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doCostsCharged").append(OpenHibernateSession.INSTANCE).append(new CostsCharged()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);
		
	}

	private class CostsCharged implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (projectId == null)
					throw new EdmException("Nie wybrano projektu");
				if (StringUtils.isEmpty(dateEntryFrom))
					dateEntryFrom = "01-01-1980";

				Date dateFrom = DateUtils.nullSafeParseJsDate(dateEntryFrom);
				Date dateTo = DateUtils.nullSafeParseJsDate(dateEntryTo);

				DSApi.context().begin();
				project = Project.find(projectId);

				if (percent == null)
					throw new EdmException("B��dna warto�� procentu");

				ProjectEntry.Query queryCount = new ProjectEntry.Query(offset, 0);
				queryCount.setProjectId(projectId);
				queryCount.setDateEntryFrom(dateFrom);
				if (dateTo != null)
					queryCount.setDateEntryTo(dateTo);
				queryCount.setNotEqCostKindIdList(new HashSet<Integer>(Arrays.asList(new Integer[] { ProjectEntry.KOSZT_POSREDNI_ID })));
				SearchResults<ProjectEntry> entriesCount = ProjectEntry.getPrejectEntries(queryCount);
				values = new HashMap<String, BigDecimal>();
				setValuesMap(entriesCount, values);
				
				

				ProjectEntry.Query query = new ProjectEntry.Query(offset, LIMIT);
				query.setProjectId(projectId);
				query.setSortField(sortField == null ? "id" : sortField);
				query.setAscending(ascending);
				query.setDateEntryFrom(dateFrom);
				if (dateTo != null)
					query.setDateEntryTo(dateTo);
				query.setCostKindId(ProjectEntry.KOSZT_POSREDNI_ID);
				entries = ProjectEntry.getPrejectEntries(query);
				ProjectEntry entry = null;
				if (entries.totalCount() > 0)
				{
					entry = entries.next();
				}
				else
				{
					entry = new ProjectEntry();
					entry.setProjectId(projectId);
					entry.setDateEntry(new Date());
					entry.setExpenditureDate(new Date());
					entry.setCostName(sm.getString("KosztPosredniAuto"));
					entry.setCostKindId(ProjectEntry.KOSZT_POSREDNI_ID);
					entry.setGross(costSummary);
					entry.setNet(costSummary);
					entry.setVat(new BigDecimal(0));
					entry.setSettlementKind(ProjectEntry.SETTLEMENT_KIND_BRUTTO);
					entry.setCurrency(project.getDefaultCurrency());
					entry.setRate(new BigDecimal(1.0000));
					entry.setEntryType(ProjectEntry.COST);
					entry.setCostDate(new Date());
					entry.create();
				}
				
				percent = percent.replace(',','.'); /** Bugfix #9201 zamiana przecinka na kropke - przecinek nie jest rozpoznawany jako separator miedzy czescia calkowita i ulamkowa */
				BigDecimal pc = new BigDecimal(percent);
				BigDecimal val = costSummary.multiply(pc);
				val = val.divide(new BigDecimal(100));
				val = val.setScale(2, RoundingMode.HALF_UP);
				entry.setGross(val);
				entry.setNet(val);
				entry.setVat(new BigDecimal(0));
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				addActionError("B��d :" + e.getMessage());
				log.error("", e);
			}
		}
	}

	private class AddAtta implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (projectId == null)
					throw new EdmException("Nie wybrano projektu!");
				if (file == null || file.getFile() == null)
					throw new EdmException("Nie wybrano pliku za��cznika!");
				DSApi.context().begin();
				project = Project.find(projectId);
				Attachment atta = new Attachment(attName != null ? attName : "Za��cznik");
				atta.setWparam(projectId);
				atta.setType(Attachment.ATTACHMENT_TYPE_DICTIONARY);
				DSApi.context().session().saveOrUpdate(atta);
				atta.createRevision(file.getFile());
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				addActionError("B��d :" + e.getMessage());
				log.error("", e);
			}
		}
	}

	private class FillForm implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				DSUser[] dpirUsersArray = DSDivision.findByCode("DPiR").getUsers();
				if(dpirUsersArray != null && dpirUsersArray.length > 0){
					dpirUsers = Arrays.asList(dpirUsersArray);
				}
				
				accountingCostKindsList = AccountingCostKind.list();
				financingInstitutionKinds = FinancingInstitutionKind.list();
				costKinds = CostKind.list();
				
				if (dateEntry == null)
					dateEntry = DateUtils.formatCommonDate(new Date());
				
				if (projectId == null)
				{
					if (defaultCurrency == null)
						defaultCurrency = "PLN";
					if (rate == null)
						rate = "1.0000";
				}
				else
				{
					if (StringUtils.isEmpty(dateEntryFrom))
						dateEntryFrom = "01-01-1980";
					
					Date dateFrom = DateUtils.nullSafeParseJsDate(dateEntryFrom);
					Date dateTo = DateUtils.nullSafeParseJsDate(dateEntryTo);
					if (dateTo == null)
						dateTo = new Date(new Date().getTime() + DateUtils.DAY);
					
					dateEntryTo = DateUtils.formatJsDate(dateTo);

					project = Project.find(projectId);
					defaultCurrency = project.getDefaultCurrency();
					if (StringUtils.isBlank(defaultCurrency))
						defaultCurrency = "PLN";
					if (StringUtils.isBlank(balanceCurr))
						balanceCurr = defaultCurrency;

					if (defaultCurrency.equals("EUR") && currency == null)
						currency = "EUR";
					else if (defaultCurrency.equals("PLN") && currency == null)
						currency = "PLN";
					
					if (rate == null)
					{
						if (currency.equals("EUR"))
							rate = Docusafe.getAdditionProperty("kurs_euro");
						else
							rate = "1.0000";
					}
					projectType = project.getProjectType();
					if (dateFrom == null)
						dateFrom = project.getStartDate();
					ProjectEntry.Query query = new ProjectEntry.Query(offset, LIMIT);
					query.setProjectId(projectId);
					query.setSortField(sortField == null ? "id" : sortField);
					query.setAscending(ascending);

					if (!DSApi.context().isAdmin() && !DSApi.context().getDSUser().inDivisionByGuid(DSPermission.PROJECT_READ_SALARY.getName() + "_" + projectId))
					{
						query.setNotEqCostKindIdList(ProjectEntry.wynagrodzeniaCostKinds);
					}
					query.setCostDateFrom(dateFrom);
					if (dateTo != null)
					{
						query.setCostDateTo(dateTo);
					}

					entries = ProjectEntry.getPrejectEntries(query);

					projectManager = project.getProjectManager();
					if(project.getResponsiblePerson() != null){
						responsiblePerson = project.getResponsiblePerson().getName();
					}
					projectName = project.getProjectName();
					nrIFPAN = project.getNrIFPAN();
					if (project.getStartDate() != null)
						startDate = DateUtils.formatJsDate(project.getStartDate());
					if (project.getFinishDate() != null)
						finishDate = DateUtils.formatJsDate(project.getFinishDate());
					contractNumber = project.getContractNumber();
					remarks = project.getRemarks();
					fullGross = project.getFullGross();
					
					if (project.getPercent() != null)
						percent = project.getPercent().toString();
					applicationNumber = project.getApplicationNumber();
					clearance = project.getClearance();
					finalReport = project.getFinalReport();
					active = project.getActive();
					if (project.getFinancingInstitutionKind() != null)
						financingInstitutionKind = project.getFinancingInstitutionKind().getId();

					Set<Attachment> attachmentsList = project.getZalaczniki();
					attachments = new ArrayList<Map<String, Object>>(attachmentsList.size());
					for (Iterator iter = attachmentsList.iterator(); iter.hasNext();)
					{
						Attachment attachment = (Attachment) iter.next();

						if (attachment == null)
						{
							event.getLog().warn("attachment=null, document=");
							continue;
						}

						Map<String, Object> bean = new HashMap<String, Object>();
						bean.put("id", attachment.getId());
						bean.put("title", attachment.getTitle());
						bean.put("cn", attachment.getCn());
						bean.put("editLink", EditAttachmentAction.getLink(attachment.getId()));

						Object[] revs = attachment.getRevisions().toArray();

						if (revs != null && revs.length > 0)
						{
							AttachmentRevision rev = (AttachmentRevision) revs[revs.length - 1];
							bean.put("recentContentLink", ViewAttachmentRevisionAction.getLink(ServletActionContext.getRequest(), rev.getId()));
							bean.put("ctime", rev.getCtime());
							bean.put("size", rev.getSize());
							bean.put("revision", rev.getRevision());
							bean.put("mime", rev.getMime());
							bean.put("icon", MimetypesFileTypeMap.getInstance().getIcon(MimetypesFileTypeMap.getInstance().getContentType(rev.getOriginalFilename())));
							bean.put("author", DSUser.safeToFirstnameLastnameName(rev.getAuthor()));
							bean.put("showRecentContentLink", "/viewserver/viewer.action?id=" + rev.getId());
							bean.put("revisionId", rev.getId());
						}
						attachments.add(bean);
					}

					values = new HashMap<String, BigDecimal>();
					ProjectEntry.Query queryCount = new ProjectEntry.Query(offset, 0);
					queryCount.setProjectId(projectId);
					queryCount.setDateEntryFrom(dateFrom);
					if (dateTo != null)
						queryCount.setDateEntryTo(dateTo);

					SearchResults<ProjectEntry> entriesCount = ProjectEntry.getPrejectEntries(queryCount);
					
					setValuesMap(entriesCount, values);
					
					StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
					String header = sm.getString("PobranoKursyWymiany");
			
					for (String itemMsg:currencyExchangeMsgSet){
					    addActionMessage(header + ": " +itemMsg);
					}

					if (entryId != null)
					{
						ProjectEntry entry = ProjectEntry.find(entryId);
						dateEntry = DateUtils.formatJsDate(entry.getDateEntry());
						if (entry.getExpenditureDate() != null)
							expenditureDate = DateUtils.formatJsDate(entry.getExpenditureDate());
						if (entry.getCostDate() != null)
							costDate = DateUtils.formatJsDate(entry.getCostDate());
						costName = entry.getCostName();
						costKindId = entry.getCostKindId();
						if (entry.getGross() != null)
							gross = entry.getGross().toString();
						if (entry.getNet() != null)
							net = entry.getNet().toString();
						if (entry.getVat() != null)
							vat = entry.getVat().toString();
						currency = entry.getCurrency();
						if (entry.getRate() != null)
							rate = entry.getRate().toString();
						settlementKind = entry.getSettlementKind();
						entryType = entry.getEntryType();
						if (entry.getDocNr() != null)
							docNr = entry.getDocNr().toString();
						if (entry.getDemandNr() != null)
							demandNr = entry.getDemandNr().toString();
						description = entry.getDescription();
						if (entry.getAccountingCostKind() != null)
							accountingCostKind = entry.getAccountingCostKind().getId();
						
						
					}

					Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset)
						{
							return HttpUtils.makeUrl("/record/projects/edit-main.action", new Object[] { "projectId", projectId, "sortField", sortField == null ? "id" : sortField, "ascending", String.valueOf(ascending), "offset", String.valueOf(offset) });
						}
					};

					pager = new Pager(linkVisitor, offset, LIMIT, entries.totalCount(), 10);
					toSpend = impactSummary.subtract(costSummary).subtract(blocadeSummary);

					try
					{
						firstPercent = ((costSummary.add(blocadeSummary).subtract(posrednieBlokada).subtract(posrednieKoszt)).divide(planSummary.subtract(posredniePlan))).multiply(new BigDecimal(100.00));
						lastPercent = ((costSummary.add(blocadeSummary)).divide(planSummary)).multiply(new BigDecimal(100.00));
					}
					catch (java.lang.ArithmeticException ex)
					{
						log.warn("!!!! Lapiemy to: " + ex.getMessage());
					}
				}
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
				try
				{
					DSApi.context().rollback();
				}
				catch (Exception e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}
		}
	}

	private void setValuesMap(SearchResults<ProjectEntry> entriesCount, Map<String, BigDecimal> values) throws EdmException
	{
		blocadeSummary = new BigDecimal(0.00);
		costSummary = new BigDecimal(0.00);
		impactSummary = new BigDecimal(0.00);
		planSummary = new BigDecimal(0.00);
		costCounterMap = new LinkedHashMap<String, EditMainAction.KostKindCounter>();
		wynagrodzenieBlokada = new BigDecimal(0.00);
		wynagrodzenieKoszt = new BigDecimal(0.00);
		wynagrodzeniePlan = new BigDecimal(0.00);
		wynagrodzenieWplyw = new BigDecimal(0.00);
		posrednieBlokada = new BigDecimal(0.00);
		posrednieKoszt = new BigDecimal(0.00);
		posredniePlan = new BigDecimal(0.00);
		posrednieWplyw = new BigDecimal(0.00);
		BigDecimal zero = new BigDecimal(0);
		for (CostKind ck : CostKind.list())
		{
			values.put(ck.getCn() + ProjectEntry.BLOCADE, zero);
			values.put(ck.getCn() + ProjectEntry.COST, zero);
			values.put(ck.getCn() + ProjectEntry.IMPACT, zero);
			values.put(ck.getCn() + ProjectEntry.PLAN, zero);
		}
		for (ProjectEntry entry : Arrays.asList(entriesCount.results()))
		{
			if (entry.getEntryType().equals(ProjectEntry.BLOCADE))
			{
				blocadeSummary = blocadeSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
				if (ProjectEntry.wynagrodzeniaCostKinds.contains(entry.getCostKindId()))
				{
					wynagrodzenieBlokada = wynagrodzenieBlokada.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
				if ("KOSZT_POS".equals(entry.getCostCategory()))
				{
					posrednieBlokada = posrednieBlokada.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
			}
			else if (entry.getEntryType().equals(ProjectEntry.COST))
			{
				costSummary = costSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
				// if("WYNAGRODZENIE".equals(entry.getCostCategory()) ||
				// "ZUS".equals(entry.getCostCategory()))
				if (ProjectEntry.wynagrodzeniaCostKinds.contains(entry.getCostKindId()))
				{
					wynagrodzenieKoszt = wynagrodzenieKoszt.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
				if ("KOSZT_POS".equals(entry.getCostCategory()))
				{
					posrednieKoszt = posrednieKoszt.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
			}
			else if (entry.getEntryType().equals(ProjectEntry.IMPACT))
			{
				impactSummary = impactSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
				// if("WYNAGRODZENIE".equals(entry.getCostCategory()) ||
				// "ZUS".equals(entry.getCostCategory()))
				if (ProjectEntry.wynagrodzeniaCostKinds.contains(entry.getCostKindId()))
				{
					wynagrodzenieWplyw = wynagrodzenieWplyw.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
				if ("KOSZT_POS".equals(entry.getCostCategory()))
				{
					posrednieWplyw = posrednieWplyw.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
			}
			else if (entry.getEntryType().equals(ProjectEntry.PLAN))
			{
				planSummary = planSummary.add(entry.getBalanceAmount(balanceCurr,currencyExchangeMsgSet));
				if (ProjectEntry.wynagrodzeniaCostKinds.contains(entry.getCostKindId()))
				{
					wynagrodzeniePlan = wynagrodzeniePlan.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
				if ("KOSZT_POS".equals(entry.getCostCategory()))
				{
					posredniePlan = posredniePlan.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
				}
			}
			if (ProjectEntry.wynagrodzeniaCostKinds.contains(entry.getCostKindId()) || "KOSZT_POS".equals(entry.getCostCategory()) || entry.getEntryType().equals(ProjectEntry.IMPACT))
				continue;
			KostKindCounter tempCost = costCounterMap.get("" + entry.getCostKindId());
			if (tempCost == null)
				tempCost = new KostKindCounter();
			tempCost.addValue(entry);
			costCounterMap.put("" + entry.getCostKindId(), tempCost);
		}
	}

	public class KostKindCounter
	{
		private String costCategory;
		BigDecimal blocadeSummary;
		BigDecimal costSummary;
		BigDecimal impactSummary;
		BigDecimal planSummary;

		public KostKindCounter()
		{
			blocadeSummary = new BigDecimal(0.00);
			costSummary = new BigDecimal(0.00);
			impactSummary = new BigDecimal(0.00);
			planSummary = new BigDecimal(0.00);
		}

		public void addValue(ProjectEntry entry)
		{

			if (entry.getEntryType().equals(ProjectEntry.BLOCADE))
			{
				blocadeSummary = blocadeSummary.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
			}
			else if (entry.getEntryType().equals(ProjectEntry.COST))
			{
				costSummary = costSummary.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
			}
			else if (entry.getEntryType().equals(ProjectEntry.IMPACT))
			{
				impactSummary = impactSummary.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
			}
			else if (entry.getEntryType().equals(ProjectEntry.PLAN))
			{
				planSummary = planSummary.add(entry.getBalanceAmountByCurrency(balanceCurr,currencyExchangeMsgSet));
			}
			this.costCategory = entry.getCostKind();
		}

		public String getProcent()
		{
			try
			{
				DecimalFormat decimalFormat = new DecimalFormat("###########0.0");
				BigDecimal sto = new BigDecimal(100);
				BigDecimal zero = new BigDecimal(0.00);
				if (planSummary.equals(zero))
					return sto.toString();
				
				BigDecimal procent = this.costSummary.add(blocadeSummary).multiply(sto).divide(planSummary);
				return procent.setScale(1, RoundingMode.HALF_UP).toString();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "b��d";
			}
		}

		public String getCostCategory()
		{
			return costCategory;
		}

		public void setCostCategory(String costCategory)
		{
			this.costCategory = costCategory;
		}

		public BigDecimal getBlocadeSummary()
		{
			return blocadeSummary.setScale(2, RoundingMode.HALF_UP);
		}

		public void setBlocadeSummary(BigDecimal blocadeSummary)
		{
			this.blocadeSummary = blocadeSummary;
		}

		public BigDecimal getCostSummary()
		{
			return costSummary.setScale(2, RoundingMode.HALF_UP);
		}

		public void setCostSummary(BigDecimal costSummary)
		{
			this.costSummary = costSummary;
		}

		public BigDecimal getImpactSummary()
		{
			return impactSummary.setScale(2, RoundingMode.HALF_UP);
		}

		public void setImpactSummary(BigDecimal impactSummary)
		{
			this.impactSummary = impactSummary;
		}

		public BigDecimal getPlanSummary()
		{
			return planSummary.setScale(2, RoundingMode.HALF_UP);
		}

		public void setPlanSummary(BigDecimal planSummary)
		{
			this.planSummary = planSummary;
		}
	}

	public String prettyPrint(Double d)
	{
		try
		{
			if (d == null || d <= 0)
				return "0.00";
			DecimalFormat decimalFormat = new DecimalFormat("###########0.00");
			return decimalFormat.format(d);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "0.00";
		}
	}

	public String procent(Double costSummary, Double blocadeSummary, Double planSummary)
	{
		try
		{
			DecimalFormat decimalFormat = new DecimalFormat("###########0.00");
			if (planSummary.doubleValue() == 0)
				return "100";
			double d = ((costSummary + blocadeSummary) * 100) / planSummary;
			return decimalFormat.format(d);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "100";
		}
	}

	private void setValue(BigDecimal plan, BigDecimal koszt, BigDecimal blokada, ProjectEntry entry)
	{
		if (entry.getEntryType().equals(ProjectEntry.BLOCADE))
			blokada = blokada.add(entry.getNet());
		else if (entry.getEntryType().equals(ProjectEntry.COST))
			koszt = koszt.add(entry.getNet());
		else if (entry.getEntryType().equals(ProjectEntry.PLAN))
			plan = plan.add(entry.getNet());
	}

	private class Create implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				Project project = new Project();
				setProjectValue(project);
				project.create();
				projectPermissions(project);
				DSApi.context().commit();
				DataBaseEnumField.reloadForTable("dsg_ifpan_proj_name");
				event.setResult("list-projects");
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}
		}
	}

	public void projectPermissions(Project project) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "PROJECT_READ_" + project.getId(), ObjectPermission.GROUP, "Projekty - odczyt - " + project.getId()));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "PROJECT_MODIFY_" + project.getId(), ObjectPermission.GROUP, "Projekty - modyfikacja - " + project.getId()));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, DSPermission.PROJECT_READ_SALARY.getName() + project.getId(), ObjectPermission.GROUP, "Projekty - odczyt wynagrodze� - " + project.getId()));
		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(project, groupName, perm.getSubject());
			}

			DSApi.context().session().flush();
		}
	}

	/**
	 * Metoda odnajduje grupe jesli nie istnieje to tworzy
	 * 
	 * @param name
	 * @param guid
	 * @throws EdmException
	 */
	protected DSDivision createOrFind(Project project, String name, String guid) throws EdmException
	{
		try
		{
			return DSDivision.find(guid.trim());
		}
		catch (DivisionNotFoundException e)
		{
			DSDivision parent = null;
			try {
				parent= DSDivision.findByName(DSDivision.UP);
			}
			catch (DivisionNotFoundException ee)
			{
				parent= DSDivision.find(DSDivision.ROOT_GUID);
			}
			return parent.createGroup(name, guid);
		}
	}

	private class AddEntry implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				ProjectEntry entry = null;
				DSApi.context().begin();
				if (entryId != null)
				{

					entry = ProjectEntry.find(entryId);
					if (entry.getEntryType().equals(ProjectEntry.COST))
					{
						entry.setCostDate(new Date());
					}
					setEntryValue(entry);

				}
				else
				{
					entry = new ProjectEntry();
					setEntryValue(entry);
					if (entry.getCostDate() == null)
					{
						entry.setCostDate(new Date());
					}
					Project.find(projectId).addEntry(entry);
				}
				DSApi.context().commit();
				dateEntry = null;
				entryId = null;
				expenditureDate = null;
				costDate = null;
				costName = null;
				costKindId = null;
				gross = null;
				net = null;
				vat = null;
				entryType = null;
				rate = null;
				currency = null;
				settlementKind = null;
				docNr = null;
				demandNr = null;
				description = null;
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	private class Update implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				project = Project.find(projectId);
				setProjectValue(project);
				DSApi.context().commit();
				DataBaseEnumField.reloadForTable("dsg_ifpan_proj_name");
				// event.setResult("list-projects");

				dateEntry = null;
				entryId = null;
				expenditureDate = null;
				costDate = null;
				costName = null;
				costKindId = null;
				gross = null;
				net = null;
				vat = null;
				entryType = null;
				rate = null;
				currency = null;
				settlementKind = null;
				docNr = null;
				demandNr = null;
				description = null;

			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}
		}
	}

	private class Delete implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				project = Project.find(projectId);
				UserFactory.getInstance().deleteDivision(DSPermission.PROJECT_READ.getName() + "_" + project.getId(), null);
				UserFactory.getInstance().deleteDivision(DSPermission.PROJECT_MODIFY.getName() + "_" + project.getId(), null);
				UserFactory.getInstance().deleteDivision(DSPermission.PROJECT_READ_SALARY.getName() + "_" + project.getId(), null);
				project.delete();
				DSApi.context().commit();
				DataBaseEnumField.reloadForTable("dsg_ifpan_proj_name");
				event.setResult("list-projects");
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}
		}
	}

	private class DeleteEntry implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				ProjectEntry.find(entryId).delete();
				DSApi.context().commit();
				entryId = null;
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}

			dateEntry = null;
			entryId = null;
			expenditureDate = null;
			costDate = null;
			costName = null;
			costKindId = null;
			gross = null;
			net = null;
			vat = null;
			entryType = null;
			docNr = null;
			demandNr = null;
			description = null;
		}
	}

	private class ValidateEntry implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			boolean canModify = false;
			try
			{
				String user = DSApi.context().getPrincipalName();
				for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
				{
					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()))
						canModify = true;
					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName() + "_" + projectId))
						canModify = true;
				}

				if (!canModify)
					addActionError("U�ytkownik nie ma uprawnie� do tworzenia/modyfikacji projekt�w");
				else
				{
					Date _date = DateUtils.nullSafeParseJsDate(dateEntry);
					if (_date == null)
						addActionError("Nie podano daty wpisu lub data jest nieprawid�owa");
					if (costKindId == null && entryType != null && !ProjectEntry.IMPACT.equals(entryType))
						addActionError("Nie wybrano rodzaju kosztu");
					if (entryType == null)
						addActionError("Nie wybrano rodzaju wpisu");

					BigDecimal _gross = null;
					BigDecimal _net = null;
					BigDecimal _vat = null;
					char CHAR_COMMA = ',';
					char CHAR_DOT = '.';

					if (StringUtils.isEmpty(gross) && "BRUTTO".equals(settlementKind))
					{
						addActionError("Nie podano kwoty brutto");
					}
					else if (!StringUtils.isEmpty(gross))
					{
						try
						{
							gross = gross.trim().replace(CHAR_COMMA, CHAR_DOT).replace(" ", "");
							int grossLength = -1;
							grossLength = (gross.indexOf(CHAR_DOT) == -1) ? gross.length() : grossLength;
							if (grossLength > 17)
								addActionError("Liczba cyfr kwoty brutto jest wi�ksza od 17-tu");
							_gross = new BigDecimal(gross);
						}
						catch (Exception ex)
						{
							addActionError("Podana suma brutto jest nieprawid��owa");
						}
					}
					if (StringUtils.isEmpty(net) && "NETTO".equals(settlementKind))
					{
						addActionError("Nie podano kwoty netto");
					}
					else if (!StringUtils.isEmpty(net))
					{
						try
						{
							net = net.trim().replace(CHAR_COMMA, CHAR_DOT).replace(" ", "");
							int netLength = -1;
							netLength = (net.indexOf(CHAR_DOT) == -1) ? net.length() : netLength;
							if (netLength > 17)
								addActionError("Liczba cyfr kwoty netto jest wi�ksza od 17-tu");
							_net = new BigDecimal(net);
						}
						catch (Exception ex)
						{
							log.error(ex.getMessage(), ex);
							addActionError("Podana suma netto jest nieprawid��owa");
						}
					}
					else
						net = "0.0";


					if (StringUtils.isEmpty(rate))
					{
						rate = "1.0000";

					}
					else
					{
						rate = rate.trim().replace(CHAR_COMMA, CHAR_DOT).replace(" ", "");
					}
					
					//rateDouble = Double.parseDouble(rate);

					if (_gross != null && _net != null && _gross.movePointRight(2).compareTo(_net.movePointRight(2)) < 0)
						addActionError("Suma netto jest wi�ksza od sumy brutto");

					if (vat != null)
					{
						try
						{
							vat = vat.trim().replace(CHAR_COMMA, CHAR_DOT).replace(" ", "");
							int vatLength = -1;
							vatLength = (vat.indexOf(CHAR_DOT) == -1) ? vat.length() : vatLength;
							if (vatLength > 17)
								addActionError("Liczba cyfr kwoty netto jest wi�ksza od 17-tu");
							_vat = new BigDecimal(vat);
						}
						catch (Exception ex)
						{
							log.error(ex.getMessage(), ex);
							addActionError("Podana stawka VAT jest niepoprawna");
						}
					}
				}

				if (hasActionErrors())
					event.skip(EV_CREATE);

			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}

		}
	}

	private class ValidateCreate implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			boolean canModify = false;
			try
			{
				String user = DSApi.context().getPrincipalName();
				for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
				{
					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()))
						canModify = true;
					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName() + "_" + projectId))
						canModify = true;
				}
				if (!canModify)
					addActionError("U�ytkownik nie ma uprawnie� do tworzenia/modyfikacji projektow");
				else
				{
					if (StringUtils.isEmpty(projectManager))
						addActionError("Nie wybrano Kierownika Projektu");
					if (StringUtils.isEmpty(projectName))
						addActionError("Nie podano nazwy projektu");
					if (StringUtils.isEmpty(nrIFPAN))
						addActionError("Nie podano numeru zlecenia IF PAN");

					Date _startDate = DateUtils.nullSafeParseJsDate(startDate);

					if (_startDate == null)
						addActionError("Nie podano daty rozpocz�cia lub data jest nieprawid�owa");
				}
				if (hasActionErrors())
					event.skip(EV_CREATE);
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				addActionError(e.getMessage());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e2)
				{
					log.error(e.getMessage(), e);
					addActionError(e2.getMessage());
				}
			}
		}
	}

	private void setProjectValue(Project project) throws EdmException
	{
		project.setNrIFPAN(nrIFPAN);
		log.info("ProjectManager: {}", projectManager);
		project.setProjectManager(projectManager);
		project.setProjectName(projectName);
		project.setStartDate(DateUtils.nullSafeParseJsDate(startDate));
		project.setFinishDate(DateUtils.nullSafeParseJsDate(finishDate));
		project.setProjectType(projectType);
		project.setDefaultCurrency(defaultCurrency);
		project.setContractNumber(contractNumber);
		project.setFullGross(fullGross);
		project.setRemarks(remarks);
		if(responsiblePerson != null){
			project.setResponsiblePerson(DSUser.findByUsername(responsiblePerson));
		}
		if (percent != null)
            project.setPercent(Float.parseFloat(percent.replace(",",".").replace(" ","")));

		project.setClearance(clearance);
		project.setFinalReport(finalReport);
		project.setActive(active);
		project.setApplicationNumber(applicationNumber);
		if (financingInstitutionKind != null)
			project.setFinancingInstitutionKind(FinancingInstitutionKind.find(financingInstitutionKind));
	}

	private void setEntryValue(ProjectEntry entry) throws EdmException
	{
		BigDecimal netB = null;
		BigDecimal grossB = null;
		BigDecimal vatB = null;
		if (StringUtils.isNotEmpty(net))
			netB = new BigDecimal(net).setScale(2, BigDecimal.ROUND_DOWN);
		if (StringUtils.isNotEmpty(vat))
			vatB = new BigDecimal(vat).setScale(2, BigDecimal.ROUND_DOWN);
		if (StringUtils.isNotEmpty(gross))
			grossB = new BigDecimal(gross).setScale(2, BigDecimal.ROUND_DOWN);
		else if (StringUtils.isNotEmpty(net) && vatB != null)
		{
			grossB = netB.add(vatB).setScale(2, BigDecimal.ROUND_DOWN);
		}
		
		entry.setDateEntry(DateUtils.nullSafeParseJsDate(dateEntry));
		entry.setExpenditureDate(DateUtils.nullSafeParseJsDate(expenditureDate));
		entry.setCostDate(DateUtils.nullSafeParseJsDate(costDate));
		entry.setCostName(costName);
		entry.setCostKindId(costKindId);
		if (grossB != null)
			entry.setGross(grossB);
		else
			entry.setGross(netB);
		entry.setNet(netB);
		entry.setVat(vatB);
		entry.setSettlementKind(settlementKind);
		entry.setCurrency(currency);
		entry.setRate(new BigDecimal(rate).setScale(4, RoundingMode.HALF_UP));
		entry.setEntryType(entryType);
		if (docNr != null)
			entry.setDocNr(docNr);
		if (demandNr != null)
			entry.setDemandNr(demandNr);
		entry.setDescription(description);
		if (accountingCostKind != null)
			entry.setAccountingCostKind(AccountingCostKind.find(accountingCostKind));
		entry.setMtime(new Date());
	}

	public List<DSUser> getUsers()
	{
		return users;
	}

	public void setUsers(List<DSUser> users)
	{
		this.users = users;
	}

	public String getProjectName()
	{
		return projectName;
	}

	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}

	public String getNrIFPAN()
	{
		return nrIFPAN;
	}

	public void setNrIFPAN(String nrIFPAN)
	{
		this.nrIFPAN = nrIFPAN;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getFinishDate()
	{
		return finishDate;
	}

	public void setFinishDate(String finishDate)
	{
		this.finishDate = finishDate;
	}

	public String getProjectManager()
	{
		return projectManager;
	}

	public void setProjectManager(String projectManager)
	{
		this.projectManager = projectManager;
	}

	public Long getProjectId()
	{
		return projectId;
	}

	public void setProjectId(Long projectId)
	{
		this.projectId = projectId;
	}

	public Project getProject()
	{
		return project;
	}

	public void setProject(Project project)
	{
		this.project = project;
	}

	public String getDateEntry()
	{
		return dateEntry;
	}

	public void setDateEntry(String dateEntry)
	{
		this.dateEntry = dateEntry;
	}

	public String getExpenditureDate()
	{
		return expenditureDate;
	}

	public void setExpenditureDate(String expenditureDate)
	{
		this.expenditureDate = expenditureDate;
	}

	public String getCostDate()
	{
		return costDate;
	}

	public void setCostDate(String costDate)
	{
		this.costDate = costDate;
	}

	public String getCostName()
	{
		return costName;
	}

	public void setCostName(String costName)
	{
		this.costName = costName;
	}

	public Integer getCostKindId()
	{
		return costKindId;
	}

	public void setCostKindId(Integer costKindId)
	{
		this.costKindId = costKindId;
	}

	public String getGross()
	{
		return gross;
	}

	public void setGross(String gross)
	{
		this.gross = gross;
	}

	public String getNet()
	{
		return net;
	}

	public void setNet(String net)
	{
		this.net = net;
	}

	public String getVat()
	{
		return vat;
	}

	public void setVat(String vat)
	{
		this.vat = vat;
	}

	public String getDocNr()
	{
		return docNr;
	}

	public void setDocNr(String docNr)
	{
		this.docNr = docNr;
	}

	public String getDemandNr()
	{
		return demandNr;
	}

	public void setDemandNr(String demandNr)
	{
		this.demandNr = demandNr;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public SearchResults<ProjectEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(SearchResults<ProjectEntry> entries)
	{
		this.entries = entries;
	}

	public String getEntryType()
	{
		return entryType;
	}

	public void setEntryType(String entryType)
	{
		this.entryType = entryType;
	}

	public Map<String, String> getEntryTypeList()
	{
		if (entryTypeList == null)
		{
			entryTypeList = new HashMap<String, String>(4);
			entryTypeList.put(ProjectEntry.BLOCADE, "Blokada");
			entryTypeList.put(ProjectEntry.COST, "Koszt");
			entryTypeList.put(ProjectEntry.PLAN, "Plan");
			entryTypeList.put(ProjectEntry.IMPACT, "Wp�yw");
		}
		return entryTypeList;
	}

	public List<CostKind> getCostKinds()
	{
		return costKinds;
	}

	public void setCostKinds(List<CostKind> costKinds)
	{
		this.costKinds = costKinds;
	}

	public void setEntryTypeList(Map<String, String> entryTypeList)
	{
		this.entryTypeList = entryTypeList;
	}

	public BigDecimal getBlocadeSummary()
	{
		return blocadeSummary.setScale(2, RoundingMode.HALF_UP);
	}

	public void setBlocadeSummary(BigDecimal blocadeSummary)
	{
		this.blocadeSummary = blocadeSummary;
	}

	public BigDecimal getCostSummary()
	{
		return costSummary.setScale(2, RoundingMode.HALF_UP);
	}

	public void setCostSummary(BigDecimal costSummary)
	{
		this.costSummary = costSummary;
	}

	public BigDecimal getImpactSummary()
	{
		return impactSummary.setScale(2, RoundingMode.HALF_UP);
	}

	public void setImpactSummary(BigDecimal impactSummary)
	{
		this.impactSummary = impactSummary;
	}

	public BigDecimal getPlanSummary()
	{
		return planSummary.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPlanSummary(BigDecimal planSummary)
	{
		this.planSummary = planSummary;
	}

	public Long getEntryId()
	{
		return entryId;
	}

	public void setEntryId(Long entryId)
	{
		this.entryId = entryId;
	}

	public Map<String, String> getColumns()
	{
		return columns;
	}

	public void setColumns(Map<String, String> columns)
	{
		this.columns = columns;
	}

	public String getSortField()
	{
		return sortField;
	}

	public void setSortField(String sortField)
	{
		this.sortField = sortField;
	}

	public boolean isAscending()
	{
		return ascending;
	}

	public void setAscending(boolean ascending)
	{
		this.ascending = ascending;
	}

	public Pager getPager()
	{
		return pager;
	}

	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	public int getOffset()
	{
		return offset;
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public static int getLimit()
	{
		return LIMIT;
	}

	public String getDateEntryFrom()
	{
		return dateEntryFrom;
	}

	public void setDateEntryFrom(String dateEntryFrom)
	{
		this.dateEntryFrom = dateEntryFrom;
	}

	public String getDateEntryTo()
	{
		return dateEntryTo;
	}

	public void setDateEntryTo(String dateEntryTo)
	{
		this.dateEntryTo = dateEntryTo;
	}

	public String getProjectType()
	{
		return projectType;
	}

	public void setProjectType(String projectType)
	{
		this.projectType = projectType;
	}

	public BigDecimal getAparaturaPlan()
	{
		return aparaturaPlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setAparaturaPlan(BigDecimal aparaturaPlan)
	{
		this.aparaturaPlan = aparaturaPlan;
	}

	public BigDecimal getAparaturaKoszt()
	{
		return aparaturaKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setAparaturaKoszt(BigDecimal aparaturaKoszt)
	{
		this.aparaturaKoszt = aparaturaKoszt;
	}

	public BigDecimal getAparaturaBlokada()
	{
		return aparaturaBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setAparaturaBlokada(BigDecimal aparaturaBlokada)
	{
		this.aparaturaBlokada = aparaturaBlokada;
	}

	public BigDecimal getMaterialyPlan()
	{
		return materialyPlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setMaterialyPlan(BigDecimal materialyPlan)
	{
		this.materialyPlan = materialyPlan;
	}

	public BigDecimal getMaterialyKoszt()
	{
		return materialyKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setMaterialyKoszt(BigDecimal materialyKoszt)
	{
		this.materialyKoszt = materialyKoszt;
	}

	public BigDecimal getMaterialyBlokada()
	{
		return materialyBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setMaterialyBlokada(BigDecimal materialyBlokada)
	{
		this.materialyBlokada = materialyBlokada;
	}

	public BigDecimal getUslugiPlan()
	{
		return uslugiPlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setUslugiPlan(BigDecimal uslugiPlan)
	{
		this.uslugiPlan = uslugiPlan;
	}

	public BigDecimal getUslugiKoszt()
	{
		return uslugiKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setUslugiKoszt(BigDecimal uslugiKoszt)
	{
		this.uslugiKoszt = uslugiKoszt;
	}

	public BigDecimal getUslugiBlokada()
	{
		return uslugiBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setUslugiBlokada(BigDecimal uslugiBlokada)
	{
		this.uslugiBlokada = uslugiBlokada;
	}

	public BigDecimal getInnePlan()
	{
		return innePlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setInnePlan(BigDecimal innePlan)
	{
		this.innePlan = innePlan;
	}

	public BigDecimal getInneKoszt()
	{
		return inneKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setInneKoszt(BigDecimal inneKoszt)
	{
		this.inneKoszt = inneKoszt;
	}

	public BigDecimal getInneBlokada()
	{
		return inneBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setInneBlokada(BigDecimal inneBlokada)
	{
		this.inneBlokada = inneBlokada;
	}

	public BigDecimal getPodrozePlan()
	{
		return podrozePlan.setScale(2, RoundingMode.HALF_UP);
	}

	public List<String> getProjectTypesAsList()
	{
		return Project.projectTypesAsList();
	}

	public void setPodrozePlan(BigDecimal podrozePlan)
	{
		this.podrozePlan = podrozePlan;
	}

	public BigDecimal getPodrozeKoszt()
	{
		return podrozeKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPodrozeKoszt(BigDecimal podrozeKoszt)
	{
		this.podrozeKoszt = podrozeKoszt;
	}

	public BigDecimal getPodrozeBlokada()
	{
		return podrozeBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPodrozeBlokada(BigDecimal podrozeBlokada)
	{
		this.podrozeBlokada = podrozeBlokada;
	}

	public String getSettlementKind()
	{
		return settlementKind;
	}

	public void setSettlementKind(String settlementKind)
	{
		this.settlementKind = settlementKind;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

	public String getRate()
	{
		return rate;
	}

	public String getBalanceCurr()
	{
		return balanceCurr;
	}

	public void setBalanceCurr(String balanceCurr)
	{
		this.balanceCurr = balanceCurr;
	}

	public Map<String, BigDecimal> getValues()
	{
		return values;
	}

	public void setValues(Map<String, BigDecimal> values)
	{
		this.values = values;
	}

	public Map<String, KostKindCounter> getCostCounterMap()
	{
		return costCounterMap;
	}

	public void setCostCounterMap(Map<String, KostKindCounter> costCounterMap)
	{
		this.costCounterMap = costCounterMap;
	}

	public BigDecimal getWynagrodzenieBlokada()
	{
		return wynagrodzenieBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setWynagrodzenieBlokada(BigDecimal wynagrodzenieBlokada)
	{
		this.wynagrodzenieBlokada = wynagrodzenieBlokada;
	}

	public BigDecimal getWynagrodzeniePlan()
	{
		return wynagrodzeniePlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setWynagrodzeniePlan(BigDecimal wynagrodzeniePlan)
	{
		this.wynagrodzeniePlan = wynagrodzeniePlan;
	}

	public BigDecimal getWynagrodzenieWplyw()
	{
		return wynagrodzenieWplyw.setScale(2, RoundingMode.HALF_UP);
	}

	public void setWynagrodzenieWplyw(BigDecimal wynagrodzenieWplyw)
	{
		this.wynagrodzenieWplyw = wynagrodzenieWplyw;
	}

	public BigDecimal getWynagrodzenieKoszt()
	{
		return wynagrodzenieKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setWynagrodzenieKoszt(BigDecimal wynagrodzenieKoszt)
	{
		this.wynagrodzenieKoszt = wynagrodzenieKoszt;
	}

	public String getContractNumber()
	{
		return contractNumber;
	}

	public void setContractNumber(String contractNumber)
	{
		this.contractNumber = contractNumber;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getFullGross()
	{
		return fullGross;
	}

	public void setFullGross(String fullGross)
	{
		this.fullGross = fullGross;
	}

	public String getDefaultCurrency()
	{
		return defaultCurrency;
	}

	public void setDefaultCurrency(String defaultCurrency)
	{
		this.defaultCurrency = defaultCurrency;
	}

	public BigDecimal getPosrednieBlokada()
	{
		return posrednieBlokada.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPosrednieBlokada(BigDecimal posrednieBlokada)
	{
		this.posrednieBlokada = posrednieBlokada;
	}

	public BigDecimal getPosredniePlan()
	{
		return posredniePlan.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPosredniePlan(BigDecimal posredniePlan)
	{
		this.posredniePlan = posredniePlan;
	}

	public BigDecimal getPosrednieWplyw()
	{
		return posrednieWplyw.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPosrednieWplyw(BigDecimal posrednieWplyw)
	{
		this.posrednieWplyw = posrednieWplyw;
	}

	public BigDecimal getPosrednieKoszt()
	{
		return posrednieKoszt.setScale(2, RoundingMode.HALF_UP);
	}

	public void setPosrednieKoszt(BigDecimal posrednieKoszt)
	{
		this.posrednieKoszt = posrednieKoszt;
	}

	public FormFile getFile()
	{
		return file;
	}

	public void setFile(FormFile file)
	{
		this.file = file;
	}

	public AttachmentRevision getMostRecentRevision()
	{
		return mostRecentRevision;
	}

	public void setMostRecentRevision(AttachmentRevision mostRecentRevision)
	{
		this.mostRecentRevision = mostRecentRevision;
	}

	public Long getZalacznik_id()
	{
		return zalacznik_id;
	}

	public void setZalacznik_id(Long zalacznik_id)
	{
		this.zalacznik_id = zalacznik_id;
	}

	public Integer getAccountingCostKind()
	{
		return accountingCostKind;
	}

	public void setAccountingCostKind(Integer accountingCostKind)
	{
		this.accountingCostKind = accountingCostKind;
	}

	public List<AccountingCostKind> getAccountingCostKindsList()
	{
		return accountingCostKindsList;
	}

	public void setAccountingCostKindsList(List<AccountingCostKind> accountingCostKindsList)
	{
		this.accountingCostKindsList = accountingCostKindsList;
	}

	public String getPercent()
	{
		return percent;
	}

	public void setPercent(String percent)
	{
		this.percent = percent;
	}

	public String getApplicationNumber()
	{
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber)
	{
		this.applicationNumber = applicationNumber;
	}

	public Boolean getClearance()
	{
		return clearance;
	}

	public void setClearance(Boolean clearance)
	{
		this.clearance = clearance;
	}

	public Boolean getFinalReport()
	{
		return finalReport;
	}

	public void setFinalReport(Boolean finalReport)
	{
		this.finalReport = finalReport;
	}

	public List<FinancingInstitutionKind> getFinancingInstitutionKinds()
	{
		return financingInstitutionKinds;
	}

	public void setFinancingInstitutionKinds(List<FinancingInstitutionKind> financingInstitutionKinds)
	{
		this.financingInstitutionKinds = financingInstitutionKinds;
	}

	public void setFinancingInstitutionKind(Integer financingInstitutionKind)
	{
		this.financingInstitutionKind = financingInstitutionKind;
	}

	public Integer getFinancingInstitutionKind()
	{
		return financingInstitutionKind;
	}

	public List<Map<String, Object>> getAttachments()
	{
		return attachments;
	}

	public void setAttachments(List<Map<String, Object>> attachments)
	{
		this.attachments = attachments;
	}

	public String getAttName()
	{
		return attName;
	}

	public void setAttName(String attName)
	{
		this.attName = attName;
	}

	public BigDecimal getToSpend()
	{
		if (toSpend != null)
		return toSpend.setScale(2, RoundingMode.HALF_UP);
		else return toSpend;
	}

	public void setToSpend(BigDecimal toSpend)
	{
		this.toSpend = toSpend;
	}

	public BigDecimal getFirstPercent()
	{
		return firstPercent;
	}

	public void setFirstPercent(BigDecimal firstPercent)
	{
		this.firstPercent = firstPercent;
	}

	public BigDecimal getLastPercent()
	{
		return lastPercent;
	}

	public void setLastPercent(BigDecimal lastPercent)
	{
		this.lastPercent = lastPercent;
	}

	public String getPlnCost()
	{
		return plnCost;
	}

	public void setPlnCost(String plnCost)
	{
		this.plnCost = plnCost;
	}

	public void setRate(String rate)
	{
		this.rate = rate;
	}

	public String getResponsiblePerson() {
		return responsiblePerson;
	}

	public void setResponsiblePerson(String responsiblePerson) {
		this.responsiblePerson = responsiblePerson;
	}

	public List<DSUser> getDpirUsers() {
		return dpirUsers;
	}

	public void setDpirUsers(List<DSUser> dpirUsers) {
		this.dpirUsers = dpirUsers;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
}