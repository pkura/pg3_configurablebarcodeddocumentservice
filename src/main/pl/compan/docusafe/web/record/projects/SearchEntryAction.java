package pl.compan.docusafe.web.record.projects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.record.projects.CostKind;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class SearchEntryAction extends EventActionSupport
{

	private Logger log = LoggerFactory.getLogger(SearchEntryAction.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(pl.compan.docusafe.web.record.projects.SearchEntryAction.class.getPackage().getName(), null);

	private int offset;
	private static final int LIMIT = 10;
	private String sortField;
	private boolean ascending;
	private int count;

	private Pager pager;

	private SearchResults<ProjectEntry> entries;

	private Map<String, String> entryTypeList;
	private List<CostKind> costKinds;

	private String projectId;

	private String dateEntryFrom;
	private String dateEntryTo;
	private String expenditureDateFrom;
	private String expenditureDateTo;
	private String costDateFrom;
	private String costDateTo;
	private String costName;
	private Integer costKindId;
	private BigDecimal grossFrom;
	private BigDecimal grossTo;
	private BigDecimal netFrom;
	private BigDecimal netTo;
	private BigDecimal vat;
	private String entryType;
	private String docNr;
	private String demandNr;
	private String description;
	private String printUrl;
	private String importUrl;
	
	//plik z wpisami do zaimportowania
	private FormFile fileXls;

	private BigDecimal netSum;

	boolean canRead = false;
	boolean canModify = false;
	public boolean omitLimit;
	public String actionName;
	public List<Project> projects;
	
	

	public static Map<String, String> columns = new LinkedHashMap<String, String>();
	static
	{
		columns.put("id", "Id");
		columns.put("projectId", "Projekt");
		columns.put("entryNumber", "Numer blokady");
		columns.put("dateEntry", "Data");
		columns.put("expenditureDate", "Data wydatku");
		columns.put("costDate", "Data kosztu");
		columns.put("costName", "Nazwa kosztu");
		columns.put("costKindId", "Rodzaj kosztu");
		columns.put("gross", "Kw. brutto w walucie");
		columns.put("net", "Kw. netto w walucie");
		columns.put("vat", "Vat w walucie");
		columns.put("entryType", "Rodzaj wpisu");
		columns.put("docNr", "Nr dokumentu");
		columns.put("demandNr", "Nr zapotrzebowania");
		columns.put("description", "Opis");
		columns.put("settlementKind", "Rozliczenie");
		columns.put("currency", "Waluta");
		columns.put("rate", "Kurs");
		columns.put("wartoscPln", "Warto�� PLN");
		columns.put("mtime", "Data modyfikacji");
	}

	protected void setup()
	{
		
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").append(OpenHibernateSession.INSTANCE).append(null, new Search()).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doGenerateXls").append(OpenHibernateSession.INSTANCE).append(null, new Search()).append(null, new GenerateXLS()).appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doImportXls").append(OpenHibernateSession.INSTANCE).append(null, new ImportXLS()).append(null, new Search()).appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TESTUJE JREBELA + DEBUGGERA");
				System.out.println("JREBEL + DEBUGGER");
				costKinds = CostKind.list();
				projects = Project.list("nrIFPAN", true, null, null);
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}
	
	private class ImportXLS implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! TUTAJ JEST IMPORT Z XLS");
				log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! TUTAJ JEST IMPORT Z XLS: " + fileXls.getName());
				
				DateFormat df = DateFormat.getDateTimeInstance();
				DateFormat dff = DateFormat.getDateInstance();
				File f = fileXls.getFile();
				HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(f)); 
				HSSFSheet sheet = wb.getSheetAt(0);
				Iterator<Row> rowIt = sheet.rowIterator();
				
				DSApi.context().begin();
				
				Map<String, CostKind> kinds = new HashMap<String, CostKind>();
				for(CostKind tempKind: CostKind.list())
				{
					kinds.put(tempKind.getTitle(), tempKind);
				}
				
				Map<String, String> reversed = new HashMap<String, String>();
				for(String str: ProjectEntry.entryTypeList.keySet())
				{
					reversed.put(ProjectEntry.entryTypeList.get(str), str);
				}
				rowIt.next();
				int nrWiersza = 0;
				while(rowIt.hasNext())
				{
					try
					{
						nrWiersza++;
						Row row = rowIt.next();
						Cell cell = row.getCell(0);

						String projectId = getStringValueFromCell(row.getCell(1));  //project id
						String blockNr =  getStringValueFromCell(row.getCell(2)); //numer blokady
						String data =  getStringValueFromCell(row.getCell(3));
//						Date dataDate = row.getCell(3).getDateCellValue();
						String dataWydatku =  getStringValueFromCell(row.getCell(4));
//						Date dataWydatkuDate = row.getCell(4).getDateCellValue();
						String dataKosztu =  getStringValueFromCell(row.getCell(5));
//						Date dataKosztuDate = row.getCell(5).getDateCellValue();
						String nazawaKosztu =  getStringValueFromCell(row.getCell(6));
						String rodzajKosztu =  getStringValueFromCell(row.getCell(7));
						String kwBrutto =  getStringValueFromCell(row.getCell(8));
//						Double kwBruttoDouble = row.getCell(8).getNumericCellValue();
						String kwNetto =  getStringValueFromCell(row.getCell(9));
//						Double kwNettoDouble = row.getCell(9).getNumericCellValue();
						String vatWal =   getStringValueFromCell(row.getCell(10));
//						Double vavWalDouble = row.getCell(10).getNumericCellValue();
						String rodzajWpisu =  getStringValueFromCell(row.getCell(11));
						String nrDokumentu =  getStringValueFromCell(row.getCell(12));
						String nrZapotrzebowania =   getStringValueFromCell(row.getCell(13));
						String opis =   getStringValueFromCell(row.getCell(14));
						String rozliczenie =  getStringValueFromCell(row.getCell(15));
						String waluta =  getStringValueFromCell(row.getCell(16));
						String kurs =  getStringValueFromCell(row.getCell(17));
//						Double kursDouble = row.getCell(17).getNumericCellValue();
						String pln =  getStringValueFromCell(row.getCell(18));
//						Double plnDouble = row.getCell(18).getNumericCellValue();
						String dataMod =  getStringValueFromCell(row.getCell(19));
//						Date dataModDate = row.getCell(19).getDateCellValue();
						
						
						//Walidacja
						
						ProjectEntry pe = null;
						
						if(cell == null || (cell!=null && (cell.getStringCellValue()==null || cell.getStringCellValue().trim().length()==0)))
						{
							pe = new ProjectEntry();
							
						}
						else
						{
							try
							{
								if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
									pe = ProjectEntry.find(Long.parseLong(String.valueOf(((Double) cell.getNumericCellValue()).intValue())));
									if(pe==null)addActionError("B��d: Nie istnieje wpis projektu o id " + cell.getStringCellValue() + ". W przypadku dodawania nowego wpisy nale�y pole ID zostawi� puste.");
								else{
									String tt = cell.getStringCellValue();
									pe = ProjectEntry.find(Long.valueOf(cell.getStringCellValue()));
									if(pe==null)addActionError("B��d: Nie istnieje wpis projektu o id " + cell.getStringCellValue() + ". W przypadku dodawania nowego wpisy nale�y pole ID zostawi� puste.");
								}
									
							}
							catch(Exception e)
							{
								log.error(e.getMessage(), e);
							}
						}
						
						
						if(projectId == null || projectId.trim().length()==0)
						{
							if(cell == null || (cell!=null && (cell.getStringCellValue()==null || cell.getStringCellValue().trim().length()==0)))
							addActionError("Nie podano numeru projektu w wierszu nr " + nrWiersza);
						}
						else
						{
							try
							{
								String trimmed[] = projectId.split(".");
								
								Long test = Long.parseLong(projectId.split("\\.")[0]);
							}
							catch(Exception e)
							{
								addActionError("Podano niew�a�ciwy nr projektu w wierszu nr " + nrWiersza);
							}
						}
						
						
						
						
						if(data==null || data.trim().length()==0){
//							if(cell == null  || (cell!=null && pe.getDateEntry()!=null))
//							addActionError("Nie podano daty w wierszu nr " + nrWiersza);
						}
						else{
						try{
							Date test = DateUtils.parseDateAnyFormat(data);
						}catch(ParseException e){
							try
							{
								Date d = row.getCell(3).getDateCellValue();
							}
							catch(Exception ee)
							{
								addActionError("Podano niepoprawn� dat� w wierszu nr " + nrWiersza);
							}
							
						}
						}
						if(dataKosztu==null || dataKosztu.trim().length()==0)
						{
//							if(cell==null || (cell!=null && pe.getCostDate()!=null))
//							addActionError("Nie podano daty kosztu w wierszu nr " + nrWiersza);
						}
						else{
						try{
							Date test = DateUtils.parseDateAnyFormat(dataKosztu);
						}catch(Exception e){
							try
							{
								Date d = row.getCell(5).getDateCellValue();
							}
							catch(Exception ee)
							{
								addActionError("Podano niepoprawn� dat� kosztu w wierszu nr " + nrWiersza);
							}
						}
						}
						
						if(rodzajWpisu==null || rodzajWpisu.trim().length()==0)
						{
							if(cell==null || (cell!=null && pe.getEntryType()!=null ))
							addActionError("Nie podano rodzaju wpisu w wierszu nr " + nrWiersza);
						}
						else if(!reversed.containsKey(rodzajWpisu)) addActionError("Podano niepoprawny rodzaj wpisu w wierszu nr " + nrWiersza);
						
						boolean testBool = !rodzajWpisu.equals("Wp�yw");
						boolean testBool1 = rodzajKosztu==null || rodzajKosztu.trim().length()==0;
						boolean testBool2 = (rodzajKosztu==null || rodzajKosztu.trim().length()==0) && !rodzajWpisu.equals("Wp�yw"); 
						
						if((rodzajKosztu==null || rodzajKosztu.trim().length()==0) && !rodzajWpisu.equals("Wp�yw"))
						{
							if((cell==null) || (cell!=null && pe.getCostKind()!=null))
							addActionError("Nie podano rodzaju kosztu w wierszu nr " + nrWiersza);
						}
						
						else if(!kinds.containsKey(rodzajKosztu) && !rodzajWpisu.equals("Wp�yw")) addActionError("Podano niepoprawny rodzaj kosztu w wierszu nr " + nrWiersza);
						
						if(waluta==null || waluta.trim().length()==0)
						{
							if(cell == null || (cell != null && pe.getCurrency()!=null))
							addActionError("Nie podano waluty w wierszu nr " + nrWiersza);
							
						}
						else if(!waluta.equals("PLN") && !waluta.equals("CHF") && !waluta.equals("EUR") && !waluta.equals("GBP") && !waluta.equals("USD")) addActionError("Podano niepoprawn� walut� w wierszu nr " + nrWiersza);
						
						if(kurs==null || kurs.trim().length()==0){
							if(cell == null || (cell != null && pe.getRate() != null))
							addActionError("Nie podano kursu w wierszu nr " + nrWiersza);
						}
						else{
						try{
							BigDecimal bd = BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(17))));
						}catch(Exception e){
							addActionError("Podano niepoprawny kurs w wierszu nr " + nrWiersza);
						}
						}
						
						if(rozliczenie==null || rozliczenie.trim().length()==0)
						{
							if(cell == null || (cell!=null && pe.getSettlementKind() != null))
							addActionError("Podano nieprawid�owy rodzaj rozliczenia w wierszu nr " + nrWiersza);
						}
						else
						{
							if(rozliczenie.equalsIgnoreCase("BRUTTO"))
							{
								if(kwBrutto==null || kwBrutto.trim().length()==0){
									addActionError("Nie podano kwoty brutto w wierszu nr " + nrWiersza);
								}
								else
								{
									try
									{
										BigDecimal test = BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(8))));
										if(test.equals(BigDecimal.ZERO)) addActionError("Podano niepoprawn� kwot� brutto w wierszu nr " + nrWiersza); 
									}
									catch(Exception e){
										addActionError("Podano niepoprawn� kwot� brutto w wierszu nr " + nrWiersza);
									}
								}
							}
							else if(rozliczenie.equalsIgnoreCase("NETTO"))
							{
								if(kwNetto==null || kwNetto.trim().length()==0) addActionError("Nie podano kwoty netto w wierszu nr " + nrWiersza);
								else
								{
									try
									{
										BigDecimal test = BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(9))));
										if(test.equals(BigDecimal.ZERO)) addActionError("Podano niepoprawn� kwot� netto w wierszu nr " + nrWiersza);
									}
									catch(Exception e){
										addActionError("Podano niepoprawn� kwot� netto w wierszu nr " + nrWiersza);
									}
								}
							}
						}
						
						ArrayList cc = (ArrayList) getActionErrors();
						
						if(pe != null && cc.size()==0)
						{
							if(projectId!=null && projectId.trim().length()>0)
							{
								String trimmed[] = projectId.split(".");
								
								pe.setProjectId(Long.parseLong(projectId.split("\\.")[0]));
							}
							else 
								continue;
							
							if(blockNr!=null && blockNr.trim().length()>0)
								pe.setEntryNumber(blockNr);
							else
								pe.setEntryNumber(null);
							
							if(data != null && data.trim().length()>0){
							try
								{
									pe.setDateEntry(DateUtils.parseDateTimeAnyFormat(data));
								}
							catch(ParseException e)
								{
									Date d = row.getCell(3).getDateCellValue();
									pe.setDateEntry(d);
								}
							}
							else
								pe.setDateEntry(new Date());
							
							if(dataWydatku != null && dataWydatku.trim().length()>0)
							{
								try{
									pe.setExpenditureDate(DateUtils.parseDateAnyFormat(dataWydatku));
								}
								catch (ParseException e)
								{
									Date d = row.getCell(4).getDateCellValue();
									pe.setExpenditureDate(d);
								}
							}
							else
								pe.setExpenditureDate(null);
							
							if(dataKosztu != null && dataKosztu.trim().length()>0) {
							try
							{
								pe.setCostDate(DateUtils.parseDateAnyFormat(dataKosztu) );
							}
							catch (ParseException e)
							{
								Date d = row.getCell(5).getDateCellValue();
								pe.setCostDate(d);
							}
							}
							else
								pe.setCostDate(new Date());
							
							if(nazawaKosztu != null && nazawaKosztu.trim().length()>0)
								pe.setCostName(nazawaKosztu);
							else
								pe.setCostName(null);
							
							if(rodzajKosztu != null && rodzajKosztu.trim().length()>0)
							{
								if(kinds.containsKey(rodzajKosztu))
								{
									pe.setCostKindId(kinds.get(rodzajKosztu).getId());
								}
							}
							else
								pe.setCostKindId(null);
								
							if(kwBrutto != null && kwBrutto.trim().length()>0) 
								pe.setGross(BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(8))))); 
							else
								pe.setGross(null);
							
							if(kwNetto != null && kwNetto.trim().length()>0) 
								pe.setNet(BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(9)))));
							else
								pe.setNet(null);
							
							if(vatWal != null && vatWal.trim().length()>0)
								pe.setVat(BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(10)))));
							else
								pe.setVat(null);
							
							if(rodzajWpisu != null && rodzajWpisu.trim().length()>0)
							{
								if(reversed.containsKey(rodzajWpisu))
								{
									pe.setEntryType(reversed.get(rodzajWpisu));
								}
							}
							else
								pe.setEntryType(null);
							
							if(nrDokumentu != null && nrDokumentu.trim().length()>0)
								pe.setDocNr(docNr);
							else
								pe.setDocNr(null);
							
							if(nrZapotrzebowania != null && nrZapotrzebowania.trim().length()>0) 
								pe.setDemandNr(nrZapotrzebowania);
							else
								pe.setDemandNr(null);
							
							if(opis != null && opis.trim().length()>0) 
								pe.setDescription(opis);
							else
								pe.setDescription(null);
							
							if(rozliczenie != null && rozliczenie.trim().length()>0) 
								pe.setSettlementKind(rozliczenie);
							else
								pe.setSettlementKind(null);
							
							if(waluta != null && waluta.trim().length()>0) 
								pe.setCurrency(waluta);
							else
								pe.setCurrency("PLN");
							
							if(kurs != null && kurs.trim().length()>0) 
								pe.setRate(BigDecimal.valueOf(Double.parseDouble(getStringValueFromCell(row.getCell(17))))); 
							else
								pe.setRate(null);
							
							pe.setMtime(new Date());
							
							DSApi.context().session().saveOrUpdate(pe);
						}
					}
					catch(Exception e)
					{
						log.error(e.getMessage(), e);
			
					}
				}
				DSApi.context().commit();
				log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! TUTAJ JEST IMPORT Z XLS: " + sheet.getSheetName());
			}
			catch(Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}

		/**
		 * Pobranie zawarto�ci przekazanej kom�rki.
		 * @param cell
		 * @return w przypadku kiedy kom�rka jest pusta albo wyst�pi exception podczas pobierania wartosc zwraca null, 
		 * w innym przypadku zawartosc komorki jako String
		 */
		private String getStringValueFromCell(Cell cell)
		{
			try
			{
				if (cell == null)
					return null;
				if (cell.getCellType() == Cell.CELL_TYPE_STRING)
					return cell.getStringCellValue();
				else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
				{
					double value = cell.getNumericCellValue();
					return String.valueOf(value);
				}
//				else if(cell.getCellType() == Cell.cell){
//					
//				}
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(), e);
				return null;
			}
			return null;
		}
	}

	private class GenerateXLS implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

			try
			{
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet("Docusafe Software");

				if (entries.totalCount() == 0)
					return;

				int rowNumber = 1;

				HSSFRow headerRow = sheet.createRow(0);

				List<String> keys = new ArrayList<String>();

				for (String key : columns.keySet())
				{
					if (!keys.contains(key))
						keys.add(key);
				}

				short headerNumber = 0;

				HSSFCellStyle headerStyle = workbook.createCellStyle();
				headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setBottomBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setLeftBorderColor(HSSFColor.RED.index);

				headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
				headerStyle.setRightBorderColor(HSSFColor.RED.index);

				headerStyle.setTopBorderColor(HSSFColor.RED.index);
				headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);

				for (String key : keys)
				{

					HSSFCell headercell = headerRow.createCell(headerNumber++);
					headercell.setCellStyle(headerStyle);
					String title = columns.get(key);
					title = title == null ? key : title;
					headercell.setCellValue(title);
				}

				List<Map<String, Object>> resultsListOfMap = new ArrayList<Map<String, Object>>();

				for (ProjectEntry entry : Arrays.asList(entries.results()))
				{
					Map<String, Object> e = new HashMap<String, Object>();
					e.put("projectId", entry.getProjectId());
					e.put("id", entry.getId());
					e.put("vat", entry.getVat());
					e.put("entryNumber", entry.getEntryNumber());
					e.put("dateEntry", entry.getDateEntry());
					e.put("expenditureDate", entry.getExpenditureDate());
					e.put("costDate", entry.getCostDate());
					e.put("costName", entry.getCostName());
					e.put("costKindId", entry.getCostKind());
					e.put("gross", entry.getGross());
					e.put("net", entry.getNet());
					e.put("entryType", entry.getEntryTypeName());
					e.put("docNr", entry.getDocNr());
					e.put("demandNr", entry.getDemandNr());
					e.put("settlementKind", entry.getSettlementKind());
					e.put("currency", entry.getCurrency());
					e.put("rate", entry.getRate());
					e.put("wartoscPln", entry.getPlnCost());
					e.put("mtime", entry.getMtime());
					String desc = entry.getDescription();
					if (desc != null)
						desc = desc.replace("\r\n", " ");
					e.put("description", desc);
					resultsListOfMap.add(e);
				}

				//styl komorki - ten sam dla kazdej
				HSSFCellStyle style = workbook.createCellStyle();
				
				for (Map<String, Object> values : resultsListOfMap)
				{
					HSSFRow row = sheet.createRow(rowNumber++);
					Set<String> set = values.keySet();
					int cellNumber = 0;
					for (String str : keys)
					{

						HSSFCell cell = row.createCell((short) cellNumber++);

						cell.setCellValue(values.get(str) == null ? "" : values.get(str).toString());

						if (values.get(str) != null && (values.get(str).equals("nie") || values.get(str).equals("tak")))
						{
							cell.setCellValue(sm.getString(values.get(str).toString()));
						}

//						HSSFCellStyle style = workbook.createCellStyle();

						cell.setCellStyle(style);
					}
				}

				for (int i = 0; i < keys.size(); i++)
				{
					try
					{
						sheet.autoSizeColumn((short) i);
					}
					catch (Exception exc)
					{

					}
				}

				File tmpfile = File.createTempFile("DocuSafe", "tmp");
				tmpfile.deleteOnExit();
				FileOutputStream fis = new FileOutputStream(tmpfile);
				workbook.write(fis);
				fis.close();
				ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
			}
			catch (Exception ioe)
			{
				log.error(ioe.getMessage(), ioe);
				event.addActionError(ioe.getMessage());
			}

			event.setResult(NONE);
		}
	}

	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{

			if (("doGenerateXls".equals(actionName)))
			{
				omitLimit = true;
			}
			else
			{
				omitLimit = false;
			}
			ProjectEntry.Query query;

			if (omitLimit)
			{
				query = new ProjectEntry.Query(offset, 0);
			}
			else
			{
				query = new ProjectEntry.Query(offset, LIMIT);
			}

			query.setCostKindId(costKindId);
			query.setCostName(costName);
			if (dateEntryFrom != null)
				query.setDateEntryFrom(DateUtils.parseJsDate(dateEntryFrom));
			if (dateEntryTo != null)
				query.setDateEntryTo(DateUtils.parseJsDate(dateEntryTo));
			query.setDemandNr(demandNr);
			query.setDescription(description);
			query.setDocNr(docNr);
			query.setEntryType(entryType);
			if (expenditureDateFrom != null)
				query.setExpenditureDateFrom(DateUtils.parseJsDate(expenditureDateFrom));
			if (expenditureDateTo != null)
				query.setExpenditureDateTo(DateUtils.parseJsDate(expenditureDateTo));
			if (costDateFrom != null)
				query.setCostDateFrom(DateUtils.parseJsDate(costDateFrom));
			if (costDateTo != null)
				query.setCostDateTo(DateUtils.parseJsDate(costDateTo));
			query.setGrossFrom(grossFrom);
			query.setGrossTo(grossTo);
			query.setNetFrom(netFrom);
			query.setNetTo(netTo);
			if (projectId != null && !"null".equals(projectId))
				query.setProjectId(Long.parseLong(projectId));
			query.setVat(vat);
			if (sortField == null)
				sortField = "id";
			query.setSortField(sortField);
			query.setAscending(ascending);
			log.info("Start search");
			try
			{
				entries = ProjectEntry.getPrejectEntries(query);

				count = entries.totalCount();

				netSum = new BigDecimal(0);

				if (count == 0)
					addActionMessage("Nie znaleziono wpisow");

				for (ProjectEntry entry : Arrays.asList(entries.results()))
				{
					if (entry.getNet() != null)
						netSum = netSum.add(entry.getNet());
				}
				
				importUrl =  HttpUtils.makeUrl("/record/projects/search-entry.action", new Object[] { "doImportXls", "true", "actionName", "doImportXls", "costName", costName, "expenditureDateFrom", expenditureDateFrom, "expenditureDateTo", expenditureDateTo, "costDateFrom", costDateFrom,
						"costDateTo", costDateTo, "dateEntryFrom", dateEntryFrom, "dateEntryTo", dateEntryTo, "demandNr", demandNr, "description", description, "docNr", docNr, "entryType", entryType, "grossFrom", grossFrom, "grossTo", grossTo, "netFrom", netFrom, "vat", vat, "netTo", netTo,
						"projectId", projectId, "sortField", sortField, "ascending", String.valueOf(ascending), "offset", String.valueOf(offset) });

				printUrl = HttpUtils.makeUrl("/record/projects/search-entry.action", new Object[] { "doGenerateXls", "true", "actionName", "doGenerateXls", "costName", costName, "expenditureDateFrom", expenditureDateFrom, "expenditureDateTo", expenditureDateTo, "costDateFrom", costDateFrom,
						"costDateTo", costDateTo, "dateEntryFrom", dateEntryFrom, "dateEntryTo", dateEntryTo, "demandNr", demandNr, "description", description, "docNr", docNr, "entryType", entryType, "grossFrom", grossFrom, "grossTo", grossTo, "netFrom", netFrom, "vat", vat, "netTo", netTo,
						"projectId", projectId, "sortField", sortField, "ascending", String.valueOf(ascending), "offset", String.valueOf(offset) });
				
				
				
				Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
					public String getLink(int offset)
					{
						return HttpUtils.makeUrl("/record/projects/search-entry.action", new Object[] { "doSearch", "true", "costName", costName, "expenditureDateFrom", expenditureDateFrom, "expenditureDateTo", expenditureDateTo, "costDateFrom", costDateFrom, "costDateTo", costDateTo,
								"dateEntryFrom", dateEntryFrom, "dateEntryTo", dateEntryTo, "demandNr", demandNr, "description", description, "docNr", docNr, "entryType", entryType, "grossFrom", grossFrom, "grossTo", grossTo, "netFrom", netFrom, "netTo", netTo, "projectId", projectId,
								"sortField", sortField, "ascending", String.valueOf(ascending), "offset", String.valueOf(offset) });
					}
				};
				pager = new Pager(linkVisitor, offset, LIMIT, count, 10);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	public String getDateEntryFrom()
	{
		return dateEntryFrom;
	}

	public void setDateEntryFrom(String dateEntryFrom)
	{
		this.dateEntryFrom = dateEntryFrom;
	}

	public String getDateEntryTo()
	{
		return dateEntryTo;
	}

	public void setDateEntryTo(String dateEntryTo)
	{
		this.dateEntryTo = dateEntryTo;
	}

	public String getExpenditureDateFrom()
	{
		return expenditureDateFrom;
	}

	public void setExpenditureDateFrom(String expenditureDateFrom)
	{
		this.expenditureDateFrom = expenditureDateFrom;
	}

	public String getExpenditureDateTo()
	{
		return expenditureDateTo;
	}

	public void setExpenditureDateTo(String expenditureDateTo)
	{
		this.expenditureDateTo = expenditureDateTo;
	}

	public String getCostDateFrom()
	{
		return costDateFrom;
	}

	public void setCostDateFrom(String costDateFrom)
	{
		this.costDateFrom = costDateFrom;
	}

	public String getCostDateTo()
	{
		return costDateTo;
	}

	public void setCostDateTo(String costDateTo)
	{
		this.costDateTo = costDateTo;
	}

	public String getCostName()
	{
		return costName;
	}

	public void setCostName(String costName)
	{
		this.costName = costName;
	}

	public Integer getCostKindId()
	{
		return costKindId;
	}

	public void setCostKindId(Integer costKindId)
	{
		this.costKindId = costKindId;
	}

	public BigDecimal getGrossFrom()
	{
		return grossFrom;
	}

	public void setGrossFrom(BigDecimal grossFrom)
	{
		this.grossFrom = grossFrom;
	}

	public BigDecimal getGrossTo()
	{
		return grossTo;
	}

	public void setGrossTo(BigDecimal grossTo)
	{
		this.grossTo = grossTo;
	}

	public BigDecimal getNetFrom()
	{
		return netFrom;
	}

	public void setNetFrom(BigDecimal netFrom)
	{
		this.netFrom = netFrom;
	}

	public BigDecimal getNetTo()
	{
		return netTo;
	}

	public void setNetTo(BigDecimal netTo)
	{
		this.netTo = netTo;
	}

	public BigDecimal getVat()
	{
		return vat;
	}

	public void setVat(BigDecimal vat)
	{
		this.vat = vat;
	}

	public String getEntryType()
	{
		return entryType;
	}

	public void setEntryType(String entryType)
	{
		this.entryType = entryType;
	}

	public String getDocNr()
	{
		return docNr;
	}

	public void setDocNr(String docNr)
	{
		this.docNr = docNr;
	}

	public String getDemandNr()
	{
		return demandNr;
	}

	public void setDemandNr(String demandNr)
	{
		this.demandNr = demandNr;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Map<String, String> getEntryTypeList()
	{
		return ProjectEntry.getEntryTypeList();
	}

	public void setEntryTypeList(Map<String, String> entryTypeList)
	{
		this.entryTypeList = entryTypeList;
	}

	public List<CostKind> getCostKinds()
	{
		return costKinds;
	}

	public void setCostKinds(List<CostKind> costKinds)
	{
		this.costKinds = costKinds;
	}

	public int getOffset()
	{
		return offset;
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public String getProjectId()
	{
		return projectId;
	}

	public void setProjectId(String projectId)
	{
		this.projectId = projectId;
	}

	public String getSortField()
	{
		return sortField;
	}

	public void setSortField(String sortField)
	{
		this.sortField = sortField;
	}

	public boolean isAscending()
	{
		return ascending;
	}

	public void setAscending(boolean ascending)
	{
		this.ascending = ascending;
	}

	public SearchResults<ProjectEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(SearchResults<ProjectEntry> entries)
	{
		this.entries = entries;
	}

	public Pager getPager()
	{
		return pager;
	}

	public void setPager(Pager pager)
	{
		this.pager = pager;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public static int getLimit()
	{
		return LIMIT;
	}

	public String getPrintUrl()
	{
		return printUrl;
	}

	public void setPrintUrl(String printUrl)
	{
		this.printUrl = printUrl;
	}
	
	public String getImportUrl()
	{
		return importUrl;
	}

	public void setImportUrl(String printUrl)
	{
		this.importUrl = printUrl;
	}

	public static Map<String, String> getColumns()
	{
		return columns;
	}

	public static void setColumns(Map<String, String> columns)
	{
		SearchEntryAction.columns = columns;
	}

	public BigDecimal getNetSum()
	{
		return netSum;
	}

	public void setNetSum(BigDecimal netSum)
	{
		this.netSum = netSum;
	}

	public boolean isOmitLimit()
	{
		return omitLimit;
	}

	public void setOmitLimit(boolean omitLimit)
	{
		this.omitLimit = omitLimit;
	}

	public String getActionName()
	{
		return actionName;
	}

	public void setActionName(String actionName)
	{
		this.actionName = actionName;
	}

	public List<Project> getProjects()
	{
		return projects;
	}

	public void setProjects(List<Project> projects)
	{
		this.projects = projects;
	}

	public FormFile getFileXls() {
		return fileXls;
	}

	public void setFileXls(FormFile fileXml) {
		this.fileXls = fileXml;
	}
	
	
}
