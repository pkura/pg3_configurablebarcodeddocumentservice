package pl.compan.docusafe.web.record.projects;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.ListUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ListProjectsAction extends EventActionSupport {

	private Logger log = LoggerFactory.getLogger(ListProjectsAction.class);
	
	private List<Project> projects;

    private String sortField;
    private boolean ascending;
    boolean canRead = false;
    private List<DSUser> users;
    private String numerIFPAN;
    private String projectManager;
    
	protected void setup() {
	
		 FillForm fillForm = new FillForm();

		 registerListener(DEFAULT_ACTION).
		 	append(OpenHibernateSession.INSTANCE).
		 	append(new CheckPermission()).
		 	append("FILL_FORM", fillForm).
		 	appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class CheckPermission implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   	
			try {
	        	DSApi.context().begin();
				String user = DSApi.context().getPrincipalName();
				for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
				{
					if (group.getGuid().equals(DSPermission.PROJECT_READ.getName()))
						canRead = true;
					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()))
						canRead = true;
				}
				DSApi.context().commit();
			}
    		catch (EdmException e)
            {
    			log.error(e.getMessage(),e);
             	addActionError(e.getMessage());
                try 
                {
                 	DSApi.context().rollback();
                } 
                catch (EdmException e2) 
                {
                	log.error(e.getMessage(),e);
                 	addActionError(e2.getMessage());
                }
             }
        }
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
        		users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
        		projects = Project.list(sortField,ascending,projectManager,numerIFPAN);
        		
        		if (!canRead)
        		{
        			DSApi.context().begin();
        			String user = DSApi.context().getPrincipalName();
        			List<Project> projs = new ArrayList<Project>();
        			for (Project project : projects)
        			{
        				for (DSDivision group : DSUser.findByUsername(user).getDivisionsAsList())
        				{
        					if (group.getGuid().equals(DSPermission.PROJECT_MODIFY.getName()+"_"+project.getId()))
        						projs.add(project);
        					else if (group.getGuid().equals(DSPermission.PROJECT_READ.getName()+"_"+project.getId()))
        						projs.add(project);
        				}
        			}	
        			projects = projs;
        			DSApi.context().commit();
        		}
				
			} catch (EdmException e) {
				log.error(e.getMessage());
			}
        }
    }
	
	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public String getNumerIFPAN() {
		return numerIFPAN;
	}

	public void setNumerIFPAN(String numerIFPAN) {
		this.numerIFPAN = numerIFPAN;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
}
