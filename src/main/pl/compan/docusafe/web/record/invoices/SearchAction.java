package pl.compan.docusafe.web.record.invoices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import org.json.JSONArray;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;

import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InvoicesKind;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;

import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.web.settings.SearchInvoicesAction;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.lambda;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchAction.java,v 1.38 2009/10/21 13:40:39 mariuszk Exp $
 */
public class SearchAction extends EventActionSupport
{
    private String thisSearchUrl;

    // @EXPORT
    private List<DSUser> users;
    private Map<String, String> termsOfPaymentList;
    private List<InvoicesKind> kindsList;
    private SearchResults<Map> results;
    private Pager pager;
    private String printUrl;
    private BigDecimal grossSum;
    
    //nowe wyszukanie
    private List<Map<String, Object>> resultsListOfMap;
    private List columns;
    private boolean newSearch;
    private Long[] invoiceIds;
	public boolean omitLimit;
	private int count;
    private SearchResults searchResult;
    
    /** The action name. */
    private String actionName;
    
    private boolean canSearch;
    private boolean canView;

    // @IMPORT
    private int offset;
    private static final int LIMIT = 10;
    private String sortField;
    private boolean ascending;

    private Integer sequenceId;
    private Integer sequenceIdFrom;
    private Integer sequenceIdTo;
    private String invoiceNo;
    private String ctimeDateFrom;
    private String ctimeDateTo;
    private String invoiceDateFrom;
    private String invoiceDateTo;
    private String paymentDateFrom;
    private String paymentDateTo;
    private String termsOfPayment;
    private String grossFrom;
    private String grossTo;
    private String description;
    private String contractNo;
    private Long contractId;
    private String vendor;
    private String nip;
    private String regon;
    private String acceptingUser;
    private String decretation;
    private String cpv;
    private String relayedTo;
    private String budgetaryRegistryDateFrom;
    private String budgetaryRegistryDateTo;
    private String creatingUser;
    private Integer year;
    private Boolean ignoreCancelled = false;
    private Integer kind;
    private Boolean ignoreArchive;
    
    private boolean saveFile;

    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(SearchAction.class.getPackage().getName(), null);

    // mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy
    // wynik�w (w klasie QueryForm)
    private static final Map<String, String> sortProperties = new HashMap<String, String>();
    static
    {
        sortProperties.put("invoice_number", "invoiceNo");
        sortProperties.put("invoice_invoiceDate", "invoiceDate");
        sortProperties.put("invoice_decretation", "decretation");
        sortProperties.put("invoice_cpv", "cpv");
        sortProperties.put("invoice_gross", "gross");
        sortProperties.put("invoice_acceptingUser", "acceptingUser");
        sortProperties.put("invoice_ctime", "ctime");
    }
    
    /** The Constant EV_FILL. */
    private static final String EV_FILL = "fillForm";
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doMyInvoice").
	        append(OpenHibernateSession.INSTANCE).
	        append(new SearchMyInvoice()).
	        append(new Search()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAction").
    		append(OpenHibernateSession.INSTANCE).
    		append(new PrepareArchive()).//omit limit
    		append(new Search()).
    		append(new SpecifiedAction()).
    		append(EV_FILL, fillForm).
    		appendFinally(CloseHibernateSession.INSTANCE);  

    }
    
    
    /**
     * The Class SpecifiedAction.
     */
    private class SpecifiedAction implements ActionListener
    {
        
        /* (non-Javadoc)
         * @see pl.compan.docusafe.webwork.event.ActionListener#actionPerformed(pl.compan.docusafe.webwork.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent event)
        {
        	if(("doGenerateXls".equals(actionName))) GenerateXLS(event);
        	else if(("doGenerateXlsFromChecked".equals(actionName))) GenerateXLSFromChecked(event);
        }
    }
    
    /**
     * The Class PrepareArchive.
     */
    private class PrepareArchive implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (("doGenerateXlsFromChecked".equals(actionName))) {
        		omitLimit = false;
        	} else {
        		omitLimit = true;
        	}
        }
    }
    /**
     * Generate xls.
     *
     * @param event the event
     */
    private void GenerateXLS(ActionEvent event)
    {
		Map<String, String> columns = new LinkedHashMap<String, String>();
		
        List<String> columnNames = getColumnNames(DSApi.context().userPreferences().node("search-invoices").get("columns", pl.compan.docusafe.web.settings.SearchInvoicesAction.DEFAULT_COLUMNS));
        List<TableColumn> tableColumns = getColumns(DSApi.context().userPreferences().node("search-invoices").get("columns", pl.compan.docusafe.web.settings.SearchInvoicesAction.DEFAULT_COLUMNS));
       
        for (TableColumn column : tableColumns) {
        	columns.put(column.getProperty(),column.getTitle() );
        }

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Docusafe Software");

        if (resultsListOfMap.size() == 0)
        	return;

        int rowNumber = 1;

        HSSFRow headerRow = sheet.createRow(0);

        List<String> keys = new ArrayList<String>();

        for (String key : columns.keySet()) 
        {
        	if (!keys.contains(key))
        		keys.add(key);
        }

        short headerNumber = 0;

        HSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setBottomBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setLeftBorderColor(HSSFColor.RED.index);

        headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setRightBorderColor(HSSFColor.RED.index);

        headerStyle.setTopBorderColor(HSSFColor.RED.index);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);

        for (String key : keys) 
        {

			HSSFCell headercell = headerRow.createCell(headerNumber++);
			headercell.setCellStyle(headerStyle);
			String title = columns.get(key);
			title = title == null ? key : title;
			headercell.setCellValue(title);
        }

        for (Map<String, Object> values : resultsListOfMap) 
        {
			HSSFRow row = sheet.createRow(rowNumber++);
			Set<String> set = values.keySet();
			int cellNumber = 0;
			for (String str : keys) 
			{

				HSSFCell cell = row.createCell((short) cellNumber++);

				cell.setCellValue(values.get(str) == null ? "" : values
						.get(str).toString());

				if (values.get(str) != null
						&& (values.get(str).equals("nie") || values.get(str)
								.equals("tak"))) 
				{
					cell.setCellValue(sm.getString(values.get(str).toString()));
				}

				HSSFCellStyle style = workbook.createCellStyle();

				cell.setCellStyle(style);
			}
        }

        for (int i = 0; i < keys.size(); i++) 
        {
        	try 
        	{
        		sheet.autoSizeColumn((short) i);
        	} 
        	catch (Exception exc) 
        	{
        		
        	}
        }

        try 
        {
			File tmpfile = File.createTempFile("DocuSafe", "tmp");
			tmpfile.deleteOnExit();
			FileOutputStream fis = new FileOutputStream(tmpfile);
			workbook.write(fis);
			fis.close();
			ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile,
							"application/vnd.ms-excel",
							"Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
        } 
        catch (IOException ioe) 
        {
        	event.addActionError(ioe.getMessage());
        }
		
        event.setResult(NONE);
	}	
    
    /**
     * Generowanie XLSa ze znalezionych dokumentow.
     *
     * @param event the event
     * @TODO - logika do wywalenia z akcji do jakiegos innego obiektu
     */
    private void GenerateXLSFromChecked(ActionEvent event)
    {	 
		if(invoiceIds==null) {
 			addActionError(sm.getString("NieWybranoFaktur"));
 			omitLimit = false;
 			return;
 		}
 		else
 		{
 			List<Invoice> invoices = null;
 			try 
 			{
 				invoices = Invoice.find(Arrays.asList(invoiceIds));
 				
 			} 
 			catch (Exception e) 
 			{
 				//log.debug("find many invoices",e);
			}
 			
 			 if (invoices.size() == 0)
             {
                 event.addActionMessage(sm.getString("NieZnalezionoFaktur"));
             }
             else {
             	resultsListOfMap = new ArrayList<Map<String, Object>>(invoices.size());
             	Iterator<Invoice> inv = invoices.iterator();
             	while (inv.hasNext()) {
             		Invoice invoice = (Invoice) inv.next();
             		Map<String, Object> bean = new HashMap<String, Object>();
             		
             		bean.put("link", "/record/invoices/edit-main.action?id="+invoice.getId());
             		bean.put("invoice_id", invoice.getId());
             		bean.put("inovice_sequenceId", invoice.getSequenceId());
             		bean.put("invoice_author", invoice.getAuthor());
             		bean.put("invoice_number", invoice.getInvoiceNo());
             		bean.put("invoice_invoiceDate", invoice.getInvoiceDate());
             		bean.put("invoice_paymentDate", invoice.getPaymentDate());
             		bean.put("invoice_saleDate", invoice.getSaleDate());
             		bean.put("invoice_termsOfPayment", invoice.getTermsOfPayment());
             		bean.put("invoice_kind", invoice.getKind().getName());
             		bean.put("invoice_gross", invoice.getGross());
             		bean.put("invoice_net", invoice.getNet());
             		bean.put("invoice_description", invoice.getDescription());
             		bean.put("invoice_ctime", invoice.getCtime());
             		
             		bean.put("invoice_vendor", invoice.getVendor().getSummary() + " " + invoice.getVendor().getAddress());
             		try {
             			if (invoice.getAcceptingUser() != null) {
                			String acceptUser = (String) DSUser.findByUsername(invoice.getAcceptingUser()).asFirstnameLastname();
                			bean.put("invoice_acceptingUser", acceptUser);
                		}
                		else bean.put("invoice_acceptingUser", "");
             		}
             		catch (EdmException e) {
             			
             		}
                    bean.put("invoice_decretation",  invoice.getDecretation());
                    bean.put("invoice_cpv", invoice.getCpv());
             		bean.put("invoice_acceptDate", invoice.getAcceptDate());
             		bean.put("invoice_acceptReturnDate", invoice.getAcceptReturnDate());
             		bean.put("invoice_relayedTo", invoice.getRelayedTo());
             		bean.put("invoice_budgetaryRegistryDate", invoice.getBudgetaryRegistryDate());
             		bean.put("invoice_remarks", invoice.getRemarks());
             		bean.put("invoice_year", invoice.getYear());
             		resultsListOfMap.add(bean);
             	}

             }
 			 
 			 GenerateXLS(event);
         
 		}
	}

   
    
    private class SearchMyInvoice implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	acceptingUser = DSApi.context().getPrincipalName();
        	ignoreArchive = true;
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	
            	if (AvailabilityManager.isAvailable("invoices.newSearch")) {
            		newSearch = true;
            	}
            	else {
            		newSearch = false;
            	}
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

                termsOfPaymentList = new LinkedHashMap<String, String>(5);
                termsOfPaymentList.put(Invoice.TERMS_TRANSFER, "Przelew");
                termsOfPaymentList.put(Invoice.TERMS_CASH, "Got�wka");
                termsOfPaymentList.put(Invoice.TERMS_CC, "Karta");

                kindsList = InvoicesKind.list();

                
                canSearch = (DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE) ||
                    DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU) ||
                    DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH)||
                    DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU_DO_AKCEPTACJI) ||
                    DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH_DO_AKCEPTACJI));
                canView = (DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR) ||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTUR_Z_DZIALU) ||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTURY)||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR_DO_AKCEPTACJI) ||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_Z_DZIALU_DO_AKCEPTACJI));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage()); 
            }
        }
    }

    private class Search implements ActionListener
    {
        @SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent event)
        {
        	saveFile = true;
        	
        	if (AvailabilityManager.isAvailable("invoices.newSearch")) {
        		newSearch = true;
        	}
        	else {
        		newSearch = false;
        	}
        	
        	if (hasActionErrors())
                 return;

        	Invoice.Query query;
        	if (omitLimit) {
        		query = new Invoice.Query(offset, 0);
        	}
        	else {
        		query = new Invoice.Query(offset, LIMIT);
        	}
        	
        	 if (sortField == null)
                 sortField = "sequenceId";
        	 
            if (sequenceId != null)
                query.setSequenceId(sequenceId);
            query.setSequenceIdFrom(sequenceIdFrom);
            query.setSequenceIdTo(sequenceIdTo);
            query.setInvoiceNo(TextUtils.trimmedStringOrNull(invoiceNo));
            query.setInvoiceDateFrom(DateUtils.nullSafeParseJsDate(invoiceDateFrom));
            query.setInvoiceDateTo(DateUtils.nullSafeParseJsDate(invoiceDateTo));
            query.setPaymentDateFrom(DateUtils.nullSafeParseJsDate(paymentDateFrom));
            query.setPaymentDateTo(DateUtils.nullSafeParseJsDate(paymentDateTo));
            query.setTermsOfPayment(TextUtils.trimmedStringOrNull(termsOfPayment));
            query.setKind(kind);
            query.setGrossFrom(TextUtils.nullSafeParseBigDecimal(grossFrom, 2));
            query.setGrossTo(TextUtils.nullSafeParseBigDecimal(grossTo, 2));
            query.setDescription(TextUtils.trimmedStringOrNull(description));
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            if (AvailabilityManager.isAvailable("invoice.archive.field"))
            {
            	if(ignoreArchive != null && ignoreArchive)
            		query.setArchive(Boolean.FALSE);
            }
            if (year != null) {
                query.setYear(year);
                String yearString = String.valueOf(year);
                query.setInvoiceDateFrom(DateUtils.nullSafeParseJsDate("01-01-" + yearString));
                query.setInvoiceDateTo(DateUtils.nullSafeParseJsDate("31-12-" + yearString));
            }
            
            if (contractId == null && StringUtils.isNotEmpty(contractNo)) 
            {
                query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) 
                {
                    try 
                    {
                        query.setContractIds((List<Long>) DSApi.context().classicSession().find(
                                "select d.id from d in class "+Contract.class.getName()+" where d.contractNo="+contractNo));
                    }
                    catch (EdmException e) 
                    {
                    }
                   
                } 
            }
            else if( contractId != null)
            {
                query.setContractId(contractId);
                if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) 
                {
                    try
                    {
                        query.setContractNo(Contract.find(contractId).getContractNo());
                    }
                    catch (EdmException e)
                    {
                    }
                }
            }
            
            if (nip != null) {
	            nip = nip.replaceAll("-", "");
	            if (nip.length() == 10 && NumberUtils.isNumber(nip)) {
	            	char _char = '-';
	            	StringBuilder sb = new StringBuilder();
	            	sb.append(nip.substring(0, 3));
	            	sb.append(_char);
	            	sb.append(nip.substring(3,6));
	            	sb.append(_char);
	            	sb.append(nip.substring(6,8));
	            	sb.append(_char);
	            	sb.append(nip.substring(8,10));
	            	nip = sb.toString();
	            }
            }

            query.setVendor(TextUtils.trimmedStringOrNull(vendor));
            query.setNip(TextUtils.trimmedStringOrNull(nip));
            query.setRegon(TextUtils.trimmedStringOrNull(regon));
            query.setAcceptingUser(TextUtils.trimmedStringOrNull(acceptingUser));
            query.setRelayedTo(TextUtils.trimmedStringOrNull(relayedTo));
            query.setBudgetaryRegistryDateFrom(DateUtils.nullSafeParseJsDate(budgetaryRegistryDateFrom));
            query.setBudgetaryRegistryDateTo(DateUtils.nullSafeParseJsDate(budgetaryRegistryDateTo));
            query.setCtimeDateFrom(DateUtils.nullSafeParseJsDate(ctimeDateFrom));
            query.setCtimeDateTo(DateUtils.nullSafeParseJsDate(ctimeDateTo));
            if (ignoreCancelled)
                query.setCancelled(Boolean.valueOf(false));
            
            query.safeSortBy(sortField, "invoiceDate", ascending);

            try
            {
            
                if (!DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE) &&
                    !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH) &&
                    !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU)&&
                    !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH_DO_AKCEPTACJI) &&
                    !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU_DO_AKCEPTACJI))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do wyszukiwania faktur");

                thisSearchUrl =
                    HttpUtils.makeUrl("/record/invoices/search.action",
                    new Object[] {
                    "doSearch", "true",
                    "sequenceId", sequenceId,
                    "ctimeDateFrom", ctimeDateFrom,
                    "ctimeDateTo", ctimeDateTo,
                    "sequenceIdFrom", sequenceIdFrom,
                    "sequenceIdTo", sequenceIdTo,
                    "invoiceNo", invoiceNo,
                    "invoiceDateFrom", invoiceDateFrom,
                    "invoiceDateTo", invoiceDateTo,
                    "paymentDateFrom", paymentDateFrom,
                    "paymentDateTo", paymentDateTo,
                    "termsOfPayment", termsOfPayment,
                    "Kind", kind,
                    "grossFrom", grossFrom,
                    "grossTo", grossTo,
                    "description", description,
                    "contractNo", contractNo,
                    "vendor", vendor,
                    "nip", nip,
                    "regon", regon,
                    "acceptingUser", acceptingUser,
                    "relayedTo", relayedTo,
                    "budgetaryRegistryDateFrom", budgetaryRegistryDateFrom,
                    "budgetaryRegistryDateTo", budgetaryRegistryDateTo,
                    "creatingUser", creatingUser,
                    "ignoreCancelled", ignoreCancelled,
                    "year", year,
                    "offset", String.valueOf(0)});

               	// nowy wyszukiwanie
               	if (newSearch)
               	{
               		
               		columns = getColumns(DSApi.context().userPreferences().node("search-invoices").get("columns", pl.compan.docusafe.web.settings.SearchInvoicesAction.DEFAULT_COLUMNS));
               		
	                searchResult = Invoice.search(query);
	                grossSum = new BigDecimal(0);	
	                count = searchResult.totalCount();

	                if (count == 0)
	                {
	                    event.addActionMessage(sm.getString("NieZnalezionoFaktur"));
	                }
	                else 
	                {
	                
	                	resultsListOfMap = new ArrayList<Map<String, Object>>(searchResult.totalCount());
	                	while (searchResult.hasNext()) {
	                		Invoice invoice = (Invoice) searchResult.next();
	                		grossSum = grossSum.add((BigDecimal) invoice.getGross());
	                		Map<String, Object> bean = new HashMap<String, Object>();
	                		bean.put("link", "/record/invoices/edit-main.action?id="+invoice.getId());
	                		bean.put("invoice_id", invoice.getId());
	                		bean.put("inovice_sequenceId", invoice.getSequenceId());
	                		bean.put("invoice_author", invoice.getAuthor());
	                		bean.put("invoice_number", invoice.getInvoiceNo());
	                		bean.put("invoice_invoiceDate", invoice.getInvoiceDate());
	                		bean.put("invoice_paymentDate", invoice.getPaymentDate());
	                		bean.put("invoice_saleDate", invoice.getSaleDate());
	                		bean.put("invoice_termsOfPayment", invoice.getTermsOfPayment());
	                		bean.put("invoice_kind", invoice.getKind().getName());
	                		bean.put("invoice_gross", invoice.getGross());
	                		bean.put("invoice_net", invoice.getNet());
	                		bean.put("invoice_description", invoice.getDescription());
	                		bean.put("invoice_ctime", invoice.getCtime());
	                		bean.put("invoice_vendor", invoice.getVendor().getSummary() + " " + invoice.getVendor().getAddress());
	                		if (invoice.getAcceptingUser() != null) {
	                			String acceptUser = DSUser.findByUsername(invoice.getAcceptingUser()).asFirstnameLastname();
	                			bean.put("invoice_acceptingUser", acceptUser);
	                		}
	                 		else bean.put("invoice_acceptingUser", "");
	                		bean.put("invoice_decretation", invoice.getDecretation());
	                		bean.put("invoice_cpv", invoice.getCpv());
	                		bean.put("invoice_acceptDate", invoice.getAcceptDate());
	                		bean.put("invoice_acceptReturnDate", invoice.getAcceptReturnDate());
	                		bean.put("invoice_relayedTo", invoice.getRelayedTo());
	                		bean.put("invoice_budgetaryRegistryDate", invoice.getBudgetaryRegistryDate());
	                		bean.put("invoice_year", invoice.getYear());
	                		bean.put("invoice_remarks", invoice.getRemarks());
	                		resultsListOfMap.add(bean);
	                	}
	                }
	                
               	}
               	else //stare wyszukiwanie
               	{

                    lambda<Invoice, Map> mapper =
                        new lambda<Invoice, Map>()
                        {
                            public Map act(Invoice inv) 
                            {
                                BeanBackedMap result = new BeanBackedMap(inv,
                                    "id", "sequenceId", "invoiceNo", 
                                    "invoiceDate", "description", "vendor", "gross", "contract");
                                try
                                {
                                    if ((inv.getContractId() != null) && (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)))
                                        result.put("contract", Contract.find(inv.getContractId()));
                                                               
                                }
                                catch (EdmException e)
                                {
                                } 
                                return result;
                            }
                        };

                    results = Invoice.search(query, mapper, Map.class);
                    
               		count = results.totalCount();
	                if (count == 0)
	                    addActionMessage("Nie znaleziono faktur");

	                grossSum = new BigDecimal(0);
	                for (Map inv : results.results())
	                {
	                    grossSum = grossSum.add((BigDecimal) inv.get("gross"));
	                }
               	}
                
                printUrl =
                    HttpUtils.makeUrl("/record/invoices/search-print.action",
                    new Object[] {
                    "sequenceId", sequenceId,
                    "ctimeDateFrom", ctimeDateFrom,
                    "ctimeDateTo", ctimeDateTo,
                    "sequenceIdFrom", sequenceIdFrom,
                    "sequenceIdTo", sequenceIdTo,
                    "invoiceNo", invoiceNo,
                    "invoiceDateFrom", invoiceDateFrom,
                    "invoiceDateTo", invoiceDateTo,
                    "paymentDateFrom", paymentDateFrom,
                    "paymentDateTo", paymentDateTo,
                    "termsOfPayment", termsOfPayment,
                    "Kind", kind,
                    "grossFrom", grossFrom,
                    "grossTo", grossTo,
                    "description", description,
                    "contractNo", contractNo,
                    "vendor", vendor,
                    "nip", nip,
                    "regon", regon,
                    "acceptingUser", acceptingUser,
                    "relayedTo", relayedTo,
                    "budgetaryRegistryDateFrom", budgetaryRegistryDateFrom,
                    "budgetaryRegistryDateTo", budgetaryRegistryDateTo,
                    "creatingUser", creatingUser,
                    "ignoreCancelled", ignoreCancelled,
                    "year", year,
                    "offset", String.valueOf(offset),               
                    "sortField", sortField,
                    "ascending", String.valueOf(ascending) });

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return
                            HttpUtils.makeUrl("/record/invoices/search.action",
                            new Object[] {
                            "doSearch", "true",
                            "sequenceId", sequenceId,
                            "ctimeDateFrom", ctimeDateFrom,
                            "ctimeDateTo", ctimeDateTo,
                            "sequenceIdFrom", sequenceIdFrom,
                            "sequenceIdTo", sequenceIdTo,
                            "invoiceNo", invoiceNo,
                            "invoiceDateFrom", invoiceDateFrom,
                            "invoiceDateTo", invoiceDateTo,
                            "year",year,
                            "paymentDateFrom", paymentDateFrom,
                            "paymentDateTo", paymentDateTo,
                            "termsOfPayment", termsOfPayment,
                            "Kind", kind,
                            "grossFrom", grossFrom,
                            "grossTo", grossTo,
                            "description", description,
                            "contractNo", contractNo,
                            "vendor", vendor,
                            "nip", nip,
                            "regon", regon,
                            "acceptingUser", acceptingUser,
                            "decretation" , decretation,
                            "cpv" , cpv,
                            "relayedTo", relayedTo,
                            "budgetaryRegistryDateFrom", budgetaryRegistryDateFrom,
                            "budgetaryRegistryDateTo", budgetaryRegistryDateTo,
                            "creatingUser", creatingUser,
                            "ignoreCancelled", ignoreCancelled,
                            "sortField", sortField,
                            "ascending", String.valueOf(ascending),
                            "offset", String.valueOf(offset)});
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, count, 10);

            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }

    }
   
    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            String date = DateUtils.formatJsDateTime((Date) object);
            
            if (date.substring(date.indexOf(' ')+1).equals("00:00"))
                return DateUtils.formatJsDate((Date) object);
            else
                return date;
        }
        else
        {
            return object.toString();
        }
    }
	
    private List<String> getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(pl.compan.docusafe.web.settings.SearchInvoicesAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }
    
    private List<TableColumn> getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.invoices.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(array.getString(i), desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null,""));
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(pl.compan.docusafe.web.settings.SearchInvoicesAction.DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null,""));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }
    
    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public Map<String, String> getTermsOfPaymentList()
    {
        return termsOfPaymentList;
    }

    public List<InvoicesKind> getKindsList()
    {
        return kindsList;
    }

    public void setInvoiceNo(String invoiceNo)
    {
        this.invoiceNo = invoiceNo;
    }

    public void setInvoiceDateFrom(String invoiceDateFrom)
    {
        this.invoiceDateFrom = invoiceDateFrom;
    }

    public void setInvoiceDateTo(String invoiceDateTo)
    {
        this.invoiceDateTo = invoiceDateTo;
    }

    public void setPaymentDateFrom(String paymentDateFrom)
    {
        this.paymentDateFrom = paymentDateFrom;
    }

    public void setPaymentDateTo(String paymentDateTo)
    {
        this.paymentDateTo = paymentDateTo;
    }

    public void setTermsOfPayment(String termsOfPayment)
    {
        this.termsOfPayment = termsOfPayment;
    }



    public void setGrossFrom(String grossFrom)
    {
        this.grossFrom = grossFrom;
    }

    public void setGrossTo(String grossTo)
    {
        this.grossTo = grossTo;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }
    
    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public void setAcceptingUser(String acceptingUser)
    {
        this.acceptingUser = acceptingUser;
    }
    
    public void setDecretation(String decretation)
    {
        this.decretation = decretation;
    }
    
    public void setCpv(String cpv)
    {
        this.cpv = cpv;
    }

    public void setRelayedTo(String relayedTo)
    {
        this.relayedTo = relayedTo;
    }

    public void setBudgetaryRegistryDateFrom(String budgetaryRegistryDateFrom)
    {
        this.budgetaryRegistryDateFrom = budgetaryRegistryDateFrom;
    }

    public void setBudgetaryRegistryDateTo(String budgetaryRegistryDateTo)
    {
        this.budgetaryRegistryDateTo = budgetaryRegistryDateTo;
    }

    public SearchResults<Map> getResults()
    {
        return results;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public boolean isCanSearch()
    {
        return canSearch;
    }

    public boolean isCanView()
    {
        return canView;
    }

    public String getPrintUrl()
    {
        return printUrl;
    }

    public void setSequenceIdFrom(Integer sequenceIdFrom)
    {
        this.sequenceIdFrom = sequenceIdFrom;
    }

    public void setSequenceIdTo(Integer sequenceIdTo)
    {
        this.sequenceIdTo = sequenceIdTo;
    }



    public void setCtimeDateFrom(String ctimeDateFrom) {
        this.ctimeDateFrom = ctimeDateFrom;
    }

    public void setCtimeDateTo(String ctimeDateTo) {
        this.ctimeDateTo = ctimeDateTo;
    }


    public BigDecimal getGrossSum() {
    	return grossSum;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }
    
    public void setIgnoreCancelled(boolean ignoreCancelled)
    {
        this.ignoreCancelled = ignoreCancelled;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

    public Integer getKind()
    {
        return kind;
    }

    public void setKind(Integer kind)
    {
        this.kind = kind;
    }

	public boolean isSaveFile() {
		return saveFile;
	}

	public void setSaveFile(boolean saveFile) {
		this.saveFile = saveFile;
	}

	/**
	 * @return the sortField
	 */
	public String getSortField() {
		return sortField;
	}

	/**
	 * @return the archive
	 */
	public Boolean getArchive() {
		return ignoreArchive;
	}

	/**
	 * @param archive the archive to set
	 */
	public void setArchive(Boolean archive) {
		this.ignoreArchive = archive;
	}

	public boolean isNewSearch() {
		return newSearch;
	}

	public void setNewSearch(boolean newSearch) {
		this.newSearch = newSearch;
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
	}

	public List<Map<String, Object>> getResultsListOfMap() {
		return resultsListOfMap;
	}

	public void setResultsListOfMap(List<Map<String, Object>> resultsListOfMap) {
		this.resultsListOfMap = resultsListOfMap;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Long[] getInvoiceIds() {
		return invoiceIds;
	}

	public void setInvoiceIds(Long[] invoiceIds) {
		this.invoiceIds = invoiceIds;
	}

	public boolean isOmitLimit() {
		return omitLimit;
	}

	public void setOmitLimit(boolean omitLimit) {
		this.omitLimit = omitLimit;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public SearchResults getSearchResult() {
		return searchResult;
	}

	public void setSearchResult(SearchResults searchResult) {
		this.searchResult = searchResult;
	}

	public Integer getSequenceId() {
		return sequenceId;
	}

	public Boolean getIgnoreCancelled() {
		return ignoreCancelled;
	}

	public void setIgnoreCancelled(Boolean ignoreCancelled) {
		this.ignoreCancelled = ignoreCancelled;
	}

	public Boolean getIgnoreArchive() {
		return ignoreArchive;
	}

	public void setIgnoreArchive(Boolean ignoreArchive) {
		this.ignoreArchive = ignoreArchive;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getSequenceIdFrom() {
		return sequenceIdFrom;
	}

	public Integer getSequenceIdTo() {
		return sequenceIdTo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public String getCtimeDateFrom() {
		return ctimeDateFrom;
	}

	public String getCtimeDateTo() {
		return ctimeDateTo;
	}

	public String getInvoiceDateFrom() {
		return invoiceDateFrom;
	}

	public String getInvoiceDateTo() {
		return invoiceDateTo;
	}

	public String getTermsOfPayment() {
		return termsOfPayment;
	}

	public String getGrossFrom() {
		return grossFrom;
	}

	public String getGrossTo() {
		return grossTo;
	}

	public String getDescription() {
		return description;
	}

	public String getContractNo() {
		return contractNo;
	}

	public Long getContractId() {
		return contractId;
	}

	public String getVendor() {
		return vendor;
	}

	public String getNip() {
		return nip;
	}

	public String getRegon() {
		return regon;
	}

	public String getRelayedTo() {
		return relayedTo;
	}

	public String getCreatingUser() {
		return creatingUser;
	}

	public Integer getYear() {
		return year;
	}

	public void setGrossSum(BigDecimal grossSum) {
		this.grossSum = grossSum;
	}

	public String getPaymentDateFrom() {
		return paymentDateFrom;
	}

	public String getPaymentDateTo() {
		return paymentDateTo;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public String getAcceptingUser() {
		return acceptingUser;
	}

	public String getDecretation()
	{
	    return decretation;
	}
	    
	public String getCpv()
	{
	    return cpv;
	}
	
	public String getBudgetaryRegistryDateFrom() {
		return budgetaryRegistryDateFrom;
	}

	public String getBudgetaryRegistryDateTo() {
		return budgetaryRegistryDateTo;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}
    
}
