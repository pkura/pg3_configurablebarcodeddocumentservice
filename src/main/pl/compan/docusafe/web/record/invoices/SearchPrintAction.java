package pl.compan.docusafe.web.record.invoices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2005-09-05 14:00:00 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchPrintAction.java,v 1.22 2009/10/21 13:40:39 mariuszk Exp $
 */
public class SearchPrintAction extends EventActionSupport
{
    private int offset;
    private static final int LIMIT = 10;
    private String sortField;
    private boolean ascending;
    private BigDecimal grossSum;

    private Integer sequenceId;
    private Integer sequenceIdFrom;
    private Integer sequenceIdTo;
    private String invoiceNo;
    private String invoiceDateFrom;
    private String invoiceDateTo;
    private String paymentDateFrom;
    private String paymentDateTo;
    private String termsOfPayment;
    private Integer Kind;
    private Integer year;
	private String grossFrom;
    private String grossTo;
    private String description;
    private String contractNo;
    private Long contractId;
    private String vendor;
    private String nip;
    private String regon;
    private String acceptingUser;
    private String relayedTo;
    private String budgetaryRegistryDateFrom;
    private String budgetaryRegistryDateTo;
    private String creatingUser;
    private boolean simplePrint;
    private boolean ignoreCancelled;
    private String ctimeDateFrom;
    private String ctimeDateTo;

    public String getCtimeDateFrom() {
		return ctimeDateFrom;
	}

	public void setCtimeDateFrom(String ctimeDateFrom) {
		this.ctimeDateFrom = ctimeDateFrom;
	}

	public String getCtimeDateTo() {
		return ctimeDateTo;
	}

	public void setCtimeDateTo(String ctimeDateTo) {
		this.ctimeDateTo = ctimeDateTo;
	}

	protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new GeneratePdf()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class GeneratePdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Invoice.Query query = new Invoice.Query(offset, 0);
            query.setSequenceId(sequenceId);
            query.setSequenceIdFrom(sequenceIdFrom);
            query.setSequenceIdTo(sequenceIdTo);
            query.setInvoiceNo(TextUtils.trimmedStringOrNull(invoiceNo));
            query.setInvoiceDateFrom(DateUtils.nullSafeParseJsDate(invoiceDateFrom));
            query.setInvoiceDateTo(DateUtils.nullSafeParseJsDate(invoiceDateTo));
            query.setPaymentDateFrom(DateUtils.nullSafeParseJsDate(paymentDateFrom));
            query.setPaymentDateTo(DateUtils.nullSafeParseJsDate(paymentDateTo));
            query.setTermsOfPayment(TextUtils.trimmedStringOrNull(termsOfPayment));
            query.setKind(Kind);
            query.setYear(year);
            query.setGrossFrom(TextUtils.nullSafeParseBigDecimal(grossFrom, 2));
            query.setGrossTo(TextUtils.nullSafeParseBigDecimal(grossTo, 2));
            query.setDescription(TextUtils.trimmedStringOrNull(description));
//            query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
//            query.setContractId(contractId);
            query.setVendor(TextUtils.trimmedStringOrNull(vendor));
            query.setNip(TextUtils.trimmedStringOrNull(nip)); 
            query.setRegon(TextUtils.trimmedStringOrNull(regon));
            query.setAcceptingUser(TextUtils.trimmedStringOrNull(acceptingUser));
            query.setRelayedTo(TextUtils.trimmedStringOrNull(relayedTo));
            query.setBudgetaryRegistryDateFrom(DateUtils.nullSafeParseJsDate(budgetaryRegistryDateFrom));
            query.setBudgetaryRegistryDateTo(DateUtils.nullSafeParseJsDate(budgetaryRegistryDateTo));
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            // 
            query.setCtimeDateFrom(DateUtils.nullSafeParseJsDate(ctimeDateFrom));
            query.setCtimeDateTo(DateUtils.nullSafeParseJsDate(ctimeDateTo));
            if (ignoreCancelled)
                query.setCancelled(false);
            
            try
            {
//            	if (contractId == null) {
//                    query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
//                    if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) {
//                            query.setContractIds((List<Long>) DSApi.context().session().find(
//                                    "select d.id from d in class " + Contract.class.getName() + " where d.contractNo="+contractNo));                        
//                    }
//                }
//            	else {
//                    query.setContractId(contractId);
//                    if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) {
//                            query.setContractNo(Contract.find(contractId).getContractNo());                
//                    }
//            	}
//            	
                if (contractId == null && StringUtils.isNotEmpty(contractNo)) 
                {
                    query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                    if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) 
                    {
                        try 
                        {
                            query.setContractIds((List<Long>) DSApi.context().classicSession().find(
                                    "select d.id from d in class "+Contract.class.getName()+" where d.contractNo="+contractNo));
                        }
                        catch (EdmException e) 
                        {
                        }
                       
                    } 
                }
                else if( contractId != null)
                {
                    query.setContractId(contractId);
                    if (Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS)) 
                    {
                        try
                        {
                            query.setContractNo(Contract.find(contractId).getContractNo());
                        }
                        catch (EdmException e)
                        {
                        }
                    }
                }
            	
            	
                if (!DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE)) 
                {
                    if (DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU)) {
                        query.setFromUserDivisions(Boolean.TRUE);
                    }
                    else 
                        if (DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH))
                            query.setAuthor(DSApi.context().getDSUser().getName());
                }
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
            
            query.safeSortBy(sortField, "sequenceId", ascending);

            File temp = null;
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE) &&
                   !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_SWOICH) &&
                   !DSApi.context().hasPermission(DSPermission.INV_WYSZUKIWANIE_Z_DZIALU))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do wyszukiwania faktur");

                SearchResults<Invoice> results = Invoice.search(query);

                if (results.count() == 0)
                    throw new EdmException("Nie znaleziono faktur pasuj�cych do zapytania");

                grossSum = new BigDecimal(0);
                for (Invoice inv : results.results())
                {
                    grossSum = grossSum.add(inv.getGross());
                }


                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                if (simplePrint)
                {
                    /* uproszczony wydruk */
                    
                    Font font = new Font(baseFont, 9);
                    
                    temp = File.createTempFile("docusafe_", "_tmp");  

                    com.lowagie.text.Document pdfDoc =
                        new com.lowagie.text.Document(PageSize.A4.rotate());
                    PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                    
                    HeaderFooter footer = new HeaderFooter(new Phrase("Strona "),true/*, new Phrase(".")*/);
                    footer.setAlignment(Element.ALIGN_CENTER);
                    pdfDoc.setFooter(footer);
                    
                    pdfDoc.open();
                    
                    int[] widths = new int[] { 5, 7, 5, 5, 5, 7, 12, 7, 11, 7, 7 };
    
                    PdfPTable table = new PdfPTable(widths.length);
                    table.setWidthPercentage(100);
                    table.setWidths(widths);
                    
    
                    table.addCell(new Phrase("Nr w rejestrze", font));              //sequenceId
                    table.addCell(new Phrase("Nr faktury", font));                  //invoiceNo
                    table.addCell(new Phrase("Data faktury", font));                //invoiceDate
                    table.addCell(new Phrase("Termin platnosci", font));            //paymentDate
                    table.addCell(new Phrase("Data przyj�cia", font));              //ctime
                    table.addCell(new Phrase("Kwota brutto", font));                //gross
                    table.addCell(new Phrase("Opis", font));                        //description
                    table.addCell(new Phrase("Numer umowy zlecenia", font));        //cotractNo
                    table.addCell(new Phrase("Sprzedawca", font));                  //vendor
                    table.addCell(new Phrase("Data przekazania do RB", font));      //budgetaryRegistryDate
                    table.addCell(new Phrase("Uwagi", font));                       //remarks
    
              //      pdfDoc.add(table);
    
             /*       table = new PdfPTable(widths.length);
                    table.setWidths(widths);
                    table.setWidthPercentage(100);
               */
                    int licz = 0;
                    while (results.hasNext())
                    {
                        Invoice invoice = (Invoice) results.next();
    
                        table.addCell(new Phrase(invoice.getSequenceId() != null ? invoice.getSequenceId().toString() : "", font));  //nr w rejestrze
                        table.addCell(new Phrase(invoice.getInvoiceNo() != null ? invoice.getInvoiceNo() : "", font));              //nr faktury
                        table.addCell(new Phrase(DateUtils.formatJsDateYY(invoice.getInvoiceDate()), font));                      //data faktury
                        table.addCell(new Phrase(DateUtils.formatJsDateYY(invoice.getPaymentDate()), font));                      //paymentDate
                        table.addCell(new Phrase(DateUtils.formatJsDateYY(invoice.getCtime()), font));                            //ctime
                        table.addCell(new Phrase(invoice.getGross() != null ? invoice.getGross().toString() : "", font));           //brutto
                        table.addCell(new Phrase(invoice.getDescription() != null ? invoice.getDescription() : "", font));          //opis
                        table.addCell(new Phrase(invoice.getContractNo() != null ? invoice.getContractNo() : "", font));            //numer umowy zlecenia
                        table.addCell(new Phrase(invoice.getVendor() != null ? invoice.getVendor().getSummary() : "", font));       //sprzedawca
                        table.addCell(new Phrase(invoice.getBudgetaryRegistryDate() != null ? DateUtils.formatCommonDate(invoice.getBudgetaryRegistryDate()) : "", font));  //data przekazania do RB
                        table.addCell(new Phrase(invoice.getRemarks() != null ? invoice.getRemarks() : "", font));                  // remarks
                        licz++;
                    }
    
                    PdfPCell cell = new PdfPCell(new Phrase("", font));
                    cell.setColspan(4);
                    table.addCell(cell);
                    table.addCell(new Phrase("suma kwot", font));
                    table.addCell(new Phrase(grossSum!=null? grossSum.toString() : "", font));
                    cell.setColspan(5);
                    table.addCell(cell);
                    pdfDoc.add(new Phrase("Data utworzenia: "+DateUtils.formatJsDateTime(new Date())+"\n", font));
                    
                    pdfDoc.add(new Phrase("Utworzone przez: "+DSApi.context().getDSUser().asFirstnameLastname()+"\n", font));
                    pdfDoc.add(table);
                    
                    pdfDoc.close();
                }
                else
                {
                    /* pe�ny wydruk */
                
                    Font font = new Font(baseFont, 12);
    
                    temp = File.createTempFile("docusafe_", "_tmp");  

                    com.lowagie.text.Document pdfDoc =
                        new com.lowagie.text.Document(PageSize.A4.rotate());
                    PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                    
                    HeaderFooter footer = new HeaderFooter(new Phrase("Strona "),true/*, new Phrase(".")*/);
                    footer.setAlignment(Element.ALIGN_CENTER);
                    pdfDoc.setFooter(footer);
                    
                    pdfDoc.open();
                    
                    int[] widths = new int[] { 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 };
    
                    PdfPTable table = new PdfPTable(widths.length);
                    //table.setWidths(widths);
                    table.setWidthPercentage(100);
    
                    table.addCell(new Phrase("Nr w rejestrze", font));              //sequenceId
                    table.addCell(new Phrase("Nr faktury", font));                  //invoiceNo
                    table.addCell(new Phrase("Data faktury", font));                //invoiceDate
                    table.addCell(new Phrase("Data sprzeda�y", font));              //saleDate
                    table.addCell(new Phrase("Termin platnosci", font));            //paymentDate
                    table.addCell(new Phrase("Data przyj�cia", font));              //ctime
                    table.addCell(new Phrase("Kwota brutto", font));                //gross
                    table.addCell(new Phrase("Kwota netto", font));                 //net
                    table.addCell(new Phrase("Opis", font));                        //description
                    table.addCell(new Phrase("Numer umowy zlecenia", font));        //cotractNo
                    table.addCell(new Phrase("Sprzedawca", font));                  //vendor
                    table.addCell(new Phrase("Akceptacja merytoryczna", font));     //acceptingUser
                    table.addCell(new Phrase("Data przekazania do a.m.", font));    //acceptDate
                    table.addCell(new Phrase("Data przekazania do RB", font));      //budgetaryRegistryDate
    
              //      pdfDoc.add(table);
    
             /*       table = new PdfPTable(widths.length);
                    table.setWidths(widths);
                    table.setWidthPercentage(100);
               */
                    while (results.hasNext())
                    {
                        Invoice invoice = (Invoice) results.next();
    
                        table.addCell(new Phrase(invoice.getSequenceId() != null ? invoice.getSequenceId().toString() : "", font));  //nr w rejestrze
                        table.addCell(new Phrase(invoice.getInvoiceNo() != null ? invoice.getInvoiceNo() : "", font));              //nr faktury
                        table.addCell(new Phrase(DateUtils.formatCommonDate(invoice.getInvoiceDate()), font));                      //data faktury
                        table.addCell(new Phrase(DateUtils.formatCommonDate(invoice.getSaleDate()), font));                            //data sprzedazy
                        table.addCell(new Phrase(DateUtils.formatCommonDate(invoice.getPaymentDate()), font));                      //paymentDate
                        table.addCell(new Phrase(DateUtils.formatCommonDate(invoice.getCtime()), font));                            //ctime
                        table.addCell(new Phrase(invoice.getGross() != null ? invoice.getGross().toString() : "", font));           //brutto
                        table.addCell(new Phrase(invoice.getNet() != null ? invoice.getNet().toString() : "", font));               //netto
                        table.addCell(new Phrase(invoice.getDescription() != null ? invoice.getDescription() : "", font));          //opis
                        table.addCell(new Phrase(invoice.getContractNo() != null ? invoice.getContractNo() : "", font));            //numer umowy zlecenia
                        table.addCell(new Phrase(invoice.getVendor() != null ? invoice.getVendor().getSummary() : "", font));       //sprzedawca
                        table.addCell(new Phrase(invoice.getAcceptingUser() != null ? DSUser.safeToFirstnameLastname(invoice.getAcceptingUser()) : "", font));              //akceptacja merytoryczna
                        table.addCell(new Phrase(invoice.getAcceptDate() != null ? DateUtils.formatCommonDate(invoice.getAcceptDate()) : "", font));                        //data przekazania do a.m.
                        table.addCell(new Phrase(invoice.getBudgetaryRegistryDate() != null ? DateUtils.formatCommonDate(invoice.getBudgetaryRegistryDate()) : "", font));  //data przekazania do RB
                    }
    
                    PdfPCell cell = new PdfPCell(new Phrase("", font));
                    cell.setColspan(5);
                    table.addCell(cell);
                    table.addCell(new Phrase("suma kwot", font));
                    table.addCell(new Phrase(grossSum!=null? grossSum.toString() : "", font));
                    cell.setColspan(7);
                    table.addCell(cell);
                    pdfDoc.add(new Phrase("Data utworzenia: "+DateUtils.formatJsDateTime(new Date())+"\n", font));
                    
                    pdfDoc.add(new Phrase("Utworzone przez: "+DSApi.context().getDSUser().asFirstnameLastname()+"\n", font));
                    pdfDoc.add(table);
                    
                    pdfDoc.close();
                }
            }
            catch (Exception e)
            {
                event.setResult(ERROR);
                event.getLog().error(e.getMessage(), e);
                addActionError(e.getMessage());
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public void setInvoiceNo(String invoiceNo)
    {
        this.invoiceNo = invoiceNo;
    }

    public void setInvoiceDateFrom(String invoiceDateFrom)
    {
        this.invoiceDateFrom = invoiceDateFrom;
    }

    public void setInvoiceDateTo(String invoiceDateTo)
    {
        this.invoiceDateTo = invoiceDateTo;
    }

    public void setPaymentDateFrom(String paymentDateFrom)
    {
        this.paymentDateFrom = paymentDateFrom;
    }

    public void setPaymentDateTo(String paymentDateTo)
    {
        this.paymentDateTo = paymentDateTo;
    }

    public void setTermsOfPayment(String termsOfPayment)
    {
        this.termsOfPayment = termsOfPayment;
    }

    public void setInvoiceKind(Integer Kind)
    {
        this.Kind = Kind;
    }

	public void setYear(Integer year) {
		this.year = year;
	}

    public void setGrossFrom(String grossFrom)
    {
        this.grossFrom = grossFrom;
    }

    public void setGrossTo(String grossTo)
    {
        this.grossTo = grossTo;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public void setAcceptingUser(String acceptingUser)
    {
        this.acceptingUser = acceptingUser;
    }

    public void setRelayedTo(String relayedTo)
    {
        this.relayedTo = relayedTo;
    }

    public void setBudgetaryRegistryDateFrom(String budgetaryRegistryDateFrom)
    {
        this.budgetaryRegistryDateFrom = budgetaryRegistryDateFrom;
    }

    public void setBudgetaryRegistryDateTo(String budgetaryRegistryDateTo)
    {
        this.budgetaryRegistryDateTo = budgetaryRegistryDateTo;
    }

    public void setSequenceIdFrom(Integer sequenceIdFrom)
    {
        this.sequenceIdFrom = sequenceIdFrom;
    }

    public void setSequenceIdTo(Integer sequenceIdTo)
    {
        this.sequenceIdTo = sequenceIdTo;
    }
    
    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }
    
    public void setSimplePrint(boolean simplePrint)
    {
        this.simplePrint = simplePrint;
    }
    
    public void setIgnoreCancelled(boolean ignoreCancelled)
    {
        this.ignoreCancelled = ignoreCancelled;
    }

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }
    
}
