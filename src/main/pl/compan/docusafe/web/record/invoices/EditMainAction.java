package pl.compan.docusafe.web.record.invoices;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InvoicesCpvCodes;
import pl.compan.docusafe.core.office.InvoicesDecretation;
import pl.compan.docusafe.core.office.InvoicesKind;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.record.invoices.Vendor;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditMainAction.java,v 1.38 2009/10/21 13:40:39 mariuszk Exp $
 */
public class EditMainAction extends EventActionSupport
{

	private Logger log = LoggerFactory.getLogger(EditMainAction.class);

    // @EXPORT
    private List users;
    private List<InvoicesCpvCodes> cpvCodesList;
    private List<InvoicesDecretation> decretationsList;
    private Map termsOfPaymentList;
    private List<InvoicesKind> kindsList;
    private Invoice invoice;
    private DSUser author;

    private boolean canCreate;
    private boolean canUpdate;
    private boolean canDelete;
    private boolean canView;

    // @EXPORT/@IMPORT
    private String invoiceNo;
    private String invoiceDate;
    private String saleDate;
    private String paymentDate;
    private String termsOfPayment;
    private Integer kind;
    private String gross;
    private String net;
    private Integer vat;
    private String description;
    private String contractNo;
    private Long contractId;
    private Contract contract;
    private String acceptDate;
    private String acceptReturnDate;
    private String remarks;
    private Boolean cancelled;

    private Vendor vendor;

    private String acceptingUser;
    private String decretation;
    private String cpv;
    private String budgetaryDescription;
    private String relayedTo;
    private String budgetaryRegistryDate;
    private Long id;

    private String divisionGuid;

    private String tab;
    private Long documentId;
    private List<Tab> tabs = new ArrayList<Tab>(2);
    
    private FormFile file;
    private String title;
    private String barcode;
    private Boolean archive;
    
    private boolean saveFile;
    private boolean newSearch;


    private static final String EV_CREATE = "create";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        if (AvailabilityManager.isAvailable("invoices.newSearch")) {
    		newSearch = true;
    	}
    	else {
    		newSearch = false;
    	}


        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).

            append(fillForm).
                append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Update()).

            append(fillForm).
                append(new Tabs()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCancel").
            append(OpenHibernateSession.INSTANCE).
            append(new Cancel(true)).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUncancel").
            append(OpenHibernateSession.INSTANCE).
            append(new Cancel(false)).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
            
    }

    private class Tabs implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            if(invoice != null && invoice.getDocument() != null)  {
                tabs.add(new Tab("Og�lne", "Og�lne", HttpUtils.makeUrl("/record/invoices/edit-main.action", new String[] { "id", ""+id, "saveFile", ""+true}), true));
                tabs.add(new Tab("Za��czniki", "Za��czniki", HttpUtils.makeUrl("/record/invoices/attachments.action", new String[] { "id", ""+id, "tmp_documentId", ""+invoice.getDocument().getId(), "documentId", ""+invoice.getDocument().getId()}), false));
            }
        }
    }


    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	 DSApi.context().begin();
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

                termsOfPaymentList = new LinkedHashMap(5);
                termsOfPaymentList.put(Invoice.TERMS_TRANSFER, "Przelew");
                termsOfPaymentList.put(Invoice.TERMS_CASH, "Got�wka");
                termsOfPaymentList.put(Invoice.TERMS_CC, "Karta");
                termsOfPaymentList.put(Invoice.TERMS_SUBSCRIPTION, "Przedp�ata");

                kindsList = InvoicesKind.list();
                cpvCodesList = InvoicesCpvCodes.list();
                decretationsList = InvoicesDecretation.list();

                canCreate = DSApi.context().hasPermission(DSPermission.INV_DODAWANIE);
                canUpdate = DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE);
                canDelete = false;//DSApi.context().hasPermission(DSPermission.INV_USUWANIE);
                canView = true;

                if (id != null)
                {
                    invoice = Invoice.find(id);
                    
                    DSUser _user = DSApi.context().getDSUser();
                    DSUser _author = DSUser.findByUsername(invoice.getAuthor());
                    boolean _inAuthorDivision = false;
                    for (DSDivision div : _author.getDivisions())
                    {
                        if (_user.inDivision(div)) 
                        {
                            _inAuthorDivision = true;
                            break;
                        }
                    }

                    if (((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                       ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(invoice.getAuthor()))))
                        canUpdate = true;
                    
                    if (!DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTURY)) {
                    	canView = false;
                    }
                    if (DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTUR_Z_DZIALU))
                    	canView = true;
                    if(DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR) && _user.getName().equals(invoice.getAuthor())) {
                        canView = true;                        
                    } 
                    
                    if (!canView)
                    	throw new EdmException("U�ytkownik nie ma uprawnie� do podgl�du tej faktury");
                    
                    cancelled = invoice.getCancelled();
                    invoiceNo = invoice.getInvoiceNo();
                    invoiceDate = DateUtils.formatJsDate(invoice.getInvoiceDate());
                    if (invoice.getSaleDate() != null)
                        saleDate = DateUtils.formatJsDate(invoice.getSaleDate());
                    paymentDate = DateUtils.formatJsDate(invoice.getPaymentDate());
                    termsOfPayment = invoice.getTermsOfPayment();
                    kind = invoice.getKind().getId();
                    gross = invoice.getGross().toString();
                    net = invoice.getNet().toString();
                    vat = invoice.getVat();
                    description = invoice.getDescription();
                    archive = invoice.getArchive();
                    if ((invoice.getContractId() == null) || (!Docusafe.moduleAvailable(Modules.MODULE_CONTRACTS))) 
                    { 
                        contractNo = invoice.getContractNo();
                        contractId = null;
                        contract = null;
                    }
                    else 
                    {
                        contract = Contract.find(invoice.getContractId());
                        contractNo = contract.getContractNo();
                        contractId = invoice.getContractId();
                    }
                    vendor = invoice.getVendor();
                    acceptingUser = invoice.getAcceptingUser();
                    decretation =  invoice.getDecretation();
                    cpv = invoice.getCpv();
                    budgetaryDescription = invoice.getBudgetaryDescription();
                    relayedTo = invoice.getRelayedTo();
                    budgetaryRegistryDate = invoice.getBudgetaryRegistryDate() != null ? DateUtils.formatJsDate(invoice.getBudgetaryRegistryDate()) : null;
                    acceptDate = invoice.getAcceptDate() != null ? DateUtils.formatJsDate(invoice.getAcceptDate()) : null;
                    acceptReturnDate = invoice.getAcceptReturnDate() != null ? DateUtils.formatJsDate(invoice.getAcceptReturnDate()) : null;
                    remarks = invoice.getRemarks();
                    divisionGuid = invoice.getDivisionGuid();

                    author = DSUser.findByUsername(invoice.getAuthor());

                    //sprawdzenie dla faktur juz istniejacych, czy maja dodane dokumenty, jesli nie, to tu dodawany jest dokument
                    if(invoice.getDocument() == null) {
                       

                        if (!(DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE) ||
                           ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                           ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(invoice.getAuthor())))))
                            throw new EdmException("U�ytkownik nie ma uprawnie� do modyfikacji faktury");
                      
                        
                        invoice.setDocument(addDefaultDocument(invoice));
                        
                    }

                    documentId = invoice.getDocument().getId();
                    DSApi.context().commit();
                }    
            }  catch (Exception e) {
            	log.error(e.getMessage(),e);
            	addActionError(e.getMessage());
            	try {
            		DSApi.context().rollback();
            	} catch (Exception e2) {
            		log.error(e.getMessage(),e);
            		addActionError(e2.getMessage());
            	}
            }
        }
    }

    private class Cancel implements ActionListener
    {
        private boolean cancel;  /* gdy rowne false to robimy "uncancel" */
        
        public Cancel(boolean cancel)
        {
            this.cancel = cancel;
        }
        
        public void actionPerformed(ActionEvent event)
        {
            try {
                DSApi.context().begin();
    
                Invoice invoice = Invoice.find(id);
                invoice.setCancelled(Boolean.valueOf(cancel));
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Invoice invoice = Invoice.find(id);
                
                DSUser _user = DSApi.context().getDSUser();
                //DSDivision _division = invoice.getDivisionGuid()!=null ? DSDivision.find(invoice.getDivisionGuid()) : null;
                DSUser _author = DSUser.findByUsername(invoice.getAuthor());
                boolean _inAuthorDivision = false;
                for (DSDivision div : _author.getDivisions())
                {
                    if (_user.inDivision(div)) 
                    {
                        _inAuthorDivision = true;
                        break;
                    }
                }
                
                
                if (!(DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE) ||
                   ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                   ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(invoice.getAuthor())))))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do modyfikacji faktury");
                
                

                invoice.setInvoiceNo(TextUtils.trimmedStringOrNull(invoiceNo));
                invoice.setInvoiceDate(DateUtils.nullSafeParseJsDate(invoiceDate));
                invoice.setSaleDate(DateUtils.nullSafeParseJsDate(saleDate));
                invoice.setPaymentDate(DateUtils.nullSafeParseJsDate(paymentDate));
                invoice.setTermsOfPayment(termsOfPayment);
                invoice.setKind(InvoicesKind.find(kind));
                invoice.setGross(new BigDecimal(gross).setScale(2, BigDecimal.ROUND_DOWN));
                invoice.setNet(new BigDecimal(net).setScale(2, BigDecimal.ROUND_DOWN));
                invoice.setVat(vat);
                invoice.setDescription(TextUtils.trimmedStringOrNull(description));
                invoice.setArchive(archive);
                if (contractId == null) {
                	invoice.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                    invoice.setContractId(null);
                }
                else 
                {
                    invoice.setContractNo(null);
                    invoice.setContractId(contractId);
                }
                vendor.normalizeFields();

                invoice.getVendor().setName((vendor.getName()));
                invoice.getVendor().setStreet((vendor.getStreet()));
                invoice.getVendor().setZip((vendor.getZip()));
                invoice.getVendor().setLocation((vendor.getLocation()));
                invoice.getVendor().setCountry((vendor.getCountry()));
                invoice.getVendor().setNip((vendor.getNip()));
                invoice.getVendor().setRegon((vendor.getRegon()));

                if (!StringUtils.isEmpty(acceptingUser))
                {
                	
                    invoice.setAcceptingUser(DSUser.findByUsername(acceptingUser));
                }
                else
                {
                    invoice.setAcceptingUser(null);
                }
              	if(decretation!=null)
              	{
              		invoice.setDecretation(decretation);
              	}
              	else {invoice.setDecretation(null);}
              	if(cpv!=null)
              	{
              		invoice.setCpv(cpv);
              	}
              	else {invoice.setCpv(null);}
                invoice.setBudgetaryDescription(TextUtils.trimmedStringOrNull(budgetaryDescription));
                invoice.setRelayedTo(TextUtils.trimmedStringOrNull(relayedTo));
                invoice.setBudgetaryRegistryDate(DateUtils.nullSafeParseJsDate(budgetaryRegistryDate));
                invoice.setAcceptDate(DateUtils.nullSafeParseJsDate(acceptDate));
                invoice.setAcceptReturnDate(DateUtils.nullSafeParseJsDate(acceptReturnDate));
                invoice.setRemarks(TextUtils.trimmedStringOrNull(remarks, 512));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(invoiceNo))
                addActionError("Nie podano numeru faktury");

            Date _invoiceDate = DateUtils.nullSafeParseJsDate(invoiceDate);
            Date _saleDate = DateUtils.nullSafeParseJsDate(saleDate);
            Date _paymentDate = DateUtils.nullSafeParseJsDate(paymentDate);

            if (_invoiceDate == null)
                addActionError("Nie podano daty faktury lub data jest nieprawid�owa");
            if (_paymentDate == null)
                addActionError("Nie podano terminu p�atno�ci lub termin jest nieprawid�owy");

          /*  if (_invoiceDate != null && _paymentDate != null &&
                _invoiceDate.after(_paymentDate))
                addActionError("Termin p�atno�ci jest wcze�niejszy ni� data faktury");*/

            if (!StringUtils.isEmpty(saleDate) && _saleDate == null)
                addActionError("Data sprzeda�y jest nieprawid�owa");

            if (StringUtils.isEmpty(termsOfPayment))
                addActionError("Nie wybrano sposobu p�atno�ci");
            if (kind == null)
                addActionError("Nie wybrano rodzaju faktura/rachunek");

            BigDecimal _gross = null;
            BigDecimal _net = null;
            char CHAR_COMMA = ',';
            char CHAR_DOT = '.';
        	
        	if (StringUtils.isEmpty(gross))
            {
                addActionError("Nie podano sumy brutto");
            }
            else
            {
            	try {
                	gross = gross.trim().replace(CHAR_COMMA, CHAR_DOT);
                	int grossLength = -1;
                	grossLength = (gross.indexOf(CHAR_DOT) == -1) ? gross.length() : grossLength;
                	if (grossLength > 17)
                		addActionError("Liczba cyfr kwoty brutto jest wi�ksza od 17-tu");
                	_gross = new BigDecimal(gross);
            	}
            	catch (Exception ex) {
            		addActionError("Podana suma brutto jest nieprawid�owa");
            	}
          
            }

            if (StringUtils.isEmpty(net))
            {
                addActionError("Nie podano sumy netto");
            }
            else
            {
                try {
                	net = net.trim().replace(CHAR_COMMA, CHAR_DOT);
                	int netLength = -1;
                	netLength = (net.indexOf(CHAR_DOT) == -1) ? net.length() : netLength;
                	if (netLength > 17)
                		addActionError("Liczba cyfr kwoty netto jest wi�ksza od 17-tu");
                    _net = new BigDecimal(net);
                }
                catch (Exception ex) {
                	log.error(ex.getMessage(),ex);
                    addActionError("Podana suma netto jest nieprawid�owa");
                }
            }

            if (_gross != null && _net != null &&
                _gross.movePointRight(2).compareTo(_net.movePointRight(2)) < 0)
                addActionError("Suma netto jest wi�ksza od sumy brutto");

//            if (vat == null)
//                addActionError("Nie podano stawki VAT");

            if (vat != null && (vat.intValue() < 0 || vat.intValue() > 100))
                addActionError("Podana stawka VAT jest niepoprawna");

            if (StringUtils.isEmpty(description))
                addActionError("Nie podano opisu");

            if (vendor == null || StringUtils.isEmpty(vendor.getName()))
                addActionError("Nie podano nazwy sprzedawcy");

//            if (vendor == null || StringUtils.isEmpty(vendor.getNip()))
//                addActionError("Nie podano numeru NIP");

            if (hasActionErrors())
                event.skip(EV_CREATE);
            }
        }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.INV_DODAWANIE))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do tworzenia faktury");               
                if (file != null)
                	title = file.getNameWithoutExtension();

                Invoice invoice = new Invoice();                
                invoice.setInvoiceNo(TextUtils.trimmedStringOrNull(invoiceNo));
                invoice.setInvoiceDate(DateUtils.nullSafeParseJsDate(invoiceDate));
                invoice.setSaleDate(DateUtils.nullSafeParseJsDate(saleDate));
                invoice.setPaymentDate(DateUtils.nullSafeParseJsDate(paymentDate));
                invoice.setTermsOfPayment(termsOfPayment);
                invoice.setKind(InvoicesKind.find(kind));                
                invoice.setGross(new BigDecimal(gross).setScale(2, BigDecimal.ROUND_DOWN));
                invoice.setNet(new BigDecimal(net).setScale(2, BigDecimal.ROUND_DOWN));
                invoice.setVat(vat);
                invoice.setDescription(TextUtils.trimmedStringOrNull(description));
                if (contractId == null) {
                    invoice.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                    invoice.setContractId(null);
                }
                else 
                {
                    invoice.setContractNo(null);
                    invoice.setContractId(contractId);
                }
                invoice.setArchive(archive);
                
                invoice.setDivisionGuid(pobierzDivisionGuid());

                vendor.normalizeFields();
                vendor.create();

                invoice.setVendor(vendor);

                if (!StringUtils.isEmpty(acceptingUser))
                {
                    invoice.setAcceptingUser(DSUser.findByUsername(acceptingUser));
                }
                invoice.setDecretation(decretation);
                invoice.setCpv(cpv);
                invoice.setBudgetaryDescription(TextUtils.trimmedStringOrNull(budgetaryDescription));
                invoice.setRelayedTo(TextUtils.trimmedStringOrNull(relayedTo));
                invoice.setBudgetaryRegistryDate(DateUtils.nullSafeParseJsDate(budgetaryRegistryDate));
                invoice.setAcceptDate(DateUtils.nullSafeParseJsDate(acceptDate));
                invoice.setAcceptReturnDate(DateUtils.nullSafeParseJsDate(acceptReturnDate));
                invoice.setRemarks(TextUtils.trimmedStringOrNull(remarks, 512));
                invoice.setYear(new Integer(GlobalPreferences.getCurrentYear()));
              
                //tu dodtkowo utworzenie pisma (pewnie trzeba bedzie dodac jeszcze jakies pola by dokument byl w wyszukiwarce)
                
                invoice.create();
                addDefaultDocument(invoice);
                //DSApi.context().commit();
                id = invoice.getId();
              
                DSUser _user = DSApi.context().getDSUser();
                DSUser _author = DSUser.findByUsername(invoice.getAuthor());
                boolean _inAuthorDivision = false;
                for (DSDivision div : _author.getDivisions())
                {
                    if (_user.inDivision(div)) 
                    {
                        _inAuthorDivision = true;
                        break;
                    }
                }
                if (((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                   ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(invoice.getAuthor()))))
                    canUpdate = true;
                
                if (!(DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTURY) ||
                    ((DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTUR_Z_DZIALU)) && (_inAuthorDivision)) ||
                    ((DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR)) && (_user.getName().equals(invoice.getAuthor()))))) {
                    canView = false;
                    throw new EdmException("U�ytkownik nie ma uprawnie� do podgl�du tej faktury");
                }
                
                if(file != null)
                {
                	if(invoice.getDocument() == null) {
                        //DSApi.context().begin();

                        if (!(DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE) ||
                           ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                           ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(invoice.getAuthor())))))
                            throw new EdmException("U�ytkownik nie ma uprawnie� do modyfikacji faktury");
                      
                        
                        invoice.setDocument(addDefaultDocument(invoice));
                        //DSApi.context().commit();
                    }

                    //DSApi.context().begin();
                    
	                Document document = Document.find(invoice.getDocument().getId());
	
	                Attachment attachment = new Attachment(TextUtils.trimmedStringOrNull(title, 254));
	                attachment.setBarcode(TextUtils.trimmedStringOrNull(barcode, 25));
	                document.createAttachment(attachment);
	                if(document instanceof InOfficeDocument){
	                	((InOfficeDocument)document).setNumAttachments(document.getAttachments().size());
	                }
	                
	                attachment.createRevision(file.getFile()).setOriginalFilename(file.getName());
	                
	                //DSApi.context().commit();
                }

                addActionMessage("Utworzono faktur� pod numerem "+invoice.getSequenceId());
                //clearForm();
                id = invoice.getId();
                event.setResult("confirm");
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
            	addActionError(e.getMessage());
                try {
                	DSApi.context().rollback();
                } catch (EdmException e2) {
                	log.error(e.getMessage(),e);
                	addActionError(e2.getMessage());
                }
               
            }
        }
    }

    private Document addDefaultDocument(Invoice invoice) throws EdmException {
        InOfficeDocument document = new InOfficeDocument();
        document.setDescription("faktura nr "+invoice.getInvoiceNo());
        document.setSummary("faktura");
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setIncomingDate(new Date());
        document.setCreatingUser(DSApi.context().getPrincipalName());
        document.setBok(false);
        document.setDocumentKind( DocumentKind.find(Long.parseLong("1")));
        document.setInvoiceKind(invoice.getKind());
        document.create();
        return document;
    }

/*
    private void clearForm()
    {
        invoiceNo = null;
        invoiceDate = null;
        saleDate = null;
        paymentDate = null;
        termsOfPayment = null;
        kind = null;
        gross = null;
        net = null;
        vat = null;
        description = null;
        contractNo = null;
        vendor = null;
        acceptingUser = null;
        budgetaryDescription = null;
        relayedTo = null;
        budgetaryRegistryDate = null;
        id = null;
    }
*/

    public String pobierzDivisionGuid() {
        DSDivision[] dsd;
        try {
            dsd = DSApi.context().getDSUser().getDivisions();
            if(dsd!=null && dsd.length > 0) {
                DSDivision dd = null;

                for(int i=0;i<dsd.length;i++) {
                    dd = dsd[i];
                    if(dd.getDivisionType() != null &&DSDivision.TYPE_POSITION.equals(dd.getDivisionType()) &&
                            dd.getParent() != null &&dd.getParent().getGuid() != null)
                        return dd.getParent().getGuid();
                }

                dd = dsd[(int)((dsd.length-1)*Math.random())];
                return dd!=null ? dd.getGuid() : DSDivision.ROOT_GUID;
            }

        } catch (EdmException e) {
        	log.error(e.getMessage(),e);
            addActionError("Blad pobrania dzialu");
        }
        return DSDivision.ROOT_GUID;
    }

    public String getInvoiceNo()
    {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo)
    {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate()
    {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate)
    {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentDate()
    {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate)
    {
        this.paymentDate = paymentDate;
    }

    public String getTermsOfPayment()
    {
        return termsOfPayment;
    }

    public void setTermsOfPayment(String termsOfPayment)
    {
        this.termsOfPayment = termsOfPayment;
    }

    public Integer getKind()
    {
        return kind;
    }

    public void setKind(Integer kind)
    {
        this.kind = kind;
    }

    public String getGross()
    {
        return gross;
    }

    public void setGross(String gross)
    {
        this.gross = gross;
    }

    public String getNet()
    {
        return net;
    }

    public void setNet(String net)
    {
        this.net = net;
    }

    public Integer getVat()
    {
        return vat;
    }

    public void setVat(Integer vat)
    {
        this.vat = vat;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    public List getUsers()
    {
        return users;
    }
    
    public List<InvoicesCpvCodes> getCpvCodesList()
    {
        return cpvCodesList;
    }
    
    public List<InvoicesDecretation> getDecretationsList()
    {
        return decretationsList;
    }

    public Map getTermsOfPaymentList()
    {
        return termsOfPaymentList;
    }

    public List<InvoicesKind> getKindsList()
    {
        return kindsList;
    }

    public String getContractNo()
    {
        return contractNo;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public Long getContractId()
    {
        return contractId;
    }

    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }
   
    public Contract getContract()
    {
        return contract;
    }

    public void setContract(Contract contract)
    {
        this.contract = contract;
    }
    
    public String getAcceptingUser()
    {
        return acceptingUser;
    }

    public void setAcceptingUser(String acceptingUser)
    {
        this.acceptingUser = acceptingUser;
    }    
    
    public String getDecretation()
    {
        return decretation;
    }
    
    public void setDecretation(String decretation)
    {
    	this.decretation = decretation;
    }
    
    public String getCpv()
    {
        return cpv;
    }
    
    public void setCpv(String cpv)
    {
    	this.cpv = cpv;
    }

    public String getBudgetaryDescription()
    {
        return budgetaryDescription;
    }

    public void setBudgetaryDescription(String budgetaryDescription)
    {
        this.budgetaryDescription = budgetaryDescription;
    }

    public String getRelayedTo()
    {
        return relayedTo;
    }

    public void setRelayedTo(String relayedTo)
    {
        this.relayedTo = relayedTo;
    }

    public String getBudgetaryRegistryDate()
    {
        return budgetaryRegistryDate;
    }

    public void setBudgetaryRegistryDate(String budgetaryRegistryDate)
    {
        this.budgetaryRegistryDate = budgetaryRegistryDate;
    }

    public Invoice getInvoice()
    {
        return invoice;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public boolean isCanCreate()
    {
        return canCreate;
    }

    public boolean isCanUpdate()
    {
        return canUpdate;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanView()
    {
        return canView;
    }
    
    public String getSaleDate()
    {
        return saleDate;
    }

    public void setSaleDate(String saleDate)
    {
        this.saleDate = saleDate;
    }

    public String getAcceptDate()
    {
        return acceptDate;
    }

    public void setAcceptDate(String acceptDate)
    {
        this.acceptDate = acceptDate;
    }

    public String getAcceptReturnDate()
    {
        return acceptReturnDate;
    }

    public void setAcceptReturnDate(String acceptReturnDate)
    {
        this.acceptReturnDate = acceptReturnDate;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public DSUser getAuthor()
    {
        return author;
    }

    public String getDivisionGuid() {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid) {
        this.divisionGuid = divisionGuid;
    }


    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(List<Tab> tabs) {
        this.tabs = tabs;
    }
    
    public Boolean getCancelled()
    {
        return cancelled;
    }

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public boolean isSaveFile() {
		return saveFile;
	}

	public void setSaveFile(boolean saveFile) {
		this.saveFile = saveFile;
	}
	
    public boolean isNewSearch() {
		return newSearch;
	}

	public void setNewSearch(boolean newSearch) {
		this.newSearch = newSearch;
	} 

	/**
	 * @return the archive
	 */
	public Boolean getArchive() {
		return archive;
	}

	/**
	 * @param archive the archive to set
	 */
	public void setArchive(Boolean archive) {
		this.archive = archive;
	}
}


