package pl.compan.docusafe.web.record.invoices;

import pl.compan.docusafe.web.office.common.AttachmentsTabAction;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;

import java.util.List;
import java.util.ArrayList;

/**
 * User: Bartlomiej.Spychalski@gmail.com
 * Date: 2006-03-22
 * Time: 11:51:41
 */
public class AttachmentsAction extends AttachmentsTabAction {

	private StringManager sm = GlobalPreferences.loadPropertiesFile(AttachmentsAction.class.getPackage().getName(),null);
    Long id;
    Long invoiceId;
    Long tmp_documentId;
    boolean saveFile;

    boolean canView;
    boolean canUpdate;
    
    protected List prepareTabs()
    {   
    	saveFile = true;
        List<Tab> tabs = new ArrayList<Tab>(2);
        if(id != null && getDocumentId() != null)  {
            invoiceId = id;
            canView = false;
            canUpdate = false;
            try {
                Invoice inv = Invoice.find(id);
    
                DSUser _user = DSApi.context().getDSUser();
                boolean _inAuthorDivision = false;
                
                if (inv.getAuthor() != null) {
                    DSUser _author = DSUser.findByUsername(inv.getAuthor());
                    
                    for (DSDivision div : _author.getDivisions())
                    {
                        if (_user.inDivision(div)) 
                        {
                            _inAuthorDivision = true;
                            break;
                        }
                    }
                }
                
                if (!DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTURY))
                	canView = false;
                if (DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTUR_Z_DZIALU))
                	canView = true;
                if(DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR) && _user.getName().equals(inv.getAuthor())) {
                    canView = true;
                } 
                
                if (!canView)
                	throw new EdmException("U�ytkownik nie ma uprawnie� do podgl�du tej faktury");
                
                if (DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE) ||
                        ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                        ((DSApi.context().hasPermission(DSPermission.INV_MODYFIKACJE_SWOICH)) && (_user.getName().equals(inv.getAuthor())))) 
                    canUpdate = true;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            
            tabs.add(new Tab(sm.getString("Ogolne"), sm.getString("Ogolne"), HttpUtils.makeUrl("/record/invoices/edit-main.action", new String[] { "id", ""+id, "saveFile", ""+saveFile}), false));
            tabs.add(new Tab(sm.getString("Zalaczniki"), sm.getString("Zalaczniki"), HttpUtils.makeUrl("/record/invoices/attachments.action", new String[] { "id", ""+id, "tmp_documentId", ""+tmp_documentId, "documentId", ""+getDocumentId()}), true));

        }
        return tabs;
    }

    public String getBaseLink()
    {
        return "/record/invoices/attachments.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvoiceId()
    {
        return invoiceId;
    }

    public Long getTmp_documentId() {
        return tmp_documentId;
    }

    public void setTmp_documentId(Long tmp_documentId) {
        this.tmp_documentId = tmp_documentId;
        //this.documentId = tmp_documentId;
    }
    
    public boolean isCanView()
    {
        return canView;
    }  
    
    public boolean isCanUpdate()
    {
        return canUpdate;
    }
}
