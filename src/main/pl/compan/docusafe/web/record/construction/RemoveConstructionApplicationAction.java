package pl.compan.docusafe.web.record.construction;

import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Bartlomiej.Spychalski@gmail.com
 */
public class RemoveConstructionApplicationAction extends ConstructionBaseAction 
{
    private Long id;
    static Logger log = LoggerFactory.getLogger(RemoveConstructionApplicationAction.class);

    protected void setup() 
    {
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Remove()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Remove implements ActionListener {

        public void actionPerformed(ActionEvent event) 
        {
            log.info("Proba usuniecia wniosku");
            if(id != null) 
            {
                try 
                {
                    DSApi.context().begin();
                    ConstructionApplication ca = ConstructionApplication.find(id);
                    ca.getOfficeCase().removeFromRegistry(Records.CONSTRUCTION);
                    ca.getOfficeCase().getAudit().add(Audit.create("deleteConstruction",DSApi.context().getPrincipalName(),
                        "Usuni�to wniosek o zezwolenie na budow� numer: "+ ca.getApplicationNo(),ca.getApplicationNo()));
                    if (ca != null) 
                        ca.delete();
                    DSApi.context().commit();

                } 
                catch (EdmException e) 
                {
                    log.error(e.getMessage());
                }
            }
        }
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }
}
