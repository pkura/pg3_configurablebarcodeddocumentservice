package pl.compan.docusafe.web.record.construction;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.*;

import java.util.Date;
import java.util.Map;

/* User: Administrator, Date: 2005-11-26 15:31:05 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CreateConstructionDecisionAction.java,v 1.8 2008/09/29 13:22:55 pecet3 Exp $
 */
public class CreateConstructionDecisionAction extends ConstructionBaseAction
{
    // @EXPORT
    private Long id;
    private String registryNo;
    private String applicationNo;
    private Date applicationSubmitDate;
    private boolean canSetClerk;
    private DSUser[] users;
    
    private Long applicationId;

    // @EXPORT/@IMPORT
    private Integer sequenceId;
    private String date;
    private Boolean accepted;
    private String decisionNo;
    private String suggestedDecisionNo;
    private String decisionNoInRegistry;
    private String remarks;
    private String operator;

    private Integer sequenceIdVerify;

    private boolean submitted;

    private static final String EV_FILL = "fill";

    // uwaga - akcja wywo�ywana jest pocz�tkowo z CaseTabAction
    // z wydarzeniem doPreCreate
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
/*
        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
*/
        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Create()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    // 1. wy�wietlenie pocz�tkowego formularza z przyciskiem doCreate
    //    sugestia subrejestru i numeru kolejnego; zmiana powoduje ponowne
    //    wygenerowanie tych numer�w
    // 2. Create - pomy�lne powoduje skip(fillform)

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                ConstructionApplication ca = ConstructionApplication.find(applicationId);
                if (ca.getConstructionDecision() != null)
                {
                    addActionError("Dla tego wniosku istnieje ju� decyzja");
                    return;
                }

                applicationNo = ca.getApplicationNo();
                applicationSubmitDate = ca.getSubmitDate();
                registryNo = ca.getRegistryNo();

                // pola wype�niane s� na podstawie dokumentu tylko przy
                // pierwszym wy�wietleniu formularza; po klikni�ciu przycisku
                // doCreate powinny w nich pozosta� warto�ci wpisane przez
                // u�ytkownika
                if (!submitted)
                {
                    sequenceId = ConstructionDecision.suggestSequenceId(registryNo);
                    date = DateUtils.formatJsDate(new Date());
                }

                if (sequenceId != null)
                {
                    Map<String,Object> map = ConstructionDecision.
                        suggestDecisionNo(ca, sequenceId);
                    suggestedDecisionNo = (String) map.get("decisionNo");
                    
                    if (decisionNo == null) 
                    {
                        decisionNo = suggestedDecisionNo;
                    }
                    if (decisionNoInRegistry == null) 
                    {
                        decisionNoInRegistry = ((Integer) map.get("decisionNoInRegistry")).toString();
                    }
                }
                
                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(ca.getInDocumentId());
                canSetClerk = doc.canSetClerk();            
                // wszyscy u�ytkownicy z dzia�u, w kt�rym jest pismo
                users = DSDivision.find(doc.getAssignedDivision()).getUsers(true);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date _date = DateUtils.nullSafeParseJsDate(date);

            if (accepted == null)
                addActionError("Nie wybrano rodzaju decyzji");
            if (sequenceId == null)
                addActionError("Nie podano numeru kolejnego wniosku");
            if (_date == null)
                addActionError("Nie podano daty decyzji");
            if (sequenceId != null && sequenceId.intValue() < 1)
                addActionError("Numer kolejny decyzji musi by� dodatni");
            decisionNo = TextUtils.trimmedStringOrNull(decisionNo,84);
            if (decisionNo == null)
                addActionError("Nie podano znaku decyzji");
            decisionNoInRegistry = TextUtils.trimmedStringOrNull(decisionNoInRegistry,16);
            if (decisionNoInRegistry == null)
                addActionError("Nie podano numeru decyzji w subrejestrze");
            if (hasActionErrors())
                return;

            // sprawdzenie, czy zmieniono warto�ci registryNo lub sequenceId
            // (poprzez pola ukryte)

            // je�eli registryNo lub sequenceId si� zmieni�y - addMessage
            // i return

            if (sequenceId == null || sequenceIdVerify == null ||
                sequenceId.intValue() != sequenceIdVerify.intValue())
            {
                addActionMessage("Wygenerowano ponownie znak wniosku, poniewa� " +
                    "u�ytkownik zmieni� numer kolejny wniosku w subrejestrze");
                return;
            }

            try
            {
                DSApi.context().begin();

                try
                {
                    ConstructionDecision.findByApplicationId(applicationId);
                    throw new EdmException("Dla tego wniosku istnieje ju� decyzja");
                }
                catch (EntityNotFoundException e)
                {
                }

                Integer _decisionNoInRegistry;
                try 
                {
                    _decisionNoInRegistry = new Integer(decisionNoInRegistry);
                }
                catch (NumberFormatException e)
                {
                    throw new EdmException("Podany numer decyzji w rejestrze jest nieprawid�owy");
                }
                ConstructionApplication ca = ConstructionApplication.find(applicationId);

             //   if (!ConstructionDecisionCounter.checkDecisionNo(decisionNoInRegistry))
             //       throw new EdmException("Nie mo�na wybra� podanego numeru decyzji w rejestrze.");
                
                Map<String,Object> map = ConstructionDecision.suggestDecisionNo(ca, sequenceId);
                suggestedDecisionNo = (String) map.get("decisionNo");
                
                ConstructionDecision cd = new ConstructionDecision();
                cd.setAccepted(accepted);
                cd.setConstructionApplication(ca);
                cd.setDate(_date);
                cd.setDecisionNo(decisionNo);
                cd.setRegistryNo(ca.getRegistryNo());
                cd.setDecisionNoInRegistry(_decisionNoInRegistry);
                cd.setSequenceId(sequenceId);
                cd.setOperator(operator);
                
                cd.create();

                id = cd.getId();

                DSApi.context().commit();

                event.setResult("construction-decision");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(Long applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getRegistryNo()
    {
        return registryNo;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public Boolean getAccepted()
    {
        return accepted;
    }

    public void setAccepted(Boolean accepted)
    {
        this.accepted = accepted;
    }

    public String getDecisionNo()
    {
        return decisionNo;
    }
    
    public void setDecisionNo(String decisionNo)
    {
        this.decisionNo = decisionNo;
    }

    public String getDecisionNoInRegistry()
    {
        return decisionNoInRegistry;
    }
    
    public void setDecisionNoInRegistry(String decisionNoInRegistry)
    {
        this.decisionNoInRegistry = decisionNoInRegistry;
    }
    
    public String getSuggestedDecisionNo()
    {
        return suggestedDecisionNo;
    }
  
    public void setSuggestedDecisionNo(String suggestedDecisionNo)
    {
        this.suggestedDecisionNo = suggestedDecisionNo;
    }
    
    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public Integer getSequenceIdVerify()
    {
        return sequenceIdVerify;
    }

    public void setSequenceIdVerify(Integer sequenceIdVerify)
    {
        this.sequenceIdVerify = sequenceIdVerify;
    }

    public boolean isSubmitted()
    {
        return submitted;
    }

    public void setSubmitted(boolean submitted)
    {
        this.submitted = submitted;
    }

    public Long getId()
    {
        return id;
    }

    public String getApplicationNo()
    {
        return applicationNo;
    }

    public Date getApplicationSubmitDate()
    {
        return applicationSubmitDate;
    }

    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }

    public boolean isCanSetClerk()
    {
        return canSetClerk;
    }

    public DSUser[] getUsers()
    {
        return users;
    }
}
