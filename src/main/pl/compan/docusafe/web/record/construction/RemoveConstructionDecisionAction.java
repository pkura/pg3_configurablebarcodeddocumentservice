package pl.compan.docusafe.web.record.construction;

import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
/**
 * @author Bartlomiej.Spychalski@gmail.com
 */
public class RemoveConstructionDecisionAction extends ConstructionBaseAction 
{
    private Long id;
    static final Logger log = LoggerFactory.getLogger(RemoveConstructionDecisionAction.class);

    protected void setup() 
    {
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Remove()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Remove implements ActionListener 
    {
        public void actionPerformed(ActionEvent event) 
        {
            if (id != null) 
            {
                try 
                {
                    DSApi.context().begin();
                    ConstructionDecision cd = ConstructionDecision.find(id);
                    if (cd != null) 
                    {
                        ConstructionApplication ca = cd.getConstructionApplication();
                        ca.setDecisionNo(null);
                        ca.setDecisionNoInRegistry(null);
                        ca.setDecisionDate(null);
                        ca.setAccepted(null);
                        ca.setConstructionDecision(null);
                        cd.delete();
                        
                        id = ca.getId(); /* przypisuje numer wniosku - bo wyswietli sie ekran z wnioskiem */
                    }
                    DSApi.context().commit();

                } 
                catch (EdmException e) 
                {
                    try 
                    {
                        DSApi.context().rollback();
                    } 
                    catch (EdmException e1) 
                    {
                        log.error(e.getMessage());
                    }
                    log.error(e.getMessage());
                }
            }
        }

    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
