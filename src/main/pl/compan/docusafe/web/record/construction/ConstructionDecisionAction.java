package pl.compan.docusafe.web.record.construction;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.common.rtf.RTF;
import pl.compan.docusafe.common.rtf.RTFException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpServletResponse;
import javax.swing.text.StyledDocument;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-11-28 12:21:13 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionDecisionAction.java,v 1.13 2008/10/06 09:39:37 pecet4 Exp $
 */
public class ConstructionDecisionAction extends ConstructionBaseAction
{
    // @EXPORT
    // z tego obiektu pobierane s� pola do wy�wietlenia w polach r/o formularza
    private ConstructionDecision cd;
    private Long applicationId;
    private String applicationNo;
    private Date applicationSubmitDate;
    private boolean canSetClerk;
    private DSUser[] users;

    // @EXPORT/@IMPORT
    private Long id;
    private String remarks;
    private String decisionNo;
    private String decisionNoInRegistry;
    private String operator;

    protected void setup()
    {        
        FillForm fillForm = new FillForm();
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doTemplateRTF").
            append(OpenHibernateSession.INSTANCE).
            append(new TemplateRTF()).
            append(fillForm);
            //appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
            if (id != null) {
                try
                {
                    if(!DSApi.isContextOpen())
                        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                    
                    cd = ConstructionDecision.find(id);
    
                    remarks = cd.getRemarks();
                    decisionNo = cd.getDecisionNo();
                    operator = cd.getOperator();
                    decisionNoInRegistry = (cd.getDecisionNoInRegistry() != null) ? (cd.getDecisionNoInRegistry().toString()) : "0";
                    
                    ConstructionApplication ca = cd.getConstructionApplication();
                    applicationId = ca.getId();
                    applicationNo = ca.getApplicationNo();
                    applicationSubmitDate = ca.getSubmitDate();
                    
                    InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(ca.getInDocumentId());
                    canSetClerk = doc.canSetClerk();
                    
                    // wszyscy u�ytkownicy z dzia�u, w kt�rym jest pismo
                    users = DSDivision.find(doc.getAssignedDivision()).getUsers(true);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String _remarks = TextUtils.trimmedStringOrNull(remarks);
            decisionNo = TextUtils.trimmedStringOrNull(decisionNo,84);
            if (decisionNo == null)
                addActionError("Nie podano znaku decyzji");
            if (hasActionErrors())
                return;
            try
            {
                Integer _decisionNoInRegistry;
                try 
                {
                    _decisionNoInRegistry = new Integer(decisionNoInRegistry);
                }
                catch (NumberFormatException e)
                {
                    throw new EdmException("Podany numer decyzji w rejestrze jest nieprawid�owy");
                }
                
                DSApi.context().begin();

                ConstructionDecision decision = ConstructionDecision.find(id);
                decision.setRemarks(_remarks);
                decision.setDecisionNo(decisionNo);
                decision.setDecisionNoInRegistry(_decisionNoInRegistry);
                decision.getConstructionApplication().setDecisionNo(decisionNo);
                decision.getConstructionApplication().setDecisionNoInRegistry(_decisionNoInRegistry);
                decision.setOperator(operator);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class TemplateRTF implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            StyledDocument document = null;
            String mime = null;
            String filename = null;
            try
            {
                DSApi.context().begin();

                ConstructionDecision cd = ConstructionDecision.find(id);
                ConstructionApplication ca = cd.getConstructionApplication();
                Resource resource = null;
                try
                {
                    resource = Resource.findResource(
                        cd.getAccepted() != null && cd.getAccepted() ?
                            ConstructionDecision.RES_ACCEPT_TEMPLATE :
                            ConstructionDecision.RES_UNACCEPT_TEMPLATE);
                }
                catch (EdmException e)
                {
                    event.addActionError("Nie znaleziono szablonu decyzji.");
                    event.getLog().error(e.getMessage(), e);
                    return;
                }
                
                mime = resource.getMime();
                filename = resource.getFileName();

                document = RTF.load(resource.getBinaryStream());

                Map<String,String> context = new HashMap<String, String>();
                context.put("nazwa", ca.getDeveloperName());
                context.put("adres", ca.getDeveloperStreet());
                context.put("kod", ca.getDeveloperZip());
                context.put("miejscowosc", ca.getDeveloperLocation());
                context.put("wniosek", ca.getApplicationNo());
                context.put("decyzja", ca.getDecisionNo());
                context.put("data", DateUtils.formatCommonDate(new Date()));

                RTF.substitute(document, context, "{", "}");

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
            catch (RTFException e)
            {
                DSApi.context().setRollbackOnly();
                event.getLog().error(e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            }

            if (document != null)
            {
                File temp = null;
                try
                {
                    temp = File.createTempFile("docusafe_", ".tmp");
                    RTF.write(document, temp);

                    HttpServletResponse response = ServletActionContext.getResponse();
                    response.setContentType(mime);
                    response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
                    //response.setHeader("Content-Disposition", "inline");
                    response.setContentLength((int) temp.length());

                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(temp);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
                finally
                {
                   // if (temp != null) temp.delete();
                }
            }
        }
    }

    public ConstructionDecision getCd()
    {
        return cd;
    }

    public Long getApplicationId()
    {
        return applicationId;
    }

    public String getApplicationNo()
    {
        return applicationNo;
    }

    public Date getApplicationSubmitDate()
    {
        return applicationSubmitDate;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    public String getDecisionNo()
    {
        return decisionNo;
    }
    
    public void setDecisionNo(String decisionNo)
    {
        this.decisionNo = decisionNo;
    }

    public String getDecisionNoInRegistry()
    {
        return decisionNoInRegistry;
    }
    
    public void setDecisionNoInRegistry(String decisionNoInRegistry)
    {
        this.decisionNoInRegistry = decisionNoInRegistry;
    }
    
    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }

    public boolean isCanSetClerk()
    {
        return canSetClerk;
    }

    public DSUser[] getUsers()
    {
        return users;
    }
}
