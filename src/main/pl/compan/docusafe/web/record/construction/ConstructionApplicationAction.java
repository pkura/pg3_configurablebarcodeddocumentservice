package pl.compan.docusafe.web.record.construction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/* User: Administrator, Date: 2005-11-28 12:21:13 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionApplicationAction.java,v 1.18 2008/02/13 10:20:34 mariuszk Exp $
 */
public class ConstructionApplicationAction extends ConstructionBaseAction
{
    // @EXPORT
    // z tego obiektu pobierane s� pola do wy�wietlenia w polach r/o formularza
    private ConstructionApplication ca;
    private boolean hasDecision;
    private Long decisionId;
    private String decisionNo;
    private Date decisionDate;
    private DSUser author;
    private boolean canSetClerk;
    
    // @EXPORT/@IMPORT
    private Long id;
    private Long caseId;
    private String summary;
    private String developerName;
    private String developerStreet;
    private String developerZip;
    private String developerLocation;
    private String remarks;
    private String usableArea;
    private String buildArea;
    private String volume;
    private String rooms;
    private String flats;
    private String fee;
    private String assignedUser;
    private DSUser[] clerks;

    
    protected void setup()
    {             
        FillForm fillForm = new FillForm();
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doWithdraw").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Withdraw()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUnwithdraw").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Unwithdraw()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (id != null)
                    ca = ConstructionApplication.find(id);
                else
                    ca = ConstructionApplication.findByOfficeCaseId(caseId);

                id = ca.getId();
                summary = ca.getSummary();
                //developer = ca.getDeveloper();
                developerName = ca.getDeveloperName();
                developerStreet = ca.getDeveloperStreet();
                developerZip = ca.getDeveloperZip();
                developerLocation = ca.getDeveloperLocation();
                remarks = ca.getRemarks();
                usableArea = ca.getUsableArea() != null ? ca.getUsableArea().toString() : null;
                buildArea = ca.getBuildArea() != null ? ca.getBuildArea().toString() : null;
                volume = ca.getVolume() != null ? ca.getVolume().toString() : null;
                rooms = ca.getRooms();
                flats = ca.getFlats();
                fee = ca.getFee() != null ? ca.getFee().toString() : null;
                
                InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(ca.getInDocumentId());
                canSetClerk = doc.canSetClerk();
                
                // wszyscy u�ytkownicy z dzia�u, w kt�rym jest pismo
                clerks = DSDivision.find(doc.getAssignedDivision()).getUsers(true);
                UserFactory.sortUsers(clerks, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                // je�eli na li�cie nie ma bie��cego referenta wniosku, trzeba go doda�
                if (ca.getClerk() != null)
                {
                    boolean clerkOnList = false;
                    for (int i=0; i < clerks.length; i++)
                    {
                        if (clerks[i].getName().equals(ca.getClerk()))
                        {
                            clerkOnList = true;
                            break;
                        }
                    }
                    if (!clerkOnList)
                    {
                        List<DSUser> tmp = new ArrayList<DSUser>(Arrays.asList(clerks));
                        tmp.add(DSUser.findByUsername(ca.getClerk()));
                        clerks = (DSUser[]) tmp.toArray(new DSUser[clerks.length]);
                        UserFactory.sortUsers(clerks, DSUser.SORT_LASTNAME_FIRSTNAME, true);
                    }
                }
                 
                author = DSUser.findByUsername(ca.getAuthor());
                assignedUser = ca.getClerk();
                
                if (ca.getConstructionDecision() != null)
                {
                    ConstructionDecision decision = ca.getConstructionDecision();
                    hasDecision = true;
                    decisionId = decision.getId();
                    decisionNo = decision.getDecisionNo();
                    decisionDate = decision.getDate();
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String _summary = TextUtils.trimmedStringOrNull(summary);
            String _developerName = TextUtils.trimmedStringOrNull(developerName);
            String _developerStreet = TextUtils.trimmedStringOrNull(developerStreet);
            String _developerZip = TextUtils.trimmedStringOrNull(developerZip);
            String _developerLocation = TextUtils.trimmedStringOrNull(developerLocation);
            String _remarks = TextUtils.trimmedStringOrNull(remarks);
           // BigDecimal _usableArea = parseBigDecimal(usableArea);
           // BigDecimal _buildArea = parseBigDecimal(buildArea);
           // BigDecimal _volume = parseBigDecimal(volume);
            BigDecimal _fee = parseBigDecimal(fee);

            if (_summary == null)
                addActionError("Nie podano opisu wniosku");
            if (_developerName == null)
                addActionError("Nie podano nazwy inwestora");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();                
                
                ConstructionApplication ca = ConstructionApplication.find(id);
                    
                if(_summary != null && ( ca.getSummary()== null || (ca.getSummary()!= null &&!ca.getSummary().equals(_summary))))
                {  
                    ca.setSummary(_summary);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana opisu wnioseku o zezwolenie na budow� na: "+ _summary,_summary));
                }
                if(_developerName != null && ( ca.getDeveloperName()== null || (ca.getDeveloperName()!= null && !ca.getDeveloperName().equals(_developerName))))
                {
                    ca.setDeveloperName(_developerName);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana nazwy inwestora wnioseku o zezwolenie na budow� na: "+ _developerName,_developerName));
                }
				
                if(_developerStreet != null && ( ca.getDeveloperStreet()== null || (ca.getDeveloperStreet()!= null && !ca.getDeveloperStreet().equals(_developerStreet))))
                {
                    ca.setDeveloperStreet(_developerStreet);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana ulicy inwestora wnioseku o zezwolenie na budow� na: "+ _developerStreet,_developerStreet));
                }
                
                if( _developerZip!= null && ( ca.getDeveloperZip()== null || (ca.getDeveloperZip()!= null && !ca.getDeveloperZip().equals(_developerZip))))
                {
                    ca.setDeveloperZip(_developerZip);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana oku pocztowego inwestora wnioseku o zezwolenie na budow� na: "+ _developerZip,_developerZip));
                }
                if( _developerLocation!= null && ( ca.getDeveloperLocation()== null || (ca.getDeveloperLocation()!= null && !ca.getDeveloperLocation().equals(_developerLocation))))
                {
                    ca.setDeveloperLocation(_developerLocation);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana miejscowo�ci inwestora wnioseku o zezwolenie na budow� na: "+ _developerLocation,_developerLocation));
                }
                if(_remarks != null && ( ca.getRemarks()== null || (ca.getRemarks()!= null && !ca.getRemarks().equals(_remarks))))
                {
                    ca.setRemarks(_remarks);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana uwagi wnioseku o zezwolenie na budow� na: "+ _remarks,_remarks));
                }
                if(usableArea != null && (  ca.getUsableArea()== null || (ca.getUsableArea()== null && !ca.getUsableArea().equals(usableArea))))
                {
                    ca.setUsableArea(usableArea);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana powierzchni u�ytkowej wnioseku o zezwolenie na budow� na: "+ usableArea,usableArea));
                }
                if( buildArea!= null && ( ca.getBuildArea()== null || (ca.getBuildArea()!= null && !ca.getBuildArea().equals(buildArea))))
                {
                    ca.setBuildArea(buildArea);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana powierzchni zabudowy wnioseku o zezwolenie na budow� na: "+ buildArea,buildArea));
                }
                if(volume != null && ( ca.getVolume()== null || (ca.getVolume()!= null && !ca.getVolume().equals(volume))))
                {
                    ca.setVolume(volume);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana kubatury wnioseku o zezwolenie na budow� na: "+ volume,volume));
                }
                if( rooms!= null && ( ca.getRooms()== null || (ca.getRooms()!= null && !ca.getRooms().equals(rooms))))
                {
                    ca.setRooms(rooms);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana liczby izb wnioseku o zezwolenie na budow� na: "+rooms,rooms));
                }
                if( flats!= null && ( ca.getFlats()== null || (ca.getFlats()!= null && !ca.getFlats().equals(flats))))
                {
                    ca.setFlats(flats);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana liczby mieszka� wnioseku o zezwolenie na budow� na: "+ flats,flats));
                }
                if(_fee != null && ( ca.getFee()== null || (ca.getFee()!= null && !ca.getFee().equals(_fee))))
                {
                    ca.setFee(_fee);
                    ca.getOfficeCase().getAudit().add(Audit.create("changeAtr",DSApi.context().getPrincipalName(),
                        "Zmiana op�aty skarbowej wnioseku o zezwolenie na budow� na: "+ _fee,_fee.toString()));
                }
                

                if (!StringUtils.isEmpty(assignedUser))
                {
                    // UserNotFoundException
                    ca.setClerk(DSUser.findByUsername(assignedUser).getName());
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

        private BigDecimal parseBigDecimal(String s)
        {
            if (s == null)
                return null;
            try
            {
                return new BigDecimal(s.replace(',', '.'));
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

    private class Withdraw implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                ConstructionApplication ca = ConstructionApplication.find(id);

                ca.setWithdrawn(Boolean.TRUE);
                ca.getOfficeCase().getAudit().add(Audit.create("withdraw",DSApi.context().getPrincipalName(),
                    "Wycofano wniosek o zezwolenie na budow� numer: "+ ca.getApplicationNo(),ca.getApplicationNo()));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Unwithdraw implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                ConstructionApplication ca = ConstructionApplication.find(id);

                ca.setWithdrawn(Boolean.FALSE);
                ca.getOfficeCase().getAudit().add(Audit.create("unwithdraw",DSApi.context().getPrincipalName(),
                    "Przywr�cno wniosek o zezwolenie na budow� numer: "+ ca.getApplicationNo(),ca.getApplicationNo()));
                

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getCaseId()
    {
        return caseId;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getDeveloperName()
    {
        return developerName;
    }

    public void setDeveloperName(String developerName)
    {
        this.developerName = developerName;
    }

    public String getDeveloperStreet()
    {
        return developerStreet;
    }

    public void setDeveloperStreet(String developerStreet)
    {
        this.developerStreet = developerStreet;
    }

    public String getDeveloperZip()
    {
        return developerZip;
    }

    public void setDeveloperZip(String developerZip)
    {
        this.developerZip = developerZip;
    }

    public String getDeveloperLocation()
    {
        return developerLocation;
    }

    public void setDeveloperLocation(String developerLocation)
    {
        this.developerLocation = developerLocation;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public ConstructionApplication getCa()
    {
        return ca;
    }

    public boolean isHasDecision()
    {
        return hasDecision;
    }

    public Long getDecisionId()
    {
        return decisionId;
    }

    public String getDecisionNo()
    {
        return decisionNo;
    }

    public Date getDecisionDate()
    {
        return decisionDate;
    }

    public String getUsableArea()
    {
        return usableArea;
    }

    public void setUsableArea(String usableArea)
    {
        this.usableArea = usableArea;
    }

    public String getBuildArea()
    {
        return buildArea;
    }

    public void setBuildArea(String buildArea)
    {
        this.buildArea = buildArea;
    }

    public String getVolume()
    {
        return volume;
    }

    public void setVolume(String volume)
    {
        this.volume = volume;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public String getFlats()
    {
        return flats;
    }

    public void setFlats(String flats)
    {
        this.flats = flats;
    }

    public String getFee()
    {
        return fee;
    }

    public void setFee(String fee)
    {
        this.fee = fee;
    }
    
    public String getAssignedUser()
    {
        return assignedUser;
    }
    
    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }
    
    public DSUser[] getClerks()
    {
        return clerks;
    }
    
    public DSUser getAuthor()
    {
        return author;
    }
    
    public boolean isCanSetClerk()
    {
        return canSetClerk;
    }
}
