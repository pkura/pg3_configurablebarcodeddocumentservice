package pl.compan.docusafe.web.record.construction;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.FileInputStream;
import java.io.IOException;

/* User: Administrator, Date: 2005-11-29 13:50:39 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionSettingsAction.java,v 1.7 2008/09/12 12:24:41 pecet1 Exp $
 */
public class ConstructionSettingsAction extends ConstructionBaseAction
{
    // @IMPORT
    private FormFile acceptTemplate;
    private FormFile unacceptTemplate;

    // @EXPORT
    private Long acceptTemplateId;
    private Long unacceptTemplateId;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                try
                {
                    acceptTemplateId = Resource.findResource(ConstructionDecision.RES_ACCEPT_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}

                try
                {
                    unacceptTemplateId = Resource.findResource(ConstructionDecision.RES_UNACCEPT_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	//@TODO - do poprawy, usuniecie ifa/calosci moze powodowac bledy w r�nych systemach
            	if(!DSApi.context().isTransactionOpen()){
            		DSApi.context().begin();
            	}

                if (acceptTemplate != null && acceptTemplate.sensible())
                {
                    updateResource(ConstructionDecision.RES_ACCEPT_TEMPLATE, acceptTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji pozytywnej");
                }

                if (unacceptTemplate != null && unacceptTemplate.sensible())
                {
                    updateResource(ConstructionDecision.RES_UNACCEPT_TEMPLATE, unacceptTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej");
                }

                DSApi.context().commit();
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private void updateResource(String systemName, FormFile file)
        throws EdmException, IOException
    {
        if (!file.getName().toLowerCase().endsWith(".rtf"))
            throw new EdmException("Spodziewano si� pliku RTF");

        Resource resource;
        try
        {
            resource = Resource.findResource(systemName);
            resource.setFileName(file.getName());
            resource.update(
                new FileInputStream(file.getFile()),
                (int) file.getFile().length());
        }
        catch (EntityNotFoundException e)
        {
            resource = Resource.create(
                systemName,
                file.getName(),
                file.getContentType(),
                new FileInputStream(file.getFile()),
                (int) file.getFile().length());
        }
    }

    public void setAcceptTemplate(FormFile acceptTemplate)
    {
        this.acceptTemplate = acceptTemplate;
    }

    public void setUnacceptTemplate(FormFile unacceptTemplate)
    {
        this.unacceptTemplate = unacceptTemplate;
    }

    public Long getAcceptTemplateId()
    {
        return acceptTemplateId;
    }

    public Long getUnacceptTemplateId()
    {
        return unacceptTemplateId;
    }
}
