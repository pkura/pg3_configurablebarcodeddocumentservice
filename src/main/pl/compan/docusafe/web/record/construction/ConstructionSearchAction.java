package pl.compan.docusafe.web.record.construction;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.core.record.construction.ConstructionDecision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.record.construction.ConstructionBaseAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/* User: Administrator, Date: 2005-12-01 15:12:45 */

import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ConstructionSearchAction.java,v 1.18 2008/12/12 12:58:33 pecet1 Exp $
 */
public class ConstructionSearchAction extends ConstructionBaseAction
{
	private static final Logger log = LoggerFactory.getLogger(ConstructionSearchAction.class);

    public static final String DECISION_ACCEPTED = "accepted";
    public static final String DECISION_UNACCEPTED = "unaccepted";
    public static final String DECISION_ALL = "all";
    public static final String DECISION_NONE = "none";

    private String thisSearchUrl;
    
    // @EXPORT
    private Map decisions = new LinkedHashMap();
    {
        decisions.put(DECISION_ACCEPTED, "Pozytywne");
        decisions.put(DECISION_UNACCEPTED, "Odmowne");
        decisions.put(DECISION_ALL, "Wszystkie");
        decisions.put(DECISION_NONE, "Brak decyzji");
    }
    private SearchResults results;
    private Pager pager;
    private List<DSUser> users;
    private String printApplicationsUrl;
    private String printDecisionsUrl;
    private String printABUrl;

    // @EXPORT/@IMPORT
    private String decision;
    private String startSubmitDate;
    private String endSubmitDate;
    private String startDecisionDate;
    private String endDecisionDate;
    private String applicationNo;
    private String decisionNo;
    private String registryNo;
    private String creatingUser;

    
    //@IMPORT
    private String sortField;
    private boolean ascending;
    private int offset;
    private final int limit = 10;

    protected void setup()
    {
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintApplications").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new PrintApplications()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintDecisions").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new PrintDecisions()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintAB").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new PrintAB()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(true, false);
        }
    }

    private class PrintApplications implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(false, false);

            File temp = null;

            try
            {
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font font = new Font(baseFont, 12);
                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                pdfDoc.open();

                int[] widths = new int[] { 1, 5, 8, 3, 3, 3 };
                PdfPTable table = new PdfPTable(widths.length);
                table.setWidths(widths);
                table.setWidthPercentage(100);

                table.addCell(new Phrase("Lp", font));
                table.addCell(new Phrase("Imi� i nazwisko lub nazwa inwestora, adres inwestora", font));
                table.addCell(new Phrase("Nazwa, adres i rodzaj obiektu", font));
                table.addCell(new Phrase("Nr i data wp�ywu wniosku", font));
                table.addCell(new Phrase("Nr i data decyzji", font));
                table.addCell(new Phrase("Uwagi", font));

                SearchResults sr = getResults();

                int i=0;
                for (; getResults().hasNext(); )
                {
                    ConstructionApplication ca = (ConstructionApplication) getResults().next();
                    i++;

                    table.addCell(new Phrase(String.valueOf(i), font));
                    table.addCell(new Phrase(TextUtils.concat(new String[] {
                        ca.getDeveloperName(), "\n", ca.getDeveloperStreet(), "\n",
                        ca.getDeveloperZip(), ca.getDeveloperLocation() }), font));
                    table.addCell(new Phrase(ca.getSummary() != null ? ca.getSummary() : "", font));
                    table.addCell(new Phrase(ca.getApplicationNo() + "\n" +
                        DateUtils.formatCommonDate(ca.getSubmitDate()), font));
                    if (ca.getDecisionNo() != null)
                    {
                        table.addCell(new Phrase(ca.getDecisionNo() + "\n" +
                            DateUtils.formatCommonDate(ca.getDecisionDate()), font));
                    }
                    else
                    {
                        table.addCell(new Phrase("", font));
                    }
                    table.addCell(new Phrase(ca.getRemarks() != null ? ca.getRemarks() : "", font));
                }

                pdfDoc.add(new Phrase("Rejestr wniosk�w o pozwoleniu na budow�", font));
                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
            catch (IOException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }

                event.setResult(NONE);
            }
        }
    }

    private class PrintDecisions implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
/*
            if (getDecision().equals(DECISION_NONE))
                addActionError("Nale�y wybra� rodzaj decyzji");
*/

            if (hasActionErrors())
                return;

            search(false, true);

            File temp = null;

            try
            {
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font font = new Font(baseFont, 12);
                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                pdfDoc.open();

                int[] widths = new int[] { 1, 8, 3, 3, 3, 5, 3 };
                PdfPTable table = new PdfPTable(widths.length);
                table.setWidths(widths);
                table.setWidthPercentage(100);

                table.addCell(new Phrase("Lp", font));
                table.addCell(new Phrase("Nazwa, adres i rodzaj obiektu", font));
                table.addCell(new Phrase("Nr i data wp�ywu wniosku", font));
                table.addCell(new Phrase("Nr i data decyzji", font));
                table.addCell(new Phrase("Informacja o z�o�onym odwo�aniu", font));
                table.addCell(new Phrase("Informacja o decyzji wydanej w trybie odwo�awczym", font));
                table.addCell(new Phrase("Uwagi", font));

                int i=0;
                for (; getResults().hasNext(); )
                {
                    ConstructionApplication ca = (ConstructionApplication) getResults().next();

                    if (ca.getDecisionNo() == null)
                        continue;

                    i++;

                    table.addCell(new Phrase(String.valueOf(i), font));
                    table.addCell(new Phrase(ca.getSummary() != null ? ca.getSummary() : "", font));
                    table.addCell(new Phrase(ca.getApplicationNo() + "\n" +
                        DateUtils.formatCommonDate(ca.getSubmitDate()), font));
                    if (ca.getDecisionNo() != null)
                    {
                        table.addCell(new Phrase(ca.getDecisionNo() + "\n" +
                            DateUtils.formatCommonDate(ca.getDecisionDate()), font));
                    }
                    else
                    {
                        table.addCell(new Phrase("", font));
                    }
                    table.addCell(new Phrase("", font));
                    table.addCell(new Phrase("", font));
                    table.addCell(new Phrase(ca.getRemarks() != null ? ca.getRemarks() : "", font));
                }

                pdfDoc.add(new Phrase("Rejestr decyzji o pozwoleniu na budow�", font));
                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
            catch (IOException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }

                event.setResult(NONE);
            }
        }
    }

    private class PrintAB implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(false, false);

            File temp = null;

            try
            {
                File fontDir = new File(Docusafe.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font font = new Font(baseFont, 12);
                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                pdfDoc.open();

                int[] widths = new int[] { 2, 8, 8, 4, 4, 4, 4, 4, 4, 4 };
                PdfPTable table = new PdfPTable(widths.length);
                table.setWidths(widths);
                table.setWidthPercentage(100);

                table.addCell(new Phrase("Lp", font));
                table.addCell(new Phrase("Imi� i nazwisko lub nazwa inwestora, adres inwestora", font));
                table.addCell(new Phrase("Nazwa, adres i rodzaj obiektu", font));
                table.addCell(new Phrase("Rodzaj decyzji", font));
                table.addCell(new Phrase("Pow. u�ytkowa\nPow. zabudowy\nKubatura", font));
                table.addCell(new Phrase("Liczba izb\nLiczba mieszka�", font));
                table.addCell(new Phrase("Nr i data wp�ywu wniosku", font));
                table.addCell(new Phrase("Nr i data decyzji", font));
                table.addCell(new Phrase("Op�ata skarbowa", font));
                table.addCell(new Phrase("Uwagi", font));

                int i=0;
                for (; getResults().hasNext(); )
                {
                    ConstructionApplication ca = (ConstructionApplication) getResults().next();
                    ConstructionDecision cd = ca.getConstructionDecision();
                    i++;

                    table.addCell(new Phrase(String.valueOf(i), font));
                    table.addCell(new Phrase(TextUtils.concat(new String[] {
                        ca.getDeveloperName(), "\n", ca.getDeveloperStreet(), "\n",
                        ca.getDeveloperZip(), ca.getDeveloperLocation() }), font));
                    table.addCell(new Phrase(ca.getSummary() != null ? ca.getSummary() : "", font));
                    if (cd != null)
                    {
                        table.addCell(new Phrase(cd.getAccepted() != null && cd.getAccepted().booleanValue() ? "pozytywna" : "odmowna", font));
                    }
                    else
                    {
                        table.addCell(new Phrase("", font));
                    }

                    table.addCell(new Phrase(
                        (ca.getUsableArea() != null ? ca.getUsableArea()+"\n" : "-\n") +
                        (ca.getBuildArea() != null ? ca.getBuildArea()+"\n" : "-\n") +
                        (ca.getVolume() != null ? ca.getVolume()+"\n" : "-"), font));

                    table.addCell(new Phrase(
                        (ca.getRooms() != null ? ca.getRooms()+"\n" : "-\n") +
                        (ca.getFlats() != null ? ca.getFlats()+"\n" : "-\n"), font));

                    table.addCell(new Phrase(ca.getApplicationNo() + "\n" +
                        DateUtils.formatCommonDate(ca.getSubmitDate()), font));
                    if (ca.getDecisionNo() != null)
                    {
                        table.addCell(new Phrase(ca.getDecisionNo() + "\n" +
                            DateUtils.formatCommonDate(ca.getDecisionDate()), font));
                    }
                    else
                    {
                        table.addCell(new Phrase("", font));
                    }
                    table.addCell(new Phrase(ca.getFee() != null ? ca.getFee().toString() : "", font));
                    table.addCell(new Phrase(ca.getRemarks() != null ? ca.getRemarks() : "", font));

                }

                pdfDoc.add(table);
                pdfDoc.close();
            }
            catch (DocumentException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
            catch (IOException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }

            if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }

                event.setResult(NONE);
            }
        }
    }

    /**
     * Wykonuje przeszukiwanie, uaktualnia zmienne results i pager.
     */
    public void search(boolean applyOffsetLimit, boolean decisions)
    {
        final String searchUrl =
            HttpUtils.makeUrl("/record/construction/construction-search.action",
                new Object[] {
                    "startSubmitDate", startSubmitDate,
                    "endSubmitDate", endSubmitDate,
                    "startDecisionDate", startDecisionDate,
                    "endDecisionDate", endDecisionDate,
                    "decision", decision,
                    "applicationNo", applicationNo,
                    "decisionNo", decisionNo,
                    "registryNo", registryNo,
                    "creatingUser", creatingUser,
                    "x", "y"
                });

        thisSearchUrl = searchUrl +
            "&doSearch=true";

        printApplicationsUrl = searchUrl + "&doPrintApplications=true";
        printDecisionsUrl = searchUrl + "&doPrintDecisions=true";
        printABUrl = searchUrl + "&doPrintAB=true";

        ConstructionApplication.Query query;

        if (applyOffsetLimit)
            query = new ConstructionApplication.Query(offset, limit);
        else
            query = new ConstructionApplication.Query();

        query.submitDate(
            DateUtils.nullSafeParseJsDate(startSubmitDate),
            DateUtils.nullSafeParseJsDate(endSubmitDate));
        query.decisionDate(
            DateUtils.nullSafeParseJsDate(startDecisionDate),
            DateUtils.nullSafeParseJsDate(endDecisionDate));
        query.applicationNo(TextUtils.trimmedStringOrNull(applicationNo));
        query.decisionNo(TextUtils.trimmedStringOrNull(decisionNo));
        query.registryNo(TextUtils.trimmedStringOrNull(registryNo));
        query.setAuthor(TextUtils.trimmedStringOrNull(creatingUser));
        if (decisions)
            query.setDefaultDecisionSort(true);
        else {
            query.setSortField(sortField);
            query.setAscending(ascending);
        }
        
        if (DECISION_ACCEPTED.equals(decision))
            query.decision(true);
        else if (DECISION_UNACCEPTED.equals(decision))
            query.decision(false);
        else if (DECISION_ALL.equals(decision))
            query.anyDecision();
        else if (DECISION_NONE.equals(decision))
            query.noDecision();

        try
        {
            results = ConstructionApplication.search(query);

            if (results.totalCount() == 0)
                addActionMessage("Nie znaleziono wniosk�w");

            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
            {
                public String getLink(int offset)
                {
                    return thisSearchUrl + (sortField != null ? ("&sortField="+sortField+"&ascending="+ascending) : "") +
                        "&offset="+offset;
                }
            };
            pager = new Pager(linkVisitor, offset, limit, results.totalCount(), 10);
        }
        catch (EdmException e)
        {
			log.error(e.getMessage(),e);
            addActionError(e.getMessage());
        }
    }

    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }
    
    public Map getDecisions()
    {
        return decisions;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public Pager getPager()
    {
        return pager;
    }

    public List getUsers()
    {
        return users;
    }
    
    public String getDecision()
    {
        return decision;
    }

    public void setDecision(String decision)
    {
        this.decision = decision;
    }

    public String getStartSubmitDate()
    {
        return startSubmitDate;
    }

    public void setStartSubmitDate(String startSubmitDate)
    {
        this.startSubmitDate = startSubmitDate;
    }

    public String getEndSubmitDate()
    {
        return endSubmitDate;
    }

    public void setEndSubmitDate(String endSubmitDate)
    {
        this.endSubmitDate = endSubmitDate;
    }

    public String getStartDecisionDate()
    {
        return startDecisionDate;
    }

    public void setStartDecisionDate(String startDecisionDate)
    {
        this.startDecisionDate = startDecisionDate;
    }

    public String getEndDecisionDate()
    {
        return endDecisionDate;
    }

    public void setEndDecisionDate(String endDecisionDate)
    {
        this.endDecisionDate = endDecisionDate;
    }

    public String getApplicationNo()
    {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo)
    {
        this.applicationNo = applicationNo;
    }

    public String getDecisionNo()
    {
        return decisionNo;
    }

    public void setDecisionNo(String decisionNo)
    {
        this.decisionNo = decisionNo;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getPrintApplicationsUrl()
    {
        return printApplicationsUrl;
    }

    public String getPrintDecisionsUrl()
    {
        return printDecisionsUrl;
    }

    public String getPrintABUrl()
    {
        return printABUrl;
    }

    public String getRegistryNo()
    {
        return registryNo;
    }

    public void setRegistryNo(String registryNo)
    {
        this.registryNo = registryNo;
    }
    
    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }
    
    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }
}
