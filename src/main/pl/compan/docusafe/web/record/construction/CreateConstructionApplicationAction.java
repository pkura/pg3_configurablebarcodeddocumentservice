package pl.compan.docusafe.web.record.construction;

import java.math.BigDecimal;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.record.construction.ConstructionApplication;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/* User: Administrator, Date: 2005-11-26 15:31:05 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CreateConstructionApplicationAction.java,v 1.16 2010/04/21 11:31:17 mariuszk Exp $
 */
public class CreateConstructionApplicationAction extends ConstructionBaseAction
{
	private static final Logger log = LoggerFactory.getLogger(CreateConstructionApplicationAction.class);
    private Long documentId;
    private Long caseId;
    private Long id;

    // @EXPORT/@IMPORT
    private String registryNo;
    private Integer sequenceId;
    private String summary;
    private String developerName;
    private String developerStreet;
    private String developerZip;
    private String developerLocation;
    private String applicationNo;
    private String remarks;

    private String registryNoVerify;
    private Integer sequenceIdVerify;

    private boolean submitted;

    private String usableArea;
    private String buildArea;
    private String volume;
    private String rooms;
    private String flats;
    private String fee;

    // @EXPORT
    private String submitDate;
    private boolean canCreateApplication;

    private static final String EV_FILL = "fill";

    // uwaga - akcja wywo�ywana jest pocz�tkowo z CaseTabAction
    // z wydarzeniem doPreCreate
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        CheckPermissions checkPermissions = new CheckPermissions();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

  /*      registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
*/
        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(checkPermissions).
            append(new Create()).
            //append(EV_FILL, fillForm).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    // 1. wy�wietlenie pocz�tkowego formularza z przyciskiem doCreate
    //    sugestia subrejestru i numeru kolejnego; zmiana powoduje ponowne
    //    wygenerowanie tych numer�w
    // 2. Create - pomy�lne powoduje skip(fillform)

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                OfficeCase officeCase = OfficeCase.find(caseId);
                String officeId = OfficeCase.find(caseId).getOfficeId();
                if (!officeId.matches(".*7351*.*")) {
                    canCreateApplication = false;
                    addActionError("Nie mo�na utworzy� wniosku, gdy� pismo nie znajduje si� w teczce 7351");
                }
                else
                    canCreateApplication = true;
                // pola wype�niane s� na podstawie dokumentu tylko przy
                // pierwszym wy�wietleniu formularza; po klikni�ciu przycisku
                // doCreate powinny w nich pozosta� warto�ci wpisane przez
                // u�ytkownika
                if (!submitted)
                {
                    try
                    {
                        /*ConstructionApplication ca = */ConstructionApplication.findByOfficeCaseId(caseId);
                        addActionError("Dla tej sprawy istnieje ju� wniosek");
                        return;
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                    OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
                    if (!(document.getType() == DocumentType.INCOMING))
                    {
                        addActionError("Spodziewano si� dokumentu przychodz�cego");
                        return;
                    }
                    InOfficeDocument indocument = (InOfficeDocument) document;

                    submitDate = DateUtils.formatJsDate(indocument.getIncomingDate());
                    summary = document.getSummary().replace('\n', ' ').replace('\r', ' ');
                    developerName = TextUtils.concat(new Object[] {
                        indocument.getSender().getFirstname(),
                        indocument.getSender().getLastname(),
                        indocument.getSender().getOrganization() });
                    developerStreet = indocument.getSender().getStreet();
                    developerZip = indocument.getSender().getZip();
                    developerLocation = indocument.getSender().getLocation();
                    registryNo = TextUtils.trimmedStringOrNull(DSApi.context().getDSUser().getIdentifier());
                    sequenceId = ConstructionApplication.suggestSequenceId(registryNo);
                    applicationNo = ConstructionApplication.suggestApplicationNo(officeCase);
                }

                String _registryNo = TextUtils.trimmedStringOrNull(registryNo);
                String _registryNoVerify = TextUtils.trimmedStringOrNull(registryNoVerify);
               
                if (_registryNo != null &&
                    !_registryNo.equals(_registryNoVerify))
                {
                    sequenceId = ConstructionApplication.suggestSequenceId(registryNo);
                    applicationNo = ConstructionApplication.suggestApplicationNo(officeCase);
                }
                if (sequenceId == null || sequenceIdVerify == null ||
                    sequenceId.intValue() != sequenceIdVerify.intValue())
                {
                    applicationNo = ConstructionApplication.suggestApplicationNo(officeCase);
                }
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage());
            }
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           /* try 
            {
                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }*/
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String _summary = TextUtils.trimmedStringOrNull(summary);
            String _developerName = TextUtils.trimmedStringOrNull(developerName);
            String _developerStreet = TextUtils.trimmedStringOrNull(developerStreet);
            String _developerZip = TextUtils.trimmedStringOrNull(developerZip);
            String _developerLocation = TextUtils.trimmedStringOrNull(developerLocation);
            String _applicationNo = TextUtils.trimmedStringOrNull(applicationNo);
            String _registryNo = TextUtils.trimmedStringOrNull(registryNo);
            String _registryNoVerify = TextUtils.trimmedStringOrNull(registryNoVerify);
            //BigDecimal _usableArea = parseBigDecimal(usableArea);
            //BigDecimal _buildArea = parseBigDecimal(buildArea);
            //BigDecimal _volume = parseBigDecimal(volume);
            BigDecimal _fee = parseBigDecimal(fee);

            if (_summary == null)
                addActionError("Nie podano opisu wniosku");
            if (_developerName == null)
                addActionError("Nie podano nazwy inwestora");
            if (_applicationNo == null)
                addActionError("Nie podano znaku wniosku");
            if (_registryNo == null)
                addActionError("Nie podano numeru subrejestru");
            if (sequenceId == null)
                addActionError("Nie podano numeru kolejnego wniosku");
            if (sequenceId != null && sequenceId.intValue() < 1)
                addActionError("Numer kolejny wniosku musi by� dodatni");

            if (hasActionErrors())
            {
                //event.skip(EV_FILL);
                return;
            }
            // sprawdzenie, czy zmieniono warto�ci registryNo lub sequenceId
            // (poprzez pola ukryte)

            // je�eli registryNo lub sequenceId si� zmieni�y - addMessage
            // i return

            if (_registryNo != null &&
                !_registryNo.equals(_registryNoVerify))
            {
                addActionMessage("Wygenerowano ponownie znak wniosku, poniewa� " +
                    "u�ytkownik zmieni� numer subrejestru");
                return;
            }

            if (sequenceId == null || sequenceIdVerify == null ||
                sequenceId.intValue() != sequenceIdVerify.intValue())
            {
                addActionMessage("Wygenerowano ponownie znak wniosku, poniewa� " +
                    "u�ytkownik zmieni� numer kolejny wniosku w subrejestrze");
                return;
            }

            try
            {
                DSApi.context().begin();

                OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
                if (!(document.getType() == DocumentType.INCOMING))
                {
                    addActionError("Spodziewano si� dokumentu przychodz�cego");
                    return;
                }
                InOfficeDocument indocument = (InOfficeDocument) document;

                OfficeCase officeCase = OfficeCase.find(caseId);
                try
                {
                    ConstructionApplication gr = ConstructionApplication.findByOfficeCase(officeCase);
                    throw new EdmException("Ze spraw� jest ju� zwi�zany wniosek " +
                        "o numerze "+gr.getSequenceId());
                }
                catch (EntityNotFoundException e)
                {
                }
                ConstructionApplication ca = new ConstructionApplication();
                ca.setSubmitDate(indocument.getIncomingDate());
                ca.setSummary(_summary);
                ca.setDeveloperName(_developerName);
                ca.setDeveloperStreet(_developerStreet);
                ca.setDeveloperZip(_developerZip);
                ca.setDeveloperLocation(_developerLocation);
                ca.setSequenceId(sequenceId);
                ca.setApplicationNo(_applicationNo);
                ca.setRegistryNo(_registryNo);
                ca.setRemarks(TextUtils.trimmedStringOrNull(remarks));
                ca.setInDocumentId(documentId);
                ca.setWithdrawn(Boolean.FALSE);
                ca.setUsableArea(usableArea);
                ca.setBuildArea(buildArea);
                ca.setVolume(volume);
                ca.setRooms(rooms);
                ca.setFlats(flats);
                ca.setFee(_fee);
                ca.create(officeCase);
                ca.getOfficeCase().getAudit().add(Audit.create("addConstruction",DSApi.context().getPrincipalName(),
                    "Utworzono wniosek o zezwolenie na budow� numer: "+ _applicationNo,_applicationNo));

                id = ca.getId();

                DSApi.context().commit();

                event.setResult("construction-application");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

        private BigDecimal parseBigDecimal(String s)
        {
            if (s == null)
                return null;
            try
            {
                return new BigDecimal(s.replace(',', '.'));
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

    public Long getCaseId()
    {
        return caseId;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public Long getId()
    {
        return id;
    }

    public String getRegistryNo()
    {
        return registryNo;
    }

    public void setRegistryNo(String registryNo)
    {
        this.registryNo = registryNo;
    }

    public String getSubmitDate()
    {
        return submitDate;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getDeveloperName()
    {
        return developerName;
    }

    public void setDeveloperName(String developerName)
    {
        this.developerName = developerName;
    }

    public String getDeveloperStreet()
    {
        return developerStreet;
    }

    public void setDeveloperStreet(String developerStreet)
    {
        this.developerStreet = developerStreet;
    }

    public String getDeveloperZip()
    {
        return developerZip;
    }

    public void setDeveloperZip(String developerZip)
    {
        this.developerZip = developerZip;
    }

    public String getDeveloperLocation()
    {
        return developerLocation;
    }

    public void setDeveloperLocation(String developerLocation)
    {
        this.developerLocation = developerLocation;
    }

    public String getApplicationNo()
    {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo)
    {
        this.applicationNo = applicationNo;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public String getRegistryNoVerify()
    {
        return registryNoVerify;
    }

    public void setRegistryNoVerify(String registryNoVerify)
    {
        this.registryNoVerify = registryNoVerify;
    }

    public Integer getSequenceIdVerify()
    {
        return sequenceIdVerify;
    }

    public void setSequenceIdVerify(Integer sequenceIdVerify)
    {
        this.sequenceIdVerify = sequenceIdVerify;
    }

    public void setSubmitted(boolean submitted)
    {
        this.submitted = submitted;
    }

    public String getUsableArea()
    {
        return usableArea;
    }

    public void setUsableArea(String usableArea)
    {
        this.usableArea = usableArea;
    }

    public String getBuildArea()
    {
        return buildArea;
    }

    public void setBuildArea(String buildArea)
    {
        this.buildArea = buildArea;
    }

    public String getVolume()
    {
        return volume;
    }

    public void setVolume(String volume)
    {
        this.volume = volume;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public String getFlats()
    {
        return flats;
    }

    public void setFlats(String flats)
    {
        this.flats = flats;
    }

    public String getFee()
    {
        return fee;
    }

    public void setFee(String fee)
    {
        this.fee = fee;
    }
 
    public boolean isCanCreateApplication()
    {
        return canCreateApplication;
    }
}
