package pl.compan.docusafe.web.record.construction;


import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.core.record.construction.DecisionArchive;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import java.util.Date;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.LogFactory;


public class DecisionArchiveAction extends EventActionSupport {
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DecisionArchiveAction.class.getPackage().getName(),null);

    // @IMPORT
    private FormFile file;
    private boolean done;

	
	public void setFile(FormFile file) {
		this.file = file;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
        }
    }
	
	private class LoadFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (file == null )
            {
                addActionError(sm.getString("NiePodanoPliku"));
                return;
            }
            
        	HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"raport.txt\"");
            
            try
            {
                done = parseAndSaveOutgoingFile(file.getFile(),response.getOutputStream());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
            catch(IOException e)
            {
            	addActionError(e.getMessage());
            	addActionError(sm.getString("ImportZostalWykonanyAleNieMoznaByloZapisacRaportu"));
            }
            if(done) addActionMessage(sm.getString("PomyslnieZakonczonoImport"));
            else addActionError(sm.getString("WystapilyProblemyZimportem"));
        }
    }
	
	public String getZaladuj()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("Zaladuj");
    }
	
	public static boolean parseAndSaveOutgoingFile(File file, OutputStream output) throws EdmException
    {
    	boolean result=true;
        try
        {
        	PrintWriter writer = new PrintWriter(output);
        	writer.println(sm.getString("RozpoczetoImport"));
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0, errorCount = 0;
            String[] prevParts = {""};
            boolean koniec = false;
        
        	if(reader.readLine() == null) {
        		koniec = true;
        	}
			DSApi.context().begin();
            while (!koniec)
            {
            	if((line = reader.readLine()) == null) {
            		line = " ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;";
            		koniec = true;
            	}
                try {
					lineCount++;
					//if(lineCount%10==0) DSApi.context().session().clear();
					line = line.trim();
					if (line.startsWith("#")){
					    lineCount--;
						continue;
					}
					String[] parts = new String[10];
					parts = line.split(";");
					
					/*if(parts.length<10) {
						lineCount--;
						continue;
					}*/
					if(lineCount == 1) prevParts = parts;
					if(!(parts[1].replace("\"","").trim()).equals(""))
					{
						DecisionArchive da = new DecisionArchive();
						if(parts.length > 0)
							da.setName(parts[0].replace("\"","").trim());
						if(parts.length > 1)
							da.setAddress(parts[1].replace("\"","").trim());
						if(parts.length > 2)
							da.setObject_kind(parts[2].replace("\"","").trim());
						if(parts.length > 3)
							da.setProposition_no(parts[3].replace("\"","").trim());
						if(parts.length > 4)
							da.setProposition_in_date(parseDate(parts[4].replace("\"","").trim()));
						if(parts.length > 5)
							da.setDecision_no(parts[5].replace("\"","").trim());
						if(parts.length > 6)
							da.setDecision_date(parseDate(parts[6].replace("\"","").trim()));
						if(parts.length > 7)
							da.setReference(parts[7].replace("\"","").trim());
						if(parts.length > 8)
							da.setDecision_after(parts[8].replace("\"","").trim());
						if(parts.length > 9)
							da.setNotice(parts[9].replace("\"","").trim());
						
						boolean created = da.create();
						if(created)
							writer.println(sm.getString("OkZaimportowanoDecyzje")+": "+parts[1].replace("\"","").trim());
						else
						{
							writer.println(sm.getString("NieZaimportowanoDecyzji")+": "+parts[1].replace("\"","").trim());
							writer.println(sm.getString("PrwdopodobnieIstniejeJuzTakDecyzja"));
							errorCount++;
						}
					}
                
				}
                catch(EdmException e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
                catch(Exception e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
                
            }
            lineCount--;
            writer.println(sm.getString("CalkowitaLiczbaBlednychImportow")+": " + errorCount);
            writer.println(sm.getString("CalkowitaLiczbaDecyzji")+": " + lineCount);
            writer.close();

			DSApi.context().commit();
            DSApi.context().session().clear();
            return result;

        }
        catch (IOException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
            throw new EdmException(sm.getString("BladPodczasCzytaniaPliku"));
        }
    }
	
	private static Date parseDate(String d)
    {
		Date result = null;
    	String data = d.substring(6);
    	data += "-" + d.substring(4, 6);
    	data += "-" + d.substring(0, 4);
    	result = DateUtils.nullSafeParseJsDate(data);
    	//result= new java.sql.Date(tmp.getYear(), tmp.getMonth(), tmp.getDay());
    	return result;
    	
    }

	public FormFile getFile() {
		return file;
	}
}
