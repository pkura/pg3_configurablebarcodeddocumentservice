package pl.compan.docusafe.web.record.construction;

import java.security.AccessControlException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * Bazowa akcja dla wszystkich akcji zwi�zanych z modu�em Construction.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public abstract class ConstructionBaseAction extends EventActionSupport
{
    /**
     * Sprawdza uprawnienia dost�pu do modu�u Architektura.
     */
    protected class CheckPermissions implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!Docusafe.moduleAvailable(Modules.MODULE_CONSTRUCTION))
                    throw new AccessControlException("Wymagany jest modu� Architektura");
                
                if (!DSApi.context().hasPermission(DSPermission.CNS_DOSTEP_OGOLNY))
                    throw new AccessControlException("Nie masz uprawnie� dost�pu do modu�u Architektura");
            }
            catch (EdmException e)
            {
                throw new IllegalStateException("Nie mo�na wej�� na ��dan� stron� - wyst�pi� nieoczekiwany b��d podczas sprawdzania uprawnie� dost�pu");
            }
        }
    }
}
