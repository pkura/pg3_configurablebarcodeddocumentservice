package pl.compan.docusafe.web.record.construction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.AbstractQuery.SortField;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.record.construction.DecisionArchive;
import pl.compan.docusafe.core.record.construction.DecisionArchive.DAQuery;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

public class DecisionArchiveListAction extends EventActionSupport {
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DecisionArchiveAction.class.getPackage().getName(),null);
	List <?extends DecisionArchive> lista;
	SearchResults<?extends DecisionArchive> list;
	//pdf
	private int fontsize;
    private static final Log log = LogFactory.getLog(DecisionArchiveListAction.class);
    private int offset;
    private final int limit = 0;
    private Pager pager;
    private String thisSearchUrl;
    //search
    private String name;
	private String address;
	private String object_kind;
	private String proposition_no;
	private Date proposition_in_date;
	private String decision_no;
	private Date decision_date;
	private String reference;
	private String decision_after;
	private String notice;
	private String startSubmitDate;
    private String endSubmitDate;
    private String startDecisionDate;
    private String endDecisionDate;
    private boolean bezPodzialu;
    private String pdfUrl;
    private List<String> documentIds;

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	public boolean isBezPodzialu() {
		return bezPodzialu;
	}

	public void setBezPodzialu(boolean bezPodzialu) {
		this.bezPodzialu = bezPodzialu;
	}

	public String getStartSubmitDate() {
		return startSubmitDate;
	}

	public void setStartSubmitDate(String startSubmitDate) {
		this.startSubmitDate = startSubmitDate;
	}

	public String getEndSubmitDate() {
		return endSubmitDate;
	}

	public void setEndSubmitDate(String endSubmitDate) {
		this.endSubmitDate = endSubmitDate;
	}

	public String getStartDecisionDate() {
		return startDecisionDate;
	}

	public void setStartDecisionDate(String startDecisionDate) {
		this.startDecisionDate = startDecisionDate;
	}

	public String getEndDecisionDate() {
		return endDecisionDate;
	}

	public void setEndDecisionDate(String endDecisionDate) {
		this.endDecisionDate = endDecisionDate;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPdf").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Pdf()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSearch").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Search()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	/*
	        	DAQuery query = new DAQuery(limit, offset);
	        	query.setSortFiled(new QueryForm.SortField("id",true));
	        	query.setOffset(10);
	        	query.setLimit(50);
	        	list = DecisionArchive.search(query);
	        	lista = fun.list(list);
	        	*/
        	try{
	        	final String searchUrl ="/record/construction/decision-archive-list.action?";
	        	thisSearchUrl = searchUrl +
	            	"&doSearch=true";
	        	DAQuery query = new DAQuery(limit, offset);
	        	query.setSortFiled(new QueryForm.SortField("id",true));
	        	list = DecisionArchive.search(query);
	        	lista = fun.list(list);
	        	if (list.totalCount() == 0)
	                addActionMessage("Nie znaleziono wniosków");
	
	            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
	            {
	                public String getLink(int offset)
	                {
	                    return thisSearchUrl + "&offset="+offset;
	                }
	            };
	        	pager = new Pager(linkVisitor, offset, limit, list.totalCount(), 100);
        	}
        	catch (Exception e) {
				// TODO: handle exception
			}
        }
    }
	
	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
        {
			try{
				final String searchUrl ="/record/construction/decision-archive-list.action?";
	        	thisSearchUrl = searchUrl +
	            	"&doSearch=true";
	        	pdfUrl =
	                HttpUtils.makeUrl("/record/construction/decision-archive-list.action",
	                    new Object[] {
	                        "startSubmitDate", startSubmitDate,
	                        "endSubmitDate", endSubmitDate,
	                        "startDecisionDate", startDecisionDate,
	                        "endDecisionDate", endDecisionDate,
	                        "name", name,
	                        "address", address,
	                        "object_kind", object_kind,
	                        "proposition_no", proposition_no,
	                        "decision_no", decision_no
	                    }); 
	        	DAQuery query = new DAQuery(limit, offset, name, address, object_kind, proposition_no, decision_no);
	        	query.setSortFiled(new QueryForm.SortField("id",true));
	        	query.setOffset(offset);
	        	query.setLimit(limit);
	        	query.setProposition_in_date_from(DateUtils.nullSafeParseJsDate(startSubmitDate));
	        	query.setProposition_in_date_to(DateUtils.nullSafeParseJsDate(endSubmitDate));
	        	query.setDecision_date_from(DateUtils.nullSafeParseJsDate(startDecisionDate));
	        	query.setDecision_date_to(DateUtils.nullSafeParseJsDate(endDecisionDate));
	        	
	        	list = DecisionArchive.search(query);
	        	lista = fun.list(list);
	        	if (list.totalCount() == 0)
	                addActionMessage("Nie znaleziono wniosków");
	
	            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
	            {
	                public String getLink(int offset)
	                {
	                    return thisSearchUrl + "&offset="+offset;
	                }
	            };
	        	pager = new Pager(linkVisitor, offset, limit, list.totalCount(), 100);
	        	DSApi.context().setAttribute("offset", offset);
	        	DSApi.context().setAttribute("name", name);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
        }
	}
	
	private class Pdf implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File temp = null;
        	try{
	            File fontDir = new File(Configuration.getHome(), "fonts");
	            File arial = new File(fontDir, "arial.ttf");
	            fontsize = 12;
	            /*
	            DAQuery query = new DAQuery();
	        	query.setSortFiled(new QueryForm.SortField("id",true));
	        	query.setOffset(offset);
	        	query.setLimit(limit);
	        	*/
	            DAQuery query = new DAQuery(limit, offset, name, address, object_kind, proposition_no, 

decision_no);
	        	query.setSortFiled(new QueryForm.SortField("id",true));
	        	query.setOffset(offset);
	        	query.setLimit(limit);
	        	query.setProposition_in_date_from(DateUtils.nullSafeParseJsDate(startSubmitDate));
	        	query.setProposition_in_date_to(DateUtils.nullSafeParseJsDate(endSubmitDate));
	        	query.setDecision_date_from(DateUtils.nullSafeParseJsDate(startDecisionDate));
	        	query.setDecision_date_to(DateUtils.nullSafeParseJsDate(endDecisionDate));
	        	list = DecisionArchive.search(query);
	        	lista = fun.list(list);
	        	if (documentIds != null)
	            {
	        		for (int i = 0; i < lista.size(); i++) {
						if(!documentIds.contains(lista.get(i).getDecision_no())){
							lista.remove(i);
							i--;
						}
					}
	            }
	        	BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
	            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
	
	            Font font = new Font(baseFont, (float)fontsize);
	
	            temp = File.createTempFile("docusafe_", "_tmp");
	            Document pdfDoc =
	                new Document(PageSize.A4.rotate(),30,60,30,30);
	            PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
	            /*
	            HeaderFooter footer =
	           	 new HeaderFooter(new 

Phrase(sm.getString("WydrukSporzadzonyPrzezWdniu",DSApi.context().getDSUser().asFirstnameLastname(),
	                        DateUtils.formatCommonDateTime(new Date())), font), new Phrase("."));
	            footer.setAlignment(Element.ALIGN_CENTER);
	            pdfDoc.setFooter(footer);
	            */
//otwarcie pdf'a
	            pdfDoc.open();

                PdfUtils.addHeaderImageToPdf(pdfDoc, sm.getString("ArchiwalneDecyzje"), baseFont);
                
                int[] widths;
                widths = (new int[] { 2, 8, 3, 3, 3, 3, 3, 5, 3});
                
                Table table = new Table(widths.length);
                table.setWidths(widths);
                table.setWidth(100);
                table.setCellsFitPage(true);
                table.setPadding(3);
                //table.setLastHeaderRow(0);
                
                Cell _lp  = new Cell(new Phrase(sm.getString("Lp"), font));
                _lp.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_lp);
                
                Cell _NazwaAdresIrodzajObiektu = new Cell(new Phrase(sm.getString("NazwaAdresIrodzajObiektu"), 

font));
                _NazwaAdresIrodzajObiektu.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_NazwaAdresIrodzajObiektu);

                Cell _NrWniosku = new Cell(new Phrase(sm.getString("NrWniosku"), font));
                _NrWniosku.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_NrWniosku);
                
                Cell _DataWniosku = new Cell(new Phrase(sm.getString("DataWniosku"), font));
                _DataWniosku.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_DataWniosku);
                
                Cell _NrDecyzji = new Cell(new Phrase(sm.getString("NrDecyzji"), font));
                _NrDecyzji.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_NrDecyzji);
                

                Cell _DataDecyzji = new Cell(new Phrase(sm.getString("DataDecyzji"), font));
                _DataDecyzji.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_DataDecyzji);

                Cell _InformacjaZlozonymOdwolaniu = new Cell(new Phrase(sm.getString("InformacjaZlozonymOdwolaniu"), 

font));
                _InformacjaZlozonymOdwolaniu.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_InformacjaZlozonymOdwolaniu);

                Cell _InformacjaOdecyzji = new Cell(new 

Phrase(sm.getString("InformacjaOdecyzjiWydanejWtrybieOdwolawczym"), font));
                _InformacjaOdecyzji.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_InformacjaOdecyzji);

                Cell _Uwagi = new Cell(new Phrase(sm.getString("Uwagi"), font));
                _Uwagi.setHorizontalAlignment(Cell.ALIGN_CENTER);
                table.addCell(_Uwagi);
                
                table.endHeaders();
                
                for (int i = 0; i < lista.size(); i++) {
					DecisionArchive da = lista.get(i);
					//lp
					table.addCell(new Phrase(String.valueOf(i+1), font));
					//Nazwa Adres i rodzaj objektu
					table.addCell(new Phrase(da.getName()+"\n"+da.getAddress()+"\n"+da.getObject_kind(), font));
					//NrIdataWplywuWniosku
					table.addCell(new Phrase(da.getProposition_no(), font));
					if(da.getProposition_in_date() != null)
						table.addCell(new Phrase(da.getProposition_in_date().toLocaleString().substring(0, 10), font));
					else
						table.addCell(new Phrase("", font));
					//NrIdataDecyzji
					table.addCell(new Phrase(da.getDecision_no(), font));
					if(da.getDecision_date() != null)
						table.addCell(new Phrase(da.getDecision_date().toLocaleString().substring(0, 10), font));
					else
						table.addCell(new Phrase("", font));
					//InformacjaZlozonymOdwolaniu
					table.addCell(new Phrase(da.getReference(), font));
					//InformacjaOdecyzjiWydanejWtrybieOdwoławczym
					table.addCell(new Phrase(da.getDecision_after(), font));
					//Uwagi
					table.addCell(new Phrase(da.getNotice(), font));
				}
                pdfDoc.add(new Phrase(sm.getString("RejestrDecyzjiOpozwoleniuNaBudowe"), font));
                pdfDoc.add(table);
                
//zamkniecie pdf'a
                pdfDoc.close();
                
        	}
            catch (DocumentException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            }
            catch (IOException e)
            {
                event.getLog().error(sm.getString("BladGenerowaniaPdf")+e.getMessage(), e);
            } catch (EdmException e) {
				// TODO Auto-generated catch block
            	LogFactory.getLog("eprint").debug("", e);
			}
            
        	if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length(),
                        "Content-Disposition: inline; filename=\"dziennik.pdf\"");
                    ServletActionContext.getResponse().getOutputStream().flush();
                    ServletActionContext.getResponse().getOutputStream().close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

	public List<? extends DecisionArchive> getLista() {
		return lista;
	}

	public void setLista(List<? extends DecisionArchive> lista) {
		this.lista = lista;
	}
	
	public void setFontsize(int size){
    	this.fontsize=size;
    }

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getObject_kind() {
		return object_kind;
	}

	public void setObject_kind(String object_kind) {
		this.object_kind = object_kind;
	}

	public String getProposition_no() {
		return proposition_no;
	}

	public void setProposition_no(String proposition_no) {
		this.proposition_no = proposition_no;
	}

	public Date getProposition_in_date() {
		return proposition_in_date;
	}

	public void setProposition_in_date(Date proposition_in_date) {
		this.proposition_in_date = proposition_in_date;
	}

	public String getDecision_no() {
		return decision_no;
	}

	public void setDecision_no(String decision_no) {
		this.decision_no = decision_no;
	}

	public Date getDecision_date() {
		return decision_date;
	}

	public void setDecision_date(Date decision_date) {
		this.decision_date = decision_date;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDecision_after() {
		return decision_after;
	}

	public void setDecision_after(String decision_after) {
		this.decision_after = decision_after;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public List<String> getDocumentIds() {
		return documentIds;
	}

	public void setDocumentIds(List<String> documentIds) {
		this.documentIds = documentIds;
	}
}
