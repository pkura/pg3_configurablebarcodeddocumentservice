package pl.compan.docusafe.web.record.contracts;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.AttachmentsTabAction;

/**
 * User: Bartlomiej.Spychalski@gmail.com
 * Date: 2006-03-22
 * Time: 11:51:41
 */
public class AttachmentsAction extends AttachmentsTabAction {

	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(AttachmentsAction.class.getPackage().getName(),null);
    Long id;
    Long tmp_documentId;
    Long contractId;

    boolean canView;
    boolean canUpdate;
    
    protected List prepareTabs()
    {
        List<Tab> tabs = new ArrayList<Tab>(2);
        
        if(id != null && getDocumentId() != null)  {
            contractId = id;
            canView = false;
            canUpdate = false;
            try {
                Contract contract = Contract.find(id);
    
                DSUser _user = DSApi.context().getDSUser();
                boolean _inAuthorDivision = false;
                
                if (contract.getAuthor() != null) {
                    DSUser _author = DSUser.findByUsername(contract.getAuthor());
                    
                    for (DSDivision div : _author.getDivisions())
                    {
                        if (_user.inDivision(div)) 
                        {
                            _inAuthorDivision = true;
                            break;
                        }
                    }
                }
                
                if ((DSApi.context().hasPermission(DSPermission.CON_PODGLAD) ||
                   ((DSApi.context().hasPermission(DSPermission.CON_PODGLAD_Z_DZIALU)) && (_inAuthorDivision)) ||
                   ((DSApi.context().hasPermission(DSPermission.CON_PODGLAD_SWOICH)) && (_user.getName().equals(contract.getAuthor())))))
                    canView = true;
                if (DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE) ||
                        ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                        ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_SWOICH)) && (_user.getName().equals(contract.getAuthor())))) 
                    canUpdate = true;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
                
            tabs.add(new Tab(sm.getString("Ogolne"), sm.getString("Ogolne"), HttpUtils.makeUrl("/record/contracts/edit-main.action", new String[] { "id", ""+id}), false));
            tabs.add(new Tab(sm.getString("Zalaczniki"), sm.getString("Zalaczniki"), HttpUtils.makeUrl("/record/contracts/attachments.action", new String[] { "id", ""+id, "tmp_documentId", ""+tmp_documentId, "documentId", ""+getDocumentId()}), true));
            tabs.add(new Tab(sm.getString("Faktury"), sm.getString("Faktury"), HttpUtils.makeUrl("/record/contracts/invoices.action",new String[]{"id", ""+id}),false));
            tabs.add(new Tab("Historia", "Historia umowy", HttpUtils.makeUrl("/record/contracts/edit-main.action",new String[]{"id", ""+id,"history","true"}),false));
        }
        return tabs;
    }

    public String getBaseLink()
    {
        return "/record/contracts/attachments.action";
    }

    public String getDocumentType()
    {
        return InOfficeDocument.TYPE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public Long getTmp_documentId() {
        return tmp_documentId;
    }

    public void setTmp_documentId(Long tmp_documentId) {
        this.tmp_documentId = tmp_documentId;
       // this.documentId = tmp_documentId;
    }
    
    public boolean isCanView()
    {
        return canView;
    }  
    
    public boolean isCanUpdate()
    {
        return canUpdate;
    }
    
}
