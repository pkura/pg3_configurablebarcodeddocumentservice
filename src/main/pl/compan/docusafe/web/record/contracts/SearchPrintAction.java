package pl.compan.docusafe.web.record.contracts;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

/* User: Administrator, Date: 2005-09-05 14:55:27 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchPrintAction.java,v 1.9 2006/05/26 09:51:49 mmanski Exp $
 */
public class SearchPrintAction extends EventActionSupport
{
    private int offset;
    private static final int LIMIT = 10;
    private String sortField;
    private boolean ascending;
    private SearchResults results;

    private Integer sequenceId;
    private String contractNo;
    private String contractDateFrom;
    private String contractDateTo;
    private String termFrom;
    private String termTo;
    private String contractKind;
    private String grossFrom;
    private String grossTo;
    private String description;
    private String vendor;
	private String nip;
    private String voucherNo;
    private String contractStatus;
    private String creatingUser;
    private Integer year;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Contract.Query query = new Contract.Query(offset, 0);
            query.setSequenceId(sequenceId);
            query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
            query.setContractDateFrom(DateUtils.nullSafeParseJsDate(contractDateFrom));
            query.setContractDateTo(DateUtils.nullSafeParseJsDate(contractDateTo));
            query.setTermFrom(DateUtils.nullSafeParseJsDate(termFrom));
            query.setTermTo(DateUtils.nullSafeParseJsDate(termTo));
            query.setContractKind(TextUtils.trimmedStringOrNull(contractKind));
            query.setGrossFrom(TextUtils.nullSafeParseBigDecimal(grossFrom, 2));
            query.setGrossTo(TextUtils.nullSafeParseBigDecimal(grossTo, 2));
            query.setDescription(TextUtils.trimmedStringOrNull(description));
            query.setVendor(TextUtils.trimmedStringOrNull(vendor));
            query.setVoucherNo(TextUtils.trimmedStringOrNull(voucherNo));
            query.setContractStatus(TextUtils.trimmedStringOrNull(contractStatus));
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            query.setYear(year);
            
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE)) 
                {
                    if (DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_Z_DZIALU))
                    {
                        query.setFromUserDivisions(Boolean.TRUE);
                    }
                    else 
                        if (DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_SWOICH))
                            query.setAuthor(DSApi.context().getDSUser().getName());
                }
            }
            catch (EdmException e)
            {    
            }
            
            query.safeSortBy(sortField, "sequenceId", ascending);

            File temp = null;

            try
            {
                if (!DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE) &&
                   !DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_SWOICH) &&
                   !DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_Z_DZIALU))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do wyszukiwania um�w");

                results = Contract.search(query);

                if (results.count() == 0)
                    throw new EdmException("Nie znaleziono um�w pasuj�cych do zapytania");

                BigDecimal grossSum = new BigDecimal(0);
                for (Contract con : (Contract[]) results.results())
                {
                    grossSum = grossSum.add(con.getGross());
                }
                
                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                Font font = new Font(baseFont, 12);

                temp = File.createTempFile("docusafe_", "_tmp");
                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
                
                HeaderFooter footer = new HeaderFooter(new Phrase("Strona "), true);
                footer.setAlignment(Element.ALIGN_CENTER);
                pdfDoc.setFooter(footer);
                
                pdfDoc.open();
                
                int[] widths = new int[] { 20, 40, 30, 50, 30, 40, 30 };

                PdfPTable table = new PdfPTable(widths.length);
                table.setWidths(widths);
                table.setWidthPercentage(100);

                table.addCell(new Phrase("Nr w rejestrze", font));
                table.addCell(new Phrase("Nr asygnaty", font));
                table.addCell(new Phrase("Data umowy", font));
                table.addCell(new Phrase("Opis", font));
                table.addCell(new Phrase("Warto��", font));
                table.addCell(new Phrase("Sprzedawca", font));
                table.addCell(new Phrase("Uwagi", font));



                /*
                pdfDoc.add(table);
                table = new PdfPTable(widths.length);
                table.setWidths(widths);
                table.setWidthPercentage(100);
                  */

                while (results.hasNext())
                {
                    Contract contract = (Contract) results.next();

                    table.addCell(new Phrase(contract.getSequenceId() != null ? contract.getSequenceId().toString() : "", font));
                    table.addCell(new Phrase(contract.getVoucherNo() != null ? contract.getVoucherNo() : "", font));
                    table.addCell(new Phrase(contract.getContractDate() != null ? DateUtils.formatCommonDate(contract.getContractDate()): "", font));
                    table.addCell(new Phrase(contract.getDescription() != null ? contract.getDescription() : "", font));
                    table.addCell(new Phrase(contract.getGross() != null ? contract.getGross().toString() : "", font));
                    table.addCell(new Phrase(contract.getVendor() != null ? contract.getVendor().getSummary() : "", font));
                    table.addCell(new Phrase(contract.getRemarks() != null ? contract.getRemarks() : "", font));
                }
                
                PdfPCell cell = new PdfPCell(new Phrase("", font));
                cell.setColspan(3);
                table.addCell(cell);
                table.addCell(new Phrase("suma warto�ci", font));
                table.addCell(new Phrase(grossSum!=null? grossSum.toString() : "", font));
                cell.setColspan(2);
                table.addCell(cell);
                
                pdfDoc.add(new Phrase("Data utworzenia: "+DateUtils.formatJsDateTime(new Date())+"\n", font));
                
                pdfDoc.add(new Phrase("Utworzone przez: "+DSApi.context().getDSUser().asFirstnameLastname()+"\n", font));
                pdfDoc.add(table);
                
                pdfDoc.close();

            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
           if (temp != null && temp.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                        temp.length()+")");

                try
                {
                    ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                        "application/pdf", (int) temp.length());
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    temp.delete();
                }
            }
        }
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public void setContractDateFrom(String contractDateFrom)
    {
        this.contractDateFrom = contractDateFrom;
    }

    public void setContractDateTo(String contractDateTo)
    {
        this.contractDateTo = contractDateTo;
    }

    public void setTermFrom(String termFrom)
    {
        this.termFrom = termFrom;
    }

    public void setTermTo(String termTo)
    {
        this.termTo = termTo;
    }

    public void setContractKind(String contractKind)
    {
        this.contractKind = contractKind;
    }

    public void setGrossFrom(String grossFrom)
    {
        this.grossFrom = grossFrom;
    }

    public void setGrossTo(String grossTo)
    {
        this.grossTo = grossTo;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public void setVoucherNo(String voucherNo)
    {
        this.voucherNo = voucherNo;
    }

    public SearchResults getResults() {
        return results;
    }

    public void setResults(SearchResults results) {
        this.results = results;
    }
    
    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }
    
    public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
}
