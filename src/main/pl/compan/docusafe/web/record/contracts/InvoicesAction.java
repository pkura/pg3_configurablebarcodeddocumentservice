package pl.compan.docusafe.web.record.contracts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
/**
 * 
 * @author Mariusz Kiljanczyk
 *
 */
public class InvoicesAction extends EventActionSupport
{

    private Long contractId;
    private List<Tab> tabs = new ArrayList<Tab>(2);
    private Contract contract;
    private Long id;
    private List<Invoice> invoices;
    private boolean canView;
    private String printUrl;
    private Invoice invoice;
    private BigDecimal grossSum;

    @Override
    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SearchInvoices()).
            // append(fillForm).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

    }
    private class Tabs implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            if(contract != null && contract.getDocument() != null)  {
                tabs.add(new Tab("Og�lne", "Og�lne", HttpUtils.makeUrl("/record/contracts/edit-main.action", new String[] { "id", ""+id}), false));
                tabs.add(new Tab("Za��czniki", "Za��czniki", HttpUtils.makeUrl("/record/contracts/attachments.action", new String[] { "id", ""+id, "tmp_documentId", ""+contract.getDocument().getId(), "documentId", ""+contract.getDocument().getId()}), false));
                tabs.add(new Tab("Faktury", "Faktury", HttpUtils.makeUrl("/record/contracts/invoices.action",new String[]{}),true));
                tabs.add(new Tab("Historia", "Historia umowy", HttpUtils.makeUrl("/record/contracts/edit-main.action",new String[]{"id", ""+id,"history","true"}),false));
            }
        }
    }
    private class SearchInvoices implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
                canView = (DSApi.context().hasPermission(DSPermission.INV_PODGLAD_SWOICH_FAKTUR) ||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTUR_Z_DZIALU) ||
                        DSApi.context().hasPermission(DSPermission.INV_PODGLAD_FAKTURY));
            }
            catch (EdmException e1)
            {
            	LogFactory.getLog("eprint").debug("", e1);
            }
            try
            {
                contract = Contract.find(id);
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }
            contractId = contract.getId();
            invoices = Invoice.findByContractId(contractId);
            if(invoices.size()==0)
                addActionMessage("Nie znaleiono faktur powi�zanych z umow�");
            else
            {   
                grossSum = new BigDecimal(0);
                for(Iterator iterator = invoices.iterator(); iterator.hasNext();)
                {
                    grossSum = grossSum.add(((Invoice)iterator.next()).getGross());
                }
                invoice = invoices.get(0);
                printUrl =
                HttpUtils.makeUrl("/record/invoices/search-print.action",
                new Object[] {
                "contractId", contractId,
                "offset", String.valueOf(0),
                "sortField", "contractNo",
                "ascending", String.valueOf(true) });
            }
            
        }
        
    }
    public Contract getContract()
    {
        return contract;
    }
    public void setContract(Contract contract)
    {
        this.contract = contract;
    }
    public Long getContractId()
    {
        return contractId;
    }
    public void setContractId(Long contractId)
    {
        this.contractId = contractId;
    }
    public Long getId()
    {
        return id;
    }
    public void setId(Long id)
    {
        this.id = id;
    }
    public List<Invoice> getInvoices()
    {
        return invoices;
    }
    public void setInvoices(List<Invoice> invoices)
    {
        this.invoices = invoices;
    }
    public List<Tab> getTabs()
    {
        return tabs;
    }
    public void setTabs(List<Tab> tabs)
    {
        this.tabs = tabs;
    }
    public boolean isCanView()
    {
        return canView;
    }
    public void setCanView(boolean canView)
    {
        this.canView = canView;
    }
    public Invoice getInvoice()
    {
        return invoice;
    }
    public void setInvoice(Invoice invoice)
    {
        this.invoice = invoice;
    }
    public String getPrintUrl()
    {
        return printUrl;
    }
    public void setPrintUrl(String printUrl)
    {
        this.printUrl = printUrl;
    }
    public BigDecimal getGrossSum()
    {
        return grossSum;
    }
    public void setGrossSum(BigDecimal grossSum)
    {
        this.grossSum = grossSum;
    }

}
