package pl.compan.docusafe.web.record.contracts;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.contracts.ContractHistory;
import pl.compan.docusafe.core.record.contracts.Vendor;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.web.common.Tab;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditMainAction.java,v 1.23 2009/10/09 12:57:23 rafalp Exp $
 */
public class EditMainAction extends EventActionSupport
{
	private static final Log log = LogFactory.getLog(EditMainAction.class);

    // @EXPORT
    private Map kindsList;
    private Contract contract;
    private List<Invoice> invoices;
    private BigDecimal invoicesGrossSum;
    private DSUser author;

    private Map statusList;

    private boolean canCreate;
    private boolean canUpdate;
    private boolean canDelete;
    private boolean canView;

    // @EXPORT/@IMPORT
    private String voucherNo;
    private String contractDate;
    private String contractKind;
    private String contractNo;
    private String description;
    private String gross;
    private String startTerm;
    private String endTerm;
    /** Okres finansowania od */
    private String periodfFrom;
    /** Okres finansowania do */
    private String periodfTo;
    private Vendor vendor;
    private String remarks;
    private Long id;
    private String contractStatus;
    private boolean history;
    private List <ContractHistory> contractHistoryList;

    private static final String EV_CREATE = "create";

    private String tab;
    private Long documentId;
    private String selectedVendor;
    private List<Tab> tabs = new ArrayList<Tab>(2);


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Create()).
            append(fillForm).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateCreate()).
            append(EV_CREATE, new Update()).
            append(fillForm).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

    }


    private class Tabs implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            if(contract != null && contract.getDocument() != null)  {
                tabs.add(new Tab("Og�lne", "Og�lne", HttpUtils.makeUrl("/record/contracts/edit-main.action", new String[] { "id", ""+id}), true));
                tabs.add(new Tab("Za��czniki", "Za��czniki", HttpUtils.makeUrl("/record/contracts/attachments.action", new String[] { "id", ""+id, "tmp_documentId", ""+contract.getDocument().getId(), "documentId", ""+contract.getDocument().getId()}), false));
                tabs.add(new Tab("Faktury", "Faktury", HttpUtils.makeUrl("/record/contracts/invoices.action",new String[]{"id", ""+id}),false));
                tabs.add(new Tab("Historia", "Historia umowy", HttpUtils.makeUrl("/record/contracts/edit-main.action",new String[]{"id", ""+id,"history","true"}),false));
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            kindsList = new LinkedHashMap(7);
            kindsList.put(Contract.KIND_UMOWA, "Umowa");
            kindsList.put(Contract.KIND_UMOWA_ZLECENIE, "Umowa-zlecenie");
            kindsList.put(Contract.KIND_ZLECENIE, "Zlecenie");
            kindsList.put(Contract.KIND_UMOWA_DZIELO, "Umowa o dzie�o");
            kindsList.put(Contract.KIND_UBEZPIECZENIE, "Umowa ubezpieczenia");
            kindsList.put(Contract.KIND_POROZUMIENIE, "Porozumienie");
            kindsList.put(Contract.KIND_ANEKS, "Aneks");
            kindsList.put(Contract.KIND_UMOWA_NAJMU, "Umowa najmu");
            kindsList.put(Contract.KIND_UMOWA_DZIERZAWY, "Umowa dzier�awy");
            kindsList.put(Contract.KIND_INNA, "Inny");

            //status umowy
            statusList = new LinkedHashMap(3);
            statusList.put(Contract.STATUS_AKTUALNA, "Aktualna");
            statusList.put(Contract.STATUS_WYGASLA, "Wygas�a");
            statusList.put(Contract.STATUS_ROZWIAZANA, "Rozwi�zana");


            try
            {
            	DSApi.context().begin();

                canCreate = DSApi.context().hasPermission(DSPermission.CON_DODAWANIE);
                canUpdate = DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE);
                canDelete = false;//DSApi.context().hasPermission(DSPermission.CON_USUWANIE);
                canView = true;

                if (id != null)
                {
                	contract = Contract.find(id);
                	
                	if (history) 
                	{
        					contractHistoryList = ContractHistory.findByContractId(id);
        					if(contractHistoryList != null)
        					{
        						for (ContractHistory ch : contractHistoryList) {
        							ch.setUserFirstnameLastname(DSUser.findById(ch.getUserId()).asFirstnameLastname());									
								}
        					}
                	}
                	else
                	{		
	                    DSUser _user = DSApi.context().getDSUser();
	                    boolean _inAuthorDivision = false;
	
	                    if (contract.getAuthor() != null) {
	                        author = DSUser.findByUsername(contract.getAuthor());
	
	                        for (DSDivision div : author.getDivisions())
	                        {
	                            if (_user.inDivision(div)) 
	                            {
	                                _inAuthorDivision = true;
	                                break;
	                            }
	                        }
	                    }
	                    if ((contract.getAuthor() == null) || (((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
	                       ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_SWOICH)) && (_user.getName().equals(contract.getAuthor())))))
	                        canUpdate = true;
	                    
	                    if (!(DSApi.context().hasPermission(DSPermission.CON_PODGLAD) ||
	                       ((DSApi.context().hasPermission(DSPermission.CON_PODGLAD_Z_DZIALU)) && (_inAuthorDivision)) ||
	                       ((DSApi.context().hasPermission(DSPermission.CON_PODGLAD_SWOICH)) && (_user.getName().equals(contract.getAuthor())))) /*&&
	                       (contract.getAuthor() != null)*/) 
	                    {
	                        canView = false;
	                        throw new EdmException("U�ytkownik nie ma uprawnie� do podgl�du tej umowy");
	                    }
	
	                    voucherNo = contract.getVoucherNo();
	                    contractDate = DateUtils.formatJsDate(contract.getContractDate());
	                    contractKind = contract.getContractKind();
	                    contractNo = contract.getContractNo();
	                    description = contract.getDescription();
	                    gross = contract.getGross().toString();
	                    startTerm = contract.getStartTerm() != null ? DateUtils.formatJsDate(contract.getStartTerm()) : null;
	                    endTerm = contract.getEndTerm() != null ? DateUtils.formatJsDate(contract.getEndTerm()) : null;
	                    // dla szpitala w brodnie tylko
	                    periodfFrom = contract.getPeriodfFrom() != null ? DateUtils.formatJsDate(contract.getPeriodfFrom()) : null;
	                    periodfTo = contract.getPeriodfTo() != null ? DateUtils.formatJsDate(contract.getPeriodfTo()) : null;
	                    // ---
	                    vendor = contract.getVendor();
	                    remarks = contract.getRemarks();
	                    contractStatus = contract.getContractStatus();
	
	                    if (vendor.getCountry() == null) {
	                    	selectedVendor = "PL";
	                    	vendor.setCountry("PL");
	                    }
	                    else
	                    	selectedVendor = vendor.getCountry();
	
	                    invoices = Invoice.findByContractId(id);
	                    if(contractNo!=null)
	                    {
	                    invoices.addAll(Invoice.findByContractNo(contractNo));
	                    }
	                    invoicesGrossSum = new BigDecimal(0);
	                    for (Invoice inv : invoices)
	                    {
	                        invoicesGrossSum = invoicesGrossSum.add(inv.getGross());
	                    }
	                    
	                    //sprawdzenie dla faktur juz istniejacych, czy maja dodane dokumenty, jesli nie, to tu dodawany jest dokument
	                    if(contract.getDocument() == null) {
	
	                        if (!(DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE) ||
	                           ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
	                           ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_SWOICH)) && (_user.getName().equals(contract.getAuthor()))))
	                           && (contract.getAuthor() != null)) 
	                            throw new EdmException("U�ytkownik nie ma uprawnie� do modyfikacji umowy");
	                        
	                        contract.setDocument(addDefaultDocument(contract));
	
	                    }
	                    DSApi.context().commit();		
	                }
                }
                else {
                	selectedVendor = "PL";
                }
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            	try {
            		DSApi.context().rollback();
            	} catch (Exception e2) {
            		addActionError(e2.getMessage());
				}
            }
        }
    }

    private class ValidateCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        //    if (StringUtils.isEmpty(contractNo))
        //       addActionError("Nie podano numeru umowy");

            Date _contractDate = DateUtils.nullSafeParseJsDate(contractDate);
            Date _startTerm = DateUtils.nullSafeParseJsDate(startTerm);
            Date _endTerm = DateUtils.nullSafeParseJsDate(endTerm);

            if (_contractDate == null)
                addActionError("Nie podano daty umowy lub data jest nieprawid�owa");

            if (_startTerm == null)
                addActionError("Nie podano terminu lub podana data jest nieprawid�owa");

            if (StringUtils.isEmpty(contractKind))
                addActionError("Nie wybrano typu umowy");

            if (StringUtils.isEmpty(gross))
            {
                addActionError("Nie podano sumy brutto");
            }
            else
            {
                try
                {
                    gross = gross.trim().replace(',', '.');
                    int grossLength = -1;
                	grossLength = (gross.indexOf('.') == -1) ? gross.length() : grossLength;
                	if (grossLength > 17)
                		addActionError("Liczba cyfr kwoty jest wi�ksza od 17-tu");

                    BigDecimal _gross = new BigDecimal(gross);
                    if (_gross.doubleValue() < 0.00)
                        throw new EdmException("Suma brutto musi by� wi�ksza od zera");
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
                catch (Exception e)
                {
                    addActionError("Podana suma brutto jest nieprawid�owa");
                }
            }

            if (StringUtils.isEmpty(description))
                addActionError("Nie podano opisu");

            if (vendor == null || StringUtils.isEmpty(vendor.getName()))
                addActionError("Nie podano nazwy sprzedawcy");

            if (hasActionErrors())
                event.skip(EV_CREATE);
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (!DSApi.context().hasPermission(DSPermission.CON_DODAWANIE))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do tworzenia umowy");

                Contract contract = new Contract();

                contract.setVoucherNo(TextUtils.trimmedStringOrNull(voucherNo));
                contract.setContractDate(DateUtils.nullSafeParseJsDate(contractDate));
                contract.setContractKind(contractKind);
                contract.setContractStatus(contractStatus);
                contract.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                contract.setDescription(TextUtils.trimmedStringOrNull(description));
                contract.setGross(new BigDecimal(gross).setScale(2, BigDecimal.ROUND_DOWN));
                contract.setStartTerm(DateUtils.nullSafeParseJsDate(startTerm));
                contract.setEndTerm(DateUtils.nullSafeParseJsDate(endTerm));
                // dla szpitala w brodnie tylko
                contract.setPeriodfFrom(DateUtils.nullSafeParseJsDate(periodfFrom));
                contract.setPeriodfTo(DateUtils.nullSafeParseJsDate(periodfTo));
                // ---
                contract.setYear(new Integer(GlobalPreferences.getCurrentYear()));
                contract.setAuthor(DSApi.context().getPrincipalName());
             
                vendor.normalizeFields();
                vendor.create();

                contract.setVendor(vendor);

                contract.setRemarks(TextUtils.trimmedStringOrNull(remarks));

                //dodanie dokumentu do umowy
            
                contract.create();
                InOfficeDocument doc =  (InOfficeDocument) addDefaultDocument(contract);
                id = contract.getId();
            
                
                // wpis do historii
                contract.setHistoryEntry("Utworzenie");

                addActionMessage("Utworzono umow� pod numerem "+contract.getSequenceId());

                event.setResult("confirm");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } finally {
            	try {
            		DSApi.context().commit();
            	} catch (Exception e) {
            		addActionError(e.getMessage());
            	}
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Contract contract = Contract.find(id);
                Contract contractBeforeSet = contract.cloneContract();

                DSUser _user = DSApi.context().getDSUser();
                boolean _inAuthorDivision = false;

                if (contract.getAuthor() != null) {
                    DSUser _author = DSUser.findByUsername(contract.getAuthor());

                    for (DSDivision div : _author.getDivisions())
                    {
                        if (_user.inDivision(div)) 
                        {
                            _inAuthorDivision = true;
                            break;
                        }
                    }
                }

                if (!(DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE) ||
                   ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_Z_DZIALU)) && (_inAuthorDivision)) ||
                   ((DSApi.context().hasPermission(DSPermission.CON_MODYFIKACJE_SWOICH)) && (_user.getName().equals(contract.getAuthor()))))
                   && (contract.getAuthor() != null)) 
                    throw new EdmException("U�ytkownik nie ma uprawnie� do modyfikacji tej umowy");


                contract.setVoucherNo(TextUtils.trimmedStringOrNull(voucherNo));
                contract.setContractDate(DateUtils.nullSafeParseJsDate(contractDate));
                contract.setContractKind(contractKind);
                contract.setContractStatus(contractStatus);
                contract.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
                contract.setDescription(TextUtils.trimmedStringOrNull(description));
                contract.setGross(new BigDecimal(gross).setScale(2, BigDecimal.ROUND_DOWN));
                contract.setStartTerm(DateUtils.nullSafeParseJsDate(startTerm));
                contract.setEndTerm(DateUtils.nullSafeParseJsDate(endTerm));

                vendor.normalizeFields();

                contract.getVendor().setName((vendor.getName()));
                contract.getVendor().setStreet((vendor.getStreet()));
                contract.getVendor().setZip((vendor.getZip()));
                contract.getVendor().setLocation((vendor.getLocation()));
                contract.getVendor().setCountry((vendor.getCountry()));
                contract.getVendor().setNip((vendor.getNip()));
                contract.getVendor().setRegon((vendor.getRegon()));

                contract.setRemarks(TextUtils.trimmedStringOrNull(remarks, 512));
                String str = createHistoryDescription(contractBeforeSet, contract);
                if (str.length() > 0)
                	contract.setHistoryEntry(str);
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private boolean wasChanged(Object beforeObj, Object afterObj) {
    	if	((beforeObj != null && afterObj != null && !beforeObj.equals(afterObj))
    			|| (beforeObj == null && afterObj != null)
    			|| (beforeObj != null && afterObj == null)
    		)
    		return true;
    	else
    		return false;
    }
    
    private String createHistoryDescription(Contract before, Contract after) {

    	StringBuilder sb = new StringBuilder();
    	if (wasChanged(before.getContractDate(), after.getContractDate()))
    	{
    		sb.append("dat� umowy na " + DateUtils.formatJsDate(after.getContractDate()));
    		sb.append(", ");
    	}
    	if (wasChanged(before.getContractKind(), after.getContractKind()))
    	{
    		sb.append("typ na " + after.getContractKind());
    		sb.append(", ");
    	}
    	if (wasChanged(before.getContractStatus(), after.getContractStatus()))
    	{
    		sb.append("status na " + after.getContractStatus());
    		sb.append(", ");
    	}
    	if (wasChanged(before.getContractNo(), after.getContractNo()))
    	{
    		sb.append("numer na " + after.getContractNo());
    		sb.append(", ");
    	}
    	if (wasChanged(before.getDescription(), after.getDescription()))
    	{
    		sb.append("opis na " + after.getDescription());
    		sb.append(", ");
    	}
    	if (wasChanged(before.getGross(), after.getGross()))
    	{
    		sb.append("wartosc na " + after.getGross());
    		sb.append(", ");
    	}
    	if (wasChanged(before.getStartTerm(), after.getStartTerm()) || wasChanged(before.getEndTerm(), after.getEndTerm()))
    	{
    		sb.append("termin obowi�zywania na");
    		if (wasChanged(before.getStartTerm(), after.getStartTerm()))
    		{
    			sb.append(" wartosc od: " + DateUtils.formatJsDate(after.getStartTerm()));
    		}
    		if (wasChanged(before.getEndTerm(), after.getEndTerm()))
    		{
    			sb.append(" wartosc do: " + DateUtils.formatJsDate(after.getEndTerm()));
    		}
    		sb.append(", ");
    	}
    	if (wasChanged(before.getVoucherNo(), after.getVoucherNo()))
    	{
    		sb.append("numer asygnaty na" + after.getVoucherNo());
    		sb.append(", ");
    	}
    	if (sb.length() > 0)
    	{
    		sb.insert(0, "Zmieniono ");
    	}
    	return sb.toString();
    }

    private Document addDefaultDocument(Contract contract) throws EdmException {
        InOfficeDocument document = new InOfficeDocument();
        document.setDescription("faktura nr "+contract.getContractNo());
        document.setSummary("faktura");
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setIncomingDate(new Date());
        document.setCreatingUser(DSApi.context().getPrincipalName());
        document.setBok(false);
        document.setContractId(contract.getId());
        document.setDocumentKind( DocumentKind.find(Long.parseLong("1")));
        document.create();
        return document;
    }

    public Map getKindsList()
    {
        return kindsList;
    }

    public Contract getContract()
    {
        return contract;
    }

    public List<Invoice> getInvoices()
    {
        return invoices;
    }

    public BigDecimal getInvoicesGrossSum()
    {
        return invoicesGrossSum;
    }

    public DSUser getAuthor()
    {
        return author;
    }

    public boolean isCanCreate()
    {
        return canCreate;
    }

    public boolean isCanUpdate()
    {
        return canUpdate;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanView()
    {
        return canView;
    }

    public String getVoucherNo()
    {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo)
    {
        this.voucherNo = voucherNo;
    }

    public String getContractDate()
    {
        return contractDate;
    }

    public void setContractDate(String contractDate)
    {
        this.contractDate = contractDate;
    }

    public String getContractKind()
    {
        return contractKind;
    }

    public void setContractKind(String contractKind)
    {
        this.contractKind = contractKind;
    }

    public String getContractNo()
    {
        return contractNo;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getGross()
    {
        return gross;
    }

    public void setGross(String gross)
    {
        this.gross = gross;
    }

    public String getStartTerm()
    {
        return startTerm;
    }

    public void setStartTerm(String startTerm)
    {
        this.startTerm = startTerm;
    }

    public String getEndTerm()
    {
        return endTerm;
    }

    public void setEndTerm(String endTerm)
    {
        this.endTerm = endTerm;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Map getStatusList() {
        return statusList;
    }

    public void setStatusList(Map statusList) {
        this.statusList = statusList;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(List<Tab> tabs) {
        this.tabs = tabs;
    }

	public String getSelectedVendor() {
		return selectedVendor;
	}

	public void setSelectedVendor(String selectedVendor) {
		this.selectedVendor = selectedVendor;
	}

	public String getPeriodfFrom() {
		return periodfFrom;
	}

	public void setPeriodfFrom(String periodfFrom) {
		this.periodfFrom = periodfFrom;
	}

	public String getPeriodfTo() {
		return periodfTo;
	}

	public void setPeriodfTo(String periodfTo) {
		this.periodfTo = periodfTo;
	}

	/**
	 * @return the history
	 */
	public boolean isHistory() {
		return history;
	}

	/**
	 * @param history the history to set
	 */
	public void setHistory(boolean history) {
		this.history = history;
	}

	/**
	 * @return the contractHistoryList
	 */
	public List<ContractHistory> getContractHistoryList() {
		return contractHistoryList;
	}

}