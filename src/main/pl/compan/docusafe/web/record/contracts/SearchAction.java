package pl.compan.docusafe.web.record.contracts;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.record.contracts.Contract;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.*;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchAction.java,v 1.20 2009/04/16 11:28:09 pecet3 Exp $
 */
public class SearchAction extends EventActionSupport
{
    private String thisSearchUrl;

    // @EXPORT/@IMPORT
    private boolean popup;
    
    // @EXPORT
    private List<DSUser> users;
    private Map kindsList;
    private Map statusList;
    private SearchResults results;
    private Pager pager;
    private String printUrl;
    private BigDecimal grossSum;
    
    private boolean canSearch;
    private boolean canView;

    // @IMPORT
    private int offset;
    private static final int LIMIT = 10;
    private String sortField;
    private boolean ascending;
    
    private Integer sequenceId;
    private String contractNo;
    private String contractDateFrom;
    private String contractDateTo;
    private String termFrom;
    private String termTo;
    /** Okres finansowania od */
    private String periodfFrom;    
    /** Okres finansowania do */
    private String periodfTo;
    private String contractKind;
    private String grossFrom;
    private String grossTo;
    private String description;
    private String vendor;
    private String nip;
    private String voucherNo;
    private String contractStatus;
    private String creatingUser;
    private Integer year;

    protected void setup()
    {
    	class SetResult implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                event.setResult(popup ? "popup" : SUCCESS);
            }
        }
    	
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                
                kindsList = new LinkedHashMap(7);
                kindsList.put(Contract.KIND_UMOWA, "Umowa");
                kindsList.put(Contract.KIND_UMOWA_ZLECENIE, "Umowa-zlecenie");
                kindsList.put(Contract.KIND_ZLECENIE, "Zlecenie");
                kindsList.put(Contract.KIND_UMOWA_DZIELO, "Umowa o dzie�o");
                kindsList.put(Contract.KIND_UBEZPIECZENIE, "Umowa ubezpieczenia");
                kindsList.put(Contract.KIND_POROZUMIENIE, "Porozumienie");
                kindsList.put(Contract.KIND_ANEKS, "Aneks");
                kindsList.put(Contract.KIND_UMOWA_NAJMU, "Umowa najmu");
                kindsList.put(Contract.KIND_UMOWA_DZIERZAWY, "Umowa dzier�awy");
                kindsList.put(Contract.KIND_INNA, "Inny");

                statusList = new LinkedHashMap(3);
                statusList.put(Contract.STATUS_AKTUALNA, "Aktualna");
                statusList.put(Contract.STATUS_WYGASLA, "Wygas�a");
                statusList.put(Contract.STATUS_ROZWIAZANA, "Rozwi�zana");


                canSearch = (DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE) ||
                        DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_Z_DZIALU) ||
                        DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_SWOICH));
                canView = (DSApi.context().hasPermission(DSPermission.CON_PODGLAD) ||
                        DSApi.context().hasPermission(DSPermission.CON_PODGLAD_Z_DZIALU) ||
                        DSApi.context().hasPermission(DSPermission.CON_PODGLAD_SWOICH));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Contract.Query query = new Contract.Query(offset, LIMIT);
            if (sequenceId != null)
                query.setSequenceId(sequenceId);
            query.setContractNo(TextUtils.trimmedStringOrNull(contractNo));
            query.setContractDateFrom(DateUtils.nullSafeParseJsDate(contractDateFrom));
            query.setContractDateTo(DateUtils.nullSafeParseJsDate(contractDateTo));
            query.setTermFrom(DateUtils.nullSafeParseJsDate(termFrom));
            query.setTermTo(DateUtils.nullSafeParseJsDate(termTo));
            query.setPeriodfFrom(DateUtils.nullSafeParseJsDate(periodfFrom));
            query.setPeriodfTo(DateUtils.nullSafeParseJsDate(periodfTo));
            query.setContractKind(TextUtils.trimmedStringOrNull(contractKind));
            query.setGrossFrom(TextUtils.nullSafeParseBigDecimal(grossFrom, 2));
            query.setGrossTo(TextUtils.nullSafeParseBigDecimal(grossTo, 2));
            query.setDescription(TextUtils.trimmedStringOrNull(description));
            query.setVendor(TextUtils.trimmedStringOrNull(vendor));
            query.setVoucherNo(TextUtils.trimmedStringOrNull(voucherNo));
            query.setContractStatus(TextUtils.trimmedStringOrNull(contractStatus));
            query.setCreatingUser(TextUtils.trimmedStringOrNull(creatingUser));
            query.setNip(TextUtils.trimmedStringOrNull(nip));
            if (year != null)
                query.setYear(year);
            
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE)) 
                {
                    if (DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_Z_DZIALU))
                    {
                        query.setFromUserDivisions(Boolean.TRUE);
                    }
                    else 
                        if (DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_SWOICH))
                            query.setAuthor(DSApi.context().getDSUser().getName());
                }
            }
            catch (EdmException e)
            {    
            }
            
            query.safeSortBy(sortField, "sequenceId", ascending);

            try
            {
                if (!DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE) &&
                    !DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_SWOICH) &&
                    !DSApi.context().hasPermission(DSPermission.CON_WYSZUKIWANIE_Z_DZIALU))
                    throw new EdmException("U�ytkownik nie ma uprawnie� do wyszukiwania um�w");

                results = Contract.search(query);

                if (results.count() == 0)
                    addActionMessage("Nie znaleziono um�w");

                grossSum = new BigDecimal(0);
                Contract[] cont = (Contract[]) results.results();
                for (int i=0; i<cont.length; i++) 
                {
                	BigDecimal gr = cont[i].getGross();
                	grossSum = grossSum.add(gr);
                }
                
                thisSearchUrl =
                    HttpUtils.makeUrl("/record/contracts/search.action",
                    new Object[] {
                    "doSearch", "true",
                    "sequenceId", sequenceId,
                    "contractNo", contractNo,
                    "contractDateFrom", contractDateFrom,
                    "contractDateTo", contractDateTo,
                    "termFrom", termFrom,
                    "termTo", termTo,
                    "periodfFrom", periodfFrom,
                    "periodfTo", periodfTo,
                    "contractKind", contractKind,
                    "grossFrom", grossFrom,
                    "grossTo", grossTo,
                    "description", description,
                    "vendor", vendor,
                    "nip", nip,
                    "voucherNo", voucherNo,
                    "creatingUser", creatingUser,
                    "year", year,
                    "popup", popup ? "true" : "false",
                    "offset", String.valueOf(offset),
                    "contractStatus", contractStatus});

                printUrl =
                    HttpUtils.makeUrl("/record/contracts/search-print.action",
                    new Object[] {
                    "sequenceId", sequenceId,
                    "contractNo", contractNo,
                    "contractDateFrom", contractDateFrom,
                    "contractDateTo", contractDateTo,
                    "termFrom", termFrom,
                    "termTo", termTo,
                    "periodfFrom", periodfFrom,
                    "periodfTo", periodfTo,
                    "contractKind", contractKind,
                    "grossFrom", grossFrom,
                    "grossTo", grossTo,
                    "description", description,
                    "vendor", vendor,
                    "nip", nip,
                    "voucherNo", voucherNo,
                    "creatingUser", creatingUser,
                    "year", year,
                    "offset", String.valueOf(offset),
                    "sortField", sortField,
                    "popup", popup ? "true" : "false",
                    "ascending", String.valueOf(ascending),
                    "contractStatus", contractStatus} );


                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return
                            HttpUtils.makeUrl("/record/contracts/search.action",
                            new Object[] {
                            "doSearch", "true",
                            "sequenceId", sequenceId,
                            "contractNo", contractNo,
                            "contractDateFrom", contractDateFrom,
                            "contractDateTo", contractDateTo,
                            "termFrom", termFrom,
                            "termTo", termTo,
                            "periodfFrom", periodfFrom,
                            "periodfTo", periodfTo,
                            "contractKind", contractKind,
                            "grossFrom", grossFrom,
                            "grossTo", grossTo,
                            "description", description,
                            "vendor", vendor,
                            "nip", nip,
                            "year", year,
                            "voucherNo", voucherNo,
                            "creatingUser", creatingUser,
                            "sortField", sortField,
                            "ascending", String.valueOf(ascending),
                            "popup", popup ? "true" : "false",
                            "offset", String.valueOf(offset),
                            "contractStatus", contractStatus});
                    }
                };
                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }

    public Map getKindsList()
    {
        return kindsList;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }
    
    public void setContractDateFrom(String contractDateFrom)
    {
        this.contractDateFrom = contractDateFrom;
    }

    public void setContractDateTo(String contractDateTo)
    {
        this.contractDateTo = contractDateTo;
    }

    public void setTermFrom(String termFrom)
    {
        this.termFrom = termFrom;
    }

    public void setTermTo(String termTo)
    {
        this.termTo = termTo;
    }

    public void setContractKind(String contractKind)
    {
        this.contractKind = contractKind;
    }

    public void setVoucherNo(String voucherNo)
    {
        this.voucherNo = voucherNo;
    }

    public void setGrossFrom(String grossFrom)
    {
        this.grossFrom = grossFrom;
    }

    public void setGrossTo(String grossTo)
    {
        this.grossTo = grossTo;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setContractNo(String contractNo)
    {
        this.contractNo = contractNo;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public boolean isPopup()
    {
        return popup;
    }

    public void setPopup(boolean popup)
    {
        this.popup = popup;
    }
    
    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public void setSequenceId(Integer sequenceId)
    {
        this.sequenceId = sequenceId;
    }

    public boolean isCanSearch()
    {
        return canSearch;
    }

    public boolean isCanView()
    {
        return canView;
    }

    public String getPrintUrl()
    {
        return printUrl;
    }
    
    public BigDecimal getGrossSum() {
    	return grossSum;
    }

    public Map getStatusList() {
        return statusList;
    }

    public void setStatusList(Map statusList) {
        this.statusList = statusList;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }
    
    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public void setYear(Integer year)
    {
        this.year = year;
    }

	public String getPeriodfFrom() {
		return periodfFrom;
	}

	public void setPeriodfFrom(String periodfFrom) {
		this.periodfFrom = periodfFrom;
	}

	public String getPeriodfTo() {
		return periodfTo;
	}

	public void setPeriodfTo(String periodfTo) {
		this.periodfTo = periodfTo;
	}
}
