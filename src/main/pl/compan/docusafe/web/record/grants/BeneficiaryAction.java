package pl.compan.docusafe.web.record.grants;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.record.grants.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.util.LinkedHashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-10-24 16:50:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BeneficiaryAction.java,v 1.4 2006/02/20 15:42:47 lk Exp $
 */
public class BeneficiaryAction extends EventActionSupport
{
    private Long requestId;
    private Integer posn;

    private String firstname;
    private String lastname;
    private String birthDate;
    private String fathersName;
    private String mothersName;
    private String schoolName;
    private String schoolStreet;
    private String schoolZip;
    private String schoolLocation;
    private String bankAccount;
    private String helpRequested;
    private String helpGranted;
    private Boolean rejected;
    private String justification;
    private String result;

    private Map results;
    private String status;
    private boolean terminated;
    private Map helpRequestedKinds;
    private Map helpGrantedKinds;
    private boolean canPush;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPush").
            append(OpenHibernateSession.INSTANCE).
            append(new Push()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            helpRequestedKinds = new LinkedHashMap();
            helpRequestedKinds.put(GrantRequest.STYPENDIUM, "Stypendium");
            helpRequestedKinds.put(GrantRequest.ZASILEK, "Zasi�ek");

            try
            {
                helpGrantedKinds = new LinkedHashMap();

                GrantRequest grantRequest = GrantRequest.find(requestId);

                if (posn != null)
                {
                    Beneficiary beneficiary = grantRequest.getBeneficiary(posn.intValue());
                    BeneficiaryFlow flow = new BeneficiaryFlow(beneficiary);
                    Step step = flow.getStep();
                    status = step.getDescription();
                    results = step.getResults();
                    terminated = step.isTerminal();
                    result = flow.suggestResult();

                    GrantRequestFlow rflow = new GrantRequestFlow(grantRequest);
                    canPush = rflow.getStep().equals(GrantRequestFlow.CZEKAJ_BENEFICJENCI);

                    // TODO: podpowiadanie odpowiedzi
                    // - czy spe�nia wiek
                    // - kryterium dochodowe


                    firstname = beneficiary.getFirstname();
                    lastname = beneficiary.getLastname();
                    birthDate = DateUtils.formatJsDate(beneficiary.getBirthDate());
                    fathersName = beneficiary.getFathersName();
                    mothersName = beneficiary.getMothersName();
                    schoolName = beneficiary.getSchoolName();
                    schoolStreet = beneficiary.getSchoolStreet();
                    schoolZip = beneficiary.getSchoolZip();
                    schoolLocation = beneficiary.getSchoolLocation();
                    bankAccount = beneficiary.getBankAccount();
                    helpRequested = beneficiary.getHelpRequested();
                    helpGranted = beneficiary.getHelpGranted();
                    rejected = beneficiary.getRejected();
                    justification = beneficiary.getJustification();

                    if (GrantRequest.STYPENDIUM.equals(beneficiary.getHelpRequested()))
                    {
                        helpGrantedKinds.put(GrantRequest.STYPENDIUM_P, "Stypendium pieni�ne");
                        helpGrantedKinds.put(GrantRequest.STYPENDIUM_R, "Stypendium rzeczowe");
                    }
                    else if (GrantRequest.ZASILEK.equals(beneficiary.getHelpRequested()))
                    {
                        helpGrantedKinds.put(GrantRequest.ZASILEK_P, "Zasi�ek pieni�ny");
                        helpGrantedKinds.put(GrantRequest.ZASILEK_R, "Zasi�ek rzeczowy");
                    }

                }
                else
                {
                    lastname = grantRequest.getLastname();
                    if (grantRequest.getBeneficiaries().size() > 0)
                    {
                        Beneficiary other = (Beneficiary) grantRequest.getBeneficiary(0);
                        fathersName = other.getFathersName();
                        mothersName = other.getMothersName();
                        bankAccount = other.getBankAccount();
                        helpRequested = other.getHelpRequested();
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                GrantRequest grantRequest = GrantRequest.find(requestId);

                Beneficiary beneficiary;
                if (posn == null)
                {
                    beneficiary = new Beneficiary();
                }
                else
                {
                    beneficiary = grantRequest.getBeneficiary(posn.intValue());
                }

                update(beneficiary);

                if (posn == null)
                {
                    grantRequest.addBeneficiary(beneficiary);
                }

                DSApi.context().commit();

                // przekierowanie dopiero tutaj, je�eli nie by�o b��d�w
                if (posn == null)
                {
                    event.setResult("grant-request");
                }

                addActionMessage("Zapisano dane");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private void update(Beneficiary beneficiary) throws EdmException
    {
        if (StringUtils.isEmpty(lastname))
            throw new EdmException("Nie podano nazwiska");
        if (StringUtils.isEmpty(birthDate))
            throw new EdmException("Nie podano daty urodzenia");
        if (StringUtils.isEmpty(schoolName))
            throw new EdmException("Nie podano nazwy szko�y");
        if (StringUtils.isEmpty(helpRequested))
            throw new EdmException("Nie wybrano rodzaju ��danej pomocy");

        beneficiary.setFirstname(TextUtils.trimmedStringOrNull(firstname));
        beneficiary.setLastname(TextUtils.trimmedStringOrNull(lastname));
        beneficiary.setBirthDate(DateUtils.nullSafeParseJsDate(birthDate));
        beneficiary.setFathersName(TextUtils.trimmedStringOrNull(fathersName));
        beneficiary.setMothersName(TextUtils.trimmedStringOrNull(mothersName));
        beneficiary.setSchoolName(TextUtils.trimmedStringOrNull(schoolName));
        beneficiary.setSchoolStreet(TextUtils.trimmedStringOrNull(schoolStreet));
        beneficiary.setSchoolZip(TextUtils.trimmedStringOrNull(schoolZip));
        beneficiary.setSchoolLocation(TextUtils.trimmedStringOrNull(schoolLocation));
        beneficiary.setBankAccount(TextUtils.trimmedStringOrNull(bankAccount));
        beneficiary.setHelpRequested(TextUtils.trimmedStringOrNull(helpRequested));
        beneficiary.setHelpGranted(TextUtils.trimmedStringOrNull(helpGranted));
        beneficiary.setRejected(rejected);
        if (rejected != null && rejected.booleanValue())
            beneficiary.setJustification(TextUtils.trimmedStringOrNull(justification));
        else
            beneficiary.setJustification(null);
    }

    private class Push implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Beneficiary beneficiary = GrantRequest.find(requestId).
                    getBeneficiary(posn.intValue());

                update(beneficiary);

                BeneficiaryFlow flow = new BeneficiaryFlow(beneficiary);
                flow.push(TextUtils.trimmedStringOrNull(result));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Map getHelpRequestedKinds()
    {
        return helpRequestedKinds;
    }

    public Long getRequestId()
    {
        return requestId;
    }

    public void setRequestId(Long requestId)
    {
        this.requestId = requestId;
    }

    public Integer getPosn()
    {
        return posn;
    }

    public void setPosn(Integer posn)
    {
        this.posn = posn;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getFathersName()
    {
        return fathersName;
    }

    public void setFathersName(String fathersName)
    {
        this.fathersName = fathersName;
    }

    public String getMothersName()
    {
        return mothersName;
    }

    public void setMothersName(String mothersName)
    {
        this.mothersName = mothersName;
    }

    public String getSchoolName()
    {
        return schoolName;
    }

    public void setSchoolName(String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getSchoolStreet()
    {
        return schoolStreet;
    }

    public void setSchoolStreet(String schoolStreet)
    {
        this.schoolStreet = schoolStreet;
    }

    public String getSchoolZip()
    {
        return schoolZip;
    }

    public void setSchoolZip(String schoolZip)
    {
        this.schoolZip = schoolZip;
    }

    public String getSchoolLocation()
    {
        return schoolLocation;
    }

    public void setSchoolLocation(String schoolLocation)
    {
        this.schoolLocation = schoolLocation;
    }

    public String getBankAccount()
    {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount)
    {
        this.bankAccount = bankAccount;
    }

    public String getHelpRequested()
    {
        return helpRequested;
    }

    public void setHelpRequested(String helpRequested)
    {
        this.helpRequested = helpRequested;
    }

    public Boolean getRejected()
    {
        return rejected;
    }

    public void setRejected(Boolean rejected)
    {
        this.rejected = rejected;
    }

    public String getJustification()
    {
        return justification;
    }

    public void setJustification(String justification)
    {
        this.justification = justification;
    }

    public String getStatus()
    {
        return status;
    }

    public Map getResults()
    {
        return results;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public boolean isTerminated()
    {
        return terminated;
    }

    public Map getHelpGrantedKinds()
    {
        return helpGrantedKinds;
    }

    public String getHelpGranted()
    {
        return helpGranted;
    }

    public void setHelpGranted(String helpGranted)
    {
        this.helpGranted = helpGranted;
    }

    public boolean isCanPush()
    {
        return canPush;
    }
}
