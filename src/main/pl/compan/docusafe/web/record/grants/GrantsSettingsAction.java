package pl.compan.docusafe.web.record.grants;

import java.io.FileInputStream;
import java.io.IOException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * 
 *@author <a href="mailto:michal.manski@com-pan.pl">Micha� Ma�ski</a>
 *
 */
public class GrantsSettingsAction extends EventActionSupport
{
    // @IMPORT
    private FormFile stypRzeczTemplate;
    private FormFile stypPienTemplate;
    private FormFile zasilRzeczTemplate;
    private FormFile zasilPienTemplate;
    private FormFile niekompletnyTemplate;
    private FormFile organNiewlasciwyTemplate;
    private FormFile placowkaNiewlasciwaTemplate;
    private FormFile dochodyNiewlasciweTemplate;
    private FormFile wiekZaNiskiTemplate;
    private FormFile wiekZaWysokiTemplate;

    // @EXPORT
    private Long stypRzeczTemplateId;
    private Long stypPienTemplateId;
    private Long zasilRzeczTemplateId;
    private Long zasilPienTemplateId;
    private Long niekompletnyTemplateId;
    private Long organNiewlasciwyTemplateId;
    private Long placowkaNiewlasciwaTemplateId;
    private Long dochodyNiewlasciweTemplateId;
    private Long wiekZaNiskiTemplateId;
    private Long wiekZaWysokiTemplateId;

    
    protected void setup()
    {
        if (!Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
            throw new IllegalStateException("Wymagany jest modu� Architektura");

        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                try
                {
                	stypRzeczTemplateId = Resource.findResource(GrantRequest.RES_STYPENDIUM_RZECZOWE_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}

                try
                {
                	stypPienTemplateId = Resource.findResource(GrantRequest.RES_STYPENDIUM_PIENIEZNE_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	zasilRzeczTemplateId = Resource.findResource(GrantRequest.RES_ZASILEK_RZECZOWY_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	zasilPienTemplateId = Resource.findResource(GrantRequest.RES_ZASILEK_PIENIEZNY_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	niekompletnyTemplateId = Resource.findResource(GrantRequest.RES_NIEKOMPLETNY_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	organNiewlasciwyTemplateId = Resource.findResource(GrantRequest.RES_ORGAN_NIEWLASCIWY_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	placowkaNiewlasciwaTemplateId = Resource.findResource(GrantRequest.RES_PLACOWKA_NIEWLASCIWA_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	dochodyNiewlasciweTemplateId = Resource.findResource(GrantRequest.RES_NIEWLASCIWE_DOCHODY_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
                
                try
                {
                	wiekZaWysokiTemplateId = Resource.findResource(GrantRequest.RES_ZA_WYSOKI_WIEK_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}

                try
                {
                	wiekZaNiskiTemplateId = Resource.findResource(GrantRequest.RES_ZA_NISKI_WIEK_TEMPLATE).getId();
                }
                catch (EntityNotFoundException e) {}
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (stypRzeczTemplate != null && stypRzeczTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_STYPENDIUM_RZECZOWE_TEMPLATE, stypRzeczTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji pozytywnej - stypendium rzeczowe");
                }

                if (stypPienTemplate != null && stypPienTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_STYPENDIUM_PIENIEZNE_TEMPLATE, stypPienTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji pozytywnej - stypendium pieni�ne");
                }
                
                if (zasilRzeczTemplate != null && zasilRzeczTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_ZASILEK_RZECZOWY_TEMPLATE, zasilRzeczTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji pozytywnej - zasi�ek rzeczowy");
                }
                
                if (zasilPienTemplate != null && zasilPienTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_ZASILEK_PIENIEZNY_TEMPLATE, zasilPienTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji pozytywnej - zasi�ek pieni�ny");
                }
                
                if (niekompletnyTemplate != null && niekompletnyTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_NIEKOMPLETNY_TEMPLATE, niekompletnyTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - wniosek niekompletny");
                }

                if (organNiewlasciwyTemplate != null && organNiewlasciwyTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_ORGAN_NIEWLASCIWY_TEMPLATE, organNiewlasciwyTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - niew�a�ciwy organ");
                }
                
                if (placowkaNiewlasciwaTemplate != null && placowkaNiewlasciwaTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_PLACOWKA_NIEWLASCIWA_TEMPLATE, placowkaNiewlasciwaTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - niew�a�ciwa plac�wka");
                }
                
                if (dochodyNiewlasciweTemplate != null && dochodyNiewlasciweTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_NIEWLASCIWE_DOCHODY_TEMPLATE, dochodyNiewlasciweTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - niew�a�ciwe dochody");
                }
                
                if (wiekZaNiskiTemplate != null && wiekZaNiskiTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_ZA_NISKI_WIEK_TEMPLATE, wiekZaNiskiTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - zbyt niski wiek");
                }

                if (wiekZaWysokiTemplate != null && wiekZaWysokiTemplate.sensible())
                {
                    updateResource(GrantRequest.RES_ZA_WYSOKI_WIEK_TEMPLATE, wiekZaWysokiTemplate);
                    addActionMessage("Zaktualizowano szablon decyzji odmownej - zbyt wysoki wiek");
                }

                DSApi.context().commit();
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private void updateResource(String systemName, FormFile file)
        throws EdmException, IOException
    {
        if (!file.getName().toLowerCase().endsWith(".rtf"))
            throw new EdmException("Spodziewano si� pliku RTF");

        Resource resource;
        try
        {
            resource = Resource.findResource(systemName);
            resource.setFileName(file.getName());
            resource.update(
                new FileInputStream(file.getFile()),
                (int) file.getFile().length());
        }
        catch (EntityNotFoundException e)
        {
            resource = Resource.create(
                systemName,
                file.getName(),
                file.getContentType(),
                new FileInputStream(file.getFile()),
                (int) file.getFile().length());
        }
    }

    public void setStypRzeczTemplate(FormFile stypRzeczTemplate)
    {
        this.stypRzeczTemplate = stypRzeczTemplate;
    }

    public Long getStypRzeczTemplateId()
    {
        return stypRzeczTemplateId;
    }

    public void setStypPienTemplate(FormFile stypPienTemplate)
    {
        this.stypPienTemplate = stypPienTemplate;
    }

    public Long getStypPienTemplateId()
    {
        return stypPienTemplateId;
    }
    
    public void setZasilRzeczTemplate(FormFile zasilRzeczTemplate)
    {
        this.zasilRzeczTemplate = zasilRzeczTemplate;
    }

    public Long getZasilRzeczTemplateId()
    {
        return zasilRzeczTemplateId;
    }
    
    public void setZasilPienTemplate(FormFile zasilPienTemplate)
    {
        this.zasilPienTemplate = zasilPienTemplate;
    }

    public Long getZasilPienTemplateId()
    {
        return zasilPienTemplateId;
    }
    
    public void setNiekompletnyTemplate(FormFile niekompletnyTemplate)
    {
        this.niekompletnyTemplate = niekompletnyTemplate;
    }

    public Long getNiekompletnyTemplateId()
    {
        return niekompletnyTemplateId;
    }
    
    public void setOrganNiewlasciwyTemplate(FormFile organNiewlasciwyTemplate)
    {
        this.organNiewlasciwyTemplate = organNiewlasciwyTemplate;
    }

    public Long getOrganNiewlasciwyTemplateId()
    {
        return organNiewlasciwyTemplateId;
    }
    
    public void setPlacowkaNiewlasciwaTemplate(FormFile placowkaNiewlasciwaTemplate)
    {
        this.placowkaNiewlasciwaTemplate = placowkaNiewlasciwaTemplate;
    }

    public Long getPlacowkaNiewlasciwaTemplateId()
    {
        return placowkaNiewlasciwaTemplateId;
    }
    
    public void setDochodyNiewlasciweTemplate(FormFile dochodyNiewlasciweTemplate)
    {
        this.dochodyNiewlasciweTemplate = dochodyNiewlasciweTemplate;
    }

    public Long getDochodyNiewlasciweTemplateId()
    {
        return dochodyNiewlasciweTemplateId;
    }
    
    public void setWiekZaNiskiTemplate(FormFile wiekZaNiskiTemplate)
    {
        this.wiekZaNiskiTemplate = wiekZaNiskiTemplate;
    }

    public Long getWiekZaNiskiTemplateId()
    {
        return wiekZaNiskiTemplateId;
    }

    public void setWiekZaWysokiTemplate(FormFile wiekZaWysokiTemplate)
    {
        this.wiekZaWysokiTemplate = wiekZaWysokiTemplate;
    }

    public Long getWiekZaWysokiTemplateId()
    {
        return wiekZaWysokiTemplateId;
    }
}
