package pl.compan.docusafe.web.record.grants;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.record.grants.*;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.common.rtf.RTF;
import pl.compan.docusafe.common.rtf.RTFException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import javax.swing.text.StyledDocument;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;

/* User: Administrator, Date: 2005-10-24 14:18:09 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: GrantRequestAction.java,v 1.8 2008/10/06 10:10:47 pecet4 Exp $
 */
public class GrantRequestAction extends EventActionSupport
{
    // @IMPORT
    private Long caseId;

    // @IMPORT/@EXPORT
    private Long id;
    private String documentDate;
    private String firstname;
    private String lastname;
    private String street;
    private String zip;
    private String location;
    private String result;
    private String income;
    private int[] posns;

    // @EXPORT
    private Integer sequenceId;
    private String status;
    private boolean terminated;
    private Map results;
    private List beneficiaries;
    private OfficeCase officeCase;
    private Long inDocumentId;

    private static final String EV_FILL = "fillForm";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPush").
            append(OpenHibernateSession.INSTANCE).
            append(new Push()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteBeneficiaries").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteBeneficiaries()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doTemplateRTF").
        	append(OpenHibernateSession.INSTANCE).
        	append(new TemplateRTF());
        	//appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                GrantRequest grantRequest;

                if (id != null)
                    grantRequest = GrantRequest.find(id);
                else
                    grantRequest = GrantRequest.findByOfficeCaseId(caseId);

                id = grantRequest.getId();
                if (grantRequest.getDocumentDate() != null)
                    documentDate = DateUtils.formatJsDate(grantRequest.getDocumentDate());
                sequenceId = grantRequest.getSequenceId();
                firstname = grantRequest.getFirstname();
                lastname = grantRequest.getLastname();
                street = grantRequest.getStreet();
                zip = grantRequest.getZip();
                location = grantRequest.getLocation();
                income = grantRequest.getIncome() != null ? grantRequest.getIncome().toString() : null;
                inDocumentId = grantRequest.getInDocumentId();
                
                beneficiaries = new ArrayList(grantRequest.getBeneficiaries().size());
                int posn = 0;
                for (Iterator iter=grantRequest.getBeneficiaries().iterator(); iter.hasNext(); )
                {
                    Beneficiary beneficiary = (Beneficiary) iter.next();
                    Map bean = new HashMap();
                    bean.put("posn", new Integer(posn++));
                    bean.put("firstname", beneficiary.getFirstname());
                    bean.put("lastname", beneficiary.getLastname());
                    bean.put("helpRequested", GrantRequest.getRequestedHelpDescription(beneficiary.getHelpRequested()));
                    bean.put("helpGranted", GrantRequest.getGrantedHelpDescription(beneficiary.getHelpGranted()));

                    Step step = new BeneficiaryFlow(beneficiary).getStep();
                    if (step == BeneficiaryFlow.ODMOWA && beneficiary.getJustification() != null)
                    {
                        bean.put("status", step.getDescription()+" (\""+beneficiary.getJustification()+"\")");
                    }
                    else
                    {
                        bean.put("status", step.getDescription());
                    }

                    beneficiaries.add(bean);
                }

                officeCase = grantRequest.getOfficeCase();
                DSApi.initializeProxy(officeCase);

                GrantRequestFlow flow = new GrantRequestFlow(grantRequest);
                status = flow.getStep().getDescription();
                results = flow.getStep().getResults();
                terminated = flow.getStep().isTerminal();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                GrantRequest grantRequest = GrantRequest.find(id);

                update(grantRequest);

                DSApi.context().commit();

                addActionMessage("Zapisano dane");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private void update(GrantRequest grantRequest)
        throws ValidationException
    {
        if (DateUtils.nullSafeParseJsDate(documentDate) == null)
            throw new ValidationException("Nie podano daty wniosku");
        if (StringUtils.isEmpty(firstname))
            throw new ValidationException("Nie podano imienia wnioskodawcy");
        if (StringUtils.isEmpty(lastname))
            throw new ValidationException("Nie podano nazwiska wnioskodawcy");
        if (StringUtils.isEmpty(street))
            throw new ValidationException("Nie podano adresu wnioskodawcy");
        if (StringUtils.isEmpty(location))
            throw new ValidationException("Nie podano nazwy miejscowości");
        if (StringUtils.isEmpty(income))
            throw new ValidationException("Nie podano dochodu");

        BigDecimal bdIncome = null;
        if (!StringUtils.isEmpty(income))
        {
            try
            {
                bdIncome = new BigDecimal(TextUtils.trimmedStringOrNull(income.replace(',', '.')));
            }
            catch (Exception e)
            {
                throw new ValidationException("Podano nieprawidłową wartość dochodu");
            }
        }

        if (bdIncome != null &&
            (bdIncome.signum() < 0 || bdIncome.scale() > 2))
            throw new ValidationException("Podano nieprawidłową wartość dochodu");

        grantRequest.setDocumentDate(DateUtils.nullSafeParseJsDate(documentDate));
        grantRequest.setFirstname(TextUtils.trimmedStringOrNull(firstname));
        grantRequest.setLastname(TextUtils.trimmedStringOrNull(lastname));
        grantRequest.setStreet(TextUtils.trimmedStringOrNull(street));
        grantRequest.setZip(TextUtils.trimmedStringOrNull(zip));
        grantRequest.setLocation(TextUtils.trimmedStringOrNull(location));
        grantRequest.setIncome(bdIncome);
    }

    private class Push implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                GrantRequest grantRequest = GrantRequest.find(id);

                update(grantRequest);

                GrantRequestFlow flow = new GrantRequestFlow(grantRequest);

                flow.push(TextUtils.trimmedStringOrNull(result));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteBeneficiaries implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (posns == null || posns.length == 0)
                return;

            try
            {
                DSApi.context().begin();

                GrantRequest grantRequest = GrantRequest.find(id);

                for (int i=0; i < posns.length; i++)
                {
                    grantRequest.deleteBeneficiary(posns[i] - i);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class TemplateRTF implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            StyledDocument document = null;
            String mime = null;
            String filename = null;
            try
            {
                DSApi.context().begin();


                GrantRequest grantRequest = GrantRequest.find(id);

                Resource resource = Resource.findResource(grantRequest.getTemplateName(0));
                mime = resource.getMime();
                filename = resource.getFileName();

                document = RTF.load(resource.getBinaryStream());

                GrantRequest.fillTemplateRTF(document, grantRequest, 0);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
            catch (RTFException e)
            {
                DSApi.context().setRollbackOnly();
                event.getLog().error(e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            }

            if (document != null)
            {
                File temp = null;
                try
                {
                    temp = File.createTempFile("docusafe_", ".tmp");
                    RTF.write(document, temp);

                    HttpServletResponse response = ServletActionContext.getResponse();
                    response.setContentType(mime);
                    response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
                    //response.setHeader("Content-Disposition", "inline");
                    response.setContentLength((int) temp.length());

                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(temp);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
                finally
                {
                    if (temp != null) temp.delete();
                }
            }
        }
    }
    
    
    public Integer getSequenceId()
    {
        return sequenceId;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public Map getResults()
    {
        return results;
    }

    public OfficeCase getOfficeCase()
    {
        return officeCase;
    }

    public boolean isTerminated()
    {
        return terminated;
    }

    public List getBeneficiaries()
    {
        return beneficiaries;
    }

    public String getIncome()
    {
        return income;
    }

    public void setIncome(String income)
    {
        this.income = income;
    }

    public void setPosns(int[] posns)
    {
        this.posns = posns;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }
    
    public Long getInDocumentId()
    {
        return inDocumentId;
    }

}
