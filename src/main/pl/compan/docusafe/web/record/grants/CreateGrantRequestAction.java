package pl.compan.docusafe.web.record.grants;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-11-26 14:50:42 */

/**
 * Tworzy wniosek stypendialny (tworzenie jest natychmiastowe,
 * w PreCreate, bez kroku przej�ciowego).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CreateGrantRequestAction.java,v 1.3 2006/07/28 14:07:44 mmanski Exp $
 */
public class CreateGrantRequestAction extends EventActionSupport
{
    private Long caseId;
    private Long documentId;
    private Long id;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                OfficeCase officeCase = OfficeCase.find(caseId);
                try
                {
                    GrantRequest gr = GrantRequest.findByOfficeCase(officeCase);
                    throw new EdmException("Ze spraw� jest ju� zwi�zany wniosek " +
                        "o numerze "+gr.getSequenceId());
                }
                catch (EntityNotFoundException e)
                {
                }
                OfficeDocument officeDocument = OfficeDocument.findOfficeDocument(documentId);
                if (!(officeDocument.getType() == DocumentType.INCOMING))
                {
                    addActionError("Spodziewano si� dokumentu przychodz�cego");
                    return;
                }
                InOfficeDocument inDocument = (InOfficeDocument) officeDocument;
                
                GrantRequest grantRequest = new GrantRequest();
                grantRequest.create(officeCase, inDocument);

                id = grantRequest.getId();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                event.setResult("error");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getCaseId()
    {
        return caseId;
    }

    public void setCaseId(Long caseId)
    {
        this.caseId = caseId;
    }

    public Long getId()
    {
        return id;
    }
    
    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }
}
