package pl.compan.docusafe.web.record.grants;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.record.invoices.Invoice;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.record.grants.BeneficiaryFlow;
import pl.compan.docusafe.core.record.grants.GrantRequestFlow;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;

import java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.*;
import com.opensymphony.webwork.ServletActionContext;
/* User: Administrator, Date: 2006-09-11 13:40:04 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: GrantsReportsAction.java,v 1.4 2008/10/06 10:10:47 pecet4 Exp $
 */
public class GrantsReportsAction extends EventActionSupport
{
    private final String ASCENDING = "asc";
    private final String DESCENDING = "desc";
    private final String DECYZJA_SORT = "sequenceId";
    private final String WNIOSKODAWCA_SORT = "lastname";
    private final String ADRES_SORT = "street";


    	//	 @EXPORT/@IMPORT
	private Integer sequenceIdFrom;
    private Integer sequenceIdTo;
    private String zip;
    private String location;
    private String street;
    private String firstname;
    private String lastname;

    private Map sortFieldsList;
    private Map sortTypesList;
    private String sortField;
    private String sortType;

    protected void setup()
    {
        if (!Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
            throw new IllegalStateException("Wymagany jest modu� Stypendia");

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintCash").
            append(OpenHibernateSession.INSTANCE).
            append(new PrintCash()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintBank").
            append(OpenHibernateSession.INSTANCE).
            append(new PrintBank()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
             sortFieldsList = new LinkedHashMap(3);
             sortFieldsList.put(DECYZJA_SORT, "Nr. decyzji");
             sortFieldsList.put(WNIOSKODAWCA_SORT, "Wnioskodawca");
             sortFieldsList.put(ADRES_SORT, "Adres");

             sortTypesList = new LinkedHashMap(3);
             sortTypesList.put(ASCENDING, "rosn�co");
             sortTypesList.put(DESCENDING, "malej�co");
        }
    }

    private class PrintCash implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(false, event);
        }
    }

    private class PrintBank implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(true, event);
        }
    }

    private void search(boolean bank, ActionEvent event)
    {
      /*  try
        {
            DSApi.context().begin();

            java.util.List max = DSApi.context().session().find(
                    "select max(printed) from dsr_gra_request r,dsr_gra_beneficiary b where r.id=b.request_id " +
                        (bank ? "and bankAccount is not null" : "and bankAccount is null"));

            Integer printed = 1;
            if (max.size() > 0 && max.get(0) != null && max.get(0) instanceof Integer)
                 printed = (Integer) max.get(0) + 1;
        */
            GrantRequest.Query query = new GrantRequest.Query();

            //query.setPrinted(Integer.valueOf(0));
            query.setFirstname(TextUtils.trimmedStringOrNull(firstname));
            query.setLastname(TextUtils.trimmedStringOrNull(lastname));
            query.setZip(TextUtils.trimmedStringOrNull(zip));
            query.setLocation(TextUtils.trimmedStringOrNull(location));
            query.setStreet(TextUtils.trimmedStringOrNull(street));
            query.setSequenceIdFrom(sequenceIdFrom);
            query.setSequenceIdTo(sequenceIdTo);
            query.setWithBank(bank);


            query.setSortField(sortField);
            query.setAscending(ASCENDING.equals(sortType));


            File temp = null;
            String filename = "Lista_wyplat.pdf";
            try
            {

                SearchResults<GrantRequest> results = GrantRequest.search(query);

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);


                Font font = new Font(baseFont, 12);

                temp = File.createTempFile("docusafe_", "_tmp");

                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));

                HeaderFooter footer = new HeaderFooter(new Phrase("Wydruk sporz�dzony przez: "+ DSApi.context().getDSUser().asFirstnameLastname()+" w dniu "+DateUtils.formatJsDateTime(new Date())+"\n", font),false/*, new Phrase(".")*/);
                footer.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.setFooter(footer);

                pdfDoc.open();

                int[] widths = new int[] { 5, 10, 10, 5, 5 };

                PdfPTable table = new PdfPTable(widths.length);
                table.setWidthPercentage(100);
                table.setWidths(widths);


                table.addCell(new Phrase("Nr decyzji", font));                        //sequenceId
                table.addCell(new Phrase("Imi� i nazwisko wnioskodawcy", font));      //firstname + lastname
                table.addCell(new Phrase("Adres wnioskodawcy", font));                //address
                table.addCell(new Phrase("Kwota", font));                             //
                if (bank)
                    table.addCell(new Phrase("Nr rachunku bankowego", font));                //bankAccount
                else
                    table.addCell(new Phrase("Potwierdzenie odbioru", font));


                while (results.hasNext())
                {
                    GrantRequest request = (GrantRequest) results.next();

                    if (request.getStep().equals(GrantRequestFlow.KONIEC.getName()) && request.getBeneficiaries().size() > 0 && request.getBeneficiary(0).getStep().equals(BeneficiaryFlow.KONIEC.getName()))
                    {
                        table.addCell(new Phrase(request.getSequenceId() != null ? request.getSequenceId().toString() : "", font));  // nr w rejestrze
                        table.addCell(new Phrase((request.getFirstname() != null ? request.getFirstname() : "") + " "                // wnioskodawca
                                      + (request.getLastname() != null ? request.getLastname() : ""), font));
                        table.addCell(new Phrase(request.getAddress() != null ? request.getAddress() : "", font));                   // adres
                        table.addCell(new Phrase(((new BigDecimal(300)).subtract(request.getIncome())).toString(), font));           //kwota
                        if (bank)
                            table.addCell(new Phrase(request.getBeneficiary(0).getBankAccount() != null ? request.getBeneficiary(0).getBankAccount() : "", font));     // konto bankowe
                        else
                            table.addCell(new Phrase("", font));
                    }
                }

                pdfDoc.add(table);
                //pdfDoc.add(new Phrase("Wydruk sporz�dzony przez: "+ DSApi.context().getDSUser().asFirstnameLastname()+" w dniu "+DateUtils.formatJsDateTime(new Date())+"\n", font));

                pdfDoc.close();

                /* ustalanie nazwy pliku wynikowego */
                filename = "Lista_wyplat_do_" + (bank ? "banku" : "kasy") + "_" + DSApi.context().getDSUser().getName() + "_" +  DateUtils.formatJsDate(new Date()) + ".pdf";

            }
            catch (Exception e)
            {
                event.setResult(ERROR);
                event.getLog().error(e.getMessage(), e);
                addActionError(e.getMessage());
                //throw new EdmException(e.getMessage());
            }

            if (temp != null && temp.exists())
                {
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                            temp.length()+")");

                    try
                    {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                            "application/pdf", (int) temp.length(), "Content-Disposition: inline; filename=\""+filename+"\"");
                    }
                    catch (IOException e)
                    {
                        event.getLog().error("", e);
                    }
                    finally
                    {
                        temp.delete();
                    }
                }

          /*  DSApi.context().commit();

        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            addActionError(e.getMessage());
        }
        catch (Exception e)
        {
            DSApi.context().setRollbackOnly();
            addActionError(e.getMessage());
        }   */
    }


    public String getFirstname()
    {
       return firstname;
    }

    public void setFirstname(String firstname)
    {
       this.firstname = firstname;
    }

    public String getLastname()
    {
       return lastname;
    }

    public void setLastname(String lastname)
    {
       this.lastname = lastname;
    }

    public String getStreet()
    {
       return street;
    }

    public void setStreet(String street)
    {
       this.street = street;
    }

    public String getZip()
    {
       return zip;
    }

    public void setZip(String zip)
    {
       this.zip = zip;
    }

    public String getLocation()
    {
       return location;
    }

    public void setLocation(String location)
    {
       this.location = location;
    }

    public Integer getSequenceIdFrom()
    {
        return sequenceIdFrom;
    }

    public void setSequenceIdFrom(Integer sequenceIdFrom)
    {
        this.sequenceIdFrom = sequenceIdFrom;
    }

    public Integer getSequenceIdTo()
    {
        return sequenceIdTo;
    }

    public void setSequenceIdTo(Integer sequenceIdTo)
    {
        this.sequenceIdTo = sequenceIdTo;
    }

    public Map getSortTypesList()
    {
        return sortTypesList;
    }

    public Map getSortFieldsList()
    {
        return sortFieldsList;
    }

    public String getSortType()
    {
        return sortType;
    }

    public void setSortType(String sortType)
    {
        this.sortType = sortType;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }
}
