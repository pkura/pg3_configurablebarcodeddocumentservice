package pl.compan.docusafe.web.record.grants;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.record.grants.GrantRequestFlow;
import pl.compan.docusafe.core.record.grants.BeneficiaryFlow;
import pl.compan.docusafe.core.record.grants.Beneficiary;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.math.BigDecimal;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.*;
import com.opensymphony.webwork.ServletActionContext;

/**
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Micha� Ma�ski</a>
 *
 */
public class GrantsSearchAction extends EventActionSupport {

   // private static final String INNA_ODMOWA = "inna odmowa";
    public static final String BRAK_DECYZJI = "BRAK_DECYZJI";
    public static final String POMOC_PIENIEZNA = "POMOC_PIENIEZNA";
    public static final String POMOC_RZECZOWA = "POMOC_RZECZOWA";

    private String thisSearchUrl;

    // @EXPORT
    private SearchResults results;
    private Pager pager;
    private String printRequestersUrl;
    private String printFinishedRequestsUrl;
    private Map helpGrantedKinds;
    private Map decisions;
    private Map justifications;

    //	 @EXPORT/@IMPORT
	private Integer sequenceIdFrom;
    private Integer sequenceIdTo;
    private String startDocumentDate;
    private String endDocumentDate;
    private String zip;
    private String location;
    private String street;
    private String firstname;
    private String lastname;
    private String schoolName;
    private String schoolStreet;
    private String schoolZip;
    private String schoolLocation;
    private String helpGranted;
    private String justification;
    private String decision;

    //  @IMPORT
    private String sortField;
    private boolean ascending;
    private int offset;
    private final int limit = 10;

    protected void setup()
    {
        if (!Docusafe.moduleAvailable(Modules.MODULE_GRANTS))
            throw new IllegalStateException("Wymagany jest modu� Stypendia");

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintRequesters").
            append(OpenHibernateSession.INSTANCE).
            append(new PrintRequesters()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doPrintFinishedRequests").
            append(OpenHibernateSession.INSTANCE).
            append(new PrintFinishedRequests()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            helpGrantedKinds = new LinkedHashMap();
            helpGrantedKinds.put(POMOC_PIENIEZNA, "pieni�na");
            helpGrantedKinds.put(POMOC_RZECZOWA, "rzeczowa");

            decisions = new LinkedHashMap();
            decisions.put(BeneficiaryFlow.KONIEC.getName(), "pozytywna");
            decisions.put(BeneficiaryFlow.ODMOWA.getName(), "negatywna");
            decisions.put(BRAK_DECYZJI, "brak decyzji");

            justifications = new LinkedHashMap();
            justifications.put(BeneficiaryFlow.NIEODPOWIEDNIE_DOCHODY_WYJASNIENIE, "kryterium dochodowe");
            justifications.put(BeneficiaryFlow.NIEODPOWIEDNI_WIEK_WYJASNIENIE, "kryterium wiekowe");
           // justifications.put(INNA_ODMOWA, "inna");
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(true,false);
        }
    }

    private class PrintRequesters implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(false,false);

            File temp = null;
            String filename = "Zestawienie_wnioskodawcow.pdf";
            try
            {

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);


                Font font = new Font(baseFont, 12);

                temp = File.createTempFile("docusafe_", "_tmp");

                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));

                HeaderFooter footer = new HeaderFooter(new Phrase("Wydruk sporz�dzony przez: "+ DSApi.context().getDSUser().asFirstnameLastname()+" w dniu "+DateUtils.formatJsDateTime(new Date())+"\n", font),false/*, new Phrase(".")*/);
                footer.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.setFooter(footer);

                pdfDoc.open();

                int[] widths = new int[] { 5, 5, 10, 10, 5, /*5,*/ 5, 5 };

                PdfPTable table = new PdfPTable(widths.length);
                table.setWidthPercentage(100);
                table.setWidths(widths);


                table.addCell(new Phrase("Nr KO", font));                          // officeNumber
                table.addCell(new Phrase("Nr sprawy", font));                      // officeId
                table.addCell(new Phrase("Imi� i nazwisko wnioskodawcy", font));      // firstname + lastname
                table.addCell(new Phrase("Adres wnioskodawcy", font));                // address
  //              table.addCell(new Phrase("Liczba os�b w rodzinie", font));
                table.addCell(new Phrase("Doch�d na 1 osob�", font));                 // income
                table.addCell(new Phrase("Sytuacja rodzinna", font));
                table.addCell(new Phrase("Uwagi", font));

                while (results.hasNext())
                {
                    GrantRequest request = (GrantRequest) results.next();
                    InOfficeDocument inDocument = (InOfficeDocument) InOfficeDocument.find(request.getInDocumentId());

                    if (request.getBeneficiaries().size() > 0)
                    {
                        table.addCell(new Phrase(inDocument.getOfficeNumber() != null ? inDocument.getOfficeNumber().toString() : "", font));  // nr KO
                        table.addCell(new Phrase(request.getOfficeId() != null ? request.getOfficeId().toString() : "", font));                // nr sprawy
                        table.addCell(new Phrase((request.getFirstname() != null ? request.getFirstname() : "") + " "                // wnioskodawca
                                      + (request.getLastname() != null ? request.getLastname() : ""), font));
                        table.addCell(new Phrase(request.getAddress() != null ? request.getAddress() : "", font));                   // adres
                //        table.addCell(new Phrase("", font));                                       // osob w rodzinie
                        table.addCell(new Phrase(request.getIncome().toString(), font));           // dochod na 1 osobe
                        table.addCell(new Phrase("", font));                                       // sytuacja rodzinna
                        table.addCell(new Phrase("", font));                                       // uwagi
                    }
                }

                pdfDoc.add(new Phrase("Zestawienie wnioskodawc�w\n", font));
                pdfDoc.add(table);

                pdfDoc.close();

                /* ustalanie nazwy pliku wynikowego */
                filename = "Zestawienie_wnioskodawcow_" + DSApi.context().getDSUser().getName() + "_" +  DateUtils.formatJsDate(new Date()) + ".pdf";
            }
            catch (EdmException e)
            {
                event.setResult(ERROR);
                event.getLog().error(e.getMessage(), e);
                addActionError(e.getMessage());
                //throw new EdmException(e.getMessage());
            }
            catch (com.lowagie.text.DocumentException f)
            {
                event.setResult(ERROR);
                event.getLog().error(f.getMessage(), f);
                addActionError(f.getMessage());
            }
            catch (IOException f)
            {
                event.setResult(ERROR);
                event.getLog().error(f.getMessage(), f);
                addActionError(f.getMessage());
            }

            if (temp != null && temp.exists())
                {
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                            temp.length()+")");

                    try
                    {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                            "application/pdf", (int) temp.length(), "Content-Disposition: inline; filename=\""+filename+"\"");
                    }
                    catch (IOException e)
                    {
                        event.getLog().error("", e);
                    }
                    finally
                    {
                        temp.delete();
                    }
                }
        }
    }

    private class PrintFinishedRequests implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            search(false, true);

            File temp = null;
            String filename = "Zestawienie_wnioskow.pdf";
            try
            {

                File fontDir = new File(Configuration.getHome(), "fonts");
                File arial = new File(fontDir, "arial.ttf");
                BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);


                Font font = new Font(baseFont, 12);

                temp = File.createTempFile("docusafe_", "_tmp");

                com.lowagie.text.Document pdfDoc =
                    new com.lowagie.text.Document(PageSize.A4.rotate());
                PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));

                HeaderFooter footer = new HeaderFooter(new Phrase("Wydruk sporz�dzony przez: "+ DSApi.context().getDSUser().asFirstnameLastname()+" w dniu "+DateUtils.formatJsDateTime(new Date())+"\n", font),false/*, new Phrase(".")*/);
                footer.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.setFooter(footer);

                pdfDoc.open();

                int[] widths = new int[] { 5, 5, 10, 10, 5, 5, 5, 5 };

                PdfPTable table = new PdfPTable(widths.length);
                table.setWidthPercentage(100);
                table.setWidths(widths);


                table.addCell(new Phrase("Nr KO", font));                          // officeNumber
                table.addCell(new Phrase("Nr sprawy", font));                      // officeId
                table.addCell(new Phrase("Nr decyzji", font));                      // sequenceId ???
                table.addCell(new Phrase("Imi� i nazwisko wnioskodawcy", font));      // firstname + lastname
                table.addCell(new Phrase("Adres wnioskodawcy", font));                // address
                table.addCell(new Phrase("Imi� i nazwisko dziecka uprawnionego", font));
                table.addCell(new Phrase("Szko�a", font));                            // school
                table.addCell(new Phrase("Decyzja", font));

                while (results.hasNext())
                {
                    GrantRequest request = (GrantRequest) results.next();
                    InOfficeDocument inDocument = (InOfficeDocument) InOfficeDocument.find(request.getInDocumentId());
                    Beneficiary beneficiary = request.getBeneficiary(0);
                    if (request.getBeneficiaries().size() > 0)
                    {
                        table.addCell(new Phrase(inDocument.getOfficeNumber() != null ? inDocument.getOfficeNumber().toString() : "", font));  // nr KO
                        table.addCell(new Phrase(request.getOfficeId() != null ? request.getOfficeId().toString() : "", font));                // nr sprawy
                        table.addCell(new Phrase(request.getSequenceId() != null ? request.getSequenceId().toString() : "", font));                // nr decyzji
                        table.addCell(new Phrase((request.getFirstname() != null ? request.getFirstname() : "") + " "                // wnioskodawca
                                      + (request.getLastname() != null ? request.getLastname() : ""), font));
                        table.addCell(new Phrase(request.getAddress() != null ? request.getAddress() : "", font));                   // adres
                        table.addCell(new Phrase((beneficiary.getFirstname() != null ? beneficiary.getFirstname() : "") + " "
                                      + (beneficiary.getLastname() != null ? beneficiary.getLastname() : ""), font));                // imie i nazwisko dziecka
                        table.addCell(new Phrase(beneficiary.getSchoolAddress(), font));                           // szkola
                        table.addCell(new Phrase(request.getDecisionDescription(), font));                                                                     // decyzja
                    }
                }

                pdfDoc.add(new Phrase("Zestawienie wniosk�w po przeprowadzeniu analizy\n", font));
                pdfDoc.add(table);

                pdfDoc.close();

                /* ustalanie nazwy pliku wynikowego */
                filename = "Zestawienie_wnioskow_" + DSApi.context().getDSUser().getName() + "_" +  DateUtils.formatJsDate(new Date()) + ".pdf";
            }
            catch (EdmException e)
            {
                event.setResult(ERROR);
                event.getLog().error(e.getMessage(), e);
                addActionError(e.getMessage());
                //throw new EdmException(e.getMessage());
            }
            catch (com.lowagie.text.DocumentException f)
            {
                event.setResult(ERROR);
                event.getLog().error(f.getMessage(), f);
                addActionError(f.getMessage());
            }
            catch (IOException f)
            {
                event.setResult(ERROR);
                event.getLog().error(f.getMessage(), f);
                addActionError(f.getMessage());
            }

            if (temp != null && temp.exists())
                {
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("temp="+temp.getAbsolutePath()+" (rozmiar="+
                            temp.length()+")");

                    try
                    {
                        ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(temp),
                            "application/pdf", (int) temp.length(), "Content-Disposition: inline; filename=\""+filename+"\"");
                    }
                    catch (IOException e)
                    {
                        event.getLog().error("", e);
                    }
                    finally
                    {
                        temp.delete();
                    }
                }
        }

    }

    /**
     * Wykonuje przeszukiwanie, uaktualnia zmienne results i pager.
     */
    public void search(boolean applyOffsetLimit, boolean onlyFinished)
    {
        final String searchUrl =
            HttpUtils.makeUrl("/record/grants/grants-search.action",
                new Object[] {
                    "startDocumentDate", startDocumentDate,
                    "endDocumentDate", endDocumentDate,
                    "sequenceIdTo", sequenceIdTo,
                    "sequenceIdFrom", sequenceIdFrom,
                    "firstname", firstname,
                    "lastname", lastname,
                    "street", street,
                    "zip", zip,
                    "schoolLocation", schoolLocation,
                    "schoolName", schoolName,
                    "schoolStreet", schoolStreet,
                    "schoolZip", schoolZip,
                    "location", location,
                    "helpGranted", helpGranted,
                    "justification", justification,
                    "decision", decision,
                    "x", "y"
                });

        thisSearchUrl = searchUrl +
            "&doSearch=true";

        printRequestersUrl = searchUrl + "&doPrintRequesters=true" + (sortField != null ? ("&sortField=" + sortField + "&ascending="+ascending) : "");
        printFinishedRequestsUrl = searchUrl + "&doPrintFinishedRequests=true" + (sortField != null ? ("&sortField=" + sortField + "&ascending="+ascending) : "");

        GrantRequest.Query query;

        if (applyOffsetLimit)
            query = new GrantRequest.Query(offset, limit);
        else
            query = new GrantRequest.Query();

        query.setStartDocumentDate(DateUtils.nullSafeParseJsDate(startDocumentDate));
        query.setEndDocumentDate(DateUtils.nullSafeParseJsDate(endDocumentDate));
        query.setFirstname(TextUtils.trimmedStringOrNull(firstname));
        query.setLastname(TextUtils.trimmedStringOrNull(lastname));
        query.setZip(TextUtils.trimmedStringOrNull(zip));
        query.setLocation(TextUtils.trimmedStringOrNull(location));
        query.setStreet(TextUtils.trimmedStringOrNull(street));
        query.setSchoolName(TextUtils.trimmedStringOrNull(schoolName));
        query.setSchoolZip(TextUtils.trimmedStringOrNull(schoolZip));
        query.setSchoolLocation(TextUtils.trimmedStringOrNull(schoolLocation));
        query.setSchoolStreet(TextUtils.trimmedStringOrNull(schoolStreet));
        query.setHelpGranted(TextUtils.trimmedStringOrNull(helpGranted));
        query.setDecision(TextUtils.trimmedStringOrNull(decision));
        query.setJustification(TextUtils.trimmedStringOrNull(justification));
        query.setSequenceIdFrom(sequenceIdFrom);
        query.setSequenceIdTo(sequenceIdTo);

        if (onlyFinished)
            query.setFinished(true);

        query.setSortField(sortField);
        query.setAscending(ascending);



        try
        {
            results = GrantRequest.search(query);

            if (results.totalCount() == 0)
                addActionMessage("Nie znaleziono wniosk�w");

            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
            {
                public String getLink(int offset)
                {
                    return thisSearchUrl + (sortField != null ? ("&sortField="+sortField+"&ascending="+ascending) : "") +
                        "&offset="+offset;
                }
            };
            pager = new Pager(linkVisitor, offset, limit, results.totalCount(), 10);
        }
        catch (EdmException e)
        {
            addActionError(e.getMessage());
        }
    }

    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public Pager getPager()
    {
        return pager;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public Integer getSequenceIdFrom()
    {
        return sequenceIdFrom;
    }

    public void setSequenceIdFrom(Integer sequenceIdFrom)
    {
        this.sequenceIdFrom = sequenceIdFrom;
    }

    public Integer getSequenceIdTo()
    {
        return sequenceIdTo;
    }

    public void setSequenceIdTo(Integer sequenceIdTo)
    {
        this.sequenceIdTo = sequenceIdTo;
    }


    public String getStartDocumentDate()
    {
        return startDocumentDate;
    }

    public void setStartDocumentDate(String startDocumentDate)
    {
        this.startDocumentDate = startDocumentDate;
    }

    public String getEndDocumentDate()
    {
        return endDocumentDate;
    }

    public void setEndDocumentDate(String endDocumentDate)
    {
        this.endDocumentDate = endDocumentDate;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getPrintRequestersUrl()
    {
        return printRequestersUrl;
    }

    public String getPrintFinishedRequestsUrl()
    {
        return printFinishedRequestsUrl;
    }

    public String getSchoolName()
    {
        return schoolName;
    }

    public void setSchoolName(String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getSchoolStreet()
    {
        return schoolStreet;
    }

    public void setSchoolStreet(String schoolStreet)
    {
        this.schoolStreet = schoolStreet;
    }

    public String getSchoolZip()
    {
        return schoolZip;
    }

    public void setSchoolZip(String schoolZip)
    {
        this.schoolZip = schoolZip;
    }

    public String getSchoolLocation()
    {
        return schoolLocation;
    }

    public void setSchoolLocation(String schoolLocation)
    {
        this.schoolLocation = schoolLocation;
    }


    public Map getJustifications()
    {
        return justifications;
    }

    public Map getDecisions()
    {
        return decisions;
    }

    public Map getHelpGrantedKinds()
    {
        return helpGrantedKinds;
    }

    public String getJustification()
    {
        return justification;
    }

    public void setJustification(String justification)
    {
        this.justification = justification;
    }

    public String getDecision()
    {
        return decision;
    }

    public void setDecision(String decision)
    {
        this.decision = decision;
    }

    public String getHelpGranted()
    {
        return helpGranted;
    }

    public void setHelpGranted(String helpGranted)
    {
        this.helpGranted = helpGranted;
    }
}
