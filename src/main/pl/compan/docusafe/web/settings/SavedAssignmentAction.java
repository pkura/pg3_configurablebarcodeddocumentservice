package pl.compan.docusafe.web.settings;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Sets;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.Set;
import java.util.prefs.Preferences;

/**
 * User: Tomasz
 * Date: 10.03.14
 * Time: 14:19
 */
public class SavedAssignmentAction extends CollectiveAssignmentBaseAction {

    private final static StringManager sm = GlobalPreferences.loadPropertiesFile(SavedAssignmentAction.class.getPackage().getName(), null);
    private static final Logger log = LoggerFactory.getLogger(SavedAssignmentAction.class);

    private Set<String> savedPlanedAssignments;
    private String[] toDelete;

    protected void setup() {
        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new PrepareTabs()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteSelected").
                append(OpenHibernateSession.INSTANCE).
                append(new DeleteSelected()).
                append(new PrepareTabs()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class PrepareTabs implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            tabs = new Tabs();

            tabs.add(new Tab(sm.getString("ZbiorczaDekretacjaUzytkownika"), sm.getString("ZbiorczaDekretacjaUzytkownika"),
                    "/settings/user-collective-assignment.action", false));
            if(DSApi.context().isAdmin()) {
                tabs.add(new Tab(sm.getString("ZbiorczaDekretacjaDomyslna"), sm.getString("ZbiorczaDekretacjaDomyslna"),
                    "/settings/default-collective-assignment.action", false));
            }

            tabs.add(new Tab(sm.getString("savedAssignment"), sm.getString("savedAssignment"),"/settings/saved-assignment.action", true));

        }
    }

    private class FillForm implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            LoggerFactory.getLogger("tomekl").debug("SavedAssignmentAction -> FillForm");
            try {

                String namesAsString = DSApi.context().userPreferences().node("assignment").get("names", null);
                if(namesAsString == null) {
                    savedPlanedAssignments = Sets.newHashSet();
                } else {
                    savedPlanedAssignments = Sets.newHashSet(Splitter.on(';').split(namesAsString));
                }
            } catch (Exception e) {
                log.debug(e.getMessage(),e);
            }
        }
    }

    private class DeleteSelected implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

            try {
                DSApi.context().begin();
                if(toDelete != null && toDelete.length > 0) {
                    String namesAsString = DSApi.context().userPreferences().node("assignment").get("names", null);
                    savedPlanedAssignments = Sets.newHashSet(Splitter.on(';').split(namesAsString));

                    for(String name : toDelete) {
                        DSApi.context().userPreferences().node("assignment").node(name).removeNode();
                        savedPlanedAssignments.remove(name);
                    }

                    DSApi.context().userPreferences().node("assignment").put("names", Joiner.on(';').skipNulls().join(savedPlanedAssignments));
                }
                DSApi.context().commit();
            } catch (Exception e) {
                log.debug(e.getMessage(),e);
                addActionError(e.getMessage());
                DSApi.context()._rollback();
            }
        }
    }

    public Set<String> getSavedPlanedAssignments() {
        return savedPlanedAssignments;
    }

    public void setSavedPlanedAssignments(Set<String> savedPLanedAssignments) {
        this.savedPlanedAssignments = savedPLanedAssignments;
    }

    public String[] getToDelete() {
        return toDelete;
    }

    public void setToDelete(String[] toDelete) {
        this.toDelete = toDelete;
    }

    @Override
    protected Preferences getPreferences() {
        return null;
    }

    @Override
    protected boolean canUpdate() {
        return true;
    }

    @Override
    public String getBaseLink() {
        return "/settings/saved-assignment.action";
    }

    @Override
    public String getSubtitle() {
        return sm.getString("savedAssignment");
    }

    @Override
    public boolean isUserPref() {
        return true;
    }

}
