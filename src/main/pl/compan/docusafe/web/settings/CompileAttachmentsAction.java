package pl.compan.docusafe.web.settings;

import java.util.*;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja s�u��ca do konfiguracji procesu kompilacji plik�w sprawy. Obecnie polega tylko na defniniowaniu
 * kolejno�ci, w kt�rej wyszukiwane dokumenty maj� by� kompilowane.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class CompileAttachmentsAction extends EventActionSupport
{
    // @EXPORT
    /** Dotychczas wybrane typy dokument�w. */
    private Map<Integer,String> chosenTypes;
    /** Dost�pne typy dokument�w (czyli te, kt�rym jeszcze nie przydzielono kolejno�ci). */
    private Map<Integer,String> availableTypes;
    
    // @IMPORT
    /** typy dokument�w wybrane przez u�ytkownika w akcji "Update" */
    private Integer[] selectedTypes;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

                String compileOrder = GlobalPreferences.getCompileAttachmentsOrder();
                List<String> currentTypes;
                if (compileOrder == null)
                    currentTypes = new ArrayList<String>();
                else
                    currentTypes = Arrays.asList(compileOrder.split(";"));
                
                chosenTypes = new LinkedHashMap<Integer, String>();
                availableTypes = new LinkedHashMap<Integer, String>();

                for (String type : currentTypes)
                {
                    try
                    {
                        Integer id = Integer.parseInt(type);
                        availableTypes.remove(id);
                    }
                    catch (NumberFormatException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                }

        }

    }

    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                if (selectedTypes != null && selectedTypes.length > 0)
                    GlobalPreferences.setCompileAttachmentsOrder(StringUtils.join(selectedTypes, ";"));
                else
                    GlobalPreferences.setCompileAttachmentsOrder(null);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Map<Integer, String> getAvailableTypes()
    {
        return availableTypes;
    }

    public Map<Integer, String> getChosenTypes()
    {
        return chosenTypes;
    }

    public void setSelectedTypes(Integer[] selectedTypes)
    {
        this.selectedTypes = selectedTypes;
    }        
}
