package pl.compan.docusafe.web.settings;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.json.JSONArray;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.SetResultListener;

public class MergeColumnsAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;

	private static final List allColumns =
        Collections.unmodifiableList(Arrays.asList(pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.ALL_COLUMNS));

    private /*final static*/ StringManager sm =
        GlobalPreferences.loadPropertiesFile(MergeColumnsAction.class.getPackage().getName(),null);
    
    // @EXPORT
    private Map<String,String> selectedColumnsMap;
    private Map<String,String> availableColumns;
    private Set<Entry<String,String>> existingColumns;
    private Map<String,String> types;
    private String columnName;
    
    // @IMPORT
    private String[] selectedColumns;
    private String[] columnNamesChecked;
    
    // @IMPORT/@EXPORT
    private String type;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("user-settings"));
        
        registerListener("doCreate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Create()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
  
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                types = new LinkedHashMap<String, String>();
                //types.put("columns", sm.getString("wspolneKolumny"));
                                                                     
                List<DocumentKind> documentKinds = DocumentKind.list(true);
                for (DocumentKind documentKind : documentKinds)
                    types.put("columns_"+documentKind.getCn(), sm.getString("specyficzneKolumnyDla")+" '"+documentKind.getName()+"'");
                if (type == null)
                    type = "columns_" + documentKinds.get(0).getCn();
                DocumentKind documentKind = type.length() > 8 ? DocumentKind.findByCn(type.substring("columns_".length())) : null;

                
                List<String> available = new ArrayList<String>(DocumentKindsManager.getAllDockindColumns(documentKind));
                availableColumns = DocumentKindsManager.getDockindColumns(documentKind, available);
                
                JSONArray array = new JSONArray(DSApi.context().systemPreferences().node("complex-columns").get(type, new JSONArray().toString()));
                Map<String,String> existCols = new TreeMap<String, String>();
                for (int i=0; i < array.length(); i++)
                {
                	existCols.put(array.getString(i), array.getString(i));
                }
                existingColumns = existCols.entrySet();
                if(columnName!=null&&!columnName.equals(""))
                {
                	array = new JSONArray(DSApi.context().systemPreferences().node("complex-columns"+type.substring("columns_".length())).get(columnName, "[]"));
                	selectedColumns = new String[array.length()];
                	for (int i=0; i < array.length(); i++)
                    {
                        selectedColumns[i] = array.getString(i);
                    }
                }
                if(selectedColumns!=null)
                {
                	selectedColumnsMap = new TreeMap<String, String>();
                	for(String col:selectedColumns)
                	{
                		selectedColumnsMap.put(col, documentKind.getFieldByCn(col).getName());
                		availableColumns.remove(col);
                	}
                }

            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            catch (ParseException e) 
            {
                addActionError(e.getMessage());
			}
        }
    }
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (selectedColumns == null || selectedColumns.length == 0)
        	{
                addActionError(sm.getString("NieWybranoKolumn"));
                return;
        	}
        	
        	if(columnName==null||columnName.equals(""))
        	{
        		addActionError(sm.getString("NiePodanoNazwyKolumny"));
        		return;
        	}
            
            try
            {
                DSApi.context().begin();

                DocumentKind documentKind = type.length() > 8 ? DocumentKind.findByCn(type.substring("columns_".length())) : null;
                
                String columnsDef;
                columnsDef = new JSONArray(Arrays.asList(selectedColumns)).toString();
                
                Preferences prefs = DSApi.context().systemPreferences().node("complex-columns"+type.substring("columns_".length()));
                prefs.put(columnName, columnsDef);
                prefs.flush();
                
                prefs = DSApi.context().systemPreferences().node("complex-columns");
                JSONArray array = new JSONArray(prefs.get(type, "[]"));
                boolean exists = false;
                for (int i=0; i < array.length(); i++)
                {
                    if(array.getString(i).equals(columnName)) exists = true;
                }
                if(!exists)
                {
                	array.put(columnName);
                	prefs.put(type, array.toString());
                	prefs.flush();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            catch (ParseException e) 
            {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
            catch (BackingStoreException e) 
            {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
            addActionMessage(sm.getString("PomyslnieUtworzonoZbiorczaKolumne", columnName));
        }
    }
    
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	        	DSApi.context().begin();
	        	for(String col:columnNamesChecked)
	        	{
	        		DSApi.context().systemPreferences().node("complex-columns"+type.substring("columns_".length())).remove(col);
	                JSONArray array = new JSONArray(DSApi.context().systemPreferences().node("complex-columns").get(type, "[]"));
	                List<String> newcols = new ArrayList<String>();
	                for (int i=0; i < array.length(); i++)
	                {
	                    if(!array.getString(i).equals(col)) newcols.add(array.getString(i));
	                }
	                array = new JSONArray(newcols);
	                DSApi.context().systemPreferences().node("complex-columns").put(type, array.toString());
	                
	                //usuwamy z preferencji uzytkownika jesli ustwail kolumne na widoczna
	                String columnsString = DSApi.context().systemPreferences().node("search-documents").get(type,"");
	                array = new JSONArray(columnsString);
	                newcols = new ArrayList<String>();
	                for (int i=0; i < array.length(); i++)
	                {
	                    if(!array.getString(i).equals("#complex#" + col)) newcols.add(array.getString(i));
	                }
	                array = new JSONArray(newcols);
	                DSApi.context().systemPreferences().node("search-documents").put(type, array.toString());
	        	}
	        	
	        	DSApi.context().commit();
	        }
	        catch (EdmException e)
	        {
	            DSApi.context().setRollbackOnly();
	            addActionError(e.getMessage());
	        }
	        catch (ParseException e) 
            {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
        }
    }
    
    public Map<String,String> getAvailableColumns()
    {
        return availableColumns;
    }

    public Map<String,String> getSelectedColumnsMap()
    {
        return selectedColumnsMap;
    }

    public void setSelectedColumns(String[] selectedColumns)
    {
        this.selectedColumns = selectedColumns;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, String> getTypes()
    {
        return types;
    }

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public Set<Entry<String,String>> getExistingColumns() {
		return existingColumns;
	}

	public void setColumnNamesChecked(String[] columnNamesChecked) {
		this.columnNamesChecked = columnNamesChecked;
	}
	
}
