package pl.compan.docusafe.web.settings;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ViewLabelsAction extends EventActionSupport
{
	static final long serialVersionUID = -1L;
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(ViewLabelsAction.class.getPackage().getName(),null);
    
    
    private List<Label> availableLabels;
    private Long[] labelsIds;
	private String name;
	private String description;
	private Long parentId;
	private String owner;
	private Integer inactionTime;
	private Boolean hidesTasks;
	private Long ancestorId;
	
    private List<Label> availableModifiableLabels;
    private Long id;
	private Boolean deletesAfter;
	private Integer saveResult;
	/*
	 * Flaga dla aktualizowanej etykiety.
	 */
	public static int LABEL_RESULT_UPDATE = 0;
	/*
	 * Flaga dla tworzonej etykiety.
	 */
	public static int LABEL_RESULT_NEW = 1;



	protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (saveResult != null)
        			if (saveResult == LABEL_RESULT_NEW) addActionMessage(sm.getString("DodanoEtykiete"));
        			else if (saveResult == LABEL_RESULT_UPDATE) addActionMessage(sm.getString("ZapisanoZmiany"));
        		availableLabels = LabelsManager.getModifyLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }
    
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(labelsIds==null||labelsIds.length==0)
        	{
        		addActionError(sm.getString("NieWybranoZadnychEtykiet"));
        		return;
        	}
        		
        	try
        	{
        		DSApi.context().begin();
        		for(int i=0;i<labelsIds.length;i++)
        		{
        			LabelsManager.deleteLabel(labelsIds[i]);
        		}
        		DSApi.context().commit();
        		addActionMessage(sm.getString(labelsIds.length == 0 ? "UsunietoEtykiete" : "UsunietoEtykiete"));        		
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(sm.getString("NieUdaloSieUsunacEtykiet"));
        	}
        	finally
        	{
        		
        	}
        }
    }
    
   

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getInactionTime() {
		return inactionTime;
	}

	public void setInactionTime(Integer inactionTime) {
		this.inactionTime = inactionTime;
	}

	public Boolean getHidesTasks() {
		return hidesTasks;
	}

	public void setHidesTasks(Boolean hidesTasks) {
		this.hidesTasks = hidesTasks;
		if(hidesTasks==null) hidesTasks = false;
	}

	public Long getAncestorId() {
		return ancestorId;
	}

	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public void setLabelsIds(Long[] labelsIds) {
		this.labelsIds = labelsIds;
	}

	public List<Label> getAvailableModifiableLabels() {
		return availableModifiableLabels;
	}

	public void setAvailableModifiableLabels(List<Label> availableModifiableLabels) {
		this.availableModifiableLabels = availableModifiableLabels;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getDeletesAfter() {
		return deletesAfter;
	}

	public void setDeletesAfter(Boolean deletesAfter) {
		this.deletesAfter = deletesAfter;
	}

	public Long[] getLabelsIds() {
		return labelsIds;
	}

	public Integer getSaveResult() {
		return saveResult;
	}

	public void setSaveResult(Integer saveResult) {
		this.saveResult = saveResult;
	}
	
	
    
    
}
