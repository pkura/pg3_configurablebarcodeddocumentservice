package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class OpsAction extends EventActionSupport {

	private Map<Integer, String> listaTypow;
	private Map<Integer, String> listaWidocznychTypow;
	private String[] listaWybranychTypow;
	
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).	        
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	listaTypow = new HashMap<Integer, String>();
        	listaWidocznychTypow = new HashMap<Integer, String>();
        	ArrayList<String> idsAl = new ArrayList<String>();
        	try
        	{
        		DSApi.context().begin();
        		//wczytanie z preferencji
        		String idsFromPref = DSApi.context().userPreferences().node("ops").get("toSave", "brak"); 
        		if(!idsFromPref.equalsIgnoreCase("brak"))
        		{
	        		String[] ids = idsFromPref.split("\\s*;\\s*");
	        		
	        		for(int i=0; i<ids.length; i++)
	        		{	
	        			idsAl.add(ids[i]);
	        		}
        		}
        		//wczytanie z preferencji : end
        		
        		DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND);
        		kind.initialize();
        		Field field = kind.getFieldByCn("RODZAJ");
        		List<EnumItem> lista = field.getEnumItems();
        		for(EnumItem item : lista)
        		{	
        			if(!idsFromPref.equalsIgnoreCase("brak") && idsAl.contains(item.getId().toString()))
        				listaWidocznychTypow.put(item.getId(), item.getTitle());
        			else listaTypow.put(item.getId(), item.getTitle());
        		}
        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{}        	
        }
    }
	
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	String toSave = "brak";
        	if(listaWybranychTypow != null)
        	{
        		toSave = "";
	        	for(int i=0;i<listaWybranychTypow.length;i++)
	        	{
	        		 	toSave += listaWybranychTypow[i];
	        		 	if(i != listaWybranychTypow.length-1)
	        		 		toSave += ";";
	        	}
        	}
	        	//DSApi.context().userPreferences().node("task-list").put("columns_"+tab, columnsDef);
	        	try
	        	{
	        		DSApi.context().begin();
	        		DSApi.context().userPreferences().node("ops").put("toSave", toSave);
	        		DSApi.context().commit();
	        	}
	        	catch(EdmException e)
	        	{}
        	
        }
    }

	public String[] getListaWybranychTypow() {
		return listaWybranychTypow;
	}

	public void setListaWybranychTypow(String[] listaWybranychTypow) {
		this.listaWybranychTypow = listaWybranychTypow;
	}

	public Map<Integer, String> getListaTypow() {
		return listaTypow;
	}

	public void setListaTypow(Map<Integer, String> listaTypow) {
		this.listaTypow = listaTypow;
	}

	public Map<Integer, String> getListaWidocznychTypow() {
		return listaWidocznychTypow;
	}

	public void setListaWidocznychTypow(Map<Integer, String> listaWidocznychTypow) {
		this.listaWidocznychTypow = listaWidocznychTypow;
	}
	
	
}
