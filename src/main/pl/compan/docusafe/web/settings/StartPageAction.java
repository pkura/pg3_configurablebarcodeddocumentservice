package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.util.LinkedHashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-04-27 14:23:28 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: StartPageAction.java,v 1.4 2007/09/13 11:38:13 mariuszk Exp $
 */
public class StartPageAction extends EventActionSupport
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(StartPageAction.class.getPackage().getName(),null);
    
    // @EXPORT
    private Map pages;

    // @EXPORT/@IMPORT
    private String name;


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            name = GlobalPreferences.getUserStartPage();

            try
            {
                pages = new LinkedHashMap();
                pages.put("", sm.getString("StronaDomyslna"));
                if (!Configuration.simpleOfficeAvailable())
                {
                    pages.put(GlobalPreferences.EXPLORE_DOCUMENTS, sm.getString("Archiwum"));
                }
                if (Configuration.officeAvailable())
                {
                    pages.put(GlobalPreferences.TASKS, sm.getString("ListaZadan"));
                }
                pages.put(GlobalPreferences.FIND_OFFICE_DOCUMENTS, sm.getString("WyszukiwarkaPism"));
                if (DSApi.context().hasPermission(DSPermission.BOK_LISTA_PISM))
                {
                    pages.put(GlobalPreferences.TASKS_BOK, sm.getString("ListaZadanBOK"));
                }
                pages.put(GlobalPreferences.SEARCH_DOCUMENTS_REDIRECT, sm.getString("WyszukiwanieDokumentow"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                GlobalPreferences.setUserStartPage(TextUtils.trimmedStringOrNull(name));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Map getPages()
    {
        return pages;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
