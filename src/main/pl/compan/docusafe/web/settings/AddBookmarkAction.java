package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.bookmarks.BookmarkRemoveThread;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.gson.Gson;

public class AddBookmarkAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AddBookmarkAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(AddBookmarkAction.class.getPackage().getName(),null);

	private BookmarkManager bm = new BookmarkManager();
	
    private Long id;
	private String name;
	private String cn;
	private String title;
	private Map<String, String> types;
	private String selectedType;
	private Boolean adminUser;
	private List<Bookmark> bookmarks;
	private boolean fromEdit;
	
    /**
     * Dostepne kolumny dla danego typu ( cn/nazwa ).
     */
    private Map<String,String> allColumns;
    /**
     * Dostepne kolumny dla danego typu ( cn/nazwa ).
     */
    private Map<String,String> availableColumns;
    /**
     * Kolumny na li�cie zada� (mapa dost�pnych kolumn MINUS kolumny wybrane).
     */
    private Map<String,String> selectedMapColumns;
    /**
     * Kolumny po kt�rych mo�na filtrowa�.
     */
    private Map<String,String> filterColumns;
    /**
     * Warto�ci zdefiniowanych filtr�w.
     */
    private String[] filterValue;
    /**
     * Wybrane kolumny do filtrowania.
     */
    private String[] selectedFilteredColumns;
    /**
     * Kolumny wybrane do zak�adki.
     */
    private String[] selectedColumns;
    /**
     * Dost�pne etykiety.
     */
    private List<Label> availableLabels;
    /**
     * Wybrane etykiety.
     */
    private Long[] selectedLabels;
    
    private List<DSUser> users;
    private String[] selectedUsers;
    
    private boolean filtersAvailable = true;
    private boolean labelsAvailable = true;
    private boolean columnsAvailable = true;
    
    private int taskCount;
    private List<Integer> taskCounts;
    
    private Map <String, String> ascendingType;
    private String ascending;
    
    private String sortColumn;
	
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
        	append(OpenHibernateSession.INSTANCE).
        	append(new AddBookmark()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAddSystemBookmark").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new AddSystemBookmark()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRemove").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Remove()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doClear").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Clear()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
	        
        registerListener("doSave").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Save()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		adminUser = DSApi.context().isAdmin();
        		types = bm.prepareTypes();
        		ascendingType = bm.prepareAscendingTypes();
        		// wybranie danego typu zak�adki je�li nie zaznaczono �adnego
        		selectedType = (selectedType == null) ? BookmarkManager.TAB_IN : selectedType;
        		
        		// wszystkie mo�liwe kolumny dla danego typu
   	            String[] allProperties = TaskListUtils.getColumnProperties(selectedType,null);
   	            // kolumny po kt�rych mo�na filtrowa�
   	            String[] filterColumnsTab = StringUtils.join(TaskListUtils.getDefaultColumnProperties("filter", null), ",").split(",");
   	            
   	            List<String> filterColumnsProp = new ArrayList<String>(Arrays.asList(filterColumnsTab));
   	            List<String> lstAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
   	            
   	            bm.prepareDockindAttributes(lstAllColumns);

   	            availableColumns = new LinkedHashMap<String,String>();
   	            filterColumns = new LinkedHashMap<String,String>();
   	            
   	            for (Iterator<String> iter=filterColumnsProp.iterator(); iter.hasNext(); )
   	            {
   	                String col = iter.next();
   	                filterColumns.put(col, TaskListUtils.getColumnDescription(col, cn));
   	            }
   	            for (Iterator<String> iter=lstAllColumns.iterator(); iter.hasNext(); )
   	            {
   	                String col = iter.next();
   	                availableColumns.put(col, TaskListUtils.getColumnDescription(col, cn));
   	            }
   	            allColumns = new HashMap<String, String>(availableColumns);
   	            // usuni�cie kolumn do filtrowania kt�rych nie ma w dostepnych kolumnach
   	            List<String> toRemove = new ArrayList<String>();
   	            for(String f : filterColumns.keySet()) {
   	            	if (!availableColumns.containsKey(f))
   	            		toRemove.add(f);
   	            }
   	            for(String tR : toRemove) {
   	            	filterColumns.remove(tR);
   	            }
   	            
   	            // wczytanie zak�adek do edycji
            	bookmarks = BookmarkManager.getAvailableBookmarksForUser(DSApi.context().getPrincipalName(), DSApi.context().isAdmin());
   	            // tutaj wczytanie mo�liwych etykiet
	   	        availableLabels = bm.getAvailableLabels();
	   	        
   	            // tutaj wczytujemy dane w przypadku edycji zak�adki
   	            Bookmark b = null;
   	            if (id != null) 
   	            {
   	            	// przygotowanie odpowiednich map
   	            	selectedMapColumns = new LinkedHashMap<String,String>();
   	            	
   	            	// znalezienie zak�adki
   	            	b = Bookmark.find(id);
   	            	if (fromEdit) 
   	            	{
   	            		selectedType=b.getType();
   	            		allProperties = TaskListUtils.getColumnProperties(selectedType,null);
   	            		lstAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
   	            		bm.prepareDockindAttributes(lstAllColumns);
   	            		availableColumns = new LinkedHashMap<String,String>();
   	            		for (Iterator<String> iter=lstAllColumns.iterator(); iter.hasNext(); )
   	    	            {
   	    	                String col = iter.next();
	   			   	  	    availableColumns.put(col, TaskListUtils.getColumnDescription(col, b.getCn()));	   			   	  	 
   	    	            }
   	            	}
   	            	allColumns = new HashMap<String, String>(availableColumns);
   	            	name = b.getName();
   	            	cn = b.getCn();
   	            	title = b.getTitle();
   	            	// pobranie wybranych kolumn
   	            	selectedColumns = (String[]) b.getColumnsList().toArray();
   	            	// je�li mamy edytowa�, przygotowanie select�w z wybranymi ju� kolumnami
   	            	for (String s : b.getColumnsList()) {
   	            		selectedMapColumns.put(s, availableColumns.get(s));
   	            		availableColumns.remove(s);
   	            	}
   	            	// etykiety
   	            	if (b.getLabelsArray() != null) {
   	            		selectedLabels = b.getLabelsArray();
   	            	}
   	            		
   	            	// tablice filtr�w
   	            	selectedFilteredColumns = new String[b.getFilters().size()];
   	            	filterValue = new String[b.getFilters().size()];
   	            	
   	            	// wczytywanie kryteri�w filtr�w
   	            	int i =0;
   	            	for(FilterCondition fc : b.getFilters())
   	            	{
   	            		selectedFilteredColumns[i] = fc.getColumn();
   	            		filterValue[i] = fc.getValues();
   	            		i++;
   	            	}
   	            	
   	            	taskCount = b.getTaskCount();
   	            	sortColumn = b.getSortField();
   	            	if (b.getAscending() != null) ascending = b.getAscending().toString();
   	            }
   	            
	            Docusafe.getAdditionProperty("taskCounts");
	            taskCounts = new ArrayList<Integer>();
	            for(String count : Docusafe.getAdditionProperty("taskCounts").split(","))
	            {
	            	taskCounts.add(Integer.parseInt(count));
	            }
	            
        		if (BookmarkManager.TAB_ORDER.equals(selectedType))
        		{
        			columnsAvailable = false;
        		}
        		
        		if (BookmarkManager.TAB_CASES.equals(selectedType) || BookmarkManager.TAB_ORDER.equals(selectedType) || BookmarkManager.TAB_WATCHES.equals(selectedType))
        		{
        			filtersAvailable = false;
        			labelsAvailable = false;
        		}
	            
	            fromEdit = false;
        	}
        	catch(Exception e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class AddBookmark implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
    			
        		if (validateValues()) {
        			addActionError(sm.getString("NameTitleTypeColumnsRequired"));
        			return;
        		}
        		if(validateCn()){
        			addActionError(sm.getString("BookmarkCodenameError"));
        			return;
        		}
        		Bookmark b = createBookmark(name, title, selectedType, cn);
    			b.setKind(Bookmark.KIND_USER_BOOKMARK);
    			b.setOwner(DSApi.context().getPrincipalName());
    			
    			if (!BookmarkManager.TAB_ORDER.equals(selectedType))
    			{
	    			// ustawienie odpowiednich kolumn
	        		b.setColumnList(Arrays.asList(selectedColumns));
	        		// je�eli odpowiednio zdefiniowano filtry
	        		if (validateDefineFilters())
	        		{
	        			for(int i=0;i<selectedFilteredColumns.length;i++) {
	        				if (selectedFilteredColumns[i] != null && filterValue[i] != null && !filterValue[i].equals("")) {
	        					b.getFilters().add(new FilterCondition(selectedFilteredColumns[i], filterValue[i]));
	        				}
	        			}
	        		}
    			}
    			
        		if (validateLabels()) b.setLabelsArray(selectedLabels);
        		
        		b.create();
        		
        		addActionMessage("Dodano zak�adk�");
        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        		log.error("",e);
        	}
        }
    }
	
	private boolean validateValues() {
		if (title==null || name==null)
			return true;
		if (BookmarkManager.TAB_ORDER.equals(selectedType) || BookmarkManager.TAB_WATCHES.equals(selectedType) || BookmarkManager.TAB_CASES.equals(selectedType))
			return false;
		if (selectedColumns == null || selectedColumns.length == 0) 
			return true;
		
		return false;
	}

	private boolean validateCn() {
		if(StringUtils.isEmpty(cn))
			return false;
		Pattern r = Pattern.compile("^[a-zA-Z0-9_]+$");
	    Matcher m = r.matcher(cn);
		if (m.matches())
			return false;
		return true;
	}
	/*private String createCn(){
		return TextUtils.latinize(name).replaceAll(" ","_").replaceAll("[^a-zA-Z0-9_]", "");
	}*/
	private boolean validateLabels() {
		if (selectedLabels != null && selectedLabels.length > 0)
			return true;
		return false;
	}
	
	private boolean validateDefineFilters() {
		if(filterValue != null && filterValue.length >0 && selectedFilteredColumns != null && selectedFilteredColumns.length >0)
			return true;
		return false;
	}
	
	private class AddSystemBookmark implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		if (validateValues()) {
        			addActionError(sm.getString("NameTitleTypeColumnsRequired"));
        			return;
        		}
        		if(validateCn()){
        			addActionError(sm.getString("BookmarkCodenameError"));
        			return;
        		}
        		
	    		// utworzenie zak�adki
        		Bookmark b = createBookmark(name, title, selectedType, cn);
				b.setKind(Bookmark.KIND_SYSTEM_BOOKMARK);
				b.setOwner(Bookmark.SYSTEM_BOOKMARK);
				// ustawienie odpowiednich kolumn
	    		b.setColumnList(Arrays.asList(selectedColumns));
	    		// je�eli odpowiednio zdefiniowano filtry
        		if (validateDefineFilters()) {
        			for(int i=0;i<selectedFilteredColumns.length;i++) {
        				if (selectedFilteredColumns[i] != null && filterValue[i] != null && !filterValue[i].equals("")) {
        					b.getFilters().add(new FilterCondition(selectedFilteredColumns[i], filterValue[i]));
        				}
        			}
        		}
        		if (validateLabels()) {
        			b.setLabelsArray(selectedLabels);
        		}
	    		b.create();
	    		id = b.getId();
	    		addActionMessage("Dodano zak�adk�");
        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		
        		if (validateValues()) {
        			addActionError(sm.getString("NameTitleTypeColumnsRequired"));
        			return;
        		}
        		if(validateCn()){
        			addActionError(sm.getString("BookmarkCodenameError"));
        			return;
        		}
        		Bookmark b = Bookmark.find(id);
        		b.setTitle(title);
        		b.setName(name);
        		b.setCn(cn);
        		b.setType(selectedType);
				// ustawienie odpowiednich kolumn
	    		b.setColumnList(Arrays.asList(selectedColumns));
	    		// je�eli odpowiednio zdefiniowano filtry
	    		b.getFilters().clear();
        		if (validateDefineFilters()) {
        			for(int i=0;i<selectedFilteredColumns.length;i++) {
        				if (selectedFilteredColumns[i] != null && filterValue[i] != null &&  !filterValue[i].equals("")) {
        					b.getFilters().add(new FilterCondition(selectedFilteredColumns[i], filterValue[i]));
        				}
        			}
        		}
    			b.setLabelsArray(selectedLabels);
    			b.setTaskCount(taskCount);
    			b.setSortField(sortColumn);
    			LoggerFactory.getLogger("przemek").debug("ASCENDING: {}",ascending);
    			if (ascending!=null) b.setAscending(Boolean.valueOf(ascending));
        		DSApi.context().session().update(b);
        		addActionMessage("Zapisano zmiany");
        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	

	
	private class Remove implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		Bookmark b = Bookmark.find(id);
        		String uIds = null;
        		String sIds = null;
        		//FIXME to powinno by� wydzielone  do bookmarks managera
        		Gson g = new Gson();
        		
        		sIds = DSApi.context().systemPreferences().node("bookmarks").get("list", "");
        		uIds = DSApi.context().userPreferences().node("bookmarks").get("list", "");
        		
        		Long[] systemIds = null;
        		if (!sIds.equals("")) {
        			systemIds = g.fromJson(sIds, Long[].class);
        		}
        		Long[] userIds = null;
        		if (!uIds.equals("")) {
        			userIds = g.fromJson(uIds, Long[].class);
        		}
        		if (b.getKind().equals(Bookmark.KIND_USER_BOOKMARK) && userIds != null && userIds.length>0) {
        			// je�eli w preferencjach u�ytkownika znajduje si� ta zak�adka to usuwamy
        			if (ArrayUtils.contains(userIds, b.getId())) {
	        			userIds = (Long[])org.apache.commons.lang.ArrayUtils.removeElement(userIds, b.getId());
	        			DSApi.context().userPreferences().node("bookmarks").put("list", g.toJson(userIds));
        			}
        		}
        		if (b.getKind().equals(Bookmark.KIND_SYSTEM_BOOKMARK)) {
        			if (systemIds!= null && ArrayUtils.contains(systemIds, b.getId())) {
	        			systemIds = (Long[])org.apache.commons.lang.ArrayUtils.removeElement(systemIds, b.getId());
	        			DSApi.context().systemPreferences().node("bookmarks").put("list", g.toJson(systemIds));
        			}
    				// w przypadku zak�adki systemowej musimy sprawdzi� preferencje wszystkich u�ytkownik�w
    				// i je�eli maj� zdefiniowan� t� zakadk� to j� usun��
    				Thread td = new Thread(new BookmarkRemoveThread(id,DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME)));
		    		td.start();
        		}
        		b.delete();
        		id = null;
        		DSApi.context().commit();
        		addActionMessage("Usuni�to zak�adk�.");
        	}
        	catch(Exception e)
        	{
        		log.error("",e);
        		addActionError(e.getMessage());
        	}
        }
    }
	
	private class Clear implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
    			DSApi.context().systemPreferences().node("bookmarks").clear();
    			DSApi.context().userPreferences().node("bookmarks").clear();
        		DSApi.context().commit();
        	}
        	catch(Exception e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private Bookmark createBookmark(String name, String title, String selectedType, String cn) {
		Bookmark b = new Bookmark();
		b.setName(name);
		b.setCn(cn);
		b.setTitle(title);
		b.setType(selectedType);
		b.setTaskCount(taskCount);
		b.setSortField(sortColumn);
		if (ascending!=null) b.setAscending(Boolean.valueOf(ascending));
		return b;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Map<String, String> getTypes() {
		return types;
	}

	public void setTypes(Map<String, String> types) {
		this.types = types;
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public Map<String, String> getAvailableColumns() {
		return availableColumns;
	}

	public void setAvailableColumns(Map<String, String> availableColumns) {
		this.availableColumns = availableColumns;
	}

	public String[] getSelectedColumns() {
		return selectedColumns;
	}

	public void setSelectedColumns(String[] selectedColumns) {
		this.selectedColumns = selectedColumns;
	}

	public Map<String, String> getFilterColumns() {
		return filterColumns;
	}

	public void setFilterColumns(Map<String, String> filterColumns) {
		this.filterColumns = filterColumns;
	}

	public String[] getSelectedFilteredColumns() {
		return selectedFilteredColumns;
	}

	public void setSelectedFilteredColumns(String[] selectedFilteredColumns) {
		this.selectedFilteredColumns = selectedFilteredColumns;
	}

	public String[] getFilterValue() {
		return filterValue;
	}

	public void setFilterValue(String[] filterValue) {
		this.filterValue = filterValue;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, String> getSelectedMapColumns() {
		return selectedMapColumns;
	}

	public void setSelectedMapColumns(Map<String, String> selectedMapColumns) {
		this.selectedMapColumns = selectedMapColumns;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public Long[] getSelectedLabels() {
		return selectedLabels;
	}

	public void setSelectedLabels(Long[] selectedLabels) {
		this.selectedLabels = selectedLabels;
	}

	public List<DSUser> getUsers() {
		return users;
	}
	
	public String[] getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(String[] selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Boolean getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(Boolean adminUser) {
		this.adminUser = adminUser;
	}

	public List<Bookmark> getBookmarks() {
		return bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public boolean isFiltersAvailable() {
		return filtersAvailable;
	}

	public void setFiltersAvailable(boolean filtersAvailable) {
		this.filtersAvailable = filtersAvailable;
	}

	public boolean isLabelsAvailable() {
		return labelsAvailable;
	}

	public void setLabelsAvailable(boolean labelsAvailable) {
		this.labelsAvailable = labelsAvailable;
	}

	public boolean isColumnsAvailable() {
		return columnsAvailable;
	}

	public void setColumnsAvailable(boolean columnsAvailable) {
		this.columnsAvailable = columnsAvailable;
	}

	public int getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(int taskCount) {
		this.taskCount = taskCount;
	}

	public List<Integer> getTaskCounts() {
		return taskCounts;
	}

	public void setTaskCounts(List<Integer> taskCounts) {
		this.taskCounts = taskCounts;
	}

	public boolean isFromEdit() {
		return fromEdit;
	}

	public void setFromEdit(boolean fromEdit) {
		this.fromEdit = fromEdit;
	}

	public Map<String, String> getAscendingType() {
		return ascendingType;
	}

	public void setAscendingType(Map<String, String> ascendingType) {
		this.ascendingType = ascendingType;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public Map<String, String> getAllColumns() {
		return allColumns;
	}

	public void setAllColumns(Map<String, String> allColumns) {
		this.allColumns = allColumns;
	}

	public String getAscending() {
		return ascending;
	}

	public void setAscending(String ascending) {
		this.ascending = ascending;
	}
	
}