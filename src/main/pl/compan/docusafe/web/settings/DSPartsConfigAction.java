package pl.compan.docusafe.web.settings;
import java.util.Map;
import pl.compan.docusafe.core.cfg.part.DSPartsConfig;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.*;

/**
 *
 * @author Micha� Sankowski
 */
public class DSPartsConfigAction extends EventActionSupport{
	private final static Logger LOG = LoggerFactory.getLogger(DSPartsConfigAction.class);

	private String configuration = "default";
	private Map<String, String> configurations;
	private Map<String, String> parts;
	private Map<String, Map<String,String>> partOptions;

	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_SESSION)
			.append(new FillForm())
			.appendFinally(CLOSE_HIBERNATE_SESSION);
	}

	public Map<String, Map<String, String>> getPartOptions() {
		return partOptions;
	}

	public void setPartOptions(Map<String, Map<String, String>> partOptions) {
		this.partOptions = partOptions;
	}

	class FillForm extends LoggedActionListener{

		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			configurations = DSPartsConfig.get().getLabeledConfigurationNames();
			parts = DSPartsConfig.get().getLabeledPartNames(configuration);
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public Map<String, String> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(Map<String, String> configurations) {
		this.configurations = configurations;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public Map<String, String> getParts() {
		return parts;
	}

	public void setParts(Map<String, String> parts) {
		this.parts = parts;
	}
}
