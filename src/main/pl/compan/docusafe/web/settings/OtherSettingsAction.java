package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.service.zebra.ZebraPrinter;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.LocaleUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import java.util.prefs.Preferences;
import pl.compan.docusafe.web.CssService;
import pl.compan.docusafe.web.archive.settings.ApplicationSettingsAction;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.filter.AuthFilter;
import pl.compan.docusafe.web.viewserver.ViewerAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/* User: Administrator, Date: 2006-03-08 16:41:27 */

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OtherSettingsAction.java,v 1.46 2010/04/26 12:22:07 pecet5 Exp $
 */
public class OtherSettingsAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(OtherSettingsAction.class);
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CollectiveAssignmentBaseAction.class.getPackage().getName(),null);
    private static final StringManager stringManager = StringManager.getManager(OtherSettingsAction.class.getPackage().getName());
    
    private boolean visibleSignature;
//    zmienna - pobierana z bazy, sprawdzana i zapisywana do bazy, w systemie nieu�ywana, IMHO niepotrzebna 
//    private Integer deadlineNotificationDaysInterval;
    private boolean viewserverWithData;
    private Integer viewserverDataSize;
    private boolean viewserverWholePage;
    private boolean viewserverDataSide;
    private boolean viewserverTiffyApplet;
    private boolean multiAssignmentObjective;
    private boolean multiDepartmentEnroll;
    private boolean remarksOnSeperatePage;
    private boolean rwaUpdateFromFile;
    private boolean layoutsAvailable;
    private String TAB_OTHER="other";
    private String TAB_CONTACT="contact";
    //dane kontaktowe:
    private String email;
    private String mobileNum;
    private String roomNum;
    
    private Map<String, String> layouts;
    private String layoutName;
    
    // @EXPORT
    private Map pages;
    private List<Tab> tabs = new ArrayList<Tab>(2);
    // @EXPORT/@IMPORT
    private String name;
    private List<String> local;
    private String lang;
    private String tab; 
    String guid;
    private Map<String, String> guids;
    private static StringManager smL =GlobalPreferences.loadPropertiesFile(OtherSettingsAction.class.getPackage().getName(),null);
    
	public String extension;
	public String communicatorNum;
	public String phoneNum;
	// liczba spraw w teczce
	private int caseCount;
    private List<Integer> caseCounts;
	private boolean simpledocumentmainin;
	private boolean simpledocumentmainout;
	private String selectedPrinter;
	private List<String> printers;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new Tabs()).
                append(new OtherSettingsAction.FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSave").
                append(OpenHibernateSession.INSTANCE).
                append(new OtherSettingsAction.Save()).
                append(new Tabs()).
                append(new SavePrinter()).
                append(new OtherSettingsAction.FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
		        append(OpenHibernateSession.INSTANCE).
		        append(new Tabs()).
                append(new SavePrinter()).
		        append(new OtherSettingsAction.SaveContact()).
		        append(new OtherSettingsAction.FillForm()).
		        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try{
            guid = TextUtils.trimmedStringOrNull(guid);
            guids = new LinkedHashMap<String, String>();
            guids.put(DSDivision.ROOT_GUID, stringManager.getString("Wspolny"));
            simpledocumentmainin = DSApi.context().userPreferences().node("other").getBoolean("simpledocumentmainin", false);
            simpledocumentmainout = DSApi.context().userPreferences().node("other").getBoolean("simpledocumentmainout", false);
            String tmp1 = DSApi.context().userPreferences().node("other").get("guid", guid);
            DSDivision [] tmp = null;
            if (guid == null)
            {
                    tmp = DSApi.context().getDSUser().getDivisions();
                if( tmp.length>0 )
                {
                    guid = tmp[0].getGuid();
                    if (!DSDivision.ROOT_GUID.equals(guid))
                            guids.put(guid, stringManager.getString(DSDivision.find(guid).getName()));
                }
            }
            if(tmp1 != null)
                guid = tmp1;
            else
                guid = DSDivision.ROOT_GUID;

                for (DSDivision div : DSApi.context().getDSUser().getDivisions())
                {
                    if (div.isPosition()) div = div.getParent();
                    if (!guids.containsKey(div.getGuid()))
                        guids.put(div.getGuid(), div.getName());
                }
            }catch(EdmException e)
            {
                // TODO Auto-generated catch block
            	LogFactory.getLog("eprint").debug("", e);
            }
            
            local = new ArrayList<String>();
            for (Locale l : LocaleUtil.AVAILABLE_LOCALES)
            {
                local.add(l.getLanguage());
            }
            lang = Docusafe.getCurrentLanguage();
            
            name = GlobalPreferences.getUserStartPage();
            layouts = new LinkedHashMap<String, String>();
            
            visibleSignature = DSApi.context().userPreferences().node("nationwide").getBoolean("visibleSignature", true);
//            deadlineNotificationDaysInterval = DSApi.context().userPreferences().node("other").getInt("deadlineNotificationDaysInterval", 1);
            if (Docusafe.hasExtra("business"))
            {
                viewserverWithData = DSApi.context().userPreferences().node("other").getBoolean("viewserverWithData", false);
                viewserverDataSize = DSApi.context().userPreferences().node("other").getInt("viewserverDataSize", ViewerAction.DEFAULT_DATA_SIZE);
                viewserverWholePage = DSApi.context().userPreferences().node("other").getBoolean("viewserverWholePage", true);
                viewserverDataSide = DSApi.context().userPreferences().node("other").getBoolean("viewserverDataSide", true);
                viewserverTiffyApplet = DSApi.context().userPreferences().node("other").getBoolean("viewserverTiffyApplet", true);
            }
            layoutsAvailable = CssService.layoutsAvailable;
            if (layoutsAvailable)
            {
                Map<String, Properties> layoutsT  = CssService.layouts;
                Set<String> set = layoutsT.keySet();
                for(String key : set)
                {
                    layouts.put(key, key);
                }
            }
            multiAssignmentObjective = DSApi.context().userPreferences().node("other").getBoolean("multiAssignmentObjective", false);
            multiDepartmentEnroll = DSApi.context().userPreferences().node("other").getBoolean("multiDepartmentEnroll", false);
            remarksOnSeperatePage = DSApi.context().userPreferences().node("other").getBoolean("remarksOnSeperatePage", true);
            rwaUpdateFromFile = DSApi.context().userPreferences().node("other").getBoolean("rwaUpdateFromFile", false);
            
            try
            {
                pages = new LinkedHashMap();
                pages.put("", sm.getString("StronaDomyslna"));
                if(AvailabilityManager.isAvailable("portletStartPage")) {
                	pages.put(GlobalPreferences.PORTLETS, sm.getString("portlets.startPageName"));
                }
                if (!Configuration.simpleOfficeAvailable() && DSApi.context().hasPermission(DSPermission.ARCHIWUM_DOSTEP))
                {
                    pages.put(GlobalPreferences.EXPLORE_DOCUMENTS, sm.getString("Archiwum"));
                }
                if (Configuration.officeAvailable() && AvailabilityManager.isAvailable("menu.left.kancelaria.listazadan"))
                {
                    pages.put(GlobalPreferences.TASKS, sm.getString("ListaZadan"));
                }
                if(AvailabilityManager.isAvailable("menu.left.wyszukiwarka"))
                {
                	pages.put(GlobalPreferences.FIND_OFFICE_DOCUMENTS, sm.getString("WyszukiwarkaPism"));
                }
                if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
                {
                	pages.put(GlobalPreferences.NEW_IN_OFFICE_DOCUMENT, sm.getString("PrzyjmijPismo"));
                }
                if (DSApi.context().hasPermission(DSPermission.BOK_LISTA_PISM))
                {
                	pages.put(GlobalPreferences.TASKS_BOK, sm.getString("ListaZadanBOK"));
                    //pages.put(GlobalPreferences.TASKS_BOK, "Lista zada� BOK");
                }
                if (AvailabilityManager.isAvailable("menu.left.wyszukiwarka")) {
                	pages.put(GlobalPreferences.SEARCH_DOCUMENTS_REDIRECT, sm.getString("WyszukiwanieDokumentow"));
                }
                if (AvailabilityManager.isAvailable("bookmarks.on")) 
                {
                	pages.put(GlobalPreferences.BOOKMARKS, sm.getString("ListyZadan"));
                }
                if(AvailabilityManager.isAvailable("news.on"))
                {
                	pages.put(GlobalPreferences.NEWS, sm.getString("News"));
                }
                if(AvailabilityManager.isAvailable("blank.page"))
                {
                	pages.put(GlobalPreferences.BLANK_PAGE, sm.getString("BLANK_PAGE"));
                }
                if(AvailabilityManager.isAvailable("p4HandWrittenSignature"))
                {
                	pages.put(GlobalPreferences.HAND_WRITTEN_SIGNATURE, sm.getString("ODBIOR_DOKUMENTOW"));
                }
                
                //lista mozliwych ilosci zadan wyswietlanych w teczce
                // widoczne tylko w przypadku gdy w pliku avaiable.properties teczki=true
                // w pliku adds.properties nale�y doda� mo�liwe ilo�ci :np: caseCounts=10,20,30,40,50
	            Docusafe.getAdditionProperty("caseCounts");
	            caseCounts = new ArrayList<Integer>();
	            for(String count : Docusafe.getAdditionProperty("caseCounts").split(","))
	            {
	            	caseCounts.add(Integer.parseInt(count));
	            }
	            caseCount = DSApi.context().userPreferences().node("other").getInt("case-count-per-page",50);
	            // dodanie do listy, wartosci uzytkownika jesli nie wystepuje w adds.properties
	            if (!caseCounts.contains(caseCount)) {
	            	caseCounts.add(caseCount);
	            	Collections.sort(caseCounts);
	            }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            
            
            //uzupelnianie danych pod zakladke kontaktow
            String username= ApplicationSettingsAction.getUsername();
            try {
	 			DSUser user = DSUser.findByUsername(username);
				email = user.getEmail();
				phoneNum = user.getPhoneNum();
				mobileNum = user.getMobileNum();
				extension = user.getExtension();
				communicatorNum = user.getCommunicatorNum();
				roomNum = user.getRoomNum();

			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			}

			printers = ZebraPrinter.definedUserPrinters();
			selectedPrinter = ZebraPrinter.getDefaultUserPrinterName();
        }
    }
    private class Tabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            if (!TAB_OTHER.equals(tab) && !TAB_CONTACT.equals(tab))
                tab = TAB_OTHER;

            tabs.add(new Tab(stringManager.getString("InneUstawienia"), stringManager.getString("InneUstawienia"),
                HttpUtils.makeUrl("/settings/other.action",
                    new String[] { "tab", TAB_OTHER }),
                TAB_OTHER.equals(tab)));

            tabs.add(new Tab(stringManager.getString("DaneKontaktowe"), stringManager.getString("DaneKontaktowe"),
                HttpUtils.makeUrl("/settings/other.action",
                    new String[] { "tab", TAB_CONTACT }),
                TAB_CONTACT.equals(tab)));

            event.setResult(tab);
        }
    }
    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                DSApi.context().userPreferences().node("nationwide").putBoolean("visibleSignature", visibleSignature);
//                if (deadlineNotificationDaysInterval == null || deadlineNotificationDaysInterval < 1)
//                    deadlineNotificationDaysInterval = 1;
//                DSApi.context().userPreferences().node("other").putInt("deadlineNotificationDaysInterval", deadlineNotificationDaysInterval);
                if (Docusafe.hasExtra("business"))
                {
                    DSApi.context().userPreferences().node("other").putBoolean("viewserverWithData", viewserverWithData);
                    if (viewserverDataSize == null)
                        viewserverDataSize = ViewerAction.DEFAULT_DATA_SIZE;
                    if (viewserverDataSize < 100)
                        viewserverDataSize = 100;
                    if (viewserverDataSize > 999)
                        viewserverDataSize = 999;
                    DSApi.context().userPreferences().node("other").putInt("viewserverDataSize", viewserverDataSize);
                    DSApi.context().userPreferences().node("other").putBoolean("viewserverWholePage", viewserverWholePage);
                    DSApi.context().userPreferences().node("other").putBoolean("viewserverDataSide", viewserverDataSide);
                    DSApi.context().userPreferences().node("other").putBoolean("viewserverTiffyApplet", viewserverTiffyApplet);
                }
                DSApi.context().userPreferences().node("other").putBoolean("multiAssignmentObjective", multiAssignmentObjective);
                DSApi.context().userPreferences().node("other").putBoolean("multiDepartmentEnroll", multiDepartmentEnroll);
                DSApi.context().userPreferences().node("other").putBoolean("remarksOnSeperatePage", remarksOnSeperatePage);
                DSApi.context().userPreferences().node("other").putBoolean("rwaUpdateFromFile", rwaUpdateFromFile);
                DSApi.context().userPreferences().node("other").put("guid", guid);
                DSApi.context().userPreferences().node("other").putInt("case-count-per-page", caseCount);
                DSApi.context().userPreferences().node("other").putBoolean("simpledocumentmainin", simpledocumentmainin);
                DSApi.context().userPreferences().node("other").putBoolean("simpledocumentmainout", simpledocumentmainout);
                Preferences prefs = Preferences.systemNodeForPackage(Docusafe.class);
                if (CssService.layoutsAvailable)
                    prefs.put(DSApi.context().getDSUser().getName(), layoutName);
                GlobalPreferences.setUserStartPage(name);
                
              ServletActionContext.getRequest().getSession().setAttribute(AuthFilter.LANGUAGE_KEY, lang);
                GlobalPreferences.setUserLocal(lang);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    private class SaveContact implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	final DSContext ctx = DSApi.open((Subject)
            	           ((HttpServletRequest) ServletActionContext.getRequest()).getSession(true).
            	           getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY));
            	String username= ApplicationSettingsAction.getUsername();
                ctx.begin();
                DSUser user = DSUser.findByUsername(username);
                if(!StringUtils.isEmpty(phoneNum))
                	user.setPhoneNum(phoneNum);
                else
                	user.setPhoneNum(null);
                
                if(!StringUtils.isEmpty(mobileNum))
                	user.setMobileNum(mobileNum);
                else
                	user.setMobileNum(null);
                
                if(!StringUtils.isEmpty(extension))
                	user.setExtension(extension);
                else
                	user.setExtension(null);
                
                if(!StringUtils.isEmpty(roomNum))
                	user.setRoomNum(roomNum);
                else
                	user.setRoomNum(null);
                	
                if(!StringUtils.isEmpty(communicatorNum))
                	user.setCommunicatorNum(communicatorNum);
                else
                	user.setCommunicatorNum(null);
                
                if(!StringUtils.isEmpty(email)){
                	if (!StringUtils.isEmpty(email) && !email.matches("^[-a-zA-Z0-9.+,!/_]+@[-a-zA-Z0-9_][-a-zA-Z0-9._]*$"))
                        throw new EdmException(sm.getString("updateUser.invalidEmail"));
                	user.setEmail(email);
                }
                else{
                	user.setEmail(null);
                }
                ctx.commit();
                tab = TAB_CONTACT;
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();

            }
        }
    }

	private class SavePrinter implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (selectedPrinter == null)
				{
					Preferences printerNode = ZebraPrinter.getPrintersNode();
					if (printerNode != null)
					{
						DSApi.context().begin();
						printerNode.removeNode();
						DSApi.context().commit();
					}
					return;
				}
				ZebraPrinter zb = ZebraPrinter.findByUserPrinterName(selectedPrinter);
				DSApi.context().begin();
				zb.saveSettings();
				DSApi.context().commit();
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			} catch (BackingStoreException e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

    public boolean isLayoutsAvailable()
    {
        return layoutsAvailable;
    }
    
    public void setLayoutsAvailable(boolean layoutsAvailable)
    {
        this.layoutsAvailable = layoutsAvailable;
    }
    
//    public Integer getDeadlineNotificationDaysInterval()
//    {
//        return deadlineNotificationDaysInterval;
//    }
//    
//    public void setDeadlineNotificationDaysInterval(Integer deadlineNotificationDaysInterval)
//    {
//        this.deadlineNotificationDaysInterval = deadlineNotificationDaysInterval;
//    }
    
    public boolean isViewserverWithData()
    {
        return viewserverWithData;
    }
    
    public void setViewserverWithData(boolean viewserverWithData)
    {
        this.viewserverWithData = viewserverWithData;
    }
    
    public Integer getViewserverDataSize()
    {
        return viewserverDataSize;
    }
    
    public void setViewserverDataSize(Integer viewserverDataSize)
    {
        this.viewserverDataSize = viewserverDataSize;
    }
    
    public boolean isViewserverWholePage()
    {
        return viewserverWholePage;
    }
    
    public void setViewserverWholePage(boolean viewserverWholePage)
    {
        this.viewserverWholePage = viewserverWholePage;
    }
    
    public boolean isMultiAssignmentObjective()
    {
        return multiAssignmentObjective;
    }
    
    public void setMultiAssignmentObjective(boolean multiAssignmentObjective)
    {
        this.multiAssignmentObjective = multiAssignmentObjective;
    }
    
    public void setMultiDepartmentEnroll(boolean multiDepartmentEnroll)
    {
        this.multiDepartmentEnroll = multiDepartmentEnroll;
    }

    public boolean isMultiDepartmentEnroll()
    {
        return multiDepartmentEnroll;
    }
    
    public void setRwaUpdateFromFile(boolean rwaUpdateFromFile)
    {
        this.rwaUpdateFromFile = rwaUpdateFromFile;
    }
    
    public boolean isRwaUpdateFromFile()
    {
        return rwaUpdateFromFile;
    }
    
    public void setRemarksOnSeperatePage(boolean remarksOnSeperatePage)
    {
        this.remarksOnSeperatePage = remarksOnSeperatePage;
    }
    
    public boolean isRemarksOnSeperatePage()
    {
        return remarksOnSeperatePage;
    }

    public boolean isVisibleSignature()
    {
        return visibleSignature;
    }
    
    public void setVisibleSignature(boolean visibleSignature)
    {
        this.visibleSignature = visibleSignature;
    }
    
    public String getLayoutName()
    {
        return layoutName;
    }
    
    public void setLayoutName(String layoutName)
    {
        this.layoutName = layoutName;
    }

    public Map getLayouts()
    {
        return layouts;
    }
    
    public Map getPages()
    {
        return pages;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List getLocal()
    {
        return local;
    }

    public void setLocal(List local)
    {

        this.local = local;
    }

    public String getLang()
    {
        return lang;
    }

    public void setLang(String lang)
    {
        this.lang = lang;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public Map<String, String> getGuids()
    {
        return guids;
    }

    public void setGuids(Map<String, String> guids)
    {
        this.guids = guids;
    }

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	public String getTab() {
		return tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getCommunicatorNum() {
		return communicatorNum;
	}

	public void setCommunicatorNum(String communicatorNum) {
		this.communicatorNum = communicatorNum;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public boolean getViewserverDataSide() {
		return viewserverDataSide;
	}

	public void setViewserverDataSide(boolean viewserverDataSide) {
		this.viewserverDataSide = viewserverDataSide;
	}

	public void setViewserverTiffyApplet(boolean viewserverTiffyApplet) {
		this.viewserverTiffyApplet = viewserverTiffyApplet;
	}

	public boolean isViewserverTiffyApplet() {
		return viewserverTiffyApplet;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public int getCaseCount() {
		return caseCount;
	}

	public void setCaseCount(int caseCount) {
		this.caseCount = caseCount;
	}

	public List<Integer> getCaseCounts() {
		return caseCounts;
	}

	public void setCaseCounts(List<Integer> caseCounts) {
		this.caseCounts = caseCounts;
	}

	public boolean isSimpledocumentmainin() {
		return simpledocumentmainin;
	}

	public void setSimpledocumentmainin(boolean simpledocumentmainin) {
		this.simpledocumentmainin = simpledocumentmainin;
	}

	public boolean isSimpledocumentmainout() {
		return simpledocumentmainout;
	}

	public void setSimpledocumentmainout(boolean simpledocumentmainout) {
		this.simpledocumentmainout = simpledocumentmainout;
	}

	public List<String> getPrinters()
	{
		return printers;
	}

	public void setPrinters(List<String> printers)
	{
		this.printers = printers;
	}

	public String getSelectedPrinter()
	{
		return selectedPrinter;
	}

	public void setSelectedPrinter(String selectedPrinter)
	{
		this.selectedPrinter = selectedPrinter;
	}
}
