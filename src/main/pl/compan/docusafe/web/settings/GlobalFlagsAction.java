package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.base.Flags;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-12 13:30:33 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: GlobalFlagsAction.java,v 1.4 2008/09/16 15:59:32 kubaw Exp $
 */
public class GlobalFlagsAction extends FlagsBaseAction
{
    public boolean isUserFlags()
    {
        return false;
    }

    public String getBaseLink()
    {
        return "/settings/global-flags.action";
    }
}
