package pl.compan.docusafe.web.settings;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.repository.SimpleFileSystemRepository;
import pl.compan.docusafe.spring.user.office.TemplateServiceImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.InputStream;
import java.util.List;

public class UserTemplatesAction extends RichEventActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(UserTemplatesAction.class);

    private FormFile file;

    @Override
    protected void setup() {

        hibernateListener(new UserTemplates());
        hibernateListener(new GlobalTemplates());
        hibernateListener(new TemplateFile());
        hibernateListener(new RemoveTemplate());
        hibernateListener(new Dockinds());
        hibernateListener(new UploadTemplate());

    }

    private class UserTemplates extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            String dockind = getParameterOrError("dockind");

            String username = DSApi.context().getDSUser().getName();

            final SimpleFileSystemRepository repo = RepositoryProvider.getUserTemplateRepository(username, dockind);

            final List<String> names = repo.listAsString();

            JsonArray ret = new JsonArray();

            for(String name: names) {
                JsonObject obj = new JsonObject();
                obj.addProperty("name", name);
                ret.add(obj);
            }

            return ret;
        }
    }

    private class GlobalTemplates extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            String dockind = getParameterOrError("dockind");

            JsonArray ret = new JsonArray();

            final SimpleFileSystemRepository repo = RepositoryProvider.getGlobalTemplateRepository(dockind);

            final List<String> names = repo.listAsString();

            for(String name: names) {
                JsonObject obj = new JsonObject();
                obj.addProperty("name", name);
                ret.add(obj);
            }

            return ret;
        }
    }

    private class TemplateFile extends FileActionListener {
        @Override
        public FileInfo fileAction(ActionEvent event) throws Exception {

            String filename = getParameterOrError("filename");
            String dockind = getParameterOrError("dockind");
            boolean asUserRepo = getParameter("asUserRepo") != null ? true : false;

            SimpleFileSystemRepository repo;
            if(asUserRepo) {
                final String name = DSApi.context().getDSUser().getName();
                repo = RepositoryProvider.getUserTemplateRepository(name, dockind);
            } else {
                repo = RepositoryProvider.getGlobalTemplateRepository(dockind);
            }

            InputStream stream = repo.get(filename);
            return new FileInfo("application/rtf", filename, stream);
        }
    }

    private class Dockinds extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            JsonArray arr = new JsonArray();

            final List<DocumentKind> list = DocumentKind.list();

            for(DocumentKind dockind: list) {

                JsonObject obj = new JsonObject();

                obj.addProperty("cn", dockind.getCn());
                obj.addProperty("name", dockind.getName());

                arr.add(obj);
            }

            return arr;
        }
    }

    private class RemoveTemplate extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            String name = getParameterOrError("name");
            String dockind = getParameterOrError("dockind");
            String username = DSApi.context().getDSUser().getName();

            getTemplateService().deleteUserTemplate(dockind, name, username);

            return null;
        }
    }

    private class UploadTemplate extends RichActionListener {
        @Override
        public void action(ActionEvent event) throws Exception {

            final String dockind = getParameterOrError("dockind");
            final String username = DSApi.context().getDSUser().getName();

            if(file == null) {
                event.addActionError("Brak pliku");
            }

            getTemplateService().addUserTemplate(file.getFile(), dockind, username);
        }
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

    private TemplateServiceImpl getTemplateService() {
        return Docusafe.getBean(TemplateServiceImpl.class);
    }
}
