package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.base.Flags;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-13 16:09:41 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditUserFlagAction.java,v 1.3 2008/09/16 15:59:32 kubaw Exp $
 */
public class EditUserFlagAction extends EditFlagBaseAction
{

    public String getBaseLink()
    {
        return "/settings/edit-user-flag.action";
    }

    public boolean isUserFlags()
    {
        return true;
    }
}
