package pl.compan.docusafe.web.settings;

import java.text.ParseException;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

public class SearchInvoicesAction extends EventActionSupport {
	
	private Map<String, String> userColumns;
	private Map<String, String> availableColumns;
	private Map<String, String> types;
	private String selectedColumns[];
	private String type;
	
	public static final String ALL_COLUMNS[] = { "invoice_id", "invoice_number", "invoice_year", "invoice_author", "invoice_invoiceDate", "invoice_paymentDate", "invoice_saleDate",
			"invoice_termasOfPayment", "invoice_kind", "invoice_gross", "invoice_net", "invoice_description", "invoice_contractNumber", "invoice_vendor", "invoice_acceptingUser",
			"invoice_acceptDate", "invoice_acceptReturnDate", "invoice_relayedTo", "invoice_budgetaryRegistryDate", "invoice_remarks", "invoice_ctime","invoice_cpv", "invoice_decretation" };

	public static final String DEFAULT_COLUMNS = (new JSONArray( Arrays.asList(new String[] { "invoice_acceptingUser", "invoice_description", "invoice_number", "invoice_gross", "invoice_vendor", "invoice_invoiceDate", "invoice_paymentDate", "invoice_remarks", "invoice_ctime", "invoice_cpv", "invoice_decretation" }))).toString();

	private static final List allColumns = Collections.unmodifiableList(Arrays.asList(ALL_COLUMNS));
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(pl.compan.docusafe.web.settings.SearchInvoicesAction.class.getPackage().getName(), null);

	public SearchInvoicesAction() {
	}

	protected void setup() 
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doReset").
			append(OpenHibernateSession.INSTANCE).
			append(new Reset()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doUpdate").
			append(OpenHibernateSession.INSTANCE).
			append(new Update()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			if (type == null)
				type = "columns";
			
			List<String> available = new ArrayList<String>(SearchInvoicesAction.allColumns);
			
			String adminColumnsDef = DSApi.context().systemPreferences().node("search-invoices").get(type, SearchInvoicesAction.DEFAULT_COLUMNS);
			String userColumnsDef = DSApi.context().userPreferences().node("search-invoices").get(type, adminColumnsDef);
			
			List<String> user = new ArrayList<String>(10);
			if (!StringUtils.isEmpty(userColumnsDef)) 
			{
				try 
				{
					JSONArray array = new JSONArray(userColumnsDef);
					for (int i = 0; i < array.length(); i++)
						user.add(array.getString(i));
				} 
				catch (ParseException e) 
				{
					user.clear();
				}
			}
			
			available.removeAll(user);
			userColumns = new LinkedHashMap<String, String>();
			availableColumns = new LinkedHashMap<String, String>();

			for (Iterator iter = user.iterator(); iter.hasNext(); )
			{
				String col = (String) iter.next();
				userColumns.put(col, SearchInvoicesAction.getColumnDescription(col));
			
			}
			
			for (Iterator iter = available.iterator(); iter.hasNext(); )
			{
				String col = (String) iter.next();
				availableColumns.put(col, SearchInvoicesAction.getColumnDescription(col));
			}
				
		}
	}

	private class Reset implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			try 
			{
				if (type == null)
					type = "columns";

				DSApi.context().begin();
				String tmp = DSApi.context().systemPreferences().node("search-invoices").get(type, SearchInvoicesAction.DEFAULT_COLUMNS);
				DSApi.context().userPreferences().node("search-invoices").put(type, tmp);
				DSApi.context().commit();
				addActionMessage(SearchInvoicesAction.sm.getString("PrzywroconoDomyslneKolumnyListyWynikow"));
			} 
			catch (EdmException e) 
			{
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}

	private class Update implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (type == null)
				type = "columns";
			if (selectedColumns == null || selectedColumns.length == 0)
				addActionError(SearchInvoicesAction.sm
						.getString("NieWybranoKolumnDoListyWynikowPrzywroconoStandardowyZestawKolumn"));
			try {
				DSApi.context().begin();
				String columnsDef;
				if (selectedColumns == null || selectedColumns.length == 0)
					columnsDef = SearchInvoicesAction.DEFAULT_COLUMNS;
				else
					columnsDef = (new JSONArray(Arrays.asList(selectedColumns)))
							.toString();
				DSApi.context().userPreferences().node("search-invoices")
						.put(type, columnsDef);
				DSApi.context().commit();
			} catch (EdmException e) {
				DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
			}
		}
	}


	public static String getColumnDescription(String column) {
		return sm.getString((new StringBuilder())
				.append("search.invoices.column.").append(column).toString());
	}

	public Map<String, String> getAvailableColumns() {
		return availableColumns;
	}

	public Map<String, String> getUserColumns() {
		return userColumns;
	}

	public void setSelectedColumns(String selectedColumns[]) {
		this.selectedColumns = selectedColumns;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getTypes() {
		return types;
	}

}
