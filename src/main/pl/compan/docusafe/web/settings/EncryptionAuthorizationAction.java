package pl.compan.docusafe.web.settings;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crypto.DocusafeKeyStore;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class EncryptionAuthorizationAction extends EventActionSupport 
{

	private StringManager sm = StringManager.getManager(EncryptionAuthorizationAction.class.getPackage().getName());
	private static final long serialVersionUID = 1L;
	private Collection<DocusafeKeyStore> authorizations;
	private Collection<DocusafeKeyStore> acquiredPasswords;
	private String password;
	private String confirmation;
	private String codeName;
	
	Logger log = LoggerFactory.getLogger(EncryptionAuthorizationAction.class);

	protected void setup() 
	{
		 registerListener(DEFAULT_ACTION).
	         append(OpenHibernateSession.INSTANCE).
	         append(new FillForm()).
	         appendFinally(CloseHibernateSession.INSTANCE);
	
	     registerListener("doAuthorize").
	         append(OpenHibernateSession.INSTANCE).
	         append(new Authorize()).
	         append(new FillForm()).
	         appendFinally(CloseHibernateSession.INSTANCE);
	     
	     registerListener("doRequestAccess").
	         append(OpenHibernateSession.INSTANCE).
	         append(new ProcessRequest()).
	         append(new FillForm()).
	         appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	acquiredPasswords = DSApi.context().credentials().getAcquiredCodes();
        	authorizations = DSApi.context().credentials().getAuthorizedCodes();
        	if((codeName == null || codeName.length() < 1) && Docusafe.getAdditionProperty("AuthorizationDefaultCodeName") != null)
        		codeName = Docusafe.getAdditionProperty("AuthorizationDefaultCodeName");
        }
    }
	
	private class Authorize implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	        	DSApi.context().credentials().authorize(password, codeName);
	        	addActionMessage(sm.getString("AutoryzacjaDlaKoduPowiodlaSie", codeName));
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        		log.error(e.getMessage(),e);
        	}
        }
    }
	
	private class ProcessRequest implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if(password==null || confirmation==null || !password.equals(confirmation))
        		{
        			throw new EdmException(sm.getString("WprowadzoneHaslaNieSaZgodne"));
        		}
        		if(password.length()<6)
        		{
        			throw new EdmException(sm.getString("WporwadzoneHasloJestZaKrotkie"));
        		}
	        	if(codeName == null)
	        	{
	        		throw new EdmException(sm.getString("WprowadzNazweKodowa"));
	        	}
	        	DSApi.context().credentials().requestPassword(password, codeName);
	        	DSApi.context().credentials().init();
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public Collection<DocusafeKeyStore> getAuthorizations() {
		return authorizations;
	}

	public Collection<DocusafeKeyStore> getAcquiredPasswords() {
		return acquiredPasswords;
	}

	

}
