package pl.compan.docusafe.web.settings;

import java.util.List;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NewLabelAction  extends EventActionSupport
{
	static final long serialVersionUID = -1L;
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(NewLabelAction.class.getPackage().getName(),null);
    
    
    private List<Label> availableLabels;
    private List<Label> availableModifiableLabels;
    private Long id;
	private String name;
	private String cn;
	private String description;
	private Long parentId;
	private String owner;
	private Integer inactionTime;
	private Boolean hidesTasks;
	private Long ancestorId;
	private Boolean deletesAfter;
	private Integer saveResult;
	
	private Boolean showEditName=false;


    protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		availableLabels = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
        		availableModifiableLabels = LabelsManager.getWriteLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
        		if(id != null && !id.equals(0L))
        		{
        			Label label = LabelsManager.findLabelById(id);
        			name = label.getName();
                    cn = label.getCn();
                    description = label.getDescription();
        			parentId = label.getParentId();
        			inactionTime = label.getInactionTime()!=null?label.getInactionTime()/24:null;
        			hidesTasks = label.getHidden();
        			ancestorId = label.getAncestorId();
        			deletesAfter = label.getDeletesAfter();
        			
        		}
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }
    
    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(owner==null||owner.equals(""))
        		owner = DSApi.context().getPrincipalName();
        	try
        	{
        		if(hidesTasks==null) hidesTasks = false;
        		if(deletesAfter==null)deletesAfter = false;
        		DSApi.context().begin();
        		Label label;
        		if(id==null)
        		{
        			if(LabelsManager.existsUserLabel(name, DSApi.context().getPrincipalName(), Label.LABEL_TYPE)){
        				addActionMessage(sm.getString("EtykietaOTakiejNazwieJuzIstnieje"));
        				return;
        			}
        			label = new Label();
        		}
        		else label = LabelsManager.findLabelById(id);
        		
        		
        		if(AvailabilityManager.isAvailable("etykietyOsobiste.ZmianaNazwyEtykiety")){
        			if(name==null||name.equals("")){
        				addActionMessage(sm.getString("EtykietaNiePodanoNazwy"));
        				return;
        			}
        			if(!(id!=null && Label.find(id).getName().equals(name)) && LabelsManager.existsUserLabel(name, DSApi.context().getPrincipalName(), Label.LABEL_TYPE)){
        				addActionMessage(sm.getString("EtykietaOTakiejNazwieJuzIstnieje"));
        				return;
        			}
        			
        		}
        		
        		label.setAncestorId(ancestorId);
        		label.setDescription(description);
        		label.setHidden(hidesTasks);
        		if(inactionTime==null) label.setInactionTime(null);
        		else label.setInactionTime(inactionTime*24);
        		label.setName(name);
                label.setCn(cn);
        		label.setParentId(parentId);
        		label.setModifiable(true);
        		label.setDeletesAfter(deletesAfter);
        		label.setType(Label.LABEL_TYPE);
        		label.setModifyRights(DSApi.context().getPrincipalName());
        		label.setReadRights(DSApi.context().getPrincipalName());
        		label.setWriteRights(DSApi.context().getPrincipalName());
        		if(id==null)
        			Persister.create(label);

        		LabelsManager.updateBeansForLabel(label,true);

        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(sm.getString("NieUdaloSieDodacEtykiety"));
        	}
        	finally
        	{
        		
        	}        	
        	saveResult = (id == null) ? ViewLabelsAction.LABEL_RESULT_NEW : ViewLabelsAction.LABEL_RESULT_UPDATE;
        	event.setResult("viewLabelsDone");
        }
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getInactionTime() {
		return inactionTime;
	}

	public void setInactionTime(Integer inactionTime) {
		this.inactionTime = inactionTime;
	}

	public Boolean getHidesTasks() {
		return hidesTasks;
	}

	public void setHidesTasks(Boolean hidesTasks) {
		this.hidesTasks = hidesTasks;
		if(hidesTasks==null) hidesTasks = false;
	}

	public Long getAncestorId() {
		return ancestorId;
	}

	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Label> getAvailableModifiableLabels() {
		return availableModifiableLabels;
	}

	public void setAvailableModifiableLabels(List<Label> availableModifiableLabels) {
		this.availableModifiableLabels = availableModifiableLabels;
	}

	public Boolean getDeletesAfter() {
		return deletesAfter;
	}

	public void setDeletesAfter(Boolean deletesAfter) {
		this.deletesAfter = deletesAfter;
	}

	public Integer getSaveResult() {
		return saveResult;
	}

	public Boolean getShowEditName() {
		return (id==null)||(AvailabilityManager.isAvailable("etykietyOsobiste.ZmianaNazwyEtykiety"));
	}

	public void setShowEditName(Boolean showEditName) {
		this.showEditName = (id==null)||(AvailabilityManager.isAvailable("etykietyOsobiste.ZmianaNazwyEtykiety"));
	}



    
    
}