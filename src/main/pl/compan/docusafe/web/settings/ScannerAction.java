package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ScannerAction.java,v 1.4 2005/08/23 14:56:31 lk Exp $
 */
public class ScannerAction extends EventActionSupport
{
    private boolean useScanner;
    /**
     * Okre�la, czy applet ma korzysta� z w�asnej wersji biblioteki JTwain
     * (false), czy spodziewa� si� jej w systemowej �cie�ce.
     */
    private boolean useSystemJTwain;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new Save()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
//            try
//            {
            useScanner = DSApi.context().userPreferences().node("scanner").getBoolean("use", false);
            useSystemJTwain = DSApi.context().userPreferences().node("scanner").getBoolean("useSystemJTwain", false);
                    //Boolean.valueOf(DSApi.context().getUserProperty(Properties.USE_SCANNER, Boolean.FALSE.toString())).booleanValue();
//            }
//            catch (EdmException e)
//            {
//                addActionError(e.getMessage());
//            }
        }
    }

    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                DSApi.context().userPreferences().node("scanner").putBoolean("use", useScanner);
                DSApi.context().userPreferences().node("scanner").putBoolean("useSystemJTwain", useSystemJTwain);
                //DSApi.context().setUserProperty(Properties.USE_SCANNER, String.valueOf(useScanner));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public boolean isUseScanner()
    {
        return useScanner;
    }

    public void setUseScanner(boolean useScanner)
    {
        this.useScanner = useScanner;
    }

    public boolean isUseSystemJTwain()
    {
        return useSystemJTwain;
    }

    public void setUseSystemJTwain(boolean useSystemJTwain)
    {
        this.useSystemJTwain = useSystemJTwain;
    }
}
