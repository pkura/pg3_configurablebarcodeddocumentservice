package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.base.Flags;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-12 14:11:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserFlagsAction.java,v 1.5 2008/09/16 15:59:33 kubaw Exp $
 */
public class UserFlagsAction extends FlagsBaseAction
{
    
    public boolean isUserFlags()
    {
        return true;
    }

    public String getBaseLink()
    {
        return "/settings/user-flags.action";
    }
}
