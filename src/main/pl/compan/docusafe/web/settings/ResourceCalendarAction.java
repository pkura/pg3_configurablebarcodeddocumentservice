package pl.compan.docusafe.web.settings;

import java.util.Date;
import java.util.List;


import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.AnyResource;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ResourceCalendarAction extends EventActionSupport {
	
	private static final Logger log = LoggerFactory.getLogger(ResourceCalendarAction.class); 
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(ResourceCalendarAction.class.getPackage().getName(), null);
	
	private List<AnyResource> resources;
	private Long resourceId;
	private String cn;
	private String name;
	
	private boolean czyUsuwac=true;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).	        
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).	        
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).	        
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAvaibility").
        	append(OpenHibernateSession.INSTANCE).	        
        	append(new ChangeAvaibility()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
    }
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	try 
        	{	
        		czyUsuwac=!AvailabilityManager.isAvailable("kalendarz.ZasobyNiewidoczneANieUsuniete");
        		if(AvailabilityManager.isAvailable("kalendarz.ZasobyNiewidoczneANieUsuniete"))resources=AnyResource.listAll();
        		else
        			resources  = AnyResource.list();
        	//	if(resourceId == null && resources != null && resources.size() > 0)
        	//		resourceId = resources.get(0).getId();
        		if(resourceId != null)
        		{
	        		AnyResource res = DSApi.context().load(AnyResource.class,resourceId);
	        		cn = res.getCn();
	        		name = res.getName();
        		}
			}
        	catch (Exception e) 
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
			}
        }
    }
	
	private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	try
        	{
        		DSApi.context().begin();
        		if(resourceId == null)
        			throw new EdmException("Nie wybrano Zasobu");

        		AnyResource res = DSApi.context().load(AnyResource.class,resourceId);
        		res.delete();
        		resourceId = null;
        		DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	try
        	{
        		
        		DSApi.context().begin();
        		if(resourceId == null)
        			throw new EdmException("Nie wybrano Zasobu");
        		AnyResource res = DSApi.context().load(AnyResource.class,resourceId);
        		res.setCn(cn);
        		res.setName(name);
        		DSApi.context().commit();
        		resourceId = null;
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }
	
	private class Add implements ActionListener
    {	
		private class ResourcesCNAlreadyExists extends Exception{
			
		}
		private class ResourcesNameAlreadyExists extends Exception{
			
		}
		public void actionPerformed(ActionEvent event)
        {   
        	try
        	{
        		DSApi.context().begin();
        		AnyResource res = new AnyResource();
        		res.setCn(cn);
        		if(AnyResource.findByCn(cn)!=null&&AvailabilityManager.isAvailable("kalendarz.unikalneZasoby")){
       			 	throw new ResourcesCNAlreadyExists();
        		}
        		res.setName(name);
        		if(AnyResource.findByName(name)!=null&&AvailabilityManager.isAvailable("kalendarz.unikalneZasoby")){
       			 	throw new ResourcesNameAlreadyExists();
        		}
        		Calendar cal = new Calendar();
        		cal.setName(name);
        		cal.setOwner(res.getCn());
        		cal.setType(Calendar.ANY_CALENDAR);
        		cal.create();
        		res.setCalendarId(cal.getId());
        		
        		if(AvailabilityManager.isAvailable("kalendarz.ZasobyNiewidoczneANieUsuniete"))res.setAvailable(true);
        		res.create();
//        		for (DSUser user : DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME))
//        		{
        			UserCalendarBinder ucb = new UserCalendarBinder();
    	        	ucb.setPermissionType(CalendarFactory.PERMISSION_SEE_ONLY);
    	        	ucb.setCalendarId(cal.getId());
    	        	ucb.setShow(true);
    	        	ucb.setUsername(DSApi.context().getPrincipalName());
    	        	ucb.create();
//				}
        		DSApi.context().commit();
        	}
        	catch(ResourcesCNAlreadyExists e){
        		log.error(sm.getString("zasobCNJuzIstnieje"),e);
        		addActionError(sm.getString("zasobCNJuzIstnieje"));
        	}
        	catch(ResourcesNameAlreadyExists e){
        		log.error(sm.getString("zasobNazwaJuzIstnieje"),e);
        		addActionError(sm.getString("zasobNazwaJuzIstnieje"));
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        	
        }
    }
	private class ChangeAvaibility implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {   
        	try
        	{
        		DSApi.context().begin();
        		if(resourceId == null)
        			throw new EdmException("Nie wybrano Zasobu");

        		AnyResource res = DSApi.context().load(AnyResource.class,resourceId);
        		res.setAvailable(!res.isAvailable());
        		resourceId = null;
        		DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public List<AnyResource> getResources() {
		return resources;
	}

	public void setResources(List<AnyResource> resources) {
		this.resources = resources;
	}

	public boolean isCzyUsuwac() {
		return czyUsuwac;
	}

	public void setCzyUsuwac(boolean czyUsuwac) {
		this.czyUsuwac = czyUsuwac;
	}
}
