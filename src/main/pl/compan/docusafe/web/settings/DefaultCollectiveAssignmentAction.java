package pl.compan.docusafe.web.settings;

import java.util.prefs.Preferences;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
/* User: Administrator, Date: 2005-12-05 14:58:16 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DefaultCollectiveAssignmentAction.java,v 1.6 2007/11/27 12:11:39 mariuszk Exp $
 */
public class DefaultCollectiveAssignmentAction extends CollectiveAssignmentBaseAction
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CollectiveAssignmentBaseAction.class.getPackage().getName(),null);
    
    protected Preferences getPreferences()
    {
        return DSApi.context().systemPreferences().node("modules/office/collectiveassignment");
    }

    public String getBaseLink()
    {
        return "/settings/default-collective-assignment.action";
    }

    public String getSubtitle()
    {
        return sm.getString("ustawieniaDomyslne");
    }

    public boolean canUpdate()
    {
        return DSApi.context().isAdmin();
    }

    public boolean isUserPref()
    {
        return false;
    }
}
