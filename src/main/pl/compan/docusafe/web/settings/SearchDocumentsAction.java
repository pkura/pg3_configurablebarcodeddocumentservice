package pl.compan.docusafe.web.settings;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.SetResultListener;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchDocumentsAction.java,v 1.12 2009/10/12 11:53:55 tomekl Exp $
 */
public class SearchDocumentsAction extends EventActionSupport
{
    private static final List allColumns =
        Collections.unmodifiableList(Arrays.asList(pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.ALL_COLUMNS));

    private /*final static*/ StringManager sm =
        GlobalPreferences.loadPropertiesFile(SearchDocumentsAction.class.getPackage().getName(),null);
    
    // @EXPORT
    private Map<String,String> userColumns;
    private Map<String,String> availableColumns;
    private Map<String,String> types;
    private String tab;
    
    // @IMPORT
    private String[] selectedColumns;
    
    // @IMPORT/@EXPORT
    private String type;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("user-settings"));

        registerListener("doReset").
            append(OpenHibernateSession.INSTANCE).
            append(new Reset()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
  
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (type == null)
                    type = "columns";
                boolean common = "columns".equals(type);
                types = new LinkedHashMap<String, String>();
                types.put("columns", sm.getString("wspolneKolumny"));
                                                                     
                List<DocumentKind> documentKinds = DocumentKindProvider.get().visible().list();
                for (DocumentKind documentKind : documentKinds)
                    types.put("columns_"+documentKind.getCn(), sm.getString("specyficzneKolumnyDla")+" '"+documentKind.getName()+"'");
                
                DocumentKind documentKind = type.length() > 8 ? DocumentKind.findByCn(type.substring("columns_".length())) : null;
                
                String adminColumnsDef = DSApi.context().systemPreferences().node("search-documents").get(type,
                        common ? pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.DEFAULT_COLUMNS :
                            DocumentKindsManager.getAllDockindColumnsString(documentKind));
                String userColumnsDef =
                    DSApi.context().userPreferences().node("search-documents").get(type,adminColumnsDef);
                
                String complexColumnsDef = "";
                
                
                List<String> user = new ArrayList<String>(10);

                if (!StringUtils.isEmpty(userColumnsDef))
                {
                    try
                    {
                        JSONArray array = new JSONArray(userColumnsDef);
                        for (int i=0; i < array.length(); i++)
                        {
                            user.add(array.getString(i));
                        }
                    }
                    catch (ParseException e)
                    {
                        user.clear();
                    }
                }
                
                
                
                
                if (common)
                {
                    List<String> available = new ArrayList<String>(allColumns);
                    available.removeAll(user);
    
                    if(!AvailabilityManager.isAvailable("wyszukiwarka.current_workflow_location") && available.contains("current_workflow_location"))
                    {
                    	available.remove("current_workflow_location");
                    }
                    
                    
                    userColumns = new LinkedHashMap<String, String>();
                    availableColumns = new LinkedHashMap<String, String>();
    
                    for (Iterator iter=user.iterator(); iter.hasNext(); )
                    {
                        String col = (String) iter.next();
                        userColumns.put(col, pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.getColumnDescription(col));
                    }
    
                    for (Iterator iter=available.iterator(); iter.hasNext(); )
                    {
                        String col = (String) iter.next();
                        availableColumns.put(col, pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.getColumnDescription(col));
                    }
                }
                else
                {
                    List<String> available = new ArrayList<String>(DocumentKindsManager.getAllDockindColumns(documentKind));
                    JSONArray array = new JSONArray(DSApi.context().systemPreferences().node("complex-columns").get(type,"[]"));
                    for (int i=0; i < array.length(); i++)
                    {
                    	available.add("#complex#"+array.getString(i));
                    }
                    available.removeAll(user);
                    userColumns = DocumentKindsManager.getDockindColumns(documentKind, user);
                    availableColumns = DocumentKindsManager.getDockindColumns(documentKind, available);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            catch(ParseException e)
            {
            	addActionError(e.getMessage());
            }
        }
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if (type == null)
                type = "columns";
            if (selectedColumns == null || selectedColumns.length == 0)
                addActionError(sm.getString("NieWybranoKolumnDoListyWynikowPrzywroconoStandardowyZestawKolumn"));
            
            try
            {
                DSApi.context().begin();

                boolean common = "columns".equals(type);
                DocumentKind documentKind = type.length() > 8 ? DocumentKind.findByCn(type.substring("columns_".length())) : null;
                
                String columnsDef;
                if (selectedColumns == null || selectedColumns.length == 0)
                {
                    columnsDef = common ? pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.DEFAULT_COLUMNS :
                        DocumentKindsManager.getAllDockindColumnsString(documentKind);
                }
                else
                {
                    columnsDef = new JSONArray(Arrays.asList(selectedColumns)).toString();
                }
                DSApi.context().userPreferences().node("search-documents").put(type, columnsDef);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Przywracanie domy�lnej listy kolumn.
     */
    private class Reset implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                boolean common = "columns".equals(type);
                DocumentKind documentKind = type.length() > 8 ? DocumentKind.findByCn(type.substring("columns_".length())) : null;
                
                String tmp = DSApi.context().systemPreferences().node("search-documents").get(type, 
                		common ? pl.compan.docusafe.web.archive.repository.search.SearchDocumentsAction.DEFAULT_COLUMNS :
                    DocumentKindsManager.getAllDockindColumnsString(documentKind));
                
                DSApi.context().userPreferences().node("search-documents").put(type,tmp);

                DSApi.context().commit();
                addActionMessage(sm.getString("PrzywroconoDomyslneKolumnyListyWynikow"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Map<String,String> getAvailableColumns()
    {
        return availableColumns;
    }

    public Map<String,String> getUserColumns()
    {
        return userColumns;
    }

    public void setSelectedColumns(String[] selectedColumns)
    {
        this.selectedColumns = selectedColumns;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, String> getTypes()
    {
        return types;
    }
}
