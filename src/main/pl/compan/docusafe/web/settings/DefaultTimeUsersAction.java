package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-05 14:58:16 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DefaultCollectiveAssignmentAction.java,v 1.3 2005/12/08 16:57:39 lk Exp $
 */
public class DefaultTimeUsersAction extends TimeUsersBaseAction_
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CollectiveAssignmentBaseAction.class.getPackage().getName(),null);
    
    protected Preferences getPreferences()
    {
        return DSApi.context().systemPreferences().node("modules/office/calendar/timeusers");
    }

    public String getBaseLink()
    {
        return "/settings/default-timeusers.action";
    }

    public String getSubtitle()
    {
        return sm.getString("ustawieniaDomyslne");
    }

    public boolean canUpdate()
    {
        return DSApi.context().isAdmin();
    }
}
