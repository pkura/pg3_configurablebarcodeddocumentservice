package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * User: Michal Manski
 * Date: 2006-05-10
 */
public class SignatureAction extends EventActionSupport
{
    private boolean useSignature;
    private boolean useSignatureDebug;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new Save()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            useSignature = DSApi.context().userPreferences().node("signature").getBoolean("use", false);
            useSignatureDebug = DSApi.context().userPreferences().node("signature").getBoolean("useSignatureDebug", false);
            
        }
    }

    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                DSApi.context().userPreferences().node("signature").putBoolean("use", useSignature);
                DSApi.context().userPreferences().node("signature").putBoolean("useSignatureDebug", useSignatureDebug);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public boolean isUseSignature()
    {
        return useSignature;
    }

    public void setUseSignature(boolean useSignature)
    {
        this.useSignature = useSignature;
    }
    
    
    public boolean isUseSignatureDebug()
    {
        return useSignatureDebug;
    }
    
    public void setUseSignatureDebug(boolean useSignatureDebug)
    {
        this.useSignatureDebug = useSignatureDebug;
    }
    
}
