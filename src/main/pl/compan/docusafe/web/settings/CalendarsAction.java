package pl.compan.docusafe.web.settings;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bouncycastle.crypto.DSA;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.AnyResource;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class CalendarsAction extends EventActionSupport {

	private static final Logger log = LoggerFactory.getLogger(CalendarsAction.class);

	private List<UserCalendarBinder> calendars;
	private List<UserCalendarBinder> shareCalendars;
	private List<Calendar> myCalendars;
	private List<DSUser> users;
	private String[] selectUsers;
	private Integer perm;
	private Long calendarId;
	private Calendar calendar;
	private String username;
	private String resourceType;
	private boolean show;
    private String calendarName;
	private String calendarOwner;
    private List<AnyResource> resources;
    private Long[] selectResources;
    private Long myCalendarId;
    private String[] calendarsIds;
    private String principalName;
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doShare").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Share()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete(true)).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDeleteMy").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete(false)).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAddResources").
	        append(OpenHibernateSession.INSTANCE).
	        append(new AddResources()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdatePosn").
	        append(OpenHibernateSession.INSTANCE).
	        append(new UpdatePosn()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    
        

    }

	private String prettyPermision(Integer type)
	{
		return ""+type;
	}
	

	private class UpdatePosn implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		if(calendarsIds == null || calendarsIds.length < 1)
        			throw new EdmException("Nie zmieniono kolejno�ci");
        		
        		for (int i = 0; i < calendarsIds.length; i++) 
        		{
        			Long calId = Long.parseLong(calendarsIds[i]); 
					UserCalendarBinder ucb = UserCalendarBinder.find(calId, DSApi.context().getPrincipalName());
					ucb.setPosn(i+1);
				}
        		DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }
	
	private class FillForm implements ActionListener
    {

		public void actionPerformed(ActionEvent event)
        {
        	//Logger kl = LoggerFactory.getLogger("kamil");
        	try
        	{		
        		principalName  = DSApi.context().getPrincipalName();
        		calendars = UserCalendarBinder.getUserCalendars(DSApi.context().getPrincipalName(), null);
        		UserCalendarBinder.initCalendars(calendars);
        		users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

        		myCalendars = Calendar.getMyCalendar();
        		// jesli nie podano id kalendarza do wczytanie, to wczytujemy pierwszy
        		if(calendarId == null && myCalendars != null && !myCalendars.isEmpty())
        			calendarId = myCalendars.get(0).getId();
        		if(calendarId != null)
        		{
        			calendar = DSApi.context().load(Calendar.class, calendarId);
        		}
        		if(myCalendarId == null)
        		{
        			myCalendarId = Calendar.getDefaultCalendar().getId();
        		}
        		shareCalendars = UserCalendarBinder.getShares(myCalendarId);
        		UserCalendarBinder.initUsers(shareCalendars);
        		
        		Set<Long> calendarsId = new HashSet<Long>();
        		for (UserCalendarBinder ucb : calendars) 
        		{
					calendarsId.add(ucb.getCalendarId());
				}
        		resources = AnyResource.list();
        		for (AnyResource res : new ArrayList<AnyResource>(resources)) 
        		{
        			if(calendarsId.contains(res.getCalendarId()))
        				resources.remove(res);
				}
			}
        	catch (EdmException e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
			}
        }
    }
	
	private class AddResources implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		if(selectResources == null || selectResources.length < 1)
        			throw new EdmException("Nie wybrano zasob�w");
        	
        		for (Long id : selectResources) 
        		{
    	        	UserCalendarBinder ucb = new UserCalendarBinder();
    	        	ucb.setPermissionType(CalendarFactory.PERMISSION_EDIT);
    	        	ucb.setCalendarId(id);
    	        	ucb.setShow(true);
    	        	ucb.setUsername(DSApi.context().getPrincipalName());
    	        	ucb.create();
				}
        		DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	private class Share implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        	try
        	{
        		DSApi.context().begin();
            	if(selectUsers == null || selectUsers.length < 1)
            		throw new EdmException("Nie wybrano �adnego u�ytkownika");
            	for (String name : selectUsers)
            	{
            		UserCalendarBinder ucb = new UserCalendarBinder();
    	        	ucb.setPermissionType(perm);
    	        	ucb.setCalendarId(myCalendarId);
    	        	ucb.setShow(true);
    	        	ucb.setUsername(name);
    	        	ucb.setColorId(0);
    	        	ucb.create();
				}
	        	
	        	DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	private class Delete implements ActionListener
    {
		private Boolean my = false;
		
		public Delete(Boolean my)
		{
			this.my = my;
		}
        public void actionPerformed(ActionEvent event)
        { 
        	try
        	{
        		Long  id;
        		DSApi.context().begin();
        		if(my)
        			id = myCalendarId;
        		else
        			id = calendarId;
        		UserCalendarBinder ucb = UserCalendarBinder.find(id,username);
        		if(ucb.getCalendar().getOwner().equals(DSApi.context().getPrincipalName()) || 
        				ucb.getUsername().equals(DSApi.context().getPrincipalName()))
        		{
        			ucb.delete();
        		}
        		else
        			addActionError("Nie mo�na anulowa� subskrypcji do w�asnego kalendarza");
	        	DSApi.context().commit();
        	}
        	catch (Exception e)
        	{
        		log.error("Wyst�pi� nieznany b��d podczas usuwania",e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if(!DSApi.context().getDSUser().getName().equals(username))
        			throw new EdmException("Nie masz uprawnie� do edycji"); 
        		
        		
        		
        		log.info("Id's kalendarza: " + calendarId + " username: " + username);
        		DSApi.context().begin();
        		
        		Integer count = null;

        		PreparedStatement ps = DSApi.context().prepareStatement("select count(cal.id) from ds_calendar cal,ds_user_calendar uc where cal.id = uc.calendarid and owner = ? and show = 1 and username = ?");
                ps.setString(1, username);
                ps.setString(2, username);
                ResultSet rs = ps.executeQuery();
                    
                if(rs.next())
                {
                	count = rs.getInt(1);
                }
                
                if(count > 0 || (count == 1 && show == true))
                {
                	UserCalendarBinder ucb = UserCalendarBinder.find(calendarId,username);
            		ucb.setShow(show);
            		DSApi.context().commit();
                }
                else
                {
                	addActionMessage("Co najmniej jeden kalendarz musi by� aktywny");
                }
        		
        	}
        	catch (Exception e) 
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	private class Add implements ActionListener
    {
		public void actionPerformed(ActionEvent event)
        {
        	try
        	{

        		DSApi.context().begin();
        		Calendar cal = new Calendar();
        		cal.setName(calendarName);
        		cal.setOwner((calendarOwner != null ? calendarOwner : DSApi.context().getPrincipalName()));
        		cal.setType(Calendar.USER_CALENDAR);
        		cal.create();
        		DSApi.context().commit();
        		calendarId = cal.getId();
        	}
        	catch (Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError("B��d "+e.getMessage());
        		DSApi.context()._rollback();
			}
        }
    }

	public List<UserCalendarBinder> getCalendars() {
		return calendars;
	}

	public void setCalendars(List<UserCalendarBinder> calendars) {
		this.calendars = calendars;
	}

	public Integer getPerm() {
		return perm;
	}

	public void setPerm(Integer perm) {
		this.perm = perm;
	}

	public List<UserCalendarBinder> getShareCalendars() {
		return shareCalendars;
	}

	public void setShareCalendars(List<UserCalendarBinder> shareCalendars) {
		this.shareCalendars = shareCalendars;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public Long getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(Long calendarId) {
		this.calendarId = calendarId;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getCalendarOwner() {
		return calendarOwner;
	}

	public void setCalendarOwner(String calendarOwner) {
		this.calendarOwner = calendarOwner;
	}

	public List<Calendar> getMyCalendars() {
		return myCalendars;
	}

	public void setMyCalendars(List<Calendar> myCalendars) {
		this.myCalendars = myCalendars;
	}

	public List<AnyResource> getResources() {
		return resources;
	}

	public void setResources(List<AnyResource> resources) {
		this.resources = resources;
	}

	public Long[] getSelectResources() {
		return selectResources;
	}

	public void setSelectResources(Long[] selectResources) {
		this.selectResources = selectResources;
	}

	public Long getMyCalendarId() {
		return myCalendarId;
	}

	public void setMyCalendarId(Long myCalendarId) {
		this.myCalendarId = myCalendarId;
	}

	public String[] getSelectUsers() {
		return selectUsers;
	}

	public void setSelectUsers(String[] selectUsers) {
		this.selectUsers = selectUsers;
	}

	public String[] getCalendarsIds() {
		return calendarsIds;
	}

	public void setCalendarsIds(String[] calendarsIds) {
		this.calendarsIds = calendarsIds;
	}

	public String getPrincipalName() {
		return principalName;
	}

	public void setPrincipalName(String principalName) {
		this.principalName = principalName;
	}
	
}
