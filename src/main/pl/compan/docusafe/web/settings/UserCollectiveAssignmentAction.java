package pl.compan.docusafe.web.settings;

import java.util.prefs.Preferences;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
/* User: Administrator, Date: 2005-12-08 11:24:28 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserCollectiveAssignmentAction.java,v 1.4 2007/11/27 12:11:40 mariuszk Exp $
 */
public class UserCollectiveAssignmentAction extends CollectiveAssignmentBaseAction
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(UserCollectiveAssignmentAction.class.getPackage().getName(),null);

    protected Preferences getPreferences()
    {
        return DSApi.context().userPreferences().node("collectiveassignment");
    }

    public String getBaseLink()
    {
        return "/settings/user-collective-assignment.action";
    }

    public String getSubtitle()
    {
        return sm.getString("ustawieniaUzytkownika");
    }

    protected boolean canUpdate()
    {
        return true;
    }


    public boolean isUserPref()
    {

        return true;
    }
}
