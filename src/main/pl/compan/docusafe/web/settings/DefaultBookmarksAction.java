package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.gson.Gson;

public class DefaultBookmarksAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(DefaultBookmarksAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(DefaultBookmarksAction.class.getPackage().getName(),null);

    /**
     * Dost�pne zak�adki systemowe.
     */
    private List<Bookmark> availableBookmarks;
    /**
     * Zapisane zak�adki systemowe jako domy�lne.
     */
    private List<Bookmark> selectedBookmarksList;
    /**
     * Wybrane zak�adki systemowe do zapisania jako domy�lne.
     */
    private Long[] selectedBookmarks;
    
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Save()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
	}
	

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		availableBookmarks = BookmarkManager.findSystemBookmarks();
        		String def = DSApi.context().systemPreferences().node("bookmarks").get("list", "");
        		Gson g = new Gson();
        		if (def != null && !def.equals(""))
        			selectedBookmarks = g.fromJson(def, Long[].class);
        		selectedBookmarksList = new ArrayList<Bookmark>();
        		for(Long l : selectedBookmarks) {
        			Bookmark b = Bookmark.find(l);
        			selectedBookmarksList.add(b);
        			availableBookmarks.remove(b);
        		}
        		

        	}
        	catch(Exception e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();     		
        		Gson g = new Gson();
        		DSApi.context().systemPreferences().node("bookmarks").put("list", g.toJson(selectedBookmarks));
        		DSApi.context().commit();
        		addActionMessage("Zapisano ustawienia");
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }

	public Long[] getSelectedBookmarks() {
		return selectedBookmarks;
	}

	public void setSelectedBookmarks(Long[] selectedBookmarks) {
		this.selectedBookmarks = selectedBookmarks;
	}

	public List<Bookmark> getSelectedBookmarksList() {
		return selectedBookmarksList;
	}

	public void setSelectedBookmarksList(List<Bookmark> selectedBookmarksList) {
		this.selectedBookmarksList = selectedBookmarksList;
	}

	public List<Bookmark> getAvailableBookmarks() {
		return availableBookmarks;
	}

	public void setAvailableBookmarks(List<Bookmark> availableBookmarks) {
		this.availableBookmarks = availableBookmarks;
	}
	
}
