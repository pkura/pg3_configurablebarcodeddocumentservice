package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.SetResultListener;

/**
 * Klasa obs�uguje konfiguracj� wy�wietlanych kolumn
 * na zak�adkach kt�re wy�wietlane s� u�ytkownikowi.
 */
public class BookmarksColumnsAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(BookmarksColumnsAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(BookmarksColumnsAction.class.getPackage().getName(),null);
	
    private BookmarkManager bm = new BookmarkManager();
    private Long bookmarkId;
    private Bookmark bookmark;
    private Tabs tabs;
    
    private String ascending;
    private Map <String, String> ascendingType;
    
    private int taskCount;
    private List<Integer> taskCounts;
    
    private String sortColumn;
    
    /**
     * Kolumny widoczne na liscie.
     */
    private Map<String, String> userColumns;
    /**
     * Dostepne kolumny.
     */
    private Map<String, String> availableColumns;
    /**
     * Wszystkie kolumny.
     */
    private Map<String, String> allColumns;

    private String[] selectedColumns;
    private String[] defaultSelectedColumns;
    private String tab;
    private Map<String, String> defaultColumns;

    private Map<String, String> defaultAvailableColumns;

    protected void setup()
    {
        PrepareTabs prepareTabs = new PrepareTabs();
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("user-settings"));
        
        registerListener("doResetDefault").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new ResetDefault()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSynchronize").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new Synchronize()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class PrepareTabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try 
        	{
	        	Map<Long, Bookmark> bookmarks  = bm.getVisibleBookmarksForUser(DSApi.context().getPrincipalName());
	        	
	            if (bookmarks.size()>0 && bookmarkId==null) bookmarkId=bookmarks.keySet().iterator().next();
	
	            tabs = new Tabs(6);
	
	            String link = "/settings/bookmarks-columns.action";
	            
	            for (Bookmark b : bookmarks.values()) {
		        	 tabs.add(new Tab(b.getName(), b.getTitle(),
				                link+"?tab="+b.getType()+"&bookmarkId="+b.getId(), b.getId().equals(bookmarkId)));
		        }
            
        	}
        	catch (EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }

    private class FillForm implements ActionListener {
        
    	public void actionPerformed(ActionEvent event) {
            try {
            	LoggerFactory.getLogger("przemek").debug("BOOKMARK ID: {}",bookmarkId);
            	if (bookmarkId != null)
            		bookmark = Bookmark.find(bookmarkId);
            		
            	tab = bookmark.getType();
            	// sortowanie rosn�co/ malej�co
	            ascendingType = bm.prepareAscendingTypes();
	            if(ascending==null)
	            {
	                ascending = DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
	                	get(BookmarkManager.BOOKMARKS_ASCENDING+bookmarkId, bookmark.getAscending()==null?"false":bookmark.getAscending().toString());
	            }
	            
	            //kolumna po kt�rej sortujemy
	            if (!BookmarkManager.TAB_CASES.equals(bookmark.getType()) && !BookmarkManager.TAB_ORDER.equals(bookmark.getType())) 
	            {
	            	sortColumn = DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
	            		get(BookmarkManager.BOOKMARKS_SORT_COLUMN+bookmarkId, bookmark.getSortField());
	            }

	            //ilo�� zada� na li�cie
	            Docusafe.getAdditionProperty("taskCounts");
	            taskCounts = new ArrayList<Integer>();
	            for(String count : Docusafe.getAdditionProperty("taskCounts").split(","))
	            {
	            	taskCounts.add(Integer.parseInt(count));
	            }
	            taskCount = DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
	            	getInt(BookmarkManager.BOOKMARKS_TASK_COUNT+bookmark.getId(),bookmark.getTaskCount());
	            
	            // wszystkie kolumny
	            String[] allProperties = TaskListUtils.getColumnProperties(tab,null);
	            // kolumny wybrane przez u�ytkownika (je�eli nie zdefiniowa� - kolumny domy�lne zak�adki)
	            String userColumnsDef = DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
	            			get(BookmarkManager.BOOKMARKS_COLUMNS+bookmarkId, bookmark.getColumns());
	            
	            List<String> lstAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
	            List<String> defaultAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
	            
	            bm.prepareDockindAttributes(lstAllColumns);
	            bm.prepareDockindAttributes(defaultAllColumns);
	            
	            List<String> lstUserColumns = new ArrayList<String>(Arrays.asList(userColumnsDef.split(",")));
	            List<String> defaultUserColumns = bookmark.getColumnsList();
	            
	            lstAllColumns.removeAll(lstUserColumns);
	            defaultAllColumns.removeAll(defaultUserColumns);
	            
	            userColumns = new LinkedHashMap<String, String>();
	            BookmarksColumnsAction.this.defaultColumns = new LinkedHashMap<String, String>();
	            availableColumns = new LinkedHashMap<String, String>();
	            defaultAvailableColumns = new LinkedHashMap<String, String>();
	            allColumns = new LinkedHashMap<String, String>();

                for (String col : lstUserColumns) {
                    userColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                }

                for (String col : defaultUserColumns) {
                    BookmarksColumnsAction.this.defaultColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                }

                for (String col : defaultAllColumns) {
                    defaultAvailableColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                }

                for (String col : lstAllColumns) {
                    availableColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col, bookmark.getCn()));
                }

                filterOutRestrictedColumns(availableColumns);

            }
            catch (EdmException e)
            {
            	log.error("", e);
            }
        }

        private void filterOutRestrictedColumns(Map<String,String> columns) throws EdmException {
            if(AvailabilityManager.isAvailable("tasklist.restrictColumnsIC")){
                if(!DSApi.context().hasPermission(DSPermission.INVOICE_TASKLIST_COLUMNS)){
                    columns.remove("dockindKwota");
                    columns.remove("dockindBusinessAtr1");
                }
            }
        }
    }

    private class Update implements ActionListener
    {
    	
        public void actionPerformed(ActionEvent event) {
        	
        	try
        	{
		    	bookmark = Bookmark.find(bookmarkId);
		    	tab = bookmark.getType();

            	boolean canEditColumns = true;
                DSApi.context().begin();
                
                if ((selectedColumns == null || selectedColumns.length == 0) && !BookmarkManager.TAB_ORDER.equals(tab))
                {
                	if(DSApi.context().hasPermission(DSPermission.WYBOR_KOLUMN_NA_LISCIE_ZADAN))
                	{
                		addActionError("Nie wybrano kolumn do listy zadan, przywr�cono standardowy zestaw kolumn.");
                	}
                	else
                	{
                		addActionError("Nie masz uprawnie� do wyboru kolumn na li�cie zada�.");
                		canEditColumns = false;
                	}
                }

                String columnsDef;

                if (selectedColumns == null || selectedColumns.length == 0)
                {
                    columnsDef = bookmark.getColumns();
                }
                else
                {
                    columnsDef = StringUtils.join(selectedColumns, ",");
                }
                
                // kolumny na li�cie zada�
                if (!BookmarkManager.TAB_ORDER.equals(tab) && canEditColumns) 
                {
                	DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_COLUMNS+bookmarkId, columnsDef);
                }

                //liczba zadan na liscie
                DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).putInt(BookmarkManager.BOOKMARKS_TASK_COUNT+bookmarkId, taskCount);
                
                if (!BookmarkManager.TAB_CASES.equals(tab) && !BookmarkManager.TAB_ORDER.equals(tab))
                {
                    DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_SORT_COLUMN+bookmarkId, sortColumn);
                    DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_ASCENDING+bookmarkId, ascending);
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     * Przywr�cenie ustawie� domy�lnych zak�adki.
     */
    private class ResetDefault implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                bookmark = Bookmark.find(bookmarkId);

                DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_COLUMNS+bookmarkId, bookmark.getColumns());
                DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_SORT_COLUMN+bookmarkId, bookmark.getSortField());
                DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).put(BookmarkManager.BOOKMARKS_ASCENDING+bookmarkId, bookmark.getAscending()!=null?bookmark.getAscending().toString():"false");
                DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).putInt(BookmarkManager.BOOKMARKS_TASK_COUNT+bookmarkId, bookmark.getTaskCount());

                DSApi.context().commit();
                addActionMessage(sm.getString("PrzywroconoDomyslneUstawienia"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     * Od�wie�enie listy zada�.
     */
    private class Synchronize implements ActionListener
    {
         public void actionPerformed(ActionEvent event)
         {
              try
              {
            	 DSApi.context().begin();
                 TasklistSynchro.Synchro(DSApi.context().getPrincipalName());
                 DSApi.context().commit();
              }
              catch (EdmException e)
              {
                  addActionError(e.getMessage());
              }
         }
    }
    
    public Long getBookmarkId() {
		return bookmarkId;
	}

	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}

	public Map<String, String> getAvailableColumns()
    {
        return availableColumns;
    }

    public Map<String, String> getUserColumns()
    {
        return userColumns;
    }

    public Map<String, String> getAllColumns()
    {
        return allColumns;
    }
    
    public void setSelectedColumns(String[] selectedColumns)
    {
        this.selectedColumns = selectedColumns;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public Tabs getTabs()
    {
        return tabs;
    }

    public String getSortColumn()
    {
        return sortColumn;
    }
    
    public void setSortColumn(String sortColumn)
    {
        this.sortColumn = sortColumn;
    }
      
    public void setAscending(String arg)
    {
        ascending = arg;
    }
    
    public String getAscending()
    {
        return ascending;
    }

    public String[] getDefaultSelectedColumns()
    {
        return defaultSelectedColumns;
    }

    public void setDefaultSelectedColumns(String[] defaultSelectedColumns)
    {
        this.defaultSelectedColumns = defaultSelectedColumns;
    }

    public Map<String, String> getDefaultColumns()
    {
        return defaultColumns;
    }

    public void setDefaultColumns(Map<String, String> defaultColumns)
    {
        this.defaultColumns = defaultColumns;
    }

    public Map<String, String> getDefaultAvailableColumns()
    {
        return defaultAvailableColumns;
    }

    public void setDefaultAvailableColumns(Map<String, String> defaultAvailableColumns)
    {
        this.defaultAvailableColumns = defaultAvailableColumns;
    }

    public void setTabs(Tabs tabs)
    {
        this.tabs = tabs;
    }

	public Map<String, String> getAscendingType() {
		return ascendingType;
	}

	public void setAscendingType(Map<String, String> ascendingType) {
		this.ascendingType = ascendingType;
	}

	public int getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(int taskCount) {
		this.taskCount = taskCount;
	}

	public List<Integer> getTaskCounts() {
		return taskCounts;
	}

	public void setTaskCounts(List<Integer> taskCounts) {
		this.taskCounts = taskCounts;
	}

}