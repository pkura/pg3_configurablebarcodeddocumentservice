package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;
import java.util.prefs.Preferences;

/* User: Administrator, Date: 2005-12-10 12:39:40 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FlagsBaseAction.java,v 1.14 2008/12/10 13:19:01 tomekl Exp $
 */
public abstract class FlagsBaseAction extends EventActionSupport
{
    StringManager sm ;
    // @IMPORT
    private Long[] flagIds;

    // @EXPORT
    private List flags;
    private boolean canAddFlags;
    private DSDivision[] divisions;

    private String c;
    private String description;
    private String divisionGuid;
    private String viewersDivisionGuid ;
    private Tabs tabs;

    public abstract boolean isUserFlags();
    public abstract String getBaseLink();

    protected void setup()
    {
        
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepareTabs()).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class PrepareTabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null);

            tabs = new Tabs(2);

            tabs.add(new Tab(sm.getString("FlagiUzytkownika"),sm.getString("FlagiUzytkownika"),
                "/settings/user-flags.action", isUserFlags()));
            if(DSApi.context().isAdmin())
            {
                tabs.add(new Tab(sm.getString("FlagiWspolne"),sm.getString("FlagiWspolne"),
                    "/settings/global-flags.action", !isUserFlags()));
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                flags = Flags.listFlags(isUserFlags());
                divisions = DSDivision.getAllGroups();
                canAddFlags = true;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (c == null)
                addActionError(sm.getString("NiePodanoKoduLiterowegoFlagi"));
            if (description == null)
                addActionError(sm.getString("NiePodanoOpisuFlagi"));
            if (c != null && c.length() > 5)
                addActionError(sm.getString("KodFlagiPowinienBycJednoznakowy"));
            divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid);
            viewersDivisionGuid  = TextUtils.trimmedStringOrNull(viewersDivisionGuid);

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();
                if((isUserFlags() && LabelsManager.existsUserLabel(c, DSApi.context().getPrincipalName(), Label.FLAG_TYPE))||
                		(!isUserFlags() && LabelsManager.existsLabel(c, Label.FLAG_TYPE)))
                {
                	addActionError(sm.getString("FlagaOTakiejNazwieJuzIstnieje"));
                }
                else
                {
                	Flags.addFlag(isUserFlags(), c.toUpperCase(), description, divisionGuid,viewersDivisionGuid );
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (flagIds == null || flagIds.length == 0)
                addActionError(sm.getString("NieZaznaczonoFlagDoUsuniecia"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                for (int i=0; i < flagIds.length; i++)
                {
                    Flags.removeFlag(isUserFlags(), flagIds[i]);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    
    
    public Long[] getFlagIds() {
		return flagIds;
	}
	public void setFlagIds(Long[] flagIds) {
		this.flagIds = flagIds;
	}
	public DSDivision[] getDivisions()
    {
        return divisions;
    }
    
    public String getC()
    {
        return c;
    }

    public void setC(String c)
    {
        this.c = c;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }
    
    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }
    
    public boolean isCanAddFlags()
    {
        return canAddFlags;
    }

    public List getFlags()
    {
        return flags;
    }
    public Tabs getTabs()
    {
        return tabs;
    }
    public void setTabs(Tabs tabs)
    {
        this.tabs = tabs;
    }
    public String getViewersDivisionGuid()
    {
        return viewersDivisionGuid;
    }
    public void setViewersDivisionGuid(String viewersDivisionGuid)
    {
        this.viewersDivisionGuid = viewersDivisionGuid;
    }
}
