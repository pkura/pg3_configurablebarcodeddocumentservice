package pl.compan.docusafe.web.settings;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;



/* User: Administrator, Date: 2005-12-05 14:40:31 */

/**
 * Klasa jest kopi� CollectiveAssignmentBaseAction, r�wnie dobrze mo�naby
 * rozszerza� tamt� klas�, ale na wszelki wypadek dla porz�dku j�
 * skopiowa�em.
 */
public abstract class TimeUsersBaseAction_ extends EventActionSupport
{
    // @EXPORT
    private String treeHtml;
    private Map targetsSelect = new LinkedHashMap();
    private DSDivision targetDivision;
    private DSUser[] users;

    // @IMPORT
    private String[] targetsSelectValues;

    // @EXPORT/@IMPORT
    private String divisionGuid;
    private String[] targets;

    protected abstract Preferences getPreferences();
    protected abstract boolean canUpdate();
    public abstract String getBaseLink();
    public abstract String getSubtitle();

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            String _divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid);

            try
            {
                targetDivision = null;
                // drzewo struktury organizacyjnej
                if (_divisionGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(_divisionGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                // przekierowanie do dzia�u, w kt�rym jest u�ytkownik
                if (targetDivision == null)
                {
                    DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
                    for (int i=0; i < divisions.length; i++)
                    {
                        if (!divisions[i].isGroup())
                        {
                            targetDivision = divisions[i];
                            break;
                        }
                    }
                }

                // domy�lnie - dzia� g��wny
                if (targetDivision == null)
                {
                    targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }

                // klasa tworz�ca urle dla element�w drzewa
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            StringBuilder url = new StringBuilder("javascript:void(post(\"");
                            url.append(((DSDivision) element).getGuid());
                            url.append("\"))");

                            return url.toString();
                        }
                        catch (Exception e)
                        {
                            event.getLog().error(e.getMessage(), e);
                            return null;
                        }
                    }
                };

                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true, false, false);

                treeHtml = tree.generateTree();

                users = targetDivision.getUsers();

                if (targetsSelectValues != null)
                {
                    // warto�ci bez ;POSITION na ko�cu
                    for (int i=0; i < targetsSelectValues.length; i++)
                    {
                        AssignmentDescriptor ad = AssignmentDescriptor.forDescriptor(targetsSelectValues[i]);
                        targetsSelect.put(ad.toDescriptor(), ad.format());
                    }
                }
                else
                {
                    Preferences prefs = getPreferences();
                    String[] keys = prefs.keys();
                    if (keys != null)
                    {
                        Arrays.sort(keys, new Comparator()
                        {
                            public int compare(Object o1, Object o2)
                            {
                            	return ((String)o1).compareTo((String)o2);
                                //return Integer.parseInt(o1.toString()) - Integer.parseInt(o2.toString());
                            }
                        });

                        for (int i=0; i < keys.length; i++)
                        {
                            AssignmentDescriptor ad = AssignmentDescriptor.forDescriptor(prefs.get(keys[i], ""));
                            targetsSelect.put(ad.toDescriptor(), ad.format());
                        }
                    }
                }
                
            }
            catch (BackingStoreException e)
            {
                addActionError(e.getMessage());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!canUpdate())
            {
                addActionError("Brak uprawnie�");
                return;
            }

            try
            {
                DSApi.context().begin();

                Preferences pref = getPreferences();
                pref.clear();
                if (targets != null)
                {
                    Arrays.sort(targets, new Comparator()
                    {
                        public int compare(Object o1, Object o2)
                        {
                            int ls = ((String) o1).lastIndexOf(';');
                            int pos1 = Integer.parseInt(((String) o1).substring(ls+1));
                            ls = ((String) o2).lastIndexOf(';');
                            int pos2 = Integer.parseInt(((String) o2).substring(ls+1));
                            return ((String)o1).compareTo((String)o2);
                            //return pos1 - pos2;
                        }
                    });

                    // ka�dy element listy targets ma posta�
                    // USERNAME;GUID;WFENGINE;WFPROCESS;OBJECTIVE;POZYCJA
                    // WFENGINE ma obecnie zawsze warto�� "internal"
                    // pole POZYCJA s�u�y tylko sortowaniu, nie powinno by�
                    // zapisywane w bazie danych

                    for (int i=0; i < targets.length; i++)
                    {
                        int ls = targets[i].lastIndexOf(';');
                        String position = targets[i].substring(ls+1);
                        String descriptor = targets[i].substring(0, ls);

                        AssignmentDescriptor ad =
                            AssignmentDescriptor.forDescriptor(descriptor);

                        pref.put(position, ad.toDescriptor());
                    }
                }

                DSApi.context().commit();

                // lista dekretacji powinna by� utworzona z bazy danych,
                // a nie z formularza
                targetsSelectValues = null;
            }
            catch (BackingStoreException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public Map getTargetsSelect()
    {
        return targetsSelect;
    }

    public String[] getTargets()
    {
        return targets;
    }

    public void setTargets(String[] targets)
    {
        this.targets = targets;
    }

    public DSDivision getTargetDivision()
    {
        return targetDivision;
    }

    public DSUser[] getUsers()
    {
        return users;
    }

    public void setTargetsSelectValues(String[] targetsSelectValues)
    {
        this.targetsSelectValues = targetsSelectValues;
    }
}
