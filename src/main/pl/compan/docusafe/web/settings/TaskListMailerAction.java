package pl.compan.docusafe.web.settings;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.TaskListMailer.MailerConfThread;
import pl.compan.docusafe.service.TaskListMailer.TaskListMailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ScannerAction.java,v 1.4 2005/08/23 14:56:31 lk Exp $
 */
public class TaskListMailerAction extends EventActionSupport
{
	StringManager sm =
        GlobalPreferences.loadPropertiesFile(TaskListMailerAction.class.getPackage().getName(),null);
    private boolean useTaskListMailer;
    private Integer timeMail;

    /** new one */
    private boolean adminAccess;
    private boolean adminBlock;
    private Integer timeMailHour;
    private Integer timeMailMin;

    private Map<Integer,String> termKinds;
    private Integer termKind;
    private Map<Integer,String> days;
    private Integer day;
    private String time;
    
    private boolean useTaskListSMS;
    private Integer termKindSMS;
    private Integer dayOfMonth;
    private Integer daySMS;
    private Integer timeSMS;
    private String timeSMSg;
    private Integer dayOfMonthSMS;
    
    private Integer invoiceDays;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
            append(OpenHibernateSession.INSTANCE).
            append(new Save()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveForUsers").
	        append(OpenHibernateSession.INSTANCE).
	        append(new SaveForUsers()).
	        append(fillForm).
	       	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveForInvoice").
	        append(OpenHibernateSession.INSTANCE).
	        append(new SaveForInvoice()).
	        append(fillForm).
	       	appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        	try {
				adminAccess = DSApi.context().getDSUser().isAdmin();
			} catch (UserNotFoundException e) {
				LogFactory.getLog("eprint").debug("", e);
			} catch (EdmException e) {
				LogFactory.getLog("eprint").debug("", e);
			}
        	useTaskListMailer = DSApi.context().userPreferences().node("taskListMailer").getBoolean("use", false);
            timeMail = DSApi.context().userPreferences().node("taskListMailer").getInt("timeMail", 1);
            adminBlock = DSApi.context().systemPreferences().node("taskListMailer").getBoolean("adminBlock", false);
            termKind = DSApi.context().userPreferences().node("taskListMailer").getInt("termKind", 1);
            day = DSApi.context().userPreferences().node("taskListMailer").getInt("dayOfWeek", 2);
            time = DSApi.context().userPreferences().node("taskListMailer").get("time", "9:00");
            dayOfMonth = DSApi.context().userPreferences().node("taskListMailer").getInt("dayOfMonth", 1);
            
            //SMS
            useTaskListSMS = DSApi.context().userPreferences().node("taskListSMS").getBoolean("use", false);
            timeSMS = DSApi.context().userPreferences().node("taskListSMS").getInt("timeSMS", 1);
//            adminBlock = DSApi.context().systemPreferences().node("taskListMailer").getBoolean("adminBlock", false);
            termKindSMS = DSApi.context().userPreferences().node("taskListSMS").getInt("termKindSMS", 1);
            daySMS = DSApi.context().userPreferences().node("taskListSMS").getInt("dayOfWeekSMS", 2);
            timeSMSg = DSApi.context().userPreferences().node("taskListSMS").get("timeSMSg", "9:00");
            dayOfMonthSMS = DSApi.context().userPreferences().node("taskListSMS").getInt("dayOfMonthSMS", 1);

    		termKinds = new LinkedHashMap<Integer, String>();
            termKinds.put(TaskListMailer.TERM_KIND_CO_ILE_MINUT, sm.getString("CoIleMinut"));
            termKinds.put(TaskListMailer.TERM_KIND_CO_ILE_GODZIN, sm.getString("CoIleGodzin"));
            termKinds.put(TaskListMailer.TERM_KIND_RAZ_DZIENNIE, sm.getString("RazDziennie"));
            termKinds.put(TaskListMailer.TERM_KIND_RAZ_W_TYGODNIU, sm.getString("RazWtygodniu"));
            termKinds.put(TaskListMailer.TERM_KIND_RAZ_W_MIESIACU, sm.getString("Razwmiesiacu"));

            days = new LinkedHashMap<Integer, String>();
            String[] dayNames = DateUtils.getDaysOfWeekMap();
            for (int i = 1; i <= 7 ; i++)
            {
            	days.put(i,dayNames[i]);
			}
            
            invoiceDays = DSApi.context().systemPreferences().node("invoice-mail").getInt("invoiceDays", 0);
        }
    }

    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if(TaskListMailer.TERM_KIND_RAZ_W_MIESIACU.equals(termKind) && (dayOfMonth > 30 || dayOfMonth < 1))
            		throw new EdmException(sm.getString("dzienMiesiacaMusiBycZPrzedzialu130"));

                DSApi.context().begin();
                if(DSApi.context().getDSUser().getEmail() != null)
                {
                	DSApi.context().userPreferences().node("taskListMailer").putBoolean("use", useTaskListMailer);
	                DSApi.context().userPreferences().node("taskListMailer").putInt("timeMail", timeMail);
	    			DSApi.context().userPreferences().node("taskListMailer").putInt("termKind", termKind);
	    			DSApi.context().userPreferences().node("taskListMailer").putInt("dayOfWeek", day);
	    			DSApi.context().userPreferences().node("taskListMailer").putInt("dayOfMonth", dayOfMonth);
	    			DSApi.context().userPreferences().node("taskListMailer").put("time", time);

	                if(useTaskListMailer)
	                {
	                	DSApi.context().systemPreferences().node("taskListMailer").putLong(DSApi.context().getDSUser().getName(), 0);
	                }
	                else
	                {
	                	DSApi.context().systemPreferences().node("taskListMailer").remove(DSApi.context().getDSUser().getName());
	                	DSApi.context().systemPreferences().node("taskListMailer").remove(DSApi.context().getDSUser().getExternalName());
	                }
	            }
                else
                {
                	event.addActionError(sm.getString("BrakAdresuEmail"));
                }
                if(AvailabilityManager.isAvailable("taskListSMS"))
                {
                	if(DSApi.context().getDSUser().getEmail() != null)
                    {
                    	DSApi.context().userPreferences().node("taskListSMS").putBoolean("use", useTaskListSMS);
    	                DSApi.context().userPreferences().node("taskListSMS").putInt("timeSMS", timeSMS);
    	    			DSApi.context().userPreferences().node("taskListSMS").putInt("termKindSMS", termKindSMS);
    	    			DSApi.context().userPreferences().node("taskListSMS").putInt("dayOfWeekSMS", daySMS);
    	    			DSApi.context().userPreferences().node("taskListSMS").putInt("dayOfMonthSMS", dayOfMonthSMS);
    	    			DSApi.context().userPreferences().node("taskListSMS").put("timeSMSg", timeSMSg);

    	                if(useTaskListMailer)
    	                {
    	                	DSApi.context().systemPreferences().node("taskListSMS").putLong(DSApi.context().getDSUser().getName(), 0);
    	                }
    	                else
    	                {
    	                	DSApi.context().systemPreferences().node("taskListSMS").remove(DSApi.context().getDSUser().getName());
    	                	DSApi.context().systemPreferences().node("taskListSMS").remove(DSApi.context().getDSUser().getExternalName());
    	                }
    	            }
                    else
                    {
                    	event.addActionError(sm.getString("BrakNumeruTelefonuKom"));
                    }
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    private class SaveForUsers implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		Thread td = new Thread(new MailerConfThread(useTaskListMailer, timeMail,termKind,day,time,dayOfMonth));
    		td.start();
    	}
    }
    
    private class SaveForInvoice implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		try
    		{
    			DSApi.context().begin();
    			DSApi.context().systemPreferences().node("invoice-mail").putInt("invoiceDays", invoiceDays);
    			DSApi.context().commit();
    		}
    		catch (Exception e) 
    		{
    			addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
			}
    	}
    }

    public boolean isUseTaskListMailer() {
		return useTaskListMailer;
	}

	public void setUseTaskListMailer(boolean useTaskListMailer) {
		this.useTaskListMailer = useTaskListMailer;
	}

	public Integer getTimeMail() {
		return timeMail;
	}

	public void setTimeMail(Integer timeMail) {
		this.timeMail = timeMail;
	}

	public boolean isAdminAccess() {
		return adminAccess;
	}

	public void setAdminAccess(boolean adminAccess) {
		this.adminAccess = adminAccess;
	}

	public boolean isAdminBlock() {
		return adminBlock;
	}

	public void setAdminBlock(boolean adminBlock) {
		this.adminBlock = adminBlock;
	}

	public Integer getTimeMailHour() {
		return timeMailHour;
	}

	public void setTimeMailHour(Integer timeMailHour) {
		this.timeMailHour = timeMailHour;
	}

	public Integer getTimeMailMin() {
		return timeMailMin;
	}

	public void setTimeMailMin(Integer timeMailMin) {
		this.timeMailMin = timeMailMin;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Map<Integer, String> getDays() {
		return days;
	}

	public void setDays(Map<Integer, String> days) {
		this.days = days;
	}

	public Map<Integer, String> getTermKinds() {
		return termKinds;
	}

	public void setTermKinds(Map<Integer, String> termKinds) {
		this.termKinds = termKinds;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getTermKind() {
		return termKind;
	}

	public void setTermKind(Integer termKind) {
		this.termKind = termKind;
	}

	public Integer getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(Integer dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public Integer getInvoiceDays() {
		return invoiceDays;
	}

	public void setInvoiceDays(Integer invoiceDays) {
		this.invoiceDays = invoiceDays;
	}

	public Integer getDaySMS() {
		return daySMS;
	}

	public void setDaySMS(Integer daySMS) {
		this.daySMS = daySMS;
	}

	public Integer getTimeSMS() {
		return timeSMS;
	}

	public void setTimeSMS(Integer timeSMS) {
		this.timeSMS = timeSMS;
	}

	public Integer getDayOfMonthSMS() {
		return dayOfMonthSMS;
	}

	public void setDayOfMonthSMS(Integer dayOfMonthSMS) {
		this.dayOfMonthSMS = dayOfMonthSMS;
	}

	public Integer getTermKindSMS() {
		return termKindSMS;
	}

	public void setTermKindSMS(Integer termKindSMS) {
		this.termKindSMS = termKindSMS;
	}

	public String getTimeSMSg() {
		return timeSMSg;
	}

	public void setTimeSMSg(String timeSMSg) {
		this.timeSMSg = timeSMSg;
	}

	public boolean isUseTaskListSMS() {
		return useTaskListSMS;
	}

	public void setUseTaskListSMS(boolean useTaskListSMS) {
		this.useTaskListSMS = useTaskListSMS;
	}
}
