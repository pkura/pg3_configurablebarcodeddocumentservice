package pl.compan.docusafe.web.settings;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.gson.Gson;
/**
 * Klasa obs�uguj�ca ustawianie odpowiednich zak�adek
 * dla pojedynczego u�ytkownika.
 */
public class ShowBookmarksAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ShowBookmarksAction.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(ShowBookmarksAction.class.getPackage().getName(),null);
	
	private Long id;
    private List<Bookmark> availableBookmarks;
    private List<Bookmark> userBookmarks = new ArrayList<Bookmark>();
    private String[] selectedBookmarks;

	
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		// wczytanie zak�adek systemowych
        		String def = DSApi.context().systemPreferences().node("bookmarks").get("list", "");
        		// wczytanie zak�. u�ytkownika
        		String ids = DSApi.context().userPreferences().node("bookmarks").get("list", def);
        		
        		Gson g = new Gson();
        		Long[] idsTab = new Long[0];
        		if (ids != null && !ids.equals(""))
        			idsTab = g.fromJson(ids, Long[].class);
        		for (Long i : idsTab) 
        		{
        			Bookmark b = Bookmark.find(i); 
        			userBookmarks.add(b);
        		}
        		// tutaj wyjmujemy wszystkie zad�adki (r�wnie� te systemowe czyli dost�pne do edycji wy��cznie dla admina)
        		// robione jest to w celu umo�liwienia u�ykownikowi wybierania zak�adek z zak�adek systemowych
        		availableBookmarks = BookmarkManager.getAvailableBookmarksForUser(DSApi.context().getPrincipalName(), true);
        		for (Bookmark bookmark  : userBookmarks) 
        		{
        			availableBookmarks.remove(bookmark);
				}
        	}
        	catch(Exception e)
        	{
        		addActionError(e.getMessage());
        		log.error("",e);
        	}
        }
    }
	
	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		Gson g = new Gson();
        		DSApi.context().userPreferences().node("bookmarks").put("list", g.toJson(selectedBookmarks));
        		DSApi.context().commit();
        		addActionMessage(sm.getString("ZapisanoZmiany"));
        	}
        	catch(Exception e)
        	{
        		log.error("",e);
        	}
        }
    }

	public List<Bookmark> getAvailableBookmarks() {
		return availableBookmarks;
	}

	public void setAvailableBookmarks(List<Bookmark> availableBookmarks) {
		this.availableBookmarks = availableBookmarks;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String[] getSelectedBookmarks() {
		return selectedBookmarks;
	}

	public void setSelectedBookmarks(String[] selectedBookmarks) {
		this.selectedBookmarks = selectedBookmarks;
	}

	public List<Bookmark> getUserBookmarks() {
		return userBookmarks;
	}

	public void setUserBookmarks(List<Bookmark> userBookmarks) {
		this.userBookmarks = userBookmarks;
	}

}