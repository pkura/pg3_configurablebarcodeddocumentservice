package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;
import java.util.prefs.Preferences;

/* User: Administrator, Date: 2005-12-13 15:15:51 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditFlagBaseAction.java,v 1.9 2008/09/25 12:32:45 kubaw Exp $
 */
public abstract class EditFlagBaseAction extends EventActionSupport
{
    private StringManager sm;
    private Long id;
    private String c;
    private String description;
    private String divisionGuid;
    private String viewersDivisionGuid ;

    private List flags;
    private DSDivision[] divisions;

    public abstract boolean isUserFlags();
    public abstract String getBaseLink();

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                flags = Flags.listFlags(isUserFlags());
                divisions = DSDivision.getAllGroups();

                Flags.FlagBean fb = Flags.getFlagBeanForLabel(LabelsManager.findLabelById(id));
                c = fb.getC();
                description = fb.getDescription();
                divisionGuid = fb.getDivisionGuid();
                viewersDivisionGuid = fb.getViewersDivisionGuid();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sm = GlobalPreferences.loadPropertiesFile(Constants.Package,null);
            if (c == null || c.length() > 3)
                addActionError(sm.getString("KodFlagiPowinienSkladacSieZjednejLitery"));
            if (description == null)
                addActionError(sm.getString("NiePodanoOpisuFlagi"));
            divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid);
            
            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();
                
                Flags.updateFlag(isUserFlags(), id, c.toUpperCase(), description, divisionGuid, viewersDivisionGuid);

                DSApi.context().commit();
                event.setResult("flags");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getC()
    {
        return c;
    }

    public void setC(String c)
    {
        this.c = c;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }
    
    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }
    
    public List getFlags()
    {
        return flags;
    }
    
    public DSDivision[] getDivisions()
    {
        return divisions;
    }
    public String getViewersDivisionGuid()
    {
        return viewersDivisionGuid;
    }
    public void setViewersDivisionGuid(String viewersDivisionGuid)
    {
        this.viewersDivisionGuid = viewersDivisionGuid;
    }
}
