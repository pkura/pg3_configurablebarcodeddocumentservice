package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.base.Flags;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-13 16:10:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditGlobalFlagAction.java,v 1.2 2008/09/16 15:59:33 kubaw Exp $
 */
public class EditGlobalFlagAction extends EditFlagBaseAction
{

    public String getBaseLink()
    {
        return "/settings/edit-global-flag.action";
    }

    public boolean isUserFlags()
    {
        return false;
    }
}
