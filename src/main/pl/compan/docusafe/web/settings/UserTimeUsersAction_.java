package pl.compan.docusafe.web.settings;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

import java.util.prefs.Preferences;
/* User: Administrator, Date: 2005-12-08 11:24:28 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserCollectiveAssignmentAction.java,v 1.1 2005/12/08 16:57:39 lk Exp $
 */
public class UserTimeUsersAction_ extends TimeUsersBaseAction_
{
    private final static StringManager sm =
        GlobalPreferences.loadPropertiesFile(UserTimeUsersAction_.class.getPackage().getName(),null);

    protected Preferences getPreferences()
    {
        return DSApi.context().userPreferences().node("timeusers");
    }

    public String getBaseLink()
    {
        return "/settings/user-timeusers.action";
    }

    public String getSubtitle()
    {
        return sm.getString("ustawieniaUzytkownika");
    }

    protected boolean canUpdate()
    {
        return true;
    }
}
