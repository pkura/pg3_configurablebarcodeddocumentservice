package pl.compan.docusafe.web.settings;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.SetResultListener;

/**
 * Obsluguje konfiguracje listy zadan w zakresie kolejnosci (i obecnosci)
 * kolumn na liscie.
 * <p>
 * Kolumny na liscie zadan sa umieszczane w Preferences jako listy nazw
 * kolumn rozdzielonych przecinkami.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TaskListAction.java,v 1.45 2010/08/17 08:05:35 pecet1 Exp $
 */
public class TaskListAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(TaskListAction.class);
    // nazwy zakladek musza byc takie, jak w akcji obslugujacej liste
    // zadan, bo sa uzywane jako parametry dla metod tej akcji
    public static final String TAB_IN = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_IN;
    public static final String TAB_OUT = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_OUT;
    public static final String TAB_INT = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_INT;
    public static final String TAB_CASES = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_CASES;
    public static final String TAB_WATCHES = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_WATCHES;
    public static final String TAB_DOCUMENT_PACKAGE = pl.compan.docusafe.web.office.tasklist.TaskListAction.TAB_DOCUMENT_PACKAGE;

    private StringManager sm=
        GlobalPreferences.loadPropertiesFile(TaskListAction.class.getPackage().getName(),null);
    // @EXPORT
    /**
     * Kolumny widoczne na liscie.
     */
    private Map<String, String> userColumns;
    /**
     * Dostepne kolumny.
     */
    private Map<String, String> availableColumns;
    /**
     * Wszystkie kolumny.
     */
    private Map<String, String> allColumns;
    private Tabs tabs;
    private Tabs defaultTabs;

    // @IMPORT
    private String[] selectedColumns;
    private String[] defaultSelectedColumns;
    private String tab;
    private String sortColumn;
    private String defaultSortColumn;
    private String ascending;
    private Map<String, String> defaultColumns;

    private boolean simpleTaskList;
    private boolean defaultSimpleTaskList;
    private Map<String, String> defaultAvailableColumns;
    
    private int taskCount;
    private List<Integer> taskCounts;
    
    private Map <String, String> ascendingType;

    private List<DSUser> userList;
    private String resetUser;
    
    protected void setup()
    {
        PrepareTabs prepareTabs = new PrepareTabs();
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCancel").
            append(new SetResultListener("user-settings"));

        registerListener("doReset").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new Reset()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doResetDefault").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new ResetDefault()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doResetDefaultForUser").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new ResetDefaultForUser()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdateDefault").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new UpdateDefault()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSynchronize").
            append(OpenHibernateSession.INSTANCE).
            append(prepareTabs).
            append(new Synchronize()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class PrepareTabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // domyslna zakladka - TAB_IN
            if (!TAB_IN.equals(tab) && !TAB_OUT.equals(tab) &&
                !TAB_INT.equals(tab) && !TAB_CASES.equals(tab) && !TAB_WATCHES.equals(tab))
            {
            	if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
            		tab = TAB_IN;
            	else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
            		tab = TAB_INT;
            	else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
            		tab = TAB_OUT;
            	else
            		tab = TAB_IN;
            }
                

            tabs = new Tabs(5);

            String link = "/settings/task-list.action";

            if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
            {
            	tabs.add(new Tab(sm.getString("PismaPrzychodzace"), sm.getString("PismaPrzychodzace"),
            			link+"?tab="+TAB_IN, TAB_IN.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
            {
	            tabs.add(new Tab(sm.getString("PismaWewnetrzne"), sm.getString("PismaWewnetrzne"),
	                link+"?tab="+TAB_INT, TAB_INT.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
            {
	            tabs.add(new Tab(sm.getString("PismaWychodzace"), sm.getString("PismaWychodzace"),
	                link+"?tab="+TAB_OUT, TAB_OUT.equals(tab)));
            }
            //if (!Docusafe.hasExtra("business")) - by�o sprawdzenie czy w extras w licencji jest "business"
            if(AvailabilityManager.isAvailable("sprawy"))
            {
                tabs.add(new Tab(sm.getString("Sprawy"), sm.getString("Sprawy"),
                    link+"?tab="+TAB_CASES, TAB_CASES.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.repository.obserwowane"))
            {
            	tabs.add(new Tab(sm.getString("Obserwowane"), sm.getString("Obserwowane"),
                    link+"?tab="+TAB_WATCHES, TAB_WATCHES.equals(tab)));
            }
            
            defaultTabs = new Tabs(5);

            String DeafultLink = "/settings/default-task-list.action";

            if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
            {
	            defaultTabs.add(new Tab(sm.getString("PismaPrzychodzace"), sm.getString("PismaPrzychodzace"),
	                DeafultLink+"?tab="+TAB_IN, TAB_IN.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
            {
	            defaultTabs.add(new Tab(sm.getString("PismaWewnetrzne"), sm.getString("PismaWewnetrzne"),
	                DeafultLink+"?tab="+TAB_INT, TAB_INT.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
            {
	            defaultTabs.add(new Tab(sm.getString("PismaWychodzace"), sm.getString("PismaWychodzace"),
	                DeafultLink+"?tab="+TAB_OUT, TAB_OUT.equals(tab)));
            }
            if (!Docusafe.hasExtra("business"))
            {
                defaultTabs.add(new Tab(sm.getString("Sprawy"), sm.getString("Sprawy"),
                        DeafultLink+"?tab="+TAB_CASES, TAB_CASES.equals(tab)));
            }
            if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
            {
            	defaultTabs.add(new Tab(sm.getString("Obserwowane"), sm.getString("Obserwowane"),
            		DeafultLink+"?tab="+TAB_WATCHES, TAB_WATCHES.equals(tab)));
            }
        }
    }

    // TODO: usuwac "column"
    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {

	            if (!DSApi.isContextOpen()){
	            	DSApi.reloadHibernate();
                }
	            if (ascendingType == null) {
		             ascendingType = new HashMap <String, String> ();
		             ascendingType.put("false", "malej�co");
		             ascendingType.put("true", "rosn�co");
	            }

                if(AvailabilityManager.isAvailable("tasklist.resetForOther")){
                    userList = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                }

	            if(ascending==null){
	                ascending = DSApi.context().userPreferences().node("task-list").get("ascending"+tab, DSApi.context().systemPreferences().node("default-task-list").get("default-ascending"+tab, "true"));
	            }

	            String[] allProperties = TaskListUtils.getColumnProperties(tab,null);
	            String[] defaultProperties = TaskListUtils.getDefaultColumnProperties(tab,null);
	            
	            String defaultColumns = DSApi.context().systemPreferences().node("default-task-list").
	                get("default-columns_"+tab, StringUtils.join(defaultProperties, ","));
	            String userColumnsDef = DSApi.context().userPreferences().node("task-list").
	                get("columns_"+tab, defaultColumns);
	            
	            List<String> lstAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
	            List<String> defaultAllColumns = new ArrayList<String>(Arrays.asList(allProperties));
	            
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr1").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr1");
	            	lstAllColumns.add("dockindBusinessAtr1");
	            }
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr2").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr2");
	            	lstAllColumns.add("dockindBusinessAtr2");
	            }
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr3").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr3");
	            	lstAllColumns.add("dockindBusinessAtr3");
	            }
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr4").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr4");
	            	lstAllColumns.add("dockindBusinessAtr4");
	            }
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr5").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr5");
	            	lstAllColumns.add("dockindBusinessAtr5");
	            }
	            if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr6").equals("false")) {
	            	defaultAllColumns.add("dockindBusinessAtr6");
	            	lstAllColumns.add("dockindBusinessAtr6");
	            }
	            
	            List<String> lstUserColumns = new ArrayList<String>(Arrays.asList(userColumnsDef.split(",")));
	            List<String> defaultUserColumns = new ArrayList<String>(Arrays.<String>asList(defaultColumns.split(",")));
	            lstAllColumns.removeAll(lstUserColumns);
	            defaultAllColumns.removeAll(defaultUserColumns);
	            userColumns = new LinkedHashMap<String, String>();
	            TaskListAction.this.defaultColumns = new LinkedHashMap<String, String>();
	            availableColumns = new LinkedHashMap<String, String>();
	            defaultAvailableColumns = new LinkedHashMap<String, String>();
	            allColumns = new LinkedHashMap<String, String>();

                for (String col : lstUserColumns) {
                    userColumns.put(col, TaskListUtils.getColumnDescription(col));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col));
                }

                for (String col : defaultUserColumns) {
                    TaskListAction.this.defaultColumns.put(col, TaskListUtils.getColumnDescription(col));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col));
                }

                for (String col : defaultAllColumns) {
                    defaultAvailableColumns.put(col, TaskListUtils.getColumnDescription(col));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col));
                }

                for (String col : lstAllColumns) {
                    availableColumns.put(col, TaskListUtils.getColumnDescription(col));
                    allColumns.put(col, TaskListUtils.getColumnDescription(col));
                }

                filterOutRestrictedColumns(availableColumns);

	            if (!TAB_CASES.equals(tab)) {
	                defaultSortColumn = DSApi.context().systemPreferences().node("default-task-list").get("default-sort-column_"+tab, "receiveDate");
	                sortColumn = DSApi.context().userPreferences().node("task-list").get("sort-column_"+tab, defaultSortColumn);
	            }
                
	            defaultSimpleTaskList = DSApi.context().systemPreferences().node("default-task-list").getBoolean("default-simple-task-list"+tab, false);
	            simpleTaskList = DSApi.context().userPreferences().node("task-list").getBoolean("simple-task-list"+tab, defaultSimpleTaskList);
	            
	            
	            //lista
	            Docusafe.getAdditionProperty("taskCounts");
	            taskCounts = new ArrayList<Integer>();
	            for(String count : Docusafe.getAdditionProperty("taskCounts").split(","))
	            {
	            	taskCounts.add(Integer.parseInt(count));
	            }
	            taskCount = DSApi.context().userPreferences().node("task-list").getInt("task-count-per-page"+tab,0);
            }
            catch (EdmException e)
            {
            	log.error("", e);
            }
        }

        private void filterOutRestrictedColumns(Map<String,String> columns) throws EdmException {
            if(AvailabilityManager.isAvailable("tasklist.restrictColumnsIC")){
                if(!DSApi.context().hasPermission(DSPermission.INVOICE_TASKLIST_COLUMNS)){
                    columns.remove("dockindKwota");
                    columns.remove("dockindBusinessAtr1");
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event) {
        	LoggerFactory.getLogger("tomekl").debug("count "+taskCount);
        	
            if (selectedColumns == null || selectedColumns.length == 0){
                try{
                    PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                    if(cache.hasPermission(
                            DSApi.context().getPrincipalName(),
                            DSPermission.WYBOR_KOLUMN_NA_LISCIE_ZADAN.getName())){
                        addActionError("Nie wybrano kolumn do listy zadan, przywr�cono standardowy zestaw kolumn.");
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            try
            {
                DSApi.context().begin();

                String columnsDef;

                if (selectedColumns == null || selectedColumns.length == 0)
                {
                    //String[] tmp = pl.compan.docusafe.web.office.tasklist.TaskListAction.getDefaultColumnProperties(tab,null);
                    String[] defaultProperties = TaskListUtils.getDefaultColumnProperties(tab,null);
                    columnsDef = DSApi.context().systemPreferences().node("default-task-list").
                    get("default-columns_"+tab, StringUtils.join(defaultProperties, ","));//StringUtils.join(tmp, ",");
                }
                else
                {
                    columnsDef = StringUtils.join(selectedColumns, ",");
                }

                DSApi.context().userPreferences().node("task-list").put("columns_"+tab, columnsDef);

                // na jakis czas, az u wszystkich klient�w sie usunie
                // dodano 23.08.2005
                DSApi.context().userPreferences().node("task-list").remove("columns");

                DSApi.context().userPreferences().node("task-list").putBoolean("simple-task-list"+tab, simpleTaskList);

                //liczba zadan na liscie
                DSApi.context().userPreferences().node("task-list").putInt("task-count-per-page"+tab, taskCount);
                
                if (!TAB_CASES.equals(tab))
                {
                    DSApi.context().userPreferences().node("task-list").put("sort-column_"+tab, sortColumn);
                    DSApi.context().userPreferences().node("task-list").put("ascending"+tab, ascending);
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class UpdateDefault implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (defaultSelectedColumns == null || defaultSelectedColumns.length == 0)
                addActionError("Nie wybrano kolumn do listy zadan, przywr�cono standardowy zestaw kolumn.");

            try
            {
                DSApi.context().begin();

                String columnsDef;

                if (defaultSelectedColumns == null || defaultSelectedColumns.length == 0)
                {
                    String[] tmp = TaskListUtils.getDefaultColumnProperties(tab,null);
                    columnsDef = StringUtils.join(tmp, ",");
                }
                else
                {
                    columnsDef = StringUtils.join(defaultSelectedColumns, ",");
                }

                //DSApi.context().userPreferences().node("task-list").put("columns_"+tab, columnsDef);
                DSApi.context().systemPreferences().node("default-task-list").put("default-columns_"+tab, columnsDef);

                // na jakis czas, az u wszystkich klient�w sie usunie
                // dodano 23.08.2005
                //DSApi.context().userPreferences().node("task-list").remove("columns");
                
                //DSApi.context().userPreferences().node("task-list").putBoolean("simple-task-list"+tab, defaultSimpleTaskList);
                
                DSApi.context().systemPreferences().node("default-task-list").putBoolean("default-simple-task-list"+tab, defaultSimpleTaskList);
                
                //liczba zadan na liscie
                DSApi.context().userPreferences().node("task-list").putInt("task-count-per-page"+tab, 20);
                
                //DSApi.context().systemPreferences().node("default-task-list").putBoolean("zmiana", true);
                if (!TAB_CASES.equals(tab))
                {
                    //DSApi.context().userPreferences().node("task-list").put("sort-column_"+tab, defaultSortColumn);
                    //DSApi.context().userPreferences().node("task-list").put("ascending"+tab, ascending);
                    DSApi.context().systemPreferences().node("default-task-list").put("default-sort-column_"+tab, defaultSortColumn);
                    DSApi.context().systemPreferences().node("default-task-list").put("default-ascending"+tab, ascending);
                }
                DSApi.context().commit();
                /*List<? extends DSUser> listaUsr;
                int LIMIT = 0;
                SearchResults<? extends DSUser> results = DSUser.search(0, LIMIT, DSUser.SORT_FIRSTNAME_LASTNAME, true, null);
                listaUsr = fun.list(results);
                int i =0;
                while(i < listaUsr.size())
                {
                    DSApi.context().begin();
                    //DSApi.context().userPreferences(listaUsr.get(i).getName()).node("task-list").put("columns_"+tab, columnsDef);
                    String pom = DSApi.context().systemPreferences().node("default-task-list").get("default-columns_"+tab, null);
                    DSApi.context().systemPreferences().node("default-task-list").putBoolean(listaUsr.get(i).getName(), true);
                    DSApi.context().userPreferences(listaUsr.get(i).getName()).node("task-list").put("columns_"+tab, pom);
                    DSApi.context().commit();
                    //DSApi.context().userPreferences(listaUsr.get(i).getName()).node("task-list").putInt("zmiana"+tab, 1);
                    i++;
                    
                }
                i = 0;
                while(i < listaUsr.size())
                {
                    //DSApi.context().userPreferences(listaUsr.get(i).getName()).node("task-list").putInt("zmiana"+tab, 1);
                    i++;
                    
                }*/
                
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(),e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private class ResetDefault implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                String[] tmp = TaskListUtils.getDefaultColumnProperties(tab,null);
                String columnsDef = StringUtils.join(tmp, ",");

                DSApi.context().userPreferences().node("task-list").put("columns_"+tab, columnsDef);
                DSApi.context().userPreferences().node("task-list").putBoolean("simple-task-list"+tab, DSApi.context().userPreferences().node("task-list").getBoolean("default-simple-task-list"+tab, true));
                DSApi.context().userPreferences().node("task-list").put("sort-column_"+tab, DSApi.context().userPreferences().node("task-list").get("default-sort-column_"+tab, "receiveDate"));
                DSApi.context().userPreferences().node("task-list").put("ascending"+tab, DSApi.context().userPreferences().node("task-list").get("default-ascending"+tab, "true"));
                
                DSApi.context().systemPreferences().node("default-task-list").put("default-columns_"+tab, columnsDef);
                DSApi.context().systemPreferences().node("default-task-list").putBoolean("default-simple-task-list"+tab, DSApi.context().userPreferences().node("task-list").getBoolean("default-simple-task-list"+tab, true));
                DSApi.context().systemPreferences().node("default-task-list").put("default-sort-column_"+tab, DSApi.context().userPreferences().node("task-list").get("default-simple-task-list"+tab, "receiveDate"));
                DSApi.context().systemPreferences().node("default-task-list").put("default-ascending"+tab, DSApi.context().userPreferences().node("task-list").get("default-ascending"+tab, "true"));

                DSApi.context().commit();
                addActionMessage(sm.getString("PrzywroconoDomyslneUstawienia"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ResetDefaultForUser extends TransactionalActionListener {

        PreparedStatement ps;

        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(resetUser == null){
                throw new EdmException("Nie wybrano u�ytkownika");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            String[] tmp = TaskListUtils.getDefaultColumnProperties(tab, null);
            String columnsDef = StringUtils.join(tmp, ",");
            String prefKey = "columns_" + tab;

//            DSApi.context().userPreferences(resetUser).node("task-list")
//                    .put("columns_" + tab, columnsDef);
//            DSApi.context().userPreferences(resetUser).node("task-list")
//                    .putBoolean("simple-task-list" + tab,
//                            DSApi.context().userPreferences(resetUser).node("task-list").getBoolean("default-simple-task-list" + tab, true));
//            DSApi.context().userPreferences(resetUser).node("task-list")
//                    .put("sort-column_" + tab,
//                            DSApi.context().userPreferences(resetUser).node("task-list").get("default-sort-column_" + tab, "receiveDate"));
//            DSApi.context().userPreferences(resetUser).node("task-list")
//                    .put("ascending" + tab,
//                            DSApi.context().userPreferences(resetUser).node("task-list").get("default-ascending" + tab, "true"));
//
//            DSApi.context().systemPreferences().node("default-task-list")
//                    .put("default-columns_" + tab, columnsDef);
//            DSApi.context().systemPreferences().node("default-task-list")
//                    .putBoolean("default-simple-task-list" + tab, DSApi.context().userPreferences(resetUser).node("task-list").getBoolean("default-simple-task-list" + tab, true));
//            DSApi.context().systemPreferences().node("default-task-list")
//                    .put("default-sort-column_" + tab, DSApi.context().userPreferences(resetUser).node("task-list").get("default-simple-task-list" + tab, "receiveDate"));
//            DSApi.context().systemPreferences().node("default-task-list")
//                    .put("default-ascending" + tab, DSApi.context().userPreferences(resetUser).node("task-list").get("default-ascending" + tab, "true"));
//

            try {
                ps = DSApi.context().prepareStatement(
                    "UPDATE DS_PREFS_VALUE \n" +
                    " SET PREF_VALUE = ? \n" +
                    " WHERE \n" +
                    " PREF_KEY       = ? \n" +
                    " AND USERNAME   = ? \n" +
                    " AND NODE_ID IN ( \n" +
                    "   SELECT ID     \n" +
                    "   FROM DS_PREFS_NODE \n" +
                    "   WHERE NAME =  'task-list')");
                ps.setString(1, columnsDef);
                ps.setString(2, prefKey);
                ps.setString(3, resetUser);
                int rows = ps.executeUpdate();
                log.info("rows = " + rows);
            } finally {
                DSApi.context().closeStatement(ps);
            }
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            addActionMessage(sm.getString("PrzywroconoDomyslneUstawienia") + " dla " + resetUser + "/" + tab);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    /**
     * Przywracanie domyslnej listy kolumn.
     */
    private class Reset implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

//                String[] tmp = pl.compan.docusafe.web.office.tasklist.TaskListAction.getDefaultColumnProperties(tab,null);
//                String columnsDef = StringUtils.join(tmp, ",");

				Preferences defaultP = DSApi.context().systemPreferences()
								.node("default-task-list");

				if(defaultP == null){
					throw new EdmException(sm.getString("nieWybranoDomyslnychKolumn"));
				}

                DSApi.context()
						.userPreferences()
						.node("task-list")
						.put("columns_"+tab,
								defaultP
								.get("default-columns_"+tab,
							null));

                DSApi.context().userPreferences().node("task-list").putBoolean("simple-task-list"+tab, DSApi.context().systemPreferences().node("default-task-list").getBoolean("default-simple-task-list"+tab, true));

                if (!TAB_CASES.equals(tab))
                {
                    DSApi.context().userPreferences().node("task-list").put("sort-column_"+tab, DSApi.context().systemPreferences().node("default-task-list").get("default-sort-column_"+tab, "receiveDate"));
                    ascending = DSApi.context().systemPreferences().node("default-task-list").get("default-ascending"+tab, "false");
                    DSApi.context().userPreferences().node("task-list").put("ascending"+tab, DSApi.context().systemPreferences().node("default-task-list").get("default-ascending"+tab, "false"));
                }
                
                DSApi.context().commit();
                addActionMessage(sm.getString("PrzywroconoDomyslneUstawienia"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
            finally {
            	DSApi._close();
            }
        }
    }
    private class Synchronize implements ActionListener
    {
         public void actionPerformed(ActionEvent event)
         {
              try
              {
            	 DSApi.context().begin();
                 TasklistSynchro.Synchro(DSApi.context().getPrincipalName());
                 DSApi.context().commit();
              }
              catch (EdmException e)
              {
                  addActionError(e.getMessage());
              }
             
         }
    }

    public Map<String, String> getAvailableColumns()
    {
        return availableColumns;
    }

    public Map<String, String> getUserColumns()
    {
        return userColumns;
    }

    public Map<String, String> getAllColumns()
    {
        return allColumns;
    }
    
    public void setSelectedColumns(String[] selectedColumns)
    {
        this.selectedColumns = selectedColumns;
    }

    public String getTab()
    {
        return tab;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public Tabs getTabs()
    {
        return tabs;
    }

    public boolean isSimpleTaskList()
    {
        return simpleTaskList;
    }

    public void setSimpleTaskList(boolean simpleTaskList)
    {
        this.simpleTaskList = simpleTaskList;
    }
    
    public String getSortColumn()
    {
        return sortColumn;
    }
    
    public void setSortColumn(String sortColumn)
    {
        this.sortColumn = sortColumn;
    }
      
    public void setAscending(String arg)
    {
        ascending = arg;
    }
    
    public String getAscending()
    {
        return ascending;
    }

    public String[] getDefaultSelectedColumns()
    {
        return defaultSelectedColumns;
    }

    public void setDefaultSelectedColumns(String[] defaultSelectedColumns)
    {
        this.defaultSelectedColumns = defaultSelectedColumns;
    }

    public boolean isDefaultSimpleTaskList()
    {
        return defaultSimpleTaskList;
    }

    public void setDefaultSimpleTaskList(boolean defaultSimpleTaskList)
    {
        this.defaultSimpleTaskList = defaultSimpleTaskList;
    }

    public String getDefaultSortColumn()
    {
        return defaultSortColumn;
    }

    public void setDefaultSortColumn(String defaultSortColumn)
    {
        this.defaultSortColumn = defaultSortColumn;
    }

    public Map<String, String> getDefaultColumns()
    {
        return defaultColumns;
    }

    public void setDefaultColumns(Map<String, String> defaultColumns)
    {
        this.defaultColumns = defaultColumns;
    }

    public Map<String, String> getDefaultAvailableColumns()
    {
        return defaultAvailableColumns;
    }

    public void setDefaultAvailableColumns(Map<String, String> defaultAvailableColumns)
    {
        this.defaultAvailableColumns = defaultAvailableColumns;
    }

    public void setTabs(Tabs tabs)
    {
        this.tabs = tabs;
    }

    public Tabs getDefaultTabs()
    {
        return defaultTabs;
    }

    public void setDefaultTabs(Tabs defaultTabs)
    {
        this.defaultTabs = defaultTabs;
    }

	public Map<String, String> getAscendingType() {
		return ascendingType;
	}

	public void setAscendingType(Map<String, String> ascendingType) {
		this.ascendingType = ascendingType;
	}

	public int getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(int taskCount) {
		this.taskCount = taskCount;
	}

	public List<Integer> getTaskCounts() {
		return taskCounts;
	}

	public void setTaskCounts(List<Integer> taskCounts) {
		this.taskCounts = taskCounts;
	}

    public String getResetUser() {
        return resetUser;
    }

    public void setResetUser(String resetUser) {
        this.resetUser = resetUser;
    }

    public List<DSUser> getUserList() {
        return userList;
    }

    public void setUserList(List<DSUser> userList) {
        this.userList = userList;
    }
}
