package pl.compan.docusafe.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgencja;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.util.HttpUtils;

/**
 * Bartlomiej Spychalski mailto:bsp@spychalski.eu
 * cell:+48 606 352 029 http://www.spychalski.eu
 * Date: 2006-05-08
 * Time: 08:42:09
 */
public class Daa {


    public static final String DEFAULT_TITLE = "Dokument archiwum agenta";


        /**
     * pobiera rodzaj sieci dla dokumentu archiwum agenta
     * @param doctype
     * @param doctypeFields
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static Doctype.EnumItem getRODZAJ_SIECI(Doctype doctype, Map doctypeFields) throws EdmException {
        if(doctype == null || doctypeFields == null) return null;
        String valueRODZAJ_SIECI = HttpUtils.valueOrNull(doctypeFields.get(doctype.getFieldByCn("RODZAJ_SIECI").getId()));
        return valueRODZAJ_SIECI != null  ? doctype.getFieldByCn("RODZAJ_SIECI").getEnumItem(new Integer(valueRODZAJ_SIECI)) : null;
    }

     /**
     * pobiera rodzaj sieci dla dokumentu archiwum agenta
     * @param doctype
     * @param doctypeFields
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static Doctype.EnumItem getTYP_DOKUMENTU(Doctype doctype, Map doctypeFields) throws EdmException {
        if(doctype == null || doctypeFields == null) return null;
        String valueTYP_DOKUMENTU = HttpUtils.valueOrNull(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()));
        return valueTYP_DOKUMENTU != null ? doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(valueTYP_DOKUMENTU)) : null;
    }

    public static void daaDocument(Document document, DaaAgent agent, DaaAgencja agencja , Doctype.EnumItem siecEnum, String typ_dokumentu, String rodzaj_dokumentu, Doctype.EnumItem klasaRaportuEnum, boolean setDoctype) throws EdmException{

        Doctype doctype = Doctype.findByCn("daa");//doctype = Doctype.findByCn("daa");

        if (siecEnum == null) throw new EdmException("Nie okre�lono rodzaju sieci.");

        String siecCn = siecEnum.getCn();
        String siecTitle = siecEnum.getTitle();

        List<String> path = new ArrayList<String>();


        //ustawienie folderow
        Folder folder = Folder.getRootFolder();
        path.add("Archiwum Agenta");
        Doctype.EnumItem siecWlasnaValue = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItemByCn("SIEC_WLASNA");
        Doctype.EnumItem siecZewnetrznaValue = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItemByCn("SIEC_ZEWNETRZNA");
        Doctype.EnumItem siecBankiValue = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItemByCn("BANKI");
        //Doctype.EnumItem e = doctype.getFieldByCn("TYP_DOKUMENTU").getEnumItem(new Integer(doctypeFields.get(doctype.getFieldByCn("TYP_DOKUMENTU").getId()).toString()));

        //warunek na siec
        if (siecWlasnaValue != null  && siecCn.equals(siecWlasnaValue.getCn().toString())) {
            path.add("Sie� w�asna");
            if (rodzaj_dokumentu != null) {
                if ("RODZAJ_AGENCJA".equals(rodzaj_dokumentu)) {
                    if (agencja != null ) {
                        if(agencja.getNazwa() != null)
                            path.add(agencja.getNazwa().trim());
                        path.add("Dokumenty Biura");
                    }
                }
                if ("RODZAJ_AGENT".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        if(agencja.getNazwa() != null) path.add(agencja.getNazwa().trim());
                    }
                    if (agent != null) {
                        path.add("Dokumenty Specjalist�w");
                        String x = "";
                        if(agent.getNazwisko() != null && agent.getNazwisko().length() > 0) x += agent.getNazwisko().trim();
                        if(agent.getNazwisko() != null && agent.getImie() != null) x += " "; //spacja
                        if(agent.getImie() != null) x += agent.getImie().trim();
                        if(agent.getNumer() != null && agent.getNumer().length() > 0) x += " ("+agent.getNumer().trim()+")";
                        path.add(pobierzPrzedzial(x.toLowerCase().trim().charAt(0), 1));
                        path.add(x);
                    }
                }
                if ("RODZAJ_RAPORT".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        if(agencja.getNazwa() != null) path.add(agencja.getNazwa().trim());
                        path.add("Raporty Biura");
                        if(klasaRaportuEnum != null)
                            path.add(klasaRaportuEnum.getTitle().trim());
                    } else {
                        path.add("_Raporty");
                        if(klasaRaportuEnum != null) path.add(klasaRaportuEnum.getTitle().trim());
                    }

                }
            }

        }
        if (siecZewnetrznaValue != null && siecCn.equals(siecZewnetrznaValue.getCn().toString())) {
            path.add("Sie� zewn�trzna");
            if (rodzaj_dokumentu != null) {
                if ("RODZAJ_AGENCJA".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x = agencja.getNazwa().trim();

                        if(x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("g-l");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) <= 'f') {
                            path.add("a-f");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'l'){
                            path.add("g-l");
                        } else if(x.toLowerCase().trim().charAt(0) <= 's') {
                            path.add("m-s");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'z') {
                            path.add("t-z");
                        }  else path.add("a-f");

                        if((agencja.getNumer() != null && agencja.getNumer().length() >0) || (agencja.getNip() != null && agencja.getNip().length() > 0)) {
                            x += " (";
                            if(agencja.getNumer() != null && agencja.getNumer().length() > 0) {
                                x += agencja.getNumer().trim();
                                if(agencja.getNip() != null && agencja.getNip().length()>0) x += ", ";
                            }
                            if(agencja.getNip() != null && agencja.getNip().length() > 0) {
                                x += agencja.getNip().trim();
                            }
                            x += ")";
                        }
                        path.add(x);
                        path.add("Dokumenty Agencji");
                    }
                }
                if ("RODZAJ_AGENT".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x = agencja.getNazwa().trim();

                        if(x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("g-l");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) <= 'f') {
                            path.add("a-f");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'l'){
                            path.add("g-l");
                        } else if(x.toLowerCase().trim().charAt(0) <= 's') {
                            path.add("m-s");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'z') {
                            path.add("t-z");
                        }  else path.add("a-f");

                        if((agencja.getNumer() != null && agencja.getNumer().length() >0) || (agencja.getNip() != null && agencja.getNip().length() > 0)) {
                            x += " (";
                            if(agencja.getNumer() != null && agencja.getNumer().length() > 0) {
                                x += agencja.getNumer().trim();
                                if(agencja.getNip() != null && agencja.getNip().length()>0) x += ", ";
                            }
                            if(agencja.getNip() != null && agencja.getNip().length() > 0) {
                                x += agencja.getNip().trim();
                            }
                            x += ")";
                        }
                        path.add(x);
                    }
                    path.add("Dokumenty OWCA");
                    if(agent != null) {
                        String x = "";
                        if(agent.getNazwisko() != null && agent.getNazwisko().length()>0) x += agent.getNazwisko().trim();
                        if(agent.getNazwisko() != null && agent.getImie() != null) x += " ";
                        if(agent.getImie() != null && agent.getImie().length() >0) x += agent.getImie().trim();
                        if(agent.getNumer() != null && agent.getNumer().length() > 0) x += " ("+agent.getNumer().trim()+")";
                        path.add(pobierzPrzedzial(x.toLowerCase().trim().charAt(0), 1));
                        path.add(x);
                    }
                }
                if ("RODZAJ_RAPORT".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x = agencja.getNazwa().trim();

                        if(x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("a-f");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("g-l");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("m-s");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) == '�') path.add("t-z");
                        else if (x.toLowerCase().trim().charAt(0) <= 'f') {
                            path.add("a-f");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'l'){
                            path.add("g-l");
                        } else if(x.toLowerCase().trim().charAt(0) <= 's') {
                            path.add("m-s");
                        } else if(x.toLowerCase().trim().charAt(0) <= 'z') {
                            path.add("t-z");
                        }  else path.add("a-f");

                        if((agencja.getNumer() != null && agencja.getNumer().length() >0) || (agencja.getNip() != null && agencja.getNip().length() > 0)) {
                            x += " (";
                            if(agencja.getNumer() != null && agencja.getNumer().length() > 0) {
                                x += agencja.getNumer().trim();
                                if(agencja.getNip() != null && agencja.getNip().length()>0) x += ", ";
                            }
                            if(agencja.getNip() != null && agencja.getNip().length() > 0) {
                                x += agencja.getNip().trim();
                            }
                            x += ")";
                        }
                        path.add(x);
                        path.add("Raporty Agencji");
                        if(klasaRaportuEnum != null)
                            path.add(klasaRaportuEnum.getTitle().trim());
                    } else {
                        path.add("_Raporty");
                        if(klasaRaportuEnum != null)
                            path.add(klasaRaportuEnum.getTitle().trim());
                    }
                }
            }

        }

        if (siecBankiValue != null && siecCn.equals(siecBankiValue.getCn().toString())) {
            path.add("Banki");
            if (rodzaj_dokumentu != null) {
                if ("RODZAJ_AGENCJA".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x += agencja.getNazwa().trim();
                        if(agencja.getNip() != null && agencja.getNip().length() > 0) x += " ("+agencja.getNip().trim()+")";
                        path.add(x);
                        path.add("Dokumenty Banku");
                    }
                }
                if ("RODZAJ_AGENT".equals(rodzaj_dokumentu)) {
                    if (agencja != null) {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x += agencja.getNazwa().trim();
                        if(agencja.getNip() != null && agencja.getNip().length() > 0) x += " ("+agencja.getNip().trim()+")";
                        path.add(x);

                    }
                    path.add("Dokumenty OWCA");
                    if(agent != null) {
                        String x = "";
                        if(agent.getNazwisko() != null && agent.getNazwisko().length() > 0) x = agent.getNazwisko().trim();
                        if(agent.getNazwisko() != null && agent.getImie() != null) x += " ";
                        if(agent.getImie() != null && agent.getImie().length() >0) x += agent.getImie().trim();
                        if(agent.getNumer() != null && agent.getNumer().length() > 0) x += " ("+agent.getNumer().trim()+")";
                        path.add(pobierzPrzedzial(x.toLowerCase().trim().charAt(0), 1));
                        path.add(x);
                    }

                }
                if ("RODZAJ_RAPORT".equals(rodzaj_dokumentu)) {
                    if(agencja != null)
                    {
                        String x = "";
                        if(agencja.getNazwa() != null && agencja.getNazwa().length() > 0) x += agencja.getNazwa().trim();
                        if(agencja.getNip() != null && agencja.getNip().length() > 0) x += " ("+agencja.getNip().trim()+")";
                        path.add(x);
                        path.add("Raporty banku");
                        if(klasaRaportuEnum != null)
                            path.add(klasaRaportuEnum.getTitle().trim());
                    }
                    else  {
                        path.add("_Raporty");
                        if(klasaRaportuEnum != null)
                            path.add(klasaRaportuEnum.getTitle().trim());
                    }
                }
            }

        }


        //tytul i opis
        List rodzaje_sieci;
        Integer rodzaj_sieci;
        String description="";
        String title = typ_dokumentu;
        rodzaje_sieci = doctype.getFieldByCn("RODZAJ_SIECI").getEnumItems();
        //rodzaj_sieci = new Integer(siec).intValue();

        if("RODZAJ_AGENT".equals(rodzaj_dokumentu)) {
            if(agent != null) {
               String x = "";
                if((agent.getNazwisko() != null && agent.getNazwisko().length()>0) || (agent.getImie() != null && agent.getImie().length() >0)) {
                    x += " (";
                    if(agent.getNazwisko() != null && agent.getNazwisko().length()>0) {
                        x += agent.getNazwisko().trim();
                        if(agent.getImie() != null && agent.getImie().length() > 0) x += " ";
                    }
                    if(agent.getImie() != null && agent.getImie().length() > 0) x += agent.getImie().trim();
                    x += ")";
                }
                description = title + x;
            } else description = title+ " (nieprzypisany)";

        } else if("RODZAJ_AGENCJA".equals(rodzaj_dokumentu)) {
            if(agencja != null) {
                String x = "";
                if(agencja.getNazwa() != null && agencja.getNazwa().length() >0) x += " ("+agencja.getNazwa().trim()+")";
                description = title + x;
            } else description = title+ " (nieprzypisany)";
        } else if("RODZAJ_RAPORT".equals(rodzaj_dokumentu)) {
            if (agencja != null)     {
                String x = "";
                if(agencja.getNazwa() != null && agencja.getNazwa().length()>0) x += " ("+agencja.getNazwa().trim()+")";
                description = title + x;
            }
            else if (siecTitle != null )
                description = title + " ("+siecTitle.trim()+")";
        }
         ///////////
        document.setTitle(title);
        document.setDescription(description);
         /////////////
        for (Iterator iter = path.iterator(); iter.hasNext();) {
            String pathSegment = (String) iter.next();
            DSApi.context().grant(folder, new String[]{ObjectPermission.READ}, "DAA", ObjectPermission.GROUP);
            DSApi.context().grant(folder, new String[] { ObjectPermission.CREATE}, "DAA", ObjectPermission.GROUP);
            folder = folder.createSubfolderIfNotPresent(pathSegment);
        }

        document.setFolder(folder);

        if (setDoctype)
            document.setDoctype(doctype);

      /*  try {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) {
            throw new EdmHibernateException(ex);
        }     */

    }


    public static void daaDocumentPermission(Document document) throws EdmException{
        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "DAA", ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "DAA - obs�uga", ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS, ObjectPermission.DELETE}, "admin", ObjectPermission.GROUP);

        try {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) {
            throw new EdmHibernateException(ex);
        }
    }

    static char alfabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',  'r', 's', 't', 'u', 'w', 'x', 'y', 'z', ' ', ' '};



    static public String  pobierzPrzedzial(char s, int n) {
        int l = alfabet.length-1;
        int i = 0;
        int chk;
        char ss;
        String str = "a-b";

        while (i < l - n) {
            chk = i + n  ;
            if (chk > l) chk = l ;
            if(chk == 0) chk = 1;
            if (alfabet[i] <= s && alfabet[chk] >= s) {
                return "" + alfabet[i] + "-" + alfabet[chk];
            }
            if(s == '�') return "a-b";
            if(s == '�') return "c-d";
            if(s == '�') return "e-f";
            if(s == '�') return "k-l";
            if(s == '�') return "m-n";
            if(s == '�') return "o-p";
            if(s == '�') return "r-s";
            if(s == '�' || s ==  '�') return "�-�";
            i++;
            i += n;
        }
        return str;
    }
}
