package pl.compan.docusafe.web.reports;

import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * Klasa bazowa dla akcji zwiazanych z raportowaniem
 * @author wkutyla
 *
 */
public abstract class AbstractReportAction extends EventActionSupport 
{
	protected ReportEnvironment reportEnvironment = new ReportEnvironment();
}
