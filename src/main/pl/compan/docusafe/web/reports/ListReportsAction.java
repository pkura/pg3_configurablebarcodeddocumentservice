package pl.compan.docusafe.web.reports;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.archive.repository.ALDPaymentVerificationAction;
import pl.compan.docusafe.webwork.IRefreshAction;
import pl.compan.docusafe.webwork.event.*;

import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ListReportsAction.java,v 1.11 2009/08/10 09:16:54 mariuszk Exp $
 */
public class ListReportsAction extends AbstractReportAction implements IRefreshAction
{
	StringManager sm =
        GlobalPreferences.loadPropertiesFile(ListReportsAction.class.getPackage().getName(),null);
    // @EXPORT
    private List reports;

    private List<pl.compan.docusafe.reports.Report> newReports;
    private boolean hasUnfinishedReports;

    // @IMPORT
    private Long[] reportIds;
    private Long[] newReportIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    public int getRefreshTime()
    {
        return 60;
    }

    public boolean isRefresh()
    {
        return hasUnfinishedReports;
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                reports = Report.findByAuthor(DSApi.context().getPrincipalName());
                newReports = reportEnvironment.getReportsForUser(DSApi.context().getDSUser());

                for (Iterator iter=reports.iterator(); iter.hasNext(); )
                {
                    if (!((Report) iter.next()).isDone())
                    {
                        hasUnfinishedReports = true;
                        break;
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                LOG.error("",e);
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if ((reportIds == null || reportIds.length == 0)&&
            		(newReportIds == null || newReportIds.length == 0))
            {
                addActionError(sm.getString("NieZaznaczonoRaportowDoUsuniecia"));
                return;
            }

            try
            {

            	if(reportIds!=null)
            	{
	                DSApi.context().begin();
	                for (int i=0; i < reportIds.length; i++)
	                {
	                    Report.find(reportIds[i]).delete();
	                }
	                DSApi.context().commit();
            	}
            	if(newReportIds!=null)
            	{
	                DSApi.context().begin();
	                for (int i=0; i < newReportIds.length; i++)
	                {
	                    reportEnvironment.deleteReportById(newReportIds[i]);
	                }
	                DSApi.context().commit();
            	}
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getReports()
    {
        return reports;
    }

    public void setReportIds(Long[] reportIds)
    {
        this.reportIds = reportIds;
    }

	public List<pl.compan.docusafe.reports.Report> getNewReports() {
		return newReports;
	}

	public void setNewReports(List<pl.compan.docusafe.reports.Report> newReports) {
		this.newReports = newReports;
	}

	public Long[] getReportIds() {
		return reportIds;
	}

	public Long[] getNewReportIds() {
		return newReportIds;
	}

	public void setNewReportIds(Long[] newReportIds) {
		this.newReportIds = newReportIds;
	}
}
