package pl.compan.docusafe.web.reports;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ViewSpecifiedReportChartAction extends AbstractReportAction
{
	private static final long serialVersionUID = 1L;
	private Long reportId;
	private Logger log =  LoggerFactory.getLogger(ViewSpecifiedReportChartAction.class);
	private StringManager sm = StringManager.getManager(ViewSpecifiedReportChartAction.class.getPackage().getName());
	private String fileName;

	protected void setup()
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGetFile").
	        append(OpenHibernateSession.INSTANCE).
	        append(new StreamFile()).
	        appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

        }
    }

	private class StreamFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		String content;
        		File f = new File(reportEnvironment.getResultsDirForReportId(reportId),fileName);
        		if(fileName.endsWith("pdf"))
        			content = "application/pdf";
        		else if(fileName.endsWith("xml"))
        			content = "application/xml";
        		else if(fileName.endsWith("csv"))
        			content = "text/csv";
        		else if(fileName.endsWith("xls"))
        			content = "application/vnd.ms-excel";
        		else
        			content = "application/octet-stream";

        		ServletUtils.streamFile(ServletActionContext.getResponse(),f , content, "Content-Disposition: attachment; filename=\""+fileName+"\"");
        		event.setResult(NONE);
        	}
        	catch(Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError(sm.getString("NieUdaloSiePobracPliku"));
        	}
        }
    }

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
