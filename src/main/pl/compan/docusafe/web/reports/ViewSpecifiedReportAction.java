package pl.compan.docusafe.web.reports;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ViewSpecifiedReportAction extends AbstractReportAction
{
	private static final long serialVersionUID = 1L;
	private Long reportId;
	private Logger log =  LoggerFactory.getLogger(ViewSpecifiedReportAction.class);
	private StringManager sm = StringManager.getManager(ViewSpecifiedReportAction.class.getPackage().getName());

	private Map<String, String> printableParams = new LinkedHashMap<String, String>();
	private String status;
	private String orderUser;
	private List<String> files;
	private String fileName;
	private Report report;
	private String title;
	private String description;
	private String remoteKey;
	private boolean chartEnabled;

    protected void setup()
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doGetFile").
	        append(OpenHibernateSession.INSTANCE).
	        append(new StreamFile()).
	        appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	PreparedStatement ps = null;
        	ResultSet rs = null;
        	DocumentKind kind = null;
        	try
        	{
        		DSApi.context().begin();
        		ps = DSApi.context().prepareStatement("select * from ds_new_report where id=?");
        		ps.setLong(1, reportId);
        		rs = ps.executeQuery();
        		if(rs.next())
        		{
	        		File file = null;
	        		remoteKey = rs.getString("remote_key");
					if(DSApi.isPostgresServer()){
						InputStream is = rs.getBinaryStream("criteria");
						file = File.createTempFile("docusafe_report_", ".xml");
						OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
						byte[] buffer = new byte[1024];
						int count;
						while ((count = is.read(buffer)) > 0) {
							os.write(buffer, 0, count);
						}
						org.apache.commons.io.IOUtils.closeQuietly(is);
						org.apache.commons.io.IOUtils.closeQuietly(os);
					} else {
						Blob blob = rs.getBlob("criteria");
						if (blob != null) {
							InputStream is = blob.getBinaryStream();
							file = File.createTempFile("docusafe_report_", ".xml");
							OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
							byte[] buffer = new byte[1024];
							int count;
							while ((count = is.read(buffer)) > 0) {
								os.write(buffer, 0, count);
							}
							org.apache.commons.io.IOUtils.closeQuietly(is);
							org.apache.commons.io.IOUtils.closeQuietly(os);
						}
					}
        			SAXReader sax = new SAXReader();

        			report = reportEnvironment.getReportInstanceByCn(rs.getString("report_cn"));
        			report.init(sax.read(file),rs.getString("report_cn"));
        			report.setId(reportId);

        			orderUser = DSUser.findByUsername(rs.getString("username")).asFirstnameLastname();
        			report.setStatus(rs.getInt("status"));
    				status = report.getTextStatus();

    				title = (rs.getString("title"));
    				description = (rs.getString("description"));
    				files = Arrays.asList(reportEnvironment.getResultsDirForReport(report).list());
    				chartEnabled = false;
    				for (String f : files) {
    					if(chartEnabled = f.endsWith(".jpg")) break;
					}
        			if(report.getDockindCn()!=null){
        				kind = DocumentKind.findByCn(report.getDockindCn());
                        assert kind != null;
                    }
        			for(ReportParameter reportParameter : report.getParams()){
        				String value = "";//(reportParameter.getValue()!= null ? reportParameter.getValue().toString() : "");
        				Field f;
        				if("dockind".equals(reportParameter.getType()) && kind!=null)
    					{
    						f = kind.getFieldByCn(reportParameter.getDockindCn());
    						if(f.getType().equals("enum")||f.getType().equals("enum-ref") || Field.DATA_BASE.equals(f.getType()))
    						{
	    						if(reportParameter.getValue() instanceof String[])
	            				{
	            					for(String s: (String[]) reportParameter.getValue())
	            					{
	            						value += f.getEnumItem(Integer.parseInt(s)).getTitle()+", ";
	            					}
	            				}
	    						else if(reportParameter.getValue() instanceof String)
	    						{
	    							value = f.getEnumItem(Integer.parseInt((String) reportParameter.getValue())).getTitle();
	    						}
    						}
    					}
        				else if("division".equals(reportParameter.getType()))
        				{
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value += DSDivision.find(s).getName()+", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							value = DSDivision.find((String) reportParameter.getValue()).getName();
    						}
        				}
        				else if("date".equals(reportParameter.getType()))
        				{
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value +=s+", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							value = (String) reportParameter.getValue();
    						}
        				}
        				else if("hour".equals(reportParameter.getType()))
        				{
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value +=s+", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							value = (String) reportParameter.getValue();
    						}
        				}
        				else if("user".equals(reportParameter.getType()))
        				{
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value +=DSUser.findByUsername(s).asFirstnameLastname()+", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							value = DSUser.findByUsername((String) reportParameter.getValue()).asFirstnameLastname();
    						}
        				}
        				else if("select".equals(reportParameter.getType()))
        				{
        					//System.out.println("select");
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value += reportParameter.getAvailableValues().get(s) != null ? reportParameter.getAvailableValues().get(s) : s + ", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							//System.out.println(reportParameter.getValue());
    							value = reportParameter.getAvailableValues().get(reportParameter.getValue()) != null ? reportParameter.getAvailableValues().get(reportParameter.getValue()) : reportParameter.getValue().toString();
    						}
        					//System.out.println(value);
        				}
        				else if("bool".equals(reportParameter.getType()))
        				{
        					value = ""+(reportParameter.getValue()!= null);
        				}
        				else if("text".equals(reportParameter.getType()))
        				{
        					if(reportParameter.getValue() instanceof String[])
            				{
            					for(String s: (String[]) reportParameter.getValue())
            					{
            						value +=s+", ";
            					}
            				}
    						else if(reportParameter.getValue() instanceof String)
    						{
    							value = (String) reportParameter.getValue();
    						}
        				}

        				if(reportParameter.getVisible())
        				{
        					printableParams.put(report.getLanguageValue(reportParameter.getLabel()), value);
        				}

        			}

        		}
        		else
        			addActionError(sm.getString("NieIstniejeRaportOPodanymId"));
        		DbUtils.closeQuietly(rs);
        		DSApi.context().commit();

        	}
        	catch(Exception e)
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(e.getMessage());
        		log.error(e.getMessage(),e);
        		return;
        	}
        	finally
        	{
        		DSApi.context().closeStatement(ps);
        	}

        }
    }

	private class StreamFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		//System.out.println(fileName);
        		String content;
        		File f = new File(reportEnvironment.getResultsDirForReportId(reportId),fileName);
        		if(fileName.endsWith("pdf"))
        			content = "application/pdf";
        		else if(fileName.endsWith("xml"))
        			content = "application/xml";
        		else if(fileName.endsWith("csv"))
        			content = "text/csv";
        		else if(fileName.endsWith("xls"))
        			content = "application/vnd.ms-excel";
        		else
        			content = "application/octet-stream";

        		ServletUtils.streamFile(ServletActionContext.getResponse(),f , content, "Content-Disposition: attachment; filename=\""+fileName+"\"");
        		event.setResult(NONE);
        	}
        	catch(Exception e)
        	{
        		log.error(e.getMessage(),e);
        		addActionError(sm.getString("NieUdaloSiePobracPliku"));
        	}
        }
    }

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public Map<String, String> getPrintableParams() {
		return printableParams;
	}

	public void setPrintableParams(Map<String, String> printableParams) {
		this.printableParams = printableParams;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderUser() {
		return orderUser;
	}

	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemoteKey() {
		return remoteKey;
	}

	public void setRemoteKey(String remoteKey) {
		this.remoteKey = remoteKey;
	}

	public boolean isChartEnabled() {
		return chartEnabled;
	}

	public void setChartEnabled(boolean chartEnabled) {
		this.chartEnabled = chartEnabled;
	}
}
