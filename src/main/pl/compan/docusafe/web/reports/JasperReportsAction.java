package pl.compan.docusafe.web.reports;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.UserToJasperReports;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsKind;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.service.reports.dockind.DockindReportCriteria;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.admin.UserToJasperReportsAction.ValueToList;
import pl.compan.docusafe.web.archive.settings.ApplicationSettingsAction;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument.Restriction;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

public class JasperReportsAction extends EventActionSupport {
	
	private static final Logger log = LoggerFactory.getLogger(JasperReportsAction.class);

    private String report;
    private Map<String,String> listOfReport;

    private String reportType;
    private Map<String,String> listOfReportType;

    private String startDate;
    private String koniecDate;
    private String user;
	private Long userId;
    private List users;
    private static Boolean isSupervisor = false;

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
                append(OpenHibernateSession.INSTANCE).
                append(new Validate()).
                append(new InitReport()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

	 public Boolean getIsSupervisor() {
			return isSupervisor;
		}

		public void setIsSupervisor(Boolean isSupervisor) {
			this.isSupervisor = isSupervisor;
		}

    public Map<String, String> getListOfReportType() {
        return listOfReportType;
    }

    public void setListOfReportType(Map<String, String> listOfReportType) {
        this.listOfReportType = listOfReportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public Map<String, String> getListOfReport() {
        return listOfReport;
    }

    public void setListOfReport(Map<String, String> listOfReport) {
        this.listOfReport = listOfReport;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setKoniecDate(String koniecDate) {
        this.koniecDate = koniecDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getKoniecDate() {
        return koniecDate;
    }
    
    public Long getUserId() {
    	return userId;
    }
    
    public void setUserId(Long userId) {
    	this.userId = userId;
    }
    
    public List getUsers() {
		return users;
	}

	public void setUsers(List users) {
		this.users = users;
	}
	
	 public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try {
				//users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
				// pobiera list� u�ytkownikow z systemu
				
				users = DSUser.getSupervisorUsers(DSApi.context().getDSUser());
				if((users==null) || (users.isEmpty())) {
					isSupervisor = false;
				} else {
					isSupervisor = true;
				}
				
			} catch (EdmException e) {
				addActionError(e.getMessage());
				log.error("", e);
			}
        	
            listOfReportType = new HashMap<String, String>();
            listOfReportType.put("PDF","PDF");
            listOfReportType.put("HTML","HTML");
            listOfReportType.put("XLS","XLS");
            listOfReportType.put("XML","XML");
            listOfReportType.put("CSV", "CSV");
            listOfReportType.put("RTF", "RTF");

            listOfReport = new HashMap<String, String>();

			Long userId = null;
			String userName = null;
			try {
				userId = DSApi.context().getDSUser().getId();
				userName = DSApi.context().getDSUser().getName();
			} catch (Exception e) {	
				log.error("", e);
			}
			
			try {
				List<JasperReportsKind> kindList = JasperReportsKind.list();
				for (JasperReportsKind rep : kindList) {

					if (GlobalPreferences.getAdminUsername().equals(userName)) {
						listOfReport.put(rep.getPath(), rep.getTitle());
					} else {
						List<UserToJasperReports> list = getUsersToJasperReportsList(userId);
						for (UserToJasperReports utjr : list) {
							if (utjr.getReportId().equals(rep.getId())) {
								listOfReport.put(rep.getPath(), rep.getTitle());

							}
						}
					}
				}

			} catch (EdmException e) {
				addActionError("Error: " + e);
			}
        }
    }


	private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
//            dateFrom = TextUtils.trimmedStringOrNull(dateFrom);
//            dateTo = TextUtils.trimmedStringOrNull(dateTo);
//            invoiceDateFrom = TextUtils.trimmedStringOrNull(invoiceDateFrom);
//            invoiceDateTo = TextUtils.trimmedStringOrNull(invoiceDateTo);
        }
    }

    private class InitReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                JasperReportsLogic jasper = null;
                jasper = new JasperReportsLogic();

                DateFormat dateFrm = new SimpleDateFormat("dd-MM-yyyy");

                Date startDateFormatted = null;
                Date endDateFormatted = null;
                if (startDate!=null)
                    startDateFormatted = dateFrm.parse(startDate);
                if (koniecDate != null)
                    endDateFormatted = dateFrm.parse(koniecDate);
                   
                if(user != null) {
                	userId =  DSUser.findByUsername(user).getId();
                } else {
                	userId = DSApi.context().getDSUser().getId();
                }
                
                File archivePackage = jasper.getReport(reportType, report, startDateFormatted, endDateFormatted, userId);
                if(archivePackage!=null){
                    String now = DateUtils.formatFolderDate(new Date());
                    ServletUtils.streamFile(ServletActionContext.getResponse(), archivePackage, "application/application",
                            "Content-Disposition: attachment; filename=\"report_" + now + "." + archivePackage.getName().split("[.]")[1]+"\"");
                }

            }
            catch (Exception e)
            {
                addActionError("Error: " + e);
            }
        }
    }
    
    public static List<UserToJasperReports> getUsersToJasperReportsList(Long userId) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UserToJasperReports.class);
		criteria.add(Restrictions.eq("userId", userId));		
		return criteria.list();
	}
}
