package pl.compan.docusafe.web.reports;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.logging.LogFactory;
import org.xml.sax.InputSource;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.reports.*;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.*;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewReportAction.java,v 1.29 2009/07/23 11:54:55 mariuszk Exp $
 */
public class ViewReportAction extends EventActionSupport
{
    private Long id;
    private boolean show;
    private boolean csv;
    private boolean csv_only;

    // @EXPORT
    private Report report;
    private String author;
    private String html; // raport wygenerowany w html
    private String pdfLink;
    private String csvLink;

    private static final String FAILED_XSL = "pl/compan/docusafe/web/reports/report-failed-html";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
            {
                addActionError("Nie podano identyfikatora raportu");
                return;
            }

            try
            {
                DSApi.context().begin();

                Report r = Report.find(id);

                if (!DSApi.context().getDSUser().isAdmin() &&
                    !DSApi.context().getPrincipalName().equals(r.getAuthor()))
                    throw new EdmException("Raport nie nale�y do zalogowanego u�ytkownika");


                String xsl;
                String foXsl;
                String csvXsl;

                File csvReport = pl.compan.docusafe.core.dockinds.Report.getCSVreport(id);
                StringWriter sw = new StringWriter();

                if (csvReport != null) {
                	csvLink = "/reports/view.action?csv=true&csv_only=true&id="+id;
                	pdfLink = null;
                } else {

	                if (r.isFailed())
	                {
	                    // raporty, kt�rych generowanie si� nie uda�o, s� pokazywane
	                    // jednym, wsp�lnym xslem
	                    xsl = FAILED_XSL;
	                }

	                else
	                {
	                    xsl = ReportGenerator.getReportProperty(r.getReportId(), "htmlXslResource");
	                    foXsl = ReportGenerator.getReportProperty(r.getReportId(), "foXslResource");
	                    csvXsl = ReportGenerator.getReportProperty(r.getReportId(), "csvXslResource");

	                    if (foXsl != null)
	                    {
	                        pdfLink = "/reports/view-pdf.action?id="+r.getId()+"&xsl=cl:"+foXsl+"_"+Docusafe.getCurrentLanguage()+".xsl";
	                    }
	                    if (csv)
	                        xsl = csvXsl;
	                    else if (csvXsl != null)
	                        csvLink = "/reports/view.action?csv=true&id="+r.getId()+"&xsl=cl:"+csvXsl+"_"+Docusafe.getCurrentLanguage()+".xsl";
	                }

	                if (r.isDone() && xsl != null)
	                {
	                    TransformerFactory tf = TransformerFactory.newInstance();
	                    InputStream is = ViewReportAction.class.getClassLoader().getResourceAsStream(xsl+"_"+Docusafe.getCurrentLanguage()+".xsl");
	                    Transformer t = tf.newTransformer(new SAXSource(new InputSource(is)));

	                    t.setParameter("repository-document-link",
	                        ServletActionContext.getRequest().getContextPath()+
	                        "/repository/edit-document.action?id=");
	                    t.setParameter("office-in-document-link",
	                        ServletActionContext.getRequest().getContextPath()+
	                        "/office/incoming/summary.action?documentId=");


	                    t.transform(new StreamSource(r.getXmlAsFile()), new StreamResult(sw));
	                }
                }
                    if (csv)
                    {
                        // nie pokazujemy strony HTML, ale zwracamy plik CSV
                    	File tmp = File.createTempFile("docusafe_", ".csv");
                    	if (isCsv_only()) {
                    		FileUtils.copyFileToFile(csvReport, tmp);
                    		LogFactory.getLog("kuba").debug("dlugosc: "+csvReport.length());
                    	} else {
                    		FileWriter out = new FileWriter(tmp);
                            out.write(sw.toString());
                            out.close();
                    	}
                    	ServletUtils.streamFile(ServletActionContext.getResponse(), tmp, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"raport.csv\"");
                        event.setResult(NONE);
                    }
                    else
                    	if (!isCsv_only()) {
                    		html = sw.toString();
                    	}
                report = r;
                author = DSUser.safeToFirstnameLastname(report.getAuthor());

                DSApi.context().commit();
            }
            catch (Exception e)
            {
                event.getLog().error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Report getReport()
    {
        return report;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getHtml()
    {
        return html;
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
    }

    public String getPdfLink()
    {
        return pdfLink;
    }

    public String getCsvLink()
    {
        return csvLink;
    }

    public void setCsv(boolean csv)
    {
        this.csv = csv;
    }

	public void setCsv_only(boolean csv_only) {
		this.csv_only = csv_only;
	}

	public boolean isCsv_only() {
		return csv_only;
	}
}
