<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
	  <xsl:output method="xml" indent="yes"/>
	<xsl:template match="/raport">
    <fo:root font-family="Arial">
    
	<fo:layout-master-set>
		<fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="2cm">
			<fo:region-body/>
		</fo:simple-page-master>
	</fo:layout-master-set>
      
	<fo:page-sequence master-reference="A4-portrait">
        
	<fo:flow flow-name="xsl-region-body">
			
		<fo:block font-size="10pt" text-align="right">Raport grupowej zmiany atrybut�w.</fo:block>
		
		<xsl:apply-templates select="info"/>
		<xsl:apply-templates select="tresc"/>
				
	</fo:flow>
		
    </fo:page-sequence>
    
	</fo:root>
  </xsl:template>
  
  <xsl:template match="info">
  	<fo:table table-layout="fixed" width="60%" space-after.optimum="13pt" padding-top="1cm">
    <fo:table-column />
    <fo:table-column />
    <fo:table-body>
        <fo:table-row>
        	<fo:table-cell><fo:block font-family="Arial"><fo:inline font-weight="bold">Zamiany zleci�:</fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial"><xsl:value-of select="who"/></fo:block></fo:table-cell>
        </fo:table-row>    
        <fo:table-row>
        	<fo:table-cell><fo:block font-family="Arial"><fo:inline font-weight="bold">Zmiany dokonano:</fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial"><xsl:value-of select="when"/></fo:block></fo:table-cell>
        </fo:table-row>    
        <fo:table-row>
        	<fo:table-cell><fo:block font-family="Arial"><fo:inline font-weight="bold">Ilo�� dokument�w:</fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial"><xsl:value-of select="count"/></fo:block></fo:table-cell>
        </fo:table-row>        
    </fo:table-body>
  </fo:table>  	
  </xsl:template>
  
  <xsl:template match="tresc">
  	<fo:table table-layout="fixed" width="100%" space-after.optimum="13pt" padding-top="1cm" border-collapse="collapse">
  		<fo:table-column column-width="3cm"/>
    	<fo:table-column />
    	<fo:table-column />
    	<fo:table-column />
    	<fo:table-body>
    		<fo:table-row>
    			<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><fo:inline font-weight="bold">Id dokumentu</fo:inline></fo:block></fo:table-cell>
    			<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><fo:inline font-weight="bold">Pole</fo:inline></fo:block></fo:table-cell>
    			<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><fo:inline font-weight="bold">Stara warto��</fo:inline></fo:block></fo:table-cell>
    			<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><fo:inline font-weight="bold">Nowa warto��</fo:inline></fo:block></fo:table-cell>
    		</fo:table-row>
    		<xsl:for-each select="document">
				<xsl:for-each select="zmiana">
					<fo:table-row>
						<fo:table-cell padding="2pt">
							<xsl:choose>
								<xsl:when test="position()=last()">
									<xsl:attribute name="border-bottom">1pt solid black</xsl:attribute>
									<xsl:attribute name="border-left">1pt solid black</xsl:attribute>
									<xsl:attribute name="border-right">1pt solid black</xsl:attribute>
								</xsl:when>
								<xsl:when test="position()=1">
									<xsl:attribute name="border-top">1pt solid black</xsl:attribute>
									<xsl:attribute name="border-left">1pt solid black</xsl:attribute>
									<xsl:attribute name="border-right">1pt solid black</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="border-left">1pt solid black</xsl:attribute>
									<xsl:attribute name="border-right">1pt solid black</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="position()=1"><fo:block font-family="Arial"><xsl:value-of select="../@id"/></fo:block></xsl:if>
						</fo:table-cell>
						<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><xsl:value-of select="@field"/></fo:block></fo:table-cell>
						<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><xsl:value-of select="@oldval"/></fo:block></fo:table-cell>
						<fo:table-cell padding="2pt" border="1pt solid black"><fo:block font-family="Arial"><xsl:value-of select="@newval"/></fo:block></fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</xsl:for-each>
    	</fo:table-body>
  	</fo:table>
  </xsl:template>
  
</xsl:stylesheet>