package pl.compan.docusafe.web.reports.office;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.ProcessReport;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.*;

/**
 * Raport proces�w
 *
 * @author Wiktor Ocet
 */
@SuppressWarnings("unused")
public class ProcessReportAction extends EventActionSupport {

    // @IMPORT
    public static final String EV_REPORT = "report";
    protected static final Logger log = LoggerFactory.getLogger(ProcessReportAction.class);
    private StringManager smL;
    //
    //
    // @EXPORT/@IMPORT
    //Long - warto�� wprowadzana przez u�ytkownika
    private String documentId;
    //Date - warto�� wprowadzana przez u�ytkownika
    private String intervalFrom;
    private String intervalTo;
    //Long - warto�� s�ownikowa
    private Long documentKind;
    private Long division;
    private Long user;
    //Boolean
    private Boolean onlyNotCompleted;
    private Boolean onlyDelayed;
    //
    //
    // @EXPORT
    private Map<Long, String> userMap;
    private Map<Long, String> documentKindMap;
    private Map<Long, String> divisionMap;
    private List<DependentValue> statusList;
    private Long[] status;
    private String durationDays = null;
    private String durationHours = null;
    private String durationMinutes = null;
    private String delayDays = null;
    private String delayHours = null;
    private String delayMinutes = null;
    private boolean canCreateReports;
    // identyfikator raportu u�ywany przy przekierowaniu do /view.action?id=...
    private Long id;

    public static String getEvReport() {
        return EV_REPORT;
    }

    public static boolean isLong(String value) {
        try {
            long lValue = Long.parseLong(value);
            return true;
        } catch (Exception e) {
            //do nothing - it's info about invalid format value
            return false;
        }
    }


    /**
     * @param value if value is not null it's adding to criteria
     */
    public static void addTo(ReportCriteria criteria, String name, Object value) {
        if (value != null)
            criteria.addEq(name, value);
    }

    public static void addRangeTo(ReportCriteria criteria, String name, Object from, Object to) {
        if (from != null || to != null)
            criteria.addRange(name, from, to);
    }

    public static Logger getLog() {
        return log;
    }

    private static Long getTime(String daysString, String hoursString, String minutesString) {
        Long days = parseToLong(daysString);
        Long hours = parseToLong(hoursString);
        Long minutes = parseToLong(minutesString);

        if (days == null && hours == null && minutes == null)
            return null;

        Long millis = 0L;
        if (days != null)
            millis += days * DateUtils.DAY;
        if (hours != null)
            millis += hours * DateUtils.HOUR;
        if (minutes != null)
            millis += minutes * DateUtils.MINUTE;
        return millis;
    }

    /**
     * @return long or null
     */
    public static Long parseToLong(String value) {
        try {
            if (StringUtils.isNotEmpty(value)) {
                return Long.parseLong(value);
            }
        } catch (Exception e) {
            //do nothing
        }
        return null;
    }

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
                append(OpenHibernateSession.INSTANCE).
                append(new Validate()).
                append(EV_REPORT, new DoReport()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isCanCreateReports() {
        return canCreateReports;
    }

    public void setCanCreateReports(boolean canCreateReports) {
        this.canCreateReports = canCreateReports;
    }

    private LinkedHashMap<Long, String> getEnumUsersMap() throws EdmException {
    	LinkedHashMap<Long, String> map = new LinkedHashMap<Long, String>();
//        List<DSUser> users =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
    	List<DSUser> users = UserFactory.getInstance().getCanAssignUsers();
		Collections.sort(users, new DSUser.UserComparatorAsLastFirstName());    	
        for (DSUser u : users)
            map.put(u.getId(), u.getLastnameFirstnameWithOptionalIdentifier());
        return map; 	 	
    }

    private LinkedHashMap<Long, String> getEnumDivisionsMap() throws EdmException {
    	LinkedHashMap<Long, String> map = new LinkedHashMap<Long, String>();
        List<DSDivision> divisions = UserFactory.getInstance().getAllCanAssignDivisionsDivisionType();
		Collections.sort(divisions, new DSDivision.DivisionComparatorAsName());
        for (DSDivision d : divisions)
            map.put(d.getId(), d.getName());
        return map;
    }

    private Map<Long, String> getEnumDocumentKindMap() throws EdmException {
        Map<Long, String> map = new HashMap<Long, String>();
        List<DocumentKind> documentKinds = DocumentKind.list(true);
        for (DocumentKind dk : documentKinds)
            map.put(dk.getId(), dk.getName());
        return map;
    }

    private List<DependentValue> getStatusList(Map<Long, String> documentKind) {
        List<DependentValue> status = new ArrayList<DependentValue>();
        for (Map.Entry<Long, String> dkME : documentKind.entrySet()) {
            try {
                DocumentKind dk = DocumentKind.find(dkME.getKey());
                Field field = dk.getFieldByCn("STATUS");
                if (field != null) {
                    List<EnumItem> enums = field.getEnumItems();
                    for (EnumItem e : enums)
                        if (!isListContains(status, e.getTitle()))
                            status.add(new DependentValue((long) e.getId(), e.getTitle(), dkME.getKey()));
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        return status;
    }

    /**
     * Returns <tt>true</tt> if this list is not null and contains the specified element <tt>value</tt>.
     * More formally, returns <tt>true</tt> if and only if this list contains
     * at least one element <tt>e</tt> such that
     * <tt>(list!=null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;e.equals(value))</tt>.
     *
     * @param list element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list contains the specified element
     */
    public static <L, V> boolean isListContains(final List<L> list, final V value) {
        if (list != null)
            for (L l : list)
                if (l.equals(value))
                    return true;
        return false;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getIntervalFrom() {
        return intervalFrom;
    }

    public void setIntervalFrom(String intervalFrom) {
        this.intervalFrom = intervalFrom;
    }

    public String getIntervalTo() {
        return intervalTo;
    }

    public void setIntervalTo(String intervalTo) {
        this.intervalTo = intervalTo;
    }

    public Long getDocumentKind() {
        return documentKind;
    }

    public void setDocumentKind(Long documentKind) {
        this.documentKind = documentKind;
    }

    public Long getDivision() {
        return division;
    }

    public void setDivision(Long division) {
        this.division = division;
    }

    public Long getUser() {
        return user;
    }

    public void setOnlyDelayed(Boolean onlyDelayed) {
        this.onlyDelayed = onlyDelayed;
    }

    public Boolean getOnlyDelayed() {
        return onlyDelayed;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public void setDurationDays(String durationDays) {
        this.durationDays = durationDays;
    }

    public void setDelayDays(String delayDays) {
        this.delayDays = delayDays;
    }

    public String getDurationDays() {
        return durationDays;
    }

    public String getDelayDays() {
        return delayDays;
    }

    public Boolean getOnlyNotCompleted() {
        return onlyNotCompleted;
    }

    public void setOnlyNotCompleted(Boolean onlyNotCompleted) {
        this.onlyNotCompleted = onlyNotCompleted;
    }

    public Long[] getStatus() {
        return status;
    }

    public void setStatus(Long[] status) {
        this.status = status;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(String durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public String getDelayHours() {
        return delayHours;
    }

    public void setDelayHours(String delayHours) {
        this.delayHours = delayHours;
    }

    public String getDelayMinutes() {
        return delayMinutes;
    }

    public void setDelayMinutes(String delayMinutes) {
        this.delayMinutes = delayMinutes;
    }


    private Map<String,String> getStatusCnTitleMap(){
        if (documentKind != null) {
            boolean opened = false;
            try {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    opened = true;
                }
                DocumentKind dk = DocumentKind.find(documentKind);
                Field field = dk.getFieldByCn("STATUS");
                if (field != null) {
                    List<String> statusTitles = new ArrayList<String>();
                    Map<String,String> statusCnTitleMap = new HashMap<String, String>();
                    List<EnumItem> enums = field.getEnumItems();
                    for (EnumItem e : enums)
                        if (isArrayContains(status, (long) (int) e.getId()))
                            statusTitles.add(e.getTitle());
                    for (EnumItem e : enums)
                        if (statusTitles.contains(e.getTitle()))
                                statusCnTitleMap.put(e.getCn(),e.getTitle());
                    if (!statusCnTitleMap.isEmpty())
                        return statusCnTitleMap;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            } finally {
                if (opened)
                    DSApi._close();
            }
        }
        return null;
    }

//    private String[] getStatusCns() {
//        return getStatus(1);
//    }
//
//    private String [] getStatusTitles(){
//        return getStatus(0);
//    }
//
//    /**
//     * @param mode 0 - titles, 1 - CNs
//     * @return
//     */
//    private String [] getStatus(int mode){
//        if (documentKind != null) {
//            boolean opened = false;
//            try {
//                if (!DSApi.isContextOpen()) {
//                    DSApi.openAdmin();
//                    opened = true;
//                }
//                DocumentKind dk = DocumentKind.find(documentKind);
//                Field field = dk.getFieldByCn("STATUS");
//                if (field != null) {
//                    List<String> statusTitles = new ArrayList<String>();
//                    List<String> statusCns = new ArrayList<String>();
//                    List<EnumItem> enums = field.getEnumItems();
//                    for (EnumItem e : enums)
//                        if (isArrayContains(status, (long) (int) e.getId()))
//                            statusTitles.add(e.getTitle());
//                    switch(mode){
//                    case 0:
//                        if (statusTitles.size() > 0)
//                            return statusTitles.toArray(new String[statusTitles.size()]);
//                        break;
//                    case 1:
//                        for (EnumItem e : enums)
//                            if (statusTitles.contains(e.getTitle()))
//                                statusCns.add(e.getCn());
//                        if (statusCns.size() > 0)
//                            return statusCns.toArray(new String[statusCns.size()]);
//                        break;
//                    }
//                }
//            } catch (Exception e) {
//                log.error(e.getMessage(), e);
//            } finally {
//                if (opened)
//                    DSApi._close();
//            }
//        }
//        return null;
//    }

    /**
     * Returns <tt>true</tt> if this array is not null and contains the specified element <tt>value</tt>.
     * More formally, returns <tt>true</tt> if and only if this array contains
     * at least one element <tt>e</tt> such that
     * <tt>(array!=null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;e.equals(value))</tt>.
     *
     * @param array element whose presence in this array is to be tested
     * @return <tt>true</tt> if this list contains the specified element
     */
    public static <A, V> boolean isArrayContains(final A[] array, final V value) {
        if (array != null)
            for (A a : array)
                if (a != null && a.equals(value))
                    return true;
        return false;
    }

    public Map<Long, String> getUserMap() {
        return userMap;
    }

    public void setUserMap(Map<Long, String> userMap) {
        this.userMap = userMap;
    }

    public Map<Long, String> getDocumentKindMap() {
        return documentKindMap;
    }

    public void setDocumentKindMap(Map<Long, String> documentKindMap) {
        this.documentKindMap = documentKindMap;
    }

    public Map<Long, String> getDivisionMap() {
        return divisionMap;
    }

    public void setDivisionMap(Map<Long, String> divisionMap) {
        this.divisionMap = divisionMap;
    }

    public List<DependentValue> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<DependentValue> statusList) {
        this.statusList = statusList;
    }

    public StringManager getSmL() {
        return smL;
    }

    public void setSmL(StringManager smL) {
        this.smL = smL;
    }

    public static class DependentValue {
        private Long id;
        private String title;
        private Long refValue;

        public DependentValue() {
        }

        public DependentValue(Long id, String title, Long refValue) {
            this.id = id;
            this.title = title;
            this.refValue = refValue;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof String)
                if (title.equalsIgnoreCase((String) obj))
                    return true;
            return super.equals(obj);
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Long getRefValue() {
            return refValue;
        }

        public void setRefValue(Long refValue) {
            this.refValue = refValue;
        }
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                documentKindMap = getEnumDocumentKindMap();
                userMap = getEnumUsersMap();
                divisionMap = getEnumDivisionsMap();
                statusList = getStatusList(documentKindMap);

                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
            } catch (EdmException e) {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

            //Long - sprawdzenie typu
            //private String documentId;
            //private String duration;
            //private String delay;
            if (StringUtils.isNotEmpty(documentId) && !isLong(documentId))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDocumentId")));
            if (StringUtils.isNotEmpty(durationDays) && !isLong(durationDays))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("durationDays"), smL.getString("prfnDays")));
            if (StringUtils.isNotEmpty(durationHours) && !isLong(durationHours))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDuration"), smL.getString("prfnHours")));
            if (StringUtils.isNotEmpty(durationMinutes) && !isLong(durationMinutes))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDuration"), smL.getString("prfnMinutes")));
            if (StringUtils.isNotEmpty(delayDays) && !isLong(delayDays))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDelay"), smL.getString("prfnDays")));
            if (StringUtils.isNotEmpty(delayHours) && !isLong(delayHours))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDelay"), smL.getString("prfnHours")));
            if (StringUtils.isNotEmpty(delayMinutes) && !isLong(delayMinutes))
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnDelay"), smL.getString("prfnMinutes")));

            //Date - sprawdzenie typu
            //private String intervalFrom;
            //private String intervalTo;
            Date dateFrom = DateUtils.nullSafeParseJsDate(intervalFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(intervalTo);
            //Date dateTo = (DateUtils.isValidateDate(intervalTo,DateUtils.jsDateFormat)) ? DateUtils.nullSafeParseJsDate(intervalTo) : null;
            if (StringUtils.isNotEmpty(intervalFrom) && dateFrom == null)
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnInterval"), smL.getString("prfnFrom")));
            if (StringUtils.isNotEmpty(intervalTo) && dateTo == null)
                addActionError(smL.getString("NiepoprawnyFormatWartosci2", smL.getString("prfnInterval"), smL.getString("prfnTo")));

            //sprawdzenie poprawno�ci zakresu dat
            if (dateFrom != null && dateTo != null && dateFrom.after(dateTo))
                addActionError(smL.getString("DataPoczatkowaJestPozniejszaOdKoncowej"));
//            if (dateTo != null && dateFrom.before(DateUtils.zeroDate)) // todo NullPointer
//                addActionError(smL.getString("DataPoczatkowaPozaZakresem"));

            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    public enum Criteria {
        documentId,
        intervalFrom,
        intervalTo,
        documentKind,
        division,
        user,
        status,
        durationMillis,
        delayMillis,
        onlyNotCompleted,
        onlyDelayed
    }
    
    private class DoReport implements ActionListener {
        public void actionPerformed(ActionEvent event) {

//            new Report() {
//                @Override
//                public void doReport() throws Exception {
//                    //To change body of implemented methods use File | Settings | File Templates.
//                }
//            };

            smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            ReportCriteria criteria = new ReportCriteria();

            try {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));

                //Long - sprawdzenie typu
                //private String documentId;
                addTo(criteria, Criteria.documentId.name(), parseToLong(documentId));

                //Date - sprawdzenie typu
                //private String intervalFrom;
                //private String intervalTo;
                addTo(criteria, Criteria.intervalFrom.name(), DateUtils.nullSafeParseJsDate(intervalFrom));
                addTo(criteria, Criteria.intervalTo.name(), DateUtils.nullSafeParseJsDate(intervalTo));

                //Long - warto�� s�ownikowa
                //private Long documentKind;
                //private Long division;
                //private Long user;
                //private Long[] status;
                addTo(criteria, Criteria.documentKind.name(), documentKind);
               
                addTo(criteria, Criteria.division.name(), getDivisionsTab(division));
                addTo(criteria, Criteria.user.name(), user);
                addTo(criteria, Criteria.status.name(), getStatusCnTitleMap());

                //Long - wartosci ustawiane na podstawie p�l
                addTo(criteria, Criteria.durationMillis.name(), getTime(durationDays, durationHours, durationMinutes));
                addTo(criteria, Criteria.delayMillis.name(), getTime(delayDays, delayHours, delayMinutes));

                //Boolean
                addTo(criteria, Criteria.onlyNotCompleted.name(), onlyNotCompleted);
                addTo(criteria, Criteria.onlyDelayed.name(), onlyDelayed);

                pl.compan.docusafe.service.reports.Report service = (pl.compan.docusafe.service.reports.Report) ServiceManager.getService(pl.compan.docusafe.service.reports.Report.NAME);
                ReportHandle handle = service.generateReport(ProcessReport.REPORT_ID, criteria, DSApi.context().getPrincipalName());

                try {
                    synchronized (handle) {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                } catch (InterruptedException e) {
                    //log.error(e.getMessage(), e);
                    //do nothing
                }

                if (!handle.isReady()) {
                    event.setResult("not-ready");
                } else {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad") + " (" + handle.getException().getMessage() + ")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            } catch (EdmException e) {
                addActionError(e.getMessage());
            }
        }

		private  Long[] getDivisionsTab(Long divisionid) throws DivisionNotFoundException, EdmException {
			if(divisionid == null)
				return null;
			else
			{
				ArrayList<Long> list = new ArrayList<Long>(Arrays.asList(DSDivision.findById(divisionid).getAncestorsIds()));
				list.add(divisionid);
				return list.toArray(new Long[list.size()]);
			}
		}

    }
}
