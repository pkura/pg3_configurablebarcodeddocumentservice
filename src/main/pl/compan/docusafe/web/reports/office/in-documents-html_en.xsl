<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:template match="/report">
    <p>Criterion of choose:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>Lp.</th>
            <th>Receive date</th>
            <th>Number of scripts</th>
        </tr>
        <xsl:apply-templates select="data/results/count"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Division: <b><xsl:value-of select="eq[@property='journal']/@name"/></b></td>
        <td>Recipient: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
    </tr>
    <tr>
        <td>Script kind: <b><xsl:value-of select="eq[@property='kind']/@name"/></b></td>
        <td>Delivery method: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
        <td>Receive method: <b><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></b></td>
    </tr>
    <tr>
        <td colspan="2">Receive date:
            <xsl:if test="range[@property='incomingDate']/@start.date">
                <b>from <xsl:value-of select="range[@property='incomingDate']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='incomingDate']/@end.date">
                <b> to <xsl:value-of select="range[@property='incomingDate']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Sender: <b><xsl:value-of select="eq[@property='sender']/@value"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="count">
    <tr>
        <td><xsl:value-of select="@sequenceId"/></td>
        <td><xsl:value-of select="@date.date"/></td>
        <td align="right">
            <xsl:choose>
                <xsl:when test="number(@count) = 0">
                    Shortage of script
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@count"/>
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="3"> </td></tr>
    <tr>
        <td colspan="2">Total</td>
        <td align="right"><xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
