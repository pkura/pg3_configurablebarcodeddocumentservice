package pl.compan.docusafe.web.reports.office;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.InDocumentsReport;
import pl.compan.docusafe.service.reports.Report;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.*;

/**
 * Rejestr korespondencji.
 * <p>
 * Raport generujący informacje o pismach wchodzących/wychodzących
 * przez Kancelarię Ogólną.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InDocumentsReportAction.java,v 1.22 2008/12/12 11:29:10 pecet1 Exp $
 */
public class InDocumentsReportAction extends EventActionSupport
{
    // @IMPORT
    private boolean forceAnd;
    
    // @EXPORT/@IMPORT
    private String incomingDateFrom;
    private String incomingDateTo;
    private Integer kindId;
    private String author;
    private Long journalId;
    private Integer deliveryId;
    private Integer outgoingDeliveryId;
    private String sender;

    // @EXPORT
    private List kinds;
    private List users;
    private Map journals;
    private List deliveries;
    private List outgoingDeliveries;
    private boolean canCreateReports;
    private StringManager smL;

    // identyfikator raportu używany przy przekierowaniu do /view.action?id=...
    private Long id;

    public static final String EV_REPORT = "report";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new DoReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                kinds = InOfficeDocumentKind.list();
                users =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                
                List<Journal> docJournals = new ArrayList<Journal>();
            	
            	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
                {
            		docJournals = Journal.findByType(Journal.INCOMING);
            		docJournals.addAll(Journal.findByType(Journal.INCOMING, true));
                }
            	else
            	{
                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                		docJournals = Journal.findByDivisionGuid(division.getGuid(),Journal.INCOMING);
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),Journal.INCOMING,true));
                    }
                    if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
                    	docJournals.add(Journal.getMainInternal());
                    }
                }
                journals = new LinkedHashMap<Long, String>(docJournals.size());
                for (Journal journal:docJournals )
                {
                    try
                    {
                        journals.put(journal.getId(),
                            (journal.isClosed() ? "("+journal.getCyear()+") " : "") +
                            (journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
                            journal.getDescription());
                    }
                    catch (DivisionNotFoundException e)
                    {
                        journals.put(journal.getId(), "[" + smL.getString("nieznanyDzial") + "] - "+journal.getDescription());
                    }
                }
                

                deliveries = InOfficeDocumentDelivery.list();
                outgoingDeliveries = OutOfficeDocumentDelivery.list();

                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            Date dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);

            if (dateFrom == null)
                addActionError(smL.getString("NiePodanoPoczatkowejDatyPrzyjeciaPisma"));

            if (dateTo == null)
                addActionError(smL.getString("NiePodanoKoncowejDatyPrzyjeciaPisma"));

            if (dateFrom != null && dateTo != null && dateFrom.after(dateTo))
                addActionError(smL.getString("DataPoczatkowaJestPozniejszaOdKoncowej"));
            
            if (dateTo != null && dateFrom.before(DateUtils.zeroDate))
            	addActionError(smL.getString("DataPoczatkowaPozaZakresem"));

            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    private class DoReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            ReportCriteria criteria = new ReportCriteria();

            Date dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);

            try
            {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));

                if (dateFrom != null || dateTo != null)
                    criteria.addRange("incomingDate", dateFrom, dateTo);

                if (kindId != null)
                    criteria.addEq("kind", kindId);

                if (!StringUtils.isEmpty(author))
                    criteria.addEq("author", DSUser.findByUsername(author).getName());

                if (journalId != null)
                    criteria.addEq("journal", journalId);

                if (deliveryId != null)
                    criteria.addEq("delivery", deliveryId);

                if (outgoingDeliveryId != null)
                    criteria.addEq("outgoingDelivery", outgoingDeliveryId);

                if (!StringUtils.isEmpty(sender))
                    criteria.addEqText("sender", sender);

                criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));
                
                Report service = (Report) ServiceManager.getService(Report.NAME);
                ReportHandle handle = service.generateReport(InDocumentsReport.REPORT_ID, criteria,
                    DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad")+" ("+handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }

/*
            try
            {
                Mailer mailer = (Mailer) ServiceManager.getService(Mailer.NAME);
                Email email = new Email();
                email.addTo("lukasz@wa.home.pl");
                email.setSubject("Wygenerowano raport");
                email.setContent("W załączniku");
                email.addAttachment(new java.io.File("c:/docusafe/CA/newcert.pem"));
                mailer.send(email);
            }
            catch (EdmException e)
            {
            }
*/
        }
    }

    public void setForceAnd(boolean forceAnd)
    {
        this.forceAnd = forceAnd;
    }
    
    public String getIncomingDateFrom()
    {
        return incomingDateFrom;
    }

    public void setIncomingDateFrom(String incomingDateFrom)
    {
        this.incomingDateFrom = incomingDateFrom;
    }

    public String getIncomingDateTo()
    {
        return incomingDateTo;
    }

    public void setIncomingDateTo(String incomingDateTo)
    {
        this.incomingDateTo = incomingDateTo;
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public List getKinds()
    {
        return kinds;
    }

    public List getUsers()
    {
        return users;
    }

    public Long getId()
    {
        return id;
    }

    public Long getJournalId()
    {
        return journalId;
    }

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public Integer getOutgoingDeliveryId()
    {
        return outgoingDeliveryId;
    }

    public void setOutgoingDeliveryId(Integer outgoingDeliveryId)
    {
        this.outgoingDeliveryId = outgoingDeliveryId;
    }

    public Map getJournals()
    {
        return journals;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public List getOutgoingDeliveries()
    {
        return outgoingDeliveries;
    }

    public boolean isCanCreateReports()
    {
        return canCreateReports;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }
}
