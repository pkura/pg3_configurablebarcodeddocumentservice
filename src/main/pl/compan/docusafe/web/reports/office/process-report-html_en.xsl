<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:template match="/report">
        <p>Search criteria:</p>
        <table>
            <xsl:apply-templates select="data/criteria"/>
        </table>
        <p></p>
        <table>
            <tr>
                <th>No.</th>
                <th>Document id</th>
                <th>Process</th>
                <th>Status (CN)</th>
                <th>Start date</th>
                <th>End date</th>
                <th>Duration</th>
                <th>Delay</th>
                <th>Division</th>
                <th>User</th>
            </tr>
            <xsl:apply-templates select="data/results/document"/>
            <xsl:apply-templates select="data/results/totalcount"/>
        </table>
        <table>
            <xsl:apply-templates select="data/results/durationMean"/>
            <xsl:apply-templates select="data/results/delayMean"/>
        </table>
    </xsl:template>

    <xsl:template match="criteria">
        <tr>
            <!--I kolumna-->
            <td width="150px">Document id:</td>
            <td width="450px"><b><xsl:value-of select="eq[@property='documentId']/@name"/></b></td>

            <!--odst�p-->
            <td width="50px"></td>

            <!--II kolumna-->
            <td width="150px">Interval:</td>
            <td width="200px">from: <b><xsl:value-of select="eq[@property='intervalFrom']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Division:</td>
            <td><b><xsl:value-of select="eq[@property='division']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>to: <b><xsl:value-of select="eq[@property='intervalTo']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>User:</td>
            <td><b><xsl:value-of select="eq[@property='user']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>only uncompleted: <b><xsl:value-of select="eq[@property='onlyNotCompleted']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td></td>
            <td></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td>Max duration:</td>
            <td><b><xsl:value-of select="eq[@property='durationMillis']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Document kind:</td>
            <td><b><xsl:value-of select="eq[@property='documentKind']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td>Delay:</td>
            <td><b><xsl:value-of select="eq[@property='delayMillis']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Status:</td>
            <td><b><xsl:value-of select="eq[@property='status']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>only delayed: <b><xsl:value-of select="eq[@property='onlyDelayed']/@name"/></b></td>
        </tr>
    </xsl:template>

    <xsl:template match="document">
        <tr>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@lp"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@documentId"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@processName"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@status"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@start"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@end"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@duration"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@delay"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@division"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@user"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="totalcount">
        <tr><td colspan="6"> </td></tr>
        <tr>
            <td colspan="6">Total: <xsl:value-of select="@count"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="durationMean">
        <tr>
            <td>Mean duration (for completed processes): </td>
            <td><xsl:value-of select="@value"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="delayMean">
        <tr>
            <td>Mean time delay (for delayed processes): </td>
            <td><xsl:value-of select="@value"/></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
