<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>Lp.</th>
            <th>Data przyj�cia</th>
            <th>Liczba pism</th>
        </tr>
        <xsl:apply-templates select="data/results/count"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Wydzia�: <b><xsl:value-of select="eq[@property='journal']/@name"/></b></td>
        <td>Przyj��: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
        <td>Spos�b dostarczenia: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
    </tr>
    <tr>
        <td colspan="2">Data pisma:
            <xsl:if test="range[@property='ctime']/@start.date">
                <b>od <xsl:value-of select="range[@property='ctime']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='ctime']/@end.date">
                <b> do <xsl:value-of select="range[@property='ctime']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Odbiorca: <b><xsl:value-of select="eq[@property='recipient']/@value"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="count">
    <tr>
        <td><xsl:value-of select="@sequenceId"/></td>
        <td><xsl:value-of select="@date.date"/></td>
        <td align="right">
            <xsl:choose>
                <xsl:when test="number(@count) = 0">
                    Brak pism
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@count"/>
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="3"> </td></tr>
    <tr>
        <td colspan="2">W sumie</td>
        <td align="right"><xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
