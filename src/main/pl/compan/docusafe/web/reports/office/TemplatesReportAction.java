package pl.compan.docusafe.web.reports.office;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.IntDocumentsReport;
import pl.compan.docusafe.service.reports.OutDocumentsReport;
import pl.compan.docusafe.service.reports.OverdueDocumentsReport;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


/**
 * @author <a href="mailto:komisarskip@student.mini.pw.edu.pl">Piotr Komisarski</a>
 */

public class TemplatesReportAction extends EventActionSupport
{
    //@IMPORT
    private String action;
    
    //@IMPORT/EXPORT
    private String templateName;
    
    //@EXPORT
    private List templates;
    private Long id;
    
    private String incomingDateFrom;
    private String incomingDateTo;
    private Integer lastDays;
    private String division;
    private String journal;
    private String sender;
    private String recipient;
    private String author;
    private Integer warnDays;
    private boolean showOverdues;
    
    
    //@PRIVATE
    private boolean templateDeleted;
    private boolean showTemplate;
    
   
    private boolean forceAnd;
    private Integer deliveryId;
    private Integer outgoingDeliveryId;
    private String kindIds;
    private String sortField;
    private String reportType;
    StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
    
    //tomekl
    private String[] liczbaDni;
    private String[] nazwa;
    private int[] run;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAutoRaportSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new autoRaportSave()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    public void readValues()
    {
    	PreparedStatement ps = null;
        try
        {
         
            ps = DSApi.context().prepareStatement("SELECT * FROM DS_REPORT_TEMPLATE WHERE REPORTNAME=?");
            ps.setString(1, templateName);
            ResultSet rs = ps.executeQuery();
            if(!rs.next())
                throw new EdmException(sm.getString("NieZnalezionoTegoSzablonu"));
            incomingDateFrom = rs.getString("INCOMINGDATEFROM");
            incomingDateTo = rs.getString("INCOMINGDATETO");
            lastDays = rs.getInt("LASTDAYS");
            division = rs.getString("DIVISIONGUID");            
            journal = rs.getString("JOURNALID");
            sender = rs.getString("SENDER");
            recipient = rs.getString("RECIPIENT");
            author = rs.getString("AUTHOR");
            kindIds = rs.getString("KINDIDS");
            reportType = rs.getString("REPORTTYPE");
            warnDays = rs.getInt("WARNDAYS");
            deliveryId = rs.getInt("DELIVERYID");
            outgoingDeliveryId = rs.getInt("OUTGOINGDELIVERYID");
            int tmp = rs.getInt("SHOWOVERDUES");
            showOverdues = tmp==0?false:true;
            tmp = rs.getInt("FORCEAND");
            forceAnd = tmp==0?false:true;
            sortField = rs.getString("SORTFIELD");
            if(lastDays==0)lastDays=null;
            if(warnDays==-1)warnDays = null;
            if(outgoingDeliveryId==-1)outgoingDeliveryId=null;
            if(deliveryId==-1) deliveryId=null;                        
        }
        catch(SQLException sqle)
        {
            addActionError(sm.getString("ProblemZkomunikacjaZbazaDanych")+": " + sqle.getMessage());
        }
        catch(EdmException ee)
        {
            addActionError(ee.getMessage());
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
    
    private void showReport()
    {
        try
        {
            showTemplate = true;
            readValues();
            if(division!=null)
            {
                DSDivision div = DSDivision.find(division);
                division = div.getName();
            }
            if(journal!=null)
            {
                Journal j = Journal.find((long)Integer.parseInt(journal));
                journal = j.getSummary();
            }
            if(author!=null)
            {
                DSUser user = DSUser.findByUsername(author);
                author = user.getFirstname() +" " +user.getLastname();
            }
        }
        catch(EdmException e)
        {
            addActionError(e.getMessage());
        }
    }
    
    public void generate(ActionEvent event)
    {
    	try
        {
        	readValues();
            
            if(reportType == null)
        		throw new EdmException(sm.getString("BrakTypuRaportu"));
        	
            ReportCriteria criteria=null; 
            if(reportType.equals("OVERDUE"))
                criteria = getOverdueReportCriteria();
            else if(reportType.equals("OUT"))
                criteria = getOutInternalReportCriteria();
            else if(reportType.equals("INTERNAL"))
                criteria = getOutInternalReportCriteria();
            
            pl.compan.docusafe.service.reports.Report service = 
                    (pl.compan.docusafe.service.reports.Report) ServiceManager.getService(pl.compan.docusafe.service.reports.Report.NAME);
            
            ReportHandle handle = null; 
            
            if(reportType.equals("OVERDUE"))
            handle = service.generateReport(OverdueDocumentsReport.REPORT_ID, criteria,
                DSApi.context().getPrincipalName());
            else if(reportType.equals("OUT"))
            handle = service.generateReport(OutDocumentsReport.REPORT_ID, criteria,
                DSApi.context().getPrincipalName());
            else if(reportType.equals("INTERNAL"))
            handle = service.generateReport(IntDocumentsReport.REPORT_ID, criteria,
                DSApi.context().getPrincipalName());

            try
            {
                synchronized (handle)
                {
                    if (!handle.isReady())
                        handle.wait(5 * 1000);
                }
            }
            catch (InterruptedException e)
            {
            }

            if (!handle.isReady())
            {
                if(event != null)
                	event.setResult("not-ready");
            }
            else
            {
                if (handle.getException() != null)
                    addActionError(sm.getString("WystapilBlad")+" ("+handle.getException().getMessage()+")");
                id = handle.getId();
                if(event != null)
                	event.setResult("view-report");
            }
        }
        catch (EdmException e)
        {
        	if(event != null)
        		addActionError(e.getMessage());
        }
    }
    
    private void deleteReport()
    {
    	PreparedStatement ps = null;
        try
        {
            templateDeleted = true;
            DSApi.context().begin();
            ps = DSApi.context().prepareStatement("DELETE FROM DS_REPORT_TEMPLATE WHERE REPORTNAME=?");
            ps.setString(1, templateName);
            ps.execute();
            addActionMessage(sm.getString("UsunietoSzablon")+" " + templateName);
            DSApi.context().commit();
        }
        catch (SQLException sqle)
        {
            DSApi.context().setRollbackOnly();
            addActionError(sqle.getMessage());
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            addActionError(e.getMessage());
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
    
    private class autoRaportSave implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	System.out.println("aaa");
        	LoggerFactory.getLogger("tomekl").debug("oo "+nazwa.length);
        }
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            templates = new ArrayList<Report>();
            if(templateName==null)
            {
            	PreparedStatement ps = null;
                try
                {
                    ps = DSApi.context().prepareStatement("SELECT * FROM DS_REPORT_TEMPLATE");
                    ResultSet rs = ps.executeQuery();
                    
                    while(rs.next())
                    {
                        templates.add(new Report(rs.getString("REPORTTYPE"),rs.getString("REPORTNAME"),rs.getString("CREATEDATE")));
                    }
                    
                }
                catch (SQLException sqle)
                {
                    addActionError(sqle.getMessage());
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
                finally
                {
                	DSApi.context().closeStatement(ps);
                }
                return;
            }
            if(action.equals("delete"))
                deleteReport();
            else if(action.equals("show"))
                showReport();
            else if(action.equals("generate"))
            {   
              generate(event);  
            }
    }
    
    
    }
    
    private ReportCriteria getOutInternalReportCriteria() throws EdmException
    {
        ReportCriteria criteria = new ReportCriteria();
        Date dateFrom = null, dateTo=null;
            
        if(lastDays!=null)
        {
            dateTo = new Date();
            dateFrom = new Date(); 
            dateFrom = DateUtils.plusDays(dateFrom, lastDays*-1);
        }
        else
        {
            dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);
            if(dateFrom==null)
                throw new EdmException(sm.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
            if(dateTo==null)
                throw new EdmException(sm.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
        }
        
        criteria.addRange("ctime", dateFrom, dateTo);
        
        if (!StringUtils.isEmpty(author))
            criteria.addEq("author", DSUser.findByUsername(author).getName());
        
        criteria.addEq("journal", Long.valueOf(journal));

        if (deliveryId != null)
            criteria.addEq("delivery", deliveryId);

        if (!StringUtils.isEmpty(recipient))
            criteria.addEqText("recipient", recipient);

        criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));

        return criteria;
    }
    
    private ReportCriteria getOverdueReportCriteria() throws EdmException
    {   
        ReportCriteria criteria = new ReportCriteria();
        Date dateFrom = null, dateTo=null;
            
        if(lastDays!=null)
        {
            dateTo = new Date();
            dateFrom = new Date(); 
            dateFrom = DateUtils.plusDays(dateFrom, lastDays*-1);
        }
        else
        {
            dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);
            if(dateFrom==null)
                throw new EdmException(sm.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
            if(dateTo==null)
                throw new EdmException(sm.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
        }
        
        //if(true)throw new RuntimeException(dateTo.toString());
        
        criteria.addRange("incomingDate", dateFrom, dateTo);
        
        if (kindIds != null && kindIds.length() > 0)
        {
            String[] strkindIds = kindIds.split(",");
            Integer[] intkindIds = new Integer[strkindIds.length];
            
            for(int i=0;i<strkindIds.length;i++)
                intkindIds[i] = Integer.parseInt(strkindIds[i]);
                
            criteria.addEnumeration("kinds", intkindIds);
        }

        if (!StringUtils.isEmpty(author))
            criteria.addEq("author", DSUser.findByUsername(author).getName());
        
        if (deliveryId != null)
            criteria.addEq("delivery", deliveryId);

        if (outgoingDeliveryId != null)
            criteria.addEq("outgoingDelivery", outgoingDeliveryId);

        if (warnDays != null)
            criteria.setAttribute("warnDays", warnDays);

        if (!StringUtils.isEmpty(sender))
            criteria.addEqText("sender", sender);

        if (!StringUtils.isEmpty(recipient))
            criteria.addEqText("recipient", recipient);

        if (journal != null)
            criteria.addEq("journalId", Long.valueOf(journal));
        else if (!StringUtils.isEmpty(division))
            criteria.addEq("divisionGuid", division);

        criteria.setAttribute("showOverdues", Boolean.valueOf(showOverdues));

        criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));
        
        criteria.setAttribute("sortField", sortField);

        return criteria;
    }
    
    private class Report
    {
        private String type;
        private String name;
        private String createDate;
        
        public String getCreateDate() {
            return createDate;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public Report(String type, String name, String createDate)
        {    
            if(type.equals("OVERDUE"))
                this.type = sm.getString("PismaPrzeterminowane");
            else if(type.equals("OUT"))
                this.type = sm.getString("PismaWychodzace");
            else if(type.equals("INTERNAL"))
                this.type = sm.getString("PismaWewnetrzne");
            this.name = name;
            this.createDate = createDate;
        }   
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public boolean getTemplateDeleted() {
        return templateDeleted;
    }
    
    public void setAction(String action) {
        this.action = action;
    }
    
    public List getTemplates() {
        return templates;
    }

    public String getIncomingDateFrom() {
        return incomingDateFrom;
    }

    public String getIncomingDateTo() {
        return incomingDateTo;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDivision() {
        return division;
    }

    public String getJournal() {
        return journal;
    }

    public Integer getLastDays() {
        return lastDays;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSender() {
        return sender;
    }

    public Integer getWarnDays() {
        return warnDays;
    }

    public boolean isShowTemplate() {
        return showTemplate;
    }

    public Long getId() {
        return id;
    }

    public boolean isShowOverdues() {
        return showOverdues;
    }

	public String[] getLiczbaDni() {
		return liczbaDni;
	}

	public void setLiczbaDni(String[] liczbaDni) {
		this.liczbaDni = liczbaDni;
	}

	public String[] getNazwa() {
		return nazwa;
	}

	public void setNazwa(String[] nazwa) {
		this.nazwa = nazwa;
	}

	public int[] getRun() {
		return run;
	}

	public void setRun(int[] run) {
		this.run = run;
	}
    
    
        
}