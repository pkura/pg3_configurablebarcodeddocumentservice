package pl.compan.docusafe.web.reports.office;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ReportFromTempleteToEmailAction extends EventActionSupport 
{
	
	Logger log = LoggerFactory.getLogger(ReportFromTempleteToEmailAction.class);
	StringManager sm = StringManager.getManager(ReportFromTempleteToEmailAction.class.getPackage().getName());
	private static final long serialVersionUID = 1L;
	private List<ReportTemplate> templatesList;
	private static Random rand = new Random();
	
	private String[] name;
	private int[] run;
	private Integer[] days;
	private String[] date;
	private Integer[] minutes;
	private Integer[] hours;
	
	private static List<Integer> avMinutes;
	private static List<Integer> avHours;
	
	
	
	public static List<Integer> getAvMinutes() {
		if(avMinutes == null)
		{
			avMinutes = new ArrayList<Integer>();
			for(int i=0;i<60;i=i+5)
			{
				avMinutes.add(i);
			}
		}
		
		return avMinutes;
	}

	public static List<Integer> getAvHours() {
		if(avHours == null)
		{
			avHours = new ArrayList<Integer>();
			for(int i=0;i<=24;i++)
				avHours.add(i);
		}
		
		
		return avHours;
	}

	
	protected void setup() 
	{
		
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}	
	
	private class ReportTemplate
	{
		
		private String reportName;
		private int run;
		private Integer reportDays;
		private Integer randomId;
		private Calendar cal;
		
		public ReportTemplate(String name, int run, Integer days)
		{
			this.reportName = name;
			this.run = run;
			this.reportDays = days;
			this.randomId = rand.nextInt();
		}
		
		public Integer getMinutes()
		{
			return cal.get(Calendar.MINUTE);
		}
		
		public Integer getHours()
		{
			return cal.get(Calendar.HOUR_OF_DAY);
		}
		
		public String getStringDate()
		{
			return DateUtils.formatJsDate(cal.getTime());
		}
		
		public Integer getRandomId()
		{			
			return randomId;
		}

		public String getReportName() {
			return reportName;
		}

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public int getRun() {
			return run;
		}

		public void setRun(int run) {
			this.run = run;
		}

		public Integer getReportDays() {
			return reportDays;
		}

		public void setReportDays(Integer reportDays) {
			this.reportDays = reportDays;
		}

		public void setRaportDate(Date raportDate) {
			if(cal == null)
				cal = Calendar.getInstance();
			
			cal.setTime(raportDate);
		}
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	//LoggerFactory.getLogger("tomekl").debug("test");
        	templatesList = new ArrayList<ReportTemplate>();
        	
        	
        	PreparedStatement ps = null;
        	try
        	{
        		ps = DSApi.context().prepareStatement("SELECT REPORTNAME FROM DS_REPORT_TEMPLATE");
                ResultSet rs = ps.executeQuery();
                
                while(rs.next())
                {
                	String templateName = rs.getString("REPORTNAME");
                	ReportTemplate rt = new ReportTemplate(templateName, 0, null);
                	rt.setReportDays(DSApi.context().userPreferences().node("autoreport").node(templateName).getInt("days", 0));
                	rt.setRun(DSApi.context().userPreferences().node("autoreport").node(templateName).getInt("eventId", 0) > 0 ? 1 : 0);
                	
                	Calendar cal = Calendar.getInstance(); 
                	cal.setTimeInMillis(DSApi.context().userPreferences().node("autoreport").node(templateName).getLong("date", 0));
                	rt.setRaportDate(cal.getTime());
                	
                	templatesList.add(rt);
                }
                rs.close();
        		DSApi.context().closeStatement(ps);
        	}
        	catch (Exception e) 
        	{
        		log.error(e.getMessage(),e);
				LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
			}
        	finally
        	{
        		DSApi.context().closeStatement(ps);
        	}
        	
        }
    }
	
	private class save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	        	if(DSUser.findByUsername(DSApi.context().getPrincipalName()).getEmail() == null)
	        	{
	        		addActionError(sm.getString("BrakAdresuEmail"));
	        		return;
	        	}
        	}
        	catch (Exception e) 
        	{
				addActionError(e.getMessage());
			}
        	
        	try
        	{
        		DSApi.context().begin();
	        	for(int i=0; i<name.length; i++)
	        	{
	        		//LoggerFactory.getLogger("tomekl").debug("data "+DateUtils.parseJsDate(date[i]));
	        		Calendar cal = Calendar.getInstance();
	        		cal.setTime(DateUtils.parseJsDate(date[i]));
	        		cal.set(Calendar.HOUR_OF_DAY, hours[i]);
	        		cal.set(Calendar.MINUTE, minutes[i]);
	        		
	        		DSApi.context().userPreferences().node("autoreport").node(name[i]).putLong("date", cal.getTimeInMillis());
	        		DSApi.context().userPreferences().node("autoreport").node(name[i]).putInt("days", days[i]);
	        		
	        		if(run[i] == 1)
	        		{
	        			String eventParams = days[i]+"|"+name[i]+";"+DSApi.context().getPrincipalName();
	        			Long eventId = EventFactory.registerEvent("ForNextDays", "SendReportViaEmail", eventParams, null, cal.getTime());
	        			DSApi.context().userPreferences().node("autoreport").node(name[i]).putInt("eventId", eventId.intValue());	        			
	        		}
	        		else
	        		{
	        			DSApi.context().userPreferences().node("autoreport").node(name[i]).putInt("eventId", 0);	        			
	        		}
	        	}
	        	DSApi.context().commit();
        	}
        	catch (Exception e) 
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(e.getMessage());
        		log.error(e.getMessage(),e);
				LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
			}
        }
    }

	public List<ReportTemplate> getTemplatesList() {
		return templatesList;
	}

	public void setTemplatesList(List<ReportTemplate> templatesList) {
		this.templatesList = templatesList;
	}

	public String[] getName() {
		return name;
	}

	public void setName(String[] name) {
		this.name = name;
	}

	public int[] getRun() {
		return run;
	}

	public void setRun(int[] run) {
		this.run = run;
	}

	public Integer[] getDays() {
		return days;
	}

	public void setDays(Integer[] days) {
		this.days = days;
	}

	public String[] getDate() {
		return date;
	}

	public void setDate(String[] date) {
		this.date = date;
	}

	public Integer[] getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer[] minutes) {
		this.minutes = minutes;
	}

	public Integer[] getHours() {
		return hours;
	}

	public void setHours(Integer[] hours) {
		this.hours = hours;
	}
}
