<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:output omit-xml-declaration="yes" />

    <xsl:template match="/report">
        <fo:root font-family="Arial">

            <fo:layout-master-set>
                <!-- fo:layout-master-set defines in its children the page layout:
                     the pagination and layout specifications
                    - page-masters: have the role of describing the intended subdivisions
                                     of a page and the geometry of these subdivisions
                                    In this case there is only a simple-page-master which defines the
                                    layout for all pages of the text
                -->
                <!-- layout information -->
                <fo:simple-page-master master-name="simple"
                                       page-height="21cm"
                                       page-width="29.7cm"
                                       margin-top="1cm"
                                       margin-bottom="1cm"
                                       margin-left="1cm"
                                       margin-right="1cm">
                    <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
                    <fo:region-before extent="1cm"/>
                    <!--
                    <fo:region-after extent="1.5cm"/>
                    -->
                </fo:simple-page-master>
            </fo:layout-master-set>
            <!-- end: defines page layout -->


            <!-- start page-sequence
                 here comes the text (contained in flow objects)
                 the page-sequence can contain different fo:flows
                 the attribute value of master-name refers to the page layout
                 which is to be used to layout the text contained in this
                 page-sequence-->
            <fo:page-sequence master-reference="simple">

                <fo:static-content flow-name="xsl-region-before">
                    <fo:block font-family="Arial" font-size="8pt" text-align="right">
                        Raport procesów
                    </fo:block>
                </fo:static-content>

                <!-- start fo:flow
                     each flow is targeted
                     at one (and only one) of the following:
                     xsl-region-body (usually: normal text)
                     xsl-region-before (usually: header)
                     xsl-region-after  (usually: footer)
                     xsl-region-start  (usually: left margin)
                     xsl-region-end    (usually: right margin)
                     ['usually' applies here to languages with left-right and top-down
                      writing direction like English]
                     in this case there is only one target: xsl-region-body
                  -->
                <fo:flow flow-name="xsl-region-body">

                    <!-- each paragraph is encapsulated in a block element
                         the attributes of the block define
                         font-family and size, line-heigth etc. -->

                    <xsl:apply-templates select="data/criteria"/>

                    <fo:table table-layout="fixed" width="100%">
                        <fo:table-column column-width="0.75*2cm"/><!--lp-->
                        <fo:table-column column-width="0.75*2.5cm"/><!--id doc-->
                        <fo:table-column/><!--proces-->
                        <fo:table-column/><!--status-->
                        <fo:table-column column-width="0.75*3cm"/><!--data rozp-->
                        <fo:table-column column-width="0.75*3cm"/><!--data zak-->
                        <fo:table-column column-width="0.75*3cm"/><!--czas wyk-->
                        <fo:table-column column-width="0.75*3cm"/><!--opoznienie-->
                        <fo:table-column column-width="0.75*3cm"/><!--dzial-->
                        <fo:table-column column-width="0.75*3cm"/><!--uzytkownik-->
                        <fo:table-header>
                            <fo:table-row>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Lp.</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Id dokumentu</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Proces</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Status (CN)</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Data rozpoczęcia</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Data zakończenia</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Czas wykonywania</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Opóźnienie</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Dział</fo:block></fo:table-cell>
                                <fo:table-cell><fo:block font-family="Arial" font-size="8pt" font-weight="bold">Użytkownik</fo:block></fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>
                        <fo:table-body>
                            <xsl:apply-templates select="data/results/document"/>
                        </fo:table-body>
                    </fo:table>

                    <xsl:apply-templates select="data/results/totalcount"/>
                    <xsl:apply-templates select="data/results/durationMean"/>
                    <xsl:apply-templates select="data/results/delayMean"/>

                    <fo:block text-align="right" font-family="Arial" font-size="8pt">
                        Created: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
                        Date: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>,
                    </fo:block>

                </fo:flow> <!-- closes the flow element-->
            </fo:page-sequence> <!-- closes the page-sequence -->
        </fo:root>
    </xsl:template>

    <xsl:template match="criteria">
        <fo:block font-family="Arial" font-size="8pt">
            <fo:inline font-weight="bold">Search criteria:</fo:inline>
        </fo:block>
        <fo:table table-layout="fixed" space-after.optimum="8">
            <fo:table-column column-width="4cm"/>
            <fo:table-column/>
            <fo:table-column column-width="4cm"/>
            <fo:table-column/>
            <fo:table-body>
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Id dokumnetu: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='documentId']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Rodzaj dokumentu: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='documentKind']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Dział: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='division']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Etap procesu: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='status']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Użytkownik: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='user']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Czas wykonywania: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='durationMillis']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Przedział czasowy:</fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Maksymalne opóźnienie: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='delayMillis']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">tylko opóźnione: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='onlyDelayed']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">tylko niezakończone: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='onlyNotCompleted']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">do: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='intervalFrom']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <!--<fo:table-cell><fo:block font-family="Arial"  font-size="8pt">od: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='intervalTo']/@name"/></fo:inline></fo:block></fo:table-cell>-->
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Document id:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='documentId']/@name"/></fo:inline></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Interval:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">from: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='intervalFrom']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Division:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='division']/@name"/></fo:inline></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">to: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='intervalTo']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">User:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='user']/@name"/></fo:inline></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">only uncompleted: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='onlyNotCompleted']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Max duration:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='durationMillis']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Document kind:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='documentKind']/@name"/></fo:inline></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Delay:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='delayMillis']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">Status:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"><fo:inline font-weight="bold"><xsl:value-of select="eq[@property='status']/@name"/></fo:inline></fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt"></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial"  font-size="8pt">only delayed: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='onlyDelayed']/@name"/></fo:inline></fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <xsl:template match="document">
        <fo:table-row>

            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@lp"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@documentId"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@processName"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@status"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@start"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@end"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@duration"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@delay"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@division"/></fo:block></fo:table-cell>
            <fo:table-cell display-align="center"><fo:block font-family="Arial" font-size="8pt"><xsl:value-of select="@user"/></fo:block></fo:table-cell>

        </fo:table-row>
    </xsl:template>

    <xsl:template match="totalcount">
        <fo:table table-layout="fixed" space-after.optimum="8">
            <fo:table-column/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block font-family="Arial" font-size="8pt">Total:
                            <xsl:value-of select="@count"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <xsl:template match="durationMean">
        <fo:table table-layout="fixed" space-after.optimum="8">
            <fo:table-column column-width="9cm"/>
            <fo:table-column/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block font-family="Arial" font-size="8pt">
                            Mean duration (for completed processes):
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block font-family="Arial" font-size="8pt">
                            <xsl:value-of select="@value"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <xsl:template match="delayMean">
        <fo:table table-layout="fixed" space-after.optimum="8">
            <fo:table-column column-width="9cm"/>
            <fo:table-column/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block font-family="Arial" font-size="8pt">
                            Mean time delay (for delayed processes):
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block font-family="Arial" font-size="8pt">
                            <xsl:value-of select="@value"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>

</xsl:stylesheet>
