package pl.compan.docusafe.web.reports.office;

import java.util.List;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Rejestra aktywnoscu.
 * <p>
 * Raport generujący informacje o aktywnosci pracowników na poszczególnych
 * etapach procesów
 * 
 */
public class ReportEwidencjaCzasuAction extends EventActionSupport
{
	
	// @EXPORT/@IMPORT
	private Integer documentKind;
	
	// @EXPORT
	private List<DocumentKind> documentKinds;
	    
	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				documentKinds = DocumentKind.list(true);
			}
			catch (Exception e)
			{
				
			}
		}
	}

	public Integer getDocumentKind()
	{
		return documentKind;
	}

	public void setDocumentKind(Integer documentKind)
	{
		this.documentKind = documentKind;
	}

	public List<DocumentKind> getDocumentKinds()
	{
		return documentKinds;
	}

	public void setDocumentKinds(List<DocumentKind> documentKinds)
	{
		this.documentKinds = documentKinds;
	}

}
