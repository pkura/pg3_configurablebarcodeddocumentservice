<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>


<xsl:template match="/report">
	<xsl:apply-templates select="info"/>	
	<xsl:apply-templates select="tresc"/>
</xsl:template>

<xsl:template match="info">
	<p></p>
	<table>
	<tr>
	<td><P><b>Zamiany zleci�: </b></P></td><td><xsl:value-of select="who"/></td>
	</tr>
	<tr>
	<td><P><b>Zmiany dokonano: </b></P></td><td><xsl:value-of select="when"/></td>
	</tr>
	<tr>
	<td><P><b>Ilo�� dokument�w: </b></P></td><td><xsl:value-of select="count"/></td>
	</tr>
	</table>    
</xsl:template>

<xsl:template match="tresc">
	<p></p>
	<table width="60%">
	<tr>
	<th><p><b>Id dokumentu</b></p></th>
	<th><p><b>Pole</b></p></th>
	<th><p><b>Stara warto��</b></p></th>
	<th><p><b>Nowa warto��</b></p></th>
	</tr>
	<xsl:for-each select="document">
		<xsl:for-each select="zmiana">
			<tr>
				<td><xsl:if test="position()=1"><xsl:value-of select="../@id"/></xsl:if></td>
				<td><xsl:value-of select="@field"/></td>
				<td><xsl:value-of select="@oldval"/></td>
				<td><xsl:value-of select="@newval"/></td>
			</tr>
		</xsl:for-each>
	</xsl:for-each>
	</table>
</xsl:template>

</xsl:stylesheet>
