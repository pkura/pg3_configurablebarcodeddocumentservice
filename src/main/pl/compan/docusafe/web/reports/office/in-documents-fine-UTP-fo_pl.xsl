<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/report">
    <fo:root font-family="Arial">

      <fo:layout-master-set>
      <!-- fo:layout-master-set defines in its children the page layout:
           the pagination and layout specifications
          - page-masters: have the role of describing the intended subdivisions
                           of a page and the geometry of these subdivisions
                          In this case there is only a simple-page-master which defines the
                          layout for all pages of the text
      -->
        <!-- layout information -->
        <fo:simple-page-master master-name="simple"
                      page-height="21cm"
                      page-width="29.7cm"
                      margin-top="1cm"
                      margin-bottom="1cm"
                      margin-left="1cm"
                      margin-right="1cm">
          <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
          <fo:region-before extent="1cm"/>
          <!--
          <fo:region-after extent="1.5cm"/>
          -->
        </fo:simple-page-master>
      </fo:layout-master-set>
      <!-- end: defines page layout -->


      <!-- start page-sequence
           here comes the text (contained in flow objects)
           the page-sequence can contain different fo:flows
           the attribute value of master-name refers to the page layout
           which is to be used to layout the text contained in this
           page-sequence-->
      <fo:page-sequence master-reference="simple">

          <fo:static-content flow-name="xsl-region-before">
            <fo:block font-family="Arial" font-size="10pt" text-align="right">
                <!--<fo:external-graphic src="logo_c.gif"/>-->
                Rejestr korespondencji przychodz�cej
            </fo:block>
          </fo:static-content>

          <!-- start fo:flow
               each flow is targeted
               at one (and only one) of the following:
               xsl-region-body (usually: normal text)
               xsl-region-before (usually: header)
               xsl-region-after  (usually: footer)
               xsl-region-start  (usually: left margin)
               xsl-region-end    (usually: right margin)
               ['usually' applies here to languages with left-right and top-down
                writing direction like English]
               in this case there is only one target: xsl-region-body
            -->
        <fo:flow flow-name="xsl-region-body">

          <!-- each paragraph is encapsulated in a block element
               the attributes of the block define
               font-family and size, line-heigth etc. -->

          <xsl:apply-templates select="data/criteria"/>

          <fo:table table-layout="fixed">
              <!-- +2 cm -->
              <fo:table-column column-width="1.7cm"/>
              <fo:table-column column-width="1.7cm"/>
              <fo:table-column column-width="0.3cm"/>
              <fo:table-column column-width="3.5cm"/>
              <fo:table-column column-width="3.5cm"/>
              <fo:table-column column-width="6.5cm"/>
              <fo:table-column column-width="4.3cm"/>
              <fo:table-column column-width="3.5cm"/>
              <fo:table-column column-width="3.5cm"/>
              <fo:table-header>
                  <fo:table-row>
                  	  <fo:table-cell><fo:block font-family="Arial" font-weight="bold">LP</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">KO</fo:block></fo:table-cell>
                      <fo:table-cell/>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Data przyj�cia</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Data pisma</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Tre��</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Nadawca</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Dzia�</fo:block></fo:table-cell>
                      <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Aktualna lokalizacja</fo:block></fo:table-cell>
                  </fo:table-row>
              </fo:table-header>
            <fo:table-body>
                <xsl:apply-templates select="data/results/document"/>
                <xsl:apply-templates select="data/results/totalcount"/>
            </fo:table-body>
          </fo:table>

          <fo:block text-align="right" font-family="Arial">
            Utworzy�: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
            Data: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>,
          </fo:block>

        </fo:flow> <!-- closes the flow element-->
      </fo:page-sequence> <!-- closes the page-sequence -->
    </fo:root>
</xsl:template>

<xsl:template match="criteria">
  <fo:block font-family="Arial">Kryteria wyboru:</fo:block>
  <fo:table table-layout="fixed" width="100%" space-after.optimum="13pt">
    <fo:table-column />
    <fo:table-column />
    <fo:table-column />
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell><fo:block font-family="Arial">Dziennik: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='journal']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Przyj��: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='author']/@name"/></fo:inline></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell><fo:block font-family="Arial">Rodzaj pisma: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='kind']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Spos�b dostarczenia: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='delivery']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Spos�b odbioru: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></fo:inline></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell number-columns-spanned="2"><fo:block font-family="Arial">
                Data przyj�cia: <fo:inline font-weight="bold">od <xsl:value-of select="range[@property='incomingDate']/@start.date"/>
                do <xsl:value-of select="range[@property='incomingDate']/@end.date"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Nadawca: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='sender']/@value"/></fo:inline></fo:block></fo:table-cell>
            <!--<fo:table-cell><fo:block font-family="Arial"></fo:block></fo:table-cell>-->
        </fo:table-row>
    </fo:table-body>
  </fo:table>
</xsl:template>

<xsl:template match="document">
    <fo:table-row>
        <fo:table-cell>
        	<fo:block wrap-option="wrap"><xsl:value-of select="@lp"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@officeNumber"/></fo:block>
        </fo:table-cell>
        <fo:table-cell><fo:block/></fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@incomingDate.date"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@date.date"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@summary"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@senderSummary"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block wrap-option="wrap"><xsl:value-of select="@division.name"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
        	<fo:block wrap-option="wrap"><xsl:value-of select="@location"/></fo:block>
        </fo:table-cell>
    </fo:table-row>
</xsl:template>

<xsl:template match="totalcount">
    <fo:table-row>
        <fo:table-cell number-columns-spanned="6">
            W sumie <xsl:value-of select="@count"/>
        </fo:table-cell>
    </fo:table-row>
</xsl:template>

</xsl:stylesheet>
