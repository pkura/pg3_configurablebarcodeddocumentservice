package pl.compan.docusafe.web.reports.office;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.PaaTaskReport;
import pl.compan.docusafe.service.reports.Report;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

@SuppressWarnings("serial")
public class PaaTaskReportAction extends EventActionSupport
{
    // @IMPORT
    
    // @EXPORT/@IMPORT
	private String nazwaZadania;
    private String author;

    // @EXPORT
    private boolean canCreateReports;
    private StringManager smL;

    // identyfikator raportu u�ywany przy przekierowaniu do /view.action?id=...
    private Long id;

    public static final String EV_REPORT = "report";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new DoReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                
            
                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

//            if (nazwaZadania == null)
//                addActionError(smL.getString("Nie Podano Nazwy Zadania"));
//            
            

            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    private class DoReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            ReportCriteria criteria = new ReportCriteria();

            try
            {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));

                if (!StringUtils.isEmpty(nazwaZadania))
                    criteria.addEqText("nazwaZadania", nazwaZadania);

                Report service = (Report) ServiceManager.getService(Report.NAME);
                ReportHandle handle = service.generateReport(PaaTaskReport.REPORT_ID, criteria,
                    DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad")+" ("+handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public Long getId()
    {
        return id;
    }

    public boolean isCanCreateReports()
    {
        return canCreateReports;
    }

	public String getNazwaZadania() {
		return nazwaZadania;
	}

	public void setNazwaZadania(String nazwaZadania) {
		this.nazwaZadania = nazwaZadania;
	}
    
    
}
