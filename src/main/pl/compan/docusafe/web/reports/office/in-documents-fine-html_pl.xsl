<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:param name="office-in-document-link"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
        	<th>LP</th>
            <th>KO</th>
            <th>Data przyj�cia</th>
            <th>Data pisma</th>
            <th>Opis pisma</th>
            <th>KOd Kreskowy</th>
            <th>Nr Przesy�ki R</th>
            <th>Nadawca</th>
            <th>Dzia�</th>
        </tr>
        <xsl:apply-templates select="data/results/document"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Dziennik: <b><xsl:value-of select="eq[@property='journal']/@name"/></b></td>
        <td>Przyj��: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
    </tr>
    <tr>
        <td>Rodzaj pisma: <b><xsl:value-of select="eq[@property='kind']/@name"/></b></td>
        <td>Spos�b dostarczenia: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
        <td>Spos�b odbioru: <b><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></b></td>
    </tr>
    <tr>
        <td colspan="2">Data przyj�cia:
            <xsl:if test="range[@property='incomingDate']/@start.date">
                <b>od <xsl:value-of select="range[@property='incomingDate']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='incomingDate']/@end.date">
                <b> do <xsl:value-of select="range[@property='incomingDate']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Nadawca: <b><xsl:value-of select="eq[@property='sender']/@value"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="document">
    <tr>
    	<td><xsl:value-of select="@lp"/></td>
        <td><xsl:element name="a">
                <xsl:attribute name="href"><xsl:value-of select="$office-in-document-link"/><xsl:value-of select="@documentId"/></xsl:attribute>
                    <xsl:value-of select="@officeNumber"/>
            </xsl:element>
        </td>
        <td><xsl:value-of select="@incomingDate.date"/></td>
        <td><xsl:value-of select="@date.date"/></td>
         <td><xsl:value-of select="@summary"/></td>
         <td><xsl:value-of select="@barcode"/></td>
         <td><xsl:value-of select="@postalregnumber"/></td>
        <td><xsl:value-of select="@senderSummary"/></td>
        <td><xsl:value-of select="@division.name"/></td>
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="6"> </td></tr>
    <tr>
        <td colspan="6">W sumie <xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
