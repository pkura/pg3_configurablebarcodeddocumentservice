<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:param name="office-in-document-link"/>

<xsl:template match="/report">
    <p>Search criteria:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>KO</th>
            <th>Document register date</th>
            <th>Description</th>
<!--
            <th>Numer w sprawie</th>
            <th>Przyj��</th>
            <th>Osoba</th>
-->
            <th>Sender</th>
            <th>Division</th>
            <th>Overduer</th>
        </tr>
        <xsl:apply-templates select="data/results/document"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Delivery method: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
        <td>Receive method: <b><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></b></td>
        <td>Registered by: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
    </tr>
    <xsl:if test="enumeration[@property='kinds']">
        <tr>
            <td colspan="3">
                Document kinds:
                <b><xsl:for-each select="enumeration[@property='kinds']/value">
                    <xsl:value-of select="@name"/><xsl:if test="following-sibling::value">, </xsl:if>
                </xsl:for-each></b>
            </td>
        </tr>
    </xsl:if>
    <tr>
        <td colspan="2">Document register date:
            <xsl:if test="range[@property='incomingDate']/@start.date">
                <b>od <xsl:value-of select="range[@property='incomingDate']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='incomingDate']/@end.date">
                <b> do <xsl:value-of select="range[@property='incomingDate']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Diary: <b><xsl:value-of select="eq[@property='journalId']/@name"/></b></td>
    </tr>
    <tr>
        <td>Sender: <b><xsl:value-of select="eq[@property='sender']/@value"/></b></td>
        <td>Recipient: <b><xsl:value-of select="eq[@property='recipient']/@value"/></b></td>
        <td>Division: <b><xsl:value-of select="eq[@property='divisionGuid']/@name"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="document">
    <tr>
        <td>
            <xsl:element name="a">
                <xsl:attribute name="href"><xsl:value-of select="$office-in-document-link"/><xsl:value-of select="@documentId"/></xsl:attribute>
                <xsl:value-of select="@officeId"/>
            </xsl:element>
        </td>
        <td><xsl:value-of select="@date.incomingDate"/></td>
        <td><xsl:value-of select="@summary"/></td>
<!--
        <td><xsl:value-of select="@caseId"/></td>
        <td><xsl:value-of select="@author"/></td>
        <td><xsl:value-of select="@currentUser"/></td>
-->
        <td><xsl:value-of select="@senderSummary"/></td>
        <td><xsl:value-of select="@division"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="@days &lt; 0">
                    <span style="color: red"><xsl:value-of select="-number(@days)"/></span>
                </xsl:when>
                <xsl:when test="@days = 0">
                    today
                </xsl:when>
                <xsl:otherwise>
                    after <xsl:value-of select="@days"/>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <!--<td><xsl:value-of select="@days"/></td>-->
    </tr>
</xsl:template>

</xsl:stylesheet>
