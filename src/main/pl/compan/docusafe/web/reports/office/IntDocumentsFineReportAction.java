package pl.compan.docusafe.web.reports.office;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.IntDocumentsFineReport;
import pl.compan.docusafe.service.reports.Report;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Rejestr korespondencji.
 * <p>
 * Raport generujący informacje o pismach wchodzących/wychodzących
 * przez Kancelarię Ogólną.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InDocumentsFineReportAction.java,v 1.12 2008/12/11 12:38:02 pecet1 Exp $
 */
public class IntDocumentsFineReportAction extends EventActionSupport
{
    // @IMPORT
    private boolean forceAnd;
    
    // @EXPORT/@IMPORT
    private String createDateFrom;
	private String createDateTo;
    private String author;
    private Long journalId;
    private String sender;
    private String description;

    // @EXPORT
    private List users;
    private Map journals;
    private boolean canCreateReports;
    private StringManager smL;

    // identyfikator raportu używany przy przekierowaniu do /view.action?id=...
    private Long id;

    public static final String EV_REPORT = "report";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new DoReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
            try
            {
            	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                users =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                
                List<Journal> docJournals = new ArrayList<Journal>();
            	
            	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
                {
            		docJournals = Journal.findByType(Journal.INTERNAL);
            		docJournals.addAll(Journal.findByType(Journal.INTERNAL, true));
                }
            	else
            	{
                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                		docJournals = Journal.findByDivisionGuid(division.getGuid(),Journal.INTERNAL);
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),Journal.INTERNAL,true));
                    }
                    if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
                    	docJournals.add(Journal.getMainInternal());
                    }
                }
                journals = new LinkedHashMap<Long, String>(docJournals.size());
				
				Map<Long,String> closedJournals = new HashMap<Long, String>();
                for (Journal journal:docJournals )
                {
                    try
                    {
						if(journal.isClosed()){
							closedJournals.put(journal.getId(),
								"("+journal.getCyear()+") "+
								(journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
								journal.getDescription());
						} else {
							journals.put(journal.getId(),
								(journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
								journal.getDescription());
						}
                    }
                    catch (DivisionNotFoundException e)
                    {
                        journals.put(journal.getId(), "[" + smL.getString("nieznanyDzial") + "] - "+journal.getDescription());
                    }
                }
				journals.putAll(closedJournals);

                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            Date dateFrom = DateUtils.nullSafeParseJsDate(createDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(createDateTo);

            if (dateFrom == null)
                addActionError(smL.getString("NiePodanoPoczatkowejDatyPrzyjeciaPisma"));

            if (dateTo == null)
                addActionError(smL.getString("NiePodanoKoncowejDatyPrzyjeciaPisma"));

            if (dateFrom != null && dateTo != null && dateFrom.after(dateTo))
                addActionError(smL.getString("DataPoczatkowaJestPozniejszaOdKoncowej"));

            if (journalId == null)
                addActionError(smL.getString("incomingDocument.invalidOutgoingJournal"));

            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    private class DoReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
            ReportCriteria criteria = new ReportCriteria();

            Date dateFrom = DateUtils.nullSafeParseJsDate(createDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(createDateTo);

            try
            {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));

                if (dateFrom != null || dateTo != null)
                    criteria.addRange("incomingDate", dateFrom, dateTo);

                if (!StringUtils.isEmpty(author))
                    criteria.addEq("author", DSUser.findByUsername(author).getName());

                if (journalId != null)
                    criteria.addEq("journal", journalId);

                if (!StringUtils.isEmpty(sender))
                    criteria.addEqText("sender", sender);
                
                if(!StringUtils.isEmpty(description))
                	criteria.addEqText("description", description);

                criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));
                
                Report service = (Report) ServiceManager.getService(Report.NAME);
                ReportHandle handle = service.generateReport(IntDocumentsFineReport.REPORT_ID, criteria,
                    DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad")+" ("+handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public void setForceAnd(boolean forceAnd)
    {
        this.forceAnd = forceAnd;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }


    public List getUsers()
    {
        return users;
    }

    public Long getId()
    {
        return id;
    }

    public Long getJournalId()
    {
        return journalId;
    }

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public Map getJournals()
    {
        return journals;
    }

    public boolean isCanCreateReports()
    {
        return canCreateReports;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }
    public String getCreateDateFrom() {
		return createDateFrom;
	}


	public void setCreateDateFrom(String createDateFrom) {
		this.createDateFrom = createDateFrom;
	}


	public String getCreateDateTo() {
		return createDateTo;
	}


	public void setCreateDateTo(String createDateTo) {
		this.createDateTo = createDateTo;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
