<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/report">
    <fo:root font-family="Arial">

      <fo:layout-master-set>
      <!-- fo:layout-master-set defines in its children the page layout:
           the pagination and layout specifications
          - page-masters: have the role of describing the intended subdivisions
                           of a page and the geometry of these subdivisions
                          In this case there is only a simple-page-master which defines the
                          layout for all pages of the text
      -->
        <!-- layout information -->
        <fo:simple-page-master master-name="simple"
                      page-height="29.7cm"
                      page-width="21cm"
                      margin-top="1cm"
                      margin-bottom="1cm"
                      margin-left="1cm"
                      margin-right="1cm">
          <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
          <fo:region-before extent="1cm"/>
          <!--
          <fo:region-after extent="1.5cm"/>
          -->
        </fo:simple-page-master>
      </fo:layout-master-set>
      <!-- end: defines page layout -->


      <!-- start page-sequence
           here comes the text (contained in flow objects)
           the page-sequence can contain different fo:flows
           the attribute value of master-name refers to the page layout
           which is to be used to layout the text contained in this
           page-sequence-->
      <fo:page-sequence master-reference="simple">

          <fo:static-content flow-name="xsl-region-before">
            <fo:block font-family="Arial" font-size="10pt" text-align="right">
                <!--<fo:external-graphic src="logo_c.gif"/>-->
                Raport zadania
            </fo:block>
          </fo:static-content>

          <!-- start fo:flow
               each flow is targeted
               at one (and only one) of the following:
               xsl-region-body (usually: normal text)
               xsl-region-before (usually: header)
               xsl-region-after  (usually: footer)
               xsl-region-start  (usually: left margin)
               xsl-region-end    (usually: right margin)
               ['usually' applies here to languages with left-right and top-down
                writing direction like English]
               in this case there is only one target: xsl-region-body
            -->
        <fo:flow flow-name="xsl-region-body">

          <!-- each paragraph is encapsulated in a block element
               the attributes of the block define
               font-family and size, line-heigth etc. -->

          <fo:table table-layout="fixed">
            <fo:table-column column-width="1cm"/>
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="4cm"/>
            <fo:table-header>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Typ <br />zadania</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Opis</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Data od</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Data do</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Stan</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Procent<br />wykonania</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Kom�rka wiod�ca<br />/Pracownik</fo:block></fo:table-cell>                                       
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
                <xsl:apply-templates select="data/results/count"/>
            </fo:table-body>
          </fo:table>

          <fo:block text-align="right" font-family="Arial">
            Utworzy�: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
            Data: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>,
          </fo:block>

        </fo:flow> <!-- closes the flow element-->
      </fo:page-sequence> <!-- closes the page-sequence -->
    </fo:root>
</xsl:template>
	<tr>  	
		<td><xsl:value-of select="@typ"/></td>
        <td><xsl:value-of select="@opis"/></td>
        <td><xsl:value-of select="@dataod"/></td>
        <td><xsl:value-of select="@datado"/></td>
        <td><xsl:value-of select="@stan"/></td>
        <td><xsl:value-of select="@procent"/>%</td>
        <td><xsl:value-of select="@komorka"/></td>
	</tr>
  

<xsl:template match="count">
    <fo:table-row>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@typ"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@opis"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@dataod"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@datado"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@stan"/>%</fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@procent"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block><xsl:value-of select="@komorka"/></fo:block>
        </fo:table-cell>
        
    </fo:table-row>
</xsl:template>

</xsl:stylesheet>
