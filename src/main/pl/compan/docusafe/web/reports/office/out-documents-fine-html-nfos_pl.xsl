<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:param name="office-int-document-link"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table style="text-align:center;">
        <tr>
        	<th>Lp.</th>
            <th>Numer KO</th>
            <th>Data pisma</th>
            <th>Autor</th>
            <th>Opis pisma</th>
            <th>Nadawca</th>
            <th>Dzia�</th>
            <th>System dziedzinowy</th>
        </tr>
        <xsl:apply-templates select="data/results/document"/>
    </table>
<xsl:apply-templates select="data/results/totalcount"/>
</xsl:template>

<xsl:template match="criteria">
    
</xsl:template>

<xsl:template match="document">
<tr>
   <td><xsl:value-of select="@lp"/></td>
   <td><xsl:element name="a"><xsl:attribute name="href"><xsl:value-of select="@document-link"/></xsl:attribute><xsl:value-of select="@officeNumber"/></xsl:element></td>
   <td><xsl:value-of select="@incomingDate.date"/></td>
   <td><xsl:value-of select="@author"/></td>
   <td><xsl:value-of select="@description"/></td>
   <td><xsl:value-of select="@senderSummary"/></td>
   <td><xsl:value-of select="@division.name"/></td>
   <td><xsl:value-of select="@systemDziedzinowy"/></td>   
</tr>


</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="6"> </td></tr>
    <tr>
        <td colspan="6">W sumie <xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
