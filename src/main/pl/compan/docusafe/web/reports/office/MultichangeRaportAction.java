package pl.compan.docusafe.web.reports.office;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.reports.MultiChangeReport;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class MultichangeRaportAction  extends EventActionSupport 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 70L;

	private String changeId;
	private String redirectUrl;
	
	protected void setup() 
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGenerate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Generate()).
	        //append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("tomekl").debug("wtf");
        }
    }
	
	private class Generate implements ActionListener
	{
		public void  actionPerformed(ActionEvent event)
		{
			if(changeId == null)
			{
				addActionError("Blad");
				return;
			}
			
			String dockindCn = null;
			Document document = DocumentHelper.createDocument();
			Element root = document.addElement( "raport" );
			Element info = root.addElement( "info" );
			Element tresc = root.addElement( "tresc" );
			String when = null;
			
			int docCount = 0;
			PreparedStatement ps = null;
			
			
			try
			{
				ps = DSApi.context().prepareStatement("select * from ds_datamart where event_id = ?");
				ps.setObject(1, changeId);
				
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					when = pl.compan.docusafe.util.DateUtils.formatCommonDateTime(new Date(rs.getTimestamp("event_date").getTime()));
					info.addElement("who").addText(DSUser.findByUsername(rs.getString("username")).asLastnameFirstnameName());
					info.addElement("when").addText(when);
					info.addElement( "dockind" ).addText(""+rs.getString("change_field_cn"));
					dockindCn = rs.getString("change_field_cn");
				}
				
				rs.close();
				DSApi.context().closeStatement(ps);
				ps = null;
			}
			catch(Exception e)
			{
				LoggerFactory.getLogger("tomekl").debug("error",e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			
			
			try
			{
				DocumentKind dockind = DocumentKind.findByCn(dockindCn);
				ps = DSApi.context().prepareStatement("select * from ds_datamart where session = ? order by document_id");
				ps.setString(1, changeId);
				
				ResultSet rs = ps.executeQuery();
				
				Long documentId = null;
				Element doc = null;
				while(rs.next())
				{
					Long docId = Long.parseLong(rs.getObject("document_id").toString());
					if(!docId.equals(documentId))
					{
						documentId = docId;
						doc = tresc.addElement("document");
						doc.addAttribute("id", ""+documentId);
						docCount++;
					}
					
					Element zmiana = doc.addElement("zmiana");
					zmiana.addAttribute("field", dockind.getFieldByCn(rs.getString("change_field_cn")).getName());
					zmiana.addAttribute("oldval", rs.getString("old_value").length() > 0 ? ""+rs.getString("old_value") : "<brak>");
					zmiana.addAttribute("newval", rs.getString("new_value").length() > 0 ? ""+rs.getString("new_value") : "<brak>");
					
				}
				rs.close();
				DSApi.context().closeStatement(ps);
				ps = null;
			}
			catch(Exception e)
			{
				LoggerFactory.getLogger("tomekl").debug("error",e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			
			info.addElement("count").addText(""+docCount);
			LoggerFactory.getLogger("tomekl").debug("xml : "+document.asXML());
			
			pl.compan.docusafe.core.reports.Report rep = null;
			try
			{
				DSApi.context().begin();
				rep = new pl.compan.docusafe.core.reports.Report(MultiChangeReport.REPORT_ID, "1.0", DSApi.context().getPrincipalName(), "Grupowa zmiana "+when);
				rep.setStatus("done");
				Persister.create(rep);
				DSApi.context().commit();
			}
			catch (Exception e)
			{
				DSApi.context()._rollback();
				LoggerFactory.getLogger("tomekl").debug("wtf",e);
			}
			
			try
			{
				File file = File.createTempFile("docusafe_report_", ".tmp");
				OutputStream output = new BufferedOutputStream(new FileOutputStream(file));
				PrintStream pr = new PrintStream(output, true, "UTF-8");
				pr.print(document.asXML());
				pr.close();
				output.close();
				
				InputStream is = new FileInputStream(file);
				ps = DSApi.context().prepareStatement("update ds_report set contentdata = ?, status = ? where id = ?");
				ps.setBinaryStream(1, is, (int) file.length());
				ps.setString(2, "done");
				ps.setLong(3, rep.getId());
				ps.execute();
				DSApi.context().closeStatement(ps);
				ps = null;
				
			}
			catch (Exception e) 
			{
				LoggerFactory.getLogger("tomekl").debug("error",e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			
			redirectUrl =  "/reports/view.action?id="+rep.getId();
			event.setResult("redirect");			
		}
	}

	public String getChangeId() {
		return changeId;
	}

	public void setChangeId(String changeId) {
		this.changeId = changeId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}
