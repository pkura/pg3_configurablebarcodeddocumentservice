<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:template match="/report">

    <p></p>
    <table>
        <tr>
        	<th>Typ <br />zadania</th>
            <th>Opis</th>
            <th>Data od</th>
            <th>Data do</th>
            <th>Stan</th>
            <th>Procent<br />wykonania</th>
            <th>Kom�rka wiod�ca<br />/Pracownik</th>
            
        </tr>
        <xsl:apply-templates select="data/results/count"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>


<xsl:template match="count">
    <tr>
    	<td><xsl:value-of select="@typ"/></td>
        <td><xsl:value-of select="@opis"/></td>
        <td><xsl:value-of select="@dataod"/></td>
        <td><xsl:value-of select="@datado"/></td>
        <td><xsl:value-of select="@stan"/></td>
        <td><xsl:value-of select="@procent"/>%</td>
        <td><xsl:value-of select="@komorka"/></td>
        
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="3"> </td></tr>
    <tr>
        <td colspan="2">W sumie</td>
        <td align="right"><xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
