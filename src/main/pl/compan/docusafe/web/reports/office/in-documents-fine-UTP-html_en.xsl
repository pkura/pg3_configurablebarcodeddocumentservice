<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:param name="office-in-document-link"/>

<xsl:template match="/report">
    <p>Criterion of choose:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>LP</th>
            <th>General administration</th>
            <th>Receive date</th>
            <th>Script date</th>
            <th>Script description</th>
            <th>Recipient</th>
            <th>Division</th>
            <th>Location</th>
        </tr>
        <xsl:apply-templates select="data/results/document"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Journal: <b><xsl:value-of select="eq[@property='journal']/@name"/></b></td>
        <td>Recipient: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
    </tr>
    <tr>
        <td>Script kind: <b><xsl:value-of select="eq[@property='kind']/@name"/></b></td>
        <td>Delivery method: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
        <td>Receive method: <b><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></b></td>
    </tr>
    <tr>
        <td colspan="2">Receive date:
            <xsl:if test="range[@property='incomingDate']/@start.date">
                <b>from <xsl:value-of select="range[@property='incomingDate']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='incomingDate']/@end.date">
                <b> to <xsl:value-of select="range[@property='incomingDate']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Sender: <b><xsl:value-of select="eq[@property='sender']/@value"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="document">
    <tr>
    	<td><xsl:value-of select="@lp"/></td>
        <td><xsl:element name="a">
                <xsl:attribute name="href"><xsl:value-of select="$office-in-document-link"/><xsl:value-of select="@documentId"/></xsl:attribute>
                    <xsl:value-of select="@officeNumber"/>
            </xsl:element>
        </td>
        <td><xsl:value-of select="@incomingDate.date"/></td>
        <td><xsl:value-of select="@date.date"/></td>
        <td><xsl:value-of select="@summary"/></td>
        <td><xsl:value-of select="@senderSummary"/></td>
        <td><xsl:value-of select="@division.name"/></td>
        <td><xsl:value-of select="@location"/></td>
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="6"> </td></tr>
    <tr>
        <td colspan="6">Total <xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
