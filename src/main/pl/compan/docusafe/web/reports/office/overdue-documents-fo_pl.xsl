<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/report">
    <fo:root font-family="Arial">

      <fo:layout-master-set>
      <!-- fo:layout-master-set defines in its children the page layout:
           the pagination and layout specifications
          - page-masters: have the role of describing the intended subdivisions
                           of a page and the geometry of these subdivisions
                          In this case there is only a simple-page-master which defines the
                          layout for all pages of the text
      -->
        <!-- layout information -->
        <fo:simple-page-master master-name="simple"
                      page-height="21cm"
                      page-width="29.7cm"
                      margin-top="1cm"
                      margin-bottom="1cm"
                      margin-left="1cm"
                      margin-right="1cm">
          <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
          <fo:region-before extent="1cm"/>
          <!--
          <fo:region-after extent="1.5cm"/>
          -->
        </fo:simple-page-master>
      </fo:layout-master-set>
      <!-- end: defines page layout -->


      <!-- start page-sequence
           here comes the text (contained in flow objects)
           the page-sequence can contain different fo:flows
           the attribute value of master-name refers to the page layout
           which is to be used to layout the text contained in this
           page-sequence-->
      <fo:page-sequence master-reference="simple">

          <fo:static-content flow-name="xsl-region-before">
            <fo:block font-family="Arial" font-size="10pt" text-align="right">
                Raport pism przeterminowanych
            </fo:block>
          </fo:static-content>

          <!-- start fo:flow
               each flow is targeted
               at one (and only one) of the following:
               xsl-region-body (usually: normal text)
               xsl-region-before (usually: header)
               xsl-region-after  (usually: footer)
               xsl-region-start  (usually: left margin)
               xsl-region-end    (usually: right margin)
               ['usually' applies here to languages with left-right and top-down
                writing direction like English]
               in this case there is only one target: xsl-region-body
            -->
        <fo:flow flow-name="xsl-region-body">

          <!-- each paragraph is encapsulated in a block element
               the attributes of the block define
               font-family and size, line-heigth etc. -->

          <xsl:apply-templates select="data/criteria"/>

          <fo:table table-layout="fixed">
            <fo:table-column column-width="2cm"/>

            <fo:table-column column-width="3.5cm"/>
            <fo:table-column column-width="6cm"/>
            <fo:table-column column-width="0.5cm"/>
<!--
            <fo:table-column column-width="4cm"/>
            <fo:table-column column-width="2cm"/>
            <fo:table-column column-width="3cm"/>
-->
            <fo:table-column column-width="3cm"/>
			<fo:table-column column-width="0.5cm"/>
            <fo:table-column column-width="9cm"/>
            <fo:table-column column-width="2cm"/>
            <fo:table-header>
                <fo:table-row>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">KO</fo:block></fo:table-cell>

                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Data przyj�cia</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Tre��</fo:block></fo:table-cell>
                    <fo:table-cell/>
<!--
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Numer w sprawie</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Przyj��</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Osoba</fo:block></fo:table-cell>
-->
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Nadawca</fo:block></fo:table-cell>
					<fo:table-cell/>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Dzia�</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block font-family="Arial" font-weight="bold">Przeterm.</fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
                <xsl:apply-templates select="data/results/document"/>
            </fo:table-body>
          </fo:table>

          <fo:block text-align="right" font-family="Arial">
            Utworzy�: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
            Data: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>,
          </fo:block>

        </fo:flow> <!-- closes the flow element-->
      </fo:page-sequence> <!-- closes the page-sequence -->
    </fo:root>
</xsl:template>

<xsl:template match="criteria">
  <fo:block font-family="Arial">Kryteria wyboru:</fo:block>
  <fo:table table-layout="fixed" width="100%" space-after.optimum="13pt">
    <fo:table-column />
    <fo:table-column />
    <fo:table-column />
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell><fo:block font-family="Arial">Spos�b dostarczenia: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='delivery']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Spos�b odbioru: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Osoba przyjmuj�ca: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='author']/@name"/></fo:inline></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell number-columns-spanned="3">
                <fo:block font-family="Arial">
                Typy dokument�w:
                    <fo:inline font-weight="bold">
                        <xsl:for-each select="enumeration[@property='kinds']/value">
                            <xsl:value-of select="@name"/><xsl:if test="following-sibling::value">, </xsl:if>
                        </xsl:for-each>
                    </fo:inline>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell number-columns-spanned="2"><fo:block font-family="Arial">
                Data przyj�cia: <fo:inline font-weight="bold">od <xsl:value-of select="range[@property='incomingDate']/@start.date"/>
                do <xsl:value-of select="range[@property='incomingDate']/@end.date"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Dziennik: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='journalId']/@name"/></fo:inline></fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell><fo:block font-family="Arial">Nadawca: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='sender']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Odbiorca: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='recipient']/@name"/></fo:inline></fo:block></fo:table-cell>
            <fo:table-cell><fo:block font-family="Arial">Dzia�: <fo:inline font-weight="bold"><xsl:value-of select="eq[@property='divisionGuid']/@name"/></fo:inline></fo:block></fo:table-cell>
        </fo:table-row>
    </fo:table-body>
  </fo:table>
</xsl:template>

<xsl:template match="document">
    <fo:table-row>
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@officeId"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@date.incomingDate"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@summary"/></fo:block>
        </fo:table-cell>
		<fo:table-cell/>
<!--
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@caseId"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@author"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@currentUser"/></fo:block>
        </fo:table-cell>
-->
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@senderSummary"/></fo:block>
        </fo:table-cell>
		<fo:table-cell/>        
        <fo:table-cell>
            <fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="@division"/></fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <xsl:choose>
                <xsl:when test="@days &lt; 0">
                    <fo:block font-family="Arial" font-size="10pt" text-align="right"><xsl:value-of select="number(@days)"/></fo:block>
                </xsl:when>
                <xsl:when test="@days = 0">
                    <fo:block font-family="Arial" font-size="10pt" text-align="right">dzisiaj</fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block font-family="Arial" font-size="10pt" text-align="right">za <xsl:value-of select="@days"/></fo:block>
                </xsl:otherwise>
            </xsl:choose>
        </fo:table-cell>
    </fo:table-row>
</xsl:template>

</xsl:stylesheet>
