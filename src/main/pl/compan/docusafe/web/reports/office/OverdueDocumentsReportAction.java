package pl.compan.docusafe.web.reports.office;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.OverdueDocumentsReport;
import pl.compan.docusafe.service.reports.Report;
import pl.compan.docusafe.service.reports.ReportCriteria;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OverdueDocumentsReportAction.java,v 1.26 2008/10/29 22:00:50 wkuty Exp $
 */
public class OverdueDocumentsReportAction extends EventActionSupport
{
    public static final String EV_REPORT = "report";
    
    private List users;
    private List deliveries;
    private List kinds;
    private List outgoingDeliveries;
    private boolean canCreateReports;
    private boolean canSeeDivision;
    private boolean canSeeJournals;
    private Map<String, String> sortFields = new LinkedHashMap<String, String>();
    private Map<Long, String> journals = new LinkedHashMap<Long, String>();

    private String incomingDateFrom;
    private String incomingDateTo;
    private String lastDays;
    //private Integer kindId;
    private Integer[] kindIds;
    private String author;
    private Integer deliveryId;
    private Integer outgoingDeliveryId;
    private Integer warnDays;
    private boolean useWarnDays;
    private String sortField;
    private boolean showOverdues;
    private boolean forceAnd;
    private String sender;
    private String recipient;
    private Long journalId;
    private String divisionGuid;
    private String divisionName;
    private String reportTemplateName;

    private Long id;
    
    private StringManager smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new DoReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT,new DoSaveReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                //smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                
                
                kinds = InOfficeDocumentKind.list();
                users =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);

                deliveries = OutOfficeDocumentDelivery.list();
                outgoingDeliveries = OutOfficeDocumentDelivery.list();

                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
                
                    DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
                    if (divisions.length > 0)
                    {
                        divisionGuid = divisions[0].getGuid();
                        divisionName = divisions[0].getPrettyPath();
                    }
                

                // domy�lnie ostrze�enie, je�eli termin odpowiedzi up�ywa
                // za dwa dni
                if (warnDays == null)
                    warnDays = new Integer(2);
                
                

                sortFields.put("overdueDays", smL.getString("LiczbaDniPrzeterminowania"));
                sortFields.put("officeNumber", smL.getString("NumerKO"));
                sortFields.put("incomingDate", smL.getString("DataPrzyjecia"));
                sortFields.put("summary", smL.getString("Opis"));
                sortFields.put("caseId", smL.getString("NumerWSprawie"));
                sortFields.put("currentUser", smL.getString("Uzytkownik"));
                sortFields.put("author", smL.getString("PrzyjmujacyPismo"));
                sortFields.put("division", smL.getString("Dzial"));
                journals = new HashMap<Long, String>();
                
                
                List<Journal> docJournals = new ArrayList<Journal>();
            	
            	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
                {
            		docJournals = Journal.findByType(Journal.INCOMING);
            		docJournals.addAll(Journal.findByType(Journal.INCOMING, true));
                }
            	else
            	{
                    for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {
                		docJournals = Journal.findByDivisionGuid(division.getGuid(),Journal.INCOMING);
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),Journal.INCOMING,true));
                    }
                    if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
                    	docJournals.add(Journal.getMainInternal());
                    }
                }
                journals = new LinkedHashMap<Long, String>(docJournals.size());
                for (Journal journal:docJournals )
                {
                    try
                    {
                        journals.put(journal.getId(),
                            (journal.isClosed() ? "("+journal.getCyear()+") " : "") +
                            (journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
                            journal.getDescription());
                    }
                    catch (DivisionNotFoundException e)
                    {
                        journals.put(journal.getId(), "[" + smL.getString("nieznanyDzial") + "] - "+journal.getDescription());
                    }
                }
                    

                
                //catch(BackingStoreException bse){}
                
                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            smL = GlobalPreferences.loadPropertiesFile(OverdueDocumentsReportAction.class.getPackage().getName(), null);
            
            Date dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);

            if (dateFrom == null && dateTo == null && lastDays==null)
                addActionError(smL.getString("MusiszOkreslicDatePoczatkowaIKoncowaLubWypelnicPoleZOstatnichDni"));          

            if (dateFrom != null && dateTo != null && dateFrom.after(dateTo))
                addActionError(smL.getString("DataPoczatkowaJestPozniejszaOdKoncowej"));

            if (useWarnDays && (warnDays == null || warnDays.intValue() < 0))
                addActionError(smL.getString("LiczbaDniDoOstrzezeniaNiePowinnaBycMniejszaOdZera"));

            if (!useWarnDays && !showOverdues)
                addActionError(smL.getString("NalezyWybracOpcjePokazywaniaPismJuzPrzeterminowanychLubPismZblizajacychSieDoPrzeterminowania."));

            try
            {	
            	
            	boolean divRights = checkRights(divisionGuid);
        		canSeeDivision = DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD)||(DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD)&&divRights);
        		if(!canSeeDivision) 
        			addActionError(smL.getString("NieMaszUprawnienDoPodgladuPismWtejKomorce"));
        		
            	
            	if(journalId!=null){
        			if((Journal.getMainIncoming().getId()==journalId||Journal.getMainInternal().getId()==journalId||Journal.getMainOutgoing().getId()==journalId)  &&  !(DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)||DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
        				addActionError(smL.getString("NieMaszUprawnienDoPogladuDziennikowGlownych"));
        			canSeeJournals = DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)||(DSApi.context().hasPermission(DSPermission.DZIENNIK_KOMORKA_PODGLAD)&&divRights);
        			if(!canSeeJournals) 
        				addActionError(smL.getString("NieMaszUprawnienDoPodgladuTegoDziennika"));
            	}

            	
            	
            }
            catch (EdmException e)
            {
                addActionError(smL.getString("BladPolaczeniaZBazaDanych"));
            }

/*
            if (dateTo != null && dateTo.after(new Date()))
                addActionError("Data ko�cowa znajduje si� w przysz�o�ci");
*/
            if(lastDays!=null)
            {
                try
                {
                    Integer.parseInt(lastDays);
                    if (dateFrom!=null && dateTo!=null && !hasActionErrors())
                    {
                        addActionMessage(smL.getString("PodaneZostalyDataPoczatkowaDataKoncowaOrazOpcjaOstatnichDni"));
                        addActionMessage(smL.getString("OpcjaZOstatnichDniBedzieWzietaPodUwage"));
                    }
                }
                catch(NumberFormatException nfe)
                {
                    addActionError(smL.getString("PoleZOstatnichDniJestWypelnioneNieprawidlowo"));
                }
            }
            
            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    private class DoSaveReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Integer days;
            Date dateFrom = null, dateTo=null;
            PreparedStatement ps = null;
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException("BrakUprawnienDoTworzeniaRaportu");
                
                ps = DSApi.context().prepareStatement("SELECT * FROM DS_REPORT_TEMPLATE WHERE REPORTNAME=?");
                ps.setString(1, reportTemplateName);
                ResultSet rs = ps.executeQuery();
                
                if(rs.next())
                    throw new EdmException(smL.getString("IstniejeJuzSzablonRaportuOTakiejNazwie"));
                DSApi.context().closeStatement(ps);
                if(lastDays!=null)
                {
                    days = Integer.parseInt(lastDays);
                    if(days<0)
                        throw new EdmException(smL.getString("LiczbaOstatnichDniNieMozeBycUjemna"));
                }
                else
                {
                    dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
                    dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);
                    if(dateFrom==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
                    if(dateTo==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
                }

                if(reportTemplateName==null || reportTemplateName.length()==0)
                    throw new EdmException(smL.getString("BrakNazwyRaportu"));
                
                StringBuffer sql = new StringBuffer();
                
                sql.append("INSERT INTO DS_REPORT_TEMPLATE (");
                
                if(lastDays==null)
                    sql.append("INCOMINGDATEFROM ,INCOMINGDATETO ");
                else 
                    sql.append("LASTDAYS");
                
                if (kindIds != null && kindIds.length > 0)
                    sql.append(",KINDIDS ");

                if (!StringUtils.isEmpty(author))
                    sql.append(",AUTHOR ");
               
                sql.append(",DELIVERYID ,OUTGOINGDELIVERYID ,WARNDAYS ");

                if (!StringUtils.isEmpty(sender))
                    sql.append(",SENDER ");

                if (!StringUtils.isEmpty(recipient))
                    sql.append(",RECIPIENT ");

                if (journalId != null)
                    sql.append(",JOURNALID ");
                else if (!StringUtils.isEmpty(divisionGuid))
                    sql.append(",DIVISIONGUID ");

                sql.append(",SHOWOVERDUES ,FORCEAND ,SORTFIELD, REPORTTYPE, REPORTNAME, CREATEDATE) VALUES ( ");
                
                if(lastDays==null)
                    sql.append("'"+incomingDateFrom+"','" + incomingDateTo + "'");
                else 
                    sql.append(lastDays);
                
                if (kindIds != null && kindIds.length > 0)
                {
                    StringBuffer tmp = new StringBuffer();
                    for(int i=0;i< kindIds.length-1; i++)
                        tmp.append(kindIds[i].toString() + ",");
                    tmp.append(kindIds[kindIds.length-1].toString());
                    sql.append(",'" + tmp.toString() + "'");
                }
                
                if (!StringUtils.isEmpty(author))
                    sql.append(",'" + author + "'");
                
                //jesli deliveryId==null to -1
                if (deliveryId != null)
                    sql.append(","  + String.valueOf(deliveryId));
                else
                    sql.append(",-1");
                
                ////jesli outgoing...==null to -1
                if (outgoingDeliveryId != null)
                    sql.append("," + String.valueOf(outgoingDeliveryId));
                else 
                    sql.append(",-1");
                
                if (!useWarnDays)
                    sql.append(",-1");
                else
                    sql.append("," + String.valueOf(warnDays));

                if (!StringUtils.isEmpty(sender))
                    sql.append(",'"+ sender +"'");

                if (!StringUtils.isEmpty(recipient))
                    sql.append(",'"+recipient+"'");

                if (journalId != null)
                    sql.append("," +String.valueOf(journalId));
                else if (!StringUtils.isEmpty(divisionGuid))
                    sql.append(",'"+String.valueOf(divisionGuid)+"'");

                sql.append(","+(showOverdues?"1":"0"));
                
                sql.append(","+(forceAnd?"1":"0"));
                
                sql.append(",'"+sortField+"'");
                
                sql.append(",'OVERDUE'");
                
                sql.append(",'" + reportTemplateName + "'");
                
                Date data = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(data);
                sql.append(",'" + cal.get(Calendar.DAY_OF_MONTH)+ "-" +cal.get(Calendar.MONTH)+ "-" + cal.get(Calendar.YEAR) + "'");
               ////////////////////////////////////////////////
                
                sql.append(");");
                
                ps = DSApi.context().prepareStatement(sql.toString());
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);
                addActionMessage(smL.getString("RaportZapisano"));
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
            	DSApi.context().closeStatement(ps);
            }
        }
    }
    
    private class DoReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            ReportCriteria criteria = new ReportCriteria();

            try
            {
                Integer days;
                Date dateFrom = null, dateTo=null;
                
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));
                
                if(lastDays!=null)
                {
                    try
                    {
                        days = Integer.parseInt(lastDays);
                    }
                    catch(NumberFormatException nfe)
                    {
                        throw new EdmException(smL.getString("PoleZOstatnichDniJestWypelnioneNieprawidlowo"));
                    }
                    dateTo = new Date();
                    dateFrom = new Date(); 
                    dateFrom = DateUtils.plusDays(dateFrom, days*-1);
                }
                else
                {
                    dateFrom = DateUtils.nullSafeParseJsDate(incomingDateFrom);
                    dateTo = DateUtils.nullSafeParseJsDate(incomingDateTo);
                    if(dateFrom==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
                    if(dateTo==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
                }
                    

                
                criteria.addRange("incomingDate", dateFrom, dateTo);

                if (kindIds != null && kindIds.length > 0)
                    criteria.addEnumeration("kinds", kindIds);

                if (!StringUtils.isEmpty(author))
                    criteria.addEq("author", DSUser.findByUsername(author).getName());

                if (deliveryId != null)
                    criteria.addEq("delivery", deliveryId);

                if (outgoingDeliveryId != null)
                    criteria.addEq("outgoingDelivery", outgoingDeliveryId);

                if (useWarnDays && warnDays != null && warnDays.intValue() > 0)
                    criteria.setAttribute("warnDays", warnDays);

                if (!StringUtils.isEmpty(sender))
                    criteria.addEqText("sender", sender);

                if (!StringUtils.isEmpty(recipient))
                    criteria.addEqText("recipient", recipient);

                if (journalId != null)
                    criteria.addEq("journalId", journalId);
                if (!StringUtils.isEmpty(divisionGuid))
                    criteria.addEq("divisionGuid", divisionGuid);

                criteria.setAttribute("showOverdues", Boolean.valueOf(showOverdues));

                criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));
                
                criteria.setAttribute("sortField", sortField);

                Report service = (Report) ServiceManager.getService(Report.NAME);
                ReportHandle handle = service.generateReport(OverdueDocumentsReport.REPORT_ID, criteria,
                    DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad") + "(" +handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private boolean checkRights(String guid)throws EdmException
    {

    	try{
    		DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
    		if(guid == DSDivision.ROOT_GUID || guid==null||guid=="") return false;
    		else{
    			DSDivision current = DSDivision.find(guid);
    	
    			while(current.getGuid()!=DSDivision.ROOT_GUID){
    				for(int i=0;i<divisions.length;i++){
    					if(divisions[i].getGuid()==current.getGuid()) return true;
    				}
    				current = current.getParent();
    				if(current==null) break;
    			}
    		}
    	
    	}
    	catch(EdmException e){
    		addActionError(smL.getString("WystapilBlad")+": "+e.getMessage());
    	}
    	return false;
    }
    
    public List getUsers()
    {
        return users;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public boolean isCanCreateReports()
    {
        return canCreateReports;
    }

    public List getKinds()
    {
        return kinds;
    }

    public List getOutgoingDeliveries()
    {
        return outgoingDeliveries;
    }

    public void setIncomingDateFrom(String incomingDateFrom)
    {
        this.incomingDateFrom = incomingDateFrom;
    }

    public void setIncomingDateTo(String incomingDateTo)
    {
        this.incomingDateTo = incomingDateTo;
    }

    public void setKindIds(Integer[] kindIds)
    {
        this.kindIds = kindIds;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public void setOutgoingDeliveryId(Integer outgoingDeliveryId)
    {
        this.outgoingDeliveryId = outgoingDeliveryId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setWarnDays(Integer warnDays)
    {
        this.warnDays = warnDays;
    }

    public Integer getWarnDays()
    {
        return warnDays;
    }

    public String getIncomingDateFrom()
    {
        return incomingDateFrom;
    }

    public String getIncomingDateTo()
    {
        return incomingDateTo;
    }

    public Integer[] getKindIds()
    {
        return kindIds;
    }

    public String getAuthor()
    {
        return author;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public Integer getOutgoingDeliveryId()
    {
        return outgoingDeliveryId;
    }

    public Map getSortFields()
    {
        return sortFields;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isUseWarnDays()
    {
        return useWarnDays;
    }

    public void setUseWarnDays(boolean useWarnDays)
    {
        this.useWarnDays = useWarnDays;
    }

    public boolean isShowOverdues()
    {
        return showOverdues;
    }

    public void setShowOverdues(boolean showOverdues)
    {
        this.showOverdues = showOverdues;
    }

    public void setForceAnd(boolean forceAnd)
    {
        this.forceAnd = forceAnd;
    }
    
    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public String getRecipient()
    {
        return recipient;
    }

    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }

    public Map getJournals()
    {
        return journals;
    }

    public Long getJournalId()
    {
        return journalId;
    }

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getDivisionName()
    {
        return divisionName;
    }

    public void setDivisionName(String divisionName)
    {
        this.divisionName = divisionName;
    }

    public String getReportTemplateName() {
        return reportTemplateName;
    }

    public void setReportTemplateName(String reportTemplateName) {
        this.reportTemplateName = reportTemplateName;
    }

    public void setLastDays(String lastDays) {
        this.lastDays = lastDays;
    }
}
