<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:template match="/report">
        <p>Kryteria wyboru:</p>
        <table>
            <xsl:apply-templates select="data/criteria"/>
        </table>
        <p></p>
        <table>
            <tr>
                <th>Lp.</th>
                <th>Id dokumentu</th>
                <th>Proces</th>
                <th>Status (CN)</th>
                <th>Data rozpocz�cia</th>
                <th>Data zako�czenia</th>
                <th>Czas wykonywania</th>
                <th>Op�nienie</th>
                <th>Dzia�</th>
                <th>U�ytkownik</th>
            </tr>
            <xsl:apply-templates select="data/results/document"/>
            <xsl:apply-templates select="data/results/totalcount"/>
        </table>
        <table>
            <xsl:apply-templates select="data/results/durationMean"/>
            <xsl:apply-templates select="data/results/delayMean"/>
        </table>
    </xsl:template>

    <xsl:template match="criteria">
        <tr>
            <!--I kolumna-->
            <td width="120px">Id dokumnetu:</td>
            <td width="450px"><b><xsl:value-of select="eq[@property='documentId']/@name"/></b></td>

            <!--odst�p-->
            <td width="50px"></td>

            <!--II kolumna-->
            <td width="200px">Przedzia� czasowy:</td>
            <td width="200px">od: <b><xsl:value-of select="eq[@property='intervalFrom']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Dzia�:</td>
            <td><b><xsl:value-of select="eq[@property='division']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>do: <b><xsl:value-of select="eq[@property='intervalTo']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>U�ytkownik:</td>
            <td><b><xsl:value-of select="eq[@property='user']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>tylko niezako�czone: <b><xsl:value-of select="eq[@property='onlyNotCompleted']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td></td>
            <td></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td>Maksymalny czas wykonywania:</td>
            <td><b><xsl:value-of select="eq[@property='durationMillis']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Rodzaj dokumentu:</td>
            <td><b><xsl:value-of select="eq[@property='documentKind']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td>Op�nienie:</td>
            <td><b><xsl:value-of select="eq[@property='delayMillis']/@name"/></b></td>
        </tr>
        <tr>
            <!--I kolumna-->
            <td>Etap procesu:</td>
            <td><b><xsl:value-of select="eq[@property='status']/@name"/></b></td>

            <!--odst�p-->
            <td></td>

            <!--II kolumna-->
            <td></td>
            <td>tylko op�nione: <b><xsl:value-of select="eq[@property='onlyDelayed']/@name"/></b></td>
        </tr>
    </xsl:template>

    <xsl:template match="document">
        <tr>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@lp"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@documentId"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@processName"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@status"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@start"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@end"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@duration"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@delay"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@division"/></td>
            <td style="padding:0 5px 0 5px;"><xsl:value-of select="@user"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="totalcount">
        <tr><td colspan="6"> </td></tr>
        <tr>
            <td colspan="6">W sumie: <xsl:value-of select="@count"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="durationMean">
        <tr>
            <td>�redni czas wykonywania proces�w (dla zako�czonych proces�w): </td>
            <td><xsl:value-of select="@value"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="delayMean">
        <tr>
            <td>�redni czas op�nienia proces�w (dla op�nionych proces�w): </td>
            <td><xsl:value-of select="@value"/></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
