package pl.compan.docusafe.web.reports.office;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Rejestr korespondencji.
 * <p>
 * Raport generuj�cy informacje o pismach wchodz�cych/wychodz�cych
 * przez Kancelari� Og�ln�.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutDocumentsReportAction.java,v 1.26 2009/02/05 12:17:30 wkuty Exp $
 */
public class OutDocumentsReportAction extends EventActionSupport
{
    // @IMPORT
    private boolean forceAnd;
    private String lastDays;
    private String reportTemplateName;
    
    // @EXPORT/@IMPORT
    private String documentDateFrom; //data przyjecia pisma
    private String documentDateTo;
    private String author;
    private Long journalId;
    private Integer deliveryId;
    private boolean internal;
    private String recipient;

    // @EXPORT
    private List users;
    private Map journals;
    private List deliveries;
    private boolean canCreateReports;
    private StringManager smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

    // identyfikator raportu u�ywany przy przekierowaniu do /view.action?id=...
    private Long id;

    public static final String EV_REPORT = "report";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new DoReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(EV_REPORT, new SaveReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	smL = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);
                users =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);

                List<Journal> docJournals = new ArrayList<Journal>();
            	String journalType = "";
            	
                if(internal)
        			journalType = Journal.INTERNAL;
                else
        			journalType = Journal.OUTGOING;
        		
            	if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
                {
            		docJournals = Journal.findByType(journalType);
            		docJournals.addAll(Journal.findByType(journalType, true));
                }
            	else
            	{            
                   if(docJournals == null)
                   {
                	   docJournals = new ArrayList<Journal>(); 
                   }
            		for (DSDivision division : DSApi.context().getDSUser().getDivisions())
                    {                    	                       
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),journalType));
                		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),journalType,true));
                    }
                    
                    if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)){
                    	if(internal)
                    		docJournals.add(Journal.getMainInternal());
                    	else
                			docJournals.add(Journal.getMainOutgoing());
                    }
                }
                

                journals = new LinkedHashMap<Long, String>(docJournals.size());
                for (Journal journal:docJournals )
                {
                    try
                    {
                        journals.put(journal.getId(),
                            (journal.isClosed() ? "("+journal.getCyear()+") " : "") +
                            (journal.getOwnerGuid() != null ? DSDivision.find(journal.getOwnerGuid()).getName()+" - " : "") +
                            journal.getDescription());
                    }
                    catch (DivisionNotFoundException e)
                    {
                        journals.put(journal.getId(), "[" + smL.getString("nieznanyDzial") + "] - "+journal.getDescription());
                    }
                }


                deliveries = OutOfficeDocumentDelivery.list();

                canCreateReports = DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Date dateFrom = DateUtils.nullSafeParseJsDate(documentDateFrom);
            Date dateTo = DateUtils.nullSafeParseJsDate(documentDateTo);
            
            if (dateFrom == null && dateTo == null && lastDays==null)
                addActionError(smL.getString("MusiszOkreslicDatePoczatkowaIKoncowaLubWypelnicPoleZOstatnichDni"));

            if (dateFrom != null && dateTo != null && dateFrom.after(dateTo))
                addActionError(smL.getString("DataPoczatkowaJestPozniejszaOdKoncowej"));
            
/*
            if (dateTo != null && dateTo.after(new Date()))
                addActionError("Data ko�cowa znajduje si� w przysz�o�ci");
*/

            if (journalId == null)
                addActionError(smL.getString("NieWybranoDzialu"));
            
            if(lastDays!=null)
            {
                try
                {
                    Integer.parseInt(lastDays);
                    if (dateFrom!=null && dateTo!=null && !hasActionErrors())
                    {
                        addActionMessage(smL.getString("PodaneZostaly")+": "+smL.getString("DataPoczatkowaDataKoncowaOrazOpcjaOstatnichDni"));
                        addActionMessage(smL.getString("OpcjaOstatnichDniBedzieWzietaPodUwage"));
                    }
                }
                catch(NumberFormatException nfe)
                {
                    addActionError(smL.getString("PoleZostatnichDniJestWypelnioneNieprawidlowo"));
                }
            }

            if (hasActionErrors())
                event.skip(EV_REPORT);
        }
    }

    private class DoReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {       
            ReportCriteria criteria = new ReportCriteria();
            Integer days;
            Date dateFrom = null, dateTo = null;

            try
            {                
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                       throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));
                
                
                if(lastDays!=null)
                {
                    try
                    {
                        days = Integer.parseInt(lastDays);
                    }
                    catch(NumberFormatException nfe)
                    {
                        throw new EdmException(smL.getString("PoleZostatnichDniJestWypelnioneNieprawidlowo"));
                    }
                    dateTo = new Date();
                    dateFrom = new Date(); 
                    dateFrom = DateUtils.plusDays(dateFrom, days*-1);
                }
                else
                {
                    dateFrom = DateUtils.nullSafeParseJsDate(documentDateFrom);
                    dateTo = DateUtils.nullSafeParseJsDate(documentDateTo);
                    if(dateFrom==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
                    if(dateTo==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
                }
                
                
                if (dateFrom != null || dateTo != null)
                    criteria.addRange("ctime", dateFrom, dateTo);

                if (!StringUtils.isEmpty(author))
                    criteria.addEq("author", DSUser.findByUsername(author).getName());

                
                criteria.addEq("journal", journalId);

                if (deliveryId != null)
                    criteria.addEq("delivery", deliveryId);

                if (!StringUtils.isEmpty(recipient))
                    criteria.addEqText("recipient", recipient);

                criteria.setAttribute("forceAnd", Boolean.valueOf(forceAnd));
                
                Report service = (Report) ServiceManager.getService(Report.NAME);

                // XXX: zmiana
                ReportHandle handle;

                if (internal)
                    handle = service.generateReport(IntDocumentsReport.REPORT_ID, criteria,
                        DSApi.context().getPrincipalName());
                else
                    handle = service.generateReport(OutDocumentsReport.REPORT_ID, criteria,
                        DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError(smL.getString("WystapilBlad")+" ("+handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class SaveReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            ReportCriteria criteria = new ReportCriteria();

            Integer days;
            Date dateFrom = null, dateTo = null;
            StringBuffer sql = new StringBuffer();
            PreparedStatement ps = null;
            try
            {
                if (!DSApi.context().hasPermission(DSPermission.RAPORTY_TWORZENIE))
                    throw new EdmException(smL.getString("BrakUprawnienDoTworzeniaRaportu"));
               
                ps = DSApi.context().prepareStatement("SELECT * FROM DS_REPORT_TEMPLATE WHERE REPORTNAME=?");
                ps.setString(1, reportTemplateName);
                ResultSet rs = ps.executeQuery();
                
                if(rs.next())
                    throw new EdmException(smL.getString("IstniejeJuzSzablonRaportuOtakiejNazwie"));
                DSApi.context().closeStatement(ps);
                if(reportTemplateName==null || reportTemplateName.length()==0)
                    throw new EdmException(smL.getString("BrakNazwyRaportu"));
                
                if(lastDays!=null)
                {
                    days = Integer.parseInt(lastDays);
                    if(days<0)
                        throw new EdmException(smL.getString("LiczbaOstatnichDniNieMozeBycUjemna"));
                }
                else
                {
                    dateFrom = DateUtils.nullSafeParseJsDate(documentDateFrom);
                    dateTo = DateUtils.nullSafeParseJsDate(documentDateTo);
                    if(dateFrom==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaOd"));
                    if(dateTo==null)
                        throw new EdmException(smL.getString("NieprawidlowoWypelnionePoleDataPrzyjeciaDo"));
                }
                
                sql.append("INSERT INTO DS_REPORT_TEMPLATE (");
                
                if(lastDays==null)
                    sql.append("INCOMINGDATEFROM ,INCOMINGDATETO ");
                else 
                    sql.append("LASTDAYS");

                if (!StringUtils.isEmpty(author))
                    sql.append(",AUTHOR");

                sql.append(",JOURNALID,DELIVERYID,WARNDAYS");

                if (!StringUtils.isEmpty(recipient))
                    sql.append("RECIPIENT");

                sql.append(",FORCEAND,REPORTTYPE,REPORTNAME,CREATEDATE) VALUES (");
                
                
                if(lastDays==null)
                    sql.append("'"+documentDateFrom+"','" + documentDateTo + "'");
                else 
                    sql.append(lastDays);
                
                if (!StringUtils.isEmpty(author))
                    sql.append(",'" + author + "'");
                
                sql.append("," +String.valueOf(journalId));
                
                if (deliveryId != null)
                    sql.append(","  + String.valueOf(deliveryId));
                else
                    sql.append(",-1");
                
                sql.append(",-1");
                
                if (!StringUtils.isEmpty(recipient))
                    sql.append(",'"+recipient+"'");
                
                sql.append(","+(forceAnd?"1":"0"));
                
                sql.append(","+(internal?"'INTERNAL'":"'OUT'"));
                
                sql.append(",'"+reportTemplateName+"'");
                
                Date data = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(data);
                sql.append(",'" + cal.get(Calendar.DAY_OF_MONTH)+ "-" +cal.get(Calendar.MONTH)+ "-" + cal.get(Calendar.YEAR) + "'");

                sql.append(")");
                //addActionMessage(sql.toString());
                ps = DSApi.context().prepareStatement(sql.toString());
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);
                addActionMessage(smL.getString("RaportZapisano"));

            }
            catch (SQLException sqle)
            {
                addActionError(sqle.getMessage());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            finally
            {
            	DSApi.context().closeStatement(ps);
            }
        }
    }
    
    public void setForceAnd(boolean forceAnd)
    {
        this.forceAnd = forceAnd;
    }
    
    public String getDocumentDateFrom()
    {
        return documentDateFrom;
    }

    public void setDocumentDateFrom(String documentDateFrom)
    {
        this.documentDateFrom = documentDateFrom;
    }

    public String getDocumentDateTo()
    {
        return documentDateTo;
    }

    public void setDocumentDateTo(String documentDateTo)
    {
        this.documentDateTo = documentDateTo;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public List getUsers()
    {
        return users;
    }

    public Long getId()
    {
        return id;
    }

    public Long getJournalId()
    {
        return journalId;
    }

    public void setJournalId(Long journalId)
    {
        this.journalId = journalId;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public Map getJournals()
    {
        return journals;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public boolean isCanCreateReports()
    {
        return canCreateReports;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public void setInternal(boolean internal)
    {
        this.internal = internal;
    }

    public String getRecipient()
    {
        return recipient;
    }

    public void setRecipient(String recipient)
    {
        this.recipient = recipient;
    }

    public void setLastDays(String lastDays) {
        this.lastDays = lastDays;
    }

    public void setReportTemplateName(String reportTemplateName) {
        this.reportTemplateName = reportTemplateName;
    }
}
