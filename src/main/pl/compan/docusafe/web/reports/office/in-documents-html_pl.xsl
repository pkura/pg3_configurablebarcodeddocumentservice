<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>Lp.</th>
            <th>Data przyj�cia</th>
            <th>Liczba pism</th>
        </tr>
        <xsl:apply-templates select="data/results/count"/>
        <xsl:apply-templates select="data/results/totalcount"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <tr>
        <td>Wydzia�: <b><xsl:value-of select="eq[@property='journal']/@name"/></b></td>
        <td>Przyj��: <b><xsl:value-of select="eq[@property='author']/@name"/></b></td>
    </tr>
    <tr>
        <td>Rodzaj pisma: <b><xsl:value-of select="eq[@property='kind']/@name"/></b></td>
        <td>Spos�b dostarczenia: <b><xsl:value-of select="eq[@property='delivery']/@name"/></b></td>
        <td>Spos�b odbioru: <b><xsl:value-of select="eq[@property='outgoingDelivery']/@name"/></b></td>
    </tr>
    <tr>
        <td colspan="2">Data przyj�cia:
            <xsl:if test="range[@property='incomingDate']/@start.date">
                <b>od <xsl:value-of select="range[@property='incomingDate']/@start.date"/> </b>
            </xsl:if>
            <xsl:if test="range[@property='incomingDate']/@end.date">
                <b> do <xsl:value-of select="range[@property='incomingDate']/@end.date"/></b>
            </xsl:if>
        </td>
        <td>Nadawca: <b><xsl:value-of select="eq[@property='sender']/@value"/></b></td>
    </tr>
</xsl:template>

<xsl:template match="count">
    <tr>
        <td><xsl:value-of select="@sequenceId"/></td>
        <td><xsl:value-of select="@date.date"/></td>
        <td align="right">
            <xsl:choose>
                <xsl:when test="number(@count) = 0">
                    Brak pism
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@count"/>
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

<xsl:template match="totalcount">
    <tr><td colspan="3"> </td></tr>
    <tr>
        <td colspan="2">W sumie</td>
        <td align="right"><xsl:value-of select="@count"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>
