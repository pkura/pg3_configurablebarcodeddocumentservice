package pl.compan.docusafe.web.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ChartAction extends EventActionSupport
{
	private Long reportId;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	ReportEnvironment re = new ReportEnvironment();
            	String mime = "image/jpeg";
            	File f = new File(re.getResultsDirForReportId(reportId),"raport.jpg");
            	InputStream resp = new FileInputStream(f);
                ServletUtils.streamResponse(ServletActionContext.getResponse(), resp, mime, resp.available(), new String[0]);
            }
            catch (Exception e)
            {
                LOG.warn(e.getMessage(), e);
            }
            event.setResult(NONE);
        }
    }

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

}
