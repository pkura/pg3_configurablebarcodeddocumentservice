<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:param name="repository-document-link"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <th>Lp.</th>
            <th>Numer</th>
            <th>Typ dokumentu</th>
            <th>Data</th>
            <th>KO</th>
            <th>Data dodania do pud�a</th>
        </tr>
<!--
        <xsl:for-each select="data/results/document">
            <xsl:sort select="@days"/>
            <xsl:apply-templates/>
        </xsl:for-each>
-->
        <xsl:apply-templates select="data/results/document"/>
    </table>
</xsl:template>

<xsl:template match="document">
    <tr>
        <td>
            <xsl:element name="a">
                <xsl:attribute name="href"><xsl:value-of select="$repository-document-link"/><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:value-of select="@lp"/>.
            </xsl:element>
        </td>
        <td>
            <xsl:element name="a">
                <xsl:attribute name="href"><xsl:value-of select="$repository-document-link"/><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:value-of select="@NR_POLISY"/>
                
            </xsl:element>
        </td>
        <td><xsl:value-of select="@RODZAJ"/></td>
        <td align="center"><xsl:value-of select="@DATA_WPLYWU"/></td>
        <td align="center"><xsl:value-of select="@officeNumber"/></td>
        <td align="center"><xsl:value-of select="@boxingDate"/></td>
    </tr>
</xsl:template>

<xsl:template match="criteria">
    <xsl:if test="eq[@property='boxId']">
        <tr>
            <td colspan="2">Numer pud�a:
                <xsl:value-of select="eq[@property='boxId']/@name"/>
            </td>
            <td></td>
        </tr>
        <xsl:if test="eq[@property='boxId']/@open.date">
            <tr>
                <td colspan="2">Otwarte:
                    <xsl:value-of select="eq[@property='boxId']/@open.date"/>
                </td>
                <td></td>
            </tr>
        </xsl:if>
        <tr>
            <td colspan="2">Zamkni�te:
                <xsl:choose>
                    <xsl:when test="eq[@property='boxId']/@close.date">
                        <xsl:value-of select="eq[@property='boxId']/@close.date"/>
                    </xsl:when>
                    <xsl:otherwise>
                        (wci�� otwarte)
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td></td>
        </tr>
    </xsl:if>
</xsl:template>


</xsl:stylesheet>
