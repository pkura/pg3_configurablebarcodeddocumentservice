<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:template match="/report">
    <xsl:apply-templates select="data"/>
</xsl:template>

<xsl:template match="data">
    <p>Wyst�pi�y b��dy podczas tworzenia raportu.</p>
    <p>Komunikat b��du: <b><xsl:value-of select="message"/></b></p>
</xsl:template>

</xsl:stylesheet>
