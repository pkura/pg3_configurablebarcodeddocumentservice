package pl.compan.docusafe.web.reports;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class SpecifyReportParamsAction extends AbstractReportAction {

    private static final long serialVersionUID = 1L;
    private Logger log = LoggerFactory.getLogger(SpecifyReportParamsAction.class);
    private StringManager sm = StringManager.getManager(SpecifyReportParamsAction.class.getPackage().getName());
    private List<ReportParameter> params;
    private Map<String, Object> values = new HashMap<String, Object>();
    private Map<String, Object> invalues = new HashMap<String, Object>();
    private String reportCn;
    private Long reportId;
    private List<DSUser> users;
    private DSDivision[] divisions;
    private DSDivision[] groups;
    private DocumentKind kind;
    private String title;
    private String description;
    private Boolean periodical;
    private Boolean periodicalCapable;
    private Boolean shareable;
    private String guid;
    private String interval;
    private FormFile file;
    private Map<String, File> files = new HashMap<String, File>();

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRegisterReport").
                append(OpenHibernateSession.INSTANCE).
                append(new RegisterReport()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            StringBuilder fromSb = new StringBuilder();
            fromSb.append("asd").append("'").append("asdsa '");

            Report r;
            try {
                //r = (Report)Class.forName(reportClass).newInstance();
                r = reportEnvironment.getReportInstanceByCn(reportCn);
                r.init(reportEnvironment.getDefaultConfigForCn(reportCn, r), reportCn);

                groups = DSDivision.getAllGroups();
                shareable = r.getShareable();
                for (ReportParameter rp : r.getParams()) {
                    if ("division".equals(rp.getType()) && rp.getAdditionalInfo() != null) {
                        divisions = DSDivision.find(rp.getAdditionalInfo()).getAncestorssWithoutPositionAndGroup();
                    } else if ("user".equals(rp.getType()) && rp.getAdditionalInfo() != null) {
                    	users = Arrays.asList(DSDivision.find(rp.getAdditionalInfo()).getUsers(true));
                    	//users = Arrays.asList(DSDivision.find(rp.getAdditionalInfo()).getUsersWithDeleted(true));
                        Collections.sort(users, DSUser.LASTNAME_COMPARATOR);
                    }
                }
                if (users == null) {
                    users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                }
                if (divisions == null) {
                    divisions = DSDivision.getOnlyDivisionsAndPositions();
                }

                if (r.getDockindCn() != null) {
                    kind = DocumentKind.findByCn(r.getDockindCn());
                }

                title = r.getTitle();
                description = r.getDescription();
                periodicalCapable = r.getPeriodicalCapable();
            } catch (Exception e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
                return;
            }
            params = r.getParams();
        }
    }

    private class RegisterReport implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            log.info("values : {}", values);
            Report r;
            PreparedStatement ps = null;
            try {
                Integer intInterval;
                try {
                    intInterval = Integer.parseInt(interval) * 24;
                } catch (Exception e) {
                    intInterval = 0;
                }
                r = reportEnvironment.getReportInstanceByCn(reportCn);
                r.init(reportEnvironment.getDefaultConfigForCn(reportCn, r), reportCn);

                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                divisions = DSDivision.getOnlyDivisionsAndPositions();
                if (r.getDockindCn() != null) {
                    kind = DocumentKind.findByCn(r.getDockindCn());
                }
                params = r.getParams();

                // System.out.println(periodical);
                if (periodical == null) {
                    periodical = false;
                }
                r.setPeriodical(periodical);
                r.setInterval(intInterval);
                r.setGuid(guid);
                for (ReportParameter rp : params) {
                    Object o = null;
                    //sprawdza czy jest wielowartosciowy
                    if ("file".equals(rp.getType())) {
                        //String filename = "kryteria" + rp.getFieldCn() +".dsr";
                        //FormFile ff = (FormFile)values.get(rp.getFieldCn());
                        //files.put(filename, ff.getFile());
                        if (file == null) {
                            throw new IllegalArgumentException("brak wybranego pliku");
                        }
                        o = file.getName();
                    } else if (invalues.get(rp.getFieldCn()) != null) {
                        String value = (String) invalues.get(rp.getFieldCn());
                        String[] tmp = value.split("\n");
                        for (int i = 0; i < tmp.length; i++) {
                            tmp[i] = tmp[i].trim();
                        }
                        o = tmp;
                    } else {
                        o = values.get(rp.getFieldCn());
                    }

                    if (o != null) {
                        if ("date".equals(rp.getType())) {
                            rp.setValue((String) o);
                        } else if ("division".equals(rp.getType()) || "user".equals(rp.getType())) {
                            rp.setValue(TextUtils.parseStringArray(o));
                        } else if ("dockind".equals(rp.getType())) {
                            rp.setValue(TextUtils.parseStringArray(o));
                        } else {
                            rp.setValue(TextUtils.parseStringArray(o));
                        }
                    }



                }
                r.setParams(params);

                DSApi.context().begin();
                Date d = new Date();
                if (DSApi.isOracleServer()) {
                    ps = DSApi.context().prepareStatement("insert into ds_new_report (id,report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(ds_new_report_id.nextval,?,?,?,?,?,?,?,?,?,?)");
                } else if(DSApi.isPostgresServer()) {
                    ps = DSApi.context().prepareStatement("insert into ds_new_report (id,report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(nextval('ds_new_report_id'),?,?,?,?,?,?,?,?,?,?)");
                } else {
                    ps = DSApi.context().prepareStatement("insert into ds_new_report (report_cn, ctime, username, status, criteria, title, description, process_date, remote_key, guid) values(?,?,?,?,?,?,?,?,?,?)");
                }
                ps.setString(1, r.getReportCn());
                ps.setTimestamp(2, new Timestamp(d.getTime()));
                ps.setString(3, DSApi.context().getPrincipalName());
                ps.setInt(4, Report.NEW_REPORT_STATUS);

               if(r.getReportCn().equals("JasperReport")){
                List<ReportParameter> params = r.getParams(); 
                for (ReportParameter reportParameter : params) {
                	if( reportParameter.getFieldCn().equals("raportType")){
                		String jasRepName = reportParameter.getValueAsString();
                		String name = jasRepName.substring(9,jasRepName.length());
                		
                		title = name.replaceAll("_", " ");
                	}
				}
               }
                File tmpfile = File.createTempFile("docusafe_report_", ".tmp");
                OutputStream output = new BufferedOutputStream(new FileOutputStream(tmpfile));
                PrintStream pr = new PrintStream(output, true, "UTF-8");
                pr.print(r.getSearchCriteria().asXML());
                pr.close();
                output.close();
                InputStream is = new FileInputStream(tmpfile);
                ps.setBinaryStream(5, is, (int) tmpfile.length());

                //ps.setString(5, r.getSearchCriteria().asXML());
                if (title == null || title.length() == 0) {
                    title = r.getTitle();
                }
                if (description == null || description.length() == 0) {
                    description = r.getDescription();
                }
                ps.setString(6, title);
                ps.setString(7, description);
                ps.setTimestamp(8, new Timestamp(new Date().getTime()));

                String remKey = DSApi.context().getPrincipalName() + System.currentTimeMillis();
                Random rand = new Random(System.currentTimeMillis());
                while (remKey.length() < 30) {
                    remKey += rand.nextInt(9) % 10;
                }
                ps.setString(9, remKey);
                ps.setString(10, r.getGuid());
                if (ps.executeUpdate() > 0) {
                    addActionMessage("ZarejestrowanoRaport");
                }

                DSApi.context().closeStatement(ps);
                ps = DSApi.context().prepareStatement("select id from ds_new_report where ctime=?");
                ps.setTimestamp(1, new Timestamp(d.getTime()));
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    reportId = rs.getLong("id");
                }
                r.setId(reportId);

                r.setDestination(reportEnvironment.getResultsDirForReport(r));

                DSApi.context().commit();
                /*for(Entry<String, File> f:files.entrySet())
                {
                FileUtils.copyFileToFile(f.getValue(), new File(r.getDestination(), f.getKey()));
                }*/

                if (file != null) {
                    FileUtils.copyFileToFile(file.getFile(), new File(r.getDestination(), "kryteria.dsr"));
                }

                event.setResult("done");
            } catch (Exception e) {
                DSApi.context().closeStatement(ps);
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List<ReportParameter> getParams() {
        return params;
    }

    public void setParams(List<ReportParameter> params) {
        this.params = params;
    }

    public Map<String, Object> getValues() {
        return values;
    }

    public void setValues(Map<String, Object> values) {
        this.values = values;
    }

    public String getReportCn() {
        return reportCn;
    }

    public void setReportCn(String reportCn) {
        this.reportCn = reportCn;
    }

    public List<DSUser> getUsers() {
        return users;
    }

    public void setUsers(List<DSUser> users) {
        this.users = users;
    }

    public DSDivision[] getDivisions() {
        return divisions;
    }

    public void setDivisions(DSDivision[] divisions) {
        this.divisions = divisions;
    }

    public DocumentKind getKind() {
        return kind;
    }

    public void setKind(DocumentKind kind) {
        this.kind = kind;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPeriodical() {
        return periodical;
    }

    public void setPeriodical(Boolean periodical) {
        this.periodical = periodical;
    }

    public Boolean getPeriodicalCapable() {
        return periodicalCapable;
    }

    public void setPeriodicalCapable(Boolean periodicalCapable) {
        this.periodicalCapable = periodicalCapable;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public Map<String, Object> getInvalues() {
        return invalues;
    }

    public void setInvalues(Map<String, Object> invalues) {
        this.invalues = invalues;
    }

    public Boolean getShareable() {
        return shareable;
    }

    public void setShareable(Boolean shareable) {
        this.shareable = shareable;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public DSDivision[] getGroups() {
        return groups;
    }

    public void setGroups(DSDivision[] groups) {
        this.groups = groups;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }
}
