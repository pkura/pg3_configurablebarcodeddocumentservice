<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/report">
    <fo:root font-family="Arial">

      <fo:layout-master-set>
      <!-- fo:layout-master-set defines in its children the page layout:
           the pagination and layout specifications
          - page-masters: have the role of describing the intended subdivisions
                           of a page and the geometry of these subdivisions
                          In this case there is only a simple-page-master which defines the
                          layout for all pages of the text
      -->
        <!-- layout information -->
        <xsl:choose>
            <xsl:when test="data/info[@orientation='horizontal']">
                <fo:simple-page-master master-name="simple"
                      page-height="21cm"
                      page-width="29.7cm"
                      margin-top="1cm"
                      margin-bottom="1cm"
                      margin-left="1cm"
                      margin-right="1cm">
                    <fo:region-body margin-top="1cm" margin-bottom="1cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>   
            </xsl:when>
            <xsl:otherwise>
                <fo:simple-page-master master-name="simple"
                      page-height="29.7cm"
                      page-width="21cm"
                      margin-top="1cm"
                      margin-bottom="1cm"
                      margin-left="1cm"
                      margin-right="1cm">
                    <fo:region-body margin-top="1cm" margin-bottom="1cm"/>
                    <fo:region-before extent="1cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>            
            </xsl:otherwise>
        </xsl:choose>
        
      </fo:layout-master-set>
      <!-- end: defines page layout -->


      <!-- start page-sequence
           here comes the text (contained in flow objects)
           the page-sequence can contain different fo:flows
           the attribute value of master-name refers to the page layout
           which is to be used to layout the text contained in this
           page-sequence-->
      <fo:page-sequence master-reference="simple">

          <fo:static-content flow-name="xsl-region-before">
            <fo:block font-family="Arial" font-size="10pt" text-align="right">
                <xsl:value-of select="data/info/@name"/>             
            </fo:block>
          </fo:static-content>

          <fo:static-content flow-name="xsl-region-after">
              <fo:block font-family="Arial" font-size="8pt" text-align="right">
                  Created: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
                  Date: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>
              </fo:block>
          </fo:static-content>

          <!-- start fo:flow
             each flow is targeted
             at one (and only one) of the following:
             xsl-region-body (usually: normal text)
             xsl-region-before (usually: header)
             xsl-region-after  (usually: footer)
             xsl-region-start  (usually: left margin)
             xsl-region-end    (usually: right margin)
             ['usually' applies here to languages with left-right and top-down
              writing direction like English]
             in this case there is only one target: xsl-region-body
          -->
        <fo:flow flow-name="xsl-region-body">

          <!-- each paragraph is encapsulated in a block element
               the attributes of the block define
               font-family and size, line-heigth etc. -->

          <xsl:apply-templates select="data/criteria"/>

          <fo:table table-layout="fixed">
            <xsl:for-each select="data/header/column">
                <fo:table-column/>
            </xsl:for-each>
            <fo:table-header>
                <fo:table-row>
                    <xsl:apply-templates select="data/header/column"/>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
                <xsl:apply-templates select="data/results/row"/>
            </fo:table-body>
          </fo:table>

            <!--
          <fo:block text-align="right" font-family="Arial">
            Utworzy�: <fo:inline font-weight="bold"><xsl:value-of select="author"/></fo:inline>,
            Data: <fo:inline font-weight="bold"><xsl:value-of select="date"/></fo:inline>,
          </fo:block>
          -->

        </fo:flow> <!-- closes the flow element-->
      </fo:page-sequence> <!-- closes the page-sequence -->
    </fo:root>
</xsl:template>

<xsl:template match="criteria">
  <fo:block font-family="Arial">Search criteria:</fo:block>
  <fo:table table-layout="fixed" width="100%" space-after.optimum="13pt">
    <fo:table-column />
    <fo:table-body>
        <xsl:apply-templates select="range"/>
        <xsl:apply-templates select="enumeration"/>
    </fo:table-body>
  </fo:table>
</xsl:template>

<xsl:template match="range">
    <fo:table-row>
        <fo:table-cell><fo:block font-family="Arial">
            <xsl:value-of select="@property"/>:
            <xsl:if test="@start">
                <fo:inline font-weight="bold">od <xsl:value-of select="@start"/> </fo:inline>
            </xsl:if>
            <xsl:if test="@end">
                <fo:inline font-weight="bold"> do <xsl:value-of select="@end"/></fo:inline>
            </xsl:if>
        </fo:block></fo:table-cell>
    </fo:table-row>
</xsl:template>

<xsl:template match="enumeration">
    <fo:table-row>
        <fo:table-cell><fo:block font-family="Arial">
            <xsl:value-of select="@property"/>: 
            <fo:inline font-weight="bold">
            <xsl:for-each select="value">
                <xsl:value-of select="@title"/><xsl:if test="following-sibling::value">, </xsl:if>
            </xsl:for-each>
            </fo:inline>
        </fo:block></fo:table-cell>
    </fo:table-row>
</xsl:template>

<xsl:template match="column">
    <fo:table-cell>
        <xsl:element name="fo:block">
            <xsl:attribute name="font-family">Arial</xsl:attribute>
            <xsl:attribute name="font-weight">bold</xsl:attribute>
            <!-- <xsl:if test="@align">--><xsl:attribute name="text-align"><!-- <xsl:value-of select="@align"/>-->center</xsl:attribute><!-- </xsl:if>-->
            <xsl:value-of select="@name"/>
        </xsl:element>
    </fo:table-cell>
</xsl:template>

<xsl:template match="row">
    <fo:table-row>
        <xsl:choose>
            <xsl:when test="@single-value">
                <fo:table-cell>
                    <fo:block font-weight="bold"><xsl:value-of select="@single-value"/></fo:block>
                </fo:table-cell>
                <xsl:element name="fo:table-cell"> 
                    <xsl:attribute name="column-number"><xsl:value-of select="@colspan"/></xsl:attribute>
                    <fo:block font-weight="bold"> </fo:block>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="cell"/>
            </xsl:otherwise>
        </xsl:choose>
    </fo:table-row>
</xsl:template>

<xsl:template match="cell">
    <fo:table-cell><xsl:element name="fo:block">
        <!-- <xsl:if test="@align">--><xsl:attribute name="text-align"><!-- <xsl:value-of select="@align"/>-->center</xsl:attribute><!-- </xsl:if>-->
        <xsl:if test="@bold"><xsl:attribute name="font-weight">bold</xsl:attribute></xsl:if>
        <xsl:value-of select="@value"/>
    </xsl:element></fo:table-cell>
</xsl:template>
</xsl:stylesheet>
