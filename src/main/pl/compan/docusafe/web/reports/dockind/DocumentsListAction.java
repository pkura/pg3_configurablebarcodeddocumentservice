package pl.compan.docusafe.web.reports.dockind;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.reports.ReportGenerator;
import pl.compan.docusafe.service.reports.dockind.AbstractDockindReport;
import pl.compan.docusafe.service.reports.dockind.AbstractDockindReport.SearchDocumentsHandler;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DocumentsListAction extends EventActionSupport
{
    private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    // @IMPORT
    private Map<String,Object> criteria = new LinkedHashMap<String, Object>();
    private String documentKindCn;
    private String reportCn;
    
    // @EXPORT
    private List<Map<String,Object>> documents;
    private Map<String,String> columns;
    private Report report;
    private Map<String,String> criteriaInfo;
    private String currentDate;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            { 
                currentDate = DateUtils.formatJsDateTime(new Date());
                DocumentKind documentKind = DocumentKind.findByCn(documentKindCn);
                documentKind.initialize();
                report = documentKind.getReport(reportCn); 
                
                String[] detailCols = report.getPropertyArray(Report.DETAILS_VIEW);
                if (detailCols.length == 0)
                    throw new EdmException("Dla tego raportu niezdefiniowano szczeg�owego wisoku");
                
                // wyszukiwanie dokument�w wed�ug podanych kryteri�w
                criteriaInfo = new LinkedHashMap<String, String>();
                
                Class c = ReportGenerator.getReportGenerator(report.getReportId());
               // Map<Long,Map<String,String>> docs = null;
                SearchDocumentsHandler handler = null;
                try
                {
                    /*Method method = c.getDeclaredMethod("searchDocuments", new Class[]{String.class, Map.class, String.class, Map.class});
                    Object result = method.invoke(null, new Object[]{reportCn, criteria, documentKindCn, criteriaInfo});
                    if (!(result instanceof Map))
                        throw new IllegalStateException("Wynik metody 'searchDocuments' w klasie: "+c.getName()+" nie jest typu Map");
                    docs = (Map<Long,Map<String,String>>) result;*/
                    Method method = c.getDeclaredMethod("getSDHandler", new Class[]{});
                    Object result = method.invoke(null, new Object[]{});
                    if (!(result instanceof SearchDocumentsHandler))
                        throw new IllegalStateException("Wynik metody 'getSDHandler' w klasie: "+c.getName()+" nie jest typu SearchDocumentsHandler");
                    handler = (SearchDocumentsHandler) result;
                }
                catch (NoSuchMethodException e)
                {
                    // gdy nie istnieje odpowiednia metoda to zak�adam, �e warto�� danej w�a�ciwo�ci r�wna null
                    throw new IllegalStateException("Nie istnieje metoda 'searchDocuments' w klasie: "+c.getName());
                }
                catch (Exception e)
                {
                    throw new EdmException("B��d podczas wyszukiwania dokument�w", e);
                }
                  
                List<Map<String,String>> docs = AbstractDockindReport.searchDocuments(reportCn, handler, criteria, documentKind, criteriaInfo);
                
                // utworzenie listy kolumn, kt�re b�d� pokazywane
                columns = new LinkedHashMap<String, String>();
                for (String col : detailCols)
                {
                    int dotPos = col.indexOf(".");
                    String type = col.substring(0, dotPos);
                    String fieldCn = col.substring(dotPos+1);
                    Criterion criterion = report.getCriterion(type, fieldCn);
                    
                    if ("standard".equals(type))
                    {
                        if ("division".equals(fieldCn))
                            columns.put(col, criterion.getName());
                        else if ("user".equals(fieldCn))
                            columns.put(col, criterion.getName());
                        else if ("date".equals(fieldCn))    
                            columns.put(col, criterion.getName());
                        else if ("description".equals(fieldCn))
                            columns.put(col, sm.getString("Opis"));
                        else if ("attachment".equals(fieldCn))
                            columns.put(col, sm.getString("Zalacznik"));
                        else if ("result".equals(fieldCn))
                            columns.put(col, handler.getResultColumnNameOnDetails(report));
                    }
                    else
                    {
                        columns.put(col, documentKind.getFieldByCn(fieldCn).getName());
                    }
                }
                
                // przygotowanie danych o dokumentach, kt�re b�d� pokazane
                if (docs != null && docs.size() > 0)
                {
                    documents = new ArrayList<Map<String,Object>>();
                    for (Map<String,String> docBean : docs)
                    {                        
                        Long documentId = Long.valueOf(docBean.get("id"));
                        Map<String,Object> bean = new LinkedHashMap<String, Object>();
                        bean.put("id", documentId);
                        Document document = Document.find(documentId);
                        FieldsManager fm = documentKind.getFieldsManager(documentId);
                        for (String col : detailCols)
                        {
                            int dotPos = col.indexOf(".");
                            String type = col.substring(0, dotPos);
                            String fieldCn = col.substring(dotPos+1);
                            if ("standard".equals(type))
                            {
                                if ("division".equals(fieldCn))
                                    bean.put(col, docBean.get("division") != null ? DSDivision.safeGetName(docBean.get("division")) : "BRAK");
                                else if ("user".equals(fieldCn))
                                    bean.put(col, docBean.get("user") != null ? DSUser.findByUsername(docBean.get("user")).asFirstnameLastname() : "BRAK");
                                else if ("date".equals(fieldCn))
                                    bean.put(col, docBean.get("date") != null ? docBean.get("date") : "BRAK");
                                else if ("description".equals(fieldCn))
                                    bean.put(col, document.getDescription());
                                else if ("attachment".equals(fieldCn))
                                {
                                    if (document.getAttachments() != null && document.getAttachments().size() > 0)
                                    {
                                        AttachmentRevision revision = document.getAttachments().get(0).getMostRecentRevision();
                                        Map<String,String> attBean = new LinkedHashMap<String, String>();
                                        attBean.put("id", revision.getId().toString());
                                        attBean.put("mime", revision.getMime());
                                        bean.put(col, attBean);
                                    }
                                }
                                else if ("result".equals(fieldCn))
                                    bean.put(col, docBean.get("result"));
                            }
                            else
                            {
                                bean.put(col, fm.getDescription(fieldCn) != null ? fm.getDescription(fieldCn).toString() : "BRAK");
                            }                            
                        }    
                        documents.add(bean);
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public Map<String, Object> getCriteria()
    {
        return criteria;
    }

    public void setCriteria(Map<String, Object> criteria)
    {
        this.criteria = criteria;
    }

    public Map<String, String> getColumns()
    {
        return columns;
    }

    public List<Map<String, Object>> getDocuments()
    {
        return documents;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public void setReportCn(String reportCn)
    {
        this.reportCn = reportCn;
    }

    public Report getReport()
    {
        return report;
    }

    public Map<String, String> getCriteriaInfo()
    {
        return criteriaInfo;
    }

    public String getCurrentDate()
    {
        return currentDate;
    }

    public void setCurrentDate(String currentDate)
    {
        this.currentDate = currentDate;
    }        
}
