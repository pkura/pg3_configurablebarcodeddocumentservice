package pl.compan.docusafe.web.reports.dockind;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.core.dockinds.Report.Criterion;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.reports.ReportHandle;
import pl.compan.docusafe.service.reports.dockind.DockindReportCriteria;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DockindReportAction extends EventActionSupport
{
    // @EXPORT/@IMPORT
    private String documentKindCn;
    private String reportCn;
    private String dateFrom;
    private String dateTo;
    private Map<String,Object> values = new HashMap<String,Object>();
    private String[] divisionGuid;
    private String[] username;
    private int days;
    private int interval;
    private String invoiceDateFrom;
    private String invoiceDateTo;
    
    // @EXPORT
    private DocumentKind kind;
    private Long id;
    private List<DSUser> users;
    private DSDivision[] divisions; 
    
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReport").
            append(OpenHibernateSession.INSTANCE).
            append(new Validate()).
            append(new InitReport()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {                
                kind = DocumentKind.findByCn(documentKindCn);
                kind.initialize();
                Report report = kind.getReport(reportCn);

                if (report == null)
                    throw new EdmException("Nie zdefiniowano dla tego rodzaju dokumentu raportu o podanej nazwie kodowej");
                
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                divisions = DSDivision.getOnlyDivisionsAndPositions(false);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Validate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            dateFrom = TextUtils.trimmedStringOrNull(dateFrom);
            dateTo = TextUtils.trimmedStringOrNull(dateTo);
            invoiceDateFrom = TextUtils.trimmedStringOrNull(invoiceDateFrom);
            invoiceDateTo = TextUtils.trimmedStringOrNull(invoiceDateTo);
        }
    }
    
    private class InitReport implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	
            	// ustawiamy kryteria raportu                
                kind = DocumentKind.findByCn(documentKindCn);
                kind.initialize();
                Report report = kind.getReport(reportCn);
                if(days!=0 && dateTo == null)
                {
                	dateTo = DateUtils.formatJsDate(DateUtils.plusDays(DateUtils.getCurrentTime(), -days));
                }
                DockindReportCriteria criteria = new DockindReportCriteria();
                criteria.setDocumentKind(kind);
                criteria.setReport(report);
                criteria.setContextPath(ServletActionContext.getRequest().getContextPath());
                for (Criterion criterion : report.getCriteria().values())
                {
                    if ("standard".equals(criterion.getType()))
                    {
                    	if ("invoiceDate".equals(criterion.getFieldCn()) &&(invoiceDateFrom!=null || invoiceDateTo!=null))
                    		criteria.addRange("invoiceDate", invoiceDateFrom, invoiceDateTo);
                        if ("date".equals(criterion.getFieldCn()) && (dateFrom != null || dateTo != null))
                            criteria.addRange("date", dateFrom, dateTo);
                        if ("division".equals(criterion.getFieldCn()) && divisionGuid != null && divisionGuid.length > 0)
                            criteria.addEnumeration("division", divisionGuid);
                        if ("user".equals(criterion.getFieldCn()) && username != null && username.length > 0)
                            criteria.addEnumeration("user", username);
                        	
                    }
                    else if ("dockind".equals(criterion.getType()))
                    {
                    	
                    	
	                        Field field = kind.getFieldByCn(criterion.getFieldCn());
	                        if (Field.ENUM.equals(field.getType()) || Field.ENUM_REF.equals(field.getType()))
	                        {
	                            Integer[] array = TextUtils.parseIntoIntegerArray(values.get(field.getCn()));
	                            if (array != null && array.length > 0)
	                                criteria.addEnumeration(field.getCn(), array);
	                        }
                    	
                        
                    }
                }                                
                
                // inicjujemy generowanie raportu
                pl.compan.docusafe.service.reports.Report service = (pl.compan.docusafe.service.reports.Report) ServiceManager.getService(pl.compan.docusafe.service.reports.Report.NAME);
                ReportHandle handle = service.generateReport(report.getReportId(), criteria,
                    DSApi.context().getPrincipalName());

                try
                {
                    synchronized (handle)
                    {
                        if (!handle.isReady())
                            handle.wait(5 * 1000);
                    }
                }
                catch (InterruptedException e)
                {
                }

                if (!handle.isReady())
                {
                    event.setResult("not-ready");
                }
                else
                {
                    if (handle.getException() != null)
                        addActionError("Wyst�pi� b��d ("+handle.getException().getMessage()+")");
                    id = handle.getId();
                    event.setResult("view-report");
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getDocumentKindCn()
    {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn)
    {
        this.documentKindCn = documentKindCn;
    }

    public String getReportCn()
    {
        return reportCn;
    }

    public void setReportCn(String reportCn)
    {
        this.reportCn = reportCn;
    }

    public String getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public String getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(String dateTo)
    {
        this.dateTo = dateTo;
    }

    public String[] getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String[] divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String[] getUsername()
    {
        return username;
    }

    public void setUsername(String[] username)
    {
        this.username = username;
    }

    public Map<String, Object> getValues()
    {
        return values;
    }

    public void setValues(Map<String, Object> values)
    {
        this.values = values;
    }

    public DocumentKind getKind()
    {
        return kind;
    }        
    
    public Long getId()
    {
        return id;
    }

    public DSDivision[] getDivisions()
    {
        return divisions;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getInvoiceDateFrom() {
		return invoiceDateFrom;
	}

	public void setInvoiceDateFrom(String invoiceDateFrom) {
		this.invoiceDateFrom = invoiceDateFrom;
	}

	public String getInvoiceDateTo() {
		return invoiceDateTo;
	}

	public void setInvoiceDateTo(String invoiceDateTo) {
		this.invoiceDateTo = invoiceDateTo;
	}
}
