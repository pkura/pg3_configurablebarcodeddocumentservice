<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:param name="repository-document-link"/>

<xsl:template match="/report">
<xsl:for-each select="data/header/column"><xsl:value-of select="@name"/>;</xsl:for-each>
<xsl:text>
</xsl:text>
<xsl:apply-templates select="data/results/row"/>
</xsl:template>

<xsl:template match="row">
<xsl:choose>
<xsl:when test="@single-value">
<xsl:value-of select="@single-value"/>;
</xsl:when>
<xsl:otherwise>
<xsl:for-each select="cell"><xsl:value-of select="@value"/>;</xsl:for-each>
<xsl:text>
</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>
