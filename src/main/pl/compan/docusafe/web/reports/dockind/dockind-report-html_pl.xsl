<?xml version="1.0" encoding="iso-8859-2"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:param name="repository-document-link"/>

<xsl:template match="/report">
    <p>Kryteria wyboru:</p>
    <table>
        <xsl:apply-templates select="data/criteria"/>
    </table>
    <p></p>
    <table>
        <tr>
            <xsl:apply-templates select="data/header/column"/>
        </tr>
        <xsl:apply-templates select="data/results/row"/>
    </table>
</xsl:template>

<xsl:template match="criteria">
    <xsl:apply-templates select="range"/>
    <xsl:apply-templates select="enumeration"/>
</xsl:template>

<xsl:template match="range">
    <tr>
        <td colspan="2"><xsl:value-of select="@property"/>:
            <xsl:if test="@start">
                <b>od <xsl:value-of select="@start"/> </b>
            </xsl:if>
            <xsl:if test="@end">
                <b> do <xsl:value-of select="@end"/></b>
            </xsl:if>
        </td>
        <td></td>
    </tr>
</xsl:template>

<xsl:template match="enumeration">
    <tr>
        <td colspan="3">
            <b><xsl:value-of select="@property"/>: </b>
            <xsl:for-each select="value">
                <xsl:value-of select="@title"/><xsl:if test="following-sibling::value">, </xsl:if>
            </xsl:for-each>
        </td>
    </tr>
</xsl:template>

<xsl:template match="column">    
    <xsl:element name="td">
        <xsl:if test="@align"><xsl:attribute name="align"><xsl:value-of select="@align"/></xsl:attribute></xsl:if>
        <b><xsl:value-of select="@name"/></b>
    </xsl:element>
</xsl:template>

<xsl:template match="row">
    <tr>
        <xsl:choose>
        <xsl:when test="@single-value">
            <xsl:element name="td">                
                <xsl:choose>
                    <xsl:when test="@link">
                        <xsl:element name="a">
                            <xsl:attribute name="href"><xsl:value-of select="@link"/></xsl:attribute>
                            <xsl:value-of select="@single-value"/>
                        </xsl:element>                
                    </xsl:when>
                    <xsl:otherwise>
                        <b><xsl:value-of select="@single-value"/></b>
                    </xsl:otherwise>        
                </xsl:choose>
            </xsl:element>
            <xsl:element name="td">    
                <xsl:attribute name="colspan"><xsl:value-of select="@colspan"/></xsl:attribute>                
            </xsl:element>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates select="cell"/>
        </xsl:otherwise>
        </xsl:choose>
    </tr>
</xsl:template>

<xsl:template match="cell">
    <xsl:element name="td">
        <xsl:if test="@align"><xsl:attribute name="align"><xsl:value-of select="@align"/></xsl:attribute></xsl:if>
        <xsl:if test="@bold"><xsl:attribute name="style">font-weight:bold</xsl:attribute></xsl:if>
        <xsl:choose>
            <xsl:when test="@link">
                <xsl:element name="a">
                    <xsl:attribute name="href"><xsl:value-of select="@link"/></xsl:attribute>
                    <xsl:value-of select="@value"/>
                </xsl:element>                
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@value"/>
            </xsl:otherwise>        
        </xsl:choose>
    </xsl:element>    
</xsl:template>

</xsl:stylesheet>
