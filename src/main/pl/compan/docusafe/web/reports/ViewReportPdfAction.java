package pl.compan.docusafe.web.reports;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.fop.Fop;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.*;
import java.net.URL;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewReportPdfAction.java,v 1.7 2009/02/28 08:02:57 wkuty Exp $
 */
public class ViewReportPdfAction extends EventActionSupport
{
    /**
     * Identyfikator obiektu Report.
     */
    private Long id;
    /**
     * Nazwa zasobu arkusza xsl.
     */
    private String xsl;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
        {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null || xsl == null)
            {
                event.getLog().warn("id == null || xsl == null");
                return;
            }

            File xml = null;
            InputStream xmlIs = null;
            File pdf = null;
            OutputStream os = null;
            InputStream sheet = null;

            try
            {
                DSApi.context().begin();

                xml = Report.find(id).getXmlAsFile();
                if (xml == null)
                    throw new EdmException("Raport "+id+" nie ma tre�ci");

                xmlIs = new BufferedInputStream(new FileInputStream(xml));

                if (xsl.startsWith("cl:"))
                {
                    sheet = ViewReportPdfAction.class.getClassLoader().getResourceAsStream(xsl.substring(3));
                }
                else
                {
                    sheet = new URL(xsl).openStream();
                }
//                String s = org.apache.commons.io.IOUtils.toString(sheet, "UTF-8");
////               String s =  new String(new URL(xsl).getFile().getBytes());
//               System.out.println(s);
                if (sheet == null)
                    throw new EdmException("Nie mo�na znale�� zasobu "+xsl);


                pdf = File.createTempFile("docusafe_report_", ".pdf");
                os = new FileOutputStream(pdf);
                Fop.renderPdf(xmlIs, sheet, os);

                DSApi.context().commit();
            }
            catch (Exception e){
                DSApi.context().setRollbackOnly();
                event.getLog().error(e.getMessage(), e);
                return;
            }
            finally
            {
            	org.apache.commons.io.IOUtils.closeQuietly(xmlIs);
            	org.apache.commons.io.IOUtils.closeQuietly(sheet);
            	org.apache.commons.io.IOUtils.closeQuietly(os);                
                xml.delete();
            }

            if (pdf != null && pdf.exists())
            {
                if (event.getLog().isDebugEnabled())
                    event.getLog().debug("rozmiar pliku "+pdf+": "+pdf.length());
                try
                {
               	 	ServletUtils.streamFile(ServletActionContext.getResponse(), pdf, "application/pdf","Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"Raport.pdf\"");
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                finally
                {
                    pdf.delete();
                }
            }
        }
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setXsl(String xsl)
    {
        this.xsl = xsl;
    }
}
