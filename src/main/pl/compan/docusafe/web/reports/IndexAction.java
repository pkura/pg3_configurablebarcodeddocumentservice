package pl.compan.docusafe.web.reports;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class IndexAction extends AbstractReportAction
{
	private static final Logger log = LoggerFactory.getLogger(IndexAction.class);
    private List<Map<String,String>> dockindReports;
    private List<Map<String,String>> newReports;
    private boolean UTPReport;
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
 
    private class FillForm implements ActionListener
    {
    	
        public void actionPerformed(ActionEvent event) 
        {
            try
            {
                newReports = reportEnvironment.listReportsForDisplay();
                dockindReports = new LinkedList<Map<String,String>>();
                for (DocumentKind documentKind : DocumentKind.list(true))
                {
                    documentKind.initialize();
                    if (documentKind.getReports() != null)
                        for (Report report : documentKind.getReports().values())
                        {
                            Map<String,String> bean = new LinkedHashMap<String, String>();
                            bean.put("cn", report.getCn());
                            bean.put("name", report.getName());
                            bean.put("description", report.getDescription());
                            bean.put("dockindCn", documentKind.getCn());
                            dockindReports.add(bean);
                        }
                }
                if(AvailabilityManager.isAvailable("reports.wlacz.UTPReport")){
                	setUTPReport(true);
                } 
            }
            catch (EdmException e)
            {
            	log.error("",e);
                addActionError(e.getMessage());
            }
        }
    }

    public List<Map<String,String>> getDockindReports()
    {
        return dockindReports;
    }

	public List<Map<String, String>> getNewReports() {
		return newReports;
	}
    
	public boolean getUTPReport()
	{
		return UTPReport;
	}
	
	public void setUTPReport( boolean UTPReport)
	{
		this.UTPReport=UTPReport;
	}
    
}
