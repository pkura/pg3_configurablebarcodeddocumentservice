package pl.compan.docusafe.web.common;

import pl.compan.docusafe.util.HtmlImage;

/**
 * Zbi�r warto�ci, z kt�rych korzysta sie przy generowaniu html'a
 * @author MSankowski
 */
public class HtmlTheme {
	
	/***************************************************************************
	 * POLA
	 **************************************************************************/
	
	/**
	 * Ikonka przej�cia do karty u�ytkownika
	 */
	private HtmlImage gotoUserIcon;
	
	/**
	 * Ikonka grupy
	 */
	private HtmlImage groupIcon;
	
	private HtmlImage positionIcon;
	
	/**
	 * Ikonka u�ytkownika
	 */
	private HtmlImage userIcon;
	
	
	
	
	
	
	
	/***************************************************************************
	 * DOMY�LNE WARTO�CI
	 **************************************************************************/
	
	{
		gotoUserIcon = new HtmlImage("/img/goto-user.gif", 19, 14);
		groupIcon = new HtmlImage("/img/folder-group.gif", 17, 17);
		positionIcon = new HtmlImage("/img/folder-position.gif", 17, 17);
		userIcon = new HtmlImage("/img/folder-user.gif", 17, 17);		
	}
	
	/***************************************************************************
	 *	STATYCZNE AKCESORY
	 **************************************************************************/
	
	private static HtmlTheme INSTANCE = new HtmlTheme(); //domy�lne warto�ci
	
	public static void setInstance(HtmlTheme theme){
		INSTANCE = theme;
	}
	
	public static HtmlTheme getInstance(){
		return INSTANCE;
	}
	
	public static HtmlImage userIcon(){
		return INSTANCE.getUserIcon();
	}	
	
	public static HtmlImage groupIcon(){
		return INSTANCE.getGroupIcon();
	}
	
	public static HtmlImage positionIcon(){
		return INSTANCE.getPositionIcon();
	}
	
	public static HtmlImage gotoUserIcon(){
		return INSTANCE.getGotoUserIcon();
	}
	
	/***************************************************************************
	 * AKCESORY
	 **************************************************************************/
	
	/**
	 *  Ikonka u�ytkownika
	 */
	public HtmlImage getUserIcon(){
		return userIcon;
	}	

	public void setGroupIcon(HtmlImage groupIcon) {
		this.groupIcon = groupIcon;
	}

	public HtmlImage getGroupIcon() {
		return groupIcon;
	}

	public void setPositionIcon(HtmlImage positionIcon) {
		this.positionIcon = positionIcon;
	}

	public HtmlImage getPositionIcon() {
		return positionIcon;
	}

	public void setGotoUserIcon(HtmlImage gotoUserIcon) {
		this.gotoUserIcon = gotoUserIcon;
	}

	public HtmlImage getGotoUserIcon() {
		return gotoUserIcon;
	}
}
