package pl.compan.docusafe.web.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
/**
* @author Mariusz Kiljanczyk
*/
public class AddAttachmentFromPagesAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(AddAttachmentFromPagesAction.class);
	
    private List<FormFile> multiFiles = new ArrayList<FormFile>();
    private Map<String,String> files = new TreeMap<String, String>();
    private String[] selectPages;
    private Long attachmentId;
    private Long documentId;
    private boolean asPdf;
    private boolean attachToOtherDocument;
    private boolean attachFiles;
    private Long binderId;
    private boolean close;
    
	protected void setup()
	{
        registerListener(DEFAULT_ACTION);
        registerListener("doNew").
        	append(OpenHibernateSession.INSTANCE).
        	append(new New()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSave").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new Save()).
	    	appendFinally(CloseHibernateSession.INSTANCE);
	}
	
    private class Save implements ActionListener
    {
        private Object[][] String;

		public void actionPerformed(ActionEvent event) 
        {
        	try
        	{
        		Integer kind = ImageKit.IMPORT_KIND_GRAY_TIFF; 

        		if(asPdf)
        		{
        			kind = ImageKit.IMPORT_KIND_PDF;
        		}        
        		
        		Document docLeasing = Document.find(documentId,false);
        		// jesli dolaczamy tylko do dokumentu związanego z umową z teczki i dokument powiazany jest z wieloma umowami
        		if(attachToOtherDocument && docLeasing.getFieldsManager().getKey(DLBinder.NUMER_UMOWY_FIELD_CN) != null && 
        				((java.util.Collection)(docLeasing.getFieldsManager().getKey(DLBinder.NUMER_UMOWY_FIELD_CN))).size() > 1 )
        		{
        			DSApi.context().begin(); 
    				String summary = "Dokument LEASINGOWY";
    				Document document = null;
    				document = new Document(summary, summary);
    				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
    				document.setDocumentKind(documentKind);
    				document.setForceArchivePermissions(true);
    				document.setCtime(new Date());
    				document.setFolder(Folder.getRootFolder());
    				document.create();
    				Long newDocumentId = document.getId();
    				
	        		Map<String, Object> values = docLeasing.getFieldsManager().getFieldValues();
	        		
	        		// numerem umowy dla nowego dokumentu staje si� numer umowy z teczki na ktorej aktualnie pracujemy
    				values.remove(DlLogic.NUMER_UMOWY_CN);
    				docLeasing = Document.find(binderId,false);
	        		values.put(DlLogic.NUMER_UMOWY_CN, docLeasing.getFieldsManager().getFieldValues().get(DLBinder.NUMER_UMOWY_FIELD_CN));
	        		//usuwamy powi�zanie starego dokumentu z umow� kt�r� w�a�nie wi�zemy z nowym dokumentem
	        		DlLogic.removeContractsFromDocument((Long)values.get(DLBinder.NUMER_UMOWY_FIELD_CN), documentId);

	        		values.put(DlLogic.STATUS_DOKUMENTU_CN, 5);
    				documentKind.setOnly(newDocumentId, values);
    				documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
    				documentKind.logic().documentPermissions(document);
    				DSApi.context().commit();
    				log.trace("Dodaje dokument");
    				
	        		if(!document.canModifyAttachments(binderId))
	        			throw new EdmException("Nie masz uprawnie� do dodania za��cznika");
	        		changeStatus(document);
	        		ImageKit.addAttachments(selectPages,document.getId(),kind);
	        		
	        		EventFactory.startEventByHandler("AddAttachmentsToDocument");
	        		addActionMessage("Dodano nowy za��cznik do dokumentu");
        		}
        		else
        		{
            		if(attachmentId != null)
    	        	{
            			docLeasing = null;
    	        		Attachment atta = Attachment.find(attachmentId,false);
    	        		Document doc = atta.getDocument();
    	        		//powrotna zmiana
//    	        		if(!doc.canModifyAttachments(binderId))
//    	        			throw new EdmException("Nie masz uprawnień do dodania załącznika");
    	        		changeStatus(doc);
    	        		ImageKit.addRevision(selectPages, attachmentId,kind,attachFiles);
    	        		
    	        		EventFactory.startEventByHandler("AddRevisionToAttachment");
    	        		addActionMessage("Dodano now� wersje za��cznika oraz zmieniono status dokumentu");
    	        	}
    	        	else
    	        	{
    	        		if(!docLeasing.canModifyAttachments(binderId))
    	        			throw new EdmException("Nie masz uprawnie� do dodania za��cznika");
    	        		changeStatus(docLeasing);
    	        		ImageKit.addAttachments(selectPages,docLeasing.getId(),kind);
    	        		
    	        		EventFactory.startEventByHandler("AddAttachmentsToDocument");
    	        		addActionMessage("Dodano nowy za��cznik do dokumentu");
    	        	}
        		}  		        		
	        	close = true;	  

        	}
        	catch (Exception e) 
        	{
				log.error("",e);
				close = true;
				addActionError("B��d dodawania za��cznika "+e.getMessage());
			}
        }

		private void changeStatus(Document doc) throws EdmException
		{
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(DlLogic.STATUS_DOKUMENTU_CN, 5);
			DSApi.context().begin();
			doc.getDocumentKind().setWithHistory(doc.getId(), values, false,Label.SYSTEM_LABEL_OWNER);
			DSApi.context().commit();			
		}
    }
    
    private class New implements ActionListener 
    {
		public void actionPerformed(ActionEvent event) 
        {        	
        	try
        	{
                files = ImageKit.splitAttachments(multiFiles, Docusafe.getPngTempFile());
			}
            catch (Exception e) 
			{
            	log.error("", e);
            	event.addActionError("B��d �adownia pliku "+ e.getMessage());
			}
        }
    }

	public FormFile getMultiFiles() 
	{
		if(this.multiFiles != null && this.multiFiles.size() > 0 )
			return this.multiFiles.get(0);
		else
			return null;
	}
    
	public void setMultiFiles(FormFile file) {
		this.multiFiles.add(file);
	}

	public Map<String, String> getFiles() {
		return files;
	}

	public void setFiles(Map<String, String> files) {
		this.files = files;
	}

	public Long getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public void setSelectPages(String[] selectPages) {
		this.selectPages = selectPages;
	}

	public String[] getSelectPages() {
		return selectPages;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setAsPdf(boolean asPdf)
	{
		this.asPdf = asPdf;
	}

	public boolean isAsPdf()
	{
		return asPdf;
	}		

	public void setBinderId(Long binderId)
	{
		this.binderId = binderId;
	}

	public Long getBinderId()
	{
		return binderId;
	}

	public boolean isClose() {
		return close;
	}

	public void setClose(boolean close) {
		this.close = close;
	}

	public boolean isAttachFiles() {
		return attachFiles;
	}

	public void setAttachFiles(boolean attachFiles) {
		this.attachFiles = attachFiles;
	}

	public boolean isAttachToOtherDocument() {
		return attachToOtherDocument;
	}

	public void setAttachToOtherDocument(boolean attachToOtherDocument) {
		this.attachToOtherDocument = attachToOtherDocument;
	}
	
}
