package pl.compan.docusafe.web.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.util.ContextLocale;
import pl.compan.docusafe.util.LocaleUtil;

import javax.servlet.*;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Locale;

/**
 * Filtr, kt�ry wi��e z bie��cym w�tkiem obiekt Locale
 * pobrany z wywo�ania (HttpServletRequest).
 * <p>
 * Ten filtr powinien znale�� si� jako pierwszy w �a�cuchu.
 *
 * @see ContextLocale
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ContextLocaleFilter.java,v 1.4 2005/11/24 16:16:41 lk Exp $
 */
public class ContextLocaleFilter implements Filter
{
    public static final Log log = LogFactory.getLog(ContextLocaleFilter.class);

    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        // sprawdza� w NumberFormat.getAvailableLocales()
        // i w DateFormat.getAvailableLocales()
//        Locale locale = servletRequest.getLocale();

        // TODO: sprawdza�, czy ServletRequest.locale jest na li�cie dost�pnych
        // w Configuration
//        if (locale == null || !locale.toString().equals("pl"))
//            locale = LocaleUtil.getLocale();

//        ContextLocale.setContextLocale(locale);

        Locale locale = LocaleUtil.getLocale();
        ContextLocale.setContextLocale(locale);

        if (log.isDebugEnabled())
            log.debug("locale="+locale);

        servletRequest.setAttribute(Config.FMT_LOCALE, locale);
        servletRequest.setAttribute(Config.FMT_FALLBACK_LOCALE, LocaleUtil.getLocale());

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy()
    {
    }
}
