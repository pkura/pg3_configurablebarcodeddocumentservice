package pl.compan.docusafe.web.common;

import pl.compan.docusafe.util.StringManager;

/**
 *
 * @author Micha� Sankowski
 */
public interface RenderBean {

	String getTemplate();
	StringManager getStringManager();

}
