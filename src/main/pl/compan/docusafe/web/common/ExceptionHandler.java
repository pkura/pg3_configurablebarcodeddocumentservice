package pl.compan.docusafe.web.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ExceptionConfig;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Klasa obsługująca sytuacje wyjątkowe.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExceptionHandler.java,v 1.3 2008/10/06 10:10:04 pecet4 Exp $
 */
public class ExceptionHandler extends org.apache.struts.action.ExceptionHandler
{
    private static final Log log = LogFactory.getLog(ExceptionHandler.class);

    public ActionForward execute(Exception e, ExceptionConfig exceptionConfig,
                                 ActionMapping actionMapping,
                                 ActionForm actionForm,
                                 HttpServletRequest request,
                                 HttpServletResponse response) throws ServletException
    {
        request.setAttribute("exception", e);
        return new ActionForward("exception"); // tiles
    }
}
