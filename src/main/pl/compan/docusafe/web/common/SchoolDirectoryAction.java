package pl.compan.docusafe.web.common;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.School;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Micha� Ma�ski</a>
 */
public class SchoolDirectoryAction extends EventActionSupport
{
    public static final String URL = "/office/common/school.action";

    public static final int LIMIT = 15;

    private String name;
    private String street;
    private String zip;
    private String location;
    
    
    private boolean closeOnReload;

    private Long schoolId;
    private List<? extends School> results;
    private Pager pager;
    private boolean canDelete;
    private boolean canAdd;
    private boolean canAddAndSubmit;

    private int offset;
    private int limit = LIMIT;

    protected void setup()
    {

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(new ValidateAdd()).
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (schoolId != null)
            {
                try
                {
                    DSApi.context().begin();

                    canDelete = true;//DSApi.context().hasPermission(DSPermission.SLOWNIK_SZKOL_USUWANIE);
                    if (!canDelete)
                        throw new EdmException("Brak uprawnie� do modyfikacji s�ownika");

                    School school = School.find(schoolId);

                    if (school.getClass().equals(School.class))
                    {
                        school.delete();
                    }

                    DSApi.context().commit();
                    schoolId = null;
                    addActionMessage("Ze s�ownika usuni�to "+school.getName());                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                canDelete = true;//DSApi.context().hasPermission(DSPermission.SLOWNIK_SZKOL_USUWANIE);
                canAdd = true;//DSApi.context().hasPermission(DSPermission.SLOWNIK_SZKOL_DODAWANIE);

                if (schoolId != null)
                {
                    School school = School.find(schoolId);

                    name = school.getName();
                    street = school.getStreet();
                    zip = school.getZip();
                    location = school.getLocation();
                }
            }
            catch (EdmException e)
            {
                 addActionError(e.getMessage());
            }
        }
    }

    private class ValidateAdd implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           /* if (!StringUtils.isEmpty(zip) && !zip.matches("[0-9]{2}-[0-9]{3}"))
                addActionError("Niepoprawny kod"); */
           
            if (hasActionErrors())
                event.cancel();
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            if (StringUtils.isEmpty(name))
            {
                addActionError("Wymagane jest nazwy szko�y");
                return;
            }

            School school = new School();
            school.setName(TextUtils.trimmedStringOrNull(name, 50));
            school.setLocation(TextUtils.trimmedStringOrNull(location, 50));
            school.setStreet(TextUtils.trimmedStringOrNull(street, 50));
            school.setZip(TextUtils.trimmedStringOrNull(zip, 14));
            
            try
            {
                DSApi.context().begin();

                boolean created = school.createIfNew();

                DSApi.context().commit();

                if (created)
                    addActionMessage("W s�owniku zapisano "+school.getName());
                else
                    addActionMessage("Taki wpis istnieje ju� w s�owniku");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            QueryForm form = new QueryForm(offset, limit);

            if (!StringUtils.isEmpty(name))
                form.addProperty("name", name.trim());
            if (!StringUtils.isEmpty(street))
                form.addProperty("street", street.trim());
            if (!StringUtils.isEmpty(zip))
                form.addProperty("zip", zip.trim());
            if (!StringUtils.isEmpty(location))
                form.addProperty("location", location.trim());

            form.addOrderAsc("name");


            try
            {
                SearchResults<? extends School> results = School.search(form);

                if (results.totalCount() == 0)
                    addActionMessage("Nie znaleziono szk� pasuj�cych do wpisanych danych");

                SchoolDirectoryAction.this.results = fun.list(results);

                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new String[] {
                            "doSearch", "true",
                            "name", name,
                            "street", street,
                            "zip", zip,
                            "location", location,
                            "offset", String.valueOf(offset)
                        });
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }



    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public List getResults()
    {
        return results;
    }

    public Long getSchoolId()
    {
        return schoolId;
    }

    public void setSchoolId(Long schoolId)
    {
        this.schoolId = schoolId;
    }

    public Pager getPager()
    {
        return pager;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanAdd()
    {
        return canAdd;
    }

    public boolean isCloseOnReload()
    {
        return closeOnReload;
    }

    public void setCloseOnReload(boolean closeOnReload)
    {
        this.closeOnReload = closeOnReload;
    }

    public boolean isCanAddAndSubmit()
    {
        return canAddAndSubmit;
    }

    public void setCanAddAndSubmit(boolean canAddAndSubmit)
    {
        this.canAddAndSubmit = canAddAndSubmit;
    }

}

