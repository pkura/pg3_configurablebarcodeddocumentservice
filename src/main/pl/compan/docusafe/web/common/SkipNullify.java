package pl.compan.docusafe.web.common;

import java.lang.annotation.*;
/* User: Administrator, Date: 2006-03-13 17:19:46 */

/**
 * Adnotacja u�ywana przez {@link pl.compan.docusafe.webwork.ParametersInterceptor}
 * do identyfikowania tych parametr�w, na kt�rych konwersja przekazywanych warto�ci
 * do pojedynczych napis�w i null nie powinna by� wykonywana.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SkipNullify.java,v 1.2 2006/03/14 08:48:10 lk Exp $
 */
@Target({ElementType.TYPE})
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface SkipNullify
{
    String[] attributes() default {};
}
