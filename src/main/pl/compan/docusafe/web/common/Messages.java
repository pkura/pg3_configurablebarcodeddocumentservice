package pl.compan.docusafe.web.common;

import java.util.*;

/**
 * Klasa, w kt�rej umieszczane s� komunikaty. Jest u�ywana
 * zamiast klas ActionMessages (ActionErrors) pochodz�cej
 * ze Struts ze wzgl�du na to, �e w EDM komunikaty emitowane
 * przez akcje pochodz� z innych plik�w oraz korzystaj�
 * z w�asnych mechanizm�w lokalizacji.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Messages.java,v 1.2 2004/07/01 20:33:00 administrator Exp $
 */
public class Messages
{
    public static final String ERRORS_KEY = Messages.class.getName()+".ERRORS";
    public static final String MESSAGES_KEY = Messages.class.getName()+".MESSAGES";

    private Map properties = new HashMap();
    private int size;

    public void add(String property, String message)
    {
        List messages = (List) properties.get(property);
        if (messages == null)
        {
            messages = new LinkedList();
            properties.put(property, messages);
        }

        messages.add(message);
        size++;
    }

    public void add(String message)
    {
        add(null, message);
    }

    public void addAll(List messages)
    {
        for (Iterator iter=messages.iterator(); iter.hasNext(); )
        {
            add(null, (String) iter.next());
        }
    }

    public List getMessages(String property)
    {
        return (List) properties.get(property);
    }

    public List getMessages()
    {
        return getMessages(null);
    }

    public int size()
    {
        return size;
    }

    public String toString()
    {
        return getClass().getName()+"[properties="+properties+"]";
    }
}
