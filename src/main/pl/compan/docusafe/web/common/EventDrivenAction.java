package pl.compan.docusafe.web.common;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;
import pl.compan.docusafe.util.StringManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Enumeration;

/**
 *
 * @author <a href="mailto:lukasz@jazzpolice.pl">Lukasz Kowalczyk</a>
 * @version $Id: EventDrivenAction.java,v 1.4 2009/04/22 07:58:01 wkuty Exp $
 */
public abstract class EventDrivenAction extends Action
{
    private final Log log = LogFactory.getLog(EventDrivenAction.class);

    private static final String DO_CANCELLED = "doCancelled";
    private static final String DO_DEFAULT = "doDefault";

    private static final StringManager sm = StringManager.getManager(Constants.Package);

    /**
     * FIXME - musi byc jakis mechanizm na awaryjne zawykanie DSApi 
     * @see org.apache.struts.action.Action#execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
     */
    public final ActionForward execute(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        throws Exception
    {

        String eventName = null;
        String eventParameter = null;
        //boolean indexed = false;

        // szukam nazwy wydarzenia
        if (isCancelled(request))
        {
            eventName = DO_CANCELLED;
        }
        else
        {
            // nazwy mo�liwych wydarze�
            String[] eventNames = getEventNames();

            // szukam parametru, kt�ry odpowiada kt�rej� z nazw mo�liwych wydarze�
            for (int i=0; eventNames != null && i < eventNames.length; i++)
            {
                for (Enumeration parametersEnum=request.getParameterNames(); parametersEnum.hasMoreElements(); )
                {
                    String param = (String)parametersEnum.nextElement();
                    //String value = request.getParameter(param);

                    String tmpEventName = param;

                    // je�eli przycisk jest typu image, na ko�cu nazwy b�d�
                    // wsp�rz�dne klikni�cia
                    if (tmpEventName.endsWith(".x") || tmpEventName.endsWith(".y"))
                    {
                        tmpEventName = tmpEventName.substring(0, tmpEventName.length()-2);
                    }

                    // na ko�cu nazwy parametru mog� znale�� si� dodatkowe argumenty
                    // uj�te w nawiasy klamrowe (inne nawiasy koliduj� z Struts)
                    if (tmpEventName.indexOf('{') > 0)
                    {
                        tmpEventName = tmpEventName.substring(0, tmpEventName.indexOf('{'));
                    }

                    // pomijam parametry, kt�re nie pasuj� do aktualnie
                    // szukanej nazwy wydarzenia
                    if (!tmpEventName.equals(eventNames[i]))
                    {
                        continue;
                    }
                    // pasuj�cy parametr
                    else
                    {
                        eventName = tmpEventName;

                        // opcjonalny parametr umieszczony w nawiasach klamrowych
                        // po nazwie parametru (np. "doUpdate{12}")

                        int leftBrace = param.indexOf('{');
                        if (leftBrace > 0 && param.indexOf('}') > leftBrace)
                        {
                            eventParameter = param.substring(leftBrace+1, param.indexOf('}'));
                        }

                        // XXX: ustawia� warto�� parametru w formularzu?
                        // XXX: r�wnie�, je�eli image?

                        break;
                    }

                }
            }
        }

        if (log.isDebugEnabled())
            log.debug("eventName="+eventName);

        if (eventName == null)
        {
            eventName = DO_DEFAULT;
        }

        if (Arrays.binarySearch(getEventNames(), eventName) < 0)
        {
            throw new RuntimeException(sm.getString("eventDrivenAction.missingEvent",
                getClass().getName(), eventName));
        }

/*
        ActionErrors errors = new ActionErrors();
        ActionMessages messages = new ActionMessages();
*/
        Messages errors = new Messages();
        Messages messages = new Messages();
        ActionForward forward = null;

        try
        {
            forward = callEvent(eventName, mapping, form, eventParameter,
                request, response, errors, messages);
        }
        catch (InvocationTargetException e)
        {
            log.error("", e);

            if (e.getCause() instanceof Exception)
            {
                throw (Exception) e.getCause();
            }

            // pierwotn� przyczyn� powstania InvocationTargetException
            // by� wyj�tek typu Throwable lub Error, nie da si� z tym
            // nic zrobi�
            throw e;
        }

        if (forward == null)
            return null;

        if (errors.size() > 0)
            request.setAttribute(Messages.ERRORS_KEY, errors);

        if (messages.size() > 0)
            request.setAttribute(Messages.MESSAGES_KEY, messages);

        return forward;

    }

    private ActionForward callEvent(
        String eventName,
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    throws Exception
    {
        if (log.isDebugEnabled())
            log.debug("callEvent '"+eventName+"'");

        ActionForward forward = null;

        // je�eli formularz jest typu DynaActionForm, pr�buj� wywo�a�
        // metod� obs�ugi wydarzenia przyjmuj�c� parametr `form' tego
        // typu; je�eli nie ma takiej metody, wywo�uj� metod� przyjmuj�c�
        // parametr `form' typu ActionForm.
        if (form instanceof DynaActionForm)
        {
            try
            {
                forward = (ActionForward)MethodUtils.invokeMethod(
                    this,
                    eventName,
                    new Object[] { mapping, form, eventParameter,
                                   request, response,
                                   errors, messages },
                    new Class[] { ActionMapping.class, DynaActionForm.class, String.class,
                                    HttpServletRequest.class, HttpServletResponse.class,
                                    Messages.class, Messages.class});
            }
            catch (NoSuchMethodException e)
            {
                forward = (ActionForward)MethodUtils.invokeMethod(
                    this,
                    eventName,
                    new Object[] { mapping, form, eventParameter,
                                   request, response,
                                   errors, messages },
                    new Class[] { ActionMapping.class, ActionForm.class, String.class,
                                    HttpServletRequest.class, HttpServletResponse.class,
                                    Messages.class, Messages.class});
            }
        }
        else
        {
            forward = (ActionForward)MethodUtils.invokeMethod(
                this,
                eventName,
                new Object[] { mapping, form, eventParameter,
                               request, response,
                               errors, messages },
                new Class[] { ActionMapping.class, ActionForm.class, String.class,
                                HttpServletRequest.class, HttpServletResponse.class,
                                Messages.class, Messages.class});
        }

        return forward;
    }

    /**
     * Metoda wywo�ywana, gdy formularz nie wygenerowa� jeszcze
     * �adnego wydarzenia (co ma miejsce np. przy pierwszym jego
     * wy�wietleniu).
     * <p>
     * Domy�lna implementacja zwraca warto�� parametru input
     * w konfiguracji akcji lub null.
     */
    public ActionForward doDefault(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        if (mapping.getInput() != null)
        {
            return new ActionForward(mapping.getInput());
        }
        else
        {
            return null;
        }
    }

    /**
     * Obs�uga wydarzenia wywo�ywana, gdy u�ytkownik wybierze
     * w formularzu przycisk <code>&lt;html:cancel></code>.
     * <p>
     * Domy�lna implementacja rzuca wyj�tek UnsupportedOperationException.
     */
    public ActionForward doCancelled(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        //return doDefault(mapping, form, request, response, errors, messages);
        throw new UnsupportedOperationException();
    }

    /**
     * Implementacja metody w klasie dziedzicz�cej powinna zwr�ci�
     * tablic� nazw metod, kt�re s� traktowane jako metody obs�uguj�ce
     * wydarzenia.
     */
    public abstract String[] getEventNames();
}
