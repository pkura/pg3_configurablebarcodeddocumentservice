package pl.compan.docusafe.web.common;

import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.archive.repository.ALDPaymentVerificationAction;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Person;

import java.util.List;
/* User: Administrator, Date: 2006-10-11 16:25:55 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class PersonGroupAction extends EventActionSupport
{
	StringManager sm = 
        GlobalPreferences.loadPropertiesFile(PersonGroupAction.class.getPackage().getName(),null);
    private String group;
    private List<String> results;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }


    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                 results = Person.findGroups(group);

                 if (results.size() == 0)
                    addActionMessage(sm.getString("NieZnalezionoGrupPasujacychDoPodanegoKryterium"));
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public List<String> getResults()
    {
        return results;
    }
}
