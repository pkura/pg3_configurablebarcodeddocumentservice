package pl.compan.docusafe.web.common;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 *
 * @author Micha� Sankowski
 */
public class SimpleRenderBean implements RenderBean{
	private final Object value;
	private final String template;
	private final static StringManager manager
			= GlobalPreferences.loadPropertiesFile(SimpleRenderBean.class.getPackage().getName(),null);

	public SimpleRenderBean(Object value, String template) {
		this.value = value;
		this.template = template;
	}

	public Object getValue() {
		return value;
	}

	public String getTemplate() {
		return template;
	}

	public StringManager getStringManager() {
		return manager;
	}
}
