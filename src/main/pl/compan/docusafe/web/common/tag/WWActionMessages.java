package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WWActionMessages.java,v 1.1 2004/08/12 15:30:37 lk Exp $
 */
public class WWActionMessages extends AbstractUITag
{
    private static final String TEMPLATE = "action-messages.vm";

    protected String getDefaultTemplate()
    {
        return TEMPLATE;
    }
}
