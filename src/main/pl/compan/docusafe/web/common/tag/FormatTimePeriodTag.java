package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

import javax.servlet.jsp.JspException;
import java.io.IOException;

/* User: Administrator, Date: 2005-07-11 14:59:42 */

/**
 * Formatuje przekazany czas do postaci s�ownej.
 * Atrybut value spodziewa si� wyra�enia OGNL ewaluuj�cego do
 * liczby typu Long okre�laj�cej przedzia� czasu w milisekundach.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatTimePeriodTag.java,v 1.2 2005/07/12 11:31:12 lk Exp $
 */
public class FormatTimePeriodTag extends WebWorkTagSupport
{
    private String value;

    public int doStartTag() throws JspException
    {
        if (value == null)
            throw new JspException("Nie podano warto�ci okresu");

        try
        {
            Long period = (Long) findValue(value, Long.class);
            if (period != null)
            {
                pageContext.getOut().print(formatPeriod(period.longValue()));
            }
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    private String formatPeriod(long period)
    {
        int seconds = (int) (period / 1000);

        if (seconds < 60)
        {
            //return conjugate("sekund", seconds);
            return "poni�ej minuty";
        }
        else if (seconds < 3600)
        {
            return conjugate("minut", seconds / 60) +
                (seconds % 60 > 0 ? " i " + conjugate("sekund", seconds % 60) : "");
        }
        else
        {
            int minutes = seconds / 60 - (seconds / 3600) *60;
            return conjugate("godzin", seconds / 3600) +
                (minutes > 0 ? ", " + conjugate("minut", minutes) : "");
        }
    }

    private String conjugate(String root, int number)
    {
        if (number == 1)
        {
            return number + " " + root + "a";
        }
        else if ((number % 10) >= 2 && (number % 10) <= 4 &&
            (number % 100 < 10 || number % 100 > 20)) 
        {
            return number + " " + root + "y";
        }
        else
        {
            return number + " " + root;
        }
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
