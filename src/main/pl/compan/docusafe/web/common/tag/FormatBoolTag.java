package pl.compan.docusafe.web.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

/**
 * Tag formatuj�cy liczb� double na walut�
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class FormatBoolTag extends WebWorkTagSupport 
{
	private static final long serialVersionUID = 1L;
	
	private String value;
	
	@Override
	public int doStartTag() throws JspException 
	{
		try
		{
			Boolean val = (Boolean) findValue(value, Boolean.class);
			pageContext.getOut().print( (val == null || !val) ? "nie" : "tak");
		}
		catch (IOException ex)
		{
			throw new JspException(ex.getMessage(), ex);
		}
		
		return EVAL_PAGE;
	}

	public void setValue(String value) 
	{
		this.value = value;
	}
}
