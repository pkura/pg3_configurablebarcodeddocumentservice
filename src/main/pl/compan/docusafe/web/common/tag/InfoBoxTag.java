package pl.compan.docusafe.web.common.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.logging.LogFactory;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

import pl.compan.docusafe.core.GlobalPreferences;
/**
 * @author Mariusz Kilja�czyk
 * */
public class InfoBoxTag extends WebWorkTagSupport
{
	private static final String BODY = ".body";
	private static final String HEADER = ".header";
	
    private String header;
    private String body;
    private String dockind;
    
    public int doStartTag () throws JspException 
    {
        try
        {
        	if(body != null && body.startsWith("'") && body.endsWith("'"))
        		body = body.substring(1,body.length()-1);
        	else
        	{
        		body = (String) findValue(body); 
        	}

        	if(header != null && header.startsWith("'") && header.endsWith("'"))
        		header = header.substring(1,header.length()-1);
        	else 
        	{
        		header = (String) findValue(header);        		
        	}
        		
        	if(body == null || header == null)
        		return SKIP_BODY;
        	
			Integer offset = 15;
			String iconSrc = ((HttpServletRequest) pageContext.getRequest()).getContextPath()+"/img/info.gif";
			String alt = null;
			String title = null;

			if("true".equals(dockind))
			{
				alt = GlobalPreferences.loadPropertiesFile("","dockind").getString(body+BODY);
				title = GlobalPreferences.loadPropertiesFile("","dockind").getString(header+HEADER);
			}
			else
			{
				  	if(ServletActionContext.getContext().getActionInvocation() == null)
		            {
				  		alt = GlobalPreferences.loadPropertiesFile("",null).getString(body);
						title = GlobalPreferences.loadPropertiesFile("",null).getString(header);
		            }
		            else
		            {
		               String sciezka = ServletActionContext.getContext().getActionInvocation().getAction().getClass().getPackage().getName();
		               alt = GlobalPreferences.loadPropertiesFile(sciezka,null).getString(body);
		               title = GlobalPreferences.loadPropertiesFile(sciezka,null).getString(header);
		            }
			}

			String iconCode= "<img src=\""+iconSrc+"\" width=\"16\" height=\"16\" border=\"0\">";
			String result = "<span title=\"offsetx=["+offset+"] offsety=["+offset+"] delay=[500] header=["+title+"] body=["+alt+"]\" style=\"vertical-align:middle;font-family:arial;font-size:20px;font-weight:bold;color:#ABABAB;cursor:pointer\">"
			+ iconCode + "</span>";
            JspWriter out = pageContext.getOut ();
            out.print(result);
        }
        catch (IOException e)
        {
        	LogFactory.getLog("eprint").error("", e);
        }

        return SKIP_BODY;
    }

    public int doEndTag () throws JspException 
    {
      return EVAL_PAGE;
    }

	public String getHeader() 
	{
		return header;
	}

	public void setHeader(String header) 
	{
		this.header = header;
	}

	public String getBody() 
	{
		return body;
	}

	public void setBody(String body) 
	{
		this.body = body;
	}

	public void setDockind(String dockind) 
	{
		this.dockind = dockind;
	}

	public String isDockind() 
	{
		return dockind;
	}

}