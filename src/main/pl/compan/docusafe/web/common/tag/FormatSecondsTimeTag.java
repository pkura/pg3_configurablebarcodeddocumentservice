package pl.compan.docusafe.web.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

/**
 * Tag zamieniaj�cy liczb� sekund na ci�g, np: 00:09:23
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class FormatSecondsTimeTag extends WebWorkTagSupport 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private String value;
	
	@Override
	public int doStartTag() throws JspException 
	{	
		try
		{
			int seconds = (Integer) findValue(value, Integer.class);
			pageContext.getOut().print(formatSeconds(seconds));
		}
		catch (IOException ex)
		{
			throw new JspException(ex.getMessage(), ex);
		}
				
		return EVAL_PAGE;
	}
	
	private String formatSeconds(int seconds)
	{
		if (seconds < 60)
		{
			if (seconds < 10)
				return "00:00:0" + seconds;
			return "00:00:" + seconds;
		}
			
		int modSec = seconds % 60;
		String secondsStr = modSec < 10 ? "0" + modSec : "" + modSec;
		int minutes = seconds / 60;
		if (minutes < 60)
		{
			if (minutes < 10)
				return "00:0" + minutes + ":" + secondsStr;
			return "00:" + minutes + ":" + secondsStr;
		}
		
		int modMin = minutes % 60;
		String minuntesStr = modMin < 10 ? "0" + modMin : "" + modMin;
		int hours = minutes / 60;
		if (hours < 10)
			return "0" + hours + ":" + minuntesStr + ":" + secondsStr;
		return hours + ":" + minuntesStr + ":" + secondsStr;
	}

	public void setValue(String value) 
	{
		this.value = value;
	}
}
