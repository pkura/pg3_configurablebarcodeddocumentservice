package pl.compan.docusafe.web.common.tag;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.StringManager;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;
import pl.compan.docusafe.util.LoggerFactory;

public class XmlLink extends WebWorkTagSupport
{
    String path;
    private static final Logger log = LoggerFactory.getLogger(XmlLink.class);
    public static final String LINK = "link";
    public static final String TEXT = "text";
    public static final String TITLE = "title";
    public static final String URL = "url";
    public static final String DK_CN = "dockind";
    public static final String LANG = "lang";
    public static final String DOCKINDS="dockinds";
    public static final String ROLES="roles";
    public static final String CN = "cn";
    public static final String NAME = "name";
    public static final String DIVISIONS="divisions";
    public static final String DIVISION="division";
    public static final String GUID="guid";
    public static final String PARAM="param";
    public static final String AVAILABLE = "available";
    public static final String PERMISSION = "permission";
    public static final String PERMISSIONS = "permissions";
    
    //private File f = new File(LangTag.class.getPackage().getName());
    private Properties properties = new Properties();
    private Locale locale;
    private StringManager sm;
    
    //mapa zawierająca Menu odpowiadające kolejnym plikom z katalogu TABS_PATH
    private static HashMap MenuMap  = null;
    
    public void setPath(String path)
    {
        this.path = path;
    }
    
    public class Link 
    {
    	private HashMap textMap ;
    	private String url ;
    	private HashMap titleMap;
    	private ArrayList dockinds;
    	private ArrayList roles;
    	private ArrayList<String> availables;
    	private ArrayList<String> permissions;
    	
    	public ArrayList<String> getPermissions() {
			return permissions;
		}
		public void setPermissions(ArrayList<String> permissions) {
			this.permissions = permissions;
		}
		private HashMap<String, String> paramsMap;
    	
    	
    	public HashMap<String, String> getParamsMap() {
			return paramsMap;
		}
		public void setParamsMap(HashMap<String, String> paramsMap) {
			this.paramsMap = paramsMap;
		}
		// tl
    	private ArrayList<String> divisions;
    	
		public ArrayList<String> getDivisions() {
			return divisions;
		}
		public void setDivisions(ArrayList<String> divisions) {
			this.divisions = divisions;
		}
		
		// tl
		
		public HashMap getTextMap() {
			return textMap;
		}
		public void setTextMap(HashMap textMap) {
			this.textMap = textMap;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public HashMap getTitleMap() {
			return titleMap;
		}
		public void setTitleMap(HashMap titleMap) {
			this.titleMap = titleMap;
		}
		public ArrayList getDockinds() {
			return dockinds;
		}
		public void setDockinds(ArrayList dockinds) {
			this.dockinds = dockinds;
		}
		public void setRoles(ArrayList roles) {
			this.roles = roles;
		}
		public ArrayList getRoles() {
			return roles;
		}
		public ArrayList<String> getAvailables() {
			return availables;
		}
		public void setAvailables(ArrayList<String> availables) {
			this.availables = availables;
		}




		
    }
    
    
    public String parseParams(String url) {
    	char[] url_chars = url.toCharArray();
    	CharArrayWriter caw = new CharArrayWriter();
    	for (int i = 0; i < url_chars.length; i++) {
    		if (url_chars[i] == '$') {
    			i+=2; // ${
    			CharArrayWriter caw2 = new CharArrayWriter();
    			while (url_chars[i] != '}') {
    				caw2.append(url_chars[i]);
    				i++;
    			}
    			
    			String prop_name = caw2.toString();
    			Object prop_value_obj = ServletActionContext.getRequest().getAttribute(prop_name);   
    			if(prop_value_obj == null)
    			{
    				prop_value_obj = findValue(prop_name);
    			}
    			String prop_value;
    			if (prop_value_obj instanceof Long) {
    				prop_value = Long.toString((Long)prop_value_obj);
    			} else {
    				prop_value = (String)ServletActionContext.getRequest().getAttribute(prop_name);
    			}

    			try {
    				if(prop_value != null)
    					caw.write(prop_value);
				} catch (IOException e) {
					LogFactory.getLog("eprint").debug("", e);
				}
    			i++; // }
    		} else {
    			caw.append(url_chars[i]);
    		}
    	}
    	return caw.toString();
    }
    
    public synchronized void fillMenuMap()
    {
    	try{
	    	Date dt = new Date();

	    	MenuMap = new HashMap();
    		
	    	    	
	    	InputStream xmlStream = Docusafe.getServletContext().getResourceAsStream("/WEB-INF/classes/tabs.xml");
	    	SAXReader saxReader = new SAXReader();
	        Document document = saxReader.read(xmlStream);
	        Element root = document.getRootElement();
	               
	        for (Iterator i = root.elementIterator(); i.hasNext();) {
	        	Element tabsElem = (Element)i.next();

	        	ArrayList links = new ArrayList();
		        for (Iterator i2 = tabsElem.elementIterator(); i2.hasNext();) {
		    		Element el = (Element) i2.next();

		    		if(el.getName().equals(LINK)){
		    			Link link = new Link();
		    			String url = el.attributeValue(URL);
		    			link.setUrl(url);
		    		 	HashMap texts = new HashMap();
		    			HashMap titles = new HashMap();
		    			ArrayList dockinds = new ArrayList();
		    			ArrayList roles = new ArrayList();
		    			ArrayList<String> guids = new ArrayList<String>(); 
		    			ArrayList<String> availables = new ArrayList<String>();
		    			ArrayList<String> permissions = new ArrayList<String>();
		    			HashMap<String, String> params = new HashMap<String, String>();
		    			for (Iterator i3 = el.elementIterator(); i3.hasNext();) {
		    				Element subElem = (Element) i3.next();
		    				
		    				
		    				if(subElem.getName().equals(LANG)){
				    			for (Iterator i4 = subElem.elementIterator(); i4.hasNext();) {
				    				
				    				Element langElem = (Element) i4.next();
				    				
				    				texts.put(langElem.getName(), langElem.attributeValue(TEXT));
				    				titles.put(langElem.getName(), langElem.attributeValue(TITLE));
				    			}
		    				}
		    				else if(subElem.getName().equals(PARAM))
		    				{
		    					params.put(subElem.attributeValue(NAME), subElem.getText());
		    				}
		    				else if(subElem.getName().equals(DOCKINDS))
		    				{
		    					for (Iterator i4 = subElem.elementIterator(); i4.hasNext();) {
				    				
				    				Element dockindElem = (Element) i4.next();
				    				if(dockindElem.getName().equals(CN)){
				    					dockinds.add(dockindElem.attributeValue(NAME));
				    				}
				    			}
		    				}
		    				else if(subElem.getName().equals(ROLES))
		    				{
		    					for (Iterator i4 = subElem.elementIterator(); i4.hasNext();) 
		    					{				    				
				    				Element roleElem = (Element) i4.next();
				    				if(roleElem.getName().equals(CN))
				    				{
				    					roles.add(roleElem.attributeValue(NAME));
				    				}
				    			}
		    				}		    				
		    				else if(subElem.getName().equals(DIVISIONS))
		    				{
		    					for (Iterator i4 = subElem.elementIterator(); i4.hasNext();) 
		    					{				    				
				    				Element divisionElem = (Element) i4.next();
				    				if(divisionElem.getName().equals(DIVISION))
				    				{
				    					guids.add(divisionElem.attributeValue(GUID));
				    				}
				    			}		    					
		    				}
		    				else if(subElem.getName().equals(AVAILABLE))
		    				{

		    					availables.add(subElem.attributeValue(NAME));	    					
		    				}
		    				else if(subElem.getName().equals(PERMISSIONS))
		    				{
		    					for (Iterator i4 = subElem.elementIterator(); i4.hasNext();) 
		    					{				    				
				    				Element permElem = (Element) i4.next();
				    				if(permElem.getName().equals(PERMISSION))
				    				{
				    					permissions.add(permElem.attributeValue(NAME));
				    				}
				    			}
		    				}

		    			}
		    			link.setTextMap(texts);
		    			link.setTitleMap(titles);
		    			link.setDockinds(dockinds);
		    			link.setRoles(roles);
		    			link.setDivisions(guids);
		    			link.setParamsMap(params);
		    			link.setAvailables(availables);
		    			link.setPermissions(permissions);
		    			links.add(link);
		    		}
		    		else
		    			throw new org.dom4j.DocumentException("nieprawidłowy element(powinien być link)");
		    	}

		        MenuMap.put(tabsElem.attributeValue(NAME), links);
	        }
    	}catch (org.dom4j.DocumentException e) {
			// TODO Auto-generated catch block
    		MenuMap = null;
    		LogFactory.getLog("eprint").error("", e);
    		e.printStackTrace();
		}
    	
    }

    public int doStartTag () throws JspException 
    {
        try
        {
     
            if(MenuMap==null){
            	fillMenuMap();
            }
         
            Object o = MenuMap.get(path);
            ArrayList links = new ArrayList<Object>();
            if(o != null)
            	links = (ArrayList)o;
            String link = "";
            
            
            String activeLink = ServletActionContext.getRequest().getRequestURI().toString();        
            String homeLink =ServletActionContext.getRequest().getContextPath();

            for (Object object : links) {
            	String text ;
            	String title ;
            	String lang = Docusafe.getCurrentLanguage();
            	String[] allDockinds ;
            	DSApi.openAdmin();
            	allDockinds= DocumentKind.listCns();
            	DSApi._close();
            	ArrayList dockinds = ((Link)object).getDockinds();
            	ArrayList roles = ((Link)object).getRoles();
            	ArrayList<String> divisions = ((Link)object).getDivisions();
            	ArrayList<String> availables = ((Link)object).getAvailables();
            	ArrayList<String> permissions = ((Link)object).getPermissions();
            	
            	boolean enabled = false ;
            	if((dockinds == null || dockinds.isEmpty()))
            	{
            		enabled = true;
            	}            	
            	else
            	{
            		for (Object object2 : allDockinds) 
            		{

						if(dockinds.contains(object2))
						{
							enabled = true;
							break;
						}
					}            		
            	}
            	if (enabled && permissions != null && !permissions.isEmpty())
            	{            		
        			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));            		
            		for (String perm : permissions) 
            		{
            			if(!DSApi.context().hasPermission(DSPermission.getByName(perm)))
						{
            				enabled = false;
							break;
						}
    				}
                    DSApi.close();
            	}
            	if(enabled && roles != null && !roles.isEmpty())
            	{
        			DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            		Set<String> setRoles = DSApi.context().getDSUser().getRoles();
            		
            		for (Object role : roles) 
            		{
            			if(!setRoles.contains(role))
						{
							enabled = false;
							break;
						}
    				}
                    DSApi.close();
            	}
            	if(enabled && availables!=null && ! availables.isEmpty())
            	{
            		DSContext ctx = DSApi.openAdmin();
            		for(String a:availables)
            		{
            			if(!AvailabilityManager.isAvailable(a)) enabled = false;
            		}
                    DSApi.close();
            	}
            	if(enabled && divisions != null && !divisions.isEmpty())
            	{
            		DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            		ArrayList<String> setDivisions = new ArrayList<String>(); 
            		//DSApi.context().getDSUser().getDivisionsAsList();
            		for(DSDivision divi : DSApi.context().getDSUser().getDivisionsAsList())
            		{
            			setDivisions.add(divi.getGuid());
            		}
            		for (Object guid : divisions) 
            		{
            			if(!setDivisions.contains(guid))
						{
							enabled = false;
							break;
						}
    				}
                    DSApi.close();
            	}
            	if(enabled)
            	{
	            	text = (String) ((Link)object).getTextMap().get(lang);
	            	title = (String) ((Link)object).getTitleMap().get(lang);
	            	String temp = (homeLink+((Link)object).getUrl()).split("\\?")[0];
	            	boolean active = activeLink.equals(temp);
	            	//System.out.println("activelink = " + activeLink +" temp = " + temp);
	            	String urlWithParams = parseParams(((Link)object).getUrl());
	            	
	            	if(!((HashMap<String, String>)((Link)object).getParamsMap()).isEmpty())
	            		urlWithParams+="?";
	            	
	            	for(String str : ((Link)object).getParamsMap().keySet())
	            	{
	            		urlWithParams+=str+"="+((Link)object).getParamsMap().get(str)+"&";
	            	}
					link+="<a href=\""+homeLink+urlWithParams+"\" title=\""+title+"\""+(active?"class=\"highlightedText\"":"")+">"+text+"</a>\n";
					link+="<img src=\""+homeLink+"/img/kropka.gif\" width=\"6\" height=\"10\"/>\n";
            	}
			}
            
            JspWriter out = pageContext.getOut ();
            //System.out.println(link);
            String linkWithoutLastDot = link.substring(0, link.lastIndexOf("<img"));
            if(ServletActionContext.getContext().getActionInvocation() == null)
            {
                out.print(linkWithoutLastDot);
            }
            else
            {
               out.print(linkWithoutLastDot);
            }
        }
        
        catch (IOException e)
        {
            // TODO Auto-generated catch block
        	LogFactory.getLog("eprint").debug("", e);
        } catch (EdmException e) {
			// TODO Auto-generated catch block
        	LogFactory.getLog("eprint").debug("", e);
			DSApi._close();
		}
       
        return SKIP_BODY;
    }

    public int doEndTag () throws JspException 
    {
      return EVAL_PAGE;
    }

}