package pl.compan.docusafe.web.common.tag;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * Pozwala na testowanie dost�pnych w�asno�ci systemu, przy czym
 * rodzaj w�asno�ci okre�laj� klasy dziedzicz�ce.
 * <p>
 * Dost�pne w�asno�ci okre�lane s� przez swoje nazwy, a ich dost�pno��
 * mo�na testowa� tworz�c wyra�enia logiczne, np.
 * <code>nazwa1 or (nazwa2 and nazwa3)</code>. 
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FeaturesTag.java,v 1.3 2006/02/20 15:42:38 lk Exp $
 */
public abstract class FeaturesTag extends BodyTagSupport
{
	private static final long serialVersionUID = 1L;
	private String test;

    /**
     * Zwraca tablic� nazw dost�pnych w�asno�ci.
     */
    protected abstract String[] getFeatures();

    public final int doStartTag() throws JspException
    {
        String[] features = getFeatures();
        Map featureMap = new HashMap(features.length);

        for (int i=0; i < features.length; i++)
        {
            featureMap.put(features[i], Boolean.TRUE);
        }

        try
        {
            Boolean result = (Boolean) Ognl.getValue(
                Ognl.parseExpression(test), new OgnlContext(), featureMap, Boolean.class);

            return result != null && result.booleanValue() ? EVAL_BODY_INCLUDE : SKIP_BODY;
        }
        catch (OgnlException e)
        {
            throw new JspException(e.getMessage(), e);
        }
    }

    public void setTest(String test)
    {
        this.test = test;
    }
}
