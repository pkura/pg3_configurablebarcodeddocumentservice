package pl.compan.docusafe.web.common.tag;

import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * Klasa bazowa dla tag�w sprawdzaj�cych obecno�� jakiej� cechy aplikacji.
 * <p>
 * W atrybutach <code>available</code> oraz <code>unavailable</code>
 * przekazuje si� nazwy (rozdzielone przecinkami) cech aplikacji, kt�re
 * powinny by� odpowiednio dost�pne lub niedost�pne, aby zawarto�� tagu
 * zosta�a wykonana.
 * <p>
 * Atrybut <code>operator</code> okre�la operator, jaki powinien zosta�
 * u�yty podczas decydowania o wyniku dzia�ania tagu. Mo�e przyjmowa�
 * warto�ci <b>AND</b> (warto�� domy�lna) lub <b>OR</b>. Wielko�� liter
 * nie ma znaczenia.
 *
 * @deprecated Zast�pione przez FeaturesTag
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FeatureTag.java,v 1.4 2007/11/27 12:11:29 mariuszk Exp $
 */
public abstract class FeatureTag extends BodyTagSupport
{
    private String available;
    private String unavailable;
    private String operator;

    public abstract boolean feature(String feature);

    public int doStartTag() throws JspException
    {
        if ((available == null || available.length() == 0) &&
            (unavailable == null || unavailable.length() == 0))
            throw new JspException("Spodziewano si� atrybutu available lub unavailable");

        if (operator != null && (available == null || unavailable == null))
            throw new JspException("Atrybut operator mo�e by� u�yty tylko wtedy, gdy " +
                "u�yte zostan� jednocze�nie atrybuty available i unavailable");

        if (operator != null && !"and".equalsIgnoreCase(operator) && !"or".equalsIgnoreCase(operator))
            throw new JspException("Atrybut operator musi mie� warto�� OR lub AND");

        boolean availableResult = true;

        // je�eli chocia� jeden z modu��w wymienionych w atrybucie available
        // jest niedost�pny, zawarto�� tagu nie jest wykonywana
        if (available != null && available.length() > 0)
        {
            if (available.indexOf(',') > 0)
            {
                StringTokenizer st = new StringTokenizer(available, ",");
                while (st.hasMoreTokens())
                {
                    if (!feature(st.nextToken().trim()))
                    {
                        availableResult = false;
                        break;
                    }
                }
            }
            else
            {
                if (!feature(available))
                    availableResult = false;
            }
        }

        boolean unavailableResult = true;

        // je�eli kt�rykolwiek z modu��w wymienionych w tagu unavailable jest
        // dost�pny, zawarto�� tagu nie jest wykonywana
        if (unavailable != null && unavailable.length() > 0)
        {
            if (unavailable.indexOf(',') > 0)
            {
                StringTokenizer st = new StringTokenizer(unavailable, ",");
                while (st.hasMoreTokens())
                {
                    if (feature(st.nextToken().trim()))
                    {
                        unavailableResult = false;
                        break;
                    }
                }
            }
            else
            {
                if (feature(unavailable))
                {
                    unavailableResult = false;
                }
            }
        }

        if (operator == null || operator.equalsIgnoreCase("and"))
        {
            return availableResult && unavailableResult ? EVAL_BODY_INCLUDE : SKIP_BODY;
        }
        else // operator = OR
        {
            return availableResult || unavailableResult ? EVAL_BODY_INCLUDE : SKIP_BODY;
        }
    }

    public void setAvailable(String available)
    {
        this.available = available;
    }

    public void setUnavailable(String unavailable)
    {
        this.unavailable = unavailable;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }
}
