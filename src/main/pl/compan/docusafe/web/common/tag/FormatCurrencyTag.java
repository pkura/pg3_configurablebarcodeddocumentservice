package pl.compan.docusafe.web.common.tag;

import java.io.IOException;
import java.text.NumberFormat;

import javax.servlet.jsp.JspException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

/**
 * Tag formatuj�cy liczb� double na walut�
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class FormatCurrencyTag extends WebWorkTagSupport 
{
	private static final Logger log = LoggerFactory.getLogger(FormatCurrencyTag.class);
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private String value;
	
	@Override
	public int doStartTag() throws JspException 
	{
		try
		{
			double price = 0.0;
			NumberFormat format = NumberFormat.getCurrencyInstance(GlobalPreferences.localeMap.get(Docusafe.getCurrentLanguage()));
			
			try
			{
			Object obj = findValue(value, Double.class);
			if(obj == null )
				pageContext.getOut().print(format.format(0));
			price = (Double) obj;
			}
			catch (Exception e) {
				log.error(e.getMessage());
			}
			 
		
			
			pageContext.getOut().print(format.format(price));
		}
		catch (IOException ex)
		{
			throw new JspException(ex.getMessage(), ex);
		}
		
		return EVAL_PAGE;
	}

	public void setValue(String value) 
	{
		this.value = value;
	}
}
