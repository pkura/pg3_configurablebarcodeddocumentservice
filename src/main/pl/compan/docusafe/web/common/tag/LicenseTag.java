package pl.compan.docusafe.web.common.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import pl.compan.docusafe.boot.Docusafe;

/**
 * Tag sprawdzajacy numer licencji.
 * @author r.powroznik
 */
public class LicenseTag extends BodyTagSupport
{
	
    private String test;

    public void setTest(String test)
    {
        this.test = test;
    }
    
    public int doStartTag() throws JspException
    {
    	String str = Integer.toString(Docusafe.getLicense().getLicenseNo());
    	return str.equals(test) ? EVAL_BODY_INCLUDE : SKIP_BODY;
    }
}
