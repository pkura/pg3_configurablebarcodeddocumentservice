package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

import javax.servlet.jsp.JspException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/* User: Administrator, Date: 2005-05-12 12:19:50 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatDateTag.java,v 1.2 2006/02/20 15:42:38 lk Exp $
 */
public class FormatDateTag extends WebWorkTagSupport
{
	private static final long serialVersionUID = 1L;
	
	private static final FormatCache cache = new FormatCache(5);
    private String value;
    private String pattern;

    public int doStartTag() throws JspException
    {
        Date date = (Date) findValue(value, Date.class);
        if (date == null)
            return EVAL_PAGE;

        if (pattern == null || pattern.trim().length() == 0)
            throw new JspException("Nie podano wzorca formatowania");

        DateFormat df = (DateFormat) cache.get(pattern);
        if (df == null)
        {
            df = new SimpleDateFormat(pattern);
            synchronized (cache)
            {
                cache.put(pattern, df);
            }
        }

        synchronized (df)
        {
            try
            {
                pageContext.getOut().print(df.format(date));
            }
            catch (IOException e)
            {
                throw new JspException(e.getMessage(), e);
            }
        }

        return EVAL_PAGE;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

	private static class FormatCache extends LinkedHashMap
    {
        private int max;

        public FormatCache(int max)
        {
            super(max+1);  // +1, bo removeEldestEntry() jest wywoływane po put()
            this.max = max;
        }

        protected boolean removeEldestEntry(Map.Entry eldest)
        {
            return size() > max;
        }
    }
}
