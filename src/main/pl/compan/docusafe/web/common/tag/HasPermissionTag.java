package pl.compan.docusafe.web.common.tag;

import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/* User: Administrator, Date: 2005-04-28 14:32:48 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: HasPermissionTag.java,v 1.1 2005/04/28 14:51:40 lk Exp $
 */
public class HasPermissionTag extends BodyTagSupport
{
    private String name;

    public int doStartTag() throws JspException
    {
        if (DSPermission.getByName(name) == null)
            throw new JspException("Nie istnieje uprawnienie o nazwie "+name);

        PermissionCache cache = null;
        try
        {
            cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
        }
        catch (ServiceException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        UserPrincipal principal = AuthUtil.getUserPrincipal((HttpServletRequest) pageContext.getRequest());

        if (principal == null)
            return SKIP_BODY;

        if (cache.hasPermission(principal.getName(), name))
            return EVAL_BODY_INCLUDE;

        return SKIP_BODY;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
