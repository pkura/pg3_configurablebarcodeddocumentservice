package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;
import pl.compan.docusafe.boot.Docusafe;

import javax.servlet.jsp.JspException;
import java.io.IOException;

public class AdditionValueTag extends WebWorkTagSupport {
    private String name;
    private String documentKindCn;

    @Override
    public int doStartTag() throws JspException {
        if(documentKindCn != null && documentKindCn.startsWith("'") && documentKindCn.endsWith("'")) {
            documentKindCn = documentKindCn.substring(1, documentKindCn.length() - 1);
        } else {
            documentKindCn = (String) findValue(documentKindCn);
        }

        String val = Docusafe.getAdditionPropertyOrDefault(name, documentKindCn, "");
        try {
            pageContext.getOut().write(val);
        } catch (IOException e) {
            throw new JspException(e.getMessage(), e);
        }
        return EVAL_PAGE;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDocumentKindCn(String documentKindCn) {
        this.documentKindCn = documentKindCn;
    }
}
