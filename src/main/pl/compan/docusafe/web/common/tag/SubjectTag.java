package pl.compan.docusafe.web.common.tag;

import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.CertificatePrincipal;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.Set;

/* User: Administrator, Date: 2005-05-19 15:20:07 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SubjectTag.java,v 1.4 2006/02/20 15:42:38 lk Exp $
 */
public class SubjectTag extends BodyTagSupport
{
    private String hasCertificate;
    private String admin;

    public int doStartTag() throws JspException
    {
        Subject subject = (Subject)
            ((HttpServletRequest) pageContext.getRequest()).getSession(true).
            getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        if (subject == null)
            return SKIP_BODY;

        if (hasCertificate != null)
        {
            boolean bHasCertificate = Boolean.valueOf(hasCertificate).booleanValue();
            Set certPrincipals;
            boolean has =
                (certPrincipals = subject.getPrincipals(CertificatePrincipal.class)) != null &&
                certPrincipals.size() > 0;
            return (bHasCertificate && has) || (!bHasCertificate && !has) ?
                EVAL_BODY_INCLUDE : SKIP_BODY;
        }

        if (admin != null)
        {
            boolean isAdmin = AuthUtil.hasRole(subject, "admin");
            if ((isAdmin && Boolean.valueOf(admin).booleanValue() == true) ||
                (!isAdmin && Boolean.valueOf(admin).booleanValue() == false))
            {
                return EVAL_BODY_INCLUDE;
            }
        }

        return SKIP_BODY;
    }

    public void setHasCertificate(String hasCertificate)
    {
        this.hasCertificate = hasCertificate;
    }

    public void setAdmin(String admin)
    {
        this.admin = admin;
    }
}
