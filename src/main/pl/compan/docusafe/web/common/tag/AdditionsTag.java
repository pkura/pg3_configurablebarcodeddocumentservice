package pl.compan.docusafe.web.common.tag;

import pl.compan.docusafe.core.cfg.Configuration;

/**
 * Tag sprawdzaj�cy czy w systemie obecny jest dany dodatek (np. polecenia czy kalendarz).
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class AdditionsTag extends FeaturesTag
{
	private static final long serialVersionUID = 1L;

	protected String[] getFeatures()
    {
        String[] adds = Configuration.getAvailableAdditions();
        return adds;
    }
}

