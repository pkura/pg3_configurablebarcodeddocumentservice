package pl.compan.docusafe.web.common.tag;

import javax.servlet.jsp.JspException;

import pl.compan.docusafe.core.AvailabilityManager;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

/**
 * TAG <ds:available> odpowiedzialny za sprawdzenie dostepnosci okreslonej funkcji
 * 
 * @author <a href="mailto:piotr.komisarski@docusafe.pl">Piotr Komisarski</a>
 */
public class AvailableTag extends WebWorkTagSupport
{
	private static final long serialVersionUID = 1L;

	/** nazwa funkcji podlegajacej sprawdzeniu pod wzgledem dostepnosci */
    private String test;
    
    private Boolean negation;
    private String avtivityId;
    
    /** CN dockindu w kontekscie ktorego sprawdzana jest dostepnosc funkcji.
     * Jesli NULL, sprawdzenie bedzie, czy funkcja jest OGOLNIE dostepna.
     * dockind moze byc podany w zapisie z ciapkami: 'xyz' albo bez: xyz */
    private String documentKindCn;

    public int doStartTag () throws JspException
    {
    	if(documentKindCn != null && documentKindCn.startsWith("'") && documentKindCn.endsWith("'"))
    		documentKindCn = documentKindCn.substring(1,documentKindCn.length()-1);
    	else
    		documentKindCn = (String) findValue(documentKindCn);

        if(avtivityId != null && avtivityId.startsWith("'") && avtivityId.endsWith("'"))
            avtivityId = avtivityId.substring(1,avtivityId.length()-1);
        else
            avtivityId = (String) findValue(avtivityId);

    	if(negation)
    		return (AvailabilityManager.isAvailable(test,documentKindCn,avtivityId))? SKIP_BODY : EVAL_BODY_INCLUDE;
    	else	
    		return (AvailabilityManager.isAvailable(test,documentKindCn,avtivityId))? EVAL_BODY_INCLUDE : SKIP_BODY;
    }

    public void setTest(String test)
    {
    	if(test.startsWith("!"))
    	{
    		this.negation = true;
    		this.test = test.substring(1);
    	}
    	else
    	{
    		this.test = test;
    		this.negation = false;
    	}
    }

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

    public String getAvtivityId() {
        return avtivityId;
    }

    public void setAvtivityId(String avtivityId) {
        this.avtivityId = avtivityId;
    }
}