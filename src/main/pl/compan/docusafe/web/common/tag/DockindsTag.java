package pl.compan.docusafe.web.common.tag;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;

/**
 * Tag sprawdzaj�cy czy w systemie obecny jest dany rodzaj dokumentu.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class DockindsTag extends FeaturesTag {

	private static final long serialVersionUID = 1L;
	private final Log log = LogFactory.getLog(getClass());

	protected String[] getFeatures() {
		try {
			DSApi.openAdmin();

			return DocumentKind.listCns();
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			DSApi._close();
		}
		return new String[]{};
	}
}
