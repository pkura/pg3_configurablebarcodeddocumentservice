package pl.compan.docusafe.web.common.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import pl.compan.docusafe.core.GlobalConstants;
import pl.compan.docusafe.core.cache.CacheDefinition;
import pl.compan.docusafe.core.cache.CacheRegistry;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Node;
import pl.compan.docusafe.web.common.Sitemap;

import com.google.common.cache.Cache;
import com.opensymphony.webwork.util.MakeIterator;
import com.opensymphony.webwork.views.jsp.WebWorkBodyTagSupport;
import com.opensymphony.xwork.util.OgnlValueStack;

/* User: Administrator, Date: 2005-07-06 11:49:28 */

/**
 * Tag generuj�cy menu ��danego poziomu.  Poziom okre�lany jest
 * atrybutem menu, kt�rego warto�� jest wyra�eniem OGNL ewaluuj�cym
 * do liczby 1 lub 2, inne warto�ci nie s� dozwolone. Warto�� 1 oznacza
 * menu najwy�szego poziomu, warto�� 2 oznacza menu kolejnego poziomu,
 * za� konkretne elementy menu s� okre�lane na podstawie adresu
 * wy�wietlanej strony.
 * <p>
 * Atrybut node okre�la nazw� zmiennej kontekstowej, w kt�rej nale�y
 * umie�ci� element Node zwi�zany z adresem wy�wietlanej strony aplikacji.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SitemapTag.java,v 1.22 2009/03/06 12:47:22 pecet1 Exp $
 */
public class SitemapTag extends WebWorkBodyTagSupport
{
    private final static Logger LOG = LoggerFactory.getLogger(SitemapTag.class);
    
    @SuppressWarnings("unchecked")
	private final static Cache<SitemapRequest, List<Node>> CACHED_NODES = 
		((Cache<SitemapRequest, List<Node>>) CacheRegistry.instance().getCache(CacheDefinition.SITE_MAP, "DEFAULT", true));
    
    /** The menu. */
    private String menu;
    
    /** The node. */
    private String node;

    /** The iterator. */
    private Iterator iterator;

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doStartTag()
     */
    @SuppressWarnings("unchecked")
	public int doStartTag() throws JspException
    {
        final HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        OgnlValueStack stack = getStack();

        if (node != null) {
            stack.getContext().put(node, Sitemap.getBoundNode());
        }

        final Integer menuLevel = menu == null ? null : (Integer) findValue(menu, Integer.class);
        if (menuLevel == null) return SKIP_BODY;
        if (menuLevel.intValue() < 1 || menuLevel.intValue() > 2){
            throw new JspException("menuLevel="+menuLevel+" (dozwolone warto�ci: 1, 2)");
        }
        final Subject subject = (Subject) request.getSession(true).
                getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        final SitemapRequest sr = new SitemapRequest();
        sr.menuLevel = menuLevel;
        UserPrincipal principal = AuthUtil.getUserPrincipal(request);
        sr.username = principal.getName();
        sr.uriPart = (String) request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI);

        List<Node> nodes;
//        long start = System.currentTimeMillis();
        try
        {
            nodes = CACHED_NODES.get(sr, new Callable<List<Node>>()
        	{
                @Override
                public List<Node> call() throws JspException
                {
                    return outerProvideNodes(subject, request, menuLevel);
                }
            });
        }
        catch (ExecutionException e)
        {
            LOG.error("",e);
            nodes = outerProvideNodes(subject, request, menuLevel);
        }
       
        iterator = MakeIterator.convert(nodes);
        // get the first
        return tryNextNode(stack);
    }
    
    public static class SitemapRequest {
        String username;
        String uriPart;
        int menuLevel;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SitemapRequest that = (SitemapRequest) o;

            if (menuLevel != that.menuLevel) return false;
            if (uriPart != null ? !uriPart.equals(that.uriPart) : that.uriPart != null) return false;
            if (username != null ? !username.equals(that.username) : that.username != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = username != null ? username.hashCode() : 0;
            result = 31 * result + (uriPart != null ? uriPart.hashCode() : 0);
            result = 31 * result + menuLevel;
            return result;
        }

        @Override
        public String toString() {
            return "SitemapRequest{" +
                    "menuLevel=" + menuLevel +
                    ", username='" + username + '\'' +
                    ", uriPart='" + uriPart + '\'' +
                    '}';
        }
    }

    private static List<Node> outerProvideNodes(Subject subject, HttpServletRequest request, Integer menuLevel) throws JspException {
        List<Node> nodes;
        if (menuLevel == 1) {
            nodes = new ArrayList(Sitemap.getMainMenu());
        } else {
            nodes = new ArrayList(Sitemap.getLeftMenu());
        }
        // zalogowany u�ytkownik

        Node.checkPermission(nodes,request,subject);
        nodes = provideNodes(request, menuLevel, nodes, subject);
        return nodes;
    }

    private static List<Node> provideNodes(HttpServletRequest request, Integer menuLevel, List<Node> nodes, Subject subject) throws JspException {
        if (menuLevel == 1)
        {
	        List newNodes = new ArrayList<MenuNodes>();
	        for (Node node : nodes)
	        {
				List<Node> children = new ArrayList<Node>(node.getChildren());
				Node.checkPermission(children,request,subject);
				newNodes.add(new MenuNodes(children,node));
			}
	        return newNodes;
        }
        else
        {
            return nodes;
        }
    }

    private int tryNextNode(OgnlValueStack stack) {
        if ((iterator != null) && iterator.hasNext()) {
            Object currentValue = iterator.next();
            stack.push(currentValue);

            String tagId = getId();

            if ((tagId != null) && (currentValue != null)) {
                pageContext.setAttribute(tagId, currentValue);
                pageContext.setAttribute(tagId, currentValue, PageContext.REQUEST_SCOPE);
            }

            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doAfterBody()
     */
    public int doAfterBody() throws JspException {
        OgnlValueStack stack = getStack();
        stack.pop();

        if (iterator.hasNext()) {
            Object currentValue = iterator.next();
            stack.push(currentValue);

            String tagId = getId();

            if ((tagId != null) && (currentValue != null)) {
                pageContext.setAttribute(tagId, currentValue);
                pageContext.setAttribute(tagId, currentValue, PageContext.REQUEST_SCOPE);
            }

            return EVAL_BODY_AGAIN;
        } else {

            // Release objects
            iterator = null;

            try {
                bodyContent.writeOut(bodyContent.getEnclosingWriter());
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }

            return SKIP_BODY;
        }
    }


    /**
     * Sets the menu.
     *
     * @param menu the new menu
     */
    public void setMenu(String menu)
    {
        this.menu = menu;
    }

    /**
     * Sets the node.
     *
     * @param node the new node
     */
    public void setNode(String node)
    {
        this.node = node;
    }
    
    /**
     * The Class MenuNodes.
     */
    public static class MenuNodes
    {
    	
	    /** The node. */
	    Node node;
    	
	    /** The children nodes. */
	    List<Node> childrenNodes;
    	
		/**
		 * Instantiates a new menu nodes.
		 *
		 * @param childrenNodes the children nodes
		 * @param node the node
		 */
		public MenuNodes(List<Node> childrenNodes, Node node) 
		{
			super();
			this.childrenNodes = childrenNodes;
			this.node = node;
		}
		
		/**
		 * Gets the node.
		 *
		 * @return the node
		 */
		public Node getNode() {
			return node;
		}
		
		/**
		 * Sets the node.
		 *
		 * @param node the new node
		 */
		public void setNode(Node node) {
			this.node = node;
		}
		
		/**
		 * Gets the children nodes.
		 *
		 * @return the children nodes
		 */
		public List<Node> getChildrenNodes() {
			return childrenNodes;
		}
		
		/**
		 * Sets the children nodes.
		 *
		 * @param childrenNodes the new children nodes
		 */
		public void setChildrenNodes(List<Node> childrenNodes) {
			this.childrenNodes = childrenNodes;
		}
    }
}
