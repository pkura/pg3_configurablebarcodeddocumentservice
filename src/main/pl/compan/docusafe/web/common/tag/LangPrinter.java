package pl.compan.docusafe.web.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.GlobalPreferences;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Logika wydzielona z LangTagu aby mozna go bylo stosowac w innych tagach
 * @author wkutyla
 *
 */
public class LangPrinter 
{
	final static Logger log = LoggerFactory.getLogger(LangPrinter.class);
	
	/**
     * Wypisuje wartosc teksu na PrintWriter
     * Metoda zostala wydzielona, aby mogla zostac wykorzystana w innych tagach DS
     * @param out
     * @param text
     * @throws IOException
     */
    public void printKeyValue(JspWriter out,String text) throws IOException
    {
    	out.print(getKeyValue(text));
    }
    
    /**
     * Wartosc Tekstu na podstawie klucza langKey
     * @param text
     * @return
     */
    public String getKeyValue(String text)
    {
    	if(ServletActionContext.getContext().getActionInvocation() == null)
        {
        	//log.trace("actionInvocation==null");
            return GlobalPreferences.loadPropertiesFile("",null).getString(text);
        }
        else
        {
           String sciezka = ServletActionContext.getContext().getActionInvocation().getAction().getClass().getPackage().getName();
           //log.trace("sciezka={}",sciezka);
           return GlobalPreferences.loadPropertiesFile(sciezka,null).getString(text);
        }
    }
}
