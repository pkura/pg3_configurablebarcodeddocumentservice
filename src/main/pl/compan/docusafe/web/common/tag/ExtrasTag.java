package pl.compan.docusafe.web.common.tag;

import pl.compan.docusafe.boot.Docusafe;

/* User: Administrator, Date: 2005-06-23 10:31:21 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExtrasTag.java,v 1.3 2006/02/20 15:42:38 lk Exp $
 */
public class ExtrasTag extends FeaturesTag
{
	private static final long serialVersionUID = 1L;

	protected String[] getFeatures()
    {
        return Docusafe.getAvailableExtras();
    }
}
