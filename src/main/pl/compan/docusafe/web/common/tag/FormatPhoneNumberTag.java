package pl.compan.docusafe.web.common.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;

/**
 * Tag formatujący numer telefonu
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class FormatPhoneNumberTag extends WebWorkTagSupport 
{
	private String value;
	
	private static final String DELIMITER = "-";
	
	@Override
	public int doStartTag() throws JspException 
	{
		try
		{
			String number = (String) findValue(value, String.class);
			StringBuffer numberBuffer = new StringBuffer();
			if (StringUtils.isNotEmpty(number) && StringUtils.isNumeric(number))
			{
				if (number.length() == 9)
				{
					// standardowy numer, np: 226340453
					// konwersja na (022) 634-04-53
					numberBuffer.append("(");
					numberBuffer.append(StringUtils.substring(number, 0, 2));
					numberBuffer.append(")");
					numberBuffer.append(" ");
					numberBuffer.append(StringUtils.substring(number, 2, 5));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 5, 7));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 7, 9));
				}
				if (number.length() == 10 && StringUtils.startsWith(number, "0"))
				{
					// numer komórkowy rozpoczynający sie od 0 np. 0505345234
					// konwersja na +48 505-345-234
					numberBuffer.append("+48 ");
					numberBuffer.append(StringUtils.substring(number, 1, 4));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 4, 7));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 7, 10));
				}
				if (number.length() == 11)
				{
					// numer komórkowy rozpoczynający sie od numeru kraju, np: 48505234567
					// konwersja na +48 505-234-567
					numberBuffer.append("+");
					numberBuffer.append(StringUtils.substring(number, 0, 2));
					numberBuffer.append(" ");
					numberBuffer.append(StringUtils.substring(number, 2, 5));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 5, 8));
					numberBuffer.append(DELIMITER);
					numberBuffer.append(StringUtils.substring(number, 8, 11));
				}
			}
			
			if (numberBuffer.length() == 0)
				// wartość numeru jest niestandardowa
				numberBuffer.append(number);
			
			pageContext.getOut().print(numberBuffer.toString());
		}
		catch (IOException ex)
		{
			throw new JspException(ex.getMessage(), ex);
		}
		
		return EVAL_PAGE;
	}

	public void setValue(String value) 
	{
		this.value = value;
	}
}
