package pl.compan.docusafe.web.common.tag;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;
import pl.compan.docusafe.boot.Docusafe;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ModulesTag.java,v 1.5 2008/10/06 10:40:35 pecet4 Exp $
 */
public class ModulesTag extends BodyTagSupport
{
    private String test;

    public void setTest(String test)
    {
        this.test = test;
    }

    public int doStartTag() throws JspException
    {
        Map modulesMap = new HashMap();
        String[] availableModules = Docusafe.getAvailableModules();
        for (int i=0; i < availableModules.length; i++)
        {
            modulesMap.put(availableModules[i], Boolean.TRUE);
        }

        try
        {
            Boolean result = (Boolean) Ognl.getValue(
                Ognl.parseExpression(test),
                new OgnlContext(),
                modulesMap,
                Boolean.class);

            return result != null && result.booleanValue() ? EVAL_BODY_INCLUDE : SKIP_BODY;
        }
        catch (OgnlException e)
        {
            throw new JspException(e.getMessage(), e);
        }
    }
}
