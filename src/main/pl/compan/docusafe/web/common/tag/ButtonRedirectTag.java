package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.ui.AbstractUITag;
import com.opensymphony.xwork.util.OgnlValueStack;

import javax.servlet.http.HttpServletRequest;
/* User: Administrator, Date: 2005-11-16 13:55:30 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ButtonRedirectTag.java,v 1.2 2006/02/20 15:42:38 lk Exp $
 */
public class ButtonRedirectTag extends AbstractUITag
{
	private static final long serialVersionUID = 1L;

	public static final String TEMPLATE = "button-redirect.vm";

    private String href;
    private String disabled;

    protected void evaluateParams(OgnlValueStack ognlValueStack)
    {
        if (valueAttr == null) {
            valueAttr = "'Submit'";
        }

        super.evaluateParams(ognlValueStack);

        addParameter("href", ((HttpServletRequest) pageContext.getRequest())
            .getContextPath()+findValue(href, String.class));
        addParameter("disabled", findValue(disabled, Boolean.class));
    }

    protected String getDefaultTemplate()
    {
        return TEMPLATE;
    }

    public void setHref(String href)
    {
        this.href = href;
    }

    public void setDisabled(String disabled)
    {
        this.disabled = disabled;
    }
}
