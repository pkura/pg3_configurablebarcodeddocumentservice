package pl.compan.docusafe.web.common.tag;

import com.opensymphony.webwork.views.jsp.WebWorkTagSupport;
import pl.compan.docusafe.util.ContextLocale;

import javax.servlet.jsp.JspException;
import java.io.IOException;
import java.text.NumberFormat;

/* User: Administrator, Date: 2005-05-12 10:58:57 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatSizeTag.java,v 1.2 2006/02/06 16:05:59 lk Exp $
 */
public class FormatSizeTag extends WebWorkTagSupport
{
    private String value;

    public int doStartTag() throws JspException
    {
        if (value == null)
            throw new JspException("Nie podano rozmiaru");

        try
        {
            Integer iSize = (Integer) findValue(value, Integer.class);
            if (iSize != null)
                pageContext.getOut().print(formatSize(iSize.intValue()));
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * Formatuje przekazan� liczb� jako rozmiar w bajtach, kilobajtach lub
     * megabajtach, zale�nie od wielko�ci liczby.
     * @return
     */
    private String formatSize(int size)
    {
        NumberFormat format = NumberFormat.getNumberInstance(ContextLocale.getLocale());
        double number;
        String unit;

        // poni�ej kilobajta - dok�adny rozmiar w bajtach
        if (size < 1024)
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size;
            unit = "B";
        }
        // poni�ej 100 KB - rozmiar w KB zaogkr�glony do dw�ch miejsc dziesi�tnych
        else if (size < 100*1024)
        {
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
            number = size / 1024.0;
            unit = "KB";
        }
        // poni�ej 1 MB - rozmiar w KB bez miejsc dziesi�tnych
        else if (size < 1024*1024)
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size / 1024.0;
            unit = "KB";
        }
        // powy�ej 1 MB - rozmiar w MB bez miejsc dziesi�tnych
        else
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size / (double) (1024*1024);
            unit = "MB";
        }

        StringBuilder result = new StringBuilder();
        result.append(format.format(number));
        result.append(" ");
        result.append(unit);

        return result.toString();
    }
}
