package pl.compan.docusafe.web.common.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TAG <ds:lang> odpowiedzialny za wypisanie tekstu we wlasciwym jezyku
 * 
 * @author wkutyla
 */
public class LangTag extends TagSupport
{
	final static long serialVersionUID = 1;
	final static Logger log = LoggerFactory.getLogger(LangTag.class); 
	final static LangPrinter langPrinter = new LangPrinter();
    String text;
    String path;    

    
    public void setText(String text) 
    {
      this.text = text;
    }
    
    public void setPath(String path)
    {
        this.path = path;
    }
    
    
    
    @Override
    public int doStartTag () throws JspException 
    {
        try
        {        	
        	if (log.isTraceEnabled())
        	{
        		log.trace("text={}",text);        	
        	}
            JspWriter out = pageContext.getOut ();
            langPrinter.printKeyValue(out,text);            
        }
        catch (IOException e)
        {
        	log.warn("", e);
        }

        return SKIP_BODY;
    }

    public int doEndTag () throws JspException 
    {
      return EVAL_PAGE;
    }
}