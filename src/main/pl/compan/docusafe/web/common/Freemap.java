package pl.compan.docusafe.web.common;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/* User: Administrator, Date: 2005-07-05 16:04:11 */

/**
 * Klasa wspomagaj�ca tworzenie map serwis�w.  Mapa znajduje si� w pliku
 * xml o dowolnej strukturze (elementy mog� nazywa� si� dowolnie, ale
 * musz� mie� atrybuty href oraz powinny mie� atrybuty label, je�eli maj�
 * by� u�ywane do tworzenia menu).
 * <p>
 * Na podstawie pliku xml tworzone jest drzewo obiekt�w {@link Node}.
 * <p>
 * Dodatkowo mo�na umieszcza� w�asne atrybuty w oddzielnych przestrzeniach
 * nazw, podaj�c metodzie {@link #read(java.io.InputStream, String[])} nazwy
 * uri odpowiadaj�cych tym przestrzeniom, aby zosta�y odczytane.
 * Warto�ci tych atrybut�w mo�na nast�pnie odczytywa� z element�w Node przy
 * pomocy {@link Node#getAttribute(String, String)}.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Freemap.java,v 1.7 2007/07/02 15:01:05 lk Exp $
 */
public class Freemap
{
    private final Node root = new Node();

    /**
     * Wyszukuje jak najg��biej po�o�on� ga��� drzewa o podanym urlu.
     * Je�eli ga��� nie zostanie znaleziona na podstawie samego parametru
     * uri, dodawany jest do niego po znaku '?' parametr queryString
     * i wyszukiwanie jest ponawiane.
     * @param uri
     * @param queryString
     * @return
     */
    public Node findByHref(String uri, String queryString)
    {
        if (uri == null)
            return null;

        Node node = root.findByHref(uri);

        if (node == null && queryString != null)
            node = root.findByHref(uri + '?' + queryString);

        return node;
    }

    /**
     * Zwraca list� element�w drzewa znajduj�cych si� na danym poziomie,
     * przez kt�ry przechodzi �cie�ka do przekazanego elementu node.
     * <p>
     * Je�eli nie da si� okre�li� menu zwracana jest pusta lista obiekt�w Node.
     * @param level Poziom, na kt�rym znajduje si� ��dana lista element�w Node.
     * G��wny element znajduje si� na poziomie 0.
     * @param node Element, do kt�rego prowadzi �cie�ka, na kt�rej musi si�
     * znajdowa� ��dana lista element�w. Mo�e mie� warto�� null tylko dla level=1.
     * Je�eli poziom tego elementu jest ni�szy ni� poziom przekazany w parametrze
     * level, zwracana jest pusta lista.
     */
    public List getNodesAt(int level, Node node)
    {
        // sytuacja wyj�tkowa - nie ma sensu sprawdza� warunk�w, bo wiadomo o jakie elementy chodzi
        if (level == 1)
            return root.getChildren();

        if (level == 0)
            return root.getSiblings();

        if (level < 1)
            throw new IllegalArgumentException("Najni�szy dozwolony poziom to 1 (level="+level+")");

        if (node == null)
            throw new NullPointerException("node");

        if (node.getLevel() < level)
            return Collections.EMPTY_LIST;

        return node.findAncestorAt(level).getSiblings();
    }

    /**
     * Odczytuje wskazany plik freemap.xml.
     * @param is Strumie� zawieraj�cy opis drzewa.
     * @param nsUris Tablica zawieraj�ca list� uri zwi�zanych z przestrzeniami nazw,
     * w kt�rych znajduj� si� atrybuty, jakie nale�y zapami�ta� w obiektach Node.
     * Mo�e by� r�wna null.
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public void read(InputStream is, String[] nsUris)
        throws ParserConfigurationException, SAXException, IOException
    {
        if (is == null)
            throw new NullPointerException("is");
        new Reader(new InputSource(is), root, nsUris);
    }

    private static class Reader extends DefaultHandler
    {
        private Stack nodeStack = new Stack();
        private Node root;
        private StringBuilder path = new StringBuilder("");
        private int level;
        private String[] nsUris;

        /**
         * @param source
         * @param root
         * @param nsUris Przestrzenie nazw, z kt�rych atrybuty maj� by�
         * zapisywane w elementach Node.
         * @throws ParserConfigurationException
         * @throws SAXException
         * @throws IOException
         */
        public Reader(InputSource source, Node root, String[] nsUris)
            throws ParserConfigurationException, SAXException, IOException
        {
            this.root = root;
            if (nsUris != null)
            {
                this.nsUris = (String[]) nsUris.clone();
                Arrays.sort(this.nsUris);
            }
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.newSAXParser().parse(source, this);
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
        {
            if (nodeStack.isEmpty())
            {
                nodeStack.push(root);
                return;
            }

            // nie obs�uguj� element�w w innej ni� domy�lna przestrzeni nazw
            if (uri != null && uri.length() > 0)
                return;

            path.append('/');
            path.append(localName);

            level++;

            String href = attributes.getValue("href");
            if (href == null || href.length() == 0)
                throw new SAXException("Atrybut href jest wymagany");
            // Zamiana znakow
            // %26 -> &
            href = decodeString(href);
            
            
            Node node = new Node(path.toString(), href, attributes.getValue("label"), level, attributes.getValue("dynamicMenu"), attributes.getValue("param"));
            
            if(attributes.getValue("htmlId") != null && attributes.getValue("htmlId").length() > 0)
                node.setHtmlId(attributes.getValue("htmlId"));
            
            if (nsUris != null)
            {
                for (int i=0; i < attributes.getLength(); i++)
                {
                    String URI = attributes.getURI(i);
                    if (URI != null && URI.length() > 0 && Arrays.binarySearch(nsUris, URI) >= 0)
                    {
                        node.addAttribute(URI, attributes.getLocalName(i), decodeString(attributes.getValue(i)));
                    }
                }
            }

            Node parent = (Node) nodeStack.peek();

            parent.add(node);
            node.setParent(parent);

            nodeStack.push(node);
        }
        /**
         * Metoda dekoduje string, podobnie do HTML Decode
         * @param tekst
         * @return
         */
        private String decodeString(String tekst)
        {
        	tekst = tekst.replaceAll("%26", "&");
        	return tekst;
        }

        public void endElement(String uri, String localName, String qName) throws SAXException
        {
            if (path.length() > 0)
                path.setLength(path.lastIndexOf("/"));
            level--;
            nodeStack.pop();
        }
    }
}
