package pl.compan.docusafe.web.common.event;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionListenerWrapper.java,v 1.1 2004/05/16 20:10:45 administrator Exp $
 */
class ActionListenerWrapper
{
    private ActionListener listener;
    private String name;

    public ActionListenerWrapper(String name, ActionListener listener)
    {
        if (listener == null)
            throw new NullPointerException("listener");
        this.name = name;
        this.listener = listener;
    }

    public String getName()
    {
        return name;
    }

    public ActionListener getListener()
    {
        return listener;
    }
}
