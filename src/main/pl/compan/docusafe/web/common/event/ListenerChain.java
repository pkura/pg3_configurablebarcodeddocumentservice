package pl.compan.docusafe.web.common.event;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Chain of ActionListener objects which are executed in order
 * they were appended.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ListenerChain.java,v 1.2 2006/02/20 15:42:38 lk Exp $
 */
public class ListenerChain
{
    private List listenerChain = new LinkedList();

    /**
     * Private constructor, creating instances of this class
     * outside EventProcessingAction is prohibited.
     */
    ListenerChain()
    {
    }

    List getChain()
    {
        return listenerChain;
    }

    ActionListener getNamedListener(String name)
    {
        for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();
            if (name.equals(wrapper.getName()))
            {
                return wrapper.getListener();
            }
        }
        return null;
    }

    /**
     * Causes the named ActionListener to be skipped while executing
     * the current listener chain. This method throws IllegalArgumentException
     * if the name of the listener was not registered with the append()
     * method or if the specified listener was already executed.
     * @param name Name of a registered ActionListener. It's the same
     *  name that's passed as the first parameter to
     *  {@link #append(java.lang.String, pl.compan.docusafe.web.common.event.ActionListener)}.
     */
/*
    void skip(String name)
    {
        if (name == null)
            throw new NullPointerException("name");

        boolean listenerFound = false;

        for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();
            if (name.equals(wrapper.getName()))
            {
                if (wrapper.isExecuted())
                    throw new IllegalArgumentException("Listener '"+name+"' was already executed.");
                wrapper.setSkip(true);
                listenerFound = true;
            }
        }

        if (!listenerFound)
            throw new IllegalArgumentException("Listener '"+name+"' not found.");
    }
*/

    /**
     * Causes the specified ActionListener to be skipped while executing
     * the current listener chain. This method throws IllegalArgumentException
     * if the name of the listener was not registered with the append()
     * method or if the specified listener was already executed.
     * @param listener An ActionListener registered into the listener
     *  chain with {@link #append(pl.compan.docusafe.web.common.event.ActionListener))
     *  or {@link #append(java.lang.String, pl.compan.docusafe.web.common.event.ActionListener)}.
     */
/*
    void skip(ActionListener listener)
    {
        if (listener == null)
            throw new NullPointerException("listener");

        boolean listenerFound = false;

        for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();
            if (listener == wrapper.getListener())
            {
                if (wrapper.isExecuted())
                    throw new IllegalArgumentException("Listener "+listener+" was already executed.");
                wrapper.setSkip(true);
                listenerFound = true;
            }
        }

        if (!listenerFound)
            throw new IllegalArgumentException("Listener '"+listener+"' not found.");
    }
*/

    public ListenerChain append(ActionListener actionListener)
    {
        return append(null, actionListener);
    }

    /**
     * Appends an ActionListener to the listener chain. The <code>name</code>
     * may be used to refer to the ActionListener. If the <code>name</code>
     * is not null, it must be unique within the ListenerChain.
     * @param name
     * @param actionListener
     * @throws IllegalArgumentException If the <code>name</code> parameter
     *  is not unique within this ListenerChain.
     * @return The current instance of the ListenerChain (<code>this</code>).
     */
    public ListenerChain append(String name, ActionListener actionListener)
    {
        // checking uniqueness of the event name
        if (name != null)
        {
            for (Iterator iter=listenerChain.iterator(); iter.hasNext(); )
            {
                if (name.equals(((ActionListenerWrapper) iter.next()).getName()))
                    throw new IllegalArgumentException("There's already an event " +
                        "listener named '"+name+"'.");
            }
        }
        ActionListenerWrapper wrapper = new ActionListenerWrapper(name, actionListener);
        listenerChain.add(wrapper);
        return this;
    }
}
