package pl.compan.docusafe.web.common.event;

import pl.compan.docusafe.core.EdmException;

import java.io.IOException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionListener.java,v 1.2 2004/10/06 14:43:58 lk Exp $
 * @deprecated Ta klasa by�a u�ywana w klasach korzystaj�cych ze Struts.
 *  Obecnie u�ycie Struts jest wycofywane na rzecz WebWork, gdzie analogiczn�
 *  funkcj� pe�ni klasa {@link pl.compan.docusafe.webwork.event.ActionListener}.
 */
public interface ActionListener
{
    void actionPerformed(ActionEvent event) throws IOException, EdmException;
}
