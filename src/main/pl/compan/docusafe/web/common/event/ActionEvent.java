package pl.compan.docusafe.web.common.event;

import org.apache.commons.logging.Log;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class represents an event that is created on an incoming
 * request which triggers an ActionListener execution. The event
 * is updated with references to the parameters which are passed
 * as the actual arguments to the {@link org.apache.struts.action.Action#execute}
 * method.
 * <p>
 * Additionally, the event has instance of the {@link Log} class
 * initialized with the name of the current (executing) class, ie.
 * the one inheriting from {@link EventProcessingAction}.
 * <p>
 * The event also has an attribute map which enables user to pass
 * arbitrary objects between the listeners registered for this event.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ActionEvent.java,v 1.3 2004/10/06 14:43:58 lk Exp $
 * @deprecated Ta klasa by�a u�ywana w klasach korzystaj�cych ze Struts.
 *  Obecnie u�ycie Struts jest wycofywane na rzecz WebWork, gdzie analogiczn�
 *  funkcj� pe�ni klasa {@link pl.compan.docusafe.webwork.event.ActionEvent}.
 */
public class ActionEvent
{
    private Log log;
    private ListenerChain listenerChain;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private ActionForm form;
    private ActionMapping mapping;
    private ActionForward forward;
    private Messages errors;
    private Messages messages;
    private Map attributes;
    private boolean cancelled;

    private Set skipListeners;
    private Set executedListeners = new HashSet();

    public ActionEvent(ListenerChain listenerChain)
    {
        this.listenerChain = listenerChain;
    }

    public Log getLog()
    {
        return log;
    }

    void setLog(Log log)
    {
        this.log = log;
    }

    public HttpServletRequest getRequest()
    {
        return request;
    }

    void setRequest(HttpServletRequest request)
    {
        this.request = request;
    }

    public HttpServletResponse getResponse()
    {
        return response;
    }

    void setResponse(HttpServletResponse response)
    {
        this.response = response;
    }

    public ActionForm getForm()
    {
        return form;
    }

    public DynaActionForm getDynaForm()
    {
        return (DynaActionForm) form;
    }

    void setForm(ActionForm form)
    {
        this.form = form;
    }

    public ActionMapping getMapping()
    {
        return mapping;
    }

    void setMapping(ActionMapping mapping)
    {
        this.mapping = mapping;
    }

    public ActionForward getForward()
    {
        return forward;
    }

    public void setForward(ActionForward forward)
    {
        this.forward = forward;
    }

    public Messages getErrors()
    {
        return errors;
    }

    void setErrors(Messages errors)
    {
        this.errors = errors;
    }

    public Messages getMessages()
    {
        return messages;
    }

    void setMessages(Messages messages)
    {
        this.messages = messages;
    }

    /**
     * Sets an attribute. Lifetime of the attribute is bounded
     * by the lifetime of the event, ie. until the request is
     * being processed by this action. The attributes do not survive
     * between requests.
     * @param name Name of the attribute.
     * @param attribute Value of the attribute, may be any object.
     */
    public void setAttribute(String name, Object attribute)
    {
        if (attributes == null)
            attributes = new HashMap();
        attributes.put(name, attribute);
    }

    /**
     * Returns named attribute value or null if the attribute
     * is missing.
     */
    public Object getAttribute(String name)
    {
        return attributes != null ? attributes.get(name) : null;
    }

    /**
     * Stops propagation of the event down the listener chain.
     */
    public void cancel()
    {
        cancelled = true;
    }

    public boolean isCancelled()
    {
        return cancelled;
    }

    public void skip(String name)
    {
        ActionListener listener = listenerChain.getNamedListener(name);
        if (listener == null)
            throw new IllegalArgumentException("No such listener: `"+name+"'.");
        if (executedListeners.contains(listener))
            throw new IllegalArgumentException("Listener `"+name+"' was already " +
                "executed.");
        if (skipListeners == null)
            skipListeners = new HashSet();
        skipListeners.add(listener);
    }

    public void skip(ActionListener listener)
    {
        if (skipListeners == null)
            skipListeners = new HashSet();
        skipListeners.add(listener);
    }

    void markAsExecuted(ActionListener listener)
    {
        executedListeners.add(listener);
    }

    boolean isSkip(ActionListener listener)
    {
        return skipListeners != null && skipListeners.contains(listener);
    }

}
