package pl.compan.docusafe.web.common.event;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SetActionForwardListener.java,v 1.1 2004/05/16 20:10:45 administrator Exp $
 */
public class SetActionForwardListener implements ActionListener
{
    private String forwardName;

    public SetActionForwardListener(String forwardName)
    {
        this.forwardName = forwardName;
    }

    public void actionPerformed(ActionEvent event)
    {
        event.setForward(event.getMapping().findForward(forwardName));
    }
}
