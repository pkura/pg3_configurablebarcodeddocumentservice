package pl.compan.docusafe.web.common.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.*;

import com.opensymphony.xwork.ActionContext;

import pl.compan.docusafe.boot.WatchDog;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Messages;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EventProcessingAction.java,v 1.8 2009/02/28 09:04:39 wkuty Exp $
 */
public abstract class EventProcessingAction extends Action
{
    private static final Log log = LogFactory.getLog(EventProcessingAction.class);
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    private Map registeredListeners = new HashMap();
    /**
     * Default event name which should be used with {@link #registerListener}
     * to register action listener that will be invoked when no other listener
     * is triggered by the request parameters.
     * <p>
     * It has the value <code>pl.compan.docusafe.web.common.event.DEFAULT_ACTION</code>.
     */
    protected static final String DEFAULT_ACTION = EventProcessingAction.class.getName()+".DEFAULT_ACTION";

    private boolean setupDone;

    
    /**
     * Przygotowuje nazwe akcji i login uzytkownika dla obiektu watchdog
     * @param startTime
     * @param finishTime
     */
    protected String prepareWatchDogCode(ActionMapping actionMapping, HttpServletRequest request)
    {       	
    	String login = "anonymous";
    	String name = actionMapping.getName();
    	//te informacje nie zawsze beda potrzebne
  	    		
    	Subject subject = (Subject) request.getSession().getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);    	
    	if (subject!=null)
    	{
    		Set users = subject.getPrincipals(UserPrincipal.class);
    		if (users != null && users.size() > 0)
    		{
    			UserPrincipal principal = (UserPrincipal) users.iterator().next();
    			login = principal.getName();
    		}
    	}    		
    	return name+";"+login;
    }
    
    
    final public ActionForward execute(ActionMapping actionMapping,
                                       ActionForm actionForm,
                                       HttpServletRequest httpServletRequest,
                                       HttpServletResponse httpServletResponse)
        throws Exception
    {
    	
    	pl.compan.docusafe.boot.WatchDog watchDog = new WatchDog(prepareWatchDogCode(actionMapping,httpServletRequest));
    	watchDog.setWebworkAction(false);
        // at least one event listener should be registered to make
        // this class usable
        if (registeredListeners.size() == 0)
            throw new IllegalStateException("No event listeners were registered.");

        ListenerChain listenerChain = null;

        // looking for request parameters triggering on of the listeners
        for (Iterator iter=registeredListeners.keySet().iterator(); iter.hasNext(); )
        {
            String action = (String) iter.next();

            // not expecting request parameter with this name
            if (DEFAULT_ACTION.equals(action))
                continue;

            String value = httpServletRequest.getParameter(action);
            if (value == null)
                value = httpServletRequest.getParameter(action+".x");

            if (value != null && value.toString().trim().length() > 0)
            {
                listenerChain = (ListenerChain) registeredListeners.get(action);
                break;
            }
        }

        // if no appropriate listener was found, falling back for the
        // default listener
        if (listenerChain == null)
        {
            listenerChain = (ListenerChain) registeredListeners.get(DEFAULT_ACTION);
            if (listenerChain == null)
                throw new IllegalArgumentException("No default event handler was " +
                    "specified.");
        }

        Messages errors = new Messages();
        Messages messages = new Messages();

        // preparing the event object

        List chain = listenerChain.getChain();

        ActionEvent event = new ActionEvent(listenerChain);
        event.setForm(actionForm);
        event.setLog(LogFactory.getLog(getClass()));
        event.setMapping(actionMapping);
        event.setRequest(httpServletRequest);
        event.setResponse(httpServletResponse);
        event.setErrors(errors);
        event.setMessages(messages);

        // executing the listeners in chain
        // if a listener throws an exception, the message is added to
        // the errors collection but the exception is not rethrown
        for (Iterator iter=chain.iterator(); iter.hasNext(); )
        {
            ActionListenerWrapper wrapper = (ActionListenerWrapper) iter.next();

            if (event.isSkip(wrapper.getListener()))
            {
                if (log.isDebugEnabled())
                    log.debug("skipping "+wrapper.getListener()+" ("+wrapper.getName()+")");
                continue;
            }

            try
            { 
                wrapper.getListener().actionPerformed(event);
                XesLog.doXesLogEventCommon(wrapper.getListener(), this, event);
                event.markAsExecuted(wrapper.getListener());
            }
            catch (Throwable t)
            {
                log.error(t.getMessage(), t);
                errors.add(sm.getString("eventProcessing.actionException",
                    t.toString()));
                if (event.getForward() == null)
                {
                    event.getRequest().setAttribute("exception", t);
                    event.setForward(event.getMapping().findForward("exception"));
                }
                break; // for
            }

            if (event.isCancelled())
                break; // for
        }

        if (errors.size() > 0)
            httpServletRequest.setAttribute(Messages.ERRORS_KEY, errors);

        if (messages.size() > 0)
            httpServletRequest.setAttribute(Messages.MESSAGES_KEY, messages);

        watchDog.tick();
        
        return event.getForward();
        
        
    }

    final public void setServlet(ActionServlet actionServlet)
    {
        super.setServlet(actionServlet);
        synchronized (this)
        {
            if (!setupDone)
            {
                registeredListeners.clear(); // hack??
                setup();
                setupDone = true;
            }
        }
    }

    /**
     * Funkcja wywo�ywana w momencie tworzenia instancji klasy
     * dziedzicz�cej po EventProcessingAction. W tej funkcji
     * nale�y zarejestrowa� procedury obs�ugi wydarze�
     */
    protected abstract void setup();

    /**
     * Registers event listener for the specified event name and
     * returns an empty listener chain.
     * @param action Event name (request parameter name which will
     *  trigger execution of the listener chain).
     * @return Empty listener chain.
     */
    protected ListenerChain registerListener(String action)
    {
        if (registeredListeners.get(action) != null)
            throw new IllegalArgumentException("W registeredListeners znajduje" +
                " si� ju� funkcja obs�ugi "+action+", registeredListeners="+
                registeredListeners);

        ListenerChain chain = new ListenerChain();
        registeredListeners.put(action, chain);

        return chain;
    }

    protected void registerListener(String action, ActionListener listener)
    {
        if (registeredListeners.get(action) != null)
            throw new IllegalArgumentException("W registeredListeners znajduje" +
                " si� ju� funkcja obs�ugi "+action+", registeredListeners="+
                registeredListeners);

        ListenerChain chain = new ListenerChain();
        chain.append(listener);
        registeredListeners.put(action, chain);
    }

    public static String getText(String key)
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString(key);
    }
    
    /**
     * Given an Action-inheriting class and a module name this
     * method returns path for this action without the context
     * path prefix.
     * @param clazz An Action class.
     * @param moduleName Struts' module name, may be null in which
     *  case the default module is used.
     * @return Path without the context path prefix or null if no such
     *  action mapping is found.
     */
    /*
    protected static String getMyPath(Class clazz, String moduleName)
    {
        // module's configuration
        ModuleConfig moduleConfig = (ModuleConfig) Configuration.getServletContext().
            getAttribute(Globals.MODULE_KEY +
            (moduleName != null ? "/" + moduleName : ""));

        // I'm going through the mappings for this module to find
        // this's mapping
        ActionConfig[] actionConfigs = moduleConfig.findActionConfigs();
        for (int i=0; actionConfigs != null && i < actionConfigs.length; i++)
        {
            if (actionConfigs[i].getTypeName().equals(clazz.getName()))
            {
                return (moduleName != null ? "/" + moduleName : "") +
                    actionConfigs[i].getPath() +
                    // TODO: nie zawsze ".do", mo�e by� inne rozszerzenie
                    ".do";
            }
        }

        return null;
    }
    */
}
