package pl.compan.docusafe.web.common;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Filtr dodaj�cy do wyniku zapytania nag��wki zabraniaj�ce przegl�darce
 * umieszczania strony w cache.
 * <p>
 * TODO: pomija� urle, kt�re generuj� pliki pdf.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NoCacheFilter.java,v 1.5 2008/10/06 10:48:50 pecet4 Exp $
 */
public class NoCacheFilter implements Filter
{
    private String[] excludes;

    public void init(FilterConfig filterConfig) throws ServletException
    {
        String excludeUris = filterConfig.getInitParameter("exclude-uris");
        if (excludeUris != null)
        {
            excludes = excludeUris.split("\\s*,\\s*");
            Arrays.sort(excludes);
        }
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String requestURI = request.getRequestURI();
        String uri = requestURI.substring(request.getContextPath().length(), requestURI.length());

        // TODO: inne ustawienia dla obrazk�w
        // je�eli bie��cy adres nie jest na li�cie wyj�tk�w, dodaj� nag��wki
        if (uri == null || Arrays.binarySearch(excludes, uri) < 0)
        {
            ((HttpServletResponse) servletResponse).addHeader("Pragma", "no-cache");
            ((HttpServletResponse) servletResponse).addHeader("Cache-Control", "no-cache");
            ((HttpServletResponse) servletResponse).addHeader("Expires", "0");
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy()
    {
    }
}
