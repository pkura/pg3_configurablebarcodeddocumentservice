package pl.compan.docusafe.web.common;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.upload.Upload;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

/* User: Administrator, Date: 2005-10-05 16:46:45 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: AcceptUploadAction.java,v 1.12 2009/02/28 08:03:43 wkuty Exp $
 */
public class AcceptUploadAction extends EventActionSupport
{
    private FormFile file;
    private String multiToken;
    private String targetFilename;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUploadToFile").
            append(OpenHibernateSession.INSTANCE).
            append(new UploadToFile()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {    
            if (event.getLog().isDebugEnabled())
                event.getLog().debug("file="+file);

            if (file == null)
                return;

            try
            {
                Upload upload = (Upload) ServiceManager.getService(Upload.NAME);

                if (multiToken != null)
                {
                    upload.retain(multiToken, file.getFile(), file.getName());
                }
                else
                {
                    String token = upload.retain(file.getFile(), file.getName());
                    ServletActionContext.getResponse().setHeader("X-Docusafe-File-Token", token);
                }
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
/*
            catch (IOException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
*/
        }
    }

    private class UploadToFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            File outFile = new File(targetFilename);

            try 
            {
                FileInputStream is = new FileInputStream(file.getFile());
                FileOutputStream os = new FileOutputStream(outFile);
                org.apache.commons.io.IOUtils.copy(is, os);                
                org.apache.commons.io.IOUtils.closeQuietly(is);
                org.apache.commons.io.IOUtils.closeQuietly(os);
            }
            catch (IOException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
        }
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public void setMultiToken(String multiToken)
    {
        this.multiToken = multiToken;
    }

    public void setTargetFilename(String targetFilename)
    {
        this.targetFilename = targetFilename;
    }
}
