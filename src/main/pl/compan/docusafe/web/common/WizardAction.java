package pl.compan.docusafe.web.common;

import org.apache.struts.action.Action;

/**
 * Akcja obs�uguj�ca formularze typu "wizard" prowadz�ce
 * u�ytkownika poprzez zestaw ekran�w. Ekrany mog� by�
 * wy�wietlane wielokrotnie, niekt�re mog� by� pomijane
 * na podstawie wynik�w interakcji z u�ytkownikiem
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WizardAction.java,v 1.1 2004/05/16 20:10:46 administrator Exp $
 */
public class WizardAction extends Action
{
}
