package pl.compan.docusafe.web.common;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.io.StreamTokenizer;
import java.io.StringReader;
/* User: Administrator, Date: 2006-02-08 13:09:21 */

/**
 * Mapa, kt�rej warto�ci pobierane s� z przekazanego beana (grafu bean�w).
 * Nazwy w�asno�ci pobieranych z beana powinny zosta� przekazane w konstruktorze.
 * <p>
 * Jest to klasa pomocnicza przeznaczona do warstwy prezentacji.  Umo�liwia
 * odczytywanie w stronie JSP w�asno�ci bez konieczno�ci odwo�ywania si�
 * do bazy danych, co cz�sto jest konieczne przy odczytywaniu w�asno�ci obiekt�w
 * zarz�dzanych przez Hibernate.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BeanBackedMap.java,v 1.5 2006/03/14 14:21:40 lk Exp $
 */
public class BeanBackedMap extends HashMap<String, Object>
{
    private static final Log log = LogFactory.getLog(BeanBackedMap.class);

    private static final Object DELEGATED = new String("DELEGATED");
    private static final Object NULL = new String("NULL");
    private static final Object NONEXISTENT = new String("NONEXISTENT");
    private Object bean;

    /**
     *
     * @param bean Klasa, z kt�rej pobrane zostan� warto�ci.
     * @param lazy Je�eli true, warto�ci pobrane zostan� z obiektu bean
     * dopiero w momencie, gdy zostan� za��dane przy pomocy metody get().
     * @param delegated Lista nazw w�asno�ci, kt�re powinny by� pobierane
     * z obiektu bean po pr�bie odczytania ich metod� get().
     */
    public BeanBackedMap(Object bean, boolean lazy, String... delegated)
    {
        this.bean = bean;
        for (String property : delegated)
        {
            if (lazy)
            {
                put(property, DELEGATED);
            }
            else
            {
                put(property, getPropertyValue(bean, property));
            }
        }
    }

    /**
     * Obiekt pobieraj�cy warto�ci atrybut�w natychmiast (lazy=false).
     * @see BeanBackedMap(Object,  boolean, String...)
     */
    public BeanBackedMap(Object bean, String... delegated)
    {
        this(bean, false, delegated);
    }

    @Override public Object get(Object key)
    {
        if (!(key instanceof String))
            throw new IllegalArgumentException("Spodziewano si� argumentu typu String");

        Object value = super.get((String) key);
        if (value == DELEGATED)
        {
            value = getPropertyValue(bean, (String) key);
            if (value == null)
                value = NULL;
            put((String) key, value);
        }

        if (value == NULL || value == NONEXISTENT)
            return null;

        return value;
    }

    @Override public boolean containsKey(Object key)
    {
        return get(key) != null;
    }

    /**
     * Pobiera atrybut przekazanego obiektu, przechodz�c do w�asno�ci
     * zagnie�d�onych, je�eli w parametrze property znajduj� si� kropki.
     * Nie obs�uguje w�asno�ci indeksowanych.
     */
    private static Object getPropertyValue(Object bean, String property)
    {
        Object contextBean = bean;
        StringTokenizer st = new StringTokenizer(property, ".");
        while (st.hasMoreTokens())
        {
            contextBean = getBeanProperty(contextBean, st.nextToken());
        }
        return contextBean;
    }

    /**
     * Pobiera bezpo�redni atrybut przekazanego obiektu.
     */
    private static Object getBeanProperty(Object bean, String property)
    {
        try
        {
            Method getter = bean.getClass().getMethod("get"+ StringUtils.capitalise(property));
            Object result = getter.invoke(bean);
            if (result == null)
                result = NULL;
            return result;
        }
        catch (NoSuchMethodException e)
        {
            try
            {
                Method getter = bean.getClass().getMethod("is"+ StringUtils.capitalise(property));
                Object result = getter.invoke(bean);
                if (result == null)
                    result = NULL;
                return result;
            }
            catch (NoSuchMethodException e1)
            {
                log.warn(e1.toString());
                return NONEXISTENT;
            }
            catch (Exception e1)
            {
                log.warn(e1.toString());
                return NULL;
            }
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
            return NULL;
        }
    }
}
