package pl.compan.docusafe.web.common;

import javax.swing.text.html.HTML.Tag;

import pl.compan.docusafe.util.HtmlImage;

/**
 * Klasa u�atwiaj�ca generowanie html'a w kodzie
 * @author MSankowski 
 */
public class HtmlUtils {

	private final static String ANCHOR_FORMAT_SIMPLE = "<a href=\"%s\">%s</a>";
	private final static String ANCHOR_FORMAT_SIMPLE_WITH_NAME = "<a name=\"%s\" href=\"%s\">%s</a>";
	private final static String ANCHOR_FORMAT_CLASS = "<a href=\"%s\" class=\"%s\">%s</a>";
	private final static String ANCHOR_FORMAT_CLASS_WITH_NAME = "<a name=\"%s\" href=\"%s\" class=\"%s\">%s</a>";
	private final static String CHECKBOX_FORMAT_SIMPLE = "<input name=\"%s\" value=\"%s\" type=\"checkbox\" %s />";
	private final static String RADIOBUTTON_FORMAT_SIMPLE = "<input name=\"%s\" value=\"%s\" type=\"radio\" %s />";
	private final static String IMG_FORMAT_SIMPLE = "<img src=\"%s\" alt=\"%s\" title=\"%s\"/>";
	private final static String IMG_FORMAT_SIMPLE_WITH_CLASS = "<img src=\"%s\" alt=\"%s\" title=\"%s\" class=\"%s\"/>";
	private final static String SPAN_FORMAT_SIMPLE ="<span>%s</span>";
	private final static String SPAN_FORMAT_CLASS ="<span class=\"%s\">%s</span>";


	public static String toHtmlAnchor(String href, String content){
		return String.format(ANCHOR_FORMAT_SIMPLE, href.replace('"', '\''), content);
	}

	public static String toHtmlAnchor(String href, String content, String klass){
		return String.format(ANCHOR_FORMAT_CLASS, href.replace('"', '\''), klass, content);
	}

	public static String toHtmlAnchorWithName(String name, String href, String content){
		return String.format(ANCHOR_FORMAT_SIMPLE_WITH_NAME, name, href.replace('"', '\''), content);
	}

	public static String toHtmlAnchorWithName(String name, String href, String content, String klass){
		return String.format(ANCHOR_FORMAT_CLASS_WITH_NAME, name, href.replace('"', '\''), klass, content);
	}

	public static String toCheckbox(String name, String value){
		return String.format(CHECKBOX_FORMAT_SIMPLE, name, value, "");
	}
	
	public static String toCheckbox(String name, String value, boolean isChecked, boolean isDisabled) {
		StringBuilder params = new StringBuilder();
		params.append((isChecked) ? " checked=\"1\"" : "");
		params.append((isDisabled) ? " disabled=\"1\"" : "");
		return String.format(CHECKBOX_FORMAT_SIMPLE, name, value, params.toString());
	}
	
	public static String toRadiobutton(String name, String value, boolean isChecked, boolean isDisabled) {
		StringBuilder params = new StringBuilder();
		params.append((isChecked) ? "checked=\"1\"" : "");
		params.append((isDisabled) ? " disabled=\"1\"" : "");
		return String.format(RADIOBUTTON_FORMAT_SIMPLE, name, value, params.toString());
	}

	public static String toImg(HtmlImage image, String alt, String title){
		return String.format(IMG_FORMAT_SIMPLE, image.getSrc(), alt, title);
	}

	public static String toImg(String src, String alt, String title){
		return String.format(IMG_FORMAT_SIMPLE, src, alt, title);
	}
	
	public static String toImgWithClass(HtmlImage image, String alt, String title, String imgClass){
		return String.format(IMG_FORMAT_SIMPLE_WITH_CLASS, image.getSrc(), alt, title, imgClass);
	}
	
	public static String toImgWithClass(String src, String alt, String title, String imgClass){
		return String.format(IMG_FORMAT_SIMPLE_WITH_CLASS, src, alt, title, imgClass);
	}

	public static String toSpan(String contents){
		return String.format(SPAN_FORMAT_SIMPLE, contents);
	}

	public static String toSpan(String contents, String klass){
		return String.format(SPAN_FORMAT_CLASS, klass, contents);
	}
	
	/**
	 * Opakowuje napis w tag html:<br/>
	 * np.: dla tagu <code>pre</code> zamienia napis <code>html</code> na <code>&lt;pre&gt;html&lt;/pre&gt;</code>
	 * @param str Napis do opakowania
	 * @param tag Tag html (mo�e by� <code>null</code>, wtedy zwraca <code>str</code>)
	 * @return opakowany napis
	 */
	public static String wrap(String str, Tag tag) {
		if(tag == null) {
			return str;
		}
		StringBuilder sb = new StringBuilder("<");
		sb.append(tag.toString()).append(">");
		sb.append(str);
		sb.append("</").append(tag.toString()).append(">");
		
		return sb.toString();
	}

}
