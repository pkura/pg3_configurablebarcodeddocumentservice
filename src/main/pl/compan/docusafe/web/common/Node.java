package pl.compan.docusafe.web.common;

import java.io.Serializable;
import java.util.*;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;

import org.apache.commons.logging.LogFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.LocaleUtil;
import pl.compan.docusafe.util.StringManager;

/* User: Administrator, Date: 2005-07-05 16:41:34 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Node.java,v 1.15 2010/07/01 13:26:44 mariuszk Exp $
 */
public class Node implements Serializable
{
    /**
     * �cie�ka uri prowadz�ca do tego elementu, dla elementu g��wnego
     * jest to "", dla pozosta�ych element�w �cie�ka zaczyna si� od
     * znaku '/'.
     */
    private String path;
    /**
     * Odno�nik (url) powi�zany z tym elementem.
     */
    private String href;
    /**
     * Etykieta (opis) elementu.
     */
    private String label;
    /**
     * Poziom zagnie�d�enia elementu w drzewie.  Element g��wny (nie posiadaj�cy
     * rodzica) znajduje si� na poziomie 0.
     */
    private int level;

    /**
     * Warto�� href bez znaku '?' i wszystkiego, co znajduje si� po tym
     * znaku.  Je�eli w href nie wyst�puje '?', pole ma warto�� null.
     */
    private String hrefWithoutQuery;
    /**
     * Kolekcja element�w potomnych opakowana w Collections.unmodifiableList().
     */
    private List unmodifiableChildren;
    /**
     * Kolekcja element�w potomnych (null, je�eli element nie ma element�w potomnych).
     */
    private List children;
    /**
     * Element nadrz�dny.
     */
    private Node parent;
    /**
     * Okresla czy dla tego poddrzewa w tym w�le menu jest generowane dynamicznie - czyli czy trzeba
     * osobno je obs�u�y�. Gdy warto�� tego pola r�na null - oznacza identyfikator dynamicznego menu,
     * wpp. oznacza, �e ten w�ze� jest standardowy.     
     */
    private String dynamicMenu;
    /**
     * Dodatkowy parametr dla dynamicznych menu - jego interpretacja zalezy od danego dynamicznego menu.
     */
    private String param;
    private Locale locale;
    private StringManager sm;
    
    private String[] uris;
    private Map[] attributes;
    /**
     * Id wykorzystywane w html czasem jest potrzebne 
     */
    private String htmlId;

    Node(String path, String href, String label, int level, String dynamicMenu, String param)
    {
        this.path = path;
        this.href = href;
        this.label = label;
        this.level = level;
        this.dynamicMenu = dynamicMenu;
        this.param = param;

        int q;
        if (href != null && (q=href.indexOf('?')) >= 0)
            hrefWithoutQuery = href.substring(0, q);
    }

    Node()
    {
        this("", null, null, 0, null, null);
    }

    /**
     * Dodaje element potomny; kolejno�� wstawiania element�w potomnych
     * jest zapami�tywana.
     */
    void add(Node child)
    {
        if (children == null) children = new LinkedList();
        children.add(child);
        unmodifiableChildren = null;
    }

    void setParent(Node parent)
    {
        this.parent = parent;
    }

    /**
     * Zwraca list� element�w potomnych; je�eli element nie ma element�w
     * potomnych, zwracana lista jest pusta (nie null).
     * @return Niemodyfikowalna lista element�w potomnych.
     */
    public List getChildren()
    {
        if (children == null)
        {
            return Collections.EMPTY_LIST;
        }
        else
        {
            if (unmodifiableChildren == null)
                unmodifiableChildren = Collections.unmodifiableList(children);
            return unmodifiableChildren;
        }
    }

    public int getLevel()
    {
        return level;
    }

    public String getLabel()
    {
        return GlobalPreferences.loadPropertiesFile("","FreeMap").getString(label);
    }    
    
    public String getHref()
    {
        return href;
    }

    public Node getParent()
    {
        return parent;
    }

    public String getDynamicMenu()
    {
        return dynamicMenu;
    }

    public String getParam()
    {
        return param;
    }

    public boolean isRoot()
    {
        return parent == null;
    }

    /**
     * Zwraca warto�� atrybutu zwi�zanego z tym elementem drzewa
     * i znajduj�cego si� we wskazanej przestrzeni nazw.
     */
    public String getAttribute(String uri, String name)
    {
        if (uris == null)
            return null;

        if (uri == null)
            throw new NullPointerException("uri");
        if (name == null)
            throw new NullPointerException("name");

        for (int i=0; i < uris.length; i++)
        {
            if (uris[i].equals(uri))
                return (String) attributes[i].get(name);
        }

        return null;
    }

    void addAttribute(String uri, String name, String value)
    {
        if (uri == null)
            throw new NullPointerException("uri");
        if (name == null)
            throw new NullPointerException("name");
        if (value == null)
            throw new NullPointerException("value");

        if (uris == null)
        {
            uris = new String[] { uri };
            attributes = new Map[] { new HashMap() };
            attributes[0].put(name, value);
        }
        else
        {
            for (int i=0; i < uris.length; i++)
            {
                if (uris[i].equals(uri))
                {
                    attributes[i].put(name, value);
                    return;
                }
            }

            String[] _uris = uris;
            uris = new String[uris.length+1];
            System.arraycopy(_uris, 0, uris, 0, _uris.length);
            uris[uris.length-1] = uri;

            Map[] _attributes = new Map[attributes.length+1];
            attributes = new Map[attributes.length+1];
            System.arraycopy(_attributes, 0, attributes, 0, _attributes.length);
            attributes[attributes.length-1] = new HashMap();

            attributes[attributes.length-1].put(name, value);
        }
    }

    /**
     * Poszukuje elementu o wskazanej warto�ci atrybutu href poczynaj�c
     * od element�w, kt�re znajduj� si� najg��biej w drzewie.  Je�eli warto��
     * atrybutu href zawiera znak '?' (i ewentualne parametry po tym znaku),
     * por�wnywana jest r�wnie� warto�� atrybutu href bez znaku '?' i znajduj�cych
     * si� po nim parametr�w.
     * @param href
     * @return Znaleziony element Node lub null.
     */
    public Node findByHref(String href)
    {
        if (href == null)
            throw new NullPointerException("href");

        if (children != null)
        {
            for (Iterator iter=children.iterator(); iter.hasNext(); )
            {
                Node node = ((Node) iter.next()).findByHref(href);
                if (node != null)
                    return node;
            }
        }

//        if ((this.href != null && href.equals(this.href)) ||
//            (this.hrefWithoutQuery != null && href.equals(this.hrefWithoutQuery)))
//            return this;

        // tylko szukanie dok�adne
        if (this.href != null && href.equals(this.href))
            return this;

        return null;
    }

    /**
     * Pobiera list� element�w znajduj�cych si� na tym samym poziomie,
     * co element bie��cy (wraz z elementem bie��cym).
     * @return Lista element�w Node (niemodyfikowalna).
     */
    public List getSiblings()
    {
        if (level == 0)
            return Arrays.asList(new Node[] { this });
        return parent.getChildren();
    }

    /**
     * Znajduje rodzica tego elementu znajduj�cego si� na ��danym poziomie
     * (element g��wny znajduje si� na poziomie 0).
     * @param level Poziom, na kt�rym znajduje si� rodzic; nie mo�e by� mniejszy
     * od zera lub wi�kszy od poziomu bie��cego elementu.
     * @return
     */
    public Node findAncestorAt(int level)
    {
        if (level > this.level)
            throw new IllegalArgumentException("Poziom, z kt�rego maj� by� pobrane" +
                " elementy r�wnoleg�e nie mo�e by� wy�szy od poziomu bie��cego elementu");
        if (level < 0)
            throw new IllegalArgumentException("level="+level+" < 0");

        if (level == this.level)
            return this;

        // poszukiwanie w�r�d rodzic�w elementu na ��danym poziomie
        // return parent.findAncestorAt(level)
        Node up = parent;
        while (up != null)
        {
            if (up.level == level)
                return up;
            up = up.parent;
        }

        throw new Error("To si� absolutnie nie powinno wydarzy� - " +
            "b��d tworzenia drzewa Sitemap, this="+this+" parent="+parent+
            " level="+level);
    }

    /**
     * Zwraca true, je�eli bie��cy element znajduje si� na �cie�ce prowadz�cej
     * do przekazanego elementu.
     */
    public boolean isInPath(Node node)
    {
        // tak by�oby wygodniej, ale �cie�ki mog� si� powtarza�
        //return node.path.startsWith(this.path);

        if (node == null)
            throw new NullPointerException("node");

        return node.path.startsWith(this.path);
        
        /*if (this.equals(node))
            return true;

        Node up = node.parent;
        while (up != null)
        {
            if (this.equals(up))
                return true;
            up = up.parent;
        }

        return false;*/
    }

    /* *
     * Zwraca true, je�eli bie��cy element znajduje si� na �cie�ce prowadz�cej
     * do elementu zwi�zanego z bie��cym w�tkiem metod� {@link Freemap#bind(String, String)}.
     */
/*
    public boolean isInPath()
    {
        Node boundNode = Freemap.getBoundNode();
        if (boundNode == null)
            return false;
        return isInPath(boundNode);
    }
*/

    public String toString()
    {
        StringBuilder toString = new StringBuilder("Node[path="+path+" href="+href+
            " label="+label+" level="+level+"]");
/*
        if (children != null)
        {
            toString.append("\n");
            for (Iterator iter=children.iterator(); iter.hasNext(); )
            {
                for (int i=0; i < level; i++) toString.append("  ");
                toString.append(iter.next());
            }
        }
*/
        return toString.toString();
    }
    
    public List availableNodes(HttpServletRequest request)
    {
        Subject subject = (Subject)
        	request.getSession(true).getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
    	List nodes = new ArrayList<Node>(getChildren());
    	try 
    	{
			checkPermission(nodes,request,subject);
		} 
    	catch (JspException e) 
    	{
			e.printStackTrace();
		}
    	return nodes;
    }
    
    public List availableNodes()
    {
    	List nodes = new ArrayList<Node>(getChildren());
    	return nodes;
    }
    
    public static void checkPermission(List nodes, HttpServletRequest request,Subject subject) throws JspException
    {
        for (Iterator iter=nodes.iterator(); iter.hasNext(); )
        {
            Node menuNode = (Node) iter.next();

            // TODO: traktowa� jako wyra�enie OGNL (FeaturesTag), a nie pojedyncz� nazw� roli
            String roles;
            if ((roles=menuNode.getAttribute(Sitemap.FREEMAP_URI, "roles")) != null)
            {
                // je�eli warto�� atrybutu zaczyna si� od nawiasu klamrowego, jest
                // to wyra�enie testuj�ce obecno�� lub nieobecno�� r�l
                if (roles.charAt(0) == '{')
                {   /*
                    if (roles.length() < 3 || roles.charAt(roles.length()-1) != '}')
                        throw new JspException("Niepoprawna sk�adnia atrybutu roles w freemap: "+roles);

                    Map rolesMap = new HashMap();
                    String[] availableRoles = { "admin", "data_admin" };
                    for (int i=0; i < availableRoles.length; i++)
                    {
                        rolesMap.put(availableRoles[i], Boolean.TRUE);
                    }

                    try
                    {
                        Boolean result = (Boolean) Ognl.getValue(
                            Ognl.parseExpression(roles.substring(1, roles.length()-1)), new OgnlContext(),
                            rolesMap,
                            Boolean.class);
                        if (result == null || !result.booleanValue())
                            iter.remove();
                    }
                    catch (OgnlException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }
                    */
                    throw new UnsupportedOperationException("Brak wsparcia dla wyra�e� okre�laj�cych role");
                }
                else
                {
                    // niezalogowany u�ytkownik na pewno nie ma r�l
                    if (subject == null)
                    {
                        iter.remove();
                        continue;
                    }
                    // lista r�l rozdzielonych przecinkami - u�ytkownik musi mie� wszystkie z nich
                    else
                    {
                      /*  String[] rAND = roles.split(",");
                        String[] rOR = roles.split(";");
                        if (rAND.length > 1 && rOR.length > 1)
                            throw new JspException("Niepoprawna sk�adnia wyra�enia atrybutu roles: nie mo�na jednocze�nie u�ywa� ',' oraz ';'");
                        boolean has;
                        if (rOR.length > 1)
                        {
                            has = false;
                            for (int i=0; i < rOR.length; i++)
                            {
                                if (AuthUtil.hasRole(subject, rOR[i].trim()))
                                {
                                    has = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            has = true;
                            for (int i=0; i < rAND.length; i++)
                            {
                                if (!AuthUtil.hasRole(subject, rAND[i].trim()))
                                {
                                    has = false;
                                    break;
                                }
                            }
                        } */
                        String[] r = roles.split(",");
                        boolean has = true;
                        for (int i=0; i < r.length; i++)
                        {   
                        	
                        	if(r[i].equals("data_admin") &&  !AuthUtil.hasRole(subject, r[i]))
                        	{
                        		//ten trim cos prsuje , bez niego wszystko dziala
                        		has = false;
                        		break;
                        	}
                        	
                            if (!AuthUtil.hasRole(subject, r[i].trim()))
                        	//if (!AuthUtil.hasRole(subject, r[i]))
                            {
                                has = false;
                                break;
                            }
                        }
                        if (!has)
                        {
                            iter.remove();
                            continue;
                        }
                    }
                }
            }

            if (Boolean.valueOf(menuNode.getAttribute(Sitemap.FREEMAP_URI, "hidden")).booleanValue())
            {
                iter.remove();
                continue;
            }

            // modu�y
            String modules;
            if ((modules=menuNode.getAttribute(Sitemap.FREEMAP_URI, "modules")) != null)
            {
                if (modules.charAt(0) == '{')
                {
                    if (modules.length() < 3 || modules.charAt(modules.length()-1) != '}')
                        throw new JspException("Niepoprawna sk�adnia wyra�enia atrybutu modules, brak zamykaj�cego" +
                            " nawiasu klamrowego: '"+modules+"'");

                    Map modulesMap = new HashMap();
                    String[] availableModules = Docusafe.getAvailableModules();
                    for (int i=0; i < availableModules.length; i++)
                    {
                        modulesMap.put(availableModules[i], Boolean.TRUE);
                    }

                    try
                    {
                        Boolean result = (Boolean) Ognl.getValue(
                            Ognl.parseExpression(modules.substring(1, modules.length()-1)), new OgnlContext(),
                            modulesMap,
                            Boolean.class);
                        if (result == null || !result.booleanValue())
                        {
                            iter.remove();
                            continue;
                        }
                    }
                    catch (OgnlException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }
                }
                else
                {
                    String[] m = modules.split(",");
                    boolean has = true;
                    for (int i=0; i < m.length; i++)
                    {
                        if (!Configuration.moduleAvailable(m[i].trim()))
                        {
                            has = false;
                            break;
                        }
                    }
                    if (!has)
                    {
                        iter.remove();
                        continue;
                    }
                }
            }

            // uprawnienia kancelaryjne
            String permissions; 
            if ((permissions=menuNode.getAttribute(Sitemap.FREEMAP_URI, "permissions")) != null )
            {
                UserPrincipal principal = AuthUtil.getUserPrincipal(request);

                // brak u�ytkownika - na pewno nie ma uprawnienia
                if (principal == null)
                {
                    iter.remove();
                    continue;
                }
                else if (permissions.charAt(0) == '{')
                {
                    if (permissions.length() < 3 || permissions.charAt(permissions.length()-1) != '}')
                        throw new JspException("Niepoprawna sk�adnia atrybutu extras w freemap: "+permissions);

                    Map permissionsMap = new HashMap();
                    List<DSPermission> permList = DSPermission.getAvailablePermissions();
                    try
                	{
                    	DSApi.open(AuthUtil.getSubject(request));
	                    for (DSPermission permission : permList)
	                    {
							permissionsMap.put(permission.getName(), DSApi.context().hasPermission(permission));
							
						}
                    }
                	catch (EdmException e)
                	{
                		LogFactory.getLog("eprint").error("", e);
					}
                	finally
                	{
                		try {DSApi.close();}catch (EdmException e) {LogFactory.getLog("eprint").debug("", e);}
                	}

                    try
                    {
                        Boolean result = (Boolean) Ognl.getValue(
                            Ognl.parseExpression(permissions.substring(1, permissions.length()-1)), new OgnlContext(),
                            permissionsMap,
                            Boolean.class);
                        if (result == null || !result.booleanValue())
                        {
                            iter.remove();
                            continue;
                        }
                    }
                    catch (OgnlException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }
                }
                else
                {
                    PermissionCache cache;
                    try
                    {
                        cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                    }
                    catch (ServiceException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }

                    String[] p = permissions.split(",");
                    boolean has = true;
                    for (int i=0; i < p.length; i++)
                    {
                        String perm = p[i].trim();
                        if (DSPermission.getByName(perm) == null)
                            throw new JspException("Nie istnieje uprawnienie o nazwie "+permissions);
                        if (!cache.hasPermission(principal.getName(), perm))
                        {
                            has = false;
                            break;
                        }
                    }

                    if (!has)
                    {
                        iter.remove();
                        continue;
                    }
                }
            }

            // dodatkowe w�a�ciwo�ci licencji
            String extras;
            if ((extras=menuNode.getAttribute(Sitemap.FREEMAP_URI, "extras")) != null)
            {
                if (extras.charAt(0) == '{')
                {
                    if (extras.length() < 3 || extras.charAt(extras.length()-1) != '}')
                        throw new JspException("Niepoprawna sk�adnia atrybutu extras w freemap: "+extras);

                    Map extrasMap = new HashMap();
                    String[] availableExtras = Docusafe.getAvailableExtras();
                    for (int i=0; i < availableExtras.length; i++)
                    {
                        extrasMap.put(availableExtras[i], Boolean.TRUE);
                    }

                    try
                    {
                        Boolean result = (Boolean) Ognl.getValue(
                            Ognl.parseExpression(extras.substring(1, extras.length()-1)), new OgnlContext(),
                            extrasMap,
                            Boolean.class);
                        if (result == null || !result.booleanValue())
                        {
                            iter.remove();
                            continue;
                        }
                    }
                    catch (OgnlException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }
                }
                else
                {
                    String[] e = extras.split(",");
                    boolean has = true;
                    for (int i=0; i < e.length; i++)
                    {
                        String extra = e[i].trim();
                        if (!Configuration.hasExtra(extra))
                        {
                            has = false;
                            break;
                        }
                    }

                    if (!has)
                    {
                        iter.remove();
                        continue;
                    }
                }
            }

            // dodatkowe funkcjonalno�ci (adds.properties)
            String additions;
            if ((additions=menuNode.getAttribute(Sitemap.FREEMAP_URI, "additions")) != null)
            {
                if (additions.charAt(0) == '{')
                {
                    if (additions.length() < 3 || additions.charAt(additions.length()-1) != '}')
                        throw new JspException("Niepoprawna sk�adnia atrybutu additions w freemap: "+additions);

                    Map additionsMap = new HashMap();
                    String[] availableAdditions = Configuration.getAvailableAdditions();
                    for (int i=0; i < availableAdditions.length; i++)
                    {
                        additionsMap.put(availableAdditions[i], Boolean.TRUE);
                    }

                    try
                    {
                        Boolean result = (Boolean) Ognl.getValue(
                            Ognl.parseExpression(additions.substring(1, additions.length()-1)), new OgnlContext(),
                            additionsMap,
                            Boolean.class);
                        if (result == null || !result.booleanValue())
                        {
                            iter.remove();
                            continue;
                        }
                    }
                    catch (OgnlException e)
                    {
                        throw new JspException(e.getMessage(), e);
                    }
                }
                else
                {
                    String[] e = additions.split(",");
                    boolean has = true;
                    for (int i=0; i < e.length; i++)
                    {
                        String addition = e[i].trim();
                        if (!Configuration.additionAvailable(addition))
                        {
                            has = false;
                            break;
                        }
                    }

                    if (!has)
                    {
                        iter.remove();
                        continue;
                    }
                }
            }
            
            
            String available;
            if ((available=menuNode.getAttribute(Sitemap.FREEMAP_URI, "available")) != null)
            {
                String[] e = available.split(",");
                boolean has = true;
                for (int i=0; i < e.length; i++)
                {
                    String addition = e[i].trim();
                    if (!AvailabilityManager.isAvailable(addition))
                    {
                        has = false;
                        break;
                    }
                }
                if (!has)
                {
                    iter.remove();
                    continue;
                }                
            }
            
            
            // rodzaje dokument�w (dockinds)
            String dockinds;
            if ((dockinds=menuNode.getAttribute(Sitemap.FREEMAP_URI, "dockinds")) != null)
            {
                try
                {
                    DSApi.openAdmin();
                    if (dockinds.charAt(0) == '{')
                    {
                        if (dockinds.length() < 3 || dockinds.charAt(dockinds.length()-1) != '}')
                            throw new JspException("Niepoprawna sk�adnia atrybutu dockinds w freemap: "+dockinds);
    
                        Map dockindsMap = new HashMap();
                        String[] availableDockinds = DocumentKind.listCns();
                        for (int i=0; i < availableDockinds.length; i++)
                        {
                            dockindsMap.put(availableDockinds[i], Boolean.TRUE);
                        }
    
                        try
                        {
                            Boolean result = (Boolean) Ognl.getValue(
                                Ognl.parseExpression(dockinds.substring(1, dockinds.length()-1)), new OgnlContext(),
                                dockindsMap,
                                Boolean.class);
                            if (result == null || !result.booleanValue())
                            {
                                iter.remove();
                                continue;
                            }
                        }
                        catch (OgnlException e)
                        {
                            throw new JspException(e.getMessage(), e);
                        }
                    }
                    else
                    {
                        String[] e = dockinds.split(",");
                        boolean has = true;
                        for (int i=0; i < e.length; i++)
                        {
                            String dockindCn = e[i].trim();
                            if (DocumentKind.findByCn(dockindCn) == null)
                            {
                                has = false;
                                break;
                            }
                        }
    
                        if (!has)
                        {
                            iter.remove();
                            continue;
                        }
                    }
                }
                catch (EdmException e)
                {                    
                    throw new JspException("B��d podczas sprawdzania dost�pnych w systemie rodzaj�w dokument�w", e);
                }
                finally
                {
                    DSApi._close();
                }
            }
        }
    }

    public String getHtmlId()
    {
        return htmlId;
    }

    public void setHtmlId(String htmlId)
    {
        this.htmlId = htmlId;
    }
    
    
    
}
