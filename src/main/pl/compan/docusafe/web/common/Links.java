package pl.compan.docusafe.web.common;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.util.HttpUtils;
import std.pair;
/* User: Administrator, Date: 2006-02-09 11:08:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Links.java,v 1.5 2007/11/28 10:06:28 kubaw Exp $
 */
public class Links
{
    /**
     * Tworzy odno�nik do dokumentu.
     * @param document
     * @param linkType Rodzaj odno�nika; "office" tworzy odno�nik do cz�ci kancelaryjnej.
     * @param fallback Je�eli true odno�nik kancelaryjny nie mo�e zosta� utworzony,
     * zwracany jest odno�nik do edycji dokumentu archiwalnego.
     * @param pairs Dodatkowe parametry, jakie nale�y do��czy� do odno�nika.
     * @return Odno�nik do dokumentu lub null.
     */
    public static String getLink(Document document, String linkType, boolean fallback,
                                 pair<String, String>... pairs)
    {
        StringBuilder link = new StringBuilder();
        if ("office".equals(linkType))
        {
            if (document.getType() == DocumentType.INCOMING)
            {
                link.append("/office/incoming/summary.action?documentId=");
                link.append(document.getId());
            }
            else if (document.getType() == DocumentType.OUTGOING)
            {
                if ( document.getSource() != null && document.getSource().startsWith("crm"))
                {
                    link.append("/office/outgoing/document-archive.action?documentId=");
                    link.append(document.getId());
                }
                else
                {
                    link.append("/office/outgoing/summary.action?documentId=");
                    link.append(document.getId());
                }
               
            }
            else if (document.getType() == DocumentType.INTERNAL)
            {
                link.append("/office/internal/summary.action?documentId=");
                link.append(document.getId());
            }

            if (!fallback)
                return null;
        }
        else
        {
            link.append("/repository/edit-document.action?id=");
            link.append(document.getId());
        }

        if (pairs != null)
        {
            for (pair<String, String> p : pairs)
            {
                link.append('&');
                link.append(p.getFirst());
                link.append('=');
                link.append(HttpUtils.urlEncode(p.getSecond()));
            }
        }

        return link.toString();
    }
}
