package pl.compan.docusafe.web.common;

import com.google.common.collect.Maps;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.*;
import java.text.Format;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Klasa zwracaj�ca szablon bazuj�cy na dockindzie
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DownloadDockindTemplateAction extends EventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(DownloadDockindTemplateAction.class);

    private Long documentId;
    private String templateName;
    private String special;
    private boolean asPdf;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {
            ServletActionContext.getResponse().setContentType("text/rtf");
            Map<String, Object> params = Maps.newHashMap();
            DocFacade df= Documents.document(documentId);
            params.put("fields", df.getFields());
            Map<Class, Format> defaultFormatters = Maps.newLinkedHashMap();
            df.getDocument().getDocumentKind().logic().setAdditionalTemplateValues(df.getId(), params, defaultFormatters);
            String filename = df.getDocument().getDocumentKind().logic().getTemplateFileName(df.getId(), templateName);
            filename += ".rtf";
            if (!asPdf)
            {
                ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                Templating.rtfTemplate(templateName, params, ServletActionContext.getResponse().getOutputStream(),defaultFormatters,special);
            }
            else
            {
                OutputStream templateOutput = new ByteArrayOutputStream();
                Templating.rtfTemplate(templateName, params, templateOutput,defaultFormatters,special);
                InputStream decodedInputFromTemplateOutput = new ByteArrayInputStream(((ByteArrayOutputStream) templateOutput).toByteArray());

                ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + filename.replace(".rtf", ".pdf") + "\"");
                PdfUtils.convertRtfToPdf(decodedInputFromTemplateOutput, ServletActionContext.getResponse().getOutputStream(), true);

            }
        }

        public Logger getLogger() {
            return LOG;
        }
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

	public String getSpecial() {
		return special;
	}

	public void setSpecial(String special) {
		this.special = special;
	}

    public boolean isAsPdf()
    {
        return asPdf;
    }

    public void setAsPdf(boolean asPdf)
    {
        this.asPdf = asPdf;
    }
}