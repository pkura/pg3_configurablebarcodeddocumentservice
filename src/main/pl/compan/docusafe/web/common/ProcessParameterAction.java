package pl.compan.docusafe.web.common;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.measure.Alert;
import pl.compan.docusafe.core.dockinds.measure.CheckPoint;
import pl.compan.docusafe.core.dockinds.measure.ProcessParameterBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@com-pan.pl">Mariusz Kilja�czyk</a>
 * @version $Id: AcceptWfAssignmentAction.java,v 1.00 2009/05/12 07:11:18 mariuszk Exp $
 */
public class ProcessParameterAction extends EventActionSupport 
{
	private Logger log = LoggerFactory.getLogger(ProcessParameterAction.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(ProcessParameterAction.class.getPackage().getName(), null);
	
	private Long documentId;
	private List<ProcessParameterBean> beans;
	
    protected void setup() 
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(fillForm)
            .appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if(documentId == null)
            		throw new EdmException(sm.getString("NieWybranoDokumentu"));
            	Document document = Document.find(documentId);
            	beans = document.getDocumentKind().logic().getProcessParameter(document);
            }
            catch (Exception e)
            {
            	log.error("",e);
                addActionError(e.getMessage());                
            }
        }
    }

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public List<ProcessParameterBean> getBeans()
	{
		return beans;
	}

	public void setBeans(List<ProcessParameterBean> beans)
	{
		this.beans = beans;
	}
    
}
