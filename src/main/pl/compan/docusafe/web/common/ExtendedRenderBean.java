package pl.compan.docusafe.web.common;

import com.google.common.collect.Maps;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

/**
 * SimmpleBean rozszerzony o ca�y zestaw warto�ci
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ExtendedRenderBean extends SimpleRenderBean{
    private final static Logger LOG = LoggerFactory.getLogger(ExtendedRenderBean.class);

    private final Map<String, Object> values = Maps.newHashMap();

    public ExtendedRenderBean(Object value, String template) {
        super(value, template);
    }

    public void putValue(String string, Object value){
        LOG.info("putValue " + string + "/" + value);
        values.put(string, value);
    }

    public void putAllValues(Map<String,?> values){
        this.values.putAll(values);
    }

    public Object get(String key){
//        LOG.info("get '{}' = '{}'", key, values.get(key));
        return values.get(key);
    }

    @Override
    public String toString() {
        return "ExtendedRenderBean{" +
                "values=" + values +
                '}';
    }
}
