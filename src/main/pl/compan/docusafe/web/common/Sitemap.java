package pl.compan.docusafe.web.common;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.regtype.Registry;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.LogFactory;

/* User: Administrator, Date: 2005-07-08 10:22:08 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Sitemap.java,v 1.7 2008/10/15 15:36:08 kubas Exp $
 */
public class Sitemap
{
    /**
     * Uri, pod jakim w pliku freemap.xml znajduj� si� rozszerzone atrybuty
     * dotycz�ce Docusafe.
     */
    public static final String FREEMAP_URI = "http://docusafe.pl/freemap";

    private static final ThreadLocal bindings = new ThreadLocal();
    private static final Freemap freemap = new Freemap();

    public static void read(InputStream is) throws EdmException
    {
        try
        {
            freemap.read(is, new String[] { FREEMAP_URI });
        }
        catch (Exception e)
        {
            throw new EdmException("B��d odczytu freemap.xml", e);
        }
    }

    public static Node bind(String uri, String queryString)
    {
        Node node = freemap.findByHref(uri, queryString);
        bindings.set(node);
        return node;
    }

    public static Node getBoundNode()
    {
        return (Node) bindings.get();
    }

    /**
     * Zwraca menu pierwszego poziomu.
     */
    public static List getMainMenu()
    {
        return freemap.getNodesAt(1, null);
    }

    public static List getLeftMenu()
    {
        Node boundNode = (Node) bindings.get();
        if (boundNode == null)
            return Collections.EMPTY_LIST;
        return freemap.getNodesAt(2, boundNode);
    }

    
    public static String[] getForbiddenRoles() {
    	Node node = (Node) bindings.get();
        if (node == null)
            return new String[0];

        Set forbiddenRoles = null;

        while (node != null)
        {
            String roles = node.getAttribute(FREEMAP_URI, "notroles");
            if (roles != null)
            {
                if (forbiddenRoles == null) forbiddenRoles = new HashSet();
                forbiddenRoles.add(roles);
            }
            node = node.getParent();
        }

        if (forbiddenRoles != null)
        {
            return (String[]) forbiddenRoles.toArray(new String[forbiddenRoles.size()]);
        }
        else
        {
            return new String[0];
        }
    }
    /**
     * Zwraca list� r�l wymaganych dla tego urla lub null.
     */
    public static String[] getRequiredRoles()
    {
        Node node = (Node) bindings.get();
        if (node == null)
            return null;

        Set requiredRoles = null;

        while (node != null)
        {
            String roles = node.getAttribute(FREEMAP_URI, "roles");
            if (roles != null)
            {
                if (requiredRoles == null) requiredRoles = new HashSet();
                requiredRoles.add(roles);
            }
            node = node.getParent();
        }

        if (requiredRoles != null)
        {
            return (String[]) requiredRoles.toArray(new String[requiredRoles.size()]);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Zwraca nazw� dynamicznego menu na poziomie 'level' dla aktualnego urla (o ile jest menu
     * dynamiczne, wpp zwraca null).
     */
    public static String dynamicMenu(int level)
    {
        Node node = (Node) bindings.get();
        while (node != null)
        {
            if (node.getLevel() == level)
                return node.getDynamicMenu();
            else
                node = node.getParent();
        }
        return null;
    }    
}
