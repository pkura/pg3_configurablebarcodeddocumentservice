package pl.compan.docusafe.web.common;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserDockindCounter;
import pl.compan.docusafe.core.office.workflow.snapshot.UserDockindCounter.DockindCount;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.out.DocumentMainAction;

public class PortletStartPageAction extends EventActionSupport {
	private final static Logger LOG = LoggerFactory
			.getLogger(PortletStartPageAction.class);
	private final static String[] DEFAULT_PORTLETS = 
		{"INCOMING", "INTERNAL", "OUTGOING", "CALENDAR", "NAMEDAY", "WEATHER"};	
	private StringManager sm = GlobalPreferences.loadPropertiesFile(PortletStartPageAction.class.getPackage().getName(),null) ;

	private List<Portlet> portlets;

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION).append(new FillForm())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		registerListener("getPortlets").append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new GetPortlets())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class PortletItem {
		String cn;
		int newItems;
		int allItems;
		String[] desc;
		boolean header;

		PortletItem(String cn, int newItems, int allItems, String[] desc,
				boolean header) {
			this.cn = cn;
			this.newItems = newItems;
			this.allItems = allItems;
			this.desc = desc;
			this.header = header;
		}
	}
	
	enum PortletType {
		CALENDAR,
		NAMEDAY,
		WEATHER
	}

	private class Portlet {
		String cn;
		String header;
		PortletItem[] items;
		PortletType type;
		String url;

		Portlet(String cn, String header, String url) {
			this.cn = cn;
			this.header = header;
			this.url = url;
		}
	}
	
	private Portlet createDocumentPortlet(String header, String url, UserDockindCounter counter) {
		String[] standardLabels = { sm.getString("portlets.onTaskList") + ":", 
				sm.getString("portlets.new"), sm.getString("portlets.all")};
		String cn = Normalizer.normalize(header, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replaceAll("\\s", "-");
		Portlet portlet = new Portlet(cn, header, url);
		
		Map<String, DockindCount> dockindCountMap = counter.getDockindCount();
		portlet.items = new PortletItem[dockindCountMap.size() + 1];
		int i = 1;
		portlet.items[0] = new PortletItem("summary", counter.getNotAccepted(), counter.getAll(), standardLabels, true);
		for(Map.Entry<String, DockindCount> entry : dockindCountMap.entrySet()) {
			DockindCount cnt = entry.getValue();
			portlet.items[i] = new PortletItem(entry.getKey(), cnt.getNotAccepted(), cnt.getAll(), new String[] {entry.getKey()}, false);
			i ++;
		}
		
		
		return portlet;
	}
	
	private Portlet createCalendarPortlet(String header) {
		String cn = Normalizer.normalize(header, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replaceAll("\\s", "-");
		Portlet portlet = new Portlet(cn, header, null);
		portlet.type = PortletType.CALENDAR;
		
		return portlet;
	}
	
	private Portlet createNamedayPortlet(String header) {
		Portlet portlet = new Portlet("nameday", header, null);
		portlet.type = PortletType.NAMEDAY;
		
		return portlet;
	}
	
	private Portlet createWeatherPortlet(String header) {
		Portlet portlet = new Portlet("weather", header, null);
		portlet.type = PortletType.WEATHER;
		
		return portlet;
	}
	
	private List<Portlet> createPortlets() throws EdmException {
		List<Portlet> portlets = new LinkedList<Portlet>();
		TaskListCounterManager countManager = new TaskListCounterManager(false);
		String username = DSApi.context().getPrincipalName();
		String availPortletsList = Docusafe.getAdditionProperty("portlets");
		String[] availPortlets;
		if(availPortletsList == null || availPortletsList.trim().isEmpty()
				|| availPortletsList.trim().toUpperCase().equals("ALL")) {
			availPortlets = DEFAULT_PORTLETS;
		} else {
			availPortlets = availPortletsList.split(",");
		}

		
		for(String portletName : availPortlets) {
			portletName = portletName.trim().toUpperCase();
		
			if(portletName.equals("INCOMING")) {
				// Pisma przychodzące
				Portlet incoming = createDocumentPortlet(sm.getString("portlets.incoming"), NotificationsManager.TASKLIST_LINK + "?tab=in", 
						countManager.getUserDockindCounter(username, "in", null));
				portlets.add(incoming);
			} else if(portletName.equals("INTERNAL")) {
				// Pisma wewnętrzne
				Portlet internal = createDocumentPortlet(sm.getString("portlets.internal"), NotificationsManager.TASKLIST_LINK + "?tab=internal", 
						countManager.getUserDockindCounter(username, "internal", null));
				portlets.add(internal);
			} else if(portletName.equals("OUTGOING")) {
				// Pisma wychodzące
				Portlet outgoing = createDocumentPortlet(sm.getString("portlets.outgoing"), NotificationsManager.TASKLIST_LINK + "?tab=out", 
						countManager.getUserDockindCounter(username, "outgoing", null));
				portlets.add(outgoing);
			} else if(portletName.equals("CALENDAR")) {
				// Kalendarz
				Portlet calendar = createCalendarPortlet(sm.getString("portlets.calendar"));
				portlets.add(calendar);
			} else if(portletName.equals("NAMEDAY")) {
				// Imieniny
				Portlet nameday = createNamedayPortlet(sm.getString("portlets.nameday"));
				portlets.add(nameday);
			} else if(portletName.equals("WEATHER")) {
				// Pogoda
				Portlet weather = createWeatherPortlet(sm.getString("portlets.weather"));
				portlets.add(weather);
			} else {
				LOG.debug("Nieznany portlet: " + portletName);
			}
		}
		
		return portlets;
	}

	private class GetPortlets extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			portlets = createPortlets();
			PrintWriter out = getOutPrintWriter();
			try {
				Gson gson = new Gson();
				out.write(gson.toJson(portlets, new TypeToken<List<Portlet>>() {
				}.getType()));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				out.write("JSON FAILED");
			} finally {
				out.flush();
				out.close();
			}
		}

		@Override
		public Logger getLogger() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	private class FillForm extends LoggedActionListener {
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
		}

		@Override
		public Logger getLogger() {
			// TODO Auto-generated method stub
			return LOG;
		}
	}

	private PrintWriter getOutPrintWriter() throws IOException {
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html; charset=ISO-8859-2");
		return resp.getWriter();
	}

	public List<Portlet> getPortlets() {
		return portlets;
	}

	public void setPortlets(List<Portlet> portlets) {
		this.portlets = portlets;
	}
}
