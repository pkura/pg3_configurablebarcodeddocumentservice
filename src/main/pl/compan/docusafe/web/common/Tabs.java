package pl.compan.docusafe.web.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
/* User: Administrator, Date: 2005-08-23 09:56:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Tabs.java,v 1.2 2006/02/20 15:42:38 lk Exp $
 */
public class Tabs extends ArrayList
{
    private Tab selectedTab;

    public Tabs()
    {
    }

    public Tabs(int initialCapacity)
    {
        super(initialCapacity);
    }

    public Tab getSelectedTab()
    {
        return selectedTab;
    }

    public boolean add(Object o)
    {
        if (!(o instanceof Tab))
            throw new IllegalArgumentException("Spodziewano si� instancji Tab");
        Tab tab = (Tab) o;
        if (tab.isSelected())
            selectedTab = tab;
        return super.add(tab);
    }

    public void add(int index, Object o)
    {
        if (!(o instanceof Tab))
            throw new IllegalArgumentException("Spodziewano si� instancji Tab");
        Tab tab = (Tab) o;
        if (tab.isSelected())
            selectedTab = tab;

        super.add(index, o);
    }

    public boolean addAll(int index, Collection c)
    {
        for (Iterator iter=c.iterator(); iter.hasNext(); )
        {
            Object o = iter.next();
            if (!(o instanceof Tab))
                throw new IllegalArgumentException("Spodziewano si� instancji Tab");
            if (((Tab) o).isSelected())
                selectedTab = (Tab) o;
        }
        return super.addAll(index, c);
    }

    public boolean addAll(Collection c)
    {
        for (Iterator iter=c.iterator(); iter.hasNext(); )
        {
            Object o = iter.next();
            if (!(o instanceof Tab))
                throw new IllegalArgumentException("Spodziewano si� instancji Tab");
            if (((Tab) o).isSelected())
                selectedTab = (Tab) o;
        }
        return super.addAll(c);
    }

    public Object remove(int index)
    {
        Tab tab = (Tab) super.remove(index);
        if (tab == selectedTab)
            selectedTab = null;
        return tab;
    }

    /**
     * @throws UnsupportedOperationException
     */
    protected void removeRange(int fromIndex, int toIndex)
    {
        throw new UnsupportedOperationException();
    }

    public Object set(int index, Object o)
    {
        if (!(o instanceof Tab))
            throw new IllegalArgumentException("Spodziewano si� instancji Tab");
        Tab tab = (Tab) o;
        if (tab.isSelected())
            selectedTab = tab;

        return super.set(index, o);
    }
}
