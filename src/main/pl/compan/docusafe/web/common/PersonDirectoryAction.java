package pl.compan.docusafe.web.common;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PersonDirectoryAction.java,v 1.52 2010/08/16 11:01:33 mariuszk Exp $
 */
public class PersonDirectoryAction extends EventActionSupport
{
    public static final String URL = "/office/common/person.action";
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(PersonDirectoryAction.class.getPackage().getName(),null);
    public static final int LIMIT = 20;

    /**
     * Parametr pomocniczy przekazywany akcji przez wywo�uj�cego.
     * Nie ma znaczenia dla tej akcji, ale powinien by� zwr�cony
     * wywo�uj�cemu w nienaruszonej postaci.
     */
    private String lparam;
    /**
     * Parametr pomocniczy przekazywany akcji przez wywo�uj�cego.
     * Nie ma znaczenia dla tej akcji, ale powinien by� zwr�cony
     * wywo�uj�cemu w nienaruszonej postaci.
     */
    private String wparam;

    private String dictionaryType;
    private String title;
    private String firstname;
    private String lastname;
    private String group;
    private String organization;
    private String organizationDivision;
    private String street;
    private String zip;
    private String location;
    private String country;
    private String email;
    private String fax;
    private String nip;
    private String pesel;
    private String regon;
    private boolean closeOnReload;
    private Boolean zgodaNaWysylke;


    private Long personId;
    private List<? extends Person> results;
    private Pager pager;
    private boolean canDelete;
    private boolean canAdd;
    private boolean canAddAndSubmit;
    private Map<String, String> guids;
    private String guid;
    private String identifikatorEpuap;
    private String adresSkrytkiEpuap;

    private int offset;
    private int limit = LIMIT;

    protected void setup()
    {
        // 1. wy�wietlenie pustego formularza z przyciskami "dodaj odbiorc�"
        // "wyszukaj", "zapisz w bazie"
        // 2. wyszukiwarka odbiorc�w
        // 3. dodawanie odbiorcy do bazy
        //registerListener(DEFAULT_ACTION);

        // nie rejestruj� domy�lnego listenera, EventActionSupport
        // samo zwr�ci SUCCESS

    	FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(new ValidateAdd()).
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doClean").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Clean()).
        	appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
    		append(OpenHibernateSession.INSTANCE).
    		append(new Update()).
            append(fillForm).
    		appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String formatNip(String nip)
    {
        if (nip != null && nip.length() > 0)
        {
            StringBuilder sb = new StringBuilder(nip.length() * 2);

            sb.append(nip.substring(0, nip.length() >= 3 ? 3 : nip.length()));
            if (nip.length() > 3)
            {
                sb.append("-");
                sb.append(nip.substring(3, nip.length() >= 6 ? 6 : nip.length()));
            }
            if (nip.length() > 6)
            {
                sb.append("-");
                sb.append(nip.substring(6, nip.length() >= 8 ? 8 : nip.length()));
            }
            if (nip.length() > 8)
            {
                sb.append("-");
                sb.append(nip.substring(8));
            }

            return sb.toString();
/*
            return nip.substring(0, nip.length() >= 3 ? 3 : nip.length()) + "-" +
                (nip.length() >= 4 ?
                    (nip.substring(3, nip.length() >= 6 ? 6 : nip.length())) + "-" +
                        (nip.length() >= 7 ?
                            (nip.substring(6, nip.length() >= 8 ? 8 : nip.length())) + "-" +
                                (nip.length() >= 9 ?
                                    (nip.substring(8, nip.length())) : "") : "") : "");
*/
        }
        else
        {
            return null;
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (personId != null)
            {
                try
                {
                    DSApi.context().begin();

                    canDelete = DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_USUWANIE);
                    if (!canDelete)
                        throw new EdmException(sm.getString("BrakUprawnienDoModyfikacjiSlownika"));

                    Person person = Person.find(personId.longValue());

                    if (person.getClass().equals(Person.class))
                    {
                        person.delete();
                    }

                    DSApi.context().commit();
                    personId = null;
                    
                    addActionMessage(sm.getString("ZeSlownikaUsunieto",person.getShortSummary()));
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
        }
    }
    private class Clean implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try{
        	canDelete = DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_USUWANIE);
            canAdd = DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_DODAWANIE);
            guid = TextUtils.trimmedStringOrNull(guid);
            if (guid == null)
                guid = DSDivision.ROOT_GUID;

            guids = new LinkedHashMap<String, String>();
            guids.put(DSDivision.ROOT_GUID, sm.getString("Wspolny"));
            try
            {
                if (!DSDivision.ROOT_GUID.equals(guid))
                    guids.put(guid, DSDivision.find(guid).getName());
                for (DSDivision div : DSApi.context().getDSUser().getDivisions())
                {
                    if (div.isPosition()) div = div.getParent();
                    if (!guids.containsKey(div.getGuid()))
                        guids.put(div.getGuid(), div.getName());
                }
            }

            catch (DivisionNotFoundException e)
            {
            }

             personId = null;
             title = null;
             firstname = null;
             lastname =null;
             organization = null;
             organizationDivision = null ;
             street = null;
             zip = null;;
             location = null;
             country = null;
             email = null;
             fax = null;
             nip = null;
             pesel =null;
             regon = null;
             group = null;
             identifikatorEpuap = null;
             adresSkrytkiEpuap = null;
             if (StringUtils.isEmpty(country)) country = "PL";

        	}
        	catch (EdmException e)
             {

                 addActionError(e.getMessage());
             }



        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                canDelete = DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_USUWANIE);
                canAdd = DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_DODAWANIE);
                guid = TextUtils.trimmedStringOrNull(guid);
                String tmp1 = DSApi.context().userPreferences().node("other").get("guid", guid);


                guids = new LinkedHashMap<String, String>();
                guids.put(DSDivision.ROOT_GUID, sm.getString("Wspolny"));
                try
                {
                    DSDivision [] tmp = null;
                    if (guid == null)
                    {
                        tmp = DSApi.context().getDSUser().getDivisions();
                        if( tmp.length>0 )
                        {
                            guid = tmp[0].getGuid();
                            if (!DSDivision.ROOT_GUID.equals(guid))
                                guids.put(guid, DSDivision.find(guid).getName());
                        }
                    }
                    if(tmp1 != null)
                        guid = tmp1;
                    else
                        guid = DSDivision.ROOT_GUID;

                    for (DSDivision div : DSApi.context().getDSUser().getDivisions())
                    {
                        if (div.isPosition()) div = div.getParent();
                        if (!guids.containsKey(div.getGuid()))
                            guids.put(div.getGuid(), div.getName());
                    }
                }
                catch (DivisionNotFoundException e)
                {
                }

                if (personId != null)
                {
                    Person person = Person.find(personId.longValue());

                    title = person.getTitle();
                    firstname = person.getFirstname();
                    lastname = person.getLastname();
                    organization = person.getOrganization();
                   	organizationDivision = person.getOrganizationDivision();
                    street = person.getStreet();
                    zip = person.getZip();
                    location = person.getLocation();
                    country = person.getCountry();
                    email = person.getEmail();
                    fax = person.getFax();
                    nip = formatNip(person.getNip());
                    pesel = person.getPesel();
                    regon = person.getRegon();
                    group = person.getPersonGroup();
                    if(person.getIdentifikatorEpuap() != null)
                    	identifikatorEpuap = person.getIdentifikatorEpuap();
                    if(person.getAdresSkrytkiEpuap() != null)
                    	adresSkrytkiEpuap = person.getAdresSkrytkiEpuap();
                    zgodaNaWysylke = person.getZgodaNaWysylke();
                }
                else
                {
                   if (StringUtils.isEmpty(country)) country = "PL";
                }
                canDelete=false;
                canAdd=false;
                canAddAndSubmit=false;
                
            }
            catch (EdmException e)
            {
                 addActionError(e.getMessage());
            }
        }
    }

    private class ValidateAdd implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!StringUtils.isEmpty(nip) &&
                !nip.matches("[0-9]+(-?[0-9]+)+"))
                addActionError(sm.getString("NiepoprawnyNumerNIP"));

           /* if (!StringUtils.isEmpty(zip) && !zip.matches("[0-9]{2}-[0-9]{3}"))
                addActionError("Niepoprawny kod"); */

            if (hasActionErrors())
                event.cancel();
        }
    }

    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(dictionaryType))
            {
                addActionError(sm.getString("NiePodanoRodzajuSlownikaOdbiorcyNadawcy"));
                return;
            }

            if (StringUtils.isEmpty(firstname) &&
                StringUtils.isEmpty(lastname) &&
                StringUtils.isEmpty(organization))
            {
                addActionError(sm.getString("WymaganeJestPodanieImieniaInazwiskaLubNazwyOrganizacji"));
                return;
            }

            guid = TextUtils.trimmedStringOrNull(guid);
            if (guid == null)
                guid = DSDivision.ROOT_GUID;

            Person person = new Person();
            try
            {
                person.setDictionaryType(dictionaryType);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                return;
            }
            person.setDictionaryGuid(guid);
            person.setCountry(TextUtils.trimmedStringOrNull(country, 2));
            person.setEmail(TextUtils.trimmedStringOrNull(email, 50));
            person.setFax(TextUtils.trimmedStringOrNull(fax, 30));
            person.setFirstname(TextUtils.trimmedStringOrNull(firstname, 50));
            person.setLastname(TextUtils.trimmedStringOrNull(lastname, 50));
            person.setLocation(TextUtils.trimmedStringOrNull(location, 50));
            person.setOrganization(TextUtils.trimmedStringOrNull(organization, 50));
            person.setOrganizationDivision(TextUtils.trimmedStringOrNull(organizationDivision, 50));
            person.setStreet(TextUtils.trimmedStringOrNull(street, 50));
            person.setTitle(TextUtils.trimmedStringOrNull(title, 30));
            person.setZip(TextUtils.trimmedStringOrNull(zip, 14));
            person.setNip(TextUtils.trimmedStringOrNull(nip != null ? nip.replaceAll("-", "") : null, 15));
            person.setPesel(TextUtils.trimmedStringOrNull(pesel, 11));
            person.setRegon(TextUtils.trimmedStringOrNull(regon, 15));
            person.setPersonGroup(TextUtils.trimmedStringOrNull(group, 50));
            person.setAdresSkrytkiEpuap(TextUtils.trimmedStringOrNull(adresSkrytkiEpuap, 50));
            person.setIdentifikatorEpuap(TextUtils.trimmedStringOrNull(identifikatorEpuap, 50));

            try
            {
                DSApi.context().begin();

                boolean created = person.createIfNew();

                DSApi.context().commit();

                if (created)
                    addActionMessage(sm.getString("WslownikuZapisano",person.getShortSummary()));
                else
                    addActionMessage(sm.getString("TakiWpisIstniejeJuzWslowniku"));

                personId = person.getId();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(dictionaryType))
            {
                addActionError(sm.getString("NiePodanoRodzajuSlownikaOdbiorcyNadawcy"));
                return;
            }

            if (StringUtils.isEmpty(firstname) &&
                StringUtils.isEmpty(lastname) &&
                StringUtils.isEmpty(organization))
            {
                addActionError(sm.getString("WymaganeJestPodanieImieniaInazwiskaLubNazwyOrganizacji"));
                return;
            }

            guid = TextUtils.trimmedStringOrNull(guid);
            if (guid == null)
                guid = DSDivision.ROOT_GUID;

            if(personId != null)
            {
            	try
            	{
                    DSApi.context().begin();

                    Person person = Person.find(personId.longValue());
            		person.setDictionaryType(dictionaryType);

            		person.setDictionaryGuid(guid);
            		person.setCountry(TextUtils.trimmedStringOrNull(country, 2));
            		person.setEmail(TextUtils.trimmedStringOrNull(email, 50));
            		person.setFax(TextUtils.trimmedStringOrNull(fax, 30));
            		person.setFirstname(TextUtils.trimmedStringOrNull(firstname, 50));
            		person.setLastname(TextUtils.trimmedStringOrNull(lastname, 50));
            		person.setLocation(TextUtils.trimmedStringOrNull(location, 50));
            		person.setOrganization(TextUtils.trimmedStringOrNull(organization, 50));
            		person.setOrganizationDivision(TextUtils.trimmedStringOrNull(organizationDivision, 50));
            		person.setStreet(TextUtils.trimmedStringOrNull(street, 50));
            		person.setTitle(TextUtils.trimmedStringOrNull(title, 30));
            		person.setZip(TextUtils.trimmedStringOrNull(zip, 14));
            		person.setNip(TextUtils.trimmedStringOrNull(nip != null ? nip.replaceAll("-", "") : null, 15));
            		person.setPesel(TextUtils.trimmedStringOrNull(pesel, 11));
            		person.setRegon(TextUtils.trimmedStringOrNull(regon, 15));
            		person.setPersonGroup(TextUtils.trimmedStringOrNull(group, 50));
                    person.setAdresSkrytkiEpuap(TextUtils.trimmedStringOrNull(adresSkrytkiEpuap, 50));
                    person.setIdentifikatorEpuap(TextUtils.trimmedStringOrNull(identifikatorEpuap, 50));

            		//boolean created = person.createIfNew();

            		DSApi.context().commit();

            	//	if (created)
            			addActionMessage(sm.getString("WslownikuZapisanoZmiany")/*+ person.getShortSummary()*/);
            	//	else
            	//		addActionMessage("Taki wpis istnieje ju� w s�owniku");
            	}
            	catch (EdmException e)
            	{
            		DSApi.context().setRollbackOnly();
            		addActionError(e.getMessage());
            	}
            }
            else
            {
            	addActionError(sm.getString("NieMoznaZapisacZmianTakaOsobaNieIstniejeWslowniku"));
            }

        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            QueryForm form = new QueryForm(offset, limit);
            
            try {
				if (DocumentKind.isAvailable(DocumentLogicLoader.NATIONWIDE_KIND)) 
				{			
		            form.addProperty("dictionaryType", Person.DICTIONARY_SENDER_RECIPIENT);
				}
				else if (!StringUtils.isEmpty(dictionaryType) && !AvailabilityManager.isAvailable("person.selection.discriminator")) 
				{
					form.addProperty("dictionaryType", dictionaryType);
				}
			} catch (EdmHibernateException ex) {
				addActionError(ex.getMessage());
			} catch (EdmException ex) {
				addActionError(ex.getMessage());
			}
			
            if (!StringUtils.isEmpty(firstname))
                form.addProperty("firstname", firstname.trim());
            if (!StringUtils.isEmpty(lastname))
                form.addProperty("lastname", lastname.trim());
            if (!StringUtils.isEmpty(organization))
                form.addProperty("organization", organization.trim());
            if (!StringUtils.isEmpty(street))
                form.addProperty("street", street.trim());
            if (!StringUtils.isEmpty(zip))
                form.addProperty("zip", zip.trim());
            if (!StringUtils.isEmpty(location))
                form.addProperty("location", location.trim());
            if(AvailabilityManager.isAvailable("person.kraj")){
            if (!StringUtils.isEmpty(country))
                form.addProperty("country", country.trim());
            }
            if (!StringUtils.isEmpty(email))
                form.addProperty("email", email.trim());
            if (!StringUtils.isEmpty(fax))
                form.addProperty("fax", fax.trim());
            if (!StringUtils.isEmpty(nip))
                form.addProperty("nip", nip.replaceAll("-", "").trim());
            if (!StringUtils.isEmpty(pesel))
                form.addProperty("pesel", pesel.trim());
            if (!StringUtils.isEmpty(regon))
                form.addProperty("regon", regon.trim());
            if (!StringUtils.isEmpty(group))
                form.addProperty("personGroup", group.trim());
            if (!StringUtils.isEmpty(guid))
                form.addProperty("dictionaryGuid", guid);
            else
                form.addProperty("dictionaryGuid", DSDivision.ROOT_GUID);
            if (!StringUtils.isEmpty(identifikatorEpuap))
                form.addProperty("identifikatorEpuap", identifikatorEpuap);
            if (!StringUtils.isEmpty(adresSkrytkiEpuap))
                form.addProperty("adresSkrytkiEpuap", adresSkrytkiEpuap);
            
            if (!StringUtils.isEmpty(organizationDivision)){
                form.addProperty("organizationDivision", organizationDivision.trim());
            }
            form.addOrderAsc("lastname");
            if(AvailabilityManager.isAvailable("person.selection.discriminator"))
            	form.addProperty("DISCRIMINATOR", "PERSON");
            try
            {
                SearchResults<? extends Person> results = Person.search(form);

                if (results.totalCount() == 0)
                    addActionMessage(sm.getString("NieZnalezionoOsobPasujacychDoWpisanychDanych"));

                PersonDirectoryAction.this.results = fun.list(results);


                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(URL, new String[] {
                            "doSearch", "true",
                            "personId", personId != null ? personId.toString() : null,
                            "firstname", firstname,
                            "lastname", lastname,
                            "organization", organization,
                            "street", street,
                            "zip", zip,
                            "location", location,
                            "country", country,
                            "email", email,
                            "fax", fax,
                            "lparam", lparam,
                            "wparam", wparam,
                            "dictionaryType", dictionaryType,
                            "nip", nip,
                            "pesel", pesel,
                            "regon", regon,
                            "group", group,
                            "guid", guid,
                            "canAddAndSubmit",canAddAndSubmit ? "true" :"false",
                            "offset", String.valueOf(offset),
                            "organizationDivision",organizationDivision
                        });
                    }
                };

                pager = new Pager(linkVisitor, offset, LIMIT,results.totalCount(),10);//results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public String getWparam()
    {
        return wparam;
    }

    public void setWparam(String wparam)
    {
        this.wparam = wparam;
    }

    public String getDictionaryType()
    {
        return dictionaryType;
    }

    public void setDictionaryType(String dictionaryType)
    {
        this.dictionaryType = dictionaryType;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public String getOrganization()
    {
        return organization;
    }

    public void setOrganization(String organization)
    {
        this.organization = organization;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(String fax)
    {
        this.fax = fax;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public List getResults()
    {
        return results;
    }

    public Long getPersonId()
    {
        return personId;
    }

    public void setPersonId(Long personId)
    {
        this.personId = personId;
    }

    public Pager getPager()
    {
        return pager;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public String getPesel()
    {
        return pesel;
    }

    public void setPesel(String pesel)
    {
        this.pesel = pesel;
    }

    public String getRegon()
    {
        return regon;
    }

    public void setRegon(String regon)
    {
        this.regon = regon;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanAdd()
    {
        return canAdd;
    }

    public boolean isCloseOnReload()
    {
        return closeOnReload;
    }

    public void setCloseOnReload(boolean closeOnReload)
    {
        this.closeOnReload = closeOnReload;
    }

    public boolean isCanAddAndSubmit()
    {
        return canAddAndSubmit;
    }

    public void setCanAddAndSubmit(boolean canAddAndSubmit)
    {
        this.canAddAndSubmit = canAddAndSubmit;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public Map<String,String> getGuids()
    {
        return guids;
    }

	public String getOrganizationDivision() {
		return organizationDivision;
	}

	public void setOrganizationDivision(String organizationDivision) {
		this.organizationDivision = organizationDivision;
	}

	public String getAdresSkrytkiEpuap() {
		return adresSkrytkiEpuap;
	}

	public void setAdresSkrytkiEpuap(String adresSkrytkiEpuap) {
		this.adresSkrytkiEpuap = adresSkrytkiEpuap;
	}

	public String getIdentifikatorEpuap() {
		return identifikatorEpuap;
	}

	public void setIdentifikatorEpuap(String identifikatorEpuap) {
		this.identifikatorEpuap = identifikatorEpuap;
	}

	public Boolean getZgodaNaWysylke() {
		return zgodaNaWysylke;
	}

	public void setZgodaNaWysylke(Boolean zgodaNaWysylke) {
		this.zgodaNaWysylke = zgodaNaWysylke;
	}


}
