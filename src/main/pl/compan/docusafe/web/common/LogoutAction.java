package pl.compan.docusafe.web.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SignonLog;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.core.xes.XesLoggedActionsTypes;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Arrays;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LogoutAction.java,v 1.11 2009/11/17 12:35:56 tomekl Exp $
 */
public class LogoutAction extends EventDrivenAction
{
    private static final Log log = LogFactory.getLog(LogoutAction.class);

    private static final String[] eventNames = new String[] {
        "doDefault"
    };
    static
    {
        Arrays.sort(eventNames);
    }

    public ActionForward doDefault(
        ActionMapping mapping,
        ActionForm form,
        String eventParameter,
        HttpServletRequest request,
        HttpServletResponse response,
        Messages errors,
        Messages messages)
    {
        try
        {
            String moduleName = getServlet().getServletContext().getInitParameter("loginModuleName");
            String xesObjectName = this.toString();
            if(! logout(request, moduleName, xesObjectName)) {
                new ActionForward(mapping.findForward("home").getPath(), true);
            }
        }
        catch (LoginException e)
        {
        	log.debug("", e);
        }

        return new ActionForward(mapping.findForward("home").getPath(), true);
    }

    public static boolean logout(HttpServletRequest request, String moduleName, String xesObjectName) throws LoginException {
        //String root = getServlet().getServletContext().getInitParameter("pl.com-pan.docusafe.ldapRoot");

        Subject subject = AuthUtil.getSubject(request);

        if (subject == null)
            return false;

        UserPrincipal principal = AuthUtil.getUserPrincipal(subject);
        String username = principal != null ? principal.getName() : "";

        LoginContext lc = new LoginContext(moduleName, subject,
                new FormCallbackHandler("", "", ""));
        lc.logout();


        //DSApi.clearCredentials(username);
        xesLogEventAction(request,username, xesObjectName);

        HttpSession session = request.getSession();
        if (session != null)
            session.invalidate();

        SignonLog.logSignoff(username);

        return true;
    }

    private static void xesLogEventAction(HttpServletRequest request, String username, String xesObjectName)
	{
    	try
		{
    		boolean opened = DSApi.openContextIfNeeded();
    		String uri = (request!=null ? request.getRequestURI() : "/docusafe/logout.do");
			if(XesLoggedActionsTypes.isActionLoggingEnableByUriAndAction(uri, "logout", null)){
				XesLog.createLoginLogoutEvent(DSUser.findByUsername(username),"logout" , xesObjectName ,uri,false);
			}
			DSApi.closeContextIfNeeded(opened);
			
		} catch (UserNotFoundException e)
		{
			log.debug("", e);
		} catch (EdmException e)
		{
			log.debug("", e);
		}
		
	}

	public String[] getEventNames()
    {
        return eventNames;
    }
}
