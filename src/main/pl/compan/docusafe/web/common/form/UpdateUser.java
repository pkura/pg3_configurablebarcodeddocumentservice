package pl.compan.docusafe.web.common.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UpdateUser.java,v 1.36 2010/07/08 10:47:27 tomekl Exp $
 */
public class UpdateUser
{
    private static final Logger log = LoggerFactory.getLogger(UpdateUser.class);
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    /**
     * Nieczytelna metoda, kt�ra spowodowa�a ju� kilka b��d�w, niczego nie u�atwia, nie u�ywa�.
     *
	 * Metoda musi by� wywo�ywana w otwartej transakcji bazodanowej. Je�eli
	 * zmieniane jest has�o, u�ytkownik przy kolejnym logowaniu musi je zmieni�.
	 * 
	 * @param username
	 *            Nazwa u�ytkownika.
	 * @param firstname
	 *            Imi�.
	 * @param lastname
	 *            Nazwisko.
	 * @param email
	 *            Email.
	 * @param password
	 *            Has�o
	 * @param password2
	 *            Weryfikacja has�a
	 * @param supervisor
	 *            Nazwa prze�o�onego
	 * @param substitutedBy
	 *            Nazwa u�ytkownika, kt�ry jest zast�pc� bie��cego
	 * @param substitutedThrough
	 *            Dzie�, do kt�rego u�ytkownik jest zast�powany
	 * @param roles
	 *            Role archiwum
	 * @param groupGuids
	 *            Guidy grup, w kt�rych jest u�ytkownik.
	 * @param officeRoleIds
	 *            Role kancelarii
	 *
	 * @param builder DataMartEventBuilder mo�e by� null, wtedy w funkcji jest tworzony nowy builder
	 */
    @Deprecated
    public static void updateUser(String username,String externalName, String firstname, String lastname,
                                  String identifier,
                                  String email, String password, String password2,
                                  boolean loginDisabled,
                                  String supervisor, String substitutedBy,
                                  Date substitutedFrom,
                                  Date substitutedThrough,
                                  String[] roles, String[] groupGuids, Long[] officeRoleIds,
                                  Boolean certificateLogin,Boolean activeDirectory)
        throws EdmException
    {
		DataMartEventBuilder builder = DataMartEventBuilder.get();
	
        if (username == null)
            throw new NullPointerException("username");

        if (password != null || password2 != null)
        {
            if (password == null || !password.equals(password2))
                throw new EdmException(sm.getString("updateUser.passwordsMismatch"));
        }

        // w nazwach domen nie mo�e by� podkre�le�, ale dopuszczam je tutaj,
        // bo czasem si� trafiaj�
        if (!StringUtils.isEmpty(email) && !email.matches("^[-a-zA-Z0-9.+,!/_]+@[-a-zA-Z0-9_][-a-zA-Z0-9._]*$"))
            throw new EdmException(sm.getString("updateUser.invalidEmail"));

        if (!StringUtils.isEmpty(substitutedBy) && substitutedThrough == null)
            throw new EdmException(sm.getString("updateUser.substitutionDateMissing"));

        if (substitutedBy != null && (substitutedFrom == null || substitutedThrough == null))
            throw new EdmException("Nie podano zakresu dat, w kt�rym zast�pstwo jest wa�ne " +
                "lub daty zosta�y niepoprawnie wpisane");

        Date today = new Date();
        if (substitutedBy != null)
        {
            if (substitutedThrough.before(today))
                throw new EdmException(sm.getString("updateUser.substitutedThroughSameDayOrBefore"));
            if (substitutedThrough.before(substitutedFrom))
                throw new EdmException("Data pocz�tku zast�pstwa jest p�niejsza ni� data " +
                    "ko�ca zast�pstwa");
        }

        // final OrganizationHelper oh =
		// DSApi.context().getOrganizationHelper();

// if (Configuration.getMaxUsers() > 0 &&
// oh.getUserCount() > Configuration.getMaxUsers())
// throw new EdmException(sm.getString("updateUser.userCountLimitReached",
// new Integer(Configuration.getMaxUsers())));

        final DSUser user = DSUser.findByUsername(username);

        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setEmail(email);
        user.setIdentifier(identifier);
        user.setLoginDisabled(loginDisabled);
        user.setExternalName(externalName);
        if(activeDirectory != null)
        {
        	user.setChangePasswordAtFirstLogon(false);
        	user.setAdUser(activeDirectory);
        	if(Docusafe.getAdditionProperty("active.directory.userDomainPrefix") != null && Docusafe.getAdditionProperty("active.directory.userDomainPrefix").trim().length()>0)
        		user.setDomainPrefix(Docusafe.getAdditionProperty("active.directory.userDomainPrefix").trim());
        	else
        		user.setDomainPrefix(null);
        }

        if (certificateLogin != null)
            user.setCertificateLogin(certificateLogin.booleanValue());

		builder
		  .checkValueChangeEvent(EventType.USER_CHANGE, "externalName",
			  user.getExternalName(), !StringUtils.isEmpty(externalName) ? externalName : null)
		  .checkValueChangeEvent(EventType.USER_CHANGE, "firstname",
			  user.getFirstname(), !StringUtils.isEmpty(firstname) ? firstname : null)
		  .checkValueChangeEvent(EventType.USER_CHANGE, "lastname",
			  user.getLastname(), !StringUtils.isEmpty(lastname) ? lastname : null)
		  .checkValueChangeEvent(EventType.USER_CHANGE, "identifier",
			  user.getIdentifier(), identifier)
		  .checkValueChangeEvent(EventType.USER_CHANGE, "email",
			  user.getEmail(), !StringUtils.isEmpty(email) ? email : null)
		  .checkValueChangeEvent(EventType.USER_CHANGE, "loginDisabled",
			  user.isLoginDisabled()+"", loginDisabled  +"");


        // prze�o�ony u�ytkownika

        //log.info("supervisor set to : '{}'", supervisor);
        if (!StringUtils.isEmpty(supervisor))
        {
            user.setSupervisor(DSUser.findByUsername(supervisor));
        }
        else
        {
            user.setSupervisor(null);
        }

        // zast�pstwo

        if (!StringUtils.isEmpty(substitutedBy) &&
            substitutedThrough != null && substitutedFrom != null)
        {
            user.setSubstitution(
                DSUser.findByUsername(substitutedBy), substitutedFrom, substitutedThrough);
        }
        else
        {
            user.cancelSubstitution();
        }

        updateArchiveRoles(roles, user);
        updateGroups(groupGuids, user);
        updateOfficeRoles(officeRoleIds, user);
        
        if(user.getLastUnsuccessfulLogin() == null) user.setChangePasswordAtFirstLogon(true);

        user.update();
        if (!StringUtils.isEmpty(password))
        {
        	if(!DSApi.context().getPrincipalName().equals(user.getName()))
        	{
	     	   	DSLog l = new DSLog();
	     	   	l.setCode(DSLog.CHANGE_PASSWORD);
	     	   	l.setCtime(new Date());
	     	   	l.setParam(user.getName());
	     	   	l.setReason(password);
	     	   	l.setUsername(DSApi.context().getPrincipalName());
	     	   	l.create();
        	}
            user.setPassword(password, true);
        }
    }
    
    public static void addSource(Map map, Object key, String newSource) throws EdmException {
    	Set<Entry<Long, String>> set = map.entrySet();
    	if (map.containsKey(key)) {
    		String source = (String)map.get(key);
    		if (source == null || source.length() == 0) {
    			source = newSource;
    		} else {
    			List<String> sources = Arrays.asList(StringUtils.split(source, ";"));
    			if (sources.contains(newSource)) {
    				return;
    			}
    			source+=";"+newSource;
    		}
    		map.put(key, source);
    	} else {
    		throw new EdmException("KeyNotFound");
    	}
    }
    
    
    public static void updateArchiveRoles(String[] roles, DSUser user) throws EdmException {
    	if (roles != null)
        {

            Set userRoles = new HashSet(user.getRoles());

            for (int i=0, n=roles.length; i < n; i++)
            {
				if(!userRoles.contains(roles[i])){
					userRoles.add(roles[i]);
				}
            }

            // nie mo�na odebra� roli admin u�ytkownikowi administracyjnemu
            if (user.getName().equals(GlobalPreferences.getAdminUsername()) &&
                !userRoles.contains(DSUser.ADMIN_ROLE))
                throw new EdmException("U�ytkownik "+user.getName()+" jest " +
                    "administratorem systemu, nie mo�na odebra� mu roli 'Administrator'");

            // zmiany w kolekcji roles w obiekcie User
            user.archiveRolesOfCollection(Arrays.asList(roles));
        }
    }
    
    public static void updateGroups(String[] groupGuids, DSUser user) throws EdmException {
    	// wszystkie grupy
        final DSDivision[] allGroups = UserFactory.getInstance().getGroups();

		DataMartEventBuilder builder = DataMartEventBuilder.get();
		
        if (groupGuids != null)
        {
            if (groupGuids.length > 0)
            {
                String[] groups = new String[groupGuids.length];
                System.arraycopy(groupGuids, 0, groups, 0, groups.length);
                Arrays.sort(groups);

                for (int i=0; i < allGroups.length; i++)
                {
                    // usuwam i dodaj� u�ytkownika do grupy nie sprawdzaj�c,
                    // czy w niej jest
                    if (Arrays.binarySearch(groups, allGroups[i].getGuid()) < 0) {
                        allGroups[i].removeUser(user);
                        log.info("Usuwa grupe {} dla u�ytkownika {}", allGroups[i].getName(), user.getName());
                        if(Configuration.coreOfficeAvailable())
                        	WorkflowFactory.removeParticipantToUsernameMapping(null, null, "ds_guid_".concat(allGroups[i].getGuid()), user.getName());
                    }
                    else {
                        allGroups[i].addUser(user);
                        log.info("Dodaje grupe {} dla u�ytkownika {}", allGroups[i].getName(), user.getName());
                        if(Configuration.coreOfficeAvailable())
                        	WorkflowFactory.addParticipantToUsernameMapping(null, null, "ds_guid_".concat(allGroups[i].getGuid()), user.getName());
                    }
                }
            }
            else
            {
                // usuwanie u�ytkownika ze wszystkich grup
                for (int i=0; i < allGroups.length; i++)
                {
                    allGroups[i].removeUser(user);
                    if(Configuration.coreOfficeAvailable())
                       	WorkflowFactory.removeParticipantToUsernameMapping(null, null, "ds_guid_".concat(allGroups[i].getGuid()), user.getName());
                }
            }
        }
    }
    
    
    /**
     * Dodaje u�ytkownikowi grup�, je�li u�ytkownik ju� jest w grupie nic nie robi
     * @param groupGuid
     * @param user
     * @throws EdmException 
     */
    public static void addUserGroup(String groupGuid, DSUser user) throws EdmException
    {
        if(groupGuid == null || groupGuid.isEmpty() || user == null)
        {
            log.error("Pusty guid lub user");
            return;
        }
        
        String[] userGroup = user.getGroupsGuid();
        for(String group : userGroup)
        {
            if(group.equalsIgnoreCase(groupGuid))
                return;
        }
        int groupLength = userGroup.length;
        String[] newUserGroup = new String[groupLength + 1];
        System.arraycopy(userGroup, 0, newUserGroup, 0, userGroup.length);
        newUserGroup[groupLength] = groupGuid;

        updateGroups(newUserGroup, user);
    }
    
    public static void updateOfficeRoles(Long[] officeRoleIds, DSUser user) throws EdmException 
    {
    	if (AvailabilityManager.isAvailable("officeRoles") && officeRoleIds != null)
        {
            Long[] roleIds = new Long[officeRoleIds.length];
            System.arraycopy(officeRoleIds, 0, roleIds, 0, roleIds.length);
            Arrays.sort(roleIds);

            Role[] officeRoles = Role.list();

            for (int i=0, n=officeRoles.length; i < n; i++)
            {
                if (Arrays.binarySearch(roleIds, officeRoles[i].getId()) >= 0)
                {
                    officeRoles[i].addUser(user.getName());
                    log.info("Dodaje role {} dla uzytkownika {}", officeRoles[i].getName(), user.getName());
                    if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                        try {
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_WSZYSTKIE_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), DSPermission.PISMO_WSZYSTKIE_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE.getName()))
                                JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_KOMORKA_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), DSPermission.PISMO_KOMORKA_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_KOMORKA_MODYFIKACJE.getName()))
                                JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), DSPermission.PISMO_KOMORKA_MODYFIKACJE.getName(), ObjectPermission.SOURCE_PERMISSION);
                            	

                        } catch (Exception e) { throw new EdmException(e); }
                    }
                }
                else
                {
                    officeRoles[i].removeUser(user.getName());
                    log.info("Usuwam role {} dla uzytkownika {}", officeRoles[i].getName(), user.getName());
                    if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
                        try {
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_WSZYSTKIE_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), DSPermission.PISMO_WSZYSTKIE_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE.getName()))
                                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_KOMORKA_PODGLAD.getName()))
                                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), DSPermission.PISMO_KOMORKA_PODGLAD.getName(), ObjectPermission.SOURCE_PERMISSION);
                            if(officeRoles[i].getPermissions().contains(DSPermission.PISMO_KOMORKA_MODYFIKACJE.getName()))
                                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), DSPermission.PISMO_KOMORKA_MODYFIKACJE.getName(), ObjectPermission.SOURCE_PERMISSION);
                        } catch (Exception e) { throw new EdmException(e); }
                    }
                }
            }

            // mapowania u�ytkownik�w w wewn�trznym workflow
            if (Configuration.coreOfficeAvailable())
            {

                // poniewa� nie mo�na umieszcza� u�ytkownik�w w g��wnym dziale,
                // mapowanie uczestnika workflow dotycz�ce tego dzia�u jest
				// tworzone
                // przy nadawaniu u�ytkownikowi uprawnienia
                // Permissions.PISMA_KO_PRZYJECIE
                if (DSApi.context().hasPermission(user, DSPermission.PISMA_KO_PRZYJECIE))
                {
                    WorkflowFactory.addParticipantToUsernameMapping(null, null,
                        "ds_guid_"+DSDivision.ROOT_GUID, user.getName());
                }
                else
                {
                    WorkflowFactory.removeParticipantToUsernameMapping(null, null,
                        "ds_guid_"+DSDivision.ROOT_GUID, user.getName());
                }

            }
        }
    }
    
    /**
	 * jezeli wartosc pod kluczem sklada sie tylko z oldSource to nie jest
	 * podmieniana na pusty string i zwracam true (wpis ma byc za chwile
	 * usuniety)
	 */
    public static boolean removeSource(Map map, Object key, String oldSource) throws EdmException {
    	if (map.containsKey(key)) {
    		String source = (String)map.get(key);
    		if (source == null) {
    			throw new EdmException("KeyNotFound");
    		}
    		String[] sources = StringUtils.split(source, ";");
    		boolean found = false;
    		String newSources = "";
    		for (String src : sources) {
    			if (src.equals(oldSource)) {
    				found = true;
    			} else {
    				newSources+=";"+src;
    			}
    		}
    		if (!found) {
    			return false;
    		}
    		if (newSources.length() > 0) {
    			if (newSources.toCharArray()[0] == ';') {
        			newSources = newSources.substring(1);
        		}
    		} else {
    			// mozna usunac
    			return true;
    		}
    		map.put(key, newSources);
    		return false;
    	} else {
    		throw new EdmException("KeyNotFound");
    	}
    }
   
    public static boolean addObjectWithSource(
    		Object object, 
    		String source, 
    		Object key, 
    		Set objects, 
    		Map sources, 
    		Object caller) throws HibernateException, EdmException {
    	if (sources != null && !sources.containsKey(key)) {
    		sources.put(key, source);
//    		objects.add(object);
//    		DSApi.context().session().flush();
//    		DSApi.context().session().refresh(caller);
//    		addSource(sources, key, source);
    		return true;
    	} else {
    		addSource(sources, key, source);
    		return false;
    	}
    }
     public static Map<Object, ArrayList<String>> getSources(Map src) {
    	 Map<Object, ArrayList<String>> result = new HashMap<Object, ArrayList<String>>();
    	 for (Entry<Object, String> entry : (Set<Entry<Object, String>>)src.entrySet()){
    		 String[] decodedSrcs = StringUtils.split(entry.getValue(), ";");
    		 result.put(entry.getKey(), new ArrayList<String>());
    		 for (String newSrc : decodedSrcs) {
    			 result.get(entry.getKey()).add(newSrc);
    		 }
    	 }
    	 return result;
     }
}
