package pl.compan.docusafe.web.common;

import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Sprawdzenie, czy has�o nie wygasa i ewentualne ostrze�enie
 * u�ytkownika.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PasswordValidityCheckAction.java,v 1.7 2009/03/06 14:39:11 pecet3 Exp $
 */
public class PasswordValidityCheckAction extends EventActionSupport
{
    private String url;
    private String what;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (url == null || url.length() == 0)
                url = "/";

            if ("1".equals(what))
            {
                event.setResult("expired");
            }
            else if ("2".equals(what))
            {
                event.setResult("mustChange");
            }
            else if ("warning".equals(what))
            {
                event.setResult("warning");
            }

            // przekierowanie do SUCCESS, czyli ${url}
        }
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getWhat()
    {
        return what;
    }

    public void setWhat(String what)
    {
        this.what = what;
    }
}
