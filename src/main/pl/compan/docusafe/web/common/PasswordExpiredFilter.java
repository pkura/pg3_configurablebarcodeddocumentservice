package pl.compan.docusafe.web.common;

import pl.compan.docusafe.core.GlobalConstants;

import javax.security.auth.Subject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PasswordExpiredFilter.java,v 1.4 2006/02/20 15:42:37 lk Exp $
 */
public class PasswordExpiredFilter implements Filter
{
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String uri = request.getRequestURI().substring(request.getContextPath().length());
        if (uri.length() == 0)
        {
            uri = "/";
        }
        else
        {
            if (uri.indexOf("//") >= 0)
                uri = uri.replaceAll("//+", "/");
        }

        Subject subject = (Subject) request.getSession(true).getAttribute(
            pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        // niezalogowany u�ytkownik nie powinien by� przekierowany tym
        // filtrem, bo w�wczas tworzy si� niesko�czony ci�g przekierowa�
        HttpSession session = request.getSession();
        if (subject != null &&
            session != null &&
            session.getAttribute(GlobalConstants.PASSWORD_EXPIRED) == Boolean.TRUE &&
            !uri.startsWith("/self-change-password.do") &&
            !uri.startsWith("/password-validity-check.action") &&
            !uri.startsWith("/logout.do"))
        {
            response.sendRedirect(request.getContextPath()+"/password-validity-check.action");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy()
    {
    }
}
