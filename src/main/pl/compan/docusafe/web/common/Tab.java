package pl.compan.docusafe.web.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Tab.java,v 1.3 2007/08/21 06:41:25 mwlizlo Exp $
 */
public class Tab
{
    private String name;
    private String title;
    private String link;
    private boolean selected;

    /**
     * Tworzy obiekt reprezentuj�cy zak�adk� zawieraj�cy jedynie w�asno�ci
     * name i title. Aby uzyska� obiekt z konkretnym odno�nikiem nale�y
     * u�y� metody {@link #getInstance(java.lang.String, boolean)} na
     * obiekcie utworzonym tym konstruktorem.
     */
    public Tab(String name, String title)
    {
        this.name = name;
        this.title = title;
    }

    public Tab(String name, String title, String link, boolean selected)
    {
        this.name = name;
        this.title = title;
        this.link = link;
        this.selected = selected;
    }

    public Tab getInstance(String link, boolean selected)
    {
        return new Tab(name, title, link, selected);
    }

    public String getName()
    {
        return name;
    }

    public String getTitle()
    {
        return title;
    }
   
    public String getLink()
    {
        return link;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public String toString()
    {
        return getName();
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Tab)) return false;

        final Tab tab = (Tab) o;

        if (name != null ? !name.equals(tab.name) : tab.name != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (name != null ? name.hashCode() : 0);
    }
}
