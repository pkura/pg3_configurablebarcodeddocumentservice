package pl.compan.docusafe.web.admin.doctype;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/* User: Administrator, Date: 2005-05-05 12:42:02 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditDoctypeAction.java,v 1.19 2008/11/30 12:42:18 wkuty Exp $
 */
public class EditDoctypeAction extends EventActionSupport
{
    //@EXPORT
    private List<Map<String, Object>> fields;
    private Integer documentCount;

    private String doctypeName;
    private Long id;
    private boolean inUse;
    private Map fieldNames = new HashMap();
    private Map fieldTypes = new HashMap();
    private Map fieldCns = new HashMap();
    private Map fieldLengths = new HashMap();
    private Map fieldOrder = new HashMap();
    private Map fieldIds = new HashMap();
    private Map deletedFieldIds = new HashMap();
    private String[] deleteIds;

    private String name;
    private String type;
    private Integer length;
    private String cn;
    private FormFile file;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSetName").
            append(OpenHibernateSession.INSTANCE).
            append(new SetName()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCommit").
            append(OpenHibernateSession.INSTANCE).
            append(new Commit()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPrepRelease").
            append(OpenHibernateSession.INSTANCE).
            append(new PrepRelease()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRelease").
            append(OpenHibernateSession.INSTANCE).
            append(new Release()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveXML").
        	append(OpenHibernateSession.INSTANCE).
        	append(new SaveXML()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);        
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                Doctype doctype = Doctype.find(id);
                doctypeName = doctype.getName();
                inUse = doctype.inUse();


                Doctype.Field[] doctypeFields = doctype.getFieldsAsArray();
                fields = new ArrayList<Map<String, Object>>(doctypeFields.length);
                for (int i=0; i < doctypeFields.length; i++)
                {
                    Map<String, Object> bean = new HashMap<String, Object>();
                    bean.put("id", doctypeFields[i].getId());
                    bean.put("cn", doctypeFields[i].getCn());
                    bean.put("name", doctypeFields[i].getName());
                    bean.put("type", doctypeFields[i].getType());
                    bean.put("length", doctypeFields[i].getLength());
                    fields.add(bean);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteIds == null || deleteIds.length == 0)
                addActionError("Nie wybrano p�l do usuni�cia");

        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            name = TextUtils.trimmedStringOrNull(name);
            type = TextUtils.trimmedStringOrNull(type);
            cn = TextUtils.trimmedStringOrNull(cn);

            if (name == null)
                addActionError("Nie podano nazwy pola");
            if (type == null)
                addActionError("Nie wybrano typu pola");
            if ("string".equals(type) && length == null)
                addActionError("Pole typu napis musi mie� okre�lon� d�ugo��");
            if ("string".equals(type) && length != null && (length.intValue() < 1 || length.intValue() > 100))
                addActionError("Pole typu napis musi mie� d�ugo�� wi�ksz� od zera i mniejsz� od stu");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Doctype doctype = Doctype.find(id);
                doctype.addField(cn, name, type, length, null);

                doctype.saveChanges();

                DSApi.context().commit();

                // czyszczenie formularza
                name = null;
                type = null;
                length = null;
                cn = null;
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (event.getLog().isDebugEnabled())
            {
                event.getLog().debug("fieldNames="+fieldNames);
                event.getLog().debug("fieldCns="+fieldCns);
                event.getLog().debug("fieldLengths="+fieldLengths);
                event.getLog().debug("fieldTypes="+fieldTypes);
                event.getLog().debug("fieldIds="+fieldIds);
                event.getLog().debug("deletedFieldIds="+deletedFieldIds);
            }

            // tablica fieldNames zawiera opisy p�l (indeksy s� generowane
            // automatycznie w JS, nie s� nigdzie zapami�tywane)
            // tablica fieldLengths zawiera d�ugo�ci p�l
            // tablica fieldOrder zawiera numer kolejne p�l tak, jak
            // wyst�powa�y one na stronie JSP
            // elementy tablic wi�zane s� warto�ciami ich kluczy

            if (fieldNames == null || fieldNames.size() == 0 ||
                fieldCns == null || fieldCns.size() == 0 ||
                fieldTypes == null || fieldTypes.size() == 0 ||
                fieldLengths == null || fieldLengths.size() == 0 ||
                fieldOrder == null || fieldOrder.size() == 0)
                return;

            try
            {
                DSApi.context().begin();

                Doctype doctype = Doctype.find(id);

                // sortowanie tablicy fieldOrder po warto�ciach entry.value
                // na podstawie posortowanych warto�ci w odpowiedniej
                // kolejno�ci wyci�gane b�d� elementy fieldNames i fieldLengths

                List entries = new ArrayList(fieldOrder.entrySet());
                Collections.sort(entries, new Comparator()
                {
                    public int compare(Object o1, Object o2)
                    {
                        if (o1 instanceof Map.Entry && o2 instanceof Map.Entry)
                        {
                            Map.Entry e1 = (Map.Entry) o1;
                            Map.Entry e2 = (Map.Entry) o2;
                            try
                            {
                                return (e1.getValue() instanceof String[] &&
                                    ((String[]) e1.getValue()).length > 0 &&
                                    e2.getValue() instanceof String[] &&
                                    ((String[]) e2.getValue()).length > 0) ?
                                    Integer.parseInt(((String[]) e1.getValue())[0]) -
                                    Integer.parseInt(((String[]) e2.getValue())[0]) : 0;
                            }
                            // NumberFormatException, NPE
                            catch (Exception e)
                            {
                                return 0;
                            }
                        }
                        return 0;
                    }
                });

                // doctype.clearFields();

                for (Iterator iter=fieldNames.entrySet().iterator(); iter.hasNext(); )
                {
                    Map.Entry e = (Map.Entry) iter.next();
                    Integer fieldId = (Integer) e.getKey();
                    String fieldName = TextUtils.trimmedStringOrNull((String) ((Object[]) e.getValue())[0]);
                    String fieldType = TextUtils.trimmedStringOrNull((String) ((Object[]) fieldTypes.get(fieldId))[0]);
                    String sFieldLength = TextUtils.trimmedStringOrNull((String) ((Object[]) fieldLengths.get(fieldId))[0]);
                    String fieldCn = TextUtils.trimmedStringOrNull((String) ((Object[]) fieldCns.get(fieldId))[0]);
                    Integer fieldLength = null;
                    if (sFieldLength != null)
                    {
                        try
                        {
                            fieldLength = new Integer(sFieldLength);
                        }
                        catch (Exception ex)
                        {
                            throw new EdmException("Nieprawid�owa d�ugo�� pola "+fieldName);
                        }
                    }

                    doctype.updateField(fieldId, fieldCn, fieldName, fieldType, fieldLength);
                }

                doctype.saveChanges();

/*
                for (Iterator iter=entries.iterator(); iter.hasNext(); )
                {
                    Map.Entry entry = (Map.Entry) iter.next();
                    Object id = entry.getKey();

                    if (!(fieldNames.get(id) instanceof String[]) ||
                        ((String[]) fieldNames.get(id)).length < 1 ||
                        !(fieldTypes.get(id) instanceof String[]) ||
                        ((String[]) fieldTypes.get(id)).length < 1 ||
                        !(fieldLengths.get(id) instanceof String[]) ||
                        ((String[]) fieldLengths.get(id)).length < 1)
                    {
                        if (event.getLog().isDebugEnabled())
                        {
                            event.getLog().debug("fieldNames("+id+")="+fieldNames.get(id)+
                                ", pomijanie");
                            event.getLog().debug("fieldLengths("+id+")="+fieldLengths.get(id)+
                                ", pomijanie");
                        }
                        continue;
                    }

                    String sFieldId = TextUtils.trimmedStringOrNull(((String[]) fieldIds.get(id))[0]);
                    Integer fieldId = sFieldId != null ? Integer.valueOf(sFieldId) : null;

                    String name = ((String[]) fieldNames.get(id))[0];
                    if (StringUtils.isEmpty(name))
                        continue;

                    String[] _cn = (String[]) fieldCns.get(id);
                    String cn = TextUtils.trimmedStringOrNull(_cn != null && _cn.length > 0 ? _cn[0] : null);

                    String type = ((String[]) fieldTypes.get(id))[0];
                    if (StringUtils.isEmpty(type))
                        continue;

                    Integer length = null;
                    if ("string".equals(type))
                        length = new Integer(((String[]) fieldLengths.get(id))[0]);

                    doctype.addField(fieldId, cn, name, type, length);
                }
*/

                DSApi.context().commit();

                addActionMessage("Zapisano zmiany");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class SetName implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(doctypeName))
                addActionError("Nie podano nowej nazwy typu");

            try
            {
                DSApi.context().begin();

                Doctype doctype = Doctype.find(id);
                doctype.setName(StringUtils.trim(doctypeName));

                DSApi.context().commit();

                addActionMessage("Zapisano zmiany");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Commit implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Doctype doctype = Doctype.find(id);

                if (doctype.inUse())
                    throw new EdmException("Typ dokumentu jest u�ywany, nie mo�na zatwierdzi� zmian");

                doctype.commit();

                DSApi.context().commit();

                addActionMessage("Zapisano zmiany");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class PrepRelease implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("release");

            try
            {
                doctypeName = Doctype.find(id).getName();
                documentCount = new Integer(Doctype.find(id).getDocumentCount());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Release implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("list");

            try
            {
                DSApi.context().begin();

                Doctype.find(id).release();

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    
    
    private class SaveXML implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if(file==null){
        				addActionError("Brak pliku xml");
        				return;
        			}
        		if(!file.sensible()){
    				addActionError("Nieprawid�owy plik xml");
    				return;
    			}
        		try{
        			FileInputStream inputStream = new FileInputStream(file.getFile());
        			PreparedStatement ps = null;
                    ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(Doctype.class)+
                        " set content = ? where id = ?");
                    ps.setBinaryStream(1,inputStream, (int)file.getFile().length());
                    ps.setLong(2, getId().longValue());
                    int rows = ps.executeUpdate();
                    DSApi.context().closeStatement(ps);
                    if (rows != 1)
                        throw new EdmException("Nie uda�o si� zapisa� danych typu dokumentu");
        		}
        		catch (FileNotFoundException ex)
        		{
        			throw new EdmException(ex.getMessage(),ex);
        		}
        		catch(SQLException ex)
        		{
        			throw new EdmException(ex.getMessage(),ex);
        		}
        	}
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                //if(hasActionErrors())
            }
        }
    }
    
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDoctypeName()
    {
        return doctypeName;
    }

    public void setDoctypeName(String doctypeName)
    {
        this.doctypeName = doctypeName;
    }

    public List getFields()
    {
        return fields;
    }

    public Map getFieldNames()
    {
        return fieldNames;
    }

    public void setFieldNames(Map fieldNames)
    {
        this.fieldNames = fieldNames;
    }

    public Map getFieldLengths()
    {
        return fieldLengths;
    }

    public void setFieldLengths(Map fieldLengths)
    {
        this.fieldLengths = fieldLengths;
    }

    public Map getFieldOrder()
    {
        return fieldOrder;
    }

    public void setFieldOrder(Map fieldOrder)
    {
        this.fieldOrder = fieldOrder;
    }

    public boolean isInUse()
    {
        return inUse;
    }

    public Map getFieldIds()
    {
        return fieldIds;
    }

    public void setFieldIds(Map fieldIds)
    {
        this.fieldIds = fieldIds;
    }

    public Map getDeletedFieldIds()
    {
        return deletedFieldIds;
    }

    public void setDeletedFieldIds(Map deletedFieldIds)
    {
        this.deletedFieldIds = deletedFieldIds;
    }

    public Integer getDocumentCount()
    {
        return documentCount;
    }

    public Map getFieldTypes()
    {
        return fieldTypes;
    }

    public void setFieldTypes(Map fieldTypes)
    {
        this.fieldTypes = fieldTypes;
    }

    public Map getFieldCns()
    {
        return fieldCns;
    }

    public void setFieldCns(Map fieldCns)
    {
        this.fieldCns = fieldCns;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Integer getLength()
    {
        return length;
    }

    public void setLength(Integer length)
    {
        this.length = length;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public void setDeleteIds(String[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }
    
    public void setFile(FormFile file)
    {
        this.file = file;
    }   
    public FormFile getFile()
    {
    	return file;
    }   
}
