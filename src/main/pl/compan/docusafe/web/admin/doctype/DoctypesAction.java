package pl.compan.docusafe.web.admin.doctype;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.webwork.event.*;

import java.util.*;

/* User: Administrator, Date: 2005-05-04 13:05:07 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DoctypesAction.java,v 1.5 2006/02/20 15:42:34 lk Exp $
 */
public class DoctypesAction extends EventActionSupport
{
    // @EXPORT
    private List<Map> doctypes;
    private String name;

    private Long[] deleteIds;
    private Long[] enableDoctypeIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                List allDoctypes = Doctype.list();
                doctypes = new ArrayList<Map>(allDoctypes.size());
                for (Iterator iter=allDoctypes.iterator(); iter.hasNext(); )
                {
                    Doctype doctype = (Doctype) iter.next();
                    Map<String, Object> bean = new HashMap<String, Object>();
                    bean.put("id", doctype.getId());
                    bean.put("name", doctype.getName());
                    bean.put("enabled", Boolean.valueOf(doctype.isEnabled()));
                    bean.put("inUse", Boolean.valueOf(doctype.inUse()));
                    doctypes.add(bean);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy tworzonego typu");

            if (hasActionErrors())
                return;

            name = StringUtils.trim(name);

            try
            {
                DSApi.context().begin();
                try
                {
                    Doctype.findByName(name);
                    throw new EdmException("Istnieje ju� typ dokumentu o nazwie "+name);
                }
                catch (EntityNotFoundException e)
                {
                }

                Doctype doctype = new Doctype(name);
                doctype.create();

                name = null; // czyszczenie formularza

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteIds == null || deleteIds.length == 0)
            {
                addActionError("Nie wybrano typ�w dokument�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < deleteIds.length; i++)
                {
                    Doctype.find(deleteIds[i]).delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
//            if (enableDoctypeIds == null || enableDoctypeIds.length == 0)
//            {
//                addActionError("Nie wybrano typ�w dokument�w do zmiany");
//                return;
//            }

            if (enableDoctypeIds == null)
                enableDoctypeIds = new Long[0];
            
            Arrays.sort(enableDoctypeIds);

            try
            {
                DSApi.context().begin();

                List doctypes = Doctype.list();

                for (Iterator iter=doctypes.iterator(); iter.hasNext(); )
                {
                    Doctype doctype = (Doctype) iter.next();
                    doctype.setEnabled(Arrays.binarySearch(enableDoctypeIds, doctype.getId()) >=0);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getDoctypes()
    {
        return doctypes;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long[] getDeleteIds()
    {
        return deleteIds;
    }

    public void setDeleteIds(Long[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }

    public Long[] getEnableDoctypeIds()
    {
        return enableDoctypeIds;
    }

    public void setEnableDoctypeIds(Long[] enableDoctypeIds)
    {
        this.enableDoctypeIds = enableDoctypeIds;
    }
}
