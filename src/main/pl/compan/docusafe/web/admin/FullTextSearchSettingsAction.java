package pl.compan.docusafe.web.admin;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.gson.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.parametrization.archiwum.PublicDocument;
import pl.compan.docusafe.parametrization.archiwum.services.AktaPubliczneService;
import pl.compan.docusafe.parametrization.archiwum.services.ListaDokumentowPublicznychResponse;
import pl.compan.docusafe.parametrization.archiwum.services.PublicDocumentsParameters;
import pl.compan.docusafe.service.IterateTimerTask;
import pl.compan.docusafe.service.lucene.NewLuceneService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.RichActionListener;
import pl.compan.docusafe.webwork.event.RichEventActionSupport;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FullTextSearchSettingsAction extends RichEventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(FullTextSearchSettingsAction.class);

    public static final String DEFAULT_INDEX_FOLDER = "indexes_akta_publiczne";

    @Override
    protected void setup() {
        hibernateListener(new GenerateDocumentIndex());
        hibernateListener(new GenerateAktaPubliczneIndex());
        hibernateListener(new SearchAkta());
    }

    private Boolean isShowIndexingErrors() {
        return AvailabilityManager.isAvailable("fulltextSearch.manualSearch.showErrors");
    }

    private class GenerateDocumentIndex extends RichActionListener {
        @Override
        public void action(final ActionEvent event) throws Exception {

            final Boolean showIndexingErrors = isShowIndexingErrors();

            IterateTimerTask<Document> task = new NewLuceneService.Task() {
                @Override
                protected void onIterationError(Exception ex, int i) {
                    if(showIndexingErrors) {
                        String message = ex.getMessage();
                        message = StringUtils.isNotBlank(message) ? ": "+message : "";
                        event.addActionError("B��d podczas indeksowania pisma"+message);
                    }
                }
            };

            task.run();

            addActionMessage("Utworzono indeks pism");
        }
    }

    private class GenerateAktaPubliczneIndex extends RichActionListener {

        private IndexWriter indexWriter;
        private FSDirectory index;

        @Override
        public void action(ActionEvent event) throws Exception {
            try {

                String indexPath = Docusafe.getAdditionPropertyOrDefault("fulltext.documentsIndexFolder", "indexes_akta_publiczne");
                final File fileIndexes = new File(Docusafe.getHome(), indexPath);
                fileIndexes.mkdir();

                index = FSDirectory.open(fileIndexes);

                IndexHelper indexHelper = new IndexHelper();
                StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);


                IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_46, analyzer);
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
                indexWriter = new IndexWriter(index, iwc);

                String queryString = "from " + PublicDocument.class.getSimpleName() + " pd, " + Document.class.getSimpleName() + " d where d.id = pd.documentId";

                log.debug("[action] queryString = {}", queryString);

                final Query query = DSApi.context().session().createQuery(queryString);

                final Iterator<Object[]> it = query.iterate();

                while(it.hasNext()) {
                    final Object[] result = it.next();

                    final PublicDocument publicDocument = (PublicDocument)result[0];
                    final Document document = (Document)result[1];

                    log.debug("doc.id = {}", publicDocument.getDocumentId());

                    org.apache.lucene.document.Document luceneDoc = new org.apache.lucene.document.Document();

                    Date ctime = publicDocument.getCtime();
                    luceneDoc.add( new StringField( "documentId", publicDocument.getDocumentId().toString(), Field.Store.YES) );
                    luceneDoc.add( new StringField( "author", Strings.nullToEmpty(publicDocument.getAuthor()), Field.Store.YES) );
                    luceneDoc.add( new LongField( "ctime", ctime == null ? 0L : ctime.getTime(), Field.Store.NO) );
                    luceneDoc.add( new StringField( "title", Strings.nullToEmpty(publicDocument.getTitle()), Field.Store.YES) );
                    luceneDoc.add( new StringField( "abstrakt", Strings.nullToEmpty(publicDocument.getAbstrakt()), Field.Store.YES) );
                    luceneDoc.add( new StringField( "location", Strings.nullToEmpty(publicDocument.getLocation()), Field.Store.YES) );
                    luceneDoc.add( new StringField( "type", Strings.nullToEmpty(publicDocument.getType()), Field.Store.YES) );
                    luceneDoc.add( new StringField( "organizationalUnit", Strings.nullToEmpty(publicDocument.getOrganizationalUnit()), Field.Store.YES) );

                    String text = NewLuceneService.getTextFromAttachments(document, null);
                    InputStream stream = IOUtils.toInputStream(text);

                    luceneDoc.add(new TextField("content", new BufferedReader(new InputStreamReader(stream, "ISO-8859-2"))));

//                String contentField = "ID: "+ doc.getDocumentId() + ", " +
//                        "Autor: "+ doc.getAuthor() + ", " +
//                        "Tytu�: "+ doc.getTitle() + ", " +
//                        "Czas utworzenia: "+ doc.getCtime() + ", " +
//                        "Abstrakt: "+ doc.getAbstrakt() + ", " +
//                        "Lokalizacja: "+ doc.getLocation() + ", " +
//                        "Typ: "+ doc.getType() + ", " +
//                        "Organizational Unit: "+ doc.getOrganizationalUnit();
//                luceneDoc.add( new Field( "content", contentField, Field.Store.YES, Field.Index.ANALYZED ) );

                    if(indexWriter.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
                        indexWriter.addDocument(luceneDoc);
                    } else {
                        indexWriter.updateDocument(new Term("documentId", publicDocument.getDocumentId().toString()), luceneDoc);
                    }

                }
            } catch(Exception ex) {
                log.error("[GenerateAktaPubliczneIndex] error", ex);

                if(isShowIndexingErrors()) {
                    String message = ex.getMessage();
                    message = StringUtils.isNotBlank(message) ? ": "+message : "";
                    event.addActionError("B��d podczas indeksowania Akt Publicznych"+message);
                }
            }

            addActionMessage("Utworzono indeksy akt publicznych");
        }

        @Override
        public void onComplete() throws Exception {
            if(indexWriter != null) {
                indexWriter.close();
            }

            if(index != null) {
                index.close();
            }
        }

        @Deprecated
        private InputStream getFirstAttachmentStream(Document document) throws EdmException {

            log.debug("[getFirstAttachmentStream] document.id = {}", document.getId());

            final List<Attachment> attachments = document.getAttachments();

            if(attachments.size() == 0) {
                log.warn("[getTextFromAttachments] no attachments");
                return null;
            }

            for(Attachment attachment: attachments) {
                for(AttachmentRevision rev: attachment.getRevisions()) {
                    log.trace("[getFirstAttachmentStream]     rev.id = {}, rev.mime = {}", rev.getId(), rev.getMime());
                    if(Objects.equal(rev.getMime(), "application/pdf")) {
                        log.trace("[getFirstAttachmentStream]     return");
                        try {
                            return rev.getBinaryStream();
                        } catch(Exception ex) {
                            log.error("[getFirstAttachmentStream]     error, returning null", ex);
                            return null;
                        }
                    }
                }
            }

            log.warn("[getTextFromAttachments] stream not found");
            return null;
        }
    }

    private class SearchAkta extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {

            JsonObject ret = new JsonObject();

            final AktaPubliczneService service = new AktaPubliczneService();

            final PublicDocumentsParameters params = new PublicDocumentsParameters();
            params.setFullTextOnly(true);

            String author = getParameter("author");

            if(author != null) {
                params.setAuthor(author);
            }

            String query = getParameter("query");
            if(query != null) {
                params.setFullText(query);
            }

            //            final ListaDokumentowPublicznychResponse result = service.wyszukiwanieOgolneDokumentow(0, 10, params);

            Long indexFrom = getLongParameter("from");
            Long indexTo = getLongParameter("to");

            indexFrom = indexFrom == null ? 0 : indexFrom;
            indexTo = indexTo == null ? 10 : indexTo;

            log.debug("indexFrom = {}, indexTo = {}", indexFrom, indexTo);

            final ListaDokumentowPublicznychResponse result = service.wyszukiwanieOgolneDokumentow(indexFrom.intValue(), indexTo.intValue(), params);

            log.debug("status = {}", result.getStatusMessage());
            ret.addProperty("status", result.getStatusMessage());

            JsonArray arr = new JsonArray();

            final PublicDocument[] docs = result.getPublicDocuments();

            if(docs != null) {
                for(PublicDocument doc: docs) {
                    //                    arr.add(new JsonPrimitive(doc.toStringLong()));

                    //                    String text = doc.getDocumentId() + ", " + doc.getAuthor() + ", " + doc.getTitle();

                    JsonObject obj = new JsonObject();

                    obj.addProperty("id", doc.getDocumentId());
                    obj.addProperty("author", doc.getAuthor());
                    obj.addProperty("title", doc.getTitle());

                    //                    arr.add(new JsonPrimitive(text));
                    arr.add(obj);
                }
            }

            ret.add("size", new JsonPrimitive(arr.size()));
            ret.add("data", arr);

            // index search
            //            JsonArray results = new JsonArray();
            //            String query = getParameterOrError("query");
            //
            //            final File fileIndexes = new File(Docusafe.getHome(), "indexes_pg");
            //            FSDirectory index = FSDirectory.open(fileIndexes);
            //            StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_29);
            //
            //            Query q = new QueryParser(Version.LUCENE_29, IndexHelper.CONTENT_FIELD, analyzer).parse(query);
            //
            //            log.debug("query = {}", q);
            //
            //            IndexSearcher searcher = new IndexSearcher(index, true);
            //            TopScoreDocCollector collector = TopScoreDocCollector.create(10, true);
            //            searcher.search(q, collector);
            //            ScoreDoc[] hitsDoc = collector.topDocs().scoreDocs;
            //
            //            log.debug("hitsDoc.length = {}", hitsDoc.length);
            //
            //            for (ScoreDoc aHitsDoc : hitsDoc) {
            //                int docId = aHitsDoc.doc;
            //                org.apache.lucene.document.Document d = searcher.doc(docId);
            //
            ////                String resultText = d.get("test") + ", " + d.get("myField") + ", " + d.get("pro");
            //                String resultText = d.get("content");
            //                results.add(new JsonPrimitive(resultText));
            //            }
            //
            //            // zamykamy searchera
            //            searcher.close();

            //            ret.add("results", results);

            return ret;
        }
    }

}
