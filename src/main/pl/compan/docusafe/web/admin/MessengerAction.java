package pl.compan.docusafe.web.admin;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.tools.messenger.remote.RemoteAnswerMessage;
import pl.compan.docusafe.tools.messenger.server.ServerCore;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:piotr.komisarski@com-pan.pl">Piotr Komisarski</a>
 */
public class MessengerAction extends EventActionSupport {

    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(MessengerAction.class.getPackage().getName(),null);
    
    private static String port;

    private List<DSUser> users;

    private String title;
    //oznacza, czy na widoku sa ustawienia czy wysylanie wiadomosci
    private boolean settingsOnView;

    

    private String message;
    private String[] usernames;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSend").append(OpenHibernateSession.INSTANCE).append(new Send()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").append(OpenHibernateSession.INSTANCE).append(new Save()).append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);
    }


    private class FillForm implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            try {
                port = String.valueOf(ServerCore.getPort());
                users = DSUser.list(0);
            }
            catch (EdmException edme) {
            }
        }
    }

    private class Send implements ActionListener {

        public void actionPerformed(ActionEvent event) {

            if (usernames == null || usernames.length == 0) {
                return;
            }
            for (String str : usernames) {
                ServerCore.pushMessage("Administrator", str, title, message, RemoteAnswerMessage.INFO);
            }
            event.addActionMessage(sm.getString("Wyslano"));
        }
    }

    private class Save implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            
            int tmp;
            try{
                tmp = Integer.parseInt(port);
                ServerCore.setPort(tmp);
                event.addActionMessage(sm.getString("Zapisano"));
            }
            catch(NumberFormatException nfe)
            {
                addActionError(sm.getString("NieprawidlowyPort"));
            }
        }
    }

    public List<DSUser> getUsers() {
        return users;
    }

    public void setUsernames(String[] usernames) {
        this.usernames = usernames;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSettingsOnView() {
        return settingsOnView;
    }

    public void setSettingsOnView(boolean settings) {
        this.settingsOnView = settings;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        MessengerAction.port = port;
    }
}