package pl.compan.docusafe.web.admin;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.OfficePoint;
import pl.compan.docusafe.core.base.OfficePointPrefix;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa dla obs�ugi punktow kanbcelaryjnych 
 tabela w bazie DSO_OFFICE_POINT
@author grzegorz.filip@docusafe.pl
 */


public class OfficePointsAction extends EventActionSupport {

	
	
	
	private static Logger log = LoggerFactory.getLogger(UserToJasperReportsAction.class);

	private List<OfficePointPrefix>  prefiksList = new ArrayList<OfficePointPrefix>();
	private List<ValueToList> list_user_to_jasper_report;
	private List<ValueToList> divisions;
	private List<ValueToList> avaialblePrefiks;
	private String selectetDefiniedPoint;
	private String usersList;
	private String newfolderName;
	private String sortUsersList;
	private String selectedDivisionId;
	private Boolean canView = false;
	private Boolean sorted = false;
	private Map<Integer, String> uprawnienia;
	private List<ValueToList> listOfficePoints;
	private Map<Long, String> reportMap;
	private String selectedPrefix;
	
	@Override
	protected void setup() {
		// TODO Auto-generated method stub
	
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAdd").append(OpenHibernateSession.INSTANCE)
				.append(new Add()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDelete").append(OpenHibernateSession.INSTANCE)
				.append(new Delete()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);
		
	}




	private class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				if (!sorted) {
					if (!DSApi.isContextOpen())
						DSApi.openContextIfNeeded();
					else
						DSApi.beginTransacionSafely();
					prefiksList = getPrefiksList();
					avaialblePrefiks =  setupAvaialblePrefix();
				
					DSDivision[] divfinded = DSDivision.getOnlyDivisions(false);
					divisions = new ArrayList<ValueToList>(divfinded.length);
					for (DSDivision dsDivision : divfinded) {
					
						String name = dsDivision.getName();
						Long id = dsDivision.getId();
						divisions.add(new ValueToList(name, id.toString(), id));
					} 

					listOfficePoints = new ArrayList<ValueToList>();
					List<OfficePoint> list = getOfficePointsList();
					for (OfficePoint op : list) {
						Long id = op.getId();
						Long reportId = op.getId();
						String folderName  = op.getFolderName();
						String prefix  = op.getPrefix();
						DSDivision div = DSDivision.findById(op.getDivisionId());
						
						listOfficePoints.add(new ValueToList("Folder " + prefix + folderName + "  jest  przypisany do Dzia�u -" + div.getName(), id.toString(), reportId));
						
						
					}

					if (DSApi.context().inTransaction())
						DSApi.context().commit();
				}
			} catch (EdmException e) {
				log.error("", e);
			}
		}

		
	}

	 private class Delete implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent event) {
				
	        	try
	        	{
	        		if (!DSApi.isContextOpen())
	        			DSApi.openAdmin();
	        		else
	        			DSApi.beginTransacionSafely();
	        		
	        		if (selectetDefiniedPoint != null){
	        			
	        			OfficePoint op = findOfficePointbyId(Long.valueOf(selectetDefiniedPoint));
	        			op.setDeleted(true);
	        			DSApi.context().session().save(op);
	        			
	        			OfficePointPrefix opPref = OfficePointPrefix.findById(op.getPrefixId());
	        			opPref.setAvailable(true);
	        			DSApi.context().session().save(opPref);
	        			
	        			selectetDefiniedPoint = null;
	        		}else{
	        			  addActionError("Nie wybrano pozycji do usuni�cia");
	        		}      	
	        	}
	        	catch (EdmException e) 
	        	{
	        		log.error("", e);
				}
			}

			
		}
		

	private class Add implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				if (!DSApi.isContextOpen())
					DSApi.openAdmin();
				else
					DSApi.beginTransacionSafely();
				
				
				if(selectedDivisionId==null || selectedPrefix ==null || newfolderName==null){
					 addActionError("Nie wybrano wszystkich danych przy tworzeniu punktu kancelaryjnego");
				}else {
				long divisionId = Long.parseLong(selectedDivisionId);
				long selectedPref = Long.parseLong(selectedPrefix);
				String folderName = newfolderName;

				OfficePointPrefix opPref = OfficePointPrefix.findById(Long.valueOf(selectedPref));
				opPref.setAvailable(Boolean.FALSE);
				DSApi.context().session().save(opPref);
				
				OfficePoint op = new OfficePoint();
				op.setDeleted(false);
				op.setDivisionId(divisionId);
				op.setPrefix(opPref.getPrefixName());
				op.setPrefixId(opPref.getId());
				op.setFolderName(folderName);
				DSApi.context().session().save(op);
				
				DSApi.context().commit();

				selectedDivisionId = null;
				selectedPrefix = null;
				newfolderName = null;
				}
			} catch (EdmException e) {
				log.error("", e);
			}
		}

	}

	@SuppressWarnings("unused")
	public class ValueToList {
		private String value;
		private String key;
		private long reportId;

		public ValueToList() {
			value = "";
			key = "";
			reportId = 0;
		}

		public ValueToList(String value, String key, Long reportId) {
			this.value = value;
			this.key = key;
			this.reportId = reportId;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		public void setReportId(long reportId) {
			this.reportId = reportId;
		}

		public long getReportId() {
			return reportId;
		}
	}
	private List<OfficePoint> getOfficePointsList() throws EdmException {
		Criteria crit = DSApi.context().session().createCriteria(OfficePoint.class);
		crit.add(Restrictions.eq("deleted", false));
		List<OfficePoint> list = crit.list();
		return list;
		
	}
	public List<ValueToList> setupAvaialblePrefix() throws EdmException {
		
		avaialblePrefiks = new ArrayList<ValueToList>();
		
		Criteria crit = DSApi.context().session().createCriteria(OfficePointPrefix.class);
		crit.add(Restrictions.eq("available", Boolean.TRUE));
		crit.addOrder(Order.asc("id"));
		List<OfficePointPrefix> prefixList = crit.list();
		for (OfficePointPrefix pref : prefixList) {
			avaialblePrefiks.add(new ValueToList(pref.getPrefixName(), String.valueOf(pref.getId()), pref.getId()));
		}
		return avaialblePrefiks ; 
	}
	private List<OfficePointPrefix> getPrefiksList() throws EdmException {
		Criteria crit = DSApi.context().session().createCriteria(OfficePointPrefix.class);
		List <OfficePointPrefix> lst = crit.list();
		prefiksList.addAll(lst);
	
	 return prefiksList;

	}
	private OfficePoint findOfficePointbyId(Long officePointId) throws EdmException {
		Criteria crit = DSApi.context().session().createCriteria(OfficePoint.class);
		crit.add(Restrictions.eq("id", officePointId));
		if (!crit.list().isEmpty())
			return (OfficePoint) crit.list().get(0);
		else
			return null;
	}
	public List<ValueToList> getList_user_to_jasper_report() {
		return list_user_to_jasper_report;
	}
	public void setList_user_to_jasper_report(
			List<ValueToList> list_user_to_jasper_report) {
		this.list_user_to_jasper_report = list_user_to_jasper_report;
	}
	public List<ValueToList> getDivisions() {
		return divisions;
	}
	public void setDivisions(List<ValueToList> divisions) {
		this.divisions = divisions;
	}
	public List<ValueToList> getAvaialblePrefiks() {
		return avaialblePrefiks;
	}
	public void setAvaialblePrefiks(List<ValueToList> avaialblePrefiks) {
		this.avaialblePrefiks = avaialblePrefiks;
	}
	public String getSelectetDefiniedPoint() {
		return selectetDefiniedPoint;
	}
	public void setSelectetDefiniedPoint(String selectetDefiniedPoint) {
		this.selectetDefiniedPoint = selectetDefiniedPoint;
	}
	public String getUsersList() {
		return usersList;
	}
	public void setUsersList(String usersList) {
		this.usersList = usersList;
	}
	public String getNewfolderName() {
		return newfolderName;
	}
	public void setNewfolderName(String newfolderName) {
		this.newfolderName = newfolderName;
	}
	public String getSelectedDivisionId() {
		return selectedDivisionId;
	}
	public void setSelectedDivisionId(String selectedDivisionId) {
		this.selectedDivisionId = selectedDivisionId;
	}
	public List<ValueToList> getListOfficePoints() {
		return listOfficePoints;
	}
	public void setListOfficePoints(List<ValueToList> listOfficePoints) {
		this.listOfficePoints = listOfficePoints;
	}
	public Map<Long, String> getReportMap() {
		return reportMap;
	}
	public void setReportMap(Map<Long, String> reportMap) {
		this.reportMap = reportMap;
	}
	public String getSortUsersList() {
		return sortUsersList;
	}
	public void setSortUsersList(String sortUsersList) {
		this.sortUsersList = sortUsersList;
	}
	public void setPrefiksList(List<OfficePointPrefix> prefiksList) {
		this.prefiksList = prefiksList;
	}
	public String getSelectedPrefix() {
		return selectedPrefix;
	}
	public void setSelectedPrefix(String selectedPrefix) {
		this.selectedPrefix = selectedPrefix;
	}
	

	
	
}
