package pl.compan.docusafe.web.admin.employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje zwi�zane z wy�wietlaniem i edycj� listy kart
 * pracownik�w, powi�zana jest z widokiem /admin/employee-card-list.action
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeCardListAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeCardListAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager sm = StringManager
			.getManager(EmployeeCardListAction.class.getPackage().getName());
	
	/**
	 * Lista kart pracownik�w
	 */
	private List<EmployeeCard> employeeCards;
	
	/**
	 * Tablica identyfikator�w kart pracownik�w do usuni�cia
	 */
	private Long[] deleteIds;
	
	/**
	 * Obiekt karty u�ytkownika wykorzystywany do budowy zapytania wyszukiwania kart pracownik�w
	 */
	private EmployeeCard empCard;
	
	/**
	 * Ci�g okre�laj�cy dat� startow� zatrudnienia dla wyszukiwarki
	 */
	private String employmentStartDate;
	
	/**
	 * Ci�g okre�laj�cy dat� zwolnienia dla wyszukiwarki
	 */
	private String employmentEndDate;
	
	/**
	 * Czy wy�wietli� jedynie zatrudnionych
	 */
	private Boolean onlyEmployed = true;
	
	/**
	 * Numer strony
	 */
	private int pageNumber = 0;
	
	/**
	 * Liczba rezultat�w
	 */
	private int maxResults = 20;
	
	/**
	 * Obiekt pager'a
	 */
	private Pager pager;
	
	/**
	 * Pole sortowania
	 */
	private String sortField = "lastName";
	
	/**
	 * Typ sortowania
	 */
	private boolean ascending = true;

	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		LoadEmployeeCardList loadEmployeeCardList = new LoadEmployeeCardList();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(loadEmployeeCardList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doFilter")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ActionListener() 
			{
				public void actionPerformed(ActionEvent event) 
				{
					// ustawienie numeru strony na 0
					pageNumber = 0;
				}
			})
			.append(loadEmployeeCardList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("deleteEmployeeCards")
			.append(OpenHibernateSession.INSTANCE)
			.append(new DeleteEmployeeCards())
			.append(loadEmployeeCardList)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Klasa implementuj�ca akcje za�adowania listy kart pracownik�w.
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadEmployeeCardList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				Map<String, String> params = new HashMap<String, String>();
				
				if (empCard == null)
					empCard = new EmployeeCard();
				
				String searchQuery = buildSearchQuery(params);
				String hql = "select emp from " + EmployeeCard.class.getName() + " emp "
					+ searchQuery + " order by emp." + sortField + " " + (ascending ? "asc" : "desc");
				
				// lista kart pracownik�w
				employeeCards = createQueryObject(hql, params)
					.setFirstResult(pageNumber)
					.setMaxResults(maxResults)
					.list();
				setNamesForDivisions(employeeCards);
				// utworzenie pagera
				createPager(searchQuery, params);
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * 
		 * Methods set up name of division in all employee cards.  
		 * 
		 * @param employeeCards
		 * @throws DivisionNotFoundException
		 * @throws NumberFormatException
		 * @throws EdmException
		 */
		private void setNamesForDivisions(List<EmployeeCard> employeeCards) throws DivisionNotFoundException, NumberFormatException, EdmException {
			for (EmployeeCard employeeCard : employeeCards) {
				String divisionName = employeeCard.getDivisionNameByDivisionId();
				employeeCard.setDivisionName(divisionName);
			}
			
		}

		/**
		 * Tworzy pager
		 * 
		 * @param searchQuery
		 * @param params
		 */
		private void createPager(String searchQuery, Map<String, String> params)
			throws EdmException
		{
			// pobranie ca�kowitej ilo�ci wynik�w zapytania
			String hql = "select count(emp.id) from " + EmployeeCard.class.getName() 
				+ " emp " + searchQuery;
			Long count = (Long) createQueryObject(hql, params).uniqueResult();
			
			pager = new Pager(new Pager.LinkVisitor() {
				public String getLink(int offset) 
				{
					return offset + "";
				}
			}, pageNumber, maxResults, Integer.parseInt(count + ""), 5, true);
		}
		
		/**
		 * Buduje zapytanie hql dla wyszukiwarki
		 * 
		 * @return
		 */
		private String buildSearchQuery(Map<String, String> params)
		{	
			StringBuffer hql = new StringBuffer(); 
			List<String> fields = new ArrayList<String>();
			
			// je�li ustawiono numer KPX
			if (!StringUtils.isEmpty(empCard.getKPX()))
			{
				fields.add("emp.KPX like :kpx");
				params.put("kpx", empCard.getKPX());
			}
			// je�li podano nazwisko i/lub imi�
			if (empCard.getUser() != null)
			{
				if (!StringUtils.isEmpty(empCard.getUser().getLastname()))
				{
					// nazwisko
					fields.add("lower(emp.lastName) like :lname");
					params.put("lname", empCard.getUser().getLastname().toLowerCase());
				}
				if (!StringUtils.isEmpty(empCard.getUser().getFirstname()))
				{
					fields.add("lower(emp.firstName) like :fname");
					params.put("fname", empCard.getUser().getFirstname().toLowerCase());
				}
			}
			// je�li podano nazw� departamentu
			if (!StringUtils.isEmpty(empCard.getDepartment()))
			{
				fields.add("emp.department like :dpmt");
				params.put("dpmt", empCard.getDepartment());
			}
			// je�li podano nazw� sp�ki
			if (!StringUtils.isEmpty(empCard.getCompany()))
			{
				fields.add("emp.company like :cmpy");
				params.put("cmpy", empCard.getCompany());
			}
			// je�li podano nazw� stanowiska
			if (!StringUtils.isEmpty(empCard.getPosition()))
			{
				fields.add("emp.position like :pstn");
				params.put("pstn", empCard.getPosition());
			}
			// je�li podano dat� zatrudnienia
			if (!StringUtils.isEmpty(employmentStartDate))
			{
				fields.add("emp.employmentStartDate >= :startDate");
				params.put("startDate", employmentStartDate);
			}
			// je�li podano dat� zwolnienia
			if (!StringUtils.isEmpty(employmentEndDate))
			{
				fields.add("emp.employmentEndDate <= :endDate");
				params.put("endDate", employmentEndDate);
			}
			// czy wy�wietli� tylko zatrudnionych
			if (StringUtils.isEmpty(employmentEndDate) && onlyEmployed)
			{
				fields.add("emp.employmentEndDate is null");
			}
			
			// po��czenie p�l
			for (String s : fields)
				System.out.println(s);
			createWhereClause(hql, fields);
			return hql.toString();
		}
		
		/**
		 * ��czy poszczeg�lne pola i wstawia do podanego StringBuffer
		 * 
		 * @param hql
		 * @param fields
		 */
		private void createWhereClause(StringBuffer hql, List<String> fields)
		{
			if (fields.size() > 0)
			{
				hql.append(" where ");
				hql.append("");
				for (int i = 0; i < fields.size(); i++)
				{
					hql.append(fields.get(i));
					if (i + 1 < fields.size())
						hql.append(" and ");
				}
			}
		}
		
		/**
		 * Tworzy obiekt query na podstawie przekazanego zapytania hql i jego parametr�w
		 * 
		 * @param hql
		 * @param params
		 * @return
		 * @throws EdmException
		 */
		private Query createQueryObject(String hql, Map<String, String> params) throws EdmException
		{
			Query query = DSApi.context().session().createQuery(hql);
			// ustawia parametry zapytania z przekazanej mapy dla metody buildSearchQuery()
			for (Entry<String, String> entry : params.entrySet())
			{
				if (entry.getKey().equals("startDate") || entry.getKey().equals("endDate"))
					query.setDate(entry.getKey(), DateUtils.parseJsDate(entry.getValue()));
				else
					query.setString(entry.getKey(), entry.getValue() + "%");
			}
			
			return query;
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� usuni�cia wybranych kart pracowniczych
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeleteEmployeeCards implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (deleteIds == null || deleteIds.length == 0)
					return;
				
				DSApi.context().begin();
				// usuniecie najpierw wszystkich urlopow powiazanych z kartami pracowniczymi
				DSApi.context().session().createQuery("delete from " + Absence.class.getName() 
						+ " a where a.employeeCard.id in (:ids)")
						.setParameterList("ids", deleteIds)
						.executeUpdate();
				
				String sql = "delete from " + EmployeeCard.class.getName() + " emp "
					+ " where emp.id in (:ids)";
				DSApi.context().session().createQuery(sql)
					.setParameterList("ids", deleteIds)
					.executeUpdate();
				
				addActionMessage(sm.getString("PoprawnieUsunietoZaznaczoneKarty"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Zwraca sk�adow� employeeCards
	 *  
	 * @return
	 */
	public List<EmployeeCard> getEmployeeCards() 
	{
		return employeeCards;
	}

	/**
	 * Zwraca sk�adow� deleteIds;
	 * 
	 * @return
	 */
	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	/**
	 * Ustawia sk�adow� deleteIds
	 * 
	 * @param deleteIds
	 */
	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}

	public EmployeeCard getEmpCard() {
		return empCard;
	}

	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}

	public String getEmploymentStartDate() 
	{
		return employmentStartDate;
	}

	public void setEmploymentStartDate(String employmentStartDate) 
	{
		this.employmentStartDate = employmentStartDate;
	}

	public String getEmploymentEndDate() 
	{
		return employmentEndDate;
	}

	public void setEmploymentEndDate(String employmentEndDate) 
	{
		this.employmentEndDate = employmentEndDate;
	}

	public Boolean getOnlyEmployed() 
	{
		return onlyEmployed;
	}

	public void setOnlyEmployed(Boolean onlyEmployed) 
	{
		this.onlyEmployed = onlyEmployed;
	}

	public int getPageNumber() 
	{
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) 
	{
		this.pageNumber = pageNumber;
	}

	public int getMaxResults() 
	{
		return maxResults;
	}

	public void setMaxResults(int maxResults) 
	{
		this.maxResults = maxResults;
	}

	public Pager getPager() 
	{
		return pager;
	}

	public void setPager(Pager pager) 
	{
		this.pager = pager;
	}

	public String getSortField() 
	{
		return sortField;
	}

	public void setSortField(String sortField) 
	{
		this.sortField = sortField;
	}

	public boolean isAscending() 
	{
		return ascending;
	}

	public void setAscending(boolean ascending) 
	{
		this.ascending = ascending;
	}
	
	public Map<Integer, Integer> getMaxResultsMap()
	{
		Map<Integer, Integer> maxResultsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 5; i < 41; i += 5)
			maxResultsMap.put(i, i);
		return maxResultsMap;
	}
}
