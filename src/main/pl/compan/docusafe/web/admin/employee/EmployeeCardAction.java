package pl.compan.docusafe.web.admin.employee;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.ws.client.absences.CardServiceStub;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetUserInfo;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetUserInfoResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.LoginRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.LoginResponse;
import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Klasa implementuj�ca akce edycji i utowrzenia nowej karty pracownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeCardAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeCardAction.class);
	
	/**
	 * String Manager
	 */
	private static final StringManager sm = StringManager
			.getManager(EmployeeCardAction.class.getPackage().getName());
	
	private static final String FORM_ERROR = "FORM_ERROR";
	
	/**
	 * Obiekt karty pracownika
	 */
	private EmployeeCard empCard;
	
	/**
	 * Ci�g z dat� zatrudnienia
	 */
	private String employmentStartDate;
	
	/**
	 * Ci�g z dat� zwolnienia
	 */
	private String employmentEndDate;
	
	/**
	 * Mapa z dost�pnymi u�ytkownikami
	 */
	private Map<String, String> usersMap = new LinkedHashMap<String, String>();
	
	/**
	 * Identyfikator wybranego u�ytkownika
	 */
	private String selectedUserName;
	
	/**
	 * Mapa departament�w
	 */
	private Map<String, String> departmentsMap = new LinkedHashMap<String, String>();
	
	/**
	 * Mapa dzia��w z wybranego departamentu
	 */
	private Map<String, String> divisionsMap = new LinkedHashMap<String, String>();
	
	/**
	 * Mapa stanowisk
	 */
	private Map<String, String> positionsMap = new LinkedHashMap<String, String>();
	
	/**
	 * Wybrany guid dzialu
	 */
	private String selectedDivision;
	
	/**
	 * Flaga sprawdzaj�ca czy nale�y przepisa� urlopy z poprzedniej karty pracownika
	 */
	private Boolean transferAbsences;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		LoadUsersMap loadUsersMap = new LoadUsersMap();
		LoadDepartments loadDepartments = new LoadDepartments();
		LoadDivisions loadDivisions = new LoadDivisions();
		LoadPositions loadPostions = new LoadPositions();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(new LoadEmployeeCard())
			.append(loadUsersMap)
			.append(loadDepartments)
			.append(loadDivisions)
			.append(loadPostions)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSave")
			.append(OpenHibernateSession.INSTANCE)
			.append(new SaveEmployeeCard())
			.append(loadUsersMap)
			.append(loadDepartments)
			.append(loadDivisions)
			.append(loadPostions)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Akcja �aduj�ca dane wybranej karty pracownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadEmployeeCard implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (empCard != null && empCard.getId() != null)
				{
					String tmpDepartment = empCard.getDepartment();
					empCard = Finder.find(EmployeeCard.class, empCard.getId());
					if (StringUtils.isNotEmpty(tmpDepartment))
						empCard.setDepartment(tmpDepartment);
				}
				if (empCard != null)
				{
					if (empCard.getEmploymentStartDate() != null)
						employmentStartDate = DateUtils.formatJsDate(empCard.getEmploymentStartDate());
					if (empCard.getEmploymentEndDate() != null)
						employmentEndDate = DateUtils.formatJsDate(empCard.getEmploymentEndDate());
					
					UserImpl user = empCard.getUser();
					if (user != null)
					{
						setSelectedUserName(user.getName());
						DSDivision[] divisions = user.getAllDivisions();
						if (divisions.length > 0)
							selectedDivision = divisions[0].getGuid();
					}
				}
				
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa akcji �aduj�ca map� u�ytkownik�w dla elementu select
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadUsersMap implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();			
				// za�adowanie u�ytkownik�w
				List users = DSApi.context().session().createQuery("select u.name, u.lastname, u.firstname from " 
						+ UserImpl.class.getName() + " u order by u.lastname asc")
						.list();
				
				usersMap.put("", "Wybierz u�ytkownika ...");
				for (int i = 0; i < users.size(); i++)
				{
					Object[] row = (Object[]) users.get(i);
					usersMap.put((String) row[0], (String) row[1] + " " + (String) row[2]);
				}
				
				if (empCard != null && empCard.getUser() != null && StringUtils.isEmpty(selectedDivision))
				{
					DSDivision[] divisions = empCard.getUser().getAllDivisions();
					if (divisions.length > 0)
						selectedDivision = divisions[0].getGuid();
				}
				if (empCard != null && empCard.getUser() != null && usersMap.get(empCard.getUser()) == null)
				{
					usersMap.put(empCard.getUser().getName(), empCard.getUser().asLastnameFirstname());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa �aduj�ca departamenty
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadDepartments implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if(!AvailabilityManager.isAvailable("employeeCard.extendedFields"))
					return;
				DSApi.context().begin();
				
                List<DockindDictionaries> deps = new ArrayList<DockindDictionaries>(DockindDictionaries.list("dsg_kp_departament"));
                Collections.sort(deps, new DockindDictionaries.TitleComparator());
                for(DockindDictionaries dic : deps)
                {
                    departmentsMap.put(dic.getCn(),dic.getTitle());
                }

				DSApi.context().commit();
			}
			catch (EdmException ex) 
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa �aduj�ca dzia�y z danego departamentu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadDivisions implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if(!AvailabilityManager.isAvailable("employeeCard.extendedFields"))
					return;
				DSApi.context().begin();
				
                List<DockindDictionaries> divs = new ArrayList<DockindDictionaries>(DockindDictionaries.list("dsg_kp_dzial"));
                Collections.sort(divs, new DockindDictionaries.TitleComparator());
                for(DockindDictionaries dic : divs)
                {
                    divisionsMap.put(dic.getCn(),dic.getTitle());
                }

				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa �aduj�ca stanowiska
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadPositions implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if(!AvailabilityManager.isAvailable("employeeCard.extendedFields"))
					return;
				DSApi.context().begin();
				
                List<DockindDictionaries> pozs = new ArrayList<DockindDictionaries>(DockindDictionaries.list("dsg_kp_stanowisko"));
                Collections.sort(pozs, new DockindDictionaries.TitleComparator());
                for(DockindDictionaries dic : pozs)
                {
                    positionsMap.put(dic.getCn(),dic.getTitle());
                }

				DSApi.context().commit();
			}
			catch (EdmException ex) 
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Akcja zapisuj�ca nowy lub edytuj�ca ju� istniej�cy wpis karty pracownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class SaveEmployeeCard implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				// sprawdzenie danych przes�anych z formularza
				checkFormData();
				
				String userName = null;
				if (StringUtils.isNotBlank(selectedUserName))
				{
					userName = selectedUserName;
					DSUser dsUser = DSUser.findByUsername(userName);
					empCard.setFirstName(dsUser.getFirstname());
					empCard.setLastName(dsUser.getLastname());
					// powi�zanie z u�ytkownikiem
					empCard.setExternalUser(userName);
				}
				else if (StringUtils.isNotBlank(empCard.getExternalUser()))
				{
					try {
						userName = empCard.getExternalUser();
						// update danych z webservice
						updateFromWS(userName);
					} catch (RemoteException e) 
					{
						LOG.error(e.getMessage(), e);
						throw new EdmException(e.getMessage());
					}
				}
                if (!AvailabilityManager.isAvailable("employeeCard.extendedFields")) {
                    empCard.setPosition("position");
                    empCard.setDepartment("department");
                }
				
				// save or update
				DSApi.context().session().saveOrUpdate(empCard);
				
				addActionMessage(sm.getString("PoprawnieZapisanoZmiany"));
				
				// wykonuje transfer urlop�w z poprzedniej karty u�ytkownika (je�li istnieje taka karta)
				if (transferAbsences != null && transferAbsences && AbsenceFactory.transferAbsences(empCard))
					addActionMessage("Poprawnie przeprowadzono transfer urlop�w na now� kart�!");
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FORM_ERROR))
					addActionError(ex.getMessage());
				
			}
		}
		
		/**
		 * Sprawdza poprawno�� p�l formularza
		 * 
		 * @throws EdmException
		 */
		private void checkFormData() throws EdmException
		{
			// ustawienie p�l daty
			if (employmentStartDate != null)
				empCard.setEmploymentStartDate(DateUtils.parseJsDate(employmentStartDate));
			if (employmentEndDate != null)
				empCard.setEmploymentEndDate(DateUtils.parseJsDate(employmentEndDate));
			
			if (StringUtils.isEmpty(empCard.getKPX()))
				addActionError(sm.getString("PodajPoprawnieNumerEwidencyjny"));
			
			if (StringUtils.isEmpty(empCard.getCompany()) && AvailabilityManager.isAvailable("employeeCard.extendedFields"))
				addActionError(sm.getString("PodajPoprawnieNazweSpolki"));
			
			if (StringUtils.isEmpty(empCard.getPosition()) && AvailabilityManager.isAvailable("employeeCard.extendedFields"))
				addActionError(sm.getString("PodajPoprawnieNazweStanowiska"));
			
			if (StringUtils.isEmpty(empCard.getDepartment()) && AvailabilityManager.isAvailable("employeeCard.extendedFields"))
				addActionError(sm.getString("WybierzPoprawnieNazweDepartamentu"));

			if (StringUtils.isEmpty(empCard.getDivision()) && AvailabilityManager.isAvailable("employeeCard.extendedFields"))
				addActionError(sm.getString("WybierzPoprawnieNazweDzialu"));
			
			if (empCard.getEmploymentStartDate() == null)
				addActionError(sm.getString("WybierzDateZatrudnienia"));
			
			if (hasActionErrors())
				throw new EdmException(FORM_ERROR);
		}
		
		/**
		 * Aktualizacja danych na karcie z WebService. G��wnie dla AEGON.
		 * 
		 * @param userName
		 * @throws RemoteException
		 */
		private void updateFromWS(String userName) throws RemoteException
		{
			// pobranie danych web service'em
			CardServiceStub stub = new CardServiceStub();
			stub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			LoginRequest loginRequest = new LoginRequest();
			loginRequest.setLogin(userName);
			// request
			GetUserInfo userInfo = new GetUserInfo();
			userInfo.setRequest(loginRequest);
			// response
			GetUserInfoResponse infoResponse = stub.getUserInfo(userInfo);
			LoginResponse loginResponse = infoResponse.get_return();
			
			empCard.setFirstName(loginResponse.getFirstName());
			empCard.setLastName(loginResponse.getLastName());
		}
	}

	/**
	 * Zwraca sk�adow� empCard
	 * 
	 * @return
	 */
	public EmployeeCard getEmpCard() 
	{
		return empCard;
	}

	/**
	 * Ustawia sk�adow� empCard
	 * 
	 * @param empCard
	 */
	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}

	public String getEmploymentStartDate() 
	{
		return employmentStartDate;
	}

	public void setEmploymentStartDate(String employmentStartDate) 
	{
		this.employmentStartDate = employmentStartDate;
	}

	public String getEmploymentEndDate() 
	{
		return employmentEndDate;
	}

	public void setEmploymentEndDate(String employmentEndDate) 
	{
		this.employmentEndDate = employmentEndDate;
	}

	public Map<String, String> getUsersMap() 
	{
		return usersMap;
	}

	public void setSelectedUserName(String selectedUserId) 
	{
		this.selectedUserName = selectedUserId;
	}

	public String getSelectedUserName() 
	{
		return selectedUserName;
	}

	public Map<String, String> getDepartmentsMap() 
	{
		return departmentsMap;
	}

	public Map<String, String> getDivisionsMap() 
	{
		return divisionsMap;
	}

	public String getSelectedDivision() 
	{
		return selectedDivision;
	}

	public void setSelectedDivision(String selectedDivision) 
	{
		this.selectedDivision = selectedDivision;
	}

	public Map<String, String> getPositionsMap() 
	{
		return positionsMap;
	}
	
	public Boolean getTransferAbsences()
	{
		return transferAbsences;
	}
	
	public void setTransferAbsences(Boolean transferAbsences)
	{
		this.transferAbsences = transferAbsences;
	}
}
