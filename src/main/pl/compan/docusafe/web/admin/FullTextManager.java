package pl.compan.docusafe.web.admin;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.api.user.office.FullTextSearchResponse;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.templating.DocumentSnapshotSerializer;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.rest.views.RichDocumentView;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class FullTextManager {

    private static final Logger log = LoggerFactory.getLogger(FullTextManager.class);

    /**
     * <p>
     *     Tworzy widok jsonowy dokumentu.
     * </p>
     * @param doc dokument
     * @return json
     */
    public static JsonObject getDocumentJsonView(Document doc) throws Exception {

        JsonObject json = new JsonObject();

        json.addProperty("id", doc.getId());
        json.addProperty("author", doc.getAuthor());
        
        try{
        	json.addProperty("authorName", DSUser.findByUsername(doc.getAuthor()).getFirstnameLastnameName());
        }
        catch(Exception e){}
        
        json.addProperty("ctime", doc.getCtime().getTime());
        json.addProperty("dockind_id", doc.getDocumentKind().getId());
        
        if(doc instanceof OfficeDocument){
        	json.addProperty("caseDocumentId", ((OfficeDocument) doc).getCaseDocumentId());
        	try{
        		json.addProperty("currentAssignmentUsername", DSUser.findByUsername(((OfficeDocument) doc).getCurrentAssignmentUsername()).getFirstnameLastnameName());
        	}
        	catch(Exception e){}
        }
        
        JsonArray fields = getDocumentFieldsJsonView(doc);

        json.add("fields", fields);

        return json;
    }

    public static JsonElement getDocumentsJsonView(List<Document> list) throws EdmException {
        JsonArray arr = new JsonArray();
        for(Document document: list) {
            try
            {
                JsonObject json = FullTextManager.getDocumentJsonView(document);
                log.debug("json = {}", json);
                arr.add(json);
            }catch(Exception e){
                log.error("", e);
                throw new EdmException( e);
            }
        }
        return arr;
    }

    public static JsonArray getDocumentFieldsJsonView(Document doc) throws Exception {
        return DocumentSnapshotSerializer.getJsonFromDocument(doc);
    }

    public static List<Document> findDocuments(String query, int maxResults) throws Exception {
        FSDirectory index = null;
        IndexSearcher searcher = null;

        try {
            // get full text results
            String indexPath = Docusafe.getAdditionPropertyOrDefault("fulltext.documentsIndexFolder", "indexes_documents");

            final File fileIndexes = new File(Docusafe.getHome(), indexPath);
            index = FSDirectory.open(fileIndexes);
            StandardAnalyzer analyzer = new StandardAnalyzer(org.apache.lucene.util.Version.LUCENE_46);

            org.apache.lucene.search.Query q = new QueryParser(org.apache.lucene.util.Version.LUCENE_46, IndexHelper.CONTENT_FIELD, analyzer).parse(query);

            log.debug("[findDocuments] query = {}", q);

            IndexReader reader = DirectoryReader.open(index);
            searcher = new IndexSearcher(reader);
            TopScoreDocCollector collector = TopScoreDocCollector.create(maxResults, true);
            searcher.search(q, collector);
            ScoreDoc[] hitsDoc = collector.topDocs().scoreDocs;

            log.debug("[findDocuments] hitsDoc.length = {}", hitsDoc.length);

            // get PublicDocument ids from full text search result

            List<Long> documentIds = new ArrayList<Long>();
            for(ScoreDoc aHitsDoc : hitsDoc) {
                int docId = aHitsDoc.doc;
                org.apache.lucene.document.Document d = searcher.doc(docId);
                documentIds.add(Long.valueOf(d.get("id")));

                log.debug("[findDocuments] d.id = {}, content = {}", d.get("id"), d.get("content"));
            }

            log.debug("[findDocuments] ids = {}", documentIds);

            // get PublicDocuments from db

            if(! documentIds.isEmpty()) {
                Criteria crit = DSApi.context().session().createCriteria(Document.class);
                crit.add(Restrictions.in("id", documentIds));
                List ret = crit.list();

                log.debug("[findDocuments] ret.size = {}", ret.size());

                return ret;
            } else {
                log.debug("[findDocuments] ids empty, return empty list");
                return new ArrayList();
            }
        } catch(Exception ex) {
            throw new Exception(ex);
        } finally {
            if(index != null) {
                index.close();
            }
        }
    }

    public static List<FullTextSearchResponse> getDocumentsFullTextResult(List<Document> doc) throws EdmException {
        List<FullTextSearchResponse> resp = Lists.newArrayList();

        for(Document d : doc){
            FullTextSearchResponse r = new FullTextSearchResponse();
            r.setId(d.getId().toString());
            r.setAuthor(d.getAuthor());
            r.setCtime(d.getCtime().toString());
            r.setDockind_id(d.getDocumentKind().getId().toString());
            Map<String,Object> mapFromDocument = DocumentSnapshotSerializer.getMapFromDocument(d);
            r.fromMap(mapFromDocument);
        }
        return resp;
    }

	public static List<FullTextSearchResponse> getDocumentsFullTextResult2(List<RichDocumentView> listRichDocs) {
        List<FullTextSearchResponse> resp = Lists.newArrayList();

        for (RichDocumentView richDoc : listRichDocs) {
            FullTextSearchResponse fullText = new FullTextSearchResponse();
            fullText.setId(richDoc.getId().toString());
            fullText.setAuthor(richDoc.getAuthor());
            fullText.setDockindCn(richDoc.getKind().getCn());
            resp.add(fullText);
		}
        return resp;
	}
}
