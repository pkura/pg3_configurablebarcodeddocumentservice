package pl.compan.docusafe.web.admin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.parametrization.aegon.FileImportDriver;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.RuntimeProperty;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceDriverInfo;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceInfo;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.fun;

import com.opensymphony.webwork.ServletActionContext;
import com.sdicons.json.validator.impl.predicates.Str;

/**
 * Pozwala na edycj� ustawie� us�ugi.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSServiceAction.java,v 1.23 2010/08/19 07:44:16 pecet1 Exp $
 */
public class DSServiceAction extends EventActionSupport
{
    // @EXPORT
    private String serviceName;
    private Map<String, String> drivers = new LinkedHashMap<String, String>();
    private List<Map<String, Object>> parameters;
    private boolean canSetDriver;
    private List<String> messages;
    private boolean showConsole;
    private List<InOfficeDocumentDelivery> inDocumentDeliveries;
    private Map<String, Object> runtimeProperties = new LinkedHashMap<String, Object>();

    // @EXPORT/@IMPORT
    private String serviceId;
    private String driverId;
    private Map<String, Object> params = new HashMap<String, Object>();
    private LinkedHashMap<String,String> availableServices;
    private boolean allServices;

    // @IMPORT
    private String newDriverId;

    private String reportId;
    
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(DSServiceAction.class.getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSetDriver").
            append(OpenHibernateSession.INSTANCE).
            append(new SetDriver()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doReport").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Report()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            // pobiera ServiceInfo dla danego serviceId
            // parameters = ServiceInfo.getImpl().getParameterDescriptors()

            try
            {
            	List<ServiceDriver> services = (List<ServiceDriver>) ServiceManager.getActiveDrivers();
            	availableServices = new LinkedHashMap<String,String>();
            	for (ServiceDriver driver : services) 
            	{
            		if(driver.getServiceId().toString().equalsIgnoreCase("DSIImportService")){
            		} else if(driver.getServiceId().toString().equalsIgnoreCase("PgImageImportService")){
            		} else if(driver.getServiceId().toString().equalsIgnoreCase("permissions")){
            		} else if(driver.getServiceId().toString().equalsIgnoreCase("tasklist")){		
            		} else if(driver.getServiceId().toString().equalsIgnoreCase("XesLogService")){		
            		} else if(driver.getServiceId().toString().equalsIgnoreCase("mailer")){		
            		} else {
            			availableServices.put(driver.getServiceId(), sm.getString( ServiceManager.getServiceInfo(driver.getServiceId()).getDescription()));
            		}
				}
            	if(serviceId == null)
            	{
            		if(services != null && services.size() > 0)
            			serviceId = services.get(0).getServiceId();
            	}
                ServiceInfo serviceInfo = ServiceManager.getServiceInfo(serviceId);

                serviceName = sm.getString(serviceInfo.getDescription());

                ServiceDriverInfo[] driverInfos = serviceInfo.getDriverInfos();

                for (int i=0; i < driverInfos.length; i++)
                {
                    drivers.put(driverInfos[i].getDriverId(), sm.getString(driverInfos[i].getDescription()));
                }

                canSetDriver = ServiceManager.canSetDriver(serviceId);

                try
                {
                    Service service = ServiceManager.getService(serviceId);

                    driverId = service.getDriverId();

                    RuntimeProperty[] rprops = service.getRuntimeProperties();
                    for (int i=0; i < rprops.length; i++)
                    {
                        runtimeProperties.put(rprops[i].getName(), rprops[i].getValue());
                    }

                    Property[] properties = service.getProperties();
                    parameters = new ArrayList<Map<String, Object>>(properties.length);
                    for (int i=0; i < properties.length; i++)
                    {
                        Map<String, Object> bean = new HashMap<String, Object>();
                        bean.put("disabled", Boolean.valueOf(!properties[i].canModify()));
                        bean.put("description", properties[i].getName());
                        bean.put("name", properties[i].getId());

                        if (properties[i].isEnumerated())
                        {
                            bean.put("widget", "select");
                            bean.put("constraints", properties[i].getValueConstraints());
                        }
                        else if (properties[i].getId().equalsIgnoreCase("password"))
                        {
                        	bean.put("widget", "password");
                        }
                        else if (properties[i].getType() == String.class ||
                            properties[i].getType() == Integer.class)
                        {
                            bean.put("widget", "textfield");
                        }
                        else if (properties[i].getType() == Boolean.class)
                        {
                            bean.put("widget", "checkbox");
                        }
                        else if (properties[i].getType() == Boolean.class)
                        {
                            bean.put("widget", "checkbox");
                        }


                        bean.put("wantsGuid", Boolean.valueOf(properties[i].wantsGuid()));
                        bean.put("wantsInDocumentDelivery", Boolean.valueOf(properties[i].wantsInDocumentDelivery()));
                        bean.put("wantsDate", Boolean.valueOf(properties[i].wantsDate()));
                        bean.put("wantsReport", Boolean.valueOf(properties[i].wantsReport()));
                        
                        if (inDocumentDeliveries == null && properties[i].wantsInDocumentDelivery())
                        {
                            inDocumentDeliveries = InOfficeDocumentDelivery.list();
                        }

                        //bean.put("wantsUsername", Boolean.valueOf(properties[i].wantsUsername()));

                        bean.put("value", properties[i].getValue());

                        if (properties[i].wantsGuid() && properties[i].getValue() != null)
                        {
                            try
                            {
                                bean.put("prettyValue",
                                    DSDivision.find(properties[i].getValue().toString()).getPrettyPath());
                            }
                            catch (DivisionNotFoundException e)
                            {
                            }
                        }

                        parameters.add(bean);
                    }

                    DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

                    // konsola
                    if (service.hasConsole())
                    {
                        showConsole = true;
                        Collection<Console.Message> rawMessages = fun.reverse(Arrays.asList(ServiceManager.getConsoleMessages(serviceId)));
                        messages = new ArrayList<String>(rawMessages.size());
                        for (Console.Message msg : rawMessages)
                        { 
                        	if (msg!=null){
                            messages.add(df.format(new Date(msg.getTimestamp()))+
                                " "+msg.getLogLevelName()+
                                " ["+msg.getDriverId()+"] "+
                                msg.getMessage());
                        	}
                        }
                    }
                }
                catch (ServiceDriverNotSelectedException e)
                {
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Wybiera aktywn� implementacj� BIP.
     */
    private class SetDriver implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (!StringUtils.isEmpty(newDriverId))
                {
                    ServiceManager.setDriver(serviceId, newDriverId);
                }
                else
                {
                    ServiceManager.setDriver(serviceId, null);
                }

                driverId = newDriverId;

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Report implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{        	
        		FileImportDriver fid = (FileImportDriver) ServiceManager.getService(serviceId);
        		ServletUtils.streamFile(ServletActionContext.getResponse(), fid.getReportFileByName(reportId), "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"raport.csv\"");
        		event.setResult(NONE);
        	}
        	catch (Exception e) 
        	{
        		LogFactory.getLog("eprint").debug("", e);
			}
        	
        }
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                ServiceInfo serviceInfo = ServiceManager.getServiceInfo(serviceId);

                serviceName = serviceInfo.getDescription();

                try
                {
                    Service service = ServiceManager.getService(serviceId);

                    Property[] properties = service.getProperties();
                    for (int i=0; i < properties.length; i++)
                    {
                        Object value = params.get(properties[i].getId());
                        if (value != null)
                        {	
                            if (properties[i].getType() == Boolean.class)
                            {
                                properties[i].setValue(Boolean.valueOf(Boolean.parseBoolean((String) value)));
                            }
                            else if (properties[i].getType() == Integer.class)
                            {
                                Integer iValue;
                                try
                                {
                                    iValue = new Integer(Integer.parseInt((String) value));
                                    properties[i].setValue(iValue);
                                }
                                catch (NumberFormatException e)
                                {
                                    throw new EdmException(
                                        sm.getString("NiepoprawnaWartoscWpoluSpodziewanoSieLiczby",properties[i].getName()));
                                }
                            }
                            else if (properties[i].getType() == String.class && value instanceof String)
                            {
                                properties[i].setValue((String) value);
                            }
                            else if (properties[i].getType() == String.class && value instanceof String[])
                            {
                            	properties[i].setValue(((String[]) value)[0]);
                            }
                            else if (properties[i].getType() == Long.class)
                            {
                            	properties[i].setValue(Long.parseLong((String) value));
                            }
                        }
                        else
                        {
                            // niezaznaczony checkbox nie wysy�a �adnej warto�ci,
                            // bior� to pod uwag�
                            if (properties[i].getType() == Boolean.class)
                            {
                                properties[i].setValue(Boolean.FALSE);
                            }
                            else
                            {
                                properties[i].setValue(null);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                	LogFactory.getLog("eprint").error("", e);
                }
                addActionMessage(sm.getString("ZapisanoParametry"));

                DSApi.context().commit();
            }
            catch (Exception e)
            {
            	LogFactory.getLog("eprint").error("", e);
            	addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public String getServiceId()
    {
        return serviceId;
    }

    public void setServiceId(String serviceId)
    {
        this.serviceId = serviceId;
    }

    public String getServiceName()
    {
        return serviceName;
    }

    public String getDriverId()
    {
        return driverId;
    }

    public void setDriverId(String driverId)
    {
        this.driverId = driverId;
    }

    public Map getDrivers()
    {
        return drivers;
    }

    public void setNewDriverId(String newDriverId)
    {
        this.newDriverId = newDriverId;
    }

    public List getParameters()
    {
        return parameters;
    }

    public Map getParams()
    {
        return params;
    }

    public void setParams(Map params)
    {
        this.params = params;
    }

    public boolean isCanSetDriver()
    {
        return canSetDriver;
    }

    public List getMessages()
    {
        return messages;
    }

    public boolean isShowConsole()
    {
        return showConsole;
    }

    public List getInDocumentDeliveries()
    {
        return inDocumentDeliveries;
    }

    public Map getRuntimeProperties()
    {
        return runtimeProperties;
    }

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public boolean isAllServices() {
		return allServices;
	}

	public void setAllServices(boolean allServices) {
		this.allServices = allServices;
	}


	public LinkedHashMap<String, String> getAvailableServices() {
		return availableServices;
	}

	public void setAvailableServices(LinkedHashMap<String, String> availableServices) {
		this.availableServices = availableServices;
	}
}
