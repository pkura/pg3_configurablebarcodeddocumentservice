package pl.compan.docusafe.web.admin;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.test.PerformanceTest;
import pl.compan.docusafe.core.test.SearchTestActivity;
import pl.compan.docusafe.core.test.SummaryTestActivity;
import pl.compan.docusafe.core.test.TasksTestActivity;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Test wydajno�ci. Akcja pozwala uruchomi� test dla wybranych u�ytkownik�w,
 * polegaj�cy na wykonywaniu czynno�ci, kt�re najcz�ciej podejmuje
 * u�ytkownik systemu.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */
public class PerformanceTestAction extends EventActionSupport
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(PerformanceTestAction.class.getPackage().getName(),null);
    
    static final Integer MAX_TEST_TIME = 1800; 
    static final Integer DEFAULT_TEST_TIME = 300; 
    static final Integer DEFAULT_MIN_WAIT_TIME = 4; 
    static final Integer DEFAULT_MAX_WAIT_TIME = 6; 
    
    static final String TEST_TASKS = "tasks";
    static final String TEST_SUMMARY = "summary";
    static final String TEST_SEARCH = "search";
    
    private Map<String, String> usersAll;
    private Map<String, String> usersSelected;
    private String[] usernames;
    private Integer testTime;
    private Integer minWaitTime;
    private Integer maxWaitTime;
    private String[] testActivities;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doStartTest").
            append(OpenHibernateSession.INSTANCE).
            append(new StartTest()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                usersAll = new LinkedHashMap<String, String>();
                usersSelected = new LinkedHashMap<String, String>();
                List<DSUser> usersList = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

                Iterator<DSUser> itr = usersList.iterator();
                
                if (usernames != null)
                {
                    for (int i = 0; i < usernames.length; ++i)
                    {
                        DSUser user = DSUser.findByUsername(usernames[i]);
                        usersSelected.put(user.getName(), user.asLastnameFirstname());
                    }
                }
                
                while (itr.hasNext())
                {
                    DSUser user = itr.next();
                    // nie b�dzie wypisywania ilo�ci zada� na listach poniewa� dla du�ej
                    // ilo�ci u�ytkownik�w strona �aduje si� d�ugo
                    /*
                    TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
                    int inCount = taskList.getDocumentTasks(user, InOfficeDocument.TYPE).size(); 
                    int intCount = taskList.getDocumentTasks(user, OutOfficeDocument.INTERNAL_TYPE).size(); 
                    int outCount = taskList.getDocumentTasks(user, OutOfficeDocument.TYPE).size(); 
                    int caseCount = OfficeCase.findUserCases(user, null).size();
                    String tasksCount = "(" + inCount + "/" + intCount + "/" + outCount + "/" + caseCount + ")";
                    */
                    if (!usersSelected.containsKey(user.getName()))
                        usersAll.put(user.getName(), user.asLastnameFirstname());// + tasksCount);
                }
                if (testTime == null)
                    testTime = DEFAULT_TEST_TIME;
                if (minWaitTime == null)
                    minWaitTime = DEFAULT_MIN_WAIT_TIME;
                if (maxWaitTime == null)
                    maxWaitTime = DEFAULT_MAX_WAIT_TIME;
                testActivities = new String[] {TEST_TASKS, TEST_SUMMARY, TEST_SEARCH};
                
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class StartTest implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                validateForm();
                int i;
                for (i = 0; i < usernames.length; ++i)
                {
                    PerformanceTest test = new PerformanceTest(usernames[i],
                        new Long(testTime * 1000), new Long(minWaitTime * 1000),
                        new Long(maxWaitTime * 1000));
                    for (int j = 0; j < testActivities.length; ++j)
                    {
                        if (testActivities[j].equals(TEST_TASKS))
                            test.addActivity("lista zada�", new TasksTestActivity(usernames[i]));
                        if (testActivities[j].equals(TEST_SUMMARY))
                            test.addActivity("podsumowanie", new SummaryTestActivity(usernames[i]));
                        if (testActivities[j].equals(TEST_SEARCH))
                            test.addActivity("wyszukiwanie", new SearchTestActivity(usernames[i]));
                    }
                    test.equalProbability(true);
                    test.start();
                    addActionMessage(sm.getString("TestDlaUzytkowikaRozpoczety",usernames[i]));
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private void validateForm() throws EdmException
    {
        if (testActivities == null || testActivities.length == 0)
            throw new EdmException(sm.getString("NieWybranoAkcjiTestowych"));
        if (usernames == null || usernames.length == 0)
            throw new EdmException(sm.getString("NieWybranoUzytkownika"));
        if (testTime == null || testTime <= 0)
            throw new EdmException(sm.getString("NiepoprawnyCzasTestu"));
        if (minWaitTime == null || minWaitTime <= 0)
            throw new EdmException(sm.getString("NiepoprawnyMinimalnyCzasOczekiwania"));
        if (maxWaitTime == null || maxWaitTime <= 0)
            throw new EdmException(sm.getString("NiepoprawnyMaksymalnyCzasOczekiwania"));
        if (maxWaitTime < minWaitTime)
            throw new EdmException(sm.getString("MaksymalnyCzasOczekiwaniaJestKrotszyOdMinimalnego"));
        if (testTime < minWaitTime)
            throw new EdmException(sm.getString("CzasTestuJestKrotszyOdMinimalnegoCzasuOczekiwania"));
        if (testTime >= MAX_TEST_TIME)
            throw new EdmException(sm.getString("ZbytDlugiCzasTestuMaksymalnieSekund", MAX_TEST_TIME));
    }

    public Map<String, String> getUsersAll()
    {
        return usersAll;
    }

    public Map<String, String> getUsersSelected()
    {
        return usersSelected;
    }

    public String[] getUsernames()
    {
        return usernames;
    }

    public void setUsernames(String[] usernames)
    {
        this.usernames = usernames;
    }

    public void setTestTime(Integer testTime)
    {
        this.testTime = testTime;
    }

    public Integer getTestTime()
    {
        return testTime;
    }

    public void setMinWaitTime(Integer minWaitTime)
    {
        this.minWaitTime = minWaitTime;
    }

    public Integer getMinWaitTime()
    {
        return minWaitTime;
    }

    public void setMaxWaitTime(Integer maxWaitTime)
    {
        this.maxWaitTime = maxWaitTime;
    }

    public Integer getMaxWaitTime()
    {
        return maxWaitTime;
    }

    public String[] getTestActivities()
    {
        return testActivities;
    }

    public void setTestActivities(String[] testActivities)
    {
        this.testActivities = testActivities;
    }

}
