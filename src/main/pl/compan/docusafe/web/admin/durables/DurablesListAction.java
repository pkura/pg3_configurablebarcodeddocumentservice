package pl.compan.docusafe.web.admin.durables;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.durables.Durable;
import pl.compan.docusafe.core.base.durables.DurablesFactory;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.field.ListValueDbEnumField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * 
 * @author User
 */
public class DurablesListAction extends EventActionSupport
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DurablesListAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager SM = StringManager.getManager(
			DurablesListAction.class.getPackage().getName());
	
	/**
	 * Lista �rodk�w trwa�ych dla widoku
	 */
	private List<Durable> durables;
	
	/**
	 * Identyfikatory �rodk�w trwa�ych do usuni�cia
	 */
	private Long[] deleteIds;
	
	/**
	 * Obiekt z parametrami do wyszukania.
	 */
	private Durable durable;
	
	/**
	 * Maksymalna liczba wynikow na stronie
	 */
	private Integer maxResults = 20;
	
	/**
	 * Strona
	 */
	private Integer offset = 0;
	
	/**
	 * Obiekt pagera
	 */
	private Pager pager;
	
	private Map<Integer, String> locations;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		DurablesList listAction = new DurablesList();
		LoadLocations loadLocations = new LoadLocations();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(loadLocations)
			.append(listAction)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("goSearch")
			.append(OpenHibernateSession.INSTANCE)
			.append(loadLocations)
			.append(listAction)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doDelete")
			.append(OpenHibernateSession.INSTANCE)
			.append(new DeleteDurables())
			.append(loadLocations)
			.append(listAction)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * 
	 * @author User
	 */
	private class DurablesList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				durables = DurablesFactory.getInstance().getDurables(durable, offset, maxResults);
				createPager();
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				addActionError(ex.getMessage());
			}
		}
		
		private void createPager() throws EdmException
		{
			int count = DurablesFactory.getInstance().getCountDurables(durable);
			pager = new Pager(new Pager.LinkVisitor() {
				public String getLink(int offset) 
				{
					return offset + "";
				}
			}, offset, maxResults, count, 5, true);
		}
	}
	
	private class LoadLocations implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				locations = new LinkedHashMap<Integer, String>();
				locations.put(0, "Wybierz lokalizacj�");
				
				List<CentrumKosztow> list = CentrumKosztow.list();
				for (CentrumKosztow centrum : list)
				{
					locations.put(centrum.getId(), centrum.getId() + " " + centrum.getName());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * 
	 * @author User
	 */
	private class DeleteDurables implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (deleteIds == null || deleteIds.length == 0)
					return;
				
				DSApi.context().begin();
				
				DurablesFactory.getInstance().deleteDurablesByIds(deleteIds);
				
				if (StringUtils.isNotEmpty(Docusafe.getAdditionProperty("durables.tablename")))
					ListValueDbEnumField.reloadForTable(Docusafe.getAdditionProperty("durables.tablename"));
				
				DSApi.context().commit();
				
				addActionMessage(SM.getString("PoprawnieUsunietoWskazaneSrodkiTrwale"));
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	public List<Durable> getDurables() 
	{
		return durables;
	}

	public void setDurables(List<Durable> durables) 
	{
		this.durables = durables;
	}

	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}

	public Durable getDurable() 
	{
		return durable;
	}

	public void setDurable(Durable durable) 
	{
		this.durable = durable;
	}

	public Integer getMaxResults() 
	{
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) 
	{
		this.maxResults = maxResults;
	}

	public Integer getOffset() 
	{
		return offset;
	}

	public void setOffset(Integer offset) 
	{
		this.offset = offset;
	}

	public Pager getPager() 
	{
		return pager;
	}

	public void setPager(Pager pager) 
	{
		this.pager = pager;
	}
	
	public Map<Integer, Integer> getMaxResultsMap()
	{
		Map<Integer, Integer> maxResultsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 5; i < 41; i += 5)
			maxResultsMap.put(i, i);
		return maxResultsMap;
	}

	public Map<Integer, String> getLocations() {
		return locations;
	}

	public void setLocations(Map<Integer, String> locations) {
		this.locations = locations;
	}
}
