package pl.compan.docusafe.web.admin.durables;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.durables.Durable;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.field.ListValueDbEnumField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Update srodka trwalego
 * 
 * @author User
 */
public class UpdateDurableAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UpdateDurableAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager SM = StringManager.getManager(
			UpdateDurableAction.class.getPackage().getName());
	
	/**
	 * Srodek trwaly do zapisania zmian
	 */
	private Durable durable;
	
	/**
	 * Lokalizacje (MPKi)
	 */
	private Map<Integer, String> locations;
	
	@Override
	protected void setup() 
	{
		LoadLocations loadLocations = new LoadLocations();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(new FillForm())
			.append(loadLocations)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doUpdate")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Update())
			.append(loadLocations)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (durable != null && durable.getId() != null)
				{
					// jesli jest id to edycja srodka trwalego
					durable = Finder.find(Durable.class, durable.getId());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	private class LoadLocations implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				locations = new LinkedHashMap<Integer, String>();
				locations.put(0, "Wybierz lokalizację");
				
				List<CentrumKosztow> list = CentrumKosztow.list();
				for (CentrumKosztow centrum : list)
				{
					locations.put(centrum.getId(), centrum.getId() + " " + centrum.getName());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * 
	 * @author User
	 */
	private class Update implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			checkFields();
			if (hasActionErrors())
				return;
			
			try
			{
				DSApi.context().begin();
				
				prepareDurable();
				DSApi.context().session().saveOrUpdate(durable);
				DSApi.context().session().flush();
				
				if (StringUtils.isNotEmpty(Docusafe.getAdditionProperty("durables.tablename")))
					ListValueDbEnumField.reloadForTable(Docusafe.getAdditionProperty("durables.tablename"));
				
				DSApi.context().commit();
				
				addActionMessage(SM.getString("PoprawnieZapisanoSrodekTrwaly"));
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		private void checkFields()
		{
			if (durable == null)
				addActionError(SM.getString("WypelnijPoprawnieFormularz"));
			
			if (StringUtils.isBlank(durable.getInventoryNumber()))
				addActionError(SM.getString("PodajPoprawnieNumerInwet"));
			
			if (durable.getLocation() == null || durable.getLocation() == 0)
				addActionError(SM.getString("PodajPoprawnieLokalizacjeSrodka"));
		}
		
		private void prepareDurable()
		{
			durable.setAvailable(Boolean.TRUE);
			durable.setInventoryNumber(durable.getInventoryNumber().trim());
			durable.setSerialNumber(durable.getSerialNumber() != null ? durable.getSerialNumber().trim() : null);
			durable.setLocation(durable.getLocation());
		}
	}
	
	public Durable getDurable() 
	{
		return durable;
	}
	
	public void setDurable(Durable durable) 
	{
		this.durable = durable;
	}
	
	public Map<Integer, String> getLocations() 
	{
		return locations;
	}
}
