package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-07-07 14:01:12 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BarcodePrinterAction.java,v 1.2 2006/02/20 15:42:33 lk Exp $
 */
public class BarcodePrinterAction extends EventActionSupport
{
    private String device;
    private boolean usePrinter;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            device = GlobalPreferences.getBarcodePrinterDevice();
            usePrinter = GlobalPreferences.getUseBarcodePrinter();
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                GlobalPreferences.setUseBarcodePrinter(usePrinter);
                GlobalPreferences.setBarcodePrinterDevice(TextUtils.trimmedStringOrNull(device));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public String getDevice()
    {
        return device;
    }

    public void setDevice(String device)
    {
        this.device = device;
    }

    public boolean isUsePrinter()
    {
        return usePrinter;
    }

    public void setUsePrinter(boolean usePrinter)
    {
        this.usePrinter = usePrinter;
    }
}
