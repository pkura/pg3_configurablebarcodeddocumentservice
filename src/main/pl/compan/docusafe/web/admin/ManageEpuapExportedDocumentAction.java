package pl.compan.docusafe.web.admin;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.Pager.LinkVisitor;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ApplicationSettingsAction.java,v 1.58 2010/07/14 13:35:15 mariuszk Exp $
 */
public class ManageEpuapExportedDocumentAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(ManageEpuapExportedDocumentAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ManageEpuapExportedDocumentAction.class.getPackage().getName(),null);
	
	private List<EpuapExportDocument> exportedDocuments;
	
	private Pager pager;
	private int offset;
	public static final int LIMIT = 15;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
            try
            {
               
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	exportedDocuments = EpuapExportDocument.sentDocuments(offset, LIMIT);
            	LinkVisitor linkVisitor = new Pager.LinkVisitor() {	
    				public String getLink(int offset) 
    				{
    					return offset + "";
    				}
    			};
    			
    			pager = new Pager(linkVisitor, offset, LIMIT, exportedDocuments.size(), 10);
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getStatusAsString(Integer id)
    {
    	return EpuapExportDocument.statusAsString(id);
    }

	public List<EpuapExportDocument> getExportedDocuments() {
		return exportedDocuments;
	}


	public void setExportedDocuments(List<EpuapExportDocument> exportedDocuments) {
		this.exportedDocuments = exportedDocuments;
	}
  
}
