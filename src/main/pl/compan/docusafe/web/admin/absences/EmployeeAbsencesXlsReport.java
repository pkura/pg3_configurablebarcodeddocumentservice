package pl.compan.docusafe.web.admin.absences;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;

import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.util.DateUtils;

public class EmployeeAbsencesXlsReport extends AbstractXlsReport 
{
	private List<EmployeeAbsenceEntry> absences;

	public EmployeeAbsencesXlsReport(String reportName,
			List<EmployeeAbsenceEntry> absences) 
	{
		super(reportName);
		
		this.absences = absences;
		
		columns = new String[] {
				"Lp.", "Nazwisko i imi�", "Data od", "Data do", 
				"Liczba dni urlopu", "Tytu�", "Pozosta�o dni urlopu"};
	}
	
	protected void createEntries()
	{
		int rowCount = 1;
		for (EmployeeAbsenceEntry reportEntry : absences)
		{
			HSSFRow row = sheet.createRow(rowCount++);
			row.createCell(0).setCellValue(reportEntry.getLp());
			row.createCell(1).setCellValue(new HSSFRichTextString( 
					reportEntry.getAbsence().getEmployeeCard().getUser().getLastname() + " " 
					+ reportEntry.getAbsence().getEmployeeCard().getUser().getFirstname()));
			row.createCell(2).setCellValue(new HSSFRichTextString(DateUtils.formatJsDate(reportEntry.getAbsence().getStartDate())));
			row.createCell(3).setCellValue(new HSSFRichTextString(DateUtils.formatJsDate(reportEntry.getAbsence().getEndDate())));
			row.createCell(4).setCellValue(reportEntry.getAbsence().getDaysNum());
			row.createCell(5).setCellValue(new HSSFRichTextString(reportEntry.getAbsence().getAbsenceType().getName()));
			row.createCell(6).setCellValue(reportEntry.getRestOfDays());
		}
	}

	public List<EmployeeAbsenceEntry> getAbsences() 
	{
		return absences;
	}

	public void setAbsences(List<EmployeeAbsenceEntry> absences) 
	{
		this.absences = absences;
	}
}
