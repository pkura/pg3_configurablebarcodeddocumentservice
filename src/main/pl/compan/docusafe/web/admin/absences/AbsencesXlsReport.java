package pl.compan.docusafe.web.admin.absences;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;

import pl.compan.docusafe.core.base.absences.AbsenceReportEntry;
import pl.compan.docusafe.util.DateUtils;

public class AbsencesXlsReport extends AbstractXlsReport
{	
	private List<AbsenceReportEntry> absences;

	public AbsencesXlsReport(String reportName,
			List<AbsenceReportEntry> absences) 
	{
		super(reportName);
		
		this.absences = absences;
		columns = new String[] {
				"Lp.", "Nazwisko i imi�", "Nazwa sp�ki", "Dzia�", "Wymiar urlopu - nale�ny", 
				"Wymiar urlopu - zaleg�y", "Wymiar urlopu - dodatkowy", "Wymiar urlopu - wykorzystanego",
				"Wymiar urlopu - pozosta�ego","Data zatrudnienia","Data zwolnienia"
			};
	}
	
	protected void createEntries()
	{
		int rowCount = 1;
		for (AbsenceReportEntry reportEntry : absences)
		{
			int i = -1; 
			HSSFRow row = sheet.createRow(rowCount++);
			row.createCell(++i).setCellValue(reportEntry.getLp());
			row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getName()));
			row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getCompanyName()));
			row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getDivisionName()));
			row.createCell(++i).setCellValue(reportEntry.getDueDays());
			row.createCell(++i).setCellValue(reportEntry.getOverdueDays());
			row.createCell(++i).setCellValue(reportEntry.getAdditionalDays());
			row.createCell(++i).setCellValue(reportEntry.getExecutedDays());
			row.createCell(++i).setCellValue(reportEntry.getRestOfDays());
			if(reportEntry.getEmploymentStartDate() != null)
				row.createCell(++i).setCellValue(DateUtils.formatBipDate(reportEntry.getEmploymentStartDate()));
			else
				row.createCell(++i).setCellValue("");
			if(reportEntry.getEmploymentEndDate() != null)
				row.createCell(++i).setCellValue(DateUtils.formatBipDate(reportEntry.getEmploymentEndDate()));
			else
				row.createCell(++i).setCellValue("");
		}
	}

	public List<AbsenceReportEntry> getAbsences() 
	{
		return absences;
	}

	public void setAbsences(List<AbsenceReportEntry> absences) 
	{
		this.absences = absences;
	}
}
