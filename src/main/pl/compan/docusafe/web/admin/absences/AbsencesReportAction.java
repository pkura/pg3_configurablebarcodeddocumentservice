package pl.compan.docusafe.web.admin.absences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class AbsencesReportAction extends EventActionSupport
{
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;

	private static final Log log = LogFactory.getLog(AbsencesReportAction.class);
	
	private List<Zestawienie> zestawieniaBeans;
	private Integer notInCount;
	private Integer maxNumber;
	private Integer fileId;
	
	protected void setup()
    {
		FillForm fillForm = new FillForm();
		
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGenerate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Generate()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doFile").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GenerateFile()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class Zestawienie
	{
		private int count;
		private Integer number;
		private String date;
		
		public Zestawienie(Integer number, Date date)
		{
			this.number = number;
			this.date = DateUtils.formatJsDateTime(date);
			this.count = 1;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		public String getDate() {
			return date;
		}

		public Integer getNumber() {
			return number;
		}
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	maxNumber = 0;
        	try
        	{
        		Map<Integer,Zestawienie> zestawienia = new LinkedHashMap<Integer, Zestawienie>();
        		
        		PreparedStatement ps = DSApi.context().prepareStatement("select inv.nr_zestawienia as nr, zest.zestawienie_data as data from DS_ABSENCE inv (nolock) join DSG_ABSENCE_ZESTAWIENIA zest on nr_zestawienia = zestawienie_id order by nr");
        		ResultSet rs = ps.executeQuery();
                while(rs.next())
                {
                	Integer numer = new Integer(rs.getInt("nr"));
                	Date data = new Date(rs.getTimestamp("data").getTime());
                	if(zestawienia.containsKey(numer))
                	{
                		zestawienia.get(numer).setCount(zestawienia.get(numer).getCount()+1);
                	}
                	else
                	{
                		zestawienia.put(numer, new Zestawienie(numer, data));
                		maxNumber = numer;
                	}
                }
                
                rs.close();
                ps.close();
                                
                
                zestawieniaBeans = new ArrayList<Zestawienie>(zestawienia.values());
                
                ps = DSApi.context().prepareStatement("select COUNT(id) as count from DS_ABSENCE where nr_zestawienia is null");
                rs = ps.executeQuery();
                while(rs.next())
                {
                	notInCount = new Integer(rs.getInt("count"));
                }
                
                rs.close();
                ps.close();
        	}
        	catch (Exception e) 
        	{
        		log.error(e.getMessage());
        		if(log.isDebugEnabled())
        		{
        			log.debug(e.getMessage(),e);
        		}
				//e.printStackTrace();
			}
        	
        }
    }
	
	private class Generate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		Integer newNum = maxNumber+1;
        		PreparedStatement ps = DSApi.context().prepareStatement("update DS_ABSENCE set nr_zestawienia = ? where nr_zestawienia is null");
        		ps.setInt(1, newNum);
        		ps.execute();
        		ps.close();
        		
        		ps = DSApi.context().prepareStatement("insert into DSG_ABSENCE_ZESTAWIENIA(ZESTAWIENIE_ID,ZESTAWIENIE_DATA,ZESTAWIENIE_USER) values (?,?,?)");
        		ps.setInt(1, newNum);
        		ps.setTimestamp(2, new Timestamp(new Date().getTime()));
        		ps.setString(3, DSApi.context().getPrincipalName());
        		ps.execute();
        		ps.close();
        	}
        	catch (Exception e) 
        	{
				//e.printStackTrace();
        		log.error(e.getMessage());
        		if(log.isDebugEnabled())
        		{
        			log.debug(e.getMessage(),e);
        		}
			}
        }
    }
	
	private class GenerateFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		//PreparedStatement ps = DSApi.context().prepareStatement("select ab.days_num, ab.start_date, ab.end_date, ab.absence_type, du.FIRSTNAME, du.LASTNAME, at.NAME, card.KPX, card.DIVISION from DS_ABSENCE ab (nolock) join DS_EMPLOYEE_CARD card on card.ID = ab.EMPLOYEE_ID join ds_user du on du.NAME = card.USER_ID join DS_ABSENCE_TYPE at on at.CODE = ab.ABSENCE_TYPE where ab.nr_zestawienia = ? and ab.absence_type not in ('WUN','WUZ')");
        		//PreparedStatement ps = DSApi.context().prepareStatement("select ab.days_num, ab.start_date, ab.end_date, ab.absence_type, du.FIRSTNAME, du.LASTNAME, at.NAME, card.KPX, card.DIVISION, card.USER_ID from DS_ABSENCE ab (nolock) join DS_EMPLOYEE_CARD card on card.ID = ab.EMPLOYEE_ID left outer join ds_user du on du.NAME = card.USER_ID join DS_ABSENCE_TYPE at on at.CODE = ab.ABSENCE_TYPE where ab.nr_zestawienia = ? and ab.absence_type not in ('WUN','WUZ')");
        		PreparedStatement ps = DSApi.context().prepareStatement("select ab.days_num, ab.start_date, ab.end_date, ab.absence_type, " +
        				"card.FIRST_NAME, card.LAST_NAME, at.NAME, card.KPX, card.DIVISION, card.USER_ID from DS_ABSENCE ab (nolock) " +
        				"join DS_EMPLOYEE_CARD card on card.ID = ab.EMPLOYEE_ID join DS_ABSENCE_TYPE at on at.CODE = ab.ABSENCE_TYPE " +
        				"where ab.nr_zestawienia = ? and ab.absence_type not in ('WUN','WUZ')");

        		ps.setInt(1, fileId);
        		ResultSet rs = ps.executeQuery();
        		
        		//StringBuffer buffer = new StringBuffer();
        		HSSFWorkbook wb = new HSSFWorkbook();
        		HSSFSheet sheet = wb.createSheet("raport");
        		
        		HSSFCellStyle cellStyle = wb.createCellStyle();
    			cellStyle.setDataFormat(wb.createDataFormat().getFormat("dd/MM/yyyy"));
    			
    			DecimalFormat myFormatter = new DecimalFormat("000000");
    			//myFormatter.format(12);
    			
        		short line = 0;
        		HSSFRow head = sheet.createRow(line);
        		head.createCell(0).setCellValue("ID");
        		head.createCell(1).setCellValue("NAZWISKO");
        		head.createCell(2).setCellValue("IMIE");
        		head.createCell(3).setCellValue("NR_EDW");
        		head.createCell(4).setCellValue("DATA_OD");
        		head.createCell(5).setCellValue("DATA_DO");
        		head.createCell(6).setCellValue("KOD_KLIENTA");
        		head.createCell(7).setCellValue("CECHA_DOD");
        		head.createCell(8).setCellValue("IL_DNI");
        		head.createCell(9).setCellValue("IL_DNI_KAL");
        		
                while(rs.next())
                {
                	line++;
                	HSSFRow row = sheet.createRow(line);
                	
                	Calendar from = Calendar.getInstance();
                	from.setTimeInMillis(rs.getTimestamp("start_date").getTime());
                	Calendar to = Calendar.getInstance();
                	to.setTimeInMillis(rs.getTimestamp("end_date").getTime());
                	
                	row.createCell(0).setCellValue(line);
                	String lastname = String.valueOf(rs.getString("LAST_NAME"));
                	String firstname = String.valueOf(rs.getString("FIRST_NAME"));
                	if(lastname.equalsIgnoreCase("null"))
                	{
                		row.createCell(1).setCellValue("login:");
                                row.createCell(2).setCellValue(rs.getString("USER_ID"));
                	}
                	else
                	{
                		row.createCell(1).setCellValue(lastname);
                    	row.createCell(2).setCellValue(firstname);                    	
                	}
                	
                	String KPX = rs.getString("KPX");
                	try
                	{
                		KPX = myFormatter.format(Integer.parseInt(KPX));
                	}
                	catch (Exception e) {}
                	row.createCell(3).setCellValue(KPX);
                	HSSFCell cell = row.createCell(4);
                	cell.setCellValue(from.getTime());
                	cell.setCellStyle(cellStyle);
                	cell = row.createCell(5);
                	cell.setCellValue(to.getTime());
                	cell.setCellStyle(cellStyle);
                	
                	String type = rs.getString("absence_type");
                	if(type.equalsIgnoreCase("U�"))
                	{
                		row.createCell(6).setCellValue("U�");
                    	row.createCell(7).setCellValue("�");
                	}
                	else
                	{
                		row.createCell(6).setCellValue(type);
                    	row.createCell(7).setCellValue("");
                	}
                	
                	
                	row.createCell(8).setCellValue(rs.getString("days_num"));
                	row.createCell(9).setCellValue(to.get(Calendar.DAY_OF_YEAR) - from.get(Calendar.DAY_OF_YEAR) + 1);
                	row.createCell(10).setCellValue(rs.getString("DIVISION"));
                	
                }
                rs.close();
                ps.close();
        		
                File file = File.createTempFile("docusafe", "absence");
                file.deleteOnExit();
                OutputStream os = new FileOutputStream(file);
                wb.write(os);
                os.close();
                
                ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"Zestawienie nr."+fileId+".xls\"");
        	}
        	catch (Exception e) 
        	{
				e.printStackTrace();
        		log.error(e.getMessage());
        		if(log.isDebugEnabled())
        		{
        			log.debug(e.getMessage(),e);
        		}
			}
        }
    }

	public List<Zestawienie> getZestawieniaBeans() {
		return zestawieniaBeans;
	}

	public void setZestawieniaBeans(List<Zestawienie> zestawieniaBeans) {
		this.zestawieniaBeans = zestawieniaBeans;
	}

	public Integer getNotInCount() {
		return notInCount;
	}

	public void setNotInCount(Integer notInCount) {
		this.notInCount = notInCount;
	}

	public Integer getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(Integer maxNumber) {
		this.maxNumber = maxNumber;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
	
}
