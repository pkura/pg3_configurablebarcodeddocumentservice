package pl.compan.docusafe.web.admin.absences;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.TimePeriod;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje edycji wybranego urlopu pracownika.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeAbsenceAction extends EventActionSupport 
{
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeAbsenceAction.class);
	
	/**
	 * String Manager
	 */
	private static final StringManager sm = StringManager
			.getManager(EmployeeAbsenceAction.class.getPackage().getName());
	
	/**
	 * Sta�a wykorzystywana w przypadku wyst�pienia b��d�w w formularzu
	 */
	private static final String FORM_ERROR = "FORM_FIELDS_ERROR";

	
	/**
	 * Mapa z typami urlop�w
	 */
	private Map<String, String> absencesTypeMap;
	
	/**
	 * Kod wybranego typu urlopu
	 */
	private String selectedAbsenceTypeCode;

	/**
	 * Wybrany urlop
	 */
	private Absence absence;
            
	/**
	 * Obiekt karty pracownika
	 */
	private EmployeeCard empCard;
	
	/**
	 * Zakres dat
	 */
	private TimePeriod timePeriod;
	
	/**
	 * Mapa z dost�pnymi u�ytkownikami
	 */
	private Map<String, String> usersMap = new LinkedHashMap<String, String>();
	
	/**
	 * Identyfikator wybranego u�ytkownika
	 */
	private String selectedUserName;
	
	/**
	 * Login u�ytkownika zenw�trznego, kt�ry zaakceptowa� wniosek
	 */
	private String externalUser;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		LoadAbsenceTypesMap loadAbsenceTypesMap = new LoadAbsenceTypesMap();
		LoadUsersMap loadUsersMap = new LoadUsersMap();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(loadAbsenceTypesMap)
			.append(loadUsersMap)
			.append(new LoadEmployeeAbsence())
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("saveEmployeeAbsence")
			.append(OpenHibernateSession.INSTANCE)
			.append(loadAbsenceTypesMap)
			.append(loadUsersMap)
			.append(new SaveEmployeeAbsence())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Klasa wczytuj�ca wybrany urlop pracownika 
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadEmployeeAbsence implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (absence != null && absence.getId() != null)
				{
					// je�li istnieje dany obiekt urlopu pobranie go z bazy
					absence = Finder.find(Absence.class, absence.getId());
					empCard = absence.getEmployeeCard();
					empCard.getId(); // lazyyyy
					timePeriod = new TimePeriod(DateUtils.formatJsDate(absence.getStartDate()), 
							DateUtils.formatJsDate(absence.getEndDate()));
					selectedAbsenceTypeCode = absence.getAbsenceType().getCode();
					
					if (absence.getEditor() != null)
						selectedUserName = absence.getEditor().getName();
					else if (absence.getEditorFullName() != null)
						externalUser = absence.getEditorFullName();
				}
				else 
				{
					// nie ma obiektu urlopu - tworzony b�dzie w formularzu nowy wpis
					if (empCard == null || empCard.getId() == null)
						throw new EdmException(sm.getString("NieWybranoKartyPracownika"));
					
					empCard = Finder.find(EmployeeCard.class, empCard.getId());
					empCard.getId(); // lazyyyy
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa �aduj�ca map� z typami urlop�w dla elementu select
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadAbsenceTypesMap implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				// pobiera typy wniosk�w urlopowych opr�cz tych oznaczonych jako wymiar
				// - ich kod rozpoczyna si� od skr�tu WU
				@SuppressWarnings("unchecked")
				List<Object[]> rows = DSApi.context().session()
					.createQuery("select at.code, at.name from " + AbsenceType.class.getName() 
							+ " at order by at.code asc")
					.list();
				
				absencesTypeMap = new LinkedHashMap<String, String>();
				for (Object[] row : rows)
				{
					absencesTypeMap.put((String) row[0], (String) row[0] + " - " + (String) row[1]);
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa akcji �aduj�ca map� u�ytkownik�w dla elementu select
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadUsersMap implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();			
				// za�adowanie u�ytkownik�w
				List users = DSApi.context().session().createQuery("select u.name, u.lastname, u.firstname from " 
						+ UserImpl.class.getName() + " u order by u.lastname asc")
						.list();
				
				usersMap.put("", "Wybierz u�ytkownika ...");
				for (int i = 0; i < users.size(); i++)
				{
					Object[] row = (Object[]) users.get(i);
					usersMap.put((String) row[0], (String) row[1] + " " + (String) row[2]);
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa edytuj�ca lub zapisuj�ca nowy urlop pracownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class SaveEmployeeAbsence implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				// pobranie obiektu karty pracownika
				empCard = Finder.find(EmployeeCard.class, empCard.getId());
				
				// sprawdzenie p�l formularza
				checkFormFields();

				absence.setEmployeeCard(empCard);
				
				AbsenceType absenceType = (AbsenceType) DSApi.context().session()
					.createQuery("from " + AbsenceType.class.getName() + " at where at.code = ?")
					.setString(0, selectedAbsenceTypeCode)
					.uniqueResult();
				absence.setAbsenceType(absenceType);
				
				absence.setStartDate(DateUtils.parseJsDate(timePeriod.getStartDate()));
				absence.setEndDate(DateUtils.parseJsDate(timePeriod.getEndDate()));
				absence.setCtime(new Date());
				
				if (StringUtils.isNotBlank(selectedUserName))
				{
					DSUser dsUser = DSUser.findByUsername(selectedUserName);
					absence.setEditor((UserImpl) dsUser);
					absence.setEditorFullName(dsUser.asFirstnameLastname());
					externalUser = null;
				} 
				else if (StringUtils.isNotBlank(externalUser))
				{
					absence.setEditor(null);
					absence.setEditorFullName(externalUser);
					selectedUserName = null;
				}
				
				if (absence.getId() != null)
					DSApi.context().session().update(absence);
				else
					AbsenceFactory.chargeAbsence(empCard, absence);

				
				addActionMessage(sm.getString("PoprawnieZapisanoZmiany"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FORM_ERROR))
					addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}

		/**
		 * Sprawdza poprawno�� p�l formularza, w przypadku b��d�w rzuca wyj�tek EdmException
		 * 
		 * @throws EdmException
		 */
		private void checkFormFields() throws EdmException
		{	
			if (absence.getYear() < 2000)
				addActionError(sm.getString("PodajPoprawnieRok"));
			
			if (absence.getDaysNum() < 0)
				addActionError(sm.getString("PodajPoprawnieLiczbeDniUrlopu"));
			
			if (timePeriod == null || timePeriod.getStartDate() == null
					|| timePeriod.getEndDate() == null)
				addActionError(sm.getString("PodajObieDaty"));
			else if (timePeriod != null)
			{
				Date startDate = DateUtils.parseJsDate(timePeriod.getStartDate());
				Date endDate = DateUtils.parseJsDate(timePeriod.getEndDate());
				if (startDate == null || endDate == null || (!startDate.before(endDate) && !startDate.equals(endDate)))
					addActionError(sm.getString("NiepoprawniePodanoDaty"));
			}
			
			if (empCard.getId() == null)
				addActionError(sm.getString("NieWybranoKartyPracownika"));
			
			if (selectedAbsenceTypeCode == null)
				addActionError(sm.getString("NieWybranoTypuUrlopu"));
			
			if (hasActionErrors())
				throw new EdmException(FORM_ERROR);
		}
	}

	public Absence getAbsence() 
	{
		return absence;
	}

	public void setAbsence(Absence absence) 
	{
		this.absence = absence;
	}

	public Map<String, String> getAbsencesTypeMap() 
	{
		return absencesTypeMap;
	}

	public void setAbsencesTypeMap(Map<String, String> absencesTypeMap) 
	{
		this.absencesTypeMap = absencesTypeMap;
	}

	public String getSelectedAbsenceTypeCode() 
	{
		return selectedAbsenceTypeCode;
	}

	public void setSelectedAbsenceTypeCode(String selectedAbsenceTypeCode) 
	{
		this.selectedAbsenceTypeCode = selectedAbsenceTypeCode;
	}

	public TimePeriod getTimePeriod() 
	{
		return timePeriod;
	}

	public void setTimePeriod(TimePeriod timePeriod) 
	{
		this.timePeriod = timePeriod;
	}

	public EmployeeCard getEmpCard() 
	{
		return empCard;
	}

	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}
	
	public Map<String, String> getUsersMap() 
	{
		return usersMap;
	}

	public void setSelectedUserName(String selectedUserId) 
	{
		this.selectedUserName = selectedUserId;
	}

	public String getSelectedUserName() 
	{
		return selectedUserName;
	}
	
	public String getExternalUser()
	{
		return externalUser;
	}
	
	public void setExternalUser(String externalUser)
	{
		this.externalUser = externalUser;
	}
}
