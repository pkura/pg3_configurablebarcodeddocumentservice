package pl.compan.docusafe.web.admin.absences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.AbsenceReportEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje wy�wietlenia listy raportu urlop�w.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsencesListAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AbsencesListAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager sm = StringManager.getManager(AbsencesListAction.class.getPackage().getName());
	
	/**
	 * Lista urlop�w
	 */
	private List<AbsenceReportEntry> absences;
	
	/**
	 * Rok, z kt�rego b�d� brane pod uwag� urlopy
	 */
	private int currentYear;
	
	/**
	 * Obiekt empCard wykorzystywany jest do ustalenia parametr�w wyszukiwania
	 */
	private EmployeeCard empCard;
	
	/**
	 * Tabela przechowuj�ca identyfikatory wymiar�w urlopowych do usuni�cia
	 */
	private Long[] deleteIds;
	
	/**
	 * Ustala czy mo�na naliczy� urlopy na kolejny rok
	 */
	private boolean canChargeAbsences;
	
	/**
	 * Numer strony
	 */
	private int pageNumber = 0;
	
	/**
	 * Liczba rezultat�w
	 */
	private int maxResults = 20;
	
	/**
	 * Obiekt pager'a
	 */
	private Pager pager;

	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		GenerateAbsencesReport absencesReport = new GenerateAbsencesReport();
		CanChargeAbsencesFromPreviousYear canChargeAbsencesFromPreviousYear = new CanChargeAbsencesFromPreviousYear();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(absencesReport)
			.append(canChargeAbsencesFromPreviousYear)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("goSearch")
			.append(OpenHibernateSession.INSTANCE)
			.append(absencesReport)
			.append(canChargeAbsencesFromPreviousYear)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("deleteEmployeeAbsences")
			.append(OpenHibernateSession.INSTANCE)
			.append(new DeleteEmployeeAbsences())
			.append(absencesReport)
			.append(canChargeAbsencesFromPreviousYear)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("chargeAbsences")
			.append(OpenHibernateSession.INSTANCE)
			.append(new ChargeAbsencesFromPreviousYear())
			.append(absencesReport)
			.append(canChargeAbsencesFromPreviousYear)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("generateReport")
			.append(OpenHibernateSession.INSTANCE)
			.append(new XlsReport())
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doPdf")
			.append(OpenHibernateSession.INSTANCE)
			.append(new PdfReport())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Akcja generuj�ca raport z wykorzystania urlopu w danym roku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class GenerateAbsencesReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (currentYear == 0)
					currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				
				// generuje raport z paramerami filtrowania
				absences = AbsenceFactory.generateReportFromYear(currentYear, empCard, pageNumber, maxResults);
				createPager();
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Tworzy pager dla widoku
		 * 
		 * @throws EdmException
		 */
		private void createPager() throws EdmException
		{
			int count = AbsenceFactory.getCountReportResults(currentYear, empCard);
			pager = new Pager(new Pager.LinkVisitor() {
				public String getLink(int offset) 
				{
					return offset + "";
				}
			}, pageNumber, maxResults, count, 5, true);
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� usuni�cia wybranych wymiar�w urlopowych z danego roku, wraz z podlegaj�cym im urlopom
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeleteEmployeeAbsences implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (deleteIds == null || deleteIds.length == 0)
					throw new EdmException(sm.getString("NieWybranoZadnychWpisow"));
				
				DSApi.context().session().createQuery("delete from " + Absence.class.getName() 
						+ " abs where abs.employeeCard.id in (:ids) and abs.year = :year")
						.setParameterList("ids", deleteIds)
						.setInteger("year", currentYear)
						.executeUpdate();
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Sprawdza czy mozna naliczyc urlopy na kolejny rok
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class CanChargeAbsencesFromPreviousYear implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				Integer currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				Integer previousYear = (Integer) DSApi.context().session().createQuery(
						"select a.year from " + Absence.class.getName() + " a group by a.year order by a.year desc")
						.setMaxResults(1)
						.uniqueResult();
				
				if (previousYear != null && (previousYear.intValue() + 1 == currentYear.intValue()))
					canChargeAbsences = true;
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Akcja naliczaj�ca urlopy na bierz�cy rok
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class ChargeAbsencesFromPreviousYear implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				AbsenceFactory.chargeAbsencesFromPreviousYear();
				addActionMessage(sm.getString("PoprawnieNaliczonoUrlopy"));
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa generujaca raport na podstawie danych z tabeli
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class XlsReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (currentYear == 0)
					currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				
				List<AbsenceReportEntry> absencesToExcell = AbsenceFactory.generateReportFromYear(currentYear, empCard);
				
				AbsencesXlsReport xlsReport = new AbsencesXlsReport("Raport wymiar�w urlopowych", absencesToExcell);
				xlsReport.generate();
				
				createContent(xlsReport, event);
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				addActionError(ex.getMessage());
			}
		}
		
		private void createContent(AbsencesXlsReport report, ActionEvent event) 
			throws EdmException
		{
			try
            {
                File  tmpfile = File.createTempFile("DocuSafe", "tmp");
                tmpfile.deleteOnExit();
                FileOutputStream fis = new FileOutputStream(tmpfile);
                report.getWorkbook().write(fis);
                fis.close();
                ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
            }
            catch(IOException ex)
            {
                addActionError(ex.getMessage());
                LOG.error(ex.getMessage(), ex);
            }
            event.setResult(NONE);
		}
	}
	
	private class PdfReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				ServletUtils.streamFile(ServletActionContext.getResponse(), AbsenceFactory.pdfCard(empCard.getExternalUser(), currentYear), "application/pdf", "Content-Disposition: attachment; filename=\"employeeCard.pdf\"");
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
                LOG.error(e.getMessage(), e);
			}
		}
	}
	
	public List<AbsenceReportEntry> getAbsences() 
	{
		return absences;
	}

	public int getCurrentYear() 
	{
		return currentYear;
	}

	public void setCurrentYear(int currentYear) 
	{
		this.currentYear = currentYear;
	}

	/**
	 * Zwraca map� z rokiem dla elementu select
	 * 
	 * @return
	 */
	public Map<Integer, Integer> getYearsMap() 
	{
		Map<Integer, Integer> yearsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 2000; i <= GregorianCalendar.getInstance().get(GregorianCalendar.YEAR); i++)
			yearsMap.put(i, i);
		return yearsMap;
	}

	public EmployeeCard getEmpCard() 
	{
		return empCard;
	}

	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}

	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}
	
	public boolean isChargeAbsencesEnabled()
	{
		return canChargeAbsences;
	}
        
        

	public int getPageNumber() 
	{
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) 
	{
		this.pageNumber = pageNumber;
	}

	public int getMaxResults() 
	{
		return maxResults;
	}

	public void setMaxResults(int maxResults) 
	{
		this.maxResults = maxResults;
	}

	public Pager getPager() 
	{
		return pager;
	}

	public void setPager(Pager pager) 
	{
		this.pager = pager;
	}
	
	public Map<Integer, Integer> getMaxResultsMap()
	{
		Map<Integer, Integer> maxResultsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 5; i < 41; i += 5)
			maxResultsMap.put(i, i);
		return maxResultsMap;
	}
}
