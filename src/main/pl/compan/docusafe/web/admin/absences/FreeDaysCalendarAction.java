package pl.compan.docusafe.web.admin.absences;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class FreeDaysCalendarAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FreeDaysCalendarAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager sm = StringManager.getManager(FreeDaysCalendarAction.class.getPackage().getName());
	
	private List<FreeDay> freeDays;
	
	private FreeDay freeDay;
	
	private String dayDate;
	
	private Long[] deleteIds;
	
	protected void setup() 
	{
		LoadFreeDaysList freeDaysList = new LoadFreeDaysList();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(freeDaysList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("addFreeDay")
			.append(OpenHibernateSession.INSTANCE)
			.append(new AddFreeDay())
			.append(freeDaysList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("deleteFreeDays")
			.append(OpenHibernateSession.INSTANCE)
			.append(new DeleteFreeDays())
			.append(freeDaysList)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * �aduje list� dni wolnych
	 * 
	 * @author User
	 */
	private class LoadFreeDaysList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				freeDays = AbsenceFactory.getFreeDays();
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				addActionError(ex.getMessage());
			}
		}
	}
	
	/**
	 * Dodaje dzie� wolny
	 * 
	 * @author User
	 */
	private class AddFreeDay implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				DSApi.context().begin();

				if (freeDay == null || StringUtils.isEmpty(dayDate) || DateUtils.parseJsDate(dayDate) == null) {
					throw new EdmException(sm.getString("Wype�nij poprawnie wszystkie pola"));
				} else {
					freeDay.setDate(DateUtils.parseJsDate(dayDate));
				}
				List<FreeDay> list = freeDay.find(freeDay.getDate());
				if (list != null && list.size() > 0) {
					throw new DSException("Ten dzie� znajduje si� w kalendarzu dni wolnych!");
				} else {
					DSApi.context().session().save(freeDay);

					addActionMessage(sm.getString("Poprawnie dodano nowy wpis"));

					DSApi.context().commit();
				}
			} catch (EdmException ex) {
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				if (!hasActionErrors())
					addActionError(ex.getMessage());
			}
		}
	}
	
	/**
	 * Usuwa wskazane dni wolne
	 * 
	 * @author User
	 */
	private class DeleteFreeDays implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (deleteIds == null || deleteIds.length == 0)
					throw new EdmException(sm.getString("Nie wybrano �adnych wpisow"));
				
				DSApi.context().session().createQuery("delete from " + FreeDay.class.getName() + " f where f.id in (:ids)")
					.setParameterList("ids", deleteIds)
					.executeUpdate();
				
				addActionMessage(sm.getString("Poprawnie usuni�to wybrane wpisy"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				if (!hasActionErrors())
					addActionError(ex.getMessage());
			}
		}
	}

	public List<FreeDay> getFreeDays() 
	{
		return freeDays;
	}

	public void setFreeDays(List<FreeDay> freeDays) 
	{
		this.freeDays = freeDays;
	}

	public FreeDay getFreeDay() 
	{
		return freeDay;
	}

	public void setFreeDay(FreeDay freeDay) 
	{
		this.freeDay = freeDay;
	}

	public String getDayDate() 
	{
		return dayDate;
	}

	public void setDayDate(String dayDate) 
	{
		this.dayDate = dayDate;
	}

	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}
}
