package pl.compan.docusafe.web.admin.absences;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.ObjectNotFoundException;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.AbsenceReportEntry;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.calendar.sql.TimeImpl;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje edycji wymiaru urlopu nale�nego i zaleg�ego oraz
 * wy�wietlaj�ca list� urlop�w wybranego pracownika w danym roku
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EmployeeAbsencesAction extends EventActionSupport 
{
	/**
	 * Serial Version UID 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeAbsencesAction.class);
	
	/**
	 * StringManager
	 */
	private static final StringManager sm = StringManager.getManager(EmployeeAbsencesAction.class.getPackage().getName());
	
	/**
	 * Sta�a oznaczaj�ca wyst�pienie b��d�w w formularzu
	 */
	private static final String FORM_ERRORS = "FORM_ERRORS";
	
	/**
	 * Obiekt karty pracownika
	 */
	private EmployeeCard empCard;
	
	/**
	 * Raport z wykorzystania urlopu dla danego pracownika
	 */
	private AbsenceReportEntry reportEntry;
	
	/**
	 * Lista urlop�w pracownika
	 */
	private List<EmployeeAbsenceEntry> emplAbsences;
	
	/**
	 * Wybrany rok urlop�w
	 */
	private int currentYear;
	
	/**
	 * Karty pracownik�w
	 */
	private Map<Long, String> employeeCardsMap;
	
	/**
	 * Identyfikator wybranej karty pracownika
	 */
	private Long selectedEmployeeCardId;
	
	/**
	 * Identyfikatory urlop�w do usuni�cia
	 */
	private Long[] deleteIds;

	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("createNewAbsences")
			.append(OpenHibernateSession.INSTANCE)
			.append(new CreateNewEmployeeAbsences())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("deleteAbsences")
			.append(OpenHibernateSession.INSTANCE)
			.append(new DeleteAbsences())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("generateReport")
			.append(OpenHibernateSession.INSTANCE)
			.append(new XlsReport())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Przygotowanie formularza wymiar�w urlopowych wybranego pracownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (currentYear == 0)
					currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				
				if (empCard != null && empCard.getId() != null)
				{
					try
					{
						empCard = Finder.find(EmployeeCard.class, empCard.getId());
						empCard.getId(); // lazyyyy
						// pobranie urlop�w, kt�re pracownik wzi�� podczas wskazanego roku kalendarzowego
						emplAbsences = AbsenceFactory.getEmployeeAbsences(empCard, currentYear);
					}
					catch (ObjectNotFoundException ex)
					{
						throw new EdmException(sm.getString("ObiektOPodanymIdentyfikatorzeNieIstnieje"));
					}
				}
				else
				{
					// je�li empCard jest null lub nie posiada ID w widoku zostanie wy�wietlony formularz do utworzenia nowego wpisu
					// wymiaru urlopowego - za�adowanie dla formularza mapy kart pracownik�w
					loadEmployeeCardsMap();
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * �aduje list� kart pracowniczych, kt�re nie posiadaj� przypisanego wymiaru urlopowego w danym roku.
		 * 
		 * @throws EdmException
		 */
		private void loadEmployeeCardsMap() throws EdmException
		{
			// zwraca list� kart pracowniczych, kt�re nie maj� przypisanych �adnych 
			// wymiar�w urlopowych w podanym roku
			List<Object[]> rows = DSApi.context().session()
				.createQuery("select ec.id, ec.KPX, ec.externalUser "
					+ "from " + EmployeeCard.class.getName() + " ec "
					+ "where ec not in (select abs.employeeCard from " + Absence.class.getName() + " abs where abs.year = ? and abs.employeeCard = ec) "
					+ "order by ec.externalUser asc")
				.setInteger(0, currentYear)
				.list();
		
			employeeCardsMap = new LinkedHashMap<Long, String>();
			for (Object[] row : rows)
			{
				String userName = (String) row[2]; //+ " " + (String) row[3];
				if (row[2] == null)
					userName = "Karta nie przypisana do u�ytkownika";
				
				try
				{
					DSUser user = DSUser.findByUsername(userName);
					userName = user.asLastnameFirstname();
				}
				catch (UserNotFoundException ex)
				{
					// nic nie rob
				}
				
				employeeCardsMap.put((Long) row[0],  userName + " - " + (String) row[1] );
			}
		}
	}
	
	/**
	 * Tworzy nowy wpis wymiaru urlopowego dla wskazanego pracownika w danym roku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class CreateNewEmployeeAbsences implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				// sprawdzenie pol formularza
				checkFormFields();
				 
				empCard = (EmployeeCard) DSApi.context().session().createQuery(
						"from " + EmployeeCard.class.getName() + " ec where ec.id = ?")
					.setLong(0, selectedEmployeeCardId)
					.uniqueResult();
				
				if (empCard == null)
					throw new EdmException(sm.getString("WybierzKartePracownika"));
				
				// sprawdzenie czy karta pracownika nie ma przypadkiem ju� przypisanych wymiar�w urlopowych na dany rok
				checkEmpCardAbsencesInCurrentYear();
				
				empCard.setAbsences(new HashSet<Absence>());
				
				// pobranie typu urlopu nale�nego i zaleg�ego
				AbsenceType dueAbsenceType = Finder.find(AbsenceType.class, "WUN");
				AbsenceType overduAbsenceType = Finder.find(AbsenceType.class, "WUZ");
				if (dueAbsenceType == null || overduAbsenceType == null)
					throw new EdmException(sm.getString("BladAplikacjiBrakTypowUrlopowWBazieDanych"));
				
				// utworzenie nowego wpisu wymiaru urlopu nale�nego
				Absence dueAbsence = new Absence(currentYear, new GregorianCalendar(currentYear, GregorianCalendar.JANUARY, 1).getTime(), 
						new GregorianCalendar(currentYear, GregorianCalendar.DECEMBER, 31).getTime(), 
						reportEntry.getDueDays(), reportEntry.getInfo(), new Date(), empCard, dueAbsenceType, null, null);
				
				// utworzenie nowego wpisu wymiaru urlopu zaleg�ego
				Absence overdueAbsence = new Absence(currentYear, new GregorianCalendar(currentYear, GregorianCalendar.JANUARY, 1).getTime(), 
						new GregorianCalendar(currentYear, GregorianCalendar.DECEMBER, 31).getTime(), 
						reportEntry.getOverdueDays(), sm.getString("NadanieWymiaruZaleglego"), new Date(), empCard, overduAbsenceType, null, null);
				
				empCard.getAbsences().add(dueAbsence);
				empCard.getAbsences().add(overdueAbsence);
				
				// zapisanie danych
				DSApi.context().session().save(dueAbsence);
				DSApi.context().session().save(overdueAbsence);
				DSApi.context().session().update(empCard);
				
				addActionMessage(sm.getString("PoprawnieDodanoWpisWymiaruUrlopowego"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FORM_ERRORS))
					addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Sprawdzenie poprawno�ci p�l formularza
		 * 
		 * @throws EdmException
		 */
		private void checkFormFields() throws EdmException
		{
			if (currentYear == 0)
				addActionError(sm.getString("PoprawnieWybierzRok"));
			
			if (selectedEmployeeCardId == null)
				addActionError(sm.getString("WybierzKartePracownika"));
			
			if (reportEntry.getDueDays() < 0)
				addActionError(sm.getString("PodajPoprawnieLiczbeDniUrlopuNaleznego"));
			
			if (reportEntry.getOverdueDays() < 0)
				addActionError(sm.getString("PodajPoprawnieLiczbeDniUrlopuZaleglego"));
			
			if (hasActionErrors())
				throw new EdmException(FORM_ERRORS);
		}
		
		/**
		 * sprawdzenie czy karta pracownika nie ma przypadkiem ju� przypisanych wymiar�w urlopowych na dany rok
		 * 
		 * @throws EdmException
		 */
		private void checkEmpCardAbsencesInCurrentYear() throws EdmException
		{
			Integer hasAbsences = (Integer) DSApi.context().session().createQuery("select 1 from " + Absence.class.getName() + " abs "
					+ "where abs.employeeCard = ? and abs.year = ?")
					.setParameter(0, empCard)
					.setInteger(1, currentYear)
					.setMaxResults(1)
					.uniqueResult();
			if (hasAbsences != null)
				throw new EdmException(sm.getString("WymiarUrlopowyDlaTejKartyPracownikaJestPrzypisany"));
		}
	}
	
	/**
	 * Usuwa wybrane urlopy
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeleteAbsences implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (deleteIds == null || deleteIds.length == 0)
					return;
				
				DSApi.context().begin();
	
				for (Long dId : deleteIds) 
				{				
					//DSApi.context().session().createQuery("delete from " + Absence.class.getName() + " abs where abs.id in (:ids)")
					//	.setParameterList("ids", deleteIds)
					//	.executeUpdate();
					Absence abs = DSApi.getObject(Absence.class, dId);
					abs.delete();
					addActionMessage(sm.getString("PoprawnieUsunietoWybraneUrlopy"));
				}
							
				DSApi.context().commit();
			}
			catch (Exception ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	private class XlsReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (currentYear == 0)
					currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				
				if (empCard != null && empCard.getId() != null)
				{
					try
					{
						empCard = Finder.find(EmployeeCard.class, empCard.getId());
						empCard.getId(); // lazyyyy
						// pobranie urlop�w, kt�re pracownik wzi�� podczas wskazanego roku kalendarzowego
						emplAbsences = AbsenceFactory.getEmployeeAbsences(empCard, currentYear);
						
						EmployeeAbsencesXlsReport absencesXlsReport = 
							new EmployeeAbsencesXlsReport("Raport z wykorzystania urlopu", emplAbsences);
						
						absencesXlsReport.generate();
						createContent(absencesXlsReport, event);
					}
					catch (ObjectNotFoundException ex)
					{
						throw new EdmException(sm.getString("ObiektOPodanymIdentyfikatorzeNieIstnieje"));
					}
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		private void createContent(EmployeeAbsencesXlsReport report, ActionEvent event) 
			throws EdmException
		{
			try
			{
				File  tmpfile = File.createTempFile("DocuSafe", "tmp");
				tmpfile.deleteOnExit();
				FileOutputStream fis = new FileOutputStream(tmpfile);
				report.getWorkbook().write(fis);
				fis.close();
				ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
			}
			catch(IOException ex)
			{
				addActionError(ex.getMessage());
         	  LOG.error(ex.getMessage(), ex);
			}
			event.setResult(NONE);
		}
	}

	public EmployeeCard getEmpCard() 
	{
		return empCard;
	}

	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}

	public AbsenceReportEntry getReportEntry() 
	{
		return reportEntry;
	}

	public void setReportEntry(AbsenceReportEntry reportEntry) 
	{
		this.reportEntry = reportEntry;
	}

	public int getCurrentYear() 
	{
		return currentYear;
	}

	public void setCurrentYear(int currentYear) 
	{
		this.currentYear = currentYear;
	}

	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return emplAbsences;
	}
	
	/**
	 * Zwraca map� z rokiem dla elementu select
	 * 
	 * @return
	 */
	public Map<Integer, Integer> getYearsMap() 
	{
		Map<Integer, Integer> yearsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 2000; i <= GregorianCalendar.getInstance().get(GregorianCalendar.YEAR); i++)
			yearsMap.put(i, i);
		return yearsMap;
	}

	public Map<Long, String> getEmployeeCardsMap() 
	{
		return employeeCardsMap;
	}

	public void setEmployeeCardsMap(Map<Long, String> employeeCardsMap) 
	{
		this.employeeCardsMap = employeeCardsMap;
	}

	public Long getSelectedEmployeeCardId() 
	{
		return selectedEmployeeCardId;
	}

	public void setSelectedEmployeeCardId(Long selectedEmployeeCardId) 
	{
		this.selectedEmployeeCardId = selectedEmployeeCardId;
	}

	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}
}
