package pl.compan.docusafe.web.admin;

import java.util.*;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.users.auth.ClientAddrPrincipal;
import pl.compan.docusafe.core.users.auth.FullNamePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SessionsAction.java,v 1.7 2007/11/27 12:11:46 mariuszk Exp $
 */
public class SessionsAction extends EventActionSupport
{
    private Set<Map<String, Object>> sessionBeans;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);

        registerListener("doLogout").
            append(new Logout()).
            append(fillForm);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            sessionBeans = new LinkedHashSet<Map<String, Object>>();

            for (Iterator iter= Docusafe.getSessions().iterator(); iter.hasNext(); )
            {
                HttpSession session = (HttpSession) iter.next();
                Map<String, Object> bean = new HashMap<String, Object>();

                bean.put("session", session);
                bean.put("sessionCreationTime", new Date(session.getCreationTime()));
                bean.put("sessionLastAccessedTime", new Date(session.getLastAccessedTime()));

                if(AvailabilityManager.isAvailable("menu.left.kancelaria.dwrprzyjmipismo")) {
                    Map<String, List<DwrFileItem>> files = Maps.newHashMap();
                    ListMultimap<String, DwrFileItem> storageFiles = DwrAttachmentStorage.getInstance().getAllFiles(session);
                    //storageFiles.as
                    bean.put("dwrFiles", DwrAttachmentStorage.getInstance().getAllFiles(session).asMap());
                }

                Subject subject = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
                if (subject != null)
                {
                    Set userPrincipals = subject.getPrincipals(UserPrincipal.class);
                    if (userPrincipals.size() > 0)
                    {
                        bean.put("userPrincipal", userPrincipals.iterator().next());
                        bean.put("userPrincipalCount", new Integer(userPrincipals.size()));
                    }
                    Set fullNamePrincipals = subject.getPrincipals(FullNamePrincipal.class);
                    if (fullNamePrincipals.size() > 0)
                    {
                        bean.put("fullNamePrincipal", fullNamePrincipals.iterator().next());
                    }
                    Set clientAddrPrincipals = subject.getPrincipals(ClientAddrPrincipal.class);
                    if (clientAddrPrincipals.size() > 0)
                    {
                        bean.put("clientAddr", ((ClientAddrPrincipal) clientAddrPrincipals.iterator().next()).getName());
                    }
                    sessionBeans.add(bean);
                }
            }
        }
    } 

    /**
     * Uniewa�nia wszystkie sesje z wyj�tkiem bie��cej.
     */
    private class Logout implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            for (Iterator iter=Docusafe.getSessions().iterator(); iter.hasNext(); )
            {
                HttpSession session = (HttpSession) iter.next();

                if (session != ServletActionContext.getRequest().getSession())
                {
                    session.invalidate();
                }
            }
        }
    }

    public Set getSessionBeans()
    {
        return sessionBeans;
    }
}
