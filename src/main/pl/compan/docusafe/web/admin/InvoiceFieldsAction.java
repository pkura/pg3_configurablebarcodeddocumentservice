package pl.compan.docusafe.web.admin;

import com.google.common.collect.Lists;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlObszaryMapping;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;

public class InvoiceFieldsAction extends EventActionSupport {

	private Logger LOG = LoggerFactory.getLogger(InvoiceFieldsAction.class);
	private static final long serialVersionUID = 1L;
	
	private SearchResults<UlObszaryMapping> invoiceFields;
	
	private List<DSUser> users;
	private List<EnumItem> iKinds;
	private List<EnumItem> cMpks;
	private List<EnumItem> cProjects;
	private List<EnumItem> cTypes;
	private List<EnumItem> cInventories;
	private List<EnumItem> cFundsources;
	
	private Long id;
    private String sortField;
    private Boolean ascending;
    
    //pola z formatek
    private Long dsuser;
    private String iKind;
    private String cMpk;
    private String cProject;
    private String cType;
    private String cInventory;
    private String cFundsource;
    private String remark;
    
	private Pager pager;
	private Integer offset; 

	private static final int LIMIT = 50;	
	
	public String getcProject() {
		return cProject;
	}

	public void setcProject(String cProject) {
		this.cProject = cProject;
	}

	public String getcType() {
		return cType;
	}

	public void setcType(String cType) {
		this.cType = cType;
	}

	public String getcMpk() {
		return cMpk;
	}

	public void setcMpk(String cMpk) {
		this.cMpk = cMpk;
	}

	public String getiKind() {
		return iKind;
	}

	public void setiKind(String iKind) {
		this.iKind = iKind;
	}

	public String getcInventory() {
		return cInventory;
	}

	public void setcInventory(String cInventory) {
		this.cInventory = cInventory;
	}

	public String getcFundsource() {
		return cFundsource;
	}

	public void setcFundsource(String cFundsource) {
		this.cFundsource = cFundsource;
	}

	public Long getDsuser() {
		return dsuser;
	}
	
	public Integer getDsuserInt() {
		return dsuser!=null ? dsuser.intValue() : null;
	}

	public void setDsuser(Long dsuser) {
		this.dsuser = dsuser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SearchResults<UlObszaryMapping> getInvoiceFields() {
		return invoiceFields;
	}

	public void setInvoiceFields(SearchResults<UlObszaryMapping> invoiceFields) {
		this.invoiceFields = invoiceFields;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public List<EnumItem> getiKinds() {
		return iKinds;
	}

	public void setiKinds(List<EnumItem> iKinds) {
		this.iKinds = iKinds;
	}

	public List<EnumItem> getcMpks() {
		return cMpks;
	}

	public void setcMpks(List<EnumItem> cMpks) {
		this.cMpks = cMpks;
	}

	public List<EnumItem> getcProjects() {
		return cProjects;
	}

	public void setcProjects(List<EnumItem> cProjects) {
		this.cProjects = cProjects;
	}

	public List<EnumItem> getcTypes() {
		return cTypes;
	}

	public void setcTypes(List<EnumItem> cTypes) {
		this.cTypes = cTypes;
	}

	public List<EnumItem> getcInventories() {
		return cInventories;
	}

	public void setcInventories(List<EnumItem> cInventories) {
		this.cInventories = cInventories;
	}

	public List<EnumItem> getcFundsources() {
		return cFundsources;
	}

	public void setcFundsources(List<EnumItem> cFundsources) {
		this.cFundsources = cFundsources;
	}

	@Override
	protected void setup() {
		 FillForm fillForm = new FillForm();
		 
		 registerListener(DEFAULT_ACTION).
		 	append(OpenHibernateSession.INSTANCE).
		 	//append(new CheckPermission()).
		 	append("FILL_FORM", fillForm).
		 	appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doAdd").
		 	append(OpenHibernateSession.INSTANCE).
		 	append("add", new Save(false)).
		 	//append(new CheckPermission()).
		 	append("FILL_FORM", fillForm).
		 	appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doUpdate").
			append(OpenHibernateSession.INSTANCE).
			append("update", new Save(true)).
			// append(new CheckPermission()).
			append("FILL_FORM", fillForm).appendFinally(CloseHibernateSession.INSTANCE);
		 
		 registerListener("doDelete").
		 	append(OpenHibernateSession.INSTANCE).
		 	append("delete", new Delete()).
		 	// append(new CheckPermission()).
		 	append("FILL_FORM", fillForm).appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class Save implements ActionListener {

		private boolean update = false;
		
		public Save(boolean update) {
			this.update = update;
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {
				try {
					UlObszaryMapping item = null;
					if (!update) {
						item = new UlObszaryMapping();
					} else if (getId() != null) {
						item = UlObszaryMapping.find(getId());
					}
					createOrUpdateMappingItem(update, item);
				} catch (EdmException e) {
					LOG.error(e.getMessage(), e);
				}

		}

		protected void createOrUpdateMappingItem(boolean update, UlObszaryMapping item) throws EdmException {
			if (getDsuser() != null) {
				if (getcMpk() != null || getiKind() != null || getcProject() != null || getcType() != null || getcInventory() != null || getcFundsource() != null) {

					if (!UlObszaryMapping.find(getcMpk(), getiKind(), getcProject(), getcType(), getcInventory(), getcFundsource()).isEmpty()) {
						addActionMessage("Znaleziono wpis o tych samych parametrach. Jego id: "
								+ UlObszaryMapping.find(getcMpk(), getiKind(), getcProject(), getcType(), getcInventory(), getcFundsource()).get(0).getId());
					}
					try {
						// DSApi.openAdmin();
						DSApi.context().begin();
						item.setCMpk(getcMpk());
						item.setCProject(getcProject());
						item.setDsuser(getDsuserInt());
						item.setIKind(getiKind());
						item.setCType(getcType());
						item.setCInventory(getcInventory());
						item.setCFundsource(getcFundsource());
						item.setRemark(getRemark());
						item.save();
						DSApi.context().commit();
					} catch (Exception e) {
						DSApi.context().rollback();
						throw new EdmException(e.getMessage());
					} finally {
						// DSApi.close();
					}
				} else {
					addActionError("Nie mo�na dokona� zapisu. Nie okre�lono �adnego spo�r�d p�l mapuj�cych.");
				}
			} else {
				addActionError("Nie mo�na dokona� zapisu. Prosz� okre�li� u�ytkownika.");
			}
		}

	}

	private class Delete implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			if (getId() != null) {
				try {
					UlObszaryMapping item = UlObszaryMapping.find(getId());
					try {
						// DSApi.openAdmin();
						DSApi.context().begin();
						item.delete();
						setId(null);
						DSApi.context().commit();
					} catch (Exception e) {
						DSApi.context().rollback();
						throw new EdmException(e.getMessage());
					} finally {
						// DSApi.close();
					}
				} catch (EdmException e) {
					LOG.error(e.getMessage(), e);
				}
			}
			
		}
		
	}
	
	private class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				try {
					setOffset(getOffset() != null ? getOffset() : 0);
					setInvoiceFields(UlObszaryMapping.list(sortField, ascending, getOffset(), LIMIT));
					Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset)
						{
							return HttpUtils.makeUrl("/admin/invoice-fields.action", new Object[] { "id", getId(), "sortField", sortField == null ? "id" : sortField, "ascending", String.valueOf(ascending), "offset", String.valueOf(offset) });
						}
					};

					pager = new Pager(linkVisitor, getOffset(), LIMIT, getInvoiceFields().totalCount(), LIMIT);
					
					setUsers(DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME));
					setiKinds(getEnumItems(CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME));
					setcMpks(getEnumItems(CostInvoiceLogic.ORGANIZATIONAL_UNIT_TABLE_NAME));
					setcProjects(getEnumItems(CostInvoiceLogic.PROJECTS_TABLE_NAME));
					setcTypes(getEnumItems(CostInvoiceLogic.PRODUCT_TABLE_NAME));
					setcInventories(getEnumItems(CostInvoiceLogic.INVENTORY_REPO_TABLE_NAME));
					setcFundsources(getEnumItems(CostInvoiceLogic.FUND_SOURCES_TABLE_NAME));

					if (getId() != null) {
						UlObszaryMapping item = UlObszaryMapping.find(getId());
						setcMpk(item.getCMpk());
						setcProject(item.getCProject());
						setDsuser(item.getDsuserLong());
						setiKind(item.getIKind());
						setcType(item.getCType());
						setcInventory(item.getCInventory());
						setcFundsource(item.getCFundsource());
						setRemark(item.getRemark());
					}

				} finally {
				}
			} catch (EdmException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		
	}

    private List<EnumItem> getEnumItems(String table) {
        try {
            return DataBaseEnumField.tableToField.get(table).iterator().next().getSortedEnumItems(true);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return Lists.newArrayList();
    }

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Boolean getAscending() {
		return ascending;
	}

	public void setAscending(Boolean ascending) {
		this.ascending = ascending;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

}
