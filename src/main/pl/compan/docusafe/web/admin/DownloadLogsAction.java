package pl.compan.docusafe.web.admin;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.PropertyConfigurator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.FormFile;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DownloadLogsAction.java,v 1.10 2009/07/03 10:58:16 mariuszk Exp $
 */
public class DownloadLogsAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(DownloadLogsAction.class);

    String fromDate;
    String toDate;
    private boolean archive;

	//wczytywanie log4j
	private FormFile file;
	private String properties;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat zipDateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm");

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		 registerListener("doUpload").
            append(OpenHibernateSession.INSTANCE).
            append(new Upload()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDownload").
            append(OpenHibernateSession.INSTANCE).
            append(new Download()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

	public FormFile getFile()
	{
		return file;
	}

	public void setFile(FormFile file)
	{
		this.file = file;
	}

	/**
	 * @return the properties
	 */
	public String getProperties()
	{
		return properties;
	}

	/**
	 * @param properties
	 *            the properties to set
	 */
	public void setProperties(String properties)
	{
		this.properties = properties;
	}

	private class Upload implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(file == null)
			{
				addActionError("Nie wybrano pliku");
				return;
			}
			InputStream stream = null;
			Properties log4jProperties = new Properties();
			try
			{
				log.info("uploading log4j properties file: filename={}", file.getName());		
				stream = new FileInputStream(file.getFile());
				log4jProperties.load(stream);
				PropertyConfigurator.configure(log4jProperties);
				StringBuilder b = new StringBuilder();
				for (Map.Entry e : log4jProperties.entrySet())
				{
					b.append(e.getKey() + "=" + e.getValue() + "<br/>");
				}
				properties = b.toString();
			}
			catch (Exception e)
			{
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
			finally
			{
				if (stream != null)
				{
					try
					{
						stream.close();
					}
					catch (Exception e)
					{
						log.error(e.getMessage(), e);
						addActionError(e.getMessage());
					}
				}
			}
		}
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			File logDirectory = new File(Docusafe.getHome(), "logs");
			File[] logs = logDirectory.listFiles(new FileFilter()
			{
				public boolean accept(File pathname)
				{
					return pathname.isFile() && pathname.getName().matches("debug.log.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]");
				}
			});

			Date start = null;
			Date end = null;

			for (int i = 0; i < logs.length; i++)
			{
				try
				{
					Date d = dateFormat.parse(logs[i].getName().substring("debug.log-".length()));
					if (start == null || start.after(d))
						start = d;
					if (end == null || end.before(d))
						end = d;
				}
				catch (ParseException e)
				{
				}
			}

			if (start != null)
				fromDate = DateUtils.formatJsDate(start);
			if (end != null)
				toDate = DateUtils.formatJsDate(end);
		}
	}

	private class Download implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			File logDirectory = new File(Docusafe.getHome(), "logs");
			File zip;

			try
			{
				zip = File.createTempFile("docusafe_logs_", ".zip");
				ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(zip));

				zipOutput.setComment("Logi systemowe Docusafe, " + DateUtils.formatCommonDateTime(new Date()) + "\n" + Docusafe.getLicenseString() + "\n" + Docusafe.getVersion() + "."
						+ Docusafe.getDistNumber());

				File debug = new File(logDirectory, "debug.log");
				if (debug.isFile())
					addFile(zipOutput, debug, "debug.txt");

				if (archive)
				{
					Date start = DateUtils.nullSafeParseJsDate(fromDate);
					Date end = DateUtils.nullSafeParseJsDate(toDate);

					if (start != null && end != null && (start.before(end) || start.equals(end)))
					{
						Calendar current = Calendar.getInstance();
						current.setTime(start);

						while (!current.getTime().after(end))
						{
							debug = new File(logDirectory, "debug.log." + dateFormat.format(current.getTime()));
							if (debug.isFile())
								addFile(zipOutput, debug, "debug." + dateFormat.format(current.getTime()) + ".txt");

							current.add(Calendar.DATE, 1);
						}
					}
				}

				zipOutput.close();
			}
			catch (IOException e)
			{
				addActionError(e.getMessage());
				if (event.getLog().isDebugEnabled())
					event.getLog().debug(e.getMessage(), e);
				return;
			}

			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + "docusafe-logs-" + zipDateFormat.format(new Date()) + ".zip\"");
			response.setContentLength((int) zip.length());

			try
			{
				OutputStream output = response.getOutputStream();
				BufferedInputStream in = new BufferedInputStream(new FileInputStream(zip));
				byte[] buf = new byte[2048];
				int count;
				while ((count = in.read(buf)) > 0)
				{
					output.write(buf, 0, count);
					output.flush();
				}
				in.close();
				output.close();
			}
			catch (IOException e)
			{
				event.getLog().error(e.getMessage(), e);
			}
			finally
			{
				zip.delete();
			}
		}

		byte[] buffer = new byte[2048];

		private void addFile(ZipOutputStream out, File file, String name) throws IOException
		{
			ZipEntry entry = new ZipEntry(name);
			entry.setComment(file.getName());
			out.putNextEntry(entry);
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
			int count;
			while ((count = in.read(buffer)) > 0)
			{
				out.write(buffer, 0, count);
			}
			in.close();
			out.closeEntry();
		}
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public boolean isArchive()
	{
		return archive;
	}

	public void setArchive(boolean archive)
	{
		this.archive = archive;
	}
}
