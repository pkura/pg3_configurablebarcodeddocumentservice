package pl.compan.docusafe.web.admin;

import com.google.common.collect.Maps;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang.StringUtils;
import org.json.XML;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jackrabbit.JackrabbitDocumentVersion;
import pl.compan.docusafe.core.jackrabbit.JackrabbitMetadata;
import pl.compan.docusafe.core.jackrabbit.JackrabbitServer;
import pl.compan.docusafe.core.jackrabbit.JackrabbitUsers;
import pl.compan.docusafe.core.jackrabbit.entities.DocumentVersionEntity;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;
import java.util.Map;

/**
 * Klasa odpowiedzialna za strony modyfikuj�ce i dodaj�ce wpisy JackrabbitServer
 *
 * @author msankowski
 */
public class JackrabbitReposAction extends RichEventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(JackrabbitReposAction.class);

    private Integer id;

    private String status;
    private String internalUrlPrefix;
    private String userUrlPrefix;
    private String login;
    private String password;
    private boolean ignoreSslErrors;

    private List<JackrabbitServer> servers;
    private Map<String, String> statuses;

    protected void setup() {
        registerListener(DEFAULT_ACTION)
                .append(OpenHibernateSession.INSTANCE)
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doTest")
                .append(OpenHibernateSession.INSTANCE)
                .append(new Test())
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd")
                .append(OpenHibernateSession.INSTANCE)
                .append(new Add())
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doEdit")
                .append(OpenHibernateSession.INSTANCE)
                .append(new Edit())
                .append(new FillForm())
                .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("addUserToJackrabbit")
                .append(OpenHibernateSession.INSTANCE)
                .append(new AddUserToJackrabbit())
                .appendFinally(CloseHibernateSession.INSTANCE);

        hibernateListener(new SendMetadata());
        hibernateListener(new SendMetadataAsync());
        hibernateListener(new GetXmlHash());
        hibernateListener(new GetXmlVersion());

    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(id != null){
                if(id==0) {
                    id = null;
                    return;
                }

                JackrabbitServer server = (JackrabbitServer) DSApi.context().session().get(JackrabbitServer.class, id);
                status = server.getStatus().name();
                internalUrlPrefix = server.getInternalUrlPrefix();
                userUrlPrefix = server.getUserUrlPrefix();
                login = server.getLogin();
                password = server.getPassword();
                ignoreSslErrors = server.isIgnoreSslErrors();
                statuses = Maps.newHashMapWithExpectedSize(JackrabbitServer.RepositoryStatus.values().length);
                for(JackrabbitServer.RepositoryStatus status : JackrabbitServer.RepositoryStatus.values()){
                    statuses.put(status.name(), status.getDescription());
                }
            } else {
                servers = DSApi.context().session().createCriteria(JackrabbitServer.class).list();
            }
        }

        @Override
        public Logger getLogger() { return LOG; }
    }

    private class Test extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(StringUtils.isBlank(internalUrlPrefix)) {
                event.addActionError("Wewn�trzny URL nie mo�e by� pusty");
            }
            if(hasActionErrors()){
                throw new EdmException("Nie mo�na doda� testowa� po��czenia bez uzupe�nienia wszystkich p�l");
            }
            getServer().testConnection();
            event.addActionMessage("Udany test po��czenia");
        }

        @Override
        public Logger getLogger() { return LOG; }
    }

    private JackrabbitServer getServer() throws EdmException {
        JackrabbitServer server;
        if(id != null && id > 0){
            server = (JackrabbitServer) DSApi.context().session().get(JackrabbitServer.class, id);
        } else {
            server = new JackrabbitServer();
        }
        server.setInternalUrlPrefix(internalUrlPrefix);
        server.setUserUrlPrefix(userUrlPrefix);
        server.setLogin(login);
        server.setPassword(password);
        if(status != null){
            server.setStatus(JackrabbitServer.RepositoryStatus.valueOf(status));
        }
        return server;
    }

    private class Add extends TransactionalActionListener {

        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(StringUtils.isBlank(internalUrlPrefix)) {
                event.addActionError("Wewn�trzny URL nie mo�e by� pusty");
            }
            if(StringUtils.isBlank(userUrlPrefix)) {
                event.addActionError("URL dla u�ytkownika nie mo�e by� pusty");
            }
            if(hasActionErrors()){
                throw new EdmException("Nie mo�na doda� nowego po��czenia bez uzupe�nienia wszystkich p�l");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            JackrabbitServer server = getServer();
            server.setIgnoreSslErrors(false);
            server.setStatus(JackrabbitServer.RepositoryStatus.DISABLED);
            DSApi.context().session().save(server);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            id = null;
        }

        @Override
        public Logger getLogger() { return LOG; }
    }

    private class Edit extends TransactionalActionListener {

        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(StringUtils.isBlank(internalUrlPrefix)) {
                event.addActionError("Wewn�trzny URL nie mo�e by� pusty");
            }
            if(StringUtils.isBlank(userUrlPrefix)) {
                event.addActionError("URL dla u�ytkownika nie mo�e by� pusty");
            }
            if(hasActionErrors()){
                throw new EdmException("Nie mo�na doda� modyfikowa� po��czenia bez uzupe�nienia wszystkich p�l");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            DSApi.context().session().update(getServer());
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            id = null;
        }

        @Override
        public Logger getLogger() { return LOG; }
    }

    private class AddUserToJackrabbit extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            String username = getParameter("username");
            String password = getParameter("password");

            JackrabbitUsers.instance().createUser(username, password);

            return new JsonPrimitive("OK");
        }
    }

    private class SendMetadata extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long documentId = getLongParameterOrError("documentId");

            JackrabbitMetadata metadata = new JackrabbitMetadata(documentId);

            String sha1 = metadata.getSha1();

            metadata.send();

            return new JsonPrimitive(sha1);
        }
    }

    private class GetXmlHash extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long documentId = getLongParameterOrError("documentId");

            JackrabbitMetadata metadata = new JackrabbitMetadata(documentId);

            String sha1 = metadata.getSha1();

            return new JsonPrimitive(sha1);
        }
    }

    private class SendMetadataAsync extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long documentId = getLongParameterOrError("documentId");

            Long ret = JackrabbitXmlSynchronizer.createEvent(documentId);

            return new JsonPrimitive(ret);
        }
    }

    /**
     * Zwraca zawarto�� danej wersji.
     */
    private class GetXmlVersion extends AjaxActionListener {

        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception  {
            Long documentId = getLongParameterOrError("documentId");
            String uuid = getParameterOrError("uuid");
            DocumentVersionEntity response = JackrabbitDocumentVersion.instance().getVersion(documentId, uuid);
            String xml = new String(Base64.decode(response.getContent().getBytes()),"UTF-8");
            return new JsonParser().parse(XML.toJSONObject(xml).toString());
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInternalUrlPrefix() {
        return internalUrlPrefix;
    }

    public void setInternalUrlPrefix(String internalUrlPrefix) {
        this.internalUrlPrefix = internalUrlPrefix;
    }

    public String getUserUrlPrefix() {
        return userUrlPrefix;
    }

    public void setUserUrlPrefix(String userUrlPrefix) {
        this.userUrlPrefix = userUrlPrefix;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIgnoreSslErrors() {
        return ignoreSslErrors;
    }

    public void setIgnoreSslErrors(boolean ignoreSslErrors) {
        this.ignoreSslErrors = ignoreSslErrors;
    }

    public List<JackrabbitServer> getServers() {
        return servers;
    }

    public void setServers(List<JackrabbitServer> servers) {
        this.servers = servers;
    }

    public Map<String, String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Map<String, String> statuses) {
        this.statuses = statuses;
    }


}
