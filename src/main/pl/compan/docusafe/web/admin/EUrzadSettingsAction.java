package pl.compan.docusafe.web.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.prefs.Preferences;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.protocol.Protocol;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.eurzad.EUrzadService;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.ws.eurzad.EUrzad_wymiana_danychStub;
import pl.compan.docusafe.ws.eurzad.SocketFactory;

public class EUrzadSettingsAction  extends EventActionSupport
{

	private static final long serialVersionUID = 1L;
	private String urzadId;
	private String login;
	private String password;
	private String address;
	private String koordynator;
	StringManager sm = StringManager.getManager(EUrzadSettingsAction.class.getPackage().getName());
	static final Logger log = LoggerFactory.getLogger(EUrzadSettingsAction.class);


	protected void setup() 
	{
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doTest").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        append(new Test()).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
	}
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (koordynator == null || urzadId == null || login == null || password == null || address == null)
            		throw new EdmException(sm.getString("WszystkiePolaMuszaBycUzupelnione"));
	        	DSApi.context().begin();
	        	Preferences node = DSApi.context().systemPreferences().node(EUrzadService.EURZAD_PREFS_NODE);
	        	urzadId = node.get(EUrzadService.URZADID_KEY, "");
	        	login = node.get(EUrzadService.LOGIN_KEY, "");
	        	password = node.get(EUrzadService.PASSWORD_KEY, "");
	        	address = node.get(EUrzadService.ADDRESS_KEY, "");
	        	koordynator = node.get(EUrzadService.KOORDYNATOR_KEY,"");
	        	DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        	}
        }
    }
	
	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (koordynator == null || urzadId == null || login == null || password == null || address == null)
            		throw new EdmException(sm.getString("WszystkiePolaMuszaBycUzupelnione"));
	        	DSApi.context().begin();
	        	Preferences node = DSApi.context().systemPreferences().node(EUrzadService.EURZAD_PREFS_NODE);
	        	node.put(EUrzadService.URZADID_KEY, urzadId);
	        	node.put(EUrzadService.LOGIN_KEY, login);
	        	node.put(EUrzadService.PASSWORD_KEY, password);
	        	node.put(EUrzadService.ADDRESS_KEY, address);
	        	node.put(EUrzadService.KOORDYNATOR_KEY, koordynator);
	        	addActionMessage(sm.getString("PomyslnieZapisanoKonfiguracje"));
	        	DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        		log.debug(""+e);
        	}
        }
    }
	
	private class Test implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	        	EUrzad_wymiana_danychStub.serviceAddress = address;
				Protocol myProtocolHandler = new Protocol("https", new SocketFactory(), 443);
				EUrzad_wymiana_danychStub eurzad = new EUrzad_wymiana_danychStub();
				eurzad._getServiceClient().getOptions().setProperty(HTTPConstants.CUSTOM_PROTOCOL_HANDLER, myProtocolHandler);
				EUrzad_wymiana_danychStub.ExportData ed = new EUrzad_wymiana_danychStub.ExportData();
				
				ed.setIdurzedu(urzadId);
				ed.setLogin(login);
				ed.setPass(password);
				
				EUrzad_wymiana_danychStub.ExportDataResponse edr = eurzad.ExportData(ed);
				File temp = File.createTempFile("eurzad", ".xml");
				OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(temp),"UTF-8");
				String xml = edr.get_return();
				//System.out.println(xml);
				fw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				fw.write(xml);
				fw.flush();
				fw.close();
				
				//temp = new File(Docusafe.getHome(),"export_data_compan.xml");
				//System.out.println("robie plik " + temp.getAbsolutePath());
				SAXReader saxReader = new SAXReader();
				Document document = saxReader.read(temp);
				Element elem = document.getRootElement();
				addActionMessage(sm.getString("PomyslniePolaczonoSieZUsluga"));
        	}
        	catch(Exception e)
        	{
        		addActionError(sm.getString("NieUdaloSiePolaczycZUsluga"));
        		e.printStackTrace();
        		log.debug(""+e);
        	}
        }
    }

	public String getUrzadId() {
		return urzadId;
	}

	public void setUrzadId(String urzadId) {
		this.urzadId = urzadId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getKoordynator() {
		return koordynator;
	}

	public void setKoordynator(String koordynator) {
		this.koordynator = koordynator;
	}
	

}
