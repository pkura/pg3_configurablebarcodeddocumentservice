package pl.compan.docusafe.web.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.zebra.ZebraPrinter;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.standard.PrinterName;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class NewBarcodePrinterAction extends EventActionSupport
{
	private static final StringManager sm = StringManager.getManager(NewBarcodePrinterAction.class.getPackage().getName());
	private static final long serialVersionUID = 1L;
	private Logger log = LoggerFactory.getLogger(NewBarcodePrinterAction.class);
	private Boolean usePrinter;
	private String userPrinterName;
	private String printerName;
	private String printerLanguage;
	private String firstLine;
	private String secondLine;
	private String barcodeType;
	private String[] fields = new String[0];
	private String barcodeValue;
	private ZebraPrinter zb;
	
	private Map<String, String> availableBarcodes;
	private Map<String, String> availablePrinters;
	private Map<String, String> availableLanguages;

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doTest").
            append(OpenHibernateSession.INSTANCE).
            append(new Test()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			availableBarcodes = prepareBarcodesMap();
			availablePrinters = preparePrintersMap();
			availableLanguages = preparePrinterLanguagesMap();
        }
    }

	private class Test implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {


        	try
        	{
				zb = new ZebraPrinter();
				zb.setLanguage(printerLanguage);
				zb.setUserPrinterName(userPrinterName);
				zb.setPrinterName(printerName);
				zb.setBarcodeType(barcodeType);
				zb.setBarcodeValue(barcodeValue);
				zb.setFirstLine(firstLine);
				zb.setSecondLine(secondLine);
				zb.setDefinedFields(Arrays.asList(fields));
				zb.setUsePrinter(usePrinter);
        		
        		zb.init();
        		Map<String,String> fields = new LinkedHashMap<String,String>();
        		fields.put("numer pisma","13095");
        		fields.put("data przyj�cia","24-08-2009 11:26");
        		fields.put("przyjmuj�cy","Bogus�aw Misztal");
        		fields.put("login","8848077e1234bba7a8e");
        		fields.put("has�o","25ea5847");
        		zb.setFields(fields);
        		zb.setBarcodeType(barcodeType);
        		zb.setBarcodeValue(barcodeValue);
        		zb.setFirstLine(firstLine);
        		zb.setSecondLine(secondLine);
            	zb.print();



            }
        	catch (EdmException e) 
        	{
				addActionError(e.getMessage());
				log.error(e.getMessage(),e);
			}
        }
    }
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
			if (userPrinterName == null)
			{
				addActionError(sm.getString("NazwaDrukarkiJestWymagana"));
				return;
			}

			ZebraPrinter printerByUserPrinterName = ZebraPrinter.findByUserPrinterName(userPrinterName);

			if (printerByUserPrinterName != null)
			{
				addActionError(sm.getString("DrukarkaOTakiejNazwieZostalaZdefiniowana"));
				return;
			}

			ZebraPrinter zb;

			if (printerByUserPrinterName != null)
				zb = printerByUserPrinterName;
			else
				zb = new ZebraPrinter();

			zb.setLanguage(printerLanguage);
			zb.setUserPrinterName(userPrinterName);
			zb.setPrinterName(printerName);
			zb.setBarcodeType(barcodeType);
			zb.setBarcodeValue(barcodeValue);
			zb.setFirstLine(firstLine);
			zb.setSecondLine(secondLine);
			zb.setDefinedFields(Arrays.asList(fields));
			zb.setUsePrinter(usePrinter);

			zb.savePrinterToDatabase();
        	addActionMessage(sm.getString("DrukarkaZostalaZapisana"));
        }
    }

	public static Map<String, String> prepareBarcodesMap()
	{
		Map<String, String> availableBarcodes = new LinkedHashMap<String, String>();
		availableBarcodes.put("B1", "Code 11");
		availableBarcodes.put("B3", "Code 39");
		availableBarcodes.put("BA", "Code 93");
		availableBarcodes.put("BC", "Code 128");
		availableBarcodes.put("B2", "Interleaved 2 of 5");
		availableBarcodes.put("B8", "EAN-8");
        availableBarcodes.put("QR", "QR");
		return availableBarcodes;
	}

	/**
	 * Pobiera wszystkie drukarki bez tych ju� zdefiniowanych w systemie
	 * @return
	 */
	public static Map<String, String> preparePrintersMap()
	{
		Map<String, String> availablePrinters = new TreeMap<String, String>();
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
		for(PrintService pi:services)
		{
			PrintServiceAttribute attr = pi.getAttribute(PrinterName.class);
			String sPrinterName = ((PrinterName) attr).getValue();
			availablePrinters.put(sPrinterName, sPrinterName);
		}
		return availablePrinters;
	}

	public static Map<String, String> preparePrinterLanguagesMap()
	{
		Map<String, String> availableLanguages = new LinkedHashMap<String, String>();
		availableLanguages.put("", "Wybierz j�zyk drukarki");
		availableLanguages.put("ZPL", "J�zyk ZPL");
		availableLanguages.put("EPL", "J�zyk EPL");
		return availableLanguages;
	}

	public Boolean getUsePrinter()
	{
		return usePrinter;
	}

	public void setUsePrinter(Boolean usePrinter)
	{
		this.usePrinter = usePrinter;
	}

	public String getPrinterName()
	{
		return printerName;
	}

	public void setPrinterName(String printerName)
	{
		this.printerName = printerName;
	}

	public String getFirstLine()
	{
		return firstLine;
	}

	public void setFirstLine(String firstLine)
	{
		this.firstLine = firstLine;
	}

	public String getSecondLine()
	{
		return secondLine;
	}

	public void setSecondLine(String secondLine)
	{
		this.secondLine = secondLine;
	}

	public String getBarcodeType()
	{
		return barcodeType;
	}

	public void setBarcodeType(String barcodeType)
	{
		this.barcodeType = barcodeType;
	}

	public String[] getFields()
	{
		return fields;
	}

	public void setFields(String[] fields)
	{
		this.fields = fields;
	}

	public String getBarcodeValue()
	{
		return barcodeValue;
	}

	public void setBarcodeValue(String barcodeValue)
	{
		this.barcodeValue = barcodeValue;
	}

	public Map<String, String> getAvailableBarcodes()
	{
		return availableBarcodes;
	}

	public void setAvailableBarcodes(Map<String, String> availableBarcodes)
	{
		this.availableBarcodes = availableBarcodes;
	}


	public Map<String, String> getAvailablePrinters()
	{
		return availablePrinters;
	}

	public void setAvailablePrinters(Map<String, String> availablePrinters)
	{
		this.availablePrinters = availablePrinters;
	}

	public ZebraPrinter getZb()
	{
		return zb;
	}

	public void setZb(ZebraPrinter zb)
	{
		this.zb = zb;
	}

	public String getPrinterLanguage() 
	{
		return printerLanguage;
	}

	public void setPrinterLanguage(String printerLanguage) 
	{
		this.printerLanguage = printerLanguage;
	}

	public Map<String, String> getAvailableLanguages() 
	{
		return availableLanguages;
	}

	public void setAvailableLanguages(Map<String, String> availableLanguages) 
	{
		this.availableLanguages = availableLanguages;
	}

	public String getUserPrinterName()
	{
		return userPrinterName;
	}

	public void setUserPrinterName(String userPrinterName)
	{
		this.userPrinterName = userPrinterName;
	}
}
