package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.BoxChangeHistory;
import pl.compan.docusafe.core.base.BoxLine;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

/* User: Administrator, Date: 2005-08-01 16:10:44 */

/**
 * TODO: pozby� si� isBoxOpen(), obecno�� boxId jest wystarczaj�ca
 * 
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: OpenBoxAction.java,v 1.24 2010/07/28 07:20:02 pecet1 Exp $
 */
public class OpenBoxAction extends EventActionSupport
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(OpenBoxAction.class.getPackage().getName(),null);
    //private String currentBoxNumber;
    //private boolean boxOpen;
    private String boxNumber;
    private String line;
    private List<Box> boxList;
    private Long existingBoxId;
    private String newBoxNumber;
    private boolean canManage;
    private String boxName;
    private String startId;
    private String endId;
    
    private String boxName1;
    private String startId1;
    private String endId1;
    private Integer item1;
    private List<BoxLineBean> boxLines;
    
    
    private String withoutId ;
    private String withoutId1 ;
    private final String nrSprawyCn = "NUMER_SPRAWY";
    private static final String DEFAULT_LINE = "default";
    //czy wy�wietli� pytanie
    private boolean ask;
    private long wrongId;

    private String startWithdrawId;
    private String endWithdrawId;
    private String boxNameWithdraw;
    private String startWithdrawId1;
    private String endWithdrawId1;
    private String boxNameWithdraw1;
    private List<pl.compan.docusafe.core.dockinds.field.EnumItem> items;
    private Integer item;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doOpenBox").
            append(OpenHibernateSession.INSTANCE).
            append(new OpenBox()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRename").
            append(OpenHibernateSession.INSTANCE).
            append(new Rename()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCloseBox").
            append(OpenHibernateSession.INSTANCE).
            append(new CloseBox()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doReopenBox").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ReopenBox()).
        	append(new FillForm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doBox").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DoBox()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doBoxPTE").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DoBoxPTE()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doCheckBoxPTE").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DoCheckBoxPTE()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doWithdraw").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DoWithdraw()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {                
                canManage = DSApi.context().hasPermission(DSPermission.PUDLO_ARCH_WSZYSTKO);
                
                boxLines = new ArrayList<BoxLineBean>();
                DocumentKind dockind = DocumentKind.findByCn(DocumentLogicLoader.DC_KIND);
                if(dockind!=null)
                	items = dockind.getFieldByCn("KATEGORIA").getEnumItems();
                
                
                
                BoxLineBean bean = new BoxLineBean();
                bean.setName(sm.getString("Domyslna"));
                bean.setLine(DEFAULT_LINE);
                
                // pobranie danych dla domy�lnej linii
                Long id = GlobalPreferences.getBoxId();

                if (id != null)
                {                                        
                    Box box = null;
                    try
                    {
                        box = Box.find(id);

                        bean.setCurrentBoxNumber(box.getName());
                        bean.setBoxOpen(GlobalPreferences.isBoxOpen());

                        // podpowiadanie kolejnego numeru pud�a
                        suggestBoxNumber(bean);
                    }
                    catch (EntityNotFoundException e)
                    {
                        GlobalPreferences.setBoxId(null);
                    }                   
                }
                boxLines.add(bean);
                
                // pobranie danych dla pozosta�ych linii
                List<BoxLine> list = BoxLine.list();
                for (BoxLine boxLine : list)
                {
                    bean = new BoxLineBean();
                    bean.setLine(boxLine.getLine());
                    bean.setName(boxLine.getName());
                    
                    Long boxId = GlobalPreferences.getBoxId(boxLine.getLine());
                    if (boxId != null)
                    {
                        Box box = null;
                        try
                        {
                            box = Box.find(boxId);
                            DSApi.context().session().refresh(box);
                            
                            bean.setCurrentBoxNumber(box.getName());
                            bean.setBoxOpen(box.isOpen());
                             
                            // podpowiadanie kolejnego numeru pud�a
                            suggestBoxNumber(bean);
                        }
                        catch (EntityNotFoundException e)
                        {
                            GlobalPreferences.setBoxId(boxLine.getLine(), null);
                        }
                    }
                    boxLines.add(bean);
                }
                
                boxList = Box.list();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    private class DoBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            startId = TextUtils.trimmedStringOrNull(startId);
            endId = TextUtils.trimmedStringOrNull(endId);

            if (startId == null)
                addActionError("Nie podano pocz�tkowego numeru dokumentu");
            if (endId == null)
                addActionError("Nie podano ko�cowego numeru dokumentu");
            
            if (hasActionErrors())
                return;

            startId = startId.toUpperCase();
            endId = endId.toUpperCase();

            try
            {
                DSApi.context().begin();

                try
                {
                    Box.findByName(null,boxName);
                    throw new EdmException("Istnieje ju� pud�o o numerze "+boxName);
                }
                catch (EntityNotFoundException e)
                {
                }

                // walidacja - czy wszystkie dokumenty maj� ten sam typ
                Integer fidRODZAJ = Doctype.findByCn("nationwide").getFieldByCn("RODZAJ").getId();
                Integer idRODZAJ = null;

                Box box = new Box(boxName);
                box.create();
                box.setOpenTime(new Date());
                box.setCloseTime(new Date());

                // umieszczanie dokument�w w pudle
                int count = 0;
                DSApi.context().commit();

                addActionMessage("W pudle "+box.getName()+" umieszczono "+count+" dokument�w");
            }
            catch (HibernateException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError("Wyst�pi� b��d odczytu dokumentu");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        } 
    }
    
    
    
    private class DoWithdraw implements ActionListener
    {
    	private String getFormattedLongWithZeros(Long l)
	    {
    		DecimalFormat formatter = new DecimalFormat();
            formatter.applyPattern("000000000");
	        return formatter.format(l);
	    }
        public void actionPerformed(ActionEvent event)
        {
        	startWithdrawId = TextUtils.trimmedStringOrNull(startWithdrawId);
            endWithdrawId = TextUtils.trimmedStringOrNull(endWithdrawId);
        	if (startWithdrawId == null)
                addActionError("Nie podano pocz�tkowego numeru dokumentu");
            if (endWithdrawId == null)
                addActionError("Nie podano ko�cowego numeru dokumentu");
    
            if (hasActionErrors())
                return;
            
            startWithdrawId = startWithdrawId.toUpperCase();
            endWithdrawId = endWithdrawId.toUpperCase();
            startWithdrawId1=startWithdrawId;
            endWithdrawId1 = endWithdrawId;
            boxNameWithdraw1 = boxNameWithdraw;
            long start,end;
            try
            {
            	start=Long.parseLong(startWithdrawId);
            	end = Long.parseLong(endWithdrawId);
            	DockindQuery dockindQuery = new DockindQuery(0,0);
				DocumentKind kind = DocumentKind.findByCn("dc");
				kind.initialize();
				
				dockindQuery.setDocumentKind(kind); 
				Field nrSprawyField = kind.getFieldByCn(nrSprawyCn);
				//dockindQuery.stringFieldRange(nrSprawyField, getFormattedLongWithZeros(start), getFormattedLongWithZeros(end));
				dockindQuery.rangeField(nrSprawyField, start, end);
				if(item != null)
					dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.DC_KIND).getFieldByCn("KATEGORIA"), item);
				SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
				Document[] docs = searchResults.results();
				Box box = null;
				try
                {
					box = Box.findByName(null,boxNameWithdraw);
                }
                catch (EntityNotFoundException e)
                {
                }
                if(box==null){
					addActionError("Pud�o o podanej nazwie nie istnieje");
				}
                if(docs==null || docs.length==0){
                	addActionError("Do podanych kryteri�w nie pasuje �aden dokument");
                }
				if(hasActionErrors()){
					return;
				}
				
				for (Document doc : docs) {
				
					FieldsManager fm = kind.getFieldsManager(doc.getId());
					if(doc.getBox()!=null && (!doc.getBox().getId().equals(box.getId()))){
						
						addActionError("Jedno z wybranych pism: "+doc.getId()+" znajduje si� w innym pudle");
						return;
					}

				}
				
				DSApi.context().begin();
				for (Document doc : docs) {
					
					FieldsManager fm = kind.getFieldsManager(doc.getId());

					if(doc.getBox()!=null && box.getId().equals(doc.getBox().getId())){
						doc.removeFromBox();
						
						BoxChangeHistory history = new BoxChangeHistory();
						history.setBoxFromName(box.getName());
						doc.setBox(null);
						history.setCurDate(new Date());
						history.setBoxToName(box.getName());
						history.setIdDoc(doc.getId());
						
						history.create();
					}
					
				}
				DSApi.context().commit();
				addActionMessage("Dokumenty wycofano z pud�a");
            }catch(NumberFormatException e){
            	
            	addActionError("Nieprawid�owy format liczb");
            	LogFactory.getLog("eprint").debug("", e);
            	return;
            }
            catch (EdmException e) {
				// TODO Auto-generated catch block
            	addActionError("Pojawi� si� problem: EdmException");
            	try {
            		if(DSApi.isContextOpen())
            		DSApi.context().rollback();
				} catch (EdmException e1) {
					// TODO Auto-generated catch block
					LogFactory.getLog("eprint").debug("", e1);
				}
				LogFactory.getLog("eprint").debug("", e);
				return;
			}
        	
        }
    }
    
    
    
    
    
    
    
    
    private class DoCheckBoxPTE implements ActionListener
    {
    	private String getFormattedLongWithZeros(Long l)
	    {
    		DecimalFormat formatter = new DecimalFormat();
            formatter.applyPattern("000000000");
	        return formatter.format(l);
	    }
        public void actionPerformed(ActionEvent event)
        {
        	startId = TextUtils.trimmedStringOrNull(startId);
            endId = TextUtils.trimmedStringOrNull(endId);
        	if (startId == null)
                addActionError("Nie podano pocz�tkowego numeru dokumentu");
            if (endId == null)
                addActionError("Nie podano ko�cowego numeru dokumentu");
            if(boxName == null)
            	addActionError("Nie podano numeru pudla");
            
            if (hasActionErrors())
                return;
            
            startId = startId.toUpperCase();
            endId = endId.toUpperCase();
            startId1=startId;
            endId1 = endId;
            item1 = item;
            withoutId1 = withoutId;
            
            	
            boxName1 = boxName;
            long start,end;
            String[] withoutList;
            try
            {
            	start=Long.parseLong(startId);
            	end = Long.parseLong(endId);
            	if(withoutId!=null)
            		withoutList = withoutId.split("\n");
            	else
            		withoutList=null;
            	DockindQuery dockindQuery = new DockindQuery(0,0);
				DocumentKind kind = DocumentKind.findByCn("dc");
				kind.initialize();
				
				dockindQuery.setDocumentKind(kind); 
				Field nrSprawyField = kind.getFieldByCn(nrSprawyCn);
				dockindQuery.rangeField(nrSprawyField, start, end)	;
				//dockindQuery.stringFieldRange(nrSprawyField, getFormattedLongWithZeros(start), getFormattedLongWithZeros(end));
				if(item != null)
					dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.DC_KIND).getFieldByCn("KATEGORIA"), item);

				SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
				Document[] docs = searchResults.results();
				Box box = null;
				if(boxName!=null){
					try
	                {
						box = Box.findByName(null,boxName);
	                }
	                catch (EntityNotFoundException e)
	                {
	                }
	                
				}
				boolean empty = true;
				for (Document doc : docs) {
	
					boolean omit = false ;
					FieldsManager fm = kind.getFieldsManager(doc.getId());
					if(withoutList!=null){
						for (String without : withoutList) {
							
							long withoutNr = Long.parseLong(without.trim());
							//if(fm.getValue("NUMER_SPRAWY").equals(L(withoutNr))){
							if(fm.getValue("NUMER_SPRAWY").equals(Long.toString(withoutNr))){
								omit = true ;
							}
						}
					}
					if(!omit){
						empty = false;
						
						if(doc.getBox()!=null && (box==null || !doc.getBox().getId().equals(box.getId()))){
							
							if(doc.getBox()!=null && box!=null){
							}
							ask = true; 
							wrongId = doc.getId();
							return;
						}
						if(doc.getBox()!=null && box!=null){
						}
					}else
					{
					}
				}
				
				if(empty){
					addActionError("Do podanych kryteri�w nie pasuje �aden dokument");
					return;
				}
				if(box==null){
					box = new Box(boxName);
					box.setOpen(true);
                    box.setOpenTime(new Date());
                    box.create();
				}
				
				DSApi.context().begin();
				for (Document doc : docs) {
					boolean omit = false ;
					FieldsManager fm = kind.getFieldsManager(doc.getId());
					if(withoutList!=null){
						for (String without : withoutList) {
							long withoutNr = Long.parseLong(without.trim());
							
							//if(fm.getValue("NUMER_SPRAWY").equals(getFormattedLongWithZeros(withoutNr))){
							if(fm.getValue("NUMER_SPRAWY").equals(withoutNr)){
								omit = true ;
							}
						}
					}
					if(!omit){
						if(!box.equals(doc.getBox())){
						doc.setBox(box);BoxChangeHistory history = new BoxChangeHistory();
						history.setBoxFromName(doc.getBox().getName());
						doc.setBox(box);
						history.setCurDate(new Date());
						history.setBoxToName(box.getName());
						history.setIdDoc(doc.getId());
						
						history.create();
						}
					}
				}
				DSApi.context().commit();
				
            }catch(NumberFormatException e){
            	
            	addActionError("Nieprawid�owy format liczb");
            	LogFactory.getLog("eprint").debug("", e);
            	return;
            }
            catch (EdmException e) {
				// TODO Auto-generated catch block
            	addActionError("Pojawi� si� problem: EdmException");
            	try {
            		if(DSApi.isContextOpen())
            		DSApi.context().rollback();
				} catch (EdmException e1) {
					// TODO Auto-generated catch block
					LogFactory.getLog("eprint").debug("", e1);
				}
				LogFactory.getLog("eprint").debug("", e);
				return;
			}
        	
        	addActionMessage("Nadano numery");
        }
    }
    private class DoBoxPTE implements ActionListener
    {
    	private String getFormattedLongWithZeros(Long l)
	    {
    		DecimalFormat formatter = new DecimalFormat();
            formatter.applyPattern("000000000");
	        return formatter.format(l);
	    }
        public void actionPerformed(ActionEvent event)
        {
         	startId = startId1;
            endId =endId1;
            item = item1;
           
           /* if(boxName1 == null)
            {
            	addActionError("Nie podano numeru pud�a");
            	return;
            }*/
            	
            long start,end;
            String[] withoutList;
            try
            {
            	start=Long.parseLong(startId1);
            	end = Long.parseLong(endId1);
            	if(withoutId1!=null)
            		withoutList = withoutId1.split("\n");
            	else
            		withoutList=null;
            	DockindQuery dockindQuery = new DockindQuery(0,0);
				DocumentKind kind = DocumentKind.findByCn("dc");
				kind.initialize();
				
				dockindQuery.setDocumentKind(kind); 
				Field nrSprawyField = kind.getFieldByCn(nrSprawyCn);
				dockindQuery.rangeField(nrSprawyField, start , end);
				//dockindQuery.stringFieldRange(nrSprawyField, getFormattedLongWithZeros(start), getFormattedLongWithZeros(end));
				if(item != null)
					dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.DC_KIND).getFieldByCn("KATEGORIA"), item);

				SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
				Document[] docs = searchResults.results();
				DSApi.context().begin();
				Box box = null;
				Box oldBox ;
				boolean boxExists=false ;
				
				try
                {
					box = Box.findByName(null,boxName1);
				
					boxExists = true;
                }
				catch (EntityNotFoundException e)
                {
                	box = new Box(boxName1);
                    box.setOpen(true);
                    box.setOpenTime(new Date());
                    box.create();
                }
                
				for (Document doc : docs) {
					boolean omit = false ;
					FieldsManager fm = kind.getFieldsManager(doc.getId());
					if(withoutList!=null){
						for (String without : withoutList) {
							long withoutNr = Long.parseLong(without.trim());
							
							//if(fm.getValue("NUMER_SPRAWY").equals(getFormattedLongWithZeros(withoutNr))){
							if(fm.getValue("NUMER_SPRAWY").equals(withoutNr)){
								omit = true ;
							}
						}
					}
					if(!omit){
						if(!box.equals(doc.getBox())){
							
							BoxChangeHistory history = new BoxChangeHistory();
							if(doc.getBox()==null)
								history.setBoxFromName(null);
							else
								history.setBoxFromName(doc.getBox().getName());
							doc.setBox(box);
							history.setCurDate(new Date());
							history.setBoxToName(box.getName());
							history.setIdDoc(doc.getId());
							history.create();
						}
					}
				}
				
				DSApi.context().commit();
				
            }catch(NumberFormatException e){
            	
            	addActionError("Nieprawid�owy format liczb");
            	LogFactory.getLog("eprint").debug("", e);
            	return;
            	
            }catch(DuplicateNameException e){
            	addActionError(e.getMessage());
            	return;
            }
            catch (EdmException e) {
				// TODO Auto-generated catch block
            	addActionError("Pojawi� si� problem: EdmException");
            	try {
            		if(DSApi.isContextOpen())
            		DSApi.context().rollback();
				} catch (EdmException e1) {
					// TODO Auto-generated catch block
					LogFactory.getLog("eprint").debug("", e1);
				}
				LogFactory.getLog("eprint").debug("", e);
				return;
			}
        	
        	addActionMessage("Nadano numery");
        }
    }
    /**
     * podpowiadanie kolejnego numer pud�a dla danej linii
     */
    private void suggestBoxNumber(BoxLineBean bean) throws EdmException
    {
        if (bean.getCurrentBoxNumber() != null && bean.isBoxOpen())
        {
            Pattern pat = Pattern.compile("[^\\d]*(\\d+).*");
            // generowanie nowego numeru pud�a (takiego, kt�ry jeszcze
            // nie istnieje w bazie danych)
            String newNumber = null;
            Matcher mat = pat.matcher(bean.getCurrentBoxNumber());
            if (mat.matches())
            {
                String sNum = mat.group(1);
                // bie��cy numer wyodr�bniony z nazwy pud�a
                long num = Long.parseLong(sNum);

                String prefix = bean.getCurrentBoxNumber().substring(0, mat.start(1));
                String suffix = bean.getCurrentBoxNumber().substring(mat.end(1), bean.getCurrentBoxNumber().length());

                num++;

                while (newNumber == null)
                {
                    String suggestedNumber = prefix + num + suffix;

                    try
                    {
                        Box.findByName(DEFAULT_LINE.equals(bean.getLine()) ? null : bean.getLine(),suggestedNumber);
                        num++;
                    }
                    catch (EntityNotFoundException e)
                    {
                        newNumber = suggestedNumber;
                    }
                }
            }

            // sugerowany kolejny numer pud�a
            bean.setBoxNumber(newNumber);
        }
    }
    
    
    private class OpenBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boxNumber = TextUtils.trimmedStringOrNull(boxNumber);

            if (boxNumber == null)
                addActionError(sm.getString("NiePodanoNowegoNumeruPudla"));

            if (hasActionErrors())
                return;

            boolean defaultLine = DEFAULT_LINE.equals(line);
            try
            {
                DSApi.context().begin();

                // zamkni�cie dotychczasowego pud�a
                Long id = defaultLine ? GlobalPreferences.getBoxId() : GlobalPreferences.getBoxId(line);
                if (id != null)
                {
                    Box box = null;
                    try
                    {
                        box = Box.find(id);
                        box.setCloseTime(new Date());
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                }

                Box box = new Box(boxNumber.toUpperCase());
                box.setOpen(true);
                box.setOpenTime(new Date());   
                if (!defaultLine)                
                    box.setLine(line);
                box.create();

                if (!defaultLine)                                   
                    GlobalPreferences.setBoxId(line, box.getId());                
                else
                {
                    GlobalPreferences.setBoxId(box.getId());
                    GlobalPreferences.setBoxOpen(true);
                }
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                try {DSApi.context().rollback();}catch (Exception f) {};
                addActionError(e.getMessage());
            }
        }
    }

    private class CloseBox implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            boolean defaultLine = DEFAULT_LINE.equals(line);
            try
            {
                DSApi.context().begin();

                // zamkni�cie dotychczasowego pud�a
                Long id = defaultLine ? GlobalPreferences.getBoxId() : GlobalPreferences.getBoxId(line);
                if (defaultLine)
                    GlobalPreferences.setBoxId(null);
                else
                    GlobalPreferences.setBoxId(line, null);
               
                if (id != null)
                {
                    Box box = null;
                    try
                    {
                        box = Box.find(id);
                        box.setCloseTime(new Date());
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                }

                if (defaultLine)
                    GlobalPreferences.setBoxOpen(false);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Rename implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (existingBoxId == null)
                addActionError(sm.getString("NieWybranoZlistyIstniejacegoPudla"));

            newBoxNumber = TextUtils.trimmedStringOrNull(newBoxNumber);

            if (newBoxNumber == null)
                addActionError(sm.getString("NiePodanoNowegoNumeruPudla"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                Box box = Box.find(existingBoxId);
                
                try
                {
                    Box.findByName(box.getLine(),newBoxNumber);
                    throw new EdmException(sm.getString("IstniejeJuzWdanejLiniiPudloOnazwie",newBoxNumber));
                }
                catch (EntityNotFoundException e)
                {
                    
                    box.setName(newBoxNumber.toUpperCase());
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class ReopenBox implements ActionListener {
    	
    	public void actionPerformed(ActionEvent event) {
    		
    		boxNumber = TextUtils.trimmedStringOrNull(boxNumber);
    		
    		if (boxNumber == null)
                addActionError(sm.getString("NiePodanoNumeruPudlaDoPonownegoOtwarcia"));
    		
    		if (hasActionErrors())
                return;
    		
    		boolean defaultLine = DEFAULT_LINE.equals(line);
    		
    		try {
    			DSApi.context().begin();
    			Box box = null;	
    			Long id = defaultLine ? GlobalPreferences.getBoxId() : GlobalPreferences.getBoxId(line);
                if (id != null)
                {	
                	//zamykanie dotychczasowego pudla
                    
                    try
                    {
                        box = Box.find(id);
                        box.setCloseTime(new Date());
                        box.setOpen(false);
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                }    
                //otwieramy stare pudlo
                box = null;
                try
                {
                	if(defaultLine) {
                		box = Box.findByName(null, boxNumber);                		
                	} else box = Box.findByName(line, boxNumber);                 	
  
                	box.setOpenTime(new Date());
                	box.setCloseTime(null);
                	box.setOpen(true);
                  
                  if(defaultLine) {
                	  GlobalPreferences.setBoxId(box.getId());
                	  GlobalPreferences.setBoxOpen(true);
                  } else GlobalPreferences.setBoxId(line, box.getId());
                  
                }
                catch (EntityNotFoundException e) 
                {
                	addActionError(e.getMessage());
                }
                DSApi.context().commit();
    		}
    		catch(EdmException e) {
    			DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
    		}
    		
    	}
    	
    }
    
   /* public String getCurrentBoxNumber()
    {
        return currentBoxNumber;
    }

    public String getBoxNumber()
    {
        return boxNumber;
    }
    */
    public void setBoxNumber(String boxNumber)
    {
        this.boxNumber = boxNumber;
    }
 
    /*  public boolean isBoxOpen()
    {
        return boxOpen;
    }*/

    public void setLine(String line)
    {
        this.line = line;
    }

    public List<BoxLineBean> getBoxLines()
    {
        return boxLines;
    }   
    
    public void setExistingBoxId(Long existingBoxId)
    {
        this.existingBoxId = existingBoxId;
    }

    public void setNewBoxNumber(String newBoxNumber)
    {
        this.newBoxNumber = newBoxNumber;
    }

    public List<Box> getBoxList()
    {
        return boxList;
    }

    public boolean isCanManage()
    {
        return canManage;
    }
    
    public class BoxLineBean
    {
        private String line;
        private String name;
        private String currentBoxNumber;
        private boolean boxOpen;        
        private String boxNumber;        

        public String getBoxNumber()
        {
            return boxNumber;
        }

        public void setBoxNumber(String boxNumber)
        {
            this.boxNumber = boxNumber;
        }

        public boolean isBoxOpen()
        {
            return boxOpen;
        }

        public void setBoxOpen(boolean boxOpen)
        {
            this.boxOpen = boxOpen;
        }

        public String getCurrentBoxNumber()
        {
            return currentBoxNumber;
        }

        public void setCurrentBoxNumber(String currentBoxNumber)
        {
            this.currentBoxNumber = currentBoxNumber;
        }

        public String getLine()
        {
            return line;
        }

        public void setLine(String line)
        {
            this.line = line;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }          
        
        public String getJsOnSubmit()
        {
            return "onSubmit('"+line+"')";
        }
    }

	public String getBoxName() {
		return boxName;
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public String getStartId() {
		return startId;
	}

	public void setStartId(String startId) {
		this.startId = startId;
	}

	public String getEndId() {
		return endId;
	}

	public void setEndId(String endId) {
		this.endId = endId;
	}

	public String getWithoutId() {
		return withoutId;
	}

	public void setWithoutId(String withoutId) {
		this.withoutId = withoutId;
	}

	public boolean isAsk() {
		return ask;
	}

	public void setAsk(boolean ask) {
		this.ask = ask;
	}

	public long getWrongId() {
		return wrongId;
	}

	public void setWrongId(long wrongId) {
		this.wrongId = wrongId;
	}

	public String getBoxName1() {
		return boxName1;
	}

	public void setBoxName1(String boxName1) {
		this.boxName1 = boxName1;
	}

	public String getStartId1() {
		return startId1;
	}

	public void setStartId1(String startId1) {
		this.startId1 = startId1;
	}

	public String getEndId1() {
		return endId1;
	}

	public void setEndId1(String endId1) {
		this.endId1 = endId1;
	}

	public String getWithoutId1() {
		return withoutId1;
	}

	public void setWithoutId1(String withoutId1) {
		this.withoutId1 = withoutId1;
	}

	public String getStartWithdrawId() {
		return startWithdrawId;
	}

	public void setStartWithdrawId(String startWithdrawId) {
		this.startWithdrawId = startWithdrawId;
	}

	public String getEndWithdrawId() {
		return endWithdrawId;
	}

	public void setEndWithdrawId(String endWithdrawId) {
		this.endWithdrawId = endWithdrawId;
	}

	public String getBoxNameWithdraw() {
		return boxNameWithdraw;
	}

	public void setBoxNameWithdraw(String boxNameWithdraw) {
		this.boxNameWithdraw = boxNameWithdraw;
	}

	public String getStartWithdrawId1() {
		return startWithdrawId1;
	}

	public void setStartWithdrawId1(String startWithdrawId1) {
		this.startWithdrawId1 = startWithdrawId1;
	}

	public String getEndWithdrawId1() {
		return endWithdrawId1;
	}

	public void setEndWithdrawId1(String endWithdrawId1) {
		this.endWithdrawId1 = endWithdrawId1;
	}

	public String getBoxNameWithdraw1() {
		return boxNameWithdraw1;
	}

	public void setBoxNameWithdraw1(String boxNameWithdraw1) {
		this.boxNameWithdraw1 = boxNameWithdraw1;
	}

	public List<pl.compan.docusafe.core.dockinds.field.EnumItem> getItems() {
		return items;
	}

	public void setItems(List<pl.compan.docusafe.core.dockinds.field.EnumItem> items) {
		this.items = items;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public Integer getItem1() {
		return item1;
	}

	public void setItem1(Integer item1) {
		this.item1 = item1;
	}
}
