package pl.compan.docusafe.web.admin.overtimes;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

/**
 * raport przelozonego w podziale na miesi�ce
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class SuperiorMonthReport extends AbstractXlsReport
{
    public List<EmployeeCard> employees;
    public int year;

    public SuperiorMonthReport(String reportName, List<EmployeeCard> employees, int year)
    {
        super(reportName);

        columns = new String[]
        {
            "Nazwisko i imi� podw�adnego", "Miesi�c",
            "Liczba wypracowanych nadgodzin", "Liczba odebranych nadgodzin", "Pozosta�e do odebrania nadgodziny"
        };

        this.employees = employees;
        this.year = year;
    }

    @Override
    protected void createEntries()
    {

        try
        {
            int rowCount = 1;
            for( EmployeeCard empCard : employees )
            {
                List<EmployeeOvertimeEntity> empOT = OvertimeFactory.getEmployeeOvertimes(empCard, year);
                for(EmployeeOvertimeEntity empEntity : empOT)
                {
                    int i = -1;
                    HSSFRow row = sheet.createRow(rowCount++);
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empCard.getFirstName() + " " + empCard.getLastName()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empEntity.getOvertimeMonth()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empEntity.getWorked()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empEntity.getReceivedWorked()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empEntity.getLeftWorked()));
                }
                
                
            }
        }
        catch ( EdmException ex )
        {
            Logger.getLogger(SuperiorMonthReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
