package pl.compan.docusafe.web.admin.overtimes;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeMonthlyEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeReceiveMonthlyEntity;
import pl.compan.docusafe.util.DateUtils;

/**
 * raporty prze�o�onego w podziale na dni
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class SuperiorDailyReport extends AbstractXlsReport
{

    public List<EmployeeCard> employees;
    public int year;

    public SuperiorDailyReport(String reportName, List<EmployeeCard> employees, int year)
    {
        super(reportName);

        columns = new String[]
        {
            "Nazwisko i imi� podw�adnego",
            "Data Wypracowania nadgodzin", "Liczba wypracowanych nadgodzin",
            "Uzasadnienie", "Data odebrania nadgodzin", "Liczba odebranych nadgodzin"
        };

        this.employees = employees;
        this.year = year;
    }

    @Override
    protected void createEntries()
    {
        try
        {
            int rowCount = 1;
            for( EmployeeCard empCard : employees )
            {
                /** Tworzy okiekt przechowuj�cy potrzebne dane*/
                List<EmployeeOvertimeMonthlyEntity> empMonthlyBean = OvertimeFactory.getEmployeeMonthlyOvertimes(empCard, year, 1);

                for( EmployeeOvertimeMonthlyEntity entity : empMonthlyBean )
                {
                    int i = -1;
                    HSSFRow row = sheet.createRow(rowCount++);
                    row.createCell(++i).setCellValue(new HSSFRichTextString(empCard.getFirstName() + " " + empCard.getLastName()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.jsDateFormat.format(entity.getDate())));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(entity.getOvertimes()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(entity.getDescription()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(entity.getFirstDateReceive()));
                    row.createCell(++i).setCellValue(new HSSFRichTextString(entity.getFirstOvertimesReceive()));
                    
                    List<EmployeeOvertimeReceiveMonthlyEntity> entityReceives = entity.getEmpOvertimeReceive();
                    
                    for(EmployeeOvertimeReceiveMonthlyEntity entityReceive : entityReceives)
                    {
                        int k = -1;
                        HSSFRow row2 = sheet.createRow(rowCount++);
                        row2.createCell(++k).setCellValue(" ");
                        row2.createCell(++k).setCellValue(" ");
                        row2.createCell(++k).setCellValue(" ");
                        row2.createCell(++k).setCellValue(" ");
                        row2.createCell(++k).setCellValue(new HSSFRichTextString(entityReceive.getDate() ));
                        row2.createCell(++k).setCellValue(new HSSFRichTextString(entityReceive.getOvertimesReceive()));
                    }
                    
                    

                }
            }
        }
        catch ( SQLException ex )
        {
            Logger.getLogger(SuperiorMonthReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch ( EdmException ex )
        {
            Logger.getLogger(SuperiorMonthReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
