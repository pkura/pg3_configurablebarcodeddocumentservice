package pl.compan.docusafe.web.admin.overtimes;

import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeCardAction extends EventActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(OvertimeCardAction.class.getPackage().getName());
    private static final StringManager sm = StringManager.getManager(OvertimeCardAction.class.getPackage().getName());
 
    private EmployeeCard empCard;
    private int currentYear;
    private List<OvertimeCard> overtimes;
    /** Wpisy do usuni�cia */
    private Long[] deleteIds;

    @Override
    protected void setup()
    {
        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(new FillForm())
            .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doBatchDelete")
            .append(OpenHibernateSession.INSTANCE)
            .append(new BatchDelete())
            .append(new FillForm())
            .appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
       @Override
       public void actionPerformed(ActionEvent event)
        {
            if(currentYear == 0 )
                currentYear = DateUtils.getCurrentYear();
            
            try
            {                 
                DSApi.context().begin();
                empCard = Finder.find(EmployeeCard.class, empCard.getId());
                empCard.getId(); // lazyyyy
                
                overtimes = OvertimeCard.findByEmployeeId(empCard.getId(), currentYear);
                
                DSApi.context().commit();
            }
            catch (EdmException ex)
            {
                    DSApi.context()._rollback();
                    addActionError(ex.getMessage());
                    log.error(ex.getMessage(), ex);
            }
        }
    }
    
    public class BatchDelete implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {                 
                DSApi.context().begin();
                
                if (deleteIds == null || deleteIds.length == 0)
                        return;

                for (Long dId : deleteIds) 
                {				
                    OvertimeCard abs = DSApi.getObject(OvertimeCard.class, dId);
                    abs.delete();
                    addActionMessage(sm.getString("PoprawnieUsunietoWybranyNadgodziny"));
                }

                DSApi.context().commit();
            }
            catch (EdmException ex)
            {
                DSApi.context()._rollback();
                addActionError(ex.getMessage());
                log.error(ex.getMessage(), ex);
            }

        }
        
    }
    public EmployeeCard getEmpCard()
    {
        return empCard;
    }

    public void setEmpCard(EmployeeCard empCard)
    {
        this.empCard = empCard;
    }

    public int getCurrentYear()
    {
        return currentYear;
    }

    public void setCurrentYear(int currentYear)
    {
        this.currentYear = currentYear;
    }

    public List<OvertimeCard> getOvertimes()
    {
        return overtimes;
    }

    public void setOvertimes(List<OvertimeCard> overtimes)
    {
        this.overtimes = overtimes;
    }

    public Long[] getDeleteIds()
    {
        return deleteIds;
    }

    public void setDeleteIds(Long[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }
}
