package pl.compan.docusafe.web.admin.overtimes;

import com.opensymphony.webwork.ServletActionContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimesSummary;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja zarz�rza list� u�ytkownik�w
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeCardListAction extends EventActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(OvertimeCardListAction.class.getPackage().getName());
    private static final StringManager sm = StringManager.getManager(OvertimeCardListAction.class.getPackage().getName());



    private List<EmployeeCard> employees;
    /**
     * Rok, z kt�rego b�d� brane pod uwag� urlopy
     */
    private int currentYear;
    private Query query;

    /**
     * Obiekt empCard wykorzystywany jest do ustalenia parametr�w wyszukiwania
     */
    private EmployeeCard empCard;

    /**
     * Tabela przechowuj�ca identyfikatory nadgodziny do usuni�cia
     */
    private Long[] deleteIds;

    /**
     * Ustala czy mo�na naliczy� urlopy na kolejny rok
     */
    private boolean canChargeOvertimes;
    /**
     * Numer strony
     */
    private int pageNumber = 0;

    /**
     * Liczba rezultat�w
     */
    private int maxResults = 20;

    /**
     * Obiekt pager'a
     */
    private Pager pager;
    
    private int lp;
        
    @Override
    protected void setup()
    {
        registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
                        .append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doBatchDelete")
			.append(OpenHibernateSession.INSTANCE)
                        .append(new FillForm())
                        .append(new BatchDelete())
			.appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("chargeOvertimes")
			.append(OpenHibernateSession.INSTANCE)
                        .append(new ChargeOvertimes())
                        .append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
        
        
        registerListener("generateReport")
			.append(OpenHibernateSession.INSTANCE)
                        .append(new FillForm())
			.append(new XlsReport())
			.appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {

                if (currentYear == 0)
                {
                    currentYear = DateUtils.getCurrentYear();
                }

                query = OvertimeFactory.generateReportFromYear(currentYear, empCard);
                canChargeOvertimes = checkCanChargeOvertimes();

                employees = query.setFirstResult(pageNumber).setMaxResults(maxResults).list();

                Iterator it = employees.iterator();
                createPager(employees.size());

                while (it.hasNext())
                {
                    EmployeeCard listEmpCard = (EmployeeCard) it.next();
                    //nadgodziny pracownika w danym roku wybranym
                    List<EmployeeOvertimeEntity> empOT = OvertimeFactory.getEmployeeOvertimes(listEmpCard, currentYear, false);
                    
                    if(empOT.isEmpty())
                    {
                        it.remove();
                        continue;
                    }
                    /** podsumowania dla pracownika w danym roku */
                    EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOT);

                    listEmpCard.setEmpYearsSummary(empOtSummary);
                }
            } catch (Exception ex)
            {
                log.error("", ex);
            }

        }
        
        /**
         * Tworzy pager dla widoku
         * 
         * @throws EdmException
         */
        private void createPager(int count) throws EdmException
        {
                pager = new Pager(new Pager.LinkVisitor() {
                        public String getLink(int offset) 
                        {
                                return offset + "";
                        }
                }, pageNumber, maxResults, count, 5, true);
        }
        
        
        public boolean checkCanChargeOvertimes() throws EdmException
        {
            @SuppressWarnings("unchecked")
            List<EmployeeCard> tocheckEmpCard = OvertimeFactory.generateReportFromYear(DateUtils.getCurrentYear(), empCard).list();
            
            for(EmployeeCard empCard : tocheckEmpCard)
            {
                List<EmployeeOvertimeEntity> empOT = OvertimeFactory.getEmployeeOvertimes(empCard, DateUtils.getCurrentYear()-1, false);
                
                if(!empOT.isEmpty())
                    return false;
            }
            return true;
        }
    }
    
    public class BatchDelete implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
             try
            {
                    DSApi.context().begin();

                    if (deleteIds == null || deleteIds.length == 0)
					throw new EdmException(sm.getString("NieWybranoZadnychWpisow"));
				
                    DSApi.context().session().createQuery("delete from " + OvertimeCard.class.getName() 
                                    + " op where op.employeeId in (:ids) and (datename(YEAR, op.overtimeDate) = :year)")
                                    .setParameterList("ids", deleteIds)
                                    .setInteger("year", currentYear)
                                    .executeUpdate();
                                
                    DSApi.context().commit();
                    
                    addActionMessage("Wybrane wpisy zosta�y usuni�te!");
            }
            catch (EdmException ex)
            {
                    DSApi.context()._rollback();
                    addActionError(ex.getMessage());
                    log.error(ex.getMessage(), ex);
            }
        }
    }
    
    public class ChargeOvertimes implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                OvertimeFactory.chargeOvertimesForNewYear(OvertimeFactory.generateReportFromYear(DateUtils.getCurrentYear()-1, empCard).list(),
                            DateUtils.getCurrentYear());
                
                addActionMessage(sm.getString("PoprawnieNaliczonoNadgodziny"));
                DSApi.context().commit();
            } catch (EdmException ex)
            {
                DSApi.context()._rollback();
                addActionError(ex.getMessage());
                log.error(ex.getMessage(), ex);
            }
        }
    }
    /**
     * Tworzy raport na podstawie zapytania
     */
    public class XlsReport implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                AbstractXlsReport xlsReport = new SuperiorMonthReport("Raport wymiar�w nadgodzin", query.list(), currentYear);
                xlsReport.generate();
                
                createContent(xlsReport, event);
                DSApi.context().commit();
            }
            catch (EdmException ex)
            {
                DSApi.context()._rollback();
                LOG.error(ex.getMessage(), ex);
                addActionError(ex.getMessage());
            }
        }
        
        private void createContent(AbstractXlsReport report, ActionEvent event) throws EdmException
        {
            try
            {
                File tmpfile = File.createTempFile("DocuSafe", "tmp");
                tmpfile.deleteOnExit();
                FileOutputStream fis = new FileOutputStream(tmpfile);   
                report.getWorkbook().write(fis);
                fis.close();
                ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/vnd.ms-excel", "Content-Disposition: attachment; filename=\"DocusafeReport.xls\"");
            }
            catch ( IOException ex )
            {
                addActionError(ex.getMessage());
                log.error(ex.getMessage(), ex);
            }
            event.setResult(NONE);
        }
    }

    public boolean isCanChargeOvertimes()
    {
        return canChargeOvertimes;
    }

    public void setCanChargeOvertimes(boolean canChargeOvertimes)
    {
        this.canChargeOvertimes = canChargeOvertimes;
    }

    public int getCurrentYear()
    {
        return currentYear;
    }

    public void setCurrentYear(int currentYear)
    {
        this.currentYear = currentYear;
    }

    public Long[] getDeleteIds()
    {
        return deleteIds;
    }

    public void setDeleteIds(Long[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }

    public EmployeeCard getEmpCard()
    {
        return empCard;
    }

    public void setEmpCard(EmployeeCard empCard)
    {
        this.empCard = empCard;
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public boolean isChargeOvertimesEnabled()
    {
            return canChargeOvertimes;
    }
    
    public void setMaxResults(int maxResults)
    {
        this.maxResults = maxResults;
    }

    public int getPageNumber()
    {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber)
    {
        this.pageNumber = pageNumber;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }
    
    
    public String getBaseUrl()
    {
        return "/absences/overtime-card-list.action";
    }
    
    /**
     * Zwraca map� z rokiem dla elementu select
     * 
     * @return
     */
    public Map<Integer, Integer> getYearsMap() 
    {
            Map<Integer, Integer> yearsMap = new LinkedHashMap<Integer, Integer>();
            for (int i = 2000; i <= GregorianCalendar.getInstance().get(GregorianCalendar.YEAR); i++)
                    yearsMap.put(i, i);
            return yearsMap;
    }
    
    public Map<Integer, Integer> getMaxResultsMap()
    {
            Map<Integer, Integer> maxResultsMap = new LinkedHashMap<Integer, Integer>();
            for (int i = 5; i < 41; i += 5)
                    maxResultsMap.put(i, i);
            return maxResultsMap;
    }

    public List<EmployeeCard> getEmployees()
    {
        return employees;
    }

    public void setEmployees(List<EmployeeCard> employees)
    {
        this.employees = employees;
    }
}
