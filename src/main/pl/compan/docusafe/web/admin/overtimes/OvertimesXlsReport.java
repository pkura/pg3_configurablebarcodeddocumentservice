package pl.compan.docusafe.web.admin.overtimes;

import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

/**
 * Tworzy raport xls dla administratora z wyszukanymi pracownikami
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class OvertimesXlsReport extends AbstractXlsReport
{
    private List<EmployeeCard> employees;
    
    public OvertimesXlsReport(String reportName,List<EmployeeCard> employees) 
	{
		super(reportName);
		
		this.employees = employees;
		columns = new String[] {
				"Lp.", "Nazwisko i imi�", "Nazwa sp�ki", "Dzia�",	
                                "Liczba wypracowanych nadgodzin","Liczba odebranych nadgodzin", "Pozosta�e nadgodziny"
			};
	}
    @Override
    protected void createEntries()
    {
        int rowCount = 1;
        for( EmployeeCard reportEntry  : employees)
        {
            int i = -1; 
            HSSFRow row = sheet.createRow(rowCount);
            row.createCell(++i).setCellValue(rowCount++);
            row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getFirstName() + " " + reportEntry.getLastName()));
            row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getCompany()));
            row.createCell(++i).setCellValue(new HSSFRichTextString(reportEntry.getDivision()));
            row.createCell(++i).setCellValue(reportEntry.getEmpYearsSummary().getAllHour());
            row.createCell(++i).setCellValue(reportEntry.getEmpYearsSummary().getAllHourReceive());
            row.createCell(++i).setCellValue(reportEntry.getEmpYearsSummary().getAllHourLeft());
        }
    }
    
}
