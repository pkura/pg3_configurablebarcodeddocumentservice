package pl.compan.docusafe.web.admin.overtimes;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeRecordAction extends EventActionSupport
{
    private static final StringManager sm = StringManager.getManager(OvertimeRecordAction.class.getPackage().getName());
    private static final Logger log = LoggerFactory.getLogger(OvertimeRecordAction.class.getPackage().getName());

    private OvertimeCard overtime;
    private EmployeeCard empCard;
    private int currentYear;
    private String overtimeDate;
    
    @Override
    protected void setup()
    {
        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(new FillForm())
            .appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSave")
            .append(OpenHibernateSession.INSTANCE)
            .append(new Save())
            .append(new FillForm())
            .appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                    DSApi.context().begin();

                    if (overtime != null && overtime.getId() != null)
                    {
                            // je�li istnieje dany obiekt urlopu pobranie go z bazy
                            overtime = Finder.find(OvertimeCard.class, overtime.getId());
                            empCard = Finder.find(EmployeeCard.class, overtime.getEmployeeId());
                            overtimeDate = DateUtils.commonDateFormat.format(overtime.getOvertimeDate());
                            empCard.getId(); // lazyyyy
                    }
                    else 
                    {
                            // nie ma obiektu urlopu - tworzony b�dzie w formularzu nowy wpis
                            if (empCard == null || empCard.getId() == null)
                                    throw new EdmException(sm.getString("NieWybranoKartyPracownika"));

                            empCard = Finder.find(EmployeeCard.class, empCard.getId());
                            empCard.getId(); // lazyyyy
                    }

                    DSApi.context().commit();
            }
            catch (EdmException ex)
            {
                    DSApi.context()._rollback();
                    addActionError(ex.getMessage());
                    log.error(ex.getMessage(), ex);
            }
        }
    }

    public class Save implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                checkFormFields();
                
                OvertimeCard otCard = new OvertimeCard();
                otCard.setDescription(overtime.getDescription());
                otCard.setTimeFrom(overtime.getTimeFrom());
                otCard.setTimeTo(overtime.getTimeTo());
                otCard.setTimeCount(overtime.getTimeCount());
                otCard.setEmployeeId(empCard.getId());
                otCard.setOvertimeDate(DateUtils.nullSafeParseJsDate(overtimeDate));
                otCard.setOvertimeReceive(overtime.getOvertimeReceive());
                if(overtime.getDocumentId() != null)
                    otCard.setDocumentId(overtime.getDocumentId());
                
                if(overtime.getId() == null)
                    otCard.saveWithPS();
                else
                {
                    otCard.setId(overtime.getId());
                    otCard.updateWithPS();
                }
                
                
                DSApi.context().commit();
                
                addActionMessage("Dodano wpis!");
            } catch (EdmException ex)
            {
                DSApi.context()._rollback();
                addActionError(ex.getMessage());
                log.error(ex.getMessage(), ex);
            }
        }
        
        public void checkFormFields() throws EdmException
        {
            Date otDate = DateUtils.nullSafeParseJsDate(overtimeDate);
            
            
            if(otDate == null)
                throw new EdmException("Pole Data jest obowi�zkowe!");

            //rzuca wyj�tkiem
            FreeDay.isFreeDay(otDate);
            
            Calendar cal = DateUtils._GREGORIAN_CALENDAR;
            cal.setTime(otDate);
            
            /** @todo dodac walidacj� czy administrator mo�e doda� odbi�r nadgodzin*/
            if (cal.get(GregorianCalendar.YEAR) < 2000)
                    addActionError(sm.getString("PodajPoprawnieRok"));
            overtime.setOvertimeDate(otDate);
            
            try{
                TimeUtils.checkTimeRange(overtime.getTimeFrom(), overtime.getTimeTo());
            } catch (EdmException ex)
            {
                addActionError(sm.getString(ex.getMessage()));
            }
            
            if (empCard.getId() == null || overtime.getEmployeeId() == null)
                    addActionError(sm.getString("NieWybranoKartyPracownika"));

            if (hasActionErrors())
                    throw new EdmException("W formularzu wyst�pi�y b��dy!");
        }
       
    }
    public int getCurrentYear()
    {
        return currentYear;
    }

    public void setCurrentYear(int currentYear)
    {
        this.currentYear = currentYear;
    }

    public EmployeeCard getEmpCard()
    {
        return empCard;
    }

    public void setEmpCard(EmployeeCard empCard)
    {
        this.empCard = empCard;
    }

    public OvertimeCard getOvertime()
    {
        return overtime;
    }

    public void setOvertime(OvertimeCard overtime)
    {
        this.overtime = overtime;
    }

    public String getOvertimeDate()
    {
        return overtimeDate;
    }

    public void setOvertimeDate(String overtimeDate)
    {
        this.overtimeDate = overtimeDate;
    }
}
