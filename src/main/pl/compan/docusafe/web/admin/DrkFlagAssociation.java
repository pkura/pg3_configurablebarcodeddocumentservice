package pl.compan.docusafe.web.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class DrkFlagAssociation extends EventActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<FlagToDivisionBean> bean;
	private List<DSDivision> dzialy;
	private String[] flagName;
	private String[] guid;
	
	protected void setup() 
	{
		
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveAssociation").
	        append(OpenHibernateSession.INSTANCE).
	        append(new saveAssociation()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
		
	}
	
	private class FlagToDivisionBean
	{
		private String flaga;
		private String guid;
		private String opisFlagi;
		
		public FlagToDivisionBean(String flaga, String opisFlagi, String guid) 
		{
			this.flaga = flaga;
			this.guid = guid;
			this.opisFlagi = opisFlagi;
		}

		public String getFlaga() {
			return flaga;
		}

		public void setFlaga(String flaga) {
			this.flaga = flaga;
		}

		public String getGuid() {
			return guid;
		}

		public void setGuid(String guid) {
			this.guid = guid;
		}

		public String getOpisFlagi() {
			return opisFlagi;
		}

		public void setOpisFlagi(String opisFlagi) {
			this.opisFlagi = opisFlagi;
		}
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	List<Label> flags = new ArrayList<Label>();
        	PreparedStatement ps = null;
        	try
        	{
        		ps = DSApi.context().prepareStatement("select id from dso_label where name like ?");
        		ps.setString(1, "DRK%");
        		ResultSet rs = ps.executeQuery();
        		while(rs.next())
        		{
        			flags.add(LabelsManager.findLabelById(rs.getLong("id")));
        		}
        		DSApi.context().closeStatement(ps);
        		ps = null;
        		dzialy = Arrays.asList(DSDivision.getAllDivisions());        		
        	}
        	catch(Exception e)
        	{
        		e.printStackTrace();
        	}
            finally
            {
            	DSApi.context().closeStatement(ps);
            }
            
            bean = new ArrayList<FlagToDivisionBean>();
            for(Label flaga : flags)
            {
            	bean.add(new FlagToDivisionBean(flaga.getName(),flaga.getDescription(),DSApi.context().systemPreferences().node("drk-mail").get(""+flaga.getId(), null)));
            }
                        
        }
    }

	private class saveAssociation implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				for(int i=0; i<flagName.length; i++)
				{
					DSApi.context().systemPreferences().node("drk-mail").put(""+LabelsManager.findLabelByName(flagName[i]).getId(), guid[i]);
					
				}
				DSApi.context().commit();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				DSApi.context()._rollback();
			}
		}
	}
	
	public List<FlagToDivisionBean> getBean() 
	{
		return bean;
	}

	public void setDzialy(List<DSDivision> dzialy) {
		this.dzialy = dzialy;
	}

	public List<DSDivision> getDzialy() {
		return dzialy;
	}

	public String[] getFlagName() {
		return flagName;
	}

	public void setFlagName(String[] flagName) {
		this.flagName = flagName;
	}

	public String[] getGuid() {
		return guid;
	}

	public void setGuid(String[] guid) {
		this.guid = guid;
	}
}
