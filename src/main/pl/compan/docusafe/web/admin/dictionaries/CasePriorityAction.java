package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CasePriorityAction.java,v 1.3 2006/02/20 15:42:33 lk Exp $
 */
public class CasePriorityAction extends EventActionSupport
{
    // @EXPORT
    private List<Map> priorities;

    // @IMPORT
    private Integer[] priorityIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                priorities = makePriorityBeans(CasePriority.list());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

/*
    public static class CanDelete {
        private boolean canDelete;
        CanDelete(boolean canDelete) { this.canDelete = canDelete; }
        public boolean canDelete() { return canDelete; }
    }
*/

    /**
     * Zwraca list� bean�w utworzonych na podstawie kolekcji przekazanych
     * obiekt�w CasePriority. Ka�dy bean ma te same metody co
     * CasePriority.
     * @param priorities
     * @return
     * @throws EdmException
     */
    protected List<Map> makePriorityBeans(List<CasePriority> priorities) throws EdmException
    {
        List<Map> result = new ArrayList<Map>(priorities.size());

        for (CasePriority priority : priorities)
        {
            BeanBackedMap map = new BeanBackedMap(priority, true, "id", "posn", "name");
            map.put("canDelete", priority.canDelete());
            result.add(map);
        }

        return result;
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (priorityIds == null || priorityIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < priorityIds.length; i++)
                {
                    CasePriority d = CasePriority.find(priorityIds[i]);
                    addActionMessage("Usuni�to '"+d.getName()+"'");
                    d.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getPriorities()
    {
        return priorities;
    }

    public void setPriorityIds(Integer[] priorityIds)
    {
        this.priorityIds = priorityIds;
    }
}
