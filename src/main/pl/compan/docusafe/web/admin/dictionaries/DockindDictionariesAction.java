package pl.compan.docusafe.web.admin.dictionaries;

import java.util.*;

import com.google.common.base.Preconditions;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CasePriorityAction.java,v 1.3 2006/02/20 15:42:33 lk Exp $
 */
public class DockindDictionariesAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(DockindDictionariesAction.class);
    // @EXPORT
    private List<DockindDictionaries> elements;

    // @IMPORT
    private Integer[] elementIds;
    private Map<String, String> documentKindFields;
    private Map<String, DocumentKind> fieldToKind;
    private String choosen;
    private String docKind;
    private Integer id;

    private boolean toAvailable;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doChange").
	        append(OpenHibernateSession.INSTANCE).	        
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReload").
                append(OpenHibernateSession.INSTANCE).
                append(new Reload()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeEntry").
	        append(OpenHibernateSession.INSTANCE).
                append(new UpdateEntryAvailability()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		documentKindFields = new HashMap<String, String>();
        		fieldToKind = new HashMap<String, DocumentKind>();
        		for(DocumentKind dk : DocumentKindProvider.get().visible().list())
        		{
//        			interesuj� nas tylko pola typu baza danych albo slownik
        			for(Field fd : dk.getFields())
        			{
        				if(fd.getType().equals(Field.DATA_BASE))
        				{
//        					do mapy dajemy nazwe tabeli i nazwe pola typu data base
        					documentKindFields.put(((DataBaseEnumField)fd).getTableName(),dk.getName()+" - "+fd.getName()+" ["+((DataBaseEnumField) fd).getTableName()+"]");
//        					do mapy dajemy nazwe tabeli i typ dokumentu
        					fieldToKind.put(((DataBaseEnumField)fd).getTableName(), dk);
        				}
        				else if(fd.getType().equals(Field.DICTIONARY))
        				{
		        			for(Field fdd : fd.getFields())
		        			{
		        				if(fdd.getType().equals(Field.DATA_BASE))
		        				{
		        					//fd.getFields();
		        					documentKindFields.put(((DataBaseEnumField)fdd).getTableName(),dk.getName()+" - "+fdd.getName());
		        					fieldToKind.put(((DataBaseEnumField)fdd).getTableName(), dk);
		        				}
		        			}
        				}
        			}
        		}
                //sortowanie listy s�ownik�w po tytule
                documentKindFields= sortByValues(documentKindFields);

                elements = new ArrayList<DockindDictionaries>();
				if(choosen != null)
				{
					docKind = fieldToKind.get(choosen).getCn();
//					lista rekord�w z selecta choosen rep. nazwe tabeli
					elements = DockindDictionaries.list(choosen);
				}
			}
        	catch (Exception e) 
			{
        		log.error("",e);
        		addActionError(e.getMessage());
			} 

        }
    }

    private class UpdateEntryAvailability extends TransactionalActionListener{
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            DockindDictionaries.updateAvailability(choosen, id, toAvailable);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            DataBaseEnumField.reloadForTable(choosen);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class Delete extends TransactionalActionListener
    {
       @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            DockindDictionaries.delete(choosen, id);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            DataBaseEnumField.reloadForTable(choosen);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class Reload implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                Preconditions.checkNotNull(choosen, "Nie wskazano s�ownika");
                DataBaseEnumField.reloadForTable(choosen);
                addActionMessage("Pomy�lnie od�wie�ono s�ownik tabeli "+choosen);
            } catch (Exception e) {
                log.error("", e);
                addActionError(e.getMessage());
            } finally {
                choosen = null;
            }
        }
    }

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public boolean isToAvailable() {
        return toAvailable;
    }

    public void setToAvailable(boolean toAvailable) {
        this.toAvailable = toAvailable;
    }

	public List<DockindDictionaries> getElements() {
		return elements;
	}

	public void setElements(List<DockindDictionaries> elements) {
		this.elements = elements;
	}

	public Integer[] getElementIds() {
		return elementIds;
	}

	public void setElementIds(Integer[] elementIds) {
		this.elementIds = elementIds;
	}

	public String getChoosen() {
		return choosen;
	}

	public void setChoosen(String choosen) {
		this.choosen = choosen;
	}

	public Map<String, String> getDocumentKindFields() {
		return documentKindFields;
	}

	public void setDocumentKindFields(Map<String, String> documentKindFields) {
		this.documentKindFields = documentKindFields;
	}

	public String getDocKind() {
		return docKind;
	}

	public void setDocKind(String docKind) {
		this.docKind = docKind;
	}


    private static Map sortByValues(Map map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

}
