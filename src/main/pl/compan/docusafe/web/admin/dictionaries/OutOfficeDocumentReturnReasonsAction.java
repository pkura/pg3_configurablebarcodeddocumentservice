package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OutOfficeDocumentReturnReason;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;

/**
 * Cele dekretacji.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentReturnReasonsAction.java,v 1.3 2006/02/08 14:54:20 lk Exp $
 */
public class OutOfficeDocumentReturnReasonsAction extends EventActionSupport
{
    // @EXPORT
    private List<OutOfficeDocumentReturnReason> reasons;

    // @IMPORT
    private Integer[] reasonIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                reasons = OutOfficeDocumentReturnReason.list();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (reasonIds == null || reasonIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < reasonIds.length; i++)
                {
                    OutOfficeDocumentReturnReason reason = OutOfficeDocumentReturnReason.find(reasonIds[i]);
                    addActionMessage("Usuni�to '"+reason.getName()+"'");
                    reason.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getReasons()
    {
        return reasons;
    }

    public void setReasonIds(Integer[] reasonIds)
    {
        this.reasonIds = reasonIds;
    }
}
