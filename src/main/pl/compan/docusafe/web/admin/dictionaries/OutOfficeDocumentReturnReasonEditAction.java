package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OutOfficeDocumentReturnReason;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OutOfficeDocumentReturnReasonEditAction.java,v 1.4 2006/02/20 15:42:34 lk Exp $
 */
public class OutOfficeDocumentReturnReasonEditAction extends EventActionSupport
{
    // @EXPORT/@IMPORT
    private Integer id;
    private String name;

    // @EXPORT
    private OutOfficeDocumentReturnReason reason;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id != null)
            {
                try
                {
                    reason = OutOfficeDocumentReturnReason.find(id);
                    DSApi.initializeProxy(reason);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (id == null)
                {
                    if (OutOfficeDocumentReturnReason.findByName(name).size() > 0)
                        throw new EdmException("Istnieje ju� cel dekretacji o nazwie '"+name+"'");

                    OutOfficeDocumentReturnReason reason = new OutOfficeDocumentReturnReason(name);
                    reason.create();
                }
                else
                {
                    OutOfficeDocumentReturnReason.find(id).setName(name);
                }

                DSApi.context().commit();
                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public OutOfficeDocumentReturnReason getReason()
    {
        return reason;
    }

    public void setReason(OutOfficeDocumentReturnReason reason)
    {
        this.reason = reason;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
