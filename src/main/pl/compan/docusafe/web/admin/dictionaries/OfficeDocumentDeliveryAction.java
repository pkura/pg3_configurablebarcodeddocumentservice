package pl.compan.docusafe.web.admin.dictionaries;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocumentDelivery;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeDocumentDeliveryAction.java,v 1.4 2007/11/27 12:11:50 mariuszk Exp $
 */
public abstract class OfficeDocumentDeliveryAction extends EventActionSupport
{
    public abstract String getDocumentType();

    /**
     * Zwraca list� bean�w utworzonych na podstawie kolekcji przekazanych
     * obiekt�w OfficeDocumentDelivery. Ka�dy bean ma te same metody co
     * OfficeDocumentDelivery.
     * @param officeDocumentDeliveries
     * @return
     * @throws EdmException
     */
    protected List<Map> makeDeliveryBeans(List<? extends OfficeDocumentDelivery> officeDocumentDeliveries) throws EdmException
    {
        List<Map> results = new ArrayList<Map>(officeDocumentDeliveries.size());

        for (OfficeDocumentDelivery delivery : officeDocumentDeliveries)
        {
            BeanBackedMap map = new BeanBackedMap(delivery, true, "id", "name", "posn");
        	LoggerFactory.getLogger("tomekl").debug("{} {}",delivery.getName(),delivery.canDelete());
            map.put("canDelete", delivery.canDelete());
            results.add(map);
        }

        return results;
    }
}
