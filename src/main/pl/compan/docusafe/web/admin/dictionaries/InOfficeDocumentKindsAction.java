package pl.compan.docusafe.web.admin.dictionaries;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentKindsAction.java,v 1.8 2009/02/10 09:47:07 pecet3 Exp $
 */
public class InOfficeDocumentKindsAction extends EventActionSupport
{
    // @EXPORT
    private List<Map> kinds;

    // @IMPORT
    private Integer[] kindIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                List<InOfficeDocumentKind> k = InOfficeDocumentKind.list();
                kinds = new ArrayList<Map>(k.size());

                for (InOfficeDocumentKind kind : k)
                {
                    BeanBackedMap map = new BeanBackedMap(kind, true, "id", "name", "posn", "days", "name_en");
                    map.put("canDelete", kind.canDelete());
                    kinds.add(map);
                }
            }
            catch (Exception e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (kindIds == null || kindIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < kindIds.length; i++)
                {
                    InOfficeDocumentKind k = InOfficeDocumentKind.find(kindIds[i]);
                    addActionMessage("Usuni�to '"+k.getName()+"'");
                    k.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List<Map> getKinds()
    {
        return kinds;
    }

    public void setKindIds(Integer[] kindIds)
    {
        this.kindIds = kindIds;
    }
}
