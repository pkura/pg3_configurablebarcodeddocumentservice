package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InvoicesCpvCodes;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class InvoicesCpvCodesEditAction extends EventActionSupport
{
//  @IMPORT/@EXPORT
    private Integer id;
    private String name;
    private String category;

    // @IMPORT

    // @EXPORT
    private InvoicesCpvCodes kind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                kind = InvoicesCpvCodes.find(id);
                DSApi.initializeProxy(kind);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano kodu Cpv");
            if (StringUtils.isEmpty(category))
                addActionError("Nie podano kategorii");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                	InvoicesCpvCodes kind = InvoicesCpvCodes.find(id);
                    kind.setName(name.trim());
                    kind.setCategory(category.trim());
                }
                else
                {
                	InvoicesCpvCodes kind = new InvoicesCpvCodes(name.trim(), category.trim());
                    kind.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    

    public void setId(Integer id)
    {
        this.id = id;
    }

    public InvoicesCpvCodes getKind()
    {
        return kind;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getCategory()
    {
        return category;
    }
    
    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
