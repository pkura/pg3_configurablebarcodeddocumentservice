package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentStatusAction.java,v 1.3 2006/02/20 15:42:34 lk Exp $
 */
public class InOfficeDocumentStatusAction extends EventActionSupport
{
    // @EXPORT
    private List<Map> statuses;

    // @IMPORT
    private Integer[] statusIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String getDocumentType()
    {
        return "in";
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                statuses = makeStatusBeans(InOfficeDocumentStatus.list());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    /**
     * Zwraca list� bean�w utworzonych na podstawie kolekcji przekazanych
     * obiekt�w OfficeDocumentDelivery. Ka�dy bean ma te same metody co
     * OfficeDocumentDelivery.
     * @param statuses
     * @return
     * @throws EdmException
     */
    protected List<Map> makeStatusBeans(List<InOfficeDocumentStatus> statuses) throws EdmException
    {
        List<Map> results = new ArrayList<Map>(statuses.size());

        for (InOfficeDocumentStatus status : statuses)
        {
            BeanBackedMap map = new BeanBackedMap(status, true, "id", "name", "cn", "posn");
            map.put("canDelete", status.canDelete());
            results.add(map);
        }

        return results;
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (statusIds == null || statusIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < statusIds.length; i++)
                {
                    InOfficeDocumentStatus d = InOfficeDocumentStatus.find(statusIds[i]);
                    addActionMessage("Usuni�to '"+d.getName()+"'");
                    d.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getStatuses()
    {
        return statuses;
    }

    public void setStatusIds(Integer[] statusIds)
    {
        this.statusIds = statusIds;
    }
}
