package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.webwork.event.*;

import java.util.List;

/**
 * Cele dekretacji.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentObjectivesAction.java,v 1.4 2009/03/03 14:41:44 pecet3 Exp $
 */
public class AssignmentObjectivesAction extends EventActionSupport
{
    // @EXPORT
    private List objectives;

    // @IMPORT
    private Integer[] objectiveIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                objectives = AssignmentObjective.list();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (objectiveIds == null || objectiveIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
            	DSApi.context().begin();
                for (int i=0; i < objectiveIds.length; i++)
                {
                    AssignmentObjective objective = AssignmentObjective.find(objectiveIds[i]);
                    addActionMessage("Usuni�to '"+objective.getName()+"'");
                    objective.delete();
                }
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	addActionError(e.getMessage());
            	DSApi.context().setRollbackOnly();
            }
        }
    }

    public List getObjectives()
    {
        return objectives;
    }

    public void setObjectiveIds(Integer[] objectiveIds)
    {
        this.objectiveIds = objectiveIds;
    }
}
