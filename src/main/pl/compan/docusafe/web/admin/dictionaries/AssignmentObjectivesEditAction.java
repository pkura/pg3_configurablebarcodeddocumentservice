package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssignmentObjective;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AssignmentObjectivesEditAction.java,v 1.3 2006/02/20 15:42:33 lk Exp $
 */
public class AssignmentObjectivesEditAction extends EventActionSupport
{
    // @EXPORT/@IMPORT
    private Integer id;
    private String name;

    // @EXPORT
    private AssignmentObjective objective;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id != null)
            {
                try
                {
                    objective = AssignmentObjective.find(id);
                }
                catch (EdmException e)
                {
                    addActionError(e.getMessage());
                }
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                if (id == null)
                {
                    if (AssignmentObjective.findByName(name).size() > 0)
                        throw new EdmException("Istnieje ju� cel dekretacji o nazwie '"+name+"'");

                    AssignmentObjective objective = new AssignmentObjective(name);
                    objective.setOfficeStatus(OfficeDocument.STATUS_NEW);
                    objective.setDocumentType(InOfficeDocument.TYPE);
                    objective.create();
                }
                else
                {
                    AssignmentObjective.find(id).setName(name);
                }

                DSApi.context().commit();
                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public AssignmentObjective getObjective()
    {
        return objective;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
