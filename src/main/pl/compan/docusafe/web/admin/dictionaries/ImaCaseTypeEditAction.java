package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ima.dictionary.CaseType;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ImaCaseTypeEditAction extends EventActionSupport
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// @IMPORT/@EXPORT
    private Integer id;
    private String title;
    private String cn;

    // @EXPORT
    private CaseType status;

    public String getDocumentType()
    {
        return "in";
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                status = CaseType.find(id);
                DSApi.initializeProxy(status);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
                addActionError("Nie podano tytu�u typu sprawy");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                	CaseType s = CaseType.find(id);
                    s.setTitle(title.trim());
                    s.setCn(TextUtils.trimmedStringOrNull(cn));
                }
                else
                {
                	CaseType status = new CaseType(title.trim());
                    status.setCn(TextUtils.trimmedStringOrNull(cn));
                    status.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public CaseType getStatus()
    {
        return status;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Integer getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }
}
