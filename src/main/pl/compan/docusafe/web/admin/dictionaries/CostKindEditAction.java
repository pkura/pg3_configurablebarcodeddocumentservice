package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.record.projects.CostKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class CostKindEditAction extends EventActionSupport
{
	
//  @IMPORT/@EXPORT
    private Integer id;
    private String title;
    private String refValue;
    private String cn;


    // @EXPORT
    private CostKind kind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                kind = CostKind.find(id);
                DSApi.initializeProxy(kind);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(title))
                addActionError("Nie podano nazwy kosztu");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                	CostKind kind = CostKind.find(id);
                    kind.setTitle(title.trim());
                    if (refValue!=null) {
                    kind.setRefValue(refValue.trim()); }
                    if (cn!=null) {
                    kind.setCn(cn.trim()); }
                }
                else
                {
                	CostKind kind = new CostKind(title.trim(), refValue != null ? refValue.trim(): null, cn != null ? cn.trim(): null);
                    kind.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    

    public void setId(Integer id)
    {
        this.id = id;
    }

    public CostKind getKind()
    {
        return kind;
    }

    public Integer getId()
    {
        return id;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
    
}