package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentKindsEditAction.java,v 1.7 2007/11/27 12:11:50 mariuszk Exp $
 */
public class InOfficeDocumentKindsEditAction extends EventActionSupport
{
    // @IMPORT/@EXPORT
    private Integer id;
    private String name;
    private Integer days;

    // @IMPORT
    private String requiredAttachmentTitle;
    private String[] attachmentSids;

    // @EXPORT
    private InOfficeDocumentKind kind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddRequiredAttachment").
            append(OpenHibernateSession.INSTANCE).
            append(new AddRequiredAttachment()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteRequiredAttachments").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteRequiredAttachments()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                kind = InOfficeDocumentKind.find(id);
                DSApi.initializeProxy(kind);
                DSApi.initializeProxy(kind.getRequiredAttachments());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy rodzaju pism");
            if (days == null)
                addActionError("Nie podano liczby dni na za�atwienie pisma");
            else if (days.intValue() < 0)
                addActionError("Liczba dni na za�atwienie nie powinna by� mniejsza od zera");
            else if (days.intValue() > 366)
                addActionError("Liczba dni na za�atwienie nie powinna by� wi�ksza ni� 366");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                    InOfficeDocumentKind kind = InOfficeDocumentKind.find(id);
                    kind.setName(name.trim());
                    kind.setDays(days.intValue());
                }
                else
                {
                    InOfficeDocumentKind kind = new InOfficeDocumentKind(name.trim(), days.intValue());
                    kind.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class AddRequiredAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(requiredAttachmentTitle))
                addActionError("Nie podano tytu�u wymaganego za��cznika");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(id);
                kind.addRequiredAttachment(requiredAttachmentTitle.trim());

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteRequiredAttachments implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (attachmentSids == null || attachmentSids.length == 0)
                addActionError("Nie zaznaczono za��cznik�w do usuni�cia");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                InOfficeDocumentKind kind = InOfficeDocumentKind.find(id);
                for (int i=0; i < attachmentSids.length; i++)
                {
                    kind.deleteRequiredAttachment(attachmentSids[i]);
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public InOfficeDocumentKind getKind()
    {
        return kind;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDays(Integer days)
    {
        this.days = days;
    }

    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Integer getDays()
    {
        return days;
    }

    public void setRequiredAttachmentTitle(String requiredAttachmentTitle)
    {
        this.requiredAttachmentTitle = requiredAttachmentTitle;
    }

    public void setAttachmentSids(String[] attachmentSids)
    {
        this.attachmentSids = attachmentSids;
    }
}
