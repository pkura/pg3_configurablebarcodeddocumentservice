package pl.compan.docusafe.web.admin.dictionaries;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.TopicFoundingSource;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class TopicFoundingSourceEditAction extends EventActionSupport{

	@Override
	protected void setup() {
		// TODO Auto-generated method stub
		
	}

	/*// @IMPORT/@EXPORT
    private Long id;
    private String name;
    private String manager;
    private List<DSUser> managers;
    private String supervisor;
    private List<DSUser> supervisors;
    
    // @EXPORT
    private TopicFoundingSource topic;
    
	protected void setup() {
		 FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).h
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		
	}

	 private class FillForm implements ActionListener
	 {
	        public void actionPerformed(ActionEvent event)
	        {
	        	try {
					managers = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
					supervisors = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				} catch (EdmException e1) {
					System.out.println(e1.getMessage());
				}
	            if (id == null)
	                return;

	            try
	            {
	                topic = TopicFoundingSource.find(id);
	                name = topic.getTitle();
	                manager = topic.getManager();
	                supervisor = topic.getSupervisor();
	            }
	            catch (EdmException e)
	            {
	                addActionError(e.getMessage());
	            }
	        }
	}

	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
           
            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                	TopicFoundingSource topic = TopicFoundingSource.find(id);
                    topic.setTitle(name.trim());
                    topic.setManager(manager);
                    topic.setSupervisor(supervisor);
                }
                else
                {
                	TopicFoundingSource topic = new TopicFoundingSource(name.trim(),manager, supervisor);
                	topic.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TopicFoundingSource getTopic() {
		return topic;
	}

	public void setTopic(TopicFoundingSource topic) {
		this.topic = topic;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public List<DSUser> getManagers() {
		return managers;
	}

	public void setManagers(List<DSUser> managers) {
		this.managers = managers;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public List<DSUser> getSupervisors() {
		return supervisors;
	}

	public void setSupervisors(List<DSUser> supervisors) {
		this.supervisors = supervisors;
	}
*/
}
