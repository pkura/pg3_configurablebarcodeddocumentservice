package pl.compan.docusafe.web.admin.dictionaries;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.TopicFoundingSource;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class TopicFoundingSourceAction extends EventActionSupport{

	// @EXPORT
    private List<TopicFoundingSource> topics;

    // @IMPORT
    private Integer[] topicIds;

	protected void setup() {
		 FillForm fillForm = new FillForm();

	        registerListener(DEFAULT_ACTION).
	            append(OpenHibernateSession.INSTANCE).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);

	        registerListener("doDelete").
	            append(OpenHibernateSession.INSTANCE).
	            append(new Delete()).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);
		
	}

	private class FillForm implements ActionListener
	 {
		 public void actionPerformed(ActionEvent event)
	     {
			 try {
				topics = TopicFoundingSource.list();
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     }
	 }
	
	 private class Delete implements ActionListener
	    {
	        public void actionPerformed(ActionEvent event)
	        {
	        }
	    }

	public List<TopicFoundingSource> getTopics() {
		return topics;
	}

	public void setTopics(List<TopicFoundingSource> topics) {
		this.topics = topics;
	}

	public Integer[] getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(Integer[] topicIds) {
		this.topicIds = topicIds;
	}

}
