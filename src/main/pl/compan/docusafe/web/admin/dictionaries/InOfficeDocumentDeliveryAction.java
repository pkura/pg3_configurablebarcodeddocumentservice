package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentDeliveryAction.java,v 1.4 2006/02/20 15:42:33 lk Exp $
 */
public class InOfficeDocumentDeliveryAction extends OfficeDocumentDeliveryAction
{
    // @EXPORT
    private List<Map> deliveries;

    // @IMPORT
    private Integer[] deliveryIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    public String getDocumentType()
    {
        return "in";
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                deliveries = makeDeliveryBeans(InOfficeDocumentDelivery.list());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deliveryIds == null || deliveryIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                DSApi.context().begin();

                for (int i=0; i < deliveryIds.length; i++)
                {
                    InOfficeDocumentDelivery d = InOfficeDocumentDelivery.find(deliveryIds[i]);
                    addActionMessage("Usuni�to '"+d.getName()+"'");
                    d.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public void setDeliveryIds(Integer[] deliveryIds)
    {
        this.deliveryIds = deliveryIds;
    }
}
