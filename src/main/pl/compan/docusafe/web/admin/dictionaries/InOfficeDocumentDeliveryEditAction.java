package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.webwork.event.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: InOfficeDocumentDeliveryEditAction.java,v 1.3 2006/02/20 15:42:33 lk Exp $
 */
public class InOfficeDocumentDeliveryEditAction extends EventActionSupport
{
    // @IMPORT/@EXPORT
    private Integer id;
    private String name;

    // @EXPORT
    private InOfficeDocumentDelivery delivery;

    public String getDocumentType()
    {
        return "in";
    }

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                delivery = InOfficeDocumentDelivery.find(id);
                DSApi.initializeProxy(delivery);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy sposobu dostarczenia");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                    InOfficeDocumentDelivery.find(id).setName(name.trim());
                }
                else
                {
                    InOfficeDocumentDelivery delivery = new InOfficeDocumentDelivery(name.trim());
                    delivery.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public InOfficeDocumentDelivery getDelivery()
    {
        return delivery;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
