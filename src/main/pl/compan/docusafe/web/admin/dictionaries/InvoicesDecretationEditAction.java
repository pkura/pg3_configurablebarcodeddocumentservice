package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InvoicesDecretation;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class InvoicesDecretationEditAction extends EventActionSupport
{
//  @IMPORT/@EXPORT
    private Integer id;
    private String name;

    // @IMPORT

    // @EXPORT
    private InvoicesDecretation kind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                kind = InvoicesDecretation.find(id);
                DSApi.initializeProxy(kind);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy dekretacji");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                	InvoicesDecretation kind = InvoicesDecretation.find(id);
                    kind.setName(name.trim());
                }
                else
                {
                	InvoicesDecretation kind = new InvoicesDecretation(name.trim());
                    kind.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    

    public void setId(Integer id)
    {
        this.id = id;
    }

    public InvoicesDecretation getKind()
    {
        return kind;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
