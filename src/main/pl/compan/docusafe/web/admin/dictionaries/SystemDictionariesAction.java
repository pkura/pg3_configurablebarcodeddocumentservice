package pl.compan.docusafe.web.admin.dictionaries;
import java.util.List;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.EditableDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import static pl.compan.docusafe.webwork.event.SessionControl.*;

/**
 *
 * @author Micha� Sankowski
 */
public class SystemDictionariesAction extends EventActionSupport{

	private final static Logger LOG = LoggerFactory.getLogger(SystemDictionariesAction.class);
	
	private List<EditableDictionary> dictionaries;

	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OPEN_HIBERNATE_SESSION).
            append(fillForm).
            appendFinally(CLOSE_HIBERNATE_SESSION);
	}

	private class FillForm extends LoggedActionListener{
		@Override
		public void actionPerformed(ActionEvent event, Logger log) throws Exception {
			dictionaries = DictionaryDictionary.INSTANCE.getAll();
            log.info("dictionaries size -> {}", dictionaries.size());
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}

	public List<EditableDictionary> getDictionaries() {
		return dictionaries;
	}

	public void setDictionaries(List<EditableDictionary> dictionaries) {
		this.dictionaries = dictionaries;
	}
}
