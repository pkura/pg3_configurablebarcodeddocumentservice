package pl.compan.docusafe.web.admin.dictionaries;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DockindDictionaries 
{
	public static Logger log = LoggerFactory.getLogger(DockindDictionaries.class);
	private Integer id;
	private String cn;
	private String title;
	private Integer centrum;
	private String refValue;

    private boolean available = true;
	
	public DockindDictionaries(Integer id, String cn, String title) {
		super();
		this.id = id;
		this.cn = cn;
		this.title = title;
	}
	
	public DockindDictionaries(Integer id, String cn, String title, Integer centurm, String refValue) 
	{
		super();
		this.id = id;
		this.cn = cn;
		this.title = title;
		this.centrum = centurm;
		this.refValue = refValue;
	}
	public DockindDictionaries(Integer id, String cn, String title, Integer centurm, String refValue,boolean available) 
	{
		super();
		this.id = id;
		this.cn = cn;
		this.title = title;
		this.centrum = centurm;
		this.refValue = refValue;
		this.available = available;
	}
	
    /**
     * Usuwa wpis
     * @param tableName
     * @param id
     * @throws EdmException 
     */    
    public static void delete(String tableName,int id) throws EdmException{
        PreparedStatement ps = null;
        try {
	    	ps = DSApi.context().prepareStatement("delete from " + tableName + " where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
	    } catch(SQLException ex){
            throw new EdmException(ex);
        } finally {
			DSApi.context().closeStatement(ps);
		}
    }
    
    public static void updateAvailability(String tableName, int id, boolean newValue) throws EdmException{
        PreparedStatement ps = null;
        try {
	    	ps = DSApi.context().prepareStatement("update " + tableName + " set available = ? where id = ?");
            ps.setBoolean(1, newValue);
            ps.setInt(2, id);
            ps.executeUpdate();
	    } catch(SQLException ex){
            throw new EdmException(ex);
        } finally {
			DSApi.context().closeStatement(ps);
		}
    }

	public static List<DockindDictionaries> list(String tableName) throws EdmException, HibernateException {
        return listFromQuery(tableQuery(tableName));
	}

    public static List<DockindDictionaries> list(String tableName, int firstId) throws EdmException, HibernateException {
        return listFromQuery(tableQueryFirstId(tableName, firstId));
    }

    private static List<DockindDictionaries> listFromQuery(String sql) throws EdmException {
        List<DockindDictionaries> list = new ArrayList<DockindDictionaries>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(sql);

            rs = ps.executeQuery();
            while (rs.next()) {
                DockindDictionaries dd = new DockindDictionaries(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5));
                dd.available = rs.getBoolean(6);
                list.add(dd);
            }
            return list;
        } catch (SQLException ex){
            throw new EdmSQLException(ex);
        } finally {
            if (rs != null) try {
                rs.close();
            } catch (SQLException e) {
                log.debug(e.getMessage(), e);
            }
            DSApi.context().closeStatement(ps);
        }
    }

    private static String tableQuery(String tableName) {
        return "select id,cn,title,centrum,refValue,available from "+tableName+" order by title";
    }

    private static String tableQueryFirstId(String tableName, int firstId){
        return "select id,cn,title,centrum,refValue,available from "+tableName+" where id >=" +firstId;
    }

    public static DockindDictionaries find(Integer id,String tableName) throws EdmException, HibernateException, SQLException
	{
		ResultSet rs = null;
		PreparedStatement ps = null;
		try
		{
	    	ps = DSApi.context().prepareStatement("select id,cn,title,centrum,refValue,available from "+tableName+ " where id = ?");
			ps.setInt(1, id);
	    	rs = ps.executeQuery();
			while (rs.next())
			{
				return new DockindDictionaries(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getString(5));			
			}
			
	    }
	    catch (EdmException e)
	    {
	        new EdmException("TODO:BLAD", e);
	    }
	    finally
		{
			if(rs != null)try{rs.close();} catch(SQLException e){LogFactory.getLog("eprint").debug("", e);}
			DSApi.context().closeStatement(ps);
		}
		return null;
	}
	
	public void create(String tableName) throws EdmException, HibernateException, SQLException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if(DSApi.isOracleServer()) {
				ps = DSApi.context().prepareStatement("insert into "+ tableName+" (id,cn,title,centrum,refValue,available)values("+tableName+"_ID.nextval,?,?,?,?,?)");
            } else {
				ps = DSApi.context().prepareStatement("insert into "+ tableName+" (cn,title,centrum,refValue,available)values(?,?,?,?,?)");
            }
	    	ps.setString(1, cn);
	    	ps.setString(2, StringUtils.left(title, 200));
	    	if(centrum != null) {
	    		ps.setInt(3, centrum);
            } else {
	    		ps.setObject(3, null);
            }
	    	ps.setString(4, refValue);
            ps.setBoolean(5, available);
			ps.execute();
			ps.close();

            DSApi.context().closeStatement(ps);
			if(this.id == null && DSApi.isSqlServer()) {
				ps = DSApi.context().prepareStatement("select @@IDENTITY as id");
				rs = ps.executeQuery();
				while(rs.next()) {
					this.id = rs.getInt(1);
				}
				rs.close();
			}
            DataBaseEnumField.reloadForTable(tableName);
	    }
	    finally
		{
	    	DSApi.context().closeStatement(ps);
		}
	}
	
	public void update(String tableName)
	{
		PreparedStatement ps = null;
		try
		{
			ps =DSApi.context().prepareStatement(
					"update "+tableName+" set cn = ?, title = ?, centrum = ?, refValue = ?, available = ? where id = ?");
			ps.setString(1, cn);
	    	ps.setString(2, StringUtils.left(title, 200));
	    	if(centrum != null)
	    		ps.setInt(3, centrum);
	    	else
	    		ps.setObject(3, null);
	    	ps.setString(4, refValue);
            ps.setBoolean(5, available);
	    	ps.setInt(6, id);
			ps.execute();
		}
		catch (HibernateException e) 
		{
			new EdmException(e.getMessage());
			log.error("",e);
		}
		catch (SQLException e) 
		{
			new EdmException(e.getMessage());
			log.error("DockindDictionaries.java:"+e,e);
		} 
		catch (EdmException e)
		{
			new EdmException(e.getMessage());
			log.error("DockindDictionaries.java:"+e,e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}
	
	public static class TitleComparator implements Comparator<DockindDictionaries>
    {
    	private static final Collator COLLATOR = Collator.getInstance(new Locale("PL","pl"));
    	
        public int compare(DockindDictionaries e1, DockindDictionaries e2)
        {
            COLLATOR.setStrength(Collator.SECONDARY);
        	return COLLATOR.compare(e1.getTitle(), e2.getTitle());        	
        }
    }
	
	
	public DockindDictionaries()
	{
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
