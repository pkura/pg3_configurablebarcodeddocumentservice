package pl.compan.docusafe.web.admin.dictionaries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.sql.DataSource;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ExternalDictionariesAction extends EventActionSupport{

	
	private String tableName;
	private String primaryKeyColumn;
	private Map<Integer, String> columnIdxToName = new HashMap<Integer, String>();
	private Map<Integer, List<String>> columnIdxToDBTypeAndPrecision = new HashMap<Integer, List<String>>();
	private List<Integer> selectedColumnsIds = new ArrayList<Integer>();
	private Map<String, String> columnOriginalNameToNewCustomName = new HashMap<String, String>();
	private List<String> dbConnectionsList;
	private String choosenDBconnection;
	private String actionType;
	private final String ACTION_CREATE = "CREATE";
	private final String ACTION_MODIFY = "MODIFY";
	private Map<String, String> availableDockinds;
	private List<String> availableExternalDictionaries;
	private String choosenDockind;
	private String dictionaryName;
	private String choosenExternalDictionary;
	private static Map<String, String> dbToDSType;
	static{
		dbToDSType = new HashMap<String,String>();
		dbToDSType.put("varchar", "string");
		dbToDSType.put("numeric", "integer");
		dbToDSType.put("numeric identity", "integer");
		dbToDSType.put("int", "integer");
		dbToDSType.put("smallint", "bool");
		dbToDSType.put("datetime", "timestamp");
	}
	
	@Override
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreateExternalDictionary").
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        append(new CreateExternalDictionary()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doModifyDockind").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new ModifyDockind()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
//            	gdy nie pobrano informacji o dostepnych bazach i nie zaznaczono zadnego z dostepnych polaczen oraz nie wybrano tabeli, z ktorej beda pobierane informacje o kolumnach do dockinda
            	if(dbConnectionsList == null && choosenDBconnection == null && columnIdxToName.size() == 0 && tableName == null){
	            	Context initCtx = new InitialContext();
	            	Context jdbcCtx = (Context) initCtx.lookup("java:comp/env"); 
	            	dbConnectionsList = new ArrayList<String>();
	            	Binding binding = null;
	            	NamingEnumeration<Binding> bindingList = jdbcCtx.listBindings("jdbc");
	            	while(bindingList.hasMore()){
	            		binding = bindingList.next();
	            		dbConnectionsList.add(binding.getName());
	            	}
	            	jdbcCtx.listBindings("jdbc").close();
            	}else if(choosenDBconnection != null){
	            	ServletActionContext.getRequest().getSession().setAttribute("choosenDBconnection", choosenDBconnection);
            	}
            }
            catch (Exception e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
    }
	
	private class CreateExternalDictionary implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent event){
			PreparedStatement ps = null;
        	ResultSet rs = null;
        	String connectionName = null;
			try{
            	actionType = ACTION_CREATE;
				if(tableName != null && columnIdxToName.size() == 0){
					Context initCtx = new InitialContext();
					connectionName = (String)ServletActionContext.getRequest().getSession().getAttribute("choosenDBconnection");
	            	DataSource dataSource = (DataSource) initCtx.lookup("java:comp/env/jdbc/" + connectionName);
	            	Connection connection = dataSource.getConnection();
//        	    	wypisanie nazwy kolumny klucza g��wnego danej tabeli
	        		ps = connection.prepareStatement(
	         	    		" SELECT column_name" + 
	         	    		" FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + 
	         	    		" WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1" + 
	         	    		" AND table_name = '" + tableName.trim() + "'");
	        		rs = ps.executeQuery();
	        		if(rs.next())	primaryKeyColumn = rs.getString(1);
// 					pobranie informacji o kolumnach wystepujacych w zapytaniu
	        		ps = connection.prepareStatement("SELECT TOP 1 * FROM " + tableName.trim());
	        	    rs = ps.executeQuery();
	        	    int numberOfColumns = rs.getMetaData().getColumnCount();
	        	    int columnIdx = 1;
	        	    List<String> columnTypeAndPrecision;
	        	    while(columnIdx <= numberOfColumns){
//            	    	mapowanie id kolumny do jej nazwy
	        	    	columnIdxToName.put(columnIdx, rs.getMetaData().getColumnName(columnIdx));
	        	    	columnTypeAndPrecision = new ArrayList<String>();
	        	    	columnTypeAndPrecision.add(rs.getMetaData().getColumnTypeName(columnIdx));
	        	    	columnTypeAndPrecision.add(String.valueOf(rs.getMetaData().getPrecision(columnIdx)));
//	        	    	mapowanie id kolumny do jej typu i precyzji 
	        	    	columnIdxToDBTypeAndPrecision.put(columnIdx, columnTypeAndPrecision);
	        	    	++columnIdx;
	        	    }
	        	    ServletActionContext.getRequest().getSession().setAttribute("columnIdToOriginalName", columnIdxToName);
	        	    ServletActionContext.getRequest().getSession().setAttribute("columnIdxToDBTypeAndPrecision", columnIdxToDBTypeAndPrecision);
	    		}else if(columnIdxToName.size() > 0){
	    			if(!isDictionaryNameUnique(dictionaryName))
	    				throw new EdmException("Ju� istnieje s�ownik o nazwie " + dictionaryName);
//	    			pozyskanie id kolumn wraz z ich oryginalnymi nazwami
	    			Map<Integer, String> columnIdToOriginalName = (Map<Integer, String>) ServletActionContext.getRequest().getSession().getAttribute("columnIdToOriginalName");
	    			columnIdxToDBTypeAndPrecision = (Map<Integer, List<String>>) ServletActionContext.getRequest().getSession().getAttribute("columnIdxToDBTypeAndPrecision");
	    			connectionName = (String)ServletActionContext.getRequest().getSession().getAttribute("choosenDBconnection");
	    			String sqlQuery = "";
	        		for(Integer columnId : columnIdxToName.keySet()){
	        			if(selectedColumnsIds.contains(String.valueOf(columnId))){
	        				if(columnIdxToName.get(columnId) == null)
	        					throw new EdmException("Nie wprowadzono wszystkich nazw dla wybranych kolumn");
		        			sqlQuery += 
		        					" INSERT INTO " + "dsg_external_dictionaries" +
		        					" VALUES (" + 
		        					" '" + connectionName + "'" +
		        					", '" + tableName + "'" + 
		        					", '" + dictionaryName + "'" + 
		        					", '" + columnIdToOriginalName.get(columnId) + "'" +
		        					", '" + columnIdxToName.get(columnId) + "'" + 
		        					", '" + columnIdxToDBTypeAndPrecision.get(columnId).get(0) + "'" + 
		        					", '" + columnIdxToDBTypeAndPrecision.get(columnId).get(1) + "'" + 
		        					")\n";
	        			}
	        		}
	        		ps = DSApi.context().prepareStatement(sqlQuery);
	        		ps.execute();
	        		addActionMessage("Dodano nowy s�ownik zewn�trzny");
	    		}
			}catch (SQLException e){
				LogFactory.getLog("eprint").debug("", e);
				if(e.getMessage().contains("Invalid object name"))
					addActionError("Tabela o nazwie " + tableName + " nie istnieje w wybranej bazie");
				else
					addActionError(e.getLocalizedMessage());
		    }catch (Exception e){
		    	LogFactory.getLog("eprint").debug("", e);
		    	if(e.getMessage() == null)
		    		addActionError("Wyst�pi� b��d w trakcie przetwarzania danych");
		    	else
		    		addActionError(e.getMessage());
		    }finally{
		    	if(hasActionErrors())	actionType = null;
		    }
		}
		
		public boolean isDictionaryNameUnique(String dictionaryName) throws SQLException, EdmException{
			String sqlQuery = "SELECT DISTINCT nazwa_slownika FROM dsg_external_dictionaries";
			PreparedStatement ps = DSApi.context().prepareStatement(sqlQuery);
    		ResultSet rs = ps.executeQuery();
    		while(rs.next()){
    			if(rs.getString("nazwa_slownika").trim().equals(dictionaryName.trim()))
    				return false;
			}
    		return true;
		}
	}
	
	private class ModifyDockind implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent event) {
			PreparedStatement ps = null;
        	ResultSet rs = null;	
			try{
				actionType = ACTION_MODIFY;
				if(choosenExternalDictionary == null && choosenDockind == null){
					ps = DSApi.context().prepareStatement("SELECT nazwa_slownika from dsg_external_dictionaries GROUP BY nazwa_slownika");
					rs = ps.executeQuery();
					availableExternalDictionaries = new ArrayList<String>();
					
					while(rs.next()){
						availableExternalDictionaries.add(rs.getString(1));
					}
	//        	    pobranie listy dost�pnych dockind�w
					availableDockinds = new HashMap<String, String>();
	    			for(DocumentKind docKind : DocumentKindProvider.get().forManualCreate().visible().type(DocumentType.INCOMING).list()){
	    				availableDockinds.put(docKind.getCn(), docKind.getName());
	    			}
				}else if(choosenExternalDictionary != null && choosenDockind != null){
					ps = DSApi.context().prepareStatement("SELECT * from dsg_external_dictionaries WHERE nazwa_slownika = '" + choosenExternalDictionary + "'");
					rs = ps.executeQuery();
					DocumentKind documentKind = DocumentKind.findByCn(choosenDockind);
//					File file = new File("C:\\KrzysiekM\\Docusafe\\etc\\dockind\\IFPAN\\normal.xml");
//			        documentKind.saveContent(file);
//			        documentKind.resetDockindInfo();
					Document documentKindXML = documentKind.getXML();
					Element dockindFieldElement = documentKindXML.getDocument().getRootElement().element("fields");
					List<String> dockindFieldData;
					Map<String, List<String>> columnNameToData = new HashMap<String, List<String>>();
					String tableName = null;
					String resourceName = null;
					while(rs.next()){
						if(tableName == null)	tableName = rs.getString("nazwa_tabeli");
						if(resourceName == null) resourceName = rs.getString("nazwa_polaczenia");
						dockindFieldData = new ArrayList<String>();
						dockindFieldData.add(rs.getString("nazwa_kolumny_uzytkownika"));
						dockindFieldData.add(rs.getString("precyzja_kolumny"));
						dockindFieldData.add(rs.getString("typ_kolumny"));
						columnNameToData.put(rs.getString("nazwa_kolumny_oryginalna").toUpperCase(), dockindFieldData);
					}
					Element dictionaryField = createDictionaryField(dockindFieldElement, choosenExternalDictionary, columnNameToData.keySet(), tableName, resourceName);
					createDictionaryContent(dictionaryField, columnNameToData);
					persistDockindXML(documentKind, documentKindXML);
					addActionMessage("Pomy�lnie zaktualizowano wskazany typ dokumentu");
				}
			}catch (Exception e){
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
		
		private void createDictionaryContent(Element dictionaryField, Map<String, List<String>> dictionaryFieldsData) throws EdmException{
			String tableColumnName;
			String userCustomColumnName;
			String columnPrecision;
			String columnType;
			int dictionaryId = Integer.parseInt(dictionaryField.attributeValue("id")) * 10;
			for(String columnName : dictionaryFieldsData.keySet()){
				tableColumnName = columnName;
				userCustomColumnName = dictionaryFieldsData.get(columnName).get(0);
				columnPrecision = dictionaryFieldsData.get(columnName).get(1); 
				columnType = dictionaryFieldsData.get(columnName).get(2);
				String fieldColumnType = dbToDSType.get(columnType);
				if(fieldColumnType == null)
					throw new EdmException("Brak definicji dla typu " + columnType + " kolumny " + tableColumnName + " wybranego s�ownika");
				Element type = dictionaryField.element("type").addElement("field")
								.addAttribute("id", String.valueOf(++dictionaryId))
								.addAttribute("cn", tableColumnName.toUpperCase())
								.addAttribute("column", tableColumnName)
								.addAttribute("name", userCustomColumnName)
								.addElement("type");
				type.addAttribute("name", fieldColumnType)
					.addAttribute("match", "context");
				if(fieldColumnType.equals("string"))
					type.addAttribute("length", columnPrecision);
			}
		}
		
		
		private Element createDictionaryField(Element dockindFieldElement, String dictionaryName, Set<String> fieldsCns, String tableName, String resourceName) throws EdmException{
//			sprawdzamy czy slownik juz istnieje w dockindzie, jesli tak wyrzucamy blad
			Iterator it = dockindFieldElement.elementIterator();
			Element dictionaryField = null;
			int lastFieldIdx = -1;
			int currentIdx;
			while (it.hasNext()) {
				dictionaryField = (Element) it.next();
				currentIdx = Integer.parseInt(dictionaryField.attributeValue("id"));
				if (lastFieldIdx < currentIdx)
					lastFieldIdx = currentIdx;
				if (dictionaryField.attributeValue("name") != null && dictionaryField.attributeValue("name").trim().equals(dictionaryName.trim())){
					throw new EdmException("Ju� istnieje s�ownik o nazwie " + dictionaryName);
				}
			}
//			jesli nie naleziono dockindu o podanej nazwie
			dictionaryField = dockindFieldElement.addElement("field")
							.addAttribute("id", String.valueOf(++lastFieldIdx))
							.addAttribute("cn", dictionaryName.trim().toUpperCase())
							.addAttribute("column", dictionaryName.trim().toLowerCase())
							.addAttribute("name", dictionaryName.trim())
							.addAttribute("cantUpdate", "true");
//			definicja i deklaracja element�w s�ownika
			String fieldsCnsString = fieldsCns.toString().substring(1,fieldsCns.toString().length()-1);
			dictionaryField.addElement("type")
				.addAttribute("name", "dictionary")
				.addAttribute("tableName", tableName)
				.addAttribute("resourceName", resourceName)
				.addAttribute("dwr-column", "1") // decyduje o polozeniu slownika w edytorze tworzenia dokumentu wart. 0/1/2 - nie tego atrybutu slownik nie zostnie wyswietlony
				.addAttribute("dwr-fieldset", "druga") // decyduje o polozeniu slownika w obrebie wybranego dwr-column wart. ?
				.addAttribute("visible-fields", fieldsCnsString)
				.addAttribute("pop-up-fields", fieldsCnsString)
				.addAttribute("prompt-fields", fieldsCnsString)
				.addAttribute("search-fields", fieldsCnsString)
				.addAttribute("pop-up-buttons", "doSelect,doClear")	
				.addAttribute("dockind-search-fields", fieldsCnsString);	

			return dictionaryField;
		}
		
		private void persistDockindXML(DocumentKind documentKind, Document documentKindXML) throws IOException, EdmException{
//			File file = new File("C:\\KrzysiekM\\Docusafe\\etc\\dockind\\IFPAN\\normal.xml");
//	        documentKind.saveContent(file);
//	        documentKind.resetDockindInfo();
	        OutputFormat format = OutputFormat.createPrettyPrint();
	        XMLWriter writer;
	        File tempFile = new File("./temp.xml");
			writer = new XMLWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile),"UTF8")), format);
	        writer.write(documentKindXML);
	        writer.close();
	        documentKind.saveContent(tempFile);
	        documentKind.resetDockindInfo();
	        tempFile.delete();
		}
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<Integer> getSelectedColumnsIds() {
		return selectedColumnsIds;
	}

	public void setSelectedColumnsIds(List<Integer> selectedColumnsIds) {
		this.selectedColumnsIds = selectedColumnsIds;
	}

	public Map<Integer, String> getColumnIdxToName() {
		return columnIdxToName;
	}

	public void setColumnIdxToName(Map<Integer, String> columnIdxToName) {
		this.columnIdxToName = columnIdxToName;
	}

	public Map<String, String> getColumnOriginalNameToNewCustomName() {
		return columnOriginalNameToNewCustomName;
	}

	public void setColumnOriginalNameToNewCustomName(
			Map<String, String> columnOriginalNameToNewCustomName) {
		this.columnOriginalNameToNewCustomName = columnOriginalNameToNewCustomName;
	}

	public String getPrimaryKeyColumn() {
		return primaryKeyColumn;
	}

	public void setPrimaryKeyColumn(String primaryKeyColumn) {
		this.primaryKeyColumn = primaryKeyColumn;
	}

	public List<String> getDbConnectionsList() {
		return dbConnectionsList;
	}

	public void setDbConnectionsList(List<String> dbConnectionsList) {
		this.dbConnectionsList = dbConnectionsList;
	}

	public String getChoosenDBconnection() {
		return choosenDBconnection;
	}

	public void setChoosenDBconnection(String choosenDBconnection) {
		this.choosenDBconnection = choosenDBconnection;
	}
	
	public Map<String, String> getAvailableDockinds() {
		return availableDockinds;
	}

	public void setAvailableDockinds(Map<String, String> availableDockinds) {
		this.availableDockinds = availableDockinds;
	}
	
	public String getChoosenDockind() {
		return choosenDockind;
	}

	public void setChoosenDockind(String choosenDockind) {
		this.choosenDockind = choosenDockind;
	}

	public String getDictionaryName() {
		return dictionaryName;
	}

	public void setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public List<String> getAvailableExternalDictionaries() {
		return availableExternalDictionaries;
	}

	public void setAvailableExternalDictionaries(
			List<String> availableExternalDictionaries) {
		this.availableExternalDictionaries = availableExternalDictionaries;
	}

	public String getChoosenExternalDictionary() {
		return choosenExternalDictionary;
	}

	public void setChoosenExternalDictionary(String choosenExternalDictionary) {
		this.choosenExternalDictionary = choosenExternalDictionary;
	}
	
}


/*package pl.compan.docusafe.web.admin.dictionaries;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.sql.DataSource;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs.FileUtil;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class OutgoingDictionariesAction extends EventActionSupport{

	
	private String tableName;
	private String primaryKeyColumn;
	private Map<Integer, String> columnIdxToName = new HashMap<Integer, String>();
	private Map<Integer, List<String>> columnIdxToDBTypeAndPrecision = new HashMap<Integer, List<String>>();
	private List<String> columnsUserFriendlyNames;
	private int [] selectedColumnsIds;
	private Map<String, String> columnOriginalNameToNewCustomName = new HashMap<String, String>();
	private List<String> dbConnectionsList;
	private String choosenDBconnection;
	private int actionState;
	private List<String> availableDockinds;
	private List<String> availableExternalDictionaries;
	private String choosenDockind;
	private String dictionaryName;
	private String choosenExternalDictionary;
	@Override
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doModifyDockind").
            append(OpenHibernateSession.INSTANCE).
            append(new ModifyDockind()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
//            	gdy nie pobrano informacji o dostepnych bazach i nie zaznaczono zadnego z dostepnych polaczen oraz nie wybrano tabeli, z kt�rej b�d� pobierane informacje o kolumnach do dockinda
            	if(dbConnectionsList == null && tableName == null && columnIdxToName.size() == 0){
//            		ServletActionContext.getRequest().getSession().setAttribute("choosenDBconnection", choosenDBconnection);
	            	// Obtain our environment naming context
	            	Context initCtx = new InitialContext();
	            	Context jdbcCtx = (Context) initCtx.lookup("java:comp/env"); 
	            	dbConnectionsList = new ArrayList<String>();
	            	Binding binding = null;
	            	NamingEnumeration<Binding> bindingList = jdbcCtx.listBindings("jdbc");
	            	while(bindingList.hasMore()){
	            		binding = bindingList.next();
	            		dbConnectionsList.add(binding.getName());
	            	}
	            	jdbcCtx.listBindings("jdbc").close();
            	}else 
            		if(choosenDBconnection != null && tableName != null){
//            			choosenDBconnection = (String) ServletActionContext.getRequest().getSession().getAttribute("choosenDBconnection");
	            		Context initCtx = new InitialContext();
		            	DataSource ds = (DataSource) initCtx.lookup("java:comp/env/jdbc/" + choosenDBconnection);
		            	Connection conn = ds.getConnection();
		            	PreparedStatement ps = null;
		            	ResultSet rs = null;
	//            		wypisanie nazwy kolumny klucza g��wnego danej tabeli
	            		ps = conn.prepareStatement(
	             	    		" SELECT column_name" + 
	             	    		" FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + 
	             	    		" WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1" + 
	             	    		" AND table_name = '" + tableName.trim() + "'");
	            		rs = ps.executeQuery();
	            		if(rs.next())	primaryKeyColumn = rs.getString(1);
	 					System.out.println("PK: " + primaryKeyColumn);
	// 					pobranie informacji o kolumnach wystepujacych w zapytaniu
	            		ps = conn.prepareStatement("SELECT TOP 1 * FROM " + tableName.trim());
	            	    rs = ps.executeQuery();
	            	    int numberOfColumns = rs.getMetaData().getColumnCount();
	            	    int columnIdx = 1;
	            	    List<String> columnTypeAndPrecision;
	            	    while(columnIdx <= numberOfColumns){
	//            	    	mapowanie id kolumny do jej nazwy
	            	    	columnIdxToName.put(
	            	    			columnIdx,
	            	    			rs.getMetaData().getColumnName(columnIdx)
	            	    	);
	            	    	columnTypeAndPrecision = new ArrayList<String>();
	            	    	columnTypeAndPrecision.add(rs.getMetaData().getColumnTypeName(columnIdx));
	            	    	columnTypeAndPrecision.add(String.valueOf(rs.getMetaData().getPrecision(columnIdx)));
	            	    	columnIdxToDBTypeAndPrecision.put(
	            	    			columnIdx,
	            	    			columnTypeAndPrecision
	            	    	);
	            	    	++columnIdx;
	            	    }
	            	    conn.close();
//	            	    rs.getMetaData().getPrecision(column)
	            	    ServletActionContext.getRequest().getSession().setAttribute("columnIdToOriginalName", columnIdxToName);
	            	    ServletActionContext.getRequest().getSession().setAttribute("columnIdxToDBTypeAndPrecision", columnIdxToDBTypeAndPrecision);
	            	    ServletActionContext.getRequest().getSession().setAttribute("DataSource", ds);
            		}else if(dictionaryName != null){
            			
            			Map<Integer, String> columnIdToOriginalName = (Map<Integer, String>) ServletActionContext.getRequest().getSession().getAttribute("columnIdToOriginalName");
            			columnIdxToDBTypeAndPrecision = (Map<Integer, List<String>>) ServletActionContext.getRequest().getSession().getAttribute("columnIdxToDBTypeAndPrecision");
            			DataSource ds = (DataSource) ServletActionContext.getRequest().getSession().getAttribute("DataSource");
            			String sqlQuery = "";
	            		for(Integer columnId : columnIdxToName.keySet()){
	            			if(columnIdxToName.get(columnId) != null)
	            			sqlQuery += 
	            					" INSERT INTO " + "dsg_external_dictionaries" +
	            					" VALUES (" + 
	            					" '" + dictionaryName + "'" + 
	            					", '" + columnIdToOriginalName.get(columnId) + "'" +
	            					", '" + columnIdxToName.get(columnId) + "'" + 
	            					", '" + columnIdxToDBTypeAndPrecision.get(columnId).get(0) + "'" + 
	            					", '" + columnIdxToDBTypeAndPrecision.get(columnId).get(1) + "'" + 
	            					")\n";
	            		}
	            		PreparedStatement ps = ds.getConnection().prepareStatement(sqlQuery);
	            		ps.execute();
	            		addActionMessage("Dodano nowy s�ownik zewn�trzny");
            		}
            }
            catch (Exception e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
    }
	
	private class ModifyDockind implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			try{
				if(choosenExternalDictionary == null && choosenDockind == null){
					PreparedStatement ps = DSApi.context().prepareStatement("SELECT nazwa_slownika from dsg_external_dictionaries GROUP BY nazwa_slownika");
					ResultSet rs = ps.executeQuery();
					availableExternalDictionaries = new ArrayList<String>();
					
					while(rs.next()){
						availableExternalDictionaries.add(rs.getString(1));
					}
	//        	    pobranie listy dost�pnych dockind�w
	        	    availableDockinds = new ArrayList<String>();
	    			for(String dockindName : DocumentKind.getAvailableDocumentKindsCn()){
	    				availableDockinds.add(dockindName);
	    			}
				}else if(choosenExternalDictionary != null && choosenDockind != null){
					PreparedStatement ps = DSApi.context().prepareStatement("SELECT * from dsg_external_dictionaries WHERE nazwa_slownika = '" + choosenExternalDictionary + "'");
					ResultSet rs = ps.executeQuery();
					DocumentKind documentKind = DocumentKind.findByCn(choosenDockind);
					Document documentKindXML = documentKind.getXML();
					Element dockindFieldElement = documentKindXML.getDocument().getRootElement().element("fields");
					List<String> dockindFieldData;
					while(rs.next()){
						dockindFieldData = new ArrayList<String>();
						dockindFieldData.add(rs.getString("nazwa_kolumny_oryginalna"));
						dockindFieldData.add(rs.getString("nazwa_kolumny_uzytkownika"));
						dockindFieldData.add(rs.getString("precyzja_kolumny"));
						dockindFieldData.add(rs.getString("typ_kolumny"));
						updateDockind(dockindFieldElement, dockindFieldData);
					}
					
//					DocumentKind.getDockindInfo(choosenDockind).setXml(documentKindXML); 
					persistDockindXML(documentKind, documentKindXML);
					addActionMessage("Pomy�lnie zaktualizowano");
				}
			}catch (Exception e){
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
		
		private void updateDockind(Element dockindFieldElement, List<String> dockindFieldData) throws EdmException{

			String tableColumnName = dockindFieldData.get(0);
			String userCustomColumnName = dockindFieldData.get(1);
			String columnPrecision = dockindFieldData.get(2); 
			String columnType = dockindFieldData.get(3);
			Iterator it = dockindFieldElement.elementIterator();
			Element field;
			boolean fieldFound = false;
			int lastFieldIdx = -1;
			int currentIdx;
			while(it.hasNext()){
				field = (Element)it.next();
				currentIdx = Integer.parseInt(field.attributeValue("id"));
				if(lastFieldIdx < currentIdx)
					lastFieldIdx = currentIdx;
				if(field.attributeValue("column") != null && field.attributeValue("column").equals(tableColumnName)){
					field.addAttribute("name", userCustomColumnName);
					if(userCustomColumnName.equals("varchar"))
						field.addAttribute("length", columnPrecision);
					fieldFound = true;
					break;
				}
			}
			if(!fieldFound){
				
				<field id="299" cn="TITLE" column="title" name="Tytu�">
	        	<type name="string" length="50" match="context"/>
		        </field>
				 
				dockindFieldElement.addElement("field")
					.addAttribute("id", "" + ++lastFieldIdx)
					.addAttribute("cn", tableColumnName.toUpperCase())
					.addAttribute("column", tableColumnName)
					.addAttribute("name", userCustomColumnName)
					.addElement("type")
						.addAttribute("name", columnType)
						.addAttribute("length", columnPrecision)
						.addAttribute("match", "context");
			}
//			Element newField = DocumentHelper.createElement("");
//			dockindFieldElement.add(newField);
		}
	
		private void persistDockindXML(DocumentKind documentKind, Document documentKindXML) throws IOException, EdmException{
			OutputFormat format = OutputFormat.createPrettyPrint();
	        XMLWriter writer;
	        File tempFile = new File("./temp.xml");
			writer = new XMLWriter(new FileWriter(tempFile), format);
	        writer.write(documentKindXML);
	        writer.close();
//	        documentKind.saveContent(tempFile);
//	        tempFile.delete();
		}
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<String> getColumnsUserFriendlyNames() {
		return columnsUserFriendlyNames;
	}

	public void setColumnsUserFriendlyNames(List<String> columnsUserFriendlyNames) {
		this.columnsUserFriendlyNames = columnsUserFriendlyNames;
	}

	public int[] getSelectedColumnsIds() {
		return selectedColumnsIds;
	}

	public void setSelectedColumnsIds(int[] selectedColumnsIds) {
		this.selectedColumnsIds = selectedColumnsIds;
	}

	public Map<Integer, String> getColumnIdxToName() {
		return columnIdxToName;
	}

	public void setColumnIdxToName(Map<Integer, String> columnIdxToName) {
		this.columnIdxToName = columnIdxToName;
	}

	public Map<String, String> getColumnOriginalNameToNewCustomName() {
		return columnOriginalNameToNewCustomName;
	}

	public void setColumnOriginalNameToNewCustomName(
			Map<String, String> columnOriginalNameToNewCustomName) {
		this.columnOriginalNameToNewCustomName = columnOriginalNameToNewCustomName;
	}

	public String getPrimaryKeyColumn() {
		return primaryKeyColumn;
	}

	public void setPrimaryKeyColumn(String primaryKeyColumn) {
		this.primaryKeyColumn = primaryKeyColumn;
	}

	public List<String> getDbConnectionsList() {
		return dbConnectionsList;
	}

	public void setDbConnectionsList(List<String> dbConnectionsList) {
		this.dbConnectionsList = dbConnectionsList;
	}

	public String getChoosenDBconnection() {
		return choosenDBconnection;
	}

	public void setChoosenDBconnection(String choosenDBconnection) {
		this.choosenDBconnection = choosenDBconnection;
	}

	public List<String> getAvailableDockinds() {
		return availableDockinds;
	}

	public void setAvailableDockinds(List<String> availableDockinds) {
		this.availableDockinds = availableDockinds;
	}

	public String getChoosenDockind() {
		return choosenDockind;
	}

	public void setChoosenDockind(String choosenDockind) {
		this.choosenDockind = choosenDockind;
	}

	public String getDictionaryName() {
		return dictionaryName;
	}

	public void setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
	}

	public int getActionState() {
		return actionState;
	}

	public void setActionState(int actionState) {
		this.actionState = actionState;
	}

	public List<String> getAvailableExternalDictionaries() {
		return availableExternalDictionaries;
	}

	public void setAvailableExternalDictionaries(
			List<String> availableExternalDictionaries) {
		this.availableExternalDictionaries = availableExternalDictionaries;
	}

	public String getChoosenExternalDictionary() {
		return choosenExternalDictionary;
	}

	public void setChoosenExternalDictionary(String choosenExternalDictionary) {
		this.choosenExternalDictionary = choosenExternalDictionary;
	}
	
}*/

//package pl.compan.docusafe.web.admin.dictionaries;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.naming.Binding;
//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingEnumeration;
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletContext;
//import javax.sql.DataSource;
//
//import org.apache.commons.configuration.web.ServletConfiguration;
//import org.apache.commons.logging.LogFactory;
//
//import com.opensymphony.webwork.ServletActionContext;
//
//import pl.compan.docusafe.core.DSApi;
//import pl.compan.docusafe.core.dockinds.DocumentKind;
//import pl.compan.docusafe.util.querybuilder.select.FromClause;
//import pl.compan.docusafe.util.querybuilder.select.OrderClause;
//import pl.compan.docusafe.util.querybuilder.select.SelectClause;
//import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
//import pl.compan.docusafe.util.querybuilder.select.WhereClause;
//import pl.compan.docusafe.webwork.event.ActionEvent;
//import pl.compan.docusafe.webwork.event.ActionListener;
//import pl.compan.docusafe.webwork.event.CloseHibernateSession;
//import pl.compan.docusafe.webwork.event.EventActionSupport;
//import pl.compan.docusafe.webwork.event.OpenHibernateSession;
//
//public class OutgoingDictionariesAction extends EventActionSupport{
//
//	
//	private String tableName;
//	private String primaryKeyColumn;
//	private Map<Integer, String> columnIdxToName = new HashMap<Integer, String>();
//	private List<String> columnsUserFriendlyNames;
//	private int [] selectedColumnsIds;
//	private Map<String, String> columnOriginalNameToNewCustomName = new HashMap<String, String>();
//	private List<String> dbConnectionsList;
//	private String choosenDBconnection;
//	private int actionState;
//	@Override
//	protected void setup()
//    {
//        FillForm fillForm = new FillForm();
//
//        registerListener(DEFAULT_ACTION).
//            append(OpenHibernateSession.INSTANCE).
//            append(fillForm).
//            appendFinally(CloseHibernateSession.INSTANCE);
//
////        registerListener("").
////            append(OpenHibernateSession.INSTANCE).
////        	  append(new ).
////            append(fillForm).
////            appendFinally(CloseHibernateSession.INSTANCE);
//    }
///*
//	ja bym to zrobil tak ze najpierw wybierasz polaczenie, potem tabele, potem kolumny i opis i zapisujesz
//	i pozniej uzytkownik moze z zapisanych wybrac dockind do ktorego chce to przyczepic
//	
//	DocumentKind.getAvailableDocumentKindsCn();
//	*/
//	private class FillForm implements ActionListener
//    {
//        public void actionPerformed(ActionEvent event)
//        {
//            try
//            {
////            	gdy nie pobrano informacji o dostepnych bazach i nie zaznaczono zadnej z dostepnych polaczen i nie wybrano tabeli, z kt�rej b�d� pobierane informacje o kolumnach do dockinda
//            	if(dbConnectionsList == null && choosenDBconnection == null && tableName == null){
////            		ServletActionContext.getRequest().getSession().setAttribute("choosenDBconnection", choosenDBconnection);
//	            	// Obtain our environment naming context
//	            	Context initCtx = new InitialContext();
//	            	Context jdbcCtx = (Context) initCtx.lookup("java:comp/env"); 
//	            	dbConnectionsList = new ArrayList<String>();
//	            	Binding binding = null;
//	            	NamingEnumeration<Binding> bindingList = jdbcCtx.listBindings("jdbc");
//	            	while(bindingList.hasMore()){
//	            		binding = bindingList.next();
//	            		dbConnectionsList.add(binding.getName());
//	            	}
//	            	jdbcCtx.listBindings("jdbc").close();
//            	}else 
//            		if(choosenDBconnection != null){
//            			ServletActionContext.getRequest().getSession().setAttribute("choosenDBconnection", choosenDBconnection);
//            		}else
//	            		if(tableName != null && !tableName.equals("")){
//	            			choosenDBconnection = (String) ServletActionContext.getRequest().getSession().getAttribute("choosenDBconnection");
//		            		Context initCtx = new InitialContext();
//			            	DataSource ds = (DataSource) initCtx.lookup("java:comp/env/jdbc/" + choosenDBconnection);
//			            	Connection conn = ds.getConnection();
//			            	PreparedStatement ps = null;
//			            	ResultSet rs = null;
//		//            		wypisanie nazwy kolumny klucza g��wnego danej tabeli
//		            		ps = conn.prepareStatement(
//		             	    		" SELECT column_name" + 
//		             	    		" FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE" + 
//		             	    		" WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1" + 
//		             	    		" AND table_name = '" + tableName.trim() + "'");
//		            		rs = ps.executeQuery();
//		            		if(rs.next())
//		            			primaryKeyColumn = rs.getString(1);
//		            		primaryKeyColumn = "test";
//		 					System.out.println("PK: " + primaryKeyColumn);
//		// 					pobranie informacji o kolumnach wystepujacych w zapytaniu
//		            		ps = conn.prepareStatement("SELECT TOP 1 * FROM " + tableName.trim());
//		            	    rs = ps.executeQuery();
//		            	    int numberOfColumns = rs.getMetaData().getColumnCount();
//		            	    int columnIdx = 1;
//		            	    while(columnIdx <= numberOfColumns){
//		//            	    	mapowanie id kolumny do jej nazwy
//		            	    	columnIdxToName.put(
//		            	    			columnIdx,
//		            	    			rs.getMetaData().getColumnName(columnIdx)
//		            	    	);
//		            	    	System.out.println(columnIdx + " : " + rs.getMetaData().getColumnName(columnIdx));
//		            	    	++columnIdx;
//		            	    }
//		            	    conn.close();
//		            	}else 
//		            		if(columnIdxToName.size() > 0){
//			            		for(Integer columnId : columnIdxToName.keySet()){
//			            			if(columnIdxToName.get(columnId) != null)
//			            				System.out.println("ID: " + columnId + " New name: " + columnIdxToName.get(columnId));
//			            		}
//		            		}
//            }
//            catch (Exception e)
//            {
//            	LogFactory.getLog("eprint").debug("", e);
//                addActionError(e.getMessage());
//            }
//        }
//    }
//
//	
//	public String getTableName() {
//		return tableName;
//	}
//
//	public void setTableName(String tableName) {
//		this.tableName = tableName;
//	}
//
//	public List<String> getColumnsUserFriendlyNames() {
//		return columnsUserFriendlyNames;
//	}
//
//	public void setColumnsUserFriendlyNames(List<String> columnsUserFriendlyNames) {
//		this.columnsUserFriendlyNames = columnsUserFriendlyNames;
//	}
//
//	public int[] getSelectedColumnsIds() {
//		return selectedColumnsIds;
//	}
//
//	public void setSelectedColumnsIds(int[] selectedColumnsIds) {
//		this.selectedColumnsIds = selectedColumnsIds;
//	}
//
//	public Map<Integer, String> getColumnIdxToName() {
//		return columnIdxToName;
//	}
//
//	public void setColumnIdxToName(Map<Integer, String> columnIdxToName) {
//		this.columnIdxToName = columnIdxToName;
//	}
//
//	public Map<String, String> getColumnOriginalNameToNewCustomName() {
//		return columnOriginalNameToNewCustomName;
//	}
//
//	public void setColumnOriginalNameToNewCustomName(
//			Map<String, String> columnOriginalNameToNewCustomName) {
//		this.columnOriginalNameToNewCustomName = columnOriginalNameToNewCustomName;
//	}
//
//	public String getPrimaryKeyColumn() {
//		return primaryKeyColumn;
//	}
//
//	public void setPrimaryKeyColumn(String primaryKeyColumn) {
//		this.primaryKeyColumn = primaryKeyColumn;
//	}
//
//	public List<String> getDbConnectionsList() {
//		return dbConnectionsList;
//	}
//
//	public void setDbConnectionsList(List<String> dbConnectionsList) {
//		this.dbConnectionsList = dbConnectionsList;
//	}
//
//	public String getChoosenDBconnection() {
//		return choosenDBconnection;
//	}
//
//	public void setChoosenDBconnection(String choosenDBconnection) {
//		this.choosenDBconnection = choosenDBconnection;
//	}
//
//	public int getActionState() {
//		return actionState;
//	}
//
//	public void setActionState(int actionState) {
//		this.actionState = actionState;
//	}
//	
//	
//}
