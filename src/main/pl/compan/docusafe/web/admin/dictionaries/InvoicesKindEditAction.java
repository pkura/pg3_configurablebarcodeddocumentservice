package pl.compan.docusafe.web.admin.dictionaries;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InvoicesKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class InvoicesKindEditAction extends EventActionSupport
{
//  @IMPORT/@EXPORT
    private Integer id;
    private String name;
    private Integer days;

    // @IMPORT

    // @EXPORT
    private InvoicesKind kind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
                return;

            try
            {
                kind = InvoicesKind.find(id);
                DSApi.initializeProxy(kind);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy rodzaju faktury");
            if (days == null)
                addActionError("Nie podano liczby dni na za�atwienie faktury");
            else if (days.intValue() < 0)
                addActionError("Liczba dni na za�atwienie nie powinna by� mniejsza od zera");
            else if (days.intValue() > 366)
                addActionError("Liczba dni na za�atwienie nie powinna by� wi�ksza ni� 366");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                if (id != null)
                {
                    InvoicesKind kind = InvoicesKind.find(id);
                    kind.setName(name.trim());
                    kind.setDays(days.intValue());
                }
                else
                {
                    InvoicesKind kind = new InvoicesKind(name.trim(), days.intValue());
                    kind.create();
                }

                DSApi.context().commit();

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    

    public void setId(Integer id)
    {
        this.id = id;
    }

    public InvoicesKind getKind()
    {
        return kind;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDays(Integer days)
    {
        this.days = days;
    }

    public Integer getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Integer getDays()
    {
        return days;
    }



}
