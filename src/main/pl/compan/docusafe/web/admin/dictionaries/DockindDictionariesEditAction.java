package pl.compan.docusafe.web.admin.dictionaries;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DictionaryEventType;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CasePriorityEditAction.java,v 1.1 2004/11/06 00:33:30 lk Exp $
 */
public class DockindDictionariesEditAction extends EventActionSupport
{
	public static Logger log = LoggerFactory.getLogger(DockindDictionariesEditAction.class);
    // @IMPORT/@EXPORT
    private Integer id;
    private String name;
    private String cn;
    private String choosen; 
    private Integer centrum;
    private String refValue;
    private String refKey;
    private List<EnumItem> refValueList;
    private String docKind;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(docKind == null) return;

    		try
    		{
	    		DocumentKind dk = DocumentKind.findByCn(docKind);
	    		refValueList = new ArrayList<EnumItem>();
	    		
	    		
	    		for(Field fd : dk.getFields())
	    		{
	    			if(fd.getType().equals(Field.DATA_BASE) 
	    					&& ((DataBaseEnumField)fd).getTableName().equals(choosen) 
	    					&& ((DataBaseEnumField)fd).getRefField() != null)
	    			{
	    				refValueList = dk.getFieldByCn(((DataBaseEnumField) fd).getRefField()).getEnumItems();
	    			}
	    		}
    		}
    		catch(Exception e)
    		{
    			log.error(
						"DockindDictionariesEditAction.java:",e);    			
    			addActionError(e+" - " + e.getMessage());
    		}
        	
            if (id == null){            	
                return;
            }

            try
            {
            	DockindDictionaries dd = DockindDictionaries.find(id, choosen);
            	if(dd != null)
            	{
            		name = dd.getTitle();
            		cn = dd.getCn();
            		refValue = dd.getRefValue();
            		centrum = dd.getCentrum();
            	}
            	
            	if (refValue != null && refValueList != null) {
					EnumItem selected = null;
					for (EnumItem element : refValueList) {
						if (refValue.equals(element.getId().toString())) {
							setRefKey(element.getTitle());
							selected = element;
							break;
						}
					}
					if (selected != null) {
						refValueList.remove(selected);
					}
				}
            }
            catch (Exception e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Rodzaj jest polem obowiązkowym");
            

            if (hasActionErrors()){
            	log.error("DockindDictionariesEditAction.java:Update: action errors");
                return;
            }
            //Logger.getLogger("MICHAEL").debug("DockindDictionariesEditAction.java:test1"+" hasActionErrors() = " +hasActionErrors());
            try
            {
                DSApi.context().begin();

                if (id != null)//update
                {
                	//Logger.getLogger("MICHAEL").debug("DockindDictionariesEditAction.java:test2" +" hasActionErrors() = " +hasActionErrors());
                	
                	DockindDictionaries dd = DockindDictionaries.find(id, choosen);
					DataMartEventBuilder.get()
						.defaultObjectId(id)
						.checkValueChangeEvent(DictionaryEventType.ENTRY_CHANGE, choosen, "cn", dd.getCn(), cn)
						.checkValueChangeEvent(DictionaryEventType.ENTRY_CHANGE, choosen, "title", dd.getTitle(), name)
						.checkValueChangeEvent(DictionaryEventType.ENTRY_CHANGE, choosen, "centrum", ObjectUtils.toString(dd.getCentrum(), null), ObjectUtils.toString(centrum, null))
						.checkValueChangeEvent(DictionaryEventType.ENTRY_CHANGE, choosen, "refValue", dd.getRefValue(), refValue);

                	dd.setCn(cn);
                	dd.setTitle(name);
                	dd.setCentrum(centrum);
                	dd.setRefValue(refValue);
                	dd.update(choosen);                	
                }
                else//create
                {
                	DockindDictionaries dd = new DockindDictionaries(null,cn,name,centrum,refValue);
                	dd.create(choosen);
                }

                DSApi.context().commit();
//                DocumentKind.resetDockindInfo(docKind);
                DataBaseEnumField.reloadForTable(choosen);

                //Logger.getLogger("MICHAEL").debug("DockindDictionariesEditAction.java:test5"+" hasActionErrors() = " +hasActionErrors());

                event.setResult("list");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error(
						"DockindDictionariesEditAction.java:"+e,e);
            } catch (HibernateException e)
            {
            	log.error(
						"DockindDictionariesEditAction.java:"+e,e);
            	addActionError(e.getMessage());
			}
            catch (SQLException e) 
            {
            	log.error(
						"DockindDictionariesEditAction.java:"+e,e);
            	addActionError(e.getMessage());
			}
            catch (Exception e){
            	log.error(
						"DockindDictionariesEditAction.java:"+e,e);
            }
        }
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getChoosen() {
		return choosen;
	}

	public void setChoosen(String choosen) {
		this.choosen = choosen;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	public String getDocKind() {
		return docKind;
	}

	public void setDocKind(String docKind) {
		this.docKind = docKind;
	}

	public List<EnumItem> getRefValueList() {
		return refValueList;
	}

	public void setRefValueList(
			List<EnumItem> refValueList) {
		this.refValueList = refValueList;
	}

	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}

	public String getRefKey() {
		return refKey;
	}

}
