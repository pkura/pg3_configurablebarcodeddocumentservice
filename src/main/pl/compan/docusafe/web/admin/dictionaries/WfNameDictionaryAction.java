package pl.compan.docusafe.web.admin.dictionaries;

import java.util.List;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.aegon.wf.dictionary.WfName;
import pl.compan.docusafe.parametrization.aegon.wf.dictionary.WfNameStatus;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.aegon.wf.WorkflowLogic;
import std.fun;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WfNameDictionaryAction extends EventActionSupport
{

    private static final Logger log = LoggerFactory.getLogger(WfNameDictionaryAction.class);
    private StringManager sm = StringManager.getManager(WfNameDictionaryAction.class.getPackage().getName());
    public static final String URL = "/admin/dictionaries/wf-name.action";
    private static final int LIMIT = 15;
    private Long id;
    private String nazwa;
    private String opis;
    private String dataRozpoczecia;
    private Long status;
    private Long kierownikTechniczny;
    private Long kierownikBiznesowy;
    private Long searchId;
    private String searchNazwa;
    private String searchOpis;
    private String searchDataRozpoczecia;
    private Long searchStatus;
    private Long searchKierownikTechniczny;
    private Long searchKierownikBiznesowy;
    private WfName wfNameInstance;
    private Boolean canDelete;
    private Boolean canAdd;
    private Boolean canRead;
    private Boolean canModify;
    private Pager pager;
    private int offset;
    private final String RENAME_ACTION = "rename";
    /**u�ywane przez strony JSP - tutaj nie ma znaczenia, powinno by� tylko przekazywane */
    private String param;
    private List<? extends WfName> results;
    private List<DSUser> users;
    private List<WfNameStatus> statuses;

    @Override
    protected void setup()
    {
        setStartedParams();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
                append(OpenHibernateSession.INSTANCE).
                append(new Clean()).
                append(new Add()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
                append(OpenHibernateSession.INSTANCE).
                append(new Clean()).
                append(new Update()).
                append(RENAME_ACTION, new RenameFolders()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
                append(OpenHibernateSession.INSTANCE).
                append(new Clean()).
                append(new Search()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
                append(OpenHibernateSession.INSTANCE).
                append(new Delete()).
                append(new FillForm()).
                appendFinally(CloseHibernateSession.INSTANCE);

    }

    private class FillForm implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (id != null)
                {
                    wfNameInstance = WfName.getInstance().find(id);
                    setFormData();
                }
            } catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Add implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
             
                if (!canAdd)
                {
                    throw new EdmException(sm.getString("BrakUprawnienDoDodawaniaRekordowDoSlownika"));
                }
                Validate();
                if (hasActionErrors())
                {
                    throw new EdmException(sm.getString("BledneDane"));
                }

                wfNameInstance = new WfName();
                wfNameInstance.setNazwa(nazwa);
                wfNameInstance.setOpis(opis);
                wfNameInstance.setDataRozpoczecia(DateUtils.nullSafeParseJsDate(dataRozpoczecia));
                wfNameInstance.setStatus(status);
                wfNameInstance.setKierownikTechniczny(kierownikTechniczny);
                wfNameInstance.setKierownikBiznesowy(kierownikBiznesowy);
                wfNameInstance.create();

                id = wfNameInstance.getId();

                DSApi.context().commit();

                addActionMessage(sm.getString("WslownikuUtworzono"));
            } catch (EdmException e)
            {
                try
                {
                    DSApi.context().rollback();
                } catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private void Validate()
    {
        if (StringUtils.isEmpty(nazwa))
        {
            addActionError(sm.getString("NiePodanoNazwy"));
        }

        if (status == null)
        {
            addActionError(sm.getString("PodajStatus"));
        }
    }

    private class Clean implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            nazwa = TextUtils.trimmedStringOrNull(nazwa);
            opis = TextUtils.trimmedStringOrNull(opis);
            dataRozpoczecia = TextUtils.trimmedStringOrNull(dataRozpoczecia);
        }
    }

    private class Update implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            boolean shouldRenameFolders = false;

            try
            {
                DSApi.context().begin();

                if (!canModify)
                {
                    throw new EdmException(sm.getString("BrakUprawnienDoModyfikowaniaRekordow"));
                }
                 
                Validate();
                if (hasActionErrors())
                {
                    throw new EdmException(sm.getString("BledneDane"));
                }
                WfName wfNameInstance = WfName.getInstance().find(id);

                if (nazwa != null && !nazwa.equals(wfNameInstance.getNazwa()))
                {
                    shouldRenameFolders = true;
                }

                wfNameInstance.setNazwa(nazwa);
                wfNameInstance.setOpis(opis);
                wfNameInstance.setDataRozpoczecia(DateUtils.nullSafeParseJsDate(dataRozpoczecia));
                wfNameInstance.setStatus(status);
                wfNameInstance.setKierownikTechniczny(kierownikTechniczny);
                wfNameInstance.setKierownikBiznesowy(kierownikBiznesowy);

                DSApi.context().session().save(wfNameInstance);
                addActionMessage(sm.getString("ElementZostalZaktualizowany"));
                DSApi.context().commit();
            } catch (EdmException e)
            {
                DSApi.context()._rollback();
                log.debug(e.getMessage(), e);
                shouldRenameFolders = false;
            }

            if (!shouldRenameFolders)
            {
                event.skip(RENAME_ACTION);
            }

        }
    }

    private class Search implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!canRead)
                {
                    throw new EdmException(sm.getString("BrakUprawnienDoWyszukiwania"));
                }
                QueryForm form = new QueryForm(offset, LIMIT);
                form.addOrderAsc("nazwa");

                if (nazwa != null)
                {
                    form.addProperty("nazwa", nazwa);
                }
                if (opis != null)
                {
                    form.addProperty("opis", opis);
                }
                if (dataRozpoczecia != null)
                {
                    form.addProperty("dataRozpoczecia", dataRozpoczecia);
                }
                if (status != null)
                {
                    form.addProperty("status", status);
                }
                if (kierownikTechniczny != null)
                {
                    form.addProperty("kierownikTechniczny", kierownikTechniczny);
                }
                if (kierownikBiznesowy != null)
                {
                    form.addProperty("kierownikBiznesowy", kierownikBiznesowy);
                }

                SearchResults<? extends WfName> results = WfName.search(form);
                if (results.totalCount() == 0)
                {
                    throw new EdmException(sm.getString("NieZnaleziono"));
                } else
                {
                    searchId = id;
                    searchNazwa = nazwa;
                    searchOpis = opis;
                    searchDataRozpoczecia = dataRozpoczecia;
                    searchStatus = status;
                    searchKierownikTechniczny = kierownikTechniczny;
                    searchKierownikBiznesowy = kierownikBiznesowy;
                }
                WfNameDictionaryAction.this.results = fun.list(results);
                
                WfNameDictionaryAction.this.results = WfName.setWfNameStatusName(WfNameDictionaryAction.this.results, statuses);
                makeDictionaryPager(results.totalCount());

            } catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (!canDelete)
                {
                    throw new EdmException(sm.getString("BrakUprawnienDoUsuwaniaRekordow"));
                }
                DSApi.context().begin();
                wfNameInstance = WfName.getInstance().find(id);
                wfNameInstance.delete();
                id = null;
                addActionMessage(sm.getString("ZeSlownikaUsunietoNazwe", wfNameInstance.getNazwa()));
                DSApi.context().commit();
            } catch (EdmException e)
            {
                try
                {
                    DSApi.context().rollback();
                } catch (EdmException e1)
                {
                    addActionError(e1.getMessage());
                }
                addActionError(e.getMessage());
            }
        }
    }

    private class RenameFolders implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.context().begin();
            	
            	WfName inst = WfName.getInstance().find(id);
            	inst.cleanFolders();
            	
            	DSApi.context().commit();              	
            }
            catch (Exception e) 
            {
            	DSApi.context()._rollback();
            	log.debug(e.getMessage(),e);
            }
        }
    }

    private void makeDictionaryPager(int resultCount)
    {
        Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
        {

            public String getLink(int offset)
            {
                return HttpUtils.makeUrl(URL, new Object[]
                        {
                            "doSearch", "true",
                            "nazwa", nazwa,
                            "opis", opis,
                            "dataRozpoczecia", dataRozpoczecia,
                            "status", status,
                            "kierownikTechniczny", kierownikTechniczny,
                            "kierownikBiznesowy", kierownikBiznesowy,
                            "searchNazwa", searchNazwa,
                            "searchOpis", searchOpis,
                            "searchDataRozpoczecia", searchDataRozpoczecia,
                            "searchStatus", searchStatus,
                            "searchKierownikTechniczny", searchKierownikTechniczny,
                            "searchKierownikBiznesowy", searchKierownikBiznesowy,
                            "offset", String.valueOf(offset),
                            "param", param
                        });
            }
        };
        pager = new Pager(linkVisitor, offset, LIMIT, resultCount, 10);
    }

    public void setFormData()
    {
        if (wfNameInstance != null)
        {
            //id = wfNameInstance.getId();
            nazwa = wfNameInstance.getNazwa();
            opis = wfNameInstance.getOpis();
            if (wfNameInstance.getDataRozpoczecia() != null)
            {
                dataRozpoczecia = wfNameInstance.getDataRozpoczecia().toString();
            }
            status = wfNameInstance.getStatus();
            kierownikTechniczny = wfNameInstance.getKierownikTechniczny();
            kierownikBiznesowy = wfNameInstance.getKierownikBiznesowy();
        }
    }

    private void setStartedParams()
    {
        try
        {
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            this.users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);

            canRead = canAdd = canDelete = canModify = false;
            DSDivision[] userGroup = DSApi.context().getDSUser().getDSGroups();
            for(DSDivision group : userGroup)
            {
                if(DSPermission.WF_READ_DICTIONARY.getName().equals(group.getGuid()))
                    canRead = true;
                
                if(DSPermission.WF_ADMIN_DICTIONARY.getName().equals(group.getGuid()))
                {
                    canAdd = canDelete = canModify = true;
                }
            }
           /* log.error("read:" +canRead);
            log.error("add:" +canAdd);
            log.error("delete:" +canDelete);
            log.error("modify:" +canModify);
           */
            this.statuses = WfNameStatus.list();

        } catch (EdmException e)
        {
            logException(e);
            addActionError(e.getMessage());
            //DSApi.context().setRollbackOnly();
        } finally
        {
            try
            {
                DSApi.close();
            } catch (EdmException e)
            {
            }
        }
    }

    public void logException(Exception e)
    {
        log.error("", e);
    }

    public void logError(String errorString)
    {
        log.error(errorString);
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public void setUsers(List users)
    {
        this.users = users;
    }

    public Boolean getCanAdd()
    {
        return canAdd;
    }

    public void setCanAdd(Boolean canAdd)
    {
        this.canAdd = canAdd;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanModify()
    {
        return canModify;
    }

    public void setCanModify(Boolean canModify)
    {
        this.canModify = canModify;
    }

    public Boolean getCanRead()
    {
        return canRead;
    }

    public void setCanRead(Boolean canRead)
    {
        this.canRead = canRead;
    }

    public String getDataRozpoczecia()
    {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(String dataRozpoczecia)
    {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getKierownikBiznesowy()
    {
        return kierownikBiznesowy;
    }

    public void setKierownikBiznesowy(Long kierownikBiznesowy)
    {
        this.kierownikBiznesowy = kierownikBiznesowy;
    }

    public Long getKierownikTechniczny()
    {
        return kierownikTechniczny;
    }

    public void setKierownikTechniczny(Long kierownikTechniczny)
    {
        this.kierownikTechniczny = kierownikTechniczny;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public Long getStatus()
    {
        return status;
    }

    public void setStatus(Long status)
    {
        this.status = status;
    }

    public String getParam()
    {
        return param;
    }

    public void setParam(String param)
    {
        this.param = param;
    }

    public WfName getWfNameInstance()
    {
        return wfNameInstance;
    }

    public void setWfNameInstance(WfName wfNameInstance)
    {
        this.wfNameInstance = wfNameInstance;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Pager getPager()
    {
        return pager;
    }

    public void setPager(Pager pager)
    {
        this.pager = pager;
    }

    public List<? extends WfName> getResults()
    {
        return results;
    }

    public void setResults(List<? extends WfName> results)
    {
        this.results = results;
    }

    public String getSearchDataRozpoczecia()
    {
        return searchDataRozpoczecia;
    }

    public void setSearchDataRozpoczecia(String searchDataRozpoczecia)
    {
        this.searchDataRozpoczecia = searchDataRozpoczecia;
    }

    public Long getSearchId()
    {
        return searchId;
    }

    public void setSearchId(Long searchId)
    {
        this.searchId = searchId;
    }

    public Long getSearchKierownikBiznesowy()
    {
        return searchKierownikBiznesowy;
    }

    public void setSearchKierownikBiznesowy(Long searchKierownikBiznesowy)
    {
        this.searchKierownikBiznesowy = searchKierownikBiznesowy;
    }

    public Long getSearchKierownikTechniczny()
    {
        return searchKierownikTechniczny;
    }

    public void setSearchKierownikTechniczny(Long searchKierownikTechniczny)
    {
        this.searchKierownikTechniczny = searchKierownikTechniczny;
    }

    public String getSearchNazwa()
    {
        return searchNazwa;
    }

    public void setSearchNazwa(String searchNazwa)
    {
        this.searchNazwa = searchNazwa;
    }

    public String getSearchOpis()
    {
        return searchOpis;
    }

    public void setSearchOpis(String searchOpis)
    {
        this.searchOpis = searchOpis;
    }

    public Long getSearchStatus()
    {
        return searchStatus;
    }

    public void setSearchStatus(Long searchStatus)
    {
        this.searchStatus = searchStatus;
    }

    public List<WfNameStatus> getStatuses()
    {
        return statuses;
    }

    public void setStatuses(List<WfNameStatus> statuses)
    {
        this.statuses = statuses;
    }
    
    public String getRequestBaseUrl(){
        return WfNameDictionaryAction.URL;
    }
}
