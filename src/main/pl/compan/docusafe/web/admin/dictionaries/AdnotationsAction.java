package pl.compan.docusafe.web.admin.dictionaries;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.office.Adnotation;

import java.util.List;
import pl.compan.docusafe.core.DSApi;

/**
 * @author Piotr Komisarski
 */
public class AdnotationsAction extends EventActionSupport
{

    private List<Adnotation> adnotations;
    private Integer[] priorityIds;
    private String actionStatus;
    private String newAdnotation;
    private boolean create;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {        
            //create = false;
            try
            {
                adnotations = Adnotation.list();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (priorityIds == null || priorityIds.length == 0)
            {
                addActionError("Nie wybrano element�w do usuni�cia");
                return;
            }

            try
            {
                
                DSApi.context().begin();

                for (int i=0; i < priorityIds.length; i++)
                {
                    Adnotation d = Adnotation.find(priorityIds[i]);
                    addActionMessage("Usuni�to '"+d.getName()+"'");
                    d.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (newAdnotation == null || newAdnotation.length() == 0)
            {
                addActionError("Pusta uwaga niedozwolona !");
                return;
            }

            try
            {
                DSApi.context().begin();
                new Adnotation(newAdnotation).create();
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    public List<Adnotation> getAdnotations()       
    {
        return adnotations;
    }
 
    public void setPriorityIds(Integer[] priorityIds)
    {
        this.priorityIds = priorityIds;
    }
    
    public boolean getCreate()
    {
        return create;
    }
    
    public void setCreate(boolean arg)
    {
        this.create = arg;
    }
    
    public void setNewAdnotation(String arg)
    {
        this.newAdnotation = arg;
    }

}
