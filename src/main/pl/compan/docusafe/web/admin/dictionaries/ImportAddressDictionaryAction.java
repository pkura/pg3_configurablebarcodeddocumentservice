package pl.compan.docusafe.web.admin.dictionaries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ImportAddressDictionaryAction extends EventActionSupport {
	
    static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(ImportAddressDictionaryAction.class.getPackage().getName(),null);

    // @IMPORT
    private FormFile file;
    private boolean done;

	
	public void setFile(FormFile file) {
		this.file = file;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doLoadFile").
            append(OpenHibernateSession.INSTANCE).
            append(new LoadFile()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
        }
    }
	
	private class LoadFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"raport.txt\"");
            if (file == null )
            {
                addActionError(sm.getString("NiePodanoPliku"));
                return;
            }
            
            
            try
            {
            	PrintWriter writer = new PrintWriter(response.getOutputStream());
            	
            	writer.print(parseAndSaveFile(file.getFile()));
            	
            	writer.close();
            }
            catch (IOException e) 
            {
            	//e.printStackTrace();
			}
            
            /*try
            {
            	
                done = parseAndSaveOutgoingFile(file.getFile(),response.getOutputStream());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
            catch(IOException e)
            {
            	addActionError(e.getMessage());
            	addActionError(sm.getString("ImportZostalWykonanyAleNieMoznaByloZapisacRaportu"));
            }*/
            //if(done) addActionMessage(sm.getString("PomyslnieZakonczonoImport"));
            //else addActionError(sm.getString("WystapilyProblemyZimportem"));
        }
    }
	
	public String getZaladuj()
    {
        return GlobalPreferences.loadPropertiesFile("",null).getString("Zaladuj");
    }
	
	private static String parseAndSaveFile(File file)
	{
		String returnString;
		try
		{	
			DSApi.context().begin();
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
			CSVStrategy strategy = new CSVStrategy(';','"',CSVStrategy.COMMENTS_DISABLED,false,false,true);
			//CSVStrategy strategy = new CSVStrategy(';','"','#',false,false,true);
			CSVParser csvParser = new CSVParser(reader,strategy);
	        String[] parts;
	        int lineNum = 0; //csvParser.getLineNumber() nie chce zadzialac
	        while((parts = csvParser.getLine()) != null)
	        {	        	
	        	lineNum++;
	        	try
	        	{
	        		
	        		if(lineNum == 1 || parts[0].startsWith("#"))
	        			throw new EdmException(sm.getString("LinniaZignorowana"));
	        		
	        		Person person = new Person();
	        		
	        		if(parts[0].trim().equalsIgnoreCase("recipient"))
	        		{
	        			person.setDictionaryType("recipient");
	        		}
	        		else if(parts[0].trim().equalsIgnoreCase("sender"))
	        		{
	        			person.setDictionaryType("sender");
	        		}
	        		else
	        		{
	        			throw new EdmException(sm.getString("NieznanyTypSlownika"));
	        		}
	        		
	        		person.setDictionaryGuid(DSDivision.ROOT_GUID);
	        		person.setCountry("PL");
	        		
	        		person.setTitle(parts[1]);
	        		person.setFirstname(parts[2]);
	        		person.setLastname(parts[3]);
	        		person.setOrganization(parts[4]);
	        		person.setOrganizationDivision(parts[5]);
	        		person.setStreet(parts[6]);
	        		person.setZip(parts[7]);
	        		person.setLocation(parts[8]);
	        		person.setEmail(parts[9]);
	        		person.setFax(parts[10]);
	        		person.setNip(parts[11]);
	        		person.setPesel(parts[12]);
	        		person.setRegon(parts[13]);
	        		person.setPersonGroup(parts[14]);
	        		
	        		if(person.createIfNew())
	        		{
	        			//zaimportowano osobe
	        			sb.append(lineNum+" "+sm.getString("ZaimportowanaOsobeId")+" "+person.getId()+"\n");
	        		}
	        		else
	        		{
	        			throw new EdmException(sm.getString("IstniejeJuzTakaOsoba"));
	        		}
	        		
	        	}
	        	catch (Exception e) 
	        	{
	        		sb.append(lineNum+" "+e.getMessage()+"\n");
				}
	        }
			reader.close();
			returnString = sb.toString();
			DSApi.context().commit();
		}
		catch (IOException e) 
		{
			DSApi.context()._rollback();
			returnString = sm.getString("BladPrzyCzytaniuPliku");
		}
		catch (Exception e) 
		{
			DSApi.context()._rollback();
			returnString = sm.getString("BladWCzsieImportu");
			LoggerFactory.getLogger("tomekl").debug("a "+e.getMessage(),e);
		}
		
		return returnString;
	}
	
	@Deprecated
	public static boolean parseAndSaveOutgoingFile(File file, OutputStream output) throws EdmException
    {
		DSDivision[] userDiv = DSApi.context().getDSUser().getDivisions();
    	boolean result=true;
        try
        {
        	PrintWriter writer = new PrintWriter(output);
        	writer.println(sm.getString("RozpoczetoImport"));
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0, errorCount = 0;
            String[] prevParts = {""};
            boolean koniec = false;
        
        	if(reader.readLine() == null) {
        		koniec = true;
        	}
			DSApi.context().begin();
            while (!koniec)
            {
            	if((line = reader.readLine()) == null) {
            		line = " ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;";
            		koniec = true;
            	}
                try {
					lineCount++;
					//if(lineCount%10==0) DSApi.context().session().clear();
					line = line.trim();
					if (line.startsWith("#")){
					    lineCount--;
						continue;
					}
					
					LoggerFactory.getLogger("tomekl").debug("line -> "+line);
					
					String[] parts = new String[16];
					parts = line.split(";");
					/*if(parts.length<16) {
						lineCount--;
						continue;
					}*/
					//if(lineCount == 1) prevParts = parts;
					if(!(parts[3].replace("\"","").trim()).equals(""))
					{
						Person osoba = new Person();
						//if(parts[0].replace("\"","").trim().equals(sm.getString("odbiorca")))
						if(parts[0].trim().equalsIgnoreCase("recipient"))	
							osoba.setDictionaryType("recipient");
						else
							osoba.setDictionaryType("sender");
						
						osoba.setDictionaryGuid(DSDivision.ROOT_GUID);
						//if(parts.length > 9)
							osoba.setCountry("PL");
						if(parts.length > 9)
							osoba.setEmail(TextUtils.trimmedStringOrNull(parts[9].replace("\"","").trim(),50));
						if(parts.length > 10)
							osoba.setFax(TextUtils.trimmedStringOrNull(parts[10].replace("\"","").trim()));
						if(parts.length > 2)
							osoba.setFirstname(TextUtils.trimmedStringOrNull(parts[2].replace("\"","").trim(), 50));
						if(parts.length > 3)
							osoba.setLastname(TextUtils.trimmedStringOrNull(parts[3].replace("\"","").trim(),50));
						if(parts.length > 8)
							osoba.setLocation(TextUtils.trimmedStringOrNull(parts[8].replace("\"","").trim(), 50));
						if(parts.length > 4)
							osoba.setOrganization(TextUtils.trimmedStringOrNull(parts[4].replace("\"","").trim(), 50));
						if(parts.length > 5)
							osoba.setOrganizationDivision(TextUtils.trimmedStringOrNull(parts[5].replace("\"","").trim(), 50));
						if(parts.length > 6)
							osoba.setStreet(TextUtils.trimmedStringOrNull(parts[6].replace("\"","").trim(), 50));
						if(parts.length > 1)
							osoba.setTitle(TextUtils.trimmedStringOrNull(parts[1].replace("\"","").trim(), 30));
						if(parts.length > 7)
							osoba.setZip(TextUtils.trimmedStringOrNull(parts[7].replace("\"","").trim(), 14));
						if(parts.length > 11)
							osoba.setNip(TextUtils.trimmedStringOrNull(parts[11].replace("\"","").trim() != null ? (parts[11].replace("\"","").trim()).replaceAll("-", "") : null, 15));
						if(parts.length > 12)
							osoba.setPesel(TextUtils.trimmedStringOrNull(parts[12].replace("\"","").trim(), 11));
						if(parts.length > 13)
							osoba.setRegon(TextUtils.trimmedStringOrNull(parts[13].replace("\"","").trim(), 15));
						if(parts.length > 14)
							osoba.setPersonGroup(TextUtils.trimmedStringOrNull(parts[14].replace("\"","").trim(), 50));
						
						boolean created = osoba.createIfNew();
						
						LoggerFactory.getLogger("tomekl").debug("osoba "+osoba.getId());
						
						if(created)
							writer.println(sm.getString("OkZaimportowanoOsobe")+": "+parts[3].replace("\"","").trim());
						else
						{
							writer.println(sm.getString("NieZaimportowanoOsoby")+": "+parts[3].replace("\"","").trim());
							writer.println(sm.getString("PrwdopodobnieIstniejeJuzTakOsoba"));
							errorCount++;
						}
					}
                
				}
                catch(EdmException e)
                {
                	LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
                	LogFactory.getLog("eprint").debug("", e);
                }
                catch(Exception e)
                {
                	LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
                	LogFactory.getLog("eprint").debug("", e);
                }
                
            }
            lineCount--;
            writer.println(sm.getString("CalkowitaLiczbaBlednychImportow")+": " + errorCount);
            writer.println(sm.getString("CalkowitaLiczbaOsob")+": " + lineCount);
            writer.close();
            DSApi.context().commit();
            //DSApi.context().session().clear();
            return result;

        }
        catch (IOException e)
        {
            throw new EdmException(sm.getString("BladPodczasCzytaniaPliku"));
        }
    }

	public FormFile getFile() {
		return file;
	}
}