package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;


public class DispatchAction extends EventActionSupport
{
	private final static Logger log = LoggerFactory.getLogger(DispatchAction.class);
    private String dispatchDivisionPrettyPath;
    private String divisionGuid;
    private boolean canPick;
    private String treeHtml;
    
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(DispatchAction.class.getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPick").
            append(OpenHibernateSession.INSTANCE).
            append(new Pick()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            try
            {
                String dispatchGuid = GlobalPreferences.getDispatchDivisionGuid();

                // wyb�r aktualnie wy�wietlanego dzia�u
                DSDivision targetDivision = null;
                if (divisionGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(divisionGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    	log.error("",e);
                    }
                }

                if (targetDivision == null && dispatchGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(dispatchGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    	log.error("",e);
                    }
                }

                DSDivision dispatchDivision = null;
                if (dispatchGuid != null)
                {
                    try
                    {
                        dispatchDivision = DSDivision.find(dispatchGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    	log.error("",e);
                    }
                }

                // klasa tworz�ca urle dla element�w drzewa
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            StringBuilder url =
                                new StringBuilder(
                                    ServletActionContext.getRequest().getContextPath()+
                                    "/admin/dispatch.action?" +
                                    "divisionGuid="+((DSDivision) element).getGuid());

                            return url.toString();
                        }
                        catch (Exception e)
                        {
                            event.getLog().error(e.getMessage(), e);
                            return null;
                        }
                    }
                };

                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true, false, false);

                treeHtml = tree.generateTree();

                canPick = targetDivision != null &&
                    (dispatchDivision == null || !dispatchDivision.getGuid().equals(targetDivision.getGuid()));
                dispatchDivisionPrettyPath = dispatchDivision != null ? dispatchDivision.getPrettyPath() : null;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Pick implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (divisionGuid == null)
                addActionError(sm.getString("NieWybranoStanowiska"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();     
                DSDivision dispatchDivision = DSDivision.find(divisionGuid);
                GlobalPreferences.setDispatchDivisionGuid(dispatchDivision.getGuid());
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getDispatchDivisionPrettyPath()
    {
        return dispatchDivisionPrettyPath;
    }

    public boolean isCanPick()
    {
        return canPick;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }
}
