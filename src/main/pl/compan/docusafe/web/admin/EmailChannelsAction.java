package pl.compan.docusafe.web.admin;


import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class EmailChannelsAction extends EventActionSupport
{
	private List<DSEmailChannel> channels;
	private String redirectUrl;
	static StringManager sm = StringManager.getManager(EmailChannelsAction.class.getPackage().getName());

	static final long serialVersionUID = -1L;
	
	protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    }
	 
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{ 
        		redirectUrl = ServletActionContext.getRequest().getRequestURI();
        		channels = MessageManager.getChannels();
        		 if (ServletActionContext.getRequest().getAttribute("info") != null){
        			 String info = (String) ServletActionContext.getRequest().getAttribute("info");
        			 event.addActionMessage(info); 
        			 ServletActionContext.getRequest().removeAttribute("info");
        		 }
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }
	public List<DSEmailChannel> getChannels() {
		return channels;
	}

	public void setChannels(List<DSEmailChannel> channels) {
		this.channels = channels;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	    
	    
}
