package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.*;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 02.10.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class BarcodePoolManagementAction extends EventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(BarcodePoolManagementAction.class);

    private String prefix;
    private String prefixToAdd;
    private int amountUsed, limit;
    private ArrayList<String> prefixes = new ArrayList<String>();

    @Override
    protected void setup() {

        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
                append(OpenHibernateSession.INSTANCE).
                append(new AddPool()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);


    }

    private class FillForm implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event) {
            try
            {
                Statement st = DSApi.context().createStatement();
                ResultSet rs = st.executeQuery("select * from dsg_pg_barcode_generator");
                while(rs.next())
                {
                    prefix = rs.getString("prefix");
                }
                rs.close();
                st.close();

                Statement st2 = DSApi.context().createStatement();
                ResultSet rs2 = st2.executeQuery("select * from dsg_pg_barcode_pool_prefix");
                while(rs2.next())
                {
                    String tempPrefix = rs2.getString("prefix");
                    if(!tempPrefix.equalsIgnoreCase(prefix))
                    {
                        prefixes.add(tempPrefix);
                    }
                }


            }
            catch(Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }

    private class AddPool implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent event) {
            try
            {
                boolean isOk=true;
                log.error("********przes�any prefix:"+prefixToAdd);
                if(prefixToAdd==null || prefixToAdd.trim().length()==0 || prefixToAdd.equalsIgnoreCase(""))
                {
                    addActionError("Nie podano prefixu");
                    isOk=false;
                }
                Statement st = DSApi.context().createStatement();
                ResultSet rs = st.executeQuery("select * from dsg_pg_barcode_pool_prefix where prefix='" + prefixToAdd.toUpperCase() + "'");

                while(rs.next())
                {
                    isOk=false;
                    addActionError("Podany prefix ju� istnieje");
                }
                rs.close();
                st.close();

                if(isOk)
                {
                    Statement st2 = DSApi.context().createStatement();
                    int result = st2.executeUpdate("insert into dsg_pg_barcode_pool_prefix(prefix) values('" + prefixToAdd.toUpperCase() + "')");
                    int result2 = st2.executeUpdate("update dsg_pg_barcode_generator set prefix='" + prefixToAdd.toUpperCase() + "', pointer_value=0");
                    st2.close();
                }
            }
            catch(Exception e)
            {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefixToAdd() {
        return prefixToAdd;
    }

    public void setPrefixToAdd(String prefixToAdd) {
        this.prefixToAdd = prefixToAdd;
    }

    public ArrayList<String> getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(ArrayList<String> prefixes) {
        this.prefixes = prefixes;
    }
}
