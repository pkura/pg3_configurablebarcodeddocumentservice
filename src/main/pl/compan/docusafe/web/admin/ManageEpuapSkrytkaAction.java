package pl.compan.docusafe.web.admin;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.util.Base64;

import com.asprise.util.tiff.B;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.parametrization.nfos.LotusAttachment;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytkaManager;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * CRUD do obiektu EpuapSkrytka
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class ManageEpuapSkrytkaAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(ManageEpuapSkrytkaAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ManageEpuapSkrytkaAction.class.getPackage().getName(),null);
	private static final long serialVersionUID = 1L;
	
	private List<EpuapSkrytka> skrytki;
	private String nazwaPliku = new String();
	
	private EpuapSkrytka skrytka = new EpuapSkrytka();
    
	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

	    registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
	    registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
	    registerListener("doImport").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ImportLotusAttachment()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
	    
	    registerListener("doChangeActive").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ChangeActivity()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
	}
	
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	skrytki = EpuapSkrytkaManager.list();
            	nazwaPliku = new String();
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if(skrytka.getId() != null)
            		skrytka = EpuapSkrytkaManager.find(skrytka.getId());
            	else
            		throw new NullPointerException("Brak skrytki do usuni�cia!");
            	
            	
            	if(skrytka == null)
            		throw new NullPointerException("Skrytka o takim id nie istnieje!");
            	else
            	{
            		DSApi.context().begin();
            		deleteCertificate(skrytka);
            		Persister.delete(skrytka);
            		addActionMessage("Skrytka zosta�a usuni�ta");
            		DSApi.context().commit();
            	}
            }
            catch (Exception e)
            {
            	try
				{
					DSApi.context().rollback();
				} catch (EdmException e1)
				{
					log.error("", e);
				}
            	log.error("", e);
                addActionError(e.getMessage());
            }
        }

		private void deleteCertificate(EpuapSkrytka skrytka) {
			try{
//				String basePath = Docusafe.getAdditionProperty("epuapSkrytka.certificateBasePath");
//		    	File dsHome = new File(Docusafe.getHome(), null);
		    	File cert = new File(Docusafe.getHome(), skrytka.getCertificateFileName());
	 
	    		if(cert.delete()){
	    			System.out.println(cert.getName() + " is deleted!");
	    		}else{
	    			throw new EdmException("Usuwanie certyfikatu sie nie powiod�o");
	    		}
	 
	    	}catch(EdmException e){
	    		log.error("", e.getMessage());	 
	    	}
		}
    }
    
    private class ImportLotusAttachment implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			
			if(AvailabilityManager.isAvailable("nfos.debug")){
				if (nazwaPliku == null || nazwaPliku.equals("")){
					addActionMessage("Brak nazwy pliku do importu");
					return;
				}
			File file = new File(Docusafe.getHome(), nazwaPliku);
			try {
				byte[] bytes = FileUtils.getBytesFromFile(file);
				DSApi.context().begin();
				LotusAttachment la = new LotusAttachment();
				la.setFileName(nazwaPliku);
				la.setLotusEmailId(222L);
//				String bytesString = Base64.encodeBase64String(bytes);
//				la.setContent(bytesString);
				la.setContent(bytes);
				la.setCreateDate(new Date());
				la.save();
				DSApi.context().commit();
			} catch (Exception e) {
				log.error("", e);
				addActionError(e.getMessage());
				DSApi.context()._rollback();
			} finally {
				DSApi._close();
			}
		} else { addActionMessage("Co� tu kombinujesz."); }
    	
    }
    }
    
    private class ChangeActivity implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
				try {
					if(skrytka.getId() != null)
		        		skrytka = EpuapSkrytkaManager.find(skrytka.getId());
		        	else
		        		throw new NullPointerException("Brak skrytki do usuni�cia!");
				
					if(skrytka == null)
		        		throw new NullPointerException("Skrytka o takim id nie istnieje!");
		        	else {
		        		DSApi.context().begin();
		        		boolean available = skrytka.isAvailable();
		        		skrytka.setAvailable(!available);
		        		addActionMessage("Skrytka zosta�a " + (available?"deaktywowana":"aktywowana"));
		        		DSApi.context().session().flush();
		        		DSApi.context().commit();
		        	}
				} catch (Exception e) {
	            	try {
						DSApi.context().rollback();
					} catch (EdmException e1) {
						log.error("", e);
					}
	            	log.error("", e);
	                addActionError(e.getMessage());
	            } 
		}
    }

	public List<EpuapSkrytka> getSkrytki()
	{
		return skrytki;
	}

	public void setSkrytki(List<EpuapSkrytka> skrytki)
	{
		this.skrytki = skrytki;
	}

	public EpuapSkrytka getSkrytka()
	{
		return skrytka;
	}

	public void setSkrytka(EpuapSkrytka skrytka)
	{
		this.skrytka = skrytka;
	}

	public String getNazwaPliku() {
		return nazwaPliku;
	}

	public void setNazwaPliku(String nazwaPliku) {
		this.nazwaPliku = nazwaPliku;
	}
	
	

}
