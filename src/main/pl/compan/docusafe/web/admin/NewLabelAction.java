package pl.compan.docusafe.web.admin;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import edu.emory.mathcs.backport.java.util.Arrays;

public class NewLabelAction extends EventActionSupport
{
	static final long serialVersionUID = -1L;
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(NewLabelAction.class.getPackage().getName(),null);
    
    
    private List<Label> availableLabels;
    private Long id;
	private String name;
	private String cn;
	private String description;
	private Long parentId;
	private String owner;
	private Integer inactionTime;
	private Boolean hidesTasks;
	private Long ancestorId;
	private Boolean modifiable;
	private Boolean deletesAfter;
	private String readRights;
	private String writeRights;
	private Map<String, String> availableRights;
	private boolean zmianaNazwy;


    protected void setup()
    {
    	FillForm fillForm = new FillForm();
    	
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{	
        		zmianaNazwy=AvailabilityManager.isAvailable("etykiety.zmianaNazwyEtykiety");
        		
        		
        		availableLabels = LabelsManager.getSystemLabels(Label.LABEL_TYPE);
        		
        		availableRights = new LinkedHashMap<String, String>();
        		availableRights.put(Label.SYSTEM_LABEL_OWNER,sm.getString("Wszyscy"));
        		availableRights.put(Label.NONE_OWNER, sm.getString("Nikt"));
        		
        		DSDivision[] divs = DSDivision.getAllDivisions();
        		Arrays.sort(divs, new Comparator<DSDivision>(){public int compare(DSDivision o1, DSDivision o2){return o1.getName().compareTo(o2.getName());}});
        		for(DSDivision d : divs)
        		{
        			if(!d.isPosition())
        				availableRights.put(d.getGuid(), d.getName());
        		}
        		
        		if(id != null && !id.equals(0L))
        		{
        			Label label = LabelsManager.findLabelById(id);
        			name = label.getName();
                    cn = label.getCn();
                    description = label.getDescription();
        			parentId = label.getParentId();
        			inactionTime = label.getInactionTime()!=null?label.getInactionTime()/24:null;
        			hidesTasks = label.getHidden();
        			ancestorId = label.getAncestorId();
        			modifiable = label.getModifiable();
        			deletesAfter = label.getDeletesAfter();
        			readRights = label.getReadRights();
        			writeRights = label.getWriteRights();
        		}
        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }
    
    private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(owner==null||owner.equals(""))
        		owner = "#system#";
        	try
        	{
        		if(hidesTasks==null) hidesTasks = false;
        		if(modifiable==null) modifiable = true;
        		if(deletesAfter==null)deletesAfter = false;
        		DSApi.context().begin();
        		Label label;
        		if(AvailabilityManager.isAvailable("etykietyGlobalne.czyWymaganaNazwa")&&(name==null||name.equals(""))){
        			addActionMessage(sm.getString("NazwaEtykietyNieMozeBycPusta"));
        			return;
        		}
        		if(id==null)
        		{
        			if(LabelsManager.existsLabel(name, Label.LABEL_TYPE)){
        				addActionMessage(sm.getString("EtykietaOTakiejNazwieJuzIstnieje"));
        				return;
        			}
        			label = new Label();
        		}
        		else label = LabelsManager.findLabelById(id);
        		
        		label.setAncestorId(ancestorId);
        		label.setDescription(description);
        		label.setHidden(hidesTasks);
        		if(inactionTime==null) label.setInactionTime(null);
        		else label.setInactionTime(inactionTime*24);
        		label.setName(name);
                label.setCn(cn);
        		label.setParentId(parentId);
        		label.setModifiable(modifiable);
        		label.setDeletesAfter(deletesAfter);
        		if(readRights!=null)
        			label.setReadRights(readRights);
        		else
        			label.setReadRights(Label.SYSTEM_LABEL_OWNER);
        		
        		if(writeRights!=null)
        			label.setWriteRights(writeRights);
        		else
        			label.setWriteRights(Label.SYSTEM_LABEL_OWNER);
        		
        		label.setModifyRights(Label.NONE_OWNER);
        		label.setType(Label.LABEL_TYPE);
        		if(id==null)
        			Persister.create(label);
        		LabelsManager.updateBeansForLabel(label, true);
        		DSApi.context().commit();
        	}
        	catch(EdmException e)
        	{
        		DSApi.context().setRollbackOnly();
        		addActionError(sm.getString("NieUdaloSieDodacEtykiety"));
        	}
        	finally
        	{
        		
        	}
        	event.setResult("viewLabels");
        }
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getInactionTime() {
		return inactionTime;
	}

	public void setInactionTime(Integer inactionTime) {
		this.inactionTime = inactionTime;
	}

	public Boolean getHidesTasks() {
		return hidesTasks;
	}

	public void setHidesTasks(Boolean hidesTasks) {
		this.hidesTasks = hidesTasks;
		if(hidesTasks==null) hidesTasks = false;
	}

	public Long getAncestorId() {
		return ancestorId;
	}

	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}

	public List<Label> getAvailableLabels() {
		return availableLabels;
	}

	public void setAvailableLabels(List<Label> availableLabels) {
		this.availableLabels = availableLabels;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getModifiable() {
		return modifiable;
	}

	public void setModifiable(Boolean modifiable) {
		this.modifiable = modifiable;
	}

	public Boolean getDeletesAfter() {
		return deletesAfter;
	}

	public void setDeletesAfter(Boolean deletesAfter) {
		this.deletesAfter = deletesAfter;
	}

	public String getReadRights() {
		return readRights;
	}

	public void setReadRights(String readRights) {
		this.readRights = readRights;
	}

	public String getWriteRights() {
		return writeRights;
	}

	public void setWriteRights(String writeRights) {
		this.writeRights = writeRights;
	}

	public Map<String, String> getAvailableRights() {
		return availableRights;
	}

	public boolean isZmianaNazwy() {
		return zmianaNazwy;
	}

	public void setZmianaNazwy(boolean zmianaNazwy) {
		this.zmianaNazwy = zmianaNazwy;
	}
	
	
    
    
}
