package pl.compan.docusafe.web.admin;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.zebra.ZebraPrinter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * Lista drukarek Zebra
 *
 * Date: 10.07.13
 * Time: 09:38
 */
public class BarcodePrinterListAction extends EventActionSupport
{
	private static final StringManager sm = StringManager.getManager(BarcodePrinterListAction.class.getPackage().getName());
	private List<ZebraPrinter> printers;

	private ZebraPrinter printer;

	private String selectedPrinter;

	private String userPrinterName;
	private String printerName;
	private String barcodeValue;
	private Boolean usePrinter;
	private String printerLanguage;
	private String firstLine;
	private String secondLine;
	private String barcodeType;
	private Map<String, String> availableBarcodes;
	private Map<String, String> availablePrinters;
	private Map<String, String> availableLanguages;
	private String[] fields = new String[0];

	private static final Logger log = LoggerFactory.getLogger(BarcodePrinterListAction.class);

	public static final String OLD_PRINTER_NAME = "Brak nazwy";

	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
				append(OpenHibernateSession.INSTANCE).
				append(fillForm).
				appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doTest").
				append(OpenHibernateSession.INSTANCE).
				append(new Test()).
				append(fillForm).
				appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSave").
				append(OpenHibernateSession.INSTANCE).
				append(new Save()).
				append(fillForm).
				appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDelete").
				append(OpenHibernateSession.INSTANCE).
				append(new Delete()).
				append(fillForm).
				appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				Preferences node = DSApi.context().systemPreferences().node(GlobalPreferences.NODE_NEW_BARCODE_PRINTER);
				//Dla drukarek które zostały zdefiniowane przed wprowadzeniem obsługi wielu drukarek
				if (ZebraPrinter.isOldPrinterDefined(node))
				{
					ZebraPrinter zb = ZebraPrinter.findByUserPrinterName(OLD_PRINTER_NAME);
					if (zb == null)
						zb = new ZebraPrinter();
					zb.retrieveSettings(node);
					zb.setUserPrinterName(OLD_PRINTER_NAME);
					DSApi.context().begin();
					DSApi.context().session().saveOrUpdate(zb);
					node.put("userPrinterName", OLD_PRINTER_NAME);
					DSApi.context().commit();

					DSApi.context().begin();
					node.put("printerId", String.valueOf(zb.getId()));
					DSApi.context().commit();
				}
				printers = ZebraPrinter.getAllSavedPrinters();
				availableBarcodes = NewBarcodePrinterAction.prepareBarcodesMap();

				availableLanguages = NewBarcodePrinterAction.preparePrinterLanguagesMap();
				availablePrinters = NewBarcodePrinterAction.preparePrintersMap();
				if (selectedPrinter != null)
				{
					printer = ZebraPrinter.findByUserPrinterName(selectedPrinter);
					if (printer != null)
						availablePrinters.put(printer.getPrinterName(), printer.getPrinterName());
				}
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Zapisanie zmian w drukarce
	 */
	private class Save implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			ZebraPrinter printerToSave = ZebraPrinter.findByUserPrinterName(selectedPrinter);
			Map<String, Object> prefsToUpdate = Maps.newHashMap();
			if (printerToSave == null)
			{
				addActionError("Nie wybrano drukarki");
				return;
			}
			if (!StringUtils.equals(printerToSave.getLanguage(), printerLanguage))
			{
				printerToSave.setLanguage(printerLanguage);
				prefsToUpdate.put("printerLanguage", printerLanguage);
			}
			if (!StringUtils.equals(printerToSave.getUserPrinterName(),userPrinterName))
			{
				printerToSave.setUserPrinterName(userPrinterName);
				prefsToUpdate.put("userPrinterName", userPrinterName);
			}
			if (!StringUtils.equals(printerToSave.getPrinterName(), printerName))
			{
				printerToSave.setPrinterName(printerName);
				prefsToUpdate.put("printerName", printerName);
			}
			if (!StringUtils.equals(printerToSave.getBarcodeType(), barcodeType))
			{
				printerToSave.setBarcodeType(barcodeType);
				prefsToUpdate.put("barcodeType", barcodeType);
			}
			if (!StringUtils.equals(printerToSave.getBarcodeValue(), barcodeValue))
			{
				printerToSave.setBarcodeValue(barcodeValue);
				prefsToUpdate.put("barcodeValue", barcodeValue);
			}
			if (!StringUtils.equals(printerToSave.getFirstLine(), firstLine))
			{
				printerToSave.setFirstLine(firstLine);
				prefsToUpdate.put("firstLine", firstLine);
			}
			if (!StringUtils.equals(printerToSave.getSecondLine(), secondLine))
			{
				printerToSave.setSecondLine(secondLine);
				prefsToUpdate.put("secondLine", secondLine);
			}
			if (printerToSave.getUsePrinter() != usePrinter)
			{
				printerToSave.setUsePrinter(usePrinter == null ? false : usePrinter);
				prefsToUpdate.put("usePrinter", usePrinter);
			}
			printerToSave.setDefinedFields(Arrays.asList(fields));
			printerToSave.updatePrefsForDatabase(prefsToUpdate);
			printerToSave.savePrinterToDatabase();

			selectedPrinter = printerToSave.getUserPrinterName();
			addActionMessage(sm.getString("DrukarkaZostalaZapisana"));
		}
	}

	private class Test implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			ZebraPrinter zb = ZebraPrinter.findByUserPrinterName(selectedPrinter);
			if (zb != null)
			{
				try
				{
					zb.init();
					Map<String,String> fields = new LinkedHashMap<String,String>();
					fields.put("numer pisma","13095");
					fields.put("data przyj�cia","24-08-2009 11:26");
					fields.put("przyjmuj�cy","Bogus�aw Misztal");
					fields.put("login","8848077e1234bba7a8e");
					fields.put("has�o","25ea5847");
					zb.setFields(fields);
					zb.print();
				} catch (EdmException e1)
				{
					log.error(e1.getMessage(), e1);
				}
			}
			else
				addActionError("Nie wybrano drukarki");

			addActionMessage(sm.getString("DrukujeStroneTestowa", zb.getUserPrinterName()));

        }
	}

	private class Delete implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			ZebraPrinter printerToDelete = ZebraPrinter.findByUserPrinterName(selectedPrinter);
			String printersNameToDelete = printerToDelete.getUserPrinterName();
			if (printerToDelete != null)
			{
				printerToDelete.deletePrefsFromDatabase();
				printerToDelete.deletePrinterFromDatabase();
			}
			else
				addActionError("BrakDrukarkiDoUsuniecia");
			selectedPrinter = null;
			addActionMessage(sm.getString("DrukarkaZostalaUsunieta", printersNameToDelete));
		}
	}
	public String getSelectedPrinter()
	{
		return selectedPrinter;
	}

	public void setSelectedPrinter(String selectedPrinter)
	{
		this.selectedPrinter = selectedPrinter;
	}

	public List<ZebraPrinter> getPrinters()
	{
		return printers;
	}

	public void setPrinters(List<ZebraPrinter> printers)
	{
		this.printers = printers;
	}

	public Map<String, String> getAvailableBarcodes()
	{
		return availableBarcodes;
	}

	public void setAvailableBarcodes(Map<String, String> availableBarcodes)
	{
		this.availableBarcodes = availableBarcodes;
	}

	public Map<String, String> getAvailablePrinters()
	{
		return availablePrinters;
	}

	public void setAvailablePrinters(Map<String, String> availablePrinters)
	{
		this.availablePrinters = availablePrinters;
	}

	public Map<String, String> getAvailableLanguages()
	{
		return availableLanguages;
	}

	public void setAvailableLanguages(Map<String, String> availableLanguages)
	{
		this.availableLanguages = availableLanguages;
	}

	public ZebraPrinter getPrinter()
	{
		return printer;
	}

	public void setPrinter(ZebraPrinter printer)
	{
		this.printer = printer;
	}

	public String getBarcodeType()
	{
		return barcodeType;
	}

	public void setBarcodeType(String barcodeType)
	{
		this.barcodeType = barcodeType;
	}

	public String getUserPrinterName()
	{
		return userPrinterName;
	}

	public void setUserPrinterName(String userPrinterName)
	{
		this.userPrinterName = userPrinterName;
	}

	public String getPrinterName()
	{
		return printerName;
	}

	public void setPrinterName(String printerName)
	{
		this.printerName = printerName;
	}

	public String getBarcodeValue()
	{
		return barcodeValue;
	}

	public void setBarcodeValue(String barcodeValue)
	{
		this.barcodeValue = barcodeValue;
	}

	public Boolean getUsePrinter()
	{
		return usePrinter;
	}

	public void setUsePrinter(Boolean usePrinter)
	{
		this.usePrinter = usePrinter;
	}

	public String getPrinterLanguage()
	{
		return printerLanguage;
	}

	public void setPrinterLanguage(String printerLanguage)
	{
		this.printerLanguage = printerLanguage;
	}

	public String getFirstLine()
	{
		return firstLine;
	}

	public void setFirstLine(String firstLine)
	{
		this.firstLine = firstLine;
	}

	public String getSecondLine()
	{
		return secondLine;
	}

	public void setSecondLine(String secondLine)
	{
		this.secondLine = secondLine;
	}

	public String[] getFields()
	{
		return fields;
	}

	public void setFields(String[] fields)
	{
		this.fields = fields;
	}


}

