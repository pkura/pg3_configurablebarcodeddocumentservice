package pl.compan.docusafe.web.admin;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.Pager.LinkVisitor;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;



/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ApplicationSettingsAction.java,v 1.58 2010/07/14 13:35:15 mariuszk Exp $
 */
public class ManageEpuapErrorDocumentAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(ManageEpuapErrorDocumentAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ManageEpuapErrorDocumentAction.class.getPackage().getName(),null);
	
	private List<EpuapExportDocument> errorDocuments;
	private Long[] ids;
	
	private Pager pager;
	private int offset;
	public static final int LIMIT = 15;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            
            try
            {
            	DSApi.context().begin();
            	if(ids == null || ids.length < 1)
            	   throw new EdmException("Nie wybrano dokumentów");
               
            	for (Long id : ids) 
            	{
            		EpuapExportDocument doc = EpuapExportDocument.find(id);
            		doc.setStatus(EpuapExportDocument.STATUS_DO_WYSYLKI);
            	}
            	DSApi.context().commit();
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
            	addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
               
            }
        }
    }
    
    public String getStatusAsString(Integer id)
    {
    	return EpuapExportDocument.statusAsString(id);
    }

    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	errorDocuments = EpuapExportDocument.erroDocuments(offset, LIMIT);
            	LinkVisitor linkVisitor = new Pager.LinkVisitor() {	
    				public String getLink(int offset) 
    				{
    					return offset + "";
    				}
    			};
    			
    			pager = new Pager(linkVisitor, offset, LIMIT, errorDocuments.size(), 10);
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(),e);
                addActionError(e.getMessage()); 
            }
        }
    }


	public List<EpuapExportDocument> getErrorDocuments() {
		return errorDocuments;
	}


	public void setErrorDocuments(List<EpuapExportDocument> errorDocuments) {
		this.errorDocuments = errorDocuments;
	}


	public Long[] getIds() {
		return ids;
	}


	public void setIds(Long[] ids) {
		this.ids = ids;
	}
}
