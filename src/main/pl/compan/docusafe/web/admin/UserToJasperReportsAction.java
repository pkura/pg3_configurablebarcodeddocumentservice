package pl.compan.docusafe.web.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.zxing.common.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.UserToJasperReports;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.reports.JasperReportsKind;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa dla widoku user-to-jasper-reports, powi�zanie u�ytkownik�w do raport�w
 * tabela w bazie DSO_USER_TO_JASPER_REPORT
 * 
 * @author marek.wojciechowski@docusafe.pl
 */

@SuppressWarnings("serial")
public class UserToJasperReportsAction extends EventActionSupport {

	private static Logger log = LoggerFactory.getLogger(UserToJasperReportsAction.class);

	private List<ValueToList> list_user_to_jasper_report;
	private List<ValueToList> users;
	private List<ValueToList> report;
	private String usersReportList;
	private String usersList;
	private String sortUsersList;
	private String reportList;
	private Boolean canView = false;
	private Boolean sorted = false;
	private Map<Integer, String> uprawnienia;
	private List<JasperReportsKind> listKindsReports;
	private Map<Long, String> reportMap;

	@Override
	protected void setup() {

		try {
			setRaports();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(fillForm).appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doAdd").append(OpenHibernateSession.INSTANCE)
				.append(new Add()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doDelete").append(OpenHibernateSession.INSTANCE)
				.append(new Delete()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSortByUser").append(OpenHibernateSession.INSTANCE)
				.append(new SortByUser()).append(fillForm)
				.appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				if (!sorted) {
					if (!DSApi.isContextOpen())
						DSApi.openContextIfNeeded();
					else
						DSApi.beginTransacionSafely();

					SearchResults<DSUser> userSearchResult = DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true);
					users = new ArrayList<ValueToList>(userSearchResult.count());
					while (userSearchResult.hasNext()) {
						DSUser user = (DSUser) userSearchResult.next();
						String name = user.asLastnameFirstnameName();
						Long id = user.getId();
						users.add(new ValueToList(name, id.toString(), id));
					}

					list_user_to_jasper_report = new ArrayList<ValueToList>();
					List<UserToJasperReports> list = getUsersToJasperReportsList();
					for (UserToJasperReports utjr : list) {
						Long id = utjr.getId();
						Long reportId = utjr.getReportId();
						String reportName = utjr.getReportName();
						DSUser userh = DSUser.findById(utjr.getUserId());

						list_user_to_jasper_report.add(new ValueToList(userh.asLastnameFirstname() + " - Przypisany do Raportu - " + reportName, id.toString(), reportId));
					}

					if (DSApi.context().inTransaction())
						DSApi.context().commit();
				}
			} catch (EdmException e) {
				log.error("", e);
			}
		}
	}

	private void setRaports() throws EdmException {
		report = new ArrayList<ValueToList>();

		 if (!DSApi.isContextOpen())
		 DSApi.openAdmin();
		 else
		 DSApi.beginTransacionSafely();

		 listKindsReports = getReportsKinds();
		 reportMap = new HashMap<Long, String>();
		 
		for (JasperReportsKind kind : listKindsReports) {
			report.add(new ValueToList(kind.getTitle(), kind.getId().toString(), kind.getId()));
			reportMap.put(kind.getId(), kind.getTitle());
		}
	}

	private class Add implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				if (!DSApi.isContextOpen())
					DSApi.openAdmin();
				else
					DSApi.beginTransacionSafely();

					try {

						long userId = Long.parseLong(usersList);
						long reportId = Long.parseLong(reportList);

                        List<UserToJasperReports> list = getUserCriteriaList(userId, reportId);

                        if (!list.isEmpty()) {
                            addActionError("U�ytkownik jest ju� przypisany do tego raportu - Aby zmieni� uprawnienia usu� i przypisz go od nowa ");
                            DSApi.context().rollback();
                        } else {
                            createJasperReport(userId, reportId, reportMap.get(reportId));
                            DSApi.context().commit();
                        }
					} catch (Exception e) {
						log.error("", e);
						throw new EdmException(e);
					}
					usersList = null;
					reportList = null;
				
			} catch (EdmException e) {
				log.error("", e);
			}
		}

	}

    public static void createJasperReport(long userId, long reportId, String reportName) throws EdmException {
        DSUser user = DSUser.findById(userId);
        String userLastname = user.getLastname();
        String userFirstname = user.getFirstname();
        String userLastNameFirstName = user.asLastnameFirstname();

        UserToJasperReports wpis = new UserToJasperReports();
        wpis.setReportId(reportId);
        wpis.setReportName(reportName);
        wpis.setUserId(userId);
        wpis.setUserName(userLastNameFirstName);
        wpis.setUserFirstname(userFirstname);
        wpis.setUserLastname(userLastname);
        wpis.setUserExternalName(user.getExternalName());
        DSApi.context().session().save(wpis);
    }

    private class Delete implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {

        	try
        	{
        		if (!DSApi.isContextOpen())
        			DSApi.openAdmin();
        		else
        			DSApi.beginTransacionSafely();
        		
        		if (usersReportList != null){
        			
        			PreparedStatement ps = null;
        			try
        			{
        				
    					ps = DSApi.context().prepareStatement("DELETE FROM DSO_USER_TO_JASPER_REPORT WHERE ID = ? ");
    					ps.setLong(1, Long.parseLong(usersReportList));
    					ps.execute();
        				
        			}
        			catch (Exception e)
        			{
        				log.error("", e);
        				throw new EdmException(e);
        			}
        			finally
        			{
        				DbUtils.closeQuietly(ps);
        			}
        			
        			usersReportList = null;
        		}        	
        	}
        	catch (EdmException e) 
        	{
        		log.error("", e);
			}
		}
	}
	
	private class SortByUser implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {

		}

	}

	public static List<UserToJasperReports> getUsersToJasperReportsList() throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UserToJasperReports.class);
		return criteria.list();
	}

	public static List<UserToJasperReports> getUserCriteriaList(long userId,
			long reportId) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UserToJasperReports.class);
		criteria.add(Restrictions.eq("reportId", reportId));
		criteria.add(Restrictions.eq("userId", userId));

		return criteria.list();
	}

	public static List<JasperReportsKind> getReportsKinds() throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(JasperReportsKind.class);
		return criteria.list();
	}

	@SuppressWarnings("unused")
	public class ValueToList {
		private String value;
		private String key;
		private long reportId;

		public ValueToList() {
			value = "";
			key = "";
			reportId = 0;
		}

		public ValueToList(String value, String key, Long reportId) {
			this.value = value;
			this.key = key;
			this.reportId = reportId;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}

		public void setReportId(long reportId) {
			this.reportId = reportId;
		}

		public long getReportId() {
			return reportId;
		}
	}

	public List<ValueToList> getList_user_to_jasper_report() {
		return list_user_to_jasper_report;
	}

	public void setList_user_to_jasper_report(
			List<ValueToList> list_user_to_jasper_report) {
		this.list_user_to_jasper_report = list_user_to_jasper_report;
	}

	public List<ValueToList> getUsers() {
		return users;
	}

	public void setUsers(List<ValueToList> users) {
		this.users = users;
	}

	public List<ValueToList> getReport() {
		return report;
	}

	public void setReport(List<ValueToList> report) {
		this.report = report;
	}

	public String getUsersReportList() {
		return usersReportList;
	}

	public void setUsersReportList(String usersReportList) {
		this.usersReportList = usersReportList;
	}

	public String getUsersList() {
		return usersList;
	}

	public void setUsersList(String usersList) {
		this.usersList = usersList;
	}

	public String getSortUsersList() {
		return sortUsersList;
	}

	public void setSortUsersList(String sortUsersList) {
		this.sortUsersList = sortUsersList;
	}

	public String getReportList() {
		return reportList;
	}

	public void setReportList(String reportList) {
		this.reportList = reportList;
	}

	public Boolean getSorted() {
		return sorted;
	}

	public void setSorted(Boolean sorted) {
		this.sorted = sorted;
	}

	public Map<Integer, String> getUprawnienia() {
		return uprawnienia;
	}

	public void setUprawnienia(Map<Integer, String> uprawnienia) {
		this.uprawnienia = uprawnienia;
	}

}
