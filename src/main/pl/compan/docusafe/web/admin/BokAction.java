package pl.compan.docusafe.web.admin;

import org.apache.commons.logging.LogFactory;

import bsh.This;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowResourceNotFoundException;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-06-13 14:19:56 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BokAction.java,v 1.9 2008/10/06 09:31:15 pecet4 Exp $
 */
public class BokAction extends EventActionSupport
{
    private String bokDivisionPrettyPath;
    private String divisionGuid;
    private boolean canPick;
    private String treeHtml;
    
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPick").
            append(OpenHibernateSession.INSTANCE).
            append(new Pick()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
            try
            {
                String bokGuid = GlobalPreferences.getBokDivisionGuid();

                // wyb�r aktualnie wy�wietlanego dzia�u
                DSDivision targetDivision = null;
                if (divisionGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(divisionGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                if (targetDivision == null && bokGuid != null)
                {
                    try
                    {
                        targetDivision = DSDivision.find(bokGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                DSDivision bokDivision = null;
                if (bokGuid != null)
                {
                    try
                    {
                        bokDivision = DSDivision.find(bokGuid);
                    }
                    catch (DivisionNotFoundException e)
                    {
                    }
                }

                // klasa tworz�ca urle dla element�w drzewa
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    {
                        try
                        {
                            StringBuilder url =
                                new StringBuilder(
                                    ServletActionContext.getRequest().getContextPath()+
                                    "/admin/bok.action?" +
                                    "divisionGuid="+((DSDivision) element).getGuid());

                            return url.toString();
                        }
                        catch (Exception e)
                        {
                            event.getLog().error(e.getMessage(), e);
                            return null;
                        }
                    }
                };

                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true, false, false);

                treeHtml = tree.generateTree();

                canPick = targetDivision != null && targetDivision.isPosition() &&
                    (bokDivision == null || !bokDivision.getGuid().equals(targetDivision.getGuid()));
                bokDivisionPrettyPath = bokDivision != null ? bokDivision.getPrettyPath() : null;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Pick implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (divisionGuid == null)
                addActionError(sm.getString("NieWybranoStanowiska"));

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                // je�eli poprzednio inny dzia� by� okre�lony jako obs�uga BOK,
                // przywracam w nim poprzednie mapowania u�ytkownik�w
                String oldBokGuid = GlobalPreferences.getBokDivisionGuid();
                if (oldBokGuid != null)
                {

                    try
                    {
                    	WorkflowFactory.removeParticipantToUsernameMappings(null, null, "ds_guid_"+oldBokGuid);
                    }
                    catch (WorkflowResourceNotFoundException e)
                    {
                    	LogFactory.getLog("eprint").debug("", e);
                    }

                    try
                    {
                        DSUser[] users = DSDivision.find(oldBokGuid).getUsers(false);
                        for (int i=0; i < users.length; i++)
                        {
                            WorkflowFactory.addParticipantToUsernameMapping(null, null,
                                "ds_guid_"+oldBokGuid, users[i].getName());
                        }
                    }
                    catch (DivisionNotFoundException e)
                    {
                    	LogFactory.getLog("eprint").debug("", e);
                    }
                }

                DSDivision bokDivision = DSDivision.find(divisionGuid);
                GlobalPreferences.setBokDivisionGuid(bokDivision.getGuid());

                DSUser bokUser = null;
                try
                {
                    bokUser = UserFactory.getInstance().createSystemUser("$bok");
                }
                catch (DuplicateNameException e)
                {
                    bokUser = UserFactory.getInstance().findSystemUser("$bok");
                }

                bokDivision.addUser(bokUser);
                bokDivision.update();

                DSDivision[] divisions = bokUser.getDivisions();
                for (int i=0; i < divisions.length; i++)
                {
                    divisions[i].removeUser(bokUser);
                }

                // workflow

                // usuwam dotychczasowe mapowania workflow na u�ytkownik�w w tym
                // stanowisku i dodaj� jedno mapowanie - na u�ytkownika $bok
                try
                {
                    WorkflowFactory.removeParticipantToUsernameMappings(null, null, "ds_guid_"+bokDivision.getGuid());
                }
                catch (WorkflowResourceNotFoundException e)
                {
                }

                WorkflowFactory.addParticipantToUsernameMapping(null, null, "ds_guid_"+bokDivision.getGuid(), "$bok");

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getBokDivisionPrettyPath()
    {
        return bokDivisionPrettyPath;
    }

    public boolean isCanPick()
    {
        return canPick;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getTreeHtml()
    {
        return treeHtml;
    }
}
