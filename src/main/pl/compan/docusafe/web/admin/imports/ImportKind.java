package pl.compan.docusafe.web.admin.imports;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import pl.compan.docusafe.core.imports.ImportEntryStatus;
import pl.compan.docusafe.core.imports.KeyValueEntity;

/**
 * Pomocnicza klasa trzymaj�ca informacje o imporcie
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ImportKind {

    private Predicate<KeyValueEntity> predicate = Predicates.alwaysTrue();
    private Function<KeyValueEntity, ImportEntryStatus> consumer;
    private Function<String[], String[]> headerFilter = Functions.identity();
    String name;

    /**
     * Zwraca "przyjazn�" nazw� importu.
     * @return
     */
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Predicate<KeyValueEntity> getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate<KeyValueEntity> predicate) {
        this.predicate = predicate;
    }

    public Function<KeyValueEntity, ImportEntryStatus> getConsumer() {
        return consumer;
    }

    public void setConsumer(Function<KeyValueEntity, ImportEntryStatus> consumer) {
        this.consumer = consumer;
    }

    public Function<String[], String[]> getHeaderFilter() {
        return headerFilter;
    }

    public void setHeaderFilter(Function<String[], String[]> headerFilter) {
        this.headerFilter = headerFilter;
    }

    @Override
    public String toString() {
        return name;
    }
}
