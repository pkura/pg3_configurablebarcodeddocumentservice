package pl.compan.docusafe.web.admin.imports;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.imports.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FileImportAction extends EventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(FileImportAction.class);
    private Map<String, ImportKind> importKinds;
    void initImports() {
        HashMap<String, ImportKind> imports = Maps.newHashMap();

        //TO wczytywa� co� takiego z innego miejsca np z xml'a,
        //doda� jaki� manager do obs�ugi tego typu import�w
        insertDicInvoiceImport(imports);
        insertContractIcImport(imports);

        importKinds = Collections.unmodifiableMap(imports);
    }

    private static void insertDicInvoiceImport(HashMap<String, ImportKind> imports) {
        ImportKind kind = new ImportKind();
        kind.setConsumer(KeyValueImports.dicInvoice());
        kind.setHeaderFilter(HeaderFilters.buildFilter()
                .possibleKeys("NAME","NUMBER","COUNTRY","NIP","NAZ_S","DAT_D","") //NAZ_S i DAT_D ignorowane
                .requiredKeys("NAME","NUMBER","NIP")
                .alias("NAME","NAZ")
                .alias("NIP","NIP_SZU")
                .alias("COUNTRY","KRJ_KOD")
                .alias("NUMBER","KOD")
                .create());
        kind.setName("Import wpis�w do s�ownika kontrahent�w");
        imports.put("dicInvoice", kind);
    }

    private static void insertContractIcImport(HashMap<String, ImportKind> imports){
        ImportKind kind;
        try {
            if(!DocumentKind.isAvailable("contract_ic")){
                return;
            }

            kind = new ImportKind();
            kind.setConsumer(KeyValueImports.contractIc());
            kind.setHeaderFilter(HeaderFilters.buildFilter()
                    .possibleKeys("BOX_NUMBER_HEADER","BOX_BARCODE","DOCUMENT_NUMBER_HEADER",
                            "DOCUMENT_NUMBER","BARCODE_HEADER","BARCODE","SUBTYPE_HEADER","SUBTYPE","DOCUMENT_DATE_HEADER",
                            "DOCUMENT_DATE","FILE","NULL","")
                    .requiredKeys("BOX_BARCODE","DOCUMENT_NUMBER","SUBTYPE","DOCUMENT_DATE","FILE")
                    .create());
            kind.setName("Import um�w");
        } catch (EdmException e) {
            LOG.error(e.getMessage(), e);
            return;
        }

        imports.put("contractIc", kind);
    }

    private String importKindKey;
    private FormFile file;
    private ImportResult result;

    @Override
    protected void setup() {
         FillForm fillForm = new FillForm();

         registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doImport").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new Import()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            initImports();
            log.info("FILE " + file);
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    private class Import extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            log.info("Import");
            if(StringUtils.isBlank(importKindKey) || !importKinds.containsKey(importKindKey)){
                throw new EdmException("Nie wybrano importu");
            }
            ImportKind ik = importKinds.get(importKindKey);

            File importFile = file.getFile();
            if(importFile.getName().endsWith(".csv")){
                csvImport(ik, importFile);
                
                //poni�ej dopisywa� obs�ug� innych rodzaj�w plik�w do importu
                //(w else ifach)
            } else if(importFile.getName().endsWith(".xls")) {
                xlsImport(ik, importFile);
            } else {
                throw new EdmException("Nieobs�ugiwany format pliku");
            }
        }

        private void xlsImport(ImportKind ik, File importFile) throws Exception {
            XlsWithHeaderFileProvider provider
                = new XlsWithHeaderFileProvider(importFile);
            provider.setHeaderFilter(ik.getHeaderFilter());

            KeyValueImport importTask = new KeyValueImport(provider, ik.getConsumer());

            result = importTask.call();

            resultReport();
        }

        private void csvImport(ImportKind ik, File importFile) throws Exception {
            CsvWithHeaderFileProvider provider
                = new CsvWithHeaderFileProvider(importFile);
            provider.setHeaderFilter(ik.getHeaderFilter());

            KeyValueImport importTask = new KeyValueImport(provider, ik.getConsumer());

            result = importTask.call();

            resultReport();
        }

        private void resultReport() {
            addActionMessage("Liczba rekord�w w pliku : " + result.getTotalCount() + " z czego : ");
            if(result.getAddedCount() != 0){
                addActionMessage("Liczba dodanych wpis�w : " + result.getAddedCount());
            }
            if(result.getDuplicateCount() != 0){
                addActionMessage("Liczba duplikat�w : " + result.getDuplicateCount());
            }
            if(result.getErrorCount() != 0){
                addActionMessage("Liczba b��d�w : " + result.getErrorCount());
            }
            if(result.getUpdatedCount() != 0){
                addActionMessage("Liczba uaktualnionych wpis�w : " + result.getUpdatedCount());
            }
            for(String errorMessage: result.getErrorMessages()){
                addActionError(errorMessage);
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

    public Map<String, ImportKind> getImportKinds(){
        return importKinds;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

      public String getImportKindKey() {
        return importKindKey;
    }

    public void setImportKindKey(String importKindKey) {
        this.importKindKey = importKindKey;
    }
}