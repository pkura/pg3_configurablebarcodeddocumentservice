package pl.compan.docusafe.web.admin;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.address.Address;
import pl.compan.docusafe.core.address.Floor;
import pl.compan.docusafe.core.address.Phone;
import pl.compan.docusafe.core.address.UserLocation;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje tworzenia i edycji danych adresowych.
 * Powi�zana jest z widokiem /admin/edit-address.jsp, /admin/edit-phones-address.jsp,
 *  /admin/edit-floors-address.jsp
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditAddressAction extends EventActionSupport
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditAddressAction.class);
	
	/**
	 * Za�adowanie �r�de� tekstowych z komunikatami z bierz�cego pakietu
	 */
	private static StringManager sm = 
		StringManager.getManager(EditAddressAction.class.getPackage().getName());
	
	/**
	 * Ci�g znak�w okre�laj�cy komunikat w przypadku b��dnego wype�nienia p�l formularza
	 * @see #checkFields()
	 * @see Save#actionPerformed(ActionEvent)
	 */
	private static final String FIELDS_ERROR = "FieldsError";
	
	/**
	 * Sk�adowa przechowuj�ca zak�adki dla widoku
	 */
	private static final Map<String, String> TABS = new LinkedHashMap<String, String>();
	
	/**
	 * Przypisanie warto�ci dla mapy zak�adek
	 */
	static
	{
		TABS.put("/admin/edit-address.action", sm.getString("DodajEdytujDaneAdresowe"));
		TABS.put("/admin/edit-phones-address.action", sm.getString("DodajEdytujTelefony"));
		TABS.put("/admin/edit-floors-address.action", sm.getString("DodajEdytujPietra"));
	}
	
	/**
	 * Sk�adowa przechowuj�ca list� zak�adek dost�pnych w widoku zarz�dzania danym adresem
	 */
	private List<Tab> tabs;
	
	/**
	 * Sk�adowa przechowuj�ca identyfikator aktualnej zak�adki
	 */
	private int tabId;
	
	/**
	 * Obiekt adresu, pliki mapuj�ce znajduj� si� w pakiecie pl.compan.docusafe.core.address
	 * @see Address
	 */
	private Address address;
	
	/**
	 * Identyfikatory numer�w telefon�w lub pi�ter, kt�re u�ytkownik zaznaczy� do usuni�cia
	 */
	private Long[] deleteIds;
	
	/**
	 * Sk�adowa wykorzystawana podczas tworzenia nowego obiektu telefonu przez
	 * u�ytkownika
	 */
	private Phone phone;
	
	/**
	 * Sk�adowa wykorzystywana podczas tworzenia nowego obiektu pi�tra przez u�ytkownika
	 */
	private Floor floor;
	
	/**
	 * Zdj�cie mapka dojazdu
	 */
	private FormFile file;
	
	@Override
	protected void setup() 
	{
		LoadTabs loadTabs = new LoadTabs();
		LoadPhones loadPhones = new LoadPhones();
		LoadFloors loadFloors = new LoadFloors();
		
		// rejestracja domy�lnej akcji
		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(new FillFrom()).
			append(loadTabs).
			append(loadPhones).
			append(loadFloors).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		// rejestracja akcji zapisania zmian lub utworzenia nowego adresu
		registerListener("doSave").
			append(OpenHibernateSession.INSTANCE).
			append(new Save()).
			append(loadTabs).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		// rejestracja akcji usuni�cia zaznaczonych telefon�w
		registerListener("doDeletePhones").
			append(OpenHibernateSession.INSTANCE).
			append(loadTabs).
			append(new DeletePhones()).
			append(loadPhones).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		// rejestracja akcji dodania nowego telefonu
		registerListener("addPhone").
			append(OpenHibernateSession.INSTANCE).
			append(loadTabs).
			append(new AddPhone()).
			append(loadPhones).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		// rejestracja akcji usuni�cia wybranych pi�ter
		registerListener("doDeleteFloors").
			append(OpenHibernateSession.INSTANCE).
			append(loadTabs).
			append(new DeleteFloors()).
			append(loadFloors).
			appendFinally(CloseHibernateSession.INSTANCE);
		
		// rejestracja akcji dodania nowego pi�tra dla adresu
		registerListener("addFloor").
			append(OpenHibernateSession.INSTANCE).
			append(loadTabs).
			append(new AddFloor()).
			append(loadFloors).
			appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Akcja wype�niaj�ca dane dla formularza
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class FillFrom implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{				
				if (address == null || address.getId() == null || address.getId() <= 0)
				{
					// je�li nie jest ustawiony parametr address.id w url'u
					address = new Address();
				}	
				else
				{
					address = Address.find(address.getId());
					if (address == null)
					{
						// rzuca wyj�tek je�li nie znaleziono adresu o podanym ID
						throw new EdmException(sm.getString("BrakAdresuID" + ": " + address.getId()));
					}
				}
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� zarz�dzania zak�adkami w widoku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadTabs implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			tabs = new ArrayList<Tab>();
			int i = 0;
			for (Entry<String, String> entry : TABS.entrySet())
			{
				boolean active = (tabId == i) ? true : false;
				tabs.add(new Tab(entry.getValue(), entry.getValue(), 
					entry.getKey() + "?tabId=" + i + "&address.id=" + address.getId(), active));
				i++;
			}
		}
	}
	
	/**
	 * Akcja zapisuj�ca nowe dane adresowe do ju� istniej�cego adresu lub
	 * tworzy nowy
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class Save implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (address == null)
					throw new EdmException("AdresJestPusty");
				
				if (address.getId() != null && address.getId() > 0)
				{
					// za�adowanie mapki dla widoku
					Image addressImage = (Image) DSApi.context().session()
							.createQuery("select a.image from " + Address.class.getName() + " a "
									+ " where a.id = ?")
							.setLong(0, address.getId())
							.uniqueResult();
					address.setImage(addressImage);
				}
				
				// sprawdzenie poprawno�ci p�l, rzuca wyj�tek EdmEception
				checkAddressFields();
				// sprawdzenie mapki dojazdu
				checkImage();
				
				if (address.getId() != null && address.getId() > 0)
				{
					DSApi.context().session().update(address);
				}
				else
				{
					// zapisanie nowego adresu
					DSApi.context().session().save(address);
				}
				
				DSApi.context().session().flush();
				
				// zapisanie pliku mapki dojazdu
				saveImage();
				
				addActionMessage(sm.getString("PoprawnieZapisanoZmiany"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FIELDS_ERROR))
					addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Metoda sprawdzaj�ca poprawno�� danych adresowych.
		 * 
		 * @throws EdmException
		 */
		private void checkAddressFields() throws EdmException
		{
			// sprawdzenie nazwy adresu
			if (address.getName() != null && address.getName().equals(""))
				addActionError(sm.getString("BrakNazwyAdresu"));
			// sprawdzenie nazwy organizacji
			if (address.getOrganization() == null || address.getOrganization().equals(""))
				addActionError(sm.getString("BrakNazwyOrganizacji"));
			// sprawdzenie nazwy ulicy
			if (address.getStreet() == null || address.getStreet().equals(""))
				addActionError(sm.getString("BrakNazwyUlicy"));
			// sprawdzenie kodu pocztowego
			if (address.getZipCode() == null || !address.getZipCode().matches("^[\\d]{2}-[\\d]{3}$"))
				addActionError(sm.getString("NieprawidlowyKodPocztowy"));
			// sprawdzenie nazwy miasta
			if (address.getCity() == null || address.getCity().equals(""))
				addActionError(sm.getString("BrakNazwyMiasta"));
			// sprawdzenie nazwy regionu
			if (address.getRegion() == null || address.getRegion().equals(""))
				addActionError(sm.getString("BrakNazwyRegionu"));
			
			if (hasActionErrors())
				throw new EdmException(FIELDS_ERROR);
		}
		
		/**
		 * Metoda sprawdzaj�ca czy przes�any plik jest obrazem, je�li nie jest rzuca wyj�tek
		 * EdmException
		 * 
		 * @throws EdmException
		 */
		public void checkImage() throws EdmException
		{
			if (file != null && file.getFile() != null)
			{	
				// sprawdzenie czy przes�any plik jest obrazem
				if (!file.getContentType().matches("^image/.+$"))
				{
					addActionError(sm.getString("PlikNieJestObrazem"));
					throw new EdmException(FIELDS_ERROR);
				}
			}
		}
		
		/**
		 * Metoda sprawdzaj�ca mapk� dojazdu i zapisuj�ca j� dla danego adresu
		 * 
		 * @throws EdmException
		 */
		private void saveImage() throws EdmException
		{
			if (file != null && file.getFile() != null)
			{	
				try
				{	
					Image image = address.getImage();
					if (image == null)
					{
						image = new Image(file.getNameWithoutExtension(), null, file.getContentType(), file.getName());
						address.setImage(image);
						DSApi.context().session().save(image);
						DSApi.context().session().update(address);
					}
					else
					{
						image.setName(file.getNameWithoutExtension());
						image.setContentType(file.getContentType());
						image.setFilename(file.getName());
						DSApi.context().session().update(image);
					}
					InputStream stream = new FileInputStream(file.getFile());
				//	image.updateImageBinaryStream(stream, stream.available());
					image.updateImage(file.getFile());
					stream.close();
				}
				catch (Exception ex)
				{
					addActionError(sm.getString("BladPodczasZapisuMapkiDojazdu"));
					LOG.error(ex.getMessage(), ex);
					throw new EdmException(FIELDS_ERROR);
				}
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� za�adowania telefon�w przypisanych do danego adresu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadPhones implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try 
			{				
				if (address != null)
					address.loadPhonesList();
			}
			catch (Exception ex)
			{
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� usuni�cia wybranych przez u�ytkownika telefon�w
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeletePhones implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{	
				if (deleteIds == null || deleteIds.length == 0)
					return;
			
				DSApi.context().begin();
				
				// usuni�cie wybranych telefon�w
				for (Long id : deleteIds)
					Phone.find(id).delete();
				
				deleteIds = null;
				
				addActionMessage(sm.getString("PoprawnieUsunietoTelefony"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� utworzenia nowego telefonu (zapisanie
	 * do bazy danych nowego obiektu Phone)
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class AddPhone implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (phone == null || address == null || address.getId() <= 0) 
					return;
				
				DSApi.context().begin();
				
				// sprawdzenie p�l formularza
				checkPhoneFields();
				
				// zapisania do bazy danych
				address = Address.find(address.getId());
				phone.setAddress(address);
				address.getPhones().add(phone);
				DSApi.context().session().save(phone);
				DSApi.context().session().update(address);
				DSApi.context().commit();
				
				phone = null;
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FIELDS_ERROR))
					addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Metoda sprawdzaj�ca pola telefonu
		 * 
		 * @throws EdmException
		 */
		private void checkPhoneFields() throws EdmException
		{
			boolean throwExc = false;
			
			if (phone.getName() == null || phone.getName().equals(""))
			{
				throwExc = true;
				addActionError(sm.getString("BrakPodanejNazwyTelefonu"));
			}
			if (phone.getType() == null || phone.getType().equals(""))
			{
				throwExc = true;
				addActionError(sm.getString("BrakPodanegoTypuTelefonu"));
			}
			if (phone.getNumber() == null || phone.getNumber().equals(""))
			{
				throwExc = true;
				addActionError(sm.getString("BrakPodanegoNumeruTelefonu"));
			}
			
			if (throwExc)
				throw new EdmException(FIELDS_ERROR);
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� za�adowania listy pi�ter danego adresu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadFloors implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (address != null)
					address.loadFloorsList();
				for(Floor f :address.getFloors())
				{
					f.getId();
				}
			}
			catch (EdmException ex)
			{
				LOG.error(ex.getMessage(),ex);
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� usuni�cia wybranych pi�ter
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeleteFloors implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{	
				if (deleteIds == null || deleteIds.length == 0)
					return;
			
				DSApi.context().begin();
				
				// usuni�cie wybranych pi�ter i powi�zanych z nimi relacji
				for (Long id : deleteIds)
				{
					Floor floor = Floor.find(id);
					List<UserLocation> locations = DSApi.context().session()
						.createQuery("from " + UserLocation.class.getName() + " u where u.floor = ?")
						.setParameter(0, floor)
						.list();
					if (locations != null && locations.size() > 0)
					{
						// dla u�ytkownik�w w lokalizacjach kt�re zostan� usuni�te nale�y 
						// przypisa� warto�� null dla pola userLocation
						DSApi.context().session().createQuery("update " + UserImpl.class.getName() 
								+ " u set u.userLocation = :loc where u.userLocation in (:locations)")
								.setParameter("loc", null)
								.setParameterList("locations", locations)
								.executeUpdate();	
						
						// teraz mozna usun�� lokalizacje
						for (UserLocation location : locations)
						{
							// usuni�cie powi�zanych lokalizacji
							Persister.delete(location);
						}
					}
					
					// teraz mo�na usun�� pi�tro
					Persister.delete(floor);
				}
				
				deleteIds = null;
				
				addActionMessage(sm.getString("PoprawnieUsunietoPietra"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� utworzenia nowego pi�tra
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class AddFloor implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (floor == null || address == null || address.getId() <= 0) 
					return;
				
				DSApi.context().begin();
				
				// sprawdzenie p�l formularza
				checkFloorFields();
				// sprawdzenie obrazu pi�tra
				checkFloorImage();
				
				address = Address.find(address.getId());
				floor.setAddress(address);
				address.getFloors().add(floor);
				DSApi.context().session().save(floor);
				DSApi.context().session().update(address);
				DSApi.context().session().flush();
				
				// pr�ba zapisu obrazu pi�tra
				saveFloorImage();
				
				DSApi.context().commit();
				
				floor = null;
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FIELDS_ERROR))
					addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Metoda sprawdzaj�ca poprawno�� p�l formularza dodania nowego pi�tra
		 * 
		 * @throws EdmException
		 */
		public void checkFloorFields() throws EdmException
		{
			if (floor.getName() == null || floor.getName().equals(""))
				addActionError(sm.getString("BrakPodanejNazwyPietra"));
			
			if (floor.getDescription() == null || floor.getDescription().equals(""))
				addActionError(sm.getString("BrakPodanegoOpisuPietra"));
			
			if (hasActionErrors())
				throw new EdmException(FIELDS_ERROR);
		}
		
		/**
		 * Metoda sprawdzaj�ca obraz pi�tra
		 * 
		 * @throws EdmException
		 */
		private void checkFloorImage() throws EdmException
		{
			if (file != null && file.getFile() != null)
			{	
				// sprawdzenie czy przes�any plik jest obrazem
				if (!file.getContentType().matches("^image/.+$"))
				{
					addActionError(sm.getString("PlikNieJestObrazem"));
					throw new EdmException(FIELDS_ERROR);
				}
			}
			else
			{
				addActionError(sm.getString("BrakObrazuPietra"));
				throw new EdmException(FIELDS_ERROR);
			}
		}
		
		/**
		 * Metoda zapisuj�ca obraz pi�tra do bazy danych
		 * 
		 * @throws EdmException
		 */
		private void saveFloorImage() throws EdmException
		{
			try
			{
				Image image = new Image(file.getNameWithoutExtension(), null, 
						file.getContentType(), file.getName());
				floor.setImage(image);
				DSApi.context().session().save(image);
				DSApi.context().session().update(floor);
				image.updateImage(file.getFile());
			//	InputStream stream = new FileInputStream(file.getFile());
			//	image.updateImageBinaryStream(stream, stream.available());
				//stream.close();
			} 
			catch (Exception ex) 
			{
				addActionError(sm.getString("BladPodczasZapisuObrazuPietra"));
				LOG.error(ex.getMessage(), ex);
				throw new EdmException(FIELDS_ERROR);
			}
		}
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� address
	 * 
	 * @return
	 */
	public Address getAddress() 
	{
		return address;
	}
	
	/**
	 * Metoda ustawiaj�ca sk�adow� address
	 * 
	 * @param address
	 */
	public void setAddress(Address address)
	{
		this.address = address;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� file
	 * 
	 * @return
	 */
	public FormFile getFile() 
	{
		return file;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� file
	 * 
	 * @param file
	 */
	public void setFile(FormFile file) 
	{
		this.file = file;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� phone
	 * 
	 * @return
	 */
	public Phone getPhone() 
	{
		return phone;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� phone
	 * 
	 * @param phone
	 */
	public void setPhone(Phone phone) 
	{
		this.phone = phone;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� floor
	 * 
	 * @return
	 */
	public Floor getFloor() 
	{
		return floor;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� floor
	 * 
	 * @param floor
	 */
	public void setFloor(Floor floor) 
	{
		this.floor = floor;
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� deleteIds
	 * 
	 * @return
	 */
	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� deleteIds
	 * 
	 * @param deleteIds
	 */
	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� tabs
	 * 
	 * @return
	 */
	public List<Tab> getTabs() 
	{
		return tabs;
	}

	/**
	 *  Metoda zwracaj�ca sk�adow� tabId
	 * 
	 * @return
	 */
	public int getTabId() 
	{
		return tabId;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� tabId
	 * 
	 * @param tabId
	 */
	public void setTabId(int tabId) 
	{
		this.tabId = tabId;
	}
}
