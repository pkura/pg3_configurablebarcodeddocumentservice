package pl.compan.docusafe.web.admin.registry;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.regtype.Registry;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 *
 */
public class RegistriesAction extends EventActionSupport
{
    // @EXPORT
    private List<Registry> registries;
    
    // @IMPORT
    private String name;
    private String shortName;
    private String cn;
    private String tableName;
    private String requiredRwa;
    private FormFile file;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        /*registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);*/
    }    
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {    
                registries = Registry.list();                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(cn))
                addActionError("Nie podano nazwy kodowej tworzonego rejestru");
         /*   if (StringUtils.isEmpty(name))
                addActionError("Nie podano pe�nej nazwy tworzonego rejestru");
            if (StringUtils.isEmpty(shortName))
                addActionError("Nie podano skr�conej nazwy tworzonego rejestru");
            if (StringUtils.isEmpty(tableName))
                addActionError("Nie podano nazwy tabeli w bazie dla tworzonego rejestru"); */
            if (file == null || !file.sensible())
                addActionError("Nie podano pliku opisuj�cego rejestr");
                
            
            if (hasActionErrors())
                return;

        //    name = StringUtils.trim(name);
        //    shortName = StringUtils.trim(shortName);

            try
            {
                DSApi.context().begin();
                // TODO: narazie ta funkcja sluzy jednoczesnie do twozrenia nowego i update popzredniego 
                // (dlatego ponizsze wylaczone chwilowo) - ROZDZIELIC!!!
               /* if (Registry.findByCn(cn) != null)
                    throw new EdmException("Istnieje ju� rejestr o nazwie kodowej: "+cn);*/

                
                createOrUpdate(cn, tableName, name, shortName, file.getFile(), requiredRwa);
             
                
                DSApi.context().commit();
                addActionMessage("Zapisano podany rejestr");
                
                // czyszczenie formularza
                name = null; 
                shortName = null;
                cn = null;
                tableName = null;
                requiredRwa = null;
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    private void createOrUpdate(String cn, String tablename, String name, String shortName, File file, String requiredRwa) throws EdmException
    {
        Registry registry = Registry.findByCn(cn);
        
        String previousCn = null;
        if (registry == null)
        {            
            registry = new Registry();
        }
        else
            previousCn = registry.getCn();

        registry.setXML(file);
        registry.initialize();
        
        /*registry.setCn(cn);
        registry.setTablename(tablename);
        registry.setName(name);
        registry.setShortName(shortName);
        registry.setRequiredRwa(requiredRwa);*/
        registry.setEnabled(Boolean.TRUE);
                        
        if (previousCn == null)
        {
            if (!cn.equals(registry.getCn()))
                throw new EdmException("Podana nazwa kodowa musi by� identyczna z t� zapisan� w pliku XML");
            if (Registry.findByCn(registry.getCn()) != null)
                throw new EdmException("Istnieje ju� rejestr o podanej nazwie kodowej");
            Persister.create(registry);
        }
        else if (!previousCn.equals(registry.getCn()))
            throw new EdmException("Zaktualizowany rejestr musi mie� tak� sam� nazw� kodow� jak ten dotychczasowy");
            
        
        registry.saveContent(file);
    }
    
    public List<Registry> getRegistries()
    {
        return registries;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public void setRequiredRwa(String requiredRwa)
    {
        this.requiredRwa = requiredRwa;
    }
}
