package pl.compan.docusafe.web.admin;

import static pl.compan.docusafe.core.dockinds.dwr.Field.DWR_PREFIX;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.EmailChannelLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.DSEmailChannelCondition;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Klasa implementuj�ca akcje tworzenia oraz edycji kana��w mailowych.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditEmailChannelAction extends EventActionSupport
{
	/**
	 * Za�adowanie �r�de� tekstowych z komunikatami z bierz�cego pakietu
	 */
	static StringManager sm = StringManager.getManager(EmailChannelsAction.class.getPackage().getName());
	protected static Logger log = LoggerFactory.getLogger(EditEmailChannelAction.class);
	/**
	 * Serial Version UID
	 */
	static final long serialVersionUID = -1L;
	
	/**
	 * Identyfikator kana�u mailowego.
	 */
	private Long id;
	
	/**
	 * Nazwa hosta np. pop3.wp.pl
	 */
	private String hostname;
	
	/**
	 * Nazwa u�ytkownika np. user@com.pl
	 */
	private String username;
	
	/**
	 * Has�o do po��czenia ze skrzynk� mailow�
	 */
	private String password;
	
	/**
	 * ???
	 */
	private String folderName;
	
	/**
	 * Nazwa kodowa kana�u mailowego
	 */
	private String kolejka;
	
	/**
	 * Warunki kana�u mailowego
	 */
	private Vector<DSEmailChannelCondition> conditions;
	
	/**
	 * Pole warunku: Od, Tytu�
	 */
	private String conditionField;
	
	/**
	 * Warto�� pola warunku
	 */
	private String conditionValue;
	
	/**
	 * Operator warunku (and, or)
	 */
	private String conditionOperator;
	
	/**
	 * Rodzaj wiadomo�ci (email, fax)
	 */
	private String contentType;
	
	/**
	 * Mapa przechowuj�ca rodzaje wiadomo�ci
	 */
	private Map<String, String> contents;
	
	/**
	 * ???????????
	 */
	private Map<Integer,String> conditionsDescList;
	private String[] conditionsPicked;
	
	/**
	 * Nazwa protoko�u (pop3, imap, pop3s, imaps)
	 */
	private String protocol;
	
	/**
	 * Mapa przechowuj�ca rodzaje protoko��w
	 */
	private Map<String, String> protocols;
	
	/**
	 * Numer portu kana�u mailowego
	 */
	private int port;
	
	/**
	 * Nazwa folderu skrzynki odbiorczej, domy�lnie ustawiana jest warto�� 'INBOX'
	 */
	private String mbox;
	
	/**
	 * Czy po��czenie b�dzie szyfrowane SSL.
	 */
	private boolean SSL;
	
	/**
	 * Czy dany kana� mailowy ma by� w��czony
	 */
	private boolean enabled;
	
	private String userLogin;
	// KONFIGURACJA SMTP
	
	private String smtpHost;
	private Integer smtpPort;
	private String smtpUsername;
	private String smtpPassword;
	private boolean smtpTLS;
	private boolean smtpSSL;
	private boolean smtpEnabled;
	/**
	 * Rodzaj wiadomo�ci (email, fax)
	 */
	private String redirectUrl;
    // POLA WYMAGANE DO OBSLUGI DOCKINDA @ DWR
    /*
        var dwrParams = {
            context: "dockind",
            action: "new",
            documentType: "<ww:property value='docType'/>",
            dockind: "<ww:property value='documentKindCn'/>",
            documentId: "<ww:property value='documentId'/>",
            docIds: "<ww:property value='dependsDocsJson' escape="false"/>",
            activity: "<ww:property value='activity'/>"
        };
    */
    private Map<String, String> dwrValues;
    private Map<String, String> documentKinds;
    private DocumentKind documentKind;
    private String documentKindCn;
    private Long documentKindId;
    
    /**
     * w�asno�� u�ywana w metodach AbstractDocumentLogic
     */
    private int docType;

	/**
	 * Konfiguracja klasy, rejestracja akcji "nas�uchuj�cych"
	 */
	protected void setup()
    {
		// przygotowanie list dla element�w <select>
		prepareContents();
    	prepareProtocols();
        prepareAvailableDocumentKinds();

    	FillForm fillForm = new FillForm();
    	
    	// rejestracja akcji wype�niaj�cej pola formularza
    	registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

    	// rejestracja akcji zapisania zmian lub utworzenia nowego kana�u mailowego
    	registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append("fillForm",fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        // rejestracja akcji usuni�cie powi�zania pomi�dzy kana�em email a dockindem
        registerListener("doRemoveDockind").
                append(OpenHibernateSession.INSTANCE).
                append(new RemoveDockind()).
                append("fillForm", fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    	
    	// rejestracja akcji dodania warunk�w do kana�u mailowego
    	registerListener("doAddCondition").
    		append(OpenHibernateSession.INSTANCE).
    		append(new AddCondition()).
    		append("fillForm",fillForm).
    		appendFinally(CloseHibernateSession.INSTANCE);
    	
    	// rejestracja akcji usuni�cia warunk�w z kana�u mailowego
    	registerListener("doRemoveConditions").
			append(OpenHibernateSession.INSTANCE).
			append(new Remove()).
			append("fillForm",fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
    	
    	registerListener("doDelateChannel").
		append(OpenHibernateSession.INSTANCE).
		append(new DeleteEmailChannel()).
		append("fillForm",fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);
    }
	 
	/**
	 * Przygotowanie mapy z typami kana��w mailowych (email, fax)
	 */
	private void prepareContents()
	{
		contents = new HashMap<String, String>();
		contents.put("fax", "fax");
		contents.put("email", "email");
	}
	
	/**
	 * Przygotowanie mapy z typami protoko��w
	 */
	private void prepareProtocols()
	{
		protocols = new HashMap<String, String>();
		protocols.put("pop3", "pop3");
		protocols.put("pop3s", "pop3s");
		protocols.put("imap", "imap");
		protocols.put("imaps", "imaps");
    }


    /**
     * Przygotowanie mapy z dockindami, kt�re mog� zosta� powi�zane z kana�em email
     * @throws EdmException
     */
    private void prepareAvailableDocumentKinds(){
        if(AvailabilityManager.isAvailable(EmailChannelLogic.EMAIL_CHANNEL_DWR_PROPERTIES)){
            documentKinds = new HashMap<String, String>();
            try {
                DSApi.openAdmin();
                for(DocumentKind dockind : DocumentKind.list()){
                    if(dockind.logic() instanceof EmailChannelLogic){
                        documentKinds.put(dockind.getCn(), dockind.getName());
                    }
                }
                DSApi._close();
            } catch (EdmException e) {
                LOG.debug(e.getMessage(),e);
            }
        }
    }

    /**
     * Utworzenie mapy z warto�ciami z dockinda (DWR) powi�zanego z kana�em
     * @return
     * @throws EdmException
     */
    private Map<String, String> getValuesFromDWR() throws EdmException {
        Map<String, String> dwrValues = new HashMap<String, String>();
        String dwrValue = null;
        Map dwrFoundParameters = ServletActionContext.getContext().getParameters();
        for(Field dockindField : documentKind.getFields()){
            if(dwrFoundParameters.containsKey(DWR_PREFIX + dockindField.getCn()) &&
                !StringUtils.EMPTY.equals((dwrValue = ((String[]) dwrFoundParameters.get(DWR_PREFIX + dockindField.getCn()))[0]))){
                dwrValues.put(DWR_PREFIX + dockindField.getCn(), dwrValue);
            }
        }
        return dwrValues;
	}
	
	/**
	 * Klasa wewn�trzna wype�niaj�ca pola formularza w przypadku edycji
	 * kana�u mailowego.
	 * 
	 */
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		// je�li jest ustawiony identyfikator kana�u mailowego
        		// wype�niane s� pola formularza.
        		if(id != null)
        		{
        			DSEmailChannel emailChannel = MessageManager.getChannel(id);
        			hostname = emailChannel.getHost();
        			password = emailChannel.getPassword();
        			username = emailChannel.getUser();
        			folderName = emailChannel.getFolderName();
        			kolejka = emailChannel.getKolejka();
        			contentType = emailChannel.getContentType();
        			conditions = MessageManager.getConditionsFromString(emailChannel.getWarunki());
        			protocol = emailChannel.getProtocol();
        			port = emailChannel.getPort();
        			mbox = emailChannel.getMbox();
        			SSL = emailChannel.isSSL();
        			enabled = emailChannel.isEnabled();
        			ServletActionContext.getRequest().getSession().setAttribute("conditions", conditions);
        			prepareConditionsDescList();
        			// KONFIGURACJA SMTP
        			smtpHost = emailChannel.getSmtpHost();
        			smtpPort = emailChannel.getSmtpPort();
        			smtpUsername = emailChannel.getSmtpUsername();
        			smtpPassword = emailChannel.getSmtpPassword();
        			smtpTLS = emailChannel.isSmtpTLS();
        			smtpSSL = emailChannel.isSmtpSSL();
        			smtpEnabled = emailChannel.isSmtpEnabled();
        			userLogin = emailChannel.getUserLogin();
                    // KONFIGURACJA DWR
                    documentKindId = emailChannel.getDocumentKindId();
                }

                // KONFIGURACJA DWR
                //za�adowanie domy�lnych ustawie� dla dockinda (DWR)
                if(documentKinds != null && documentKinds.size() > 0 && documentKindCn == null){
                    documentKindCn = String.valueOf(((HashMap)documentKinds).keySet().toArray()[0]);
                    docType = getDocTypeFromDockind(DocumentKind.findByCn(documentKindCn));
                }

        	}
        	catch(EdmException e)
        	{
        		addActionError(e.getMessage());
        	}
        }
    }

	private class DeleteEmailChannel implements ActionListener {
		public void actionPerformed(ActionEvent event) throws IOException {

			try {
				// rozpocz�cie transakcji
				DSApi.context().begin();

				DSEmailChannel emailChannel;
				if (id != null) {
					emailChannel = MessageManager.getChannel(id);
					emailChannel.setEnabled(false);
					emailChannel.setDeleted(true);
					DSApi.context().session().save(emailChannel);
					DSApi.context().commit();
					event.addActionError("Usuni�to kana� email: "
							+ emailChannel.getKolejka());
					ServletActionContext.getRequest().getSession().removeAttribute("conditions");
					ServletActionContext.getRequest().getSession().setAttribute("info", "Usuni�to kana� email: "
							+ emailChannel.getKolejka());
					ServletActionContext.getResponse().sendRedirect(getRedirectUrl());
				}
				else {
					event.addActionMessage("Nie wskazano Kana�u email ");
				}
			} catch (EdmException e) {
				log.error("", e);
			}
	
		
		}
	}
	/**
	 * Klasa wewn�trzna tworz�ca nowy lub edytuj�ca ju� istniej�cy kana� mailowy. 
	 * 
	 */
	private class Save implements ActionListener
	{
	    public void actionPerformed(ActionEvent event)
	    {
	    	try
        	{
	    		// pobranie warunk�w kana�u mailowego z obiektu sesji
	    		conditions = (Vector<DSEmailChannelCondition>)ServletActionContext.getRequest().getSession().getAttribute("conditions");
		    	
	    		// rozpocz�cie transakcji
	    		DSApi.context().begin();
		    	
		    	// sprawdzenie p�l formularza
		    	checkFields();
		    	
		    	DSEmailChannel emailChannel;
		    	if(id != null) 
		    		emailChannel = MessageManager.getChannel(id);
		    	else
		    		emailChannel = new DSEmailChannel();
		    	
		    	// ustawienie p�l formularza w kanale mailowym
		    	emailChannel.setHost(hostname);
		    	emailChannel.setPassword(password);
		    	emailChannel.setUser(username);
		    	emailChannel.setFolderName(folderName);
		    	emailChannel.setKolejka(kolejka);
		    	emailChannel.setCtime(new Date());
		    	emailChannel.setContentType(contentType);
		    	emailChannel.setWarunki(MessageManager.getStringFromConditions(conditions));
		    	emailChannel.setEnabled(enabled);
		    	emailChannel.setSSL(SSL);
		    	emailChannel.setPort(port);
		    	emailChannel.setProtocol(protocol);
		    	emailChannel.setMbox(mbox);
		    	emailChannel.setUserLogin(userLogin);
		    	emailChannel.setDeleted(false);
		    	
		    	// KONFIGURACJA SMTP
		    	if(smtpEnabled){
	    			emailChannel.setSmtpHost(smtpHost);
	    			emailChannel.setSmtpPort(smtpPort);
	    			emailChannel.setSmtpUsername(smtpUsername);
	    			emailChannel.setSmtpPassword(smtpPassword);
	    			emailChannel.setSmtpTLS(smtpTLS);
	    			emailChannel.setSmtpSSL(smtpSSL);
		    	}else if(emailChannel.isSmtpEnabled()){
		    		emailChannel.setSmtpTLS(false);
	    			emailChannel.setSmtpSSL(false);
		    	}
		    	emailChannel.setSmtpEnabled(smtpEnabled);

                // wykonujemy akcje zwiazane z dockindem powiazanym z kanalem email
                manageDockindAction(emailChannel);

		    	// wyczyszczenie niepotrzebnych danych z sesji
		    	ServletActionContext.getRequest().getSession().removeAttribute("conditions");
		    	// zapisanie zmian
		    	if(id != null){
		    		DSApi.context().session().update(emailChannel);
		    	event.addActionMessage("Zaktualizowano kana� email: "+emailChannel.getKolejka());
		    	}
		    	else{
		    		DSApi.context().session().save(emailChannel);
		    		event.addActionMessage("Utworzono kana� email: "+emailChannel.getKolejka());
		    		clearFields();
		    	}
		    
                // zatwierdzenie transakcji
                DSApi.context().commit();
            }catch(EdmException e){
                LOG.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
                LOG.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
	
			/**
	         * Utworzenie, zaktualizowanie b�d� usuni�cie powi�zania kana�u email z dockindem
	         * @param emailChannel wybrany kana� emai
	         * @throws EdmException
	         * @throws SQLException
	         */
	        private void manageDockindAction(DSEmailChannel emailChannel) throws EdmException, SQLException {
	            //documentKindCn przyjmuje wartosc rozna od null wylacznie gdy available.properties emailChannelDWR jest ustawiony na true a uzytkownik wybral pozycje z listy dostepnych dockindow
	            if(documentKindCn != null && (documentKind = DocumentKind.findByCn(documentKindCn)) != null){
	                docType = getDocTypeFromDockind(documentKind);
	                dwrValues = getValuesFromDWR();
	                if(documentKind.logic() instanceof EmailChannelLogic){
	                    //je�li kana� email ma zosta� powi�zany z dockindem innym ni� do tej pory
	                    //to usuwamy informacje o dotychczasowym powiazaniu z wczesniej powi�zanego z nim dockindem
	                    if(emailChannel.getDocumentKindId() != null){
	                        //dotychczasowy dockind powiazany z kanalem jest inny niz ten wybrany na formatce przez uzytkownika (czyt. aktualizujemy informacje o dockindzie powiazanym z kanalem email)
	                        if(emailChannel.getDocumentKindId() != documentKind.getId()){
	                            DocumentKind oldDockind = DocumentKind.find(emailChannel.getDocumentKindId());
	                            //usuwamy powiazanie z dotychczasowym dockindem
	                            ((EmailChannelLogic)oldDockind.logic()).onDelete(emailChannel);
	                            //dodajemy powiazanie z nowym, wybranym dockindem
	                            ((EmailChannelLogic)documentKind.logic()).onCreate(emailChannel, dwrValues);
	                            emailChannel.setDocumentKindId(documentKind.getId());
	                        }else{
	                            ((EmailChannelLogic)documentKind.logic()).onUpdate(emailChannel, dwrValues);
	                        }
	                    }else{
	                        //tworzymy powiazanie pomiedzy kana�em email a wybranym dockindem
	                        ((EmailChannelLogic)documentKind.logic()).onCreate(emailChannel, dwrValues);
	                        emailChannel.setDocumentKindId(documentKind.getId());
	                    }
	                }else{
	                    throw new EdmException("Typ dokumentu " + documentKind.getName() + " nie jest wpierany przez kana�y email");
	                }
	            }
	        }
	    }
        /**
         * Utworzenie, zaktualizowanie b�d� usuni�cie powi�zania kana�u email z dockindem
         * @param emailChannel wybrany kana� emai
         * @throws EdmException
         * @throws SQLException
         */
        private void manageDockindAction(DSEmailChannel emailChannel) throws EdmException, SQLException {
            //documentKindCn przyjmuje wartosc rozna od null wylacznie gdy available.properties emailChannelDWR jest ustawiony na true a uzytkownik wybral pozycje z listy dostepnych dockindow
            if(documentKindCn != null && (documentKind = DocumentKind.findByCn(documentKindCn)) != null){
                docType = getDocTypeFromDockind(documentKind);
                dwrValues = getValuesFromDWR();
                if(documentKind.logic() instanceof EmailChannelLogic){
                    //je�li kana� email ma zosta� powi�zany z dockindem innym ni� do tej pory
                    //to usuwamy informacje o dotychczasowym powiazaniu z wczesniej powi�zanego z nim dockindem
                    if(emailChannel.getDocumentKindId() != null){
                        //dotychczasowy dockind powiazany z kanalem jest inny niz ten wybrany na formatce przez uzytkownika (czyt. aktualizujemy informacje o dockindzie powiazanym z kanalem email)
                        if(emailChannel.getDocumentKindId() != documentKind.getId()){
                            DocumentKind oldDockind = DocumentKind.find(emailChannel.getDocumentKindId());
                            //usuwamy powiazanie z dotychczasowym dockindem
                            ((EmailChannelLogic)oldDockind.logic()).onDelete(emailChannel);
                            //dodajemy powiazanie z nowym, wybranym dockindem
                            ((EmailChannelLogic)documentKind.logic()).onCreate(emailChannel, dwrValues);
                            emailChannel.setDocumentKindId(documentKind.getId());
                        }else{
                            ((EmailChannelLogic)documentKind.logic()).onUpdate(emailChannel, dwrValues);
                        }
                    }else{
                        //tworzymy powiazanie pomiedzy kana�em email a wybranym dockindem
                        ((EmailChannelLogic)documentKind.logic()).onCreate(emailChannel, dwrValues);
                        emailChannel.setDocumentKindId(documentKind.getId());
                    }
                }else{
                    throw new EdmException("Typ dokumentu " + documentKind.getName() + " nie jest wpierany przez kana�y email");
                }
            }
        }
    

    /**
     * Pobieranie z xmla dockinda informacji o typie dokumentu (archive, in, out, mix etc.), kt�ry opisuje.
     * Informacja wymagana w trakcie wykonywania metod klasy DwrFacade
     * @param documentKind
     * @return
     * @throws EdmException
     */
    int getDocTypeFromDockind(DocumentKind documentKind) throws EdmException {
        String dockindXMLDocType = documentKind.getXML().getRootElement().attribute("types").getStringValue();
        //jesli dockind ma zdefiniowany wiecej niz jeden typ dokumentu to bierzemy pierwszy z wyliczonych typow
        if(dockindXMLDocType.contains(",")){
            dockindXMLDocType = dockindXMLDocType.substring(0, dockindXMLDocType.indexOf(","));
        }
        for(DocumentType dt: DocumentType.values()){
            if(dockindXMLDocType.equals(dt.getName())){
                return dt.ordinal();
            }
        }
        throw new EdmException("Brak zdefiniowanego typu dokumentu dla warto�ci dockinda types " + dockindXMLDocType);
    }
    
    
    private void clearFields() {
		hostname=null; password = null;username= null;
		folderName= null;kolejka= null;contentType= null;
		conditions= null;enabled= false;SSL= false;
		port= 0;protocol= null;mbox= null;userLogin= null;
	}
	/**
	 * Metoda sprawdzaj�ca poprawno�� p�l formularza, ustawia mo�liwe warto�ci
	 * domy�lne.
	 * 
	 * @throws EdmException
	 */
	private void checkFields() throws EdmException
	{
		boolean throwExc = false;
		// Nazwa kodowa
		if (kolejka == null || kolejka.equals(""))
		{
			throwExc = true;
			addActionError("BrakNazwyKodowej");
		}
		// Nazwa hosta
		if (hostname == null) 
    	{
    		throwExc = true;
    		addActionError("BrakNazwyHosta");
    	}
		// Has�o
    	if (password == null)
    	{
    		throwExc = true;
    		addActionError("BrakHasla");
    	}
    	// Nazwa u�ytkownika
    	if (username == null)
    	{
    		throwExc = true;
    		addActionError("BrakNazwyUzytkownika");
    	}
    	// Port, domy�lny -1
    	if (port == 0)
    	{
    		port = -1;
    	}
    	// Nazwa folderu skrzynki, domy�lny: 'INBOX'
    	if (mbox == null || mbox.equals(""))
    	{
    		mbox = "INBOX";
    	}
    	
    	//	Je�li w��czamy poczt� wychodz�c� dla kana�u email
    	if(smtpEnabled){
    		if(smtpHost == null){
    			throwExc = true;
        		addActionError("BrakNazwyHosta");
    		}
    		
    		if(smtpPort == null){
    			throwExc = true;
        		addActionError("Brak portu");
    		}
    		
    		if(smtpUsername == null){
    			throwExc = true;
        		addActionError("BrakNazwyUzytkownika");
    		}
    		
    		/*if(smtpPassword == null){
    			throwExc = true;
        		addActionError("BrakHasla");
    		}*/
    	}
    	// w przypadku napotkania b��d�w rzuca wyj�tek
    	if (throwExc)
    		throw new EdmException("BledyWPolach");
	}
	
	/**
	 * Klasa wewn�trzna implementuj�ca akcj� usuni�cia warunk�w kana�u mailowego.
	 * 
	 */
	private class Remove implements ActionListener
	{
	    public void actionPerformed(ActionEvent event)
	    {
	    	prepareContents();
	    	if(conditionsPicked!=null)
	    	{
	    		conditions = (Vector<DSEmailChannelCondition>)ServletActionContext.getRequest().getSession().getAttribute("conditions");
	    		if(conditions!=null)
    			{
		    		for(String i:conditionsPicked)
		    		{
		    			conditions.remove(Integer.parseInt(i));
		    		}
		    		ServletActionContext.getRequest().getSession().setAttribute("conditions", conditions);
    			}
	    		prepareConditionsDescList();
	    		event.skip("fillForm");
	    	}
	    }
	}

    /**
     * Klasa wewn�trzna implementuj�ca akcj� usuni�cia informacji o powi�zaniu kana�u mailowego z dockindem
     *
     */
    private class RemoveDockind implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            try{
                // rozpocz�cie transakcji
                DSApi.context().begin();

                DSEmailChannel emailChannel;
                if(id != null)
                    emailChannel = MessageManager.getChannel(id);
                else
                    throw new EdmException("Nie wybrano kana�u email");

                if(emailChannel.getDocumentKindId() == null){
                    throw new EdmException("Nieodnaleziono powi�zania pomi�dzy kana�em email a wskazanym typem dokumentu");
                }else{
                    documentKind = DocumentKind.find(emailChannel.getDocumentKindId());
                }

                // usuni�cie powi�zania pomi�dzy kana�em email a typem dokumentu
                if(documentKind.logic() instanceof EmailChannelLogic){
                    ((EmailChannelLogic) documentKind.logic()).onDelete(emailChannel);
                }

                emailChannel.setDocumentKindId(null);
                // zapisanie zmian
                DSApi.context().session().update(emailChannel);

                // zatwierdzenie transakcji
                DSApi.context().commit();
            }catch(EdmException e){
                LOG.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
                LOG.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
	
	/**
	 * Klasa wewn�trzna implementuj�ca akcj� dodawania warunku do kana�u mailowego.
	 * 
	 */
	private class AddCondition implements ActionListener
	{
	    public void actionPerformed(ActionEvent event)
	    {
	    	prepareContents();
	    	if(conditionValue != null && !conditionValue.equals(""))
	    	{
		    	conditions = (Vector<DSEmailChannelCondition>)ServletActionContext.getRequest().getSession().getAttribute("conditions");
	    		if(conditions == null) conditions = new Vector<DSEmailChannelCondition>();
	    		DSEmailChannelCondition cc = new DSEmailChannelCondition();
		    	cc.setField(conditionField);
		    	cc.setOperator(conditionOperator);
		    	cc.setValue(conditionValue);
		    	conditions.add(cc);
		    	ServletActionContext.getRequest().getSession().setAttribute("conditions", conditions);
		    	prepareConditionsDescList();
		    	event.skip("fillForm");
	    	}
	    }
	}
	
	private void prepareConditionsDescList()
	{
		String ret;
		conditionsDescList = new LinkedHashMap<Integer, String>();
		int counter = 0;
		if(conditions== null) return;
		for(DSEmailChannelCondition cc:conditions)
		{
			ret = "";
			if(counter>0)
			{
				if(cc.getOperator().equalsIgnoreCase("and"))
				{
					ret += "  oraz ";
				}
				else if(cc.getOperator().equalsIgnoreCase("or"))
				{
					ret += "lub ";
				}
			}
		
			if(cc.getField().equalsIgnoreCase("title"))
			{
				ret += "'" +sm.getString("Tytul")+"' zawiera ";
			}
			else if(cc.getField().equalsIgnoreCase("from"))
			{
				ret += "'" +sm.getString("Od")+"' zawiera ";
			}
			
			ret += "'" + cc.getValue() + "'";
			conditionsDescList.put(counter, ret);
			counter++;
		}
	}
	
	/*public String getConditionsDesc()
	{
		String ret = "";
		boolean first = true;
		for(ChannelCondition cc: conditions)
		{
			if(!first)
			{
				if(cc.getOperator().equalsIgnoreCase("and"))
				{
					ret += "\toraz ";
				}
				else if(cc.getOperator().equalsIgnoreCase("or"))
				{
					ret += "lub ";
				}
			}
			else
			{
				first = false;
			}
			
			if(cc.getField().equalsIgnoreCase("title"))
			{
				ret += "'" +sm.getString("Tytul")+"' zawiera ";
			}
			else if(cc.getField().equalsIgnoreCase("from"))
			{
				ret += "'" +sm.getString("Od")+"' zawiera ";
			}
			
			ret += "'" + cc.getValue() + "'";
			ret += "\n";
		}
		return ret;
	}*/

	public String getHostname() 
	{
		return hostname;
	}

	public void setHostname(String host) 
	{
		this.hostname = host;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getFolderName() 
	{
		return folderName;
	}

	public void setFolderName(String folderName) 
	{
		this.folderName = folderName;
	}

	public String getKolejka() 
	{
		return kolejka;
	}

	public void setKolejka(String kolejka) 
	{
		this.kolejka = kolejka;
	}

	public Vector<DSEmailChannelCondition> getConditions() 
	{
		return conditions;
	}

	public void setConditions(Vector<DSEmailChannelCondition> conditions) 
	{
		this.conditions = conditions;
	}

	public String getConditionField() 
	{
		return conditionField;
	}

	public void setConditionField(String conditionField) 
	{
		this.conditionField = conditionField;
	}

	public String getConditionValue() 
	{
		return conditionValue;
	}

	public void setConditionValue(String conditionValue) 
	{
		this.conditionValue = conditionValue;
	}

	public String getConditionOperator() 
	{
		return conditionOperator;
	}

	public void setConditionOperator(String conditionOperator) 
	{
		this.conditionOperator = conditionOperator;
	}

	public String getContentType() 
	{
		return contentType;
	}

	public void setContentType(String contentType) 
	{
		this.contentType = contentType;
	}

	public Map<Integer, String> getConditionsDescList() 
	{
		return conditionsDescList;
	}

	public void setConditionsDescList(Map<Integer, String> conditionsDescList) 
	{
		this.conditionsDescList = conditionsDescList;
	}

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public String[] getConditionsPicked() 
	{
		return conditionsPicked;
	}

	public void setConditionsPicked(String[] conditionsPicked) 
	{
		this.conditionsPicked = conditionsPicked;
	}

	public Map<String, String> getContents() 
	{
		return contents;
	}

	public void setContents(Map<String, String> contents) 
	{
		this.contents = contents;
	}

	public Map<String, String> getProtocols() 
	{
		return protocols;
	}

	public void setProtocols(Map<String, String> protocols) 
	{
		this.protocols = protocols;
	}

	public String getProtocol() 
	{
		return protocol;
	}

	public void setProtocol(String protocol) 
	{
		this.protocol = protocol;
	}

	public int getPort() 
	{
		return port;
	}

	public void setPort(int port) 
	{
		this.port = port;
	}

	public String getMbox() 
	{
		return mbox;
	}

	public void setMbox(String mbox) 
	{
		this.mbox = mbox;
	}

	public boolean isSSL() 
	{
		return SSL;
	}

	public void setSSL(boolean sSL) 
	{
		SSL = sSL;
	}

	public boolean isEnabled() 
	{
		return enabled;
	}

	public void setEnabled(boolean enabled) 
	{
		this.enabled = enabled;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUsername() {
		return smtpUsername;
	}

	public void setSmtpUsername(String smtpUsername) {
		this.smtpUsername = smtpUsername;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public boolean isSmtpTLS() {
		return smtpTLS;
	}

	public void setSmtpTLS(boolean smtpTLS) {
		this.smtpTLS = smtpTLS;
	}

	public boolean isSmtpSSL() {
		return smtpSSL;
	}

	public void setSmtpSSL(boolean smtpSSL) {
		this.smtpSSL = smtpSSL;
	}

	public boolean isSmtpEnabled() {
		return smtpEnabled;
	}

	public void setSmtpEnabled(boolean smtpEnabled) {
		this.smtpEnabled = smtpEnabled;
	}  
	
	public String getUserLogin() {
		return userLogin;
	}
	
	public void setUserLogin(String userLogin) {
		this.userLogin=userLogin;
	}
	
    public String getDocumentKindCn() {
        return documentKindCn;
    }

    public void setDocumentKindCn(String documentKindCn) {
        this.documentKindCn = documentKindCn;
    }

    public Map<String, String> getDocumentKinds() {
        return documentKinds;
    }

    public void setDocumentKinds(Map<String, String> documentKinds) {
        this.documentKinds = documentKinds;
    }

    public int getDocType() {
        return docType;
    }

    public void setDocType(int docType) {
        this.docType = docType;
    }

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}



