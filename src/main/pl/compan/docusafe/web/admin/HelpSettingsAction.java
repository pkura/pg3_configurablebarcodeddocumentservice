package pl.compan.docusafe.web.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Ustawienia pomocy. Akcja pozwala okre�li� foldery dla r�nych typ�w
 * plik�w pomocy(pomoc og�lna, pomoc dedykowana, narz�dzia), doda� do
 * nich nowe pliki, usuwa� je i ustawia� ich opisy.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */
public class HelpSettingsAction extends EventActionSupport
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(HelpSettingsAction.class);
    
    public static final int FOLDER_TYPE_GENERAL = 1;
    public static final int FOLDER_TYPE_SPECIFIC = 2;
    public static final int FOLDER_TYPE_TOOLS = 3;
    
    public static String DEFAULT_GENERAL_FOLDER;// = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("podreczniki_ogolne")).getAbsolutePath();
    public static String DEFAULT_SPECIFIC_FOLDER;// = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("podreczniki_dedykowane")).getAbsolutePath();
    public static String DEFAULT_TOOLS_FOLDER;// = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("narzedzia")).getAbsolutePath();
    
    private String generalHelpDir;
    private String specificHelpDir;
    private String toolsDir;
    private String description;
    private FormFile file;
    
    private String actionFilename;
    private Integer actionType;
    private String newDesc;
    
    private Integer folderType;
    private Map<Integer, String> folderTypes = new LinkedHashMap<Integer, String>();
    private List<HelpFile> helpFiles = new ArrayList<HelpFile>();
    
    //private char PATH_SEPARATOR = '\\';
    private String PATH_SEPARATOR = "/";

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        //Update update = new Update();
        GetFolders getFolders = new GetFolders();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(getFolders).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

     /*   registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(update).
            append(getFolders).
            append(fillForm).
           appendFinally(CloseHibernateSession.INSTANCE);*/

        registerListener("doAddFile").
            append(OpenHibernateSession.INSTANCE).
           // append(update).
            append(getFolders).
            append(new AddFile()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(getFolders).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeDesc").
            append(OpenHibernateSession.INSTANCE).
            append(getFolders).
            append(new ChangeDesc()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class GetFolders implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                // nie mo�na obecnie ustali� r�cznie katalogu na pomoce ze wzgl�d�w bezpiecze�stwa - 
                // u�ywane domy�lne
                //Preferences pref = getPreferences();
            	DEFAULT_GENERAL_FOLDER = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("podreczniki_ogolne")).getAbsolutePath();
            	DEFAULT_SPECIFIC_FOLDER = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("podreczniki_dedykowane")).getAbsolutePath();
            	DEFAULT_TOOLS_FOLDER = new File(Docusafe.getHome().getAbsolutePath()+"/"+sm.getString("narzedzia")).getAbsolutePath();
                generalHelpDir = DEFAULT_GENERAL_FOLDER;//pref.get("general", "");
                specificHelpDir = DEFAULT_SPECIFIC_FOLDER;//pref.get("specific", "");
                toolsDir = DEFAULT_TOOLS_FOLDER;//pref.get("tools", "");
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                folderTypes.put(FOLDER_TYPE_GENERAL, sm.getString("pomocyOgolnej"));
                folderTypes.put(FOLDER_TYPE_SPECIFIC, sm.getString("pomocyDedykowanej"));
                folderTypes.put(FOLDER_TYPE_TOOLS, sm.getString("narzedzi"));
                
                SAXReader reader = new SAXReader();
                Document doc = null;
                
                File contentsFile = new File(generalHelpDir + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        helpFiles.add(new HelpFile(elFile, FOLDER_TYPE_GENERAL));
                    }
                }
                
                contentsFile = new File(specificHelpDir + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        helpFiles.add(new HelpFile(elFile, FOLDER_TYPE_SPECIFIC));
                    }
                }
                
                contentsFile = new File(toolsDir + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        helpFiles.add(new HelpFile(elFile, FOLDER_TYPE_TOOLS));
                    }
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    // obecnie ta akcja nieu�ywana
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                
                Preferences pref = getPreferences();
                
                if (generalHelpDir != null)
                    pref.put("general", generalHelpDir);
                if (specificHelpDir != null)
                    pref.put("specific", specificHelpDir);
                if (toolsDir != null)
                    pref.put("tools", toolsDir);
                
                DSApi.context().commit();
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    static void copyFile(File fileFrom, File fileTo) throws IOException
    {
        InputStream in = new FileInputStream(fileFrom);
        OutputStream out = new FileOutputStream(fileTo);
        
        int rd;
        byte[] buf = new byte[1000];
        while ((rd = in.read(buf)) > 0)
        {
            out.write(buf, 0, rd);
        }
        
        out.close();
        in.close();
    }
    
    private class AddFile implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String path = null;
            try
            {                              
                if (folderType == FOLDER_TYPE_GENERAL)
                    path = generalHelpDir;
                else if (folderType == FOLDER_TYPE_SPECIFIC)
                    path = specificHelpDir;
                else if (folderType == FOLDER_TYPE_TOOLS)
                    path = toolsDir;
                if (path != null)
                {
                    File dir = new File(path);
                    if (!dir.exists())
                    {
                        boolean success = dir.mkdir();
                        if (!success)
                            throw new EdmException(sm.getString("OdpowiedniKatalogPrzechowujacyPlikiNieIstniejeOrazWystapilBladPodczasProbyJegoUtworzenia"));
                    }
                    else
                        if (!dir.isDirectory())
                            throw new EdmException(sm.getString("ZamiastOdpowiedniegoKataloguPrzechowujacegoPlikiUstalonyJestPlik"));

                    Document contents = null;
                    File contentsFile = new File(path + PATH_SEPARATOR + "contents.xml");
                    SAXReader reader = new SAXReader();
                    DocumentFactory factory = DocumentFactory.getInstance();
                    Element root = null;
                    if (contentsFile.exists())
                    {
                        contents = reader.read(contentsFile);
                        root = contents.getRootElement();
                    }
                    else
                    {
                        root = factory.createElement("contents");
                        contents = factory.createDocument(root);
                    }
                    
                    if (contents != null)
                    {
                        if (file == null)
                            throw new EdmException(sm.getString("NieWybranoPliku"));
                        else if (file.getFile() == null)
                            throw new EdmException(sm.getString("NieWybranoPliku"));

                        String newFileName = file.getFile().getName();
                        File fileFrom = file.getFile();
                        if (!fileFrom.exists())
                            throw new EdmException(sm.getString("PodanyPlikNieIstnieje"));
                        File fileTo = new File(path + PATH_SEPARATOR + file.getName());
                        copyFile(fileFrom, fileTo);
                        if (findFileElement(root, newFileName) != null)
                            throw new EdmException(sm.getString("plikJuzIstnieje",newFileName));

                        HelpFile helpFile = new HelpFile(newFileName, description, folderType);
                        root.add(helpFile.getXMLElement());
                        
                        FileOutputStream outputStream = new FileOutputStream(contentsFile);
                        new XMLWriter(outputStream).write(contents);
                        outputStream.close();
                    }
                    addActionMessage(sm.getString("NowyPlikZostalDodany"));
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String path = null;
            try
            {
                //if (!DSApi.context().isAdmin())
                //    throw new EdmException("Brak uprawnie� do edycji pomocy");
                if (actionType == FOLDER_TYPE_GENERAL)
                    path = generalHelpDir;
                else if (actionType == FOLDER_TYPE_SPECIFIC)
                    path = specificHelpDir;
                else if (actionType == FOLDER_TYPE_TOOLS)
                    path = toolsDir;
                if (path != null)
                {
                    SAXReader reader = new SAXReader();
                    Document doc = reader.read(path + PATH_SEPARATOR + "contents.xml");
                    Element el = findFileElement(doc.getRootElement(), actionFilename);
                    if (el == null)
                        throw new EdmException(sm.getString("NieZnalezionoPlikuDoUsuniecia"));
                    File file = new File(path + PATH_SEPARATOR + actionFilename);
                    if (!doc.getRootElement().remove(el))
                        throw new EdmException(sm.getString("NieMoznaUsunacPliku"));
                    if (!file.delete())
                        throw new EdmException(sm.getString("NieMoznaUsunacPliku"));

                    File contentsFile = new File(path + PATH_SEPARATOR + "contents.xml");
                    FileOutputStream outputStream = new FileOutputStream(contentsFile);
                    new XMLWriter(outputStream).write(doc);
                    outputStream.close();
                    addActionMessage(sm.getString("PlikZostalUsuniety"));
                }
                else
                    throw new EdmException(sm.getString("PlikNieMaOkreslonegoFolderu"));
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private class ChangeDesc implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                //if (!DSApi.context().isAdmin())
                //    throw new EdmException("Brak uprawnie� do edycji pomocy");
                String path = null;

                if (actionType == FOLDER_TYPE_GENERAL)
                    path = generalHelpDir;
                else if (actionType == FOLDER_TYPE_SPECIFIC)
                    path = specificHelpDir;
                else if (actionType == FOLDER_TYPE_TOOLS)
                    path = toolsDir;
                if (path != null)
                {
                    SAXReader reader = new SAXReader();
                    Document doc = reader.read(path + PATH_SEPARATOR + "contents.xml");
                    Element el = findFileElement(doc.getRootElement(), actionFilename);
                    if (el == null)
                        throw new EdmException(sm.getString("NieZnalezionoPlikuDoZmianyOpisu"));
                    el.element("description").setText(newDesc);
                    
                    File contentsFile = new File(path + PATH_SEPARATOR + "contents.xml");
                    FileOutputStream outputStream = new FileOutputStream(contentsFile);
                    new XMLWriter(outputStream).write(doc);
                    outputStream.close();
                }
                else
                    throw new EdmException(sm.getString("PlikNieMaOkreslonegoFolderu"));
                addActionMessage(sm.getString("OpisPlikuZostalZmieniony"));

            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private static Element findFileElement(Element root, String filename)
    {
        if (filename == null)
            return null;
        Iterator itr = root.elementIterator("file");
        while (itr.hasNext())
        {
            Element el = (Element) itr.next();
            Element elName = el.element("name");
            if (elName != null && elName.getText().equals(filename))
                return el;
        }
        return null;
    }
    
    protected Preferences getPreferences()
    {
        return DSApi.context().systemPreferences().node("modules/help/folders");
    }

    public void setGeneralHelpDir(String generalHelpDir)
    {
        this.generalHelpDir = generalHelpDir;
    }

    public String getGeneralHelpDir()
    {
        return generalHelpDir;
    }

    public void setSpecificHelpDir(String specificHelpDir)
    {
        this.specificHelpDir = specificHelpDir;
    }

    public String getSpecificHelpDir()
    {
        return specificHelpDir;
    }

    public void setToolsDir(String toolsDir)
    {
        this.toolsDir = toolsDir;
    }

    public String getToolsDir()
    {
        return toolsDir;
    }

    public Map<Integer,String> getFolderTypes()
    {
        return folderTypes;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setFolderType(Integer folderType)
    {
        this.folderType = folderType;
    }

    public Integer getFolderType()
    {
        return folderType;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public List<HelpFile> getHelpFiles()
    {
        return helpFiles;
    }

    public void setActionFilename(String actionFilename)
    {
        this.actionFilename = actionFilename;
    }

    public void setActionType(Integer actionType)
    {
        this.actionType = actionType;
    }

    public void setNewDesc(String newDesc)
    {
        this.newDesc = newDesc;
    }

    /**
     * Klasa pomocnicza dla plik�w pomocy.
     * 
     * @author Marcin Wlizlo
     */
    public class HelpFile
    {
        private String filename;
        private String description;
        private int type;
        
        public HelpFile(String filename, String description, int type)
        {
            this.filename = filename;
            this.description = description;
            this.type = type;
        }
        
        public HelpFile(Element el, int type)
        {
            Element elName = el.element("name");
            this.filename = elName.getText();
            Element elDescription = el.element("description");
            this.description = elDescription.getText();
            this.type = type;
        }
        
        /**
         * Tworzy i zwraca element dokumentu XML opisuj�cy plik pomocy
         * @return
         */
        public Element getXMLElement()
        {
            DocumentFactory factory = DocumentFactory.getInstance(); 
            Element el = factory.createElement("file");
            Element elName = factory.createElement("name");
            elName.setText(filename);
            Element elDescription = factory.createElement("description");
            elDescription.setText(description);
            el.add(elName);
            el.add(elDescription);
            return el;
        }

        public void setFilename(String filename)
        {
            this.filename = filename;
        }

        public String getFilename()
        {
            return filename;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }

        public void setType(int type)
        {
            this.type = type;
        }

        public int getType()
        {
            return type;
        }
        
        public String getTypeDescription()
        {
            if (FOLDER_TYPE_GENERAL == type)
                return "podr�cznik og�lny";
            else if (FOLDER_TYPE_SPECIFIC == type)
                return "podr�cznik dedykowany";
            else 
                return "narz�dzie";
        }
    }
    
}