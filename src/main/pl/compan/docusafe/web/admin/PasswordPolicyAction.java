package pl.compan.docusafe.web.admin;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.prefs.Preferences;

import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PasswordPolicy;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Nowy spos�b kontroli wa�no�ci has�a:
 * - filtr, kt�ry zawsze sprawdza wa�no�� has�a, a kilka dni przed up�ywem
 *   spowoduje wy�wietlenie ostrze�enia na dole ekranu (nad paskiem statusu),
 *   a po zalogowaniu na ca�ym ekranie.
 *
 * PaswordPolicyAction zapisuje w ServletContext aktualne parametry
 * (poprzez funkcj� pomocnicz� w PasswordPolicy).
 *
 * Filtr odczytuje parametry, je�eli nie ma ich w ServletContext.
 *
 *
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PasswordPolicyAction.java,v 1.16 2009/03/06 12:45:23 pecet3 Exp $
 */
public class PasswordPolicyAction extends EventActionSupport
{
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(PasswordPolicyAction.class.getPackage().getName(),null);
    
    private boolean passwordExpires;
    private Integer daysToExpire;
    private Integer minPasswordLength;
    private boolean mustHaveDigits;
    private Integer howManyDigits;
    private boolean mustHaveLetters;
    private Integer howManyLetters;
    private boolean mustHaveSpecials;
    private Integer howManySpecials;
    private boolean mustHaveSmallLetters;
    private Integer howManySmallLetters;
    
    private boolean changePasswordAtFirstLogon;
    private boolean lockingPasswords;
    private Integer howManyIncorrectLogins;
    
    public static final String EV_UPDATE = "update";

    private String expireDate;
    
    

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new ValidateUpdate()).
            append(EV_UPDATE, new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doClearHistory").
        	append(OpenHibernateSession.INSTANCE).
        	append(new ClearHistory()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSetExpireDate").
    		append(OpenHibernateSession.INSTANCE).
    		append(new SetExpireDate()).
    		append(fillForm).
    		appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Preferences prefs = DSApi.context().systemPreferences().
                node(PasswordPolicy.NODE_PASSWORD_POLICY);

            passwordExpires = prefs.getBoolean(PasswordPolicy.KEY_PASSWORD_EXPIRES, false);
            daysToExpire = new Integer(prefs.getInt(PasswordPolicy.KEY_DAYS_TO_EXPIRE, 0));
            minPasswordLength = new Integer(prefs.getInt(PasswordPolicy.KEY_MIN_PASSWORD_LENGTH, PasswordPolicy.KEY_MIN_PASSWORD_LENGTH_DEFAULT));
            mustHaveDigits = prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_DIGITS, false);
            howManyDigits = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_DIGITS, 0));
            mustHaveLetters = prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_LETTERS, false);
            howManyLetters = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_LETTERS, 0));
            mustHaveSpecials = prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SPECIALS, false);
            howManySpecials = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SPECIALS, 0));
            mustHaveSmallLetters = prefs.getBoolean(PasswordPolicy.KEY_MUST_HAVE_SMALL_LETTERS, false);
            howManySmallLetters = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_SMALL_LETTERS, 0));
            
            changePasswordAtFirstLogon = prefs.getBoolean(PasswordPolicy.KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON, true);            
            lockingPasswords = prefs.getBoolean(PasswordPolicy.KEY_LOCKING_PASSWORDS, false);
            howManyIncorrectLogins = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_INCORRECT_LOGINS, 0));
        }
    }

    private class ValidateUpdate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (passwordExpires)
            {
                if (daysToExpire == null)
                {
                    addActionError(sm.getString("NiePodanoLiczbyDniPoKtorychWygasaHaslo"));
                }
                else if (daysToExpire.intValue() < 1)
                {
                    addActionError(sm.getString("LiczbaDniPoKtorychWygasaHasloNieMozeBycMniejszaOdJednego"));
                }
            }

            if (minPasswordLength == null)
            {
                addActionError(sm.getString("NiePodanoMinimalnejDlugosciHasla"));
            }
            else if (minPasswordLength.intValue() < 6)
            {
                addActionError(sm.getString("MinimalnaDlugoscHaslaPowinnaWynosicPrzynajmniej",6));
            }
            else if (minPasswordLength.intValue() > PasswordPolicy.MAX_PASSWORD_LENGTH)
            {
                addActionError(sm.getString("MinimalnaDlugoscHaslaMozeWyniescCoNajwyzejZnakow",
                    PasswordPolicy.MAX_PASSWORD_LENGTH));
            }
            
            if(mustHaveDigits)
            	validateCharType(howManyDigits,sm.getString("cyfr"));
            if(mustHaveLetters)
            	validateCharType(howManyLetters,sm.getString("duzychLiter"));
            if(mustHaveSmallLetters)
            	validateCharType(howManySmallLetters,sm.getString("malychLiter"));
            if(mustHaveSpecials)
            	validateCharType(howManySpecials,sm.getString("znakowSpecjalnych"));
            if(lockingPasswords)
            	{
	            	if(howManyIncorrectLogins==null)
	            		addActionError(sm.getString("NiePodanoLiczbyNieprawidlowychProbZalogowaniaDoZablokowaniaKonta"));
	            	if(howManyIncorrectLogins<1)
	            		addActionError(sm.getString("NieprawidlowaLiczbaProbZalogowaniaDoZablokowaniaKonta"));
            	}
            	
           /* if (mustHaveDigits)
            {
                if (howManyDigits == null)
                {
                    addActionError("Nie podano minimalnej liczby cyfr w ha�le");
                }
                else if (howManyDigits.intValue() < 1)
                {
                    addActionError("Minimalna liczba cyfr w ha�le powinna by� wi�ksza od zera");
                }
                else if (minPasswordLength != null && howManyDigits.intValue() > minPasswordLength.intValue())
                {
                    addActionError("Minimalna liczba cyfr w ha�le nie mo�e by� wi�ksza ni� "+
                        "minimalna d�ugo�� has�a");
                }
            }*/
            
            if (hasActionErrors())
                event.skip(EV_UPDATE);
        }
        
        
        /**
         * 
         * @param howManyChars - jak wiele znakow danego typu
         * @param charType - typ znakow ("liter", "cyfr", lub "znak�w specjalnych")
         * 
         */
        private void validateCharType(Integer howManyChars, String charType)
        {
        	if(howManyChars==null)
        		addActionError(sm.getString("NiePodanoMinimalnejLiczbyWhasle",charType));
        	else if (howManyChars.intValue() < 1)
                addActionError(sm.getString("MinimalnaLiczbaWhaslePowinnaBycWiekszaOdZera",charType));
        	 else if (minPasswordLength != null && howManyChars.intValue() > minPasswordLength.intValue())
                 addActionError(sm.getString("MinimalnaLiczbaWhasleNieMozeBycWiekszaNizMinimalnaDlugoscHasla",charType));
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
	            Preferences prefs = DSApi.context().systemPreferences().
	                node(PasswordPolicy.NODE_PASSWORD_POLICY);

            try
            {
                DSApi.context().begin();

                prefs.putBoolean(PasswordPolicy.KEY_PASSWORD_EXPIRES, passwordExpires);
                if (daysToExpire != null)
                {
                    prefs.putInt(PasswordPolicy.KEY_DAYS_TO_EXPIRE, daysToExpire.intValue());
                }
                else
                {
                    prefs.remove(PasswordPolicy.KEY_DAYS_TO_EXPIRE);
                }
                prefs.putBoolean(PasswordPolicy.KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON, changePasswordAtFirstLogon);
                prefs.putInt(PasswordPolicy.KEY_MIN_PASSWORD_LENGTH, minPasswordLength.intValue());
                
                
                prefs.putBoolean(PasswordPolicy.KEY_MUST_HAVE_DIGITS, mustHaveDigits);
                if (howManyDigits != null)
                    prefs.putInt(PasswordPolicy.KEY_HOW_MANY_DIGITS, howManyDigits.intValue());
                else
                    prefs.remove(PasswordPolicy.KEY_HOW_MANY_DIGITS);
                
                prefs.putBoolean(PasswordPolicy.KEY_MUST_HAVE_LETTERS, mustHaveLetters);
                if (howManyLetters != null)
                    prefs.putInt(PasswordPolicy.KEY_HOW_MANY_LETTERS, howManyLetters.intValue());
                else
                    prefs.remove(PasswordPolicy.KEY_HOW_MANY_LETTERS);
                
                prefs.putBoolean(PasswordPolicy.KEY_MUST_HAVE_SMALL_LETTERS, mustHaveSmallLetters);
                if (howManySmallLetters != null)
                    prefs.putInt(PasswordPolicy.KEY_HOW_MANY_SMALL_LETTERS, howManySmallLetters.intValue());
                else
                    prefs.remove(PasswordPolicy.KEY_HOW_MANY_SMALL_LETTERS);
                
                prefs.putBoolean(PasswordPolicy.KEY_MUST_HAVE_SPECIALS, mustHaveSpecials);
                if (howManySpecials != null)
                    prefs.putInt(PasswordPolicy.KEY_HOW_MANY_SPECIALS, howManySpecials.intValue());
                else
                    prefs.remove(PasswordPolicy.KEY_HOW_MANY_SPECIALS);
                
                prefs.putBoolean(PasswordPolicy.KEY_LOCKING_PASSWORDS, lockingPasswords);
                if (howManyIncorrectLogins != null)
                    prefs.putInt(PasswordPolicy.KEY_HOW_MANY_INCORRECT_LOGINS, howManyIncorrectLogins.intValue());
                else
                    prefs.remove(PasswordPolicy.KEY_HOW_MANY_INCORRECT_LOGINS);

                java.util.Properties props = PasswordPolicy.getProperties(prefs);

                DSApi.context().commit();

                //u�ywane przez PasswordValidityFilter
                Docusafe.getServletContext().setAttribute(
                    PasswordPolicy.PROPERTIES_KEY, props);

                addActionMessage(sm.getString("ZapisanoDane"));
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
        
    }

    private class ClearHistory implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		PreparedStatement ps = null;
			     try
			     {
			         DSApi.context().begin();
			         ps = DSApi.context().prepareStatement("delete from ds_user_passwd_history");
			         ps.executeUpdate();			
			         DSApi.context().commit();
			         addActionMessage(sm.getString("HistoriaHaselWyczyszczona"));			         
			     }
			     catch (SQLException e)
			     {
			         throw new EdmSQLException(e);
			     }
			     catch (HibernateException e)
		         {
		             throw new EdmHibernateException(e);
		         }
			     finally
			     {
			    	 DSApi.context().closeStatement(ps);
			     }
        	}
		    catch (EdmException e)
		    {
		         DSApi.context().setRollbackOnly();
		         addActionError(e.getMessage());
		    }
	    }
    }
    
    private class SetExpireDate implements ActionListener 
    {
        public void actionPerformed(ActionEvent event)
        {
        	Preferences prefs = DSApi.context().systemPreferences().
            	node(PasswordPolicy.NODE_PASSWORD_POLICY);
        	
        	if(!prefs.getBoolean(PasswordPolicy.KEY_PASSWORD_EXPIRES, false))
        	{
        		addActionError(sm.getString("OpcjaWygaszaniaHaselMusiBycWybrana"));
        		return;
        	}
        	Date date = DateUtils.nullSafeParseJsDate(expireDate);
        	
        	if(date == null)
        	{
        		addActionError(sm.getString("PoleDataWygasnieciaNieJestWypelnioneLubJestWypelnioneNieprawidlowo"));
        		return;
        	}
        	
        	if(DateUtils.substract(date,new Date())<0)
        	{
        		addActionError(sm.getString("NieMoznaWybracDatyWczesniejszej"));
        		return;
        	}
        	
        	Integer _daysToExpire = new Integer(prefs.getInt(PasswordPolicy.KEY_DAYS_TO_EXPIRE, 0));
        	if(DateUtils.substract(date,new Date())>_daysToExpire)
        	{
        		addActionError(sm.getString("NieMoznaWybracDatyPozniejszejNizDataWynikajacaZopcjiWygaszeniaHasel"));
        		return;
        	}
        	
        	date = DateUtils.plusDays(date, _daysToExpire*-1);
        	Calendar cal = Calendar.getInstance();
        	cal.setTime(date);
        	String dateToSqlUpdate = "'"+cal.get(Calendar.YEAR)+"-"+String.valueOf(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH)+"'";
            
            
            Date dateAlreadyExpired = new Date();
            dateAlreadyExpired = DateUtils.plusDays(dateAlreadyExpired, _daysToExpire*-1);
            cal.setTime(dateAlreadyExpired);
            String dateAlreadyExpiredToSqlUpdate = "'"+cal.get(Calendar.YEAR)+"-"+String.valueOf(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH)+"'"; 
        	try
        	{
        		 PreparedStatement ps = null;
			     try
			     {
			    	 /**
			    	  * @TODO - do przerobki na prawdziwy PS
			    	  */
			         DSApi.context().begin();
			         ps = DSApi.context().prepareStatement
			         ("UPDATE DS_USER SET LASTPASSWORDCHANGE="+dateToSqlUpdate+" where LASTPASSWORDCHANGE>" + dateAlreadyExpiredToSqlUpdate);
			         ps.executeUpdate();			
			         DSApi.context().commit();
			         addActionMessage(sm.getString("HaslaWszystkichUzytkownikowStracaWaznosc",expireDate));
			         ps.close();
			     }
			     catch (SQLException e)
			     {
			         throw new EdmSQLException(e);
			     }
			     catch (HibernateException e)
		         {
		             throw new EdmHibernateException(e);
		         }
			     finally
			     {
			    	 DSApi.context().closeStatement(ps);
			     }
        	}
		    catch (EdmException e)
		    {
		         DSApi.context().setRollbackOnly();
		         addActionError(e.getMessage());
		    }
	    }    	
	}

    
    public boolean isPasswordExpires()
    {
        return passwordExpires;
    }

    public void setPasswordExpires(boolean passwordExpires)
    {
        this.passwordExpires = passwordExpires;
    }

    public Integer getDaysToExpire()
    {
        return daysToExpire;
    }

    public void setDaysToExpire(Integer daysToExpire)
    {
        this.daysToExpire = daysToExpire;
    }

    public Integer getMinPasswordLength()
    {
        return minPasswordLength;
    }

    public void setMinPasswordLength(Integer minPasswordLength)
    {
        this.minPasswordLength = minPasswordLength;
    }

    public boolean isMustHaveDigits()
    {
        return mustHaveDigits;
    }

    public void setMustHaveDigits(boolean mustHaveDigits)
    {
        this.mustHaveDigits = mustHaveDigits;
    }

    public Integer getHowManyDigits()
    {
        return howManyDigits;
    }

    public void setHowManyDigits(Integer howManyDigits)
    {
        this.howManyDigits = howManyDigits;
    }
    
    public boolean isMustHaveLetters()
    {
        return mustHaveLetters;
    }

    public void setMustHaveLetters(boolean mustHaveLetters)
    {
        this.mustHaveLetters = mustHaveLetters;
    }

    public Integer getHowManyLetters()
    {
        return howManyLetters;
    }

    public void setHowManyLetters(Integer howManyLetters)
    {
        this.howManyLetters = howManyLetters;
    }
    
    public boolean isMustHaveSmallLetters()
    {
        return mustHaveSmallLetters;
    }

    public void setMustHaveSmallLetters(boolean mustHaveSmallLetters)
    {
        this.mustHaveSmallLetters = mustHaveSmallLetters;
    }

    public Integer getHowManySmallLetters()
    {
        return howManySmallLetters;
    }

    public void setHowManySmallLetters(Integer howManySmallLetters)
    {
        this.howManySmallLetters = howManySmallLetters;
    }
    
    public boolean isMustHaveSpecials()
    {
        return mustHaveSpecials;
    }

    public void setMustHaveSpecials(boolean mustHaveSpecials)
    {
        this.mustHaveSpecials = mustHaveSpecials;
    }

    public Integer getHowManySpecials()
    {
        return howManySpecials;
    }

    public void setHowManySpecials(Integer howManySpecials)
    {
        this.howManySpecials = howManySpecials;
    }

	public String getExpireDate() {
		return expireDate;
	}
	
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public Integer getHowManyIncorrectLogins() {
		return howManyIncorrectLogins;
	}

	public void setHowManyIncorrectLogins(Integer howmanyIncorrectLogins) {
		this.howManyIncorrectLogins = howmanyIncorrectLogins;
	}

	public boolean isLockingPasswords() {
		return lockingPasswords;
	}

	public void setLockingPasswords(boolean lockingPasswords) {
		this.lockingPasswords = lockingPasswords;
	}

	public boolean isChangePasswordAtFirstLogon() {
		return changePasswordAtFirstLogon;
	}

	public void setChangePasswordAtFirstLogon(boolean changePasswordAtFirstLogon) {
		this.changePasswordAtFirstLogon = changePasswordAtFirstLogon;
	}


}