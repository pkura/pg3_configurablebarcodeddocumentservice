package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.util.prefs.Preferences;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OfficeIdsAction.java,v 1.7 2006/02/20 15:42:33 lk Exp $
 */
public class OfficeIdsAction extends EventActionSupport
{
    private String portfolioPrefix;
    private String portfolioPrefixSep;
    private String portfolioMiddle;
    private String portfolioSuffixSep;
    private String portfolioSuffix;
    private String portfolioRegexp;

    private String subportfolioPrefix;
    private String subportfolioPrefixSep;
    private String subportfolioMiddle;
    private String subportfolioSuffixSep;
    private String subportfolioSuffix;
    private String subportfolioRegexp;

    private String casePrefix;
    private String casePrefixSep;
    private String caseMiddle;
    private String caseSuffixSep;
    private String caseSuffix;
    private String caseRegexp;
    private String officeCaseRegexpInfo;
    private String officeFolderRegexpInfo;
    private String officeSubfolderRegexpInfo;
   
    private String documentFormat;
//    private String documentRegexp;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Preferences prefs = DSApi.context().systemPreferences().node(
                Container.NODE_BASE+"/"+DSDivision.ROOT_GUID);

            portfolioPrefix = prefs.get(Container.OF_PREFIX, null);
            portfolioPrefixSep = prefs.get(Container.OF_PREFIX_SEP, null);
            portfolioMiddle = prefs.get(Container.OF_MIDDLE, null);
            portfolioSuffixSep = prefs.get(Container.OF_SUFFIX_SEP, null);
            portfolioSuffix = prefs.get(Container.OF_SUFFIX, null);
            portfolioRegexp = prefs.get(Container.OF_REGEXP, null);
            officeFolderRegexpInfo = prefs.get(Container.OF_REGEXP_INFO, null);
            
            subportfolioPrefix = prefs.get(Container.OSF_PREFIX, null);
            subportfolioPrefixSep = prefs.get(Container.OSF_PREFIX_SEP, null);
            subportfolioMiddle = prefs.get(Container.OSF_MIDDLE, null);
            subportfolioSuffixSep = prefs.get(Container.OSF_SUFFIX_SEP, null);
            subportfolioSuffix = prefs.get(Container.OSF_SUFFIX, null);
            subportfolioRegexp = prefs.get(Container.OSF_REGEXP, null);
            officeSubfolderRegexpInfo = prefs.get(Container.OSF_REGEXP_INFO, null);
            
            

            casePrefix = prefs.get(Container.OC_PREFIX, null);
            casePrefixSep = prefs.get(Container.OC_PREFIX_SEP, null);
            caseMiddle = prefs.get(Container.OC_MIDDLE, null);
            caseSuffixSep = prefs.get(Container.OC_SUFFIX_SEP, null);
            caseSuffix = prefs.get(Container.OC_SUFFIX, null);
            caseRegexp = prefs.get(Container.OC_REGEXP, null);
            officeCaseRegexpInfo = prefs.get(Container.OC_REGEXP_INFO, null);
//            documentRegexp = prefs.get(Container.DOC_REGEXP, null);
            documentFormat = prefs.get(Container.DOC_FORMAT, null);
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                Preferences prefs = DSApi.context().systemPreferences().node(
                    Container.NODE_BASE+"/"+DSDivision.ROOT_GUID);

                portfolioPrefix = TextUtils.trimmedStringOrNull(portfolioPrefix);
                portfolioPrefixSep = TextUtils.trimmedStringOrNull(portfolioPrefixSep);
                portfolioMiddle = TextUtils.trimmedStringOrNull(portfolioMiddle);
                portfolioSuffixSep = TextUtils.trimmedStringOrNull(portfolioSuffixSep);
                portfolioSuffix = TextUtils.trimmedStringOrNull(portfolioSuffix);
                portfolioRegexp = TextUtils.trimmedStringOrNull(portfolioRegexp);

                subportfolioPrefix = TextUtils.trimmedStringOrNull(subportfolioPrefix);
                subportfolioPrefixSep = TextUtils.trimmedStringOrNull(subportfolioPrefixSep);
                subportfolioMiddle = TextUtils.trimmedStringOrNull(subportfolioMiddle);
                subportfolioSuffixSep = TextUtils.trimmedStringOrNull(subportfolioSuffixSep);
                subportfolioSuffix = TextUtils.trimmedStringOrNull(subportfolioSuffix);
                subportfolioRegexp = TextUtils.trimmedStringOrNull(subportfolioRegexp);

                casePrefix = TextUtils.trimmedStringOrNull(casePrefix);
                casePrefixSep = TextUtils.trimmedStringOrNull(casePrefixSep);
                caseMiddle = TextUtils.trimmedStringOrNull(caseMiddle);
                caseSuffixSep = TextUtils.trimmedStringOrNull(caseSuffixSep);
                caseSuffix = TextUtils.trimmedStringOrNull(caseSuffix);
                caseRegexp = TextUtils.trimmedStringOrNull(caseRegexp);
                officeFolderRegexpInfo= TextUtils.trimmedStringOrNull(officeFolderRegexpInfo);
                officeSubfolderRegexpInfo = TextUtils.trimmedStringOrNull(officeSubfolderRegexpInfo);
                officeCaseRegexpInfo = TextUtils.trimmedStringOrNull(officeCaseRegexpInfo);
                
                documentFormat = TextUtils.trimmedStringOrNull(documentFormat);
//                documentRegexp = TextUtils.trimmedStringOrNull(documentRegexp);

                prefsPut(prefs, Container.OF_PREFIX, portfolioPrefix);
                prefsPut(prefs, Container.OF_PREFIX_SEP, portfolioPrefixSep);
                prefsPut(prefs, Container.OF_MIDDLE, portfolioMiddle);
                prefsPut(prefs, Container.OF_SUFFIX_SEP, portfolioSuffixSep);
                prefsPut(prefs, Container.OF_SUFFIX, portfolioSuffix);
                prefsPut(prefs, Container.OF_REGEXP, portfolioRegexp);
                prefsPut(prefs, Container.OF_REGEXP_INFO, officeFolderRegexpInfo);
                
                prefsPut(prefs, Container.OSF_PREFIX, subportfolioPrefix);
                prefsPut(prefs, Container.OSF_PREFIX_SEP, subportfolioPrefixSep);
                prefsPut(prefs, Container.OSF_MIDDLE, subportfolioMiddle);
                prefsPut(prefs, Container.OSF_SUFFIX_SEP, subportfolioSuffixSep);
                prefsPut(prefs, Container.OSF_SUFFIX, subportfolioSuffix);
                prefsPut(prefs, Container.OSF_REGEXP, subportfolioRegexp);
                prefsPut(prefs, Container.OSF_REGEXP_INFO, officeSubfolderRegexpInfo);
                
                prefsPut(prefs, Container.OC_PREFIX, casePrefix);
                prefsPut(prefs, Container.OC_PREFIX_SEP, casePrefixSep);
                prefsPut(prefs, Container.OC_MIDDLE, caseMiddle);
                prefsPut(prefs, Container.OC_SUFFIX_SEP, caseSuffixSep);
                prefsPut(prefs, Container.OC_SUFFIX, caseSuffix);
                prefsPut(prefs, Container.OC_REGEXP, caseRegexp);
                prefsPut(prefs, Container.OC_REGEXP_INFO, officeCaseRegexpInfo);

                prefsPut(prefs, Container.DOC_FORMAT, documentFormat);
//                prefsPut(prefs, Container.DOC_REGEXP, documentRegexp);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

        private void prefsPut(Preferences prefs, String key, String value) {
            if (value != null) {
                prefs.put(key, value);
            } else {
                prefs.remove(key);
            }
        }
    }

    public String getPortfolioPrefix()
    {
        return portfolioPrefix;
    }

    public void setPortfolioPrefix(String portfolioPrefix)
    {
        this.portfolioPrefix = portfolioPrefix;
    }

    public String getPortfolioPrefixSep()
    {
        return portfolioPrefixSep;
    }

    public void setPortfolioPrefixSep(String portfolioPrefixSep)
    {
        this.portfolioPrefixSep = portfolioPrefixSep;
    }

    public String getPortfolioMiddle()
    {
        return portfolioMiddle;
    }

    public void setPortfolioMiddle(String portfolioMiddle)
    {
        this.portfolioMiddle = portfolioMiddle;
    }

    public String getPortfolioSuffixSep()
    {
        return portfolioSuffixSep;
    }

    public void setPortfolioSuffixSep(String portfolioSuffixSep)
    {
        this.portfolioSuffixSep = portfolioSuffixSep;
    }

    public String getPortfolioSuffix()
    {
        return portfolioSuffix;
    }

    public void setPortfolioSuffix(String portfolioSuffix)
    {
        this.portfolioSuffix = portfolioSuffix;
    }

    public String getCasePrefix()
    {
        return casePrefix;
    }

    public void setCasePrefix(String casePrefix)
    {
        this.casePrefix = casePrefix;
    }

    public String getCasePrefixSep()
    {
        return casePrefixSep;
    }

    public void setCasePrefixSep(String casePrefixSep)
    {
        this.casePrefixSep = casePrefixSep;
    }

    public String getCaseMiddle()
    {
        return caseMiddle;
    }

    public void setCaseMiddle(String caseMiddle)
    {
        this.caseMiddle = caseMiddle;
    }

    public String getCaseSuffixSep()
    {
        return caseSuffixSep;
    }

    public void setCaseSuffixSep(String caseSuffixSep)
    {
        this.caseSuffixSep = caseSuffixSep;
    }

    public String getCaseSuffix()
    {
        return caseSuffix;
    }

    public void setCaseSuffix(String caseSuffix)
    {
        this.caseSuffix = caseSuffix;
    }

    public String getDocumentFormat()
    {
        return documentFormat;
    }

    public void setDocumentFormat(String documentFormat)
    {
        this.documentFormat = documentFormat;
    }

    public String getSubportfolioPrefix()
    {
        return subportfolioPrefix;
    }

    public void setSubportfolioPrefix(String subportfolioPrefix)
    {
        this.subportfolioPrefix = subportfolioPrefix;
    }

    public String getSubportfolioPrefixSep()
    {
        return subportfolioPrefixSep;
    }

    public void setSubportfolioPrefixSep(String subportfolioPrefixSep)
    {
        this.subportfolioPrefixSep = subportfolioPrefixSep;
    }

    public String getSubportfolioMiddle()
    {
        return subportfolioMiddle;
    }

    public void setSubportfolioMiddle(String subportfolioMiddle)
    {
        this.subportfolioMiddle = subportfolioMiddle;
    }

    public String getSubportfolioSuffixSep()
    {
        return subportfolioSuffixSep;
    }

    public void setSubportfolioSuffixSep(String subportfolioSuffixSep)
    {
        this.subportfolioSuffixSep = subportfolioSuffixSep;
    }

    public String getSubportfolioSuffix()
    {
        return subportfolioSuffix;
    }

    public void setSubportfolioSuffix(String subportfolioSuffix)
    {
        this.subportfolioSuffix = subportfolioSuffix;
    }

    public String getPortfolioRegexp() {
        return portfolioRegexp;
    }

    public void setPortfolioRegexp(String portfolioRegexp) {
        this.portfolioRegexp = portfolioRegexp;
    }

    public String getSubportfolioRegexp() {
        return subportfolioRegexp;
    }

    public void setSubportfolioRegexp(String subportfolioRegexp) {
        this.subportfolioRegexp = subportfolioRegexp;
    }

    public String getCaseRegexp() {
        return caseRegexp;
    }

    public void setCaseRegexp(String caseRegexp) {
        this.caseRegexp = caseRegexp;
    }

	
	public String getOfficeCaseRegexpInfo()
	{
		return officeCaseRegexpInfo;
	}

	
	public void setOfficeCaseRegexpInfo(String officeCaseRegexpInfo)
	{
		this.officeCaseRegexpInfo = officeCaseRegexpInfo;
	}

	
	public String getOfficeFolderRegexpInfo()
	{
		return officeFolderRegexpInfo;
	}

	
	public void setOfficeFolderRegexpInfo(String officeFolderRegexpInfo)
	{
		this.officeFolderRegexpInfo = officeFolderRegexpInfo;
	}

	
	public String getOfficeSubfolderRegexpInfo()
	{
		return officeSubfolderRegexpInfo;
	}

	
	public void setOfficeSubfolderRegexpInfo(String officeSubfolderRegexpInfo)
	{
		this.officeSubfolderRegexpInfo = officeSubfolderRegexpInfo;
	}

//    public String getDocumentRegexp() {
//        return documentRegexp;
//    }
//
//    public void setDocumentRegexp(String documentRegexp) {
//        this.documentRegexp = documentRegexp;
//    }
}
