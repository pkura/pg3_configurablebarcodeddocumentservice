package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.address.Address;
import pl.compan.docusafe.core.address.Floor;
import pl.compan.docusafe.core.address.UserLocation;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* User: Administrator, Date: 2005-04-26 13:30:32 */

/**
 * Klasa implememtuj�ca akcj� wy�wietlenia listy adres�w.
 * Powi�zana jest z widokiem /admin/address.jsp
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 * @version $Id: AddressAction.java,v 1.7 2010/05/17 14:17:56 kamilj Exp $
 */
public class AddressAction extends EventActionSupport
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddressAction.class);
	
	/**
	 * Za�adowanie �r�de� tekstowych z komunikatami z bierz�cego pakietu
	 */
	private static StringManager sm = 
		StringManager.getManager(EditAddressAction.class.getPackage().getName());

	/**
	 * Lista dodanych adres�w
	 */
    private List<Address> addresses;
    
    /**
     * Lista z identyfikatorami adres�w do usuni�cia
     * @see Delete
     */
    private Long[] deleteIds;
    
    /**
     * Sk�adowa przechowuj�ca identyfikator centrali
     */
    private Long headOfficeId;

    /**
     * Konfiguracja akcji
     */
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Delete()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSelectHeadOffice")
        	.append(OpenHibernateSession.INSTANCE)
        	.append(new SelectHeadOffice())
        	.append(fillForm)
        	.appendFinally(CloseHibernateSession.INSTANCE);
    }

    /**
     * Klasa implementuj�ca akcj� wy�wietlenia dost�pnych adres�w
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
            {
                DSApi.context().begin();
                addresses = Address.getAll();
                DSApi.context().commit();
            }
            catch (EdmException ex)
            {
                DSApi.context().setRollbackOnly();
                addActionError(ex.getMessage());
                LOG.error(ex.getMessage(), ex);
            }
        }
    }
    
    /**
     * Klasa wewnt�rzna wykonuj�ca akcj� usuni�cia wybranych adres�w
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class Delete implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{
    		try
    		{	
    			if (deleteIds == null || deleteIds.length == 0)
    				return;
    			
    			DSApi.context().begin();
    			
    			// usuni�cie wybranych adres�w
    			for (Long id : deleteIds)
    			{
    				Address address = Address.find(id);
    				// pobranie identyfikator�w pi�ter, kt�re zostan� usuni�te
    				List<Long> deleteFloorIds = (List<Long>) DSApi.context().session()
    					.createQuery("select f.id from " + Floor.class.getName() + " f where f.address = ?")
    					.setParameter(0, address)
    					.list();
    				
    				// usuni�cie wybranych pi�ter i powi�zanych z nimi relacji
    				for (Long floorId : deleteFloorIds)
    				{
    					Floor floor = Floor.find(floorId);
    					List<UserLocation> locations = DSApi.context().session()
    						.createQuery("from " + UserLocation.class.getName() + " u where u.floor = ?")
    						.setParameter(0, floor)
    						.list();
    					if (locations != null && locations.size() > 0)
    					{
    						// dla u�ytkownik�w w lokalizacjach kt�re zostan� usuni�te nale�y 
    						// przypisa� warto�� null dla pola userLocation
    						DSApi.context().session().createQuery("update " + UserImpl.class.getName() 
    								+ " u set u.userLocation = :loc where u.userLocation in (:locations)")
    								.setParameter("loc", null)
    								.setParameterList("locations", locations)
    								.executeUpdate();	
    						
    						// teraz mozna usun�� lokalizacje
    						for (UserLocation location : locations)
    						{
    							// usuni�cie powi�zanych lokalizacji
    							Persister.delete(location);
    						}
    					}
    				}
    				
    				// teraz mo�na usun�� adres wraz z pi�trami
    				Persister.delete(address);
    			}
    			
    			addActionMessage(sm.getString("PoprawnieUsunietoAdresy"));
    			
    			DSApi.context().commit();
    		}
    		catch (EdmException ex)
    		{
    			addActionError(ex.getMessage());
    			DSApi.context()._rollback();
    			LOG.error(ex.getMessage(), ex);
    		}
    	}
    }
    
    /**
     * Klasa implementuj�ca akcj� ustawienia wybranego adresu jako central�
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class SelectHeadOffice implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{
    		try
    		{
    			DSApi.context().begin();
    			
    			if (headOfficeId == null)
    				throw new EdmException(sm.getString("ZaznaczCentrale"));
    			
    			Address address = Address.find(headOfficeId);
    			if (address == null)
    				throw new EdmException(sm.getString("ZaznaczCentrale"));
    			
    			address.setHeadOffice(true);
    			DSApi.context().session().update(address);
    			DSApi.context().session().createQuery(
    					"update " + Address.class.getName() + " a set " +
						"a.headOffice = ? where a.id != ?")
						.setBoolean(0, false)
						.setLong(1, address.getId())
						.executeUpdate();
    			
    			// ustawienie adresu centrali w ustawieniach globalnych
    			Map<String, String> addressMap = new LinkedHashMap<String, String>();
    			addressMap.put(GlobalPreferences.ORGANIZATION, address.getOrganization());
    			addressMap.put(GlobalPreferences.STREET, address.getStreet());
    			addressMap.put(GlobalPreferences.ZIP, address.getZipCode());
    			addressMap.put(GlobalPreferences.LOCATION, address.getRegion());
    			GlobalPreferences.setAddress(addressMap);
    			
    			addActionMessage(sm.getString("PoprawnieZmienionoCentrale"));
    			
    			DSApi.context().commit();
    		}
    		catch (EdmException ex)
    		{
    			DSApi.context()._rollback();
    			addActionError(ex.getMessage());
    			LOG.error(ex.getMessage(), ex);
    		}
    	}
    }
    
    /**
     * Metoda zwracaj�ca list� zapisanych w bazie adres�w organizacji
     * 
     * @return
     */
	public List<Address> getAddresses() 
	{
		return addresses;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� deleteIds
	 * 
	 * @return
	 */
	public Long[] getDeleteIds() 
	{
		return deleteIds;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� deleteIds
	 * 
	 * @param deleteIds
	 */
	public void setDeleteIds(Long[] deleteIds) 
	{
		this.deleteIds = deleteIds;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� headOffice
	 * 
	 * @return
	 */
	public Long getHeadOfficeId() 
	{
		return headOfficeId;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� headOffice
	 * 
	 * @param headOfficeId
	 */
	public void setHeadOfficeId(Long headOfficeId) 
	{
		this.headOfficeId = headOfficeId;
	}
}
