package pl.compan.docusafe.web.admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class CloseUserTask extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CloseUserTask.class);
    
    private Map<String, String> usersAll;
    private String[] usernames;
    private String documentDate;
    private List<String> messages;
    
    private static StringManager sm =
        GlobalPreferences.loadPropertiesFile(CloseUserTask.class.getPackage().getName(),null);
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doClose").
            append(OpenHibernateSession.INSTANCE).
            append(new Close()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doCloseILPLAPP").
	        append(OpenHibernateSession.INSTANCE).
	        append(new CloseILPLAPP()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    
    private class FillForm implements ActionListener 
    {

        public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("kamilj").debug("test loggera");
             try
            {
                usersAll = new LinkedHashMap<String, String>();
                List<DSUser> usersList = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                Iterator<DSUser> itr = usersList.iterator();
                while (itr.hasNext())
                {
                    DSUser user = itr.next();
                    usersAll.put(user.getName(), user.asLastnameFirstname());
                }
                          
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }  
        }
    }
    
    private class CloseILPLAPP implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
           if(documentDate != null)
           {
               
                if( usernames != null && usernames.length >0)
                {
                	try
                	{
                		Thread t = new Thread(new CloseILPLAPPTask(DateUtils.parseJsDate((documentDate))));//dd-mm-yyyy
                		t.setPriority(Thread.MIN_PRIORITY);
                        t.start();
                	}
                	catch (Exception e) {
						
					}
            		
                    event.setResult("success");
                }
                else
                {
                    addActionMessage(sm.getString("NieWybranoZadnegoUzytkownika"));
                }
           }
           else
           {
               addActionMessage(sm.getString("NiePodanoDaty")); 
           }
        } 
    }
    private class CloseILPLAPPTask implements Runnable
    {
    	private Date date;
    	public CloseILPLAPPTask(Date date)
    	{
    		this.date = date;
    	}

		public void run() 
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
				try 
				{
					DSApi.openAdmin();
					ps = DSApi.context().prepareStatement("select dso_document_id from dsw_tasklist  where dockindname  = 'Windykacja' and incomingdate < ?");
					ps.setDate(1, new java.sql.Date(date.getTime()));
					log.error("Zamyka zadanie {}",new java.sql.Date(date.getTime()));
					rs = ps.executeQuery();
					int i = 0;
					while(rs.next())
					{
						
						DSApi.context().begin();
						WorkflowFactory.getInstance().manualFinish(rs.getLong(1), false);
						DSApi.context().commit();
						i++;
						if(i%100 == 99)
						{
							log.error("Czyszczenie sesi po {} dokumentach",i);
							DSApi.context().session().flush();
							DSApi.context().session().clear();
						}
					}
					
				} 
				catch (Exception e) 
				{
					log.error(e.getMessage(), e);
				}
				finally
				{
					DSApi._close();
				}
				log.debug("Skonczyl");
			
		}
    }
    private class Close implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
           if(documentDate != null)
           {
               
                if( usernames != null && usernames.length >0)
                {
                	try
                	{
                		Thread t = new Thread(new CloseTask(usernames,DateUtils.parseDateAnyFormat(documentDate)));
                		t.setPriority(Thread.MIN_PRIORITY);
                        t.start();
                	}
                	catch (Exception e) {
						
					}
            		
                    event.setResult("success");
                }
                else
                {
                    addActionMessage(sm.getString("NieWybranoZadnegoUzytkownika"));
                }
           }
           else
           {
               addActionMessage(sm.getString("NiePodanoDaty")); 
           }
        } 
    }
    
    private class CloseTask implements Runnable
    {
    	private String[] names;
    	private Date date;
    	public CloseTask(String[] name,Date date)
    	{
    		this.names = name;
    		this.date = date;
    	}

		public void run() 
		{
			for (String username : names)
			{
				log.debug("Start clean dla {}",username);
				TaskList tasks;
				try 
				{
					DSApi.openAdmin();
					tasks = (TaskList) ServiceManager.getService(TaskList.NAME);
					log.debug("SZUKA {}",new Date().getTime());
					List<Task> userTasks = tasks.getTasks(DSUser.findByUsername(username));
					log.debug("Znalazl {} zadan {}",new Date().getTime(),userTasks.size());
					int i = 0;
					for (Task task : userTasks)
					{
						if (task.getReceiveDate() != null &&  task.getReceiveDate().getTime() < date.getTime())
						{
							log.error("Zamyka zadanie {}",task.getDocumentId());
							DSApi.context().begin();
							String activityId = WorkflowFactory.getInstance().keyToId(task.getActivityKey());
							WorkflowFactory.getInstance().manualFinish(activityId, false);	
							DSApi.context().commit();
							i++;
							if(i%100 == 99)
							{
								log.debug("Czyszczenie sesi po {} dokumentach",i);
								DSApi.context().session().flush();
								DSApi.context().session().clear();
							}
						}
						else
						{
							log.debug("Nie Zamyka zadania {}",task.getDocumentId());
						}
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				finally
				{
					DSApi._close();
				}
				log.debug("Skonczyl");
			}
		}
    }
    
    public String[] getUsernames()
    {
        return usernames;
    }

    public void setUsernames(String[] usernames)
    {
        this.usernames = usernames;
    }

    public Map<String, String> getUsersAll()
    {
        return usersAll;
    }

    public void setUsersAll(Map<String, String> usersAll)
    {
        this.usersAll = usersAll;
    }

    public String getDocumentDate()
    {
        return documentDate;
    }

    public void setDocumentDate(String documentDate)
    {
        this.documentDate = documentDate;
    }

    public List<String> getMessages()
    {
        return messages;
    }

    public void setMessages(List<String> messages)
    {
        this.messages = messages;
    }
    
}
