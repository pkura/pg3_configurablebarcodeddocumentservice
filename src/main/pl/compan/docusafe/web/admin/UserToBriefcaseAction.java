package pl.compan.docusafe.web.admin;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.UserToBriefcase;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 *  Klasa dla widoku user-to-briefcaser, powi�zanie u�ytkownik�w do teczek 
 *  tabela w bazie DSO_USER_TO_BRIEFCASE
 *  
 * @author Grzegorz Filip
 *
 */
@SuppressWarnings("serial")
public class UserToBriefcaseAction extends EventActionSupport
{
	
	

	private static Logger log = LoggerFactory.getLogger(UserToBriefcaseAction.class);
	
	private List<ValueToList> list_user_to_briefcase;
	private List<ValueToList> users;
	private List<ValueToList> briefcase;
	private String usersBriefcaseList;
	private String usersList;
	private String sortUsersList;
	

	private String briefcaseList;
	private Boolean canView =false;
	private Boolean canCreate=false;
	private Boolean canModify=false;
	private Boolean	canPrint=false;
	private Boolean	canViewCases=false;
	private Boolean	sorted = false;
	private Boolean canAddCasestoBriefcase=false;
	
	
	

	Map <Integer ,String > uprawnienia;
	
	//private Boolean canView;
	
	/*canView = true;
	canDelete = false;
	canCreate = false;
	canChangeNumber = false;
	canMoveToNextYear = false;
	canPrint=false;
	canModify=false;
	canSetClerk=false;*/

	
	protected void setup()
    {
		try {
			setBriefcase();
		} catch (EdmException e) {
			log.error("", e);
			e.printStackTrace();
		}
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDelete").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Delete()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSortByUser").
        append(OpenHibernateSession.INSTANCE).
        append(new sortByUser()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDefault").
        append(OpenHibernateSession.INSTANCE).
        append(new doDefaultAction()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	private class FillForm implements ActionListener
    	
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			//umieszcza list� u�ytkownik�w, teczek i list� mapowania w formularzu

			try
			{
				if (!sorted)
				{
					if (!DSApi.isContextOpen())
						DSApi.openContextIfNeeded();//Admin().begin();
					else
						DSApi.beginTransacionSafely();

					SearchResults<DSUser> userSearchResult = DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true);
					users = new ArrayList<ValueToList>(userSearchResult.count());
					while (userSearchResult.hasNext())
					{
						DSUser user = (DSUser) userSearchResult.next();
						String name = user.asLastnameFirstnameName();
						Long id = user.getId();
						users.add(new ValueToList(name, id.toString(), id));
					}
					//lista mapowania
					list_user_to_briefcase = new ArrayList<ValueToList>();
					List<UserToBriefcase> list = getUsersToBriefcaseList();
					for (UserToBriefcase utb : list)
					{

						Long idWpisu = utb.getId();
						Long idContainer = utb.getContainerId();
						String nazwaTeczki = utb.getContainerOfficeId();
						DSUser userh = DSUser.findById(utb.getUserId());
						String uprawnienia = getUprawnienia(utb);
						list_user_to_briefcase.add(new ValueToList(userh.asLastnameFirstname() + " - Przypisany do Teczki -> " + nazwaTeczki
								+ " z uprawnieniami  : " + uprawnienia, idWpisu.toString(), idContainer));
					}

					sortListUserToBriefcase(list_user_to_briefcase);
					

					if (DSApi.context().inTransaction())
						DSApi.context().commit();
				}
			} catch (EdmException e)
			{
				log.error("", e);
			} catch (SQLException e)
			{
				log.error("", e);
			}
		}

	}
	
	private class doDefaultAction implements ActionListener
    {
		 public void actionPerformed(ActionEvent event)
	        {   
			 sorted=false;
	        }
    }
	
	
	private class sortByUser implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			//umieszcza list� POsostowana po u�ytkowniku teczek

			try
			{
				if (!DSApi.isContextOpen())
					DSApi.openAdmin().begin();
				else
					DSApi.beginTransacionSafely();

				if (sortUsersList == null)
				{
					addActionError("Nie wybrano u�ytkownika");
					DSApi.context().rollback();
				} else
				{

					SearchResults<DSUser> userSearchResult = DSUser.getUsers(0, 0, DSUser.SORT_LASTNAME_FIRSTNAME, true);
					users = new ArrayList<ValueToList>(userSearchResult.count());

					while (userSearchResult.hasNext())
					{
						DSUser user = (DSUser) userSearchResult.next();
						String name = user.asLastnameFirstnameName();
						Long id = user.getId();

						users.add(new ValueToList(name, id.toString(), id));
					}
					DSUser userSelected = DSUser.findById(Long.parseLong(sortUsersList));
					//lista mapowania
					list_user_to_briefcase = new ArrayList<ValueToList>();

					List<UserToBriefcase> list = getUserSelectedList(userSelected.getId());

					for (UserToBriefcase utb : list)
					{

						Long idWpisu = utb.getId();
						Long idContainer = utb.getContainerId();
						String nazwaTeczki = utb.getContainerOfficeId();
						DSUser userh = DSUser.findById(utb.getUserId());
						String uprawnienia = getUprawnienia(utb);

						list_user_to_briefcase.add(new ValueToList(userh.asLastnameFirstname() + " - Przypisany do Teczki -> " + nazwaTeczki
								+ " z uprawnieniami  : " + uprawnienia, idWpisu.toString(), idContainer));
					}
					if (list_user_to_briefcase.isEmpty())
					{
						addActionError("Do u�ytkownika " + userSelected.asLastnameFirstnameName() + " Nie przypisano �adnych Uprawnie�");
					} else
					{
						sortListUserToBriefcase(list_user_to_briefcase);
					}

					sorted = true;
					if (DSApi.context().inTransaction())
						DSApi.context().commit();

				}
			} catch (EdmException e)
			{
				log.error("", e);
			} catch (SQLException e)
			{
			}
		}


	}
	
	private class Add implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			//dodanie do listy mapowania wybranego u�ytkownika i drukarki
			try
			{
				if (!DSApi.isContextOpen())
					DSApi.openAdmin();
				else
					DSApi.beginTransacionSafely();

				//usersList i briefcaseList zawieraj� id 
				if (usersList == null || briefcaseList == null)
				{
					addActionError("nie wybrano uzytkownika lub teczki");
					DSApi.context().rollback();
				} else
				{

					try
					{

						long userId = Long.parseLong(usersList);
						long containerId = Long.parseLong(briefcaseList);

						List<UserToBriefcase> list = getUserCriteriaList(userId, containerId);

						if (!list.isEmpty())
						{
							addActionError("U�ytkownik jest ju� przypisany do tej Teczki - Aby zmieni� uprawnienia usu� i przypisz go od nowa ");
							DSApi.context().rollback();
						} else
						{


							DSUser user = DSUser.findById(userId);
							String userName = user.getLastname();
							String userLastname = user.getLastname();
							String userFirstname = user.getFirstname();
							String userLastNameFirstName = user.asLastnameFirstname();
							OfficeFolder of = OfficeFolder.find(containerId);
							String oficeId = of.getOfficeId();

							UserToBriefcase wpis = new UserToBriefcase();
							wpis.setContainerId(containerId);
							wpis.setContainerOfficeId(oficeId);
							wpis.setUserId(userId);
							wpis.setUserName(userName);
							wpis.setUserFirstname(userFirstname);
							wpis.setUserLastname(userLastname);
							wpis.setCanView(canView);
							wpis.setCanCreate(canCreate);
							wpis.setCanModify(canModify);
							wpis.setCanPrint(canPrint);
							wpis.setCanViewCases(canViewCases);
							wpis.setCanAddCasestoBriefcase(canAddCasestoBriefcase);

							DSApi.context().session().save(wpis);

							DSApi.context().commit();
						}
					} catch (Exception e)
					{
						log.error("", e);
						throw new EdmException(e);
					}
					usersList = null;
					briefcaseList = null;
				}
			} catch (EdmException e)
			{
				log.error("", e);
			}
		}

	}
	
	private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	//usuni�cie z tabeli wybranego rekordu z listy mapowania
        	
        	try
        	{
        		if (!DSApi.isContextOpen())
        			DSApi.openAdmin();
        		else
        			DSApi.beginTransacionSafely();
        		
        		if (usersBriefcaseList != null){
        			//delete from DSO_USER_TO_BARCODE_PRINTER where userId = userPrinterList
        			
        			PreparedStatement ps = null;
        			try
        			{
        				
    					ps = DSApi.context().prepareStatement("DELETE FROM DSO_USER_TO_BRIEFCASE WHERE ID = ? ");
    					ps.setLong(1, Long.parseLong(usersBriefcaseList));
    					ps.execute();
        				
        			}
        			catch (Exception e)
        			{
        				log.error("", e);
        				throw new EdmException(e);
        			}
        			finally
        			{
        				DbUtils.closeQuietly(ps);
        			}
        			
        			usersBriefcaseList = null;
        		}        	
        	}
        	catch (EdmException e) 
        	{
        		;
			}
        }
    }
	
	//klasa do wrzucania danych do selecta
	@SuppressWarnings("unused")
	public class ValueToList {
		private String value;
		private String key;
		private long containerId;
		 
		public ValueToList() {
			value = "";
			key = "";
			containerId= 0;
		}
		public ValueToList(String value, String key ,Long idContainer) {
			this.value = value;
			this.key = key;
			this.containerId = idContainer;
		}
		public void setValue(String value){
			this.value = value;
		}
		public String getValue(){
			return value;
		}
		public void setKey(String key){
			this.key = key;
		}
		public String getKey(){
			return key;
		}
		public void setcontainerId(long containerId){
			this.containerId = containerId;
		}
		public long getcontainerId(){
			return containerId;
		}
	}

	private void sortListUserToBriefcase(List<ValueToList> list)
	{
		Collections.sort(list, new Comparator<ValueToList>() {

			public int compare(ValueToList o1, ValueToList o2)
			{
				return o1.containerId > o2.containerId ? -1 : 1;
			}
		});
		
	}
	private static List<UserToBriefcase> getUserCriteriaList(long userId, long containerId) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(UserToBriefcase.class);
		criteria.add(Restrictions.eq("containerId", containerId));
		criteria.add(Restrictions.eq("userId", userId));
		
		 return  criteria.list();
	}
	private static List<UserToBriefcase> getUserSelectedList(long userId) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(UserToBriefcase.class);
		criteria.add(Restrictions.eq("userId", userId));
		
		 return  criteria.list();
	}
	
	private static List<UserToBriefcase> getAssignedCasesByUser(long userId) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(UserToBriefcase.class)
		.setProjection(Projections.projectionList()
	    .add(Projections.property("containerId"), "containerId"))
	    .setResultTransformer(Transformers.aliasToBean(UserToBriefcase.class));
		criteria.add(Restrictions.eq("userId", userId));
	 
		 return  criteria.list();
	}
	
	private static List<UserToBriefcase> getUsersToBriefcaseList() throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(UserToBriefcase.class);
		
		 return  criteria.list();
	}
	
	public static boolean isOnListBriefcase(OfficeFolder portfolio, String userName) throws EdmException
	{
		DSUser user = DSUser.findByUsername(userName);

		if (!DSApi.context().inTransaction())
			DSApi.context().begin();

		try
		{
			long userId = user.getId();
			long containerId = portfolio.getId();

			List<UserToBriefcase> list = getUserCriteriaList(userId, containerId);
			if (!list.isEmpty())
				return true;

		} catch (Exception e)
		{
			log.error("B�ad przy wyszukiwaniu przypisania u�ytkownika do teczki ");
		}
		return false;

	}
	private String getUprawnienia(UserToBriefcase utb) throws SQLException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("");
		List<Boolean> list = getBolleanListPermission(utb);
		int z =1;
		for (Boolean b :list){
			if (b){
			 sb.append(uprawnienia.get(z)+" / ");
			}
			z++;
		}
		return sb.toString();
	}
	private List<Boolean> getBolleanListPermission(UserToBriefcase utb)
	{
		List<Boolean> list = new ArrayList<Boolean>();
		list.add(utb.getCanView());
		list.add(utb.getCanCreate());
		list.add(utb.getCanModify());
		list.add(utb.getCanPrint());
		list.add(utb.getCanViewCases());
		list.add(utb.getCanAddCasestoBriefcase());
		return  list;
	}

	private void setUprawnienia()
	{
		uprawnienia = new HashMap<Integer, String>();
		uprawnienia.put(1, "Mo�e podej�e�");
		uprawnienia.put(2, "Mo�e utworzy�");
		uprawnienia.put(3, "Mo�e modyfikowa�");
		uprawnienia.put(4, "Mo�e wydrukowa�");
		uprawnienia.put(5, "Mo�e przeglada� sprawy w teczce ");
		uprawnienia.put(6,"Mo�e dodawa� sprawy do teczki");
		
	}
	public static List<Long> getPrzypisaneSprawy(String userName) throws EdmException
	{
		DSUser user = DSUser.findByUsername(userName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Long> listCases = new ArrayList();
		if (!DSApi.context().inTransaction())
			DSApi.context().begin();
		
		try
		{ 
			
			List<UserToBriefcase>  list = getAssignedCasesByUser(user.getId());
			for (UserToBriefcase utb : list){
				 List<OfficeCase> cases = OfficeCase.findByParentId(utb.getContainerId());
				 for (OfficeCase c :cases ){
					 listCases.add(c.getId());
				 }
			}
		}catch (Exception e) {
			log.error("B�ad przy wyszukiwaniu przypisania u�ytkownika do teczki ");
		}
		return listCases;
		
	}
	public static Boolean hasPermisionToCase(DSUser dsUser, Document document) throws EdmException
	{
		
		OfficeDocument od = (OfficeDocument) document;
		if (od.getContainingCaseId()==null) 
			return true ;
		OfficeCase oc = OfficeCase.find(od.getContainingCaseId());
		long containerId = oc.getParent().getId();
		OfficeFolder of = OfficeFolder.find(containerId);
		
		 UserToBriefcase utb = resultsFolder(of,dsUser.getName());
		 if(utb==null)
			 return false;
		 else 
			return utb.getCanView();

	}
	public static boolean canVievCasesFromListBriefcase(OfficeFolder officeFolder, DSUser dsUser) throws EdmException
	{
		 UserToBriefcase utb = resultsFolder(officeFolder, dsUser.getName());
		 if(utb==null)
			 return false;
		 else 
		 return (Boolean) utb.getCanViewCases();
	}
	
	public static boolean canCreateCaseFromListBriefcase(OfficeFolder officeFolder, DSUser dsUser) throws EdmException
	{	
		 UserToBriefcase utb = resultsFolder(officeFolder, dsUser.getName());
		 if(utb==null)
			 return false;
		 else 
		return (Boolean)  utb.getCanAddCasestoBriefcase();
	}
	public static boolean canCreatePortfolioFromListBriefcase(OfficeFolder officeFolder, DSUser dsUser) throws EdmException
	{
		 UserToBriefcase utb = resultsFolder(officeFolder, dsUser.getName());
		 if(utb==null)
			 return false;
		 else 
		 return (Boolean) utb.getCanCreate();
	}
	
	
	public static UserToBriefcase resultsFolder(OfficeFolder portfolio, String userName) throws EdmException {
		try {
			
			DSUser user = DSUser.findByUsername(userName);
			long userId = user.getId();
			long containerId =portfolio.getId();

			
			  List<UserToBriefcase> list =  getUserCriteriaList(userId , containerId);
			  if(!list.isEmpty())
					return list.get(0);
					  else 
						  return null;
		
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}

	}
	

	//ustawienie dost�pnych teczek
	private void setBriefcase() throws EdmException{
		 //dodanie teczek do listy                
        briefcase = new ArrayList<ValueToList>();
        setUprawnienia();
        if (!DSApi.isContextOpen())
        	DSApi.openAdmin();
		else
			DSApi.beginTransacionSafely();
		
			PreparedStatement ps = null;
			try {
				ps = DSApi.context().prepareStatement("select * from DSO_CONTAINER where DISCRIMINATOR like 'FOLDER' order by id desc");
		
		ResultSet rs = ps.executeQuery();
		while(rs.next())
		{
			Long idContainer = rs.getLong(1);
			String nazwaTeczki = rs.getString(4);
			briefcase.add(new ValueToList(nazwaTeczki, idContainer.toString() ,idContainer));
			
		}
		
		rs.close();
		DSApi.context().closeStatement(ps);
		DSApi.close();
		}
		catch (Exception e) {
			log.error("", e);
		}
		
		
	}
	
	
	
	public static void onCreateBriefcase (Integer sequenceId ,String officeId) throws  SQLException, EdmException{
		
		
	/*	Statement st = DSApi.context().createStatement();
		PreparedStatement ps = null;
		
		try
		{		
			ps = DSApi.context().prepareStatement("Select MAX (ID) from DSO_USER_TO_BRIEFCASE");
				ResultSet rs = ps.executeQuery();
				
				
				long count = (long) rs.getInt(0);
				ps.close();
				ps = null;
				
				ps = DSApi.context().prepareStatement("INSERT INTO DSO_USER_TO_BRIEFCASE (containerId, containerOfficeId, userId, userName, userFirstname, userLastname) VALUES(?,?,?,?,?,?)");
				//ps.setLong(1, count+1);
				ps.setInt(1, sequenceId);
				ps.setString(2, officeId);
				ps.setInt(3, 1);
				ps.setString(4,  "admin");
				ps.setString(5,  "admin");
				ps.setString(6,  "admin");
				ps.execute();
				ps.close();
			
		}catch (Exception e) {
			log.error("", e);
			
		}
		DSApi.context().closeStatement(st);*/
		
	}
	public static void onUpdateBriefcase (Integer sequenceId ,String oldOfficeId, String newOfficeId) throws SQLException, EdmException{

		Statement st = DSApi.context().createStatement();
		//PreparedStatement ps = null;
		
		try
		{
			
		StringBuilder  sb = new StringBuilder();
			sb.append("UPDATE DSO_USER_TO_BRIEFCASE SET containerOfficeId = REPLACE(containerOfficeId,'"+oldOfficeId+"','"+newOfficeId+"')");
			sb.append("WHERE containerId = "+sequenceId.toString());
			sb.append(" and containerOfficeId like"+"'"+oldOfficeId+"'");	
			st.executeUpdate(sb.toString());
			
		} catch (SQLException e) {
        	
		}
		DSApi.context().closeStatement(st);
	}
	public static void onDeleteBriefcase (Integer sequenceId ,String officeId) throws SQLException, EdmException{

		Statement st = DSApi.context().createStatement();
		PreparedStatement ps = null;
		
		try
		{
				ps = DSApi.context().prepareStatement("DELETE FROM DSO_USER_TO_BRIEFCASE where officeId =? ");
				ps.setString(1, officeId);
				ps.execute();
				ps.close();
			
		}catch (Exception e) {
			log.error("", e);
		}
		DSApi.context().closeStatement(st);
	}

	public List<ValueToList> getlist_user_to_briefcase() {
		return list_user_to_briefcase;
	}

	public void setlist_user_to_briefcase(List<ValueToList> list_printer_user) {
		this.list_user_to_briefcase = list_user_to_briefcase;
	}

	public List<ValueToList> getUsers() {
		return users;
	}

	public void setUsers(List<ValueToList> users) {
		this.users = users;
	}

	public List<ValueToList> getbriefcase() {
		return briefcase;
	}

	public void setbriefcase(List<ValueToList> briefcase) {
		this.briefcase = briefcase;
	}

	public String getusersBriefcaseList() {
		return usersBriefcaseList;
	}

	public void setusersBriefcaseList(String usersBriefcaseList) {
		this.usersBriefcaseList = usersBriefcaseList;
	}

	public String getUsersList() {
		return usersList;
	}

	public void setUsersList(String usersList) {
		this.usersList = usersList;
	}

	public String getbriefcaseList() {
		return briefcaseList;
	}

	public void setbriefcaseList(String briefcaseList) {
		this.briefcaseList = briefcaseList;
	}
	public List<ValueToList> getList_user_to_briefcase() {
		return list_user_to_briefcase;
	}

	public void setList_user_to_briefcase(List<ValueToList> list_user_to_briefcase) {
		this.list_user_to_briefcase = list_user_to_briefcase;
	}

	public List<ValueToList> getBriefcase() {
		return briefcase;
	}

	public void setBriefcase(List<ValueToList> briefcase) {
		this.briefcase = briefcase;
	}

	public String getUsersBriefcaseList() {
		return usersBriefcaseList;
	}

	public void setUsersBriefcaseList(String usersBriefcaseList) {
		this.usersBriefcaseList = usersBriefcaseList;
	}

	public String getSortUsersList() {
		return sortUsersList;
	}

	public void setSortUsersList(String sortUsersList) {
		this.sortUsersList = sortUsersList;
	}

	public String getBriefcaseList() {
		return briefcaseList;
	}

	public void setBriefcaseList(String briefcaseList) {
		this.briefcaseList = briefcaseList;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Boolean getCanModify() {
		return canModify;
	}

	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}

	public Boolean getCanPrint() {
		return canPrint;
	}

	public void setCanPrint(Boolean canPrint) {
		this.canPrint = canPrint;
	}
	public Boolean getSorted() {
		return sorted;
	}

	public void setSorted(Boolean sorted) {
		this.sorted = sorted;
	}

	public Boolean getCanCreate() {
		return canCreate;
	}

	public void setCanCreate(Boolean canCreate) {
		this.canCreate = canCreate;
	}
	public Boolean getCanViewCases()
	{
		return canViewCases;
	}
	
	public void setCanViewCases(Boolean canViewCases)
	{
		this.canViewCases = canViewCases;
	}
	
	public Boolean canAddCasestoBriefcase()
	{
		return canAddCasestoBriefcase;
	}
	
	public void setCanAddCasestoBriefcase(Boolean canAddCasestoBriefcase)
	{
		this.canAddCasestoBriefcase = canAddCasestoBriefcase;
	}
	
	
	

}
