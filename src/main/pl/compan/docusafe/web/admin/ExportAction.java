package pl.compan.docusafe.web.admin;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Eksportuje dane systemowe do pliku.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExportAction.java,v 1.4 2007/11/27 12:11:46 mariuszk Exp $
 */
public class ExportAction extends EventActionSupport
{
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doExportUsers").
            append(OpenHibernateSession.INSTANCE).
            append(new ExportUsers()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    private class ExportUsers implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {

            HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("text/xml");
            response.setHeader("Content-Disposition", "attachment; filename=\"docusafe-users.xml\"");

            try
            {
                OutputStream os = response.getOutputStream();
                UserFactory.getInstance().exportUsersAndDivisions(os);
                os.close();
            }
            catch (IOException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
            }
        }
    }

}
