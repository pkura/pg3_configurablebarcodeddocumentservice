package pl.compan.docusafe.web.admin;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crypto.DocusafeDatabaseKey;
import pl.compan.docusafe.core.crypto.DocusafeKeyStore;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ManageEncryptionAction extends EventActionSupport 
{

	private Logger log = LoggerFactory.getLogger(ManageEncryptionAction.class);
	private StringManager sm = StringManager.getManager(ManageEncryptionAction.class.getPackage().getName());
	private static final long serialVersionUID = 1L;
	private List<DocusafeKeyStore> keys;
	private List<DocusafeKeyStore> grantedKeys;
	private Long keyId;
	private String key;

	protected void setup() 
	{
		registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	
	    registerListener("doGrantAccess").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GrantAccess()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    registerListener("doDeleteKey").
	        append(OpenHibernateSession.INSTANCE).
	        append(new DeleteKey()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	try
        	{
        		grantedKeys = new ArrayList<DocusafeKeyStore>();
        		keys = new ArrayList<DocusafeKeyStore>();
        		for(DocusafeKeyStore dks:DSApi.getAvailableKeyStoresInstances())
        		{
        			grantedKeys.addAll(dks.getAllGrantedKeys());
        			keys.addAll(dks.getKeysToGrant());
        		}
        	}
        	catch (EdmException e) 
        	{
				addActionError(e.getMessage());
				log.error(e.getMessage(),e);
			}
        }
    }
	
	private class GrantAccess implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	if(keyId==null)
        		addActionError(sm.getString("PodajIdKlucza"));
        	try
        	{
        		DocusafeKeyStore dks = DocusafeDatabaseKey.findKey(keyId);
        		DSApi.context().begin();
        		key = DSApi.context().credentials().grantKey(dks, dks.getKeyCode());
        		DSApi.context().commit();
        	}
        	catch (EdmException e) 
        	{
        		DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
				log.error(e.getMessage(),e);
			}
        	
        }
    }
	
	private class DeleteKey implements ActionListener
    {
        public void actionPerformed(final ActionEvent event)
        {
        	if(keyId==null)
        		addActionError(sm.getString("PodajIdKlucza"));
        	try
        	{
        		DocusafeKeyStore dks = DocusafeDatabaseKey.findKey(keyId);
        		DSApi.context().begin();
        		DSApi.context().session().delete(dks);
        		DSApi.context().commit();
        	}
        	catch (EdmException e) 
        	{
        		DSApi.context().setRollbackOnly();
				addActionError(e.getMessage());
				log.error(e.getMessage(),e);
			}
        	
        }
    }

	public List<DocusafeKeyStore> getKeys() {
		return keys;
	}

	public void setKeys(List<DocusafeKeyStore> keys) {
		this.keys = keys;
	}

	public Long getKeyId() {
		return keyId;
	}

	public void setKeyId(Long keyId) {
		this.keyId = keyId;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public List<DocusafeKeyStore> getGrantedKeys()
	{
		return grantedKeys;
	}

	public void setGrantedKeys(List<DocusafeKeyStore> grantedKeys)
	{
		this.grantedKeys = grantedKeys;
	}
	
	
	

}
