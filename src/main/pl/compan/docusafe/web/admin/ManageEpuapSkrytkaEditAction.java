package pl.compan.docusafe.web.admin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.drools.util.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.nfos.NfosSystem;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytkaManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * CRUD do obiektu EpuapSkrytka
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class ManageEpuapSkrytkaEditAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(ManageEpuapSkrytkaEditAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ManageEpuapSkrytkaEditAction.class.getPackage().getName(),null);
	private static final long serialVersionUID = 1L;
	//modyfikowana skrytka
	private EpuapSkrytka skrytka = new EpuapSkrytka();
	private FormFile certificate;
	private String certificatePassword;
	private String certificateHidden;	
	public String getCertificateHidden() {
		return certificateHidden;
	}

	public void setCertificateHidden(String certificateHidden) {
		this.certificateHidden = certificateHidden;
	}

	public String getCertificatePasswordHidden() {
		return certificatePasswordHidden;
	}

	public void setCertificatePasswordHidden(String certificatePasswordHidden) {
		this.certificatePasswordHidden = certificatePasswordHidden;
	}
	private String certificatePasswordHidden;
	private boolean newSkrytka;
	
    
	public boolean isNewSkrytka() {
		return newSkrytka;
	}
	
	public boolean getNewSkrytka() {
		return newSkrytka;
	}

	public void setNewSkrytka(boolean newSkrytka) {
		this.newSkrytka = newSkrytka;
	}

	@Override
	protected void setup()
	{
		FillForm fillForm = new FillForm();

	    registerListener(DEFAULT_ACTION).
	        append(OpenHibernateSession.INSTANCE).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	    
	    registerListener("doCreate").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Validate()).
        	append(new Create()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
	    
		registerListener("doEdit").
			append(OpenHibernateSession.INSTANCE).
			append(new Validate()).
			append(new Edit()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}
	/*@Override
	protected String preExecute() {
		if(skrytka.getId() == null || skrytka.getId() < 0){
			newSkrytka = true;
		}
		return super.preExecute();
	}*/
	
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	log.trace("filform");
        	logRequest(log);
        	
            try
            {
            	if(skrytka.getId() != null && skrytka.getId() > 0)
            	{
            		skrytka = EpuapSkrytkaManager.find(skrytka.getId());
            		certificateHidden = skrytka.getCertificateFileName();
            		certificatePasswordHidden = skrytka.getCertificatePassword();
            		newSkrytka = false;
//            		File file = new File(Docusafe.getHome() + "/" + skrytka.getCertificateFileName());
//					setCertificate(new FormFile(file, file.getName(), null));
//					setCertificatePassword(skrytka.getCertificatePassword());
//					event.setAttribute("certificatePassword", skrytka.getCertificatePassword());
            	}
            	else
            		newSkrytka = true;
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Validate implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
        {
    		//validacja wymaganych p�l
    		if(!validujPola()) return;
    		
            try
            {
            	/*if(skrytka.getId() != null && skrytka.getId() > 0)
            	{
            		skrytka = EpuapSkrytkaManager.find(skrytka.getId());
            	}*/
            	if(AvailabilityManager.isAvailable("epuap.multi.param1.enable") &&
            			"nfos".equals(Docusafe.getAdditionProperty("epuap.multi.param1.type")))
            	{
	            	EpuapSkrytka skrytkaUstawiona = EpuapSkrytkaManager.findByIdAndParam1(skrytka.getId() == null ? 0 : skrytka.getId(),skrytka.getParam1());
	            	if(skrytkaUstawiona != null)
	            	{
	            		addActionError("Taka skrytka z ustawionym systemem dziedzionowym ju� istnieje!");
	            		
	            		return;
	            	}
            	}
            	
            }catch(Exception e){
            	log.error("", e);
            	addActionError("Wyst�pi� nieznany b��d!");
            }
        }

		private boolean validujPola() {
			String identyfikatorPodmiotu = skrytka.getIdentyfikatorPodmiotu();
			String adresSkrytki = skrytka.getAdresSkrytki();
			String nazwaSkrytki = skrytka.getNazwaSkrytki();
			String email = skrytka.getEmail();
			String nfosSystem = skrytka.getParam1();
			
			boolean error = false;
			if(identyfikatorPodmiotu == null || identyfikatorPodmiotu.equals("")){
				addActionError("Brak identyfikatora podmiotu.");
				error = true;
			}
			if(adresSkrytki == null || adresSkrytki.equals("")){
				addActionError("Brak adresu skrytki.");
				error = true;
			}
			if(nazwaSkrytki == null || nazwaSkrytki.equals("")){
				addActionError("Brak nazwy skrytki.");
				error = true;
			}
			if (newSkrytka) {
				if (certificate == null || certificate.equals("")) {
					addActionError("Brak certyfikatu.");
					error = true;
				}
				if (certificatePassword == null
						|| certificatePassword.equals("")) {
					addActionError("Brak has�a certyfikatu.");
					error = true;
				}
			}
			if(email == null || email.equals("")){
				addActionError("Brak adresu email.");
				error = true;
			}
			if(nfosSystem == null || nfosSystem.equals("")){
				addActionError("Wybierz system dziedzinowy.");
				error = true;
			}
			return !error;
		}
    }
    	
    private class Create implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
        {
            try
            {   
            	if(hasActionErrors()) return;
            	saveCertificateToDisc();
            	if(!StringUtils.isEmpty(certificatePassword))
            	{
            		skrytka.setCertificatePassword(certificatePassword);
            	}
            	
            	if(skrytka.getId() == null){
            		Persister.create(skrytka);
            		addActionMessage("Utworzono skrytke o nazwie: " + skrytka.getNazwaSkrytki());
            		String aktywna = skrytka.getParam2();
                	if(aktywna == null || aktywna.equals("")){
        				addActionMessage("Uwaga: Skrytka jest nieaktywna.");
        			}
            	} else {
            		Persister.update(skrytka);
            		addActionMessage("Zaktualizowano");
            		log.info("aktualizuje {}", skrytka.getIdentyfikatorPodmiotu());
            	}
            	DSApi.context().session().flush();
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Edit implements ActionListener
    {
    	@Override
    	public void actionPerformed(ActionEvent event) {
    		if(hasActionErrors()) return;
    		try {
    			
    			if(skrytka.getCertificateFileName() == null)
    				skrytka.setCertificateFileName(certificateHidden);
    			if(skrytka.getCertificatePassword() == null)
    				skrytka.setCertificatePassword(certificatePasswordHidden);
    			Persister.update(skrytka);
    			
//    			if(certificate.getName() != null)
//    				skrytka.setCertificateFileName(certificate.getName());
//    			if(certificatePassword != null)
//    				skrytka.setCertificatePassword(certificatePassword);
//    			skrytka.saveOrUpdate();
    			
    			
//				TODO Poprawi� edycj� certyfikat�w, 
				saveCertificateToDisc();
            	if(!StringUtils.isEmpty(certificatePassword))
            	{
            		skrytka.setCertificatePassword(certificatePassword);
            	}
				
				addActionMessage("Zaktualizowano");
				log.info("aktualizuje {}", skrytka.getIdentyfikatorPodmiotu());
				DSApi.context().session().flush();
			} catch (Exception e) {
				addActionError(e.getMessage());
			}
    	}
    }

    private void saveCertificateToDisc() throws EdmException
    {    	
    	if(certificate == null)
    		return;
    	String basePath = Docusafe.getAdditionProperty("epuapSkrytka.certificateBasePath");
    	File dsHome = new File(Docusafe.getHome(), basePath);
    	if(!dsHome.exists())
    	{
    		dsHome.mkdir();
    	}
   		File cert = new File(dsHome, certificate.getFile().getName());
   		if(cert.exists())
   			throw new EdmException("Certyfikat o takiej nazwie ju� istnieje");
   		else
   		{
   			certificate.getFile().renameTo(cert);
   			skrytka.setCertificateFileName(basePath + "/" +certificate.getFile().getName());
  		}
    }
    
    /**
     * Zwraca list� system�w Nfos
     * @return
     */
    public List<NfosSystem> getNfosSystem()
    {
    	List<NfosSystem> systems = new ArrayList<NfosSystem>();
    	try
		{
    		DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
			systems = NfosSystem.list(true);
			log.error("ilosc {}", systems.size());
		} catch (EdmException e)
		{
			log.error("",e);
		}finally{
			DSApi._close();
		}
    	
    	return systems;
    }
	public EpuapSkrytka getSkrytka()
	{
		return skrytka;
	}

	public void setSkrytka(EpuapSkrytka skrytka)
	{
		this.skrytka = skrytka;
	}
	
	public FormFile getCertificate() {
		return certificate;
	}
	
	public void setCertificate(FormFile certificate) {
		this.certificate = certificate;
	}
	public String getCertificatePassword() {
		return certificatePassword;
	}
	public void setCertificatePassword(String certificatePassword) {
		this.certificatePassword = certificatePassword;
	}
	
	
}
