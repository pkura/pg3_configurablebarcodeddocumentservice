package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.jbpm.Constants;
import pl.compan.docusafe.core.office.workflow.jbpm.SwimlaneMapping;
import pl.compan.docusafe.util.TextUtils;

import java.util.LinkedHashMap;
import java.util.List;
/* User: Administrator, Date: 2007-02-14 16:57:01 */
import java.util.Map;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class EditSwimlaneMappingAction extends EventActionSupport
{
    private List tabs;
    private DSDivision[] divisions;
    private Long id;
    private String name;
    private String divisionGuid;
    private boolean creating;
    private Map<Integer,String> assignmentTypes;
    private Integer assignmentType;

    private String redirectUrl;

    private static String EV_FILL = "fill";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doInitCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new FillCreateForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(EV_FILL, new FillCreateForm()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(EV_FILL, new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(new Tabs(EditSwimlaneMappingAction.this).createTabs());
            try
            {
                creating = false;
                divisions = DSDivision.getAllDivisions();
                assignmentTypes = Constants.getAssignmentTypes();

                if (id != null)
                {
                    SwimlaneMapping swimlaneMapping = SwimlaneMapping.find(id);
                    name = swimlaneMapping.getName();
                    divisionGuid = swimlaneMapping.getDivisionGuid();
                    assignmentType = swimlaneMapping.getAssignmentType();
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class FillCreateForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(new Tabs(EditSwimlaneMappingAction.this).createTabs());
            try
            {
                creating = true;
                divisions = DSDivision.getAllDivisions();
                assignmentTypes = Constants.getAssignmentTypes();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (id != null)
                {
                    DSApi.context().begin();

                    SwimlaneMapping swimlaneMapping = SwimlaneMapping.find(id);
                    swimlaneMapping.setName(name);
                    swimlaneMapping.setDivisionGuid(divisionGuid);
                    swimlaneMapping.setAssignmentType(assignmentType);

                    DSApi.context().commit();
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            name = TextUtils.trimmedStringOrNull(name, 62);
            if (name == null)
            {
                addActionError("Nie podano nazwy dzia�u z Workflow");
                return;
            }

            try
            {
                DSApi.context().begin();

                if (SwimlaneMapping.findByName(name) != null)
                    throw new EdmException("Istnieje ju� mapowanie o takiej nazwie dzia�u z Workflow");

                SwimlaneMapping swimlaneMapping = new SwimlaneMapping();
                swimlaneMapping.setName(name);
                swimlaneMapping.setDivisionGuid(divisionGuid);
                swimlaneMapping.setAssignmentType(assignmentType);

                swimlaneMapping.create();

                DSApi.context().commit();

                event.skip(EV_FILL);
                event.setResult(REDIRECT);
                redirectUrl = "/admin/workflow/swimlane-mapping.action";
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (id != null)
                {
                    DSApi.context().begin();

                    SwimlaneMapping swimlaneMapping = SwimlaneMapping.find(id);
                    swimlaneMapping.delete();

                    DSApi.context().commit();

                    event.skip(EV_FILL);
                    event.setResult(REDIRECT);
                    redirectUrl = "/admin/workflow/swimlane-mapping.action";
                }
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public DSDivision[] getDivisions()
    {
        return divisions;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public boolean isCreating()
    {
        return creating;
    }

    public void setCreating(boolean creating)
    {
        this.creating = creating;
    }

    public List getTabs() {
        return tabs;
    }

    public void setTabs(List tabs) {
        this.tabs = tabs;
    }

    public Integer getAssignmentType()
    {
        return assignmentType;
    }

    public void setAssignmentType(Integer assignmentType)
    {
        this.assignmentType = assignmentType;
    }

    public Map<Integer, String> getAssignmentTypes()
    {
        return assignmentTypes;
    }
    
}
