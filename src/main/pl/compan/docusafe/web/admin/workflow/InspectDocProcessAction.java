package pl.compan.docusafe.web.admin.workflow;

import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.LogFactory;
import org.jbpm.JbpmConfiguration;
import org.jbpm.api.ManagementService;
import org.jbpm.api.job.Job;
import org.jbpm.pvm.internal.job.TimerImpl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotProvider;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.sql.PreparedStatementBuilder;
import pl.compan.docusafe.web.common.RenderBean;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.*;

public class InspectDocProcessAction extends EventActionSupport {
	private static final Logger log = LoggerFactory.getLogger(InspectDocProcessAction.class);

	private Long docId;
	private boolean jbpm;
	private boolean showTasks;
	private boolean allowRefresh;
	private String participantId;
	private String userFullName;
	private String divFullName;
	private WorkflowActivity manual;
	private List<WorkflowActivity> cc;
	private List<RenderBean> processRenderBeans;
	private String docIds;
	private String docIdsToRefresh;
    
	//import
	private String processName;
	private String processId;
	private String processAction;

	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION)
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doClose")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new Close())
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doCloseActiviti")
                .append(OPEN_HIBERNATE_SESSION)
                .append(new CloseActiviti())
                .append(fillForm)
                .appendFinally(CLOSE_HIBERNATE_SESSION);

		registerListener("doMultiClose")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new Close(true))
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

		registerListener("doProcessAction")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new ProcessAction())
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);

        registerListener("doRefreshTasklist")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new RefreshTasklist())
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
       
        registerListener("RefreshTasklistToList")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new RefreshTasklistToList())
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
        
		registerListener("doRefresh")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new Refresh())
				.append(fillForm)
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doRefreshJobs")
				.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
				.append(new RefreshJobs())
				.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class FillForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			DSContext ctx = null;
			try {
				ctx = DSApi.context();
				WorkflowFactory wf = WorkflowFactory.getInstance();

				jbpm = WorkflowFactory.jbpm;

//                log.info("before x");
//                Jbpm4Provider.getInstance().x();
//                log.info("after x");

				setManual(null);
				setCc(new LinkedList<WorkflowActivity>());
				
                if (docId != null) {
                    if(jbpm) {
                        processJbpm4Workflow(wf);
                    } else {
                        processInternalWorkflow(wf);
                    }
                } else {
                    showTasks = false;
                }

                log.info("show tasks {}, allowRefresh {}", showTasks, allowRefresh);
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}

	
			
        private void processJbpm4Workflow(WorkflowFactory wf) throws EdmException, DocumentLockedException {
            Document doc;
            doc = Document.find(docId);
            showTasks = true;

            processRenderBeans = new LinkedList<RenderBean>();
            log.trace("Initializing process definitions");
            for (ProcessDefinition pd : doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions()) {
                log.trace("Process definition:" + pd.getName());
                processRenderBeans.addAll(pd.getView().render(pd.getLocator().lookupForDocument(doc),
                        ProcessActionContext.PROCESS_ADMIN_VIEW));
            }
        }

        private void processInternalWorkflow(WorkflowFactory wf) throws EdmException {
            Document doc;

            try {
                doc = Document.find(docId);
            } catch (Exception e) {
                throw new EdmException("Dokument nie znaleziony");
            }

            String[] actIds = WorkflowFactory.findAllDocumentTasks(docId);

            for (String actId : actIds) {
                WorkflowActivity activity = wf.getWfActivity(actId);
                if (wf.isManual(activity)) {
                    setManual(activity);
                    if (activity.getCurrentUser() != null) {
                        userFullName = UserFactory.getInstance()
                                .findByUsername(activity.getCurrentUser())
                                .asFirstnameLastnameName();
                    } else {
                        userFullName = "";
                    }
                    divFullName = DSDivision.safeGetName(activity
                            .getCurrentGuid());
                    if (!jbpm) {
                        setParticipantId(activity.getParticipantId());
                    } else {
                        setParticipantId(null);
                    }
                } else {
                    getCc().add(activity);
                }
            }

            showTasks = getManual() != null || getCc().size() > 0;

            processRenderBeans = new LinkedList<RenderBean>();
            log.trace("Initializing process definitions");
            for (ProcessDefinition pd : doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcessDefinitions()) {
                log.trace("Process definition:" + pd.getName());
                getProcessRenderBeans().addAll(pd.getView().render(pd.getLocator().lookupForDocument(doc), ProcessActionContext.PROCESS_ADMIN_VIEW));
            }

            allowRefresh = doc.getDocumentKind() != null
                    && doc.getDocumentKind().logic().getAcceptanceManager() != null;
        }
    }

	private class RefreshJobs  implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			log.warn("Start Refresh Jobs");
			ManagementService managementService = Jbpm4Provider.getInstance().getManagementService();
			List<org.jbpm.api.ProcessInstance> list = Jbpm4Provider.getInstance().getExecutionService().createProcessInstanceQuery().list();
			log.warn("Start Refresh Jobs procesInstanceList size: {}", list.size());
			for (org.jbpm.api.ProcessInstance processInstance : list) {
			  List<Job> jobs = managementService.createJobQuery().processInstanceId(processInstance.getId()).list();
			  for (Job job : jobs)
			  {
				  if (job instanceof TimerImpl && job.getException() != null && job.getException().contains("NullPointer") && ((TimerImpl) job).getRepeat() == null)
				  {
					  TimerImpl timer = (TimerImpl)job;
					  try
					  {
						  log.warn("Execution Job, ID: {}, Exception: {} ", job.getId(), job.getException());
						  managementService.executeJob(job.getId());
						  log.warn("Executed Job, ID: {}", job.getId());
					  }
					  catch (Exception e)
					  {
						  log.error("Execited Job, ID: {}, Exception: {}", job.getId(), e.getMessage());
					  }
				  }
			  }
			}

		}
	}
    private class RefreshTasklist extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            TaskSnapshot.updateByDocument(Document.find(docId));
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }
    
    private class RefreshTasklistToList extends TransactionalActionListener {
        @Override
        
        public void transaction(ActionEvent event, Logger log) throws Exception {
        	String[] ids=docIdsToRefresh.split(",");
        	int refreshed=0;
        	for(int i=0;i<ids.length;i++){
        		try{
        		ids[i]=ids[i].replaceAll("\\s+","").trim();
        		TaskSnapshot.updateByDocument(Document.find(Long.parseLong(ids[i])));
        		refreshed++;
        		}catch(Exception e){
        			log.error("blad odswiezania dla dokumentu o id= "+ids[i]);continue;
        		}
        	}
        	log.error("odswiezono dokumentow: "+refreshed+" z zadanych:"+ids.length);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    private class CloseActiviti implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();

            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",docId);
            List<Execution> execList = query.list();
            for(Execution e : execList) {
                runtimeService.deleteProcessInstance(e.getProcessInstanceId(), "Awaryjne zakonczenie procesu");
            }

            PreparedStatement ps = null;
            try {
                ps = DSApi.context().prepareStatement("delete from ds_process_for_document where document_id = ?");
                ps.setLong(1, docId);
                ps.execute();
            } catch (Exception e) {
                LOG.error(e.getMessage(),e);
            } finally {
                DbUtils.closeQuietly(ps);
            }
        }
    }

    private class Close implements ActionListener {

        private boolean multiClose;

        public Close(boolean multiClose) {
            this.multiClose = multiClose;
        }

        public Close() {
        }

        public void actionPerformed(ActionEvent event) {
            DSContext ctx = null;
            try {
                ctx = DSApi.context();
                ctx.begin();
                WorkflowFactory wf = WorkflowFactory.getInstance();

                Set<Long> docIds = Sets.newLinkedHashSet();
                if (multiClose) {
                    if (getDocIds() != null) {
                        for (String id : getDocIds().split(",") ){
                            try {
                                docIds.add(Long.valueOf(id));
                            } catch (NumberFormatException e) {
                                throw new IllegalArgumentException("nie mo�na sparsowac: "+id);
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("nie podano listy id dokumentow do zamkniecia");
                    }
                } else {
                    docIds.add(getDocId());
                }

                if(wf instanceof InternalWorkflowFactory){
                    for (Long docId : docIds) {
                        String activityId = wf.findManualTask(docId);
                        wf.manualFinish(activityId, false);
                    }
                } else if(wf instanceof Jbpm4WorkflowFactory) {
                    for (Long docId : docIds) {
                        String firstActivityId = Jbpm4ProcessLocator.taskIds(docId).iterator().next();
                        wf.manualFinish(firstActivityId, false);
                    }
                } else {
                    throw new IllegalStateException("nieznany WorkflowFactory");
                }

                ctx.commit();
                //DSApi.close();
            } catch (Exception e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
                if( ctx.isTransactionOpen()) ctx._rollback();
            }
        }
    }

	private class Refresh implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			DSContext ctx = null;
			try {
				ctx = DSApi.context();
				ctx.begin();
				
				Document doc = Document.find(docId);
				doc.getDocumentKind().logic().getAcceptanceManager().refresh(doc,doc.getFieldsManager(), null);
				
				ctx.commit();
			} catch (Exception e) {
				ctx.setRollbackOnly();
				addActionError(e.getMessage());
				log.error(e.getLocalizedMessage(), e);
			}
		}
	}

	private class ProcessAction extends LoggedActionListener {
		// TODO uog�lni� na wszystkie akcje
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			if (log.isTraceEnabled()) {
				log.trace("processId = " + getProcessId());
				log.trace("processName = " + getProcessName());
				log.trace("processAction = " + getProcessAction());
			}
			OfficeDocument doc = OfficeDocument.find(docId);
			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().getProcess(getProcessName());
			ProcessInstance pi = pdef.getLocator().lookupForId(processId);
			DSApi.context().begin();
			pdef.getLogic().process(
					pi,
					ProcessActionContext.action(processAction).document(doc));
			DSApi.context().commit();
			addActionMessage("Akcja " + getProcessAction() + " zosta�a wykonana" );
		}

		@Override
		public Logger getLogger() {
			return log;
		}
	 }

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	public Long getDocId() {
		return docId;
	}

	public void setJbpm(boolean jbpm) {
		this.jbpm = jbpm;
	}

	public boolean isJbpm() {
		return jbpm;
	}

	public void setShowTasks(boolean showTasks) {
		this.showTasks = showTasks;
	}

	public boolean isShowTasks() {
		return showTasks;
	}

	public void setManual(WorkflowActivity manual) {
		this.manual = manual;
	}

	public WorkflowActivity getManual() {
		return manual;
	}

	public void setCc(List<WorkflowActivity> cc) {
		this.cc = cc;
	}

	public List<WorkflowActivity> getCc() {
		return cc;
	}

	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

	public String getParticipantId() {
		return participantId;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setDivFullName(String divFullName) {
		this.divFullName = divFullName;
	}

	public String getDivFullName() {
		return divFullName;
	}

	public void setAllowRefresh(boolean allowRefresh) {
		this.allowRefresh = allowRefresh;
	}

	public boolean isAllowRefresh() {
		return allowRefresh;
	}

	public List<RenderBean> getProcessRenderBeans() {
		return processRenderBeans;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessAction() {
		return processAction;
	}

	public void setProcessAction(String processAction) {
		this.processAction = processAction;
	}

    public String getDocIds() {
        return docIds;
    }

    public void setDocIds(String docIds) {
        this.docIds = docIds;
    }

	public String getDocIdsToRefresh() {
		return docIdsToRefresh;
	}

	public void setDocIdsToRefresh(String docIdsToRefresh) {
		this.docIdsToRefresh = docIdsToRefresh;
	}
}
