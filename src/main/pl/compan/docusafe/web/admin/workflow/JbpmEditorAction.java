package pl.compan.docusafe.web.admin.workflow;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.ajax.AjaxJsonResult;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.organization.OrganizationTreeJBPMEditorXMLGenerator;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import static pl.compan.docusafe.webwork.event.SessionControl.*;


public class JbpmEditorAction extends EventActionSupport {
	private String name;
	private final Logger log = LoggerFactory.getLogger(JbpmEditorAction.class);
	private String editorGUIConfig;
	
	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(fillForm)
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doGetOrganizationGraph")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(fillForm)
			.append(new GetOrganizationGraph())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doGetGUIConfigurationFile")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(fillForm)
			.append(new GetGUIConfigurationFile())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
		registerListener("doGetOrganiztionLdif")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			//.append(fillForm)
			.append(new GetOrganizationLdif())
			.append(CLOSE_HIBERNATE_AND_JBPM_SESSION);
		
	}

	private class FillForm implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			setEditorGUIConfig(Docusafe.getHome().getAbsolutePath()+File.separator+"jbpmeditorgui.properties");
		}
	}
	
	private class GetOrganizationLdif implements ActionListener
	{
		private class OrganizationLdif extends AjaxJsonResult 
		{
			private String ldifContent;
			private String organizationName;
			
			public String getOrganizationName() {
				return organizationName;
			}

			public void setOrganizationName(String organizationName) {
				this.organizationName = organizationName;
			}

			public String getLdifContent() {
				return ldifContent;
			}

			public void setLdifContent(String ldifContent) {
				this.ldifContent = ldifContent;
			}

			public OrganizationLdif(String ldifContent) {
				super();
				this.ldifContent = ldifContent;
			}
			
			public OrganizationLdif() {
				super();
			}
		}
			
		public void actionPerformed(ActionEvent event) 
		{
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/plain; charset=UTF-8");
			PrintWriter out = null;
			try 
			{
				 out = resp.getWriter();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
			
			OrganizationLdif org = new OrganizationLdif();
			try
			{
				DSDivision root =  DSDivision.find(DSDivision.ROOT_GUID);
				org.setOrganizationName(root.getCode());
				org.setLdifContent(root.getLdif());
			}
			catch(Exception e)
			{
				LOG.error(e.getMessage(), e);
				org.success = false;
				org.errorMessage = e.getLocalizedMessage();
			}
			out.write(org.toJson());
			out.flush();
			out.close();
		}
		
	}
	
	private class GetOrganizationGraph implements ActionListener {
		
		private class OrganizationGraphXML extends AjaxJsonResult {
			private String xmlContent;
			private String organizationName;
			
			public String getOrganizationName() {
				return organizationName;
			}

			public void setOrganizationName(String organizationName) {
				this.organizationName = organizationName;
			}

			public String getXmlContent() {
				return xmlContent;
			}

			public void setXmlContent(String xmlContent) {
				this.xmlContent = xmlContent;
			}

			public OrganizationGraphXML(String xmlContent) {
				super();
				this.xmlContent = xmlContent;
			}
			
			public OrganizationGraphXML() {
				super();
			}
			
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/xml; charset=UTF-8");
			PrintWriter out = null;
			try {
				 out = resp.getWriter();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
			
			OrganizationTreeJBPMEditorXMLGenerator generator = new OrganizationTreeJBPMEditorXMLGenerator();
			String xmlContent = null;
			OrganizationGraphXML organizationGraphXML = new OrganizationGraphXML();
			try{
				DSDivision root =  DSDivision.find(DSDivision.ROOT_GUID);
				xmlContent = generator.generateXML(root);
				organizationGraphXML.setOrganizationName(root.getCode());
				organizationGraphXML.setXmlContent(xmlContent);
			}catch(IOException e){
				LOG.error(e.getMessage(), e);
				organizationGraphXML.success = false;
				organizationGraphXML.errorMessage = e.getLocalizedMessage();
			}catch(EdmException e){
				LOG.error(e.getMessage(), e);
				organizationGraphXML.success = false;
				organizationGraphXML.errorMessage = e.getLocalizedMessage();
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
				organizationGraphXML.success = false;
				organizationGraphXML.errorMessage = e.getLocalizedMessage();
			}
			out.write(organizationGraphXML.toJson());
			out.flush();
			out.close();
		}
	}

	private class GetGUIConfigurationFile implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent event) {
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/plain; charset=UTF-8");
			
			BufferedReader reader = null;
			PrintWriter out = null;
			try {
				 out = resp.getWriter();
				 reader = new BufferedReader(new FileReader(getEditorGUIConfig()));
				 String line;
				 String configurationFileContent = "";
				 while((line = reader.readLine()) != null){
					 configurationFileContent += line + "\n";
				 }
				out.write(configurationFileContent);
				out.flush();
				out.close();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEditorGUIConfig() {
		return editorGUIConfig;
	}

	public void setEditorGUIConfig(String editorGUIConfig) {
		this.editorGUIConfig = editorGUIConfig;
	}

}
