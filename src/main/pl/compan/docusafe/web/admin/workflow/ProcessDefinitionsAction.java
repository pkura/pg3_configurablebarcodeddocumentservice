package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.*;
import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.taskmgmt.exe.TaskInstance;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.office.workflow.jbpm.Constants;
import pl.compan.docusafe.core.office.workflow.jbpm.JbpmManager;
import pl.compan.docusafe.core.EdmException;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class ProcessDefinitionsAction extends EventActionSupport
{


    public List<ProcessBean> definitions = new ArrayList<ProcessBean>();
    public List tabs;


    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    public String getBaseLink()
    {
        return "/admin/workflow/process-definitions.action";
    }

    public List getTabs() {
        return tabs;
    }

    public void setTabs(List tabs) {
        this.tabs = tabs;
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {

                setTabs(new Tabs(ProcessDefinitionsAction.this).createTabs());

                Map<Long,String> jbpmProcesses = JbpmManager.prepareJbpmProcesses();

                JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
                JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
                try
                {
                    for (Long definitionId : jbpmProcesses.keySet())
                    {
                        List<ProcessDefinition> temp;
                        GraphSession graphSession = jbpmContext.getGraphSession();
                        ProcessDefinition definition = graphSession.getProcessDefinition(definitionId);
                        ProcessBean bean = new ProcessBean();
                        if(definition!=null) bean.setName(definition.getName());
                        bean.setNameForUser(jbpmProcesses.get(definitionId));
                        if(definition!=null)bean.setVersion(definition.getVersion());
                        if(definition!=null)bean.setId(definition.getId());
                        bean.setUrl("/admin/workflow/process-definition-inspect.action");
                        if(definition!=null)bean.setInstancesCount(graphSession.findProcessInstances(definition.getId()).size());
                        definitions.add(bean);
                    }
                }
                finally
                {
                    jbpmContext.close();
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }

    }
    private class ProcessBean
    {
        private String name;
        private String nameForUser;
        private int version;
        private String url;
        private int instancesCount;
        private long id;

        public long getId()
        {
            return id;
        }
        public void setId(long id)
        {
            this.id = id;
        }
        public int getInstancesCount()
        {
            return instancesCount;
        }
        public void setInstancesCount(int instancesCount)
        {
            this.instancesCount = instancesCount;
        }
        public String getName()
        {
            return name;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public String getNameForUser()
        {
            return nameForUser;
        }
        public void setNameForUser(String nameForUser)
        {
            this.nameForUser = nameForUser;
        }
        public int getVersion()
        {
            return version;
        }
        public void setVersion(int version)
        {
            this.version = version;
        }
        public String getUrl()
        {
            return url;
        }
        public void setUrl(String url)
        {
             String[] params = new String[] {
                        "processDefId", String.valueOf(id)};
             this.url = HttpUtils.makeUrl(url, params);
        }
    }

}
