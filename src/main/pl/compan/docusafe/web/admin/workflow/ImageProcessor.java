package pl.compan.docusafe.web.admin.workflow;
/*
 * JBoss, Home of Professional Open Source
 * Copyright 2005, JBoss Inc., and individual contributors as indicated
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.xpath.DefaultXPath;
import org.jbpm.JbpmContext;
import org.jbpm.file.def.FileDefinition;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.Token;
import org.jbpm.taskmgmt.exe.TaskInstance;

public class ImageProcessor
{

  private static final long serialVersionUID = 1L;
  private long taskInstanceId = -1;
  private long tokenInstanceId = -1;
  
  private byte[] gpdBytes = null;
  private byte[] imageBytes = null;
  private Token currentToken = null;
  private ProcessDefinition processDefinition = null;
  
  static String currentTokenColor = "red";
  static String childTokenColor = "blue";
  static String tokenNameColor = "blue";
  

  public void release() {
    taskInstanceId = -1;
    gpdBytes = null;
    imageBytes = null;
    currentToken = null;
  }

  public void doEndTag()
  {
    try {
      initialize();
      retrieveByteArrays();
      if (gpdBytes != null && imageBytes != null) {
        writeTable();
      }
    } catch (IOException e) {
    	LogFactory.getLog("eprint").debug("", e);
    } catch (DocumentException e) {
    	LogFactory.getLog("eprint").debug("", e);
    }
    release();

  }

  private void retrieveByteArrays() {
    try {
      FileDefinition fileDefinition = processDefinition.getFileDefinition();
      gpdBytes = fileDefinition.getBytes("gpd.xml");
      imageBytes = fileDefinition.getBytes("processimage.jpg");
    } catch (Exception e) {
    	LogFactory.getLog("eprint").debug("", e);
    }
  }

  private String writeTable() throws IOException, DocumentException {

    int borderWidth = 4;
    Element rootDiagramElement = DocumentHelper.parseText(new String(gpdBytes)).getRootElement();
    int[] boxConstraint;
    int[] imageDimension = extractImageDimension(rootDiagramElement);
    String imageLink = "processimage?definitionId=" + processDefinition.getId();
    StringBuilder view = new StringBuilder();
    if (tokenInstanceId > 0) {

        List allTokens = new ArrayList();
        walkTokens(currentToken, allTokens);
        
    	view.append("<div style='position:relative; background-image:url(" + imageLink + "); width: " + imageDimension[0] + "px; height: " + imageDimension[1] + "px;'>");

        for (int i = 0; i < allTokens.size(); i++)
        {
            Token token = (Token) allTokens.get(i);
            boxConstraint = extractBoxConstraint(rootDiagramElement, token);

            //Adjust for borders
            boxConstraint[2]-=borderWidth*2;
            boxConstraint[3]-=borderWidth*2;

        	view.append("<div style='position:absolute; left: "+ boxConstraint[0] +"px; top: "+ boxConstraint[1] +"px; ");

            if (i == (allTokens.size() - 1)) {
            	view.append("border: " + currentTokenColor);
            }
            else {            	
    			view.append("border: " + childTokenColor);
            }
            
            view.append(" " + borderWidth + "px groove; "+
            			"width: "+ boxConstraint[2] +"px; height: "+ boxConstraint[3] +"px;'>");
			
            if(token.getName()!=null)
            {
                 view.append("<span style='color:" + tokenNameColor + ";font-style:italic;position:absolute;left:"+ (boxConstraint[2] + 15) +"px;'>" + token.getName() +"</span>");
            }

            view.append("</div>");
        }
        view.append("</div>"); 
        
    }
    else
    {
    	boxConstraint = extractBoxConstraint(rootDiagramElement);
    	
	    view.append("<table border=0 cellspacing=0 cellpadding=0 width=" + imageDimension[0] + " height=" + imageDimension[1] + ">");
	    view.append("  <tr>");
	    view.append("    <td width=" + imageDimension[0] + " height=" + imageDimension[1] + " style=\"background-image:url(" + imageLink + ")\" valign=top>");
	    view.append("      <table border=0 cellspacing=0 cellpadding=0>");
	    view.append("        <tr>");
	    view.append("          <td width=" + (boxConstraint[0] - borderWidth) + " height=" + (boxConstraint[1] - borderWidth)
	            + " style=\"background-color:transparent;\"></td>");
	    view.append("        </tr>");
	    view.append("        <tr>");
	    view.append("          <td style=\"background-color:transparent;\"></td>");
	    view.append("          <td style=\"border-color:" + currentTokenColor + "; border-width:" + borderWidth + "px; border-style:groove; background-color:transparent;\" width="
	            + boxConstraint[2] + " height=" + (boxConstraint[3] + (2 * borderWidth)) + ">&nbsp;</td>");
	    view.append("        </tr>");
	    view.append("      </table>");
	    view.append("    </td>");
	    view.append("  </tr>");
	    view.append("</table>");
    }
    return view.toString();
  }

  private int[] extractBoxConstraint(Element root) {
    int[] result = new int[4];
    String nodeName = currentToken.getNode().getName();
    XPath xPath = new DefaultXPath("//node[@name='" + nodeName + "']");
    Element node = (Element) xPath.selectSingleNode(root);
    result[0] = Integer.valueOf(node.attribute("x").getValue()).intValue();
    result[1] = Integer.valueOf(node.attribute("y").getValue()).intValue();
    result[2] = Integer.valueOf(node.attribute("width").getValue()).intValue();
    result[3] = Integer.valueOf(node.attribute("height").getValue()).intValue();
    return result;
  }

  private int[] extractBoxConstraint(Element root, Token token) {
	    int[] result = new int[4];
	    String nodeName = token.getNode().getName();
	    XPath xPath = new DefaultXPath("//node[@name='" + nodeName + "']");
	    Element node = (Element) xPath.selectSingleNode(root);
	    result[0] = Integer.valueOf(node.attribute("x").getValue()).intValue();
	    result[1] = Integer.valueOf(node.attribute("y").getValue()).intValue();
	    result[2] = Integer.valueOf(node.attribute("width").getValue()).intValue();
	    result[3] = Integer.valueOf(node.attribute("height").getValue()).intValue();
	    return result;
	  }
  
  private int[] extractImageDimension(Element root) {
    int[] result = new int[2];
    result[0] = Integer.valueOf(root.attribute("width").getValue()).intValue();
    result[1] = Integer.valueOf(root.attribute("height").getValue()).intValue();
    return result;
  }

  private void initialize() {
    JbpmContext jbpmContext = JbpmContext.getCurrentJbpmContext(); 
    if (this.taskInstanceId > 0) {
    	TaskInstance taskInstance = jbpmContext.getTaskMgmtSession().loadTaskInstance(taskInstanceId);
    	currentToken = taskInstance.getToken();
    }
    else
    {
    	if (this.tokenInstanceId > 0) 
    		currentToken = jbpmContext.getGraphSession().loadToken(this.tokenInstanceId);
    }
    processDefinition = currentToken.getProcessInstance().getProcessDefinition();
  }

  private void walkTokens(Token parent, List allTokens)
  {
      Map children = parent.getChildren();
      if(children != null && children.size() > 0)
      {
          Collection childTokens = children.values();
          for (Iterator iterator = childTokens.iterator(); iterator.hasNext();)
          {
              Token child = (Token) iterator.next();
              walkTokens(child,  allTokens);
          }
      }

      allTokens.add(parent);
  }

  public void setTask(long id) {
    this.taskInstanceId = id;
  }

  public void setToken(long id) {
	this.tokenInstanceId = id;  
  }
  
}
