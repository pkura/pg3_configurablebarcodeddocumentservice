package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.*;


import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.taskmgmt.exe.TaskInstance;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.office.workflow.jbpm.Constants;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

//import java.util.List;
//import java.util.ArrayList;
import java.util.zip.ZipInputStream;
import java.net.URL;
import java.net.MalformedURLException;
//import java.io.IOException;
import java.io.*;
import java.util.*;

public class ProcessDeployAction extends EventActionSupport
{

    //private static final String EV_ADD = "add";
	public List tabs;
	private File file;
	private String fileName;
    private String nameForUser;
	
	protected void setup()
    {

		FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDeploy").
        	append(OpenHibernateSession.INSTANCE).
        	append(fillForm).
        	append(new Upload("file")).
        	append(new Deploy()).
        	appendFinally(CloseHibernateSession.INSTANCE);

    }

    public String getBaseLink()
    {
        return "/admin/workflow/process-deploy.action";
    }
    
    private class FillForm implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
        {
    		setTabs(new Tabs(ProcessDeployAction.this).createTabs());
        
    		
        }
            

    }
    
    private class Deploy implements ActionListener
    {
    	
    	    	   	
    	public void actionPerformed(ActionEvent event)
    	{
    		Long definitionId = null;
            String definitionName = null;
            Integer definitionVersion = null;
            JbpmConfiguration jbpmConfiguration = Docusafe.getJbpmConfiguration();
    	    try
            {
                URL archiveUrl = file.toURL();//file.toURL();
                ZipInputStream zis = new ZipInputStream(archiveUrl.openStream());
                JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();
                ProcessDefinition processDefinition = ProcessDefinition.parseParZipInputStream(zis);
                try
                {
                    jbpmContext.deployProcessDefinition(processDefinition);
                    zis.close();
                    definitionId = processDefinition.getId();
                    definitionName = processDefinition.getName();
                    definitionVersion = processDefinition.getVersion();
                }
                finally
                {
                    jbpmContext.close();
                }

                DSApi.context().begin();

                pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition definition = new pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition();

                definition.setDefinitionName(definitionName);
                definition.setDefinitionId(definitionId);
                definition.setDefinitionVersion(definitionVersion);
                definition.setNameForUser(nameForUser);

                definition.create();

                DSApi.context().commit();

                event.addActionMessage("Pomy�lnie dodano definicj� procesu");
                event.setResult("deployed");
            }
    	    catch(MalformedURLException e){
    	    	addActionError("Podany plik nie istnieje");
    	    }
    	    catch(IOException e){
    	    	addActionError("Nie mo�na otworzy� pliku");
    	    }
    	    /*catch(NullPointerException e){

    	    }*/
            catch (EdmException e)
            {
                addActionError("B��d podczas zapisywania w bazie informacji o utworzonej definicji procesu");
            }
        }
    	
    }
    private class Upload implements ActionListener
    {
    	private String fieldName;

        public Upload(String fieldName)
        {
             this.fieldName = fieldName;
        }
         
    	public void actionPerformed(ActionEvent event)
        {
            if (!(ServletActionContext.getRequest() instanceof MultiPartRequestWrapper))
            {
                addActionError("Nie otrzymano pliku.");
                //event.skip(EV_ADD);
                return;
            }

            MultiPartRequestWrapper multiWrapper = (MultiPartRequestWrapper)
                ServletActionContext.getRequest();

            if (multiWrapper.hasErrors())
            {
                for (Iterator iter=multiWrapper.getErrors().iterator(); iter.hasNext(); )
                {
                    addActionError((String) iter.next());
                }
                //event.skip(EV_ADD);
                return;
            }

            Enumeration e = multiWrapper.getFileNames();

            while (e.hasMoreElements())
            {
                // get the value of this input tag
                String inputValue = (String) e.nextElement();

                if (!fieldName.equals(inputValue))
                    continue;

                // get the content type
                //contentType = multiWrapper.getContentType(inputValue);

                // get the name of the file from the input tag
                fileName = multiWrapper.getFilesystemName(inputValue);

                // Get a File object for the uploaded File
                file = multiWrapper.getFile(inputValue);

                break;
            }

            // If it's null the upload failed
            if (file == null)
            {
                addActionError("Nie wybrano pliku lub plik ma zerow� d�ugo��");
                //event.skip(EV_ADD);
            }
            else if (file.length() == 0)
            {
                addActionError("Wybrany plik ma zerow� d�ugo��");
                //event.skip(EV_ADD);
            }
        }
    	
    
    }
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

    public String getNameForUser()
    {
        return nameForUser;
    }

    public void setNameForUser(String nameForUser)
    {
        this.nameForUser = nameForUser;
    }

    public List getTabs() {
		return tabs;
	}

	public void setTabs(List tabs) {
		this.tabs = tabs;
	}
	
}
