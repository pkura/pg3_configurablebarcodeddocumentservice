package pl.compan.docusafe.web.admin.workflow;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.jbpm.api.ProcessDefinition;
import org.jbpm.api.ProcessDefinitionQuery;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.workflow.JBPMUtils;
import pl.compan.docusafe.core.office.workflow.jbpm.JBPMDefBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.editor.BPMN2JPDLUtils;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.opensymphony.webwork.ServletActionContext;

public class ProcessDefinitionAction extends EventActionSupport {

	private Logger log = LoggerFactory.getLogger(ProcessDefinitionAction.class);
	private JBPMUtils jutil = new JBPMUtils();

    /** Stare definicje dla jbpm3 */
	private List<JBPMDefBean> jbpm3definitions;
    private List<JBPMDefBean> jbpm4definitions;
    private List<JBPMDefBean> allDefinitions;

	private FormFile newProcessDef;	
	/** zrodlo nowej definicji, albo z pliku albo predefiniowana (z wara)*/
	private String source;
	/** mapa definicji z wara */
	private Map predefinedList;
	private String predefinedToLoad;
	private String name;
    private boolean jbpm3 = false;
    private boolean validate;

    
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		registerListener("doLoad")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Load())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doGetSchema").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetSchema()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doGetBpmn").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetBpmn()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doLoadBpmn").
	        append(OpenHibernateSession.INSTANCE).
	        append(new LoadBpmn()).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class BpmnProc {
		String id;
		String name;
		String xml;
	}
	
	private class GetBpmn implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/xml; charset=UTF-8");
			PrintWriter out = null;
			try {
				 out = resp.getWriter();
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
			
			List<ProcessDefinition> pdefs = Jbpm4Provider.getInstance()
                .getRepositoryService()
                .createProcessDefinitionQuery()
                .orderDesc(ProcessDefinitionQuery.PROPERTY_NAME)
                .orderDesc(ProcessDefinitionQuery.PROPERTY_VERSION)
                .list();
			
			BpmnProc bpmnProc = null;
			if(!Strings.isNullOrEmpty(name)) {
				for(ProcessDefinition pdef : pdefs) {
					if(pdef.getName().equalsIgnoreCase(name)) {
						bpmnProc = new BpmnProc();
						bpmnProc.id = pdef.getId();
						bpmnProc.name = pdef.getName();
					}
				}
			}
			
			if(bpmnProc == null) {
				log.error("Nie znaleziono procesu {}", name);
				return;
			}

			Document bpmnDocument;
			try {
				bpmnDocument = Jbpm4Utils.getProcessResourceAsDocument(bpmnProc.name, Jbpm4Utils.JPDL);
				bpmnProc.xml = bpmnDocument.asXML();
			} catch (DocumentException e) {
				log.error("ERROR: b��d w trakcie tworzenia dokumentu dla procesu " + bpmnProc.name + ": " + e.getMessage());
			}
			
			out.write(bpmnProc.xml);
			out.flush();
			out.close();
		}
	}
	
	private class LoadBpmn implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/html; charset=UTF-8");
			PrintWriter out = null;
			String loadingResult = "failure";
			try {
				out = resp.getWriter();
				loadingResult = deployProcessToJBPMRepository(newProcessDef.getFile());
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}finally{
				out.write(new Gson().toJson(loadingResult));
				out.flush();
				out.close();
			}
		}
		
		public String deployProcessToJBPMRepository(File bpmnFile){		
			SAXReader saxReader = new SAXReader();
			Document bpmnDocument;
			File zipArchive = null;
			try {
				bpmnDocument = saxReader.read(new BufferedReader(new FileReader(bpmnFile)));
				String archiveName = bpmnFile.getName().substring(0, bpmnFile.getName().lastIndexOf("."));
				zipArchive = Jbpm4Utils.createJBPMZipArchive(archiveName, bpmnDocument);
		        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipArchive));
//		        TODO aktualnie wdro�enie nie przechodzi z powodu b��du walidacji struktury pliku jpdl patrz -> TODO w public static Map<String,Document> bpmn2jpdl(Document source)
				String deployId = Jbpm4Provider
	                .getInstance()
	                .getRepositoryService()
	                .createDeployment()
	                .addResourcesFromZipInputStream(zis)
	                .deploy();
				IOUtils.closeQuietly(zis);
				log.info("Wdro�enie udane, id wdro�enia : " + deployId);
				return "success";
			} catch (DocumentException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally{
				if(zipArchive != null && zipArchive.exists())
					zipArchive.delete();
			}
			return "failure";
		}
	}
	
    private class GetSchema implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
        	{
            	File file  = new File(Docusafe.getHome() + "/templates/" + name +".pdf");
            	ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/pdf", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"" + name + ".pdf\"");
        	}        	
        	catch (Exception e) {
				log.debug(e.getStackTrace());
			}
        	
        	
        }
    }

    private class FillForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			try {
                log.info("process definition action");
				setOldDefinitions(jutil.getDefBeans());
				setPredefinedList(jutil.getDefinitionsFromWar());

                getJbpm4List();

                log.info("jbpm4definitions.size() == " + jbpm4definitions.size());

                allDefinitions = new ArrayList<JBPMDefBean>(jbpm3definitions);
                allDefinitions.addAll(jbpm4definitions);
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(),e);
			}
		}

        private void getJbpm4List() {
            List<ProcessDefinition> pdefs =
                Jbpm4Provider
                    .getInstance()
                    .getRepositoryService()
                    .createProcessDefinitionQuery()
                    .orderDesc(ProcessDefinitionQuery.PROPERTY_NAME)
                    .orderDesc(ProcessDefinitionQuery.PROPERTY_VERSION)
                    .list();

            jbpm4definitions = new ArrayList<JBPMDefBean>(pdefs.size());

            JBPMDefBean prev = null;
            for(ProcessDefinition pd: pdefs){
                JBPMDefBean bean = new JBPMDefBean();
                bean.setName(pd.getName());
                bean.setId(pd.getId());
                bean.setProcessNum(
                    Jbpm4Provider
                        .getInstance()
                        .getExecutionService()
                        .createProcessInstanceQuery()
                        .processDefinitionId(pd.getId())
                        .count());
                bean.setActiveProcessNum(bean.getProcessNum());
                bean.setVersion(pd.getVersion());

                bean.setResources(new LinkedHashSet<String>( //hashset -> by zainicjalizowa� proxy
                    Jbpm4Provider
                        .getInstance()
                        .getRepositoryService()
                        .getResourceNames(
                            pd.getDeploymentId())));

                if(prev != null && prev.getName().equals(bean.getName())){
                    prev.getVersions().add(bean);
                } else {
                    jbpm4definitions.add(bean);
                    prev = bean;
                }
            }
        }
    }

    private class Load extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {

            if(jbpm3){
                log.info("zmiana proces�w w jbpm3");
                new LoadJbpm3().actionPerformed(event);
            } else {

                log.info("zmiana proces�w w jbpm4");
                File deploymentFile = newProcessDef.getFile();

                String deployId;
                if(deploymentFile.getName().endsWith(".zip")){

                    List<String> validationErrors = null;
                    if(validate && !(validationErrors=Jbpm4Utils.validateZip(deploymentFile)).isEmpty())
                    {
                        for(String error : validationErrors)
                            addActionError(error);
                        return;
                    }

                    ZipInputStream is = null;
                    try {
                        is = new ZipInputStream(new FileInputStream(deploymentFile));
                        deployId = Jbpm4Provider
                                    .getInstance()
                                    .getRepositoryService()
                                    .createDeployment()
                                    .addResourcesFromZipInputStream(is)
                                    .deploy();
                    } finally {
                    	try{
                        	IOUtils.closeQuietly(is);
                    	}catch(Exception e){
                    		System.out.println(e.getMessage());
                    	}
                    }
                } else {
                    deployId = Jbpm4Provider
                                    .getInstance()
                                    .getRepositoryService()
                                    .createDeployment()
                                    .addResourceFromFile(deploymentFile)
                                    .deploy();
                }
                addActionMessage("Wdro�enie udane, id wdro�enia : " + deployId);
            }
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }

    /**
     * Akcja dla jbpm3 - obecnie wycofywany
     */
	private class LoadJbpm3 implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			boolean ok = false;
			if ("fromFile".equals(source)) {
				if (newProcessDef != null) {
					 if (newProcessDef.getContentType().equals("application/zip") || newProcessDef.getContentType().equals("text/xml")) {
						 ok = true;
					 }
					 else {
						 if (newProcessDef.getContentType().equals("application/octet-stream")) {
							 if (newProcessDef.getName().endsWith(".xml")) {
								 ok = true;
							 }
						 }
					 }
				} else {
					ok = false;
				}
				if (ok) {
					try {
						jutil.loadProcessDefinition(newProcessDef.getFile(), newProcessDef.getName());				
					} catch (Exception e) {
						addActionError(e.getMessage());
						log.error(e.getMessage(),e);
					}
				} else {
					addActionError("Prosz� poda� definicj� procesu w archiwum .zip lub pliku .xml");
				}
			} else if ("predefined".equals(source)) {
				try {
					jutil.loadPredefined(predefinedToLoad);				
				} catch (Exception e) {
					addActionError(e.getMessage());
					log.error(e.getMessage(),e);
				}
			} else {
				addActionError("Prosz� wybra� �r�d�o definicji");
			}
		}
	}
	
	public void setOldDefinitions(List<JBPMDefBean> definitions) {
		this.jbpm3definitions = definitions;
	}

	public List<JBPMDefBean> getOldDefinitions() {
		return jbpm3definitions;
	}

	public void setNewProcessDef(FormFile newProcessDef) {
		this.newProcessDef = newProcessDef;
	}

	public FormFile getNewProcessDef() {
		return newProcessDef;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSource() {
		return source;
	}

	public void setPredefinedList(Map predefinedList) {
		this.predefinedList = predefinedList;
	}

	public Map getPredefinedList() {
		return predefinedList;
	}

	public void setPredefinedToLoad(String predefinedToLoad) {
		this.predefinedToLoad = predefinedToLoad;
	}

	public String getPredefinedToLoad() {
		return predefinedToLoad;
	}

    public boolean isJbpm3() {
        return jbpm3;
    }

    public void setJbpm3(boolean jbpm3) {
        this.jbpm3 = jbpm3;
    }

    public List<JBPMDefBean> getJbpm4definitions() {
        return jbpm4definitions;
    }

    public void setJbpm4definitions(List<JBPMDefBean> jbpm4definitions) {
        this.jbpm4definitions = jbpm4definitions;
    }

      public List<JBPMDefBean> getAllDefinitions() {
        return allDefinitions;
    }

    public void setAllDefinitions(List<JBPMDefBean> allDefinitions) {
        this.allDefinitions = allDefinitions;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
