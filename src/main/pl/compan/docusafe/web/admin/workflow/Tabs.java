package pl.compan.docusafe.web.admin.workflow;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.BaseTabsAction;
import pl.compan.docusafe.web.office.in.SummaryAction;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.List;

public class Tabs {
	
	private static final Log log = LogFactory.getLog(Tabs.class);
	private EventActionSupport current;
	
	public Tabs(EventActionSupport s)
	{
		current = s;
	}
    
    public List createTabs(){
    	
    	List<Tab> tabs = new ArrayList<Tab>(8);
    	
    	String[] params = new String[] {};
    	
    	tabs.add(new Tab("Definicje proces�w", "Definicje proces�w",
                HttpUtils.makeUrl("/admin/workflow/process-definitions.action", params),
                current instanceof ProcessDefinitionsAction || current instanceof ProcessDefinitionInspectAction
                || current instanceof ProcessInstanceInspectAction));
    	tabs.add(new Tab("Dodanie definicji procesu", "Dodanie definicji procesu",
                HttpUtils.makeUrl("/admin/workflow/process-deploy.action", params),
                current instanceof ProcessDeployAction));
        tabs.add(new Tab("Mapowanie struktury", "Mapowanie struktury",
                HttpUtils.makeUrl("/admin/workflow/swimlane-mapping.action", params),
                current instanceof SwimlaneMappingAction || current instanceof EditSwimlaneMappingAction));
        return tabs;
    }

}
