package pl.compan.docusafe.web.admin.workflow;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletResponse;

import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

public class ProcessDefinitionInspectAction extends EventActionSupport
{
    public List tabs;
    private List processInstances;
    private String name;
    private String nameForUser;
    private int version;
    private long processDefId;
    private String image;
    private boolean confirmDel = false;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doConfirm").
            append(OpenHibernateSession.INSTANCE).
            append(new Confirm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doGetImage").
            append(OpenHibernateSession.INSTANCE).
            append(new GetImage()).
            appendFinally(CloseHibernateSession.INSTANCE);

    }

    public String getBaseLink()
    {
        String[] params = new String[] {
                "processDefId", String.valueOf(processDefId)};
        return HttpUtils.makeUrl("/admin/workflow/process-definition-inspect.action", params);
    }

    public List getTabs() {
        return tabs;
    }

    public void setTabs(List tabs) {
        this.tabs = tabs;
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(new Tabs(ProcessDefinitionInspectAction.this).createTabs());
            setConfirmDel(false);
            JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
            try{
                ProcessDefinition pd = cont.getGraphSession().getProcessDefinition(processDefId);
                version = pd.getVersion();
                name = pd.getName();
                image = getBaseLink()+"&doGetImage=true";
            }
            catch(org.jbpm.JbpmException e)
            {
                addActionError("B��d podczas pobierania informacji o definicji procesu o id="+processDefId);
            }
            finally
            {
                cont.close();
            }
            processInstances = getInstances(getProcessDefId());

            try
            {
                pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition definition = pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition.findByDefinitionId(processDefId);
                if (definition != null)
                    nameForUser = definition.getNameForUser();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }

        }

    }

    public class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition definition = pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition.findByDefinitionId(processDefId);
                definition.setNameForUser(nameForUser);

                //TODO: zrobic odswiezanie lsty zadan po ProcessDefinition.id

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }

    }

    public class Confirm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(new Tabs(ProcessDefinitionInspectAction.this).createTabs());
            JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
            try
            {
                ProcessDefinition pd = cont.getGraphSession().getProcessDefinition(processDefId);
                version = pd.getVersion();
                name = pd.getName();
            }
            finally
            {
                cont.close();
            }
            processInstances = getInstances(getProcessDefId());
            setConfirmDel(true);
        }

    }

    public class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition definition = pl.compan.docusafe.core.office.workflow.jbpm.ProcessDefinition.findByDefinitionId(processDefId);

                definition.delete();

                JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
                try
                {
                    cont.getGraphSession().deleteProcessDefinition(processDefId);
                }
                finally
                {
                    cont.close();
                }

                DSApi.context().commit();

                setConfirmDel(false);
                event.addActionMessage("Pomy�lnie usuni�to definicj� procesu " + name + " wersja " + version);
                event.setResult("deleted");
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }

        }

    }

    public class GetImage implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String filename = "processimage.jpg";
            JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
            try
            {
                ProcessDefinition pd = cont.getGraphSession().getProcessDefinition(processDefId);
                InputStream in = pd.getFileDefinition().getInputStream(filename);

                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");

                try
                {
                    OutputStream output = response.getOutputStream();
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
            }
            finally
            {
                cont.close();
            }
        }
    }

    public List getInstances(long id) {

        JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        GraphSession graphSession = jbpmContext.getGraphSession();

        ArrayList processInstancesList = new ArrayList();

        List listProcessInstance = graphSession.findProcessInstances(id);

        if (listProcessInstance.isEmpty() == false) {
          ListIterator listProcessInstances = listProcessInstance.listIterator();
          while (listProcessInstances.hasNext()) {
            ProcessInstance processInstance = (ProcessInstance) listProcessInstances.next();
            processInstancesList.add(new ProcessInstanceBean(processInstance.getId(), processInstance.getStart(), processInstance.getEnd()));
          }
        }

        return processInstancesList;
    }

    public List getProcessInstances() {
        return processInstances;
    }

    public long getProcessDefId() {
        return processDefId;
    }

    public void setProcessDefId(long processDefId) {
        this.processDefId = processDefId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isConfirmDel() {
        return confirmDel;
    }

    public void setConfirmDel(boolean confirmedDel) {
        this.confirmDel = confirmedDel;
    }

    public String getNameForUser()
    {
        return nameForUser;
    }

    public void setNameForUser(String nameForUser)
    {
        this.nameForUser = nameForUser;
    }

    private class ProcessInstanceBean
    {
        private Date start;
        private Date end;
        private long id;
        private String url;

        public Date getEnd() {
            return end;
        }

        public void setEnd(Date end) {
            this.end = end;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Date getStart() {
            return start;
        }

        public void setStart(Date start) {
            this.start = start;
        }

        public ProcessInstanceBean(long id, Date start, Date end)
        {
            this.end = end;
            this.id = id;
            this.start = start;
            setUrl("/admin/workflow/process-instance-inspect.action");

        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            String[] params = new String[] {
                    "processId", String.valueOf(id)};
            this.url = HttpUtils.makeUrl(url, params);
        }

    }
}
