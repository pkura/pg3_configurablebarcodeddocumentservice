package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.office.workflow.jbpm.Constants;

import org.jbpm.JbpmContext;
import org.jbpm.db.GraphSession;
import org.jbpm.context.exe.ContextInstance;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.def.ProcessDefinition;


import java.util.List;
import java.util.ArrayList;
import java.util.*;
import java.io.*;


public class ProcessInstanceInspectAction extends EventActionSupport
{

	
	public List tabs;
	private String name;
	private int version;
	private long processId;
	private long processDefId;
	private Map variables;
	private String image;
	private boolean confirmDel = false;
	
	
    

	protected void setup()
    {

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doConfirm").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Confirm()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doDelete").
    		append(OpenHibernateSession.INSTANCE).
    		append(new Delete()).
    		appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doSave").
			append(OpenHibernateSession.INSTANCE).
			append(new Save()).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);
     
    }
    
    public String getBaseLink()
    {
        String[] params = new String[] {
	            "processId", String.valueOf(processId)};
        return HttpUtils.makeUrl("/admin/workflow/process-instance-inspect.action", params);
    }
    
    public List getTabs() {
		return tabs;
	}

	public void setTabs(List tabs) {
		this.tabs = tabs;
	}
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	setTabs(new Tabs(ProcessInstanceInspectAction.this).createTabs());
        	setConfirmDel(false);
        	JbpmContext jbpmContext = Docusafe.getJbpmConfiguration().createJbpmContext();
        	try{
        		GraphSession gs = jbpmContext.getGraphSession();
        		ProcessInstance pi = gs.getProcessInstance(processId);
        		version = pi.getProcessDefinition().getVersion();
        		name = pi.getProcessDefinition().getName();
        		variables = pi.getContextInstance().getVariables();		
        	}
        	finally
        	{
        		jbpmContext.close();
        	}
        	
        }    
    
    }
    
    public class Confirm implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		setTabs(new Tabs(ProcessInstanceInspectAction.this).createTabs());
        	JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
        	try{
        		GraphSession gs = cont.getGraphSession();
        		ProcessInstance pi = gs.getProcessInstance(processId);
        		version = pi.getProcessDefinition().getVersion();
        		name = pi.getProcessDefinition().getName();

        	}
        	finally
        	{
        		
        		cont.close();
        	}
    		setConfirmDel(true);
    	}
    	
    }
    
    public class Delete implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
        	JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
        	try{
        		GraphSession gs = cont.getGraphSession();
        		ProcessInstance pi = gs.getProcessInstance(processId);
        		processDefId = pi.getProcessDefinition().getId();
        		cont.getGraphSession().deleteProcessInstance(processId); 		
        	}
        	finally
        	{
        		cont.close();
        	}
    		setConfirmDel(false);
    		event.addActionMessage("Pomy�lnie usuni�to proces o numerze " + processId + " o definicji " + name + " wersja " + version);
    		event.setResult("deleted");
    		
    	}
    	
    }
    public class Save implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{
    		JbpmContext cont = Docusafe.getJbpmConfiguration().createJbpmContext();
        	try{
        		
        		ProcessInstance pi = cont.loadProcessInstanceForUpdate(processId);
        		ContextInstance cs = pi.getContextInstance();
        		variables = cs.getVariables();
        		Set varSet = variables.entrySet();
        		Iterator iter = varSet.iterator();
        		while(iter.hasNext())
        		{
        			Map.Entry ent = (Map.Entry)iter.next();
        			if((String)ent.getKey()==Constants.VARIABLE_DOCUMENT_ID)
        				cs.setVariable((String)ent.getKey(), (Long)ent.getValue());
        			if((String)ent.getKey()==Constants.VARIABLE_DOCUMENT_TYPE||(String)ent.getKey()==Constants.VARIABLE_NEXT_ACTOR)
        				cs.setVariable((String)ent.getKey(), (String)ent.getValue());
        			if((String)ent.getKey()==Constants.VARIABLE_TASK_ACCEPTED)
        				cs.setVariable((String)ent.getKey(), (Boolean)ent.getValue());
        		//	cs.
        		}
        		//cont.save(pi);

        	}
        	finally
        	{
        		cont.close();
        	}
    	
    	}
    }


	
	public long getProcessId() {
		return processId;
	}

	public void setProcessId(long processDefId) {
		this.processId = processDefId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isConfirmDel() {
		return confirmDel;
	}

	public void setConfirmDel(boolean confirmedDel) {
		this.confirmDel = confirmedDel;
	}

	public Map getVariables() {
		return variables;
	}

	public void setVariables(Map variables) {
		this.variables = variables;
	}

	public long getProcessDefId() {
		return processDefId;
	}

	public void setProcessDefId(long processDefId) {
		this.processDefId = processDefId;
	}



	
}
