package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

/**
 * User: Grzegorz  Filip
 * Date: 07.09.2015
 * Time: 9:31
 */
public class JasperAdminAction extends EventActionSupport {
    private final Logger LOG = LoggerFactory.getLogger(JasperAdminAction.class);

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION)
            .append(OPEN_HIBERNATE_AND_JBPM_SESSION)
            .append(fillForm)
            .appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
    }

    private class FillForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            LOG.info("ActivitiEditorAction fillform");
        }
    }

}
