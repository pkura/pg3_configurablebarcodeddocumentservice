package pl.compan.docusafe.web.admin.workflow;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.office.workflow.jbpm.SwimlaneMapping;
import pl.compan.docusafe.core.office.workflow.jbpm.Constants;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
/* User: Administrator, Date: 2007-02-14 16:25:46 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class SwimlaneMappingAction extends EventActionSupport
{
    private List tabs;
    private List<Map<String,Object>> swimlanes;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            setTabs(new Tabs(SwimlaneMappingAction.this).createTabs());

            try
            {
                List<SwimlaneMapping> mappings = SwimlaneMapping.findAll();
                swimlanes = new ArrayList<Map<String,Object>>();
                for (SwimlaneMapping swimlaneMapping : mappings)
                {
                    Map<String,Object> bean = new HashMap<String,Object>();
                    bean.put("id", swimlaneMapping.getId());
                    bean.put("name", swimlaneMapping.getName());
                    bean.put("divisionName", DSDivision.find(swimlaneMapping.getDivisionGuid()).getName());
                    bean.put("assignmentType", Constants.getAssignmentTypes().get(swimlaneMapping.getAssignmentType()));
                    swimlanes.add(bean);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public List getTabs() {
        return tabs;
    }

    public void setTabs(List tabs) {
        this.tabs = tabs;
    }

    public List<Map<String, Object>> getSwimlanes()
    {
        return swimlanes;
    }
}
