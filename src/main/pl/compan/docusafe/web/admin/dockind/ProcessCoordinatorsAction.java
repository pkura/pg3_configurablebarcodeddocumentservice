package pl.compan.docusafe.web.admin.dockind;

import java.util.*;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.dockinds.ChannelProvider;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.dockinds.ProcessChannel;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * 
 * @author Mariusz Kilja�czyk
 */
public class ProcessCoordinatorsAction extends EventActionSupport
{
    public final static Logger log = LoggerFactory.getLogger(ProcessCoordinatorsAction.class);

    private LinkedHashSet<Bean> channels = new LinkedHashSet<Bean>();
    private String documentKindCn;
    private List<DocumentKind> documentKinds;
    private Map<String,String> guids = new HashMap<String,String>();
    private Map<String,String> user = new HashMap<String,String>();
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            { 
                if (documentKindCn == null){
                    documentKindCn = DocumentKind.getDefaultKind();
                }
                documentKinds = DocumentKindProvider.get().visible().list();
                DocumentKind kind = DocumentKind.findByCn(documentKindCn);
                kind.initialize();
                getBeansFromProviders(kind);
                getBeansFromStaticChannels(kind);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                //b��d w FillFormie trzeba logowa�
                log.error(e.getMessage(), e);
            }
        }

        private void getBeansFromProviders(DocumentKind kind) throws EdmException {
            for(ChannelProvider cp: kind.getDockindInfo().getChannelProviders()){
                for(ProcessChannel pc : cp.getEditableChannels()){
                    channels.add(channelToBean(pc));
                }
            }
        }

        private void getBeansFromStaticChannels(DocumentKind kind) throws EdmException {
            List<ProcessChannel> tmpChannels = kind.getProcessChannels();
            if(tmpChannels != null)
            {
                for (ProcessChannel channel : tmpChannels)
                {
                    Bean bean = channelToBean(channel);
                    channels.add(bean);
                }
            }
        }

        private Bean channelToBean(ProcessChannel channel) throws EdmException {
            String guid = null;
            String name = null;
            String user = null;
            List<ProcessCoordinator> tmpl = ProcessCoordinator.find(documentKindCn,channel.getCn());
            if(tmpl != null && tmpl.size()>0)
            {
                guid = tmpl.get(0).getGuid();
                if(guid == null || guid.length() < 1)
                    guid = DSDivision.ROOT_GUID;
                name = DSDivision.find(guid).getPrettyPath();
                user = tmpl.get(0).getUsername();
            }
            else
            {
                guid = DSDivision.ROOT_GUID;
                name = DSDivision.find(guid).getPrettyPath();
            }
            return new Bean(channel.getCn(),name,channel.getName(),guid,user);
        }
    }
    
    public String getGuid(String cn)
    {
    	try 
    	{
			return ProcessCoordinator.find(documentKindCn, cn).get(0).getGuid();
		} catch (EdmException e) 
		{
			LogFactory.getLog("eprint").debug("", e);
		}
		return null;
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
        	DocumentKind kind;
			try 
			{
				DSApi.context().begin();
				kind = DocumentKind.findByCn(documentKindCn);
				kind.initialize();
	            List<ProcessChannel> tmpChannels = kind.getProcessChannels();
	            if(tmpChannels != null)
                {
	                for (ProcessChannel channel : tmpChannels)
	                {
	                	List<ProcessCoordinator> processCoordinators = ProcessCoordinator.find(documentKindCn, channel.getCn());
	                	if(processCoordinators != null && processCoordinators.size() > 0)
	                	{
	                		ProcessCoordinator processCoordinator = processCoordinators.get(0);

							DataMartEventBuilder.get()
								.defaultObjectId(processCoordinator.getId())
								.checkValueChangeEvent(EventType.COORDINATOR_CHANGE, "username", processCoordinator.getUsername(), user.get(channel.getCn()))
								.checkValueChangeEvent(EventType.COORDINATOR_CHANGE, "guid", processCoordinator.getGuid(), guids.get(channel.getCn()));

	                		processCoordinator.setUsername(user.get(channel.getCn()));
	                		processCoordinator.setGuid(guids.get(channel.getCn()));
	                		
	                		DSApi.context().session().save(processCoordinator);
	                		DSApi.context().session().flush();
	                		DSApi.context().session().clear();
	                	}
	                	else
	                	{
	                		ProcessCoordinator processCoordinator = new ProcessCoordinator();
	                		processCoordinator.setChannelCn(channel.getCn());
	                		processCoordinator.setDocumentKindCn(documentKindCn);
	                		processCoordinator.setUsername(user.get(channel.getCn()));
	                		processCoordinator.setGuid(guids.get(channel.getCn()));
	                		processCoordinator.create();

							DataMartEventBuilder.get()
								.event(EventType.COORDINATOR_CREATE)
								.objectId(processCoordinator.getId());
	                	}
	                }
	            }
	            DSApi.context().commit();
			} 
			catch (EdmException e) 
			{
				LogFactory.getLog("eprint").error("", e);
			}
        }
    }
    
    public class Bean
    {
    	private String cn;
    	private String guidName;
    	private String channelName;
    	private String guid;
    	private String user;

        @Override
        public int hashCode() {
            return cn.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Bean && ((Bean)obj).cn.equals(this.cn);
        }

        public Bean(String cn,String guidName,String channelName,String guid,String user)
    	{
    		this.cn=cn;
    		this.guidName=guidName;
    		this.channelName=channelName;
    		this.guid=guid;
    		this.user = user;
    	}
    	
    	public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}
		public String getCn() {
			return cn;
		}
		public void setCn(String cn) {
			this.cn = cn;
		}
		public String getGuidName() {
			return guidName;
		}
		public void setGuidName(String guidName) {
			this.guidName = guidName;
		}
		public String getChannelName() {
			return channelName;
		}
		public void setChannelName(String channelName) {
			this.channelName = channelName;
		}
		public String getGuid() {
			return guid;
		}
		public void setGuid(String guid) {
			this.guid = guid;
		}
    	
    }

	public LinkedHashSet<Bean> getChannels() {
		return channels;
	}

	public void setChannels(LinkedHashSet<Bean> channels) {
		this.channels = channels;
	}

	public String getDocumentKindCn() {
		return documentKindCn;
	}

	public void setDocumentKindCn(String documentKindCn) {
		this.documentKindCn = documentKindCn;
	}

	public Map<String, String> getGuids() {
		return guids;
	}

	public void setGuids(Map<String, String> guids) {
		this.guids = guids;
	}

	public List<DocumentKind> getDocumentKinds() {
		return documentKinds;
	}

	public void setDocumentKinds(List<DocumentKind> documentKinds) {
		this.documentKinds = documentKinds;
	}

	public Map<String, String> getUser() {
		return user;
	}

	public void setUser(Map<String, String> user) {
		this.user = user;
	}

    
}
