package pl.compan.docusafe.web.admin.dockind;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.HtmlTree;
import pl.compan.docusafe.web.tree.acceptance.AcceptanceStructureTree;
import pl.compan.docusafe.web.tree.acceptance.AcceptanceTreeNodeAdapter;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * 
 */
@SuppressWarnings("serial")
public class AcceptanceStructureAction extends EventActionSupport {
	
	public final static Logger log = LoggerFactory.getLogger(AcceptanceStructureAction.class);
	//IMPORT
	/**
	 * Kod akceptacji
	 */
	private String cn;
	
	/**
	 * Warto�� steruj�ca - wraz z cn slu�y do identyfikacji dzia�u akceptuj�cego,
	 * w przypadku eventu doConditionChange s�u�y do dok�adenego wyznaczenia AcceptanceCondition
	 */
	private String fieldValue;
	
	//EXPORT
	private String editType = null;		
	private String selectedDivisionGuid;
	private DSDivision[] allDivisions;
	private String treeHtml;
	
	private List<CentrumKosztow> centra = new LinkedList<CentrumKosztow>();

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	protected void setup() {
		FillForm fillForm = new FillForm();				
		
		registerListener("doConditionChange").append(OpenHibernateSession.INSTANCE)
			.append(new ChangeCondition())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class ChangeCondition implements ActionListener {
		public void actionPerformed(ActionEvent event) {			
			
				if(getSelectedDivisionGuid() == null){
					try {
						allDivisions = DSDivision.getAllDivisions();
						//Arrays.sort(allDivisions);
						editType = "centrum";
					} catch (EdmException e) {
						log.error(e + " - " + e.getMessage(),e);
						addActionError(e.getMessage());
					}
				} else {
					try {
						DSApi.context().begin();
						
						List<AcceptanceCondition> ac = AcceptanceCondition.find(cn, fieldValue);
						if(ac != null && ac.size() > 0){
							ac.get(0).setUsername(null);
							ac.get(0).setDivisionGuid(selectedDivisionGuid);
							DSApi.context().session().update(ac.get(0));							
						} else {
							addActionError("B��d wyboru dzia�u");
						}
						
						DSApi.context().commit();
					} catch (EdmException e) {
						DSApi.context().setRollbackOnly();
						log.error(e + " - " + e.getMessage(),e);
						addActionError(e.getMessage());
					}

				}
						
		}	
	}		

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			try {
				Object expanded = getExpanded();								
				// klasa tworz�ca urle dla element�w drzewa
				final UrlVisitor urlVisitor = new UrlVisitor() {
					public String getUrl(Object element) {
						if(element instanceof AcceptanceTreeNodeAdapter){
							AcceptanceTreeNodeAdapter el = (AcceptanceTreeNodeAdapter) element;
							return "acceptance-structure.action?cn="+ el.getCn() 
								+ (el.getFieldValue() != null ?
									"&fieldValue=" + el.getFieldValue()
									: "");
						} else {
							return "acceptance-structure.action";
						}								
					}
				};

				//TODO - uog�lni� na r�ne rodzaje dokumnet�w 
				final HtmlTree tree = 
					AcceptanceStructureTree.newTree(
							urlVisitor,
							DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND) , 
							expanded, 
							ServletActionContext.getRequest().getContextPath());				

				treeHtml = tree.generateTree();
			}

			catch (EdmException e) {
				addActionError(e.getMessage());
			}
		}
		
		private void fillCentra(String guid) throws NumberFormatException, EdmException{
			if(guid == null){
				return;
			}
			
			for(AcceptanceCondition ac: AcceptanceCondition.findByGuid(guid)){
				getCentra().add(CentrumKosztow.find(Integer.parseInt(ac.getFieldValue())));
			}
		}
			
		private Object getExpanded() throws EdmException{
			Object expanded = null;
			if (cn != null) {
				if (fieldValue != null) {
					List<AcceptanceCondition> l = AcceptanceCondition.find(
							cn, fieldValue);
					if(l.size() > 0){
						expanded = l.get(0);
						fillCentra(l.get(0).getDivisionGuid());
					}
				} else { //<=> if(fieldValue == null) <=> wybrana akceptacja jest w korzeniu
					List<AcceptanceCondition> l = AcceptanceCondition.find(cn);
					expanded = l.size() > 0 ? l.get(0) : null;				
				}
			}
			return expanded;
		}
	}

	public String getTreeHtml() {
		return treeHtml;
	}

	public void setTreeHtml(String treeHtml) {
		this.treeHtml = treeHtml;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public List<CentrumKosztow> getCentra() {
		return centra;
	}

	public void setEditType(String editType) {
		this.editType = editType;
	}

	public String getEditType() {
		return editType;
	}

	public DSDivision[] getAllDivisions() {
		return allDivisions;
	}

	public void setSelectedDivisionGuid(String selectedDivisionGuid) {
		log.debug("AcceptanceStructureAction.java:selectedDivisionGuid = " + selectedDivisionGuid);
		this.selectedDivisionGuid = selectedDivisionGuid;
	}

	public String getSelectedDivisionGuid() {
		return selectedDivisionGuid;
	}
}
