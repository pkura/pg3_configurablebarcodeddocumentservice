package pl.compan.docusafe.web.admin.dockind;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class AcceptancesAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(AcceptancesAction.class);

    // @IMPORT/@EXPORT
    private Long id;
    private String cn;
    private String name;
    private String value;
    private String username;
    private String divisionGuid;
    /** Maksymalna kwota do akceptacji */
    private Double maxAmountForAcceptance;
    private List<CentrumKosztow> centra;
    private String by;
    // @EXPORT
    private List<DSUser> users;
    private List<DSDivision> divisions;
    private List<Map<String,Object>> conditions;
    
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
                DSDivision[] divisionTab  = DSDivision.getAllDivisions();
                divisions = new ArrayList<DSDivision>();
                for (int i = 0; i < divisionTab.length; i++) 
                {
                	if(!divisionTab[i].isHidden())
                		divisions.add(divisionTab[i]);
				}
                Collections.sort(divisions, new DSDivision.DivisionComparatorAsName());
                centra = CentrumKosztow.listAll();
                
                conditions = AcceptanceCondition.getConditionsBeans(by);
                
            }
            catch (EdmException e)
            {
				log.error(e.getMessage(), e);
                addActionError(e.getMessage());                
            }
        }
    }
    
    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            cn = TextUtils.trimmedStringOrNull(cn);
            username = TextUtils.trimmedStringOrNull(username);
            divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid);
            value = TextUtils.trimmedStringOrNull(value);
            
            try
            {
                DSApi.context().begin();
				DataMartEventBuilder builder = DataMartEventBuilder.create();

                
                AcceptanceCondition condition = new AcceptanceCondition();
                condition.setCn(cn);
                condition.setDivisionGuid(divisionGuid);
                condition.setUsername(username);
                condition.setFieldValue(value);
                condition.setMaxAmountForAcceptance(maxAmountForAcceptance);
						
				builder.event(EventType.ACCEPTANCE_CONDITION_CREATE)
					.objectId(DSApi.context().session().save(condition));

//				builder.log();
                AcceptanceCondition.clearCaches();
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
				log.error(e.getLocalizedMessage(), e);
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
            {
                addActionError("Nie wskazano warunku do usuniÍcia");
                return;
            }
            
            try
            {
                DSApi.context().begin();
				DataMartEventBuilder builder = DataMartEventBuilder.create();
                
                AcceptanceCondition condition = AcceptanceCondition.find(id);
                Persister.delete(condition);
				
				builder.event(EventType.ACCEPTANCE_CONDITION_DELETE).objectId(id);

//				builder.log();
                AcceptanceCondition.clearCaches();
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getDivisionGuid()
    {
        return divisionGuid;
    }

    public void setDivisionGuid(String divisionGuid)
    {
        this.divisionGuid = divisionGuid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public List<DSDivision> getDivisions()
    {
        return divisions;
    }

    public List<DSUser> getUsers()
    {
        return users;
    }

    public List<Map<String, Object>> getConditions()
    {
        return conditions;
    }

	public List<CentrumKosztow> getCentra() {
		return centra;
	}

	public void setCentra(List<CentrumKosztow> centra) {
		this.centra = centra;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public Double getMaxAmountForAcceptance() {
		return maxAmountForAcceptance;
	}

	public void setMaxAmountForAcceptance(Double maxAmountForAcceptance) {
		this.maxAmountForAcceptance = maxAmountForAcceptance;
	}        
}
