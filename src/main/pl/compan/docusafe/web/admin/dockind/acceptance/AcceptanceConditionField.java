package pl.compan.docusafe.web.admin.dockind.acceptance;

/**
 * Interfejs obiektu zawierającego dane potrzebne by odpowiedniowo zdefiniować poziom akceptacji.
 * @author MSankowski
 *
 */
public interface AcceptanceConditionField {

	public String getCn();

	public String getDivisionGuid();
	
	public String getText();
}