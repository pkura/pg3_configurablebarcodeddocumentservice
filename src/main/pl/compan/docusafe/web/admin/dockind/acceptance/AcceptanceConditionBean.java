package pl.compan.docusafe.web.admin.dockind.acceptance;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

public class AcceptanceConditionBean implements AcceptanceConditionField {

	private final AcceptanceCondition base;
	private final DSDivision division;
	
	private final static Logger log = LoggerFactory.getLogger(AcceptanceConditionBean.class);
	public AcceptanceConditionBean(AcceptanceCondition ac) throws EdmException{
		base = ac;
		DSDivision temp = null;
		try {			
			temp = ac.getDivisionGuid() != null? DSDivision.find(ac.getDivisionGuid()): null;
			
		} catch (DivisionNotFoundException e) {
			log.error("",e);
		} 
		division = temp;
	}
	
	public String getCn() {		
		return base.getCn();
	}

	public String getDivisionGuid() {		
		log.trace("base.getDivisionGuid() = {}",base.getDivisionGuid());
		return base.getDivisionGuid();
	}

	public String getText() {
		log.trace("division.getName() = {}",division.getName() );
		return division.getName();
	}
	
	

}
