package pl.compan.docusafe.web.admin.dockind;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment;
import pl.compan.docusafe.core.office.GroupPermissions;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class KontaKsiegoweAction extends EventActionSupport {
	private final static Logger LOG = LoggerFactory.getLogger(KontaKsiegoweAction.class);
 
	private Integer id;
	private Integer idComment;
	private String name;
	private String account_number;
	private Boolean needdescription;
	
	private List<Map<String,Object>> konta;

	private boolean sadVisible;
	private boolean fixedAssetsVisible;
    private boolean omitDirAcceptance;
    private Collection<AccountNumberComment> comments;
    private String newComment;

	protected void setup() {
		
		FillForm fillForm = new FillForm();
		FillEditForm fillEditForm = new FillEditForm();
		
		registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
		    
		registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    			
		registerListener("doView").
            append(OpenHibernateSession.INSTANCE).
            append(fillEditForm).
            appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillEditForm).
            appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doHide").
            append(OpenHibernateSession.INSTANCE).
            append(new Hide()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddComment").
            append(OpenHibernateSession.INSTANCE).
            append(new AddComment()).
            append(fillEditForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doShowComment").
        append(OpenHibernateSession.INSTANCE).
        append(new ShowComment()).
        append(fillEditForm).
        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doHideComment").
        append(OpenHibernateSession.INSTANCE).
        append(new HideComment()).
        append(fillEditForm).
        appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                konta = new LinkedList<Map<String,Object>>();

                List<AccountNumber> list = AccountNumber.listAll();
                for (AccountNumber konto : list)
                {
                    Map<String,Object> bean = new LinkedHashMap<String, Object>();
                    bean.put("id", konto.getId());
                    bean.put("account_number", konto.getNumber());
                    bean.put("name", konto.getName());  
                    bean.put("deleteLink", "/admin/konta-ksiegowe.action?doDelete=true&id="+konto.getId());
					bean.put("hideLink", "/admin/konta-ksiegowe.action?doHide=true&id="+konto.getId());
					bean.put("isVisibleInSystem", konto.isVisibleInSystem());
                    bean.put("ic3098Flag", konto.isIc3098Flag());
                    konta.add(bean);
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());                
            }
        }
    }
	
	private class FillEditForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("edit-konto");
            try
            {
                AccountNumber konto = AccountNumber.find(id);
                comments = AccountNumberComment.findForAccount(konto.getNumber());
                name = konto.getName();
                account_number = konto.getNumber();
                needdescription = konto.getNeeddescription();
				if(konto.getDiscriminator() != null){
					sadVisible = (konto.getDiscriminator() & 1) == 1;
					fixedAssetsVisible = (konto.getDiscriminator() & 2) == 2;
				}
                omitDirAcceptance = konto.isIc3098Flag();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
            event.setResult("edit-konto");
        }
    }

    private class AddComment extends TransactionalActionListener {
        @Override
        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
            if(StringUtils.isBlank(newComment)){
                throw new EdmException("Komentarz nie mo�e by� pusty");
            }
            if(!GroupPermissions.canModifyAccountComments(DSApi.context().getPrincipalName())){
                throw new EdmException("Brak uprawnie� do edycji komentarzy dla kont kosztowych");
            }
        }

        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            AccountNumber ac = AccountNumber.find(id);
			AccountNumberComment anc = new AccountNumberComment();
            anc.setAccountNumber(ac.getNumber());
            anc.setName(newComment);
            DSApi.context().session().save(anc);
        }

        @Override
        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
            event.setResult("edit-konto");
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
    }

	private class Hide extends TransactionalActionListener{

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception {
			AccountNumber ac = AccountNumber.find(id);
			ac.setVisibleInSystem(!ac.isVisibleInSystem());
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
	}
	
	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            account_number = TextUtils.trimmedStringOrNull(account_number);
            name = TextUtils.trimmedStringOrNull(name);        
            
            try
            {
                DSApi.context().begin();
                
                AccountNumber konto = AccountNumber.find(id);
                konto.setNumber(account_number);
                konto.setName(name);
                konto.setNeeddescription(needdescription);
				konto.setDiscriminator((sadVisible?1:0) + (fixedAssetsVisible?2:0));
                konto.setIc3098Flag(omitDirAcceptance);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }            
        }
    }
	
	private class Add implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
						
			name = TextUtils.trimmedStringOrNull(name);
			account_number = TextUtils.trimmedStringOrNull(account_number);
			
			try {
				DSApi.context().begin();
				if(AccountNumber.findByNumber(account_number) != null)
					throw new EdmException("Istnieje konto o tym numerze");
				
				AccountNumber konto = new AccountNumber();
				konto.setName(name);
				konto.setNumber(account_number);
				konto.setNeeddescription(needdescription);
				konto.setVisibleInSystem(true);
				
				Persister.create(konto); 
				DSApi.context().commit();
				
				name = null;
				account_number = null;				
				
			} 
			catch(EdmException e) 
			{				
				addActionError(e.getMessage());
                DSApi.context()._rollback();
			}
			
			
		}
	}
	
	private class Delete implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (id == null)
            {
                addActionError("Nie wskazano warunku do usuni�cia");
                return;
            }
            
            try
            {
                DSApi.context().begin();
                
                AccountNumber konto = AccountNumber.find(id);
                Persister.delete(konto);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
	
	
	 private class ShowComment extends TransactionalActionListener {
	        @Override
	        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
	            if(!GroupPermissions.canModifyAccountComments(DSApi.context().getPrincipalName())){
	                throw new EdmException("Brak uprawnie� do edycji komentarzy dla kont kosztowych");
	            }
	        }

	        @Override
	        public void transaction(ActionEvent event, Logger log) throws Exception {
				AccountNumberComment anc = AccountNumberComment.find(idComment);
	            anc.setVisible(true);
	        }

	        @Override
	        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
	            event.setResult("edit-konto");
	        }

	        @Override
	        public Logger getLogger() {
	            return LOG;
	        }
	    }
	 
	 
	 private class HideComment extends TransactionalActionListener {
	        @Override
	        public void beforeTransaction(ActionEvent event, Logger log) throws Exception {
	            if(!GroupPermissions.canModifyAccountComments(DSApi.context().getPrincipalName())){
	                throw new EdmException("Brak uprawnie� do edycji komentarzy dla kont kosztowych");
	            }
	        }

	        @Override
	        public void transaction(ActionEvent event, Logger log) throws Exception {
	        	AccountNumberComment anc = AccountNumberComment.find(idComment);
	            anc.setVisible(false);
	        }

	        @Override
	        public void afterTransaction(ActionEvent event, Logger log) throws Exception {
	            event.setResult("edit-konto");
	        }

	        @Override
	        public Logger getLogger() {
	            return LOG;
	        }
	    }
	 
	 public Integer getId() {
		 return id;
	 }

	 public void setId(Integer id) {
		 this.id = id;
	 }

	 public void setNumber(String account_number) {
		 this.account_number = account_number;
	 }
	 
	 public String getNumber() {
		 return account_number;
	 }
	 
	 public void setName(String name) {
		 this.name = name;
	 }
	 
	 public String getName() {
		 return this.name;
	 }
	 
	 public List<Map<String, Object>> getKonta() {
	 	return konta;
	 }

	public Boolean getNeeddescription() {
		return needdescription;
	}

	public void setNeeddescription(Boolean needdescription) {
		this.needdescription = needdescription;
	}

	public boolean isSadVisible() {
		return sadVisible;
	}

	public void setSadVisible(boolean sadVisible) {
//		LOG.debug("sadVisible = " + sadVisible);
		this.sadVisible = sadVisible;
	}

	public boolean isFixedAssetsVisible() {
		return fixedAssetsVisible;
	}

	public void setFixedAssetsVisible(boolean fixedAssetsVisible) {
//		LOG.debug("fixedAssetsVisible = " + fixedAssetsVisible);
		this.fixedAssetsVisible = fixedAssetsVisible;
	}

    public boolean isOmitDirAcceptance() {
        return omitDirAcceptance;
    }

    public void setOmitDirAcceptance(boolean omitDirAcceptance) {
        this.omitDirAcceptance = omitDirAcceptance;
    }

    public Collection<AccountNumberComment> getComments() {
        return comments;
    }

    public String getNewComment() {
        return newComment;
    }

    public void setNewComment(String newComment) {
        this.newComment = newComment;
    }

	public Integer getIdComment() {
		return idComment;
	}

	public void setIdComment(Integer idComment) {
		this.idComment = idComment;
	}
    
    
}