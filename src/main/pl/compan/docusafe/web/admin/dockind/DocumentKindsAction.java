package pl.compan.docusafe.web.admin.dockind;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.io.XMLWriter;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.crypto.DockindCryptoDigester;
import pl.compan.docusafe.core.crypto.IndividualRepositoryKey;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.parametrization.ic.AssecoImportManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.google.gson.Gson;
import com.opensymphony.webwork.ServletActionContext;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 *
 */
public class DocumentKindsAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger(DocumentKindsAction.class);
	private static final Log archive_log = LogFactory.getLog("archive_log");
	
    // @EXPORT
    private List<DocumentKind> documentKinds;
    private List<String> availableDocumentKindsCn = DocumentKind.getAvailableDocumentKindsCn();
    
    // @IMPORT
    private String name;
    private String cn;
    private String tableName;
    private FormFile file;
    private Long fromId;
    private Long toId;
    /**
     * Dla JSON - string zawieraj�cy xml do zapisu
     */
    private String xml;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdateDicInvoice")
                .append(OpenHibernateSession.INSTANCE)
                .append(new UpdateDicInvoice())
                .append(fillForm)
                .appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetXml").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetXml()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doCreateJson").
	        append(OpenHibernateSession.INSTANCE).
	        append(new CreateJson()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetXmlJson").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetXmlJson()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doGetDockindsJson").
	        append(OpenHibernateSession.INSTANCE).
	        append(new GetDockindsJson()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doEnableDisable").
        	append(OpenHibernateSession.INSTANCE).
        	append(new EnableDisable()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSaveAll").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new SaveAll()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        

        /*registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);*/
    }    
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {    
            	DockindCryptoDigester dcd = DockindCryptoDigester.getInstance();
                documentKinds = DocumentKind.list();   
                for (DocumentKind dk : documentKinds) 
                {
                	if(dcd.isVerifyable(dk.getCn()))
                		dk.setAvailableToDate(dcd.getDockindAvailabilityToDate(dk.getCn()));
				}
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private final class UpdateDicInvoice extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log) throws Exception {
            if(fromId == null) {
                addActionError("Brak fromId");
            } else {
                new AssecoImportManager().updateDicInvoice(fromId);
            }
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }
    
    private class EnableDisable implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
        	{
            	DSApi.context().begin();
            	DocumentKind.findByCn(cn).setEnabled(!DocumentKind.findByCn(cn).isEnabled());
            	DSApi.context().commit();
        	}        	
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
        		DSApi.context().setRollbackOnly();
				log.debug(e.getStackTrace());
			}
        }
    }
    
    private class GetXml implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
        	{
        		HttpServletResponse resp = ServletActionContext.getResponse();
        		resp.setContentType("text/xml");
        		resp.setHeader("Content-disposition", "attachment; filename=\""+cn+".xml\"");
        		Document xml_to_print = DocumentKind.findByCn(cn).getXML();
        		XMLWriter writer = new XMLWriter(resp.getOutputStream(), new org.dom4j.io.OutputFormat());
        		writer.write(xml_to_print);
        	}        	
        	catch (Exception e) {
				log.debug(e.getStackTrace());
			}
        	
        	
        }
    }
    
    private PrintWriter getOutPrintWriter() {
    	try {
	    	HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/json; charset=utf-8");
			return resp.getWriter();
    	} catch(IOException e) {
    		log.error(e.getMessage(), e);
    		return null;
    	}
    }
    
    private class CreateJson implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			PrintWriter out = getOutPrintWriter();
			log.debug("CreateJsonp:");
			log.debug(xml);
			log.debug("===========================");
			
			try {
				DSApi.context().begin();
				FormFile formFile = new FormFile(xml.getBytes("iso-8859-2"), cn + ".xml");
				
				createOrUpdate(cn, null, null, formFile.getFile(), null);
			} catch(Exception e) {
				DSApi.context().setRollbackOnly();
				log.error(e.getMessage(), e);
			} 
			
			try {
                DSApi.context().commit();
                DocumentKind.resetDockindInfo(cn);
                TaskListAction.initDockind();
                name = null; 
                cn = null;                
                tableName = null;
            } catch (EdmException e) {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("",e);
            }
            
            out.flush();
            out.close();
		}
    }
    
    private class GetXmlJson implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			PrintWriter out = getOutPrintWriter();
			
			try {
				Document doc = DocumentKind.findByCn(cn).getXML();
				Writer osw = new StringWriter();
	    		XMLWriter writer = new XMLWriter(osw, new org.dom4j.io.OutputFormat());
	    		writer.write(doc);
	    		writer.close();
	    		
	    		String xmlString = osw.toString();
	    		String respString = (new Gson()).toJson(xmlString);
	    		
	    		log.debug("GetXmlJsonp:");
				log.debug(xmlString);
				log.debug("===========================");
	  
	    		out.write(respString);
			} catch(EdmException e) {
				log.error(e.getMessage(), e);
				out.write("FAIL");
			} catch(IOException e) {
				log.error(e.getMessage(), e);
				out.write("FAIL");
			} finally {
				out.flush();
				out.close();
			}
		}
    }
    
    private class GetDockindsJson implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			PrintWriter out = getOutPrintWriter();
			/**
			 * Cn -> Nazwa
			 */
			SortedMap<String, String> dockinds = new TreeMap<String, String>();
			
			try {
				documentKinds = DocumentKind.list();
				for(DocumentKind dockind : documentKinds) {
					dockinds.put(dockind.getCn(), dockind.getName());
				}
				
				out.write((new Gson()).toJson(dockinds));
			} catch(EdmException e) {
				log.error(e.getMessage(), e);
				out.write("FAIL");
			} finally {
				out.flush();
				out.close();
			}
		}
    }
    
    private class SaveAll implements ActionListener
    {
		public void actionPerformed(ActionEvent event) 
		{
			
			
			/*try 
			{	
				DocumentKind dk = DocumentKind.findByCn(cn);
				Long docKindId = dk.getId();
				List<Long> documents = pl.compan.docusafe.core.base.Document.findByDocumentKind(docKindId);
				Thread thread = new Thread(new archiveDocument(documents,dk));
	            thread.start(); 
	            archive_log.error("Rozpocz�to ponowny zapis dokument�w dla cn "+cn);			
			}*/
			try 
			{	
				//LoggerFactory.getLogger("tomekl").debug("test {} {}",fromId,toId);
				DocumentKind dk = DocumentKind.findByCn(cn);
				Long docKindId = dk.getId();
				List<Long> documents = null;
				
				if(fromId != null && toId != null && toId > fromId)
				{
					documents = pl.compan.docusafe.core.base.Document.findByDocumentKindAndRange(docKindId, fromId, toId);					
				}
				else if(fromId != null && toId == null)
				{
					LoggerFactory.getLogger("tomekl").debug("test");
					documents = pl.compan.docusafe.core.base.Document.findByFolder(fromId);
				}
				else
				{
					documents = pl.compan.docusafe.core.base.Document.findByDocumentKind(docKindId);
				}
				Thread thread = new Thread(new archiveDocument(documents,dk));
	            thread.start(); 
	            archive_log.error("Rozpoczeto ponowny zapis dokumentow dla cn "+cn);			
			}
			catch (EdmException e) 
			{
				log.error(e.getMessage(), e);
			} catch (HibernateException e)
			{
                log.error(e.getMessage(), e);
			} catch (SQLException e) 
			{
                log.error(e.getMessage(), e);
			}
			
		}
    	
    }
    
    private class archiveDocument implements Runnable
    {
    	private List<Long> documents;
    	private DocumentKind dk;
    	private final int BATCH_SIZE = 100;
    	
    	
		public archiveDocument(List<Long> documents,DocumentKind dk) 
		{
			super();
			
			if (documents == null || documents.size() == 0)
				throw new IllegalStateException("documents jest null lub == 0");
			
			if (dk == null)
				throw new IllegalStateException("documentKid jest null");
			
			this.documents = documents;
			this.dk = dk;
		}

		public void run() 
		{
			try
			{
				archive_log.info("Liczba dokument�w do ponownego zapisu: " + documents.size());
				
				// liczba partii
				int parts = (int) (Math.ceil(((double) documents.size() / (double) BATCH_SIZE)));
				archive_log.info("Liczba partii dokument�w: " + parts);
				
				List<Long> failed = new ArrayList<Long>();
				
				// przetworzenie dokument�w partiami
				for (int i = 0; i < parts; i++)
				{
					int p = (i * BATCH_SIZE);
					int idx = 0;
					
					try
					{
						DSApi.openAdmin();
						DSApi.context().begin();
						
						for (int k = 0; k < BATCH_SIZE; k++)
						{
							idx = p + k;
							if (idx < documents.size())
							{
								try
								{
									pl.compan.docusafe.core.base.Document doc = pl.compan.docusafe.core.base.Document.find(documents.get(idx));
									dk.logic().archiveActions(doc,DocumentLogic.TYPE_SAVE_ALL);
									dk.logic().documentPermissions(doc);
								}
								catch (Exception e) {
									log.error("Przechwycony "+e.getMessage(),e);
								}
							}
						}
						
						archive_log.info("Wyczyszczenie sesji na idx: " + idx);
						DSApi.context().session().flush();
						DSApi.context().session().clear();
						DSApi.context().commit();
						DSApi.close();
						//DSApi.openAdmin();
					}
					catch (Exception e)
					{
						DSApi.context().rollback();
						archive_log.error(e.getMessage(), e);
						
						for (int k = 0; k < BATCH_SIZE; k++)
						{
							idx = p + k;
							if (idx < documents.size())
								failed.add(documents.get(p + k));
						}
					}
					finally
					{
						DSApi.close();
					}
				}
				
				archive_log.info("Zako�czono zapis dokument�w. Poprawnie zapisano dokument�w: " + (documents.size() - failed.size()));
				archive_log.info("Nie uda�o si� zapisa� dokument�w: " + failed.size());
				archive_log.info("Dokumenty, kt�rych nie uda�o si� ponownie zapisa�: " + failed);
			} 
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
			
//			try
//			{
//				DSApi.openAdmin();
//				
//				int i = 0;		
//				archive_log.error("START SAVE ALL DLA "+ documents.size() );
//				
//				for (Long document : documents) 
//				{
//					try 
//					{
//						DSApi.context().begin();
//						pl.compan.docusafe.core.base.Document doc = pl.compan.docusafe.core.base.Document.find(document);
//						dk.logic().archiveActions(doc,DocumentLogic.TYPE_ARCHIVE);
//						dk.logic().documentPermissions(doc);
//						DSApi.context().session().flush();
//						DSApi.context().commit();
//						i++;
//						
//						//LoggerFactory.getLogger("kamilj").debug("i == " + i);
//						
//						if(i % 10 == 0)
//						{
//							archive_log.error("Czyszczenie sesji po "+i+" dokumentach");
//							DSApi.context().session().clear();
//						}
//					}
//					catch (Exception e) 
//					{
//						DSApi.context().session().clear();
//						DSApi.context().rollback();
//						archive_log.error("B��d podczas ponownego zapisu dokumentu "+document+" "+e.getMessage(),e);
//					}					
//				}
//				archive_log.error("Zapisano ponownie "+i+" dokument�w "+ dk.getName());
//			}
//			catch (Exception e) 
//			{		
//				archive_log.error("B��D 007"+e.getMessage(),e);
//			}
//			finally
//			{
//				try {
//					DSApi.close();
//				} catch (EdmException e) 
//				{
//					log.error("",e);
//				}
//			}
		}
    	
    }
    
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	if(tableName!=null && tableName.length()>35)
        	{
        		addLicense();
        		addActionError("Nazwa tabeli nie moze przekraczac 35 znakow");
        		return;
        	}
        	else if("support".equals(tableName))
        	{
        		addSupport();
        		return;
        	}
        	
        	if (StringUtils.isEmpty(cn))
                addActionError("Nie podano nazwy kodowej tworzonego typu");
        	if (file == null || !file.sensible())
                addActionError("Nie podano pliku opisuj�cego rodzaj dokumentu");
            /*if (StringUtils.isEmpty(name))
            {
                addActionError("Nie podano nazwy tworzonego typu");
            }
            if (StringUtils.isEmpty(tableName))
            {
                addActionError("Nie podano nazwy tabeli w bazie dla tworzonego rodzaju");
            }*/
            
            if (hasActionErrors())
                return;
            cn =  StringUtils.trim(cn);
            name = StringUtils.trim(name);
            tableName = StringUtils.trim(tableName);
            
            try {
                DSApi.context().begin();
                // TODO: narazie ta funkcja sluzy jednoczesnie do twozrenia nowego i update popzredniego 
                // (dlatego ponizsze wylaczone chwilowo) - ROZDZIELIC!!!
               /* if (DocumentKind.findByCn(cn) != null)
                    throw new EdmException("Istnieje ju� typ dokumentu o nazwie kodowej: "+cn);*/

                if (!DocumentKind.getAvailableDocumentKindsCn().contains(cn))
                {
					if (AvailabilityManager.isAvailable("createDockindByUser"))
					{
               		 	DocumentKind.getAvailableDocumentKindsCn().add(cn);
               	 	}
					else
						throw new EdmException("Nie jest w systemie zaimplementowany rodzaj dokumentu o podanej nazwie kodowej. Podaj prawid�ow� nazw� kodow�.");
                }
                createOrUpdate(cn, tableName, name, file.getFile(), null);

            } catch (Exception e) {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("",e);
			}

            try {
                // czyszczenie formularza
                DSApi.context().commit();
                DocumentKind.resetDockindInfo(cn);
                TaskListAction.initDockind();
                name = null; 
                cn = null;                
                tableName = null;
                addActionMessage("Zapisano podany rodzaj dokumentu");
            } catch (EdmException e) {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
                log.error("",e);
            }
        }
    }
    
    private void createOrUpdate(String cn, String tablename, String name, File file, String classname) throws EdmException
    {
        DocumentKind documentKind = DocumentKind.findByCn(cn);
        
        if (documentKind == null)
        {
        	if (StringUtils.isEmpty(name))
                addActionError("Nie podano nazwy tworzonego typu");
            if (StringUtils.isEmpty(tableName))
                addActionError("Nie podano nazwy tabeli w bazie dla tworzonego rodzaju");
            
            documentKind = new DocumentKind();
            documentKind.setCn(cn);
            documentKind.setTablename(tablename);
            documentKind.setName(name);
            documentKind.setEnabled(true);
            documentKind.setAddDate(new Date());
            Persister.create(documentKind);
        }
        else
        {
        	documentKind.setTablename(tablename != null ? tablename : documentKind.getTablename());
            documentKind.setName(name != null ? name : documentKind.getName());
            documentKind.setAddDate(new Date());
        }
      try {  
        documentKind.saveContent(file);
        documentKind.loadXmlData(); 
      }catch(  Exception e ){
    	  DSApi.context().rollback();
      }
        //dzi�ki temu wywala je�li dockind b�dzie nieprawid�owy
    }
    
    private void addLicense()
    {
    	try
    	{
    		DSApi.context().begin();
    		IndividualRepositoryKey irk = new IndividualRepositoryKey();
    		irk.setPrivateKey(tableName);
    		irk.setUsername("docusafe");
    		irk.setPublicKey(tableName);
    		irk.setStatus(IndividualRepositoryKey.AKTYWNY);
    		irk.setKeyCode("docusafe");
    		
    		DSApi.context().session().save(irk);
    		DSApi.context().commit();
    	}
    	catch (Exception e) 
    	{
    		DSApi.context().setRollbackOnly();
			log.error(e.getMessage(),e);
		}
    }
    
    private void addSupport()
    {
    	try
    	{
    		DSApi.context().begin();
    		DSUser.findByUsername(DSApi.context().getPrincipalName()).addRole("support");
    		DSApi.context().commit();    		
    		addActionMessage("Aby uaktywnic konto support zaloguj sie jeszcze raz");
    	}
    	catch (Exception e) 
    	{
    		DSApi.context().setRollbackOnly();
    		log.error(e.getMessage(),e);
		}
    }
    
    public List<DocumentKind> getDocumentKinds()
    {
        return documentKinds;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public void setFile(FormFile file)
    {
        this.file = file;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

	public Long getFromId() {
		return fromId;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	public Long getToId() {
		return toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

	public void setAvailableDocumentKindsCn(List<String> availableDocumentKindsCn) {
		this.availableDocumentKindsCn = availableDocumentKindsCn;
	}

	public List<String> getAvailableDocumentKindsCn() {
		return availableDocumentKindsCn;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getXml() {
		return xml;
	}
}
