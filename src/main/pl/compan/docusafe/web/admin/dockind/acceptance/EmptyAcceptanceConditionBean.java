package pl.compan.docusafe.web.admin.dockind.acceptance;

import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;

public class EmptyAcceptanceConditionBean implements AcceptanceConditionField {
	
	private final AcceptancesDefinition.Acceptance emptyBase;	
	
	public EmptyAcceptanceConditionBean(AcceptancesDefinition.Acceptance emptyBase){
		this.emptyBase = emptyBase;
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.web.admin.dockind.AcceptanceConditionDataProvider#getCn()
	 */
	public String getCn(){
		return emptyBase.getCn();
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.web.admin.dockind.AcceptanceConditionDataProvider#getDivisionGuid()
	 */
	public String getDivisionGuid(){
		return "";
	}

	public String getText() {
		return "Wybierz dzia�";
	}

}
