package pl.compan.docusafe.web.admin.dockind;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.addressing.EndpointReference;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.HtmlTree;
import pl.compan.docusafe.web.tree.acceptance.AcceptanceOrganizationUrlProvider;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub.ConditionEntry;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub.ConditionRequest;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub.ConditionResponse;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub.Exchange;
import pl.compan.docusafe.ws.client.absences.ConditionServiceStub.ExchangeResponse;

import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.Collections;

public class AcceptancesTreeAction extends EventActionSupport
{
	private static final Logger log = LoggerFactory.getLogger("acceptance_division");

	private Long acceptanceDivisionId;
	private AcceptanceDivisionImpl acceptanceDivision;
	private String treeHtml;
	private Long toChangeId;
	
	private Map<String ,String> divisionTypes;
	private String divisionType;
	private String code;
	private String name;
	private List<DSUser> users;
	private String user;
	private Integer ogolna;
	private List<DSDivision> divisions;
	private List<CentrumKosztow> centrumKosztow;
	private Integer idCK;
	private String guid;
	
	public String getBaseLink()
    {
        return "/absences/acceptances-tree.action";
    }
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRemove").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Remove()).
	        append(new FillForm()).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doPersist").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Persist()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("tomekl").debug("test");
        	try
        	{
        		divisionType = null;
	        	code = null;
	        	name = null;
	        	user = null;
	        	ogolna = null;
	        	guid = null;
        		
        		divisionTypes = new HashMap<String, String>();
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE, "akceptacja");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_DIVISION_DIVISION_TYPE, "grupa");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE, "uzytkownik");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE, "uzytkownik zewnetrzny");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE, "dzia�");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_CENTRUM_KOSZTOW_TYPE, "Centrum kosztow");
	        	divisionTypes.put(AcceptanceDivisionImpl.ACCEPTANCE_FIELD_VALUE_TYPE, "Warto�� pola");
        		
	        	users = DSUser.list(0);	  
	        	divisions = Arrays.asList(DSDivision.getOnlyDivisionsAndPositions(false));
	        	java.util.Collections.sort(divisions, DSDivision.NAME_COMPARTOR);
	        	centrumKosztow = CentrumKosztow.list();
	        	Collections.sort(users, DSUser.LASTNAME_COMPARATOR);
	        	//System.out.println(acceptanceDivisionId);
	        	if(acceptanceDivisionId != null)
	        		acceptanceDivision = AcceptanceDivisionImpl.findById(acceptanceDivisionId);
	        	if(acceptanceDivision == null)
	        		acceptanceDivision = AcceptanceDivisionImpl.findByCode("root");
	        	
	        	final AcceptanceOrganizationUrlProvider provider = new AcceptanceOrganizationUrlProvider()
	        	{
					public String getUrl(AcceptanceDivisionImpl div) 
					{
						StringBuilder url = new StringBuilder(
								ServletActionContext.getRequest().getContextPath()
								+ getBaseLink()
								+ "?acceptanceDivisionId="
								+ div.getId()
						);
						return url.toString();
					}
	        	};
	        	
	        	final HtmlTree tree = ExtendedOrganizationTree.createAcceptancesOrganizationTree(acceptanceDivision, provider, ServletActionContext.getRequest().getContextPath());
	        	treeHtml = tree.generateTree();
        	}
        	catch (Exception e) 
        	{
        		log.error(e.getMessage(),e);
				e.printStackTrace();
			}
        }
    }
    
    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("tomekl").debug("add");
        	try
        	{
        		if(toChangeId == null)
        			throw new EdmException("toChangeId null");
        			
        		DSApi.context().begin();
        		
        		if(divisionType.equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE))
        		{
        			AcceptanceDivisionImpl.create("xxx", DSUser.findByUsername(user).asLastnameFirstname(), divisionType, user, toChangeId, 0);
        		}
        		else if(divisionType.equals(AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE))
        		{
        			AcceptanceDivisionImpl.create("xxx", name, divisionType, "ext:"+code, toChangeId, 0);
        		}
        		else if(divisionType.equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE))
        		{
        			AcceptanceDivisionImpl.create(guid, name, divisionType, code, toChangeId, 0);
        		}
        		else if(divisionType.equals(AcceptanceDivisionImpl.ACCEPTANCE_CENTRUM_KOSZTOW_TYPE))
        		{
        			AcceptanceDivisionImpl.create(idCK!=null?idCK.toString():"xxx", name, divisionType, code, toChangeId, 0);
        		}
        		else
        		{
        			AcceptanceDivisionImpl.create("xxx", name, divisionType, code, toChangeId, ogolna == null ? 0 : ogolna);
        		}
        		DSApi.context().commit();
        		DSApi.context().session().clear();
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
				DSApi.context()._rollback();
				e.printStackTrace();
			}
        }
    }
    
    private class Remove implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	LoggerFactory.getLogger("tomekl").debug("remove");
        	try
        	{
        		DSApi.context().begin();
        		AcceptanceDivisionImpl.remove(toChangeId);
        		DSApi.context().commit();
        		DSApi.context().session().clear();
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
        		DSApi.context()._rollback();
        		e.printStackTrace();
			}
        }
    }

    private class Persist implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	PreparedStatement ps = null;
        	LoggerFactory.getLogger("tomekl").debug("persist");
        	try
        	{
        		DSApi.context().begin();
        		for(AcceptanceCondition ac : AcceptanceCondition.list())
        		{
        			if(ac.getCn().equals("szef_dzialu") || ac.getCn().equals("dzial_personalny"))
        			{
        				pl.compan.docusafe.core.Persister.delete(ac);
        			}
        		}
        		
        		String slaveLink = Docusafe.getAdditionProperty("acceptanceTree.removeBeforeSave");
        		if(slaveLink != null && slaveLink.equals("true"))
        		{
        			String del = "delete from ds_acceptance_condition";
        			ps = DSApi.context().prepareStatement(del);
                    ps.execute();
        		}

        		
        		saveAcceptances2(AcceptanceDivisionImpl.findByCode("root"), new HashSet<String>());
        		
        		DSApi.context().commit();
        	}
        	catch (Exception e) 
        	{
        		addActionError(e.getMessage());
        		DSApi.context()._rollback();
        		e.printStackTrace();
			}
        	finally
        	{
    			DSApi.context().closeStatement(ps);
        	}
        	
        	//webservice
        	try
        	{
        		
        		String slaveLink = Docusafe.getAdditionProperty("absences.slave");
        		if(slaveLink != null)
        		{
        			ConditionServiceStub stub = new ConditionServiceStub();
            		stub._getServiceClient().getOptions().setTo(new EndpointReference(slaveLink));
            		
            		Exchange ex = new Exchange();
            		ConditionRequest req = new ConditionRequest();
            		        		
            		req.setConditions(getEntries());        		
            		
            		ex.setRequest(req);
            		ExchangeResponse response = stub.exchange(ex);
            		ConditionResponse conditionResponse = response.get_return();
            		if(conditionResponse.getStatus() < 1)
            			throw new EdmException(conditionResponse.getError());
        		}        		
        		
        		
        	}
        	catch (Exception e) 
        	{
				log.debug(e.getMessage(),e);
				e.printStackTrace();
			}
        }
    }
    
    private static ConditionEntry[] getEntries() throws EdmException
    {
    	List<ConditionEntry> entries = new ArrayList<ConditionServiceStub.ConditionEntry>();
    	for(AcceptanceCondition ac : AcceptanceCondition.list())
		{
			if(ac.getCn().equals("szef_dzialu") || ac.getCn().equals("dzial_personalny"))
			{
				ConditionEntry entry = new ConditionEntry();
				entry.setCn(ac.getCn());
				entry.setDivisionGuid(ac.getDivisionGuid());
				entry.setFieldValue(ac.getFieldValue());
				entry.setMaxAmountForAcceptance(ac.getMaxAmountForAcceptance() == null ? -1d : ac.getMaxAmountForAcceptance());
				entry.setUsername(ac.getUsername());
				entries.add(entry);
			}
		}
    	
    	return entries.toArray(new ConditionEntry[entries.size()]);
    }
    
    
    private static void saveAcceptances2(AcceptanceDivisionImpl root, Set<String> acceptancesToRewrite) throws EdmException
    {
    	Set<String> rewrite = new HashSet<String>(acceptancesToRewrite);   	
    	
    	for(AcceptanceDivisionImpl child : root.getChildren())
    	{
    		if(child.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DIVISION_DIVISION_TYPE))
    		{
    			saveAcceptances2(child, rewrite);
    		}
    		else if(child.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE) && child.getOgolna().equals(1))
    		{
    			for(AcceptanceDivisionImpl acceptance : child.getChildren())
    			{
    				AcceptanceCondition condition = new AcceptanceCondition();
    				condition.setCn(child.getCode());
    				if (acceptance.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE))
    				{
    					condition.setDivisionGuid(acceptance.getGuid());
    					condition.setUsername(null);
    				}
    				else
    				{
    					condition.setDivisionGuid(null);
    					condition.setUsername(acceptance.getCode());
    				}
    				pl.compan.docusafe.core.Persister.create(condition);
    			}
    		}
    		else if((child.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE) 
    				|| child.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE)
    				)&& child.getOgolna().equals(0))
    		{
    			for(AcceptanceDivisionImpl acceptance : child.getChildren())
    			{
    				if(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE.equals(acceptance.getDivisionType())||
    						AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE.equals(acceptance.getDivisionType()))
    					rewrite.add(child.getCode()+";"+acceptance.getCode()+";"+acceptance.getDivisionType());
    				else
    					rewrite.add(child.getCode()+";"+acceptance.getGuid()+";"+acceptance.getDivisionType());
    				
    			}
    			List<String> users = new ArrayList<String>();
    			for(AcceptanceDivisionImpl user : child.getParent().getChildren())
    			{
    				if(user.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE))
    				{
    					users.add(user.getCode());
    				}
    				
    				if(user.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE) || user.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_FIELD_VALUE_TYPE))
    				{
    					users.add(user.getCode());
    				}
    				
    				if(user.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE)||user.getDivisionType().equals(AcceptanceDivisionImpl.ACCEPTANCE_CENTRUM_KOSZTOW_TYPE))
    				{
    					users.add(user.getGuid());
    				}
    			}
    			
    			for(String str : rewrite)
    			{
    				for(String userval : users)
					{
    					AcceptanceCondition condition = new AcceptanceCondition();
        				condition.setCn(str.split(";")[0]);
        				if(AcceptanceDivisionImpl.ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE.equals(str.split(";")[2]) || 
        						AcceptanceDivisionImpl.ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE.equals(str.split(";")[2]))
        				{
	        				condition.setDivisionGuid(null);
	        				condition.setUsername(str.split(";")[1]);
        				}
        				else
        				{
        					condition.setDivisionGuid(str.split(";")[1]);
	        				condition.setUsername(null);
        				}
        				condition.setFieldValue(userval);
        				pl.compan.docusafe.core.Persister.create(condition);
					}
    			}
    		
    		}    		
    	}
    }
    
    public Long getAcceptanceDivisionId() {
		return acceptanceDivisionId;
	}

	public void setAcceptanceDivisionId(Long acceptanceDivisionId) {
		this.acceptanceDivisionId = acceptanceDivisionId;
	}

	public AcceptanceDivisionImpl getAcceptanceDivision() {
		return acceptanceDivision;
	}

	public void setAcceptanceDivision(AcceptanceDivisionImpl acceptanceDivision) {
		this.acceptanceDivision = acceptanceDivision;
	}

	public String getTreeHtml() {
		return treeHtml;
	}

	public void setTreeHtml(String treeHtml) {
		this.treeHtml = treeHtml;
	}

	public Long getToChangeId() {
		return toChangeId;
	}

	public void setToChangeId(Long toChangeId) {
		this.toChangeId = toChangeId;
	}

	public Map<String, String> getDivisionTypes() {
		return divisionTypes;
	}

	public String getDivisionType() {
		return divisionType;
	}

	public void setDivisionType(String divisionType) {
		this.divisionType = divisionType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getOgolna() {
		return ogolna;
	}

	public void setOgolna(Integer ogolna) {
		this.ogolna = ogolna;
	}

	public List<DSDivision> getDivisions() {
		return divisions;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public List<CentrumKosztow> getCentrumKosztow() {
		return centrumKosztow;
	}

	public Integer getIdCK() {
		return idCK;
	}

	public void setIdCK(Integer idCK) {
		this.idCK = idCK;
	}
}
