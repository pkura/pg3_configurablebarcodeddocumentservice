package pl.compan.docusafe.web.admin.dockind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.opensymphony.xwork.ActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.web.admin.dockind.acceptance.AcceptanceConditionBean;
import pl.compan.docusafe.web.admin.dockind.acceptance.AcceptanceConditionField;
import pl.compan.docusafe.web.admin.dockind.acceptance.EmptyAcceptanceConditionBean;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Tworze i zmienia ca�e kana�
 */
@SuppressWarnings("serial")
public class CreateChannelAction extends EventActionSupport 
{
	public final static Logger log = LoggerFactory.getLogger(CreateChannelAction.class);
	//IMPORT
	/**
	 * Kod akceptacji
	 */
	private String cn;
	
	/**
	 * Warto�� steruj�ca - wraz z cn slu�y do identyfikacji dzia�u akceptuj�cego,
	 */
	private String fieldValue;
	private boolean editing;
	
	/**
	 * Warto�ci, steruj�ce, kt�re ulagaj� zmianie
	 */
	private String[] cns;
	
	
	//EXPORT
	private DSDivision[] allDivisions;
	private List<AcceptanceConditionField> acceptanceConditions;	
	
	//TODO doda� inne typy field value
	private List<CentrumKosztow> centra;
	private CentrumKosztow selected;		
	
	//INTERNAL
	private AcceptancesDefinition acceptanceDefinition;
	
	

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}
	
	public List<AcceptanceConditionField> getAcceptanceConditions(){
		return acceptanceConditions;
	}

	protected void setup() {
		FillForm fillForm = new FillForm();				

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doCreate").append(OpenHibernateSession.INSTANCE)			
			.append(new Create())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doEdit").append(OpenHibernateSession.INSTANCE)			
			.append(new Edit())
			.append(fillForm)
			.appendFinally(CloseHibernateSession.INSTANCE);
	
		
	}
	
	private class Create implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			
			log.debug("CreateChannelAction.java:create");
		}		
	}
	
	/**
	 * Zmienia lub tworzy nowe akceptance conditions.
	 * @author MSankowski	 
	 */
	private class Edit implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			
			log.debug("CreateChannelAction.java:edit");
			
			try{
				Map m =(Map)ActionContext.getContext().getParameters();			
				DSApi.context().begin();		
				
				for(String codeName: cns){		
					
					String guid = ((String[]) m.get(codeName))[0];
					if(guid == null || guid.equals("")){
						addActionError("Nie wybrano dzia�u dla \"" + codeName + '"');
						continue;
					} else if(hasActionErrors()){
						continue;
					}
					
					AcceptanceCondition ac;
					List<AcceptanceCondition> list = AcceptanceCondition.find(codeName, fieldValue);
					if(list.size() == 0){ //utw�rz nowy AcceptanceCondition
						ac = new AcceptanceCondition();
						ac.setFieldValue(fieldValue);
						ac.setCn(codeName);
						DSApi.context().session().save(ac);
						addActionMessage("Utworzono nowy warunek akceptacji dla kodu:\"" + codeName+'"');
					} else { //zmie� istniej�cy					
						ac = list.get(0);
					}
					
					
					if(!guid.equals(ac.getDivisionGuid())){
						ac.setDivisionGuid(guid);
						addActionMessage("Dla " + codeName + " wybrano grup� \"" + DSDivision.find(guid).getName() + '"');
					}
				} //foreach
				
				if(hasActionErrors()){
					DSApi.context().rollback();
				} else {
					DSApi.context().commit();
				}
				
			} catch (Exception e){
				DSApi.context().setRollbackOnly();
				log.error("",e);				
			}	
		}		
	}

	private class FillForm implements ActionListener {
		
		private int thisLevel = Integer.MIN_VALUE;
		
		public void actionPerformed(ActionEvent event) {
			try {
				log.debug("CreateChannelAction.java: cn = "+cn +", fieldValue = " + fieldValue);
				
				allDivisions = DSDivision.getAllDivisions();
				Arrays.sort(allDivisions, DSDivision.NAME_COMPARTOR);
				
				if(editing){
					selected = CentrumKosztow.find(Integer.parseInt(fieldValue));
				} else {
					centra = CentrumKosztow.list();
				}
				
				
				AcceptanceCondition ac = AcceptanceCondition.find(cn, fieldValue).get(0);
				
				//TODO doda� mo�liwo�� innych typ�w dokumnet�w				
				DocumentKind kind =DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
				kind.initialize();
				
				acceptanceDefinition = 
					kind.getAcceptancesDefinition();					

				List<AcceptanceConditionField> retList = new ArrayList<AcceptanceConditionField>(acceptanceDefinition.getMaxLevel());
				if(!isEditing()){ //je�li tworzymy nowy kana� - nie uzupe�niaj wszystkich p�l tylke to do kt�rych u�ytkownik doszed� w drzewie
					thisLevel = acceptanceDefinition.getAcceptances().get(cn).getLevel();
				}
				
				log.debug("CreateChannelAction.java:thisLevel{}",thisLevel);
				
				for(int i=1; i<= acceptanceDefinition.getMaxLevel(); ++i){
					AcceptancesDefinition.Acceptance actype = acceptanceDefinition.getLevelAcceptances().get(i);
					if(actype.getFieldCn() == null) {
						break;
					} 				
					retList.add(createAcceptanceConditionField(actype));
				}
				acceptanceConditions = retList;
								
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);				
			}
		}
		
		private AcceptanceConditionField createAcceptanceConditionField(AcceptancesDefinition.Acceptance acceptanceType) throws EdmException{
			String levelCn = acceptanceType.getCn();
			List<AcceptanceCondition> conds = AcceptanceCondition.find(levelCn, fieldValue);
			if(conds.size() > 0 && acceptanceType.getLevel() >= thisLevel){
				return new AcceptanceConditionBean(conds.get(0));
				
			} else {
				return new EmptyAcceptanceConditionBean(acceptanceType);				
			}
		}
	}
	


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public List<CentrumKosztow> getCentra() {
		return centra;
	}

	public DSDivision[] getAllDivisions() {
		return allDivisions;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setCns(String[] cns) {
		this.cns = cns;
	}

	public String[] getCns() {
		return cns;
	}

	private void setSelected(CentrumKosztow selected) {
		this.selected = selected;
	}

	private CentrumKosztow getSelected() {
		return selected;
	}
}
