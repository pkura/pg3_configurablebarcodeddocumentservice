package pl.compan.docusafe.web.admin.dockind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.HtmlTree;
import pl.compan.docusafe.util.UrlVisitor;
import pl.compan.docusafe.web.tree.OrganizationTree;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
@SuppressWarnings("serial")
public class DivisionTreeAction extends EventActionSupport
{
    private DSDivision targetDivision;
    private String treeHtml;
    private String divisionGuid;
    private List<Map<String, Object>> users;
    private String cn;

	private boolean showGroups;


	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            { 
            	targetDivision = DSDivision.find(divisionGuid);
            	  // klasa tworz�ca urle dla element�w drzewa
                final UrlVisitor urlVisitor = new UrlVisitor()
                {
                    public String getUrl(Object element)
                    { 
                      return "division-tree.action?divisionGuid="+((DSDivision) element).getGuid()+"&cn="+cn;
                    }
                };

                if (targetDivision == null)
                {
                    targetDivision = DSDivision.find(DSDivision.ROOT_GUID);
                }
                
                final HtmlTree tree = OrganizationTree.newTree(
                    targetDivision,
                    urlVisitor,
                    ServletActionContext.getRequest(),
                    true,isShowGroups(), false);

                tree.setAdditionLink(cn);
                tree.setAdditionName("asd");
                treeHtml = tree.generateTree(); 
                if (targetDivision != null && !targetDivision.isRoot())
                {
                    final DSUser[] divisionUsers = targetDivision.getUsers();

                    users = new ArrayList<Map<String, Object>>(divisionUsers.length);

	                for (int i=0, n=divisionUsers.length; i < n; i++)
	                {
	                    if (divisionUsers[i].isDeleted())
	                        continue;
	
	                    Map<String, Object> bean = new HashMap<String, Object>();
	
	                    bean.put("description", divisionUsers[i].asFirstnameLastname());
	                    bean.put("name", divisionUsers[i].getName());
	
	                    users.add(bean);
	                }
                }
            }
            
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

	public DSDivision getTargetDivision()
	{
		return targetDivision;
	}

	public void setTargetDivision(DSDivision targetDivision) 
	{
		this.targetDivision = targetDivision;
	}

	public String getTreeHtml() 
	{
		return treeHtml;
	}

	public void setTreeHtml(String treeHtml)
	{
		this.treeHtml = treeHtml;
	}

	public String getDivisionGuid()
	{
		return divisionGuid;
	}

	public void setDivisionGuid(String divisionGuid) 
	{
		this.divisionGuid = divisionGuid;
	}
    
    public List<Map<String, Object>> getUsers() {
		return users;
	}

	public void setUsers(List<Map<String, Object>> users) {
		this.users = users;
	}

	public boolean isShowGroups() {
		return showGroups;
	}

	public void setShowGroups(boolean showGroups) {
		this.showGroups = showGroups;
	}
}
