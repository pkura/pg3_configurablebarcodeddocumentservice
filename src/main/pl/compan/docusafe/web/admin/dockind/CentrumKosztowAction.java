package pl.compan.docusafe.web.admin.dockind;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.swing.SwingUtilities;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.extractor.EventBasedExcelExtractor;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceModeRule;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.LocationForIC;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.IntercarsAcceptanceMode;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.naming.StringResource;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import static pl.compan.docusafe.core.datamart.DataMartEventProcessors.*;
/**
 * Akcja, kt�ra s�u�y do wy�wietlenia listy centr�w kosztowych (centrum-kosztow.jsp) i
 * ich edycji (edit-centrum-kosztow.jsp)
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @author Micha� Sankowski
 */

public class CentrumKosztowAction extends EventActionSupport	
{
	public final static Logger log = LoggerFactory.getLogger(CentrumKosztowAction.class);
	static StringManager sm = GlobalPreferences.loadPropertiesFile(CentrumKosztowAction.class.getPackage().getName(),null);
	
    // @IMPORT/@EXPORT
    private Integer id;
    private String symbol;
    private String name;
    private String order;
    private Boolean defSmplAcc;
    private boolean available;

    /** IC - automatyczne dodawanie centrum kosztowego */
    private boolean autoAdded;
    private Integer defaultAccountId;
    private Integer defaultLocationId;
    private String symbol2;

    /** IC - czy dzia� powi�zany z centrum wymaga podania nazwy umowy */
    private boolean contractNeeded;
    
    private List<AccountNumber> accounts;

    /** IC - dost�pne lokalizacje - lista potrzebna do poa defaultLocationId*/
    private List<LocationForIC> locations;
	
	private Boolean intercarsPowered;
    
    // @EXPORT
    private List<CentrumKosztow> centra;
    
    /** Centrum do edycji*/
    private CentrumKosztow centrum;
    
    /** tylko dla intercarsa - tryby akceptacji */
    private int[] modes;
    private Set<Integer> availableModes;

	private ArrayList<String> acceptanceModeNames;
    private Map<Integer, String> acceptanceModeAmounts;
    
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        FillEditForm fillEditForm = new FillEditForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doChangeAvailability").
            append(OpenHibernateSession.INSTANCE).
            append(new ChangeAvailability()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
            append(OpenHibernateSession.INSTANCE).
            append(new Add()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
                
        registerListener("doView").
            append(OpenHibernateSession.INSTANCE).
            append(fillEditForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillEditForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                if (order == null) {
                    order = "symbol";
                }
                centra = CentrumKosztow.listAllBy(order);
            }
            catch (EdmException e) {
                addActionError(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

    private class ChangeAvailability extends TransactionalActionListener {
        @Override
        public void transaction(ActionEvent event, Logger log) throws Exception {
            CentrumKosztow.find(id).setAvailable(available);
        }

        @Override
        public Logger getLogger() {
            return log;
        }
    }
    
    private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            symbol = TextUtils.trimmedStringOrNull(symbol);
         /*   username = TextUtils.trimmedStringOrNull(username);
            divisionGuid = TextUtils.trimmedStringOrNull(divisionGuid); */
            name = TextUtils.trimmedStringOrNull(name);
            
            try
            {
                DSApi.context().begin();
                
                CentrumKosztow centrum = new CentrumKosztow();
                centrum.setSymbol(symbol);
                centrum.setName(name);
                Persister.create(centrum);

				DataMartEventBuilder.get()
						.event(EventType.CENTRUM_KOSZTOW_CREATE)
						.objectId(centrum.getId().toString());
					
                DSApi.context().commit();

                // czyszczenie formularza
                symbol = null;
                name = null;
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    private class FillEditForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("edit-centrum");
            try
            {                
                centrum = CentrumKosztow.find(id);
                name = centrum.getName();
                symbol = centrum.getSymbol();
                defSmplAcc = centrum.getDefaultSimpleAcceptance();
				intercarsPowered = centrum.isIntercarsPowered();
                autoAdded = centrum.isAutoAdded();
                symbol2 = centrum.getSymbol2();

                defaultAccountId = centrum.getDefaultAccountNumber();
                defaultLocationId = centrum.getDefaultLocationId();
                
                accounts = AccountNumber.list();
                locations = LocationForIC.list();
                contractNeeded = centrum.isContractNeeded();
                
                setAvailableModes(new HashSet<Integer>());
                for(IntercarsAcceptanceMode mode: centrum.getAvailableAcceptanceModes()){
                	log.debug("CentrumKosztowAction.java:FillEditForm ID = {}",mode.getId());
                	getAvailableModes().add(mode.getId());
                }

				setAcceptanceModeNames(
					new ArrayList<String>(
						Collections.nCopies(IntercarsAcceptanceMode.values().length, (String)null)));
				for(IntercarsAcceptanceMode mode: IntercarsAcceptanceMode.values()){
					String value = 
						StringResource.get("acceptanceModeName." + centrum.getId() + "." + mode.getId());
					getAcceptanceModeNames().set(mode.ordinal(), StringUtils.trimToEmpty(value));
				}

                acceptanceModeAmounts = Maps.newHashMap();
                for(IntercarsAcceptanceMode mode: IntercarsAcceptanceMode.values()){
                    AcceptanceModeRule rule = AcceptanceModeRule.get(id, mode.getId());
                    if(rule!= null){
                        acceptanceModeAmounts.put(rule.getAcceptanceModeId(), rule.getMinAmount().toString());
                    }
                }
                
                log.debug("CentrumKosztowAction.java:FillEditForm modes = {}",getAvailableModes().toString());
                                                                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }
    
    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	log.debug("CentrumKosztowAction.java:Update modes = {}",Arrays.toString(getModes()));
            symbol = TextUtils.trimmedStringOrNull(symbol);
            name = TextUtils.trimmedStringOrNull(name);                       
            
            try
            {
                DSApi.context().begin();
				DataMartEventBuilder eventBuilder = DataMartEventBuilder.create();
                
                CentrumKosztow centrum = CentrumKosztow.find(id);
				
				if(!centrum.getSymbol().equals(symbol)){
					eventBuilder.event(EventType.CENTRUM_KOSZTOW_CHANGE)
							.objectId(centrum.getId().toString())
							.valueChange("symbol", centrum.getSymbol(), symbol);
					centrum.setSymbol(symbol);
				}
				if(!centrum.getName().equals(name)){
					eventBuilder.event(EventType.CENTRUM_KOSZTOW_CHANGE)
							.objectId(centrum.getId().toString())
							.valueChange("name", centrum.getName(), name);
					centrum.setName(name);
				}
				centrum.setDefaultSimpleAcceptance(defSmplAcc);
                centrum.setAutoAdded(autoAdded);
                centrum.setDefaultAccountNumber(defaultAccountId);
                centrum.setDefaultLocationId(defaultLocationId);
                centrum.setContractNeeded(contractNeeded);
                centrum.setSymbol2(symbol2);
				
				if(DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND) != null){
					centrum.setIntercarsPowered(intercarsPowered == Boolean.TRUE);
					Set<IntercarsAcceptanceMode> newModes = EnumSet.noneOf(IntercarsAcceptanceMode.class);
					if(modes != null){
						for(int mode: modes){
							newModes.add(IntercarsAcceptanceMode.values()[mode]);						
						}				
					} else {
						addActionError(sm.getString("NieZaznaczonoZadnegoTrybuAkceptacji"));
					}
					log.debug("CentrumKosztowAction.java:Update modes = {}",newModes);
					if(!centrum.getAvailableAcceptanceModes().equals(newModes)){
						eventBuilder.event(EventType.CENTRUM_KOSZTOW_CHANGE)
								.objectId(centrum.getId().toString())
								.valueChange("availableAcceptanceModes", 
									centrum.getAvailableAcceptanceModes().toString(),
									newModes.toString());
						centrum.setAvailableAcceptanceModes(newModes);
					}

                    HttpServletRequest request = ServletActionContext.getRequest();
                    for(IntercarsAcceptanceMode mode: newModes){
                        String value = request.getParameter("acceptanceModeName" + mode.getId());
                        if(!StringUtils.isEmpty(value)){
                            StringResource.put("acceptanceModeName." + centrum.getId() + "." + mode.getId(), value);
                        }

                        String minAmountString = request.getParameter("acceptanceModeMinimalAmount"+mode.getId());
                        AcceptanceModeRule rule = AcceptanceModeRule.findOrCreate(id, mode.getId());
                        if(!StringUtils.isEmpty(minAmountString)){
                            rule.setMinAmount(new BigDecimal(minAmountString).setScale(2, RoundingMode.HALF_EVEN));
                        }
                    }
				}

				if(!hasActionErrors()){
					DSApi.context().commit();
				} else {
					DSApi.context().rollback();
				}
            }
            catch (EdmException e)
            {
				log.error(e.getMessage(), e);
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }
    
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (id == null)
            {
                addActionError("Nie wskazano warunku do usuni�cia");
                return;
            }
            
            try
            {
                DSApi.context().begin();
                
                CentrumKosztow centrum = CentrumKosztow.find(id);
                if (!centrum.isEmpty()) {
                	addActionError("Nie mo�na usun�� centrum kosztowego z kt�rym powi�zane s� dokumenty");
                	return;
                }

				DataMartEventBuilder.get()
						.event(EventType.CENTRUM_KOSZTOW_DELETE)
						.objectId(centrum.getId());
                
                Persister.delete(centrum);
                
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public void setSymbol(String symbol)
    {
        this.symbol = symbol;
    }

    public List<CentrumKosztow> getCentra()
    {
        return centra;
    }

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Boolean getDefSmplAcc() {
		return defSmplAcc;
	}

	public void setDefSmplAcc(Boolean defSmplAcc) {
		this.defSmplAcc = defSmplAcc;
	}

	public void setCentrum(CentrumKosztow centrum) {
		this.centrum = centrum;
	}

	public CentrumKosztow getCentrum() {
		return centrum;
	}

	public void setModes(int[] modes) {
		this.modes = modes;
	}

	public int[] getModes() {
		return modes;
	}
	
	public IntercarsAcceptanceMode[] getIntercarsAcceptanceModes(){
		return IntercarsAcceptanceMode.values();
	}

	public void setAvailableModes(Set<Integer> availableModes) {
		this.availableModes = availableModes;
	}

	public Set<Integer> getAvailableModes() {
		return availableModes;
	}
     public boolean isAutoAdded() {
        return autoAdded;
    }

    public void setAutoAdded(boolean autoAdded) {
        this.autoAdded = autoAdded;
    }

    public Integer getDefaultAccountId() {
        return defaultAccountId;
    }

    public void setDefaultAccountId(Integer defaultAccountId) {
        this.defaultAccountId = defaultAccountId;
    }

    /**
	 * @return the acceptanceModeNames
	 */
	public ArrayList<String> getAcceptanceModeNames() {
		return acceptanceModeNames;
	}

	/**
	 * @param acceptanceModeNames the acceptanceModeNames to set
	 */
	public void setAcceptanceModeNames(ArrayList<String> acceptanceModeNames) {
		this.acceptanceModeNames = acceptanceModeNames;
	}

	/**
	 * @return the intercarsPowered
	 */
	public Boolean getIntercarsPowered() {
		return intercarsPowered;
	}

	/**
	 * @param intercarsPowered the intercarsPowered to set
	 */
	public void setIntercarsPowered(Boolean intercarsPowered) {
		this.intercarsPowered = intercarsPowered;
	}

    public List<AccountNumber> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountNumber> accounts) {
        this.accounts = accounts;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

	/**
	 * @return the defaultLocationId
	 */
	public Integer getDefaultLocationId() {
		return defaultLocationId;
	}

	/**
	 * @param defaultLocationId the defaultLocationId to set
	 */
	public void setDefaultLocationId(Integer defaultLocationId) {
		this.defaultLocationId = defaultLocationId;
	}

	/**
	 * @return the locations
	 */
	public List<LocationForIC> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<LocationForIC> locations) {
		this.locations = locations;
	}

    public boolean isContractNeeded() {
        return contractNeeded;
    }

    public void setContractNeeded(boolean contractNeeded) {
        this.contractNeeded = contractNeeded;
    }

    public String getSymbol2() {
        return symbol2;
    }

    public void setSymbol2(String symbol2) {
        this.symbol2 = symbol2;
    }

    public Map<Integer, String> getAcceptanceModeAmounts() {
        return acceptanceModeAmounts;
    }

    public void setAcceptanceModeAmounts(Map<Integer, String> acceptanceModeAmounts) {
        this.acceptanceModeAmounts = acceptanceModeAmounts;
    }
}
