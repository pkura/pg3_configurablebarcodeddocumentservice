package pl.compan.docusafe.web.admin;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.certificates.auth.DSHostPublicKey;
import pl.compan.docusafe.core.certificates.auth.DSPublicKey;
import pl.compan.docusafe.core.certificates.auth.PublicKeyManager;
import pl.compan.docusafe.core.certificates.auth.PublicKeyType;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.certificates.auth.PublicKeyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class HostAccessAction extends EventActionSupport {

    private static final Logger log = LoggerFactory.getLogger(HostAccessAction.class);

    DSHostPublicKey publicKey = new DSHostPublicKey();
    List<DSHostPublicKey> keys = new ArrayList();

    private Long id;
    private FormFile file;
    private String keyType;

    @Override
    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
                append(OpenHibernateSession.INSTANCE).
                append(new Update()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
                append(OpenHibernateSession.INSTANCE).
                append(new Delete()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                // DSApi.open();
                if (id != null && id > 0) {
                    publicKey = (DSHostPublicKey) PublicKeyManager.findById(id);
                    keyType = publicKey.getKeyType().name();
                }
                keys = PublicKeyManager.list();
            } catch (Exception e) {
                log.error("", e);
                event.addActionError(e.getMessage());
            } finally {
                //DSApi._close();
            }
        }
    }


    private class Update implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (StringUtils.isNotBlank(keyType)) {
                publicKey.setKeyType(keyType);
            }

            try {
                publicKey  = (DSHostPublicKey) DSApi.context().session().merge(publicKey);
                log.error("{}", publicKey.toString());
                DSApi.context().begin();

                String pkBase64 = null;
                if(file != null && file.getFile() != null && file.getFile().exists()) {
                    pkBase64 = pl.compan.docusafe.util.FileUtils.encodeByBase64(file.getFile());
                    publicKey.setPublicKey(pkBase64);
                }
                if(hostNameExist(publicKey.getHostName()) && publicKey.getId() == null){
                	throw new EdmException("Podany host ju� istnieje!");
                }

                DSApi.context().session().saveOrUpdate(publicKey);

                DSApi.context().commit();

                event.addActionMessage("Wpis zaktualizowano");
            } catch (Exception e) {
                log.error("", e);
                DSApi.context()._rollback();
                event.addActionError(e.getMessage());
            }

        }

		private boolean hostNameExist(String hostName) {
			
			List<DSHostPublicKey> results = PublicKeyManager.findOneByHostName(hostName);
			if(CollectionUtils.isEmpty(results)){
				return false;
			}
			
			return true;
		}
    }

    private class Delete implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            if (id != null && id > 0) {
                try {
                    log.error("{}", publicKey.toString());

                    DSApi.context().begin();
                    DSHostPublicKey pk = (DSHostPublicKey) PublicKeyManager.findById(id);
                    DSApi.context().session().delete(pk);
                    event.addActionMessage("Wpis usuni�to");
                    DSApi.context().commit();
                    publicKey = new DSHostPublicKey();
                    id = null;

                } catch (Exception e) {
                    log.error("", e);
                    DSApi.context()._rollback();
                    event.addActionError(e.getMessage());
                }
            }

        }
    }
    
    public DSHostPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(DSHostPublicKey publickKey) {
        this.publicKey = publickKey;
    }

    public List<DSHostPublicKey> getKeys() {
        return keys;
    }

    public void setKeys(List<DSHostPublicKey> keys) {
        this.keys = keys;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }

    public PublicKeyType[] getKeyTypes() {
        return PublicKeyType.values();
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }
}
