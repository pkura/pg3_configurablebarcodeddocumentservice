package pl.compan.docusafe.web.admin;

import pl.compan.docusafe.webwork.event.*;

/* User: Administrator, Date: 2005-06-14 12:25:07 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SystemFoldersAction.java,v 1.3 2006/02/08 14:54:20 lk Exp $
 */
public class SystemFoldersAction extends EventActionSupport
{
    private String trashPrettyPath;
    private Long trashId;
    private String officePrettyPath;
    private Long officeId;
    private String archivePrettyPath;
    private Long archiveId;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            //Folder trash = Folder.findSystemFolder(Folder.SN_TRASH);
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        }
    }

    public String getTrashPrettyPath()
    {
        return trashPrettyPath;
    }

    public void setTrashPrettyPath(String trashPrettyPath)
    {
        this.trashPrettyPath = trashPrettyPath;
    }

    public Long getTrashId()
    {
        return trashId;
    }

    public void setTrashId(Long trashId)
    {
        this.trashId = trashId;
    }

    public String getOfficePrettyPath()
    {
        return officePrettyPath;
    }

    public void setOfficePrettyPath(String officePrettyPath)
    {
        this.officePrettyPath = officePrettyPath;
    }

    public Long getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(Long officeId)
    {
        this.officeId = officeId;
    }

    public String getArchivePrettyPath()
    {
        return archivePrettyPath;
    }

    public void setArchivePrettyPath(String archivePrettyPath)
    {
        this.archivePrettyPath = archivePrettyPath;
    }

    public Long getArchiveId()
    {
        return archiveId;
    }

    public void setArchiveId(Long archiveId)
    {
        this.archiveId = archiveId;
    }
}
