package pl.compan.docusafe.web;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

@SuppressWarnings("serial")
public class UserInfoAction extends EventActionSupport {
	Logger LOG = LoggerFactory.getLogger(UserInfoAction.class);
	private List<Tab> tabs;
	private int tabId;
	private DSUser user;
	private String username;
	private boolean loggedIn;
	private String sites;
	private boolean edit;
	private String aboutMe;
	private boolean save;
	
	public final static int TAB_SUMMARY = 0;
	public final static int TAB_BILLING = 1;
	public final static int TAB_DOCUMENTS = 2;
	public final static int TAB_INDIVIDUAL = 3;
	public final static int TAB_ADDITIONAL = 4;
	public final static String ACTION_NAMES[] = {
		"/help/user-info.actionxxx", "/help/user-billing.actionxxx",
		"/help/user-docs.actionxxx", "/help/user-sites.actionxxx",
		"/help/user-additional-info.actionxxx"
	};

	@Override
	protected void setup() {
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new PrepareTabs())
			.append(new Summary())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}

	private class Summary extends LoggedActionListener {
		Logger kl = LoggerFactory.getLogger("kamil");

		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			
				user = DSUser.findByUsername(username);
				user.getDivisions();
				switch(tabId) {
					default:
					case TAB_SUMMARY:
						loggedIn = isLoggedIn(username);
						kl.debug("User: " + username + ", " + user.getName() + ", " + user.getId());
					break;
					
					case TAB_BILLING:
						
					break;
					
					case TAB_DOCUMENTS:
						
					break;
					
					case TAB_INDIVIDUAL:
						if(save)
							saveUserInfo(user);
						else
							readUserInfo(user);
						kl.debug("aboutMe = " + aboutMe);
						kl.debug("sites = " + sites);
						edit = username.equals(DSApi.context().getPrincipalName());
					break;
				}
		}

		@Override
		public Logger getLogger() {
			return LOG;
		}
		
		private boolean isLoggedIn(String username) {
			Logger kl = LoggerFactory.getLogger("kamil");
			boolean ret = false;
			
			for(Iterator iter = Docusafe.getSessions().iterator(); iter.hasNext();) {
				HttpSession session = (HttpSession) iter.next();
				Subject sub = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
				Set userPrincipals = sub.getPrincipals(UserPrincipal.class);
			
				if(userPrincipals.size() > 0) {
					UserPrincipal up = (UserPrincipal) userPrincipals.iterator().next();
					
					if(up != null) {
						kl.debug("username = " + username + " ?=? " + up.getName());
						if(up.getName().equals(username))
							return true;
					}
				}
			}
			
			return ret;
		}
		
		private void readUserInfo(DSUser user) {
			Long userId = user.getId();
			ResultSet rs;
			
			try {
				String sql = "select about_me, sites from ds_user_info where USER_ID=?";
				
				DSApi.context().begin();
				PreparedStatement ps = DSApi.context().prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();
				DSApi.context().commit();
				
				if(rs.next()) {
					aboutMe = rs.getString(1);
					sites = rs.getString(2);
				}
				
			} catch(Exception e) {
				LOG.error(e.getMessage());
			}
		}
		
		private void saveUserInfo(DSUser user) {
			Long userId = user.getId();
			kl.debug("B�dziem sejwowa�!");
			
			try {
				String sql = "update ds_user_info set about_me=?, sites=? where USER_ID=?";
				kl.debug(userId + " " + sites);
				DSApi.context().begin();
				PreparedStatement ps = DSApi.context().prepareStatement(sql);
				ps.setString(1, aboutMe);
				ps.setString(2, sites);
				ps.setLong(3, userId);
				ps.execute();
				DSApi.context().commit();
			} catch(Exception e) {
				LOG.error(e.getMessage());
			}
		}
	}
	
	private class PrepareTabs extends LoggedActionListener {
		final String[] tabNames = {"Podsumowanie", "Billingi", "Indywidualne strony", "Dane dodatkowe"};
		
		@Override
		public void actionPerformed(ActionEvent event, Logger log)
				throws Exception {
			tabs = new ArrayList<Tab>();
			
			for(int i = 0; i < tabNames.length; i ++) {
				boolean active = tabId == i ? true : false;
				String link = ACTION_NAMES[i];
				log.error("tabsytest: link=" + link + " ; active=" + active + " ; i=" + i + " ; tabId="+tabId);
				tabs.add(new Tab(tabNames[i], tabNames[i], link + "?tabId=" + i + "&username=" + username, active));
			}
		}
		@Override
		public Logger getLogger() {
			return LOG;
		}
		
	}
	
	public String getBaseLink() {
		return "/help/user-info.action";
	}
	
	public String getActionLink() {
		return ACTION_NAMES[tabId];
	}

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	public int getTabId() {
		return tabId;
	}

	public void setTabId(int tabId) {
		this.tabId = tabId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public DSUser getUser() {
		return user;
	}

	public void setUser(DSUser user) {
		this.user = user;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getSites() {
		return sites;
	}

	public void setSites(String sites) {
		this.sites = sites;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public void setSave(boolean save) {
		this.save = save;
	}

	public boolean isSave() {
		return save;
	}
}
