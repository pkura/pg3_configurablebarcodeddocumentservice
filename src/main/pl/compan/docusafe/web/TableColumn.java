package pl.compan.docusafe.web;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TableColumn.java,v 1.2 2008/02/21 14:58:35 mariuszk Exp $
 */
public class TableColumn
{
    private String property;
    private String title;
    private String sortDesc;
    private String sortAsc;
    private String columnName;

	public TableColumn(String property, String title, String sortDesc, String sortAsc,String columnName)
    {
        this.property = property;
        this.title = title;
        this.sortDesc = sortDesc;
        this.sortAsc = sortAsc;
        this.columnName = columnName;
    }
	
	public TableColumn(String property, String title, String sortDesc, String sortAsc)
    {
        this.property = property;
        this.title = title;
        this.sortDesc = sortDesc;
        this.sortAsc = sortAsc;
        this.columnName = "";
    }

    public JsonObject asJson() {
        JsonObject ret = new JsonObject();

        ret.addProperty("property", property);
        ret.addProperty("title", title);
        ret.addProperty("sortAsc", sortAsc);
        ret.addProperty("sortDesc", sortDesc);
        ret.addProperty("columnName", columnName);

        return ret;
    }

    public String getProperty()
    {
        return property;
    }

    public String getTitle()
    {
        return title;
    }

    public String getSortDesc()
    {
        return sortDesc;
    }

    public String getSortAsc()
    {
        return sortAsc;
    }
    
    public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
}
