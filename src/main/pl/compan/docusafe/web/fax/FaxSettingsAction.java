package pl.compan.docusafe.web.fax;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.webwork.event.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FaxSettingsAction.java,v 1.4 2006/02/20 15:42:38 lk Exp $
 */
public class FaxSettingsAction extends EventActionSupport
{
    // @EXPORT
    private List kinds;
    private List deliveries;
    private List users;
    private List allUsers;

    private Integer kindId;
    private Integer deliveryId;
    private String addUser;
    private String[] deleteUsers;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doAddUser").
            append(OpenHibernateSession.INSTANCE).
            append(new AddUser()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteUsers").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteUsers()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                // XXX:
                kindId = null; // new Integer(DSApi.context().getProperty(Properties.FAX_DOCUMENT_KIND_ID, "0"));
                deliveryId = null; // new Integer(DSApi.context().getProperty(Properties.FAX_DOCUMENT_DELIVERY_ID, "0"));

                String jsonUsernames = null; // DSApi.context().getProperty(Properties.FAX_ASSIGNMENT_USERNAMES, null);
                if (jsonUsernames != null)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(jsonUsernames);
                        users = new ArrayList(jsonArray.length());
                        for (int i=0; i < jsonArray.length(); i++)
                        {
                            try
                            {
                                users.add(DSUser.findByUsername(jsonArray.getString(i)));
                            }
                            catch (UserNotFoundException e)
                            {
                            }
                        }
                    }
                    catch (ParseException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                }

                kinds = InOfficeDocumentKind.list();
                deliveries = InOfficeDocumentDelivery.list();
                allUsers = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

/*
                if (kindId != null)
                {
                    InOfficeDocumentKind kind = InOfficeDocumentKind.find(kindId);
                    DSApi.context().setProperty(Properties.FAX_DOCUMENT_KIND_ID, kind.getId().toString());
                }

                if (deliveryId != null)
                {
                    InOfficeDocumentDelivery delivery = InOfficeDocumentDelivery.find(deliveryId);
                    DSApi.context().setProperty(Properties.FAX_DOCUMENT_DELIVERY_ID, delivery.getId().toString());
                }
*/

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class AddUser implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(addUser))
            {
                addActionError("Nie wybrano użytkownika z listy");
                return;
            }

            try
            {
                DSApi.context().begin();

                List usernames;

                String jsonUsernames = null; //DSApi.context().getProperty(Properties.FAX_ASSIGNMENT_USERNAMES, null);
                if (jsonUsernames != null)
                {

                    try
                    {
                        JSONArray jsonArray = new JSONArray(jsonUsernames);

                        usernames = new ArrayList(jsonArray.length());
                        for (int i=0; i < jsonArray.length(); i++)
                        {
                            usernames.add(jsonArray.getString(i));
                        }

                    }
                    catch (ParseException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                        usernames = new ArrayList(1);
                    }
                }
                else
                {
                    usernames = new ArrayList(1);
                }


                String name = DSUser.findByUsername(addUser).getName();
                if (!usernames.contains(name))
                    usernames.add(name);

                //DSApi.context().setProperty(Properties.FAX_ASSIGNMENT_USERNAMES, new JSONArray(usernames).toString());

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteUsers implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteUsers == null || deleteUsers.length == 0)
            {
                addActionError("Nie wybrano użytkowników do usunięcia z listy");
                return;
            }

            try
            {
                DSApi.context().begin();

                String jsonUsernames = null; //DSApi.context().getProperty(Properties.FAX_ASSIGNMENT_USERNAMES, null);
                if (jsonUsernames != null)
                {
                    List usernames;
                    try
                    {
                        JSONArray jsonArray = new JSONArray(jsonUsernames);

                        usernames = new ArrayList(jsonArray.length());
                        for (int i=0; i < jsonArray.length(); i++)
                        {
                            usernames.add(jsonArray.getString(i));
                        }

                        // usuwanie zaznaczonych użytkowników z listy
                        for (int i=0; i < deleteUsers.length; i++)
                        {
                            usernames.remove(deleteUsers[i]);
                        }

/*
                        if (usernames.size() > 0)
                        {
                            DSApi.context().setProperty(Properties.FAX_ASSIGNMENT_USERNAMES, new JSONArray(usernames).toString());
                        }
                        else
                        {
                            DSApi.context().setProperty(Properties.FAX_ASSIGNMENT_USERNAMES, null);
                        }
*/
                    }
                    catch (ParseException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                    }
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public Integer getKindId()
    {
        return kindId;
    }

    public void setKindId(Integer kindId)
    {
        this.kindId = kindId;
    }

    public Integer getDeliveryId()
    {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId)
    {
        this.deliveryId = deliveryId;
    }

    public List getKinds()
    {
        return kinds;
    }

    public List getDeliveries()
    {
        return deliveries;
    }

    public List getUsers()
    {
        return users;
    }

    public List getAllUsers()
    {
        return allUsers;
    }

    public void setAddUser(String addUser)
    {
        this.addUser = addUser;
    }

    public void setDeleteUsers(String[] deleteUsers)
    {
        this.deleteUsers = deleteUsers;
    }
}
