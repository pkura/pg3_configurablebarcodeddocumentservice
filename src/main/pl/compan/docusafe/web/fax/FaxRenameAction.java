package pl.compan.docusafe.web.fax;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import java.util.*;


public class FaxRenameAction extends EventActionSupport
{
	private Long id;
	private String title;
	private Date ctime;
	private String source;
	private BeanBackedMap bean;
	
	private RetainedObject fax;
	
	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
        append(OpenHibernateSession.INSTANCE).
        append(new Update()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
        
    }
	
	private class FillForm implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			try {

				fax = RetainedObject.find(id);
				title = fax.getTitle();
				source = fax.getSource();
				ctime = fax.getCtime();
				bean = new BeanBackedMap(fax);
				if (!fax.getAttachments().isEmpty()) {
					bean.put("contentLink", "/service/view-retained-attachment.action?id=" + fax.getAttachments().get(0).getId());
					bean.put("attach", fax.getAttachments().get(0));
				}

			} catch (EdmException e) {
				addActionError(e.getMessage());
			}
		}
	}
	
	private class Update implements ActionListener
	{
		 public void actionPerformed(ActionEvent event)
	        {
	           
	            try
	            {
	                DSApi.context().begin();
	                fax=RetainedObject.find(id);
	                fax.setTitle(title);
	                DSApi.context().commit();
	                event.setResult("view-staging-area");
	            }
	            catch (EdmException e)
	            {
	                DSApi.context().setRollbackOnly();
	                addActionError(e.getMessage());
	            }
	        }
		
		
		
	
	}
	
	public Long getId()
	{
		return id;
	}
	
	public void setId(Long i)
	{
		id=i;
	}
	
	public RetainedObject getFax()
	{
		return fax;
	}
	
	public void setFax(RetainedObject f)
	{
		fax=f;
	}
	
	public void setTitle(String s){
		title=s;
	}
	
	public String getTitle(){
		return title;
	}
			
	public Date getCtime(){
		return ctime;
	}
	
	public void setCtime(Date d){
		ctime=d;
	}
	
	public String getSource(){
		return source;
	}
	
	public void setSource(String s){
		source=s;
	}
	
	public BeanBackedMap getBean(){
		return bean;
	}
}
