package pl.compan.docusafe.web.fax;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.stagingarea.RetainedObject;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Akcja wy�wietlaj�ca list� plik�w umieszczonych w przechowalni
 * przez FaxService.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewStagingAreaAction.java,v 1.6 2008/10/06 11:30:38 pecet4 Exp $
 */
public class ViewStagingAreaAction extends EventActionSupport
{
    private List<Map> results;

    private String[] deleteIds;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                List<RetainedObject> retainedObjects = RetainedObject.findBySource("fax");
                if (retainedObjects.size() > 0)
                {
                    results = new ArrayList<Map>(retainedObjects.size());
                    for (RetainedObject object : retainedObjects)
                    {
                        BeanBackedMap bean = new BeanBackedMap(object, "id", "title", "ctime");
                        if (!object.getAttachments().isEmpty())
                        {
                            bean.put("contentLink", "/service/view-retained-attachment.action?id="+object.getAttachments().get(0).getId());
                            bean.put("attach", object.getAttachments().get(0));
                        }
                        bean.put("renameLink", "/fax/rename-fax.action?id="+object.getId());
                        results.add(bean);
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteIds != null)
            {
                try
                {
                    DSApi.context().begin();

                    for (String id : deleteIds)
                    {
                        RetainedObject.find(Long.parseLong(id)).delete();
                    }

                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
        }
    }

    public List<Map> getResults()
    {
        return results;
    }

    public void setDeleteIds(String[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }
}
