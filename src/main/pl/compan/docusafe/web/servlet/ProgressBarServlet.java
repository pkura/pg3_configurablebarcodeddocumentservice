package pl.compan.docusafe.web.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProgressBarServlet extends HttpServlet //implements Runnable
{

	private ServletContext context;
    private Thread timer;
    private String message;
    private int counter;
    private int taskCounter;
    private HashMap tasks;
   
   
    public  void doGet(HttpServletRequest request, HttpServletResponse  response)
        throws IOException, ServletException {
        doProcess(request, response);
    }

    public  void doPost(HttpServletRequest request, HttpServletResponse  response)
        throws IOException, ServletException {
        doProcess(request, response);
        
    }
    
    private void doProcess(HttpServletRequest request, HttpServletResponse response)
                   throws IOException, ServletException {
        
    	try
    	{
    		String taskId = request.getParameter("taskId");
    		
            //Thread.sleep(2000);
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write("<percentage>"+ (Integer)ProgressBarPool.getActivitiesInstance().get(taskId) +"</percentage>");
    	}
    	catch(Exception e)
    	{
    		
    	}
        
        
    }
    
    
}