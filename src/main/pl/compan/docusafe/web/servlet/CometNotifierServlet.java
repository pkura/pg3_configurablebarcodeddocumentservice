package pl.compan.docusafe.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.CometEvent;
import org.apache.catalina.CometProcessor;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.comet.CometMessage;
import pl.compan.docusafe.web.comet.CometMessage.MessageKind;
import pl.compan.docusafe.web.comet.CometMessageManager;

public class CometNotifierServlet extends HttpServlet implements
		CometProcessor {
	private static final long serialVersionUID = 1L;
	
	public final static String MESSAGE_KIND = "MESSAGE_KIND";
	public final static String AUTO_CLOSE = "AUTO_CLOSE";

	private final static Logger LOG = LoggerFactory.getLogger(CometNotifierServlet.class);
	private final static Logger kl = LoggerFactory.getLogger("kamil");
	
	private MessageSender messageSender = null;
	private static final Integer TIMEOUT = 60 * 1000;
	
	@Override
	public void destroy() {
		messageSender.stop();
		messageSender = null;
	}

	@Override
	public void init() throws ServletException {
		messageSender = new MessageSender();
		Thread messageSenderThread = new Thread(messageSender, "MessageSender[" + System.currentTimeMillis() + "]");
		messageSenderThread.setDaemon(true);
		messageSenderThread.start();
	}

	public void event(final CometEvent event) throws IOException,
			ServletException {
		HttpServletRequest request = event.getHttpServletRequest();
		HttpServletResponse response = event.getHttpServletResponse();
		
		HttpSession ses = request.getSession(false);
		kl.debug("CometEvent {} for {}, {}", event.getEventType().toString(), request.hashCode(), ses != null ? ses.getId() : "NULL");
		kl.debug("Response {}", response.hashCode());
		kl.debug("Request {}", request.hashCode());
		kl.debug("Session {}", ses != null ? ses.getId() : "NULL");
		CometConnection cometConnection = new CometConnection(response, request);
		if (event.getEventType() == CometEvent.EventType.BEGIN) {
			request.setAttribute("org.apache.tomcat.comet.timeout", TIMEOUT);
			kl.debug("BEGIN EVENT for {}", request.getHeader("User-Agent"));
//			messageSender.addCometConnection(cometConnection);
			synchronized(messageSender.cometConnections) {
				if(messageSender.cometConnections.remove(cometConnection)) {
					kl.debug("Removed comet connection", cometConnection.messageKind);
				}
				messageSender.cometConnections.add(cometConnection);
			}
			synchronized(messageSender.messages) {
				messageSender.messages.notifyAll();
			}
		} else if (event.getEventType() == CometEvent.EventType.ERROR) {
			kl.debug("ERROR EVENT for {}, SUBERROR EVENT {}", request.getHeader("User-Agent"), event.getEventSubType().toString());
			
			synchronized(messageSender.cometConnections) {
				messageSender.cometConnections.remove(cometConnection);
			}
			event.close();
		} else if (event.getEventType() == CometEvent.EventType.END) {
			kl.debug("END EVENT for {}", request.getHeader("User-Agent"));
			synchronized(messageSender.cometConnections) {
				messageSender.cometConnections.remove(cometConnection);
			}
			event.close();
		} else if (event.getEventType() == CometEvent.EventType.READ) {
		}

	}
	
	private class CometConnection {
		HttpServletResponse connection;
		String login;
		MessageKind messageKind;
		boolean autoClose = false;
		
		public CometConnection(HttpServletResponse connection, HttpServletRequest request) {
			this.connection = connection;
			try {
				this.login = AuthUtil.getUserPrincipal(request).getName();
			} catch(IllegalStateException e) {
				// gdy u�ytkownik jest wylogowany i tomcat pr�buje utworzy� sesj�
				LOG.error(e.getMessage(), e);
				this.login = null;
			}
			
			String messageKindParam = request.getParameter(MESSAGE_KIND);
			if(StringUtils.isEmpty(messageKindParam)) {
				if(kl.isDebugEnabled()) {
					kl.debug("Unknown message kind {}", messageKindParam != null ? messageKindParam : "NULL");
					kl.debug("Setting message kind to default {}", MessageKind.SIGNATURE_REQUEST.toString());
					messageKind = MessageKind.SIGNATURE_REQUEST;
				}
			} else {
				try {
					messageKind = MessageKind.valueOf(messageKindParam.trim().toUpperCase());
				} catch(IllegalArgumentException e) {
					kl.debug(e.getMessage(), e);
					kl.debug("Setting message kind to default {}", MessageKind.SIGNATURE_REQUEST.toString());
				}
			}
			
			String closeParam = request.getParameter(AUTO_CLOSE);
			if(!StringUtils.isEmpty(closeParam)) {
				autoClose = Boolean.parseBoolean(closeParam);
			}
			
			connection.setContentType("text/html;charset=ISO-8859-2");
			connection.setHeader("Pragma", "no-cache");
			connection.addHeader("Cache-Control", "must-revalidate");
			connection.addHeader("Cache-Control", "no-cache");
			connection.addHeader("Cache-Control", "no-store");
			connection.setDateHeader("Expires", 0);
			try {
				connection.flushBuffer();
			} catch (IOException e) {
				kl.error(e.getMessage(), e);
			}
			kl.debug("Response {} headers set up", request.getHeader("User-Agent"));
		}
		
		@Override
		public int hashCode() {
			return connection.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			CometConnection other = (CometConnection) obj;
			return this.connection.equals(other.connection);
		}
	}
	
	private class MessageSender implements Runnable {

		protected boolean running = true;
		private CometMessageManager manager = CometMessageManager.getInstance();
		private Map<String, List<CometMessage>> messages = CometMessageManager.getInstance().getAllCometMessages();
		Set<CometConnection> cometConnections = new HashSet<CometConnection>();

		public void stop() {
			running = false;
		}

		public void run() {
			while (running) {
				boolean waitForMessages = true;
				synchronized(cometConnections) {
					synchronized(messages) {
						for(CometConnection cometConnection : cometConnections) {
							if(!manager.getCometMessages(cometConnection.login, cometConnection.messageKind).isEmpty()) {
								waitForMessages = false;
								break;
							}
						}
					}
				}
				
				if(waitForMessages) {
					synchronized(messages) {
						try {
							messages.wait();
						} catch (InterruptedException e) {
							LOG.error(e.getMessage(), e);
						}
					}
				}
				
				//System.out.println(System.currentTimeMillis());	
					
				synchronized(cometConnections) {
					for(Iterator<CometConnection> connIter = cometConnections.iterator(); connIter.hasNext(); ) {
						CometConnection cometConnection = connIter.next();
						List<CometMessage> msgs = null;
						if(cometConnection.login != null) {
							StringBuilder allMessages = new StringBuilder();
							PrintWriter writer = null;
							try {
								writer = cometConnection.connection.getWriter();
							} catch(IOException e) {
								LOG.error(e.getMessage(), e);
							}
							boolean flushWriter = false;
							synchronized(messages) {
								msgs = messages.get(cometConnection.login);
								if(msgs != null) {
									for(Iterator<CometMessage> iter = msgs.iterator(); iter.hasNext(); ) {
										CometMessage msg = iter.next();
										if(msg.getMessageKind().equals(cometConnection.messageKind)) {
											//writer.println(msg.toJsonString());
											allMessages.append(msg.toJsonString()).append(System.getProperty("line.separator"));
											flushWriter = true;
											iter.remove();
										}
									}
								} else {
									kl.debug("Msg is NULL!");
								}
							}
							
							if(flushWriter) {
								try {
									kl.debug("Flushing writer {}", writer.hashCode());
//									System.out.println(allMessages.toString());
									writer.write(allMessages.toString());
									writer.flush();
									if(cometConnection.autoClose) {
										kl.debug("Closing writer {}", writer.hashCode());
										writer.close(); 
									}
								} catch(NullPointerException e) {
									// NullPointer mo�e wyskoczy�, gdy po��czenie zosta�o przerwane
									LOG.error(e.getMessage(), e);
								}
							}
						}
					}
				}
			}
		}
	}
}
