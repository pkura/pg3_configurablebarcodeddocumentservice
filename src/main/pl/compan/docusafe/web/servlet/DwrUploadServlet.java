package pl.compan.docusafe.web.servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.service.upload.UploadDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DwrUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 590156623656321656L;
    private static final Logger log = LoggerFactory.getLogger(DwrUploadServlet.class);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		DwrAttachmentStorage storage = DwrAttachmentStorage.getInstance();
		boolean multipart = ServletFileUpload.isMultipartContent(req);
		
		if(multipart) {
			DiskFileItemFactory itemFactory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(itemFactory);
			itemFactory.setRepository(UploadDriver.tempDir);
			try {
				List<FileItem> allItems = upload.parseRequest(req);
				List<DwrFileItem> fileItems = new ArrayList<DwrFileItem>();
				String fieldCn = null;
                String id = null;
				for(FileItem item : allItems) {
					if(item.isFormField()) {
						if(item.getFieldName().equals("fieldCn")) {
							fieldCn = item.getString();
						} else if(item.getFieldName().equals("id")) {
                            id = item.getString();
                        }
					} else {
						fileItems.add(new DwrFileItem(item, id));
					}
				}
				storage.addFiles(req.getSession(false), DwrUtils.getCnWithoutPrefix(fieldCn), fileItems);
			} catch (FileUploadException e) {
				log.error(e.getMessage(), e);
			}
			
		} else {
            String fileId = req.getParameter("id");
            String fieldCn = req.getParameter("fieldCn");
            storage.remove(req.getSession(false), DwrUtils.getCnWithoutPrefix(fieldCn), fileId);
		}
	}

	
}
