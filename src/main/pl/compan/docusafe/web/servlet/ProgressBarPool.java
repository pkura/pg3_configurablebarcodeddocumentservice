package pl.compan.docusafe.web.servlet;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ProgressBarPool {
	
	private static Map activities;
	
	
	/**
	 * metoda zwraca zsynchronizowana HashMap<String, Integer>
	 * @return
	 */
	public static Map getActivitiesInstance()
	{
		if(ProgressBarPool.activities==null)
			activities = Collections.synchronizedMap(new HashMap<String, Integer>());
		return activities;
	}

}
