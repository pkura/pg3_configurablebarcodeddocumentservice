package pl.compan.docusafe.web.news;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.*;

import java.text.ParseException;
import java.util.*;

public class NotificationAddAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(NotificationAddAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(NotificationAddAction.class.getPackage().getName(),null);
	
	private Long id;
	private Long documentId;
	private String documentLink;
	private String content;
	private String user;
	private String [] usersIds;
	private String [] divisionsGuids;
	private String [] groupGuids;
	private DSUser dsUser;
	private List<DSUser> users;
	private List<DSDivision> divisions;
	private List<DSDivision> groups;
	private Date notificationDate;
	private String stringDate;
	private Boolean hasPermission;
	
	@Override
	protected void setup() {	
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("addNotification").
	        append(OpenHibernateSession.INSTANCE).
	        append(new AddNotification()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSUser dsUser = DSApi.context().getDSUser();
        		if(documentId != null){
        			documentLink = buildLink();
        		}
        		if (DSApi.context().hasPermission(DSPermission.DEFINIWANIE_POWIADOMIEN_DO_UZYTKOWNIKOW)){
        			hasPermission = true;
        			users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
        			DSDivision[] divArray = DSDivision.getOnlyDivisions(false);
                    divisions = Arrays.asList(divArray);
                    Collections.sort(divisions, DSDivision.NAME_COMPARTOR);
        		} else {
        			users = new ArrayList<DSUser>();
            		users.add(dsUser);
        		}
        		user = dsUser.getName();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class AddNotification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		dsUser = DSApi.context().getDSUser();
        		if (DSApi.context().hasPermission(DSPermission.DEFINIWANIE_POWIADOMIEN_DO_UZYTKOWNIKOW)){
        			if (!(usersIds != null || divisionsGuids != null || groupGuids != null)) {
            			addActionError(sm.getString("AnyUserRequired"));
            			return;
            		} else if (stringDate == null || content==null) {
            			addActionError(sm.getString("DateAndUserAndContentRequired"));
            			return;
            		}
        			
        			DSApi.context().begin();
        			
	        		Set<Notification> notificationList = new HashSet<Notification>();
	        		Set<DSUser> userSet = new HashSet<DSUser>();
        			if(usersIds != null && usersIds.length > 0){
    	        		if(documentLink != null && documentLink.length() > 0){
    	        			//definicja powiadomienia
    	        			Notification notificationDef = new Notification(documentId, content, documentLink, dsUser, dsUser, NotificationsManager.DEFINITION_TYPE,
    	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, 0L);
    	        			notificationDef.create();
    	        			if(usersIds != null){
    	        				for(String id : usersIds){
        	        				DSUser usr = DSUser.findById(Long.valueOf(id));
        	        				if(!userSet.contains(usr)){
        	        					notificationList
            	        				.add(new Notification(documentId, content, documentLink, usr , dsUser, NotificationsManager.CHILD_TYPE,
            	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        					userSet.add(usr);
        	        				} 
        	        			}
    	        			}
    	        			
    	        			if(divisionsGuids != null){
    	        				for(String guid : divisionsGuids){
        	        				DSDivision div = DSDivision.find(guid);
        	        				DSUser[] usr =  div.getUsers();
        	        				for(DSUser u : usr){
        	        					if(!userSet.contains(u)){
        	        						notificationList
                	        				.add(new Notification(documentId, content, documentLink, u , dsUser, NotificationsManager.CHILD_TYPE,
                	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        						userSet.add(u);
        	        					} 
        	        					
        	        				}
        	        			}
    	        			}
    	        			
    	        			if(groupGuids != null){
    	        				for(String guid : groupGuids){
        	        				DSDivision div = DSDivision.find(guid);
        	        				DSUser[] usr =  div.getUsers();
        	        				for(DSUser u : usr){
        	        					if(!userSet.contains(u)){
        	        						notificationList
                	        				.add(new Notification(documentId, content, documentLink, u , dsUser, NotificationsManager.CHILD_TYPE,
                	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        						userSet.add(u);
        	        					} 
        	        				}
        	        			}
    	        			}
    	        			
    	        		} else {
    	        			//definicja powiadomienia
    	        			Notification notificationDef = new Notification(documentId, content, NotificationsManager.NOT_LIST_LINK, dsUser, dsUser, NotificationsManager.DEFINITION_TYPE,
    	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, 0L);
    	        			notificationDef.create();
    	        			if(usersIds != null){
    	        				for(String id : usersIds){
        	        				DSUser usr = DSUser.findById(Long.valueOf(id));
        	        				if(!userSet.contains(usr)){
        	        					notificationList
            	        				.add(new Notification(documentId, content, NotificationsManager.NOT_LIST_LINK, usr , dsUser, NotificationsManager.CHILD_TYPE,
            	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        					userSet.add(usr);
        	        				} 
        	        			}
    	        			}
    	        			
    	        			if(divisionsGuids != null){
    	        				for(String guid : divisionsGuids){
        	        				DSDivision div = DSDivision.find(guid);
        	        				DSUser[] usr =  div.getUsers();
        	        				for(DSUser u : usr){
        	        					if(!userSet.contains(u)){
        	        						notificationList
                	        				.add(new Notification(documentId, content, NotificationsManager.NOT_LIST_LINK, u , dsUser, NotificationsManager.CHILD_TYPE,
                	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        						userSet.add(u);
        	        					} 
        	        				}
        	        			}
    	        			}
    	        			
    	        			if(groupGuids != null){
    	        				for(String guid : groupGuids){
        	        				DSDivision div = DSDivision.find(guid);
        	        				DSUser[] usr =  div.getUsers();
        	        				for(DSUser u : usr){
        	        					if(!userSet.contains(u)){
        	        						notificationList
                	        				.add(new Notification(documentId, content, NotificationsManager.NOT_LIST_LINK, u , dsUser, NotificationsManager.CHILD_TYPE,
                	        						DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId()));
        	        						userSet.add(u);
        	        					} 
        	        				}
        	        			}
    	        			}
    	        			
    	        		}
    	        		
    	        		for(Notification not : notificationList){
    	        			not.create();
    	        		}
    	        		DSApi.context().commit();
    	        		addActionMessage(sm.getString("NotificationAddedwithNum") + ": " + notificationList.size());
        			}
            			
            			
        		} else {
	        		if (stringDate == null || content==null) {
	        			addActionError(sm.getString("DateAndUserAndContentRequired"));
	        			return;
	        		}
	        		
	        		DSApi.context().begin();
	        		Notification notification = null;
	        		Notification notificationDef = null;
	        		if((documentLink != null) && (documentLink.length() > 0)){
	        			notificationDef = new Notification(documentId, content, documentLink, dsUser, dsUser, NotificationsManager.DEFINITION_TYPE,
	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, 0L);
	        			notificationDef.create();
	        			notification = new Notification(documentId, content, documentLink, dsUser, dsUser, NotificationsManager.CHILD_TYPE,
	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId());
	        			notification.create();
	        			
	        		} else {
	        			notificationDef = new Notification(documentId, content, documentLink, dsUser, dsUser, NotificationsManager.DEFINITION_TYPE,
	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, 0L);
	        			notificationDef.create();
	        			notification = new Notification(null , content, NotificationsManager.TASKLIST_LINK, dsUser, dsUser, NotificationsManager.CHILD_TYPE,
	        					DateUtils.parseDateTimeAnyFormat(stringDate), true, false, notificationDef.getId());
	        			notification.create();
	        			
	        		}
	        		DSApi.context().commit();
	        		addActionMessage(sm.getString("NotificationAdded"));
        		}
        	} catch(EdmException e) {
        		log.error("",e);
        	} catch (ParseException e) {
				e.printStackTrace();
			} 
        }
    }

	/* Buduje link do dokumentu */
	private String buildLink(){
		StringBuilder linkBuilder = new StringBuilder();
		OfficeDocument document = null;
		try {
			if(documentId != null){
				document = OfficeDocument.find(documentId);
			}
			
			if(document != null){
				if(document instanceof OutOfficeDocument){
					if(((OutOfficeDocument)document).isInternal() == true){
						linkBuilder.append("/office/internal");
					} else {
						linkBuilder.append("/office/outgoing");
					}
				} else if(document instanceof InOfficeDocument){
					linkBuilder.append("/office/incoming");
				}
			}
			
			linkBuilder.append(NotificationsManager.DOCUMENT_LINK);
			linkBuilder.append("?documentId=" + documentId.toString());
		} catch (Exception e) {
			log.error(null, e);
		}
		return linkBuilder.toString();
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public DSUser getDsUser() {
		return dsUser;
	}

	public void setDsUser(DSUser dsUser) {
		this.dsUser = dsUser;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private static class LastnameComparator implements Comparator<DSUser>{
		public int compare(DSUser o1, DSUser o2) {
			return o1.getLastname().compareTo(o2.getLastname());
		}
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public List<DSDivision> getDivisions() {
		return divisions;
	}

	public void setDivisions(List<DSDivision> divisions) {
		this.divisions = divisions;
	}

	public List<DSDivision> getGroups() {
		return groups;
	}

	public void setGroups(List<DSDivision> groups) {
		this.groups = groups;
	}

    public String[] getUsersIds()
    {
        return usersIds;
    }

    public void setUsersIds(String[] usersIds)
    {
        this.usersIds = usersIds;
    }

    public String[] getDivisionsGuids()
    {
        return divisionsGuids;
    }

    public void setDivisionsGuids(String[] divisionsGuids)
    {
        this.divisionsGuids = divisionsGuids;
    }

    public String[] getGroupGuids()
    {
        return groupGuids;
    }

    public void setGroupGuids(String[] groupGuids)
    {
        this.groupGuids = groupGuids;
    }

    public Boolean getHasPermission() {
		return hasPermission;
	}

	public void setHasPermission(Boolean hasPermission) {
		this.hasPermission = hasPermission;
	}

	public String getDocumentLink() {
		return documentLink;
	}

	public void setDocumentLink(String documentLink) {
		this.documentLink = documentLink;
	}

}
