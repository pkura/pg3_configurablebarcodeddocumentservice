package pl.compan.docusafe.web.news;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;

import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;

import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.news.News;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ShowNewsAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ShowNewsAction.class);
	private static final String LINK = "/news/show-news.action";
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(ShowNewsAction.class.getPackage().getName(),null);
	// aktualno�ci
	private Long id;
	private List<Tab> tabs = new ArrayList<Tab>();
	private SearchResults<? extends News> newsList;
	private Long newsType;
	private NewsType actualType;
	// powiadomienia
	private Long notificationId;
	private List<Notification> notifications;
	private boolean przekieruj;
	private String redirectUrl;
	private int offset;
	private int limit = 3;
	private static final String EV_FILL = "fillForm";
	private static final String ACCEPT = "accept";
	private Pager pager;

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	@Override
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DeleteNews()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAccept").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(ACCEPT, new AcceptNotification()).
	    	append(EV_FILL, fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
	}
	

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		// ustalenie typu zak�adki, je�li nie zaznaczono �adnej domy�lna z powiadomieniami
        		if(newsType != null) 
        			actualType = NewsType.find(newsType);
        		
        		// przygotowanie zak�adek (tylko tych aktywnych)
        		List<NewsType> types = NewsType.findAllActive();
        		for(NewsType t : types) {
        			// je�eli nie wybrano zak�adki wybranie pierwszej z brzegu
        			if (actualType==null) actualType=t; 
        			//zaznaczenie id aktualnie wskazanej
        			boolean selected = (t.getId()==actualType.getId())?true:false;
        			Tab tab = new Tab(t.getName(), t.getName(), LINK+"?newsType="+t.getId(), selected);
        			tabs.add(tab);
        		}
        		
    			// wybranie wszystkich post�w z danego typu
    			newsList = News.search(offset, limit, actualType);
                Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return HttpUtils.makeUrl(LINK, new Object[] 
                            {
                                "newsType", newsType,
                                "offset", String.valueOf(offset)
                            }
                        );
                    }
                };
    			pager = new Pager(linkVisitor, offset, limit, newsList.totalCount(), 3);
    			
    			//dodanie brakujacych 
    			DSApi.context().begin();
    			NotificationsManager.addTasklistNotification(DSApi.context().getPrincipalName());
    			DSApi.context().commit();
    			// wy�wietlenie powiadomie�
    			notifications = Notification.findByUser(DSApi.context().getDSUser());
    			
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class DeleteNews implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		// zaznaczenie wiadomo�ci jako usuni�tej
        		News news = News.find(id);
        		news.setDeleted(true);
        		DataMartEvent dme = new DataMartEvent(false,null, null, DataMartDefs.NEWS_DELETED, "ID", ""+id, null, null);
                DataMartManager.storeEvent(dme);
        		DSApi.context().commit();
        		addActionMessage(sm.getString("DeleteSuccess"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class AcceptNotification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		// zaznaczenie wiadomo�ci jako usuni�tej
        		Notification not = Notification.find(notificationId);
        		redirectUrl = not.getLink();
        		not.delete();
        		
        		DSApi.context().commit();
        		
            	if (przekieruj) {
            		event.skip(EV_FILL);
        			event.setResult(REDIRECT);
            	}
        		
        	}
        	catch(Exception e)
        	{	
        		log.error("",e);
        		DSApi.context()._rollback();
        	}

        }
    }

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setNewsType(Long newsType) {
		this.newsType = newsType;
	}

	public Long getNewsType() {
		return newsType;
	}

	public NewsType getActualType() {
		return actualType;
	}

	public void setActualType(NewsType actualType) {
		this.actualType = actualType;
	}

	public SearchResults<? extends News> getNewsList() {
		return newsList;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public boolean isPrzekieruj() {
		return przekieruj;
	}

	public void setPrzekieruj(boolean przekieruj) {
		this.przekieruj = przekieruj;
	}

	public static String getLINK() {
		return LINK;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public void setNewsList(SearchResults<? extends News> newsList) {
		this.newsList = newsList;
	}

}
