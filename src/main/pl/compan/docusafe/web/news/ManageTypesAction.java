package pl.compan.docusafe.web.news;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ManageTypesAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ManageTypesAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(ManageTypesAction.class.getPackage().getName(),null);
	
	private List<NewsType> types;
	private String newType;
	// liczba post�w na stronie
	private int postNumber;
    private List<Integer> postNumbers;

	@Override
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doAdd").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Add()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doChange").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Change()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		types = NewsType.findAll();
                //lista mozliwych ilosci post�w w aktualno�ciach
                // w pliku adds.properties nale�y doda� mo�liwe ilo�ci :np: postNumbers=10,20,30,40,50
	            Docusafe.getAdditionProperty("postNumbers");
	            postNumbers = new ArrayList<Integer>();
	            for(String count : Docusafe.getAdditionProperty("postNumbers").split(","))
	            {
	            	postNumbers.add(Integer.parseInt(count));
	            }
	            postNumber = GlobalPreferences.getPostNumber();

        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Add implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (newType == null || newType.equalsIgnoreCase("")) {
        			addActionError(sm.getString("NameRequired"));
        			return;
        		}
        		
        		NewsType nt = new NewsType(newType);
        		nt.create();
        		addActionMessage(sm.getString("AddSuccess"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Change implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		GlobalPreferences.setPostNumber(postNumber);
        		DSApi.context().commit();
        		addActionMessage(sm.getString("EditPostNumberSuccess"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	public void setTypes(List<NewsType> types) {
		this.types = types;
	}

	public List<NewsType> getTypes() {
		return types;
	}

	public void setNewType(String newType) {
		this.newType = newType;
	}

	public String getNewType() {
		return newType;
	}

	public int getPostNumber() {
		return postNumber;
	}

	public void setPostNumber(int postNumber) {
		this.postNumber = postNumber;
	}

	public List<Integer> getPostNumbers() {
		return postNumbers;
	}

	public void setPostNumbers(List<Integer> postNumbers) {
		this.postNumbers = postNumbers;
	}

}
