package pl.compan.docusafe.web.news;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.news.News;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class EditNewsAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AddNewsAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(AddNewsAction.class.getPackage().getName(),null);
	
	private Long id;
	private Map<Long,String> types;
	private Long selectedType;
	private String subject;
	private String description;
	
	@Override
	protected void setup() {	
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSave").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Save()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		List<NewsType> newsTypes = NewsType.findAllActive();
        		types = new HashMap<Long, String>();
        		for (NewsType t : newsTypes) {
        			types.put(t.getId(), t.getName());
        		}
        		News news = News.find(id);
        		selectedType = news.getNewsType().getId();
        		subject = news.getSubject();
        		description = news.getDescription();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Save implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (subject == null || description==null) {
        			addActionError(sm.getString("SubjectAndDescriptionRequired"));
        			return;
        		}
        		System.out.println("mariusz");
        		DSApi.context().begin();
        		// aktualizacja aktualności
        		News news = News.find(id);
        		news.setSubject(subject);
        		news.setDescription(description);
        		news.setNewsType(NewsType.find(selectedType));
        		DataMartEvent dme = new DataMartEvent(false,null, null, DataMartDefs.NEWS_CHANGE, ""+id, null, null, null);
                DataMartManager.storeEvent(dme);
        		DSApi.context().commit();
        		addActionMessage(sm.getString("NewsEdited"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }

	public Map<Long, String> getTypes() {
		return types;
	}

	public void setTypes(Map<Long, String> types) {
		this.types = types;
	}

	public Long getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(Long selectedType) {
		this.selectedType = selectedType;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

}
