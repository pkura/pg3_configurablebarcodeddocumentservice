package pl.compan.docusafe.web.news;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.news.News;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NotificationAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(NotificationAction.class);
	private static final String LINK = "/news/notification.action";
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(NotificationAction.class.getPackage().getName(),null);
	
	// aktualności
	private Long id;
	private List<Tab> tabs = new ArrayList<Tab>();
	
	private Long notificationId;
	// wszystkie powiadomienia użytkownika
	private static List<Notification> notifications;
	// nowe powiadomienia uzytkownika
	private static List<Notification> newNotifications;
	// wlasne powiadomienia uzytkownika
	private static List<Notification> ownNotifications;
	// otrzymane powiadomienia uzytkownika
	private static List<Notification> recivedNotifications;
	// aktualne powiadomienia (ktore przekroczyly czas notificationDate)
	private static List<Notification> actualNotifications;
	
	private String sortField;
    boolean ascending;
	private boolean przekieruj;
	private String redirectUrl;
	private int offset;
	private int limit = 3;
	private static final String EV_FILL = "fillForm";
	private static final String ACCEPT = "accept";
	private static final String READ = "read";
	private static final String REDIRECT = "redirect";
	
	/**
	 * Numer strony
	 */
	private int pageNumber = 0;
	
	/**
	 * Liczba rezultatów
	 */
	private int maxResults = 5;
	
	/**
	 * Obiekt pager'a
	 */
	private Pager pager;

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	@Override
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDelete").
        	append(OpenHibernateSession.INSTANCE).
        	append(new Delete()).
        	append(fillForm).
        	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doDeleteChild").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new DeleteChild()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doRead").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new ReadNotification()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doReadNew").
	    	append(OpenHibernateSession.INSTANCE).
	    	append(new ReadNewNotification()).
	    	append(fillForm).
	    	appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		Tab tab = new Tab("Lista powiadomień", "Lista powiadomień");
        		tabs.add(tab);
        		// wyświetlenie powiadomień
        		DSUser user = DSApi.context().getDSUser();
        		// lista własnych powiadomień
        		ownNotifications = Notification.findOwnNotificationByUser(user, sortField, ascending);
        		recivedNotifications = Notification.findActualNotificationByUser(user, sortField, ascending);
    			//actualNotifications = Notification.findActualNotificationByUser(user);
    			//newNotifications = Notification.findNewNotificationByUser(user);
    			notifications = Notification.findByUser(user);
    			//createPager();
    			
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private void createPager() throws EdmException
	{
		int count = notifications.size();
		pager = new Pager(new Pager.LinkVisitor() {
			public String getLink(int offset) 
			{
				return offset + "";
			}
		}, pageNumber, maxResults, count, 5, true);
	}
	
	private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		Notification notificationDef = Notification.find(id);
        		List<Notification> notifications = Notification.findChildNotifications(notificationDef.getId());
        		if(notifications.size() > 0){
        			for(Notification not : notifications){
        				not.delete();
        			}
        		}
        		notificationDef.delete();
        		DSApi.context().commit();
        		addActionMessage(sm.getString("DeleteSuccess"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class DeleteChild implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		Notification notification = Notification.find(id);
        		notification.update(NotificationsManager.DELETED);
        		DSApi.context().commit();
        		addActionMessage(sm.getString("DeleteSuccess"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class ReadNotification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		// oznaczenie wiadomości jako przeczytanej
        		Notification not = Notification.find(notificationId);
        		not.setRead(true);
        		not.update(not.getNotificationDate(), not.getContent(), not.isRead(), not.isNewNotification(), NotificationsManager.CHILD_TYPE);
        		DSApi.context().commit();
        	}
        	catch(Exception e)
        	{	
        		log.error("",e);
        		DSApi.context()._rollback();
        	}
        }
    }
	
	private class ReadNewNotification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		DSApi.context().begin();
        		// oznaczenie wiadomości jako przeczytanej
        		Notification not = Notification.find(notificationId);
        		not.setNewNotification(false);
        		not.update(not.getNotificationDate(), not.getContent(), not.isRead(), not.isNewNotification(), NotificationsManager.CHILD_TYPE);
        		DSApi.context().commit();
        	}
        	catch(Exception e)
        	{	
        		log.error("",e);
        		DSApi.context()._rollback();
        	}

        }
    }

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public boolean isPrzekieruj() {
		return przekieruj;
	}

	public void setPrzekieruj(boolean przekieruj) {
		this.przekieruj = przekieruj;
	}

	public static String getLink() {
		return LINK;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	

	public static List<Notification> getNewNotifications() {
		return newNotifications;
	}

	public static void setNewNotifications(List<Notification> newNotifications) {
		NotificationAction.newNotifications = newNotifications;
	}

	public static List<Notification> getActualNotifications() {
		return actualNotifications;
	}

	public static void setActualNotifications(List<Notification> actualNotifications) {
		NotificationAction.actualNotifications = actualNotifications;
	}

	public static List<Notification> getOwnNotifications() {
		return ownNotifications;
	}

	public static void setOwnNotifications(List<Notification> ownNotifications) {
		NotificationAction.ownNotifications = ownNotifications;
	}

	public static List<Notification> getRecivedNotifications() {
		return recivedNotifications;
	}

	public static void setRecivedNotifications(
			List<Notification> recivedNotifications) {
		NotificationAction.recivedNotifications = recivedNotifications;
	}

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }
}
