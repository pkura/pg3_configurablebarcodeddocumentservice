package pl.compan.docusafe.web.news;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ManageTypesEditAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ManageTypesEditAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(ManageTypesEditAction.class.getPackage().getName(),null);
	
	private Long id;
	private NewsType newsType;
	private String name;
	private boolean disableTab;

	@Override
	protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);
	}

	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (id != null) {
        			newsType = NewsType.find(id);
        			name = newsType.getName();
        			disableTab = newsType.isDisabled();
        		}
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }
	
	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (name==null || name.equalsIgnoreCase("")) {
        			addActionError(sm.getString("NameRequired"));
        			return;
        		}
        		DSApi.context().begin();
        		
        		if (id != null) {
        			newsType = NewsType.find(id);
        			newsType.setName(name);
        			newsType.setDisabled(disableTab);
        		}
        		addActionMessage(sm.getString("EditSuccess"));
        		
        		DSApi.context().commit();
        		DSApi.context().session().flush();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        		DSApi.context()._rollback();
        	}
        }
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NewsType getNewsType() {
		return newsType;
	}

	public String getName() {
		return name;
	}

	public void setNewsType(NewsType newsType) {
		this.newsType = newsType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDisableTab() {
		return disableTab;
	}

	public void setDisableTab(boolean disableTab) {
		this.disableTab = disableTab;
	}

}
