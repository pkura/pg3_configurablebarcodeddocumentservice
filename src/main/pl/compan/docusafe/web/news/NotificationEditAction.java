package pl.compan.docusafe.web.news;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.news.News;
import pl.compan.docusafe.core.news.NewsType;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class NotificationEditAction extends EventActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(NotificationEditAction.class);
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(NotificationEditAction.class.getPackage().getName(),null);
	
	private Long id;
	private Long documentId;
    private String documentLink;
	private Map<Long,String> types;
	private String content;
	private String name;
	private String user;
	private DSUser dsUser;
	private List<DSUser> users;
	private Date notificationDate;
    private String stringDate;
	private boolean cantEdit;
	@Override
	protected void setup() {	
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doUpdate").
	        append(OpenHibernateSession.INSTANCE).
	        append(new Update()).
	        append(fillForm).
	        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReadAndEdit").
                append(OpenHibernateSession.INSTANCE).
                append(new ReadNotification()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        
	}
	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
                if(documentId != null){
                    documentLink = buildLink();
                }
        		Notification notification = Notification.find(id);
        		//users = DSUser.list(0);	
        		users = new ArrayList<DSUser>();
        		users.add(notification.getUser());
        		dsUser = notification.getUser();
        		user = dsUser.getName();
        		notificationDate = notification.getNotificationDate();
        		documentId = notification.getDocumentId();
        		content = notification.getContent();
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
        }
    }

    /* Buduje link do dokumentu */
    private String buildLink(){
        StringBuilder linkBuilder = new StringBuilder();
        OfficeDocument document = null;
        try {
            if(documentId != null){
                document = OfficeDocument.find(documentId);
            }

            if(document != null){
                if(document instanceof OutOfficeDocument){
                    if(((OutOfficeDocument)document).isInternal() == true){
                        linkBuilder.append("/office/internal");
                    } else {
                        linkBuilder.append("/office/outgoing");
                    }
                } else if(document instanceof InOfficeDocument){
                    linkBuilder.append("/office/incoming");
                }
            }

            linkBuilder.append(NotificationsManager.DOCUMENT_LINK);
            linkBuilder.append("?documentId=" + documentId.toString());
        } catch (Exception e) {
            log.error(null, e);
        }
        return linkBuilder.toString();
    }

	private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	try
        	{
        		if (stringDate == null || content==null) {
        			addActionError(sm.getString("DateAndUserAndContentRequired"));
        			return;
        		}
        		Notification notificationDef = Notification.find(id);
        		List<Notification> notifications = Notification.findChildNotifications(notificationDef.getId());
        		if(notifications.size() > 0){
        			for(Notification not : notifications){
        				not.update(DateUtils.parseDateTimeAnyFormat(stringDate), content, false, true, NotificationsManager.CHILD_TYPE);
        			}
        		}
        		notificationDef.update(DateUtils.parseDateTimeAnyFormat(stringDate), content, false, true, NotificationsManager.DEFINITION_TYPE);
        		addActionMessage(sm.getString("NotificationUpdated"));
        	}
        	catch(EdmException e)
        	{
        		log.error("",e);
        	}
            catch (ParseException e)
            {
                log.error("", e);
            }
        }
    }

    private class ReadNotification implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();
                // oznaczenie wiadomości jako przeczytanej
                Notification not = Notification.find(id);
                not.setRead(true);
                not.update(not.getNotificationDate(), not.getContent(), true, false, NotificationsManager.CHILD_TYPE);
                DSApi.context().commit();
            }
            catch(Exception e)
            {
                log.error("",e);
                DSApi.context()._rollback();
            }
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<Long, String> getTypes() {
		return types;
	}

	public void setTypes(Map<Long, String> types) {
		this.types = types;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public DSUser getDsUser() {
		return dsUser;
	}

	public void setDsUser(DSUser dsUser) {
		this.dsUser = dsUser;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

    public boolean isCantEdit()
    {
        return cantEdit;
    }

    public void setCantEdit(boolean cantEdit)
    {
        this.cantEdit = cantEdit;
    }

    public String getStringDate()
    {
        return stringDate;
    }

    public void setStringDate(String stringDate)
    {
        this.stringDate = stringDate;
    }

    public String getDocumentLink()
    {
        return documentLink;
    }

    public void setDocumentLink(String documentLink)
    {
        this.documentLink = documentLink;
    }
}
