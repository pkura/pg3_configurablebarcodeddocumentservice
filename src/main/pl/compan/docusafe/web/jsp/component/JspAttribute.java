package pl.compan.docusafe.web.jsp.component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspAttribute {
    private static final boolean DEFAULT_EDITABLE = true;
    protected String jspTitle;
    protected String jspFieldName;
    protected Object jspValue;
    protected JspFieldType jspType;
    protected String jspCssClass;
    protected boolean editable;

    /**
     * @param value it's value of filed or SelectField
     */
    private JspAttribute(String fieldName, String title, Object value, boolean editable) {
        this.jspFieldName = fieldName;
        this.jspTitle = title;
        this.jspValue = value;
        this.jspType = getType(value);
        this.editable = editable;
        this.jspCssClass = "";
    }

    public JspFieldType getType(Object fieldValue) {
        return fieldValue != null && fieldValue instanceof SelectField ? JspFieldType.SELECT : JspFieldType.TEXT;
    }

    public String getJspTitle() {
        return jspTitle;
    }

    public void setJspTitle(String jspTitle) {
        this.jspTitle = jspTitle;
    }

    public String getJspFieldName() {
        return jspFieldName;
    }

    public void setJspFieldName(String jspFieldName) {
        this.jspFieldName = jspFieldName;
    }

    public Object getJspValue() {
        return jspValue;
    }

    public void setJspValue(Object jspValue) {
        this.jspValue = jspValue;
    }

    public JspFieldType getJspType() {
        return jspType;
    }

    public void setJspType(JspFieldType jspType) {
        this.jspType = jspType;
    }

    public String getJspCssClass() {
        return jspCssClass;
    }

    public void setJspCssClass(String jspCssClass) {
        this.jspCssClass = jspCssClass;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String)
            return jspFieldName.equals((String) obj);
        return super.equals(obj);
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean getEditable() {
        return editable;
    }

    public JspAttribute setEditable(boolean editable) {
        this.editable = editable;
        return this;
    }


    public static class Factory {

        /**
         * Tworzy edytowalne pole typu TEXT
         */
//        public static JspAttribute createField( String fieldName ){
//            return new JspAttribute(fieldName, null, null);
//        }

        /**
         * Tworzy edytowalne pole typu TEXT
         */
        public static JspAttribute createTextField(String fieldName, String title, boolean editable) {
            return new JspAttribute(fieldName, title, null, editable);
        }

        /**
         * Tworzy edytowalne pole typu SELECT
         */
        public static JspAttribute createSelectField(String fieldName, String title, boolean editable, JspAttribute.SelectField selectField) {
            return new JspAttribute(fieldName, title, selectField, editable);
        }

        /**
         * Tworzy nieedytowalny jedynie wy�wietlany jak tekst atrybut
         */
        public static JspAttribute createAttr(String title, Object value) {
            return new JspAttribute(null, title, value, false);
        }

        /**
         * Tworzy nieedytowalny jedynie wy�wietlany jak pole atrybut
         */
        public static JspAttribute createAttrAsField(String title, Object value) {
            return new JspAttribute(null, title, value, true);
        }

        public static JspAttribute createTextFilter(String fieldName) {
            return new JspAttribute(fieldName, null, null, true);
        }

        public static JspAttribute createSelectFilter(String fieldName, SelectField allKindsFieldSelect) {
            return new JspAttribute(fieldName, null, allKindsFieldSelect, true);
        }
    }

    /**
     * elementy nie mog�/nie powinny zawiera� kluczy null
     */
    public static class SelectField {

        public static final String DEFAULT_SELECTED_ID = "";
        private List<SelectItem> jspItems;
        private String jspSelectedId;
        private String jspDefaultSelectedId = null;

        /**
         * Ustawia podane id - je�eli istnieje taki element
         */
        public SelectField(List<SelectItem> items, String selectedId) {
            this(items);
            selectById(selectedId);
        }

        /**
         * Ustawia podane id - je�eli istnieje taki element
         */
        public SelectField selectById(String id) {
            SelectItem selectItem = getItemById(id);
            if (selectItem != null)
                jspSelectedId = selectItem.getJspId();
            return this;
        }

        /**
         * id por�wnywane przez equals
         */
        public SelectItem getItemById(String id) {
            for (SelectItem item : jspItems)
                if (item.getJspId().equals(id))
                    return item;
            return null;
        }

        public SelectField(List<SelectItem> items) {
            this.jspItems = items;
            jspSelectedId = (items.size() > 0) ? items.get(0).getJspId() : null;
        }

        public static SelectField createBooleanSelectField(String falseTitle, String trueTitle, String defaultTitle) {
            List<SelectItem> items = new ArrayList<SelectItem>();
            items.add(new SelectItem("false", falseTitle));
            items.add(new SelectItem("true", trueTitle));
            return (new SelectField(addDefaultItem(defaultTitle, items)));
        }

        public static List<SelectItem> addDefaultItem(String defaultItemTitle, List<SelectItem> items) {
            items.add(0, new SelectItem(DEFAULT_SELECTED_ID, defaultItemTitle));
            return items;
        }

        /**
         * elementowi z domy�lnym tutu�em przypisany zostanie domy�lne id
         */
        public static List<SelectItem> createSelectItems(String defaultItemTitle, List<String> itemTitles) {
            List<SelectItem> items = createSelectItems(itemTitles);
            return addDefaultItem(defaultItemTitle, items);
        }

        public static List<SelectItem> createSelectItems(List<String> itemTitles) {
            List<SelectItem> items = new ArrayList<SelectItem>();
            for (String title : itemTitles)
                items.add(new SelectItem(Integer.toString(items.size()), title));
            return items;
        }

        /**
         * Ustawia id na podstawie tytulu - je�eli istnieje taki element
         */
        public SelectField selectByTitle(String title) {
            SelectItem selectItem = getItemByTitle(title);
            if (selectItem != null)
                jspSelectedId = selectItem.getJspId();
            return this;
        }

        /**
         * title por�wnywane przez equalsIgnoreCase
         */
        public SelectItem getItemByTitle(String title) {
            for (SelectItem item : jspItems)
                if (item.getJspTitle().equalsIgnoreCase(title))
                    return item;
            return null;
        }

        /**
         * Ustawia domy�lne id dla domy�lnego elementu - je�eli istnieje elemnt o domy�lnym id istnieje
         */
        public SelectField setDefaultSelectedId() {
            return setDefaultSelectedId(DEFAULT_SELECTED_ID);
        }

        public List<SelectItem> getJspItems() {
            return jspItems;
        }

        public void setJspItems(List<SelectItem> jspItems) {
            this.jspItems = jspItems;
        }

        /**
         * Dzia�a pod warunkiem, �e ustawiona jest warto�� domy�lnego id
         */
        public boolean isDefaultSelectedId() {
            return isDefaultId(jspSelectedId);
        }

        /**
         * Dzia�a pod warunkiem, �e ustawiona jest warto�� domy�lnego id
         */
        public boolean isDefaultId(String id) {
            return DEFAULT_SELECTED_ID.equalsIgnoreCase(id);
        }

        /**
         * Ustawia id domy�lnego elementu - je�eli elemnt o podanym id istnieje
         */
        public SelectField setDefaultSelectedId(String jspDefaultSelectedId) {
            SelectItem selectItem = getItemById(jspDefaultSelectedId);
            if (selectItem != null)
                this.jspDefaultSelectedId = jspDefaultSelectedId;
            return this;
        }

        public SelectItem getSelectedItem() {
            return getItemById(jspSelectedId);
//            for (SelectItem item : jspItems)
//                if ((jspSelectedId!=null && jspSelectedId.equals(item.getJspId())) || item.getJspId() == null)
//                    return item;
//            return null;
        }

        public void setJspSelectedId(String jspSelectedId) {
            this.jspSelectedId = jspSelectedId;
        }
    }

    public static class SelectItem {
        private String jspId;
        private String jspTitle;

        public SelectItem(String id, String title) {
            this.jspId = id;
            this.jspTitle = title;
        }

        public String getJspId() {
            return jspId;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof String)
                if (jspTitle.equalsIgnoreCase((String) obj))
                    return true;
            return super.equals(obj);
        }

        public void setJspId(String jspId) {
            this.jspId = jspId;
        }

        public String getJspTitle() {
            return jspTitle;
        }

        public void setJspTitle(String jspTitle) {
            this.jspTitle = jspTitle;
        }
    }

    public void addCssClass(CssClass newCss) {
        addCssClass(newCss.css);
    }

    public void addCssClass(String newCss) {
        Pattern pattern = Pattern.compile("^((.*\\s)|)" + newCss + "((\\s.*)|)$");
        if (!pattern.matcher(jspCssClass).find()) {
            jspCssClass += " " + newCss;
            jspCssClass = jspCssClass.trim();
        }
    }

    public void removeCssClass(String removingCss) {
        jspCssClass = jspCssClass.replaceAll("((^\\s*)|(\\s))" + removingCss + "((\\s*$)|(\\s))", " ");
        jspCssClass = jspCssClass.trim();
    }

    public void clearCssClass() {
        jspCssClass = "";
    }

    public enum CssClass {
        REQUIRED("required"),
        INVALID("invalid");

        public final String css;

        CssClass(String css) {
            this.css = css;
        }
    }
}
