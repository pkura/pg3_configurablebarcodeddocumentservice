package pl.compan.docusafe.web.jsp.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspComponentCheckboxesStringId {

    private boolean isSet = false;

    private long lastMaxId = 0;

    private String jspTitle;
    private String[] jspSelectedIds = new String[0];
    private List<Checkbox> jspData = new ArrayList<Checkbox>();

    public JspComponentCheckboxesStringId(String title) {
        this.jspTitle = title;
    }

    /**
     * @param id Jeżeli null - automatycznie przyporządkowywany id
     * @param title
     * @param checked Jeżeli null - nie powoduje zmiany stanu checkboxa.
     * @param code niewymagany
     * @return
     */
    public JspComponentCheckboxesStringId add(String id, String title, Boolean checked, Object code) {
        List<String> selectedList = new ArrayList<String>(Arrays.asList(jspSelectedIds));

        String cbId = id;
        if (cbId == null){
            do{
                cbId = Long.toString(++lastMaxId);
            } while (selectedList.contains(cbId));
        }

        jspData.add(new Checkbox(cbId, title, code));

        if (checked!=null) {
            if (!selectedList.contains(cbId)){
                if (checked == true){
                    selectedList.add(cbId);
                    jspSelectedIds = selectedList.toArray(new String[selectedList.size()]);
                }
            } else {
                if (checked == false){
                    selectedList.remove(cbId);
                    jspSelectedIds = selectedList.toArray(new String[selectedList.size()]);
                }
            }
        }

        return this;
    }

//    public JspComponentCheckboxesStringId add(String id, String title, boolean checked) {
//        return add(id, title, checked, null);
//    }
//
//    public JspComponentCheckboxesStringId add(String id, String title) {
//        return add(id, title, null, null);
//    }
//
//    public JspComponentCheckboxesStringId add(String title, boolean checked) {
//        return add(null, title, checked, null);
//    }
//
//    public JspComponentCheckboxesStringId add(String title) {
//        return add(null, title, null, null);
//    }

    public JspComponentCheckboxesStringId clearChecked() {
        jspSelectedIds = new String[0];
        return this;
    }

    public JspComponentCheckboxesStringId check(String[] ids) {
        check(Arrays.asList(ids));
        return this;
    }

    public JspComponentCheckboxesStringId check(List<String> ids) {
        List<String> list = Arrays.asList(jspSelectedIds);

        list.addAll(ids);

        jspSelectedIds = list.toArray(new String[list.size()]);
        return this;
    }

    public String getJspTitle() {
        return jspTitle;
    }

    public String[] getJspSelectedIds() {
        return jspSelectedIds;
    }

    public List<Checkbox> getJspData() {
        return jspData;
    }

    public void setJspTitle(String jspTitle) {
        this.jspTitle = jspTitle;
    }

    public void setJspSelectedIds(String[] jspSelectedIds) {
        this.jspSelectedIds = jspSelectedIds;
    }

    public void setJspData(List<Checkbox> jspData) {
        this.jspData = jspData;
    }

    public boolean isSelected(String id) {
        if (id != null) {
            for (String checked : jspSelectedIds)
                if (id.equals(checked))
                    return true;
        }
        return false;
    }

    private static class Checkbox {
        private String jspId;
        private String jspTitle;
        private Object code;

        private Checkbox(String jspId, String jspTitle) {
            this(jspId, jspTitle, null);
        }

        private Checkbox(String jspId, String jspTitle, Object code) {
            this.jspId = jspId;
            this.jspTitle = jspTitle;
            this.code = code;
        }

        public String getJspId() {
            return jspId;
        }

        public String getJspTitle() {
            return jspTitle;
        }

        public Object getCode() {
            return code;
        }
    }

    public boolean isSet() {
        return isSet;
    }
    public void set(){
        isSet = true;
    }
}
