package pl.compan.docusafe.web.jsp.component.permission;

import pl.compan.docusafe.web.jsp.component.JspPermissionManager;
import pl.compan.docusafe.web.swd.management.utils.SwdPermission;
import pl.compan.docusafe.web.swd.management.utils.SwdProfile;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Wlasciwosci pol (odpowiednich dla statusu dokumentu)
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspStatusFieldsPropertiesSet {

    protected Map<String, JspFieldProperties> components = new HashMap<String, JspFieldProperties>();
    protected Map<String, JspFieldProperties> fields = new HashMap<String, JspFieldProperties>();

    public JspFieldProperties getFieldProperties(JspPermissionManager.JspFieldKind fieldKind, String fieldName) {
        switch (fieldKind) {
            case COMPONENT:
                return components.get(fieldName);
            case FIELD:
                return fields.get(fieldName);
            default:
                throw new IllegalArgumentException();
        }
    }

    public JspFieldProperties getOrCreateProperties(String name, JspPermissionManager.JspFieldKind fieldKind){
        Map<String, JspFieldProperties> map = getMap(fieldKind);
        JspFieldProperties properties = map.get(name);
        if (properties == null){
            properties = new JspFieldProperties();
            map.put(name,properties);
        }
        return properties;
    }
    private Map<String, JspFieldProperties> getMap(JspPermissionManager.JspFieldKind fieldKind){
        switch(fieldKind){
            case COMPONENT:
                return components;
            case FIELD:
                return fields;
            default:
                throw new IllegalArgumentException();
        }
    }

    public JspStatusFieldsPropertiesSet setFieldModificatorsByPermissions (Collection<SwdPermission.Permission> permissions){
        return setFieldModificatorsByPermissions(permissions.toArray(new SwdPermission.Permission[permissions.size()]));
    }

    public JspStatusFieldsPropertiesSet setFieldModificatorsByPermissions (SwdPermission.Permission... permissions){
        for (SwdPermission.Permission p : permissions)
            setFieldModificatorsByPermission(p);
        return this;
    }

    private JspStatusFieldsPropertiesSet setFieldModificatorsByPermission(SwdPermission.Permission permission) {
        for (String groupName : permission.fieldsGroup.fieldsNames){
            JspFieldProperties prop = getOrCreateProperties(groupName, permission.fieldsGroup.fieldKind);

            SwdPermission.PermissionType type = SwdPermission.PermissionType.getPermissionType(permission);
            String[] swdProfilesNames = SwdProfile.getProfilesNamesAsArrayFor(permission);
            switch(type){
                case READ:
                    prop.addReadProfiles(swdProfilesNames);
                    prop.addReadPermission(type.name());
                    break;
                case EDIT:
                    prop.addEnabledProfiles(swdProfilesNames);
                    prop.addEnabledPermission(type.name());
                    break;
                case SHOW:
                    prop.addShownProfiles(swdProfilesNames);
                    prop.addShownPermission(type.name());
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }

        return this;
    }
}