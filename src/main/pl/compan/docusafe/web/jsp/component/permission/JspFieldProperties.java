package pl.compan.docusafe.web.jsp.component.permission;

import edu.emory.mathcs.backport.java.util.Arrays;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspFieldProperties {

    public final Set<String> readPermission = new HashSet<String>();
    public final Set<String> enabledPermission = new HashSet<String>();
    public final Set<String> shownPermission = new HashSet<String>();
    public final Set<String> readRoles = new HashSet<String>();
    public final Set<String> enabledRoles = new HashSet<String>();
    public final Set<String> shownRoles = new HashSet<String>();
    public final Set<String> readProfiles = new HashSet<String>();
    public final Set<String> enabledProfiles = new HashSet<String>();
    public final Set<String> shownProfiles = new HashSet<String>();

    public JspFieldProperties addReadPermission(String... permissionsNames) {
        readPermission.addAll(Arrays.asList(permissionsNames));
        return this;
    }

    public JspFieldProperties addEnabledPermission(String... permissionsNames) {
        enabledPermission.addAll(Arrays.asList(permissionsNames));
        return this;
    }

    public JspFieldProperties addShownPermission(String... permissionsNames) {
        shownPermission.addAll(Arrays.asList(permissionsNames));
        return this;
    }

    public JspFieldProperties addReadRoles(String... rolesNames) {
        readRoles.addAll(Arrays.asList(rolesNames));
        return this;
    }

    public JspFieldProperties addEnabledRoles(String... rolesNames) {
        enabledRoles.addAll(Arrays.asList(rolesNames));
        return this;
    }

    public JspFieldProperties addShowndRoles(String... rolesNames) {
        shownRoles.addAll(Arrays.asList(rolesNames));
        return this;
    }

    public JspFieldProperties addReadProfiles(String... profilesNames) {
        readProfiles.addAll(Arrays.asList(profilesNames));
        return this;
    }

    public JspFieldProperties addEnabledProfiles(String... profilesNames) {
        enabledProfiles.addAll(Arrays.asList(profilesNames));
        return this;
    }

    public JspFieldProperties addShownProfiles(String... profilesNames) {
        shownProfiles.addAll(Arrays.asList(profilesNames));
        return this;
    }

    public String[] getRequiredReadPermissionAsArray() {
        return readPermission.toArray(new String[readPermission.size()]);
    }

    public String[] getRequiredShownPermissionAsArray() {
        return shownPermission.toArray(new String[shownPermission.size()]);
    }

    public String[] getRequiredEnabledPermissionAsArray() {
        return enabledPermission.toArray(new String[enabledPermission.size()]);
    }

    public String[] getRequiredReadRolesAsArray() {
        return readRoles.toArray(new String[readRoles.size()]);
    }

    public String[] getRequiredShownRolesAsArray() {
        return shownRoles.toArray(new String[shownRoles.size()]);
    }

    public String[] getRequiredEnabledRolesAsArray() {
        return enabledRoles.toArray(new String[enabledRoles.size()]);
    }

    public String[] getRequiredReadProfilesAsArray() {
        return readProfiles.toArray(new String[readProfiles.size()]);
    }

    public String[] getRequiredShownProfilesAsArray() {
        return shownProfiles.toArray(new String[shownProfiles.size()]);
    }

    public String[] getRequiredEnabledProfilesAsArray() {
        return enabledProfiles.toArray(new String[enabledProfiles.size()]);
    }
}