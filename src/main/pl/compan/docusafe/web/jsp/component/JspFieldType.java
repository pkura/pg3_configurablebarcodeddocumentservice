package pl.compan.docusafe.web.jsp.component;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public enum JspFieldType {
    TEXT ("TEXT"),
    SELECT ("SELECT"),
    CHECKBOX("CHECKBOX");

    public final String jspCode;

    private JspFieldType(String name) {
        this.jspCode = name;
    }

    public String getJspCode() {
        return jspCode;
    }
}
