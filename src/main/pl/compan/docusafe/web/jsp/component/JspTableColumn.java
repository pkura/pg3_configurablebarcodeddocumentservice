package pl.compan.docusafe.web.jsp.component;

import pl.compan.docusafe.web.TableColumn;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspTableColumn extends TableColumn {
    public static final int DEFAULT_SMALL_WIDTH = 80;
    public static final int DEFAULT_DOUBLE_SMALL_WIDTH = 160;

    protected Integer width;
    protected JspAttribute filter;

    private JspTableColumn(String property, String title, String sortDesc, String sortAsc, String columnName) {
        super(property, title, sortDesc, sortAsc, columnName);
        init(null, null);
    }

    private JspTableColumn(String property, String title, String sortDesc, String sortAsc) {
        super(property, title, sortDesc, sortAsc);
        init(null, null);
    }

    public JspTableColumn(JspAttribute filter, Integer width, String property, String title, String sortDesc, String sortAsc, String columnName) {
        this(property, title, sortDesc, sortAsc, columnName);
        init(width, filter);
    }

//    public JspTableColumn(JspAttribute filter, Integer width, String property, String title, String sortDesc, String sortAsc) {
//        this(property, title, sortDesc, sortAsc);
//        init(width, filter);
//    }

    public JspTableColumn(Integer width, String property, String title, String sortDesc, String sortAsc, String columnName) {
        this(null, width, property, title, sortDesc, sortAsc, columnName);
    }

//    public JspTableColumn(Integer width, String property, String title, String sortDesc, String sortAsc) {
//        this(null, width, property, title, sortDesc, sortAsc);
//    }

    private void init(Integer columnWidth, JspAttribute filter) {
        this.width = columnWidth;
        this.filter = filter;
    }

    public JspAttribute getFilter() {
        return filter;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public void setFilter(JspAttribute filter) {
        this.filter = filter;
    }
}
