package pl.compan.docusafe.web.jsp.component;

import java.util.ArrayList;
import java.util.List;

/**
 * Dla atrybutu w klasie pochodnej musi istnie� metoda get (chyba i set) dla pola o identycznej nazwie jak jspFieldName.
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class JspComponentAttributes {

    protected String jspTitle;
    protected List<JspAttribute> jspData;
    private boolean isSet = false;

    public JspComponentAttributes() {
        this(null);
    }

    public JspComponentAttributes(String jspTitle) {
        this(jspTitle, new ArrayList<JspAttribute>());
    }

    public JspComponentAttributes(String jspTitle, List<JspAttribute> jspData) {
        this.jspTitle = jspTitle;
        this.jspData = jspData;
    }

    public JspAttribute findAttributeByFieldName (String fieldName){
        for(JspAttribute attr : jspData)
            if (attr.equals(fieldName))
                return attr;
        return null;
    }

    /**
     * W przypadku pola typu select pozwala na ustawienie czy dany element jest wybrany
     */
    public boolean isSelected(String fieldName, String select) {
        return false;
    }

    public String getJspTitle() {
        return jspTitle;
    }

    public List<JspAttribute> getJspData() {
        return jspData;
    }

    public JspComponentAttributes setJspTitle(String jspTitle) {
        this.jspTitle = jspTitle;
        return this;
    }

    public JspComponentAttributes setJspData(List<JspAttribute> jspData) {
        this.jspData = jspData;
        return this;
    }

    public JspComponentAttributes addAttribute(JspAttribute attr) {
        jspData.add(attr);
        return this;
    }

    public boolean isSet() {
        return isSet;
    }

    public void set(){
        isSet = true;
    }
}
