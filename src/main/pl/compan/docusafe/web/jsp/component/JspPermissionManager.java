package pl.compan.docusafe.web.jsp.component;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.permission.JspFieldProperties;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class JspPermissionManager {

    private static Logger log = LoggerFactory.getLogger(JspPermissionManager.class);
//    protected final boolean defaultReadonly;
    protected final boolean defaultDisabled;
    protected final boolean defaultHidden;

    public JspPermissionManager(/*boolean defaultReadonly, */boolean defaultDisabled, boolean defaultHidden) {
//        this.defaultReadonly = defaultReadonly;
        this.defaultDisabled = defaultDisabled;
        this.defaultHidden = defaultHidden;
    }

    public static boolean isAdmin() {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsUserIfNeed();
            DSUser user = getLoginUser();
            return user != null && user.isAdmin();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
    }

    /**Wymaga otwartego jako u�ytkownik context-u - np przez {@link DSContextOpener}.openAsUserIfNeed()*/
    public static DSUser getLoginUser() {
        DSUser user = null;
        try {
            user = DSApi.context().getDSUser();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return user;
    }

    public static boolean isAddsTrue(String addsName) {
        return "true".equals(Docusafe.getAdditionProperty(addsName));
    }

    public static boolean hasRoles(String... okRoleNames) {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsUserIfNeed();
            DSUser user = getLoginUser();

            Set<String> userRolesNames = user.getRoles();
            for (String okName : okRoleNames)
                if (userRolesNames.contains(okName))
                    return true;
            return false;
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
    }

    public static boolean hasProfileName(String... okProfileNames) {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsUserIfNeed();
            DSUser user = getLoginUser();

            String[] userProfileNames = user.getProfileNames();
            for (String okName : okProfileNames)
                for (String upName : userProfileNames)
                    if (upName.equalsIgnoreCase(okName))
                        return true;
            return false;

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
    }

    public static boolean hasPermission(String... okPermissionsNames) {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsUserIfNeed();
            DSUser user = getLoginUser();

            Set<String> userRolesNames = user.getRoles();
            Set<Role> userRoles = new HashSet<Role>();

            for (String urName : userRolesNames) {
                try {
                    Role uRole = Role.findByName(urName);
                    if (uRole!=null)
                        userRoles.add(uRole);
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }

            for (Role uRole : userRoles) {
                Set<String> userPermissions = uRole.getPermissions();
                for (String okName : okPermissionsNames)
                    if (userPermissions.contains(okName))
                        return true;
            }

            return false;

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
    }


    private boolean getValue(Boolean value, boolean defaultValue){
        return value == null ? defaultValue : value;
    }
//    public Boolean isFieldReadonly(String fieldName){
//        return getValue(isFieldReadonlyBody(fieldName),defaultReadonly);
//    }
//
//    public Boolean isComponentReadonly(String componentName){
//        return getValue(isComponentReadonlyBody(componentName),defaultReadonly);
//    }

    public Boolean isFieldDisabled(String fieldName){
        return getValue(isFieldDisabledBody(fieldName),defaultDisabled);
    }

    public Boolean isComponentDisabled(String componentName){
        return getValue(isComponentDisabledBody(componentName),defaultDisabled);
    }

    public Boolean isFieldHidden(String fieldName){
        return getValue(isFieldHiddenBody(fieldName),defaultHidden);
    }

    public Boolean isComponentHidden(String componentName){
        return getValue(isComponentHiddenBody(componentName),defaultHidden);
    }

//    public abstract Boolean isFieldReadonlyBody(String fieldName);

    public abstract Boolean isFieldDisabledBody(String fieldName);

    public abstract Boolean isFieldHiddenBody(String fieldName);

//    public abstract Boolean isComponentReadonlyBody(String componentName);

    public abstract Boolean isComponentDisabledBody(String componentName);

    public abstract Boolean isComponentHiddenBody(String componentName);

    public boolean isFieldHidden(String componentName, String fieldName) {
        Boolean permissionFromComponent = isComponentHiddenBody(componentName);
        if (permissionFromComponent != null)
            return permissionFromComponent;
        Boolean permissionFromField = isFieldHiddenBody(fieldName);
        if (permissionFromField != null)
            return permissionFromField;
        return defaultHidden;
    }

//    public boolean isFieldReadonly(String componentName, String fieldName) {
//        Boolean permissionFromComponent = isComponentReadonlyBody(componentName);
//        if (permissionFromComponent != null)
//            return permissionFromComponent;
//        Boolean permissionFromField = isFieldReadonlyBody(fieldName);
//        if (permissionFromField != null)
//            return permissionFromField;
//        return defaultHidden;
//    }

    public boolean isFieldDisabled(String componentName, String fieldName) {
        Boolean permissionFromComponent = isComponentDisabledBody(componentName) ;
        if (permissionFromComponent != null)
            return permissionFromComponent;
        Boolean permissionFromField = isFieldDisabledBody(fieldName);
        if (permissionFromField != null)
            return permissionFromField;
        return defaultHidden;
    }

    public enum JspFieldKind {
        COMPONENT, FIELD
    }

    public static abstract class DocumentStatus extends JspPermissionManager {

        private Long documentId;
        private boolean cacheEnabled = false;
        private JspStatusFieldsPropertiesSet cacheAllFieldProperties = null;
        //protected Map<String, JspStatusFieldsPropertiesSet> statusFieldsProperties = new HashMap<String, JspStatusFieldsPropertiesSet>();

        public DocumentStatus(/*boolean defaultReadonly, */boolean defaultDisabled, boolean defaultHidden) {
            this(null, /*defaultReadonly, */defaultDisabled, defaultHidden);
        }

        public DocumentStatus(Long documentId, /*boolean defaultReadonly, */boolean defaultDisabled, boolean defaultHidden) {
            super(/*defaultReadonly, */defaultDisabled, defaultHidden);
            setDocumentId(documentId);
        }

        public DocumentStatus setCacheEnabled(boolean cacheEnabled) {
            this.cacheEnabled = cacheEnabled;
            return this;
        }

        public void setDocumentId(Long documentId){
            this.documentId = documentId;
        }

//        public void setDocumentById(Long documentId) {
//            try {
//                document = documentId != null ? Document.find(documentId) : null;
//            } catch (EdmException e) {
//                document = null;
//                log.error(e.getMessage(), e);
//            }
//        }
//        public void setDocument(Document document) {
//            this.document = document;
//        }

        /** Wymaga otwartego kontekstu */
        public Document getDocument() {
            Document document = null;
            try {
                if (documentId != null)
                    document = Document.find(documentId);
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
            return document;
        }

        protected boolean canReadDocument() {
            DSContextOpener opener = null;
            try {
                opener = DSContextOpener.openAsUserIfNeed();
                Document document = getDocument();
                return document == null || document.canRead();
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                return false;
            }finally {
                DSContextOpener.closeIfNeed(opener);
            }
        }

        protected String getStatusCn() {
            String status = null;
            DSContextOpener opener = null;
            try {
                opener = DSContextOpener.openAsUserIfNeed();
                Document document = getDocument();
                status = getStatusCn(document);
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            } finally {
                DSContextOpener.closeIfNeed(opener);
            }
            return status;
        }

        /** Wymaga otwartego kontekstu */
        public static String getStatusCn(Document document) {
            Object status = null;
            try {
                status = document.getFieldsManager().getEnumItemCn("STATUS");
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
            return status != null && StringUtils.isNotBlank(status.toString()) ? status.toString() : null;
        }

        /**
         * Gdy dla p�l maj� zosta� ustalone warto�ci domyslne, mo�na zwr�ci� null
         */
        protected final JspFieldProperties getFieldProperties (JspFieldKind fieldKind, String fieldName){
            if (!cacheEnabled || cacheAllFieldProperties == null)
                cacheAllFieldProperties = getFieldsProperties();

            if (cacheAllFieldProperties == null) return null;
            JspFieldProperties fieldProperties = cacheAllFieldProperties.getFieldProperties(fieldKind, fieldName);
            return fieldProperties;
        }

        /**
         * Gdy dla p�l maj� zosta� ustalone warto�ci domyslne, mo�na zwr�ci� null
         */
        protected JspStatusFieldsPropertiesSet getFieldsProperties() {
            JspStatusFieldsPropertiesSet propertiesSet = null;
            DSContextOpener opener = null;
            try {
                opener = DSContextOpener.openAsUserIfNeed();
                Document document = getDocument();

                String status = getStatusCn(document);
                if (!StringUtils.isBlank(status)) {
                    DocumentLogic logic = document.getDocumentKind().logic();
                    if (logic != null) {
                        propertiesSet = getFieldsProperties(logic, status);
                    }
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            } finally {
                DSContextOpener.closeIfNeed(opener);
            }

            return propertiesSet;
        }

        /** Gdy dla p�l maj� zosta� ustalone warto�ci domyslne, mo�na zwr�ci� null */
        protected abstract JspStatusFieldsPropertiesSet getFieldsProperties(DocumentLogic logic, String status);

//        public Boolean isReadonly(JspFieldKind fieldKind, String fieldName) {
//            return StringUtils.isNotBlank(fieldName) && canReadDocument() ? isReadonlyBody(fieldKind, fieldName) : null;
//        }

        public Boolean isDisabled(JspFieldKind fieldKind, String fieldName) {
            return StringUtils.isNotBlank(fieldName) && canReadDocument() ? isDisabledBody(fieldKind, fieldName) : null;
        }

        public Boolean isHidden(JspFieldKind fieldKind, String fieldName) {
            return StringUtils.isNotBlank(fieldName) && canReadDocument() ? isHiddenBody(fieldKind, fieldName) : null;
        }

        protected Boolean isAny(String[] permissionsNames, String[] rolesNames, String[] profilesNames){
//            if (permissionsNames.length == 0 && rolesNames.length == 0 && profilesNames.length == 0) return null;
//            return hasPermission(permissionsNames) || hasRoles(rolesNames) || hasProfileName(profilesNames);
            return getNegative(isNoAny(permissionsNames, rolesNames, profilesNames));
        }

        protected Boolean isNoAny(String[] permissionsNames, String[] rolesNames, String[] profilesNames){
            if (permissionsNames.length == 0 && rolesNames.length == 0 && profilesNames.length == 0) return null;
            return !hasPermission(permissionsNames) && !hasRoles(rolesNames) && !hasProfileName(profilesNames);
        }

        protected Boolean getNegative(Boolean value){
            return value == null ? null : !value;
        }

        protected Boolean isDisabledBody(JspFieldKind fieldKind, String fieldName) {
            JspFieldProperties fieldProperties = getFieldProperties(fieldKind, fieldName);
            if (fieldProperties == null) return null;
            String [] permissionsNames = fieldProperties.getRequiredEnabledPermissionAsArray();
            String [] rolesNames = fieldProperties.getRequiredEnabledRolesAsArray();
            String [] profilesNames = fieldProperties.getRequiredEnabledProfilesAsArray();
            return isNoAny(permissionsNames, rolesNames, profilesNames);
        }

        protected Boolean isHiddenBody(JspFieldKind fieldKind, String fieldName) {
            JspFieldProperties fieldProperties = getFieldProperties(fieldKind, fieldName);
            if (fieldProperties == null) return null;
            String [] permissionsNames = fieldProperties.getRequiredReadPermissionAsArray();
            String [] rolesNames = fieldProperties.getRequiredReadRolesAsArray();
            String [] profilesNames = fieldProperties.getRequiredReadProfilesAsArray();
            return isNoAny(permissionsNames, rolesNames, profilesNames);
        }


    }
}
