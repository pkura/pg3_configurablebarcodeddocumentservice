package pl.compan.docusafe.web.jsp.component;

import com.asprise.util.tiff.E;
import pl.compan.docusafe.web.swd.management.utils.AlphanumComparator;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class JspComponentTable<T> implements Serializable{

    protected Boolean jspHeaderDisabled;
    protected String jspTitle;

    protected List<JspTableColumn> jspColumns;
    protected List<T> jspData;

    protected Boolean jspFilterEnabled;
    protected String jspFilterColumnTitle;
    protected Integer jspFilterColumnWidth;

    protected OrderChecker orderChecker;
    protected ChosenItemGetter chosenItemGetter;
    protected Layout parentLayout = Layout.MAIN;

    public enum Layout {
        MAIN, POPUP;

        public static Layout getLayout(String name){
            return name != null ? JspComponentTable.Layout.valueOf(name.toUpperCase()) : MAIN;
        }
    }
    public JspComponentTable(String title){
        this(title, null);
    }
    public JspComponentTable(OrderChecker orderChecker){
        this(null, orderChecker);
    }
    public JspComponentTable(String title, OrderChecker orderChecker){
        this.jspTitle = title;
        this.orderChecker = orderChecker;
    }

    /**
     * W przypadku pola typu select pozwala na ustawienie czy dany element jest wybrany
     */
    public boolean isSelected(JspTableColumn column, String select) {
        return false;
    }

    public abstract String getData(T customer, JspTableColumn column);

    public String getLink(T customer, JspTableColumn column){
        return null;
    }

    public boolean isOrderBy (JspTableColumn column, Boolean ascending){
        return orderChecker != null && orderChecker.isOrderBy(column, ascending);
    }


//    public abstract String getColumnWidth(String column);
//    public abstract String getFilterName(String column);
//    public abstract String getFilterType(String column);


    public String getJspTitle() {
        return jspTitle;
    }

    public JspComponentTable setJspTitle(String jspTitle) {
        this.jspTitle = jspTitle;
        return this;
    }

    public List<JspTableColumn> getJspColumns() {
        return jspColumns;
    }

    public JspComponentTable setJspColumns(List<JspTableColumn> jspColumns) {
        this.jspColumns = jspColumns;
        return this;
    }

    public List<T> getJspData() {
        return jspData;
    }

    public JspComponentTable setJspData(List<T> jspData) {
        this.jspData = jspData;
        return this;
    }

    public JspComponentTable setJspFilterColumnTitle(String jspFilterColumnTitle) {
        this.jspFilterColumnTitle = jspFilterColumnTitle;
        return this;
    }

    public String getJspFilterColumnTitle() {
        return jspFilterColumnTitle;
    }

    public Integer getJspFilterColumnWidth() {
        return jspFilterColumnWidth;
    }

    public JspComponentTable setJspFilterColumnWidth(Integer jspFilterColumnWidth) {
        this.jspFilterColumnWidth = jspFilterColumnWidth;
        return this;
    }

    public Boolean getJspFilterEnabled() {
        return jspFilterEnabled;
    }

    public JspComponentTable setJspFilterEnabled(Boolean enabled) {
        this.jspFilterEnabled = enabled;
        return this;
    }

    public Boolean getJspHeaderDisabled() {
        return jspHeaderDisabled;
    }

    public void setJspHeaderDisabled(Boolean jspHeaderDisabled) {
        this.jspHeaderDisabled = jspHeaderDisabled;
    }

    protected static <T> void sortByColumn(List<T> listToSort, final DataGetter<E,T> dataGetter, final boolean ascending) {
        final AlphanumComparator alphanumComparator = new AlphanumComparator();
        Collections.sort(listToSort,
                new Comparator<Object>() {

                    @Override
                    public int compare(Object o1, Object o2) {

                        String o1Name = dataGetter.getData((T)o1);
                        String o2Name = dataGetter.getData((T)o2);

                        if (o1Name == null && o2Name == null)
                            return 0;
                        if (o1Name == null)
                            o1Name = "";
                        if (o2Name == null)
                            o2Name = "";

                        return ascending(alphanumComparator.compare(o1Name.toString(), o2Name.toString()));
                    }

                    private int ascending(int winner) {
                        return ascending ? winner : -winner;
                    }
                }
        );
    }

    protected abstract static class DataGetter<E,T> {
        protected E column;
        public DataGetter (E column){
           this.column = column;
        }
        public abstract String getData(T obj);
    }

    public ChosenItemGetter getChosenItemGetter() {
        return chosenItemGetter;
    }

    public OrderChecker getOrderChecker() {
        return orderChecker;
    }

    public void setOrderChecker(OrderChecker orderChecker) {
        this.orderChecker = orderChecker;
    }

    public void setChosenItemGetter(ChosenItemGetter chosenItemGetter) {
        this.chosenItemGetter = chosenItemGetter;
    }

    public interface OrderChecker {
        boolean isOrderBy(JspTableColumn column, Boolean ascending);
    }


    public static abstract class ChosenItemGetter<T>{

        public String getChosenInputName(){
            return "Wybierz";
        }
        public String getChosenInputClass(){
            return "btn wybierz";
        }

        public abstract Object getDataItemId (T item);
    }

    public Layout getParentLayout() {
        return parentLayout;
    }

    public void setParentLayout(Layout parentLayout) {
        this.parentLayout = parentLayout;
    }
}
