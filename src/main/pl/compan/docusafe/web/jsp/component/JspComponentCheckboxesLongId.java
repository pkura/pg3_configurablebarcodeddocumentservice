package pl.compan.docusafe.web.jsp.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class JspComponentCheckboxesLongId {

    private boolean isSet = false;

    private long lastMaxId = 0;

    private String jspTitle;
    private Long[] jspSelectedIds = new Long[0];
    private List<Checkbox> jspData = new ArrayList<Checkbox>();

    public JspComponentCheckboxesLongId(String title) {
        this.jspTitle = title;
    }

    /**
     * @param id Jeżeli null - automatycznie przyporządkowywany id
     * @param title
     * @param checked Jeżeli null - nie powoduje zmiany stanu checkboxa.
     * @param code niewymagany
     * @return
     */
    public JspComponentCheckboxesLongId add(Long id, String title, Boolean checked, Object code) {
        List<Long> selectedList = new ArrayList<Long>(Arrays.asList(jspSelectedIds));

        Long cbId = id;
        if (cbId == null){
            do{
                cbId = ++lastMaxId;
            } while (selectedList.contains(cbId));
        }

        jspData.add(new Checkbox(cbId, title, code));

        if (checked!=null) {
            if (!selectedList.contains(cbId)){
                if (checked == true){
                    selectedList.add(cbId);
                    jspSelectedIds = selectedList.toArray(new Long[selectedList.size()]);
                }
            } else {
                if (checked == false){
                    selectedList.remove(cbId);
                    jspSelectedIds = selectedList.toArray(new Long[selectedList.size()]);
                }
            }
        }

        return this;
    }


    public JspComponentCheckboxesLongId clearChecked() {
        jspSelectedIds = new Long[0];
        return this;
    }

    public JspComponentCheckboxesLongId check(Long[] ids) {
        check(Arrays.asList(ids));
        return this;
    }

    public JspComponentCheckboxesLongId check(List<Long> ids) {
        List<Long> list = Arrays.asList(jspSelectedIds);

        list.addAll(ids);

        jspSelectedIds = list.toArray(new Long[list.size()]);
        return this;
    }

    public String getJspTitle() {
        return jspTitle;
    }

    public Long[] getJspSelectedIds() {
        return jspSelectedIds;
    }

    public List<Checkbox> getJspData() {
        return jspData;
    }

    public void setJspTitle(String jspTitle) {
        this.jspTitle = jspTitle;
    }

    public void setJspSelectedIds(Long[] jspSelectedIds) {
        this.jspSelectedIds = jspSelectedIds;
    }

    public void setJspData(List<Checkbox> jspData) {
        this.jspData = jspData;
    }

    public boolean isSelected(Long id) {
        if (id != null) {
            for (Long checked : jspSelectedIds)
                if (id.equals(checked))
                    return true;
        }
        return false;
    }

    private static class Checkbox {
        private Long jspId;
        private String jspTitle;
        private Object code;

        private Checkbox(long jspId, String jspTitle) {
            this(jspId, jspTitle, null);
        }

        private Checkbox(long jspId, String jspTitle, Object code) {
            this.jspId = jspId;
            this.jspTitle = jspTitle;
            this.code = code;
        }

        public Long getJspId() {
            return jspId;
        }

        public String getJspTitle() {
            return jspTitle;
        }

        public Object getCode() {
            return code;
        }
    }

    public boolean isSet() {
        return isSet;
    }
    public void set(){
        isSet = true;
    }
}
