package pl.compan.docusafe.web.wizard;

import java.util.Map;
/* User: Administrator, Date: 2005-09-05 15:33:20 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PageDef.java,v 1.1 2005/09/05 13:56:02 lk Exp $
 */
class PageDef
{
    private String id;
    private String url;
    private Map attributes;

    public PageDef(String id, String url)
    {
        this.id = id;
        this.url = url;
    }

    public String getId()
    {
        return id;
    }

    public String getUrl()
    {
        return url;
    }

    public void addAttribute(String name, String value)
    {
        attributes.put(name, value);
    }

    public Map getAttributes()
    {
        return attributes;
    }
}
