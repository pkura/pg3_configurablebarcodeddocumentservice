package pl.compan.docusafe.web.wizard;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/* User: Administrator, Date: 2005-09-02 13:18:16 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WizardPage.java,v 1.2 2006/02/20 15:42:51 lk Exp $
 */
public class WizardPage
{
    private String id;
    private String url;
    private Map attributes;
    private boolean skipped;

    public WizardPage(String id, String url, Map attributes)
    {
        this.id = id;
        this.url = url;
        if (attributes != null)
            this.attributes = new HashMap(attributes);
    }

    public String getId()
    {
        return id;
    }

    public String getUrl()
    {
        return url;
    }

    void setSkipped(boolean skipped)
    {
        this.skipped = skipped;
    }

    public boolean isSkipped()
    {
        return skipped;
    }

    Map getAttributes()
    {
        return attributes;
    }

    void setAttributes(Map attributes)
    {
        this.attributes = attributes;
    }

    public void addAttribute(String name, Serializable value)
    {
        if (attributes == null)
            attributes = new HashMap();
        attributes.put(name, value);
    }

    public Serializable getAttribute(String name)
    {
        if (attributes == null)
            return null;
        return (Serializable) attributes.get(name);
    }
}
