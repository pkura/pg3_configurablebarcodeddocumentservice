package pl.compan.docusafe.web.wizard;

import java.util.LinkedList;
import java.util.List;
/* User: Administrator, Date: 2005-09-05 15:33:36 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: WizardDef.java,v 1.2 2006/02/20 15:42:51 lk Exp $
 */
class WizardDef
{
    private String id;
    private List pages = new LinkedList();

    public WizardDef(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void addPage(PageDef pageDef)
    {
        pages.add(pageDef);
    }

    public List getPages()
    {
        return pages;
    }
}
