package pl.compan.docusafe.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** 
 *   @author Piotr Komisarski
 *   @todo - do przerobienia ustawienia - nie powinnismy korzystac z prefsow
 *   Ta klasa przechwytuje ��dania do plik�w CSS.
 *   Sama wypisuje do HttpResponse plik css w zale�no�ci od ustawie�
 */
@SuppressWarnings("serial")
public class CssService extends HttpServlet 
{
	private static final Logger log = LoggerFactory.getLogger(CssService.class);

	public final static Map<String,Properties> layouts = new HashMap<String, Properties>();
	public final static Map<String,ResourceCacheBean> cachedCSS = new HashMap<String,ResourceCacheBean>();
	
	public static boolean layoutsAvailable;
	private static byte[] cssBytes;
	private static byte[] cssIE6Bytes;
	private static byte[] cssIE7Bytes;
	private static byte[] cssIE11Bytes;
	private static byte[] cssOtherBytes;
	//private static String cssAsString;

	public static void initCss()
	{	
		log.debug("initCss");
		InputStream	io =  CssService.class.getClassLoader().getResourceAsStream("main.css");
		InputStream	ioIE6 =  CssService.class.getClassLoader().getResourceAsStream("mainIE6.css");
		InputStream	ioIE7 =  CssService.class.getClassLoader().getResourceAsStream("mainIE7.css");
		InputStream	ioIE11 =  CssService.class.getClassLoader().getResourceAsStream("mainIE11.css");
		InputStream	ioOther =  CssService.class.getClassLoader().getResourceAsStream("mainOther.css");
		
		try 
		{
			cssBytes = new byte[io.available()];
			io.read(cssBytes);
			io.close();
			
			cssIE6Bytes = new byte[ioIE6.available()];
			ioIE6.read(cssIE6Bytes);
			ioIE6.close();
			
			cssIE7Bytes = new byte[ioIE7.available()];
			ioIE7.read(cssIE7Bytes);
			ioIE7.close();
			
			cssIE11Bytes = new byte[ioIE11.available()];
			ioIE11.read(cssIE11Bytes);
			ioIE11.close();
			
			cssOtherBytes = new byte[ioOther.available()];
			ioOther.read(cssOtherBytes);
			ioOther.close();

			Properties prop = new Properties();
			prop.load(CssService.class.getClassLoader().getResourceAsStream("css.properties"));
			layouts.put("classic", prop);
		}
		catch (IOException e) 
		{
			log.error(e.getMessage(),e);
			log.error(e.getLocalizedMessage(),e);
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		resp.setHeader("Cache-Control", "public, max-age="+(24*60*60)); // cache 24 godziny
		resp.setContentType("text/css");
		OutputStream os = resp.getOutputStream();	
		printColors(os, req);
		os.close();        
	}

	private void printColors(OutputStream os, HttpServletRequest req) throws IOException
	{
		long start = System.currentTimeMillis();		
		Properties current;
		String user = "admin";	

		/*
		if(AuthUtil.getSubject(req)!=null)
		{
			try
			{
				DSApi.open(AuthUtil.getSubject(req));
				user = DSApi.context().getDSUser().getName(); 
			}
			catch (EdmException ex)
			{				
				log.error(ex.getMessage(), ex);
			}
			finally 
			{
				try{DSApi._close();}catch(IllegalStateException ise){}
			}
		}*/
		String usertest = (String) req.getHeader("user-agent");
		String css = new String(cssBytes);
		String addCss = "";
		String resKey = null;
		
    	if(usertest.indexOf("Trident/7.0; rv:11.0") > 0)
    	{
    		resKey = "IE11";
    		addCss = new String(cssIE11Bytes);
    	}
    	else if(usertest.indexOf("MSIE 7.0") > 0)
    	{
    		resKey = "IE7";
    		addCss = new String(cssIE7Bytes);
    	}
    	else if(usertest.indexOf("MSIE 6.0") > 0)
    	{
    		resKey = "IE6";
    		addCss = new String(cssIE6Bytes);
    	}
    	else
    	{
    		resKey = "OTHER";
    		addCss = new String(cssOtherBytes);
     	}
    	
    	ResourceCacheBean cache = cachedCSS.get(resKey);
    	if (cache==null)
    	{
    		//Jesli nie ma layoutu to go inicjujemy
        	log.trace("inicjujemy CSSa dla {}",resKey);
        	if(usertest.indexOf("Trident/7.0; rv:11.0") > 0)
        	{        		
        		addCss = new String(cssIE11Bytes);
        	}
        	else if(usertest.indexOf("MSIE 7.0") > 0)
        	{        		
        		addCss = new String(cssIE7Bytes);
        	}
        	else if(usertest.indexOf("MSIE 6.0") > 0)
        	{
        		addCss = new String(cssIE6Bytes);
        	}
        	else
        	{ 
        		addCss = new String(cssOtherBytes);
         	}
        	css = addCss + css;
    		
    		Preferences prefs = Preferences.systemNodeForPackage(Docusafe.class);
    		String layout = prefs.get(user,"classic");
    		current = layouts.get(layout);
    	
    		Set<Object> propertyNames = current.keySet();
    		for (Object key : propertyNames) 
    		{
    			css = css.replaceAll("%"+key+"%",current.getProperty((String)key));			
    		}
    		css = css.replaceAll("%path%", req.getContextPath());
    		cache = new ResourceCacheBean();
    		cache.setResKey(resKey);
    		cache.setResourceAsBytes(css.getBytes());
    		cachedCSS.put(resKey, cache);
    	}
    	os.write(cache.getResourceAsBytes());
		long stop = System.currentTimeMillis();
		Long duration = stop - start;
		log.trace("duration={}",duration); 
	}
	
}