package pl.compan.docusafe.web.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
interface Handler {
    void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception;
}
