package pl.compan.docusafe.web.rest;

import pl.compan.docusafe.parametrization.ic.CommentsForAccountsProvider;

/**
 * Spis serwis�w do RestServlet
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
enum Service {
    IC_COMMENTS_SELECT_FOR_ACCOUNT("/select/ic/comments", new CommentsForAccountsProvider());

    final String pathPrefix;
    final Handler handler;

    Service(String pathPrefix, Handler handler){
        this.pathPrefix = pathPrefix;
        this.handler = handler;
    }

}
