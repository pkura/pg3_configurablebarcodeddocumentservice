package pl.compan.docusafe.web.rest;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.gson.Gson;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class SelectDataProvider implements Handler{
    private final static Logger LOG = LoggerFactory.getLogger(SelectDataProvider.class);

    public static final class Option {
        String value;
        String key;
        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private final Function<Map.Entry<String, String>, Option> ENTRY_TO_OPTION = new Function<Map.Entry<String, String>, Option>(){
        @Override public Option apply(Map.Entry<String, String> entry) {
            Option ret = new Option();
            ret.key = entry.getKey();
            ret.value = entry.getValue();
            return ret;
        }
    };

    @Override
    public final void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception{
        try{
            DSApi.open(AuthUtil.getSubject(req));
            resp.setContentType("application/json;charset=utf-8");
            new Gson().toJson(Collections2.transform(getSelectData(req).entrySet(), ENTRY_TO_OPTION).toArray(),
                    Option[].class,
                    resp.getWriter());
        } finally {
            DSApi._close();
        }
    }

    public abstract Map<String, String> getSelectData(HttpServletRequest req) throws EdmException;
}