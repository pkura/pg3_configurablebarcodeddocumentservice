package pl.compan.docusafe.web.rest;

import com.google.common.collect.Maps;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Servlet dla serwis�w a'la REST
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class RestServlet extends HttpServlet {
    private final static Logger LOG = LoggerFactory.getLogger(RestServlet.class);

    private final static Pattern ROOT_PATTERN = Pattern.compile(".*?/rest");

    private final static ConcurrentNavigableMap<String, Handler> HANDLER_MAP = initHandlerMap();

    private static ConcurrentNavigableMap<String, Handler> initHandlerMap() {
        ConcurrentNavigableMap<String, Handler> ret = new ConcurrentSkipListMap<String, Handler>();
        for(Service service: Service.values()){
            ret.put(service.pathPrefix, service.handler);
        }
        return ret;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String relativePath = ROOT_PATTERN.split(req.getRequestURI(),2)[1];
        Map.Entry<String, Handler> entry = HANDLER_MAP.floorEntry(relativePath);
        LOG.info("prefix for '{}' = '{}'", relativePath, entry == null ? "null" : entry.getKey());

        try {
            if(entry != null && relativePath.startsWith(entry.getKey())){//prawid�owy prefix
                entry.getValue().handle(req, resp);
            } else {
                LOG.info("prefix map : {}", HANDLER_MAP);
                resp.sendError(404);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            resp.sendError(500);
        }
    }
}
