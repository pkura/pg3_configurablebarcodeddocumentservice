package pl.compan.docusafe.web;

import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.EdmException;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
/* User: Administrator, Date: 2006-10-30 11:25:32 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: DrsInfo.java,v 1.2 2007/03/30 14:56:59 lk Exp $
 */
public class DrsInfo
{
    private String drsNR_SZKODY;
    private Integer drsNR_KOLEJNY;
    private String drsLink;

    public static List<DrsInfo> createDrsInfoList(List<Long> drsIds) throws EdmException
    {
        List<DrsInfo> list = new ArrayList<DrsInfo>();
        for (int i=0; i < drsIds.size(); i++)
        {
            Doctype drsDoctype = Doctype.findByCn("nationwide_drs");
            Map drsValues = drsDoctype.getValueMapByCn(drsIds.get(i));
            DrsInfo obj = new DrsInfo();
            obj.setDrsNR_SZKODY((String) drsValues.get("NR_SZKODY"));
            obj.setDrsNR_KOLEJNY((Integer) drsValues.get("NR_KOLEJNY"));
            obj.setDrsLink("/repository/edit-doctype-document.action?id="+drsIds.get(i));
            list.add(obj);
        }
        return list;
    }

    public String getDrsNR_SZKODY()
    {
        return drsNR_SZKODY;
    }

    public void setDrsNR_SZKODY(String drsNR_SZKODY)
    {
        this.drsNR_SZKODY = drsNR_SZKODY;
    }

    public Integer getDrsNR_KOLEJNY()
    {
        return drsNR_KOLEJNY;
    }

    public void setDrsNR_KOLEJNY(Integer drsNR_KOLEJNY)
    {
        this.drsNR_KOLEJNY = drsNR_KOLEJNY;
    }

    public String getDrsLink()
    {
        return drsLink;
    }

    public void setDrsLink(String drsLink)
    {
        this.drsLink = drsLink;
    }
}
