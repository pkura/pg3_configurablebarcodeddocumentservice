package pl.compan.docusafe.web.support;

import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.web.support.cli.CallHandler;
import pl.compan.docusafe.web.support.cli.CommandHandler;
import pl.compan.docusafe.web.support.cli.DataMartReaderHandler;
import pl.compan.docusafe.web.support.cli.HistoryHandler;
import pl.compan.docusafe.web.support.cli.IllegalCommandArgumentsException;
import pl.compan.docusafe.web.support.cli.RefreshHandlers;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import static pl.compan.docusafe.webwork.event.SessionControl.*;
/**
 * Obs�uga terminalu administratora
 * @author Micha� Sankowski
 */
public class TerminalAction extends EventActionSupport
{
	static FillForm fillForm = new FillForm();

	private Logger log = LoggerFactory.getLogger(TerminalAction.class);
	private StringBuilder response;
	private static Map<String, CommandHandler> commandHandlers;
	static {
		commandHandlers = new ConcurrentHashMap<String, CommandHandler>();
		commandHandlers.put("call", new CallHandler());
		commandHandlers.put("refresh", new RefreshHandlers());
		commandHandlers.put("history", new HistoryHandler());
		commandHandlers.put("datamart", new DataMartReaderHandler());
	}

	private String command;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("cmd").
            append(OPEN_HIBERNATE_AND_JBPM_SESSION).
            append(new ProcessCommand()).
            appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
    }

	public String getResponse() {
		return response.toString();
	}

	public void setResponse(String response) {
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

    private static class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {
		}
	}

	private class ProcessCommand implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			response = new StringBuilder();
			try {
				if (StringUtils.isEmpty(command)) {
					return;
				}

				command = command.trim();

				String[] splittedCommand = command.split(" ", 2);

				CommandHandler handler = commandHandlers.get(splittedCommand[0]);
				if (handler == null) {
					response.append("Nieznane polecenie " + splittedCommand[0]);
					return;
				} else {
					if(splittedCommand.length > 1){
						response.append(handler.process(splittedCommand[1].trim()));
					} else {
						response.append(handler.process(""));
					}
				}

//				processCall(command.substring(5));
			} catch(IllegalCommandArgumentsException e){
				response.append(e.getMessage());
			} catch (Exception e) {
				//addActionError(e.getMessage());
				writeToResponse(e, response);
			}

		}
	}

	public static void metoda(){}
	
	private StringBuilder processSql(String command) throws EdmException
	{
		response = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			
			ps = DSApi.context().prepareStatement(command);
			rs = ps.executeQuery();
			response.append("<table style=\"color:white !important; font-family:monospace !important;\">");
			ResultSetMetaData rsmd = rs.getMetaData();
			response.append("<tr>");
			for (int i = 0, max = rsmd.getColumnCount(); i < max; ++i) 
			{
				response.append("<th style=\"font-family:monospace !important\">");
				response.append(rsmd.getColumnName(max));
				response.append("</th>");
			}
			response.append("</tr>");

			while(rs.next())
			{
				response.append("<tr>");
				for (int i = 1, max = rsmd.getColumnCount(); i <= max; ++i) {
					response.append("<td style=\"font-family:monospace !important\">");
					response.append(rs.getObject(i));
					response.append("</td>");
				}
				response.append("</tr>");
			}
			response.append("</table>");
		} 
		catch (Exception e) 
		{
			log.error("",e);
			response.append(e.getLocalizedMessage());
			response.append("Statement = " + command);
			throw new EdmException(e);
		} 
		finally
		{
			try
			{
				rs.close();
				DSApi.context().closeStatement(ps);
			}
			catch(Exception e){}
		}
		return response;
	}
	
//	private StringBuilder processCall(String command) throws EdmException
//	{
//		response = new StringBuilder();
//		try
//		{
//			String[] cmds = command.split(" ");
//			ClassLoader clazzLoader = ClassLoader.getSystemClassLoader();
//
//			//clazzLoader
//			Class clazz = Class.forName(cmds[0]);
//			Object instance = clazz.newInstance();
//			Method method = clazz.getMethod(cmds[1]);
//			Runnable thread = new TerminalThread(method, instance);
//			new Thread(thread).start();
//			response.append("Wywolano metode ").append(cmds[1]).append(" z klasy ").append(clazz.getCanonicalName()).append(" w nowym watku.");
//
//		}
//		catch (Exception e)
//		{
//			//response.append(e.getMessage());
//			log.error(e.getMessage(),e);
//			writeToResponse(e, response);
//		}
//		return response;
//
//	}
	
	
	
	private void writeToResponse(Exception e, StringBuilder sb)
	{
		sb.append(e.getClass().getCanonicalName()+ ":" + e.getMessage() + "<br/>");
		for(StackTraceElement ste:e.getStackTrace())
		{
			sb.append("&#9;" + ste.toString()+"<br/>");
		}
	}
}
