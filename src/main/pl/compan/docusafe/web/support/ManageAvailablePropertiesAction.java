package pl.compan.docusafe.web.support;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.NewAvailabilityManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * ManageAvailablePropertiesAction
 * 
 * @author tomek
 * 
 */
public class ManageAvailablePropertiesAction extends EventActionSupport
{
	private Logger log = LoggerFactory.getLogger(ManageAvailablePropertiesAction.class);
	
	private List<AvailableBean> availableBean;
	private String[] key;
	private String[] homeprop;

	protected void setup()
	{
		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSave").
			append(OpenHibernateSession.INSTANCE).
			append(new Save()).append(new FillForm()).
			appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class AvailableBean
	{
		private String key;

		private AvailableBean(String key)
		{
			this.key = key;
		}

		public Boolean getHomeprop()
		{
			return NewAvailabilityManager.isAvailableOrNull(key, NewAvailabilityManager.HOME_CONFIG);
		}

		public Boolean getLicenseprop()
		{
			return NewAvailabilityManager.isAvailableOrNull(key, NewAvailabilityManager.LICENSE_CONFIG);
		}

		public Boolean getDefaultprop()
		{
			return NewAvailabilityManager.isAvailableOrNull(key, NewAvailabilityManager.DEFAULT_CONFIG);
		}

		public Boolean getProp()
		{
			return NewAvailabilityManager.isAvailableOrNull(key, null);
		}

		public String getKey()
		{
			return key;
		}
	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(final ActionEvent event)
		{
			availableBean = new ArrayList<AvailableBean>();

			for (Iterator iter = NewAvailabilityManager.getAvailablesKeysIterator(); iter.hasNext();)
			{
				availableBean.add(new AvailableBean((String) iter.next()));
			}
		}
	}

	/**
	 * klasa czysci availablesy z home aplikacji, zapisuje w nich te przekazane
	 * z formatki i resetuje availabilitymanagera co powoduje ponowna wczytanie
	 * konfiguracji juz z nowymi opcjami w home
	 * 
	 * @author tomek
	 * 
	 */
	private class Save implements ActionListener
	{
		public void actionPerformed(final ActionEvent event)
		{
			
			try
			{
				PropertiesConfiguration newCfg = new PropertiesConfiguration(new File(Docusafe.getHome()
						+ "/available.properties"));
				newCfg.clear();

				for (int i = 0; i < key.length; i++)
				{
					if (homeprop[i].length() > 0)
					{
						newCfg.setProperty(key[i], Boolean.valueOf(homeprop[i]).booleanValue());
					}
				}

				newCfg.save();
				NewAvailabilityManager.reload();
			} 
			catch (Exception e)
			{
				log.error(e.getMessage(),e);
				//e.printStackTrace();
			}
		}
	}

	public List<AvailableBean> getAvailableBean()
	{
		return availableBean;
	}

	public String[] getKey()
	{
		return key;
	}

	public void setKey(String[] key)
	{
		this.key = key;
	}

	public String[] getHomeprop()
	{
		return homeprop;
	}

	public void setHomeprop(String[] homeprop)
	{
		this.homeprop = homeprop;
	}
}
