package pl.compan.docusafe.web.support.cli;

import com.lowagie.text.html.HtmlEncoder;
import java.sql.PreparedStatement;
import org.apache.commons.lang.ArrayUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.datamart.DataMartQuery;


/**
 *
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public class DataMartReaderHandler implements CommandHandler{

	private final static String USAGE =
			HtmlEncoder.encode(
			"USAGE: \n"
		+	"(-ec|-event_code) <event code(s)> \n"
		+	"(-ecl|-event_code_like) <event code prefix>\n" );

	public CharSequence process(String arg) throws Exception {
		StringBuilder builder = new StringBuilder();

		String[] args = arg.split(" ");

		DataMartQuery query = null;
		int i;

		if(args.length < 2){
			throw new IllegalCommandArgumentsException("Nieprawidłowe argumenty \n" + USAGE);
		}
		
		if(args[0].equals("-ec") || args[0].equals("-event_code")){
			i = 1;
			while(i < args.length && !args[i].startsWith("-")) ++i;
			if( i == 2 ){
				query = DataMartQuery.forEventCode(args[1]);
			} else {
				query = DataMartQuery.forEventCodes((String[])ArrayUtils.subarray(args, 1, i));
			}
		} else if (args[0].equals("-ecl") || args[0].equals("-event_code_like")) {
			i = 1;
			while(i < args.length && !args[i].startsWith("-")) ++i;
			if( i == 2 ){
				query = DataMartQuery.forEventCodePrefix(args[1]);
			} else {

			}
		} else {
			throw new IllegalCommandArgumentsException("Nieznany argument : " + args[0] + " \n" + USAGE);
		}

//		for(; i< args.length; ++i){
//
//		}

		PreparedStatement ps = null;
		try{
			ps = query.prepareStatement();
//			builder.append("Wykonywanie kwerendy : " + ps.toString() + "\n");
			CliTools.renderAndClose(ps.executeQuery(), builder);
		} finally {
			DSApi.context().closeStatement(ps);
		}

		return builder;
	}

}


