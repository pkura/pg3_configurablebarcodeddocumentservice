package pl.compan.docusafe.web.support.cli;

/**
 * Funktor obs�uguj�cy jedno polecenie
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface CommandHandler {

	/**
	 * Obs�uga polecenia, trafia tu tylko cz�� po poleceniu
	 * @param args - argumenty polecenia
	 * @return
	 */
	CharSequence process(String args) throws Exception;
}
