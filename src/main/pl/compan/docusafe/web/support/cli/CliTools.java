package pl.compan.docusafe.web.support.cli;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import org.apache.commons.dbutils.DbUtils;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class CliTools {

	static void renderAndClose(ResultSet rs, Appendable ret) throws Exception {
		try{
			renderResultSet(rs, ret);
		} finally {
			DbUtils.closeQuietly(rs);
		}
	}

	static void renderResultSet(ResultSet rs, Appendable response) throws Exception {
		response.append("<table style=\"color:white !important; font-family:monospace !important;\">");
		ResultSetMetaData rsmd = rs.getMetaData();
		response.append("<tr>");
		for (int i = 1, max = rsmd.getColumnCount(); i <= max; ++i) {
			response.append("<th style=\"font-family:monospace !important\">");
			response.append(rsmd.getColumnName(i));
			response.append("</th>");
		}
		response.append("</tr>");

		while (rs.next()) {
			response.append("<tr>");
			for (int i = 1, max = rsmd.getColumnCount(); i <= max; ++i) {
				response.append("<td style=\"font-family:monospace !important\">");
				response.append(rs.getObject(i) + "");
				response.append("</td>");
			}
			response.append("</tr>");
		}
		response.append("</table>");
	}
}
