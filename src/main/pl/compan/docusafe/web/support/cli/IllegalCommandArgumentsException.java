package pl.compan.docusafe.web.support.cli;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class IllegalCommandArgumentsException extends IllegalArgumentException{

	private final static Logger log = LoggerFactory.getLogger(IllegalCommandArgumentsException.class);

	public IllegalCommandArgumentsException(String s) {
		super(s);
	}
}
