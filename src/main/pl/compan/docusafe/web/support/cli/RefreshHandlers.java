package pl.compan.docusafe.web.support.cli;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Obs�uga zbioru komend od�wierzaj�cych co�
 * @author Micha� Sankowski
 */
public class RefreshHandlers implements CommandHandler {

	private final static Logger log = LoggerFactory.getLogger(RefreshHandlers.class);

	private final static Map<String, CommandHandler> refreshCommands = new ConcurrentHashMap<String, CommandHandler>();
	static{
		refreshCommands.put("permissions", new RefreshPermissionHandler());
	}

	public CharSequence process(String args) throws Exception {
		String[] arguments = args.split(" ", 2);
		CommandHandler handler = refreshCommands.get(arguments[0]);
		if(handler == null){
			throw new IllegalCommandArgumentsException("");
		}
		if(arguments.length > 1){
			return handler.process(arguments[1]);
		} else {
			return handler.process("");
		}
	}
}

/**
 * Od�wie�a uprawnienia. Sk�adnia:
 * -d (lista ID dokument�w)
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class RefreshPermissionHandler implements CommandHandler{
	private final static Logger log = LoggerFactory.getLogger(RefreshPermissionHandler.class);
	public CharSequence process(String args) throws Exception {
		String[] arguments = args.split(" ");
		StringBuilder builder = new StringBuilder();
		if(arguments.length < 2){
			throw new IllegalCommandArgumentsException("Brak odpowiednich argument�w");
		}
		if(arguments[0].equals("-d")){
			refreshDocuments(arguments, builder);
		}
		return builder;
	}

	private void refreshDocuments(String[] arguments, StringBuilder out) throws EdmException {
		for(int i=1; i<arguments.length; ++i){
			DSApi.context().begin();
			try{
				Document doc = Document.find(Long.parseLong(arguments[i]));
				doc.getDocumentKind().logic().documentPermissions(doc);
				DSApi.context().commit();
				out.append("Uprawnienia dokumentu ID: " + arguments[i] + " zosta�y od�wierzone\n");
			} catch (Exception e) {
				DSApi.context().setRollbackOnly();
				log.error(e.getMessage(), e);
				out.append("Podczas od�wie�ania dokumentu ID:" + arguments[i] + " wyst�pi� wyj�tek\n");
			}
		}
	}
}
