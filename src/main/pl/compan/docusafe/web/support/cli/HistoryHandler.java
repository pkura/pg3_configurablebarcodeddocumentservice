package pl.compan.docusafe.web.support.cli;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 *
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public class HistoryHandler implements CommandHandler{

	public CharSequence process(String args) throws Exception {
		StringBuilder ret = new StringBuilder();
		String[] splited = args.split(" ", 2);
		if(splited[0].equals("-oid")){
			processObjectIds(splited[1].split(" "), ret);
		} else {
			ret.append("nieobsługiwana opcja " + splited[0]);
		}
		return ret;
	}

	private void processObjectIds(String[] string, StringBuilder ret) throws SQLException, EdmException, Exception {
		for(String oid: string){
			processObjectId(oid, ret);
		}
	}

	private void processObjectId(String oid, StringBuilder ret) throws SQLException, EdmException, Exception {
		PreparedStatement statement = null;
		try{
			statement = DSApi.context().prepareStatement(
					"select document_id, object_id, event_code, change_field_cn, old_value, new_value, username"
					+ " from ds_datamart where object_id = ?");
			statement.setString(1, oid);
			CliTools.renderAndClose(statement.executeQuery(), ret);
		} finally {
			DSApi.context().closeStatement(statement);
		}
	}
}
