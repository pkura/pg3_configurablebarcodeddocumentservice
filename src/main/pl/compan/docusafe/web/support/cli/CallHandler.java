package pl.compan.docusafe.web.support.cli;
import java.lang.reflect.Method;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class CallHandler implements CommandHandler {

	private final static Logger log = LoggerFactory.getLogger(CallHandler.class);

	public CharSequence process(String command) throws Exception {
		StringBuilder response = new StringBuilder();
		String[] cmds = command.split(" ");
		try {
			Class clazz = Class.forName(cmds[0]);
			Object instance = clazz.newInstance();
			Method method = clazz.getMethod(cmds[1]);
			Runnable thread = new TerminalThread(method, instance);
			new Thread(thread).start();
			response.append("Wywolano metode ").append(cmds[1]).append(" z klasy ").append(clazz.getCanonicalName()).append(" w nowym watku.");

		} catch (ClassNotFoundException ex) {
			response.append("Klasa nie znaleziona : " + cmds[0]);
		}
		return response;
	}

	private class TerminalThread implements Runnable
	{
		private Method method;
		private Object instance;

		public TerminalThread(Method method,Object instance)
		{
			this.method = method;
			this.instance = instance;
		}

		public void run()
		{
			try
			{
				method.invoke(instance);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(),e);
			}

		}

	}
}
