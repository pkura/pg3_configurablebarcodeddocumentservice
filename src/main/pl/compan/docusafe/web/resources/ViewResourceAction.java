package pl.compan.docusafe.web.resources;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.resources.Resource;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/* User: Administrator, Date: 2005-11-29 13:59:15 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ViewResourceAction.java,v 1.4 2009/02/28 08:11:35 wkuty Exp $
 */
public class ViewResourceAction extends EventActionSupport
{
    private Long id;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm);
            //appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult(NONE);

            if (id == null)
                return;

            File temp = null;
            String mime = null;
            String filename = null;
            int size = 0;

            try
            {
                Resource resource = Resource.find(id);
                mime = resource.getMime();
                filename = resource.getFileName();
                size = resource.getSize();

                InputStream is = resource.getBinaryStream();
                temp = File.createTempFile("docusafe_", ".tmp");
                FileOutputStream os = new FileOutputStream(temp);
                org.apache.commons.io.IOUtils.copy(is,os);
                org.apache.commons.io.IOUtils.closeQuietly(is);
                org.apache.commons.io.IOUtils.closeQuietly(os);
            }
            catch (IOException e)
            {
                event.getLog().error(e.getMessage(), e);
                return;
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
                return;
            }
            finally
            {
                DSApi._close();
            }

            if (temp != null)
            {
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType(mime);
                response.setHeader("Content-Disposition", "attachment" +
                    (filename != null ? "; filename=\""+filename+"\"" : ""));
                //response.setHeader("Content-Disposition", "inline");
                response.setContentLength((int) temp.length());
                try
                {
                    OutputStream output = response.getOutputStream();
                    FileInputStream in = new FileInputStream(temp);
                    byte[] buf = new byte[8192];
                    int count;
                    while ((count = in.read(buf)) > 0)
                    {
                        output.write(buf, 0, count);
                        output.flush();
                    }
                    output.close();
                    in.close();
                }
                catch (IOException e)
                {
                    event.getLog().error("", e);
                }
                catch (Throwable t)
                {
                    event.getLog().error("", t);
                }
                finally
                {
                    temp.delete();
                }

            }
        }
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
