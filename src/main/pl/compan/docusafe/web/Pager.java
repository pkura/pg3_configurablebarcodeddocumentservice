package pl.compan.docusafe.web;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.web.office.tasklist.TaskListAction;

/**
 * Obiekt wspomagaj�cy tworzenie listy odno�nik�w do kolejnych stron
 * wynik�w, je�eli nie mieszcz� si� one na jednej stronie.
 * <p>
 * Spos�b u�ycia znajduje si� w pliku /pager-links-include.jsp
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Pager.java,v 1.10 2009/03/13 09:34:59 pecet3 Exp $
 */
public class Pager
{
    /**
     * Wsp�lna cz�� odno�nik�w prowadz�cych do kolejnych stron wynik�w.
     * Odno�nik musi by� tak skonstruowany, by mo�na by�o do��czy� do
     * niego na ko�cu warto�� parametru offset.
     */
    private String linkBase;
    private LinkVisitor linkVisitor;
    private LinkMap linkMap = new LinkMap();
    /**
     * Offset odpowiadaj�cy odno�nikowi do pierwszej strony wynik�w.
     */
    private int beginOffset;
    /**
     * Offset odpowiadaj�cy odno�nikowi do ostatniej strony wynik�w.
     * Je�eli strona jest tylko jedna endOffset = beginOffset.
     */
    private int endOffset;
    /**
     * Liczba wynik�w na ka�dej stronie, jednocze�nie r�nica pomi�dzy
     * offsetami kolejnych stron wynik�w.
     */
    private int limit;
    /**
     * Offset bie��cej strony.
     */
    private int offset;
    /**
     * Numer strony odpowiadaj�cy pierwszemu odno�nikowi (numery stron zaczynaj�
     * si� od jednego). Zazwyczaj ma warto�� 1 z wyj�tkiem sytuacji, gdy wynik�w
     * jest tak du�o, �e nie mo�na pokaza� odno�nik�w do wszystkich stron i pokazywane
     * s� odno�niki tylko do cz�ci stron, w�wczas pierwszy odno�nik nie musi prowadzi�
     * do strony numer jeden tylko do strony numer startPage.
     */
    private int startPage; // 1-based
    /**
     * Liczba odno�nik�w do stron (co najmniej jeden).
     */
    private int linkCount;
    /**
     * Okre�la czy linki do zmiany stron s� w Javascripcie - to znaczy, �e nie b�dzie bezpo�redniego 
     * prze�adowania strony, ale po klikni�ciu na dany link Pager-a nast�pi wywo�anie funkcji w Javascripcie.
     */
    private boolean jsLinks;

    private String firstPageLink;
    private String prevPageLink;
    private String lastPageLink;
    private String nextPageLink;

    public Pager(LinkVisitor linkVisitor, int offset, int limit, int totalCount, int maxLinks, boolean jsLinks)
    {
        init(linkVisitor, offset, limit, totalCount, maxLinks, jsLinks);
    }
    
    /**
     *
     * @param linkVisitor Klasa tworz�ca odno�niki do kolejnych stron wynik�w.
     * @param offset Pocz�tkowy offset.
     * @param limit Liczba wynik�w na stronie.
     * @param totalCount Ca�kowita liczba wynik�w.
     * @param maxLinks Maksymalna liczba odno�nik�w do stron wynik�w (nieu�ywane).
     */
    public Pager(LinkVisitor linkVisitor, int offset, int limit, int totalCount, int maxLinks)
    {
        init(linkVisitor, offset, limit, totalCount, maxLinks, false);
    }

    private void init(LinkVisitor linkVisitor, int offset, int limit, int totalCount, int maxLinks, boolean jsLinks)
    {
        if (linkVisitor == null)
            throw new NullPointerException("linkVisitor");
        if (limit <= 0)
          throw new IllegalArgumentException("limit="+limit);

        
        this.linkVisitor = linkVisitor;
        this.limit = limit;
        this.offset = offset;
        this.jsLinks = jsLinks;

        // je�eli bie��ca strona (wynikaj�ca z offsetu) mie�ci si� w ramach
        // maxLinks * limit - 1: pokazujemy maxLinks link�w

        // je�eli bie��ca strona jest r�wna lub wi�ksza maxLinks:
        // pokazujemy maxLinks link�w po lewej stronie wybranej strony
        // oraz maxLinks link�w po jej prawej stronie (je�eli jest tyle wynik�w)

        int thisPage = offset/limit + 1; // numer bie��cej strony - od jednego

        if (thisPage < maxLinks)
        {
            this.beginOffset = 0;

            // ((totalCount-1) / limit) * limit
            int totalPages = (totalCount - 1) / limit + 1;
            if (totalPages > maxLinks)
            {
                this.endOffset = (maxLinks - 1) * limit;
            }
            else
            {
                this.endOffset = (totalPages - 1) * limit;
            }
            //this.endOffset = (totalCount / limit) * limit;
            //if (totalCount % limit == 0 && this.endOffset > 0)
            //    this.endOffset--;
            this.startPage = 1; // beginOffset/limit + 1
            this.linkCount = (endOffset - beginOffset) / limit + 1;
        }
        else
        {
            // maxLinks link�w po lewej stronie
            this.startPage = thisPage - maxLinks <= 0 ? 1 : thisPage - maxLinks;
            this.beginOffset = (this.startPage - 1) * limit;
            int totalPages = (totalCount - 1) / limit + 1;
            if (totalPages > thisPage + maxLinks)
            {
                this.endOffset = (thisPage + maxLinks - 1) * limit;
            }
            else
            {
                this.endOffset = (totalPages - 1) * limit;
            }
        }

        this.linkCount = (endOffset - beginOffset) / limit + 1;

        if (offset > 0)
            firstPageLink = linkVisitor.getLink(0);
        if (offset < ((totalCount - 1) / limit) * limit)
            lastPageLink = linkVisitor.getLink(((totalCount - 1) / limit) * limit);
        if (offset - limit >= 0)
            prevPageLink = linkVisitor.getLink(offset - limit);
        if (offset + limit < totalCount)
            nextPageLink = linkVisitor.getLink(offset + limit);
    }
    
    /**
     * Zwraca obiekt Map implementuj�cy wy��cznie metod� get(Object).
     * Jest to obej�cie braku mo�liwo�ci wywo�ywania funkcji w expression
     * language JSTL (standard tego nie przewiduje). Metoda get() zwracanego
     * obiektu przyjmuje argument typu Integer oznaczaj�cy offset. Warto��
     * tego obiektu przekazywana jest do metody {@link LinkVisitor#getLink(int)}.
     */
    public Map getLink()
    {
        return linkMap;
    }

    public String getLinkBase()
    {
        return linkBase;
    }

    public int getBeginOffset()
    {
        return beginOffset;
    }

    public int getEndOffset()
    {
        return endOffset;
    }

    public int getLimit()
    {
        return limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public int getStartPage()
    {
        return startPage;
    }

    public int getLinkCount()
    {
        return linkCount;
    }

    public boolean isJsLinks()
    {
        return jsLinks;
    }

    public String getFirstPageLink()
    {
        return firstPageLink;
    }

    public String getPrevPageLink()
    {
        return prevPageLink;
    }

    public String getLastPageLink()
    {
        return lastPageLink;
    }

    public String getNextPageLink()
    {
        return nextPageLink;
    }

    public interface LinkVisitor
    {
        String getLink(int offset);
    }

    class LinkMap implements Map
    {
        public Object get(Object key)
        {
            if (linkVisitor == null)
                throw new IllegalStateException("Nie przekazano obiektu LinkVisitor");
            return linkVisitor.getLink(((Integer) key).intValue());
        }

        public int size()
        {
            throw new UnsupportedOperationException();
        }

        public void clear()
        {
            throw new UnsupportedOperationException();
        }

        public boolean isEmpty()
        {
            throw new UnsupportedOperationException();
        }

        public boolean containsKey(Object key)
        {
            throw new UnsupportedOperationException();
        }

        public boolean containsValue(Object value)
        {
            throw new UnsupportedOperationException();
        }

        public Collection values()
        {
            throw new UnsupportedOperationException();
        }

        public void putAll(Map t)
        {
            throw new UnsupportedOperationException();
        }

        public Set entrySet()
        {
            throw new UnsupportedOperationException();
        }

        public Set keySet()
        {
            throw new UnsupportedOperationException();
        }

        public Object remove(Object key)
        {
            throw new UnsupportedOperationException();
        }

        public Object put(Object key, Object value)
        {
            throw new UnsupportedOperationException();
        }
    }
}
