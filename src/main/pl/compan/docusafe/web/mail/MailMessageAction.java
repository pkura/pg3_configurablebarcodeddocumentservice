package pl.compan.docusafe.web.mail;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MessageAttachment;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.util.DateUtils;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.io.*;

import com.opensymphony.webwork.ServletActionContext;
/* User: Administrator, Date: 2006-11-28 12:03:18 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: MailMessageAction.java,v 1.4 2009/01/25 19:33:50 wkuty Exp $
 */
public class MailMessageAction extends EventActionSupport
{
    private Long messageId;  /* id wiadomosci, ktora ogladamy */
    private Integer attachmentNo; /* numer zalacznika */
    private String subject;
    private String sentDate;
    private String from;
    private String sender;
    private String content;
    private List<String> recipientsTO;
    private List<String> recipientsCC;
    private List<String> recipientsBCC;
    private List<Map<String,Object>> attachments;

    protected void setup()
    {
        FillForm fillForm = new FillForm();
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        registerListener("doGetAttachment").
            append(OpenHibernateSession.INSTANCE).
            append(new GetAttachment()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                if (messageId != null)
                {
                    MailMessage mailMessage = MailMessage.find(messageId);

                    subject = mailMessage.getSubject();
                    sentDate = DateUtils.formatJsDateTime(mailMessage.getSentDate());
                    from = mailMessage.getFrom();

                    Properties props = new Properties();
                    Session session = Session.getDefaultInstance(props, null);

                    InputStream input = mailMessage.getBinaryStream();
                    MimeMessage message = new MimeMessage(session, input);
                    input.close();

                    recipientsTO = createRecipients(message, Message.RecipientType.TO);
                    recipientsCC = createRecipients(message, Message.RecipientType.CC);
                    recipientsBCC = createRecipients(message, Message.RecipientType.BCC);
                    sender = message.getSender() != null ? message.getSender().toString() : null;

                    attachments = new ArrayList<Map<String,Object>>();
                    
                    if (message.getContent() instanceof Multipart)
                    {
                        Multipart mp = (Multipart) message.getContent();

			            for (int j=0, m = mp.getCount(); j < m; j++) {
			                Part part = mp.getBodyPart(j);

			                
                            String disposition = part.getDisposition();
//                            System.out.println("sisp "+ disposition);
                            if ((disposition != null) && ((disposition.equals(Part.ATTACHMENT)) || ((disposition.equals(Part.INLINE)))))
			                { 
                                Map<String,Object> map = new HashMap<String,Object>();
                                String fileName = part.getFileName();
                                map.put("id", j); 
                                if(fileName == null){
                                	if(MessageManager.contentTypes.keySet().contains(part.getContentType().split(";")[0])){
                                		fileName = MessageManager.contentTypes.get(part.getContentType().split(";")[0]);
                                	}
                                	map.put("filename", fileName);
                                }else{
                                	map.put("filename", MimeUtility.decodeText(fileName));
                                }
                                map.put("filesize", part.getSize());
                                attachments.add(map);
                            }
                            
                            if (disposition == null && j == 0 && !(part.getContent() instanceof MimeMultipart))
                            {
                                if (part.getContent() instanceof String)
                                    content = (String) part.getContent();
                            }else if(disposition != null && disposition.equals(Part.INLINE) && part.getContent() instanceof String){
                            	//	an inline content-disposition, which means that it should be automatically displayed when the message is displayed, 
                            	//	or an attachment content-disposition, in which case it is not displayed automatically and requires some form of action from the user to open it.
                            	content = (String) part.getContent();
                            }
                        }
                    }
                    else
                    {
                        if (message.getContent() instanceof String)
                            content = (String) message.getContent();
                    }

                }
            }
            catch (IOException e)
            {
                addActionError(e.getMessage());
            }
            catch (MessagingException e)
            {
                addActionError(e.getMessage());
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private List<String> createRecipients(MimeMessage message, Message.RecipientType type)
    {
        try
        {
            Address address[] = message.getRecipients(type);
            List<String> recipients = new ArrayList<String>();
            if (address != null)
            {
                for (int i=0; i < address.length; i++)
                {                 
                    recipients.add(address[i].toString());
                }
            }
            return recipients;
        }
        catch (MessagingException e)
        {
            addActionError("Napotkano b��d w nag��wku wiadomo�ci - odbiorcy");
            return null;
        }
    }

    private class GetAttachment implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
                // do odczytu bloba trzeba otworzy� transakcj�

               /* InputStream input = null;
                Part part = null;
                if (messageId != null)
                {
                    MailMessage mailMessage = MailMessage.find(messageId);

                    Properties props = new Properties();
                    Session session = Session.getDefaultInstance(props, null);

                    MimeMessage message = new MimeMessage(session, mailMessage.getBinaryStream());

                    if (!(message.getContent() instanceof Multipart))
                        throw new EdmException("Wybrany mail nie posiada za��cznik�w");

                    Multipart mp = (Multipart) message.getContent();

                    if (attachmentNo >= mp.getCount() || attachmentNo < 0)
                        throw new EdmException("Nie istnieje za��cznik o zadanym numerze");
                    part = mp.getBodyPart(attachmentNo.intValue());

                    String disposition = part.getDisposition();
                    if ((disposition != null) &&
                        ((disposition.equals(Part.ATTACHMENT) ||
                        (disposition.equals(Part.INLINE)))))
                    {
                        input = part.getInputStream();
                    }
                    else
                        new EdmException("Nie istnieje za��cznik o zadanym numerze");

                }
                 */


                BlobInputStream stream = null;
                boolean success = false;
                File content = null;
                MessageAttachment attachment = null;
                try
                {
                    DSApi.context().begin();

                    attachment = MessageAttachment.find(messageId, attachmentNo);
                    
                    stream = attachment.getBinaryStream();

                    if (stream != null)
                    {
                        content = File.createTempFile("docusafe_doc_sig", ".tmp");
                        FileOutputStream os = new FileOutputStream(content);
                        org.apache.commons.io.IOUtils.copy(stream, os);                        
                        os.close();
                        stream.close();
                        stream = null;
                    }

                    DSApi.context().commit();

                    success = true;

                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    if (content != null) content.delete();
                    addActionError(e.getMessage());
                }
                catch (Exception e)
                {
                    DSApi.context().setRollbackOnly();
                    if (content != null) content.delete();
                    addActionError(e.getMessage());
                }
                finally
                {
                    if (stream != null) try { stream.close(); } catch (Exception e) { }
                    //DSApi._close();
                }

                if (success && content != null)
                {
                    event.setResult("view");

                    
                    HttpServletResponse response = ServletActionContext.getResponse();
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment; filename=\""+attachment.getFilename()+"\"");
                    //response.setHeader("Content-Disposition", "inline");
                    response.setContentLength(attachment.getSize()); 
                    
                    try
                    {
                        OutputStream output = response.getOutputStream();
                        FileInputStream input = new FileInputStream(content);
                        byte[] buf = new byte[8192];
                        int count;
                        while ((count = input.read(buf)) > 0)
                        {
                            output.write(buf, 0, count);
                            output.flush();
                        }
                        output.close();
                        input.close();

                    }
                    catch (IOException e)
                    {
                        event.getLog().error("", e);
                    }
                    catch (Throwable t)
                    {
                        event.getLog().error("", t);
                    }
                    finally
                    {
                        content.delete();
                    }

                }

        }
    }

    public Long getMessageId()
    {
        return messageId;
    }

    public void setMessageId(Long messageId)
    {
        this.messageId = messageId;
    }

    public void setAttachmentNo(Integer attachmentNo)
    {
        this.attachmentNo = attachmentNo;
    }

    public String getSubject()
    {
        return subject;
    }

    public String getSentDate()
    {
        return sentDate;
    }

    public String getFrom()
    {
        return from;
    }

    public String getSender()
    {
        return sender;
    }

    public String getContent()
    {
        return content;
    }

    public List<String> getRecipientsTO()
    {
        return recipientsTO;
    }

    public List<String> getRecipientsCC()
    {
        return recipientsCC;
    }

    public List<String> getRecipientsBCC()
    {
        return recipientsBCC;
    }

    public List<Map<String, Object>> getAttachments()
    {
        return attachments;
    }
}
