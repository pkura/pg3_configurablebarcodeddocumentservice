package pl.compan.docusafe.web.mail;

import pl.compan.docusafe.webwork.event.*;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MessageManager;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;

import java.util.*;

/* User: Administrator, Date: 2006-11-15 14:20:37 */

/**
 * Akcja wy�wietlaj�ca list� otrzymanych i nieprzetworzonych maili.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 * @version $Id: ViewUnprocessedMailAction.java,v 1.7 2010/04/09 11:06:41 kamilj Exp $
 */
public class ViewUnprocessedMailAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;

	/**
	 * Zmienna przetrzymuj�ca wiadomo�ci dla widoku
	 */
    private List messages;
    
    /**
     * Zmienna przetrzymuj�ca identyfikator aktualnego kana�u mailowego
     */
    private Long emailChannelId;
    
    /**
     * Mapa dla elementu select, wy�wietlaj�cego w��czone kana�y mailowe
     */
    private Map<Long, String> emailChannels = new LinkedHashMap<Long, String>();
    
    /**
     * Lista z identyfikatorami wiadomo�ci, kt�re maj� zosta� oznaczone jako processed = true
     */
    private Long[] deleteIds;

    /**
     * Konfiguracja akcji, rejestracja obiekt�w nas�uchuj�cych
     */
    protected void setup()
    {
        FillForm fillForm = new FillForm();
        // rejestracja akcji wype�niaj�cej formularz danymi
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        // rejestracja akcji usuwaj�cej wybrane wiadomo�ci 
        //(oznaczenie wiadomo�ci jako processed = true)
        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        // rejestracja akcji ustawiaj�cej wiadomo�ci z wybranego kana�u mailowego
        registerListener("doSetMessages").
        	append(OpenHibernateSession.INSTANCE).
        	append(new DoSetMessages()).
        	appendFinally(CloseHibernateSession.INSTANCE);
        	
    }
    
    /**
     * Wype�nienie formularza danymi.
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	DSApi.context().begin();
            	// pobranie i wstawienie do mapy w��czonych kana��w mailowych
            	List<DSEmailChannel> channelList = loadEnabledEmailChannels();
            	// ustawienie identyfikatora kana�u mailowego
            	if (channelList.size() > 0 && emailChannelId == null)
            	{
            		emailChannelId = channelList.get(0).getId();
            		messages = getUnprocessedMessages();
            	}
            	
                DSApi.context().commit();
            }
            catch (EdmException e)
            {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }
    
    /**
     * Akcja usuwaj�ca zaznaczone przez u�ytkownika wiadomo�ci
     * (oznaczenie wiadomo�ci jako processed = true)
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteIds != null)
            {
                try
                {
                	// otworzenie transakcji
                    DSApi.context().begin();
                    
                    // usuni�cie wszystkich zaznaczonych wiadomo�ci
                    for (Long id : deleteIds)
                    {
                        MailMessage.find(id).setProcessed(Boolean.TRUE);
                    }
                    
                    // pobranie wiadomo�ci dla widoku
                    messages = getUnprocessedMessages();
                    
                    // zatwierdzenie transakcji
                    DSApi.context().commit();
                }
                catch (EdmException e)
                {
                    DSApi.context().setRollbackOnly();
                    addActionError(e.getMessage());
                }
            }
        }
    }
    
    /**
     * Ustawienie wiadomo�ci dla wybranego kana�u mailowego.
     * 
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private class DoSetMessages implements ActionListener
    {
    	public void actionPerformed(ActionEvent event) 
    	{
    		try 
    		{
    			// otworzenie transakcji
    			DSApi.context().begin();
    			// pobranie nieprzetworzonych wiadomo�ci 
    			messages = getUnprocessedMessages();
    			// za�adowanie w��czonych kana��w mailowych dla widoku
    			loadEnabledEmailChannels();
    			// zatwierdzenie transakcji
    			DSApi.context().commit();
    		}
    		catch (EdmException e)
    		{
    			DSApi.context().setRollbackOnly();
    			addActionError(e.getMessage());
    		}
    		
    	}
    }
    
    /**
     * Pobranie wszystkich wiadomo�ci z danego kana�u mailowego, kt�re
     * posidaj� w�a�ciwo�� processed = false.
     * 
     * @return
     * @throws EdmException
     */
    private List<MailMessage> getUnprocessedMessages() 
    	throws EdmException
    {
    	if (emailChannelId == null)
    		return null;
    	DSEmailChannel emailChannel = (DSEmailChannel) Finder.find(DSEmailChannel.class, emailChannelId);
    	// wyszukanie wiadomo�ci z danego kana�u mailowego
    	return MailMessage.getUnprocessedMessagesFromEmailChannel(emailChannel);
    }
    
    /**
     * �aduje w��czone kana�y mailowe.
     * 
     * @return
     * @throws EdmException
     */
    private List<DSEmailChannel> loadEnabledEmailChannels()
    	throws EdmException
    {
    	// pobranie dost�pnych kana��w i zapisanie ich do mapy w�a�ciwo�ci
    	List<DSEmailChannel> channelList = MessageManager.getEnabledChannels();
    	DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
    	for (DSEmailChannel channel : channelList)
    	{
    		if(AvailabilityManager.isAvailable("UTP.wlacz.przypiszKanalEmail")){
    			if(user.getName().equals(channel.getUserLogin())){
    				emailChannels.put(channel.getId(), channel.getKolejka());
    			} else {
    				addActionMessage("Brak przypisnaych kana��w email");
    			}
    		} else {
    			emailChannels.put(channel.getId(), channel.getKolejka());
    		}
    	}
    	return channelList;
    }
    
    /**
     * Metoda zwracaj�ca wiadomo�ci z danego kana�u mailowego dla widoku
     * 
     * @return
     */
    public List getMessages()
    {
        return messages;
    }

    /**
     * Metoda ustawiaj�ca identyfikatory wiadomo�ci do usuni�cia.
     * 
     * @param deleteIds
     */
    public void setDeleteIds(Long[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }
    
    /**
     * Metoda ustawiaj�ca identyfikator wybranego kana�u mailowego
     * 
     * @param emailChannelId
     */
	public void setEmailChannelId(Long emailChannelId) 
	{
		this.emailChannelId = emailChannelId;
	}

	/**
	 * Metoda zwracaj�ca identyfikator aktualnie wybranego kana�u mailowego
	 * 
	 * @return
	 */
	public Long getEmailChannelId() 
	{
		return emailChannelId;
	}
	
	/**
	 * Metoda zwracaj�ca map� w��czonych kana��w mailowych dla elementu select z widoku.
	 * 
	 * @return
	 */
	public Map<Long, String> getEmailChannels()
	{
		return emailChannels;
	}
}
