package pl.compan.docusafe.web.userinfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * Klasa abstrakcyjna dla klas implementuj�cych akcje dla zarz�dzania kontaktami.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
abstract public class UserInfoAbstractAction extends EventActionSupport 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserInfoAbstractAction.class);
	
	/**
	 * Za�adowanie �r�de� tekstowych z komunikatami z bierz�cego pakietu
	 */
	protected static StringManager sm = 
		StringManager.getManager(UserInfoAbstractAction.class.getPackage().getName());
	
	/**
	 * Sk�adowa oznaczaj�ca, �e wyst�pi� b��d w polach formularza
	 */
	protected static final String FORM_FIELDS_ERROR = "FORM_FIELDS_ERROR";
	
	/**
	 * Sk�adowa przechowuj�ca zak�adki dla widoku w kontaktach
	 */
	private static final Map<String, String> INFO_TABS = new LinkedHashMap<String, String>();
	
	private static final Map<String, String> EDITOR_TABS = new LinkedHashMap<String, String>();
	
	/**
	 * Przypisanie warto�ci dla mapy zak�adek
	 */
	static
	{
		INFO_TABS.put("/help/user-info.action", sm.getString("PodsumowanieProfilu"));
		INFO_TABS.put("/help/user-additional-info.action", sm.getString("DaneDodatkoweProfilu"));
		INFO_TABS.put("/help/user-location.action", sm.getString("LokalizacjaPracownika"));
		INFO_TABS.put("/help/user-billing.action", sm.getString("BilingiProfilu"));
		LOG.error("tabstest: " + sm.getString("DaneDodatkoweProfilu"));
		//INFO_TABS.put("/help/user-docs.action", sm.getString("DokumentyProfilu"));
		INFO_TABS.put("/help/user-absences.action", sm.getString("UrlopyPracownika"));
		INFO_TABS.put("/help/user-sites.action", sm.getString("IndywidualneStronyProfilu"));
		
		EDITOR_TABS.put("/help/edit-user-info.action", sm.getString("EdycjaProfilu"));
		EDITOR_TABS.put("/help/edit-user-supervisor.action", sm.getString("EdycjaPrzelozonego"));
		EDITOR_TABS.put("/help/edit-user-location.action", sm.getString("EdycjaLokalizacji"));
	}
	
	/**
	 * Sk�adowa przechowuj�ca list� zak�adek dost�pnych w widoku w kontaktach
	 */
	private List<Tab> tabs;
	
	/**
	 * Sk�adowa przechowuj�ca identyfikator aktualnej zak�adki
	 */
	private int tid;
	
	/**
	 * Je�li warto�� tej sk�adowej to true s� ustawiane zak�adki edycyjne
	 */
	private boolean eid = false;
	
	/**
	 * Sk�adowa przechowuj�ca obiekt u�ytkownika
	 */
	private UserImpl userImpl;
	
	/**
	 * Sk�adowa przechowuj�ca obiekt klasy �aduj�cej zak�adki dla widoku
	 */
	protected LoadTabs loadTabs;
	
	/**
	 * Sk�adowa przechowuj�ca informacj� o tym czy dany profil mo�e by� edytowany
	 */
	private boolean editable;
	
	/**
	 * Sk�adowa przechowuj�ca informacj� o czy jest na w�asnym profilu
	 */
	private boolean userProfile;

	/**
	 * Sk�adowa przechowuj�ca obiekt klasa CheckPermissions @see {@link CheckPermissions}
	 */
	protected CheckPermissions checkPermissions;
	
	/**
	 * Klasa implementuj�ca akcj� zarz�dzania zak�adkami w widoku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	class LoadTabs implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			if (userImpl == null)
				userImpl = new UserImpl();
			
			if(userImpl.getName() == null)
				userImpl.setName(DSApi.context().getPrincipalName());
			
			tabs = new ArrayList<Tab>();
			int i = 0;
			
			Set<Entry<String, String>> entrySet;
			if (isEid())
				entrySet = EDITOR_TABS.entrySet();
			else
				entrySet = INFO_TABS.entrySet();
				
			for (Entry<String, String> entry : entrySet)
			{
//				if("/help/user-billing.action".equals( entry.getKey()) && !editable)
				if("/help/user-billing.action".equals( entry.getKey()) && !AvailabilityManager.isAvailable("user_billings"))
					continue;
//				if("/help/user-absences.action".equals( entry.getKey()) && !editable)
				if("/help/user-absences.action".equals( entry.getKey()) && !AvailabilityManager.isAvailable("user_absences"))
					continue;
				if("/help/user-additional-info.action".equals(entry.getKey()) && (!AvailabilityManager.isAvailable("user_additional_data") || !DSApi.context().getPrincipalName().equals(userImpl.getName()))) continue;
				boolean active = (tid == i) ? true : false;
				tabs.add(new Tab(entry.getValue(), 
								 entry.getValue(), 
								 entry.getKey() + "?tid=" + i + "&userImpl.name=" + userImpl.getName() 
								 				+ (isEid() ? "&eid=true" : ""), 
								 active));
				i++; 
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� sprawdzenia uprawnie� u�ykownika do edycji profilu
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	protected class CheckPermissions implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				if (userImpl == null)
					userImpl = new UserImpl();
				
				if(userImpl.getName() == null)
					userImpl.setName(DSApi.context().getPrincipalName());
				// sprawdzenie uprawnie�
				editable = hasPermissions();
				userProfile = DSApi.context().getPrincipalName().equals(userImpl.getName());
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				editable = false;
			}
		}
		
		/**
		 * Sprawdza uprawnienia u�ytkownika
		 * 
		 * @return
		 * @throws EdmException
		 */
		private boolean hasPermissions() throws EdmException
		{
			if (userImpl == null)
			{
				return false;
			}
			
			// sprawdzenie czy dany u�ytkownik ma prawo do edycji w�asnego profilu
			if (DSApi.context().hasPermission(DSPermission.KONTAKTY_EDYCJA_WLASNEGO)
					&& DSApi.context().getPrincipalName().equals(userImpl.getName())) 
			{
				return true;
			}
			// sprawdza czy dany u�ytkownik ma prawa do edycji wszystkich profili
			if (DSApi.context().hasPermission(DSPermission.KONTAKTY_EDYCJA_WSZYSTKICH))
			{
				return true;
			}
			// sprawdzenie czy dany u�ytkownik ma prawa do edycji profili w dziale
			if (DSApi.context().hasPermission(DSPermission.KONTAKTY_EDYCJA_W_DZIALE))
			{
				// sprawdzenie czy wybrany profil znajduje si� w dziale u�ytkownika
				for (DSDivision division : DSApi.context().getDSUser().getDivisions())
				{
					if (userImpl.inDivision(division))
					{
						return true;
					}
				}
			}
			
			return false;
		}
	}
	
	/**
	 * Metoda zwracaj�ca sk�adow� loadTabs
	 * 
	 * @return
	 */
	public LoadTabs getLoadTabsObj()
	{
		if (loadTabs == null)
			loadTabs = new LoadTabs();
		return loadTabs;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� tabs
	 * 
	 * @return
	 */
	public List<Tab> getTabs() 
	{
		return tabs;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� tabs
	 * 
	 * @param tabs
	 */
	public void setTabs(List<Tab> tabs) 
	{
		this.tabs = tabs;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� tid
	 * 
	 * @return
	 */
	public int getTid() 
	{
		return tid;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� tid
	 * 
	 * @param tid
	 */
	public void setTid(int tid) 
	{
		this.tid = tid;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� eid
	 * 
	 * @return
	 */
	public boolean isEid() 
	{
		return eid;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� eid
	 * 
	 * @param eid
	 */
	public void setEid(boolean eid) 
	{
		this.eid = eid;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� userImpl
	 * 
	 * @return
	 */
	public UserImpl getUserImpl() 
	{
		return userImpl;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� userImpl
	 * 
	 * @param user
	 */
	public void setUserImpl(UserImpl user) 
	{
		this.userImpl = user;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� editable
	 * 
	 * @return
	 */
	public boolean isEditable() 
	{
		return editable;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� editable
	 * 
	 * @param editable
	 */
	public void setEditable(boolean editable) 
	{
		this.editable = editable;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� checkPermissions
	 * 
	 * @return
	 */
	public CheckPermissions getCheckPermissionsObj() 
	{
		if (checkPermissions == null)
			checkPermissions = new CheckPermissions();
		return checkPermissions;
	}

	/**
	 * Metoda sprawdzaj�ca czy dany u�ytkownik jest zalogowany
	 * 
	 * @return
	 */
	public boolean isLoggedIn() 
	{
		if (userImpl == null)
			return false;
		
		boolean ret = false;
		
		// przegl�d po wszystkich sesjach czy dany u�ytkownik jest w danej chwili zalogowany
		for(Iterator<HttpSession> iter = Docusafe.getSessions().iterator(); iter.hasNext();) 
		{
			HttpSession session = (HttpSession) iter.next();
			Subject sub = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
			Set<UserPrincipal> userPrincipals = sub.getPrincipals(UserPrincipal.class);
		
			if(userPrincipals.size() > 0) 
			{
				UserPrincipal up = (UserPrincipal) userPrincipals.iterator().next();
				
				if(up != null) 
				{
					if(up.getName().equals(userImpl.getName()))
						return true;
				}
			}
		}
		
		return ret;
	}

	public boolean isUserProfile() {
		return userProfile;
	}

	public void setUserProfile(boolean userProfile) {
		this.userProfile = userProfile;
	}
}
