package pl.compan.docusafe.web.userinfo;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Klasa OrganizationAction.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class OrganizationAction extends EventActionSupport 
{
	private Logger log = LoggerFactory.getLogger(OrganizationAction.class);
	private String divisionGuid;
	private String htmlTree;

	@Override
	protected void setup() 
	{
		
	        FillForm fillForm = new FillForm();

	        registerListener(DEFAULT_ACTION).
	            append(OpenHibernateSession.INSTANCE).
	            append(fillForm).
	            appendFinally(CloseHibernateSession.INSTANCE);
	    }
	
    private class FillForm implements ActionListener
    {
        

		public void actionPerformed(ActionEvent event)
        {
        	try
        	{
	            DSDivision division = null;
	            if (StringUtils.isEmpty(divisionGuid)) 
	            	divisionGuid = DSDivision.ROOT_GUID;

				try 
				{
					division = DSDivision.find(divisionGuid);
				}
				catch (DivisionNotFoundException e) 
				{
					division = DSDivision.find(DSDivision.ROOT_GUID);
				}
				generateHtmlTree(division, DSApi.context().getDSUser());
        	}
        	catch (Exception e)
        	{
				log.error(e.getMessage(),e);
			}
        }
    }
    
	/**
	 * Metoda ustawiaj�ca do sk�adowej htmlTree wygenerowane drzewo z dzia�ami i pracownikami organizacji,
	 * z kt�rego mo�na wybra� prze�o�onego dla danego pracownika
	 * 
	 * @param division
	 * @param userSupervisor
	 * @throws EdmException
	 */
	private void generateHtmlTree(DSDivision division,DSUser user) throws EdmException
	{
		// utworzenie obiektu klasy generuj�cej odpowiedni link przypisany do danego dzia�u w drzewie
		OrganizationUrlProvider provider = new OrganizationUrlProvider() 
		{
			public String getUrl(DSDivision div, DSUser user) 
			{
				return ServletActionContext.getRequest().getContextPath()				
				+"/help/organization.action"
				+(div.getGuid() != null ? "?divisionGuid=" + div.getGuid() + "#" + div.getGuid() : "");
				
				
			}
		};
		// wygenerowanie drzewa z dzia�ami i u�ytkownikami
		htmlTree = ExtendedOrganizationTree.createOrganizationTreeViewToContacts(division, user, provider, 
        		ServletActionContext.getRequest().getContextPath())
        	.generateTree();
	}

	public String getDivisionGuid() {
		return divisionGuid;
	}

	public void setDivisionGuid(String divisionGuid) {
		this.divisionGuid = divisionGuid;
	}

	public String getHtmlTree() {
		return htmlTree;
	}

	public void setHtmlTree(String htmlTree) {
		this.htmlTree = htmlTree;
	}


}
