package pl.compan.docusafe.web.userinfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.bilings.UserPhoneNumber;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa akcji do wy�wietlania i filtrowania biling�w u�ytkownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class UserPhoneNumbersAction extends UserInfoAbstractAction
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserPhoneNumbersAction.class);
	private List<DSUser> users;
	private Map<String,String> usernames  = new HashMap<String, String>();
	private String phoneNumber;
	private String username;
	private Long[] ids;
	private List<UserPhoneNumber> numbers;
	private Long id;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doConnect")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Connect())
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
		registerListener("doReconnect")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Reconnect())
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
		registerListener("doDelete")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Delete())
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{		
				users  = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				for(DSUser user : users)
				{
					usernames.put(user.getName(), user.asLastnameFirstname());
				}
				numbers = UserPhoneNumber.search(username,phoneNumber);
				
			}
			catch (Exception ex)
			{
				addActionError(ex.getMessage());
				LOG.info(ex.getMessage(), ex);
			}
		}
	}
	
	private class Delete implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				if(ids == null || ids.length < 1)
					throw new EdmException("Nie wybrano �adnego numeru");
				
				for(Long pId : ids)
				{
					UserPhoneNumber upn = UserPhoneNumber.find(pId);
					upn.delete();
				}
				username = null;
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				LOG.error(e.getMessage(),e);
				addActionError("B��d : "+e.getMessage());
				DSApi.context()._rollback();
			}
		}
	}
	
	
	private class Reconnect implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				if(ids == null || ids.length < 1)
					throw new EdmException("Nie wybrano �adnego numeru");
				
				for(Long pId : ids)
				{
					UserPhoneNumber upn = UserPhoneNumber.find(pId);
					upn.setUsername(null);
				}
				username = null;
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				LOG.error(e.getMessage(),e);
				addActionError("B��d : "+e.getMessage());
				DSApi.context()._rollback();
			}
		}
	}
	
	private class Connect implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try
			{
				DSApi.context().begin();
				if(ids == null || ids.length < 1)
					throw new EdmException("Nie wybrano �adnego numeru");
				if(username == null || username.length() < 1)
					throw new EdmException("Nie wybrano u�ytkownika");
				
				for(Long pId : ids)
				{
					UserPhoneNumber upn = UserPhoneNumber.find(pId);
					upn.setUsername(username);
				}
				username = null;
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				LOG.error(e.getMessage(),e);
				addActionError("B��d : "+e.getMessage());
				DSApi.context()._rollback();
			}
		}
	}

	public String getUserLastnameFirstname(String name) 
	{
		return usernames.get(name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public List<UserPhoneNumber> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<UserPhoneNumber> numbers) {
		this.numbers = numbers;
	}
	
}