package pl.compan.docusafe.web.userinfo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.bilings.Biling.PhoneTypes;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.TimePeriod;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa akcji do wyświetlania i filtrowania bilingów użytkownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class BilingReportsAction extends UserInfoAbstractAction
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BilingReportsAction.class);
	
	/**
	 * Zakres dat
	 */
	private TimePeriod period;

	private String type;
	
	private String limit;
	
	private List<ReportBean> result = new ArrayList<ReportBean>();
	
	/**
	 * Typy telefonów
	 */
	private Map<String, String> phoneTypes;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSearch")
			.append(OpenHibernateSession.INSTANCE)
			.append(new Search())
			.append(new FillForm())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{		

			}
			catch (Exception ex)
			{
				addActionError(ex.getMessage());
				LOG.info(ex.getMessage(), ex);
			}
		}
	}
	
	private class Search implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{		
				if(period == null || period.getStartDate() == null || period.getEndDate() == null)
					throw new EdmException("Nie wybrano zakresu dat");
				if("MOBILE".equals(type))
				{
					LOG.debug("wyszukiwanie po MOBILE");
					ps = DSApi.context().prepareStatement("select sum(netto), u.name ,u.limitMobile from ds_biling b1, ds_user u where " +
					" u.DELETED = 0 and b1.phone_type = ? and b1.phone_number_from = u.MOBILE_PHONE_NUM and b1.date_time > ? and b1.date_time < ? " +
					"GROUP by u.name, u.limitMobile having sum(b1.netto) >  ( select u2.limitMobile from ds_user u2 where u2.name = u.name) ");
				}
				else if ("OFFICE".equals(type))
				{
					ps = DSApi.context().prepareStatement("select sum(netto), u.name from ds_biling b1, ds_user u where " +
						"u.DELETED = 0 and b1.phone_type = ? and b1.phone_number_from = u.phone_num and b1.date_time > ? and b1.date_time < ? GROUP by u.name having sum(b1.netto) > ?");
				}
				else if ("PRIVATE".equals(type))
				{
					ps = DSApi.context().prepareStatement("select sum(netto), u.name from ds_biling b1, ds_user u where " +
						"u.DELETED = 0 and b1.phone_type = ? and ( b1.phone_number_from = u.phone_num or b1.phone_number_from = u.MOBILE_PHONE_NUM)" +
						"and b1.date_time > ? and b1.date_time < ? GROUP by u.name having sum(b1.netto) > ?");
				}
				else
				{
					ps = DSApi.context().prepareStatement("select sum(netto), u.name from ds_biling b1, ds_user u where " +
							"u.DELETED = 0 and ( b1.phone_number_from = u.phone_num or b1.phone_number_from = u.MOBILE_PHONE_NUM)" +
							"and b1.date_time > ? and b1.date_time < ? GROUP by u.name having sum(b1.netto) > ?");
				}
				int index = 1;
				if("MOBILE".equals(type) || "OFFICE".equals(type) || "PRIVATE".equals(type))
				{
					ps.setString(index, type);
					index ++;
				}
				ps.setDate(index, new java.sql.Date(DateUtils.parseDateAnyFormat(period.getStartDate()).getTime()));
				index ++;
				ps.setDate(index, new java.sql.Date(DateUtils.parseDateAnyFormat(period.getEndDate()).getTime()));
				index++;
				if(!"MOBILE".equals(type))
				{
					if(limit == null || limit.length() < 1)
						limit = "0";
					ps.setInt(index, Integer.parseInt(limit));
					index++;
				}
				rs = ps.executeQuery();
				Double l = 0.0;
				try
				{
					l = Double.parseDouble(limit);
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				while (rs.next())
				{
					
					if("MOBILE".equals(type))
						l = rs.getDouble(3);		

					result.add(new ReportBean(rs,l));
				}
			}
			catch (Exception ex)
			{
				addActionError(ex.getMessage());
				LOG.info(ex.getMessage(), ex);
			}
			finally
			{
				
			}
		}
	}
	
	private class ReportBean
	{
		private String user;
		private Double netto;
		private Double limit;
		private Double exceed;
		
		public ReportBean(ResultSet rs,Double limit) throws Exception 
		{
			this.netto = rs.getDouble(1);
			this.user = DSUser.findByUsername(rs.getString(2)).asLastnameFirstname();
			this.limit = limit;
			this.exceed = netto - limit;
			LOG.debug("ReportBean: {} - {}", this.netto, this.user );
		}
		public Double getNetto() {
			return netto;
		}
		public void setNetto(Double netto) {
			this.netto = netto;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public Double getExceed() {
			return exceed;
		}
		public void setExceed(Double exceed) {
			this.exceed = exceed;
		}
		public Double getLimit() {
			return limit;
		}
		public void setLimit(Double limit) {
			this.limit = limit;
		}
		
	}

	public Map<String, String> getPhoneTypes() 
	{
		phoneTypes = new LinkedHashMap<String, String>();
		phoneTypes.put("", "wszystkie");
		phoneTypes.put(PhoneTypes.OFFICE.name(), PhoneTypes.OFFICE.getName());
		phoneTypes.put(PhoneTypes.MOBILE.name(), PhoneTypes.MOBILE.getName());
		phoneTypes.put(PhoneTypes.PRIVATE.name(), PhoneTypes.PRIVATE.getName());
		return phoneTypes;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public TimePeriod getPeriod() {
		return period;
	}

	public void setPeriod(TimePeriod period) {
		this.period = period;
	}

	public List<ReportBean> getResult() {
		return result;
	}

	public void setResult(List<ReportBean> result) {
		this.result = result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setPhoneTypes(Map<String, String> phoneTypes) {
		this.phoneTypes = phoneTypes;
	}
}