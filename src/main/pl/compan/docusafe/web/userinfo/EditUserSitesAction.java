package pl.compan.docusafe.web.userinfo;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa akcji zapisująca informacje o użytkowniku, powiązana jest z widokiem /help/user-sites.jsp
 * 
 * @author  <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditUserSitesAction extends LoadUserInfoAction 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditUserSitesAction.class);

	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		super.setup();
		
		registerListener("saveUserSites")
			.append(OpenHibernateSession.INSTANCE)
			.append(getLoadTabsObj())
			.append(getCheckPermissionsObj())
			.append(new SaveUserSites())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Akcja odpowiedzialna za zapisania danych o użytkowniku
	 * 
	 * @author  <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class SaveUserSites implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (getUserImpl() == null && getUserImpl().getName() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));
				
				DSApi.context().session().createQuery("update " + UserImpl.class.getName()
						+ " usr set usr.aboutMe = ?, usr.sites = ? where usr.name = ?")
						.setString(0, getUserImpl().getAboutMe())
						.setString(1, getUserImpl().getSites())
						.setString(2, getUserImpl().getName())
						.executeUpdate();
				
				addActionMessage(sm.getString("PoprawnieZapisanoZmiany"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
}
