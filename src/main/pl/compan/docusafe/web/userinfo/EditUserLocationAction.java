package pl.compan.docusafe.web.userinfo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.address.Address;
import pl.compan.docusafe.core.address.Floor;
import pl.compan.docusafe.core.address.UserLocation;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje edycji lokalizacji danego u�ytkownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditUserLocationAction extends UserInfoAbstractAction 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditUserLocationAction.class);
	
	/**
	 * Mapa dla elementu select z nazwami dost�pnych lokalizacji organizacji
	 */
	private Map<Long, String> organizations;
	
	/**
	 * Mapa dla elementu selecet z nazwami dost�pnych pi�ter w wybranej lokalizacji
	 */
	private Map<Long, String> floors;
	
	/**
	 * Identyfikator wybranej lokalizacji organizacji
	 */
	private Long selectedOrganizationId;
	
	/**
	 * Identyfikator wybranego pi�tra
	 */
	private Long selectedFloorId;
	
	/**
	 * Lokalizacja u�ytkownika
	 */
	private UserLocation userLocation;
	
	/**
	 * Obraz wybranego pi�tra
	 */
	private Image floorImage;

	/**
	 * Metoda konfiguracyjna
	 */
	@Override
	protected void setup() 
	{
		CheckUserLocation checkUserLocation = new CheckUserLocation();
		LoadOrganizations loadOrganizations = new LoadOrganizations();
		LoadOrganizationFloors loadOrganizationFloors = new LoadOrganizationFloors();
		LoadFloorImage loadFloorImage = new LoadFloorImage();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			
			.append(checkUserLocation)
			.append(loadOrganizations)
			.append(loadOrganizationFloors)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("loadFloorImage")
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			
			.append(loadOrganizations)
			.append(loadOrganizationFloors)
			.append(loadFloorImage)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("saveUserLocation")
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			
			.append(loadOrganizations)
			.append(loadOrganizationFloors)
			.append(loadFloorImage)
			.append(new SaveUserLocation())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Sprawdza i ustawia obecn� lokalizacj� u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class CheckUserLocation implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				if (selectedOrganizationId != null || selectedFloorId != null)
					return;
				
				DSApi.context().begin();
				
				UserImpl user = getUserImpl();
				if (user != null && user.getName() != null)
				{
					user = (UserImpl) UserImpl.findByUsername(user.getName());
					setUserImpl(user);
				}
				if (getUserImpl() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));
				
				userLocation = getUserImpl().getUserLocation();
				if (userLocation != null)
				{
					Floor floor = userLocation.getFloor();
					selectedFloorId = floor.getId();
					selectedOrganizationId = floor.getAddress().getId();
					floorImage = floor.getImage();
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcje za�adowania mapy z nazwami organizacji
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadOrganizations implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				organizations = new LinkedHashMap<Long, String>();
				
				// pobranie nazw adres�w lokalizacji i numer�w identyfikacyjnych
				String sql = "select adr.id, adr.name, adr.organization from " + Address.class.getName()
					+ " adr order by adr.id asc";
				List results = DSApi.context().session().createQuery(sql).list();
				for (int i = 0; i < results.size(); i++)
				{
					Object[] row = (Object[]) results.get(i);
					organizations.put((Long) row[0], (String) row[1] + " - " + (String) row[2]);
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� wczytania pi�ter wybranej lokalizacji 
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadOrganizationFloors implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				List results = null;
				if (selectedOrganizationId == null)
				{
					Address address = (Address) DSApi.context().session().createQuery("from " + Address.class.getName() 
										+ " a order by a.id asc")
										.setMaxResults(1)
										.uniqueResult();
					if (address != null)
					{
						// wybranie listy pi�ter z pierwszego adresu zapisanego w bazie danych
						results = DSApi.context().session().createQuery(
										  "select flr.id, flr.name, flr.description from " + Floor.class.getName() + " flr "
										  + " where flr.address = ?")
										.setParameter(0, address)
										.list();
					}
				}
				else
				{
					Address address = (Address) DSApi.context().session().createQuery("from " + Address.class.getName() 
												+ " a where a.id = ?")
												.setLong(0, selectedOrganizationId)
												.uniqueResult();
					if (address != null)
					{
						// wybranie listy pi�ter z podanego identyfikatora adresu
						results = DSApi.context().session().createQuery(
										  "select flr.id, flr.name, flr.description from " + Floor.class.getName() + " flr "
										+ " where flr.address = ?")
										.setLong(0, selectedOrganizationId)
										.list();
					}
				}
				
				if (results == null || results.size() == 0)
					throw new EdmException(sm.getString("BrakPieterWLokalizacji"));

				// wczytanie nazwy pi�tra i jego identyfikatora do mapy
				floors = new LinkedHashMap<Long, String>();
				for (int i = 0; i < results.size(); i++)
				{
					Object[] row = (Object[]) results.get(i);
					floors.put((Long) row[0], (String) row[1] + " - " + (String) row[2]);
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� wczytania i przekazania obrazu pi�tra dla widoku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadFloorImage implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (selectedFloorId == null)
					throw new EdmException(sm.getString("NieprawidlowyIdentyfikatorPietra"));
				
				// wyszukanie obrazu pietra
				floorImage = (Image) DSApi.context().session().createQuery(
						"select f.image from " + Floor.class.getName() + " f where f.id = ?")
						.setLong(0, selectedFloorId)
						.uniqueResult();
				
				if (floorImage == null)
					throw new EdmException(sm.getString("NieZnalezionoObrazuPietra"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcje ustawienia lokalizacji u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class SaveUserLocation implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
			DSApi.context().begin();
				
				if (userLocation == null)
					throw new EdmException(sm.getString("NieWybranoPunktowLokalizacyjnych"));
				
				if (selectedFloorId == null)
					throw new EdmException(sm.getString("BrakWybranegoPietra"));
				
				// ustawienie pi�tra dla lokalizacji u�ytkownika
				userLocation.setFloor(Floor.find(selectedFloorId));
				
				if (userLocation.getId() != null)
				{
					// update pozycji
					DSApi.context().session().merge(userLocation);
				}
				else
				{
					// zapisanie nowej pozycji
					DSApi.context().session().save(userLocation);
					DSApi.context().session().flush();
					DSApi.context().session().refresh(userLocation);
				}
				Long locId = userLocation.getId();
				DSApi.context().session().createQuery("update " + UserImpl.class.getName() 
						+ " usr set usr.userLocation = ? where usr.name = ? ")
						.setLong(0,locId )
						.setString(1, getUserImpl().getName())
						.executeUpdate();
				
				addActionMessage(sm.getString("PoprawnieZapisanoNowaLokalizacje"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Metoda zwracaj�ca sk�adow� organizations
	 * 
	 * @return
	 */
	public Map<Long, String> getOrganizations() 
	{
		return organizations;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� floors
	 * 
	 * @return
	 */
	public Map<Long, String> getFloors() 
	{
		return floors;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� selectedOrganizationId
	 * 
	 * @return
	 */
	public Long getSelectedOrganizationId() 
	{
		return selectedOrganizationId;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� selectedOrganizationId
	 * 
	 * @param selectedOrganizationId
	 */
	public void setSelectedOrganizationId(Long selectedOrganizationId) 
	{
		this.selectedOrganizationId = selectedOrganizationId;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� selectedFloorId
	 * 
	 * @return
	 */
	public Long getSelectedFloorId() 
	{
		return selectedFloorId;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� selectedFloorId
	 * 
	 * @param selectedFloorId
	 */
	public void setSelectedFloorId(Long selectedFloorId) 
	{
		this.selectedFloorId = selectedFloorId;
	}

	public UserLocation getUserLocation() 
	{
		return userLocation;
	}

	/**
	 *
	 * 
	 * @param userLocation
	 */
	public void setUserLocation(UserLocation userLocation) 
	{
		this.userLocation = userLocation;
	}

	/**
	 * Metoda zwraca sk�adow� floorImage
	 * 
	 * @return
	 */
	public Image getFloorImage() 
	{
		return floorImage;
	}

	/**
	 * Metoda ustawia sk�adow� floorImage
	 * 
	 * @param floorImage
	 */
	public void setFloorImage(Image floorImage) 
	{
		this.floorImage = floorImage;
	}
}
