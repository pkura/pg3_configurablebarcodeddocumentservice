package pl.compan.docusafe.web.userinfo;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.users.sql.UserAddress;
import pl.compan.docusafe.core.users.sql.UserData;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.userinfo.LoadUserInfoAction.LoadUser;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

public class UserAdditionalInfoAction extends UserInfoAbstractAction {

	private static final Logger log = LoggerFactory.getLogger(UserAdditionalInfoAction.class);
	
	
	@Override
	protected void setup() 
	{
		
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new LoadUser())
//			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	/*
	 * Lista adresów użytkownika widoczna w zakładce Dane dodatkowe (wybranego użytkownika)
	 */
	private Set<UserAddress> adresy;
	private Set<UserData> dane;
	private UserData single;
	private String workDiv = "xxx";
	
	protected class LoadUser implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
//				DSApi.context().begin();
//				isAdmin = DSApi.context().isAdmin();
				StringBuilder sb = new StringBuilder();
				if(getUserImpl() == null)
				{
					setUserImpl(new UserImpl());
					getUserImpl().setName(DSApi.context().getPrincipalName());
				}
				if (getUserImpl() != null && getUserImpl().getName() != null)
				{
					// wyszukanie użytkownika o podanej nazwie
					UserImpl user = (UserImpl) UserImpl.findByUsername(getUserImpl().getName());
					// pobranie przełożonych
					user.getSupervisorsHierarchy();
					// ustawienie obiektu usera dla widoku
					setUserImpl(user);
				}
				if(getUserImpl().getUserAddress() != null){
					for(UserAddress ua: getUserImpl().getUserAddress()){ 
//						setAdresy(getUserImpl().getUserAddress());
					}
					adresy = getUserImpl().getUserAddress();
				}
				if(getUserImpl().getUserData() != null){
					boolean pierwszy = true;
					for(UserData data: getUserImpl().getUserData()){
						log.error("testuserdata" + data.getDrugieimie() + " " + sb);
						
						single = data;
						sb.append(data.getUmowastanowisko() + " , ");
					}
//					setDane(getUserImpl().getUserData());
					dane = getUserImpl().getUserData();
					workDiv = sb.toString();
					
				}
//				DSApi.context().commit();
			}
			catch (Exception ex)
			{
				log.error("TESTUSERDATA"+ex.getMessage(), ex);
//				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				
			}
		}
	}

	public Set<UserAddress> getAdresy() {
		return adresy;
	}

//	public void setAdresy(Set<UserAddress> adresy) {
//		this.adresy = adresy;
//	}
	
	public Set<UserData> getDane(){
		return dane;
	}
	
	public UserData getSingle(){
		return this.single;
	}
	
	public String getWorkDiv(){
		return this.workDiv;
	}
	
//	public void setDane(Set<UserData> data){
//		this.dane = data;
//	}
	

}
