package pl.compan.docusafe.web.userinfo;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.userinfo.UserInfoAbstractAction.CheckPermissions;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementuj�ca akcje wy�wietlania urlop�w pracownika
 * 
 * @author<a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class UserAbsencesAction extends UserInfoAbstractAction 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserAbsencesAction.class);

	/**
	 * Rok, z kt�rego b�d� wy�wietlone urlopy
	 */
	private int year;
	
	/**
	 * Obiekt karty pracownika
	 */
	private EmployeeCard empCard;
	
	/**
	 * Mapa kart pracownika dla elementu select
	 */
	private Map<Long, String> employeeCardsMap = new HashMap<Long, String>();
	
	/**
	 * Lista urlop�w pracownika
	 */
	private List<EmployeeAbsenceEntry> emplAbsences;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		LoadEmployeeCards loadEmployeeCards = new LoadEmployeeCards();
		LoadEmployeeCardAbsences loadEmployeeCardAbsences = new LoadEmployeeCardAbsences();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.append(loadEmployeeCards)
			.append(loadEmployeeCardAbsences)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	

	/**
	 * Metoda zwracaj�ca sk�adow� checkPermissions
	 * 
	 * @return
	 */
	public CheckPermissions getCheckPermissionsObj() 
	{
		if (checkPermissions == null)
			checkPermissions = new CheckPermissions();
		return checkPermissions;
	}
	
	/**
	 * Klasa �aduj�ca karty pracownika dla elementu select
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadEmployeeCards implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (getUserImpl() != null && getUserImpl().getName() != null)
				{
					// wyszukanie u�ytkownika o podanej nazwie
					UserImpl user = (UserImpl) UserImpl.findByUsername(getUserImpl().getName());
					setUserImpl(user);
				}
				if (getUserImpl() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));
				
				// pobranie kart i utworzenie mapy
				List<Object[]> rows = DSApi.context().session().createQuery(
						"select ec.id, ec.KPX, ec.user.lastname, ec.user.firstname from "
						+ EmployeeCard.class.getName() + " ec where ec.user = ?")
					    .setParameter(0, getUserImpl())
						.list();
				for (Object[] row : rows)
				{
					employeeCardsMap.put((Long) row[0], 
							row[1] + " - " + row[2] + " " + row[3]);
				}
				
				if (employeeCardsMap.size() == 0)
					addActionError(sm.getString("BrakKartPracownika"));
				else if (empCard == null)
				{
					empCard = new EmployeeCard();
					empCard.setId(employeeCardsMap.keySet().iterator().next());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			} 
		}
	}
	
	/**
	 * Klasa �aduj�ca urlopy z danej karty pracownika z okre�olego roku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadEmployeeCardAbsences implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try 
			{
				if (empCard == null || empCard.getId() == null)
					return;
				
				DSApi.context().begin();
				
				if (year == 0)
					year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				
				empCard = Finder.find(EmployeeCard.class, empCard.getId());
				if(empCard != null)
				{
					empCard.getId(); // lazyyyy
					// pobranie urlop�w, kt�re pracownik wzi�� podczas wskazanego roku kalendarzowego
					emplAbsences = AbsenceFactory.getEmployeeAbsences(empCard, year);
				}
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage()); 
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	public int getYear() 
	{
		return year;
	}

	public void setYear(int year) 
	{
		this.year = year;
	}

	public EmployeeCard getEmpCard() 
	{
		return empCard;
	}

	public void setEmpCard(EmployeeCard empCard) 
	{
		this.empCard = empCard;
	}

	public Map<Long, String> getEmployeeCardsMap() 
	{
		return employeeCardsMap;
	}

	public void setEmployeeCardsMap(Map<Long, String> employeeCardsMap) 
	{
		this.employeeCardsMap = employeeCardsMap;
	}

	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return emplAbsences;
	}

	public void setEmplAbsences(List<EmployeeAbsenceEntry> emplAbsences) 
	{
		this.emplAbsences = emplAbsences;
	}
	
	/**
	 * Zwraca map� z rokiem dla elementu select
	 * 
	 * @return
	 */
	public Map<Integer, Integer> getYearsMap() 
	{
		Map<Integer, Integer> yearsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 2000; i <= GregorianCalendar.getInstance().get(GregorianCalendar.YEAR); i++)
			yearsMap.put(i, i);
		return yearsMap;
	}
}
