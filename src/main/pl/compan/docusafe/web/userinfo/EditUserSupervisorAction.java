package pl.compan.docusafe.web.userinfo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.tree.organization.ExtendedOrganizationTree;
import pl.compan.docusafe.web.tree.organization.OrganizationUrlProvider;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa akcji odpowiedzialna za akcje wyboru prze�o�onego dla danego
 * u�ytkownika.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditUserSupervisorAction extends UserInfoAbstractAction 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditUserSupervisorAction.class);
	
	/**
	 * Maksymalna liczba przelozonych
	 */
	private static final int MAX_SUPERVISORS = 3;
	
	/**
	 * Identyfikator zaznaczonego dzia�u
	 */
	private String dguid;
	
	/**
	 * Drzewo z dzia�ami i u�ytkownikami
	 */
	private String htmlTree;
	
	/**
	 * Nazwa przelozonego
	 */
	private String supervisor;
	
	/**
	 * Tablica z prze�o�onymi
	 */
	private String[] supervisorNames;
	
	/**
	 * Lista prze�o�onych
	 */
	private List<UserImpl> supervisorList;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		FillForm fillForm = new FillForm();
		LoadSupervisorList loadSupervisorList = new LoadSupervisorList();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.append(fillForm)
			.append(loadSupervisorList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("selectSupervisors")
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.append(new SelectSupervisors())
			.append(fillForm)
			.append(loadSupervisorList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("deleteSupervisors")
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.append(new DeleteSupervisors())
			.append(fillForm)
			.append(loadSupervisorList)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Klasa implementuj�ca akcj� wy�wietlenia drzewa z dzia�ami z mo�liwo�ci�
	 * wyboru przelozonego 
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				// sprawdzenie uprawnien do edycji profilu
				if (!isEditable())
					throw new EdmException(sm.getString("BrakUprawnienDoEdycjiProfilu"));
				
				if (getUserImpl() != null && getUserImpl().getName() != null)
				{
					// wyszukanie u�ytkownika o podanej nazwie
					UserImpl user = (UserImpl) UserImpl.findByUsername(getUserImpl().getName());
					setUserImpl(user);
				}
				if (getUserImpl() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));

				DSDivision division = selectDivision(getUserImpl());
				
				// ustawienie sk�adowej htmlTree, wygenerowanie drzewa struktury organizacji
				generateHtmlTree(division);
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Metoda zwracaj�ca dzia� u�ytkownika
		 * 
		 * @param user
		 * @return
		 * @throws EdmException
		 */
		private DSDivision selectDivision(UserImpl user) throws EdmException
		{
			DSDivision division = null;
			// je�li istnieje ju� prze�o�ony i nie podano dguid, wyszukanie ostatniego dzia�u, w kt�rym si� znajduje
			if (user != null && dguid == null)
			{
				DSDivision[] dguids = user.getDivisions();
				if (dguids != null && dguids.length > 0)
				{
					division = dguids[0];
					dguid = division.getGuid();
				}
			}
			
			// je�li brak dguid to ustawienie rootdivision
			if (dguid == null)
				dguid = DSDivision.ROOT_GUID;
			
			division = DSDivision.find(dguid);
			
			return division;
		}
		
		/**
		 * Metoda ustawiaj�ca do sk�adowej htmlTree wygenerowane drzewo z dzia�ami i pracownikami organizacji,
		 * z kt�rego mo�na wybra� prze�o�onego dla danego pracownika
		 * 
		 * @param division
		 * @param userSupervisor
		 * @throws EdmException
		 */
		private void generateHtmlTree(DSDivision division) throws EdmException
		{
			// utworzenie obiektu klasy generuj�cej odpowiedni link przypisany do danego dzia�u w drzewie
			OrganizationUrlProvider provider = new OrganizationUrlProvider() 
			{
				public String getUrl(DSDivision div, DSUser user) 
				{
					return ServletActionContext.getRequest().getContextPath()
							+ "/help/edit-user-supervisor.action?tid=" + getTid() 
							+ "&userImpl.name=" + getUserImpl().getName()
							+ "&eid=true&dguid=" + div.getGuid();
				}
			};
			// wygenerowanie drzewa z dzia�ami i u�ytkownikami
            htmlTree = ExtendedOrganizationTree.createSelectSupervisorOrganizationTree(division, getUserImpl(), provider, 
            		ServletActionContext.getRequest().getContextPath())
            	.generateTree();
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� ustawienia prze�o�onego dla danego u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class SelectSupervisors implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (supervisorNames == null || supervisorNames.length == 0)
					throw new EdmException(sm.getString("NieprawidlowyWyborPrzelozonego"));
				
				List<UserImpl> users = DSApi.context().session().createQuery(
						"from " + UserImpl.class.getName() + " u where u.name in (:names)")
						.setParameterList("names", supervisorNames)
						.list();
				
				// pobranie usera
				setUserImpl((UserImpl) UserImpl.findByUsername(getUserImpl().getName()));
				
				Set<UserImpl> supervisorsToSave = new HashSet<UserImpl>();
				// dodanie ju� istniejacych przelozonych
				for (UserImpl user : getUserImpl().getSupervisors())
				{
					supervisorsToSave.add(user);
				}
				// dodanie nowych
				for (UserImpl user : users)
				{
					if (getUserImpl().getSupervisors() != null
							&& !getUserImpl().getSupervisors().contains(user))
					{
						supervisorsToSave.add(user);
					}
				}
				
				// sprawdzenie czy nie wybrano za duzo przelozonych
				if (supervisorsToSave != null && supervisorsToSave.size() > MAX_SUPERVISORS)
					throw new EdmException(sm.getString("MaksymalnaIloscPrzelozonych"));
				
				getUserImpl().setSupervisors(supervisorsToSave);
				// zapisanie zmian
				DSApi.context().session().update(getUserImpl());
				DSApi.context().commit();
				
				addActionMessage(sm.getString("PoprawnieDodanoPrzelozonych"));
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� usuni�cia wybranych prze�ozonych
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DeleteSupervisors implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (supervisorNames != null && supervisorNames.length > 0)
				{
					// pobranie przelozonych
					List<UserImpl> supervisors = DSApi.context().session().createQuery(
							"from " + UserImpl.class.getName() + " u where u.name in (:names)")
							.setParameterList("names", supervisorNames)
							.list();
					
					// pobranie usera
					setUserImpl((UserImpl) UserImpl.findByUsername(getUserImpl().getName()));
					
					for (UserImpl user : supervisors)
					{
						getUserImpl().getSupervisors().remove(user);
					}
					
					DSApi.context().session().update(getUserImpl());
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}
	
	/**
	 * Klasa �aduj�ca list� prze�o�onych dla tabeli w widoku
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadSupervisorList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				supervisorList = DSApi.context().session().createQuery(
						"select s from " + UserImpl.class.getName() + " u join u.supervisors s where u.name = ?")
						.setString(0, getUserImpl().getName())
						.list();
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Metoda zwracaj�ca sk�adow� dguid
	 * 
	 * @return
	 */
	public String getDguid() 
	{
		return dguid;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� dguid
	 * 
	 * @param dguid
	 */
	public void setDguid(String dguid) 
	{
		this.dguid = dguid;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� htmlTree
	 * 
	 * @return
	 */
	public String getHtmlTree() 
	{
		return htmlTree;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� htmlTree
	 * 
	 * @param htmlTree
	 */
	public void setHtmlTree(String htmlTree) 
	{
		this.htmlTree = htmlTree;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� supervisor
	 * 
	 * @return
	 */
	public String getSupervisor() 
	{
		return supervisor;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� supervisor
	 * 
	 * @param supervisor
	 */
	public void setSupervisor(String supervisor) 
	{
		this.supervisor = supervisor;
	}

	public String[] getSupervisors() 
	{
		return supervisorNames;
	}

	public void setSupervisors(String[] supervisors) 
	{
		this.supervisorNames = supervisors;
	}

	public void setSupervisorList(List<UserImpl> supervisorList) 
	{
		this.supervisorList = supervisorList;
	}

	public List<UserImpl> getSupervisorList() 
	{
		return supervisorList;
	}

	public String[] getSupervisorNames() 
	{
		return supervisorNames;
	}

	public void setSupervisorNames(String[] supervisorNames) 
	{
		this.supervisorNames = supervisorNames;
	}
}
