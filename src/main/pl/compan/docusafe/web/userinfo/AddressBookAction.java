package pl.compan.docusafe.web.userinfo;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import com.lowagie.text.Cell;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.address.Address;
import pl.compan.docusafe.core.address.Floor;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;	
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja odpowiedzialna za wy�wietlanie kontakt�w.
 * Istnieje mo�liwo�� sortowania wg. nazwisk oraz imion,
 * a tak�e wyszukiwaniu wg. nazwisk.
 * @author <a href="mailto:rafal.sylwestrzuk@com-pan.pl">Rafa� Sylwestrzuk</a>
 */
public class AddressBookAction extends EventActionSupport
{
    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddressBookAction.class);
	
	/**
	 * String Manager
	 */
	private static final StringManager sm = StringManager.getManager(AddressBookAction.class.getPackage().getName());
    
    /**
     * Lista znalezionych u�ytkownik�w
     */
    private List<UserImpl> usersList;
    
    /**
     * Mapa adres�w organizacji
     */
    private Map<Long, String> organizations;
    
    /**
     * Mapa z pi�trami danej organizacji
     */
    private Map<Long, String> floors;
    
    /**
     * Identyfikator zaznaczonego adresu organizacji
     */
    private Long selectedOrganizationId;
    
    /**
     * Okre�la czy zosta� zmieniony identyfikator adresu organizacji
     */
    private boolean organizationIdReload = false;
    
    /**
     * Obiekt u�ytkownika
     */
    private UserImpl user;
    
    /**
     * Identyfikator pi�tra u�ytkownika
     */
    private Long selectedFloorId;
    
    /**
     * Sortowanie rosn�co
     */
    private boolean ascending = true;
    
    /**
     * Pole po kt�ry odb�dzie si� sortowanie
     */
    private String sortField = "lastname";
    
    /**
     * Numer strony
     */
    private int pageNumber = 0;
    
    /**
     * Liczba rezultat�w
     */
    private int maxResults = 20;
    
    /**
     * Pager listy u�ytkownik�w
     */
    private Pager pager;
    
    private String position;
    
    private String division;
    
    /**
     * Przelozony uzytkownika
     */
    private String przelozony="";
    
    /**
     * Dzial glowny
     */
    private String dzialGlowny="";
    
    /**
     * Dzial glowny
     */
    private String stanowisko="";
    
    /**
     * Konfiguracja
     */
	protected void setup()
    {
		LoadOrganizationsMap loadOrganizationsMap = new LoadOrganizationsMap();
		LoadFloorsMap loadFloorsMap = new LoadFloorsMap();
		LoadUsersList loadUsersList = new LoadUsersList();
		
        registerListener(DEFAULT_ACTION)
            .append(OpenHibernateSession.INSTANCE)
            .append(loadOrganizationsMap)
            .append(loadFloorsMap)
            .append(loadUsersList)
            .appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doFilter")
        	.append(OpenHibernateSession.INSTANCE)
        	.append(new ActionListener() {
				public void actionPerformed(ActionEvent event) 
				{
					// je�li filtruje dane, ustawienie numeru strony na 0
					pageNumber = 0;
				}
			})
        	.append(loadOrganizationsMap)
        	.append(loadFloorsMap)
        	.append(loadUsersList)
        	.appendFinally(CloseHibernateSession.INSTANCE);
        	
        registerListener("doReport")
        	.append(OpenHibernateSession.INSTANCE)
        	.append(new Report())
        	.append(loadOrganizationsMap)
            .append(loadFloorsMap)
            .append(loadUsersList)
            .appendFinally(CloseHibernateSession.INSTANCE);
    }
	
	/**
	 * Za�adowanie listy wyszukiwanych u�ytkownik�w
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadUsersList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				Map<String, Object[]> searchParams = buildSearchParamsMap();
				if (searchParams == null)
				{
					// pobranie wszystkich user�w
					usersList = DSApi.context().session().createQuery(
							"from " + UserImpl.class.getName() + " u where deleted <> 1 order by u.lastname asc")
							.setFirstResult(pageNumber)
							.setMaxResults(maxResults)
							.list();
					createPager(null, "");
				}
				else
				{
					String fetchDivisions = "";
					 if (division != null && !division.equals("") || position != null && !position.equals(""))
						 fetchDivisions = " join fetch u.divisions div ";
					 
					String where = createWhereClause(searchParams);
					// utworzenie obiektu query
					Query query = DSApi.context().session().createQuery(
							"from " + UserImpl.class.getName() + " u " + fetchDivisions + where + " order by u." + sortField + (ascending ? " asc" : " desc"));
					// ustawienie parametr�w wyszukiwania
					setupQueryParams(query, searchParams);
					usersList = query.setFirstResult(pageNumber).setMaxResults(maxResults).list();
					createPager(searchParams, where);
				}
				for (UserImpl userImpl : usersList) 
				{
					 userImpl.setAtWork(AbsenceFactory.isUserAtWork(userImpl.getName()));
					 try
					 {
						 userImpl.getOriginalDivisionsWithoutGroup()[0].getPrettyPath();
					 }	
					 catch (Exception e) {
						// TODO: handle exception
					}
				}
				DSApi.context().commit();
			}
			catch (Exception ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
			}
		}
		
		/**
		 * Tworzy pager na podstawie parametr�w wyszukiwania
		 * 
		 * @param searchParams
		 * @param whereClause
		 * @throws EdmException
		 */
		private void createPager(Map<String, Object[]> searchParams, String whereClause) throws EdmException
		{
			Query query;
			if (StringUtils.isNotEmpty(division) || StringUtils.isNotEmpty(position))
				query = DSApi.context().session().createQuery(
						"select count(u.name) from " + UserImpl.class.getName() + " u join u.divisions div " + whereClause);
			else
			// utworzenie obiektu query zliczaj�cego ilo�� wynik�w
				query = DSApi.context().session().createQuery(
					"select count(u.name) from " + UserImpl.class.getName() + " u " + whereClause);
			if (searchParams != null)
				setupQueryParams(query, searchParams);
			
			Long result = (Long) query.uniqueResult();
			int count = Integer.parseInt(result + "");
			
			// utworzenie pagera
			pager = new Pager(new Pager.LinkVisitor() {	
				public String getLink(int offset) 
				{
					return offset + "";
				}
			}, pageNumber, maxResults, count, 5, true);
		}
		
		/**
		 * Metoda buduje map� z parametrami zapytania 
		 * Posta� mapy jest nast�puj�ca:
		 * Map<String, Object[]>
		 * - String: cze�� zapytania HQL
		 * - Object[]: tablica z dwoma elementami: nazwa parametru i warto�� parametru
		 * 
		 * @return
		 */
		private Map<String, Object[]> buildSearchParamsMap()
		{
			if (user == null)
				return null;
			
			// Posta� mapy jest nast�puj�ca: 
			// String - cze�� zapytania HQL
			// Object[] - tablica z dwoma elementami: nazwa parametru i warto�� parametru
			Map<String, Object[]> paramsMap = new HashMap<String, Object[]>();
			paramsMap.put("u.deleted <> :DELETED", new Object[] {"DELETED", true});
			// nazwisko
			if (StringUtils.isNotEmpty(user.getLastname()))
				paramsMap.put("lower(u.lastname) like :LASTNAME", new Object[] {"LASTNAME", user.getLastname().toLowerCase() + "%"});
			// imie
			if (StringUtils.isNotEmpty(user.getFirstname()))
				paramsMap.put("lower(u.firstname) like :FIRSTNAME", new Object[] {"FIRSTNAME", user.getFirstname().toLowerCase() + "%"});
			// nazwa uzytkownika
			if (StringUtils.isNotEmpty(user.getName()))
				paramsMap.put("lower(u.name) like :NAME", new Object[] {"NAME", user.getName().toLowerCase() + "%"});
			// kod uzytkownia
			if (StringUtils.isNotEmpty(user.getIdentifier()))
				paramsMap.put("u.identifier like :USERID", new Object[] {"USERID", user.getIdentifier() + "%"});
			// dzia�
			if (StringUtils.isNotEmpty(division))
				paramsMap.put("(div.divisionType = 'position' and lower(div.parent.name) like :DIVNAME)", 
						new Object[] {"DIVNAME", division.toLowerCase() + "%"});
			// stanowisko
			if (StringUtils.isNotEmpty(position))
				paramsMap.put("(lower(div.name) like :POSNAME and div.divisionType = 'position')", 
						new Object[] {"POSNAME", position.toLowerCase() + "%"});
				
			
			// lokalizacja
			// identyfikator pi�tra
			if (selectedFloorId != null && selectedFloorId > 0)
			{
				paramsMap.put("u.userLocation.floor.id = :FLOORID", new Object[] {"FLOORID", selectedFloorId});
			}
			// numer pokoju
			if (user.getRoomNum() != null)
				paramsMap.put("u.roomNum = :ROOMNUM", new Object[] {"ROOMNUM", user.getRoomNum()});
			
			return paramsMap;
		}
		
		/**
		 * Tworzy klauzule where zapytania hql
		 * 
		 * @param searchParams
		 * @return
		 */
		private String createWhereClause(Map<String, Object[]> searchParams)
		{
			StringBuffer hql = new StringBuffer();
			Object[] keys = searchParams.keySet().toArray();
			// p�tla buduj�ca ci�g zapytania
			for (int i = 0; i < keys.length; i++)
			{
				hql.append(keys[i]);
				if (i + 1 < keys.length)
					hql.append(" and ");
			}
			
			// Opis Stanowiska - rozbicie poszczeg�lnych s��w
			// i zbudowanie zapytania hql
			String[] words = explodePostDescWords(user.getPostDescription());
			if (words != null)
			{
				if (!hql.toString().equals(""))
					hql.append(" and ");
				
				for (int i = 0; i < words.length; i++)
				{
					hql.append(" u.postDescription like :WORD" + i);
					if (i + 1 < words.length)
						hql.append(" or ");
				}
			}
			
			return " where " + hql.toString();
		}
		
		/**
		 * Ustawia parametry zapytania
		 * 
		 * @param query
		 * @param searchParams
		 */
		private void setupQueryParams(Query query, Map<String, Object[]> searchParams)
		{
			// ustawienie parametr�w zapytania z warto�ci mapy
			for (Object[] param : searchParams.values())
			{
				query.setParameter((String) param[0], param[1]);
			}
			
			// jesli wyszukuje si� po opisie stanowiska - dodanie parametr�w do zapytania
			String[] words = explodePostDescWords(user.getPostDescription());
			if (words != null)
			{
				int k = 0;
				for (String word : words)
				{
					query.setString("WORD" + k++, "%" + word + "%");
				}
			}
		}
		
		/**
    	 * Metoda rozdzielaj�ca tekst opisu stanowiska na poszczeg�lne s�owa,
    	 * usuwa zb�dne spacje i znaki specjalne
    	 * 
    	 * @return
    	 * @throws EdmException
    	 */
    	private String[] explodePostDescWords(String postDescWords)
    	{
    		if (postDescWords == null || postDescWords.equals(""))
    			return null;
    		
    		postDescWords = postDescWords.replaceAll("[^\\p{L}0-9]+", " ");
    		postDescWords = postDescWords.replaceAll("\\s+", " ");
    		return StringUtils.split(postDescWords);
    	}
	}
	
	public String getPositionString(DSUser user)
	{
		try
		{
			if(user.getOriginalDivisionsWithoutGroup() != null && user.getOriginalDivisionsWithoutGroup().length > 0)
			{
				DSDivision div = user.getOriginalDivisionsWithoutGroup()[0];
				String path = div.getPrettyPath();
				String[] tab = path.split("/");
				if(DSDivision.TYPE_POSITION.equals(div.getDivisionType()))
				{
					return tab[tab.length-1];
				}
			}
			
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return "brak";
	}
	
	public String getDivisionString(DSUser user)
	{
		try
		{
			if(user.getOriginalDivisionsWithoutGroup() != null && user.getOriginalDivisionsWithoutGroup().length > 0)
			{
				DSDivision div = user.getOriginalDivisionsWithoutGroup()[0];
				String path = div.getPrettyPath();
				String[] tab = path.split("/");
				if(DSDivision.TYPE_POSITION.equals(div.getDivisionType()))
				{
					
					return tab[tab.length-2];
				}
				else
				{
					return tab[tab.length-1];
				}
			}
			
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return "brak";
	}
	
	/**
	 * Klasa implementuj�ca akcje za�adowania mapy z nazwami organizacji dla elementu select
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadOrganizationsMap implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				organizations = new LinkedHashMap<Long, String>();
				
				// pobranie nazw adres�w lokalizacji i numer�w identyfikacyjnych
				String sql = "select adr.id, adr.name, adr.organization from " + Address.class.getName()
					+ " adr join adr.floors flr where flr.id is not null order by adr.id asc";
				List results = DSApi.context().session().createQuery(sql).list();
				
				// pierwszy wpis
				organizations.put(0L, "Wybierz adres lokalizacji");
				// utworzenie mapy
				for (int i = 0; i < results.size(); i++)
				{
					Object[] row = (Object[]) results.get(i);
					organizations.put((Long) row[0], (String) row[1] + " - " + (String) row[2]);
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
			}
		}
	}
	
	/**
	 * Za�adowanie mapy pi�ter z danego adresu organizacji dla elementu select
	 * 
	 * @author @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadFloorsMap implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				floors = new LinkedHashMap<Long, String>();
				
				if (selectedOrganizationId == null || selectedOrganizationId == 0)
				{
					floors.put(0L, "Wybierz adres pi�tra");
					selectedFloorId = null;
					return;
				}
				
				DSApi.context().begin();
				
				// wybranie listy pi�ter z podanego identyfikatora adresu
				List results = DSApi.context().session().createQuery(
								  "select flr.id, flr.name, flr.description from " + Floor.class.getName() + " flr "
								+ " where flr.address = ?")
								.setLong(0, selectedOrganizationId)
								.list();
				
				if (results == null || results.size() == 0)
					throw new EdmException(sm.getString("BrakPieterWLokalizacji"));

				// wczytanie nazwy pi�tra i jego identyfikatora do mapy
				for (int i = 0; i < results.size(); i++)
				{
					Object[] row = (Object[]) results.get(i);
					floors.put((Long) row[0], (String) row[1] + " - " + (String) row[2]);
					
					if (i == 0 && organizationIdReload)
					{
						selectedFloorId = (Long) row[0];
					}
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
			}
		}
	}
	
	/**
	 * Wydruk kontakt�w w formie pdf
	 */
	private class Report implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			LoadUsersList user = new LoadUsersList();
			try
			{
				File pdfFile= null;
				try {
					DSApi.context().begin();

					Map<String, Object[]> searchParams = user.buildSearchParamsMap();
					if (searchParams == null)
					{
						// pobranie wszystkich user�w
						usersList = DSApi.context().session().createQuery(
								"from " + UserImpl.class.getName() + " u where deleted <> 1 order by u.lastname asc")
								.setFirstResult(pageNumber)
								.setMaxResults(maxResults)
								.list();
						user.createPager(null, "");
					}
					else
					{
						String fetchDivisions = "";
						if (division != null && !division.equals("") || position != null && !position.equals(""))
							fetchDivisions = " join fetch u.divisions div ";

						String where = user.createWhereClause(searchParams);
						// utworzenie obiektu query
						Query query = DSApi.context().session().createQuery(
								"from " + UserImpl.class.getName() + " u " + fetchDivisions + where + " order by u." + sortField + (ascending ? " asc" : " desc"));
						// ustawienie parametr�w wyszukiwania
						user.setupQueryParams(query, searchParams);
						usersList = query.setFirstResult(pageNumber).setMaxResults(maxResults).list();
						user.createPager(searchParams, where);
					}

					// font
					File fontDir= new File(Docusafe.getHome(), "fonts");
					File arial= new File(fontDir, "arial.ttf");
					BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

					Font fontSuperSmall= new Font(baseFont, (float) 6);
					Font fontSmall= new Font(baseFont, (float) 8);
					Font fontNormal= new Font(baseFont, (float) 12);
					Font fontNormal_Large= new Font(baseFont, (float) 14);
					Font fontLarge= new Font(baseFont, (float) 16);

					// file
					pdfFile= File.createTempFile("docusafe_", "_tmp");
					pdfFile.deleteOnExit();
					com.lowagie.text.Document pdfDoc= new com.lowagie.text.Document(PageSize.A4);
					PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

					pdfDoc.open();

					// Tabela z danymi
					Table contentTable= new Table(12);
					contentTable.setCellsFitPage(true);
					contentTable.setWidth(110);
					contentTable.setPadding(1);
					contentTable.setBorder(1);
					contentTable.setWidths( new int[]{2,6,6,5,10,10,10,5,10,10,7,4});

					// Naglowki
					contentTable.addCell( new Cell(new Paragraph("Lp.", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Nazwisko", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Imie", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Dzia�", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Stanowisko", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Prze�o�ony", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Tel. stacjonarny", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Nr wewn.", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Tel. kom�rkowy", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("E-mail", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Komunikator", fontSmall)));
					contentTable.addCell( new Cell(new Paragraph("Numer pokoju", fontSmall)));
					Integer lp_index = 1;
					// Tresc tabeli
					for (UserImpl userImpl : usersList) 
					{
						Cell lp = new Cell(new Paragraph(lp_index.toString(), fontSmall));
						Cell nazwisko = new Cell(new Paragraph(userImpl.getLastname(), fontSmall));
						Cell imie = new Cell(new Paragraph(userImpl.getFirstname(), fontSmall));
						contentTable.addCell(lp);
						contentTable.addCell(nazwisko);
						contentTable.addCell(imie);
						
						DSDivision[] dsDiv = userImpl.getDivisions();
						if(dsDiv != null){
//						for(DSDivision ds:dsDiv){
							
								
								for(int i=0;i<dsDiv.length;i++){
									if(dsDiv[i].getDivisionType().equals("division")){
								String division = dsDiv[i].getName() + " "; 
 
								dzialGlowny+=division;
									}
								}
								Cell dzial = new Cell(new Paragraph(dzialGlowny, fontSmall));	
								contentTable.addCell(dzial);	
								dzialGlowny = "";
//									}
								
								/*PreparedStatement ps = null;
								ps = DSApi
										.context()
										.prepareStatement(
												"SELECT DIVISIONTYPE FROM DSO_JOURNAL_ENTRY where DOCUMENTID=? and JOURNAL_ID=1");
								ps.setLong(1, newDocument.getId());
								ResultSet rs = ps.executeQuery();
								if(rs.next()){
									((OfficeDocument) newDocument).setOfficeNumber(Integer.parseInt(rs.getString(1)));
								}*/
						} else {
							Cell dzial = new Cell(new Paragraph("", fontSmall));	
							contentTable.addCell(dzial);
						}
								for(DSDivision ds:dsDiv){
									
								if(ds.getDivisionType().equals("position")){
									stanowisko = ds.getName();
								} 
								
							
								}
								Cell stanowiskoUser = new Cell(new Paragraph(stanowisko, fontSmall));
								contentTable.addCell(stanowiskoUser);
								stanowisko = "";
								
							
							
							List<UserImpl> userP = userImpl.getSupervisorsHierarchy();
							if(userP != null){
								for(int i=0;i<userP.size();i++){
									String nowy = userP.get(i).getFirstname() + " " +userP.get(i).getLastname() + " "; 

									przelozony+=nowy;
								}
								Cell przelozonyU = new Cell(new Paragraph(przelozony, fontSmall));
								contentTable.addCell(przelozonyU);
								przelozony = "";
								
							} else {
								Cell przelozonyU = new Cell(new Paragraph(przelozony, fontSmall));
								contentTable.addCell(przelozonyU);
							}
							if(userImpl.getPhoneNum() != null){
							Cell tel = new Cell(new Paragraph(userImpl.getPhoneNum(), fontSmall));
							contentTable.addCell(tel);
							} else {
								Cell tel = new Cell(new Paragraph("", fontSmall));
								contentTable.addCell(tel);
							}
							Cell nrWew = new Cell(new Paragraph(userImpl.getExtension(), fontSmall));
							Cell kom = new Cell(new Paragraph(userImpl.getMobileNum(), fontSmall));
							Cell email = new Cell(new Paragraph(userImpl.getEmail(), fontSmall));
							Cell komunikator = new Cell(new Paragraph(userImpl.getCommunicatorNum(), fontSmall));
							Cell pokoj = new Cell(new Paragraph(userImpl.getRoomNum(), fontSmall));

					
							contentTable.addCell(nrWew);
							contentTable.addCell(kom);
							contentTable.addCell(email);
							contentTable.addCell(komunikator);
							contentTable.addCell(pokoj);
						
						++lp_index;
					}
					pdfDoc.add(contentTable);
					pdfDoc.close();
				} catch (Exception e) {
					throw new EdmException(e);
				}

				DSApi.context().commit();
				ServletUtils.streamFile(ServletActionContext.getResponse(),pdfFile, "application/pdf",
						"Content-Disposition: attachment; filename=\"wydruk kontakt�w.pdf\"");

			}
			catch (Exception ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
			}
		}
	}
    
    public List<UserImpl> getUsersList() 
    {
		return usersList;
	}

	public void setUsersList(List<UserImpl> usersList) 
	{
		this.usersList = usersList;
	}

	public UserImpl getUser() 
	{
		return user;
	}

	public void setUser(UserImpl user) 
	{
		this.user = user;
	}

	public Long getUserFloorId() 
	{
		return selectedFloorId;
	}

	public void setUserFloorId(Long userFloorId) 
	{
		this.selectedFloorId = userFloorId;
	}

	public Map<Long, String> getOrganizations() 
	{
		return organizations;
	}

	public void setOrganizations(Map<Long, String> organizations) 
	{
		this.organizations = organizations;
	}

	public Map<Long, String> getFloors() 
	{
		return floors;
	}

	public void setFloors(Map<Long, String> floors) 
	{
		this.floors = floors;
	}

	public Long getSelectedOrganizationId() 
	{
		return selectedOrganizationId;
	}

	public void setSelectedOrganizationId(Long selectedOrganizationId) 
	{
		this.selectedOrganizationId = selectedOrganizationId;
	}

	public Long getSelectedFloorId() 
	{
		return selectedFloorId;
	}

	public void setSelectedFloorId(Long selectedFloorId) 
	{
		this.selectedFloorId = selectedFloorId;
	}

	public boolean isAscending() 
	{
		return ascending;
	}

	public void setAscending(boolean ascending) 
	{
		this.ascending = ascending;
	}

	public String getSortField() 
	{
		return sortField;
	}

	public void setSortField(String sortField) 
	{
		this.sortField = sortField;
	}

	public boolean isOrganizationIdReload() 
	{
		return organizationIdReload;
	}

	public void setOrganizationIdReload(boolean organizationIdReload) 
	{
		this.organizationIdReload = organizationIdReload;
	}

	public int getPageNumber() 
	{
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) 
	{
		this.pageNumber = pageNumber;
	}

	public int getMaxResults() 
	{
		return maxResults;
	}

	public void setMaxResults(int maxResults) 
	{
		this.maxResults = maxResults;
	}

	public Pager getPager() 
	{
		return pager;
	}

	public void setPager(Pager pager) 
	{
		this.pager = pager;
	}
	
	public Map<Integer, Integer> getMaxResultsMap()
	{
		Map<Integer, Integer> maxResultsMap = new LinkedHashMap<Integer, Integer>();
		for (int i = 5; i < 41; i += 5)
			maxResultsMap.put(i, i);
		return maxResultsMap;
	}
	
	public String getDivision() 
	{
		return division;
	}
	
	public void setDivision(String division) 
	{
		this.division = division;
	}
	
	public String getPosition() 
	{
		return position;
	}
	
	public void setPosition(String position) 
	{
		this.position = position;
	}
}