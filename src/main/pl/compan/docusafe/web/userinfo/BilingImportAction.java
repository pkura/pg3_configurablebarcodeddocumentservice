package pl.compan.docusafe.web.userinfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.bilings.Biling;
import pl.compan.docusafe.core.base.bilings.BilingsFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;


public class BilingImportAction extends EventActionSupport 
{
	private final static Logger log = LoggerFactory.getLogger(BilingImportAction.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(BilingImportAction.class.getPackage().getName(),null);
    private FormFile file;
    private Map<String,String> phoneToUser;
    private String bilingNumber;
    private Integer type; 
    private LinkedHashMap<Integer, String> importTypes;
    
    public static final Integer IMPORT_ERA = 1;
    public static final Integer IMPORT_NETIA = 2;
    
	public void setFile(FormFile file) {
		this.file = file;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doImport").
            append(OpenHibernateSession.INSTANCE).
            append(new Import()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }

	
	private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
        }
    }
	
	private class Import implements ActionListener
	{
        public void actionPerformed(ActionEvent event)
        {
			try 
			{	
				
				//DSApi.context().begin();
				File file = File.createTempFile("docusafe", "importB");
                file.deleteOnExit();                
                OutputStream os = new FileOutputStream(file);
                StringBuilder sb = new StringBuilder();
                sb.append("Status;Z numeru;Do;czas;\n");
				if(IMPORT_ERA.equals(type))
				{
					EraImport(sb);
				}
				else if(IMPORT_NETIA.equals(type))
				{
					if(bilingNumber == null || bilingNumber.length() < 1)
						throw new EdmException("Wymagany parametr numer bilingu");
					NetiaImport(sb);
				}
				addActionMessage("Import zakończony");
				//DSApi.context().commit();
                os.write(sb.toString().getBytes("windows-1250"));
                os.close();        
                ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"LOG.csv\"");
                event.setResult(NONE);
			} 
			catch (Exception e) 
			{
				log.error(e.getMessage(), e);
				addActionError("Bł±d : "+e.getMessage());
			}
        }
    }
	
	private void NetiaImport(StringBuilder sb) throws EdmException, Exception
	{
		FileReader fr = new FileReader(file.getFile());
		BufferedReader br = new BufferedReader(fr);
		br.readLine();
		String line;
		String[] dane;
		int i = 0;
		while ((line = br.readLine()) != null)
		{
			i++;
			String phoneNumberFrom = null;
			String phoneNumberTo = null;
			String timePeriod = null;
			try
			{
				if(i %100 == 99)
				{
					log.error("Czyszczenie sesji po "+i+" dokumentach");
					DSApi.context().session().flush();
					DSApi.context().session().clear();
					DSApi.close();
					DSApi.openAdmin();
				}
				DSApi.context().begin();
				dane = line.split(";");
	        	String biulingNumber = bilingNumber;
	        	phoneNumberFrom = dane[9];
				String dateDay = dane[1];
				String dateTime = dane[2];
				phoneNumberTo = dane[3];
				String direction = dane[4];
				timePeriod = dane[5];
				String netto = dane[7];
				String nettoR = netto.replaceAll(",",".");
				Double nettoDouble = Double.parseDouble(nettoR);
				Date dateCal = DateUtils.parseDateTimeAnyFormat(dateDay+" "+dateTime);
				String[] times = timePeriod.split(":");
				int period = Integer.parseInt(times[2]) + (Integer.parseInt(times[1])*60) + (Integer.parseInt(times[0])*60*60);
				if(phoneNumberFrom.length() < 7)
				{
					Integer num = Integer.parseInt(phoneNumberFrom);
					if(num >= 300 && num <= 399)
						phoneNumberFrom = "814613"+phoneNumberFrom;
					else if(num < 400)
						phoneNumberFrom = "223112"+phoneNumberFrom;
					else
						phoneNumberFrom = "223377"+phoneNumberFrom;
				}
				addCall(dateCal,direction,phoneNumberTo,nettoDouble,phoneNumberFrom,biulingNumber,period,"OFFICE",sb,null);
				DSApi.context().commit();
	    	}
	    	catch (Exception e) 
	    	{
	    		sb.append(e.getMessage()+";"+phoneNumberFrom +";"+ phoneNumberTo + ";"+timePeriod+";\n");
				log.error("Bł±d dodania w lini "+line);
				DSApi.context().rollback();
			}
		}
	}
	
	private void EraImport(StringBuilder sb) throws EdmException, Exception
	{
		phoneToUser = new HashMap<String, String>();
		File xlsfile = file.getFile();
		InputStream is = new FileInputStream(xlsfile);
	    POIFSFileSystem fs = new POIFSFileSystem( is );
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        Iterator<Row> rows = sheet.rowIterator();
        if(rows.hasNext())
        	rows.next();
        while( rows.hasNext() )
        {
        	Row row = rows.next();
    	    if(row.getRowNum() > 0  )
    	    {
    			String phoneNumberFrom = null;
    			String phoneNumberTo = null;
    			Date timePeriod = null;
    			Double gprs = 0.0;
    	    	try
    	    	{
					if(row.getRowNum()%100 == 99)
					{
						log.error("Czyszczenie sesji po "+row.getRowNum()+" dokumentach");
						DSApi.context().session().flush();
						DSApi.context().session().clear();
						DSApi.close();
						DSApi.openAdmin();
					}
	    	    	DSApi.context().begin();
	            	String biulingNumber = getStringValueFromCell(row.getCell(0),row);
	            	phoneNumberFrom = getStringValueFromCell(row.getCell(1),row);
	    			Date date = row.getCell(2).getDateCellValue();
	    			Date time = row.getCell(3).getDateCellValue();
	    			phoneNumberTo = getStringValueFromCell(row.getCell(4),row);
	    			String direction = getStringValueFromCell(row.getCell(5),row);
	    			try
	    			{
	    				timePeriod = row.getCell(9).getDateCellValue();
	    			}
	    			catch (Exception e) {
//	    				System.out.println(e.getMessage());
//	    				System.out.println( "time period "+ row.getCell(11).getCellType());
//	    				System.out.println( getStringValueFromCell(row.getCell(10),row));
//	    				System.out.println(row.getCell(10).getNumericCellValue());
//	    				System.out.println("tp as string "+ row.getCell(11).getStringCellValue());
						log.error("Nie odczytaĹ‚ timePeriod");
					}
	    		//	System.out.println("LL"+timePeriod);
	    			try
	    			{
	    				gprs = row.getCell(11).getNumericCellValue();
	    			}
	    			catch (IllegalStateException e) {
				//		System.out.println( row.getCell(11).getCellType());
						String gprsString = getStringValueFromCell(row.getCell(11),row);
						if(!StringUtils.isEmpty(gprsString))
							gprs = Double.parseDouble(gprsString);
					}
	    			String netto = getStringValueFromCell(row.getCell(14),row);
	    			Double nettoDouble = Double.parseDouble(netto.replaceAll("zł", ""));
	    			
	    			int period = 0;
	    			if(timePeriod != null)
	    			{
		    			GregorianCalendar periodCal = new GregorianCalendar();
		    			periodCal.setTime(timePeriod);
		    			int second = periodCal.get(GregorianCalendar.SECOND);
		    			int minute =  periodCal.get(GregorianCalendar.MINUTE);
		    			int hour =  periodCal.get(GregorianCalendar.HOUR_OF_DAY);
		    			period = second + (minute*60) + (hour*60*60);
	    			}
	    			
	    			GregorianCalendar dateCal = new GregorianCalendar();
	    			dateCal.setTime(date);
	    			GregorianCalendar timeCal = new GregorianCalendar();
	    			timeCal.setTime(time);
	    			dateCal.set(GregorianCalendar.HOUR_OF_DAY, timeCal.get(GregorianCalendar.HOUR_OF_DAY));
	    			dateCal.set(GregorianCalendar.MINUTE, timeCal.get(GregorianCalendar.MINUTE));
	    	    	addCall(dateCal.getTime(),direction,phoneNumberTo,nettoDouble,phoneNumberFrom,biulingNumber,period,"MOBILE",sb,gprs);
	    	    	DSApi.context().commit();
    	    	}
    	    	catch (Exception e) 
    	    	{
    	    		System.out.println(e.getMessage());
    	    		sb.append(e.getMessage()+";"+phoneNumberFrom +";"+ phoneNumberTo + ";"+timePeriod+";\n");
					log.error("Bł±d dodania w lini "+row.getRowNum(),e);
					DSApi.context().rollback();
				}
	    	}
        }
	}
	
	
	

	private void addCall(Date dateCal, String direction, String phoneNumberTo, Double nettoDouble, String phoneNumberFrom, String biulingNumber, int period,String type, StringBuilder sb,Double gprs) throws Exception 
	{
		if(StringUtils.isEmpty(phoneNumberFrom))
			throw new EdmException("Brak numeru from");
		if(StringUtils.isEmpty(phoneNumberTo))
			throw new EdmException("Brak numeru to");
		if(dateCal == null)
			throw new EdmException("Brak czasu poł±czenia");
//		if(BilingsFactory.getBilingByPhoneTime(phoneNumberFrom, phoneNumberTo, dateCal) != null)
	//		throw new EdmException("Duplikat "+phoneNumberFrom+" "+phoneNumberTo + " "+ DateUtils.formatCommonDateTime(dateCal));
		Biling biling = new Biling();
		biling.setDateTime(dateCal);
		biling.setDirection(direction);
		biling.setNetto(nettoDouble);
		biling.setPhoneNumberFrom(phoneNumberFrom);
		
		biling.setPhoneNumberTo(phoneNumberTo);
		biling.setTimePeriod(period);
		String username = BilingsFactory.getUsernameFromPhone(phoneNumberFrom);
		if(username == null || username.length() < 1)
			throw new EdmException("Nie znaleziono użytkownika dla numeru "+phoneNumberFrom);
		biling.setUsername(username);
		biling.setType((username != null ? BilingsFactory.getCallType(phoneNumberTo,username,type):type));
		biling.setImpulses(0);
		biling.setBilingNumber(biulingNumber);
		biling.setGPRS(gprs != null ? gprs.intValue(): null);
		biling.create();
		sb.append("OK;"+biling.getPhoneNumberFrom() +";"+ biling.getPhoneNumberTo() + ";"+biling.getTimePeriod()+";\n");
	}


	private String getStringValueFromCell(Cell cell,Row row)
	{
		CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());
        //System.out.println(cellRef.formatAsString());
      //  System.out.print(" - ");
        String result = "";
        switch (cell.getCellType()) 
        {
            case Cell.CELL_TYPE_STRING:
            	result = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                	result = cell.getDateCellValue().toString();
                } else {
                	double d = cell.getNumericCellValue();
                	result = ""+new Double(d);
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
            	result = ""+cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
            	result = cell.getCellFormula();
                break;
            default:
            	result = "Bďż˝ďż˝D";
        }
       // System.out.println(result);
        return result;
	}

	public FormFile getFile() {
		return file;
	}

	public String getBilingNumber() {
		return bilingNumber;
	}

	public void setBilingNumber(String bilingNumber) {
		this.bilingNumber = bilingNumber;
	}

	public Map<String, String> getPhoneToUser() {
		return phoneToUser;
	}

	public void setPhoneToUser(Map<String, String> phoneToUser) {
		this.phoneToUser = phoneToUser;
	}

	public LinkedHashMap<Integer, String> getImportTypes() {
		importTypes = new LinkedHashMap<Integer, String>();
		importTypes.put(IMPORT_ERA, "Era");
		importTypes.put(IMPORT_NETIA, "Netia");
		return importTypes;
	}

	public void setImportTypes(LinkedHashMap<Integer, String> importTypes) {
		this.importTypes = importTypes;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
