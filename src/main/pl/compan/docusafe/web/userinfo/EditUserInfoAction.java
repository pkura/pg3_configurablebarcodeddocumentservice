package pl.compan.docusafe.web.userinfo;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.crypto.params.DSAValidationParameters;
import org.hibernate.Query;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.userinfo.LoadUserInfoAction.LoadUser;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

/**
 * Klasa akcji odpowiedzialna za edycj� danych u�ytkownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class EditUserInfoAction extends LoadUserInfoAction
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditUserInfoAction.class);
	
	/**
	 * Sk�adowa wykorzystywana podczas zmiany zdj�cia u�ytkownika
	 */
	private FormFile file;
	
	
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		super.setup();
		
		registerListener("doEditData")
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(getLoadTabsObj())
			.append(new CheckPermissions())
			.append(getLoadUserPhotoObj())
			.append(new EditData())
			.append(new LoadUser())
			.append(new AddsEdit())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
	private class AddsEdit implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			try{
				String option = Docusafe.getAdditionProperty("user_edit_option");
				LOG.error("wybrana opcja=" + option);
				if(option.equalsIgnoreCase("off")){
					addsEdit = false;
				}
				else if(option.equalsIgnoreCase("admin")){
					DSApi.context().begin();
					if(DSApi.context().isAdmin()) addsEdit = true;
					else addsEdit = false;
				}
				else addsEdit = true;
			}catch(Exception e){
				addsEdit=false;
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Klasa akcji odpowiedzialna za przeprowadzenie edycji danych u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class EditData implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin(); 

				// sprawdzenie uprawnien do edycji profilu
				if (!isEditable() && ! isUserProfile())
					throw new EdmException(sm.getString("BrakUprawnienDoEdycjiProfilu"));
				
				// sprawdzenie poprawno�ci przes�anych danych 
				checkUserFields();
				// zapisanie zmian 
				saveChanges();
				
				// sprawdzenie obrazka
				checkImage();
				
				if (file != null && file.getFile() != null)
				{	
					Image image = getImage();
					if (image == null)
					{
						// brak obrazu, tworzony jest nowy
						image = new Image("Zdj�cie profilu", "", file.getContentType(), file.getName());
						DSApi.context().session().save(image);
						// zapis obrazu
						saveImage(image);
						DSApi.context().session().createQuery(
								"update " + UserImpl.class.getName() + " u set u.image = ? where u.id = ?")
								.setParameter(0, image)
								.setLong(1, getUserImpl().getId())
								.executeUpdate();
						// ustawienie obiektu obrazu dla widoku
						setImage(image);
					}
					else
					{
						image.setContentType(file.getContentType());
						image.setFilename(file.getName());
						DSApi.context().session().update(image);
						// zapis obrazu
						saveImage(image);
					}	
				}
				addActionMessage(sm.getString("PoprawnieZapisanoZmiany"));
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				if (!ex.getMessage().equals(FORM_FIELDS_ERROR))
					addActionError(ex.getMessage());
			}
		}
		
		/**
		 * Sprawdza poprawno�� p�l formularza
		 * 
		 * @throws EdmException
		 */
		private void checkUserFields() throws EdmException
		{
			UserImpl user = getUserImpl();
			
			// sprawdzenie pola imi�
			if (StringUtils.isEmpty(user.getFirstname()))
				addActionError(sm.getString("BrakPodanegoImienia"));
			// sprawdzenie pola nazwisko
			if (StringUtils.isEmpty(user.getLastname()))
				addActionError(sm.getString("BrakPodanegoNazwiska"));
			// sprawdzenie pola opisu stanowiska
			if (user.getPostDescription() != null && user.getPostDescription().length() > 255)
				addActionError(sm.getString("NieprawidlowePoleOpisuStanowiska"));
			
			if (hasActionErrors())
				throw new EdmException(FORM_FIELDS_ERROR);
		}
		
		/**
		 * Metoda zapisuj�ca wprowadzone zmiany w profilu u�ytownika
		 * 
		 * @throws EdmException
		 */
		private void saveChanges() throws EdmException
		{
			String sql = "update " + UserImpl.class.getName() + " u set"
				+ " u.externalName = :extName, u.firstname = :fName," 
				+ " u.lastname = :lName, u.identifier = :idr,"
				+ " u.email = :email, u.phoneNum = :phoneNum,"
				+ " u.extension = :ext, u.mobileNum = :mobNum,"
				+ " u.communicatorNum = :comNum, u.roomNum = :roomNum,"
				+ (getUserImpl().getLimitPhone() != null ?" u.limitPhone = :pLimitPhone," :"")
				+ (getUserImpl().getLimitMobile() != null ?" u.limitMobile = :pLimitMobile," :"")
				+ " u.postDescription = :pDesc"
				+ " where u.id = :uid";
			
			Query query = DSApi.context().session().createQuery(sql);
			query.setString("extName", getUserImpl().getExternalName())
				 .setString("fName", getUserImpl().getFirstname())
				 .setString("lName", getUserImpl().getLastname())
				 .setString("idr", getUserImpl().getIdentifier())
				 .setString("email", getUserImpl().getEmail())
				 .setString("phoneNum", getUserImpl().getPhoneNum())				 
				 
				 .setString("ext", getUserImpl().getExtension())
				 .setString("mobNum", getUserImpl().getMobileNum())
				 
				 .setString("comNum", getUserImpl().getCommunicatorNum())
				 .setString("roomNum", getUserImpl().getRoomNum())
				 .setString("pDesc", getUserImpl().getPostDescription())
				 .setLong("uid", getUserImpl().getId());
			if(getUserImpl().getLimitPhone() != null)
				query.setFloat("pLimitPhone", getUserImpl().getLimitPhone());
			if(getUserImpl().getLimitMobile() != null)
				query.setFloat("pLimitMobile", getUserImpl().getLimitMobile());
			
			query.executeUpdate();	 
		}
		
		/**
		 * Metoda sprawdzaj�ca czy przes�any plik jest obrazem, je�li nie jest rzuca wyj�tek
		 * EdmException
		 * 
		 * @throws EdmException
		 */
		public void checkImage() throws EdmException
		{
			if (file != null && file.getFile() != null)
			{	
				// sprawdzenie czy przes�any plik jest obrazem
				if (!file.getContentType().matches("^image/.+$"))
				{
					addActionError(sm.getString("PlikNieJestObrazem"));
					throw new EdmException(FORM_FIELDS_ERROR);
				}
			}
		}
		
		/**
		 * Metoda sprawdzaj�ca mapk� dojazdu i zapisuj�ca j� dla danego adresu
		 * 
		 * @throws EdmException
		 */
		private void saveImage(Image image) throws EdmException
		{
			if (file != null && file.getFile() != null)
			{	
				try
				{	
				//	InputStream stream = new FileInputStream(file.getFile());
					image.updateImage(file.getFile());
					//	image.updateImageBinaryStream(stream, stream.available());
				//	stream.close();
				}
				catch (Exception ex)
				{
					addActionError(sm.getString("BladPodczasZapisuMapkiDojazdu"));
					throw new EdmException(FORM_FIELDS_ERROR);
				}
			}
		}
	}

	/**
	 * Metoda zwracaj�ca sk�adow� file
	 * 
	 * @return
	 */
	public FormFile getFile() 
	{
		return file;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� file
	 * 
	 * @param file
	 */
	public void setFile(FormFile file) 
	{
		this.file = file;
	}

}
