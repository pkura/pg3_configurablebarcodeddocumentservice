package pl.compan.docusafe.web.userinfo;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.bilings.Biling;
import pl.compan.docusafe.core.base.bilings.BilingsFactory;
import pl.compan.docusafe.core.base.bilings.UserPhoneNumber;
import pl.compan.docusafe.core.base.bilings.Biling.PhoneTypes;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.Pair;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TimePeriod;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa akcji do wy�wietlania i filtrowania biling�w u�ytkownika
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class UserBilingsAction extends UserInfoAbstractAction
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserBilingsAction.class);
	
	/**
	 * Numer strony
	 */
	private int pageNumber = 0;
	
	/**
	 * Liczba wynik�w
	 */
	private int maxResults = 20;
	
	/**
	 * Pole sortowania
	 */
	private String sortField = "dateTime";
	
	/**
	 * Kierunek sortowania
	 */
	private boolean ascending = false;
	
	/**
	 * Zakres dat
	 */
	private TimePeriod period;
	
	/**
	 * Lista bilingowa
	 */
	private List<Biling> bilings;
	
	/***
	 * Suma koszt�w po�acze�
	 */
	private Double sum;
	
	/**
	 * Paginator
	 */
	private Pager pager;
	
	/**
	 * Obiekt bilingu
	 */
	private Biling biling;
	
	/**
	 * Typy telefon�w
	 */
	private Map<String, String> phoneTypes;
	
	/**
	 * Numer telefonu do przeniesienia
	 */
	private String phoneNumber;
	/**
	 * Numer bilingu potrzebny aby zaktualizoweac tylko wpisy z tego bilingu
	 */
	private String bilingNumber;
	
	private boolean adminAction = false;

	private List<DSUser> users;
	
	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		UserBilingsList bilingsList = new UserBilingsList();
		
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			
			.append(bilingsList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSearch")
			.append(OpenHibernateSession.INSTANCE)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			
			.append(new ActionListener() {
				public void actionPerformed(ActionEvent event) 
				{
					// je�li filtruje wyniki to ustawienie numeru strony na pierwsz�
					pageNumber = 0;
				}
			})
			.append(bilingsList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("moveToPrivate")
			.append(OpenHibernateSession.INSTANCE)
			.append(getLoadTabsObj())
			.append(getCheckPermissionsObj())
			.append(new MoveToPrivatePhoneNumber())
			.append(bilingsList)
			.appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("removeFromPrivate")
			.append(OpenHibernateSession.INSTANCE)
			.append(getLoadTabsObj())
			.append(getCheckPermissionsObj())
			.append(new RemoveFromPrivatePhoneNumber())
			.append(bilingsList)
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Akcja wy�wietlaj�ca i filtruj�ca bilingi u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class UserBilingsList implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				if(adminAction)
				{
					users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				}
				
				if (getUserImpl() != null && getUserImpl().getName() != null)
				{
					UserImpl user = (UserImpl) DSUser.findByUsername(getUserImpl().getName());
					setUserImpl(user);
				}
				if (getUserImpl() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));
				
				if (biling == null)
					biling = new Biling();
				biling.setUsername(getUserImpl().getName());
				
				Pair<Date> datePair = null;
				// sprawdzenie zakresu dat
				if (period != null)
				{
					datePair = new Pair<Date>();
					if (period.getStartDate() != null)
						datePair.setFirst(DateUtils.parseJsDate(period.getStartDate()));
					if (period.getEndDate() != null)
						datePair.setSecond(DateUtils.parseJsDate(period.getEndDate()));
				}
				bilings = BilingsFactory.getBilings(biling, datePair, pageNumber, maxResults, sortField, ascending);
				sum = BilingsFactory.getBilingsSum(biling, datePair, pageNumber, maxResults, sortField, ascending);
				createPager(datePair);
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.info(ex.getMessage(), ex);
			}
		}
		
		/**
		 * Tworzy pager dla listy bilingowej
		 * 
		 * @param datePair
		 * @throws EdmException
		 */
		private void createPager(Pair<Date> datePair) throws EdmException
		{
			pager = new Pager(new Pager.LinkVisitor() {
				
				public String getLink(int offset) 
				{
					return offset + "";
				}
			}, pageNumber, maxResults, BilingsFactory.getCount(biling, datePair), 5, true);
		}
	}
	
	/**
	 * Przeniesienie wybranego numeru do prywatnych
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class RemoveFromPrivatePhoneNumber implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				UserPhoneNumber userPhoneNumber = BilingsFactory.findUserPhoneNumber(phoneNumber, getUserImpl().getName());
				if(userPhoneNumber != null)
				{
					userPhoneNumber.setUsername(null);
					DSApi.context().session().createQuery("update " + Biling.class.getName() 
							+ " b set b.type = ? where b.phoneNumberTo = ? and b.bilingNumber = ?")
							.setString(0, PhoneTypes.OFFICE.name())
				 			.setString(1, phoneNumber)
							.setString(2, bilingNumber)
							.executeUpdate();
					addActionMessage(sm.getString("PrzeniesionoZNumerowPrywatnych"));
				}
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				addActionError(ex.getMessage());
			}
		}
	}
	
	/**
	 * Przeniesienie wybranego numeru do prywatnych
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class MoveToPrivatePhoneNumber implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				UserPhoneNumber userPhoneNumber = new UserPhoneNumber(phoneNumber, getUserImpl().getName());
				DSApi.context().session().save(userPhoneNumber);
				
				DSApi.context().session().createQuery("update " + Biling.class.getName() 
						+ " b set b.type = ? where b.phoneNumberTo = ? and b.bilingNumber = ?")
						.setString(0, PhoneTypes.PRIVATE.name())
						.setString(1, phoneNumber)
						.setString(2, bilingNumber)
						.executeUpdate();
				
				addActionMessage(sm.getString("PrzeniesionoDoNumerowPrywatnych"));
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				LOG.error(ex.getMessage(), ex);
				addActionError(ex.getMessage());
			}
		}
	}

	public int getPageNumber() 
	{
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) 
	{
		this.pageNumber = pageNumber;
	}

	public int getMaxResults() 
	{
		return maxResults;
	}

	public void setMaxResults(int maxResults) 
	{
		this.maxResults = maxResults;
	}

	public List<Biling> getBilings() 
	{
		return bilings;
	}

	public void setBilings(List<Biling> bilings) 
	{
		this.bilings = bilings;
	}

	public Pager getPager() 
	{
		return pager;
	}

	public void setPager(Pager pager) 
	{
		this.pager = pager;
	}

	public String getSortField() 
	{
		return sortField;
	}

	public void setSortField(String sortField) 
	{
		this.sortField = sortField;
	}

	public boolean isAscending() 
	{
		return ascending;
	}

	public void setAscending(boolean ascending) 
	{
		this.ascending = ascending;
	}

	public TimePeriod getPeriod() 
	{
		return period;
	}

	public void setPeriod(TimePeriod timePeriod) 
	{
		this.period = timePeriod;
	}

	public Biling getBiling() 
	{
		return biling;
	}

	public void setBiling(Biling biling) 
	{
		this.biling = biling;
	}

	public Map<String, String> getPhoneTypes() 
	{
		phoneTypes = new LinkedHashMap<String, String>();
		phoneTypes.put("", "wszystkie");
		phoneTypes.put(PhoneTypes.OFFICE.name(), PhoneTypes.OFFICE.getName());
		phoneTypes.put(PhoneTypes.MOBILE.name(), PhoneTypes.MOBILE.getName());
		phoneTypes.put(PhoneTypes.PRIVATE.name(), PhoneTypes.PRIVATE.getName());
		return phoneTypes;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
	
	public Map<Integer, String> getMaxResultsMap()
	{
		Map<Integer, String> maxResultsMap = new LinkedHashMap<Integer, String>();
		for (int i = 5; i < 41; i += 5)
			maxResultsMap.put(i, i+"");
		maxResultsMap.put(Integer.MAX_VALUE, "Wszystkie");
		return maxResultsMap;
	}

	public String getBilingNumber() {
		return bilingNumber;
	}

	public void setBilingNumber(String bilingNumber) {
		this.bilingNumber = bilingNumber;
	}

	public boolean isAdminAction() {
		return adminAction;
	}

	public void setAdminAction(boolean adminAction) {
		this.adminAction = adminAction;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Double getSum() {
		return sum;
	}

	public void setSum(Double sum) {
		this.sum = sum;
	}
}
