package pl.compan.docusafe.web.userinfo;

import static pl.compan.docusafe.webwork.event.SessionControl.CLOSE_HIBERNATE_AND_JBPM_SESSION;
import static pl.compan.docusafe.webwork.event.SessionControl.OPEN_HIBERNATE_AND_JBPM_SESSION;

import java.util.Date;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

/**
 * Klasa odpowiedzialna za implementacj� akcji wczytania danych u�ytkownika oraz zdj�cia profilu
 * z przekazanych parametr�w z adresu url.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class LoadUserInfoAction extends UserInfoAbstractAction 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LoadUserInfoAction.class);
	
	/**
	 * Zdj�cie profilu u�ytkownika
	 */
	private Image image;
	
	/***
	 * Czy ma uprawnienia admin;
	 */
	private Boolean isAdmin;
	
	/**
	 * Czy pracoenik jest w pracy czy na urloipie 
	 */
	private Boolean atWork;
	
	/**
	 * Kiedy wraca 
	 */
	private Date whenReturn;
	
	/**
	 * Sk�adowa przechowuj�ca obiekt klasy @see {@link LoadUserPhoto}
	 */
	private LoadUserPhoto loadUserPhoto;

	public DSDivision[] divisions;

	private  String substitution;
	
	//zmienna przechowuj�ca informacj� czy i jaka warto�� jest ustawiona dla addsa user_edit_option w adds.properties (domy�lnie jest ustawione na on)
		//user_edit_option = on    - edycja w��czona
		//					 off   - edycja wy��czona
		//					 admin - tylko admin mo�e edytowa�
	protected boolean addsEdit = true;

	/**
	 * Konfiguracja akcji
	 */
	@Override
	protected void setup() 
	{
		loadUserPhoto = new LoadUserPhoto();
		
		registerListener(DEFAULT_ACTION)
			.append(OPEN_HIBERNATE_AND_JBPM_SESSION)
			.append(new LoadUser())
			.append(loadUserPhoto)
			.append(getCheckPermissionsObj())
			.append(getLoadTabsObj())
			.append(new AddsEdit())
			.appendFinally(CLOSE_HIBERNATE_AND_JBPM_SESSION);
	}
	
	private class AddsEdit implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			try{
				String option = Docusafe.getAdditionProperty("user_edit_option");
				LOG.error("wybrana opcja=" + option);
				if(option.equalsIgnoreCase("off")){
					addsEdit = false;
				}
				else if(option.equalsIgnoreCase("admin")){
					DSApi.context().begin();
					if(DSApi.context().isAdmin()) addsEdit = true;
					else addsEdit = false;
				}
				else addsEdit = true;
			}catch(Exception e){
				addsEdit=false;
				LOG.error(e.getMessage(), e);
			}
		}
		
	}
	
	/**
	 * Klasa implementuj�ca akcj� za�adowania danych u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	protected class LoadUser implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				isAdmin = DSApi.context().isAdmin();
				
				if(getUserImpl() == null)
				{
					setUserImpl(new UserImpl());
					getUserImpl().setName(DSApi.context().getPrincipalName());
				}
				if (getUserImpl() != null && getUserImpl().getName() != null)
				{
					// wyszukanie u�ytkownika o podanej nazwie
					UserImpl user = (UserImpl) UserImpl.findByUsername(getUserImpl().getName());
					// pobranie prze�o�onych
					user.getSupervisorsHierarchy();
					// ustawienie obiektu usera dla widoku
					setUserImpl(user);
					
					if(user.getSubstituteUser() != null)
						substitution = user.getSubstituteUser().asFirstnameLastname();
					
				}
				
				if (getUserImpl() == null)
					throw new EdmException(sm.getString("BrakUzytkownikaOTakiejNazwie"));
				atWork = AbsenceFactory.isUserAtWork(getUserImpl().getName());
				if(!atWork)
				{
					whenReturn = AbsenceFactory.whenUserRetern(getUserImpl().getName());
					Long subUserId = AbsenceFactory.whoSubSuser(getUserImpl().getId());
					System.out.println(subUserId);
					if(subUserId != null)
						substitution = DSApi.context().load(UserImpl.class, subUserId).asLastnameFirstname();
					
				}
				divisions = getUserImpl().getOriginalDivisionsWithoutGroup();
				DSApi.context().commit();
			}
			catch (Exception ex)
			{
				LOG.error(ex.getMessage(), ex);
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				
			}
		}
	}
	
	/**
	 * Klasa implementuj�ca akcj� za�adowania zdj�cia danego u�ytkownika
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class LoadUserPhoto implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
				DSApi.context().begin();
				
				if (getUserImpl() != null)
				{
					// za�adowanie zdj�cia profilu u�ytkownika
					image = getUserImpl().getImage();
				}
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
				addActionError(ex.getMessage());
				LOG.error(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Metoda zwracaj�ca sk�adow� image
	 * 
	 * @return
	 */
	public Image getImage() 
	{
		return image;
	}
	
	/**
	 * Metoda ustawiaj�ca sk�adow� image
	 * 
	 * @param image
	 */
	public void setImage(Image image) 
	{
		this.image = image;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� loadUserPhoto
	 * 
	 * @return
	 */
	public LoadUserPhoto getLoadUserPhotoObj()
	{
		return loadUserPhoto;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public DSDivision[] getDivisions() {
		return divisions;
	}

	public void setDivisions(DSDivision[] divisions) {
		this.divisions = divisions;
	}

	public Boolean getAtWork() {
		return atWork;
	}

	public void setAtWork(Boolean atWork) {
		this.atWork = atWork;
	}

	public Date getWhenReturn() {
		return whenReturn;
	}

	public void setWhenReturn(Date whenReturn) {
		this.whenReturn = whenReturn;
	}

	public String getSubstitution() {
		return substitution;
	}

	public void setSubstitution(String substitution) {
		this.substitution = substitution;
	}

	public boolean isAddsEdit() {
		return addsEdit;
	}

	public void setAddsEdit(boolean addsEdit) {
		this.addsEdit = addsEdit;
	}
}
