package pl.compan.docusafe.web.certificates.admin;

import org.bouncycastle.openssl.PasswordFinder;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.certificates.PEMReader;
import pl.compan.docusafe.core.certificates.UserCertificate;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ListCsrsAction.java,v 1.7 2006/02/20 15:42:37 lk Exp $
 */
public class ListCsrsAction extends EventActionSupport
{
    private List csrs;
    private Long[] signIds;
    private String keyPassword;
    private boolean canSign = true;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSign").
            append(OpenHibernateSession.INSTANCE).
            append(new Sign()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doReject").
            append(OpenHibernateSession.INSTANCE).
            append(new Reject()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                List list = UserCertificate.list(false);
                csrs = new ArrayList(list.size());
                for (Iterator iter=list.iterator(); iter.hasNext(); )
                {
                    UserCertificate uc = (UserCertificate) iter.next();
                    Map bean = new HashMap();
                    bean.put("id", uc.getId());
                    bean.put("user", DSUser.findByUsername(uc.getUsername()));
                    bean.put("csr", uc.getPKCS10CertificationRequest());
                    csrs.add(bean);
                }

                String cert = GlobalPreferences.getCaCertificatePath();
                String key = GlobalPreferences.getCaPrivateKeyPath();
                if (cert == null || key == null)
                {
                    canSign = false;
                    addActionError("Nie mo�na podpisywa� CSR, poniewa� nie podano " +
                        "�cie�ek do certyfikatu i klucza prywatnego CA");
                }
                else
                {
                    File certFile = new File(cert);
                    File keyFile = new File(key);
                    if (!certFile.exists() || !certFile.isFile() || !certFile.canRead())
                    {
                        addActionError("Podana �cie�ka do certyfikatu CA jest nieprawid�owa");
                        canSign = false;
                    }
                    if (!keyFile.exists() || !keyFile.isFile() || !keyFile.canRead())
                    {
                        addActionError("Podana �cie�ka do klucza prywatnego CA jest nieprawid�owa");
                        canSign = false;
                    }
                }
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Sign implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (signIds == null || signIds.length == 0)
                addActionError("Nie wybrano CSR do podpisania");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                String caCertPath = GlobalPreferences.getCaCertificatePath();
                String caKeyPath = GlobalPreferences.getCaPrivateKeyPath();

                if (caCertPath == null)
                    throw new EdmException("Nie podano �cie�ki do certyfikatu CA");
                if (caKeyPath == null)
                    throw new EdmException("Nie podano �cie�ki do klucza prywatnego CA");

                File caCertFile = new File(caCertPath);
                File caKeyFile = new File(caKeyPath);

                if (!caCertFile.exists() || !caCertFile.isFile() || !caCertFile.canRead())
                    addActionError("Podana �cie�ka do certyfikatu CA jest nieprawid�owa");
                if (!caKeyFile.exists() || !caKeyFile.isFile() || !caKeyFile.canRead())
                    addActionError("Podana �cie�ka do klucza prywatnego CA jest nieprawid�owa");

                PrivateKey privateKey = readPrivateKey(caKeyFile, keyPassword);
                X509Certificate caCertificate = readCertificate(caCertFile);

                int count = 0;

                for (int i=0; i < signIds.length; i++)
                {
                    UserCertificate csr = UserCertificate.findById(signIds[i]);
                    try
                    {
                        // X.509
                        csr.sign(caCertificate, privateKey, 365,
                            csr.getId().intValue());
                        X509Certificate certificate = csr.getX509Certificate();

                        DSUser user = DSUser.findByUsername(csr.getUsername());
                        user.addCertificate(certificate);
                        user.update();

                        // PKCS#7
/*
                        PKCS7SignedData pkcs7 = new PKCS7SignedData(
                        PKCS7SignedData==3118008==pkcs7
                            privateKey,
                            new Certificate[] { certificate },
                            "SHA1" );
*/

                        // PKCS7SignedData pkcs7 =

                        // csr.setEncodedPkcs7(new String(Base64.encodeBase64(pkcs7.getEncoded()), "iso-8859-1"));

                        count++;
                    }
                    catch (UserNotFoundException e)
                    {
                        addActionMessage("Nie znaleziono u�ytkownika "+csr.getUsername()+" certyfikat "+
                            csr.getPKCS10CertificationRequest().getCertificationRequestInfo().getSubject().toString()+
                            " nie zostanie podpisany");
                    }
                }

                DSApi.context().commit();

                addActionMessage("Podpisano "+count+" certyfikat�w");
            }
            catch (Exception e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Reject implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (signIds == null || signIds.length == 0)
                addActionError("Nie wybrano CSR do odrzucenia");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                for (int i=0; i < signIds.length; i++)
                {
                    UserCertificate csr = UserCertificate.findById(signIds[i]);
                    csr.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private PrivateKey readPrivateKey(File file, final String password) throws EdmException
    {
        FileReader reader = null;
        try
        {
            reader = new FileReader(file);
            Object key = new PEMReader(reader,
                new PasswordFinder() {
                    public char[] getPassword()
                    {
                        return password != null ? password.toCharArray() : new char[0];
                    }
                }).readObject();

            if (key instanceof KeyPair)
            {
                return ((KeyPair) key).getPrivate();
            }
            else if (key instanceof PrivateKey)
            {
                return (PrivateKey) key;
            }
            else
            {
                throw new EdmException("Nie rozpoznano obiektu reprezentuj�cego klucz prywatny: "+
                    (key != null ? key.getClass().getName() : "null")+" pobranego z pliku "+file);
            }
        }
        catch (EdmException e)
        {
            throw e;
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu klucza prywatnego CA, by� " +
                "mo�e podano b��dne has�o", e);
        }
        catch (Exception e)
        {
            throw new EdmException("Wyst�pi� b��d podczas odczytu klucza prywatnego: "+e.getMessage(), e);
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }
    }

    private X509Certificate readCertificate(File file) throws EdmException
    {
        FileReader reader = null;
        try
        {
            reader = new FileReader(file);
            Object cert = new PEMReader(reader).readObject();

            if (cert instanceof X509Certificate)
            {
                return (X509Certificate) cert;
            }
            else
            {
                throw new EdmException("Nie rozpoznano obiektu reprezentuj�cego certyfikat CA: "+
                    (cert != null ? cert.getClass().getName() : "null")+" pobranego z pliku "+file);
            }
        }
        catch (EdmException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new EdmException("Wyst�pi� b��d podczas odczytu certyfikatu CA: "+e.getMessage());
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }

    }

    public List getCsrs()
    {
        return csrs;
    }

    public void setSignIds(Long[] signIds)
    {
        this.signIds = signIds;
    }

    public void setKeyPassword(String keyPassword)
    {
        this.keyPassword = keyPassword;
    }

    public boolean isCanSign()
    {
        return canSign;
    }
}
