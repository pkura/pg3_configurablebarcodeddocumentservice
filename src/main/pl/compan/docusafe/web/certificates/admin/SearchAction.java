package pl.compan.docusafe.web.certificates.admin;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.certificates.UserCertificate;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.webwork.event.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* User: Administrator, Date: 2005-11-03 14:04:55 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: SearchAction.java,v 1.4 2008/10/06 11:10:15 pecet4 Exp $
 */
public class SearchAction extends EventActionSupport
{
    private static final int LIMIT = 10;

    private String signingTimeFrom;
    private String signingTimeTo;
    private String expirationTimeFrom;
    private String expirationTimeTo;
    private String expired; // "yes", "nearly"
    private String username;
    private int offset;
    private String sortField;
    private boolean ascending;

    private List users;
    private SearchResults results;
    private Pager pager;
    private Map expiredOptions = new LinkedHashMap();
    {
        expiredOptions.put("yes", "wygas�e");
        expiredOptions.put("nearly", "prawie wygas�e");
    }

    private String thisSearchUrl;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
            append(new Search()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                
                UserCertificate.CertificateQuery query =
                    new UserCertificate.CertificateQuery(offset, LIMIT);
    
                if (!StringUtils.isEmpty(sortField))
                    if (ascending)
                        query.orderAsc(sortField);
                    else
                        query.orderDesc(sortField);
    
                query.signingTime(
                    DateUtils.nullSafeParseJsDate(signingTimeFrom),
                    DateUtils.nullSafeParseJsDate(signingTimeTo));
    
                if (!StringUtils.isEmpty(expired))
                {
                    if ("yes".equals(expired))
                    {
                        query.expirationTime(null, new Date());
                        query.orderDesc("expirationTime");
                    }
                    else if ("nearly".equals(expired))
                    {
                        query.expirationTime(null, new Date());
                        query.orderAsc("expirationTime");
                    }
                }
                else
                {
                    query.expirationTime(
                        DateUtils.nullSafeParseJsDate(expirationTimeFrom),
                        DateUtils.nullSafeParseJsDate(expirationTimeTo));
                }
    
                if (!StringUtils.isEmpty(username))
                {
                    query.username(username);
                }
    
                query.orderAsc("signingTime");
    
                thisSearchUrl = HttpUtils.makeUrl(
                    "/certificates/admin/search.action",
                    new Object[] {
                        "doSearch", "true",
                        "signingTimeFrom", signingTimeFrom,
                        "signingTimeTo", signingTimeTo,
                        "expirationTimeFrom", expirationTimeFrom,
                        "expirationTimeTo", expirationTimeTo,
                        "expired", expired,
                        "username", username
                    }
                );

            
                // w search (visitQuery) nie widac kryteriow
                results = UserCertificate.search(query);
//                    " total="+results.totalCount()+" count="+results.count()+
//                    " expired="+expired);

                if (results.count() == 0)
                    addActionMessage("Nie znaleziono certyfikat�w");

                pager = new Pager(new Pager.LinkVisitor()
                {
                    public String getLink(int offset)
                    {
                        return thisSearchUrl + "&offset="+offset;
                    }
                }, offset, LIMIT, results.totalCount(), 10);
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    public String getSortLink(String field, boolean ascending)
    {
        return thisSearchUrl + "&sortField="+field+"&ascending="+ascending;
    }

    public String getSigningTimeFrom()
    {
        return signingTimeFrom;
    }

    public void setSigningTimeFrom(String signingTimeFrom)
    {
        this.signingTimeFrom = signingTimeFrom;
    }

    public String getSigningTimeTo()
    {
        return signingTimeTo;
    }

    public void setSigningTimeTo(String signingTimeTo)
    {
        this.signingTimeTo = signingTimeTo;
    }

    public String getExpirationTimeFrom()
    {
        return expirationTimeFrom;
    }

    public void setExpirationTimeFrom(String expirationTimeFrom)
    {
        this.expirationTimeFrom = expirationTimeFrom;
    }

    public String getExpirationTimeTo()
    {
        return expirationTimeTo;
    }

    public void setExpirationTimeTo(String expirationTimeTo)
    {
        this.expirationTimeTo = expirationTimeTo;
    }

    public String getExpired()
    {
        return expired;
    }

    public void setExpired(String expired)
    {
        this.expired = expired;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public List getUsers()
    {
        return users;
    }

    public SearchResults getResults()
    {
        return results;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public Map getExpiredOptions()
    {
        return expiredOptions;
    }

    public Pager getPager()
    {
        return pager;
    }

    public String getSortField()
    {
        return sortField;
    }

    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }

    public boolean isAscending()
    {
        return ascending;
    }

    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }
}
