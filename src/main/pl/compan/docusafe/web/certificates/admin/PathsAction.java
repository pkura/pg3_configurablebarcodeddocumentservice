package pl.compan.docusafe.web.certificates.admin;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.*;

import java.io.File;

/* User: Administrator, Date: 2005-06-09 09:51:06 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PathsAction.java,v 1.2 2006/02/20 15:42:37 lk Exp $
 */
public class PathsAction extends EventActionSupport
{
    private String caCertPath;
    private String caKeyPath;

    private static final String EV_FILL = "fill";

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(EV_FILL, fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            caCertPath = GlobalPreferences.getCaCertificatePath();
            caKeyPath = GlobalPreferences.getCaPrivateKeyPath();
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (caCertPath != null)
            {
                File certFile = new File(caCertPath);
                if (!certFile.exists() || !certFile.isFile() || !certFile.canRead())
                {
                    addActionError("Podana �cie�ka do certyfikatu CA jest nieprawid�owa");
                }
            }

            if (caKeyPath != null)
            {
                File keyFile = new File(caKeyPath);
                if (!keyFile.exists() || !keyFile.isFile() || !keyFile.canRead())
                {
                    addActionError("Podana �cie�ka do klucza prywatnego CA jest nieprawid�owa");
                }
            }

            if (hasActionErrors())
            {
                event.skip(EV_FILL);
                return;
            }

            try
            {
                DSApi.context().begin();


                GlobalPreferences.setCaCertificatePath(TextUtils.trimmedStringOrNull(caCertPath));
                GlobalPreferences.setCaPrivateKeyPath(TextUtils.trimmedStringOrNull(caKeyPath));

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public String getCaCertPath()
    {
        return caCertPath;
    }

    public void setCaCertPath(String caCertPath)
    {
        this.caCertPath = caCertPath;
    }

    public String getCaKeyPath()
    {
        return caKeyPath;
    }

    public void setCaKeyPath(String caKeyPath)
    {
        this.caKeyPath = caKeyPath;
    }
}
