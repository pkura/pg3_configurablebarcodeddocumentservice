package pl.compan.docusafe.web.certificates;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.w3c.dom.NodeList;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SignatureValidator{
	private static final Logger log = LoggerFactory.getLogger(SignatureValidator.class);

	/**
	 * weryfikuje poprawność podpisu
	 * @param signatureFile
	 * @return
	 * @throws Exception
	 */
    public static boolean validate(String signatureFile) throws Exception{

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		org.w3c.dom.Document doc =
			dbf.newDocumentBuilder().parse(IOUtils.toInputStream(signatureFile,"UTF-8"));

		// wyszukanie elementu z podpisem
		NodeList nl =
			doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
		if (nl.getLength() == 0) {
			throw new Exception("Cannot find Signature element");
		}

		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
		
		for(int i=0; i<nl.getLength(); i++){
			KeyValueKeySelector keySelector = new KeyValueKeySelector();
			keySelector.signatureFile=signatureFile;
			keySelector.i = i;
			
			DOMValidateContext valContext = new DOMValidateContext(keySelector, nl.item(i));
			XMLSignature signature = fac.unmarshalXMLSignature(valContext);

			//walidacja podpisu
			boolean coreValidity = signature.validate(valContext);

			// Check core validation status
			if (coreValidity == false) {
				log.error("Signature " + i + " failed core validation");
				boolean sv = signature.getSignatureValue().validate(valContext);
				log.error("signature validation status: " + sv);
				// check the validation status of each Reference
				Iterator k = signature.getSignedInfo().getReferences().iterator();
				for (int j=0; k.hasNext(); j++) {
					boolean refValid =
						((Reference) k.next()).validate(valContext);
					log.error("ref["+j+"] validity status: " + refValid);
				}
				return false;
			} else {
				log.info("Signature " + i + " passed core validation");
			}
		}
		return true;
		
    }
	/**
	 * KeySelector zwraca klucz publiczny certyfikatu
	 */
	private static class KeyValueKeySelector extends KeySelector {
		public String signatureFile;
		public int i;
		
		public KeySelectorResult select(KeyInfo keyInfo,
				KeySelector.Purpose purpose,
				AlgorithmMethod method,
				XMLCryptoContext context)
		throws KeySelectorException {
			if (keyInfo == null) {
				throw new KeySelectorException("Null KeyInfo object!");
			}
			X509Certificate cert = null;
			try {
				cert = extractX509Certificate(IOUtils.toInputStream(signatureFile,"UTF-8"),i);
			} catch (Exception e) {
				e.printStackTrace();
			}
			SignatureMethod sm = (SignatureMethod) method;
			PublicKey pk = null;
			if(cert!=null){
				pk = cert.getPublicKey();
				if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
					return new SimpleKeySelectorResult(pk);
				}
			}
			throw new KeySelectorException("No KeyValue element found!");
		}

		//@@@FIXME: this should also work for key types other than DSA/RSA
		static boolean algEquals(String algURI, String algName) {
			if (algName.equalsIgnoreCase("DSA") &&
					algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) {
				return true;
			} else if (algName.equalsIgnoreCase("RSA") &&
					algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)) {
				return true;
			} else {
				return false;
			}
		}
		
	}

	private static class SimpleKeySelectorResult implements KeySelectorResult {
		private PublicKey pk;
		SimpleKeySelectorResult(PublicKey pk) {
			this.pk = pk;
		}

		public Key getKey() { return pk; }
	}
	public static X509Certificate extractX509Certificate(InputStream es) throws Exception {
		return extractX509Certificate(es, 0);
	}
	
	public static X509Certificate extractX509Certificate(InputStream es, int i) throws Exception {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Object result = getCertificateElement(es);
        if(result instanceof Element) {
            Element el = (Element) result;
            StringBuilder sb = new StringBuilder()
            		.append("-----BEGIN CERTIFICATE-----\n")
            		.append(el.getText().replace(" ",""))
            		.append("\n-----END CERTIFICATE-----");
            return (X509Certificate) cf.generateCertificate(IOUtils.toInputStream(sb.toString(),"UTF-8"));
        }
        else if(result instanceof ArrayList){
        	ListIterator iter = ((ArrayList) result).listIterator();
        	int k = 0;
        	while(iter.hasNext()){
        		Object o = iter.next();
        		if(k==i){
		    		if(o instanceof Element){
		    			Element el = (Element) o;
		                StringBuilder sb = new StringBuilder()
		                		.append("-----BEGIN CERTIFICATE-----\n")
		                		.append(el.getText().replace(" ",""))
		                		.append("\n-----END CERTIFICATE-----");
		                log.debug("der encoding:\n{}", sb);
		                return (X509Certificate) cf.generateCertificate(IOUtils.toInputStream(sb.toString()));
		    		}
        		}
        		k++;
        	}
        }
		return null; 
    }
	private static Object getCertificateElement(InputStream es) throws IOException, DocumentException {
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ds","http://www.w3.org/2000/09/xmldsig#");
		
		org.dom4j.Document xml = new SAXReader().read(es);
	    XPath xpath = xml.createXPath("//ds:X509Certificate");
	
	    xpath.setNamespaceURIs(namespaces);
	    Object result = xpath.evaluate(xml.getRootElement());			
		
		return result;
	}
    
}