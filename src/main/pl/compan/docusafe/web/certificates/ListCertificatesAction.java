package pl.compan.docusafe.web.certificates;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.certificates.UserCertificate;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wy�wietla wszystkie certyfikaty u�ytkownika oraz jego CSR
 * oczekuj�ce na podpisanie.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ListCertificatesAction.java,v 1.10 2008/10/06 10:34:53 pecet4 Exp $
 */
public class ListCertificatesAction extends EventActionSupport
{
    // @EXPORT
    private List<Map<String, Object>> certificates;
    private List<UserCertificate> csrs;
    private String csrDn;

    private FormFile csrFile;
    private Long[] deleteIds;
    private String pkcs10Data;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDeleteCsrs").
            append(OpenHibernateSession.INSTANCE).
            append(new DeleteCsrs()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSend").
            append(OpenHibernateSession.INSTANCE).
            append(new Send()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSendGen").
            append(OpenHibernateSession.INSTANCE).
            append(new SendGen()).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                X509Certificate[] certs = DSApi.context().getDSUser().getCertificates();
                certificates = new ArrayList<Map<String, Object>>(certs.length);
                for (X509Certificate cert : certs)
                {
                    Map<String, Object> bean = new HashMap<String, Object>();
                    bean.put("subjectDN", cert.getSubjectDN().getName());
                    bean.put("issuerDN", cert.getIssuerDN().getName());
                    try
                    {
                        bean.put("encoded64", new String(Base64.encodeBase64(cert.getEncoded())));
                    }
                    catch (CertificateEncodingException e)
                    {
                        bean.put("encoded64", "");
                    }
                    bean.put("signature", HttpUtils.urlEncode(new String(Base64.encodeBase64(cert.getSignature())), "iso-8859-1"));

                    // wyszukanie obiektu UserCertificate pasuj�cego do tego obiektu
                    List<UserCertificate> signedCerts = UserCertificate.findByUsername(DSApi.context().getPrincipalName(), true);
                    for (UserCertificate signedCert : signedCerts)
                    {
                        if (signedCert.getX509Certificate().equals(cert))
                        {
                            bean.put("pkcs7", signedCert.getEncodedPkcs7());
                            bean.put("stime", signedCert.getSigningTime());
                            break;
                        }
                    }

                    //bean.put("pkcs7", certs[i].getEncoded())
                    certificates.add(bean);
                }
                csrs = UserCertificate.findByUsername(DSApi.context().getPrincipalName(), false);

                DSUser user = DSApi.context().getDSUser();
                csrDn = "C=PL;"+
                    (user.getEmail() != null ? "Email="+user.getEmail()+";" : "") +
                    "CN="+ TextUtils.latinize(user.asFirstnameLastname().length() > 0 ? user.asFirstnameLastname() : user.getName())+";";
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class DeleteCsrs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (deleteIds == null || deleteIds.length == 0)
                addActionError("Nie wybrano element�w do usuni�cia");

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                //for (int i=0; i < deleteIds.length; i++)
                for (Long id : deleteIds)
                {
                    UserCertificate certificate = UserCertificate.findById(id);
                    if (certificate.getUsername().equals(DSApi.context().getPrincipalName()))
                        certificate.delete();
                }

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class Send implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (csrFile == null || !csrFile.sensible())
                addActionError("Nie podano nazwy pliku lub plik ma zerow� d�ugo��");
            if (csrFile.getFile().length() > 10*1024)
                addActionError("Przekazany plik jest zbyt du�y, by� mo�e nie jest to prawid�owy plik PKCS#10");

            ByteArrayOutputStream os = new ByteArrayOutputStream((int) csrFile.getFile().length());
            try
            {
                byte[] buf = new byte[1024];
                int count;
                InputStream is = new FileInputStream(csrFile.getFile());
                while ((count = is.read(buf)) > 0)
                {
                    os.write(buf, 0, count);
                }
                is.close();
            }
            catch (IOException e)
            {
                addActionError("Nie mo�na odczyta� przekazanego pliku, skontaktuj si� z administratorem systemu");
            }

            PKCS10CertificationRequest csr = null;

            try
            {
                csr = UserCertificate.getPKCS10CertificationRequest(os.toByteArray());
            }
            catch (Exception e)
            {
                addActionError("Niepoprawny format pliku CSR");
                LogFactory.getLog("eprint").debug("", e);
            }

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                UserCertificate.create(csr, DSApi.context().getPrincipalName(), null);

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class SendGen implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (pkcs10Data == null || pkcs10Data.length() == 0)
                addActionError("Nie otrzymano CSR");

            byte[] pkcsBytes = null;
            try
            {
                pkcsBytes = org.bouncycastle.util.encoders.Base64.decode(pkcs10Data);
            }
            catch (Exception e)
            {
                addActionError("Otrzymane CSR jest niepoprawnie zakodowane (base64)");
            }

            if (pkcsBytes == null)
                addActionError("Nie mo�na zdekodowa� CSR");

            PKCS10CertificationRequest pkcs10 = null;

            try
            {
                pkcs10 = UserCertificate.getPKCS10CertificationRequest(pkcsBytes);
            }
            catch (EdmException e)
            {
                addActionError("Nie mo�na zdekodowa� CSR ("+e.getMessage()+")");
                event.getLog().error(e.getMessage(), e);
            }

            if (hasActionErrors())
                return;

            try
            {
                DSApi.context().begin();

                UserCertificate.create(pkcs10, DSApi.context().getPrincipalName(), null);

                DSApi.context().commit();

                addActionMessage("Otrzymano ��danie certyfikatu wydane dla \""+pkcs10.getCertificationRequestInfo().getSubject()+"\"");
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    public List getCertificates()
    {
        return certificates;
    }

    public void setCsrFile(FormFile csrFile)
    {
        this.csrFile = csrFile;
    }

    public List getCsrs()
    {
        return csrs;
    }

    public void setDeleteIds(Long[] deleteIds)
    {
        this.deleteIds = deleteIds;
    }

    public void setPkcs10Data(String pkcs10Data)
    {
        this.pkcs10Data = pkcs10Data;
    }

    public String getCsrDn()
    {
        return csrDn;
    }
}
