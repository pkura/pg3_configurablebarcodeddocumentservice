package pl.compan.docusafe.web.certificates;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.PublicKey;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.XmlGenerator;
import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.core.certificates.ElectronicSignatureBean;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.BooleanColumnField;
import pl.compan.docusafe.core.dockinds.field.BooleanField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessActionEntry;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;
import pl.compan.docusafe.parametrization.pg.PgXmlGenerator;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.LoggedActionListener;
import pl.compan.docusafe.web.office.common.TransactionalActionListener;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Akcja umo�liwiaj�ca podpisywanie xml'i i sprawdzanie poprawno�ci podpisu
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class SignXmlAction extends EventActionSupport {
    private final static Logger LOG = LoggerFactory.getLogger(SignXmlAction.class);
    private final static StringManager sm =
            GlobalPreferences.loadPropertiesFile(SignXmlAction.class.getPackage().getName(), null);

    static Map<String, String> getStyleSheetDocumentPi() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("type","text/xls");
        map.put("href", Docusafe.getBaseUrl() + "/certificates/document.xsl");
        return map;
    }

    static Map<String, String> getStyleSheetActionPi() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("type","text/xls");
        map.put("href", Docusafe.getBaseUrl() + "/certificates/action.xsl");
        return map;
    }
	public static final Integer USLUGA_DORECZYCIEL = 50;
	public static final Integer USLUGA_SKRYTKA = 51;
    private String certificateData;
    private String signatureData;
    private String attachmentDescription;

    //EXPORT/IMPORT
    /** Plik (tre��) xml do podpisu */
    private String signedFile;
    /** Adres strony do kt�rej nale�y powr�ci� po podpisaniu */
    private String returnUrl;

    //IMPORT
    private Boolean epuap;				// czy podpisywany element jest do wyslania w ePUAP
    private Long attachmentRevision;
    private Long documentId;
    private String actionName;
    private String processName;
    private String processId;
    private String processActivity;
    /** Plik (tre��) podpisu */
    private String signatureFile;
    private Long signatureId;

    protected void setup() {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
                append(OpenHibernateSession.INSTANCE).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSign").
                append(OpenHibernateSession.INSTANCE).
                append(new SignXml()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doValidate").
                append(OpenHibernateSession.INSTANCE).
                append(new ValidateSign()).
                append(fillForm).
                appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm extends LoggedActionListener {
        @Override
        public void actionPerformed(ActionEvent event, Logger log)
                throws Exception {
            log.info("returnUrl = '{}'", returnUrl);
            if(signatureId != null) {
                ElectronicSignature es = ElectronicSignatureStore.getInstance().get(signatureId);
                X509Certificate cert = ElectronicSignatureStore.getInstance().extractX509Certificate(es);
                signatureFile = es.getSignatureFile();
                signedFile = es.getSignedFile();
//                LOG.info("X509 cert data\nissuerDN='{}'\nnotBefore='{}'\nnotAfter'{}'\nsigAlgName='{}'\nsubjectDN='{}'",
//                        cert.getIssuerX500Principal().
//                        cert.getNotBefore(),
//                        cert.getNotAfter(),
//                        cert.getSigAlgName(),
//                        cert.getSubjectDN());
                StringBuilder sb = new StringBuilder();
                sb.append("Wystawione przez:\n   ").append(cert.getIssuerX500Principal().getName())
                        .append("\n\nWystawione dla:\n   ").append(cert.getSubjectDN().getName()+"\n").append(cert.getSubjectX500Principal().getName())
                        .append("\n\nNumer seryjny:\n   ").append(cert.getSerialNumber())
                        .append("\n\nWa�ny od:\n   ").append(DateUtils.formatJsDate(cert.getNotBefore()))
                        .append("\n\nWa�ny do:\n   ").append(DateUtils.formatJsDate(cert.getNotAfter()))
                        ;
                certificateData = sb.toString();
                sb.setLength(0);
                sb.append("Czas podpisu:\n   ").append(DateUtils.formatJsDateTime(es.getDate()))
                    .append("\n\nWa�ny certyfikat w momencie podpisywania:\n   ")
                    ;
                try {
                    cert.checkValidity(es.getDate());
                    sb.append("Tak");
                } catch (CertificateExpiredException ex) {
                    sb.append("Nie - podpis wykonany po okresie wa�no�ci");
                } catch (CertificateNotYetValidException ex) {
                    sb.append("Nie - podpis wykonany przed okresem wa�no�ci");
                }
                if(es.getVerifyDate()!=null){
                	sb.append("\n\nWeryfikacja podpisu:\n");
                	if(es.getVerifyResult().booleanValue()==true)
                		sb.append("   Poprawny");
                	else
                		sb.append("   Niepoprawny");
        			sb.append("\n\nData weryfikacji:\n   ")
                		.append(DateUtils.formatJsDateTime(es.getVerifyDate()));	
                }
                signatureData = sb.toString();
            } else if(attachmentRevision != null){
                prepareSigningFileForAttachment(event, log);
            } else if(documentId != null) {
            	if( epuap!=null && epuap==true )
            		prepareSigningFileForDocumentEpuap(documentId);
            	else
            		prepareSigningFileForDocument(documentId);
            } else if(actionName != null) {
                prepareSigningFileForProcessAction();
            } else {
                addActionError("Nie ma nic do podpisania");
            }
        }

        @Override
        public Logger getLogger() {
            return LOG;
        }
	}

        
    private void prepareSigningFileForProcessAction() throws EdmException {
        LOG.info("actionName = {}, processName = {}, processId = {}, processActivity = {}",
                actionName, processName, processId, processActivity);
        Document doc = Document.find(Jbpm4ProcessLocator.documentIdForProcess(processId));
        ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
                .getProcessesDeclarations().getProcess(getProcessName());
        ProcessInstance pi = pdef.getLocator().lookupForId(getProcessId());

        DocumentFactory df =  DocumentFactory.getInstance();
        org.dom4j.Document xml = df.createDocument();
        xml.setProcessingInstructions(
                Arrays.asList(
                        df.createProcessingInstruction("xml-stylesheet", getStyleSheetActionPi())
                ));
        xml.add(df.createElement("action"));

        xml.getRootElement()
                .addAttribute("process-name", sm.getString("process." + processName + ".label"))
                .addAttribute("action-name", sm.getString("process." + processName + ".actions." + actionName + ".label"));

        signedFile = xml.asXML();
    }

    public void prepareSigningFileForDocumentEpuap(Long documentId)  throws Exception 
	{
    	OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(documentId);
    	
    	String data = ( doc.getDocumentDate()!=null )? DateUtils.formatJsDate(doc.getDocumentDate()) : "";
		signedFile= EpuapExportManager.createOdpowiedz(doc.getDescription(),data,doc.getAttachments());
		/*PrintWriter zapiss4 = new PrintWriter("H:/signedfilepoczatkowa.xml");
	      zapiss4.println(signedFile);
	      zapiss4.close();*/
		//BASE64Encoder encoder = new BASE64Encoder();
		//BASE64Decoder decoder = new BASE64Decoder();
		
		/*try{
			String encodedBytes = encoder.encodeBuffer(signedFile.getBytes());
			System.out.println("encodedBytes " + encodedBytes);
			signedFile = encodedBytes;
			byte[] decodedBytes = decoder.decodeBuffer(encodedBytes);
			System.out.println("decodedBytes " + new String(decodedBytes));
	    } catch (IOException e) {
	      e.printStackTrace();
	   
		String defCharset = signedFile;
		String tekst = new String(defCharset.getBytes("UTF-8"));
		String tekst="";
		signedFile = tekst;
		
		signedFile = signedFile.trim();
		
		String text = "";
		PrintWriter zapiss = new PrintWriter("E:/aEPUAP/signedFileDoprzerobki.xml");
	      zapiss.println(signedFile);
	      zapiss.close();
		 }*/
		// wyci�cie bajt�w za�acznika z sygnat�ry do weryfikacji polskich znak�w 
		String defCharset = signedFile;
		String zalacznik ="";
		
		
		
		/*List<String> polskieZnaki = new ArrayList();
		
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");

		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");
		polskieZnaki.add("�");

		String[] zamiana = null;
		zamiana[0]="a";
		zamiana[1]="e";
		zamiana[2]="c";
		zamiana[3]="l";
		zamiana[4]="o";
		zamiana[5]="n";
		zamiana[6]="s";
		zamiana[7]="x";
		zamiana[8]="z";

		zamiana[9]= "A";
		zamiana[10]="E";
		zamiana[11]="C";
		zamiana[12]="L";
		zamiana[13]="O";
		zamiana[14]="N";
		zamiana[15]="S";
		zamiana[16]="X";
		zamiana[17]="Z";

		for(int i = 0; i < 18; i++)
		{
			while (defCharset.equals(polskieZnaki.get(i)))
			{
				int pos= defCharset.indexOf(polskieZnaki[i]);
				defCharset = "" + (defCharset.substring(0, pos) + zamiana[i] +
						defCharset.substring((pos + polskieZnaki[i].length()), defCharset.length()));
			}
		}
		*/
		
		int posPoczatek = signedFile.lastIndexOf("<wnio:Zalaczniki>");
		int posKoncowa =  signedFile.indexOf("</wnio:Zalaczniki>"); 
		//wydobycie  tre�ci za�acznika 
		if (posPoczatek < 0  || posKoncowa < 0){
			posPoczatek = 0;
			posKoncowa = 2;
		 zalacznik = signedFile.substring(posPoczatek, posKoncowa);
		
		}else{
			 zalacznik = signedFile.substring(posPoczatek, posKoncowa);
			 
				

		}
		
		String text = "";

		// zamiana polskich znak�w 
		for(int i=0; i<defCharset.length();i++){
			if (i == posPoczatek){
				i = posKoncowa -1; 
			} else 
			{
			String c = String.valueOf(defCharset.charAt(i));
			
			if(c.equals("�")){
				text+="a";
			}else
			if(c.equals("�")){
				text+="A";
			}else
			if(c.equals("�")){
				text+="c";
			}else
			if(c.equals("�")){
				text+="C";
			}else
			if(c.equals("�")){
				text+="e";
			}else
			if(c.equals("�")){
				text+="E";
			}else
			if(c.equals("�")){
				text+="l";
			}else
			if(c.equals("�")){
				text+="L";
			}else
			if(c.equals("�")){
				text+="n";
			}else
			if(c.equals("�")){
				text+="N";
			}else
			if(c.equals("�")){
				text+="o";
			}else
			if(c.equals("�")){
				text+="O";
			}else
			if(c.equals("�")){
				text+="z";
			}else
			if(c.equals("�")){
				text+="Z";
			}else
			if(c.equals("�")){
				text+="s";
			}else
			if(c.equals("�")){
				text+="S";
			}else
			if(c.equals("�")){
				text+="z";
			}else
			if(c.equals("�")){
				text+="Z";
			}else{
				text+=c;
			}
		}
		}
		
		signedFile=text;
		//int posPoczatek2 = signedFile.indexOf("<str:DaneZalacznika>");
		//int posKoncowa2 = signedFile.indexOf("</str:DaneZalacznika>");
		/*PrintWriter zapis = new PrintWriter("H:/zalacznik.xml");
	      zapis.println(zalacznik);
	      zapis.close();
	      
		PrintWriter zapiss = new PrintWriter("H:/signedfileBezZAL.xml");
	      zapiss.println(signedFile);
	      zapiss.close();*/
		//signedFile = IOUtils.toString(new FileInputStream(Docusafe.getHome().getAbsolutePath()
		//		+ "/odpowiedz.xml"), "UTF-8");
		// do�aczenie tre�ci za�acznika do podpisu i zwr�cenie jego warto�ci  
		signedFile = signedFile.substring(0, posPoczatek) + zalacznik + signedFile.substring(posPoczatek);
		/*PrintWriter zapiss2 = new PrintWriter("H:/signedfileGotowa.xml");
	      zapiss2.println(signedFile);
	      zapiss2.close();*/
		}
		
		/*
		 text="";
		for (int i=0; i<signedFile.length();i++){
			char c= signedFile.charAt(i);
			switch(c){
			//male
			case 'a' : 
				{
				text+="a";
				break;
			}
			case '�' : {
				text+="c";
				break;
			}
			case '�' : {
				text+="e";
				break;
			}
			case '�' : {
				text+="l";
				break;
			}
			case '�' : {
				text+="n";
				break;
			}
			case '�' : {
				text+="o";
				break;
			}
			case '�' : {
				text+="s";
				break;
			}
			case '�' : {
				text+="z";
				break;
			}
			case '�' : {
				text+="z";
				break;
			}
			//duze
			case 'a' : 
			{
				text+="A";
				break;
			}
			case '�' : {
				text+="C";
				break;
			}
			case '�' : {
				text+="E";
				break;
			}
			case '�' : {
				text+="L";
				break;
			}
			case '�' : {
				text+="N";
				break;
			}
			case '�' : {
				text+="O";
				break;
			}
			case '�' : {
				text+="S";
				break;
			}
			case '�' : {
				text+="Z";
				break;
			}
			case '�' : {
				text+="Z";
				break;
			}
			default: text+=c;
			}
		}
	
		
		signedFile= text;
		signedFile.replace("", "a");
		signedFile.replace("", "A");
		signedFile.replace("�", "z");
		signedFile.replace("�", "Z");
		signedFile.replace("�", "s");
		signedFile.replace("�", "S");
		signedFile.replace("�", "z");
		signedFile.replace("�", "Z");
		
		text = signedFile;
		
	      */
	      
		

	

	private void prepareSigningFileForDocument(Long documentId) throws EdmException {
        DocumentFactory df =  DocumentFactory.getInstance();
        org.dom4j.Document xml = df.createDocument();
        xml.setProcessingInstructions(
                Arrays.asList(
                    df.createProcessingInstruction("xml-stylesheet", getStyleSheetDocumentPi())
                ));

        xml.add(df.createElement("document"));

        Document doc = Document.find(documentId);
        if(AvailabilityManager.isAvailable("PgXmlGenerator"))
        signedFile = generatePgXml(doc);
        else{
        FieldsManager fm = doc.getFieldsManager();
        fm.initialize();
        String signatureFields = doc.getDocumentKind().getDockindInfo()
                                    .getProperties().get("electronic-signature-fields");
       
        LOG.info("signatureFields = {}", signatureFields);
        
        if(signatureFields != null){
            for(String fieldCn : signatureFields.split(",")){
                addFieldError(xml.getRootElement(), fm, fieldCn.trim());
            }
        } else {
            for(String fieldCn : fm.getFieldsCns()){
                addFieldError(xml.getRootElement(), fm, fieldCn.trim());
            }
        }
      
		
        signedFile = xml.asXML();
        }
    }

	private String generatePgXml(Document doc )
	{
		try
		{
			XmlGenerator generator = new PgXmlGenerator((OfficeDocument) doc).generateXml();
			String xmlAsString =   new String(generator.getXml());
			PolishStringUtils psu = new PolishStringUtils();
			return psu.zamienPolskieZnaki(xmlAsString);
		} catch (TransformerException e)
		{
			LOG.error("",e);
		} catch (EdmException e)
		{
			LOG.error("",e);
		} catch (IOException e)
		{
			LOG.error("",e);
		} catch (ParserConfigurationException e)
		{
			LOG.error("",e);
		}
		addActionError("Nie udalo sie wygenerowa� dokumentu <<!>> skontaktuj sie z administratorem systemu<<!>> "+doc.getId());
		
		return null;
		
		
	}

	private void addFieldError(Element xml, FieldsManager fm, String fieldCn) throws EdmException {
        Element el = DocumentFactory.getInstance().createElement("field");
        el.addAttribute("name", fm.getField(fieldCn).getName());
        el.setText((fm.getStringValue(fieldCn)!=null ? fm.getStringValue(fieldCn) :""));
        xml.add(el);
    }

    private void prepareSigningFileForAttachment(ActionEvent event, Logger log) throws Exception {
        AttachmentRevision ar = AttachmentRevision.find(attachmentRevision);
        StringBuilder sb = new StringBuilder();

        attachmentDescription = "Za��cznik '" + ar.getFileName() + "'";

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
          .append("<file>")
          .append(Base64.encodeBase64String(IOUtils.toByteArray(ar.getBinaryStream())))
          .append("</file>");

        signedFile = sb.toString();
    }

	private class SignXml extends TransactionalActionListener
	{

		@Override
		public void transaction(ActionEvent event, Logger log) throws Exception
		{
			log.info("SignXml \nsignedFile={} \nsignatureFile={} attachmentId={}", signedFile, signatureFile, attachmentRevision);
			if (attachmentRevision != null)
			{
				ElectronicSignatureStore.getInstance().storeAttachmentSignature(attachmentRevision, signedFile, signatureFile);
			} else if (actionName != null)
			{
				LOG.info("actionName = {}, processName = {}, processId = {}, processActivity = {}", actionName, processName, processId, processActivity);
				returnUrl = Docusafe.getBaseUrl() + "/office/tasklist/user-task-list.action";
				OfficeDocument doc = OfficeDocument.find(Jbpm4ProcessLocator.documentIdForProcess(processId));
				ProcessDefinition pd = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(processName);
				ProcessInstance pi = pd.getLocator().lookupForId(processId);
				pd.getLogic().process(pi, ProcessActionContext.action(actionName).document(doc).activityId(processActivity));
				ProcessActionEntry pae = new ProcessActionEntry(actionName, doc.getId(), pi, DSApi.context().getPrincipalName());

				DSApi.context().session().save(pae);
				ElectronicSignatureStore.getInstance().storeActionSignature(pae, signedFile, signatureFile);
				TaskSnapshot.updateByDocument(doc);
			} else if (documentId != null)
			{
				ElectronicSignatureStore.getInstance().storeDocumentSignature(documentId, signedFile, signatureFile);

				OfficeDocument doc = OfficeDocument.find(documentId);
				if (AvailabilityManager.isAvailable("dodajDoDziennikaPoPodpisie"))
				{
					bindToJurnal(doc);
				}
				if (AvailabilityManager.isAvailable("wyslijOdrazuDoEpuapPoPodpisaniu"))
				{
					bindToJurnal(doc);
					sentToEpuap(doc, event);
					event.setResult("result");
				}
				addActionMessage(sm.getString("podpisWykonanoPomyslnie"));
				setPodpisany(doc);
				event.setResult("result");
			} else
			{
				addActionMessage(sm.getString("podpisWykonanoPomyslnie"));
				event.setResult("result");
			}
		}

		private void sentToEpuap(OfficeDocument doc, ActionEvent event) throws EdmException
		{
			if (doc instanceof OutOfficeDocument)
			{
				Map<String, Object> values = doc.getFieldsManager().getFieldValues();
				if (values.get("EPUAP") == USLUGA_DORECZYCIEL)
				{
					ElectronicSignatureStore signs = ElectronicSignatureStore.getInstance();
					List<ElectronicSignatureBean> signatures = signs.findSignaturesForDocument(doc.getId());
					EpuapExportManager.sendToEpuap(getDocumentId(), false, EpuapExportDocument.USLUGA_DORECZYCIEL);
					setPodpisany(doc);
					DSApi.context().session().save(doc);
					addActionMessage(sm.getString("WyslanoDoEpuap"));

				} else if (values.get("EPUAP") == USLUGA_SKRYTKA)
				{
					EpuapExportManager.sendToEpuap(getDocumentId(), false, EpuapExportDocument.USLUGA_SKRYTKA);
					setPodpisany(doc);
					DSApi.context().session().save(doc);
					addActionMessage(sm.getString("WyslanoDoEpuap"));

				}
			}
		}

		private void setPodpisany(OfficeDocument doc) throws EdmHibernateException, EdmSQLException, EdmException
		{
			if (AvailabilityManager.isAvailable("zablokujDokumentPoPodpisie"))
			{
				List<Field> fieldlist = doc.getDocumentKind().getDockindInfo().getDockindFields();
				for (Field f : fieldlist)
				{
					if ((f instanceof BooleanColumnField) && f.getCn().equals("ZABLOKUJ"))
					{
						Map<String, Object> values = new HashMap<String, Object>();
						values.put("ZABLOKUJ", true);
						doc.getDocumentKind().setOnly(doc.getId(), values);
					}

				}
				doc.setBlocked(true);
			}
			
			
		}

		@Override
        public Logger getLogger() {
            return LOG;
        }
    }
		
	public class ValidateSign implements ActionListener
    {

		public void actionPerformed(ActionEvent event)
		{
			try
			{
				if (!DSApi.context().inTransaction())
					DSApi.context().begin();
				ElectronicSignature es = ElectronicSignatureStore.getInstance().get(signatureId);
				boolean validationStatus = false;
				if (es.getBaseSignatureId() != null && es.getBaseSignatureId().longValue() != 0)
					validationStatus = SignatureValidator.validate(ElectronicSignatureStore.getInstance().get(es.getBaseSignatureId()).getSignatureFile());
				else
					validationStatus = SignatureValidator.validate(es.getSignatureFile());
				Date verificationDate = new Date();
				es.setVerifyDate(verificationDate);
				es.setVerifyResult(validationStatus);
				DSApi.context().session().save(es);

				if (DSApi.isPostgresServer())
				{
					//przy pobieraniu /zapisywaniu/ updatowaniu Large objects z postgresa  np typ text  konieczne jest wylaczenie auto comita na tym obiekcie
					DSApi.context().session().connection().setAutoCommit(false);
					DSApi.context().commit();
					DSApi.context().session().connection().setAutoCommit(true);
				} else
				{
					DSApi.context().commit();
				}



				String msg = DateUtils.formatJsDateTime(verificationDate);
				if (validationStatus)
					addActionMessage(sm.getString("Podpis zweryfikowany poprawnie (" + msg + ")."));
				else
					addActionError(sm.getString("Podpis zweryfikowany jako niepoprawny(" + msg + ")."));
			} catch (Exception e)
			{
				addActionError("Podpis niepoprawny");
				addActionError(e.getMessage());
				LOG.debug(e.getMessage());
				try
				{
					DSApi.context().rollback();
				} catch (EdmException f)
				{
				};
			}
		}
	}
	
    
    public Long getAttachmentRevision() {
        return attachmentRevision;
    }

	public void bindToJurnal(OfficeDocument doc) throws EdmException
	{

		if (doc.getOfficeNumber() == null || doc.getOfficeNumber().toString().isEmpty())
		{
			Calendar currentDay = Calendar.getInstance();
			currentDay.setTime(GlobalPreferences.getCurrentDay());
			Journal journal = Journal.getMainOutgoing();
			Long journalId;
			journalId = journal.getId();
			Integer sequenceId = null;
			sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
			((OutOfficeDocument) doc).bindToJournal(journalId, sequenceId);
			DSApi.context().session().save(doc);

			addActionMessage(sm.getString("Nadano numer KO"));
		}
	}

	public void setAttachmentRevision(Long attachmentRevision) {
        this.attachmentRevision = attachmentRevision;
    }

    public String getSignedFile() {
        return signedFile;
    }

    public void setSignedFile(String signedFile) {
        this.signedFile = signedFile;
    }

    public String getSignatureFile() {
        return signatureFile;
    }

    public void setSignatureFile(String signatureFile) {
        this.signatureFile = signatureFile;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getAttachmentDescription() {
        return attachmentDescription;
    }

    public void setAttachmentDescription(String attachmentDescription) {
        this.attachmentDescription = attachmentDescription;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessActivity() {
        return processActivity;
    }

    public void setProcessActivity(String processActivity) {
        this.processActivity = processActivity;
    }

    public String getReturnUrl() {
//        return HttpUtils.urlEncode(returnUrl);
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public Long getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(Long signatureId) {
        this.signatureId = signatureId;
    }

    public String getCertificateData() {
        return certificateData;
    }

    public String getSignatureData() {
        return signatureData;
    }

	public Boolean getEpuap()
	{
		return epuap;
	}

	public void setEpuap(Boolean epuap)
	{
		this.epuap= epuap;
	}
}