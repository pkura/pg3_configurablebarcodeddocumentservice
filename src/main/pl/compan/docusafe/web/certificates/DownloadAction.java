package pl.compan.docusafe.web.certificates;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openssl.PEMWriter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.certificates.UserCertificate;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

/* User: Administrator, Date: 2005-06-09 12:10:09 */

/**
 * Pozwala na pobranie certyfikatu w formacie DER lub PEM (OpenSSL).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DownloadAction.java,v 1.6 2008/10/06 09:58:03 pecet4 Exp $
 */
public class DownloadAction extends EventActionSupport
{
    private Long id;
    private String username;
    private String signature;
    private boolean pem;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if ((signature == null || signature.length() == 0) && id == null)
            {
                event.getLog().error("Nie podano parametru id ani signature");
                return;
            }


            byte[] sig = null;
            if (signature != null)
            {
                try
                {
                    sig = Base64.decodeBase64(signature.getBytes());
                }
                catch (Exception e)
                {
                    event.getLog().error(e.getMessage(), e);
                    return;
                }
            }

            X509Certificate certificate = null;

            try
            {
                if (id != null)
                {
                    certificate = UserCertificate.findById(id).getX509Certificate();
                }
                else
                {
                    // administrator mo�e tak pobra� certyfikat dowolnego u�ytkownika
                    X509Certificate[] certs;
                    if (!StringUtils.isEmpty(username) && DSApi.context().isAdmin())
                    {
                        certs = DSUser.findByUsername(username).getCertificates();
                    }
                    else
                    {
                        certs = DSApi.context().getDSUser().getCertificates();
                    }

                    // jako� nie znajduje tego certyfikatu

                    for (int i=0; i < certs.length; i++)
                    {

                        if (Arrays.equals(certs[i].getSignature(), sig))
                        {
                            certificate = certs[i];
                            break;
                        }
                    }
                }
            }
            catch (EdmException e)
            {
                event.getLog().error(e.getMessage(), e);
            }

            event.getLog().info("certificate="+certificate);

            if (certificate != null)
            {
                byte[] output;
                if (pem)
                {
                    try
                    {
                        StringWriter writer = new StringWriter(2048);
                        PEMWriter pemWriter = new PEMWriter(writer);
                        pemWriter.writeObject(certificate);
                        pemWriter.close();
                        output = writer.toString().getBytes();
                    }
                    catch (IOException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        output = certificate.getEncoded();
                    }
                    catch (CertificateEncodingException e)
                    {
                        event.getLog().error(e.getMessage(), e);
                        return;
                    }
                }

                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/x-x509-cert");
                response.setHeader("Content-Disposition", "attachment; filename=\"certificate."+(pem ? "pem" : "cer")+"\"");
                response.setContentLength(output.length);
                try
                {
                    OutputStream os = response.getOutputStream();
                    os.write(output);
                    os.close();
                }
                catch (Throwable t)
                {
                    event.getLog().error(t.getMessage(), t);
                }
            }
        }
    }

    public void setSignature(String signature)
    {
        this.signature = signature;
    }

    public void setPem(boolean pem)
    {
        this.pem = pem;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
