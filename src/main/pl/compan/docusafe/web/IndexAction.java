package pl.compan.docusafe.web;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/* User: Administrator, Date: 2005-04-27 14:32:51 */

/**
 * Obs�uguje stron� g��wn� aplikacji (wywo�ywane po zalogowaniu si�
 * u�ytkownika).
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: IndexAction.java,v 1.7 2010/04/26 12:22:08 pecet5 Exp $
 */
public class IndexAction extends EventActionSupport
{
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            String name = GlobalPreferences.getUserStartPage();
            
            if (GlobalPreferences.TASKS.equals(name) && !AvailabilityManager.isAvailable("menu.left.kancelaria.listazadan")
            		&& AvailabilityManager.isAvailable("bookmarks.on"))
            {
            	name = GlobalPreferences.BOOKMARKS;
            }
            
            try
            {
                if ((GlobalPreferences.TASKS.equals(name) && Configuration.officeAvailable()) ||
                    (GlobalPreferences.FIND_OFFICE_DOCUMENTS.equals(name) && Configuration.simpleOfficeAvailable()) ||
                    (GlobalPreferences.TASKS_BOK.equals(name) && DSApi.context().hasPermission(DSPermission.BOK_LISTA_PISM)) ||
                    (GlobalPreferences.EXPLORE_DOCUMENTS.equals(name)) ||
                    GlobalPreferences.SEARCH_DOCUMENTS_REDIRECT.equals(name) ||
                    GlobalPreferences.NEW_IN_OFFICE_DOCUMENT.equals(name) || GlobalPreferences.BOOKMARKS.equals(name)
                    || GlobalPreferences.NEWS.equals(name) || GlobalPreferences.PORTLETS.equals(name) ||
                    GlobalPreferences.BLANK_PAGE.equals(name) || GlobalPreferences.HAND_WRITTEN_SIGNATURE.equals(name))
                {
                	//jesloi nie ma dostepu do archiwum to nie mozna go tam przekierowac
                	if(!DSApi.context().hasPermission(DSPermission.ARCHIWUM_DOSTEP) && GlobalPreferences.SEARCH_DOCUMENTS_REDIRECT.equals(name))
                		event.setResult(GlobalPreferences.TASKS);
                	else
                		event.setResult(name);
                }
                else
                {
                    event.setResult(getDefaultResult());
                }
            }
            catch (Exception e)
            {
                event.setResult(getDefaultResult());
            }
        }
    }

    private String getDefaultResult()
    {
    	if(Docusafe.getAdditionProperty("startPage") != null && Docusafe.getAdditionProperty("startPage").length() > 1)
    	{
    		return Docusafe.getAdditionProperty("startPage");
    	}
    	else if (AvailabilityManager.isAvailable("bookmarks.on")) 
    	{
    		return (GlobalPreferences.BOOKMARKS);
    	}
    	else if (Configuration.officeAvailable() || Configuration.faxAvailable())
        {
            return (GlobalPreferences.TASKS);
        }
        else if (Configuration.simpleOfficeAvailable())
        {
            return (GlobalPreferences.FIND_OFFICE_DOCUMENTS);
        }
        else
        {
            return (GlobalPreferences.EXPLORE_DOCUMENTS);
        }
    }
}
