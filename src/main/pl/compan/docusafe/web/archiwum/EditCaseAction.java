package pl.compan.docusafe.web.archiwum;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import nu.hoohoo.mine.tabs.Tab;
import nu.hoohoo.mine.tabs.TabsTag;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForward;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;

import com.lowagie.text.Cell;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.record.Records;
import pl.compan.docusafe.core.record.grants.GrantRequest;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.archiwum.ArchivePackageManager;
import pl.compan.docusafe.parametrization.archiwum.UdostepnionyZasob;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DynaBeans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.common.event.ActionEvent;
import pl.compan.docusafe.web.common.event.ActionListener;
import pl.compan.docusafe.web.common.event.EventProcessingAction;
import pl.compan.docusafe.web.office.common.AssignmentHistoryTabAction;
import std.fun;
import std.lambda;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditCaseAction.java,v 1.85 2010/07/27 11:26:27 kamilj Exp $
 */
public class EditCaseAction extends EventProcessingAction
{
    private static final String FORWARD = "archives/edit-case";
    private Logger log = LoggerFactory.getLogger(EditCaseAction.class);
    /**
     * Spos�b u�ycia akcji - w wyskakuj�cym okienku, tylko podgl�d.
     */
    public static final String APPLICATION_VIEW_POPUP = "APPLICATION_VIEW_POPUP";

    private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;

    private String suggestedOfficeId;
    private Integer customSequenceId;
    private Integer suggestedSequenceId;

    private Boolean customOfficeId;
    private Boolean closing;

	protected void setup()
    {
        FillForm fillForm = new FillForm(false);
        SetForward setForward = new SetForward();

        registerListener(DEFAULT_ACTION).
            append(setForward).
            append(fillForm);

        registerListener("doUpdate").
            append(setForward).
            append(new Update()).
            append(fillForm);

        registerListener("doAddRemark").
            append(setForward).
            append(new AddRemark()).
            append(fillForm);
        
        registerListener("doDelete").
            append(setForward).
            append(new Delete()).
            append(fillForm);
        
        registerListener("doMetrics").
	        append(setForward).
	        append(new Metrics()).
	        append(fillForm);

//        registerListener("doClose").
//            append(setForward).
//            append(new Close()).
//            append(fillForm);

        registerListener("doSetPortfolio").
            append(setForward).
            append(new SetPortfolio()).
            append(new FillForm(true));
        
        registerListener("doFinishSetPortfolio").
            append(setForward).
            append(new FinishSetPortfolio()).
            append(new FillForm(true));
        
        registerListener("doChangeNumber").
            append(setForward).
            append(new ManageChangeCaseNumber()).
            append(fillForm);

        registerListener("doArchivePackage").
                append(setForward).
                append(new PrepareArchivePackage()).
                append(fillForm);
    }

    private class SetForward implements ActionListener
    {
        
        public void actionPerformed(ActionEvent event)
        {
            String application = (String) event.getDynaForm().get("application");

            if (APPLICATION_VIEW_POPUP.equals(application))
            {
                event.setForward(event.getMapping().findForward("view_popup"));
            }
            else
            {
                event.setForward(event.getMapping().findForward("main"));
            }
        }
    }

/*
    private class Close implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long id = (Long) event.getDynaForm().get("id");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                OfficeHelper office = ctx.getOfficeHelper();

                Case c = office.getCase(id);

                c.setClosed(true);

                ctx.commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }
*/

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long id = (Long) event.getDynaForm().get("id");
            Long precedentId = (Long) event.getDynaForm().get("precedentId");
            Integer priority = (Integer) event.getDynaForm().get("priority");
            Integer statusId = (Integer) event.getDynaForm().get("status");
            String assignedUser = (String) event.getDynaForm().get("assignedUser");
            String description = (String) event.getDynaForm().get("description");
            String title = (String) event.getDynaForm().get("caseTitle");
            
            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                OfficeCase c = OfficeCase.find(id);

                boolean closed = c.isClosed();

                DSUser.findByUsername(assignedUser);
                c.setClerk(assignedUser);
                
                if(title.length()==0) throw new EdmException("Nie podano tytu�u sprawy");
                		
                if(title.length()>199) throw new EdmException("Za d�ugi tytu� sprawy");
                c.setTitle(title);

                c.setPriority(CasePriority.find(priority));
                
                if(description.length()>199) throw new EdmException("Za d�ugi opis sprawy");
                c.setDescription(description);

                if (DSApi.context().hasPermission(DSPermission.SPRAWA_ZMIANA_TERMINU))
                {
                    if (StringUtils.isEmpty((String) event.getDynaForm().get("finishDate")))
                        throw new EdmException("Nie podano daty zako�czenia sprawy");

                    Date finishDate = DateUtils.nullSafeParseJsDate((String) event.getDynaForm().get("finishDate"));

//                    if (c.getFinishDate() != null && DateUtils.difference(c.getFinishDate(), finishDate) > 0)
//                    {
//
//                    }
                    c.setFinishDate(finishDate);
                }
                c.setStatus(CaseStatus.find(statusId));

                if (precedentId != null)
                {
                    OfficeCase precedent = OfficeCase.find(precedentId);
                    if (event.getLog().isDebugEnabled())
                        event.getLog().debug("precedent="+precedent);
                    c.setPrecedent(precedent);
                }
                else
                {
                    c.setPrecedent(null);
                }

                // do us�ugi BIP przekazywane s� tylko pisma wchodz�ce
                List documents = c.getDocuments(InOfficeDocument.class);

                // je�eli sprawa zosta�a zamkni�ta lub ponownie otwarta,
                // wysy�am do BIP informacj� o pismach w tej sprawie
                if (((closed && !c.isClosed()) || (!closed && c.isClosed())) &&
                    documents.size() > 0)
                {
                	/*
                    try
                    {
                        Bip bip = (Bip) ServiceManager.getService(Bip.NAME);
                        for (Iterator iter=documents.iterator(); iter.hasNext(); )
                        {
                            InOfficeDocument doc = (InOfficeDocument) iter.next();
                            bip.submit(doc);
                        }
                    }
                    catch (ServiceException e)
                    {
                    }
                	*/
                	DataMartEventBuilder.get().event(DataMartEventProcessors.EventType.BIP_CASE_INFO).documentId(c.getId());
                }
                if(c.getStatus().isClosingStatus())
                {
                	TaskList tasks = (TaskList) ServiceManager.getService(TaskList.NAME);
                	List<Task> userTasks = tasks.getTasks(DSUser.findByUsername(DSApi.context().getPrincipalName()));
                	List<OfficeDocument> docList = c.getDocuments();
                	for (OfficeDocument officeDoc : docList) 
                	{
						for (Task task : userTasks)
						{
							
							if(task.getDocumentId().equals(officeDoc.getId()))
							{
								String activityId = WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(),task.getActivityKey());
								WorkflowFactory.getInstance().manualFinish(activityId, false);
								event.getMessages().add("Zako�czono prace z dokumentem "+ officeDoc.getOfficeNumber());								                                
							}
						}
					}
                }
                
                ctx.commit();
                event.getMessages().add("Zapisano zmiany");
            }
            catch (EdmException e)
            {
                if (ctx != null) DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            catch (Exception e)
            {
                if (ctx != null) DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
                LogFactory.getLog(EdmException.class).error(e);
            }
            finally
            {
                DSApi._close();
            }
        }
    }
    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long id = (Long) event.getDynaForm().get("id");
            String reason = (String) event.getDynaForm().get("reason");
            if(reason == null || reason.equals(""))
            {
                event.getErrors().add("Nale�y poda� pow�d usuni�cia sprawy");
                event.getRequest().setAttribute("delete", Boolean.valueOf(true));
                return;
            }
            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();
                OfficeCase c = OfficeCase.find(id);
                if(!c.getDocuments().isEmpty())
                {
                    event.getErrors().add("Nie mo�na usun�� sprawy poniewa� znajduj� sie w niej dokumenty");
                    event.getRequest().setAttribute("delete", Boolean.valueOf(true));
                    return;
                }
               
                DSLog l = new DSLog();
                l.setCode(DSLog.DELETE_CASE);
                l.setCtime(new Date());
                l.setParam(c.getOfficeId());
                l.setLparam(c.getId());
                l.setReason(StringUtils.left(reason,200));
                l.setUsername(DSApi.context().getPrincipalName());
                l.create();
                c.delete();
                ctx.commit(); 
                event.setForward(new ActionForward("/archives/tasklist/current-user-task-list.action?a=a&tab=cases"));
            }
            catch (EdmException e)
            {
                try
                {
                    ctx.rollback();
                }
                catch (EdmException e1)
                {
                	LogFactory.getLog("eprint").debug("", e1);
                }
                LogFactory.getLog("eprint").debug("", e);
            }
            finally
            {
                try
                {
                    DSApi.close();
                }
                catch (EdmException e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
            }   
        } 
    }
    
    private class Metrics implements ActionListener
    {
        
        public void actionPerformed(ActionEvent event)
        {
        	DSContext ctx = null;
        	try {
        		Long id = (Long) event.getDynaForm().get("id");
            	
        		ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();
                
            	OfficeCase c = OfficeCase.find(id);
            	
            	/**
            	 * Troche nieeleganckie rozwiazanie likwidujace blad w ktorym zamiast pdfu pojawialy sie krzaki
            	 */
            	
            	HttpServletResponse response = ServletActionContext.getResponse();
            	
                try {
        			Field field = response.getClass().getDeclaredField("included");
        			field.setAccessible(true);
        			field.set(response, false);
        		} catch (Exception e){}
                
				ServletUtils.streamFile(response, pdfList(c), "application/pdf",
						"Content-Disposition: attachment; filename=\"metrykaSprawy.pdf\"");
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				log.error("", e);
				try
                {
                    ctx.rollback();
                }
                catch (EdmException e1)
                {
                	LogFactory.getLog("eprint").debug("", e1);
                }
                LogFactory.getLog("eprint").debug("", e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("",e);
			}
            finally
            {
                try
                {
                    DSApi.close();
                }
                catch (EdmException e)
                {
                	LogFactory.getLog("eprint").debug("", e);
                }
            }   
        }
        
        class HistoriaWpis
        {
        	Date data;
        	String osoba;
        	String czynnosc;
        	String identyfikator;
        	
        	HistoriaWpis(Date data, String osoba, String czynnosc, String identyfikator){
        		this.data=data;
        		this.osoba=osoba;
        		this.czynnosc=czynnosc;
        		this.identyfikator=identyfikator;
        	}
        }
        
        private File pdfList(OfficeCase _case) throws EdmException
		{
        	List<OfficeDocument> docs= _case.getDocuments();
        	
        	List<HistoriaWpis> historia = getData(docs);
        	
        	
        	File pdfFile= null;
			try {
				// font
				File fontDir= new File(Docusafe.getHome(), "fonts");
				File arial= new File(fontDir, "arial.ttf");
				BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

				Font fontSuperSmall= new Font(baseFont, (float) 6);
				Font fontSmall= new Font(baseFont, (float) 8);
				Font fontNormal= new Font(baseFont, (float) 12);
				Font fontNormal_Large= new Font(baseFont, (float) 14);
				Font fontLarge= new Font(baseFont, (float) 16);

				// file
				pdfFile= File.createTempFile("docusafe_", "_tmp");
				pdfFile.deleteOnExit();
				com.lowagie.text.Document pdfDoc= new com.lowagie.text.Document(PageSize.A4);
				PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

				pdfDoc.open();

				// content

				/* ********************************** */
				Table topTable= new Table(2);
				topTable.setCellsFitPage(true);
				topTable.setWidth(100);
				topTable.setPadding(3);
				topTable.setBorder(1);
				topTable.setWidths( new int[]{25,75});
				
				Cell oznaczenieSprawyLabel = new Cell(new Paragraph("Oznaczenie sprawy", fontNormal));
				Cell oznaczenieSprawy= new Cell(new Paragraph(_case.getOfficeId(), fontNormal));
				
				Cell tytulSprawyLabel = new Cell(new Paragraph("Tytu� sprawy", fontNormal));
				Cell tytulSprawy= new Cell(new Paragraph(_case.getTitle(), fontNormal));
				
				topTable.addCell(oznaczenieSprawyLabel);
				topTable.addCell(oznaczenieSprawy);
				topTable.addCell(tytulSprawyLabel);
				topTable.addCell(tytulSprawy);
				
				pdfDoc.add(topTable);
				
				// Tabela z wlasciwymi danymi
				Table contentTable= new Table(5);
				contentTable.setCellsFitPage(true);
				contentTable.setWidth(100);
				contentTable.setPadding(3);
				contentTable.setBorder(1);
				contentTable.setWidths( new int[]{10,15,20,35,20});
				
				// Naglowki
				contentTable.addCell( new Cell(new Paragraph("Lp.", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Data podj�tej czynno�ci", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Oznaczenie osoby podejmuj�cej dan� czynno��", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Okre�lenie podejmowanej czynno�ci", fontNormal)));
				contentTable.addCell( new Cell(new Paragraph("Wskazanie identyfikatora w aktach sprawy, do kt�rego odnosi si� dana czynno��", fontNormal)));
				
				Integer lp_index = 1;
				// Tresc tabeli
				for( HistoriaWpis wpis : historia)
				{
					Cell lp = new Cell(new Paragraph(lp_index.toString(), fontNormal));
					Cell data = new Cell(new Paragraph(wpis.data.toLocaleString(), fontNormal));
					Cell osoba = new Cell(new Paragraph(wpis.osoba, fontNormal));
					Cell czynnosc = new Cell(new Paragraph(wpis.czynnosc, fontNormal));
					Cell identyfikator = new Cell(new Paragraph(wpis.identyfikator, fontNormal));
					
					contentTable.addCell(lp);
					contentTable.addCell(data);
					contentTable.addCell(osoba);
					contentTable.addCell(czynnosc);
					contentTable.addCell(identyfikator);
					
					++lp_index;
				}
				
				pdfDoc.add(contentTable);
				
				
				pdfDoc.close();
			} catch (Exception e) {
				throw new EdmException(e);
			}

			return pdfFile;
        	
		}

		private List<HistoriaWpis> getData(List<OfficeDocument> docs) throws EdmException
		{
			List<HistoriaWpis> historia = new ArrayList<EditCaseAction.Metrics.HistoriaWpis>();
        	for(OfficeDocument doc : docs)
        	{
        		List<AssignmentHistoryEntry> historiaDekretacji = doc.getAssignmentHistory();
        		List<Audit> historiaPisma = doc.getWorkHistory();
        		
        		String docId = doc.getId().toString();
        		
        		for( AssignmentHistoryEntry ashistory : historiaDekretacji)
        		{
        			AssignmentHistoryTabAction.AssignmentHistoryBean ashistoryBean = new AssignmentHistoryTabAction.AssignmentHistoryBean(ashistory);
        			historia.add(new HistoriaWpis(
        				ashistory.getCtime(),	// data
        				DSUser.safeToFirstnameLastname(ashistory.getSourceUser()),	// osoba
        				ashistoryBean.getContent(),	// czynnosc
        				docId	// identyfikator
        				));
        		}
        		
        		for( Audit audit : historiaPisma)
        			historia.add(new HistoriaWpis(
        				audit.getCtime(),	// data
        				DSUser.safeToFirstnameLastname(audit.getUsername()),	// osoba
        				audit.getDescription(),	// czynnosc
        				docId	// identyfikator
        				));
        	}
        	return historia;
		}
    }

    private class AddRemark implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            Long id = (Long) event.getDynaForm().get("id");
            String remarkContent = (String) event.getDynaForm().get("remarkContent");

            if (StringUtils.isEmpty(remarkContent))
                return;

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                OfficeCase c = OfficeCase.find(id);

                Remark remark = new Remark(remarkContent, ctx.getPrincipalName());

                c.addRemark(remark);

                ctx.commit();

                event.getDynaForm().set("remarkContent", null);
            }
            catch (EdmException e)
            {
                if (ctx != null) DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class FillForm implements ActionListener
    {
        private boolean settingPortfolio;

        public FillForm(boolean settingPortfolio)
        {
            this.settingPortfolio = settingPortfolio;
        }

        public void actionPerformed(ActionEvent event)
        {
            event.getRequest().setAttribute("caseRegistryLink",
                event.getRequest().getContextPath()+
                "/archives/find-cases.action");
                //CaseRegistryAction.getLink(event.getRequest()));

//            event.getRequest().setAttribute("caseRegistryPopupLink",
//                    event.getRequest().getContextPath()+
//                    CaseRegistryAction.getLink(event.getRequest(),
//                        CaseRegistryAction.APPLICATION_POPUP_PICK));

            final String returnUrl = (String) event.getDynaForm().get("returnUrl");
            final String application = (String) event.getDynaForm().get("application");

            if (!StringUtils.isEmpty(returnUrl))
                event.getRequest().setAttribute("returnUrl",
                    event.getRequest().getContextPath()+
                    (String) event.getDynaForm().get("returnUrl"));

            boolean viewOnly =
                APPLICATION_VIEW_POPUP.equals(application);
            event.getRequest().setAttribute("viewOnly", Boolean.valueOf(viewOnly));


            final Long id = (Long) event.getDynaForm().get("id");


            // zak�adki (Og�lne, Dokumenty, Uwagi, Historia zmian)
            Tab[] tabs = new Tab[5];
            tabs[0] = new Tab("summary", event.getRequest().getContextPath()+getLink(id, application, "summary", returnUrl), "Podsumowanie");
            tabs[1] = new Tab("main", event.getRequest().getContextPath()+getLink(id, application, "main", returnUrl), "Og�lne");
            tabs[2] = new Tab("documents", event.getRequest().getContextPath()+getLink(id, application, "documents", returnUrl), "Pisma");
            tabs[3] = new Tab("remarks", event.getRequest().getContextPath()+getLink(id, application, "remarks", returnUrl), "Uwagi");
            tabs[4] = new Tab("history", event.getRequest().getContextPath()+getLink(id, application, "history", returnUrl), "Historia sprawy");

            event.getRequest().setAttribute(TabsTag.KEY, tabs);

            DSContext ctx = null;
            try
            {
                // #pragma used
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));

                event.getRequest().setAttribute(
                    DSPermission.SPRAWA_ZMIANA_TERMINU.getName(),
                    Boolean.valueOf(DSApi.context().hasPermission(DSPermission.SPRAWA_ZMIANA_TERMINU)));
                event.getRequest().setAttribute("canSetPortfolio", Boolean.valueOf(DSApi.context().hasPermission(DSPermission.SPRAWA_PRZESUWANIE_DO_INNEJ_TECZKI)));
                event.getRequest().setAttribute("canChangeCaseNumber", Boolean.valueOf(DSApi.context().hasPermission(DSPermission.SPRAWA_ZMIANA_NUMERU)));
                // priorytety
                final List priorityBeans = new ArrayList(4);
                final List priorities = CasePriority.list();
                //for (int i=0; i < Case.PRIORITIES.length; i++)
                for (Iterator iter=priorities.iterator(); iter.hasNext(); )
                {
                    CasePriority priority = (CasePriority) iter.next();
                    DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
                    bean.set("label", priority.getName());
                    bean.set("value", String.valueOf(priority.getId()));
                    priorityBeans.add(bean);
                }
                event.getRequest().setAttribute("priorityBeans", priorityBeans);

                final List statuses = CaseStatus.list();
                final List statusBeans = new ArrayList(statuses.size());
                for (int i=0; i < statuses.size(); i++)
                {
                    final DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
                    bean.set("label", ((CaseStatus) statuses.get(i)).getName());
                    bean.set("value", String.valueOf(((CaseStatus) statuses.get(i)).getId()));
                    statusBeans.add(bean);
                }
                event.getRequest().setAttribute("statusBeans", statusBeans);

                class Flag { boolean state; }
                final Flag currentUserOnList = new Flag();
                final Flag currentClerkOnList = new Flag();

                final OfficeCase c = OfficeCase.find(id);
                //NewBipDriver.getXmlFromCase(c);
                boolean canView = DSApi.context().hasPermission(DSPermission.SPRAWA_WSZYSTKIE_PODGLAD) || DSApi.context().isAdmin();
                if (!canView)
                {
                	DSUser user = DSApi.context().getDSUser();
                    if (DSApi.context().hasPermission(DSPermission.SPRAWA_KOMORKA_PODGLAD))
                    {
                        DSDivision[] divisions = user.getDivisions();
                        for (int i=0; i < divisions.length; i++)
                        {
                        	if (c.getDivisionGuid().equals(divisions[i].getGuid()))
                        	{
                        		canView = true;
                        		break;
                        	}
                        	if (divisions[i].isPosition())
                        	{
                        		DSDivision parent = divisions[i].getParent();
                                if ((parent != null) && (c.getDivisionGuid().equals(parent.getGuid())))
                        		{
                        			canView = true;
                            		break;
                        		}
                        	}
                        }
                    }

                    if (c.getClerk().equals(user.getName()))
                    		canView = true;

                }
                if (!canView && AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){
                	//w tym momencie jezeli by� przypisany do teczki to ma wglada do wszystkich spraw w teczce
                	//mo�na to pozniej bardziej rozbudowa�

                	canView = UserToBriefcaseAction.isOnListBriefcase( OfficeFolder.find(c.getParent().getId()), DSApi.context().getDSUser().getName());

                }
                if (!canView){
                    viewOnly = UdostepnionyZasob.isBorrowed(DSApi.context().getDSUser().getName(), UdostepnionyZasob.SPRAWA, c.getId());
                    canView = viewOnly;
                    event.getRequest().setAttribute("viewOnly", Boolean.valueOf(viewOnly));
                }

                event.getRequest().setAttribute("canView", Boolean.valueOf(canView));
                if (!canView)
                	event.getErrors().add("Brak uprawnie� do podgl�du tej sprawy");

                event.getRequest().setAttribute("casePriority",
                    c.getPriority() != null ? c.getPriority().getName() : null);
                event.getRequest().setAttribute("caseStatus",
                    c.getStatus() != null ? c.getStatus().getName() : null);

                final String currentUsername = DSApi.context().getPrincipalName();
                DSUser[] clerks = DSDivision.find(c.getDivisionGuid()).getUsers(true);
                Collection<DynaBean> userBeans = fun.map(clerks, new lambda<DSUser, DynaBean>()
                {
                    public DynaBean act(DSUser user)
                    {
                        DynaBean bean = DynaBeans.newHtmlOption();
                        bean.set("value", user.getName());
                        bean.set("label", user.asFirstnameLastname());
                        if (user.getName().equals(currentUsername))
                            currentUserOnList.state = true;
                        if (user.getName().equals(c.getClerk()))
                            currentClerkOnList.state = true;
                        return bean;
                    }
                });

                if (!currentUserOnList.state)
                {
                    DynaBean ubean = DynaBeans.newHtmlOption();
                    ubean.set("value", DSApi.context().getPrincipalName());
                    ubean.set("label", DSApi.context().getDSUser().asFirstnameLastname());
                    userBeans.add(ubean);
                }

                if (!currentClerkOnList.state && c.getClerk() != null)
                {
                    DynaBean ubean = DynaBeans.newHtmlOption();
                    DSUser clerk = DSUser.findByUsername(c.getClerk());
                    ubean.set("value", clerk.getName());
                    ubean.set("label", clerk.asFirstnameLastname());
                    userBeans.add(ubean);
                }

                event.getRequest().setAttribute("clerks", userBeans);

                event.getDynaForm().set("finishDate", DateUtils.formatJsDate(c.getFinishDate()));

                if (c.getParent() != null && !settingPortfolio)
                {
                    event.getDynaForm().set("portfolio", c.getParent().getOfficeId());
                    event.getRequest().setAttribute("portfolio", c.getParent().getOfficeId());
                    event.getDynaForm().set("portfolioId", c.getParent().getId());
                }

                event.getDynaForm().set("description",c.getDescription());

                if (c.getDescription() != null) {
                	String tempDescription = c.getDescription();
	                if (tempDescription.length() > 50)
	                	tempDescription = tempDescription.substring(0, 49) + "(..)";
	                event.getRequest().setAttribute("description", tempDescription);
                }
                event.getDynaForm().set("caseTitle", (c.getTitle()!=null? c.getTitle():""));
                if (c.getTitle() != null) {
                	String tempTitle = c.getTitle();
	                if (tempTitle.length() > 50)
	                	tempTitle = tempTitle.substring(0, 49) + "(..)";
	                event.getRequest().setAttribute("caseTitle", tempTitle);
                }

                event.getDynaForm().set("assignedUser", c.getClerk());

                DSApi.initializeProxy(c.getRwa());
                event.getRequest().setAttribute("rwa", c.getRwa());

                event.getRequest().setAttribute("case", c);
                event.getRequest().setAttribute("caseAuthor",
                    DSUser.safeToFirstnameLastnameName(c.getAuthor()));
                event.getRequest().setAttribute("caseAssignedUser",
                    DSUser.safeToFirstnameLastnameName(c.getClerk()));

                if (c.getPrecedent() != null)
                {
                    event.getDynaForm().set("precedentId", c.getPrecedent().getId());
                    event.getRequest().setAttribute("precedentLink",
                        event.getRequest().getContextPath()+
                        EditCaseAction.getLink(c.getPrecedent().getId()));
                }

                // lista dokument�w w sprawie
                //Map documents = c.getDocumentMap();
                List documents = c.getDocuments();
                List documentBeans = new ArrayList(documents.size());

                for (Iterator iter=documents.iterator(); iter.hasNext(); )
                {
                    OfficeDocument document = (OfficeDocument) iter.next();

                    DynaBean bean = DynaBeans.bean(DynaBeans.officeDocument);
                    bean.set("caseDocumentIdentifier", document.getCaseDocumentId());
                    if (document.getType() == DocumentType.INCOMING)
                    {
                        bean.set("incoming", Boolean.TRUE);
                        InOfficeDocument in = (InOfficeDocument) document;
                        if (in.getSender() != null)
                        {
                            Sender sender = in.getSender();
                            bean.set("sender", sender.getSummary());
                        }
                        if (in.getRecipients() != null && in.getRecipients().size() > 0)
                        {
                            bean.set("recipients", in.getRecipients());
//                            Recipient recipient = (Recipient) in.getRecipients().get(0);
//                            bean.set("recipient", recipient.getSummary());
                        }
                        else
                        {
                            bean.set("recipients", Collections.EMPTY_LIST);
                        }
                        bean.set("incomingDate", in.getIncomingDate());
                        bean.set("officeNumber", in.getOfficeNumber() != null ?
                            in.getOfficeNumber().toString() : "");
                        bean.set("description", in.getDescription());

                        bean.set("link", event.getRequest().getContextPath()+
                            "/office/incoming/summary.action?documentId="+document.getId());
                    }
                    else if ((document.getType() == DocumentType.OUTGOING) || (document.getType() == DocumentType.INTERNAL))
                    {
                        if (((OutOfficeDocument) document).isInternal())
                        {
                            bean.set("internal", Boolean.TRUE);
                            OutOfficeDocument out = (OutOfficeDocument) document;
                            if (out.getSender() != null)
                            {
                                Sender sender = out.getSender();
                                bean.set("sender",
                                    TextUtils.nullSafeString(sender.getFirstname()) +
                                    " "+
                                    TextUtils.nullSafeString(sender.getLastname()) +
                                    "/" +
                                    TextUtils.nullSafeString(sender.getOrganization()));
                            }
                            if (out.getRecipients() != null && out.getRecipients().size() > 0)
                            {
                                bean.set("recipients", out.getRecipients());
                            }
                            else
                            {
                                bean.set("recipients", Collections.EMPTY_LIST);
                            }
                            bean.set("incomingDate", out.getCtime());

                            bean.set("officeNumber", out.getOfficeNumber() != null ?
                                out.getOfficeNumber().toString() : "");
                            bean.set("description", out.getDescription());

                            bean.set("link", event.getRequest().getContextPath()+
                                "/office/internal/summary.action?documentId="+document.getId());
                        }
                        else
                        {
                            bean.set("outgoing", Boolean.TRUE);
                            OutOfficeDocument out = (OutOfficeDocument) document;
                            if (out.getSender() != null)
                            {
                                Sender sender = out.getSender();
                                bean.set("sender",
                                    TextUtils.nullSafeString(sender.getFirstname()) +
                                    " "+
                                    TextUtils.nullSafeString(sender.getLastname()) +
                                    "/" +
                                    TextUtils.nullSafeString(sender.getOrganization()));
                            }
                            if (out.getRecipients() != null && out.getRecipients().size() > 0)
                            {
                                bean.set("recipients", out.getRecipients());
                            }
                            else
                            {
                                bean.set("recipients", Collections.EMPTY_LIST);
                            }
                            bean.set("incomingDate", out.getCtime());
                            bean.set("officeNumber", out.getOfficeNumber() != null ?
                                out.getOfficeNumber().toString() : "");
                            bean.set("description", out.getDescription());

                            bean.set("link", event.getRequest().getContextPath()+
                                "/office/outgoing/summary.action?documentId="+document.getId());
                        }
                    }

                    documentBeans.add(bean);
                }

                event.getRequest().setAttribute("documents", documentBeans);




                // lista komentarzy

                List remarks = c.getRemarks();
                Collections.reverse(remarks);
                event.getRequest().setAttribute("remarks", remarks);

                List audit = c.getAudit();
                Collections.reverse(audit);
                List auditBeans = new ArrayList(audit.size());
                for (Iterator iter=audit.iterator(); iter.hasNext(); )
                {
                    Audit auditEvent = (Audit) iter.next();
                    DynaBean bean = DynaBeans.bean(DynaBeans.audit);
                    bean.set("ctime", auditEvent.getCtime());
                    bean.set("description", auditEvent.getDescription());
                    bean.set("user", DSUser.safeToFirstnameLastnameName(auditEvent.getUsername()));
                    auditBeans.add(bean);
                }

                event.getRequest().setAttribute("audit", auditBeans);

                // statusy
/*
                final CaseStatus[] statuses = office.getCaseStatuses();
                final List statusBeans = new ArrayList(statuses.length);
                for (int i=0, n=statuses.length; i < n; i++)
                {
                    final DynaBean bean = DynaBeans.bean(DynaBeans.htmlOption);
                    bean.set("label", statuses[i].getDescription());
                    bean.set("value", statuses[i].getId().toString());
                    statusBeans.add(bean);
                }
                event.getRequest().setAttribute("statusBeans", statusBeans);

                if (c.getStatus() != null)
                    event.getDynaForm().set("status", c.getStatus().getId());
*/
                if (c.getStatus() != null)
                    event.getDynaForm().set("status", c.getStatus().getId());


                if (c.getPriority() != null)
                    event.getDynaForm().set("priority", c.getPriority().getId());


                // rejestry

                if (Docusafe.moduleAvailable(Modules.MODULE_GRANTS) &&
                    c.isInRegistry(Records.GRANTS))
                {
                    try
                    {
                        GrantRequest grantRequest = GrantRequest.findByOfficeCase(c);
                        event.getRequest().setAttribute("grantRequestId",
                            grantRequest.getId());
                        event.getRequest().setAttribute("grantRequestSequenceId",
                            grantRequest.getSequenceId());
                    }
                    catch (EntityNotFoundException e)
                    {
                    }
                }

            }
            catch (EdmException e)
            {
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class SetPortfolio implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {         
            event.getRequest().setAttribute("actionType", "settingPortfolio");
            
            Long id = (Long) event.getDynaForm().get("id");
            Long portfolioId = (Long) event.getDynaForm().get("portfolioId");

            DSContext ctx = null;
            try
            {
                ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                ctx.begin();

                //OfficeCase c = OfficeCase.find(id);

                //c.setParent(OfficeFolder.find(portfolioId));
                OfficeCase c = new OfficeCase(OfficeFolder.find(portfolioId));
                
                suggestedSequenceId = customSequenceId = c.suggestSequenceId();

                String[] breakdown = c.suggestOfficeId();
                suggestedOfficeId = StringUtils.join(breakdown, "");
                customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                customOfficeIdPrefixSeparator = breakdown[1];
                customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                customOfficeIdSuffixSeparator = breakdown[3];
                customOfficeIdSuffix = officeIdSuffix = breakdown[4];
                
                event.getDynaForm().set("suggestedSequenceId", suggestedSequenceId);
                event.getRequest().setAttribute("suggestedSequenceId", suggestedSequenceId);
                event.getDynaForm().set("customSequenceId", customSequenceId);
                event.getDynaForm().set("suggestedOfficeId", suggestedOfficeId);
                event.getRequest().setAttribute("suggestedOfficeId", suggestedOfficeId);
                event.getDynaForm().set("customOfficeIdPrefix", customOfficeIdPrefix);
                event.getDynaForm().set("customOfficeIdPrefixSeparator", customOfficeIdPrefixSeparator);
                event.getDynaForm().set("customOfficeIdMiddle", customOfficeIdMiddle);
                event.getDynaForm().set("customOfficeIdSuffixSeparator", customOfficeIdSuffixSeparator);
                event.getDynaForm().set("customOfficeIdSuffix", customOfficeIdSuffix);
                event.getDynaForm().set("officeIdPrefix", officeIdPrefix);
                event.getDynaForm().set("officeIdMiddle", officeIdMiddle);
                event.getDynaForm().set("officeIdSuffix", officeIdSuffix);
                event.getDynaForm().set("portfolioId", portfolioId);
                
                ctx.commit();
            }
            catch (EdmException e)
            {
                if (ctx != null) DSApi.context().setRollbackOnly();
                event.getErrors().add(e.getMessage());
            }
            finally
            {
                DSApi._close();
            }
        }
    }

    private class ManageChangeCaseNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            suggestedOfficeId = (String) event.getDynaForm().get("suggestedOfficeId");
            if (TextUtils.trimmedStringOrNull(suggestedOfficeId) == null)
            {
                DSContext ctx = null;     
                try 
                {
                    ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
                    ctx.begin();
                    
                    /* po raz pierwszy wywolujemy akcje "zmiany" - musimy zainicjowac numer aktualnymi danymi */
                    event.getRequest().setAttribute("actionType", "beforeChangingNumber");
                    
                    Long id = (Long) event.getDynaForm().get("id");
                    
                    OfficeCase c = OfficeCase.find(id);                 
                    
                    suggestedSequenceId = customSequenceId = c.getSequenceId();
    
                    suggestedOfficeId = c.getOfficeId();
                    customOfficeIdPrefix = officeIdPrefix = c.getOfficeIdPrefix();
                    if (c.getOfficeIdPrefix() != null && c.getOfficeIdPrefix().length() > 0)
                    {
                        int index = c.getOfficeIdPrefix().length(); // = koniec prefixu +1 
                        customOfficeIdPrefixSeparator = c.getOfficeId().substring(index,index+1); 
                    }
                    else
                        customOfficeIdPrefixSeparator = null;
                    customOfficeIdMiddle = officeIdMiddle = c.getOfficeIdMiddle();
                    if (c.getOfficeIdSuffix() != null && c.getOfficeIdSuffix().length() > 0)
                    {
                        int index = c.getOfficeId().length() - c.getOfficeIdSuffix().length(); // = poczatek suffixu
                        customOfficeIdSuffixSeparator = c.getOfficeId().substring(index-1,index);
                    }
                    else
                        customOfficeIdSuffixSeparator = null;
                    customOfficeIdSuffix = officeIdSuffix = c.getOfficeIdSuffix();
                        
                    ctx.commit();
                }
                catch (EdmException e)
                {
                	log.error(e.getMessage(),e);
                    if (ctx != null) DSApi.context().setRollbackOnly();
                    
                    event.getErrors().add(e.getMessage());                
                }
                finally
                {
                    setParameters(event);
                    DSApi._close();
                }
            }
            else 
            {
                event.getRequest().setAttribute("actionType", "changingNumber");
                changeCaseNumber(event, false);
            }
        }
    }
    
    private class FinishSetPortfolio implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.getRequest().setAttribute("actionType", "settingPortfolio");
            Long portfolioId = (Long) event.getDynaForm().get("portfolioId");
            event.getRequest().setAttribute("portfolioId", portfolioId);
            changeCaseNumber(event, true);
        }
    }
    
    private void changeCaseNumber(ActionEvent event, boolean settingPortfolio)
    {
        Long id = (Long) event.getDynaForm().get("id");
        Long portfolioId = (Long) event.getDynaForm().get("portfolioId");
        customSequenceId = (Integer) event.getDynaForm().get("customSequenceId");
        suggestedSequenceId = (Integer) event.getDynaForm().get("suggestedSequenceId");
        customOfficeIdPrefix = (String) event.getDynaForm().get("customOfficeIdPrefix");
        customOfficeIdPrefixSeparator = (String) event.getDynaForm().get("customOfficeIdPrefixSeparator");
        customOfficeIdMiddle = (String) event.getDynaForm().get("customOfficeIdMiddle");
        customOfficeIdSuffixSeparator = (String) event.getDynaForm().get("customOfficeIdSuffixSeparator");
        customOfficeIdSuffix = (String) event.getDynaForm().get("customOfficeIdSuffix");
        officeIdPrefix = (String) event.getDynaForm().get("officeIdPrefix");
        officeIdSuffix = (String) event.getDynaForm().get("officeIdSuffix");
        officeIdMiddle = (String) event.getDynaForm().get("officeIdMiddle");
        suggestedOfficeId = (String) event.getDynaForm().get("suggestedOfficeId");
        customOfficeId = (Boolean) event.getDynaForm().get("customOfficeId");
        
        DSContext ctx = null;
        try
        {
            ctx = DSApi.open(AuthUtil.getSubject(event.getRequest()));
            ctx.begin();
            
            OfficeCase officeCase = OfficeCase.find(id);
                        
            if (settingPortfolio) {
            	OfficeFolder sp = OfficeFolder.find(portfolioId);
                officeCase.setParentOnly(sp);
                officeCase.setRwa(sp.getRwa());
            }
            
            if (customOfficeId != null && customOfficeId.booleanValue())
            {
                if (customSequenceId == null)
                    throw new EdmException("Nie podano numeru kolejnego sprawy lub " +
                        "wpisano niepoprawn� liczb�");

                if (customSequenceId.intValue() <= 0)
                    throw new EdmException("Numer kolejny sprawy musi by� wi�kszy od zera");

               if (!officeCase.sequenceIdAvailable(customSequenceId))
                    if (officeCase.getSequenceId().intValue() != customSequenceId.intValue() || settingPortfolio)
                        /* gdy tylko zmieniamy numer to mozemy pozostawic taki sam */
                        throw new EdmException("Wybrany numer kolejny ("+customSequenceId+") jest niedost�pny");
   

                // zmodyfikowano numer kolejny sprawy, ponowne wygenerowanie
                // ca�ego numeru
                if (suggestedSequenceId.intValue() != customSequenceId.intValue())
                {
                    suggestedSequenceId = customSequenceId;
                    officeCase.setSequenceId(customSequenceId);
                    String[] breakdown = officeCase.suggestOfficeId();
                    suggestedOfficeId = StringUtils.join(breakdown, "");
                    customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                    customOfficeIdPrefixSeparator = breakdown[1];
                    customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                    customOfficeIdSuffixSeparator = breakdown[3];
                    customOfficeIdSuffix = officeIdSuffix = breakdown[4];
                    
                    setParameters(event);
                    
                    event.getMessages().add("Wygenerowano nowy znak sprawy dla zmienionego numeru porz�dkowego");
                  
                    return;
                }


                officeCase.setSequenceId(customSequenceId);
                
                String oid = StringUtils.join(new String[] {
                    customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                    customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                    customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                    customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                    customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
                }, "");

                officeCase.changeOfficeId(oid,
                    customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                    customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                    customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "", false/*bylo settingPortfolio*/);
            }
            else
            {
              /*  if (settingPortfolio) {
                    officeCase.setParent(OfficeFolder.find(portfolioId));
                }*/
                officeCase.setSequenceId(suggestedSequenceId);
                officeCase.changeOfficeId(suggestedOfficeId,
                    officeIdPrefix, officeIdMiddle, officeIdSuffix, false/*bylo settingPortfolio*/);
            }
            
          /*  if (settingPortfolio)
            {
                officeCase.getAudit().add(Audit.create("portfolio",
                        DSApi.context().getPrincipalName(),
                        "Sprawa zosta�a przeniesiona do teczki "+officeCase.getParent().getOfficeId(), 
                        officeCase.getParent().getOfficeId(), officeCase.getParent().getId()));
            }*/
            
            List<InOfficeDocument> inDocuments = /*Finder.list(OfficeDocument.class, null);*/InOfficeDocument.findByOfficeCase(officeCase);               
            for (InOfficeDocument document : inDocuments) 
            {
                //if (document.getContainingCase() != null && document.getContainingCase().getId() == officeCase.getId())                 
                document.setContainingCase(officeCase,false);
                
                TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
            }
            
            List<OutOfficeDocument> outDocuments = /*Finder.list(OfficeDocument.class, null);*/OutOfficeDocument.findByOfficeCase(officeCase);
            for (OutOfficeDocument document : outDocuments) 
            {
                //if (document.getContainingCase() != null && document.getContainingCase().getId() == officeCase.getId())                 
                document.setContainingCase(officeCase,false);
                
                TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
            }
            
            ctx.commit();
        }
        catch (EdmException e)
        {
            if (ctx != null) DSApi.context().setRollbackOnly();
            event.getErrors().add(e.getMessage());
            setParameters(event);
        } catch (SQLException e) {
			if (ctx != null) DSApi.context().setRollbackOnly();
            event.getErrors().add(e.getMessage());
            setParameters(event);
		}
        finally
        {
            DSApi._close();
        }
    }
    
    private void setParameters(ActionEvent event)
    {
        event.getDynaForm().set("suggestedSequenceId", suggestedSequenceId);
        event.getRequest().setAttribute("suggestedSequenceId", suggestedSequenceId);
        event.getDynaForm().set("customSequenceId", customSequenceId);
        event.getDynaForm().set("suggestedOfficeId", suggestedOfficeId);
        event.getRequest().setAttribute("suggestedOfficeId", suggestedOfficeId);
        event.getDynaForm().set("customOfficeIdPrefix", customOfficeIdPrefix);
        event.getDynaForm().set("customOfficeIdPrefixSeparator", customOfficeIdPrefixSeparator);
        event.getDynaForm().set("customOfficeIdMiddle", customOfficeIdMiddle);
        event.getDynaForm().set("customOfficeIdSuffixSeparator", customOfficeIdSuffixSeparator);
        event.getDynaForm().set("customOfficeIdSuffix", customOfficeIdSuffix);
        event.getDynaForm().set("officeIdPrefix", officeIdPrefix);
        event.getDynaForm().set("officeIdMiddle", officeIdMiddle);
        event.getDynaForm().set("officeIdSuffix", officeIdSuffix);
    }
    
    public static String getLink(Long id)
    {
        return getLink(id, null);
    }

    public static String getLink(Long id, String application)
    {
        return getLink(id, application, null);
    }

    public static String getLink(Long id, String application, String tabId)
    {
        return getLink(id, application, tabId, null);
    }

    /**
     * Odno�nik do edycji sprawy z adresem powrotnym. Adres nie powinien
     * posiada� prefiksu aplikacji (contextPath).
     */
    public static String getLink(Long id, String application, String tabId, String returnUrl)
    {
        ModuleConfig config = (ModuleConfig)
            Configuration.getServletContext().getAttribute(Globals.MODULE_KEY);
        ForwardConfig fc = config.findForwardConfig(FORWARD);
        return fc.getPath() +
            "?id="+id +
            (application != null ? "&application="+application : "") +
            (tabId != null ? "&tabId="+tabId : "") +
            (returnUrl != null ? "&returnUrl="+encodeURL(returnUrl) : "")+ "&addToTask=true";
    }

    private static String encodeURL(String url)
    {
        try
        {
            // TODO: kodowanie trzeba uzale�nia� od kodowania strony w przegl�darce
            return URLEncoder.encode(url, "iso-8859-2");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    public Boolean getClosing()
    {
		return closing;
	}

	public void setClosing(Boolean closing) 
	{
		this.closing = closing;
	}

    private class PrepareArchivePackage implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) throws IOException {
            boolean contextOpened = false;
            try {
                contextOpened = DSApi.openContextIfNeeded();
                ArchivePackageManager man = new ArchivePackageManager();
                Long id = (Long) event.getDynaForm().get("id");
                OfficeCase officeCase = null;
                try {
                    officeCase = OfficeCase.find(id);
                } catch (EdmException e) {
                    log.error("B��d podczas szukania sprawy: "+e);
                    event.getErrors().add(e.getMessage());
                }
                List<OfficeCase> listaSpraw = new ArrayList<OfficeCase>();
                listaSpraw.add(officeCase);
                try {
                    File archivePackage = man.preparePackage(listaSpraw);
                    if(archivePackage!=null){
                        String now = DateUtils.formatFolderDate(new Date());
                        ServletUtils.streamFile(ServletActionContext.getResponse(), archivePackage , "application/zip",
                                "Content-Disposition: attachment; filename=\"paczka_archiwalna_" + now +".zip\"");
                    }

                } catch (EdmException e) {
                    log.error("B��d podczas przygotowania paczki archiwalnej:"+e);
                    event.getErrors().add(e.getMessage());
                }
            } catch (EdmException e) {
                log.error("B��d podczas szukania sprawy: "+e);
                event.getErrors().add(e.getMessage());
            } catch (Exception e) {
                log.error("B��d podczas szukania sprawy: "+e);
                event.getErrors().add(e.getMessage());
            }
            finally {
                DSApi.closeContextIfNeeded(contextOpened);
            }

        }
    }
}
