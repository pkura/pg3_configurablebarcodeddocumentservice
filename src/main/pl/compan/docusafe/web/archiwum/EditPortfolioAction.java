package pl.compan.docusafe.web.archiwum;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.api.user.office.WorkHistoryBean;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.UserToBriefcase;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.parametrization.archiwum.UdostepnionyZasob;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.filter.AuthFilter;
import pl.compan.docusafe.web.office.common.WorkHistoryTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import pl.compan.docusafe.web.admin.UserToBriefcaseAction;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EditPortfolioAction.java,v 1.51 2009/12/08 15:44:52 pecet5 Exp $
 */
public class EditPortfolioAction extends EventActionSupport
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(EditPortfolioAction.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
    public static final String TAB_MAIN = "main";
    public static final String TAB_AUDIT = "audit";

    // @EXPORT
    private List users;
    private OfficeFolder portfolio;
    private List tabs = new ArrayList(3);
    private List workHistory;
    private String listDivisionGuid;
    private boolean canDelete;
    private boolean canChangeNumber;
    private boolean canCreate;
    private boolean canSetClerk;
    private boolean canModify;
    private boolean canPrint;
    private boolean canView;
    private boolean canMoveToNextYear;
    private Pager pager;
    private String user;
    
    private boolean OnListBriefcase;
    private List<TableColumn> columns;
    ArrayList<Map<String, Object>> sprawy;
    
    // @EXPORT/@IMPORT
    private Long id;
    private String name;
    private String assignedUser;
    private String subname;
    private Long rwaCategoryId;
    private String prettyRwaCategory;
    private String barcode;
    private String days;

    private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    private String officeIdPrefix;
    private String officeIdSuffix;
    private String officeIdMiddle;

    private String suggestedOfficeId;
    private Integer customSequenceId;
    private Integer suggestedSequenceId;

    private Boolean customOfficeId;
    private String pdfLink;

    private Long childrenFrom;
    private Long childrenTo;
    
    private Boolean changingNumber;
    private Boolean mainPortfolio;
    
    private String findCaseNumber;
    private List<OfficeCase> casesFound;
    
    private String redirectUrl;
    
    private boolean generujEtykiete;
    private boolean czyDlaSegregatora;
    //kontrahenci
    private Map contractors; 
    private Long contractorId;
    private Long contractorIdOld;
    // @IMPORT
    private String contractorNameOld;
    private String tab;
    private int offset;
    // zmienna odpowiada za ilo�� spraw wy�wietlanych na stronie, domy�lnie 50
    private static int DEFAULT_LIMIT = 50;

    protected void setup()
    {
        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doUpdate").
            append(OpenHibernateSession.INSTANCE).
            append(new Update()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doPreCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new PreCreate()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doCreate").
            append(OpenHibernateSession.INSTANCE).
            append(new Create()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDelete").
            append(OpenHibernateSession.INSTANCE).
            append(new Delete()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doChangeNumber").
            append(OpenHibernateSession.INSTANCE).
            append(new PreChangeNumber()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doMoveToNextYear").
            append(OpenHibernateSession.INSTANCE).
            append(new MoveToNextYear()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doSearch").
            append(OpenHibernateSession.INSTANCE).
     //search wyukonywane przy kazdym fillformie       append(new Search()).
            append(new FillForm()).
            append(new Tabs()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doSearchAll").
        	append(OpenHibernateSession.INSTANCE).
        	append(new SearchAll()).
        	append(new FillForm()).
        	append(new Tabs()).
        	appendFinally(CloseHibernateSession.INSTANCE);
       

    }

    private boolean canDeletePortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_USUWANIE_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_USUWANIE_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private boolean canCreatePortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_TWORZENIE_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_TWORZENIE_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private boolean canSetClerk(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_ZMIANA_REFERENTA_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_ZMIANA_REFERENTA_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private boolean canModifyPortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_MODYFIKACJA_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_MODYFIKACJA_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private boolean canPrintPortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_WYDRUK_WSZEDZIE) ||
            (DSApi.context().hasPermission(DSPermission.TECZKA_WYDRUK_KOMORKA) &&
                DSApi.context().getDSUser().inDivision(division, true));
    }

    private boolean canViewPortfolio(DSDivision division) throws EdmException
    {
        return DSApi.context().hasPermission(DSPermission.TECZKA_PODGLAD_WSZEDZIE) ||
                DSApi.context().getDSUser().inDivision(division, true);
    }
    private boolean isOnListBriefcase(OfficeFolder portfolio, String user) throws EdmException
    {
       if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){
    	   return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.isOnListBriefcase( portfolio, user );
       } 
     return false;
    }
    
    private UserToBriefcase getPermisionFromListBriefcase(OfficeFolder portfolio, String user) throws EdmException
    {
    	   return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.resultsFolder( portfolio, user );
    }

    /**
     * Tworzy zak�adki. Ta klasa powinna by� wywo�ywana w ramach
     * otwartej sesji Hibernate i po klasie FillForm, poniewa�
     * oczekuje, �e pole portfolio b�dzie mia�o nadan� warto��.
     */
    private class Tabs implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (!TAB_MAIN.equals(tab) && !TAB_AUDIT.equals(tab))
                tab = TAB_MAIN;

            tabs.add(new Tab("Og�lne", "Og�lne",
                HttpUtils.makeUrl("/archives/edit-portfolio-archive.action",
                    new String[] {
                        "tab", TAB_MAIN,
                        "id", String.valueOf(id) }),
                TAB_MAIN.equals(tab)));

            tabs.add(new Tab(
                (portfolio != null && portfolio.isSubFolder() ? "Historia podteczki" : "Historia teczki"),
                (portfolio != null && portfolio.isSubFolder() ? "Historia podteczki" : "Historia teczki"),
                HttpUtils.makeUrl("/archives/edit-portfolio-archive.action",
                    new String[] {
                        "tab", TAB_AUDIT,
                        "id", String.valueOf(id) }),
                TAB_AUDIT.equals(tab)));

            // je�eli kt�ra� z poprzednich akcji wymusi�a przekierowanie do
            // konkretnej strony, nie zmieniam go
            if (event.getResult() == null)
                event.setResult(tab);
        }
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                user = DSApi.context().getPrincipalName();
                
                portfolio = OfficeFolder.find(id);
                DSDivision division = DSDivision.find(portfolio.getDivisionGuid());
                
                canView = canViewPortfolio(division);
                
                if(user.equals(portfolio.getClerk()) )
                {  
                	canView = true;
                }
				columns=new ArrayList<TableColumn>(7);
				columns.add(new TableColumn("lp","L.p.",null,null));
				columns.add(new TableColumn("znak","Znak sprawy",null,null));
				columns.add(new TableColumn("tytul","Tytu�",null,null));
				columns.add(new TableColumn("dataUtw","Data utworzenia sprawy",null,null));
				columns.add(new TableColumn("kategoria","Kat. akt",null,null));
				columns.add(new TableColumn("dataPrzek","Data przekazania do archiwum",null,null));
				
				columns.add(new TableColumn("miejscePrzech",sm.getString("archiwumFizyczneMiejscePrzechowywaniaAkt"+((Docusafe.getAdditionProperty("archwium.archiwum_zakladowe_name")==null)?"":Docusafe.getAdditionProperty("archwium.archiwum_zakladowe_name"))),null,null));
               
                if (DSApi.context().hasPermission(DSPermission.PODGLAD_WSZYSTKICH_TECZEK_W_PODWYDZIALACH))
            	{
                	List<String> divGuids = new ArrayList<String>();
                	DSUser user = DSApi.context().getDSUser();
                	DSDivision[] divisions = user.getSubordinateDivisions();
                	for (DSDivision div :divisions)
                		 divGuids.add(div.getGuid());
                	for (String s : divGuids){
                		if (s.contains(division.getGuid())){
                    		canView = true;
                    		canDelete = false;
                    		canCreate = false;
                    		canChangeNumber = false;
                    		canMoveToNextYear = false;
                    		canPrint=false;
                    		canModify=false;
                    		canSetClerk=false;
                    		break;
                    	}
                	}
                	
            	}
                
                
                if(OnListBriefcase =isOnListBriefcase(portfolio,user)){
                	canView = true;
                }

                if (!canView)
                    canView = UdostepnionyZasob.isBorrowed(user,UdostepnionyZasob.TECZKA, portfolio.getId());

               /* if ( 	isOnListBriefcase portfolio, user ){
                	canView = true;
                }
                */
                if (canView)
                {
                    days = portfolio.getDays() != null ? portfolio.getDays().toString() : null;
                    
                    // inicjalizacja kolekcji dla JSP
                    DSApi.initializeProxy(portfolio.getChildren());
                    DSApi.initializeProxy(portfolio.getParent());
                    for (Iterator iter = portfolio.getChildren().iterator(); iter.hasNext();)
                    {
                        Container c = (Container) iter.next();
                        if (c instanceof OfficeCase)
                            DSApi.initializeProxy(((OfficeCase) c).getStatus());
                    }
                    
                    
                    if (OnListBriefcase){
                    	UserToBriefcase utb = getPermisionFromListBriefcase(portfolio, user);
                    	canView =  utb.getCanView();
                    	canCreate = utb.getCanCreate();
                    	canModify= utb.getCanModify();
                    	canPrint= utb.getCanPrint();
                    	
                    
                    }else {
                    prettyRwaCategory = portfolio.getRwa().getRwa() + ": " + portfolio.getRwa().getDescription();
                    canDelete = portfolio.getChildren().size() == 0 && canDeletePortfolio(division);
                    canCreate = canCreatePortfolio(division);
                    canSetClerk = canSetClerk(division);
                    canChangeNumber = DSApi.context().hasPermission(DSPermission.TECZKA_ZMIANA_NUMERU);
                    canModify = canModifyPortfolio(division);
                    canPrint = canPrintPortfolio(division);
                    
                    
                    int currentYear = GlobalPreferences.getCurrentYear();
                    canMoveToNextYear = (DSApi.context().hasPermission(DSPermission.TECZKA_PRZEPISYWANIE_NA_NOWY_ROK)/*DSApi.context().isAdmin()*/) && (portfolio.getYear() == currentYear - 1)
                        && (portfolio.getParent() == null);
                    }
                    users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
                    
                    if (portfolio.getAudit() != null)
                    {
                        workHistory = new ArrayList(portfolio.getAudit().size());
                        List workHistoryCopy = new ArrayList(portfolio.getAudit());
                        Collections.reverse(workHistoryCopy);
                        for (Iterator iter = workHistoryCopy.iterator(); iter.hasNext();)
                        {
                            workHistory.add(new WorkHistoryBean((Audit) iter.next()));
                        }
                    }
                    
                    Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                    {
                        public String getLink(int offset)
                        {
                            return HttpUtils.makeUrl("/archives/edit-portfolio-archive.action", new Object[] { "id", id, "offset",
                                String.valueOf(offset) });
                        }
                    };
                    // wyswietlenie odpowiedniej ilosci spraw na stronie w teczce, w zaleznosci od ustawien uzyktownika
                    DEFAULT_LIMIT = DSApi.context().userPreferences().node("other").getInt("case-count-per-page",DEFAULT_LIMIT);
                    Search s=new Search();
                    s.actionPerformed(event);
                    
                    
                    int wielkoscWynikow=sprawy.size();
                   // childrenFrom = new Long(offset);
                    //childrenTo = new Long(offset + DEFAULT_LIMIT - 1);
                    if(offset+DEFAULT_LIMIT>sprawy.size())
                    	sprawy=new ArrayList<Map<String, Object>>(sprawy.subList(offset, sprawy.size()));
                    else
                    	sprawy=new ArrayList<Map<String, Object>>( sprawy.subList(offset, offset + DEFAULT_LIMIT ));
                    pager = new Pager(linkVisitor, offset, DEFAULT_LIMIT, wielkoscWynikow, 10);
                    
                   
                    
                    
                    if(AvailabilityManager.isAvailable("UTP.teczki.przypisz.kontrahent")){

                    	contractors=new HashMap<String,String>();
                    	contractors.put(Long.parseLong("0"), "Nie przypisany");
                    	contractorNameOld="Nie przypisany";
                    	try {

                    		PreparedStatement ps=DSApi.context().prepareStatement("select id_kontrahenta from UTP_KONT_DO_TECZKI where id_teczki=?");
                    		ps.setLong(1, portfolio.getId());
                    		ResultSet rs=ps.executeQuery();
                    		if(rs.next())contractorIdOld=rs.getLong("id_Kontrahenta");
                    		else contractorIdOld=Long.parseLong("0");

                    		ps=DSApi.context().prepareStatement("select distinct id,imie,nazwisko from [utp].[dbo].[DSG_KONTRAHENT]");
                    		rs=ps.executeQuery();
                    		while(rs.next()){


                    			String name="";
                    			if(rs.getString("nazwisko")!=null){
                    				if(!rs.getString("nazwisko").equals("")){
                    					name=rs.getString("nazwisko");
                    				}
                    			}
                    			if(rs.getString("imie")!=null){
                    				if(!rs.getString("imie").equals("")){
                    					name=name+" "+rs.getString("imie");
                    				}
                    			}

                    			if(contractorIdOld.longValue()==rs.getLong("id")){
                    				contractorNameOld=name;
                    			}
                    			contractors.put(rs.getLong("id"),name);
                    		}
                			

                    	} catch (SQLException e) {
                    		// TODO Auto-generated catch block
                    		e.printStackTrace();
                    	}

                    }
                    
                    
                    //generowanie etykiet
                    if(generujEtykiete)generujEtykiete(czyDlaSegregatora);
                    	
                    
                    
                    
                }
                else 
                {
                    addActionError("Brak uprawnie� do podgl�du teczki");
                }
                
            }
            catch (EdmException e)
            {
                addActionError(e.getMessage());
            }
        }
    }

    private class Update implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(name))
                addActionError("Nie podano opisu teczki");

            if (hasActionErrors())
                return;

            Integer _days = null;
            
            if (TextUtils.trimmedStringOrNull(days) != null) {
                try
                {
                    _days = new Integer(TextUtils.trimmedStringOrNull(days));
                    
                }
                catch (Exception e)
                {
                    addActionError("Podana liczba dni jest nieprawid�owa");
                }
            }
            
            name = name.trim();

            try
            {
                DSApi.context().begin();

                portfolio = OfficeFolder.find(id);

                String oldName = portfolio.getName();
                portfolio.setName(name);

                portfolio.setDays(_days);
                
                String oldAssignedUser = portfolio.getClerk();
                DSUser user = null;

                if (canSetClerk(DSDivision.find(portfolio.getDivisionGuid())))
                {
                	user = DSUser.findByUsername(assignedUser);
                    portfolio.setClerk(assignedUser);
                }

                portfolio.setBarcode(TextUtils.trimmedStringOrNull(barcode));
                
                
                if(AvailabilityManager.isAvailable("UTP.teczki.przypisz.kontrahent")){
                	PreparedStatement ps=DSApi.context().prepareStatement("select id_kontrahenta from UTP_KONT_DO_TECZKI where id_teczki=?");
            		ps.setLong(1, portfolio.getId());
					ResultSet rs=ps.executeQuery();
            		if(rs.next())contractorIdOld=rs.getLong("id_Kontrahenta");
            		else contractorIdOld=Long.parseLong("0");
            		String tmp="";
            		if(contractorId==null){
            			contractorId=(long) 0;
            		}
                	if(contractorId.longValue()!=contractorIdOld.longValue()){
                			ps=DSApi.context().prepareStatement("update UTP_KONT_DO_TECZKI set id_kontrahenta=? where id_teczki=?");
                			ps.setLong(1, contractorId);
                			ps.setLong(2, portfolio.getId());
                			if(ps.executeUpdate()==0){
                				//nie bylo jeszcze wpisu
                				ps=DSApi.context().prepareStatement("insert into UTP_KONT_DO_TECZKI(id_kontrahenta,id_teczki) values(?,?)");
                    			ps.setLong(1, contractorId);
                    			ps.setLong(2, portfolio.getId());
                    			ps.executeUpdate();
                			}
                			if(contractorId!=0){
                			ps=DSApi.context().prepareStatement("select distinct imie,nazwisko from dbo.DSG_KONTRAHENT where id=?");
							ps.setLong(1, contractorId);
                			rs=ps.executeQuery();
                			if(rs.next()){
                					tmp = sm.getString("ZmianaKontrahentaNa",rs.getString("imie")+" "+rs.getString("nazwisko"));
                			}
                			}
                			else{
                				tmp=sm.getString("ZmianaKontrahentaNa","Nie przypisany");
                			}
                			portfolio.getAudit().add(Audit.create("contractor", DSApi.context().getPrincipalName(),
                	               tmp));
                			 
                			
                			
                	}
                		
                }
                DSApi.context().commit();

                if (user != null &&
                    (oldAssignedUser == null || !oldAssignedUser.equals(assignedUser)))
                    addActionMessage("Zmieniono referenta teczki na "+
                        user.asFirstnameLastname());

                if (oldName == null || !oldName.equals(name))
                    addActionMessage("Zmieniono nazw� teczki na "+name);
               
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
            	DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
        }
    }

    private class PreCreate implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (StringUtils.isEmpty(subname))
                addActionError("Nie podano nazwy podteczki");

            if (hasActionErrors())
                return;

            event.setResult("pre-create");

            try
            {
                portfolio = OfficeFolder.find(id);

                if (!canCreatePortfolio(DSDivision.find(portfolio.getDivisionGuid()))){
                	if (!canCreatePortfolioFromListBriefcase(portfolio))
                    throw new EdmException("Brak uprawnie� do utworzenia podteczki");
                }

                OfficeFolder subfolder = new OfficeFolder(subname,
                    DSDivision.find(portfolio.getDivisionGuid()), portfolio.getRwa(), portfolio);

                suggestedSequenceId = customSequenceId = subfolder.suggestSequenceId();

                String[] breakdown = subfolder.suggestOfficeId();
                suggestedOfficeId = StringUtils.join(breakdown, "");
                customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                customOfficeIdPrefixSeparator = breakdown[1];
                customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                customOfficeIdSuffixSeparator = breakdown[3];
                customOfficeIdSuffix = officeIdSuffix = breakdown[4];

            }
            catch (EdmException e)
            {
                event.setResult(SUCCESS);
                addActionError(e.getMessage());
            }
        }

		private boolean canCreatePortfolioFromListBriefcase(OfficeFolder portfolio) throws UserNotFoundException, EdmException
		{
			if (!AvailabilityManager.isAvailable("menu.left.user.to.briefcase"))
	    		return false;
	    	else
	    	return  pl.compan.docusafe.web.admin.UserToBriefcaseAction.canCreatePortfolioFromListBriefcase(portfolio ,DSApi.context().getDSUser() );
		}
    }

    /**
     * Tworzenie podteczki lub zmiana numeru teczki. 
     * (gdy changingNumber==true to zmieniamy tylko numer teczki)
     */
    private class Create implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            if (changingNumber == null)
                changingNumber = Boolean.valueOf(false);
            if (mainPortfolio == null)
                mainPortfolio = Boolean.valueOf(false);
            
            if (!changingNumber) {
                if (StringUtils.isEmpty(subname))
                    addActionError("Nie podano nazwy podteczki");
    
                if (hasActionErrors())
                    return;
    
                subname = TextUtils.trimmedStringOrNull(subname);
            }
            
            try
            {
                DSApi.context().begin();

                portfolio = OfficeFolder.find(id);

                // TODO: sprawdzi�, czy istnieje podteczka o tej nazwie
/*
                for (Iterator iter=portfolio.getChildren().iterator(); iter.hasNext(); )
                {
                    if ((((Portfolio) iter.next()).getName().equals(subname)))
                        throw new EdmException("W tej teczce znajduje si� ju� podteczka o nazwie "+subname);
                }
*/
                
                OfficeFolder subfolder;
                if (changingNumber)
                    /* na zmiennej jest ta teczka ktora tworzymy/zmieniamy
                     * wiec musze na nia przypisac portfolio, bo to jej zmieniamy numer 
                     */
                    subfolder = portfolio;
                else {
                    subfolder = new OfficeFolder(subname,
                        DSDivision.find(portfolio.getDivisionGuid()), portfolio.getRwa(), portfolio);
                    subfolder.setClerk(DSApi.context().getPrincipalName());
                    subfolder.setYear(new Integer(GlobalPreferences.getCurrentYear()));
                }
                
                if (customOfficeId != null && customOfficeId.booleanValue())
                {
                    if (!mainPortfolio)
                    {
                        if (customSequenceId == null)
                            throw new EdmException("Nie podano numeru kolejnego podteczki lub " +
                                "wpisano niepoprawn� liczb�");
    
                        if (customSequenceId.intValue() <= 0)
                            throw new EdmException("Numer kolejny podteczki musi by� wi�kszy od zera");
    
                        if (!subfolder.sequenceIdAvailable(customSequenceId))
                            if (!changingNumber || subfolder.getSequenceId().intValue() != customSequenceId.intValue())
                                /* gdy tylko zmieniamy numer to mozemy pozostawic taki sam */
                                throw new EdmException("Wybrany numer kolejny ("+customSequenceId+") jest niedost�pny");
    
                        subfolder.setSequenceId(customSequenceId);
    
                        // zmodyfikowano numer kolejny sprawy, ponowne wygenerowanie
                        // ca�ego numeru
                        if (suggestedSequenceId.intValue() != customSequenceId.intValue())
                        {
                            suggestedSequenceId = customSequenceId;
                            subfolder.setSequenceId(customSequenceId);
                            String[] breakdown = subfolder.suggestOfficeId();
                            suggestedOfficeId = StringUtils.join(breakdown, "");
                            customOfficeIdPrefix = officeIdPrefix = breakdown[0];
                            customOfficeIdPrefixSeparator = breakdown[1];
                            customOfficeIdMiddle = officeIdMiddle = breakdown[2];
                            customOfficeIdSuffixSeparator = breakdown[3];
                            customOfficeIdSuffix = officeIdSuffix = breakdown[4];
                            addActionMessage("Wygenerowano nowy numer podteczki dla zmienionego numeru porz�dkowego");
                            event.setResult("pre-create");
                            return;
                        }
                    }
                    
                    String oid = StringUtils.join(new String[] {
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
                    }, "");

                    subfolder.changeOfficeId(oid,
                        customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                        customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                        customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "", !changingNumber);
                }
                else
                {
                    subfolder.setSequenceId(suggestedSequenceId);
                    subfolder.changeOfficeId(suggestedOfficeId,
                        officeIdPrefix, officeIdMiddle, officeIdSuffix, !changingNumber);
                }

                // suggest ...
/*
                Portfolio sub = portfolio.createSubPortfolio(
                    subname,
                    DSApi.context().getDSUser().getName(),
                    DSApi.context().getDSUser().getName());
*/

                DSApi.context().commit();

                if (changingNumber){
                    addActionMessage("Zmieniono symbol teczki na "+subfolder.getOfficeId());
                    
                	if (AvailabilityManager.isAvailable("menu.left.user.to.briefcase")){
                		String oldOfficeId = officeIdPrefix+customOfficeIdPrefixSeparator+officeIdMiddle; 
                	pl.compan.docusafe.web.admin.UserToBriefcaseAction.onUpdateBriefcase(suggestedSequenceId ,oldOfficeId, subfolder.getOfficeId() );
                	}
                } else
                    addActionMessage("Utworzono podteczk� "+subfolder.getOfficeId());
            }
            catch (EdmException e)
            {
                event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
            	event.setResult("pre-create");
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
        }
    }

    private class Delete implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                portfolio = OfficeFolder.find(id);

                if (!canDeletePortfolio(DSDivision.find(portfolio.getDivisionGuid())))
                    throw new EdmException("Brak uprawnie� do usuni�cia teczki");

                if (portfolio.getChildren().size() > 0)
                    throw new EdmException("Nie mo�na usun�� teczki zawieraj�cej podteczki lub sprawy");

                //portfolio.getAudit().clear();

                listDivisionGuid = portfolio.getDivisionGuid();

/*
                try
                {
                    Division portfolioDivision = Division.find(portfolio.getDivisionGuid());
                    if (portfolioDivision.parent() != null)
                        listDivisionGuid = portfolioDivision.parent().getGuid();
                }
                catch (DivisionNotFoundException e)
                {
                }
*/

                portfolio.delete();

                DSApi.context().commit();

                event.setResult("list");
                event.cancel();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    
    private class PreChangeNumber implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            event.setResult("pre-create");

            try
            {
                DSApi.context().begin();
                
                portfolio = OfficeFolder.find(id);
                
                if (!DSApi.context().hasPermission(DSPermission.TECZKA_ZMIANA_NUMERU))
                    throw new EdmException("Brak uprawnie� do zmiany numeru teczki");
                suggestedSequenceId = customSequenceId = portfolio.getSequenceId();

                suggestedOfficeId = portfolio.getOfficeId();
                customOfficeIdPrefix = officeIdPrefix = portfolio.getOfficeIdPrefix();
                if (portfolio.getOfficeIdPrefix() != null && portfolio.getOfficeIdPrefix().length() > 0)
                {
                    int index = portfolio.getOfficeIdPrefix().length(); // = koniec prefixu +1 
                    customOfficeIdPrefixSeparator = portfolio.getOfficeId().substring(index,index+1); 
                }
                else
                    customOfficeIdPrefixSeparator = null;                
                customOfficeIdMiddle = officeIdMiddle = portfolio.getOfficeIdMiddle();
                if (portfolio.getOfficeIdSuffix() != null && portfolio.getOfficeIdSuffix().length() > 0)
                {
                    int index = portfolio.getOfficeId().length() - portfolio.getOfficeIdSuffix().length(); // = poczatek suffixu
                    customOfficeIdSuffixSeparator = portfolio.getOfficeId().substring(index-1,index);
                }
                else
                    customOfficeIdSuffixSeparator = null;
                customOfficeIdSuffix = officeIdSuffix = portfolio.getOfficeIdSuffix();

                if (portfolio.getParent() == null)
                    mainPortfolio = Boolean.valueOf(true);
                
                changingNumber = Boolean.valueOf(true);
                
                DSApi.context().commit();

            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            }
        }
    }

    private class MoveToNextYear implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                DSApi.context().begin();

                int currentYear = GlobalPreferences.getCurrentYear();
                OfficeFolder rootFolder = OfficeFolder.find(id);
               // canMoveToNextYear = (DSApi.context().isAdmin()) && (portfolio.getYear() == currentYear - 1) && (portfolio.getParent() == null);

                // tworze nowa teczke
                OfficeFolder newFolder = new OfficeFolder(
                    rootFolder.getName(),
                    DSDivision.find(rootFolder.getDivisionGuid()),
                    rootFolder.getRwa(), null);
                newFolder.setClerk(rootFolder.getClerk());
                newFolder.setSequenceId(rootFolder.getSequenceId());
                newFolder.setYear(new Integer(currentYear));

                newFolder.create(rootFolder.getOfficeId(),
                    rootFolder.getOfficeIdPrefix(),
                    rootFolder.getOfficeIdMiddle(),
                    rootFolder.getOfficeIdSuffix());

                // tworze podteczki
                OfficeFolder.createSubfolders(newFolder, rootFolder.getSubfolders(), currentYear);

                addActionMessage("Utworzono now� teczk� "+newFolder.getOfficeId());

                DSApi.context().commit();
            }
            catch (EdmException e)
            {
                DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
            } catch (SQLException e) {
				 DSApi.context().setRollbackOnly();
                addActionError(e.getMessage());
			}
        }
    }
    
    private class SearchAll implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{	
    		findCaseNumber=null;
    	}
    }
    
    
    private class Search implements ActionListener
    {
    	public void actionPerformed(ActionEvent event)
    	{	
    		try
    		{ 
    			sprawy=new ArrayList<Map<String,Object>>();
    			int licznik=1;
    			DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
    			String miejsce;
    			Date data;
    			FromClause from=new FromClause();
    			TableAlias fromSpis=from.createTable(NewCaseArchiverScheduled.wykazItemsTableName);

    			SelectClause select=new SelectClause(false);
    			SelectColumn dataPrzek = select.add(fromSpis, "DATE_SHRED");
    			SelectColumn miejscePrzech = select.add(fromSpis, "ARCH_PLACE");

    			WhereClause where = new WhereClause();
    			where.add(Expression.eq(fromSpis.attribute("FOLDER_OFFICEID"), portfolio.getOfficeId()));
    			SelectQuery selectQuery = new SelectQuery(select, from, where, null);
    			ResultSet rs=selectQuery.resultSet();
    			if(rs.next()){
    				if(rs.getString(dataPrzek.getPosition())!=null){
    					data=rs.getDate(dataPrzek.getPosition());
    				}
    				else data=null;
    				if(rs.getString(miejscePrzech.getPosition())!=null){
    					miejsce=rs.getString(miejscePrzech.getPosition());
    				}
    				else miejsce="---";
    			}
    			else{
    				miejsce="---";
    				data=null;
    			}




    			if (findCaseNumber == null)
    			{	
    				findCaseNumber=null;
    				casesFound=OfficeCase.findByParentId(portfolio.getId());
    				for(OfficeCase c:casesFound){
    					Map<String,Object> sprawa=new LinkedHashMap<String,Object>(7);
    					sprawa.put("lp",Integer.toString(licznik++));
    					sprawa.put("znak",c.getOfficeId());
    					sprawa.put("tytul", c.getTitle());
    					sprawa.put("dataUtw",df.format(c.getCtime()));
    					sprawa.put("kategoria", c.getRwa().getAcHome());   	
    					if(data!=null){
    						sprawa.put("dataPrzek", df.format(data));
    					}
    					else sprawa.put("dataPrzek", "---");
    					sprawa.put("miejscePrzech", miejsce);
    					sprawa.put("link", "edit-case.do?id=" + (Long) c.getId());
    					sprawy.add(sprawa);
    				}


    			}
    			else{
    				findCaseNumber = TextUtils.trimmedStringOrNull(findCaseNumber);
    				if (findCaseNumber.indexOf('-') < 0){
    					casesFound = OfficeCase.findByOfficeIdLike(findCaseNumber, OfficeFolder.find(id));


    					for(OfficeCase c:casesFound){
    						Map<String,Object> sprawa=new LinkedHashMap<String,Object>(7);
    						sprawa.put("lp",Integer.toString(licznik++));
    						sprawa.put("znak",c.getOfficeId());
    						sprawa.put("tytul", c.getTitle());
    						sprawa.put("dataUtw",df.format(c.getCtime()));
    						sprawa.put("kategoria", c.getRwa().getAcHome());
    						if(data!=null){
    							sprawa.put("dataPrzek", df.format(data));
    						}
    						else sprawa.put("dataPrzek", "---");
    						sprawa.put("miejscePrzech", miejsce);
    						sprawa.put("link", "edit-case.do?id=" + (Long) c.getId());
    						sprawy.add(sprawa);
    					}

    				}
    				else
    				{
    					OfficeCase caseFound = OfficeCase.findByOfficeIdLike(findCaseNumber);

    					Map<String,Object> sprawa=new LinkedHashMap<String,Object>(7);
    					sprawa.put("lp",Integer.toString(licznik++));
    					sprawa.put("znak",caseFound.getOfficeId());
    					sprawa.put("tytul",caseFound.getTitle());
    					sprawa.put("dataUtw",df.format(caseFound.getCtime()));
    					sprawa.put("kategoria", caseFound.getRwa().getAcHome());
    					if(data!=null){
    						sprawa.put("dataPrzek", df.format(data));
    					}
    					else sprawa.put("dataPrzek", "---");
    					sprawa.put("miejscePrzech", miejsce);
    					sprawa.put("link", "edit-case.do?id=" + (Long) caseFound.getId());
    					sprawy.add(sprawa);

    				}     
    			}
    		}
    		catch (EdmException e)
    		{
    			log.debug("", e);
    			addActionError(e.getMessage());
    		} catch (SQLException e) {
    			log.debug("", e);
    			e.printStackTrace();
    		}
    	}
    }

    private void generujEtykiete(boolean czyDlaSegregatora) {
    	String zawartosc = "";
    	BufferedReader br;
    	String line;
    	try {
    		InputStream inputStream;
    		if(czyDlaSegregatora)inputStream = 
    				new FileInputStream(Docusafe.getHome()+"/templates/opis_teczki_czyli_segregator.rtf");
    		else inputStream = 
    				new FileInputStream(Docusafe.getHome()+"/templates/opis_teczki_czyli_etykieta.rtf");
    		br = new BufferedReader(new InputStreamReader(inputStream));
    		while((line=br.readLine())!=null){
    			zawartosc =zawartosc+ line;
    		}
    		br.close();

    		zawartosc=zawartosc.replace("$OFFICEID",OfficeFolder.find(id).getOfficeId());
    		zawartosc=zawartosc.replace("$KAT",OfficeFolder.find(id).getRwa().getAcHome());
    		zawartosc=zawartosc.replace("$DIVISIONNAME", OfficeFolder.find(id).getDivisionName());
    		//zawartosc=zawartosc.replace("$SYGNATURA", "to-do prawdopodobnie officeid/numernaspiciezdodb");
    		//zawartosc=zawartosc.replace("$LATA", OfficeFolder.find(id).getYear().toString());

    		/*   select items.FOLDER_OFFICEID,items.DATE_FROM,items.DATE_TO,spis.number from "+NewCaseArchiverScheduled.wykazItemsTableName+" items
           left join "+NewCaseArchiverScheduled.wykazMultipleTableName+" multi on multi.FIELD_VAL=items.ID
           left join "+NewCaseArchiverScheduled.wykazTableName+" spis on spis.DOCUMENT_ID=multi.DOCUMENT_ID*/

    		SelectClause select=new SelectClause(true);
    		FromClause from = new FromClause();
    		TableAlias fromItems=from.createTable(NewCaseArchiverScheduled.wykazItemsTableName);
    		String cast="";
    		//jesli postgres castujemy joina jako integer 
    		if(DSApi.isPostgresServer())
    			 cast = " ::integer";
    		TableAlias fromMulti=from.createJoinedTable(NewCaseArchiverScheduled.wykazMultipleTableName, true, fromItems, FromClause.LEFT_OUTER, "$l.id=$r.field_val "+cast );
    		TableAlias fromSpis=from.createJoinedTable(NewCaseArchiverScheduled.wykazTableName, true,fromMulti, FromClause.LEFT_OUTER,"$l.document_id=$r.document_id");
    		//SelectColumn officeId = select.add(fromItems, "FOLDER_OFFICEID");
    		SelectColumn dateFrom = select.add(fromItems, "DATE_FROM");
    		SelectColumn dateTo = select.add(fromItems, "DATE_TO");
    		SelectColumn numer = select.add(fromSpis, "number_itr");
    		SelectColumn documentId = select.add(fromSpis, "DOCUMENT_ID");

    		WhereClause where=new WhereClause();
    		where.add(Expression.eq(fromItems.attribute("FOLDER_OFFICEID"),OfficeFolder.find(id).getOfficeId()));
    		
    		SelectQuery selectQuery = new SelectQuery(select, from, where, null);
    		ResultSet rs=selectQuery.resultSet();
    		if(rs.next()){
    			String spis="";
    			/*spis+=OfficeDocument.find(rs.getLong(documentId.getPosition())).getTitle();*/
    			//spis+=" numer ";
    			spis+=rs.getString(numer.getPosition());
    			/*if(czyDlaSegregatora){
    				int licznik=1;
    				while(zawartosc.contains("$SPIS"+licznik))licznik++;
    				licznik--;
    				for(int i=1;i<=licznik;i++){
    					String fragment="";
    					//linia po 14 znak�w
    					if((i-1)*14>spis.length()){
    					}
    					else if(i*14>spis.length()){
    						fragment=spis.substring((i-1)*14, spis.length());
    					}
    					else{
    						fragment=spis.substring((i-1)*14, i*14);
    					}
    					
    					
    					zawartosc=zawartosc.replace("$SPIS"+i,fragment);
    				}
    			}
    			else*/ zawartosc=zawartosc.replace("$SPIS",spis);
    			String lata="Lata ";
    			lata+=Integer.toString(DateUtils.parseDateAnyFormat(rs.getString(dateFrom.getPosition())).getYear()+1900);
    			lata+="-";
    			lata+=Integer.toString(DateUtils.parseDateAnyFormat(rs.getString(dateTo.getPosition())).getYear()+1900);
    			zawartosc=zawartosc.replace("$LATA",lata);
    		}


    		File  tmpfile = File.createTempFile("DocuSafe", "tmp");


    		FileOutputStream outputStream = 
    				new FileOutputStream(tmpfile);

    		outputStream.write(zawartosc.getBytes());						
    		tmpfile.deleteOnExit();
    		outputStream.close();
    		ServletUtils.streamFile(ServletActionContext.getResponse(), tmpfile, "application/msword", "Content-Disposition: attachment; filename=\"etykieta.rtf\"");


    	} catch (FileNotFoundException e) {
    		log.debug("",e);
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    		log.debug("",e);
    	} catch (EdmException e) {
    		log.debug("",e);
    		e.printStackTrace();
    	} catch (SQLException e) {
    		log.debug("",e);
    		e.printStackTrace();
    	} catch (ParseException e) {
    		log.debug("",e);
    		e.printStackTrace();
    	}
    }
    
    public String getLink(){
    //	return  "&sortField="+field+"&ascending="+ascending;
    	return "/archives/edit-portfolio-archive.action?id="+getId().toString()
    			+ "&offset="+ offset;

    }
    public List getUsers()
    {
        return users;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public OfficeFolder getPortfolio()
    {
        return portfolio;
    }

    public String getAssignedUser()
    {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser)
    {
        this.assignedUser = assignedUser;
    }

    public String getSubname()
    {
        return subname;
    }

    public void setSubname(String subname)
    {
        this.subname = subname;
    }

    public Long getRwaCategoryId()
    {
        return rwaCategoryId;
    }

    public void setRwaCategoryId(Long rwaCategoryId)
    {
        this.rwaCategoryId = rwaCategoryId;
    }

    public String getPrettyRwaCategory()
    {
        return prettyRwaCategory;
    }

    public void setPrettyRwaCategory(String prettyRwaCategory)
    {
        this.prettyRwaCategory = prettyRwaCategory;
    }

    public void setTab(String tab)
    {
        this.tab = tab;
    }

    public List getTabs()
    {
        return tabs;
    }

    public List getWorkHistory()
    {
        return workHistory;
    }

    public String getListDivisionGuid()
    {
        return listDivisionGuid;
    }

    public boolean isCanDelete()
    {
        return canDelete;
    }

    public boolean isCanChangeNumber()
    {
        return canChangeNumber;
    }
    
    public boolean isCanCreate()
    {
        return canCreate;
    }

    public boolean isCanSetClerk()
    {
        return canSetClerk;
    }

    public void setCanSetClerk(boolean canSetClerk)
    {
        this.canSetClerk = canSetClerk;
    }

    public boolean isCanModify()
    {
        return canModify;
    }

    public boolean isCanView()
    {
        return canView;
    }

    public boolean isCanPrint()
    {
        return canPrint;
    }

    public Pager getPager()
    {
        return pager;
    }
    
    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    
    public Long getChildrenFrom()
    {
        return childrenFrom;
    }
  
    public Long getChildrenTo()
    {
        return childrenTo;
    }
    
    public String getBarcode()
    {
        return barcode;
    }

    public void setBarcode(String barcode)
    {
        this.barcode = barcode;
    }

    public String getCustomOfficeIdPrefix()
    {
        return customOfficeIdPrefix;
    }

    public void setCustomOfficeIdPrefix(String customOfficeIdPrefix)
    {
        this.customOfficeIdPrefix = customOfficeIdPrefix;
    }

    public String getCustomOfficeIdPrefixSeparator()
    {
        return customOfficeIdPrefixSeparator;
    }

    public void setCustomOfficeIdPrefixSeparator(String customOfficeIdPrefixSeparator)
    {
        this.customOfficeIdPrefixSeparator = customOfficeIdPrefixSeparator;
    }

    public String getCustomOfficeIdMiddle()
    {
        return customOfficeIdMiddle;
    }

    public void setCustomOfficeIdMiddle(String customOfficeIdMiddle)
    {
        this.customOfficeIdMiddle = customOfficeIdMiddle;
    }

    public String getCustomOfficeIdSuffixSeparator()
    {
        return customOfficeIdSuffixSeparator;
    }

    public void setCustomOfficeIdSuffixSeparator(String customOfficeIdSuffixSeparator)
    {
        this.customOfficeIdSuffixSeparator = customOfficeIdSuffixSeparator;
    }

    public String getCustomOfficeIdSuffix()
    {
        return customOfficeIdSuffix;
    }

    public void setCustomOfficeIdSuffix(String customOfficeIdSuffix)
    {
        this.customOfficeIdSuffix = customOfficeIdSuffix;
    }

    public String getOfficeIdPrefix()
    {
        return officeIdPrefix;
    }

    public void setOfficeIdPrefix(String officeIdPrefix)
    {
        this.officeIdPrefix = officeIdPrefix;
    }

    public String getOfficeIdSuffix()
    {
        return officeIdSuffix;
    }

    public void setOfficeIdSuffix(String officeIdSuffix)
    {
        this.officeIdSuffix = officeIdSuffix;
    }

    public String getOfficeIdMiddle()
    {
        return officeIdMiddle;
    }

    public void setOfficeIdMiddle(String officeIdMiddle)
    {
        this.officeIdMiddle = officeIdMiddle;
    }

    public String getSuggestedOfficeId()
    {
        return suggestedOfficeId;
    }

    public void setSuggestedOfficeId(String suggestedOfficeId)
    {
        this.suggestedOfficeId = suggestedOfficeId;
    }

    public Boolean getCustomOfficeId()
    {
        return customOfficeId;
    }

    public void setCustomOfficeId(Boolean customOfficeId)
    {
        this.customOfficeId = customOfficeId;
    }

    public Integer getSuggestedSequenceId()
    {
        return suggestedSequenceId;
    }

    public void setSuggestedSequenceId(Integer suggestedSequenceId)
    {
        this.suggestedSequenceId = suggestedSequenceId;
    }

    public Integer getCustomSequenceId()
    {
        return customSequenceId;
    }

    public void setCustomSequenceId(Integer customSequenceId)
    {
        this.customSequenceId = customSequenceId;
    }

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    public String getDays()
    {
        return days;
    }
    
    public void setDays(String days)
    {
        this.days = days;
    }
    
    public void setChangingNumber(Boolean changingNumber)
    {
        this.changingNumber = changingNumber;
    }
    
    public Boolean getChangingNumber()
    {
        return changingNumber;
    }
    
    public void setMainPortfolio(Boolean mainPortfolio)
    {
        this.mainPortfolio = mainPortfolio;
    }
    
    public Boolean getMainPortfolio()
    {
        return mainPortfolio;
    }

    public boolean isCanMoveToNextYear()
    {
        return canMoveToNextYear;
    }

    public void setFindCaseNumber(String findCaseNumber)
    {
        this.findCaseNumber = findCaseNumber;
    }

    public String getFindCaseNumber()
    {
        return findCaseNumber;
    }

    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    public List<OfficeCase> getCasesFound()
    {
        return casesFound;
    }

    public String getUser()
    {
        return user;
    }


	public Map getContractors() {
		return contractors;
	}

	public Long getContractorIdOld() {
		return contractorIdOld;
	}

	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}

	public String getContractorNameOld() {
		return contractorNameOld;
	}

	public void setContractorNameOld(String contractorNameOld) {
		this.contractorNameOld = contractorNameOld;
	}

	public List<TableColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}

	public ArrayList<Map<String, Object>> getSprawy() {
		return sprawy;
	}

	public void setSprawy(ArrayList<Map<String, Object>> sprawy) {
		this.sprawy = sprawy;
	}

	public boolean isGenerujEtykiete() {
		return generujEtykiete;
	}

	public void setGenerujEtykiete(boolean generujEtykiete) {
		this.generujEtykiete = generujEtykiete;
	}

	public boolean isCzyDlaSegregatora() {
		return czyDlaSegregatora;
	}

	public void setCzyDlaSegregatora(boolean czyDlaSegregatora) {
		this.czyDlaSegregatora = czyDlaSegregatora;
	}
}
