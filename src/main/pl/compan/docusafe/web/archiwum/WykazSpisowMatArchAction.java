package pl.compan.docusafe.web.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;








import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.SpisDokDoAPLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class WykazSpisowMatArchAction extends EventActionSupport{
	protected static Logger log = LoggerFactory.getLogger(WykazSpisowMatArchAction.class);

	private  final static String NUMER="numer";
	private  final static String DATA_PRZEKAZ="DATA_PRZEK";
	//private  final static String AUTOR="autor";
	private  final static String ILOSC_TECZEK="iloscTeczek";
	private  final static String ILOSC_POZ="iloscPoz";
	private  final static String STATUS_DOKUMENTU="statusDok";
	private  final static String UWAGI="uwagi";
	private  final static String KOMORG="komOrg";

	//wynik
	ArrayList<Map<String, Object>> trueResults;

	//tmp
	ArrayList<Map<String, Object>> results;
	ArrayList<Map<String, Object>> results2;


	//IMPORT
	private List<TableColumn> columns;
	private int offset;
	private static int limit=20;
	private String thisSearchUrl;
	private String numerOd;
	private String numerDo;
	private String utwFrom;
	private String utwTo;
	private List<DSUser> users;
	private String person;
	private Map<String, String> statusy;
	private String status;

	private Pager pager;

	private boolean ascending;

	private String sortField;


	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			thisSearchUrl = "/archives/wykaz-mat-arch.action?doSearch=true"
					+ (numerOd != null ? "&numerOd ="+numerOd  : "") 
					+ (numerOd != null ? "&numerOd ="+numerOd  : "")
					+ (numerDo != null ? "&numerDo ="+numerDo  : "")
					+ (utwFrom != null ? "&utwFrom ="+utwFrom  : "")
					+ (utwTo != null ? "&utwTo ="+utwTo  : "")
					+ (person != null ? "&person ="+person  : "")
					+ (status != null ? "&status ="+status  : "")
					+ "&offset="+ offset;

			boolean ctx;
			try {
				ctx = DSApi.openContextIfNeeded();

				if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE)) {
					users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				} else if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA)
						|| DSApi.context().hasPermission(
								DSPermission.DODAWANIE_PISM_DO_SPRAW_W_DZIALE)) {
					// tylko z dzia�u users
					Arrays.asList(DSApi
							.context()
							.getDSUser()
							.getNeighbours(
									new String[] { DSDivision.TYPE_DIVISION,
											DSDivision.TYPE_POSITION }));
				} else {
					users = new ArrayList<DSUser>(1);
					users.add(DSApi.context().getDSUser());
				}
				statusy=new LinkedHashMap<String,String>(4);
				statusy.put("","");
				statusy.put(SpisDokDoAPLogic.STATUS_PRZYJETY,SpisDokDoAPLogic.STATUS_PRZYJETY);
				statusy.put(SpisDokDoAPLogic.STATUS_PRZEKAZANY_DO_AP,SpisDokDoAPLogic.STATUS_PRZEKAZANY_DO_AP);
				statusy.put(SpisDokDoAPLogic.STATUS_ODRZUCONY,SpisDokDoAPLogic.STATUS_ODRZUCONY);

				columns=new ArrayList<TableColumn>(7);
				columns.add(new TableColumn(NUMER,"Nr spisu",null,null));
				columns.add(new TableColumn(DATA_PRZEKAZ,"Data przekazania akt",null,null));
			//	columns.add(new TableColumn(AUTOR,"Autor spisu",null,null));
				columns.add(new TableColumn(ILOSC_TECZEK,"Liczba teczek",null,null));
				columns.add(new TableColumn(ILOSC_POZ,"Ilosc pozycji na spisie",null,null));
				columns.add(new TableColumn(KOMORG,"Nazwa kom�rki przekazuj�cej akta",null,null));
				columns.add(new TableColumn(UWAGI,"Uwagi",null,null));
				columns.add(new TableColumn(STATUS_DOKUMENTU,"Status dokumentu",null,null));




				PreparedStatement ps=DSApi.context().prepareStatement("select spis.document_id as id,spis.KOM_ORG as kom,spis.numer as numer,spis.status as status,spis.DATA_PRZEK as DATA_PRZEK,liczbaPoz,sum(items.FOLDER_COUNT) as liczbaTeczek from ds_ar_spis_dok_do_ap spis   "
						+ "left join ds_document d on d.id=spis.document_id "
						+ "left join (select count(multi.document_id)as liczbaPoz,multi.DOCUMENT_ID as id  from ds_ar_spis_dok_do_ap_multi multi group by multi.DOCUMENT_ID) teczki on teczki.id=d.ID   "
						+ "left join ds_ar_spis_dok_do_ap_multi multi on multi.DOCUMENT_ID=spis.document_id "
						+ "left join ds_ar_teczki_do_ap teczkiAP on cast(multi.FIELD_VAL as numeric)=teczkiAP.ID "
						+ "left join DS_AR_INVENTORY_ITEMS items on items.FOLDER_OFFICEID=teczkiAP.idteczki2  "
						+ "group by spis.document_id,spis.KOM_ORG,numer,status,data_przek,liczbaPoz "
						+ "order by numer asc");
				//ps.setInt(1,offset);
				//	ps.setInt(2,offset+limit);
				ResultSet rs=ps.executeQuery();

				results = new ArrayList<Map<String, Object>>();
				results2 = new ArrayList<Map<String, Object>>();




				while(rs.next()){
					HashMap<String,Object> mapa=new HashMap<String,Object>();
					mapa.put(NUMER, rs.getInt("numer"));
					//statusy 0-nowy,2 - przekazanedoAP, 3 odrzucony 
					if(rs.getString("status")!=null){
						if(rs.getString("status").equals("0"))mapa.put(STATUS_DOKUMENTU, SpisDokDoAPLogic.STATUS_NOWY_SPIS);
						else if(rs.getString("status").equals("1"))mapa.put(STATUS_DOKUMENTU, SpisDokDoAPLogic.STATUS_PRZYJETY);
						else if(rs.getString("status").equals("2"))mapa.put(STATUS_DOKUMENTU, SpisDokDoAPLogic.STATUS_PRZEKAZANY_DO_AP);
						else if(rs.getString("status").equals("5"))mapa.put(STATUS_DOKUMENTU, SpisDokDoAPLogic.STATUS_ODRZUCONY);
						else mapa.put(STATUS_DOKUMENTU,"nie przypisany? b��d");
					}
					else mapa.put(STATUS_DOKUMENTU,"nie przypisany? b��d");
					SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
					mapa.put(DATA_PRZEKAZ, rs.getString(DATA_PRZEKAZ)!=null? df.format(DateUtils.parseDateAnyFormat(rs.getString(DATA_PRZEKAZ))):null);
				//	mapa.put(AUTOR, DSUser.findByUsername(Document.find(rs.getLong("id")).getAuthor()).asLastnameFirstname() );
					if(rs.getString("liczbaTeczek")!=null&&!rs.getString("liczbaTeczek").equals(""))
						mapa.put(ILOSC_TECZEK, rs.getInt("liczbaTeczek"));
					else mapa.put(ILOSC_TECZEK,"0");
					if(rs.getString("liczbaPoz")!=null&&!rs.getString("liczbaPoz").equals(""))
						mapa.put(ILOSC_POZ, rs.getInt("liczbaPoz"));
					else mapa.put(ILOSC_POZ,"0");
					
					if(AvailabilityManager.isAvailable("PG.parametrization")){
						mapa.put("link", "/office/outgoing/document-archive.action?documentId="+rs.getString("id"));
					}else
					mapa.put("link", "/office/internal/document-archive.action?documentId="+rs.getString("id"));
					
					String uwagi="";
					for(Remark uwaga:OfficeDocument.find(rs.getLong("id")).getRemarks()){
						uwagi+=uwaga.getContent();
					}
					mapa.put(UWAGI, uwagi);

					mapa.put(KOMORG, rs.getString("kom")!=null?DSDivision.findById(rs.getInt("kom")).getName():"---");

					results.add(mapa);
				}

				for(int i=0;i<results.size();i++){
					boolean dodaj=true;
					if(numerOd!=null&&dodaj){
						if((Integer)results.get(i).get(NUMER)<Integer.parseInt(numerOd)){
							dodaj=false;
						}	
					}
					if(numerDo!=null&&dodaj){
						if((Integer)results.get(i).get(NUMER)>Integer.parseInt(numerDo)){
							dodaj=false;
						}	
					}
					if(utwFrom!=null&&dodaj){
						if(results.get(i).get(DATA_PRZEKAZ)!=null){
							if((DateUtils.parseDateAnyFormat(results.get(i).get(DATA_PRZEKAZ).toString())).before(DateUtils.parseDateAnyFormat(utwFrom))){
								dodaj=false;
							}
						}
					}
					if(utwTo!=null&&dodaj){
						if(results.get(i).get(DATA_PRZEKAZ)!=null){
							if((DateUtils.parseDateAnyFormat(results.get(i).get(DATA_PRZEKAZ).toString()).after(DateUtils.parseDateAnyFormat(utwTo)))){
								dodaj=false;
							}
						}
					}
/*					if(person!=null&&dodaj){
						if(!results.get(i).get(AUTOR).equals(DSUser.findByUsername(person).asLastnameFirstname()))
							dodaj=false;
					}*/
					if(status!=null&&dodaj){
						if(!results.get(i).get(STATUS_DOKUMENTU).equals("")){
							if(!results.get(i).get(STATUS_DOKUMENTU).equals(status))
								dodaj=false;
						}
					}

					if(dodaj)results2.add(results.get(i));
				}
				DSApi.closeContextIfNeeded(ctx);
				if(sortField!=null){
					if(sortField.equals("numer"))Collections.sort(results2,new porownajPoNumerze());
					if(sortField.equals("data"))Collections.sort(results2,new porownajPoDacie());
					if(sortField.equals("depart"))Collections.sort(results2,new porownajPoDepart());
				}
				else Collections.sort(results2,new porownajPoDacie());




				if(results2.size()!=0){
					if(offset>results2.size())offset=0;

					if(offset+limit>results2.size()){

						trueResults=new ArrayList<Map<String, Object>>(results2.subList(offset, results2.size()));
					}
					else{
						trueResults=new ArrayList<Map<String, Object>>(results2.subList(offset, offset+limit));
					}
				}

				Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
				{
					public String getLink(int offset) {
						return HttpUtils.makeUrl(
								"/archives/wykaz-mat-arch.action",
								new Object[] { "doSearch","true",
										"numerOd",numerOd,
										"numerDo",numerDo,
										"utwFrom",utwFrom,
										"utwTo",utwTo,
										"person",person,
										"status",status,
										"sortField",sortField,
										"ascending",String.valueOf(ascending),
										"offset",
										String.valueOf(offset) });
					}
				};
				//int liczbaSpisow=Document.findByDocumentKind(SpisDokDoAPLogic.SPIS_DOK_DO_AP).size();
				pager = new Pager(linkVisitor, offset, limit, results2.size(), 10);

			} catch (EdmException e) {
				log.error("",e);
				e.printStackTrace();
			}
			catch (SQLException e) {
				log.error("",e);
				e.printStackTrace();
			} catch (ParseException e) {
				log.error("",e);
				e.printStackTrace();
			}

		}
	}

	public String getSortLink(String field, boolean asc) {
		return thisSearchUrl + "&sortField=" + field + "&ascending=" + asc;
	}

	class porownajPoNumerze implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey(NUMER)&&o2.containsKey(NUMER)){
				if(o1.get(NUMER)!=null&&o2.get(NUMER)!=null){
					if(Integer.parseInt(o1.get(NUMER).toString())>Integer.parseInt(o2.get(NUMER).toString())){
						if(ascending)return -1;
						else return 1;
					}
					else if(Integer.parseInt(o1.get(NUMER).toString())<Integer.parseInt(o2.get(NUMER).toString())) {
						if(ascending)return 1;
						else return -1;
					}
					else return 0;
				}
				if(o1.get(NUMER)!=null){
					if(ascending)return 1;
					else return -1;
				}
				else{
					if(ascending)return -1;
					else return 1;
				}

			}
			return 0;
		}

	}

	class porownajPoDepart implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey(KOMORG)&&o2.containsKey(KOMORG)){
				if(o1.get(KOMORG)!=null&&o2.get(KOMORG)!=null){
					if(o1.get(KOMORG).toString().compareToIgnoreCase(o2.get(KOMORG).toString())>0){
						if(ascending)return -1;
						else return 1;
					}
					else if(o1.get(KOMORG).toString().compareToIgnoreCase(o2.get(KOMORG).toString())<0){
						if(ascending)return 1;
						else return -1;
					}
					else return 0;


				}
				if(o1.get(KOMORG)!=null){
					if(ascending)return -1;
					else return 1;
				}
				else{
					if(ascending)return 1;
					else return -1;
				}


			}
			return 0;
		}

	}
	class porownajPoDacie implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {

				if(o1.containsKey(DATA_PRZEKAZ)&&o2.containsKey(DATA_PRZEKAZ)){
			
				if(o1.get(DATA_PRZEKAZ)!=null&&o2.get(DATA_PRZEKAZ)!=null){
					try {
						if((DateUtils.parseDateAnyFormat(o1.get(DATA_PRZEKAZ).toString()).compareTo(DateUtils.parseDateAnyFormat(o2.get(DATA_PRZEKAZ).toString())))==0)return 0;
						if((DateUtils.parseDateAnyFormat(o1.get(DATA_PRZEKAZ).toString()).before(DateUtils.parseDateAnyFormat(o2.get(DATA_PRZEKAZ).toString())))){
							if(ascending)return 1;
							else return -1;
						}
						else {
							if(ascending)return -1;
							else return 1;
						}
					} catch (ParseException e) {
						try{
							DateUtils.parseDateAnyFormat(o1.get(DATA_PRZEKAZ).toString());
						}
						catch(ParseException e2){
							try{
								DateUtils.parseDateAnyFormat(o2.get(DATA_PRZEKAZ).toString());
							}
							catch(ParseException e3){
								return 0;
							}
							if(ascending)return 1;
							else return -1;
						}
						if(ascending)return -1;
						else return 1;
					}


				}
				if(o1.get(DATA_PRZEKAZ)==null&&o2.get(DATA_PRZEKAZ)==null) return 0;
				if(o1.get(DATA_PRZEKAZ)!=null){
					if(ascending)return 1;
					else return -1;
				}
				else{
					if(ascending)return -1;
					else return 1;
				}

			}
			if(o1.containsKey(DATA_PRZEKAZ)){
				if(ascending)return 1;
				else return -1;
			}
			else if (o2.containsKey(DATA_PRZEKAZ)) {
				if(ascending)return -1;
				else return 1;
			}
			else return 0;
		}
		

		

	}

	public String prettyPrint(Object object)
	{
		if (object == null) return "";
		if (object instanceof Date)
		{
			String date = DateUtils.formatJsDateTime((Date) object);

			if (date.substring(date.indexOf(' ')+1).equals("00:00"))
				return DateUtils.formatJsDate((Date) object);
			else
				return date;
		}
		else
		{
			return object.toString();
		}
	}


	public ArrayList<Map<String, Object>> getResults() {
		return results;
	}


	public void setResults(ArrayList<Map<String, Object>> results) {
		this.results = results;
	}


	public Pager getPager() {
		return pager;
	}


	public void setPager(Pager pager) {
		this.pager = pager;
	}




	public List getColumns() {
		return columns;
	}



	public int getOffset() {
		return offset;
	}


	public void setOffset(int offset) {
		this.offset = offset;
	}


	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}


	public static int getLimit() {
		return limit;
	}


	public static void setLimit(int limit) {
		WykazSpisowMatArchAction.limit = limit;
	}


	public String getThisSearchUrl() {
		return thisSearchUrl;
	}


	public void setThisSearchUrl(String thisSearchUrl) {
		this.thisSearchUrl = thisSearchUrl;
	}


	public String getNumerOd() {
		return numerOd;
	}


	public void setNumerOd(String numerOd) {
		this.numerOd = numerOd;
	}


	public String getNumerDo() {
		return numerDo;
	}


	public void setNumerDo(String numerDo) {
		this.numerDo = numerDo;
	}


	public ArrayList<Map<String, Object>> getResults2() {
		return results2;
	}


	public void setResults2(ArrayList<Map<String, Object>> results2) {
		this.results2 = results2;
	}


	public ArrayList<Map<String, Object>> getTrueResults() {
		return trueResults;
	}


	public void setTrueResults(ArrayList<Map<String, Object>> trueResults) {
		this.trueResults = trueResults;
	}


	public String getUtwFrom() {
		return utwFrom;
	}


	public void setUtwFrom(String utwFrom) {
		this.utwFrom = utwFrom;
	}


	public String getUtwTo() {
		return utwTo;
	}


	public void setUtwTo(String utwTo) {
		this.utwTo = utwTo;
	}


	public List<DSUser> getUsers() {
		return users;
	}


	public void setUsers(List<DSUser> users) {
		this.users = users;
	}


	public String getPerson() {
		return person;
	}


	public void setPerson(String person) {
		this.person = person;
	}


	public Map<String, String> getStatusy() {
		return statusy;
	}


	public void setStatusy(Map<String, String> statusy) {
		this.statusy = statusy;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public boolean isAscending() {
		return ascending;
	}


	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}


	public String getSortField() {
		return sortField;
	}


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

}
