package pl.compan.docusafe.web.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.json.JSONArray;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentHelper;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.document.DocumentFinder;
import pl.compan.docusafe.core.base.document.DocumentFinderProvider;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindProvider;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeCaseFinderProvider;
import pl.compan.docusafe.core.office.container.ContainerFinder;
import pl.compan.docusafe.core.office.container.OfficeFolderFinderProvider;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.archive.repository.Constants;
import pl.compan.docusafe.web.common.BeanBackedMap;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
import std.lambda;

public class FindArchiveAction extends EventActionSupport {
	
	public static final StringManager sm = StringManager.getManager(Constants.Package);
	private Logger log = LoggerFactory.getLogger(FindArchiveAction.class);
			
	public static final String DEFAULT_COLUMNS; // = "[\"officeNumber\", \"ctime\", \"sender\", \"description\", \"status\"]";
	    static
	    {
	        DEFAULT_COLUMNS = new JSONArray(Arrays.asList(new String[] {
	            "document_id", "document_title", "document_ctime","officeNumber","document_author","archivedDate","attachments"
	        })).toString();
	    }
	    
	    // mapowanie nazw kolumn na w�asno�ci, kt�rych nazw mo�na u�y� do sortowania listy
	    // wynik�w (w klasie QueryForm)
	    private static final Map<String, String> sortProperties = new HashMap<String, String>();
	    static
	    {
	        sortProperties.put("document_id", "id");
	        sortProperties.put("document_title", "title");
	        sortProperties.put("document_ctime", "ctime");
	        sortProperties.put("document_mtime", "mtime");
	    }
	private static final int LIMIT = 20;
	private int offset;

	private String thisSearchUrl;

	// @EXPORT/@IMPORT
	private boolean popup;

	// @EXPORT
	private List<DSUser> users;
	//w przypadku spraw i teczek
	private SearchResults<Map> results;
	//w przypadku dokument�w lub officeid jesli szukal po id
	ArrayList<Map<String, Object>> results2;
	ArrayList<Map<String, Object>> trueResults;
	private Pager pager;
	// private Long id;

	private Map<Integer, String> records = new LinkedHashMap<Integer, String>();
    private List columns;
	// @IMPORT
	private String officeId;
	private String archDateFrom;
	private String archDateTo;
	private String cdateFrom;
	private String cdateTo;
	private String sortField;
	private boolean ascending;
	private short kind;
	private Map<Short, String> kinds;
	private String docKind;
	private Map<String, String> docKinds;
	private String achome;
	private Map<String, String> achomeList;
	private String symbol;
	private Map<String, String> symbolList;
	private String[] person;
	private String[] assignedUser;
	private String title;
	private Integer year;
	private Integer seqNum;
	private Map<Short,String> archiveStatuses;
	private String archiveStatus;

	// private int mode;
	private Integer rwaId;

    /**
     * Pole do wyszukiwania pe�notekstowego.
     */
    private String content;

    /**
     * Czy bierzemy pod uwag� kolejno�� s��w w polu content.
     */
    private Boolean contentWordOrder;

	private static final String EV_SEARCH = "search";

	@Override
	protected void setup() {
		class SetResult implements ActionListener {
			public void actionPerformed(ActionEvent event) {
				event.setResult(popup ? "popup" : SUCCESS);

			}
		}

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(new SetResult())
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").append(OpenHibernateSession.INSTANCE)
				.append(new SetResult())
				.append(new ValidateSearch()).append(EV_SEARCH, new Search())
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doShowPzypisane")
				.append(OpenHibernateSession.INSTANCE).append(new SetResult())
				.append(new ValidateSearch())
				.
				// append(EV_PRZYP, new showPrzypisane()).
				append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

	}

    public Boolean getContentWordOrder() {
        return contentWordOrder;
    }

    public void setContentWordOrder(Boolean contentWordOrder) {
        this.contentWordOrder = contentWordOrder;
    }


    private class FillForm implements ActionListener {
				public void actionPerformed(ActionEvent event) {
			boolean userAll = false;

			try {
				ArrayList docKindList=new ArrayList(DocumentKindProvider.get().visible().list());
				docKinds = new LinkedHashMap<String, String>();
				docKinds.put("", "");
				for(Iterator<DocumentKind> it=DocumentKindProvider.get().visible().list().iterator();it.hasNext();){
					DocumentKind dc=it.next();
					docKinds.put(dc.getCn(), dc.getName());
				}
				
				PreparedStatement ps;
				ps = DSApi
						.context()
						.prepareStatement(
								"select distinct achome from v_dso_rwaa where achome is not null");
				ResultSet rs = ps.executeQuery();
				achomeList = new LinkedHashMap<String, String>();
				achomeList.put("", "");
				String tmp = "";
				while (rs.next()) {
					tmp = rs.getString("achome");
					achomeList.put(tmp, tmp);
				}
				ps = DSApi
						.context()
						.prepareStatement(
								"select distinct code from v_dso_rwaa ");
				rs = ps.executeQuery();
				symbolList = new LinkedHashMap<String, String>();
				symbolList.put("", "");
				tmp = "";
				while (rs.next()) {
					tmp = rs.getString("code");
					symbolList.put(tmp, tmp);
				}

				if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE)) {
					users = DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
				} else if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA)
						|| DSApi.context().hasPermission(
								DSPermission.DODAWANIE_PISM_DO_SPRAW_W_DZIALE)) {
					// tylko z dzia�u users
					Arrays.asList(DSApi
							.context()
							.getDSUser()
							.getNeighbours(
									new String[] { DSDivision.TYPE_DIVISION,
											DSDivision.TYPE_POSITION }));
				} else {
					users = new ArrayList<DSUser>(1);
					users.add(DSApi.context().getDSUser());
				}

				// DSUser user = DSApi.context().getDSUser();

				List<ArchiveStatus> ls=Arrays.asList(Container.ArchiveStatus.values());
				archiveStatuses=new HashMap<Short,String>();
				for(int i=0;i<ls.size();i++){
					archiveStatuses.put((short)i, ls.get(i).toString());
				}
				
				
			} catch (EdmException e) {
				addActionError(e.getMessage());
			} catch (SQLException e) {
				log.error("[FillForm] error", e);
			}

		}
	}

	private class ValidateSearch implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			Date dCdateFrom = DateUtils.nullSafeParseJsDate(archDateFrom);
			Date dCdateTo = DateUtils.nullSafeParseJsDate(archDateTo);
			if (dCdateFrom != null && dCdateTo != null
					&& dCdateFrom.after(dCdateTo))
				addActionError("Pocz�tkowa data utworzenia jest p�niejsza "
						+ "od daty ko�cowej");
			dCdateFrom = DateUtils.nullSafeParseJsDate(cdateFrom);
			dCdateTo = DateUtils.nullSafeParseJsDate(cdateTo);
			if (dCdateFrom != null && dCdateTo != null
					&& dCdateFrom.after(dCdateTo))
				addActionError("Pocz�tkowa data utworzenia jest p�niejsza "
						+ "od daty ko�cowej");

			if (hasActionErrors())
				event.skip(EV_SEARCH);
		}
	}

	private class Search implements ActionListener {
		public void actionPerformed(final ActionEvent event) {

			thisSearchUrl = "/archives/find-archive.action?doSearch=true"
					+ "&offset="
					+ offset
					+ "&kind="
					+ kind
					+ (officeId != null ? "&officeId=" + officeId : "")
					+ (title != null ? "&title=" + title : "")
					+ (rwaId != null ? "&rwaId=" + rwaId : "")
					+ (year != null ? "&year=" + year : "")
					+ (archDateFrom != null ? "&archDateFrom=" + archDateFrom
							: "")
					+ (archDateTo != null ? "&archDateTo=" + archDateTo : "")
					+ (cdateFrom != null ? "&cdateFrom=" + cdateFrom : "")
					+ (cdateTo != null ? "&cdateTo=" + cdateTo : "")
					+ (person != null ? "&person=" + person[0] : "")
					+ (assignedUser != null ? "&assignedUser="
							+ assignedUser[0] : "")
					+ (seqNum != null ? "&seqNum=" + seqNum : "")
					+(docKind!=null ? "&docKind="+docKind : "");
			try {

				if (kind == (short) 1) {
					// teczki
					OfficeFolder.Query query = OfficeFolder
							.query(offset, LIMIT);
					// query.set(true);

					//query.setArchived(true);
					/*if(archiveStatus!=null){
						if(Integer.parseInt(archiveStatus)!=0)
							query.setArchiveStatus(Integer.parseInt(archiveStatus));
						else{
							query.setArchiveStatusIsArchived();
						}
					}
					else query.setArchiveStatusIsArchived();*/
					query.setArchiveStatus(Container.ArchiveStatus.Archived.ordinal());
					query.setOfficeId(TextUtils.trimmedStringOrNull(officeId));
					query.safeSortBy(sortField, "officeId", ascending);
					query.setArchDateFrom(DateUtils
							.nullSafeParseJsDate(archDateFrom));
					query.setArchDateTo(DateUtils
							.nullSafeParseJsDate(archDateTo));
					query.setAuthor(person);
					query.setName(title);
					query.setRwaId(rwaId);
					query.setYear(year);
					query.setCdateFrom(DateUtils.nullSafeParseJsDate(cdateFrom));
					query.setCdateTo(DateUtils.nullSafeParseJsDate(cdateTo));
					query.setClerk(assignedUser);
                    query.setContent(content);
                    query.setContentWordOrder(contentWordOrder == null ? false : true);
					lambda<OfficeFolder, Map> mapper = new lambda<OfficeFolder, Map>() {
						public Map act(OfficeFolder officeFolder) {
							BeanBackedMap result = new BeanBackedMap(
									officeFolder, "id", "officeId", "name",
									 "year", "archivedDate",
									"storagePlace");
							try {
								result.put("clerk", DSUser
										.safeToFirstnameLastname(officeFolder
												.getClerk()));
								result.put("author", DSUser
										.safeToFirstnameLastname(officeFolder
												.getAuthor()));
							} catch (EdmException e) {
								event.getLog().warn(e.getMessage(), e);
							}
							return result;
						}
					};

                    ContainerFinder officeFolderFinder;
                    try {
                        officeFolderFinder = OfficeFolderFinderProvider.instance().provide(query, mapper, Map.class);
                    } catch (Exception e) {
                        throw new EdmException("B��d podczas wyszukiwania, skontaktuj si� z administratorem", e);
                    }

                    results = officeFolderFinder.find();


						// z wynikow z parametrami z kontenera trzeba odrzucic
						// te, ktore nie spelniaja pozostalych param, jak
						// chociazby achome
						checkRestParameters();
					
					if (results.totalCount() == 0)
						addActionError("Nie znaleziono �adnych teczek");

					Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset) {
							return HttpUtils.makeUrl(
									"/archives/find-archive.action",
									new Object[] { "doSearch", "true",
											"officeId", officeId, "kind", kind,
											"name", title, "archDateFrom",
											archDateFrom, "archDateTo",
											archDateTo, "cdateFrom", cdateFrom,
											"cdateTo", cdateTo, "sortField",
											sortField, "person", person,
											"assignedUser", assignedUser,
											"rwaId", rwaId, "year", year,
											"ascending",
											String.valueOf(ascending), "popup",
											popup ? "true" : "false", "offset",
											String.valueOf(offset) });
						}
					};

					pager = new Pager(linkVisitor, offset, LIMIT,
							results.totalCount(), 10);

				} else if (kind == (short) 2) {
					// sprawy

					OfficeCase.Query query = OfficeCase.query(offset, LIMIT);
					/*if(archiveStatus!=null){
						if(Integer.parseInt(archiveStatus)!=0)
							query.setArchiveStatus(Integer.parseInt(archiveStatus));
						else{
							query.setArchiveStatusIsArchived();
						}
					}
					else query.setArchiveStatusIsArchived();*/
					
					query.setArchiveStatus(Container.ArchiveStatus.Archived.ordinal());
					query.setOfficeId(TextUtils.trimmedStringOrNull(officeId));
					query.safeSortBy(sortField, "officeId", ascending);
					query.setAuthor(person);
					query.setArchDateFrom(DateUtils
							.nullSafeParseJsDate(archDateFrom));
					query.setArchDateTo(DateUtils
							.nullSafeParseJsDate(archDateTo));
					query.setTitle(title);
					query.setRwaId(rwaId);
					query.setYear(year);
					query.setSeqNum(seqNum);
					query.setCdateFrom(DateUtils.nullSafeParseJsDate(cdateFrom));
					query.setCdateTo(DateUtils.nullSafeParseJsDate(cdateTo));
					query.setClerk(assignedUser);
                    query.setContent(content);
                    query.setContentWordOrder(contentWordOrder == null ? false : true);
					
					lambda<OfficeCase, Map> mapper = new lambda<OfficeCase, Map>() {
						public Map act(OfficeCase officeCase) {
							BeanBackedMap result = new BeanBackedMap(
									officeCase, "id", "finishDate", "openDate",
									"officeId", "daysToFinish",
									"description",  "title",
									"archivedDate");
							try {
								DSApi.initializeProxy(officeCase.getStatus());
								result.put("clerk", DSUser
										.safeToFirstnameLastname(officeCase
												.getClerk()));
							} catch (EdmException e) {
								event.getLog().warn(e.getMessage(), e);
							}
							return result;
						}
					};

                    ContainerFinder officeCaseFinder;
                    try {
                        officeCaseFinder = OfficeCaseFinderProvider.instance().provide(query, mapper, Map.class);
                    } catch (Exception e) {
                        throw new EdmException("B��d podczas wyszukiwania, skontaktuj si� z administratorem", e);
                    }

                    results = officeCaseFinder.find();
					// z wynikow z parametrami z kontenera trzeba odrzucic
					// te, ktore nie spelniaja pozostalych param, jak
					// chociazby achome
					checkRestParameters();
					
					if (results.totalCount() == 0)
						addActionError("Nie znaleziono �adnych spraw");

					Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
						public String getLink(int offset) {
							return HttpUtils.makeUrl(
									"/archives/find-archive.action",
									new Object[] { "doSearch", "true",
											"officeId", officeId, "kind", kind,
											"title", title, "archDateFrom",
											archDateFrom, "archDateTo",
											archDateTo, "cdateFrom", cdateFrom,
											"cdateTo", cdateTo, "person",
											person, "assignedUser",
											assignedUser, "sortField",
											sortField, "rwaId", rwaId, "year",
											year, "seqNum", seqNum,
											"ascending",
											String.valueOf(ascending), "popup",
											popup ? "true" : "false", "offset",
											String.valueOf(offset) });
						}
					};

					pager = new Pager(linkVisitor, offset, LIMIT,
							results.totalCount(), 10);

				} else if (kind == (short) 3) {
					// dokumenty
					
					QueryForm queryForm = new QueryForm(0, 1);
					//najpierw musze znac liczbe wszystkich elementow,
					// zawezanie bedzie takze po tym w jakiej sprawie sie znajduja, wiec 
					// zwykle uzycie queryform nie wystarcza

                    DocumentFinder finder;
                    int allDocumentsCount;
                    try {
                        finder = DocumentFinderProvider.instance().provide(queryForm);
                        allDocumentsCount = finder.find().totalCount();
                    } catch (Exception e) {
                        throw new EdmException("Nieznany b��d", e);
                    }

					queryForm = new QueryForm(0, allDocumentsCount);
		            if (title != null)
		                queryForm.addProperty("title", title);
		            if (docKind!= null &&!docKind.equals(""))
		                queryForm.addProperty("documentKindIds", DocumentKind.findByCn(docKind).getId());
		            if (cdateFrom != null)
		                queryForm.addProperty("startCtime", DateUtils.nullSafeParseJsDate(cdateFrom));
		            if (cdateTo != null)
		                queryForm.addProperty("endCtime", DateUtils.nullSafeParseJsDate(cdateTo));
		            
		            if (person != null)
		            	queryForm.addProperty("author", person);
		            if (officeId != null)
		                queryForm.addProperty("id", officeId);
                    if (content != null)
                        queryForm.addProperty("content", content);
		            if(archDateFrom!=null)queryForm.addProperty("archDateFrom",DateUtils
							.nullSafeParseJsDate(archDateFrom));
					if(archDateTo!=null)queryForm.addProperty("archDateTo",DateUtils
							.nullSafeParseJsDate(archDateTo));
					//queryForm.addPaddPropertyroperty("isArchived", "1");
					queryForm.addProperty("isArchived",true);
                    queryForm.addProperty("contentWordOrder", contentWordOrder == null ? false : contentWordOrder);
					
		            
		            if (sortField != null)
		            {
		                if (ascending) queryForm.addOrderAsc(sortField);
		                else queryForm.addOrderDesc(sortField);
		            }
		            else
		            {
		                queryForm.addOrderAsc("title");
		            }
		           
	                columns = getColumns(DSApi.context().userPreferences().node("search-documents").get("columns", DEFAULT_COLUMNS));
	                List columnNames = getColumnNames(DSApi.context().userPreferences().node("search-documents").get("columns", DEFAULT_COLUMNS));

                    try {
                        finder = DocumentFinderProvider.instance().provide(queryForm);
                        results = finder.find();
                    } catch (Exception e) {
                        throw new EdmException("Nieznany b��d", e);
                    }

                    if (results.totalCount() == 0)
						addActionError("Nie znaleziono �adnych dokument�w");
                    
                    int effectiveNumFound = 0;

	                if(results.totalCount() > 0)
	                {
	                	results2 = new ArrayList<Map<String, Object>>();

	                    while (results.hasNext())
	                    {		                    	
	                        Document document = (Document) results.next();
	                    	
	                        if(!checkAChome(document.getId())){
	                    		continue;
	                    	}
	                        
	                        if(document instanceof InOfficeDocument){
                                Long CaseId = ((InOfficeDocument) document).getContainingCaseId();

                                if(CaseId != null) {
                                    OfficeCase oc=OfficeCase.find(CaseId);
                                    if(oc.getArchiveStatus()!=ArchiveStatus.Archived)continue;
                                }
	                        }
	                        if(document instanceof OutOfficeDocument){
	                        	  Long CaseId = ((OutOfficeDocument) document).getContainingCaseId();

                                if(CaseId != null) {
                                    OfficeCase oc=OfficeCase.find(CaseId);
                                    if(oc.getArchiveStatus()!=ArchiveStatus.Archived)continue;
                                }
	                        }
	                        
	                        Map<String, Object> bean = new HashMap<String, Object>();
	                        /*if (document.getDocumentKind() != null)
	                            bean.put("link", "/repository/edit-dockind-document.action?id="+document.getId());
	                        else*/
	                        if(DocumentType.INTERNAL.equals(document.getType()))
	                        {
	                        	if(AvailabilityManager.isAvailable("PG.parametrization")){
	    							bean.put("link", "/office/outgoing/document-archive.action?documentId="+document.getId());
	    						}else
	                        	bean.put("link", "/office/internal/document-archive.action?documentId="+document.getId());
	                        }
	                        else if(DocumentType.INCOMING.equals(document.getType()))
	                        	bean.put("link", "/office/incoming/document-archive.action?documentId="+document.getId());
	                        else if(DocumentType.OUTGOING.equals(document.getType()))
	                        	bean.put("link", "/office/outgoing/document-archive.action?documentId="+document.getId());
	                        else
	                        	bean.put("link", "/repository/edit-dockind-document.action?id="+document.getId());
	                        bean.put("document_id", document.getId());
	                        bean.put("document_title", document.getTitle());
	                        bean.put("document_ctime", document.getCtime());
	                        bean.put("document_author", DSUser.safeToFirstnameLastname(document.getAuthor()));
	                        bean.put("document_remark",document.getLastRemark());
	                        bean.put("canReadAttachments", Boolean.valueOf(document.canReadAttachments()));
	                        bean.put("archivedDate", document.getArchivedDate());

                            // full text search attachments highlighting
                            bean.put("attachments", document.getAbstractDescription());

	                        if(document instanceof InOfficeDocument)bean.put("officeNumber",((InOfficeDocument)document).getOfficeNumber());
	                        results2.add(bean);

                            effectiveNumFound++;
	                    }

                        if(effectiveNumFound == 0) {
                            addActionError(sm.getString("NieZnalezionoDokumentow"));
                        } else {
                            if(offset+LIMIT>results2.size()) {
                                trueResults=new ArrayList(results2.subList(offset, results2.size()));
                            } else {
                                trueResults=new ArrayList(results2.subList(offset, offset+LIMIT));
                            }

                            Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor()
                            {
                                public String getLink(int offset) {
                                    return HttpUtils.makeUrl(
                                            "/archives/find-archive.action",
                                            new Object[] { "doSearch", "true",
                                                    "officeId", officeId, "kind", kind,
                                                    "title", title, "archDateFrom",
                                                    archDateFrom, "archDateTo",
                                                    archDateTo, "cdateFrom", cdateFrom,
                                                    "cdateTo", cdateTo, "person",
                                                    person,  "sortField",
                                                    sortField, "rwaId", rwaId,
                                                    "seqNum", seqNum,
                                                    "ascending",
                                                    String.valueOf(ascending), "popup",
                                                    popup ? "true" : "false", "offset",
                                                    String.valueOf(offset) });
                                }
                            };
                            pager = new Pager(linkVisitor, offset, LIMIT, results2.size(), 10);
                        }
	                }
		            
				}
			} catch (EdmException e) {
				log.error("[Search] error", e);
				addActionError(e.getMessage());
			}

		}

		private void checkRestParameters() {
			try {
				if (achome != null &&!achome.equals("") ) {
					Map[] resultList=results.results();
					ArrayList resultsList=new ArrayList(Arrays.asList(resultList));
					for (int i=0;i<resultsList.size();i++) {						
						Map m = resultList[i];
						Long id = (Long) m.get("id");

						PreparedStatement ps = DSApi
								.context()
								.prepareStatement(
										"select b.achome from dso_container a"
												+ " left join v_dso_rwaa b on b.ID=a.RWA_ID "
												+ " where a.id=?");
						ps.setLong(1, id);
						ResultSet rs = ps.executeQuery();
						rs.next();
						String tmp=rs.getString("achome");
						if (!rs.getString("achome").equals(achome)){
							resultsList.remove(i);
							i--;
						}	
					}
					results=new SearchResultsAdapter<Map>(resultsList,0,resultsList.size() , Map.class);
					
				}
				if (symbol != null &&!symbol.equals("") ) {
					Map[] resultList=results.results();
					ArrayList resultsList=new ArrayList(Arrays.asList(resultList));
					for (int i=0;i<resultsList.size();i++) {						
						Map m = (Map)resultsList.get(i);
						Long id = (Long) m.get("id");

						PreparedStatement ps = DSApi
								.context()
								.prepareStatement(
										"select b.code from dso_container a"
												+ " left join v_dso_rwaa b on b.ID=a.RWA_ID "
												+ " where a.id=?");
						ps.setLong(1, id);
						ResultSet rs = ps.executeQuery();
						rs.next();
						String tmp=rs.getString("code");
						if (!rs.getString("code").equals(symbol)){
							resultsList.remove(i);
							i--;
						}	
					}
					results=new SearchResultsAdapter<Map>(resultsList,0,resultsList.size() , Map.class);
					
				}
				

			} catch (SQLException e) {
				log.debug("[Search.checkRestParameters] error",e);
			} catch (EdmException e) {
				log.debug("[Search.checkRestParameters] error",e);
			}

		}

		private boolean checkAChome(Long documentId){
			if(achome == null)
				return true;
			
			boolean result = false;
			
			try{
				Query query = DSApi.context().session().createQuery("SELECT d FROM OfficeCase c, Document d WHERE c.rwa.acHome=:achome AND d.containingCaseId=c.id AND d.id=:documentId");
				query.setParameter("achome", achome);
				query.setParameter("documentId", documentId);
				
				result = !query.list().isEmpty();
			}
			catch(EdmException e){}
			
			return result;
		}
	}

	/**
	 * Funkcja wywo�ywana ze strony JSP podczas wypisywania wynik�w
	 * wyszukiwania. Funkcja jest wywo�ywana na tej samej instancji
	 * ActionSupport, kt�ra wykonywa�a wyszukiwanie.
	 */
	public String getFolderLink(Map folderBean) {
		return "/archives/edit-portfolio-archive.action?id=" + folderBean.get("id");
	}
    public String getCaseLink(Map caseBean)
    {   
        return "edit-case.do?id=" + (Long) caseBean.get("id"); 
    }
	/**
	 * Funkcja wywo�ywana ze strony JSP.
	 */
	public String getSortLink(String field, boolean asc) {
		return thisSearchUrl + "&sortField=" + field + "&ascending=" + asc;
	}
	private List getColumnNames(String columnsString)
    {
        List<String> cols = new ArrayList<String>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                cols.add(property);
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    cols.add(property);
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }
        return cols;
    }

    private List getColumns(String columnsString)
    {
        List<TableColumn> cols = new ArrayList<TableColumn>(10);
        try
        {
            JSONArray array = new JSONArray(columnsString);
            for (int i=0; i < array.length(); i++)
            {
                String property = array.getString(i);
                String desc = sm.getString("search.documents.column."+property);
                String propDesc = (String) sortProperties.get(property);
                String propAsc = (String) sortProperties.get(property);
                cols.add(new TableColumn(array.getString(i), desc,
                    propDesc != null ? getSortLink(propDesc, false) : null,
                    propAsc != null ? getSortLink(propAsc, true) : null,""));
            }
        }
        catch (ParseException e)
        {
            try
            {
                JSONArray array = new JSONArray(DEFAULT_COLUMNS);
                for (int i=0; i < array.length(); i++)
                {
                    String property = array.getString(i);
                    String desc = sm.getString("search.documents.column."+property);
                    String propDesc = (String) sortProperties.get(property);
                    String propAsc = (String) sortProperties.get(property);
                    cols.add(new TableColumn(array.getString(i), desc,
                        propDesc != null ? getSortLink(propDesc, false) : null,
                        propAsc != null ? getSortLink(propAsc, true) : null,""));
                }
            }
            catch (ParseException pe)
            {
                throw new RuntimeException(pe.getMessage(), e);
            }
        }

        return cols;
    }
    public String prettyPrint(Object object)
    {
        if (object == null) return "";
        if (object instanceof Date)
        {
            String date = DateUtils.formatJsDateTime((Date) object);
            
            if (date.substring(date.indexOf(' ')+1).equals("00:00"))
                return DateUtils.formatJsDate((Date) object);
            else
                return date;
        }
        else
        {
            return object.toString();
        }
    }

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public boolean isAscending() {
		return ascending;
	}

	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

	public String getArchDateFrom() {
		return archDateFrom;
	}

	public void setArchDateFrom(String archDateFrom) {
		this.archDateFrom = archDateFrom;
	}

	public String getArchDateTo() {
		return archDateTo;
	}

	public void setArchDateTo(String archDateTo) {
		this.archDateTo = archDateTo;
	}

	public Integer getRwaId() {
		return rwaId;
	}

	public void setRwaId(Integer rwaId) {
		this.rwaId = rwaId;
	}

	public List<DSUser> getUsers() {
		return users;
	}

	public void setUsers(List<DSUser> users) {
		this.users = users;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}


	public Map<Integer, String> getRecords() {
		return records;
	}

	public void setRecords(Map<Integer, String> records) {
		this.records = records;
	}

	public SearchResults<Map> getResults() {
		return results;
	}

	public void setResults(SearchResults<Map> results) {
		this.results = results;
	}

	public Map<Short, String> getKinds() {
		kinds = new LinkedHashMap<Short, String>(3);
		kinds.put((short) 1, "Teczka");
		kinds.put((short) 2, "Sprawa");
		kinds.put((short) 3, "Dokument");
		return kinds;
	}

	public void setKinds(Map<Short, String> kinds) {
		this.kinds = kinds;
	}

	public short getKind() {
		return kind;
	}

	public void setKind(short kind) {
		this.kind = kind;
	}

	public String[] getPerson() {
		return person;
	}

	public void setPerson(String[] person) {
		this.person = person;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getCdateFrom() {
		return cdateFrom;
	}

	public void setCdateFrom(String cdateFrom) {
		this.cdateFrom = cdateFrom;
	}

	public String getCdateTo() {
		return cdateTo;
	}

	public void setCdateTo(String cdateTo) {
		this.cdateTo = cdateTo;
	}

	public String[] getAssignedUser() {
		return assignedUser;
	}

	public void setAssignedUser(String[] assignedUser) {
		this.assignedUser = assignedUser;
	}

	public Integer getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	public String getAchome() {
		return achome;
	}

	public void setAchome(String achome) {
		this.achome = achome;
	}

	public Map<String, String> getAchomeList() {

		return achomeList;
	}

	public void setAchomeList(Map<String, String> achomeList) {
		this.achomeList = achomeList;
	}

	public Map<String, String> getSymbolList() {
		return symbolList;
	}

	public void setSymbolList(Map<String, String> symbolList) {
		this.symbolList = symbolList;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getDocKind() {
		return docKind;
	}

	public void setDocKind(String docKind) {
		this.docKind = docKind;
	}

	public Map<String, String> getDocKinds() {
		return docKinds;
	}

	public void setDocKinds(Map<String, String> docKinds) {
		this.docKinds = docKinds;
	}

	public ArrayList<Map<String, Object>> getResults2() {
		return results2;
	}

	public void setResults2(ArrayList<Map<String, Object>> results2) {
		this.results2 = results2;
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
	}

	public boolean isPopup() {
		return popup;
	}

	public void setPopup(boolean popup) {
		this.popup = popup;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public static int getLimit() {
		return LIMIT;
	}

	public Map<Short, String> getArchiveStatuses() {
		return archiveStatuses;
	}

	public void setArchiveStatuses(Map<Short, String> archiveStatuses) {
		this.archiveStatuses = archiveStatuses;
	}

	public String getArchiveStatus() {
		return archiveStatus;
	}

	public void setArchiveStatus(String archiveStatus) {
		this.archiveStatus = archiveStatus;
	}
	public ArrayList<Map<String, Object>> getTrueResults() {
		return trueResults;
	}
	public void setTrueResults(ArrayList<Map<String, Object>> trueResults) {
		this.trueResults = trueResults;
	}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
