package pl.compan.docusafe.web.archiwum;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class CaseArchiver extends EventActionSupport{

	private static final Logger log = LoggerFactory.getLogger(CaseArchiver.class);
	
	private List<DSDivision> div;
	private String division;
	protected void setup()
	{
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
			append(OpenHibernateSession.INSTANCE).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doManualCreate").
			append(OpenHibernateSession.INSTANCE).
			append(new ManualCreate()).
			append(fillForm).
			appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	private class FillForm implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				DSUser user = DSUser.findById(DSApi.context().getDSUser().getId());
				div = Arrays.asList(user.getDivisionsWithoutGroupPosition());
				
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}
	private class ManualCreate implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				// ustaw date: rok-2, styczen 01
				Calendar calendar= Calendar.getInstance();
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 2);
				calendar.set(Calendar.MONTH, /* 0-based */0);
				calendar.set(Calendar.DAY_OF_MONTH, /* 1-based */1);
				calendar.set(Calendar.HOUR_OF_DAY, /* 0-based */0);
				calendar.set(Calendar.MINUTE, /* 0-based */0);
				Date finishDate= calendar.getTime();
				NewCaseArchiverScheduled.generujWykazy(finishDate, division);
			} catch (Exception e) {
				addActionError(e.getMessage());
				log.error(e.getMessage(), e);
			}
		}
	}
	
	public List<DSDivision> getDiv() {
		return div;
	}
	
	public void setDiv(List<DSDivision> div) {
		this.div=div;
	}

	public void setDivision(String division){
		this.division=division;
	}
	
}
