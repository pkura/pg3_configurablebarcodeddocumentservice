package pl.compan.docusafe.web.archiwum;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;










import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class WykazUdostepnionychAction extends EventActionSupport{
	protected static Logger log = LoggerFactory.getLogger(WykazUdostepnionychAction.class);

	private static final String UDOSTEPNIANIE_TABELA_MULTI = "DS_AR_WNIOSEK_UDOSTEP_MULTI";
	private static final String UDOSTEPNIANIE_TABELA = "DS_AR_WNIOSEK_UDOSTEP";
	private static final String UDOSTEPNIANIE_TABELA_NIEZDIGITALIZOWANE="DS_AR_WNIOSEK_UDOSTEP_NIEZDIGITALIZOWANE";
	private static final String UDOST_MULTI_PISMO_NIEZDYGITALIZOWANE="LISTA_NIEZDIGITALIZOWANE";
	private static final String WYCOFANIE_TABELA="DS_AR_WYCOFANIE";
	private static final String TABELA_PERSON = "DSO_PERSON";
	public static final Object STATUS_UDOSTEPNIONE =UdostepnienieLogic.STATUS_ID_POWIADOMIENIE_O_UDOSTEPNIENIU ;
	public static final Object STATUS_UDOSTEPNIONE1 =UdostepnienieLogic.STATUS_ID_POTWIERDZENIE_ODBIORU;

	// @IMPORT
	private String lpOd;
	private String lpDo;
	private String zwDateFrom;
	private String zwDateTo;
	private String wypDateFrom;
	private String wypDateTo;
	private String person;
	private List<TableColumn> columns;
	private int offset;
	private static int limit=20;
	private List<DSUser> users;
	private String rodzajPisma;
	private Map<String, String> rodzajePism;
	



	// @EXPORT
	private Pager pager;
	ArrayList<Map<String, Object>> results;
	ArrayList<Map<String, Object>> trueResults;
	// @IMPORT/EXPORT
	private String thisSearchUrl;
	private boolean ascending;

	private String sortField;
	@Override
	protected void setup() {
		FillForm fillForm = new FillForm();

		registerListener(DEFAULT_ACTION).
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		appendFinally(CloseHibernateSession.INSTANCE);

	}


	private class FillForm implements ActionListener
	{	



		public void actionPerformed(ActionEvent event)
		{


			boolean ctx;
			try {
				ctx = DSApi.openContextIfNeeded();


				if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_WSZEDZIE)) {
					users = DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
				} else if (DSApi.context().hasPermission(
						DSPermission.SZUK_UZYTK_DOSTEP_KOMORKA)
						|| DSApi.context().hasPermission(
								DSPermission.DODAWANIE_PISM_DO_SPRAW_W_DZIALE)) {
					// tylko z dzia�u users
					Arrays.asList(DSApi
							.context()
							.getDSUser()
							.getNeighbours(
									new String[] { DSDivision.TYPE_DIVISION,
											DSDivision.TYPE_POSITION }));
				} else {
					users = new ArrayList<DSUser>(1);
					users.add(DSApi.context().getDSUser());
				}

				thisSearchUrl = "/archives/wykaz-udostep.action?doSearch=true"
						+ "&offset="
						+ offset
						+ (lpOd != null ? "&lpOd="+lpOd : "") 
						+ (lpDo != null ? "&lpDo="+lpDo : "") 
						+ (zwDateFrom != null ? "&zwDateFrom="+zwDateFrom : "") 
						+ (zwDateTo != null ? "&zwDateTo="+zwDateFrom : "") 
						+ (wypDateFrom != null ? "&wypDateFrom="+wypDateFrom : "") 
						+ (wypDateTo != null ? "&wypDateTo="+wypDateTo : "") 
						+ (person != null ? "&person="+person : "") 
						+ (rodzajPisma != null ? "&rodzajPisma="+rodzajPisma : "") ;


				ctx = DSApi.openContextIfNeeded();	

				columns=new ArrayList<TableColumn>(7);
				columns.add(new TableColumn("lp","L.p.",null,null));
				//columns.add(new TableColumn("document_id","Id dokumentu",null,null));		
				columns.add(new TableColumn("dataWyp","Data wypo�yczenia",null,null));
				columns.add(new TableColumn("dataZw","Data zwrotu",null,null));
				columns.add(new TableColumn("korzystajacy","Nazwisko i imi� korzystajacego z akt",null,null));
				columns.add(new TableColumn("komorka","Kom�rka organizacyjna lub zak�ad pracy",null,null));
				columns.add(new TableColumn("znak","Znak akt",null,null));
				columns.add(new TableColumn("forma","Forma elektroniczna/Fizyczne",null,null));	
				rodzajePism= new LinkedHashMap<String, String>();
				rodzajePism.put("puste","");
				rodzajePism.put("wycofane","Wycofane");
				rodzajePism.put("udostepnione","Udost�pnione");
				rodzajePism.put("wypozyczone","Wypo�yczone");


				results=new ArrayList<Map<String, Object>>();

				if(rodzajPisma==null||rodzajPisma.equals("puste")||rodzajPisma.equals("udostepnione")||rodzajPisma.equals("wypozyczone")){
					//dodanie udostepnionych
					SelectClause select=new SelectClause(false);
					FromClause from=new FromClause();

					TableAlias fromMulti=from.createTable(UDOSTEPNIANIE_TABELA_MULTI);

					TableAlias fromUdost=from.createJoinedTable(UDOSTEPNIANIE_TABELA, true,fromMulti, FromClause.LEFT_OUTER,"$l.document_id=$r.document_id");
					SelectColumn selectDiv = select.add(fromUdost, "DS_DIVISION_id_uzytkownika");
					SelectColumn documentId = select.add(fromUdost, "document_id");
					SelectColumn terminZwrotu = select.add(fromUdost, "data_zwrotu_dokumentow");
					SelectColumn rodzaj = select.add(fromMulti, "FIELD_CN");
					SelectColumn terminWyp = select.add(fromUdost, "data_odbioru_dokumentow");
					SelectColumn userId = select.add(fromUdost, "DS_USER_id");
					SelectColumn czyZewnetrzny = select.add(fromUdost, "is_wnioskodawca_zewnetrzny");
					SelectColumn multiFieldVal = select.add(fromMulti, "FIELD_VAL");

					WhereClause where = new WhereClause();
					if(rodzajPisma!=null&&rodzajPisma.equals("wypozyczone"))where.add(Expression.eq(fromMulti.attribute("FIELD_CN"), UDOST_MULTI_PISMO_NIEZDYGITALIZOWANE));
					//Junction OR = Expression.disjunction();
				//	OR.add(Expression.eq(fromUdost.attribute("STATUSDOKUMENTU"), STATUS_UDOSTEPNIONE));
					//OR.add(Expression.eq(fromUdost.attribute("STATUSDOKUMENTU"), STATUS_UDOSTEPNIONE1));
					//where.add(OR);
					SelectQuery selectQuery = new SelectQuery(select, from, where, null);


					ResultSet rs=selectQuery.resultSet();
					while(rs.next()){
						HashMap<String, Object> result=new HashMap<String,Object >();
						SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");

						if(rs.getString(rodzaj.getPosition()).equals("LISTA_DOKUMENTOW")||rs.getString(rodzaj.getPosition()).equals("LISTA_SPRAW")||rs.getString(rodzaj.getPosition()).equals("LISTA_TECZEK"))
						{
							result.put("forma", "Elektroniczna");
							if(rs.getString(rodzaj.getPosition()).equals("LISTA_DOKUMENTOW"))
								result.put("znak", znajdzZnakTeczki(rs.getLong(multiFieldVal.getPosition()),1));
							else if(rs.getString(rodzaj.getPosition()).equals("LISTA_SPRAW"))
								result.put("znak", znajdzZnakTeczki(rs.getLong(multiFieldVal.getPosition()),2));
							else if(rs.getString(rodzaj.getPosition()).equals("LISTA_TECZEK"))
								result.put("znak", znajdzZnakTeczki(rs.getLong(multiFieldVal.getPosition()),3));
							else result.put("znak", "---");
						}
						else {
							result.put("forma", "Fizyczna");
							result.put("znak", "nie dotyczy");
						}
						try{
							if(rs.getString(terminZwrotu.getPosition())!=null)result.put("dataZw", df.format(DateUtils.parseDateAnyFormat(rs.getString(terminZwrotu.getPosition()))));
							if(rs.getString(terminWyp.getPosition())!=null)result.put("dataWyp",  df.format(DateUtils.parseDateAnyFormat(rs.getString(terminWyp.getPosition()))));
						}catch (ParseException e) {
							log.error("",e);
							e.printStackTrace();
						}
						if(rs.getString(selectDiv.getPosition())!=null)result.put("komorka", DSDivision.findById(rs.getInt(selectDiv.getPosition())).getName());
						else result.put("komorka", "---");

						if(rs.getString(czyZewnetrzny.getPosition())==null||rs.getString(czyZewnetrzny.getPosition()).equals("1")){
							result.put("korzystajacy", DSUser.findById(rs.getLong(userId.getPosition())).asLastnameFirstname());
						}
						else{
							SelectClause selectperson=new SelectClause(false);
							FromClause fromperson=new FromClause();

							TableAlias fromP=fromperson.createTable(TABELA_PERSON);
							SelectColumn imie = selectperson.add(fromP, "FIRSTNAME");
							SelectColumn nazwisko = selectperson.add(fromP, "LASTNAME");
							SelectColumn firma = selectperson.add(fromP, "ORGANIZATION");
							WhereClause whereP = new WhereClause();
							if(DSApi.isPostgresServer())
							whereP.add(Expression.eq(fromP.attribute("DOCUMENT_ID"), Long.valueOf(rs.getString(documentId.getPosition()))));
							else 
							whereP.add(Expression.eq(fromP.attribute("DOCUMENT_ID"), rs.getString(documentId.getPosition())));
							
							SelectQuery selectQueryP = new SelectQuery(selectperson, fromperson, whereP, null);
							ResultSet rsP=selectQueryP.resultSet();
							if(rsP.next()){
								result.put("korzystajacy", rsP.getString(nazwisko.getPosition())+" "+rsP.getString(imie.getPosition()));
								if(rsP.getString(firma.getPosition())!=null)result.put("komorka", rsP.getString(firma.getPosition()));
							}
							else result.put("korzystajacy","blad pobrania zewnetrzengo wnioskodawcy");

						}
						//result.put("document_id",rs.getString(documentId.getPosition()));
						if(AvailabilityManager.isAvailable("PG.parametrization")){
							result.put("link", "/office/outgoing/document-archive.action?documentId="+rs.getString(documentId.getPosition()));
						}else 
						result.put("link", "/office/internal/document-archive.action?documentId="+rs.getString(documentId.getPosition()));
						
						results.add(result);
					}
					
				}
				if(rodzajPisma==null||rodzajPisma.equals("puste")||rodzajPisma.equals("wycofane")){
					//dodanie wycofanych
					FromClause from=new FromClause();
					TableAlias fromWycof=from.createTable(WYCOFANIE_TABELA);
					SelectClause select=new SelectClause(false);
					SelectColumn documentId = select.add(fromWycof, "DOCUMENT_ID");
					SelectColumn terminWyp = select.add(fromWycof, "data_wycofania");
					SelectColumn userId = select.add(fromWycof, "DS_USER_id_wnioskujacy");
					SelectColumn userDivisionId = select.add(fromWycof, "DS_DIVISION_id");
					SelectColumn caseId=select.add(fromWycof, "sprawa_archiwista");

					WhereClause where = new WhereClause();
				//	where.add(Expression.eq(fromWycof.attribute("STATUSDOKUMENTU"), 5));
					SelectQuery selectQuery = new SelectQuery(select, from, where, null);
					ResultSet rs=selectQuery.resultSet();
					while(rs.next()){
						SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
						HashMap<String, Object> result=new HashMap<String,Object >();
						try{
							if(rs.getString(terminWyp.getPosition())!=null)result.put("dataWyp",  df.format(DateUtils.parseDateAnyFormat(rs.getString(terminWyp.getPosition()))));
						}catch (ParseException e) {
							log.error("",e);
							e.printStackTrace();
						}
						result.put("dataZw", "nie dotyczy");
						result.put("korzystajacy", DSUser.findById(rs.getLong(userId.getPosition())).asLastnameFirstname());

						result.put("forma","Elektroniczny");
						if(rs.getString(userDivisionId.getPosition())!=null&&!rs.getString(userDivisionId.getPosition()).equals("0"))result.put("komorka", DSDivision.findById(Integer.parseInt(rs.getString(userDivisionId.getPosition()))).getName());
						else result.put("komorka", "---");
						Long id=rs.getLong(documentId.getPosition());
						if(rs.getLong(caseId.getPosition())!=0)result.put("znak", znajdzZnakTeczki(rs.getLong(caseId.getPosition()),2));
						
						if(AvailabilityManager.isAvailable("PG.parametrization")){
							result.put("link", "/office/outgoing/document-archive.action?documentId="+rs.getString(documentId.getPosition()));
						}else
						result.put("link", "/office/internal/document-archive.action?documentId="+id.toString());
						
						results.add(result);
					}
				}

				//przydzielenie lp
				for(int i=0;i<results.size();i++){
					results.get(i).put("lp", i+1);
				}
				//result - wszystkie wyniki
				//result2 - zawezone wyniki
				ArrayList<Map<String, Object>> results2=new ArrayList<Map<String, Object>>();
				//sortowanie
				if(sortField!=null){
					if(sortField.equals("numer"))Collections.sort(results,new porownajPoNumerze());
					if(sortField.equals("data"))Collections.sort(results,new porownajPoDacieZw());
					if(sortField.equals("depart"))Collections.sort(results,new porownajPoDepart());
					if(sortField.equals("korzystajacy"))Collections.sort(results,new porownajPoKorz());
					if(sortField.equals("dataWyp"))Collections.sort(results,new porownajPoDacieWyp());
				}
				else Collections.sort(results,new porownajPoDacieWyp());
				
				
				for(int i=0;i<results.size();i++){
					try{
						boolean pasujeDoParamWysz=true;
						if(lpOd!=null){if((Integer)results.get(i).get("lp")<Integer.parseInt(lpOd)){pasujeDoParamWysz=false;}}
						if(lpDo!=null){if((Integer)results.get(i).get("lp")>Integer.parseInt(lpDo)){pasujeDoParamWysz=false;}}
						if(wypDateFrom!=null&&pasujeDoParamWysz){
							if(results.get(i).get("dataWyp")!=null){
								if((DateUtils.parseDateAnyFormat(results.get(i).get("dataWyp").toString())).before(DateUtils.parseDateAnyFormat(wypDateFrom))){
									pasujeDoParamWysz=false;
								}
							}
						}
						if(wypDateTo!=null&&pasujeDoParamWysz){
							if(results.get(i).get("dataWyp")!=null){
								if((DateUtils.parseDateAnyFormat(results.get(i).get("dataWyp").toString()).after(DateUtils.parseDateAnyFormat(wypDateTo)))){
									pasujeDoParamWysz=false;
								}
							}
						}
						if(zwDateFrom!=null&&pasujeDoParamWysz){
							if(results.get(i).get("dataZw")!=null){
								if((DateUtils.parseDateAnyFormat(results.get(i).get("dataZw").toString()).before(DateUtils.parseDateAnyFormat(zwDateFrom)))){
									pasujeDoParamWysz=false;
								}
							}
						}
						if(zwDateTo!=null&&pasujeDoParamWysz){
							if(results.get(i).get("dataZw")!=null){
								if((DateUtils.parseDateAnyFormat(results.get(i).get("dataZw").toString()).after(DateUtils.parseDateAnyFormat(zwDateTo)))){
									pasujeDoParamWysz=false;
								}
							}
						}
						if(person!=null&&pasujeDoParamWysz){
							if(!results.get(i).get("korzystajacy").equals(DSUser.findByUsername(person).asLastnameFirstname()))
								pasujeDoParamWysz=false;
						}
						if(pasujeDoParamWysz)
							results2.add(results.get(i));
					}catch (ParseException e) {
						//nie dodaje, wpis z data nie pasuje do filtra - dotyczy pism wycofanych, gdzie nie ma daty zwrotu
					}
				}

				//trueResults - wyniki do pagera
				if(results2.size()!=0){
					if(offset>results2.size())offset=0;

					if(offset+limit>results2.size()){

						trueResults=new ArrayList<Map<String, Object>>(results2.subList(offset, results2.size()));
					}
					else{
						trueResults=new ArrayList<Map<String, Object>>(results2.subList(offset, offset+limit));
					}
				}




				Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
					public String getLink(int offset) {
						return HttpUtils.makeUrl(
								"/archives/wykaz-udostep.action",
								new Object[] { "doSearch", "true",
										"lpOd", lpOd, "lpDo", lpDo,
										"zwDateFrom", zwDateFrom, "zwDateTo",
										zwDateTo, "wypDateFrom",
										wypDateFrom, "wypDateTo", wypDateTo,
										"person",person,
										"rodzajPisma",rodzajPisma,
										"sortField",sortField,
										"ascending",String.valueOf(ascending),
										"offset",
										String.valueOf(offset) });
					}
				};

				pager = new Pager(linkVisitor, offset, limit,
						results2.size(), 10);






				DSApi.closeContextIfNeeded(ctx);
			} catch (EdmException e) {
				log.error("",e);
				e.printStackTrace();
			} catch (SQLException e) {
				log.error("",e);
				e.printStackTrace();
			} 
		}



	}
	
	
	
	public String getSortLink(String field, boolean asc) {
		return thisSearchUrl + "&sortField=" + field + "&ascending=" + asc;
	}
	class porownajPoDacieZw implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey("dataZw")&&o2.containsKey("dataZw")){
				if(o1.get("dataZw")!=null&&o2.get("dataZw")!=null){
					try {
						if((DateUtils.parseDateAnyFormat(o1.get("dataZw").toString()).compareTo(DateUtils.parseDateAnyFormat(o2.get("dataZw").toString())))==0)return 0;
						if((DateUtils.parseDateAnyFormat(o1.get("dataZw").toString()).before(DateUtils.parseDateAnyFormat(o2.get("dataZw").toString())))){
							return -1;
						}
						else return 1;
					} catch (ParseException e) {
						try{
							DateUtils.parseDateAnyFormat(o1.get("dataZw").toString());
						}
						catch(ParseException e2){
							try{
								DateUtils.parseDateAnyFormat(o2.get("dataZw").toString());
							}
							catch(ParseException e3){
								return 0;
							}
							if(ascending)return 1;
							else return -1;
						}
						if(ascending)return -1;
						else return 1;
					}
				}
				if(o1.get("dataZw")==null&&o2.get("dataZw")==null) return 0;
				if(o1.get("dataZw")!=null){
					if(ascending)return 1;
					else return -1;
				}
				else if(o2.get("dataZw")!=null){
					if(ascending)return -1;
					else return 1;
				} 
				else return 0;

			}

			if(o1.containsKey("dataZw")){
				if(ascending)return 1;
				else return -1;
			}
			else if (o2.containsKey("dataZw")) {
				if(ascending)return -1;
				else return 1;
			}
			else return 0;
		}

	}
	class porownajPoDacieWyp implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey("dataWyp")&&o2.containsKey("dataWyp")){
				if(o1.get("dataWyp")!=null&&o2.get("dataWyp")!=null){
					try {
						if((DateUtils.parseDateAnyFormat(o1.get("dataWyp").toString()).compareTo(DateUtils.parseDateAnyFormat(o2.get("dataWyp").toString())))==0)return 0;
						if((DateUtils.parseDateAnyFormat(o1.get("dataWyp").toString()).before(DateUtils.parseDateAnyFormat(o2.get("dataWyp").toString())))){
							return -1;
						}
						else return 1;
					} catch (ParseException e) {
						try{
							DateUtils.parseDateAnyFormat(o1.get("dataWyp").toString());
						}
						catch(ParseException e2){
							try{
								DateUtils.parseDateAnyFormat(o2.get("dataWyp").toString());
							}
							catch(ParseException e3){
								return 0;
							}
							if(ascending)return 1;
							else return -1;
						}
						if(ascending)return -1;
						else return 1;
					}


				}
				if(o1.get("dataWyp")==null&&o2.get("dataWyp")==null) return 0;
				if(o1.get("dataWyp")!=null){
					if(ascending)return 1;
					else return -1;
				}
				else{
					if(ascending)return -1;
					else return 1;
				}

			}
			if(o1.containsKey("dataWyp")){
				if(ascending)return 1;
				else return -1;
			}
			else if (o2.containsKey("dataWyp")) {
				if(ascending)return -1;
				else return 1;
			}
			else return 0;
			
		
		}

	}


	class porownajPoDepart implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey("komorka")&&o2.containsKey("komorka")){
				if(o1.get("komorka")!=null&&o2.get("komorka")!=null){
					if(o1.get("komorka").toString().compareToIgnoreCase(o2.get("komorka").toString())>0){
						if(ascending)return -1;
						else return 1;
					}
					else if(o1.get("komorka").toString().compareToIgnoreCase(o2.get("komorka").toString())<0){
						if(ascending)return 1;
						else return -1;
					}
					else return 0;


				}
				if(o1.get("komorka")!=null){
					if(ascending)return -1;
					else return 1;
				}
				else{
					if(ascending)return 1;
					else return -1;
				}


			}
			return 0;
		}

	}
	class porownajPoKorz implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey("korzystajacy")&&o2.containsKey("korzystajacy")){
				if(o1.get("korzystajacy")!=null&&o2.get("korzystajacy")!=null){
					if(o1.get("korzystajacy").toString().compareToIgnoreCase(o2.get("korzystajacy").toString())>=0){
						if(ascending)return -1;
						else return 1;
					}
					else {
						if(ascending)return 1;
						else return -1;
					}


				}
				if(o1.get("korzystajacy")!=null){
					if(ascending)return -1;
					else return 1;
				}
				else{
					if(ascending)return 1;
					else return -1;
				}


			}
			return 0;
		}

	}
	class porownajPoNumerze implements Comparator<Map<String, Object>>{

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if(o1.containsKey("lp")&&o2.containsKey("lp")){
				if(o1.get("lp")!=null&&o2.get("lp")!=null){
					if(Integer.parseInt(o1.get("lp").toString())>Integer.parseInt(o2.get("lp").toString())){
						if(ascending)return -1;
						else return 1;
					}
					else if(Integer.parseInt(o1.get("lp").toString())<Integer.parseInt(o2.get("lp").toString())){
						if(ascending)return 1;
						else return -1;
					}
					else return 0;
				}
				if(o1.get("lp")!=null){
					if(ascending)return 1;
					else return -1;
				}
				else{
					if(ascending)return -1;
					else return 1;
				}

			}
			return 0;
		}

	}


	public String znajdzZnakTeczki(Long id,int rodzaj){
		//rodzaj 1 - dokument, 2 - sprawa, 3 - teczka
		String znak="";
		boolean ctx;
		if(id==0)return "---";
		try {
			ctx = DSApi.openContextIfNeeded();
			if(rodzaj==3){
				znak=OfficeFolder.find(id).getOfficeId();
			}
			else if(rodzaj==2){
				znak=OfficeCase.find(id).getParent().getOfficeId();
			}
			else if(rodzaj==1){
				if(OfficeDocument.find(id).getContainingCaseId()!=null)
					znak=OfficeCase.find(OfficeDocument.find(id).getContainingCaseId()).getParent().getOfficeId();
			}



			DSApi.closeContextIfNeeded(ctx);
		} catch (EdmException e) {
			log.error("",e);
			return "";
		}
		return znak;

	}


	public String prettyPrint(Object object)
	{
		if (object == null) return "";
		if (object instanceof Date)
		{
			String date = DateUtils.formatJsDateTime((Date) object);

			if (date.substring(date.indexOf(' ')+1).equals("00:00"))
				return DateUtils.formatJsDate((Date) object);
			else
				return date;
		}
		else
		{
			return object.toString();
		}
	}


	public List<TableColumn> getColumns() {
		return columns;
	}


	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}


	public int getOffset() {
		return offset;
	}


	public void setOffset(int offset) {
		this.offset = offset;
	}


	public static int getLimit() {
		return limit;
	}


	public static void setLimit(int limit) {
		WykazUdostepnionychAction.limit = limit;
	}


	public ArrayList<Map<String, Object>> getResults() {
		return results;
	}


	public void setResults(ArrayList<Map<String, Object>> results) {
		this.results = results;
	}


	public String getThisSearchUrl() {
		return thisSearchUrl;
	}


	public void setThisSearchUrl(String thisSearchUrl) {
		this.thisSearchUrl = thisSearchUrl;
	}

	public String getLpOd() {
		return lpOd;
	}


	public void setLpOd(String lpOd) {
		this.lpOd = lpOd;
	}


	public String getLpDo() {
		return lpDo;
	}


	public void setLpDo(String lpDo) {
		this.lpDo = lpDo;
	}


	public String getZwDateFrom() {
		return zwDateFrom;
	}


	public void setZwDateFrom(String zwDateFrom) {
		this.zwDateFrom = zwDateFrom;
	}


	public String getZwDateTo() {
		return zwDateTo;
	}


	public void setZwDateTo(String zwDateTo) {
		this.zwDateTo = zwDateTo;
	}


	public String getWypDateFrom() {
		return wypDateFrom;
	}


	public void setWypDateFrom(String wypDateFrom) {
		this.wypDateFrom = wypDateFrom;
	}


	public String getWypDateTo() {
		return wypDateTo;
	}


	public void setWypDateTo(String wypDateTo) {
		this.wypDateTo = wypDateTo;
	}
	public List<DSUser> getUsers() {
		return users;
	}


	public void setUsers(List<DSUser> users) {
		this.users = users;
	}


	public Pager getPager() {
		return pager;
	}


	public void setPager(Pager pager) {
		this.pager = pager;
	}


	public ArrayList<Map<String, Object>> getTrueResults() {
		return trueResults;
	}


	public void setTrueResults(ArrayList<Map<String, Object>> trueResults) {
		this.trueResults = trueResults;
	}


	public String getPerson() {
		return person;
	}


	public void setPerson(String person) {
		this.person = person;
	}


	public String getRodzajPisma() {
		return rodzajPisma;
	}


	public void setRodzajPisma(String rodzajPisma) {
		this.rodzajPisma = rodzajPisma;
	}


	public Map<String, String> getRodzajePism() {
		return rodzajePism;
	}


	public void setRodzajePism(Map<String, String> rodzajePism) {
		this.rodzajePism = rodzajePism;
	}


	public boolean isAscending() {
		return ascending;
	}


	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}


	public String getSortField() {
		return sortField;
	}


	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

}
