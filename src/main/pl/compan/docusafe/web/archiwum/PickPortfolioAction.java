package pl.compan.docusafe.web.archiwum;



/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PickPortfolioAction.java,v 1.8 2007/10/05 14:06:47 kubaw Exp $
 */
public class PickPortfolioAction extends PortfoliosBaseAction
{
    
    protected final String baseLink = "/office/pick-portfolio.action";
    public String getBaseLink()
    {
        return baseLink;
    }
}
