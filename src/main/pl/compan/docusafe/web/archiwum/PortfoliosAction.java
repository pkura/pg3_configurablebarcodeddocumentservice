package pl.compan.docusafe.web.archiwum;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.OfficeFolder;




/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PortfoliosAction.java,v 1.8 2007/10/05 14:06:47 kubaw Exp $
 */
public class PortfoliosAction extends PortfoliosBaseAction
{
	protected String baseLink = "/archives/portfolios-archive.action";

	public String getBaseLink()
	{
		return baseLink;
	}
	@Override
	public List getPortfolios()
	{
		List portfolios =super.getPortfolios();
		List filteredPortfolios=new ArrayList<OfficeFolder>();
		try {

			for(Object f:portfolios){

				boolean ctx=DSApi.openContextIfNeeded();
				//teczki tylko te ktore maja status zarchiwizowane
				if(ArchiveStatus.Archived== ((OfficeFolder)f).getArchiveStatus())
						filteredPortfolios.add(f);
				
				
				DSApi.closeContextIfNeeded(ctx);
			}
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filteredPortfolios;
	}

}
