package pl.compan.docusafe.web.archiwum;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.HttpUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.Pager;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class WykazSpisowAction extends EventActionSupport {

	public static final String DEFAULT_COLUMNS;
	static {
		DEFAULT_COLUMNS = new JSONArray(Arrays.asList(new String[] { "nrSpisu",
				"dataPrzyj", "depart", "lPozS", "lPozT", "uwagi" })).toString();
	}

	private final static Logger log = LoggerFactory
			.getLogger(WykazSpisowAction.class);
	// IMPORT/EXPORT

	private static final String EV_SEARCH = "search";
	private String achome;
	private Map<String, String> achomeList;
	private String act;
	private Map<String, String> actList;
	private String team;
	private Map<String, String> teamList;
	private String status = "8";// satus na spisie zdawczo-odbiorczym
								// oznaczajacy, ze jest przekazane i przyjete w
								// archiwum
	private Map<String, String> statusList;
	private Long[] selectedIds;
	private List columns;
	private String numerySpisow;
	private boolean ascending;

	private String sortField;
	private int offset;
	private static int limit = 20;
	private String thisSearchUrl;
	// EXPORT
	private List<Map<String, Object>> results;
	private Pager pager;

	@Override
	protected void setup() {

		registerListener(DEFAULT_ACTION).append(OpenHibernateSession.INSTANCE)
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doSearch").append(OpenHibernateSession.INSTANCE)
				.append(new FillForm())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doExport").append(OpenHibernateSession.INSTANCE)
				.append(new Export())
				.appendFinally(CloseHibernateSession.INSTANCE);

		registerListener("doTemplate").append(OpenHibernateSession.INSTANCE)
				.append(new GenerateTemplate())
				.appendFinally(CloseHibernateSession.INSTANCE);

	}

	private class FillForm implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			/*try {*/

				thisSearchUrl = "/archives/wykaz-spisow.action?doSearch=true"
						+ "&offset="
						+ offset
						+ (achome != null ? "&achome =" + achome : "")
						+ (act != null ? "&act =" + act : "")
						+ (team != null ? "&team =" + team : "")
						+ (numerySpisow != null ? "&numerySpisow ="
								+ numerySpisow : "");

				achomeList = new LinkedHashMap<String, String>();
				achomeList.put("", "");
				achomeList.put("A", "Kategoria A");
				achomeList.put("B", "Reszta");
				actList = new LinkedHashMap<String, String>();
				actList.put("", "");
				actList.put("aktowy", "Aktowy");
				actList.put("nieaktowy", "Nieaktowy");
				teamList = new LinkedHashMap<String, String>();
				PreparedStatement ps;
				/*
				 * try { ps = DSApi .context() .prepareStatement(
				 * "select * from ds_archives_team"); ResultSet rs =
				 * ps.executeQuery(); teamList.put("", ""); while(rs.next()){
				 * teamList.put(rs.getString("id"),rs.getString("title"));}
				 */

				doSearch();
				if (sortField != null) {
					if (sortField.equals("numer")) {
						Collections.sort(results, new porownajPoNumerze());
					}
					if (sortField.equals("data")) {
						Collections.sort(results, new porownajPoDacie());
					}
					if (sortField.equals("depart")) {
						Collections.sort(results, new porownajPoDepart());
					}
				} else {

					Collections.sort(results, new porownajPoDacie());
				}

				int liczbaWynikow = results.size();
				// zawezenie do pagera
				if (results.size() != 0) {
					if (offset > results.size())
						offset = 0;

					if (offset + limit > results.size()) {

						results = new ArrayList<Map<String, Object>>(
								results.subList(offset, results.size()));
					} else {
						results = new ArrayList<Map<String, Object>>(
								results.subList(offset, offset + limit));
					}
				}

				Pager.LinkVisitor linkVisitor = new Pager.LinkVisitor() {
					public String getLink(int offset) {
						return HttpUtils.makeUrl(
								"/archives/wykaz-spisow.action", new Object[] {
										"doSearch", "true", "achome", achome,
										"act", act, "team", team,
										"numerySpisow", numerySpisow,
										"sortField", sortField, "ascending",
										String.valueOf(ascending), "offset",
										String.valueOf(offset) });
					}
				};

				pager = new Pager(linkVisitor, offset, limit, liczbaWynikow, 10);

		/*	} catch (Exception ex) {
				log.debug(ex);
				ex.printStackTrace();
			}*/

			/*
			 * } catch (EdmException ex) { log.error("", ex);
			 * ex.printStackTrace(); }
			 */

		}
	}

	private void doSearch() {
		columns = getColumns(DSApi.context().userPreferences()
				.node("wykaz-spisow").get("columns", DEFAULT_COLUMNS));
		try {
			results = new ArrayList<Map<String, Object>>();
			StringBuilder sql;
			if (DSApi.isOracleServer())
				sql = new StringBuilder(
						"select a.document_id,a.number_itr as number_itr,CONCAT(CONCAT(u.firstname,' '), u.lastname) as nameAutor, "
								+ "d.name as depart,a.date_inventory_transfer as dataPrzek from "
								+ NewCaseArchiverScheduled.wykazTableName
								+ " a "
								+ "left join ds_division d on d.id=a.ds_division_id "
								+ "left join DS_USER u on u.ID=a.created_by ");
			else if (DSApi.isPostgresServer()) {
				sql = new StringBuilder(
						"select a.document_id,a.number_itr as number_itr, concat_ws(' ', u.firstname, u.lastname) as nameAutor, "
								+ "d.name as depart,a.date_inventory_transfer as dataPrzek from "
								+ NewCaseArchiverScheduled.wykazTableName
								+ " a "
								+ "left join ds_division d on d.id=a.ds_division_id "
								+ "left join DS_USER u on u.ID=a.created_by ");
			} else {
				sql = new StringBuilder(
						"select a.document_id,a.number_itr as number_itr,u.firstname+' '+ u.lastname as nameAutor, "
								+ "d.name as depart,a.date_inventory_transfer as dataPrzek from "
								+ NewCaseArchiverScheduled.wykazTableName
								+ " a "
								+ "left join ds_division d on d.id=a.ds_division_id "
								+ "left join DS_USER u on u.ID=a.created_by ");
			}
			if (achome != null || team != null || act != null
					|| numerySpisow != null || status != null) {
				sql.append(" where STATUSDOKUMENTU=" + status + " and");
				if (achome != null) {
					if (!achome.equals("")) {
						if (achome.equals("A")) {
							sql.append(" is_category_a = '1' and");
						} else
							sql.append(" is_category_a = '0' and");
					}

				}
				if (act != null) {
					// TODO aktowy/nieaktowy podobnie status
				}
				if (team != null) {
					if (!team.equals(""))
						sql.append(" ds_archive_team_id ='" + team + "' and");
				}
				if (numerySpisow != null) {
					if (!numerySpisow.equals("")) {
						if (numerySpisow.contains("-")) {
							String[] tmp = numerySpisow.split("-");
							if (tmp.length == 2) {
								sql.append(" a.number_itr>=" + tmp[0]
										+ " and a.number_itr<=" + tmp[1]
										+ " and");
							}
						} else {
							sql.append(" a.number_itr=" + numerySpisow + " and");
						}
					}
				}
				/*
				 * if(status!=null){ sql.append("  ='1' and"); }
				 */

				// usuniecie and
				sql.delete(sql.length() - 3, sql.length());
			}
			PreparedStatement ps = DSApi.context().prepareStatement(
					sql.toString());
			ResultSet rs = ps.executeQuery();
			int i = 0;

			while (rs.next()) {

				Map<String, Object> tmp = new HashMap<String, Object>(8);
				try {
					OfficeDocument.find(rs.getLong("document_id"));
				} catch (Exception e) {
					log.error( // zmiana liter z duzych na male
							"Nie znaleziono documenty o id : "
									+ rs.getLong("document_id"), e);
					continue;

				}
				tmp.put("id", rs.getString("document_id"));
				if (rs.getString("number_itr") != null
						&& !rs.getString("number_itr").equals(""))
					tmp.put("nrSpisu", rs.getString("number_itr"));
				else
					tmp.put("nrSpisu", "nie przyznano numeru");
				/*
				 * if(!rs.getString("nameAuthor").equals(" ")) tmp.put("autor",
				 * rs.getString("nameAutor")); else tmp.put("autor",
				 * "nie wybrany");
				 */
				tmp.put("depart", rs.getString("depart"));
				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				tmp.put("dataPrzyj",
						(rs.getString("dataPrzek")) != null ? df
								.format(DateUtils.parseDateAnyFormat(rs
										.getString("dataPrzek"))) : null);
				String uwagi = "";

				for (Remark uwaga : OfficeDocument.find(
						rs.getLong("document_id")).getRemarks()) {
					uwagi += uwaga.getContent();
				}
				tmp.put("uwagi", uwagi);
				PreparedStatement ps2;
				if (DSApi.isOracleServer())
					ps2 = DSApi
							.context()
							.prepareStatement(
									"select count(id) as lPozS from "
											+ NewCaseArchiverScheduled.wykazMultipleTableName
											+ " where document_id=? ");
				else
					ps2 = DSApi
							.context()
							.prepareStatement(
									"select count(id) as lPozS from "
											+ NewCaseArchiverScheduled.wykazMultipleTableName
											+ " where document_id=? ");
				ps2.setLong(1, rs.getLong("document_id"));
				ResultSet rs2 = ps2.executeQuery();
				if (rs2.next()) {
					if (rs2.getString("lPozS") != null)
						tmp.put("lPozS", rs2.getString("lPozS"));
					else
						tmp.put("lPozS", "0");
				}
				if (DSApi.isOracleServer())
					ps2 = DSApi
							.context()
							.prepareStatement(
									"SELECT SUM(NVL(FOLDER_COUNT,0)) as folder_count "
											+ "from "
											+ NewCaseArchiverScheduled.wykazItemsTableName
											+ " where id "
											+ "in(select field_val from  "
											+ NewCaseArchiverScheduled.wykazMultipleTableName
											+ " where DOCUMENT_ID=?) ");
				else if (DSApi.isPostgresServer())
					ps2 = DSApi
					.context()
					.prepareStatement(
							"SELECT SUM(coalesce(FOLDER_COUNT,0)) as folder_count "
									+ "from "
									+ NewCaseArchiverScheduled.wykazItemsTableName
									+ " where id "
									+ "in(select cast(field_val as numeric) from "
									+ NewCaseArchiverScheduled.wykazMultipleTableName
									+ " where DOCUMENT_ID=?) ");
				else
					ps2 = DSApi
							.context()
							.prepareStatement(
									"SELECT SUM(ISNULL(FOLDER_COUNT,0)) as folder_count "
											+ "from "
											+ NewCaseArchiverScheduled.wykazItemsTableName
											+ " where id "
											+ "in(select field_val from "
											+ NewCaseArchiverScheduled.wykazMultipleTableName
											+ " where DOCUMENT_ID=?) ");

				ps2.setLong(1, rs.getLong("document_id"));
				rs2 = ps2.executeQuery();
				if (rs2.next()) {
					if (rs2.getString("folder_count") != null)
						tmp.put("lPozT", rs2.getString("folder_count"));
					else
						tmp.put("lPozT", "0");
				}

				results.add(tmp);
			}

		} catch (SQLException e) {
			log.error("", e);
			e.printStackTrace();
		} catch (EdmException e) {
			log.error("", e);
			e.printStackTrace();
		} catch (ParseException e) {
			log.error("", e);
			e.printStackTrace();
		}

	}

	private class Search implements ActionListener {
		public void actionPerformed(final ActionEvent event) {
			doSearch();
			if (results.size() == 0)
				event.addActionError("Brak wynik�w wyszukiwania");
		}
	}

	public String prettyPrint(Object object) {
		if (object == null)
			return "";
		if (object instanceof Date) {
			String date = DateUtils.formatJsDateTime((Date) object);

			if (date.substring(date.indexOf(' ') + 1).equals("00:00"))
				return DateUtils.formatJsDate((Date) object);
			else
				return date;
		} else {
			return object.toString();
		}
	}

	public String getSortLink(String field, boolean asc) {
		return thisSearchUrl + "&sortField=" + field + "&ascending=" + asc;
	}

	private class Export implements ActionListener {

		public void actionPerformed(final ActionEvent event) {
			doSearch();
			if (selectedIds == null) {
				event.addActionError("Nie zaznaczono zadnego spisu");
			} else {
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = new Date();
				// System.out.println(dateFormat.format(date));
				// albo csv??

				int counterRow = 0;
				try {
					File tmpfile = File.createTempFile("DocuSafe", "tmp");
					// String
					// filename=Docusafe.getHome().getAbsolutePath()+"\\wykazy\\wykaz_"+dateFormat.format(date)+".xls";

					HSSFWorkbook workbook = new HSSFWorkbook();
					HSSFSheet sheet = workbook.createSheet("FirstSheet");

					HSSFRow rowhead = sheet.createRow((short) counterRow++);
					rowhead.createCell((short) 0).setCellValue("L.p.");
					rowhead.createCell((short) 1).setCellValue("Numer spisu");
					rowhead.createCell((short) 2).setCellValue(
							"Data przyjecia fizycznego");
					rowhead.createCell((short) 3).setCellValue("Departament");
					rowhead.createCell((short) 4).setCellValue("Autor");
					rowhead.createCell((short) 5).setCellValue(
							"Liczba pozycji spisu");
					rowhead.createCell((short) 6).setCellValue("Liczba teczek");
					rowhead.createCell((short) 7).setCellValue("Uwagi");
					/*
					 * pozycje na druku HSSFRow row=
					 * sheet.createRow((short)counterRow++);
					 * row.createCell((short
					 * )0).setCellValue("Pozycje dla spisu");
					 * row.createCell((short)1).setCellValue("Znak Teczki");
					 * row.createCell((short)2).setCellValue("Tytu�");
					 * row.createCell((short)3).setCellValue("Data skrajna od");
					 * row.createCell((short)4).setCellValue("Data skrajna do");
					 * row.createCell((short)5).setCellValue("Komentarze");
					 * row.createCell
					 * ((short)6).setCellValue("Kategoria archiwalna");
					 */
					counterRow++;
					// autsize dla kolumn
					for (int i = 0; i < 8; i++) {
						sheet.autoSizeColumn((short) i);
					}
					List<Long> selectedIdsList = Arrays.asList(selectedIds);
					int licznikSpisow = 1;
					for (Iterator<Map<String, Object>> it = results.iterator(); it
							.hasNext();) {

						Map<String, Object> tmp = it.next();
						Long id;

						if (tmp.get("id") instanceof String)
							id = Long.parseLong((String) tmp.get("id"));
						else
							id = (Long) tmp.get("id");
						if (selectedIdsList.contains(id)) {
							// spisy
							HSSFRow row = sheet.createRow((short) counterRow++);

							row.createCell((short) 0).setCellValue(
									Integer.toString(licznikSpisow));
							licznikSpisow++;

							if (tmp.get("nrSpisu") != null)
								row.createCell((short) 1).setCellValue(
										tmp.get("nrSpisu").toString());
							else
								row.createCell((short) 1).setCellValue("-");

							/*
							 * po co tutaj parsowa� jak parsujemy wczesniej w
							 * wwynikach wrzucamy do tempa wpisu i tutaj
							 * wrzucamy tylko stringa do dokumentu z data ?
							 * if(tmp
							 * .get("dataPrzyj")!=null)row.createCell((short)
							 * 2).
							 * setCellValue(DateUtils.formatCommonDate(DateUtils
							 * .
							 * parseDateAnyFormat(tmp.get("dataPrzyj").toString(
							 * )))); else row.createCell((short)
							 * 2).setCellValue("-");
							 */
							if (tmp.get("dataPrzyj") != null)
								row.createCell((short) 2).setCellValue(
										tmp.get("dataPrzyj").toString());
							else
								row.createCell((short) 2).setCellValue("-");

							if (tmp.get("depart") != null)
								row.createCell((short) 3).setCellValue(
										tmp.get("depart").toString());
							else
								row.createCell((short) 3).setCellValue("-");

							if (tmp.get("autor") != null)
								row.createCell((short) 4).setCellValue(
										tmp.get("autor").toString());
							else
								row.createCell((short) 4).setCellValue("-");

							if (tmp.get("lPozS") != null)
								row.createCell((short) 5).setCellValue(
										tmp.get("lPozS").toString());
							else
								row.createCell((short) 5).setCellValue("-");

							if (tmp.get("lPozT") != null)
								row.createCell((short) 6).setCellValue(
										tmp.get("lPozT").toString());
							else
								row.createCell((short) 6).setCellValue("-");

							if (tmp.get("Uwagi") != null)
								row.createCell((short) 7).setCellValue(
										tmp.get("Uwagi").toString());
							else
								row.createCell((short) 7).setCellValue("-");

							/*
							 * pozycje na drukowaniu spisu
							 * 
							 * PreparedStatement ps2 = DSApi .context()
							 * .prepareStatement(
							 * "SELECT FOLDER_OFFICEID,FOLDER_TITLE, COMMENTS, DATE_FROM, DATE_TO, CATEGORY_ARCH "
							 * +
							 * "from "+NewCaseArchiverScheduled.wykazItemsTableName
							 * +" where id in(" +
							 * "  	select field_val from  "+NewCaseArchiverScheduled
							 * .
							 * wykazMultipleTableName+" where DOCUMENT_ID=?) ");
							 * ps2.setLong(1,
							 * Long.valueOf(tmp.get("id").toString()));
							 * ResultSet rs2 = ps2.executeQuery(); if
							 * (rs2.next()) { //pozycje spisu row=
							 * sheet.createRow((short)counterRow++);
							 * row.createCell(0).setCellValue("");
							 * if(rs2.getString
							 * ("FOLDER_OFFICEID")!=null)row.createCell((short)
							 * 1
							 * ).setCellValue(rs2.getString("FOLDER_OFFICEID"));
							 * else row.createCell((short) 1).setCellValue("-");
							 * 
							 * 
							 * if(rs2.getString("FOLDER_TITLE")!=null)row.createCell
							 * ((short)
							 * 2).setCellValue(rs2.getString("FOLDER_TITLE"));
							 * else row.createCell((short) 2).setCellValue("-");
							 * 
							 * if(rs2.getString("COMMENTS")!=null)row.createCell(
							 * (short)
							 * 3).setCellValue(rs2.getString("COMMENTS")); else
							 * row.createCell((short) 3).setCellValue("-");
							 * 
							 * 
							 * if(rs2.getString("DATE_FROM")!=null)row.createCell
							 * ((short)
							 * 4).setCellValue(DateUtils.formatCommonDate
							 * (DateUtils
							 * .parseDateAnyFormat(rs2.getString("DATE_FROM"
							 * )))); else row.createCell((short)
							 * 4).setCellValue("-");
							 * 
							 * 
							 * if(rs2.getString("DATE_TO")!=null)row.createCell((
							 * short)
							 * 5).setCellValue(DateUtils.formatCommonDate(
							 * DateUtils
							 * .parseDateAnyFormat(rs2.getString("DATE_TO"))));
							 * else row.createCell((short) 5).setCellValue("-");
							 * 
							 * 
							 * if(rs2.getString("CATEGORY_ARCH")!=null)row.
							 * createCell((short)
							 * 6).setCellValue(rs2.getString("CATEGORY_ARCH"));
							 * else row.createCell((short) 6).setCellValue("-");
							 * 
							 * }
							 */

						}
					}
					FileOutputStream fileOut = new FileOutputStream(tmpfile);
					workbook.write(fileOut);
					fileOut.close();

					tmpfile.deleteOnExit();
					FileOutputStream fis = new FileOutputStream(tmpfile);
					workbook.write(fis);
					fis.close();
					ServletUtils.streamFile(ServletActionContext.getResponse(),
							tmpfile, "application/vnd.ms-excel",
							"Content-Disposition: attachment; filename=\"wykaz_"
									+ dateFormat.format(date) + ".xls\"");
					// System.out.println("Your excel file has been generated!");
					// event.addActionMessage("Wyeksportowano zaznaczone pozycje
					// z wykazu );
				} catch (Exception ex) {
					log.error("", ex);
					addActionError(ex.getMessage());

				}
			}
		}
	}

	private class GenerateTemplate implements ActionListener {

		public void actionPerformed(final ActionEvent event) {
			doSearch();
			if (selectedIds == null) {
				event.addActionError("Nie zaznaczono spisu do wygenerowania rtf-a");
				return;
			}
			if (selectedIds.length > 1) {
				event.addActionError("Wybrano wiecej niz 1 pozycje");
				return;
			}
			PreparedStatement ps1 = null;
			PreparedStatement ps2 = null;
			Long wykazId = selectedIds[0];
			try {
				if (wykazId != null) {
					Long id;

					Map<String, Object> params = Maps.newHashMap();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					OfficeDocument document;

					String templateName = "spisZdawczoOdbiorczy.rtf";
					String special = "true";
					ServletActionContext.getResponse().setContentType(
							"text/rtf");

					OfficeDocument doc = OfficeDocument.find(wykazId);
					FieldsManager fm = doc.getFieldsManager();
					fm.initialize();
					List<Map<String, String>> pozycje = Lists.newArrayList();
					Double metry_bierzace = 0D;

					// $DIVISION i $NUMBER_ITR
					String query1 = "select ds_division_id, number_itr from "
							+ NewCaseArchiverScheduled.wykazTableName
							+ " wykaz " + " where wykaz.DOCUMENT_ID = "
							+ wykazId;

					ps1 = DSApi.context().prepareStatement(query1);
					ResultSet queryResult1 = ps1.executeQuery();
					queryResult1.next();
					Long divisionId = queryResult1.getLong("ds_division_id");
					Integer number = queryResult1.getInt("number_itr");

					String division = DSDivision
							.findById(divisionId.intValue()).getName();

					params.put("DIVISION", division != null ? division
							: ".........");
					params.put("NUMBER_ITR",
							(number != null && number != 0) ? number
									: ".........");

					// zawartosc tabelki

					String query2 = "select * from "
							+ NewCaseArchiverScheduled.wykazMultipleTableName
							+ " multi left join "
							+ NewCaseArchiverScheduled.wykazItemsTableName
							+ " item on multi.FIELD_VAL = item.id ";
					if(DSApi.isPostgresServer()){
						query2	+="::text ";
					}
					query2+= " where multi.DOCUMENT_ID = " + wykazId;

					ps2 = DSApi.context().prepareStatement(query2);
					ResultSet queryResult2 = ps2.executeQuery();
					int i = 0;

					while (queryResult2.next()) {
						i++;
						int column = 0;
						HashMap<String, String> pozycja = new HashMap<String, String>(
								9);

						pozycja.put("lp", Integer.toString(i));

						// znak teczki
						String znak = queryResult2.getString("FOLDER_OFFICEID");
						pozycja.put("znak", StringUtils.isNotEmpty(znak) ? znak
								: "-----");

						// tytul teczki
						String tytul = queryResult2.getString("FOLDER_TITLE");
						pozycja.put("rwatytul",
								StringUtils.isNotEmpty(tytul) ? tytul : "-----");

						// uwagi
						String uwagi = queryResult2.getString("COMMENTS");
						pozycja.put("uwagi",
								StringUtils.isNotEmpty(uwagi) ? uwagi : "-----");

						// daty od, do
						String dataOd = queryResult2.getString("DATE_FROM");
						pozycja.put(
								"dataOd",
								StringUtils.isNotEmpty(dataOd) ? DateUtils
										.formatJsDate(dateFormat.parse(dataOd))
										: "-----");
						String dataDo = queryResult2.getString("DATE_TO");
						pozycja.put(
								"dataDo",
								StringUtils.isNotEmpty(dataDo) ? DateUtils
										.formatJsDate(dateFormat.parse(dataDo))
										: "-----");

						// kategoria
						String kategoria = queryResult2
								.getString("CATEGORY_ARCH");
						pozycja.put("kat",
								StringUtils.isNotEmpty(kategoria) ? kategoria
										: "-----");

						// liczba teczek
						String liczba = queryResult2.getString("FOLDER_COUNT");
						pozycja.put("liczbaTeczek", StringUtils
								.isNotEmpty(liczba) ? liczba : "-----");

						// miejsce przechowywania
						String miejsce = queryResult2.getString("ARCH_PLACE");
						pozycja.put("miejsce",
								StringUtils.isNotEmpty(miejsce) ? miejsce
										: "-----");

						// data zniszczenia
						String dataZniszcz = queryResult2
								.getString("DATE_SHRED");
						pozycja.put(
								"dataZnisz",
								StringUtils.isNotEmpty(dataZniszcz) ? DateUtils
										.formatJsDate(dateFormat
												.parse(dataZniszcz)) : "-----");

						pozycje.add(pozycja);

						metry_bierzace += queryResult2
								.getDouble("FOLDER_LENGTH_METER");
					}
					params.put("TECZKI", pozycje);

					Map<Class, Format> defaultFormatters = Maps
							.newLinkedHashMap();
					ServletActionContext.getResponse().setHeader(
							"Content-Disposition",
							"attachment; filename=\"" + templateName + "\"");
					Templating.rtfTemplate(templateName, params,
							ServletActionContext.getResponse()
									.getOutputStream(), defaultFormatters,
							special);

				} else {
					event.addActionError("Blad wybranej pozycji");
					return;
				}

			} catch (DocumentNotFoundException e) {
				log.debug("Blad generowania rtf", e);
				e.printStackTrace();
			} catch (EdmException e) {
				log.debug("Blad generowania rtf", e);
				e.printStackTrace();
			} catch (Exception e) {
				event.addActionError("Blad generowania rtf");
				log.error("Blad przy generowaniu rtf spisu zd-od nr " + wykazId
						+ " " + e.getMessage(), e);
			} finally {
				if (ps1 != null)
					DSApi.context().closeStatement(ps1);
				if (ps2 != null)
					DSApi.context().closeStatement(ps2);
			}
		}

	}

	class porownajPoNumerze implements Comparator<Map<String, Object>> {

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			try {
				if (o1.containsKey("nrSpisu") && o2.containsKey("nrSpisu")) {
					if (o1.get("nrSpisu") != null && o2.get("nrSpisu") != null) {
						if (Integer.parseInt(o1.get("nrSpisu").toString()) > Integer
								.parseInt(o2.get("nrSpisu").toString())) {
							if (ascending)
								return -1;
							else
								return 1;
						} else {
							if (ascending)
								return 1;
							else
								return -1;
						}

					}
					if (o1.get("nrSpisu") != null) {
						if (ascending)
							return 1;
						else
							return -1;
					} else {
						if (ascending)
							return -1;
						else
							return 1;
					}

				}
			} catch (NumberFormatException e) {
				try {
					Integer.parseInt(o1.get("nrSpisu").toString());
				} catch (NumberFormatException e2) {
					try {
						Integer.parseInt(o2.get("nrSpisu").toString());
					} catch (NumberFormatException e3) {
						return 0;
					}

					if (ascending)
						return -1;
					else
						return 1;
				}
				if (ascending)
					return 1;
				else
					return -1;
			}

			return 0;

		}
	}

	class porownajPoDepart implements Comparator<Map<String, Object>> {

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if (o1.containsKey("depart") && o2.containsKey("depart")) {
				if (o1.get("depart") != null && o2.get("depart") != null) {
					if (o1.get("depart").toString()
							.compareTo(o2.get("depart").toString()) > 0) {
						if (ascending)
							return -1;
						else
							return 1;
					} else if (o1.get("depart").toString()
							.compareTo(o2.get("depart").toString()) < 0) {
						if (ascending)
							return 1;
						else
							return -1;
					} else
						return 0;

				}
				if (o1.get("depart") != null) {
					if (ascending)
						return -1;
					else
						return 1;
				} else {
					if (ascending)
						return 1;
					else
						return -1;
				}

			}
			return 0;
		}

	}

	class porownajPoDacie implements Comparator<Map<String, Object>> {

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			if (o1.containsKey("dataPrzyj") && o2.containsKey("dataPrzyj")) {
				if (o1.get("dataPrzyj") != null && o2.get("dataPrzyj") != null) {
					try {
						if ((DateUtils.parseDateAnyFormat(o1.get("dataPrzyj")
								.toString()).compareTo(DateUtils
								.parseDateAnyFormat(o2.get("dataPrzyj")
										.toString()))) == 0)
							return 0;
						if ((DateUtils.parseDateAnyFormat(o1.get("dataPrzyj")
								.toString()).before(DateUtils
								.parseDateAnyFormat(o2.get("dataPrzyj")
										.toString())))) {
							if (ascending)
								return 1;
							else
								return -1;
						} else {
							if (ascending)
								return -1;
							else
								return 1;
						}
					} catch (ParseException e) {
						try {
							DateUtils.parseDateAnyFormat(o1.get("dataPrzyj")
									.toString());
						} catch (ParseException e2) {
							try {
								DateUtils.parseDateAnyFormat(o2
										.get("dataPrzyj").toString());
							} catch (ParseException e3) {
								return 0;
							}
							if (ascending)
								return 1;
							else
								return -1;
						}
						if (ascending)
							return -1;
						else
							return 1;
					}

				}
				if (o1.get("dataPrzyj") == null && o2.get("dataPrzyj") == null)
					return 0;
				if (o1.get("dataPrzyj") != null) {
					if (ascending)
						return 1;
					else
						return -1;
				} else {
					if (ascending)
						return -1;
					else
						return 1;
				}

			}
			if (o1.containsKey("dataPrzyj")) {
				if (ascending)
					return 1;
				else
					return -1;
			} else if (o2.containsKey("dataPrzyj")) {
				if (ascending)
					return -1;
				else
					return 1;
			} else
				return 0;
		}

	}

	private List getColumns(String columnsString) {
		List<TableColumn> cols = new ArrayList<TableColumn>(10);
		try {
			JSONArray array = new JSONArray(columnsString);
			for (int i = 0; i < array.length(); i++) {
				String property = array.getString(i);
				cols.add(new TableColumn(array.getString(i), null, null, null,
						""));
			}
		} catch (ParseException e) {
			try {
				JSONArray array = new JSONArray(DEFAULT_COLUMNS);
				for (int i = 0; i < array.length(); i++) {
					String property = array.getString(i);

					cols.add(new TableColumn(array.getString(i), null, null,
							null, ""));
				}
			} catch (ParseException pe) {
				log.error("", e);
				throw new RuntimeException(pe.getMessage(), e);
			}
		}

		return cols;
	}

	public String getWykazLink(Map bean) {
		
		if(AvailabilityManager.isAvailable("PG.parametrization"))
			return "/office/outgoing/document-archive.action?documentId="
			+ bean.get("id");
			
		else 
			
		return "/office/internal/document-archive.action?documentId="
				+ bean.get("id");
	}

	public String getAchome() {
		return achome;
	}

	public void setAchome(String achome) {
		this.achome = achome;
	}

	public Map<String, String> getAchomeList() {
		return achomeList;
	}

	public void setAchomeList(Map<String, String> achomeList) {
		this.achomeList = achomeList;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Map<String, String> getTeamList() {
		return teamList;
	}

	public void setTeamList(Map<String, String> teamList) {
		this.teamList = teamList;
	}

	public List<Map<String, Object>> getResults() {
		return results;
	}

	public void setResults(List<Map<String, Object>> results) {
		this.results = results;
	}

	public List getColumns() {
		return columns;
	}

	public void setColumns(List columns) {
		this.columns = columns;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public Map<String, String> getActList() {
		return actList;
	}

	public void setActList(Map<String, String> actList) {
		this.actList = actList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNumerySpisow() {
		return numerySpisow;
	}

	public void setNumerySpisow(String numerySpisow) {
		this.numerySpisow = numerySpisow;
	}

	public Map<String, String> getStatusList() {
		return statusList;
	}

	public void setStatusList(Map<String, String> statusList) {
		this.statusList = statusList;
	}

	public void setSelectedIds(Long[] selectedIds) {
		this.selectedIds = selectedIds;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public Long[] getSelectedIds() {
		return selectedIds;
	}

	public boolean isAscending() {
		return ascending;
	}

}
