package pl.compan.docusafe.web;

/**
 * Klasa sluzaca do cachowania zasobow generowanych dynamicznie, ale czesto powtarzalnych
 * CSSow, GIFow, etc.
 * @author wkutyla
 *
 */
public class ResourceCacheBean 
{
	private String resKey;
	private String resourceAsString;
	private byte[] resourceAsBytes;
	
	
	public String getResKey() {
		return resKey;
	}
	public void setResKey(String resKey) {
		this.resKey = resKey;
	}
	public String getResourceAsString() {
		return resourceAsString;
	}
	public void setResourceAsString(String resourceAsString) {
		this.resourceAsString = resourceAsString;
	}
	public byte[] getResourceAsBytes() {
		return resourceAsBytes;
	}
	public void setResourceAsBytes(byte[] resourceAsBytes) {
		this.resourceAsBytes = resourceAsBytes;
	}
}
