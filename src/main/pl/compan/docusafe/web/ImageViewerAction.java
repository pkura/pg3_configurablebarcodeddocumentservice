package pl.compan.docusafe.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

/**
 * Klasa implementująca akcje wyświetlania obrazków z klasy pl.compan.docusafe.core.base.Image
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class ImageViewerAction extends EventActionSupport 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identyfikator obrazka
	 */
	private Long imgId;
	
	/**
	 * Konfiguracja akcji, rejestracja obiektów
	 */
	@Override
	protected void setup() 
	{
		registerListener(DEFAULT_ACTION)
			.append(OpenHibernateSession.INSTANCE)
			.append(new DisplayImage())
			.appendFinally(CloseHibernateSession.INSTANCE);
	}
	
	/**
	 * Klasa implementująca akcję wyświetlenia obrazka o podanym identyfikatorze
	 * 
	 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
	 */
	private class DisplayImage implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			if (imgId == null)
				return;
			
			try
			{
				DSApi.context().begin();
				
				// wyszukanie obiektu obrazu
				Image image = Image.find(imgId);
				if (image == null)
					throw new EdmException("Image entity not exists!");
				
				// utworzenie pliku tymczasowego
				File contentFile = createTempFile(image.getImageBinaryStream());
				// utworzenie contentu obrazka
				createContent(contentFile, image.getContentType(), event);
				
				DSApi.context().commit();
			}
			catch (EdmException ex)
			{
				DSApi.context()._rollback();
			}
		}
		
		/**
		 * Metoda tworząca tymczasowy plik zawierający dane binarne obrazu
		 * 
		 * @param stream
		 * @return
		 * @throws EdmException
		 */
		public File createTempFile(InputStream stream) throws EdmException
		{
			if (stream == null)
				throw new EdmException("Stream Is Null");
			
			try
			{
				File contentFile = File.createTempFile("img-viewer", ".tmp");
				FileOutputStream out = new FileOutputStream(contentFile);
				IOUtils.copy(stream, out);
				out.close();
				stream.close();
				stream = null;
				
				return contentFile;
			}
			catch (IOException ex)
			{
				throw new EdmException("IOError");
			}
		}
		
		/**
		 * Metoda wyświetlająca dany obraz poprzez odpowiednie ustawienie obiektu response (HttpServletResponse)
		 * 
		 * @param contentFile
		 * @param contentType
		 * @param event
		 * @throws EdmException
		 */
		public void createContent(File contentFile, String contentType, ActionEvent event) 
			throws EdmException
		{
			event.setResult("view");
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType(contentType);
			try
			{
				OutputStream output = response.getOutputStream();
				FileInputStream input = new FileInputStream(contentFile);
				byte[] buf = new byte[8192];
                int count;
                while ((count = input.read(buf)) > 0)
                {
                	output.write(buf, 0, count);
                    output.flush();
                }
                output.close();
                input.close();
			}
			catch (IOException ex)
			{
				throw new EdmException("Cannot create image content");
			}
			finally
			{
				contentFile.delete();
			}
		}
	}

	/**
	 * Metoda zwracająca składową imgId
	 * 
	 * @return
	 */
	public Long getImgId() 
	{
		return imgId;
	}

	/**
	 * Metoda ustawiająca składową imgId
	 * 
	 * @param imgId
	 */
	public void setImgId(Long imgId) 
	{
		this.imgId = imgId;
	}
}
