package pl.compan.docusafe.web;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Adapter interfejsu java.util.Map pozostawiaj�cy do implementacji
 * jedynie metod� {@link #get(java.lang.Object)}. Pozosta�e metody
 * rzucaj� wyj�tek UnsupportedOperationException.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SortingLinks.java,v 1.2 2006/02/20 15:42:33 lk Exp $
 */
public abstract class SortingLinks implements Map
{
    /**
     * Metoda powinna zwr�ci� odno�nik odpowiadaj�cy przekazanemu
     * argumentowi. W aplikacji Docusafe przyj�to konwencj� nazewnictwa
     * odno�nik�w: &lt;nazwa w�asno�ci&gt; "_" &lt;asc | desc&gt;.
     * Na przyk�ad: "caseId_asc", "title_desc".
     * <p>
     * Zwracany odno�nik nie powinien zawiera� �cie�ki kontekstowej
     * (request.getContextPath()).
     * <p>
     * Je�eli nazwa jest nierozpoznana metoda powinna rzuci� wyj�tek
     * IllegalArgumentException.
     */
    public abstract Object get(Object key);

    public int size()
    {
        throw new UnsupportedOperationException();
    }

    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    public boolean isEmpty()
    {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object key)
    {
        throw new UnsupportedOperationException();
    }

    public boolean containsValue(Object value)
    {
        throw new UnsupportedOperationException();
    }

    public Collection values()
    {
        throw new UnsupportedOperationException();
    }

    public void putAll(Map t)
    {
        throw new UnsupportedOperationException();
    }

    public Set entrySet()
    {
        throw new UnsupportedOperationException();
    }

    public Set keySet()
    {
        throw new UnsupportedOperationException();
    }

    public Object remove(Object key)
    {
        throw new UnsupportedOperationException();
    }

    public Object put(Object key, Object value)
    {
        throw new UnsupportedOperationException();
    }
}
