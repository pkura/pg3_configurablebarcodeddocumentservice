package pl.compan.docusafe.web.logic;

public abstract class  DockingItem 
{
	/**
	 * id elementu
	 */
	protected String id;
	/**
	 * Klucz w pliku availability.properties jaki musi byc spelniony 
	 */
	protected String availabilityKey = null;
	/**
	 * Wymagane extrasy w licencji 
	 */
	protected String extras = null;
	/**
	 * Wymagany dockind
	 */
	
	protected String dockind = null;
	
	/** 
	 * Wymagane uprawnienie do wyswietlenia
	 */
	protected String permissionCode = null;
	
	/**  
	 * Wymagane wlaczenie odpowiedniej wlasnoci w swoich preferencjach 
	 */
	
	protected String preferenceKey = null;
	/**
	 * Akcja na jaka wskazuje element
	 */
	
	protected String action;
	/**
	 * Lang key - klucz z tekstem
	 */
	protected String langKey;
	
	public abstract String getItemName();
	
	/**
	 * Metoda okresla czy element ma byc widoczny czy nie
	 * @todo - cal do zrobienia
	 * @return
	 */
	public boolean isVisible()
	{
		return true;
	}

	public String getAvailabilityKey() {
		return availabilityKey;
	}

	public void setAvailabilityKey(String availabilityKey) {
		this.availabilityKey = availabilityKey;
	}

	public String getExtras() {
		return extras;
	}

	public void setExtras(String extras) {
		this.extras = extras;
	}

	public String getDockind() {
		return dockind;
	}

	public void setDockind(String dockind) {
		this.dockind = dockind;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public String getPreferenceKey() {
		return preferenceKey;
	}

	public void setPreferenceKey(String preferenceKey) {
		this.preferenceKey = preferenceKey;
	}
}
