package pl.compan.docusafe.web.logic;

import java.util.*;
import java.io.IOException;
/** 
 * Klasa odpowiedzialna za obsluge "inteligentnych" elementow stron WWW
 * @author wkutyla
 *
 */

public class WebManager 
{
	/**
	 * Klasa zawierajaca punkty dokowania
	 */
	public static Map dockingBays = new TreeMap();
	
	/**
	 * Metoda inicjuje strukture na podstawie pliku docking-bays.xml
	 * @param is - InputStream pliku
	 * @throws IOException
	 * @todo - zrobic to
	 */
	public static void initDockingBayStructure(java.io.InputStream is) throws IOException
	{
		dockingBays = new TreeMap();
	}
}
