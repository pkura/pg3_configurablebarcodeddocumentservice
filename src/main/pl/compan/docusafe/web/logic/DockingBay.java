package pl.compan.docusafe.web.logic;

import java.util.*;
/**
 * Klasa reprezentujaca element DockingBay
 * @author wkutyla
 *
 */
@Deprecated
public class DockingBay 
{
	
	public final static String ARCHIVE_SEARCH_HEADER_MENU = "archive.search.header.menu";
	
	/** 
	 * Dokowane elementy 
	 **/
	private Map items = new TreeMap();
	/**
	 * Kod punktu dokowania
	 */
	private String dockingBayCode;
	/**
	 * Nazwa punktu dokowania
	 */
	private String dockingBayName;
	
	public Map getItems() {
		return items;
	}
	public void setItems(Map items) {
		this.items = items;
	}
	public String getDockingBayCode() {
		return dockingBayCode;
	}
	public void setDockingBayCode(String dockingBayCode) {
		this.dockingBayCode = dockingBayCode;
	}
	public String getDockingBayName() {
		return dockingBayName;
	}
	public void setDockingBayName(String dockingBayName) {
		this.dockingBayName = dockingBayName;
	}
}
