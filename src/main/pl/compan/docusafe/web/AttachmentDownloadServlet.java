package pl.compan.docusafe.web;

import org.apache.tika.io.IOUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Servlet odpowiadający za zwrócenie treści załącznika
 *
 * @author msankowski
 */
public class AttachmentDownloadServlet extends HttpServlet {
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentDownloadServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            DSApi.open(AuthUtil.getSubject(req));
            long attachmentRevisionId = Long.parseLong(req.getParameter("id"));
            String binderParam = req.getParameter("binderId");
            String asPdf = req.getParameter("asPdf");
            Long binderId = binderParam == null ? null : Long.parseLong(binderParam);
            AttachmentRevision rev = AttachmentRevision.find(attachmentRevisionId);

            if (!hasPermission(binderId, rev)){
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            if(rev instanceof JackrabbitAttachmentRevision){

            } else {
                InputStream is = null;
                try {
                    is = rev.getAttachmentStream();
                } finally {
                    IOUtils.closeQuietly(is);
                }
            }
        } catch (Throwable ex) {
            LOG.error(ex.getMessage(), ex);
            resp.sendError(500);
        } finally {
            DSApi._close();
        }
    }

    private boolean hasPermission(Long binderId, AttachmentRevision rev) throws EdmException {
        return !rev.getAttachment().isDocumentType()
                || rev.getAttachment().getDocument().canReadAttachments(binderId);
    }
}
