package pl.compan.docusafe.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;

/**
 * @author Mariusz Kilja�czyk
 * FIXME
 * @deprecated - implementacja z lekka do dupy
 */
@Deprecated
public class ShowPngService extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(ShowPngService.class);

    //trzeba zmienic zeby nie lapal wszystkich png bo to bez sensu.
    @Deprecated
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {    	
    	String path  = null;
    	InputStream is = null;
    	OutputStream os = null;
    	try
    	{
    		path = req.getRequestURI();
	        String[] tmpTab = path.split("/");
	        String fileName = tmpTab[tmpTab.length - 1];
	        String tempFile = Docusafe.getPngTempFile();	        
	        String repl = tempFile.replace("\\","/");
	        path = repl+"/"+ fileName;
	        log.trace("png service {}",path);
	        is = req.getSession().getServletContext().getResourceAsStream(path);
	        if (is == null) 
	        {
	        	log.trace("png nie znaleziony");
	        	is = new FileInputStream(path);
	        }
	        os = resp.getOutputStream();	
	        org.apache.commons.io.IOUtils.copy(is, os);        
	       
    	}
    	catch (Exception e) 
    	{
			log.debug("FileNotFoundException Nie mo�na odnale�� okre�lonego pliku " +path);
		}
    	finally    	
    	{
    		org.apache.commons.io.IOUtils.closeQuietly(os);
	        org.apache.commons.io.IOUtils.closeQuietly(is);
    	}
    }
}