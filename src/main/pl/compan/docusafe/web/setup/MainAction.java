package pl.compan.docusafe.web.setup;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.web.wizard.Wizard;
import pl.compan.docusafe.web.wizard.Wizards;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/* User: Administrator, Date: 2005-09-05 15:30:32 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MainAction.java,v 1.4 2008/10/06 10:40:31 pecet4 Exp $
 */
public class MainAction extends EventActionSupport
{
    // @EXPORT
    private Wizard wizard;
    private String homeDirectory;

    // @IMPORT
    private String wizardState;

    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);

        registerListener("doNext").
            append(new Next());
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            wizard = Wizards.getInstance().getWizard("setup");

            homeDirectory = "test";
        }
    }

    private class Next implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                wizard = Wizards.getInstance().getWizard("setup");
                wizard.setStateAsBase64(wizardState);

                wizard.advance();
                event.setResult(wizard.getCurrentPage().getId());
            }
            catch (EdmException e)
            {
            	LogFactory.getLog("eprint").debug("", e);
            }
        }
    }

    public Wizard getWizard()
    {
        return wizard;
    }
}
