package pl.compan.docusafe.web;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.rest.helpers.MultiOptional;
import pl.compan.docusafe.rest.views.AttachmentView;
import pl.compan.docusafe.service.imports.ImageImportDSIHandler;
import pl.compan.docusafe.spring.user.AttachmentService;
import pl.compan.docusafe.spring.user.AttachmentServiceInContext;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.BokAction;
import pl.compan.docusafe.web.admin.HelpSettingsAction;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * Akcja odpowiedzialna za wy�wietlanie dost�pnych plik�w pomocy
 * i mo�liwo�� ich pobrania. Pliki dziel� si� na 3 dzia�y:
 * pomoc og�lna, pomoc dedykowana i narz�dzia.
 * 
 * @author <a href="mailto:mwlizlo@com-pan.pl">Marcin Wlizlo</a>
 */
public class HelpIndexAction extends EventActionSupport
{
    private Map<String, String> general = new LinkedHashMap<String, String>();
    private Map<String, String> specific = new LinkedHashMap<String, String>();
    private Map<String, String> tools = new LinkedHashMap<String, String>();
    private StringManager sm =
        GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(),null);
    
    //private char PATH_SEPARATOR ='\\';
    private String PATH_SEPARATOR = "/";
    
    public String DEFAULT_GENERAL_FOLDER =
    	Docusafe.getHome().getAbsolutePath()+PATH_SEPARATOR+sm.getString("podreczniki_ogolne");
    public String DEFAULT_SPECIFIC_FOLDER = 
    	Docusafe.getHome().getAbsolutePath()+PATH_SEPARATOR+sm.getString("podreczniki_dedykowane");
    public String DEFAULT_TOOLS_FOLDER =
    	Docusafe.getHome().getAbsolutePath()+PATH_SEPARATOR+sm.getString("narzedzia");
    
    private int downloadType;
    private String downloadFilename;
    private String htmlHelpFile;
    
    
    public String getHtmlHelpFile() {
		return htmlHelpFile;
	}

	public void setHtmlHelpFile(String htmlHelpFile) {
		this.htmlHelpFile = htmlHelpFile;
	}

	protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doDownload").
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            append(new Download()).
            appendFinally(CloseHibernateSession.INSTANCE);
        
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                // nie mo�na obecnie ustali� r�cznie katalogu na pomoce ze wzgl�d�w bezpiecze�stwa - 
                // u�ywane domy�lne
                //Preferences pref = getPreferences();
//                String generalDir = HelpSettingsAction.DEFAULT_GENERAL_FOLDER;//pref.get("general", "");
//                String specificDir = HelpSettingsAction.DEFAULT_SPECIFIC_FOLDER;//pref.get("specific", "");
//                String toolsDir = HelpSettingsAction.DEFAULT_TOOLS_FOLDER;//pref.get("tools", "");

            	File htmlHelpF = new File(Docusafe.getHome().getAbsolutePath()+PATH_SEPARATOR+"help.htm");
            	if(htmlHelpF.exists()) {
            		htmlHelpFile = FileUtils.readFileToString(htmlHelpF);
            	}
            	
                SAXReader reader = new SAXReader();
                Document doc = null;

                File contentsFile = new File(DEFAULT_GENERAL_FOLDER + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        Element elName = elFile.element("name");
                        Element elDescription = elFile.element("description");
                        general.put(elName.getText(), elDescription.getText());
                    }
                }
                
                contentsFile = new File(DEFAULT_SPECIFIC_FOLDER + PATH_SEPARATOR +"contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        Element elName = elFile.element("name");
                        Element elDescription = elFile.element("description");
                        specific.put(elName.getText(), elDescription.getText());
                    }
                }
                
                contentsFile = new File(DEFAULT_TOOLS_FOLDER + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    doc = reader.read(contentsFile);
                    Element root = doc.getRootElement();
                    Iterator itr = root.elementIterator("file");
                    while (itr.hasNext())
                    {
                        Element elFile = (Element) itr.next();
                        Element elName = elFile.element("name");
                        Element elDescription = elFile.element("description");
                        tools.put(elName.getText(), elDescription.getText());
                    }
                }
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                LogFactory.getLog("eprint").debug("", e);
            }
        }
    }

    private class Download implements ActionListener
    {
        private boolean checkFile(String filename, Document doc)
        {
            Element root = doc.getRootElement();
            Iterator itr = root.elementIterator("file");
            while (itr.hasNext())
            {
                Element elFile = (Element) itr.next();
                Element elName = elFile.element("name");
                if (elName != null && elName.getText().equals(filename))
                {
                    return true;
                }
            }
            return false;
        }
        
        public void actionPerformed(ActionEvent event)
        {
            try
            {
                String dir = "";
                if (downloadType == HelpSettingsAction.FOLDER_TYPE_GENERAL)
                    dir = DEFAULT_GENERAL_FOLDER;
                else if (downloadType == HelpSettingsAction.FOLDER_TYPE_SPECIFIC)
                    dir = DEFAULT_SPECIFIC_FOLDER;
                else if (downloadType == HelpSettingsAction.FOLDER_TYPE_TOOLS)
                    dir = DEFAULT_TOOLS_FOLDER;
                else
                    throw new EdmException("Niew�a�ciwy typ pliku: " + downloadType);
                
                String filename = downloadFilename;
                String onServerFilename = dir + PATH_SEPARATOR + downloadFilename;

                SAXReader reader = new SAXReader();
                File contentsFile = new File(dir + PATH_SEPARATOR + "contents.xml");
                if (contentsFile.exists())
                {
                    Document doc = reader.read(contentsFile);
                    if(doc==null)
                        throw new EdmException("Nie mo�na pobra� pliku .." + filename);
                    if (!checkFile(filename, doc))
                        throw new EdmException("Nie mo�na pobra� pliku ." + filename);
                }
                else
                    throw new EdmException("Nie mo�na pobra� pliku " + filename);
                                    
                HttpServletResponse response = ServletActionContext.getResponse();
                response.setContentType("application/x-download");
                response.setHeader("Content-Disposition", "attachment; filename=" + filename);
                
                File onServerFile = new File(onServerFilename);
                response.setContentLength((int) onServerFile.length());
                
                FileInputStream in = new FileInputStream(onServerFile);
                OutputStream out = response.getOutputStream();
                byte[] buf = new byte[8192];
                int count;
                while ((count = in.read(buf)) > 0)
                {
                    out.write(buf, 0, count);
                    out.flush();
                }
                out.close();
                in.close();
            }
            catch (Exception e)
            {
                addActionError(e.getMessage());
                LogFactory.getLog("eprint").debug("", e);
            }
        }
    }

    protected Preferences getPreferences()
    {
        return DSApi.context().systemPreferences().node("modules/help/folders");
    }

    public Map<String, String> getGeneral()
    {
        return general;
    }

    public Map<String, String> getSpecific()
    {
        return specific;
    }

    public Map<String, String> getTools()
    {
        return tools;
    }

    public void setDownloadType(int downloadType)
    {
        this.downloadType = downloadType;
    }

    public int getDownloadType()
    {
        return downloadType;
    }

    public void setDownloadFilename(String downloadFilename)
    {
        this.downloadFilename = downloadFilename;
    }

    public String getDownloadFilename()
    {
        return downloadFilename;
    }

}
