package pl.compan.docusafe.web.bookmarks;

import java.security.Principal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.Subject;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BookmarkConfThread implements Runnable
{
	private Logger log = LoggerFactory.getLogger(BookmarkConfThread.class);
	private List<DSUser> users;
	private Long bookmarkId;

	public BookmarkConfThread(Long bookmarkId,List<DSUser> dsusers)
	{
		this.bookmarkId = bookmarkId;
		this.users = dsusers;
	}

	public void run()
	{
		try
		{
			for(DSUser user : users)
			{
				Set<Principal> principals = new HashSet<Principal>();
		        principals.add(new UserPrincipal(user.getName()));
		        principals.add(new RolePrincipal("admin"));
		        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
		        DSContext ctx = DSApi.open(subject);

				if(!user.isDeleted())
				{
					try
					{
						ctx.begin();
						ctx.userPreferences().node("bookmarks").putLong(bookmarkId.toString(), bookmarkId);
						ctx.commit();
					}
					catch (EdmException e)
					{
						log.error("",e);
						DSApi.context().setRollbackOnly();
					}
					DSApi.close();
				}
			}
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
}
