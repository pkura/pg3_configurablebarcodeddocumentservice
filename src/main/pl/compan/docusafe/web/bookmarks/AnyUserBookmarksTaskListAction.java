package pl.compan.docusafe.web.bookmarks;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.web.office.tasklist.TaskListAction.StringValueComparator;
/**
 * Wszystkie zakładki danego użytkownika.
 * @author przemek nowak <przemek.nowak.pl@gmail.com>
 */
public class AnyUserBookmarksTaskListAction extends BookmarksTaskListAction {
	private static final Logger log = LoggerFactory.getLogger(AnyUserBookmarksTaskListAction.class);
	private static final long serialVersionUID = 1L;

	@Override
	public String getBaseLink() {
		return "/bookmarks/any-user-bookmarks.action?bookmarkId="+getBookmarkId();
	}

	  protected DSUser getDSUser() throws EdmException
	    {
	        if (getUsername() == null)
	            return null;

	        try
	        {
	            return DSUser.findByUsername(getUsername());
	        }
	        catch (UserNotFoundException e)
	        {
	            return null;
	        }
	    }
	    
	    protected DSDivision getDSDivision() throws EdmException
	    {
	        if (getUsername() == null)
	            return null;

	        try
	        {
	            return DSDivision.findByName(getUsername());
	        }
	        catch (Exception e)
	        {
	            return null;
	        }
	    }

	    protected LinkedHashMap<String,String> getDSUserAndDivisions() throws EdmException 
	    {
	    	if(AvailabilityManager.isAvailable("dl"))
				return DLBinder.getDSUserAndDivisions();
	    	LinkedHashMap<String,String> tempMap = new LinkedHashMap<String,String>();
	    	StringValueComparator bvc =  new StringValueComparator(tempMap);
	    //	LinkedHashMap<String,String> sortedMap = new LinkedHashMap(bvc);
	        List<DSDivision> dsd = UserFactory.getInstance().getAllDivisionsDivisionType();
	    	for(DSDivision d : dsd)
	    	{   		
	    		if(!d.isHidden()) 
	    		{
	    			tempMap.put(d.getName(), d.getName());
	    		}    	
	    	}
	    	List<DSUser> dsu = UserFactory.getInstance().getCanAssignUsers();
	        for(DSUser d : dsu)
	        {
	        	tempMap.put(d.getName(),d.asLastnameFirstname());
	        }
	        dsu = null;
	        return tempMap;
//	        sortedMap.putAll(tempMap);
//	        return sortedMap;
	     }     

    protected List getDSUserList() throws EdmException {
        return UserFactory.getInstance().getCanAssignUsers();
     }

	@Override
	public boolean isOwnTasks() {
		return false;
	}
	
	@Override
	public String getListName() {
		return "any-user-bookmarks-task-list";
	}
	
}
