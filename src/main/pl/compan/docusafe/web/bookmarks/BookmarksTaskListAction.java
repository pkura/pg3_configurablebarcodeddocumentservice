package pl.compan.docusafe.web.bookmarks;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.hibernate.mapping.Column;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.core.bookmarks.FilterCondition;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.web.TableColumn;
import pl.compan.docusafe.web.office.tasklist.FilterColumn;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;

/**
 * Listy zada� - zak�adki.
 * 
 */
public class BookmarksTaskListAction extends TaskListAction
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(BookmarksTaskListAction.class);

	protected DSUser getDSUser() throws EdmException
    {
    	 return DSApi.context().getDSUser();
    }
    
    protected List getDSUserList() throws EdmException
    {
        return null;
    }

    public boolean isOwnTasks()
    {
        return true;
    }

    public String getBaseLink()
    {
        return "/bookmarks/bookmarks.action?bookmarkId="+getBookmarkId();
    }

	public void setTab(String tab) {
		super.setTab(tab);
	}
	
    public boolean isBookmarkTasklist() {
		return true;
	}
    
    protected void prepareBookmarksTasklist() {
		if (getBookmarkId()!=null) {
			try {
				bookmark = Bookmark.find(getBookmarkId());
			} catch (EdmException e) {
				log.error("",e);
			}
		}
    }
    
	protected String preparePrefix() {
		if (getBookmarkId() != null) {
			return getBookmarkId().toString();
		} else {
			return "bookmarks";
		}
	}
    
    /**
	 * Przygotowanie dodatkowych etykiet dla zak�adek.
	 */
	protected void prepareBookmarksLabels(Collection<Label> labels) {
		if (bookmark != null) {
			Long[] lab = bookmark.getLabelsArray();
			if (lab != null && lab.length > 0) {
				// je�eli ustawione labele zawieraj� etykiet� "Wszystkie"
				if (org.apache.commons.lang.ArrayUtils.contains(lab, -1L)) {
					labels = new Vector<Label>();
					return;
				}
				for(Long id : lab) {
					try {
						Label l = Label.find(id);
						// je�eli zmienimy contain na true to b�dzie mo�na usuwa� etykiety ze zdefiniowanych zak�adek
						boolean contain = false;
						for(Label la : labels) {
							if(la.getId().equals(l.getId())) contain = true;
						}
						if (!contain) {
							labels.add(l);
						}
					} catch (EdmException e) {
						log.error("",e);
					}
				}
			}
		}
		
	}
    
	public List<FilterCondition> prepareFilterConditions() {
		// tutaj przygotowanie filter conditions z wyci�gni�tych zak�adek
		List<FilterCondition> filters = new ArrayList<FilterCondition>();
		// je�eli mamy tak� zak�adk� to zwracamy jej zdefiniowane filtry
		if (bookmark != null && !bookmark.getType().equals(TAB_WATCHES) && !bookmark.getType().equals(TAB_CASES) && !bookmark.getType().equals(TAB_WATCHES)) {
			for(FilterCondition fc : bookmark.getFilters()) {
				try {
					parseFilter(fc);
					filters.add(fc);
				} catch (EdmException e) {
					log.error(BookmarksTaskListAction.class.getName()+": Nie mo�na odczyta� paramet�w filtra");
					addActionMessage("Pomini�to filtr: "+fc.getColumn()+", b��d podczas parsowania zdefiniowanych warto�ci.");
				}
			}
			return new ArrayList<FilterCondition>(filters);
		}
		
		return null;
	}
	
    /**
     * Metoda odpowiedzialna za parsowanie filtr�w.
     * @return
     */
    protected void parseFilter(FilterCondition fc) throws EdmException {
    	// to ma by� ta odpowiednia nazwa kolumny
    	String parsedFilterBy = "";
    	
        if(UNPARSED_FILTER_BY.contains(fc.getColumn())){
            parsedFilterBy = fc.getColumn();
        } else if(fc.getValues() != null && fc.getColumn() != null && !TAB_WATCHES.equals(getTab()) && !IGNORED_FILTER_BY.contains(fc.getColumn())) {
        	parsedFilterBy = fc.getColumn();
        	try
        	{
        		if(parsedFilterBy.equals("clerk"))
            	{
        			parsedFilterBy = "documentClerk";
            	}
        		else if(parsedFilterBy.equals("sender"))
            	{
        			parsedFilterBy = "activity_lastActivityUser"; // dekretujacy
            	}
            	TaskSnapshot ts = TaskSnapshot.getInstance();
	            Column dsa = DSApi.getColumn(ts.getClass(), parsedFilterBy);
	            Type t = DSApi.getColumnType(ts.getClass(), parsedFilterBy);
	            parsedFilterBy = dsa.getName();
	            
	            // tutaj nast�puje parsowanie odpowiednich warto�ci kt�re u�ytkownik wpisa� jako String
	            for(String value : fc.getValuesList()) {
	            	// dodatkowo trimujemy
	            	value = value.trim();
	            	
	            	Object filterValue = null;
	            
		            if(parsedFilterBy.equals("documentClerk") 
	            	|| parsedFilterBy.equals("documentAuthor")
	            	||parsedFilterBy.equals("document_author")
	            	||parsedFilterBy.equals("document_clerk")
	            	||parsedFilterBy.equals("dsw_process_context_param_val"))
		            {
		            	DSUser tempUser = null;
		            	String[] tmp = value.split(" ");
		            	try
		            	{
	    	            	tempUser = DSUser.findByFirstnameLastname(tmp[0], tmp[1]);
	    	            	if(tempUser == null)
	    	            	{
	    	            		tempUser = DSUser.findByFirstnameLastname(tmp[1], tmp[0]);
	    	            		if(tempUser != null)
	    	            			filterValue = tempUser.getName();
	    	            	}
	    	            	else
	    	            	{
	    	            		filterValue = tempUser.getName();
	    	            	}
		            	}
		            	catch (Exception e) 
		            	{
		            		log.error(e.getMessage(),e);		
						}
		            }
		            else if(t.getReturnedClass().equals(Integer.class))
		            {
		            	filterValue = Integer.parseInt(value);
		            }
		            else if(t.getReturnedClass().equals(Date.class))
		            {
		            	filterValue = DateUtils.parseDateTimeAnyFormat(value);
		            }
		            else if(t.getReturnedClass().equals(Timestamp.class))
		            {
		            	filterValue = DateUtils.parseDateAnyFormat(value);
		            }
		            else if(t.getReturnedClass().equals(String.class))
		            {
		            	filterValue = value;
		            }
		            else if(t.getReturnedClass().equals(Float.class))
		            {
		            	filterValue = Float.parseFloat(value);
		            }
		            else if(t.getReturnedClass().equals(Long.class))
		            {
		            	filterValue = Long.parseLong(value);
		            }
		            else
		            {
		            	filterValue = value;
		            }
		            // dodanie sparsowanej warto�ci
		            fc.getParsedValues().add(filterValue);
	            }
	            
        	}
        	catch (Exception e) 
        	{
        		log.warn(e.getMessage(),e);
        		throw new EdmException("B��d podczas parsowania filtra");
			}
        	// ustawienie odpowiedniej nazwy kolumny w bazie danych
        	fc.setColumnTableName(parsedFilterBy);
        }
    	
    }
    
    /**
     * Odczytuje kolumny dla zdefiniowanej zak�adki.
     * Je�eli nie zdefiniowano kolumn w zak�adce u�ywane s� domy�lne kolumny.
     */
    protected List<TableColumn> getActualColumns()
    {
        String[] properties;
        if(StringUtils.isEmpty(orderTab))
        {
        	orderTab = TAB_REALIZE_ORDERS; 
        }
        // domy�lna lista kolumn
        properties = StringUtils.join(TaskListUtils.getDefaultColumnProperties(getTab(), getOrderTab()), ",").split(",");
        List<TableColumn> actualColumns = new ArrayList<TableColumn>();
        if (bookmark != null && bookmark.getColumnsList() != null && bookmark.getColumnsList().size() > 0 && !TAB_ORDER.equals(bookmark.getType())) {
//        	String[] bookmarkColumns = (String[])bookmark.getColumnsList().toArray();
        	String[] bookmarkColumns = bookmark.getColumnsForUser();
        	
        	for (int i=0; i < bookmarkColumns.length; i++)
	        {
	            actualColumns.add(new TableColumn(bookmarkColumns[i],
	                TaskListUtils.getColumnDescription(transformOrderProperty(bookmarkColumns[i]), bookmark.getCn()),
	                getBaseLink() + "&username="+getUsername() +
	                    "&tab="+getTab()+
	                    "&sortField="+("shortReceiveDate".equals(bookmarkColumns[i]) ? "receiveDate" : bookmarkColumns[i]) +
	                    "&ascending=false&withBackup="+getWithBackup()+
	                    "&filterBy="+getFilterBy()+
	                    "&filterName="+getFilterName(),
	                    
	                getBaseLink() + "&username="+getUsername() +
	                    "&tab="+getTab()+
	                    "&sortField="+("shortReceiveDate".equals(bookmarkColumns[i]) ? "receiveDate" : bookmarkColumns[i]) +
	                    "&ascending=true" +
	                    "&withBackup="+getWithBackup()+
	                    "&filterBy="+getFilterBy()+
	                    "&filterName="+getFilterName()
	                    ,""));
	        }
        } else {
	        for (int i=0; i < properties.length; i++)
	        {
	            actualColumns.add(new TableColumn(properties[i],
	                TaskListUtils.getColumnDescription(transformOrderProperty(properties[i]), null),
	                getBaseLink() + "&username="+getUsername() +
	                    "&tab="+getTab()+
	                    "&sortField="+("shortReceiveDate".equals(properties[i]) ? "receiveDate" : properties[i]) +
	                    "&ascending=false&withBackup="+getWithBackup()+
	                    "&filterBy="+getFilterBy()+
	                    "&filterName="+getFilterName(),
	                    
	                getBaseLink() + "&username="+getUsername() +
	                    "&tab="+getTab()+
	                    "&sortField="+("shortReceiveDate".equals(properties[i]) ? "receiveDate" : properties[i]) +
	                    "&ascending=true" +
	                    "&withBackup="+getWithBackup()+
	                    "&filterBy="+getFilterBy()+
	                    "&filterName="+getFilterName()
	
	                    ,""));
	        }
        }

        return actualColumns;
    }
    
    protected List<FilterColumn> getFilterColumn()
    {
    	List<FilterColumn> result = new ArrayList<FilterColumn>();
    	// domy�lne kolumny z typu pisma
    	String[] properties = StringUtils.join(TaskListUtils.getDefaultColumnProperties(getTab(), getOrderTab()), ",").split(",");
    	
    	String[] availableProperties = StringUtils.join(TaskListUtils.getDefaultColumnProperties("filter", null), ",").split(",");
    	Map<String,String> availablePropertiesMap = new TreeMap<String, String>();
    	
    	for (int i = 0; i < availableProperties.length; i++) 
    	{
    		availablePropertiesMap.put(availableProperties[i],availableProperties[i]);
    	}
    	
    	result.add(new FilterColumn("Brak",sm.getString("Brak")));
    	result.add(new FilterColumn("documentOfficeNumber", TaskListUtils.getColumnDescription("documentOfficeNumber", bookmark.getCn())));

    	if (bookmark != null && bookmark.getColumnsList() != null && bookmark.getColumnsList().size() > 0) {
        	// przygotowanie kolumn do filtrowania wg zdefiniowanych w filtrze
//    		String[] bookmarkColumns = (String[])bookmark.getColumnsList().toArray();
    		String[] bookmarkColumns = bookmark.getColumnsForUser();
        	for (int i = 0; i < bookmarkColumns.length; i++) 
        	{
        		if(availablePropertiesMap.containsKey(bookmarkColumns[i]) && !"documentOfficeNumber".equals(bookmarkColumns[i]))
        			result.add(new FilterColumn(bookmarkColumns[i], TaskListUtils.getColumnDescription(bookmarkColumns[i],bookmark.getCn())));
        	}        
        } else {
        	for (int i = 0; i < properties.length; i++) 
        	{
        		if(availablePropertiesMap.containsKey(properties[i]) && !"documentOfficeNumber".equals(properties[i]))
        			result.add(new FilterColumn(properties[i], TaskListUtils.getColumnDescription(properties[i], null)));
        	}        	
        }
    	
        PermissionCache cache = null;
        try {
            cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            if(cache.hasPermission(DSApi.context().getPrincipalName(), DSPermission.TASK_COUNT.getName())){
                result.add(new FilterColumn("task_count_1", "Tylko przypisane zadania"));
                result.add(new FilterColumn("task_count_2_or_more", "Tylko wsp�lne zadania"));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

		return result;
    }
    
	public Boolean getShowNumberNewTask() {
		return true;
	}
	
	@Override
	protected boolean getSortOrder()
	{
		return Boolean.parseBoolean(DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
            	get(BookmarkManager.BOOKMARKS_ASCENDING+getBookmarkId(), bookmark != null && bookmark.getAscending()!=null?bookmark.getAscending().toString():"false"));
	}
	
	@Override
	protected String getDefaultSortField()
	{
		return DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
			get(BookmarkManager.BOOKMARKS_SORT_COLUMN+getBookmarkId(), bookmark!=null?bookmark.getSortField():"receiveDate");
	}
	
	@Override
	protected Integer getDefaultLimit()
	{
		return DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
    		getInt(BookmarkManager.BOOKMARKS_TASK_COUNT+getBookmarkId(),bookmark != null?bookmark.getTaskCount():20);
	}

	@Override
	public String getListName() {
		return "bookmarks";
	}

	@Override
	protected DSDivision getDSDivision() throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}
    
}