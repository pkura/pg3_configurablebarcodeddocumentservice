package pl.compan.docusafe.web.bookmarks;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.bookmarks.Bookmark;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Tab;
import pl.compan.docusafe.web.common.Tabs;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.web.office.tasklist.TaskListTabAction;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Klasa odpowiada za przygotowanie odpowiednich tab�w na podstawie zdefiniowanych zak�adek.
 * @author PN
 */
public class TasklistBookmarkTab {
	private static final Logger log = LoggerFactory.getLogger(TasklistBookmarkTab.class);
	StringManager smTab = GlobalPreferences.loadPropertiesFile("","tab");
	
	private Bookmark bookmark;
	private String tab;
	private Long bookmarkId;
	private Tabs tabs;
	private String username;
	private TaskListTabAction base;
	private String sortField;
	private Boolean ascending;
	private BookmarkManager bm;
	
	private String orderTab;
	private Tabs orderTabs;
	
	private static String TAB_IN = TaskListAction.TAB_IN;
	private static String TAB_OUT = TaskListAction.TAB_OUT;
	private static String TAB_INT = TaskListAction.TAB_INT;
	private static String TAB_ORDER = TaskListAction.TAB_ORDER;
	private static String TAB_WATCHES = TaskListAction.TAB_WATCHES;
	private static String TAB_CALENDAR = TaskListAction.TAB_CALENDAR;
	private static String TAB_MY_ORDERS = TaskListAction.TAB_MY_ORDERS;
	private static String TAB_REALIZE_ORDERS = TaskListAction.TAB_REALIZE_ORDERS;
	private static String TAB_WATCH_ORDERS = TaskListAction.TAB_WATCH_ORDERS;
	private static String TAB_CASES = TaskListAction.TAB_CASES;
    public static  String TAB_DOCUMENT_PACKAGE = TaskListAction.TAB_DOCUMENT_PACKAGE;
	
	public TasklistBookmarkTab(TaskListTabAction base)
	{
		this.tab = base.getTab();
		this.orderTab = base.getOrderTab();
		this.username = base.getUsername();;
		this.base = base;
		this.sortField = base.getSortField();
		this.ascending = base.getAscending();
		this.bookmarkId = base.getBookmarkId();
		bm = new BookmarkManager();
	}
	
	public void initTab()
	{
		Long tmpBookmarkId = null;
		boolean otherBookmarks = false;
		
		try 
		{
			otherBookmarks = username != null && !username.equals("null"); 
    		Long sessionBookmarkId = (Long)ServletActionContext.getRequest().getSession().getAttribute("bookmarkId");
    		Map<Long, Bookmark> bookmarks = bm.getVisibleBookmarksForUser(username);

    		if (bookmarks.size()>0) tmpBookmarkId = bookmarks.keySet().iterator().next(); 
    		
			if (bookmarkId==null) {
				if (otherBookmarks || sessionBookmarkId==null) {
					bookmarkId = tmpBookmarkId;
				}
				else {
					bookmarkId = sessionBookmarkId;
				}
			}
			
			// w przypadku kiedy zaznaczamy kalendarz nie mo�emy wybra� zadnej zakladki, zaznaczamy kalendarz
			if (TAB_CALENDAR.equals(tab) || "assignmentJournalIn".equals(tab) || "assignmentJournalOut".equals(tab))
				bookmarkId = null;
			
			// tutaj na wszelki wypadek sprawdzenie czy znaleziono zak�adk� o podanym id
			if (bookmarkId != null) 
			{
				try {
					bookmark = Bookmark.find(bookmarkId);
				} catch (Exception e) {
					tab = TAB_IN;
					log.error("",e);
					bookmarkId = tmpBookmarkId;
				}
			}
			
			base.setBookmarkId(bookmarkId);
			
			if (bookmark != null && bookmark.getType() != null)
				tab = bookmark.getType();
			
			if (!TAB_IN.equals(tab) && !TAB_OUT.equals(tab) &&
					!TAB_INT.equals(tab) && !TAB_WATCHES.equals(tab) && !TAB_CASES.equals(tab) 
					&& !TAB_ORDER.equals(tab) && !TAB_CALENDAR.equals(tab) && !TAB_DOCUMENT_PACKAGE.equals(tab)
                    && !"assignmentJournalIn".equals(tab) && !"assignmentJournalOut".equals(tab))
			{
				if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
					tab = TAB_IN;
				else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
					tab = TAB_INT;
				else if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace"))
					tab = TAB_OUT;
			}

			base.setTab(tab);
			tabs = new Tabs(6);
			
	         String link;
	         String tmp = base.getBaseLink().split("\\?")[0];
	         if (username != null)
	         {
	             link = tmp +
	                 "?username="+username+
	                 (sortField != null ? "&sortField="+sortField : "") +
	                 "&ascending="+ascending;
	         }
	         else
	         {
	             link = tmp + "?a=a";
	         }
	         
	         for (Bookmark b : bookmarks.values()) {
	        	 tabs.add(new TabEx(b.getName(), b.getTitle(),
			                link+"&tab="+b.getType()+"&bookmarkId="+b.getId(), b.getId().equals(bookmarkId), b.getType(),b.getId()));
	         }
	 
	         if (TAB_ORDER.equals(tab))
	         {
	             if (!TAB_MY_ORDERS.equals(orderTab) && !TAB_REALIZE_ORDERS.equals(orderTab) && !TAB_WATCH_ORDERS.equals(orderTab))
	                 orderTab = TAB_REALIZE_ORDERS;

	             orderTabs = new Tabs(3);

	             orderTabs.add(new Tab(smTab.getString("WydanePolecenia"), smTab.getString("WydanePolecenia"),
	                 link+"&bookmarkId="+bookmarkId+"&tab="+TAB_ORDER+"&orderTab="+TAB_MY_ORDERS, TAB_MY_ORDERS.equals(orderTab)));
	             orderTabs.add(new Tab(smTab.getString("RealizowanePolecenia"), smTab.getString("RealizowanePolecenia"),
	                 link+"&bookmarkId="+bookmarkId+"&tab="+TAB_ORDER+"&orderTab="+TAB_REALIZE_ORDERS, TAB_REALIZE_ORDERS.equals(orderTab)));
	             orderTabs.add(new Tab(smTab.getString("PrzekazanePolecenia"), smTab.getString("PrzekazanePolecenia"),
	                 link+"&bookmarkId="+bookmarkId+"&tab="+TAB_ORDER+"&orderTab="+TAB_WATCH_ORDERS, TAB_WATCH_ORDERS.equals(orderTab)));
	         }
	         
	         if (Configuration.additionAvailable(Configuration.ADDITION_CALENDAR) && !otherBookmarks)
	         {
	        	 try
	        	 {
	        		 tabs.add(new Tab(smTab.getString("Kalendarz"), smTab.getString("ZarzadzanieTerminami"),
	        				 "/office/tasklist/calendar-own.action?fromBookmarks=true&daily=true"
	        				 +"&tab="+TAB_CALENDAR+
	        				 (base.isOwnTasks()?"":("&username=" + DSApi.context().getDSUser().getName())), TAB_CALENDAR.equals(tab)));
	        	 }
	        	 catch (Exception e)
	        	 {
	        		 base.addActionError(e.getMessage());
	        	 }
	         }

            if(AvailabilityManager.isAvailable("assignment.journal")) {
                tabs.add(new Tab(smTab.getString("AssignmentJournalIn"),smTab.getString("AssignmentJournalIn"),"/office/tasklist/assignment-journal.action?type=in","assignmentJournalIn".equals(tab)));
                tabs.add(new Tab(smTab.getString("AssignmentJournalOut"),smTab.getString("AssignmentJournalOut"),"/office/tasklist/assignment-journal.action?type=out","assignmentJournalOut".equals(tab)));
            }
	         
	         base.setOrderTab(orderTabs);
	         base.setTabs(tabs);
	         
		} catch (Exception e ) {
			log.error("",e);
		}
    
	}
	
    class TabEx extends Tab
    {
    	public String idName;
    	public Long idbookamrk;
    	
    	private TabEx() {
    		super("", "");
    	}
    	
    	TabEx(String name, String title, String link, boolean selected, String idName,Long idbokamrk)
    	{
    		super(name, title, link, selected);
    		this.idName = idName;
    		this.idbookamrk = idbokamrk;
    	}
    }
	
}