package pl.compan.docusafe.web.bookmarks;

import java.security.Principal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.Subject;

import org.apache.commons.lang.ArrayUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.gson.Gson;

public class BookmarkRemoveThread implements Runnable
{
	private Logger log = LoggerFactory.getLogger(BookmarkRemoveThread.class);
	private List<DSUser> users;
	private Long bookmarkId;

	public BookmarkRemoveThread(Long bookmarkId, List<DSUser> users)
	{
		this.bookmarkId = bookmarkId;
		this.users = users;
	}

	public void run()
	{
		try
		{
			for(DSUser user : users)
			{
				Set<Principal> principals = new HashSet<Principal>();
		        principals.add(new UserPrincipal(user.getName()));
		        principals.add(new RolePrincipal("admin"));
		        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
		        DSContext ctx = DSApi.open(subject);

				if(!user.isDeleted())
				{
					try
					{
						ctx.begin();
						Gson g = new Gson();
						String uIds = DSApi.context().userPreferences().node("bookmarks").get("list", "");
		        		
		        		Long[] userIds = null;
		        		if (!uIds.equals("")) {
		        			userIds = g.fromJson(uIds, Long[].class);
		        		}
	        			if (userIds != null && ArrayUtils.contains(userIds, bookmarkId)) {
		        			userIds = (Long[])org.apache.commons.lang.ArrayUtils.removeElement(userIds, bookmarkId);
		        			DSApi.context().userPreferences().node("bookmarks").put("list", g.toJson(userIds));
	        			}
						ctx.commit();
					}
					catch (EdmException e)
					{
						log.error("",e);
						DSApi.context().setRollbackOnly();
					}
					DSApi.close();
				}
			}
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
}
