package pl.compan.docusafe.web.machinetree;


import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.http.HttpSession;

import org.bouncycastle.asn1.x509.DSAParameter;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.parametrization.yetico.tree.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class ShowTreeAction extends EventActionSupport{
	
	private String machineTree;
	private Integer elementType;
	private Long elementId;
	private Integer parentType;
	private Integer level;
	private Long parentId;
	private String itemForm;
	private String infoForm;
	private String addNewForm;
	private String kod, nazwa, kind;
	private ArrayList<MachineElement> searchResults;
    private ArrayList<String> searchResults2;


    public static Logger log = LoggerFactory.getLogger(ShowTreeAction.class);

	@Override
	protected void setup() {
		
		FillForm fillForm = new FillForm();
		
		registerListener(DEFAULT_ACTION).
        append(OpenHibernateSession.INSTANCE).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doExpand").
        append(OpenHibernateSession.INSTANCE).
        append(new Expand()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doInfo").
        append(OpenHibernateSession.INSTANCE).
        append(new Expand()).
        append(new Info()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doAddForm").
        append(OpenHibernateSession.INSTANCE).
        append(new Expand()).
        append(new AddForm()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doEditForm").
        append(OpenHibernateSession.INSTANCE).
        append(new Expand()).
        append(new EditForm()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doAdd").
        append(OpenHibernateSession.INSTANCE).
        append(new AddItem()).
        append(fillForm).
        append(new Expand()).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doEdit").
        append(OpenHibernateSession.INSTANCE).
        append(new Expand()).
        append(new EditItem()).
        append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doAddNew").
		append(OpenHibernateSession.INSTANCE).
		append(fillForm).
		append(new AddNewForm()).
        appendFinally(CloseHibernateSession.INSTANCE);
		
		registerListener("doSearch").
		append(OpenHibernateSession.INSTANCE).
		append(new SearchElement()).
		append(fillForm).
        appendFinally(CloseHibernateSession.INSTANCE);

        registerListener("doRemove").
        append(OpenHibernateSession.INSTANCE).
        append(new RemoveItem()).
        append(fillForm).
        append(new Expand()).
        appendFinally(CloseHibernateSession.INSTANCE);

	}
	
	private class FillForm implements ActionListener
    {

		@Override
		public void actionPerformed(ActionEvent event){
			
			if(elementId!=null)
			{
				machineTree = MachineTree.newExpandedTree(elementId).generateTree();
				event.setAttribute("tree", machineTree);
			}

			else
			{	
				machineTree = MachineTree.newTree().generateTree();
				event.setAttribute("tree", machineTree);
			}
			elementType=null;
			elementId=null;
			parentType=null;
			level=null;
			
		}
		
    }
	
	private class SearchElement implements ActionListener
    {

		@Override
		public void actionPerformed(ActionEvent event){
			try
			{
			//	String rodzaj = null;
				if(kind!=null) {

                    Integer tempInt = Integer.parseInt(kind);
                    Class clazz = null;
                    switch (tempInt) {
                        case 1:
                            clazz = Zaklad.class; break;//cri.add(Restrictions.eq("typ", "LOC")); break;
                        case 2:
                            clazz = RodzajMaszyny.class; break;//cri.add(Restrictions.eq("typ", "KIND")); break;
                        case 3:
                            clazz = Maszyna.class;break;//cri.add(Restrictions.eq("typ", "MACHINE")); break;
                        case 4:
                            clazz = Podzespol.class;break;//cri.add(Restrictions.eq("typ", "COMPONENT")); break;
                        case 5:
                            clazz = Czesc.class;break;//cri.add(Restrictions.eq("typ", "PART")); break;
                        case 6:
                            clazz = CzescZamienna.class;break;//cri.add(Restrictions.eq("typ", "SPARE")); break;
                    }

                    Criteria cri = DSApi.context().session().createCriteria(clazz);

                    if (kod != null) cri.add(Restrictions.like("kod", "%" + kod + "%"));
                    if (nazwa != null) cri.add(Restrictions.like("nazwa", "%" + nazwa + "%"));
                    searchResults = (ArrayList<MachineElement>) cri.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

                }
			}
			catch(Exception e)
			{
				log.error(e.getMessage());
			}
		}	
    }
	
	private class AddNewForm implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event) {
			log.error("robie addNew !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			if(ServletActionContext.getRequest().getParameter("kind")!=null){
				
				Integer kind = Integer.parseInt(ServletActionContext.getRequest().getParameter("kind"));
				if(ServletActionContext.getRequest().getParameter("parentId")!=null)
				{
					Long parentId = Long.parseLong(ServletActionContext.getRequest().getParameter("parentId"));
					addNewForm= MachineDictionary.getInstance().getForm(kind, null, null, null, parentId);
				}
				else addNewForm= MachineDictionary.getInstance().getForm(kind, null, null, null, null);
			}
			else
			addNewForm = MachineDictionary.getInstance().getAddNewForm(null);
			log.error("robie addNew !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + addNewForm);
		}
		
	}
	
	private class Info implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event) {
			try{
			if(elementId !=null && elementId>0)
			{
				log.error("ROBIE INFO!!!!!!!!!!!!!!!!!");
				Long id = Long.parseLong(ServletActionContext.getRequest().getParameter("id"));
				
				infoForm=MachineDictionary.getInstance().getInfo(elementType, id, level, parentType, parentId);
				log.error("!!!!!!!!!!!INFO FORM" + infoForm);
			} 
			}catch(Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
		
	}
	
	private class Expand implements ActionListener
    {
 
		@Override
		public void actionPerformed(ActionEvent event) {
			if(elementId==null && ServletActionContext.getRequest().getParameter("id") != null) elementId   = Long.parseLong(ServletActionContext.getRequest().getParameter("id"));
			if(elementType==null && ServletActionContext.getRequest().getParameter("type") != null) elementType   = Integer.parseInt(ServletActionContext.getRequest().getParameter("type"));
		}
		
    }
	
	private class AddItem implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent event) {
			log.error("XXXXXXXXXXXXXX: robie add item");
			if(ServletActionContext.getRequest().getParameter("type")!=null)elementType=Integer.parseInt(ServletActionContext.getRequest().getParameter("type"));
			if(ServletActionContext.getRequest().getParameter("type")!=null)elementType=Integer.parseInt(ServletActionContext.getRequest().getParameter("type"));
			if(elementType!=null && elementType>0)
			{
				
				try{
					DSApi.context().begin();
				switch(elementType){
                 case MachineTreeModel.MACHINE_TREE_LOC:
                    log.error("XXXXXXXXXXXXXXXXX: robie add item zak�ad");
                    Zaklad zaklad = new Zaklad();
                    zaklad.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
                    DSApi.context().session().save(zaklad);
                    DSApi.context().session().flush();
                    elementId=zaklad.getId();
                    log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                    break;
				case MachineTreeModel.MACHINE_TREE_KIND:
					log.error("XXXXXXXXXXXXXXXXX: robie add item rodzaj maszyny");
                    Zaklad zakladparent = (Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", parentId)).list().get(0);
					RodzajMaszyny rodzaj = new RodzajMaszyny();
					rodzaj.setKod(ServletActionContext.getRequest().getParameter("kod"));
					rodzaj.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
                    rodzaj.setZaklad(zakladparent);
                    zakladparent.getRodzajMaszyny().add(rodzaj);
                    DSApi.context().session().saveOrUpdate(rodzaj);
                    DSApi.context().session().saveOrUpdate(zakladparent);
					DSApi.context().session().flush();
					elementId=rodzaj.getId();
                    parentId=zakladparent.getId();
					log.error("XXXXXXXXXXXXXXtest id=" + elementId);
					break;
				case MachineTreeModel.MACHINE_TREE_MACHINE:
					RodzajMaszyny parent = (RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", parentId)).list().get(0);
					Maszyna maszyna = new Maszyna();
					maszyna.setKod(ServletActionContext.getRequest().getParameter("kod"));
					maszyna.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
					maszyna.setModel(ServletActionContext.getRequest().getParameter("model"));
					maszyna.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					maszyna.setNrSeryjny(ServletActionContext.getRequest().getParameter("nrSeryjny"));
					maszyna.setProducent(ServletActionContext.getRequest().getParameter("producent"));
					maszyna.setRodzaj(parent);
					parent.getMaszyny().add(maszyna);
					DSApi.context().session().saveOrUpdate(maszyna);
					DSApi.context().session().saveOrUpdate(parent);
					DSApi.context().session().flush();
					elementId=maszyna.getId();
					parentId=parent.getId();
					break;
				case MachineTreeModel.MACHINE_TREE_COMPONENT:
					Maszyna maszynaParent = (Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", parentId)).list().get(0);
					Podzespol podzespol = new Podzespol();
					podzespol.setKod(ServletActionContext.getRequest().getParameter("kod"));
					podzespol.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					podzespol.setMaszyna(maszynaParent);
					maszynaParent.getPodzespoly().add(podzespol);
					DSApi.context().session().saveOrUpdate(podzespol);
					DSApi.context().session().saveOrUpdate(maszynaParent);
					DSApi.context().session().flush();
					elementId=podzespol.getId();
					parentId=maszynaParent.getId();
					break;
				case MachineTreeModel.MACHINE_TREE_PART:
					Podzespol podzespolParent = (Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", parentId)).list().get(0);
					Czesc czesc = new Czesc();
					czesc.setKod(ServletActionContext.getRequest().getParameter("kod"));
					czesc.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					czesc.setOpis(ServletActionContext.getRequest().getParameter("opis"));
					czesc.setRodzajczesci(RodzajCzesci.valueOf(ServletActionContext.getRequest().getParameter("rodzaj")));
					czesc.setPodzespol(podzespolParent);
					podzespolParent.getCzesci().add(czesc);
					DSApi.context().session().saveOrUpdate(czesc);
					DSApi.context().session().saveOrUpdate(podzespolParent);
					DSApi.context().session().flush();
					elementId=czesc.getId();
					parentId=podzespolParent.getId();
					break;
					
			     /*case MachineTreeModel.MACHINE_TREE_SPARE_PART:
					Czesc czescParent = (Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", parentId)).list().get(0);
					CzescZamienna czescZamienna = new CzescZamienna();
					czescZamienna.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					czescZamienna.setProducent(ServletActionContext.getRequest().getParameter("producent"));
					if(ServletActionContext.getRequest().getParameter("strategiczna").equals("on"))
						czescZamienna.setStrategiczna(true);
					else 
						czescZamienna.setStrategiczna(false);
					czescZamienna.setNrSeryjny(ServletActionContext.getRequest().getParameter("nrSeryjny"));
					czescZamienna.setZdjecie(ServletActionContext.getRequest().getParameter("zdjecie"));
					czescZamienna.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));

					if(czescZamienna.getCzesci()==null) czescZamienna.setCzesci(new HashSet<Czesc>());
					czescZamienna.getCzesci().add(czescParent);
					if(czescParent.getCzesciZamienne()==null) czescParent.setCzesciZamienne(new HashSet<CzescZamienna>());
					czescParent.getCzesciZamienne().add(czescZamienna);
					DSApi.context().session().saveOrUpdate(czescZamienna);
					DSApi.context().session().saveOrUpdate(czescParent);
//					DSApi.context().session().flush();
					elementId=czescZamienna.getId();
					parentId=czescParent.getId();
					break; */

                    case MachineTreeModel.MACHINE_TREE_SPARE_PART:
                        Czesc czescParent = (Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", parentId)).list().get(0);
                        CzescZamienna czescZamienna = new CzescZamienna();
                        czescZamienna.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
                        czescZamienna.setProducent(ServletActionContext.getRequest().getParameter("producent"));
                        if(ServletActionContext.getRequest().getParameter("strategiczna").equals("on"))
                            czescZamienna.setStrategiczna(true);
                        else
                            czescZamienna.setStrategiczna(false);
                        czescZamienna.setNrSeryjny(ServletActionContext.getRequest().getParameter("nrSeryjny"));
                        czescZamienna.setZdjecie(ServletActionContext.getRequest().getParameter("zdjecie"));
                        czescZamienna.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
                        czescZamienna.setCzesci(czescParent);
                        czescParent.getCzesciZamienne().add(czescZamienna);
                        DSApi.context().session().saveOrUpdate(czescZamienna);
                        DSApi.context().session().saveOrUpdate(czescParent);
//					DSApi.context().session().flush();
                        elementId=czescZamienna.getId();
                        parentId=czescParent.getId();
                        break;
				}
				DSApi.context().commit();
				DSApi.context().session().flush();
				itemForm=MachineDictionary.getInstance().getForm(elementType, null, level, parentType, parentId);
			}catch(Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}

		}
    }
    private class RemoveItem implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event) {
            log.error("XXXXXXXXXXXXXX: robie remove item");
            if(ServletActionContext.getRequest().getParameter("type")!=null)elementType=Integer.parseInt(ServletActionContext.getRequest().getParameter("type"));
            if(ServletActionContext.getRequest().getParameter("id")!=null) elementId= Long.valueOf(Integer.parseInt(ServletActionContext.getRequest().getParameter("id")));
            if(elementType!=null && elementType>0)
            {

                try{
                    DSApi.context().begin();
                    switch(elementType) {


                        case MachineTreeModel.MACHINE_TREE_KIND:
                            log.error("XXXXXXXXXXXXXXXXX: robie remove item rodzaj maszyny");
                            Zaklad zakladparent = (Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", parentId)).list().get(0);
                            RodzajMaszyny rodzaj = (RodzajMaszyny)DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", elementId)).list().get(0);
                            zakladparent.getRodzajMaszyny().remove(rodzaj);
                            pl.compan.docusafe.core.Persister.delete(rodzaj);
                           // DSApi.context().session().saveOrUpdate(rodzaj);
                           DSApi.context().session().delete(rodzaj);
                          //  DSApi.context().session().saveOrUpdate(zakladparent);
                           // DSApi.context().session().flush();
                          elementId = zakladparent.getId();
                         //  parentId = Long.valueOf(zakladparent.getParentId());
                            log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                            break;

                        case MachineTreeModel.MACHINE_TREE_MACHINE:
                            log.error("XXXXXXXXXXXXXXXXX: robie remove item rodzaj maszyny");
                            RodzajMaszyny rodzajparent = (RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", parentId)).list().get(0);
                            Maszyna maszyna = (Maszyna)DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", elementId)).list().get(0);
                            rodzajparent.getMaszyny().remove(maszyna);
                            pl.compan.docusafe.core.Persister.delete(maszyna);
                            // DSApi.context().session().saveOrUpdate(rodzaj);
                            DSApi.context().session().delete(maszyna);
                            //  DSApi.context().session().saveOrUpdate(zakladparent);
                            // DSApi.context().session().flush();
                            elementId = rodzajparent.getId();
                            //  parentId = Long.valueOf(zakladparent.getParentId());
                            log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                            break;

                        case MachineTreeModel.MACHINE_TREE_COMPONENT:
                            log.error("XXXXXXXXXXXXXXXXX: robie remove item podzespo�y maszyny");
                            Maszyna maszynaparent = (Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", parentId)).list().get(0);
                            Podzespol podzespol = ( Podzespol)DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", elementId)).list().get(0);
                            maszynaparent.getPodzespoly().remove(podzespol);
                            pl.compan.docusafe.core.Persister.delete(podzespol);
                            // DSApi.context().session().saveOrUpdate(rodzaj);
                            DSApi.context().session().delete(podzespol);
                            //  DSApi.context().session().saveOrUpdate(zakladparent);
                            // DSApi.context().session().flush();
                            elementId = maszynaparent.getId();
                            //  parentId = Long.valueOf(zakladparent.getParentId());
                            log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                            break;
                        case MachineTreeModel.MACHINE_TREE_PART:
                            log.error("XXXXXXXXXXXXXXXXX: robie remove item czesci maszyny");
                            Podzespol podzespolparent = (Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", parentId)).list().get(0);
                            Czesc czesc = (Czesc)DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", elementId)).list().get(0);
                            podzespolparent.getCzesci().remove(czesc);
                            pl.compan.docusafe.core.Persister.delete(czesc);
                            // DSApi.context().session().saveOrUpdate(rodzaj);
                            DSApi.context().session().delete(czesc);
                            //  DSApi.context().session().saveOrUpdate(zakladparent);
                            // DSApi.context().session().flush();
                            elementId = podzespolparent.getId();
                            //  parentId = Long.valueOf(zakladparent.getParentId());
                            log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                            break;
                        case MachineTreeModel.MACHINE_TREE_SPARE_PART:
                            log.error("XXXXXXXXXXXXXXXXX: robie remove item czesci zamienne maszyny");
                            Czesc czescparent = (Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", parentId)).list().get(0);
                            CzescZamienna czesczamienna = (CzescZamienna)DSApi.context().session().createCriteria(CzescZamienna.class).add(Restrictions.eq("id", elementId)).list().get(0);
                            czescparent.getCzesciZamienne().remove(czesczamienna);
                            pl.compan.docusafe.core.Persister.delete(czesczamienna);
                            // DSApi.context().session().saveOrUpdate(rodzaj);
                            DSApi.context().session().delete(czesczamienna);
                            //  DSApi.context().session().saveOrUpdate(zakladparent);
                            // DSApi.context().session().flush();
                            elementId = czescparent.getId();
                            //  parentId = Long.valueOf(zakladparent.getParentId());
                            log.error("XXXXXXXXXXXXXXtest id=" + elementId);
                            break;


                    }













                    DSApi.context().commit();
                DSApi.context().session().flush();
               // itemForm=MachineDictionary.getInstance().getForm(elementType, null, level, parentType, parentId);
                    MachineDictionary.getInstance();
                }catch(Exception e)
                {
                    log.error(e.getMessage(), e);
                }
            }

        }
    }
	
	private class AddForm implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent event) {
			if(elementType!=null && elementId !=null && elementType>0 && elementId>0)
			{
				log.error("YYYYYYYYYYYYYYY test parentId=" + parentId);
				if(ServletActionContext.getRequest().getParameter("addType")!=null){
					Integer temp = Integer.parseInt(ServletActionContext.getRequest().getParameter("addType"));
					Integer tempParent = Integer.parseInt(ServletActionContext.getRequest().getParameter("prentType"));
//					Integer tempParId = Integer.parseInt(ServletActionContext.getRequest().getParameter(""));
//					switch(tempParent){
//					case MachineTreeModel.MACHINE_TREE_KIND
//					}
					itemForm=MachineDictionary.getInstance().getForm(temp, null, level, tempParent, parentId);
				}
				
				else
				itemForm=MachineDictionary.getInstance().getForm(elementType, null, level, parentType, parentId);
			}
		}
    }
	
	private class EditItem implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent event) {
			if(elementType!=null && elementId !=null && elementType>0 && elementId>0)
			{
				log.error("XXXXXXXXXXXXXXXXX: robie edit item");
				try{
					DSApi.context().begin();
				switch(elementType){
                    case MachineTreeModel.MACHINE_TREE_LOC:

                        Zaklad zaklad = (Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", elementId)).list().get(0);
                        zaklad.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
                        DSApi.context().session().saveOrUpdate(zaklad);
                        break;
                    case MachineTreeModel.MACHINE_TREE_KIND:
                        RodzajMaszyny rodzaj = (RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", elementId)).list().get(0);
                        rodzaj.setKod(ServletActionContext.getRequest().getParameter("kod"));
                        rodzaj.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
                        DSApi.context().session().saveOrUpdate(rodzaj);
                        break;
				case MachineTreeModel.MACHINE_TREE_MACHINE:
					Maszyna maszyna = (Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", elementId)).list().get(0);
					maszyna.setKod(ServletActionContext.getRequest().getParameter("kod"));
					maszyna.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
					maszyna.setModel(ServletActionContext.getRequest().getParameter("model"));
					maszyna.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					maszyna.setNrSeryjny(ServletActionContext.getRequest().getParameter("nrSeryjny"));
					maszyna.setProducent(ServletActionContext.getRequest().getParameter("producent"));
					DSApi.context().session().saveOrUpdate(maszyna);
					break;
				case MachineTreeModel.MACHINE_TREE_COMPONENT:
					Podzespol podzespol = (Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", elementId)).list().get(0);
					podzespol.setKod(ServletActionContext.getRequest().getParameter("kod"));
					podzespol.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					DSApi.context().session().saveOrUpdate(podzespol);
					break;
				case MachineTreeModel.MACHINE_TREE_PART:
					Czesc czesc = (Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", elementId)).list().get(0);
					czesc.setKod(ServletActionContext.getRequest().getParameter("kod"));
					czesc.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					czesc.setOpis(ServletActionContext.getRequest().getParameter("opis"));
					czesc.setRodzajczesci(RodzajCzesci.valueOf(ServletActionContext.getRequest().getParameter("rodzajCzesci")));
					DSApi.context().session().saveOrUpdate(czesc);
					break;
					
				case MachineTreeModel.MACHINE_TREE_SPARE_PART:
					CzescZamienna czescZamienna = (CzescZamienna) DSApi.context().session().createCriteria(CzescZamienna.class).add(Restrictions.eq("id", elementId)).list().get(0);
					czescZamienna.setNazwa(ServletActionContext.getRequest().getParameter("nazwa"));
					czescZamienna.setProducent(ServletActionContext.getRequest().getParameter("producent"));
					if(ServletActionContext.getRequest().getParameter("strategiczna").equals("on"))
						czescZamienna.setStrategiczna(true);
					else 
						czescZamienna.setStrategiczna(false);
					czescZamienna.setNrSeryjny(ServletActionContext.getRequest().getParameter("nrSeryjny"));
					czescZamienna.setZdjecie(ServletActionContext.getRequest().getParameter("zdjecie"));
					czescZamienna.setLokalizacja(Lokalizacja.valueOf(ServletActionContext.getRequest().getParameter("lokalizacja")));
					DSApi.context().session().saveOrUpdate(czescZamienna);
					break; 
				}
				DSApi.context().commit();
				itemForm=MachineDictionary.getInstance().getForm(elementType, null, level, parentType, parentId);
			}catch(Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
		}
    }
	
	private class EditForm implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent event) {
			if(elementType!=null && elementId !=null && elementType>0 && elementId>0)
			{
				Long id = Long.parseLong(ServletActionContext.getRequest().getParameter("id"));
				itemForm=MachineDictionary.getInstance().getForm(elementType, id, level, parentType, parentId);
			} 
		}
    }

	public String getMachineTree() {
		return machineTree;
	}

	public void setMachineTree(String machineTree) {
		this.machineTree = machineTree;
	}
	
	private void expand(int elementType, int id)
	{
		
	}

	public Integer getElementType() {
		return elementType;
	}

	public void setElementType(Integer elementType) {
		this.elementType = elementType;
	}

	public Long getElementId() {
		return elementId;
	}

	public void setElementId(Long elementId) {
		this.elementId = elementId;
	}

	public Integer getParentType() {
		return parentType;
	}

	public void setParentType(Integer parentType) {
		this.parentType = parentType;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getItemForm() {
		return itemForm;
	}

	public void setItemForm(String itemForm) {
		this.itemForm = itemForm;
	}

	public String getInfoForm() {
		return infoForm;
	}

	public void setInfoForm(String infoForm) {
		this.infoForm = infoForm;
	}

	public String getAddNewForm() {
		return addNewForm;
	}

	public void setAddNewForm(String addNewForm) {
		this.addNewForm = addNewForm;
	}

	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public ArrayList<MachineElement> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(ArrayList<MachineElement> searchResults) {
		this.searchResults = searchResults;
	}
	
	
}
