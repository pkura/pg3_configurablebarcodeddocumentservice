package pl.compan.docusafe.web.webservice;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.FormFile;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.io.File;

/**
 * User: Tomasz
 * Date: 07.05.14
 * Time: 11:10
 */
public class FileUploadAction extends EventActionSupport {

    private String filename;
    private FormFile file;
    private Long id;

    protected void setup() {
        registerListener(DEFAULT_ACTION).
            append(new FillForm());

        registerListener("upload").
            append(new Upload());

        registerListener("download").
            append(new Download());
    }

    private class FillForm extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            return new JsonPrimitive("FileUploadAction-FillForm");
        }
    }

    private class Upload extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            Long arId = null;
            try {
                DSApi.openAdmin();
                DSApi.context().begin();

                Document doc = new Document(getName(),getName());
                DocumentKind kind = DocumentKind.findByCn("crmkind");
                doc.setDocumentKind(kind);
                doc.setFolder(Folder.getRootFolder());
                doc.create();

                DSApi.context().session().flush();

                kind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
                kind.logic().documentPermissions(doc);

                Attachment attachment = new Attachment(getName());
                doc.createAttachment(attachment);
                AttachmentRevision ar = attachment.createRevision(file.getFile());
                ar.setOriginalFilename(getName());

                DSApi.context().session().flush();

                DSApi.context().commit();
                arId = ar.getId();

            } catch (Exception e) {
                DSApi.context()._rollback();
            } finally {
                DSApi._close();
            }

            return new JsonPrimitive(arId);
        }

        private String getName() {
            String fn = filename;
            if(fn.contains("\n")) {
                String[] fns = fn.split("\n");
                fn = fns[fns.length-1];
            }
            return fn;
        }
    }

    private class Download implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            LoggerFactory.getLogger("tomekl").debug("Download");
            try {
                DSApi.openAdmin();

                AttachmentRevision ar = AttachmentRevision.find(id);
                File f = ar.getAsTempFile();
                ServletUtils.streamFile(ServletActionContext.getResponse(), f, null, "Content-Disposition: inline; filename=\"" + f.getName() + "\"");
            } catch (Exception e) {
                LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
            } finally {
                DSApi._close();
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public FormFile getFile() {
        return file;
    }

    public void setFile(FormFile file) {
        this.file = file;
    }
}
