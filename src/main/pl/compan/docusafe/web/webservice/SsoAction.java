package pl.compan.docusafe.web.webservice;


import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import pl.compan.docusafe.core.users.auth.SsoTokenStore;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

/**
 * User: Tomasz
 * Date: 17.03.14
 * Time: 13:56
 */
public class SsoAction extends EventActionSupport {

    private String tokenKey;

    protected void setup() {
        registerListener(DEFAULT_ACTION).
            append(new FillForm());
    }

    private class FillForm extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            String login = SsoTokenStore.instance().getLoginForToken(tokenKey);
            return new JsonPrimitive(login != null ? login : "invalid");
        }
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }
}
