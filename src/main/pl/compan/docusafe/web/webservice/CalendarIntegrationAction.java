package pl.compan.docusafe.web.webservice;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.calendar.sql.TimeImpl;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.calendar.JSResource;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.AjaxActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import java.util.Date;
import java.util.List;

/**
 * User: Tomasz
 * Date: 27.03.14
 * Time: 11:09
 */
public class CalendarIntegrationAction extends EventActionSupport {

    protected void setup() {
        registerListener(DEFAULT_ACTION).
                append(new FillForm());

        registerListener("Add").
                append(new Add());

        registerListener("Update")
                .append(new Update());

        registerListener("Delete")
                .append(new Delete());
    }

    private class FillForm extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> FillForm");
            return new JsonPrimitive("niy");
        }
    }

    private class Add extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Put - start");

            String user = getParameter("user");
            String desc = getParameter("desc");
            Long startDate = getLongParameter("startDate");
            Long endDate = getLongParameter("endDate");
            Boolean allDay = Boolean.valueOf(getParameter("allDay"));

            Long eventId = -1L;
            DSApi.openAdmin();

            try {

                DSApi.context().begin();

                TimeImpl time = new TimeImpl();
                DSUser duser = DSUser.findByUsername(user);
                time.setCreator(duser);
                time.create();

                JSResource jsr = new JSResource();
                jsr.setCn(user);
                jsr.setCalendarId(UserCalendarBinder.getUserCalendars(duser.getName(), true).get(0).getCalendarId());
                jsr.setPartTwoOfName(duser.getLastname());
                jsr.setPartOneOfName(duser.getFirstname());
                jsr.setConfirm(true);
                jsr.setType("DSUSER");

                List<JSResource> l = Lists.newLinkedList();
                l.add(jsr);

                Date sd = new Date();
                sd.setTime(startDate);

                Date ed = new Date();
                ed.setTime(endDate);

                eventId = time.setCalendarEvent(l, sd, ed, desc, "", allDay, l.get(0), false, 0, 0, null, null, false);

                DSApi.context().commit();

            } catch (Exception e) {
                DSApi.context()._rollback();
                e.printStackTrace();
                throw new EdmException(e.getMessage(),e);
            } finally {
                DSApi._close();
            }

            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Put - end");
            return new JsonPrimitive(eventId);
        }
    }

    private class Update extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Update - start");

            String user = getParameter("user");
            String desc = getParameter("desc");
            Long startDate = getLongParameter("startDate");
            Long endDate = getLongParameter("endDate");
            Long eventId = getLongParameter("eventId");
            Boolean allDay = Boolean.valueOf(getParameter("allDay"));


            DSApi.openAdmin();
            try {
                DSApi.context().begin();

                DSUser duser = DSUser.findByUsername(user);
                JSResource jsr = new JSResource();
                jsr.setCn(user);
                jsr.setCalendarId(UserCalendarBinder.getUserCalendars(duser.getName(), true).get(0).getCalendarId());
                jsr.setPartTwoOfName(duser.getLastname());
                jsr.setPartOneOfName(duser.getFirstname());
                jsr.setConfirm(true);
                jsr.setType("DSUSER");

                List<JSResource> l = Lists.newLinkedList();
                l.add(jsr);

                Date sd = new Date();
                sd.setTime(startDate);
                Date ed = new Date();
                ed.setTime(endDate);

                CalendarEvent calendarEvent = DSApi.context().load(CalendarEvent.class, eventId);
                TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
                eventId = time.setCalendarEvent(l, sd, ed, desc, "", allDay, l.get(0), false, 0, 0, null, null, false);

                DSApi.context().commit();
            } catch (Exception e) {
                DSApi.context()._rollback();
                e.printStackTrace();
                throw new EdmException(e.getMessage(),e);
            } finally {
                DSApi._close();
            }

            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Update - end");
            return new JsonPrimitive(eventId);
        }
    }

    private class Delete extends AjaxActionListener {
        @Override
        public JsonElement ajaxAction(ActionEvent event) throws Exception {
            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Delete - start");

            Long eventId = getLongParameter("eventId");
            String user = getParameter("user");

            DSApi.openAdmin();
            try {

                DSApi.context().begin();

                CalendarEvent calendarEvent = DSApi.context().load(CalendarEvent.class, eventId);
                TimeImpl time = DSApi.context().load(TimeImpl.class, calendarEvent.getTimeId());
                time.delete();
                calendarEvent.delete();

                DSApi.context().commit();

            } catch (Exception e) {
                DSApi.context()._rollback();
                e.printStackTrace();
                throw new EdmException(e.getMessage(),e);
            } finally {
                DSApi._close();
            }

            LoggerFactory.getLogger("tomekl").debug("CalendarIntegrationAction -> Delete - end");
            return new JsonPrimitive(eventId);
        }
    }
}
