package pl.compan.docusafe.web.webservice;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.core.users.auth.DSLoginModule;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import com.opensymphony.webwork.ServletActionContext;

/**
 * Serwlet wywo�ywany przez Docusafe Notifier (DN)
 * <p>
 * DN mo�e wywo�a� serwlet z parametrem okre�laj�cym id najnowszego
 * zadania z poprzedniego wywo�ania.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NotifierAction.java,v 1.28 2010/05/18 12:25:41 tomekl Exp $
 */
public class NotifierAction extends EventActionSupport
{
	 private static final Log log = LogFactory.getLog(NotifierAction.class);
	 private static final ActiveDirectoryManager ad = new ActiveDirectoryManager(Docusafe.getAdditionProperty(DSLoginModule.ACTIVE_DIRECTORY_URL));
	 private static Map<String, Date> activeUsers = new HashMap();
    /**
     * Wersja programu deklarowana przez Messengera, mo�e by� null.
     */
	
    private String v;

    protected void setup()
    {	// Zakomentowane bo nie dziala. DLA RAFALA
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(fillForm);
    }
    // Zakomentowane bo nie dziala. DLA RAFALA

    private DSUser authorize(String authorizationHeader) throws EdmException
    {
    	log.trace("authorize");
        if (authorizationHeader == null || !authorizationHeader.startsWith("Basic "))
            return null;

        String credentials = authorizationHeader.substring("Basic ".length());

        if (credentials.length() == 0)
            return null;

        String loginPass = new String(Base64.decode(credentials.getBytes()));

        int c = loginPass.indexOf(':');
        if (c <= 0)
            return null;

        String login = loginPass.substring(0, c);
        String password = loginPass.substring(c+1);

        try
        {
            DSUser user = null;
            try
        	{
        		user = DSUser.findByUsername(login);
        	}
        	catch (UserNotFoundException e) 
			{
        		 user = DSUser.findByExternalName(login);
			}
            
        	try
        	{
	        	if(user.isAdUser() && "true".equalsIgnoreCase(Docusafe.getAdditionProperty(DSLoginModule.ACTIVE_DIRECTORY_ON)))
	        	{
	        		log.trace("ad user "+login);
	        		String hash = DigestUtils.md5Hex(login+password);
	        		
	        		Calendar jutro = Calendar.getInstance();
	        		jutro.set(Calendar.DAY_OF_YEAR, jutro.get(Calendar.DAY_OF_YEAR)+1);
	        		jutro.set(Calendar.HOUR, -6);
	        		jutro.set(Calendar.MINUTE, 0);
	        		
	        		if(activeUsers.containsKey(hash) && activeUsers.get(hash).before(jutro.getTime()))
	        		{
	        			log.trace("activeUsers contains");
	        			return user;
	        		}
	        		else if(ad.checkPassword(user,login, password))
	        		{
	        			log.trace("activeUsers not contains");
	        			activeUsers.put(hash, new Date());
	        			return user;
	        		}
	        	}
        	}
        	catch (Exception e) 
        	{
				log.debug("Blad logowania do ad - notifier",e);
			}
        	
        	if (user.verifyPassword(password))
                return user;
        }
        catch (UserNotFoundException e)
        {
        	log.error("",e);
        }

        return null;
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
        	
            event.setResult(NONE);
            if(AvailabilityManager.isAvailable("disabled.NotifierAction"))
            	return;
            HttpServletResponse response = ServletActionContext.getResponse();
            String authorization = ServletActionContext.getRequest().getHeader("Authorization");

            if (log.isDebugEnabled())
                log.debug("Authorization "+authorization);

            if (authorization == null)
            {
                response.setStatus(401);
                response.addHeader("WWW-Authenticate", "Basic realm=\"Notifier\"");
                return;
            }
            
            byte[] data = null;
            String contentType = null;

            try
            {
                DSApi.openAdmin();

                DSUser user;
                if ((user = authorize(authorization)) == null)
                {
                    response.setStatus(401);
                    response.addHeader("WWW-Authenticate", "Basic realm=\"Notifier\"");
                    return;
                }

                if (log.isDebugEnabled())
                    log.debug("getNotifyData");
                
                if ("1.0".equals(v))
                {
                    //data = getNotifyDataV1(user);
                    data = getNotifyDataV3(user);
                }
                else if("tomek".equals(v))
                {
                	data = getNotifyDataV2(user);
                }
                else if("3.0".equals(v))
                {
                	data = getNotifyDataV3(user);
                }
                else
                {
                    data = new byte[0];
                }
            }
            catch (EdmException e)
            {
            	log.error(e.getMessage(), e);
            }
            catch (IOException e)
            {
            	log.error(e.getMessage(), e);
            }
            finally
            {
                DSApi._close();
            }

            if (data != null)
            {
                if (log.isDebugEnabled())
                {
                    try
                    {
                        if (log.isDebugEnabled())
                        	log.debug(new String(data, "iso-8859-1"));
                    }
                    catch (UnsupportedEncodingException e)
                    {
                    }
                }

                try
                {
                    if (contentType != null)
                        response.setContentType(contentType);
                    response.setContentLength(data.length);
                    OutputStream stream = response.getOutputStream();
                    int i = 0;
                    while (data.length - i > 0)
                    {
                        int count = data.length - i;
                        if (count > 512)
                            count = 512;
                        stream.write(data, i, count);
                        stream.flush();
                        i += count;
                    }
                    stream.close();
                }
                catch (Exception e)
                {
                	log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    private byte[] getNotifyDataV3(DSUser user) throws IOException, EdmException
    {
        StringBuilder sb = new StringBuilder();

        TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
        
        List<Task> tasks = (List<Task>) taskList.getTasks(user);
        if(tasks != null)
	        for (Task task : tasks)
	        {	    
	            if (task.isAccepted())
	              continue;
	            
	            if(task.getDocumentType().toString().equalsIgnoreCase("internal"))
	            {
	            	sb.append("int");
	            } else sb.append(task.getDocumentType());
	            
	            sb.append(':');
	            sb.append(task.getActivityKey());
	            sb.append('-');
	            sb.append(task.getReceiveDate().getTime());
	            sb.append('\n');
	        }
        //else return "brak-danych".toString().getBytes("iso-8859-1");
        return sb.toString().getBytes("iso-8859-1");
    }
    
    private byte[] getNotifyDataV2(DSUser user) throws IOException, EdmException
    {
    	StringBuilder sb = new StringBuilder();
    	TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
    	
    	sb.append("tomek-debugg");
    	sb.append('\n');
    	sb.append("user: ");
    	sb.append(user);
    	sb.append('\n');
    	sb.append("lista: ");
    	sb.append(taskList.getTasks(user).size());
    	sb.append('\n');
    	
    	for(Task task : taskList.getTasks(user))
    	{
    		sb.append("--- ");
    		sb.append(task.getDocumentType());
    		sb.append(" : ");
    		sb.append(task.getDocumentId());
    		sb.append(" : ");
    		sb.append(task.getActivityKey());
    		sb.append(" ---");
    		sb.append('\n');
    		
    	}
    	
    	
    	return sb.toString().getBytes("iso-8859-1");
    }
    
    private byte[] getNotifyDataV1(DSUser user) throws IOException, EdmException
    {
        StringBuilder sb = new StringBuilder();

        TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
        
        //List inTasks = taskList.getSimpleInDocumentTasks(user);//taskList.getDocumentTasks(user, InOfficeDocument.TYPE);
        
        //List<Task> tasks = (List<Task>) taskList.getTasks(user);
        /*
        List<Task> tasks = (List<Task>) taskList.getDocumentTasks(user, InOfficeDocument.TYPE);
        if(tasks != null)
	        for (Task task : tasks)
	        {
	            //Task task = (Task) iter.next();
	            if (task.isAccepted())
	              continue;
	            
	            if(sb.append(task.getDocumentType().toString()).equals("internal"))
	            	sb.append("int");
	            else
	            	
	            sb.append("in:");
	            //sb.append(task.getDocumentType().toString());
	            sb.append(':');
	            sb.append(task.getActivityKey());
	            sb.append('-');
	            sb.append(task.getReceiveDate().getTime());
	            sb.append('\n');
	        }*/
        
        
        List<Task> inTasks = (List<Task>) taskList.getDocumentTasks(user, InOfficeDocument.TYPE);
        //if(inTasks != null)
	        //for (Iterator iter=inTasks.iterator(); iter.hasNext(); )
        	for (Task task : inTasks)
	        {
	            //Task task = (Task) iter.next();
	            if (task.isAccepted())
	                continue;
	
	            sb.append("in:");
	            sb.append(task.getActivityKey());
	            sb.append('-');
	            sb.append(task.getReceiveDate().getTime());
	            sb.append('\n');
	        }

        List<Task> outTasks = (List<Task>) taskList.getDocumentTasks(user, OutOfficeDocument.TYPE);
        //if(outTasks != null)
	        //for (Iterator iter=outTasks.iterator(); iter.hasNext(); )
        	for (Task task : outTasks)
	        {
	            //Task task = (Task) iter.next();
	            if (task.isAccepted())
	                continue;
	
	            sb.append("out:");
	            sb.append(task.getActivityKey());
	            sb.append('-');
	            sb.append(task.getReceiveDate().getTime());
	            sb.append('\n');
	        }

        List<Task> intTasks = (List<Task>) taskList.getDocumentTasks(user, OutOfficeDocument.INTERNAL_TYPE);
        //if(intTasks != null)
	        //for (Iterator iter=intTasks.iterator(); iter.hasNext(); )
        	for (Task task : intTasks)
	        {
	            //Task task = (Task) iter.next();
	            if (task.isAccepted())
	                continue;
	
	            sb.append("int:");
	            sb.append(task.getActivityKey());
	            sb.append('-');
	            sb.append(task.getReceiveDate().getTime());
	            sb.append('\n');
	        }
		
        return sb.toString().getBytes("iso-8859-1");
    }
	
    public void setV(String v)
    {
        this.v = v;
    }
}
