package pl.compan.docusafe.web.tag.security;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.auth.FullNamePrincipal;
import pl.compan.docusafe.core.users.auth.SubstitutedUserPrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.StringManager;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Set;

/**
 * Tag wypisuj�cy do strumienia nazw� zalogowanego u�ytkownika
 * zgodnie z podanym formatem (atrybut <code>format</code>).
 * <code>%u</code> zamieniane jest na nazw� u�ytkownika,
 * <code>%n</code> zamieniane jest na imi� i nazwisko u�ytkownika.
 * <code>%z</code> zamieniane jest na list� zast�powanych u�ytkownik�w
 *  w nawiasach kwadratowych.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LoggedUserTag.java,v 1.5 2006/02/20 15:42:51 lk Exp $
 */
public class LoggedUserTag extends TagSupport
{
    StringManager sm = GlobalPreferences.loadPropertiesFile("", null);
    private String format;

    public int doStartTag() throws JspException
    {
        Subject subject = (Subject)
            ((HttpServletRequest) pageContext.getRequest()).getSession(true).
            getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        String username = null;
        String fullname = null;
        String substituted = null;

        if (subject != null)
        {
            Set users = subject.getPrincipals(UserPrincipal.class);
            if (users != null && users.size() > 0)
            {
                username = ((UserPrincipal) users.iterator().next()).getName();
            }
            Set fullnames = subject.getPrincipals(FullNamePrincipal.class);
            if (fullnames != null && fullnames.size() > 0)
            {
                fullname = ((FullNamePrincipal) fullnames.iterator().next()).getName();
            }
            Set substs = subject.getPrincipals(SubstitutedUserPrincipal.class);
           if (substs != null && substs.size() > 0)
           {
               substituted = "( " + sm.getString("zastepuje") + " " + substs.toString() + " )";
           }
        }

        StringBuilder result = new StringBuilder(32);

        CharacterIterator iter=new StringCharacterIterator(format);
        for (char c=iter.first(); c != CharacterIterator.DONE; c=iter.next())
        {
            if (c == '%')
            {
                c = iter.next();
                if (c == CharacterIterator.DONE)
                    throw new JspException("Brak symbolu po znaku '%'.");
                switch (c)
                {
                    case 'u': result.append(username); break;
                    case 'n': result.append(fullname); break;
                    case 'z': if (substituted != null) result.append(substituted); break;
                    default: throw new JspException("Nieznany symbol: "+c);
                }
            }
            else
            {
                result.append(c);
            }
        }

        try
        {
            pageContext.getOut().print(result.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }
}
