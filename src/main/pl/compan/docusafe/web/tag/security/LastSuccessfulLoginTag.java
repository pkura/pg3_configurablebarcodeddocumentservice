package pl.compan.docusafe.web.tag.security;

import pl.compan.docusafe.core.users.auth.UserPrincipal;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LastSuccessfulLoginTag.java,v 1.2 2005/09/13 15:14:04 lk Exp $
 */
public class LastSuccessfulLoginTag extends TagSupport
{
    private static DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public int doStartTag() throws JspException
    {
        Subject subject = (Subject)
            ((HttpServletRequest) pageContext.getRequest()).getSession(true).
            getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        if (subject == null)
            return EVAL_PAGE;

        UserPrincipal user = null;
        Set users = subject.getPrincipals(UserPrincipal.class);
        if (users != null && users.size() > 0)
        {
            user = ((UserPrincipal) users.iterator().next());
        }

        if (user.getLastSuccessfulLogin() == null)
            return EVAL_PAGE;

        try
        {
            synchronized (format)
            {
                pageContext.getOut().print(format.format(user.getLastSuccessfulLogin()));
            }
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }
}
