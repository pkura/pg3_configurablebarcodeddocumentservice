package pl.compan.docusafe.web.tag.security;

import pl.compan.docusafe.core.users.auth.RolePrincipal;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.Iterator;
import java.util.Set;

/**
 * Tag wypisuj�cy swoj� zawarto��, je�eli bie��cy u�ytkownik
 * ma prawo do nadawania uprawnie�.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CanSetPermissionsTag.java,v 1.3 2006/02/20 15:42:51 lk Exp $
 * @deprecated Ten tag nie powinien by� u�ywany, bo sprawdza jedynie, czy
 *  u�ytkownik jest administratorem.
 */
public class CanSetPermissionsTag extends BodyTagSupport
{
    public int doStartTag() throws JspException
    {
        Subject subject = (Subject)
            ((HttpServletRequest) pageContext.getRequest()).getSession(true).
            getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        boolean isAdmin = false;

        if (subject != null)
        {
            Set roles = subject.getPrincipals(RolePrincipal.class);
            for (Iterator iter=roles.iterator(); iter.hasNext(); )
            {
                RolePrincipal role = (RolePrincipal) iter.next();
                if ("admin".equals(role.getName()))
                {
                    isAdmin = true;
                    break;
                }
            }
        }

        if (isAdmin)
        {
            return EVAL_BODY_INCLUDE;
        }
        else
        {
            return SKIP_BODY;
        }
    }
}
