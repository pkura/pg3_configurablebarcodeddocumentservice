package pl.compan.docusafe.web.tag.security;

import java.util.Set;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.LoggerFactory;

/***
 * Tag sprawdzajacy czy uzytkownik ma wlaczone Active Directory
 * @author Mariusz Kiljanczyk
 */
public class ActiveDirectoryUserTag extends TagSupport
{
	private static final long serialVersionUID = 1L;
	private Boolean negation;

	public int doStartTag() throws JspException
	{
		Subject subject = (Subject) ((HttpServletRequest) pageContext.getRequest()).getSession(true).getAttribute(
				pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
		
		Boolean adUser = false;
		if(negation == null)
			negation = false;
		if (subject != null)
		{
			Set<UserPrincipal> users = subject.getPrincipals(UserPrincipal.class);
			if (users != null && users.size() > 0)
			{
				adUser = ((UserPrincipal) users.iterator().next()).getActiveDirectoryUser();
			}
		}
		if (adUser == null)
			adUser = false;
		if(negation)
		{
			return (adUser) ? EVAL_BODY_INCLUDE : SKIP_BODY;	
		}
		else
		{
			return (adUser) ? SKIP_BODY : EVAL_BODY_INCLUDE;
		}
		
	}

	public void setNegation(Boolean negation)
	{
		this.negation = negation;
	}

	public Boolean getNegation()
	{
		return negation;
	}
}
