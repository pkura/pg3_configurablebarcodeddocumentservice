package pl.compan.docusafe.web.tag.core;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ChooseOtherwiseTag.java,v 1.2 2006/02/20 15:42:50 lk Exp $
 */
public class ChooseOtherwiseTag extends BodyTagSupport
{
    public int doStartTag() throws JspException
    {
        ChooseTag choose = (ChooseTag) findAncestorWithClass(this, ChooseTag.class);
        if (choose.noneSucceeded())
        {
            return EVAL_BODY_BUFFERED;
        }
        return SKIP_BODY;
    }

    public int doAfterBody() throws JspException
    {
        ChooseTag choose = (ChooseTag) findAncestorWithClass(this, ChooseTag.class);
        if (choose.noneSucceeded() && bodyContent != null)
        {
            choose.otherwise(bodyContent.getString());
        }
        return EVAL_PAGE;
    }
}
