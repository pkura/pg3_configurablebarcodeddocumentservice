package pl.compan.docusafe.web.tag.core;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Tag umo�liwiaj�cy wyb�r wykonania odpowiedniego fragmentu pliku JSP
 * na podstawie warto�ci zwracanych przez zagnie�d�one tagi
 * {@link pl.compan.docusafe.web.tag.core.ChooseConditionTag}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ChooseTag.java,v 1.1 2004/07/01 20:37:11 administrator Exp $
 */
public class ChooseTag extends BodyTagSupport
{
    private String body;

    public void doInitBody() throws JspException
    {
        body = null;
    }

    public int doStartTag() throws JspException
    {
        return EVAL_BODY_BUFFERED;
    }

    public int doEndTag() throws JspException
    {
        if (body != null)
        {
            try
            {
                pageContext.getOut().print(body.trim());
            }
            catch (IOException e)
            {
                throw new JspException(e.getMessage(), e);
            }
        }

        return EVAL_PAGE;
    }

    void success(String body)
    {
        // tylko pierwszy tag wywo�uj�cy t� metod� mo�e nada�
        // warto�� zmiennej body
        if (this.body == null)
            this.body = body;
    }

    void otherwise(String body)
    {
        if (this.body == null)
            this.body = body;
    }

    boolean noneSucceeded()
    {
        return body == null;
    }
}
