package pl.compan.docusafe.web.tag.core;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * Klasa abstrakcyjna reprezentuj�ca warunek
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ChooseConditionTag.java,v 1.2 2006/02/20 15:42:50 lk Exp $
 */
public abstract class ChooseConditionTag extends BodyTagSupport
{
    private boolean success;

    public final int doStartTag() throws JspException
    {
        // tutaj, a nie w doInitBody, bo nie zawsze mog� liczy�,
        // �e tagi dziedzicz�ce po tej klasie wywo�aj� metod�
        // super.doInitBody
        success = false;

        if (conditionMet())
        {
            success = true;
            return EVAL_BODY_BUFFERED;
        }
        return SKIP_BODY;
    }

    public final int doAfterBody() throws JspException
    {
        if (success && bodyContent != null)
        {
            ChooseTag choose = (ChooseTag) findAncestorWithClass(this, ChooseTag.class);
            choose.success(bodyContent.getString());
        }
        return EVAL_PAGE;
    }

    /**
     * Metoda, kt�ra powinna by� zaimplementowana w klasach
     * dziedzicz�cych.
     * @return Zwraca true, je�eli spe�niony jest warunek zwi�zany
     *  z tym tagiem.
     */
    public abstract boolean conditionMet();
}
