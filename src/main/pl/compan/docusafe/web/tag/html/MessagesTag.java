package pl.compan.docusafe.web.tag.html;

import pl.compan.docusafe.util.XmlUtils;
import pl.compan.docusafe.web.common.Messages;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Tag wypisuj�cy do strumienia list� b��d�w zgromadzonych
 * w obiekcie {@link pl.compan.docusafe.web.common.Messages} umieszczonym jako atrybut
 * HttpServletRequest odpowiadaj�cy kluczowi
 * {@link pl.compan.docusafe.web.common.Messages#MESSAGES_KEY}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MessagesTag.java,v 1.3 2006/02/20 15:42:50 lk Exp $
 */
public class MessagesTag extends TagSupport
{
    private String property;

    public int doStartTag() throws JspException
    {
        Messages messages = getMessages();
        if (messages == null)
            return SKIP_BODY;

        List strings = messages.getMessages(property);
        if (strings == null || strings.size() == 0)
            return SKIP_BODY;

        JspWriter writer = pageContext.getOut();

        try
        {
            writer.println(getHeader());

            for (Iterator iter=strings.iterator(); iter.hasNext(); )
            {
                String msg = (String) iter.next();

                writer.print(getPrefix());
                writer.print(XmlUtils.escapeXml(msg));
                writer.println(getPostfix());
            }

            writer.println(getFooter());
        }
        catch (IOException e)
        {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    protected String getHeader()
    {
        return "<ul>";
    }

    protected String getFooter()
    {
        return "</ul>";
    }

    protected String getPrefix()
    {
        return "<li>";
    }

    protected String getPostfix()
    {
        return "</li>";
    }

    protected Messages getMessages()
    {
        return (Messages) pageContext.getRequest().getAttribute(Messages.MESSAGES_KEY);
    }
}
