package pl.compan.docusafe.web.tag.html;

import pl.compan.docusafe.web.common.Messages;

/**
 * Tag wypisuj�cy do strumienia list� b��d�w zgromadzonych
 * w obiekcie {@link pl.compan.docusafe.web.common.Messages} umieszczonym jako atrybut
 * HttpServletRequest odpowiadaj�cy kluczowi
 * {@link pl.compan.docusafe.web.common.Messages#ERRORS_KEY}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ErrorsTag.java,v 1.2 2006/02/20 15:42:50 lk Exp $
 */
public class ErrorsTag extends MessagesTag
{
    protected Messages getMessages()
    {
        return (Messages) pageContext.getRequest().getAttribute(Messages.ERRORS_KEY);
    }

    protected String getHeader()
    {
        return "<ul class='error'>";
    }

    protected String getFooter()
    {
        return "</ul>";
    }
}
