package pl.compan.docusafe.web.tag.html;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WindowOpenTagBaseBeanInfo.java,v 1.2 2006/02/20 15:42:51 lk Exp $
 */
public class WindowOpenTagBaseBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        PropertyDescriptor[] result = new PropertyDescriptor[7];

        try
        {
            result[0] = new PropertyDescriptor("href", WindowOpenTagBase.class,
                null, "setHrefExpr");
            result[1] = new PropertyDescriptor("windowHeight", WindowOpenTagBase.class,
                null, "setWindowHeight");
            result[2] = new PropertyDescriptor("windowWidth", WindowOpenTagBase.class,
                null, "setWindowWidth");
            result[3] = new PropertyDescriptor("windowTarget", WindowOpenTagBase.class,
                null, "setWindowTarget");
            result[4] = new PropertyDescriptor("styleClass", WindowOpenTagBase.class,
                null, "setStyleClass");
            result[5] = new PropertyDescriptor("forward", WindowOpenTagBase.class,
                null, "setForward");
            result[6] = new PropertyDescriptor("windowResizable", WindowOpenTagBase.class,
                null, "setWindowResizable");
        }
        catch (IntrospectionException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }

        return result;
    }
}
