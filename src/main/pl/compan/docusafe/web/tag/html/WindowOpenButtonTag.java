package pl.compan.docusafe.web.tag.html;

import javax.servlet.jsp.JspException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WindowOpenButtonTag.java,v 1.9 2007/09/05 08:22:33 mwlizlo Exp $
 */
public class WindowOpenButtonTag extends WindowOpenTagBase
{
    private String value;

    public int doStartTag() throws JspException
    {
        super.doStartTag();
        return EVAL_BODY_BUFFERED;
    }

    public int doEndTag() throws JspException
    {
        String sBodyContent = (getBodyContent() != null ?
            getBodyContent().getString() : "");

        StringBuilder link = new StringBuilder(30);

        link.append("<input type=\"button\"");
        if (getStyleClass() != null)
        {
            link.append(" class=\"");
            link.append(getStyleClass());
            link.append("\"");
        }
        link.append(" onclick=\"");
        link.append(getWindowOpenString());
        link.append("; return false;\"");
        if (value != null)
        {
            link.append(" value=\"");
            link.append(value);
            link.append("\"");
        }
        else
        {
            link.append(" value=\"");
            link.append(sBodyContent);
            link.append("\"");
        }
        link.append("/>");

        try
        {
            pageContext.getOut().print(link.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    public void setValue(String value)
    {
            this.value = value;
    }  
    
}
