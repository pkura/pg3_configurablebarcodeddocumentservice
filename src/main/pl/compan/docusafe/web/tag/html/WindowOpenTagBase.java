package pl.compan.docusafe.web.tag.html;

import org.apache.struts.Globals;
import org.apache.struts.config.ForwardConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.taglibs.standard.tag.common.core.NullAttributeException;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

/**
 * Tag bazowy dla tag�w tworz�cych przyciski, odno�niki etc. otwieraj�ce
 * nowe okna przy pomocy funkcji JavaScript window.open().
 * <p>
 * TODO: nie wszystkie metody ustawiaj�ce w�asno�ci s� zwracane przez
 * {@link WindowOpenTagBaseBeanInfo#getPropertyDescriptors}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WindowOpenTagBase.java,v 1.3 2006/02/06 16:06:01 lk Exp $
 */
public class WindowOpenTagBase extends BodyTagSupport
{
    private String href;
    private String hrefExpr;

    private String windowTarget;

    private String windowChannelmode;
    private String windowDirectories;
    private String windowFullscreen;
    private String windowHeight;
    private String windowLeft;
    private String windowLocation;
    private String windowMenubar;
    private String windowResizable;
    private String windowScrollbars;
    private String windowStatus;
    private String windowTitlebar;
    private String windowToolbar;
    private String windowTop;
    private String windowWidth;

    private String styleClass;

    public int doStartTag() throws JspException
    {
        try
        {
            if (hrefExpr != null)
                setHref((String) eval("window-open-tag-base", "href", hrefExpr,
                    String.class, this, pageContext));
        }
        catch (NullAttributeException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        // metoda musi by� zamiplementowana w klasie dziedzicz�cej
        return -1;
    }

    private Object eval(String tagName, String attrName, String attrValue, Class attrType,
                        Tag tag, PageContext pageContext)
    throws JspException, NullAttributeException
    {
        Object result = ExpressionUtil.evalNotNull(tagName, attrName, attrValue, attrType,
            tag, pageContext);
        if (result == null)
            throw new NullAttributeException(attrName, tagName);
        return result;
    }

    protected String getWindowOpenString()
    {
        StringBuilder sb = new StringBuilder(30);

        sb.append("window.open('");
        sb.append(href);
        sb.append("', ");

        if (windowTarget == null)
            sb.append("null, ");
        else
            sb.append("'"+windowTarget+"', ");

        sb.append("'");

        if (windowChannelmode != null)
            sb.append("channelmode="+windowChannelmode+",");
        if (windowDirectories != null)
            sb.append("directories="+windowDirectories+",");
        if (windowFullscreen != null)
            sb.append("fullscreen="+windowFullscreen+",");
        if (windowWidth != null)
            sb.append("width="+windowWidth+",");
        if (windowHeight != null)
            sb.append("height="+windowHeight+",");
        if (windowLeft != null)
            sb.append("left="+windowLeft+",");
        if (windowLocation != null)
            sb.append("location="+windowLocation+",");
        if (windowMenubar != null)
            sb.append("menubar="+windowMenubar+",");
        if (windowResizable != null)
            sb.append("resizable="+windowResizable+",");
        if (windowScrollbars != null)
            sb.append("scrollbars="+windowScrollbars+",");
        if (windowStatus != null)
            sb.append("status="+windowStatus+",");
        if (windowTitlebar != null)
            sb.append("titlebar="+windowTitlebar+",");
        if (windowToolbar != null)
            sb.append("toolbar="+windowToolbar+",");
        if (windowTop != null)
            sb.append("top="+windowTop);

        // Uwaga: ostatnia w�asno�� okna nie ma na ko�cu przecinka

        sb.append("'");

        sb.append(")");

        return sb.toString();
    }

    public void setForward(String forward)
    {
        // org.apache.struts.util.RequestUtils
        ModuleConfig config =
            (ModuleConfig) pageContext.getRequest().getAttribute(Globals.MODULE_KEY);
        if (config == null) { // Backwards compatibility hack
            config =
                (ModuleConfig) pageContext.getServletContext().getAttribute(Globals.MODULE_KEY);
            pageContext.getRequest().setAttribute(Globals.MODULE_KEY, config);
        }

        StringBuilder sbHref = new StringBuilder();

        ForwardConfig fc = config.findForwardConfig(forward);
        if (fc == null) {
            throw new RuntimeException("Nie znaleziono forwardu "+forward);
        }
        if (fc.getPath().startsWith("/")) {
            sbHref.append(((HttpServletRequest) pageContext.getRequest()).getContextPath());
//            if (fc.getContextRelative())
//            {
                if (!fc.getPath().startsWith("/")) {
                    sbHref.append("/");
                }
                sbHref.append(fc.getPath());
//            }
//            else
//            {
//                String forwardPattern = config.getControllerConfig().getForwardPattern();
//                if (forwardPattern == null) {
//                    // Performance optimization for previous default behavior
//                    sbHref.append(config.getPrefix());
//                    // smoothly insert a '/' if needed
//                    if (!fc.getPath().startsWith("/")) {
//                        sbHref.append("/");
//                    }
//                    sbHref.append(fc.getPath());
//                } else {
//                    boolean dollar = false;
//                    for (int i = 0; i < forwardPattern.length(); i++) {
//                        char ch = forwardPattern.charAt(i);
//                        if (dollar) {
//                            switch (ch) {
//                                case 'M' :
//                                    sbHref.append(config.getPrefix());
//                                    break;
//                                case 'P' :
//                                    // add '/' if needed
//                                    if (!fc.getPath().startsWith("/")) {
//                                        sbHref.append("/");
//                                    }
//                                    sbHref.append(fc.getPath());
//                                    break;
//                                case '$' :
//                                    sbHref.append('$');
//                                    break;
//                                default :
//                                    ; // Silently swallow
//                            }
//                            dollar = false;
//                            continue;
//                        } else if (ch == '$') {
//                            dollar = true;
//                        } else {
//                            sbHref.append(ch);
//                        }
//                    }
//                }
//
//            }
        } else {
            sbHref.append(fc.getPath());
        }

        this.href = sbHref.toString();
    }

    public void setHrefExpr(String hrefExpr)
    {
        this.hrefExpr = hrefExpr;
    }

    protected String getWindowTarget()
    {
        return windowTarget;
    }

    protected String getHref()
    {
        return href;
    }

    public void setHref(String href)
    {
        this.href = href;
    }

    public void setWindowTarget(String windowTarget)
    {
        this.windowTarget = windowTarget;
    }

    public void setWindowChannelmode(String windowChannelmode)
    {
        this.windowChannelmode = windowChannelmode;
    }

    public void setWindowDirectories(String windowDirectories)
    {
        this.windowDirectories = windowDirectories;
    }

    public void setWindowFullscreen(String windowFullscreen)
    {
        this.windowFullscreen = windowFullscreen;
    }

    public void setWindowHeight(String windowHeight)
    {
        this.windowHeight = windowHeight;
    }

    public void setWindowLeft(String windowLeft)
    {
        this.windowLeft = windowLeft;
    }

    public void setWindowLocation(String windowLocation)
    {
        this.windowLocation = windowLocation;
    }

    public void setWindowMenubar(String windowMenubar)
    {
        this.windowMenubar = windowMenubar;
    }

    public void setWindowResizable(String windowResizable)
    {
        this.windowResizable = windowResizable;
    }

    public void setWindowScrollbars(String windowScrollbars)
    {
        this.windowScrollbars = windowScrollbars;
    }

    public void setWindowStatus(String windowStatus)
    {
        this.windowStatus = windowStatus;
    }

    public void setWindowTitlebar(String windowTitlebar)
    {
        this.windowTitlebar = windowTitlebar;
    }

    public void setWindowToolbar(String windowToolbar)
    {
        this.windowToolbar = windowToolbar;
    }

    public void setWindowTop(String windowTop)
    {
        this.windowTop = windowTop;
    }

    public void setWindowWidth(String windowWidth)
    {
        this.windowWidth = windowWidth;
    }

    public String getStyleClass()
    {
        return styleClass;
    }

    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }
}
