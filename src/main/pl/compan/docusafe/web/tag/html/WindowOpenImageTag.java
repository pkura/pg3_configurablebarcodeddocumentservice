package pl.compan.docusafe.web.tag.html;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import pl.compan.docusafe.core.GlobalPreferences;

import java.io.IOException;

/**
 * Tworzy tag <code>&lt;input type="image"&gt;</code>
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WindowOpenImageTag.java,v 1.7 2007/09/05 08:22:33 mwlizlo Exp $
 */
public class WindowOpenImageTag extends WindowOpenTagBase
{
    private String src;
    private String imageWidth;
    private String imageHeight;
    private String title;

    public int doStartTag() throws JspException
    {
        super.doStartTag();
        return EVAL_BODY_BUFFERED;
    }

    public int doEndTag() throws JspException
    {
        StringBuilder link = new StringBuilder(30);

        link.append("<input type=\"image\"");
        if (getStyleClass() != null)
        {
            link.append(" class=\"");
            link.append(getStyleClass());
            link.append("\"");
        }
        link.append(" onclick=\"");
        link.append(getWindowOpenString());
        link.append("; return false;\"");

        link.append(" src=\"");
        if (src.startsWith("/"))
            link.append(((HttpServletRequest) pageContext.getRequest()).getContextPath());
        link.append(src);
        link.append("\"");

        link.append(" width=\"");
        link.append(imageWidth);
        link.append("\"");

        link.append(" height=\"");
        link.append(imageHeight);
        link.append("\"");

        if (title != null)
        {
            link.append(" title=\"");
            link.append(title);
            link.append("\"");
        }

        link.append("/>");

        try
        {
            pageContext.getOut().print(link.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    public void setSrc(String src)
    {
        this.src = src;
    }

    public void setImageWidth(String imageWidth)
    {
        this.imageWidth = imageWidth;
    }

    public void setImageHeight(String imageHeight)
    {
        this.imageHeight = imageHeight;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
