package pl.compan.docusafe.web.tag.html;

import org.apache.struts.taglib.TagUtils;
import pl.compan.docusafe.core.GlobalConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LinkTag.java,v 1.5 2008/06/10 12:26:39 mariuszk Exp $
 */
public class LinkTag extends BodyTagSupport
{
    private String forward;
    private String href;
    private String selectedClass;

    private String text;

    public int doStartTag() throws JspException
    {
        StringBuilder result = new StringBuilder();

        result.append("<a href=\"");

        String url;
        try
        {
            url = TagUtils.getInstance().computeURL(
                    pageContext, forward, href, null, null, null, null, null, false, false);
        }
        catch (MalformedURLException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        result.append(url);
        result.append("\"");

        String uri = null;
        if (url != null)
        {
            if (url.indexOf('?') > 0)
            {
                uri = url.substring(0, url.indexOf('?'));
            }
            else
            {
                uri = url;
            }
        }

        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

        if (uri != null &&
            uri.equals(request.getContextPath()+request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI)))
        {
            result.append(" class=\"");
            result.append(selectedClass);
            result.append("\"");
        }

        result.append(">");

        try
        {
        	
        	
            pageContext.getOut().print(result.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_BODY_BUFFERED;
    }

    public int doAfterBody() throws JspException
    {
        if (bodyContent != null)
        {
            String value = bodyContent.getString().trim();
            if (value.length() > 0)
                text = value;
        }
        return SKIP_BODY;
    }

    public int doEndTag() throws JspException
    {
        StringBuilder result = new StringBuilder();
        if (text != null)
            result.append(text);
        result.append("</a>");

        try
        {
            pageContext.getOut().print(result.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    public void release()
    {
        super.release();
        forward = null;
        href = null;
        selectedClass = null;
        text = null;
    }

    public void setForward(String forward)
    {
        this.forward = forward;
    }

    public void setHref(String href)
    {
        this.href = href;
    }

    public void setSelectedClass(String selectedClass)
    {
        this.selectedClass = selectedClass;
    }
}
