package pl.compan.docusafe.web.tag.html;

import pl.compan.docusafe.util.StringManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Tag wypisuj�cy komunikat pochodz�cy z pliku jspLocalStrings_*.properties
 * (domy�lny pakiet) lub z pliku wybranego tagiem {@link SelectStringManagerTag}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MessageTag.java,v 1.3 2008/10/06 10:40:33 pecet4 Exp $
 */
public class MessageTag extends BodyTagSupport
{
    private static final String GLOBAL_JSP_RESOURCES = "jspLocalStrings";
    /**
     * Je�eli true, wypisywany komunikat powinien pochodzi� ze wsp�lnego
     * pliku z komunikatami JSP.
     */
    private boolean global;
    private String key;
    private static StringManager sm;

    public int doStartTag() throws JspException
    {
        sm = null;

        if (!global)
            sm = (StringManager) pageContext.getRequest().getAttribute(SelectStringManagerTag.STRING_MANAGER_KEY);

        if (sm == null)
            sm = StringManager.getManager("", GLOBAL_JSP_RESOURCES);

        return EVAL_BODY_BUFFERED;
    }

    public int doAfterBody() throws JspException
    {
        if (bodyContent != null && key == null)
        {
            key = bodyContent.getString().trim();
        }
        return SKIP_BODY;
    }

    public int doEndTag() throws JspException
    {
        String message = sm.getString(key);

        try
        {
            pageContext.getOut().print(message);
        }
        catch (IOException e)
        {
            throw new JspTagException(e.getMessage());
        }

        key = null;

        return EVAL_PAGE;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setGlobal(boolean global)
    {
        this.global = global;
    }
}
