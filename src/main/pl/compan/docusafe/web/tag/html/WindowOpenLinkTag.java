package pl.compan.docusafe.web.tag.html;

import javax.servlet.jsp.JspException;
import java.io.IOException;

/**
 * Wypisuje odnośnik (tag A) otwierający nowe okno.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WindowOpenLinkTag.java,v 1.3 2006/02/06 16:06:00 lk Exp $
 */
public class WindowOpenLinkTag extends WindowOpenTagBase
{
    public int doStartTag() throws JspException
    {
        super.doStartTag();
        return EVAL_BODY_BUFFERED;
    }

    public int doEndTag() throws JspException
    {
        String sBodyContent = (getBodyContent() != null ?
            getBodyContent().getString() : "");

        StringBuilder link = new StringBuilder(30);

        link.append("<a href=\"");
        link.append(getHref());
        link.append("\"");
        if (getWindowTarget() != null)
        {
            link.append(" target=\"");
            link.append(getWindowTarget());
            link.append("\"");
        }
        if (getStyleClass() != null)
        {
            link.append(" class=\"");
            link.append(getStyleClass());
            link.append("\"");
        }
        link.append(" onclick=\"");
        link.append(getWindowOpenString());
        link.append("; return false;\"");
        link.append(">");

        link.append(sBodyContent);

        link.append("</a>");

        try
        {
            pageContext.getOut().print(link.toString());
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }
}
