package pl.compan.docusafe.web.tag.html;

import pl.compan.docusafe.util.StringManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Umieszcza w obiekcie Request obiekt StringManager zwi�zany
 * z zasobem (plikiem z komunikatami) o podanej nazwie. Plik
 * musi si� znajdowa� w domy�lnym pakiecie, wybierana jest
 * nazwa samego pliku.
 * <p>
 * Z tak wybranego obiektu StringManager korzysta tag
 * {@link MessageTag}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SelectStringManagerTag.java,v 1.2 2006/02/20 15:42:51 lk Exp $
 */
public class SelectStringManagerTag extends TagSupport
{
    private String name;
    static final String STRING_MANAGER_KEY = SelectStringManagerTag.class.getName()+
        ".STRING_MANAGER_KEY";

    public int doStartTag() throws JspException
    {
        StringManager sm = StringManager.getManager("", name);
        pageContext.getRequest().setAttribute(STRING_MANAGER_KEY, sm);
        return EVAL_PAGE;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
