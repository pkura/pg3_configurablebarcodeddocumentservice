package pl.compan.docusafe.web.tag.fmt;

import org.apache.taglibs.standard.tag.common.core.NullAttributeException;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;
import pl.compan.docusafe.util.ContextLocale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.NumberFormat;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatSizeTag.java,v 1.3 2006/02/20 15:42:50 lk Exp $
 */
public class FormatSizeTag extends TagSupport
{
    private String sizeExpr;
    private Integer size;

    public int doStartTag() throws JspException
    {
        if (sizeExpr != null)
            setSize((Integer) eval("format-size", "size", sizeExpr, Integer.class,
                this, pageContext));

        try
        {
            pageContext.getOut().print(formatSize(size.intValue()));
        }
        catch (IOException e)
        {
            throw new JspException(e.getMessage(), e);
        }

        return EVAL_PAGE;
    }

    private Object eval(String tagName, String attrName, String attrValue, Class attrType,
                        Tag tag, PageContext pageContext)
    throws JspException, NullAttributeException
    {
        Object result = ExpressionUtil.evalNotNull(tagName, attrName, attrValue, attrType,
            tag, pageContext);
        if (result == null)
            throw new NullAttributeException(attrName, tagName);
        return result;
    }

    private String formatSize(int size)
    {
        NumberFormat format = NumberFormat.getNumberInstance(ContextLocale.getLocale());
        double number;
        String unit;

        // poni�ej kilobajta - dok�adny rozmiar w bajtach
        if (size < 1024)
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size;
            unit = "B";
        }
        // poni�ej 100 KB - rozmiar w KB zaogkr�glony do dw�ch miejsc dziesi�tnych
        else if (size < 100*1024)
        {
            format.setMaximumFractionDigits(2);
            format.setMinimumFractionDigits(2);
            number = size / 1024.0;
            unit = "KB";
        }
        // poni�ej 1 MB - rozmiar w KB bez miejsc dziesi�tnych
        else if (size < 1024*1024)
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size / 1024.0;
            unit = "KB";
        }
        // powy�ej 1 MB - rozmiar w MB bez miejsc dziesi�tnych
        else
        {
            format.setMaximumFractionDigits(0);
            format.setMinimumFractionDigits(0);
            number = size / (double) (1024*1024);
            unit = "MB";
        }

        StringBuilder result = new StringBuilder();
        result.append(format.format(number));
        result.append(" ");
        result.append(unit);

        return result.toString();
    }

    public void setSizeExpr(String sizeExpr)
    {
        this.sizeExpr = sizeExpr;
    }

    public void setSize(Integer size)
    {
        this.size = size;
    }
}
