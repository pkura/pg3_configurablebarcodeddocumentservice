package pl.compan.docusafe.web.tag.fmt;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatSizeTagBeanInfo.java,v 1.2 2006/02/20 15:42:50 lk Exp $
 */
public class FormatSizeTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        PropertyDescriptor[] result = new PropertyDescriptor[1];

        try
        {
            result[0] = new PropertyDescriptor("size", FormatSizeTag.class,
                null, "setSizeExpr");
        }
        catch (IntrospectionException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }

        return result;
    }
}
