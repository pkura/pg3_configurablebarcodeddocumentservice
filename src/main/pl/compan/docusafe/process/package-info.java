/**
 * Paczka zawierająca klasy korzystające z projektu docusafe-process-engine
 * dostosowuje je do API z pl.compan.docusafe.core.dockinds.process
 */
@ParametersAreNonnullByDefault
package pl.compan.docusafe.process;

import javax.annotation.ParametersAreNonnullByDefault;