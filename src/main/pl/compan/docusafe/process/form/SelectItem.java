package pl.compan.docusafe.process.form;

public class SelectItem {
    Long id;
    String userName;
    String statusTitle;
    String user;
    String status;

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getStatusTitle() {
        return statusTitle;
    }
    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String debugString() {
            return "SelectItem{" +
                    "id=" + id +
                    ", userName='" + userName + '\'' +
                    ", statusTitle='" + statusTitle +
                    '}';
        }
}
