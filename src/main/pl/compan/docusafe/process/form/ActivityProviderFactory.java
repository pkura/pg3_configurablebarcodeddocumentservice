package pl.compan.docusafe.process.form;


import com.google.common.collect.*;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.FactoryBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Fabryka dla ActivityController ułatwiająca utworzenie instancji poprzez Spring'a
 *
 * @author msankowski
 */
public class ActivityProviderFactory implements FactoryBean<ActivityProvider>, BeanNameAware{
    private final static Logger LOG = LoggerFactory.getLogger(ActivityProviderFactory.class);

    private Multimap<String, Button> buttons = ArrayListMultimap.create();
    private Multimap<String, LinkButton> linkButtons = ArrayListMultimap.create();
    private Multimap<String, MessageButton> messageButtons = ArrayListMultimap.create();

    private ActivityProvider instance;

    private Map<String, StateForm> prefixStateForms = Maps.newHashMap();

    private String beanName;

    @Override
    public ActivityProvider getObject() {
        if(instance == null){
            LOG.info("initializing ActivitiController '{}' - \nbuttons = {}\nlinkButtons = {}\nmessageButtons = {}", beanName, buttons, linkButtons, messageButtons);
            instance = new ActivityController(buttons,linkButtons,messageButtons);
            if(!prefixStateForms.isEmpty()){
                instance = new CompositeActivitiProvider(ImmutableList.<ActivityProvider>of(
                    instance,
                    new StatePrefixActivityProvider(prefixStateForms)
                ));
            }
        }
        return instance;
    }

    @Override
    public Class<?> getObjectType() {
        return ActivityController.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setStates(List<StateForm> states) {
        try {
            if(instance != null) {
                throw new IllegalStateException("factory already initialized");
            }
            buttons.clear();
            linkButtons.clear();
            messageButtons.clear();
            for(StateForm stateForm : states){
                if(stateForm.getState() != null){
                    buttons.putAll(stateForm.getState(), stateForm.buttons);
                    linkButtons.putAll(stateForm.getState(), stateForm.linkButtons);
                    messageButtons.putAll(stateForm.getState(), stateForm.messageButtons);
                } else {
                    StateForm previous = prefixStateForms.put(stateForm.statePrefix, stateForm);
                    if(previous != null){
                        throw new IllegalStateException("dwa obiekty StateForm do tego samego prefixu " + stateForm.statePrefix);
                    }
                }
            }
            buttons = ImmutableMultimap.copyOf(buttons);
            linkButtons = ImmutableMultimap.copyOf(linkButtons);
            messageButtons = ImmutableMultimap.copyOf(messageButtons);
        } catch (NullPointerException ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    @Override
    public void setBeanName(String s) {
        this.beanName = s;
    }
}
