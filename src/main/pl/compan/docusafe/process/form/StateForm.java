package pl.compan.docusafe.process.form;

import java.util.Collections;
import java.util.List;

/**
 * Bean u�atwiaj�cy tworzenie ActivitiProviderFactory
 */
public class StateForm {
    String state;
    String statePrefix;
    List<Button> buttons = Collections.emptyList();
    List<LinkButton> linkButtons = Collections.emptyList();
    List<MessageButton> messageButtons = Collections.emptyList();

    public String getState() {
        return state;
    }

    public void setState(String state) {
        if(statePrefix != null){
            throw new IllegalStateException("ten bean ma ju� ustawione pole statePrefix nie mo�na ustawi� r�wnie� state");
        }
        this.state = state;
    }

    public String getStatePrefix() {
        return statePrefix;
    }

    public void setStatePrefix(String statePrefix) {
        if(state != null){
            throw new IllegalStateException("ten bean ma ju� ustawione pole state nie mo�na ustawi� r�wnie� statePrefix");
        }
        this.statePrefix = statePrefix;
    }

    public List<Button> getButtons() {
        return buttons;
    }

    public void setButtons(List<Button> buttons) {
        this.buttons = buttons;
    }

    public List<LinkButton> getLinkButtons() {
        return linkButtons;
    }

    public void setLinkButtons(List<LinkButton> linkButtons) {
        this.linkButtons = linkButtons;
    }

    public List<MessageButton> getMessageButtons() {
        return messageButtons;
    }

    public void setMessageButtons(List<MessageButton> messageButtons) {
        this.messageButtons = messageButtons;
    }

    @Override
    public String toString() {
        return "StateForm{" +
                "state='" + state + '\'' +
                ", statePrefix='" + statePrefix + '\'' +
                ", buttons=" + buttons +
                ", linkButtons=" + linkButtons +
                ", messageButtons=" + messageButtons +
                '}';
    }
}
