package pl.compan.docusafe.process.form;

/**
 * Klasa okre�laj�ca czynno�� jak� mo�na wykona� dla danego procesu
 * - co� jak Field tylko dla przycisk�w
 */
public class Button {
    String name;
    String label;
    String cssClass;
    /** akcja onClick wykonywana mo�liwie jako ostatnia */
    String onClick;
    String confirmMsg;
    String confirmMsgConditionalField;
    String confirmMsgConditionalValue;

    boolean inNewLine;
    /** Wymu� dodanie uwagi */
    boolean forceRemark;
    /** Zapisz w bazie danych */
    boolean stored;
    /** Wymu� podpis decyzji */
    boolean signed;
    /** Wymus brak walidowania */
    boolean noValidate;
    /** sprawdzenie tylko jednego pola */
    boolean correction;
    /** potwierdzenie akcji */
    boolean confirm;
    /** warunkowe potwierdzenie akcji */
    boolean confirmConditional;
    /** dodanie listy akcpetacji */
    boolean selectAcceptance;
    boolean selectAcceptanceExtended;
    /** pole do sprawdzenia */
    String fieldCn;

    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isInNewLine() {
        return inNewLine;
    }

    public void setInNewLine(boolean inNewLine) {
        this.inNewLine = inNewLine;
    }

    public boolean isForceRemark() {
        return forceRemark;
    }

    public void setForceRemark(boolean forceRemark) {
        this.forceRemark = forceRemark;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public boolean isStored() {
        return stored;
    }

    public void setStored(boolean stored) {
        this.stored = stored;
    }

    public boolean isNoValidate() {
        return noValidate;
    }

    public void setNoValidate(boolean noValidate) {
        this.noValidate = noValidate;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getOnClick() {
        return onClick;
    }

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

    public String getConfirmMsg() {
        return confirmMsg;
    }

    public void setConfirmMsg(String confirmMsg) {
        this.confirmMsg = confirmMsg;
    }

    public String debugString() {
        return "Button{" + "forceRemark=" + forceRemark + ", name='" + name + '\'' + ", label='" + label + '\'' + ", cssClass='" + cssClass + '\''
                + ", confirmMsg='" + confirmMsg + '\'' + ", stored=" + stored + ", noValidate=" + noValidate + ", signed=" + signed + ", confirm="
                + confirm + '}';
    }

    public boolean isCorrection() {
        return correction;
    }

    public void setCorrection(boolean correction) {
        this.correction = correction;
    }

    public String getFieldCn() {
        return fieldCn;
    }

    public void setFieldCn(String fieldCn) {
        this.fieldCn = fieldCn;
    }

    public boolean isSelectAcceptance() {
        return selectAcceptance;
    }

    public void setSelectAcceptance(boolean selectAcceptance) {
        this.selectAcceptance = selectAcceptance;
    }

    public boolean isSelectAcceptanceExtended() {
        return selectAcceptanceExtended;
    }

    public void setSelectAcceptanceExtended(boolean selectAcceptanceExtended) {
        this.selectAcceptanceExtended = selectAcceptanceExtended;
    }

    public String getConfirmMsgConditionalField() {
        return confirmMsgConditionalField;
    }

    public void setConfirmMsgConditionalField(String confirmMsgConditionalField) {
        this.confirmMsgConditionalField = confirmMsgConditionalField;
    }

    public String getConfirmMsgConditionalValue() {
        return confirmMsgConditionalValue;
    }

    public void setConfirmMsgConditionalValue(String confirmMsgConditionalValue) {
        this.confirmMsgConditionalValue = confirmMsgConditionalValue;
    }

    public boolean isConfirmConditional() {
        return confirmConditional;
    }

    public void setConfirmConditional(boolean confirmConditional) {
        this.confirmConditional = confirmConditional;
    }


}
