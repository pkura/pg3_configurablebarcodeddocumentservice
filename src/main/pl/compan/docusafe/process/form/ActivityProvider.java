package pl.compan.docusafe.process.form;

import com.google.common.collect.Multimap;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;

/**
 *  Interfejs obiektu tworzącego obiekty do wyświetlenia przez szablon dla procesu
 */
public interface ActivityProvider {
    Multimap<String,Object> getActivities(SingleStateProcessInstance pi);
}
