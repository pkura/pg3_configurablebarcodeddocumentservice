package pl.compan.docusafe.process.form;

public class MessageButton
{
    String label;
    String type;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String debugString() {
        return "MessageButton{" +
                "label='" + label + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
