package pl.compan.docusafe.process.form;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa dostarczaj�ca informacji o mo�liwych czynno�ciach, kt�re mo�na wykona�,
 * na razie jbpm4 nie umo�liwia zbadania mo�lliwych przej�� w procesie, podobnie jak Activiti
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ActivityController implements ActivityProvider {
    private final static Logger LOG = LoggerFactory.getLogger(ActivityController.class);

    private final Multimap<String, Button> buttons;
    private final Multimap<String, LinkButton> linkButtons;
    private final Multimap<String, MessageButton> messageButtons;

    public ActivityController(Multimap<String, Button> buttons, Multimap<String, LinkButton> linkButtons, Multimap<String, MessageButton> messageButtons){
        Preconditions.checkNotNull(buttons, "buttons cannot be null");
        this.buttons = ImmutableMultimap.copyOf(buttons);
        this.linkButtons = ImmutableMultimap.copyOf(linkButtons);
        this.messageButtons = ImmutableMultimap.copyOf(messageButtons);
        LOG.info("creating ActivitiController \nbuttons = {}\nlinkButtons={}\nmessageButtons={}",this.buttons, this.linkButtons, this.messageButtons);
    }

    @Override
    public Multimap<String,Object> getActivities(SingleStateProcessInstance pi){
        //todo cacheowanie
        String state = pi.getSingleState();
        Multimap<String, Object> ret = ArrayListMultimap.create();
        ret.putAll("activities", buttons.get(state));
        ret.putAll("linkButtons", linkButtons.get(state));
        ret.putAll("messageButtons", messageButtons.get(state));
        LOG.debug("[{}] getting controls for state '{}'={}", System.identityHashCode(this), pi.getSingleState(), ret);
        return ret;
    }

    @Override
    public String toString() {
        return "ActivityController[" + System.identityHashCode(this) + "{" +
                "buttons=" + buttons +
                ", linkButtons=" + linkButtons +
                ", messageButtons=" + messageButtons +
                '}';
    }

    public static Button createButton(String label, String signal, String fieldCn, boolean inNewLine, boolean confirm, String confirmMsg, boolean confirmConditional, String confirmMsgConditionalField, String confirmMsgConditionalValue, String onClick, String cssClass, boolean correction, boolean forceRemark, boolean stored, boolean noValidate, boolean signed, boolean selectAcceptance, boolean selectAcceptanceExtended){
        Button ret = new Button();
        ret.name = signal;
        ret.label = label;
        ret.confirmMsg = confirmMsg;
        ret.cssClass = cssClass;
        ret.onClick = onClick;
        ret.correction = correction;
        ret.fieldCn = fieldCn;
        ret.forceRemark = forceRemark;
        ret.signed = signed;
        ret.noValidate = noValidate;
        ret.inNewLine = inNewLine;
        ret.confirm = confirm;
        // potwierdzenie akcji-warunkowe, potwierdzenie wy�wietlane w momencie, gdy pole-formatka cn:confirmMsgConditionalField jest r�wne warto�ci confirmMsgConditionalValue
        // przyk�adowe u�ycie:
        // <button name="to selected_department" confirmConditional="true" confirmMsg="Czy na pewno jest to ostatnia faktura dotycz�ca powi�zanej z faktur� umowy?" confirmMsgConditionalField="DWR_OSTATNIA_FAKTURA" confirmMsgConditionalValue="1" label="Akceptuj (Przeka� do wybranego dzia�u" />
        ret.confirmConditional = confirmConditional;
        ret.confirmMsgConditionalField = confirmMsgConditionalField; 
        ret.confirmMsgConditionalValue = confirmMsgConditionalValue;
        ret.selectAcceptance = selectAcceptance;
        ret.selectAcceptanceExtended = selectAcceptanceExtended;
        ret.stored = stored || signed; //podpisa� mo�na tylko zapisan� decyzj�
        LOG.info("created {}", ret.debugString());
        return ret;
    }

    public static LinkButton createLinkButton(String label, String link, String queryString){
        LinkButton ret = new LinkButton();
        ret.label = label;
        ret.link = link;
        ret.queryString = queryString;
        LOG.info("created {}", ret.debugString());
        return ret;
    }

    public static LinkButton createDownloadTemplateButton(String label, String templateName, String special, String asPdf){
        LinkButton ret = new LinkButton();
        ret.label = label;
        ret.link = "download-dockind-template.action";
        ret.queryString = "templateName="+templateName;
        ret.special = special;
        ret.asPdf = "true".equals(asPdf);
        LOG.info("created {}", ret.debugString());
        return ret;
    }

    public static SelectItem createSelectItem(Long id, String userName, String user, String statusTitle, String status){
        SelectItem ret = new SelectItem();
        ret.id = id;
        ret.userName = userName;
        ret.user = user;
        ret.statusTitle = statusTitle;
        ret.status = status;
        LOG.info("created {}", ret.debugString());
        return ret;
    }
    
	public static LinkButton createSubmitButton(String label,String queryString) {
        LinkButton ret = new LinkButton();
        ret.label = label;
        ret.queryString = queryString;
        ret.dockinAction = true;
        LOG.info("created {}", ret.debugString());
        return ret;
	}

    public static MessageButton createMessageButton(String label,String type)
    {
        MessageButton button = new MessageButton();
        button.label = label;
        button.type  = type;
        LOG.info("created {}",button.debugString());
        return button;
    }
}


