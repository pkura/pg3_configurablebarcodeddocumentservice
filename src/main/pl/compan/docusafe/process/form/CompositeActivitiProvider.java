package pl.compan.docusafe.process.form;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;

import java.util.Collection;

/**
 *  ActivitiProvider, który potrafi łączyć implementacje kilku ActivitiProviderów
 */
public class CompositeActivitiProvider implements ActivityProvider{
    private final Collection<ActivityProvider> providers;

    public CompositeActivitiProvider(Collection<ActivityProvider> providers) {
        this.providers = ImmutableList.copyOf(providers);
    }

    @Override
    public Multimap<String, Object> getActivities(SingleStateProcessInstance pi) {
        Multimap<String, Object> ret = null;
        for(ActivityProvider provider : providers){
            Multimap<String, Object> next = provider.getActivities(pi);
            if(!next.isEmpty()){
                if(ret == null){
                    ret = ArrayListMultimap.create();
                }
                ret.putAll(next);
            }
        }
        return ret != null ? ret : ImmutableMultimap.<String, Object>of();
    }
}
