package pl.compan.docusafe.process.form;

import com.google.common.collect.*;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;
import java.util.NavigableMap;

/**
 * ActivitiProvider, kt�ry zwraca przyciski dla danych prefiks�w
 */
public class StatePrefixActivityProvider implements ActivityProvider{
    private final static Logger LOG = LoggerFactory.getLogger(ActivityController.class);

    private final NavigableMap<String, StateForm> prefixTree = Maps.newTreeMap();

    public StatePrefixActivityProvider(Map<String, StateForm> prefixMap){
        for(Map.Entry<String, StateForm> entry: prefixMap.entrySet()){
            String prev = prefixTree.floorKey(entry.getKey());
            if(prev != null && entry.getKey().startsWith(prev)){
                throw new IllegalArgumentException("niejednoznaczne prefixy = '" + prev + "','" + entry.getKey() + "'");
            }
            String next = prefixTree.ceilingKey(entry.getKey());
            if(next != null && next.startsWith(entry.getKey())){
                throw new IllegalArgumentException("niejednoznaczne prefixy = '" + next + "','" + entry.getKey() + "'");
            }
            prefixTree.put(entry.getKey(), entry.getValue());
        }
        LOG.info("creating StatePrefixActivityProvider {}", prefixMap);
    }

    @Override
    public Multimap<String, Object> getActivities(SingleStateProcessInstance pi) {
        //todo cacheowanie
        String prev = prefixTree.floorKey(pi.getSingleState());
        if(pi.getSingleState().startsWith(prev)){
            Multimap<String, Object> ret = ArrayListMultimap.create();
            StateForm stateForm = prefixTree.get(prev);
            ret.putAll("activities", stateForm.buttons);
            ret.putAll("linkButtons", stateForm.linkButtons);
            ret.putAll("messageButtons", stateForm.messageButtons);
            LOG.debug("getting controls for state '{}'={}", pi.getSingleState(), ret);
            return ret;
        } else {
            return ImmutableMultimap.of();
        }
    }
}
