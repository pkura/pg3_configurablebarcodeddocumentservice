package pl.compan.docusafe.process.form;

public class LinkButton {
    String label;
    String link;
    String queryString;
    String special;
    boolean asPdf;

    boolean dockinAction;

    public boolean isDockinAction() {
        return dockinAction;
    }

    public void setDockinAction(boolean dockinAction) {
        this.dockinAction = dockinAction;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public boolean isAsPdf() {
        return asPdf;
    }

    public void setAsPdf(boolean asPdf) {
        this.asPdf = asPdf;
    }

    public String debugString() {
        return "LinkButton{" +
                "label='" + label + '\'' +
                ", link='" + link + '\'' +
                ", queryString='" + queryString + '\'' +
                ", special='" + special + '\'' +
                '}';
    }
}
