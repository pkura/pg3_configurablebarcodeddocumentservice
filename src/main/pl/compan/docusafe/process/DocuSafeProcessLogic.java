package pl.compan.docusafe.process;

import com.google.common.collect.ImmutableMap;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.ProcessAction;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.ProcessLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

/**
 * Logika procesu w zewn�trznym ProcessEngine
 *
 * @author msankowski
 */
public class DocuSafeProcessLogic implements ProcessLogic {
    public final static String DOCUMENT_ID_PARAM_KEY = "doc-id";
    public final static String DOCUMENT_ID_PARAM_OTHER = "docId"; // <-- HAX NA SANKOZIEGO
    public final static String DOCUMENT_AUTHOR = "author";
    /** Nazwa parametru, kt�ry zawiera osob� dekretuj�c� */
    public final static String ASSIGNER_PARAM_KEY = "assigner";
    public final static String CORRECTION_USER_KEY = "correctionUserKey";
    public final static String CORRECTION_USER_KEY_DEF = "noone";

    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeProcessLogic.class);

    private final ProcessEngineType engineType;
    private ProcessEngine processEngine;
    private WorkflowEngine workflowEngine;

    /** nazwa procesu activiti */
    private final String processName;

    public DocuSafeProcessLogic(ProcessEngineType engineType, String processName){
        this.processName = processName;
        this.engineType = engineType;
    }

    public void checkInit(){
        if(this.processEngine == null) {
            this.processEngine = Docusafe.getBean(engineType.processEngineBeanName, ProcessEngine.class);
            this.workflowEngine = Docusafe.getBean(engineType.workflowEngineBeanName, WorkflowEngine.class);
        }
    }

    @Override
    public void startProcess(OfficeDocument od, Map<String, ?> params) throws EdmException {
        checkInit();
        if(params.containsKey(DOCUMENT_ID_PARAM_KEY)){
            throw new IllegalArgumentException("Parametry procesu nie mog� zawiera� klucza '" + DOCUMENT_ID_PARAM_KEY + "' id dokumentu jest zapisywana w tej warto��i");
        }
        Map p = ImmutableMap.<String,Object>builder().putAll(params)
                .put(DOCUMENT_ID_PARAM_KEY, od.getId())
                .put(DOCUMENT_ID_PARAM_OTHER, od.getId())
                .put(CORRECTION_USER_KEY,CORRECTION_USER_KEY_DEF)
                .put(DOCUMENT_AUTHOR,od.getAuthor())
                .build();
        String processId = processEngine.startProcess(processName, p);
        ProcessId id = new ProcessId(ProcessEngineType.ACTIVITI, processId);
        ProcessForDocument pfd = new ProcessForDocument(id, od.getId(), processName);
        DSApi.context().session().save(pfd);
    }

    @Override
    public void process(ProcessInstance pi, ProcessAction pa) throws EdmException {
        checkInit();
        if(pa.getActivityId() != null){
            workflowEngine.signalAction(pa.getActivityId().replaceAll("[^,]*,",""), pa.getActionName());
        } else {
            processEngine.signalEvent(pi.getId().toString(), pa.getActionName());
        }
        pa.getDocument().getDocumentKind().logic().archiveActions(pa.getDocument(),0);
        pa.reassigned(true);
        TaskSnapshot.updateByDocumentId(pa.getDocumentId(), "anyType");
    }

    public void setVariables(String processId, Map<String, ?> variables) {
        processEngine.setVariables(processId, variables);
    }
}
