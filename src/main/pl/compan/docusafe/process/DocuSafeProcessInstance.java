package pl.compan.docusafe.process;

import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * DocuSafeProcessInstance
 *
 * @author msankowski
 */
public class DocuSafeProcessInstance implements SingleStateProcessInstance {
    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeProcessInstance.class);

    private final String processName;
    private final String id;
    private final String state;

    public DocuSafeProcessInstance(ProcessInfo base) {
        this.processName = base.getDefinitionName();
        this.id = base.getId();
        this.state = base.getStates().iterator().next();
    }

    public DocuSafeProcessInstance(TaskInfo base){
        this.processName = base.getProcessDefinitionName();
        this.id = base.getProcessId();
        this.state = base.getState();
    }

    @Override
    public String getProcessName() {
        return processName;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getSingleState() {
        return state;
    }
}
