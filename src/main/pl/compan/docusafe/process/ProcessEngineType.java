package pl.compan.docusafe.process;

import pl.compan.docusafe.util.IdEntity;

/**
 * Enum zawieraj�cy silnik procesu, z kt�rym powi�zany jest dany dokument
 */
public enum ProcessEngineType implements IdEntity<Byte> {
    ACTIVITI((byte)1, "activiti", "docusafeActivitiProcessEngine", "docusafeActivitiWorkflowEngine");

    final byte id;
    /** nazwa bean'a Spring'owego, w kt�rym siedzi ProcessEngine tej implementacji */
    final String processEngineBeanName;
    final String workflowEngineBeanName;
    final String key;

    ProcessEngineType(byte id, String key, String processEngineBeanName, String workflowEngineBeanName) {
        this.id = id;
        this.key = key;
        this.processEngineBeanName = processEngineBeanName;
        this.workflowEngineBeanName = workflowEngineBeanName;
    }

    /**
     * Zwraca warto�� wykorzystywan� przy r�nych mapowaniach poszczeg�lnych silnik�w
     * @return klucz, unikalny dla ProcessEngineType, nigdy null
     */
    public String getKey() {
        return key;
    }

    @Override
    public Byte getId() {
        return id;
    }
}
