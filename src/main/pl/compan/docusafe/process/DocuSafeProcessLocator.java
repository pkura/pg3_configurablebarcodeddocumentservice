package pl.compan.docusafe.process;

import com.google.common.collect.Lists;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.ProcessLocator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.sql.JdbcUtils;

import java.util.Collection;
import java.util.List;

/**
 * DocuSafeProcessLocator
 *
 * @author msankowski
 */
class DocuSafeProcessLocator implements ProcessLocator{
    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeProcessLocator.class);

    private final ProcessEngineType engineType;
    private ProcessEngine processEngine;
    private WorkflowEngine workflowEngine;

    private final String processName;

    public DocuSafeProcessLocator(ProcessEngineType engineType, String processName){
        this.processName = processName;
        this.engineType = engineType;
    }

    public void checkInit(){
        if(this.processEngine == null) {
            this.processEngine = Docusafe.getBean(engineType.processEngineBeanName, ProcessEngine.class);
            this.workflowEngine = Docusafe.getBean(engineType.workflowEngineBeanName, WorkflowEngine.class);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<DocuSafeProcessInstance> lookupForDocument(Document doc) throws EdmException {
        checkInit();
        return pack(DSApi.context().session().getNamedQuery(ProcessForDocument.FIND_PROCESSES_FOR_DOCUMENT_QUERY)
                .setParameter("documentId", doc.getId())
                .setParameter("processName", processName)
                .list());
    }

    private Collection<DocuSafeProcessInstance> pack(Collection<Object> ids) throws EdmException {
        checkInit();
        List<DocuSafeProcessInstance> ret = Lists.newArrayListWithCapacity(ids.size());
        for(Object id: ids){
            ret.add(lookupForId(id));
        }
        return ret;
    }

    @Override
    public DocuSafeProcessInstance lookupForId(Object id) throws EdmException {
        checkInit();
        ProcessInfo pi = processEngine.getProcessInstance(id.toString());
        return new DocuSafeProcessInstance(pi);
    }

    @Override
    public ProcessInstance lookupForTaskId(String id) throws EdmException {
        checkInit();
        id = id.replaceAll("[^,]*,","");
        TaskInfo taskInfo = workflowEngine.getTask(id);
        return new DocuSafeProcessInstance(taskInfo);
    }
}
