package pl.compan.docusafe.process;

import com.google.common.collect.Multimap;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.AbstractProcessView;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.dockinds.process.SingleStateProcessInstance;
import pl.compan.docusafe.core.office.workflow.AssignmentDescriptor;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.ActivityProviderFactory;
import pl.compan.docusafe.process.form.ActivityProvider;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.web.common.RenderBean;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class DocuSafeActivitiesProcessView extends AbstractProcessView {
    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeActivitiesProcessView.class);

    public final static DocuSafeActivitiesProcessView EMPTY = new DocuSafeActivitiesProcessView(new ActivityProviderFactory().getObject());

    private final ActivityProvider activityController;

    public DocuSafeActivitiesProcessView(ActivityProvider activityController) {
        LOG.info("creating DocuSafeActivitiesProcessView using activitiController = {}", activityController);
        this.activityController = activityController;
    }

    @Override
    public RenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        LOG.info("rendering DocuSafeActivitiesProcessView for " + context.getActionType());
        if(!(pi instanceof SingleStateProcessInstance)){
            throw new IllegalArgumentException("pi not instanceof SingleStateProcessInstance");
        }
        SingleStateProcessInstance spi = (SingleStateProcessInstance) pi;
        ExtendedRenderBean ret = getRenderBean(spi, context);
        if(ret.getTemplate() != EMPTY_TEMPLATE){//w tym przypadku mo�na tak por�wnywa�
            ret.putAllValues(provideActivities(spi, context, activityController).asMap());

            //TODO: wywalic do wlasnego ProcessView
            try {
                Preferences p = DSApi.context().userPreferences().node("collectiveassignment");
                p.keys();
                List<SelectItem> selectItems = new ArrayList<SelectItem>();
                for(int i=0; i<p.keys().length;i++) {
                    AssignmentDescriptor a = AssignmentDescriptor.forDescriptor(p.get(p.keys()[i], ""));
                    if(a.getUsername() != null && a.getUsername().length() > 0) {
                        selectItems.add(ActivityController.createSelectItem(Integer.valueOf(i).longValue(),a.getUsername(),a.getUsername(),"",""));
                    }
                }
                ret.putValue("selectItems", selectItems);

            } catch (Exception e) {
                LOG.debug(e.getMessage(),e);
            }
        }
        return ret;
    }

    protected Multimap<String, Object> provideActivities(SingleStateProcessInstance pi,
                                                         ProcessActionContext context,
                                                         ActivityProvider ac) {
        Multimap<String, Object> ret = ac.getActivities(pi);
        return ret;
    }

    private ExtendedRenderBean getRenderBean(SingleStateProcessInstance pi, ProcessActionContext context){
        switch(context.getActionType()){
            case ARCHIVE_VIEW:
            case USER_VIEW: {
                return new ExtendedRenderBean(pi, "jbpm4-default-process-view.vm");
            }
            default:
                return new ExtendedRenderBean(pi, EMPTY_TEMPLATE);
        }
    }

    @Override
    public String toString() {
        return "DocuSafeActivitiesProcessView{" +
                "activityController=" + activityController +
                '}';
    }
}
