package pl.compan.docusafe.process;

import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessLocator;
import pl.compan.docusafe.core.dockinds.process.ProcessLogic;
import pl.compan.docusafe.core.dockinds.process.ProcessView;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * ProcessDefinition dla proces�w w zewn�trznych ProcessEngine.
 *
 * Konfigurowany poprzez Spring'a
 *
 * @author msankowski
 */
public class DocuSafeProcessDefinition implements ProcessDefinition {
    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeProcessDefinition.class);

    private final String name;

    private final ProcessEngineType processEngineType; //na razie tylko Activiti, do rozszerzenia

    //mo�na w przysz�o�ci pomy�le� nad konfiguracj� typ�w tych instancji poprzez springa
    private final DocuSafeProcessLogic processLogic;
    private final DocuSafeProcessLocator processLocator;
    private final DocuSafeActivitiesProcessView processView;

    private CreationType creationType = CreationType.INTERNAL;

    public DocuSafeProcessDefinition(String name, DocuSafeActivitiesProcessView view) {
        this.processEngineType = ProcessEngineType.ACTIVITI;
        this.name = name;
        this.processLocator = new DocuSafeProcessLocator(processEngineType, name);
        this.processLogic = new DocuSafeProcessLogic(processEngineType, name);
        this.processView = view;
        LOG.info("creating DocuSafeProcessDefinition for process '{}', processView={}", name, processView);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ProcessView getView() {
        return processView;
    }

    @Override
    public ProcessLogic getLogic() {
        return processLogic;
    }

    @Override
    public ProcessLocator getLocator() {
        return processLocator;
    }

    @Override
    public CreationType getCreationType() {
        return creationType;
    }

    // metoda by�a dodana do ProcessDefinition w r�wnoleg�ym branchu,
    // wi�c p�ki co zwracamy tutaj name.
    @Override
    public String getTitle() {
        return name;
    }

    public void setCreationType(CreationType creationType) {
        this.creationType = creationType;
    }

    public ProcessEngineType getProcessEngineType() {
        return processEngineType;
    }
}
