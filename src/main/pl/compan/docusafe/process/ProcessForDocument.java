package pl.compan.docusafe.process;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.sql.JdbcUtils;

import javax.persistence.*;

/**
 * Encja zapisująca powiązania dokumentu z procesami,
 * w przeciwieństwie do ds_document_to_jbpm4 wspiera wiele silników procesów
 *
 * @author msankowski
 */
@Entity
@Table(name = "ds_process_for_document")
@NamedQueries({
    @NamedQuery(name="FIND_PROCESSES_FOR_DOCUMENT",
                query = "SELECT pfd.id.processId FROM ProcessForDocument pfd "
                      + "WHERE pfd.processDefinitionId = :processName AND pfd.documentId = :documentId"),
    @NamedQuery(name="ProcessForDocument.findDocuments", query = "SELECT d.documentId FROM ProcessForDocument d " +
                     "WHERE d.processDefinitionId = :processName "),
    @NamedQuery(name="ProcessForDocument.findProcesses", query = "SELECT d FROM ProcessForDocument d " +
                "WHERE d.documentId = :documentId ")
})
public class ProcessForDocument {
    private final static Logger LOG = LoggerFactory.getLogger(ProcessForDocument.class);
    public final static String FIND_PROCESSES_FOR_DOCUMENT_QUERY = "FIND_PROCESSES_FOR_DOCUMENT";

    @Id
    @EmbeddedId
    private ProcessId id;

    @Column(name="document_id", updatable = false, nullable = false)
    private long documentId;

    /** Id definicji procesu w zewnętrznym ProcessEngine */
    @Column(name="definition_id", updatable = false, nullable = false, length = 255)
    private String processDefinitionId;

    /** Dla hibernate'a */
    protected ProcessForDocument() {}

    public ProcessForDocument(ProcessId id, long documentId, String processDefinitionId) {
        this.id = id;
        this.documentId = documentId;
        this.processDefinitionId = processDefinitionId;
    }

    public static long documentIdForProcess(String processId, ProcessEngineType engineType) throws EdmException {
        return JdbcUtils.selectLong("SELECT document_id FROM ds_process_for_document\n" +
                "WHERE engine_id = ? AND process_id = ?", engineType.getId(), processId);
    }


    public static List<String> getProcessIdsForDocument (Long  documentId, ProcessEngineType engineType) throws EdmException {
        return JdbcUtils.selectStrings("SELECT process_id FROM ds_process_for_document\n" +
                "WHERE engine_id = ? AND document_id = ?",engineType.getId() ,documentId );
    }

    public static List<Long> getDocumentsForProcess(String processName) throws EdmException {
        return (List<Long>) DSApi.context().session().getNamedQuery("ProcessForDocument.findDocuments")
                .setParameter("processName", processName).list();
    }

    public static List<ProcessForDocument> getProcessesForDocument(Long documentId) throws EdmException {
        return (List<ProcessForDocument>) DSApi.context().session().getNamedQuery("ProcessForDocument.findProcesses")
                .setParameter("documentId", documentId).list();
    }


    public ProcessId getId() {
        return id;
    }

    public long getDocumentId() {
        return documentId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }
}
