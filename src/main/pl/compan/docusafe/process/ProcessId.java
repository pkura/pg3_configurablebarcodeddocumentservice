package pl.compan.docusafe.process;

import pl.compan.docusafe.util.IdEnumSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Id procesu w zewnętrzym ProcessEngine
 *
 * @author sankozi
 */
@Embeddable
public class ProcessId implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(ProcessId.class);

    private final static IdEnumSupport<Byte, ProcessEngineType> ENGINE_TYPES = IdEnumSupport.create(ProcessEngineType.class);

    /** Id enuma ProcessEngineType */
    @Column(name = "engine_id", updatable = false)
    private byte processEngineId;

    /** Id procesu z zewnętrznego systemu */
    @Column(name = "process_id", length = 255, nullable = false, updatable = false)
    private String processId;

    /** dla hibernate'a */
    protected ProcessId(){}

    public ProcessId(ProcessEngineType processEngineType, String processId) {
        this.processEngineId = processEngineType.id;
        this.processId = checkNotNull(processId);
    }

    public ProcessEngineType getProcessEngineType(){
        return ENGINE_TYPES.get(processEngineId);
    }

    public String getProcessId() {
        return processId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessId processId1 = (ProcessId) o;

        if (processEngineId != processId1.processEngineId) return false;
        if (!processId.equals(processId1.processId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) processEngineId;
        result = 31 * result + processId.hashCode();
        return result;
    }
}
