package pl.compan.docusafe.core.bookmarks;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

public class FilterColumn {
	private Long id;
	private String title;
	private String columnName;
	
	public FilterColumn(String title,String columnName) {
		this.title = title;
		this.columnName = columnName;
	}
	
	public FilterColumn() {}
	
	public static FilterColumn find(Long id) throws EdmException
	{
		  return (FilterColumn)Finder.find(FilterColumn.class, id);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

}