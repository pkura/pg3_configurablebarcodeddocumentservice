package pl.compan.docusafe.core.bookmarks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.TextUtils;

import com.google.gson.Gson;
/**
 * Klasa reprezentująca w systemie zakładkę.
 */
public class Bookmark {
	
	public static final Map<Integer,String> KINDS;
	public static final Integer KIND_SYSTEM_BOOKMARK = 0;
	public static final Integer KIND_USER_BOOKMARK = 1;
	public static final String SYSTEM_BOOKMARK = "#system#";
	
	static {
		KINDS = new HashMap<Integer, String>();
		KINDS.put(KIND_SYSTEM_BOOKMARK, "Systemowa");
		KINDS.put(KIND_USER_BOOKMARK, "Użytkownika");
	}
	
	private Long id;
	private Set<FilterCondition> filters = new HashSet<FilterCondition>();
	private List<String> columns = new ArrayList<String>();
	private Long[] labels;
	
	private Boolean active;
	private String cn;
	private String type;
	private String name;
	private String title;
	private Integer kind;
	private String owner;
	private String sortField;
	private Boolean ascending;
	private Integer taskCount;
	
	public Bookmark() {
		this.active = true;
		this.ascending = true;
	}
	
	public static Bookmark find(Long id) throws EdmException
	{
		  return Finder.find(Bookmark.class, id);
	}
	
	/**
	 * Tworzy obiekt w bazie danych.
	 * @throws EdmException
	 */
	public void create() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        }
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
	}
	
    public void delete() throws EdmException 
    {
    	if (this.getId() < 0)
    		throw new EdmException("Nie wolno usuwać wbudowanych zakładek");
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            throw new EdmException("Blad usuniecia zakładki: "+e.getMessage());
        }
    }
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		if(StringUtils.isEmpty(cn)){
			this.cn = TextUtils.latinize(this.name).replaceAll(" ","_").replaceAll("[^a-zA-Z0-9_]", "");
		}else {
			this.cn = cn;
		}
	}

	public Set<FilterCondition> getFilters() {
		return filters;
	}

	public void setFilters(Set<FilterCondition> filters) {
		this.filters = filters;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getColumns() {
		String tmp = "";
		for (String s : columns) {
			tmp += s+",";
		}
		return tmp;
	}

	public void setColumns(String columns) {
		this.columns = Arrays.asList(columns.split(","));
	}
	
	public String getLabels() {
		Gson g = new Gson();
		String json = g.toJson(labels);
		return json;
	}

	public void setLabels(String labels) {
		Gson g = new Gson();
		if (labels != null && !labels.equals(""))
			this.labels = g.fromJson(labels, Long[].class);
	}
	
	public Long[] getLabelsArray() {
		return this.labels;
	}
	
	public void setLabelsArray(Long[] la) {
		this.labels = la;
	}
	
	public List<String> getColumnsList() {
		return columns;
	}
	
	public String[] getColumnsForUser()
	{
		String columns = DSApi.context().userPreferences().node(BookmarkManager.BOOKMARKS_SETTINGS).
			get(BookmarkManager.BOOKMARKS_COLUMNS+getId(), getColumns());
		return columns.split(",");
	}
	
	public void setColumnList(List<String> columns) {
		this.columns = columns;
	}

	public Integer getKind() {
		return kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}
	
	public String getKindDescription() {
		return KINDS.get(this.kind);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bookmark other = (Bookmark)obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Integer getTaskCount() {
		return taskCount;
	}

	public void setTaskCount(Integer taskCount) {
		this.taskCount = taskCount;
	}

	public Boolean getAscending() {
		return ascending;
	}

	public void setAscending(Boolean ascending) {
		this.ascending = ascending;
	}
	
}