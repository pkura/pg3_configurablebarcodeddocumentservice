package pl.compan.docusafe.core.bookmarks;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Column;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeOrder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;

import com.google.gson.Gson;
/**
 * Klasa pomocnicza do zarz�dzania zak�adkami.
 * @author PN
 */
public class BookmarkManager {
	private static final Logger log = LoggerFactory.getLogger(BookmarkManager.class);
	private static final StringManager smTab = GlobalPreferences.loadPropertiesFile("","tab");
	private static final StringManager sm =
        GlobalPreferences.loadPropertiesFile(BookmarkManager.class.getPackage().getName(),null);
	
	// poni�ej identyfikatory zak�adek wbudowanych (tzw. embedded)
	// kt�rych nawet administator nie mo�e usun��
	// maj� one za zadanie odwzorowa� "stare" listy zada�
	// cech� charakterystyczn� tych zak�adek jest to, �e maj�
	// ujemne identyfikatory (�aden id dodany do bazy danych nie ma mo�liwo�ci aby by� ujemny)
	public static final long BOOKMARK_ID_IN = -1;
	public static final long BOOKMARK_ID_INTERNAL = -2;
	public static final long BOOKMARK_ID_OUT = -3;
	public static final long BOOKMARK_ID_ORDER = -4;
	public static final long BOOKMARK_ID_CASE = -5;
	public static final long BOOKMARK_ID_WATCHES = -6;
	public static final long BOOKMARK_ID_DOCUMENT_PACKAGE = -7;
    public static final String TAB_IN = InOfficeDocument.TYPE;
    public static final String TAB_OUT = OutOfficeDocument.TYPE;
    public static final String TAB_INT = OutOfficeDocument.INTERNAL_TYPE;
    public static final String TAB_WATCHES = "watches";
    public static final String TAB_CASES = "cases";
    public static final String TAB_ORDER = OfficeOrder.TYPE;
    public static final String TAB_DOCUMENT_PACKAGE = "paczkiDokumentow";
	
	/**
	 * W�ze� w kt�rym trzymane s� ustawienia indywidulane u�ytkownika dla zak�adek.
	 */
	public static final String BOOKMARKS_SETTINGS = "bookmarks-settings";
	public static final String BOOKMARKS_ASCENDING = "ascending_";
	public static final String BOOKMARKS_TASK_COUNT = "task-count-per-page_";
	public static final String BOOKMARKS_SORT_COLUMN = "sort-column_";
	public static final String BOOKMARKS_COLUMNS = "columns_";
	
	/**
	 * Lista dost�pnych modu��w w systemie.
	 * Odwzorowana poprzez przechowywanie string�w 
	 * zawieraj�cych nazwy tab�w.
	 */
	public static final List<String> AVAILABLE_MODULES;
	/**
	 * Lista przechowuj�ca identyfikatory dost�pnych zak�adek wbudowanych
	 * do systemu. Identyfikatory dla tych zak�adek s� unikalne (ujemne).
	 */
	public static final List<Long> AVAILABLE_BOOKMARKS_IDS;
	
	static {
		AVAILABLE_MODULES = new ArrayList<String>(6);
		AVAILABLE_BOOKMARKS_IDS = new ArrayList<Long>(6);
		
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
        {
			AVAILABLE_MODULES.add(TaskListAction.TAB_IN);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_IN);
        }
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
        {
			AVAILABLE_MODULES.add(TaskListAction.TAB_INT);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_INTERNAL);
        }
		if((AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace") 
				|| AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzacesimple")))
		{
			AVAILABLE_MODULES.add(TaskListAction.TAB_OUT);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_OUT);
		}
        if (Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
        {
        	AVAILABLE_MODULES.add(TaskListAction.TAB_ORDER);
        	AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_ORDER);
        }
		if(AvailabilityManager.isAvailable("menu.left.repository.obserwowane"))
		{
			AVAILABLE_MODULES.add(TaskListAction.TAB_WATCHES);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_WATCHES);
		}
		if (AvailabilityManager.isAvailable("sprawy"))
        {
			AVAILABLE_MODULES.add(TaskListAction.TAB_CASES);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_CASE);
        }
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow"))
        {
			AVAILABLE_MODULES.add(TaskListAction.TAB_DOCUMENT_PACKAGE);
			AVAILABLE_BOOKMARKS_IDS.add(BOOKMARK_ID_DOCUMENT_PACKAGE);
        }
	}
	
	/**
	 * Metoda zwraca list� zak�adek systemowych oraz danego u�ytkownika. S�y�y do wy�wietlania
	 * wszystkich zak�adek, kt�re mo�e ustawi� sobie u�ytkwnik.
	 * Metoda ta s�u�y do zwracania listy zak�adek osoby z uprawnieniami administratorskimi.
	 * @param username
	 * @param zmienna okre�laj�ca czy u�ytkownik jest administratorem
	 * @return
	 * @throws EdmException
	 */
	public static List<Bookmark> getAvailableBookmarksForUser(String username, boolean isAdmin) throws EdmException 
	{
		String sql = "SELECT b FROM b IN CLASS "+Bookmark.class.getName() + " WHERE b.owner=:owner ";
		if (isAdmin) sql = sql + " OR b.owner=:system ORDER BY b.id DESC";
		Query q = DSApi.context().session().createQuery(sql);
		q.setString("owner", username);
		if (isAdmin) q.setString("system", Bookmark.SYSTEM_BOOKMARK);		
		return filterAvailableBookmarks(q.list());
	}
	
	/**
	 * Lista zak�adek domy�lnych (WBUDOWANYCH) do systemu.
	 * @return
	 * @throws EdmException
	 */
	public static List<Bookmark> getDefaultBookmarks() throws EdmException
	{
		String sql = "SELECT b FROM b IN CLASS "+Bookmark.class.getName() + " WHERE b.id < 0 ORDER BY b.id DESC";
		Query q = DSApi.context().session().createQuery(sql);
		return filterAvailableBookmarks(q.list());
	}
	
	/**
	 * Filtrowanie zad�adek w celu zwr�cenia tylko tych, kt�re s� dost�pne danym systemie.
	 * IN, INTERNAL, OUT, itd.
	 * @param in
	 * @return
	 */
	public static List<Bookmark> filterAvailableBookmarks(List<Bookmark> in)
	{
		List<Bookmark> out = new ArrayList<Bookmark>();
		for(Bookmark b : in)
		{
			if (AVAILABLE_MODULES.contains(b.getType())) out.add(b);
		}
		return out;
	}
	
	/**
	 * Metoda zwraca list� zak�adek systemowych.
	 * @return
	 * @throws EdmException
	 */
	public static List<Bookmark> findSystemBookmarks() throws EdmException {
		Criteria crit = DSApi.context().session().createCriteria(Bookmark.class);
		crit.add(Restrictions.eq("kind", Bookmark.KIND_SYSTEM_BOOKMARK));
		crit.addOrder(Order.desc("id"));
		return filterAvailableBookmarks(crit.list());
	}
	
	/**
	 * W metodzie podajemy id zak�adki do usuni�cia oraz to co wyci�gniemy z w�z�a u�ytkownika z zak�adkami.
	 * Je�eli metoda znajdzie id podanej zak�adki to je usuwa i zwraca odpowiedni string do zapisania w ustawieniach.
	 * Je�eli brak jest takiego id w podanej li�cie to zwraca to co otrzyma�a na wej�ciu.
	 * @param idsList - lista id zak�adek wyci�gni�ta z w�z�a
	 * @param bookmarkId - id zak�adki
	 * @return
	 */
	public static String removeBookmarkFromNode(String idsList,Long bookmarkId) 
	{	
		Gson g = new Gson();
		Long[] ids = null;
		if (idsList != null && !idsList.equals("")) {
			ids = g.fromJson(idsList, Long[].class);
		}
		if (ids != null && ids.length>0) {
			if (ArrayUtils.contains(ids, bookmarkId)) {
    			ids = (Long[])org.apache.commons.lang.ArrayUtils.removeElement(ids, bookmarkId);
    			return g.toJson(ids);
			}
		}
		return idsList != null ? idsList : "";
	}
	
	/**
	 * Metoda dodaje id zak�adki do w�z�a z zak�adkami.
	 * @see BookmarkManager#removeBookmarkFromNode(String, Long)
	 * @param idsList lista id zak�adek wyci�gni�ta z w�z�a
	 * @param bookmarkId id zak�adki
	 * @return odpowieni String kt�ry mo�na zapisa� bezpo�rednio w w�le
	 */
	public static String addBookmarkToNode(String idsList, Long bookmarkId) 
	{
		Gson g = new Gson();
		Long[] ids = null;
		if (idsList != null && !idsList.equals("")) {
			ids = g.fromJson(idsList, Long[].class);
		} else {
			ids = new Long[0];
		}

		if (!ArrayUtils.contains(ids, bookmarkId)) {
			ids = (Long[])ArrayUtils.add(ids, bookmarkId);
			return g.toJson(ids);
		} else {
			return g.toJson(ids);
		}
	}
	
	/**
	 * Metoda zwraca list� zak�adek, kt�re dany u�ytkownik ma mie� wy�wietlone na "listach zada�".
	 * Zwr�cone zak�adki wynikaj� zar�wno z ustawie� systemowych jak i ustawie� u�ytkownika.
	 * @return
	 * @throws EdmException 
	 */
	public Map<Long, Bookmark> getVisibleBookmarksForUser(String username) throws EdmException
	{
		boolean sysBook = false;
		boolean userBook = false;
		boolean otherBookmarks = false;
		
		String ids = null;
		String def = DSApi.context().systemPreferences().node("bookmarks").get("list", "");
		otherBookmarks = username != null && !username.equals("null"); 
		// pobieramy zak�aki ogl�danego u�ytkownika lub aktualnie zalogowanego
		if (otherBookmarks)
		{
			ids = DSApi.context().userPreferences(username).node("bookmarks").get("list", def);
		}
		else
		{
			ids = DSApi.context().userPreferences().node("bookmarks").get("list", def);
		}
		
		Gson g = new Gson();
		Long[] systemBookmarks = null;
		Long[] userBookmarks  = null;
		if (def != null && !def.equals("")) {
			systemBookmarks = g.fromJson(def, Long[].class);    			
		}
		if (ids != null && !ids.equals("")) {
			userBookmarks = g.fromJson(ids, Long[].class);
		}
		
		//tutaj wzi�cie pierwszej w kolejno�ci zak�adki
		if (systemBookmarks != null && systemBookmarks.length > 0) 
		{
			sysBook = true;
		}
		if (userBookmarks != null && userBookmarks.length > 0) 
		{
			userBook = true;
		}
		
        Map<Long, Bookmark> bookmarks = new LinkedHashMap<Long, Bookmark>();
        //przygotowanie odpowiednich link�w dla zak�adek systemowych + u�ytkownika
        if (sysBook && !userBook)
        {
	       	 for(int i=0;i<systemBookmarks.length;i++) 
	       	 {
	       		Bookmark b = Bookmark.find(new Long(systemBookmarks[i]));
		            bookmarks.put(b.getId(), b);
	       	 }
        }
        
        if (userBook) 
        {
	       	 for(int i=0;i<userBookmarks.length;i++) 
	       	 {
	       		Bookmark b = Bookmark.find(new Long(userBookmarks[i]));
		            bookmarks.put(b.getId(), b);
	       	 }
        }
		
		return bookmarks;
	}
	
	/**
	 * Przygotowanie mo�liwych typ�w zak�adek.
	 * Mo�liwe typy zak�adek wynikaj� z ustawnie� systemowych (AVAILABLES & ADDS).
	 */
	public Map<String, String> prepareTypes() 
	{
		Map<String, String> types = new LinkedHashMap<String, String>(6);
		// opcja zak�adek z pismami przychodz�cymi
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.przyjmijpismo"))
        {
			types.put(TAB_IN, smTab.getString("PismaPrzychodzace"));
        }
		// opcja zak�adek z pismami wychodz�cymi
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowychodzace") 
				|| AvailabilityManager.isAvailable("menu.left.kancelaria.simpleprzyjmipismo"))
		{
			types.put(TAB_OUT, smTab.getString("PismaWychodzace"));
		}
		// opcja zak�adek z pismami wewn�trznymi
		if(AvailabilityManager.isAvailable("menu.left.kancelaria.pismowewnetrzne"))
        {
			types.put(TAB_INT, smTab.getString("PismaWewnetrzne"));
        }
		//
        if (Configuration.additionAvailable(Configuration.ADDITION_ORDERS))
        {
           types.put(TAB_ORDER, smTab.getString("Polecenia"));
        }
		// opcja zak�adek z pismami obserwowanymi
		if(AvailabilityManager.isAvailable("menu.left.repository.obserwowane"))
		{
			types.put(TAB_WATCHES, smTab.getString("Obserwowane"));
		}
		// opcja zak�adek - sprawy
		if (AvailabilityManager.isAvailable("sprawy"))
        {
			types.put(TAB_CASES, smTab.getString("Sprawy"));
        }
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow"))
        {
     		
			types.put(TAB_DOCUMENT_PACKAGE, smTab.getString("Paczki"));
        }
		return types;
	}
	
	/**
	 * Przygotowanie do listy wyboru kolumn odpowiednich atrbut�w dockind je�eli takie s� ustawione.
	 * @param lstAllColumns lista kolumn do kt�rej mamy doda� atrybuty
	 */
	public void prepareDockindAttributes(List<String> lstAllColumns) 
	{
	   if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr1").equals("false"))
	    {
	    	lstAllColumns.add("dockindBusinessAtr1");
	    }
	    if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr2").equals("false"))
	    {
	    	lstAllColumns.add("dockindBusinessAtr2");
	    }
	    if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr3").equals("false"))
	    {
	    	lstAllColumns.add("dockindBusinessAtr3");
	    }
	    if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr4").equals("false"))
	    {
	    	lstAllColumns.add("dockindBusinessAtr4");
	    }
	    if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr5").equals("false"))
	    {
	    	lstAllColumns.add("dockindBusinessAtr5");
	    }
	 	if(!Docusafe.getAdditionProperty("column.dockindBusinessAtr6").equals("false"))
	 	{
	 		lstAllColumns.add("dockindBusinessAtr6");
	 	}
	}
	
	/**
	 * Przygotowanie odpowiednich typ�w sortowa� (rosn�co/malej�co)
	 * @return
	 */
	public Map<String, String> prepareAscendingTypes()
	{
		Map<String, String> ascendingType = new HashMap <String, String>();
        ascendingType.put("false", "malej�co");
        ascendingType.put("true", "rosn�co");
        return ascendingType;
	}
	
	/**
	 * Lista etykiet do wyboru przy definiowaniu zak�adki.
	 * @return
	 * @throws EdmException
	 */
	public List<Label> getAvailableLabels() throws EdmException
	{
		List<Label> availableLabels = new Vector<Label>();
		 if (AvailabilityManager.isAvailable("labels"))
		 {
		 	List<Label> flags = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.FLAG_TYPE);
		 	for(Label lab : flags)
		 	{
		 		lab.setAliasName(sm.getString("Flaga")+" " +lab.getName());
		 	}
			availableLabels = flags;
			List<Label> labels = LabelsManager.getReadLabelsForUser(DSApi.context().getPrincipalName(), Label.LABEL_TYPE);
			for(Label lab:labels)
			{
				lab.setAliasName(lab.getName());
			}
			availableLabels.addAll(labels);
			
			Label ll = new Label();
			ll.setId(-1L);
			ll.setAliasName(sm.getString("Wszystkie"));
		    availableLabels.add(ll);
		 }
         return availableLabels;
	}
	
	 public static void parseFilter(FilterCondition fc) throws EdmException {
	    	// to ma by� ta odpowiednia nazwa kolumny
	    	String parsedFilterBy = "";
	    	
	        if(TaskListAction.UNPARSED_FILTER_BY.contains(fc.getColumn())){
	            parsedFilterBy = fc.getColumn();
	        } else if(fc.getValues() != null && fc.getColumn() != null ) 
	        {
	        	parsedFilterBy = fc.getColumn();
	        	try
	        	{
	        		if(parsedFilterBy.equals("clerk"))
	            	{
	        			parsedFilterBy = "documentClerk";
	            	}
	        		else if(parsedFilterBy.equals("sender"))
	            	{
	        			parsedFilterBy = "activity_lastActivityUser"; // dekretujacy
	            	}
	            	TaskSnapshot ts = TaskSnapshot.getInstance();
		            Column dsa = DSApi.getColumn(ts.getClass(), parsedFilterBy);
		            Type t = DSApi.getColumnType(ts.getClass(), parsedFilterBy);
		            parsedFilterBy = dsa.getName();
		            
		            // tutaj nast�puje parsowanie odpowiednich warto�ci kt�re u�ytkownik wpisa� jako String
		            for(String value : fc.getValuesList()) {
		            	// dodatkowo trimujemy
		            	value = value.trim();
		            	
		            	Object filterValue = null;
		            
			            if(parsedFilterBy.equals("documentClerk") 
		            	|| parsedFilterBy.equals("documentAuthor")
		            	||parsedFilterBy.equals("document_author")
		            	||parsedFilterBy.equals("document_clerk")
		            	||parsedFilterBy.equals("dsw_process_context_param_val"))
			            {
			            	DSUser tempUser = null;
			            	String[] tmp = value.split(" ");
			            	try
			            	{
		    	            	tempUser = DSUser.findByFirstnameLastname(tmp[0], tmp[1]);
		    	            	if(tempUser == null)
		    	            	{
		    	            		tempUser = DSUser.findByFirstnameLastname(tmp[1], tmp[0]);
		    	            		if(tempUser != null)
		    	            			filterValue = tempUser.getName();
		    	            	}
		    	            	else
		    	            	{
		    	            		filterValue = tempUser.getName();
		    	            	}
			            	}
			            	catch (Exception e) 
			            	{
			            		log.error(e.getMessage(),e);		
							}
			            }
			            else if(t.getReturnedClass().equals(Integer.class))
			            {
			            	filterValue = Integer.parseInt(value);
			            }
			            else if(t.getReturnedClass().equals(Date.class))
			            {
			            	filterValue = DateUtils.parseDateTimeAnyFormat(value);
			            }
			            else if(t.getReturnedClass().equals(Timestamp.class))
			            {
			            	filterValue = DateUtils.parseDateAnyFormat(value);
			            }
			            else if(t.getReturnedClass().equals(String.class))
			            {
			            	filterValue = value;
			            }
			            else if(t.getReturnedClass().equals(Float.class))
			            {
			            	filterValue = Float.parseFloat(value);
			            }
			            else if(t.getReturnedClass().equals(Long.class))
			            {
			            	filterValue = Long.parseLong(value);
			            }
			            else
			            {
			            	filterValue = value;
			            }
			            // dodanie sparsowanej warto�ci
			            fc.getParsedValues().add(filterValue);
		            }
		            
	        	}
	        	catch (Exception e) 
	        	{
	        		log.warn(e.getMessage(),e);
	        		throw new EdmException("B��d podczas parsowania filtra");
				}
	        	// ustawienie odpowiedniej nazwy kolumny w bazie danych
	        	fc.setColumnTableName(parsedFilterBy);
	        }
	    	
	    }
	
}