package pl.compan.docusafe.core.bookmarks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

public class FilterCondition {
	private Long id;
	private String column;
	// ta kolumna nie jest zapisywana w bazie danych - jest przygotowywana podczas parsowania filtru
	private String columnTableName;
	// ta kolumna nie jest zapisywana w bazie danych - jest przygotowywana podczas parsowania filtru
	private List<Object> parsedValues = new ArrayList<Object>();
	private Boolean active;
	private List<String> values = new ArrayList<String>();
	
	public FilterCondition(String column,String values) {
		this();
		this.column = column;
		setValues(values);
	}
	
	public FilterCondition() {
		this.active = true;
	}
	
	public static FilterCondition find(Long id) throws EdmException
	{
		  return (FilterCondition)Finder.find(FilterCondition.class, id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getValues() {
		String tmp = "";
		for (String s : values) {
			tmp += s+",";
		}
		return tmp.substring(0, tmp.lastIndexOf(","));
	}

	public void setValues(String values) {
		this.values = Arrays.asList(values.split(","));
	}
	
	public List<String> getValuesList() {
		return values;
	}

	public String getColumnTableName() {
		return columnTableName;
	}

	public void setColumnTableName(String columnTableName) {
		this.columnTableName = columnTableName;
	}

	public List<Object> getParsedValues() {
		return parsedValues;
	}

	public void setParsedValues(List<Object> parsedValues) {
		this.parsedValues = parsedValues;
	}
	
	public String asArrayString()
	{
		StringBuilder sb = new StringBuilder();
		for (String value : values) {
			sb.append("'").append(value).append("'");
			sb.append(",");
		}
		
		return sb.substring(0, sb.length()-1);
		
	}
	
}