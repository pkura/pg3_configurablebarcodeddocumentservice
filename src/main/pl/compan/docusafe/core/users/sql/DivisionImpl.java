package pl.compan.docusafe.core.users.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Optional;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.util.IdUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.form.UpdateUser;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionImpl.java,v 1.34 2010/08/04 06:59:44 mariuszk Exp $
 */
public class DivisionImpl extends DSDivision
{
	public final static Logger log = LoggerFactory.getLogger(DivisionImpl.class);
    public final static StringManager sm =
            GlobalPreferences.loadPropertiesFile(DivisionImpl.class.getPackage().getName(),null);

    private Long id;
    private String divisionType;
    private String guid;
    private String name;
    private String code;
    private boolean hidden;
    private String description;
    private String externalId;
    
    private DivisionImpl parent;
    private Set<DSUser> users;
    private Map<Long,String> usersSource;
    
    private Set<DSUser> backupUsers;
    private Set<DSDivision> children;
    
    
    public DivisionImpl()
    {
    }
    
   
    
    DivisionImpl(DivisionImpl parent, String divisionType, String guid, String name, String code, String externalId)
    {
        this.parent = parent;
        this.divisionType = divisionType;
        this.guid = guid;
        this.name = name;
        this.code = code;
        this.externalId = externalId;
    }

    
    void clearUsers() throws EdmException
    {
        if (users != null)
        {
            DSApi.initializeProxy(users);
            users.clear();
        }
    }

    public String getGuid()
    {
        return guid;
    }
    
   
    public String getName()
    {
    	StringBuilder sb = new StringBuilder();
        if(isHidden()){
            sb.append("(").append(sm.getString("ukryty")).append(") ");
        } 
        if(isCodeAsNamePrefix() && code != null) {
        	sb.append(code).append(" ");
        }
        sb.append(name);
        
        return sb.toString();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public void setParent(DivisionImpl parent) {
		this.parent = parent;
	}

	public String getDescription()
    {
        return description;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDivisionType()
    {
        return divisionType;
    }

    public DSDivision getParent() throws DivisionNotFoundException,ReferenceToHimselfException, EdmException
    {
    	if(parent != null && this.getId().equals(parent.getId()))
    		throw new ReferenceToHimselfException(this.getGuid());
        return parent;
    }

    public Optional<DivisionImpl> getParentSafe() {
        return Optional.fromNullable(parent);
    }

    private Set<DSDivision> accessibleChildren;

    public DSDivision[] getChildren() throws EdmException
    {
        if (children != null)
        {
            if (accessibleChildren == null)
            {
                accessibleChildren = new LinkedHashSet<DSDivision>(children.size());
                for (DSDivision child : children)
                {
                    if (!child.isHidden())
                        accessibleChildren.add(child);
                }
            }

            return accessibleChildren.toArray(new DSDivision[accessibleChildren.size()]);
        }

        return new DSDivision[0];
    }
    
    
    public DSDivision[] getAncestors() throws EdmException
    {
        List<DSDivision> childs = new ArrayList<DSDivision>(Arrays.asList(getChildren()));
        List<DSDivision> ret =  new ArrayList<DSDivision>(childs);
        for(DSDivision div:childs)
        {
        	DSDivision[]  acctab = div.getAncestors();
        	if(acctab.length > 0)
        		ret.addAll(Arrays.asList(acctab));
        }
        return ret.toArray(new DSDivision[ret.size()]);
    }
    
    public Long[] getAncestorsIds() throws EdmException
    {
        List<DSDivision> childs = new ArrayList<DSDivision>(Arrays.asList(getChildren()));
        List<Long> ret =  new ArrayList<Long>();
        for(DSDivision div:childs)
        {
        	ret.add(div.getId());
        	Long[]  acctab = div.getAncestorsIds();
        	if(acctab.length > 0)
        		ret.addAll(Arrays.asList(acctab));
        }
        return ret.toArray(new Long[ret.size()]);
    }
    
    
    public DSDivision[] getAncestorssWithoutPositionAndGroup() throws EdmException
    {
        List<DSDivision> childs = new ArrayList<DSDivision>(Arrays.asList(getChildren(DSDivision.TYPE_DIVISION)));
        List<DSDivision> ret =  new ArrayList<DSDivision>(childs);
        for(DSDivision div:childs)
        {
        	if(div.isPosition() || div.isGroup())
        		continue;
        	else
        	{
	        	DSDivision[]  acctab = div.getAncestorssWithoutPositionAndGroup();
	        	if(acctab.length > 0)
	        		ret.addAll(Arrays.asList(acctab));
        	}
        }
        return ret.toArray(new DSDivision[ret.size()]);
    }

    public boolean isRoot()
    {
        return parent == null;
    }

    protected boolean addUserSpi(DSUser user, String source) throws EdmException 
    {
    	if (users == null)
            users = new HashSet<DSUser>();
        else
            DSApi.initializeProxy(users);
    	if (usersSource == null)
    		usersSource = new HashMap<Long, String>();
    	else
    		DSApi.initializeProxy(usersSource);

        if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
            try {
                    JackrabbitPrivileges.instance().addReadPrivilege(null, user.getName(), this.getGuid(), ObjectPermission.SOURCE_DIVISION);
            } catch (Exception e) { throw new EdmException(e); }
        }

    	return UpdateUser.addObjectWithSource(user, source, user.getId(), users, usersSource, this);
    }
  
    protected boolean addUserSpi(DSUser user) throws EdmException
    {
        return addUserSpi(user, "own");
    }
    
    @Override
    protected boolean addUserSpi(DSUser user, boolean backup, String source) throws EdmException 
    {
    	log.trace("addUserSpi");
    	if(addUserSpi(user, source)) 
    	{
    		if (backup) 
    		{
    			log.trace("backup=true");
				if (backupUsers == null) 
				{
					backupUsers=new HashSet<DSUser>();
				} else 
				{
					DSApi.initializeProxy(backupUsers);
				}
				backupUsers.add(user);
			}
    		return true;
    	} else {
    		return false;
    	}
    }
    
    protected boolean addUserSpi(DSUser user,boolean backup) throws EdmException {
    	return addUserSpi(user, backup, "own");
    }
    
    protected boolean removeUserSpi(DSUser user, String source) throws EdmException
    {
        if (users == null || !users.contains(user)) {
        	return false;
        } else {
        	DSApi.initializeProxy(users);
        }
        if (UpdateUser.removeSource(usersSource, user.getId(), source)) {
        	backupUsers.remove(user);
        	return users.remove(user);
        }

        if(AvailabilityManager.isAvailable("jackrabbit.permissions")) {
            try {
                JackrabbitPrivileges.instance().revokeReadPrivilege(null, user.getName(), this.getGuid(), ObjectPermission.SOURCE_DIVISION);
            } catch (Exception e) { throw new EdmException(e); }
        }

        return true;
    }
    
    protected boolean removeUserSpi(DSUser user) throws EdmException
    {
        return removeUserSpi(user, "own");
    }

    public DSDivision createDivision(String name, String code) throws EdmException
    {
        return create(TYPE_DIVISION, name, code);
    }
    
    public DSDivision createDivision(String name,String code, String guid) throws EdmException
    {
        return createImpl(TYPE_DIVISION, name, code, guid, null);
    }
    
    public DSDivision createDivisionWithExternal(String name,String code, String externalId) throws EdmException
    {
        return create(TYPE_DIVISION, name, code, externalId);
    }

    public DSDivision createGroup(String name) throws EdmException
    {
        return create(TYPE_GROUP, name, null);
    }

    public DSDivision createGroup(String name, String guid) throws EdmException
    {
        return createImpl(TYPE_GROUP, name, null, guid, null);
    }    
    
    public DSDivision createPosition(String name) throws EdmException
    {
        return create(TYPE_POSITION, name, null);
    }

    public DSDivision createSwimlane(String name) throws EdmException
    {
        return create(TYPE_SWIMLANE, name, null);
    }
    
    private DivisionImpl create(String type, String name, String code) throws EdmException
    {
        return createImpl(type, name, code, null, null);
    }
    
    private DivisionImpl create(String type, String name, String code, String externalId) throws EdmException
    { 
        return createImpl(type, name, code, null, externalId);
    }
    
    private DivisionImpl createImpl(String type, String name, String code, String guid, String externalId) throws EdmException
    {
        if (type == null)
            throw new NullPointerException("type");
        if (name == null)
            throw new NullPointerException("name");

        name = name.trim();
        DSDivision[] subdivisions = getChildren();
        for (int i=0; i < subdivisions.length; i++){
            if (subdivisions[i].getName().equals(name)){
                throw new DuplicateNameException("Istnieje już element struktury o tej nazwie:" + name);
			}
        }
        DivisionImpl division = new DivisionImpl(this, type, guid != null ? guid : IdUtils.getNetworkSafeGuid(), name, code, externalId);
        Persister.create(division);
        return division;
    }
    
    public DSUser[] getUsers() throws EdmException
    {
    	return getUsersWithDeleted(false);
    }

    public DSUser[] getUsersWithDeleted(Boolean withDeleted) throws EdmException
    {
        if (users != null)
        {
            DSApi.initializeProxy(users);

            // usuwanie z listy użytkowników systemowych
            List<DSUser> tmp = new ArrayList<DSUser>(users.size());
            for (Iterator iter=users.iterator(); iter.hasNext(); )
            {
                DSUser u = (DSUser) iter.next();
                if (!u.isSystem())
                {
                	if((u.isDeleted() && withDeleted )|| !u.isDeleted())
                		tmp.add(u);
                }
            }
            if (!isPosition()) 
            {
	            DSDivision[] subdivisions = getChildren();
	            for (int i=0; i < subdivisions.length; i++)
	            {
	            	if (subdivisions[i].isPosition()) {
	            		DSUser[] subusers = subdivisions[i].getUsers(withDeleted);
	            		for (int j=0; j < subusers.length; j++)
	            			tmp.add(subusers[j]);
	            	}
	            }
            }
            return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
            //return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }

        return new DSUser[0];
    }
    public DSUser[] getUsersWithoutBackup() throws EdmException
    {
        if (users != null)
        {
            DSApi.initializeProxy(users);
            
            // usuwanie z listy użytkowników systemowych
            List<DSUser> tmp = new ArrayList<DSUser>(users.size());
            for (Iterator iter=users.iterator(); iter.hasNext(); )
            {
                DSUser u = (DSUser) iter.next();
                if (!u.isSystem() && !backupUsers.contains(u) && !u.isDeleted())
                {
                		tmp.add(u);
                }
            }
            if (!isPosition()) 
            {
	            DSDivision[] subdivisions = getChildren();
	            for (int i=0; i < subdivisions.length; i++)
	            {
	            	if (subdivisions[i].isPosition()) {
	            		DSUser[] subusers = subdivisions[i].getUsers();
	            		for (int j=0; j < subusers.length; j++){
	            			if(!backupUsers.contains(subusers[j]))
	            				tmp.add(subusers[j]);
	            			
	            		}
	            	}
	            }
            }
            return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
            //return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }

        return new DSUser[0];
    }
    

    public DSUser[] getOriginalUsers() throws EdmException
    {
        if (users != null)
        {
            DSApi.initializeProxy(users);

            // usuwanie z listy użytkowników systemowych
            List<DSUser> tmp = new ArrayList<DSUser>(users.size());
            for (Iterator iter=users.iterator(); iter.hasNext(); )
            {
                DSUser u = (DSUser) iter.next();
                if (!u.isSystem()&& !u.isDeleted())
                    tmp.add(u);
            }
            return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
            //return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }

        return new DSUser[0];
    }
    public DSUser[] getOriginalUsersWithoutBackup() throws EdmException
    {
        if (users != null)
        {
            DSApi.initializeProxy(users);

            // usuwanie z listy użytkowników systemowych
            List<DSUser> tmp = new ArrayList<DSUser>(users.size());
            for (Iterator iter=users.iterator(); iter.hasNext(); )
            {
                DSUser u = (DSUser) iter.next();
                if (!u.isSystem() && !backupUsers.contains(u)&& !u.isDeleted())
                    tmp.add(u);
            }
            return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
            //return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }

        return new DSUser[0];
    }
    
    public DSUser[] getOriginalBackupUsers() throws EdmException
    {
        if (backupUsers != null)
        {
            DSApi.initializeProxy(backupUsers);
            
            // usuwanie z listy użytkowników systemowych
            List<DSUser> tmp = new ArrayList<DSUser>(backupUsers.size());
            for (Iterator iter=backupUsers.iterator(); iter.hasNext(); )
            {
                DSUser u = (DSUser) iter.next();
                if (!u.isSystem() && !u.isDeleted())
                    tmp.add(u);
            }
            return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
            //return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }

        return new DSUser[0];
    }
    
    
    protected DSUser[] getAllUsers() throws EdmException
    {
        if (users != null)
        { 
        	List<DSUser> tmp = new ArrayList<DSUser>();
            tmp.addAll(users);
            if (!isPosition()) 
            {
	            DSDivision[] subdivisions = getChildren();
	            for (int i=0; i < subdivisions.length; i++)
	            {
	            	if (subdivisions[i].isPosition()) {
	            		DSUser[] subusers = subdivisions[i].getUsers();
	            		for (int j=0; j < subusers.length; j++)
	            			tmp.add(subusers[j]);
	            	}
	            }
            }
        	return (DSUser[]) tmp.toArray(new DSUser[tmp.size()]);
        }
        else
        	return new DSUser[0]; 
    }
    
    

    protected DSUser[] getAllOriginalUsers() throws EdmException
    {
        return users != null ? (DSUser[]) users.toArray(new DSUser[users.size()]) : new DSUser[0]; 
    }
    
    public String[] getUserSources(DSUser user) {
    	if (!usersSource.containsKey(user.getId())) {
    		return null;
    	}
    	return StringUtils.split(usersSource.get(user.getId()), ";");
    }
    
    public void update() throws EdmException
    {
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DivisionImpl)) return false;

        final DivisionImpl division = (DivisionImpl) o;

        if (id != null ? !id.equals(division.id) : division.id != null) return false;

        return true;
    }


    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    /* */

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    public void setDivisionType(String divisionType)
    {
        this.divisionType = divisionType;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public void setParent(DSDivision parent)
    {
        this.parent = (DivisionImpl) parent;
    }

    private void setUsers(Set<DSUser> users)
    {
        this.users = users;
    }

    private void setChildren(Set<DSDivision> children)
    {
        this.children = children;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setHidden(boolean hidden)
    {
        this.hidden = hidden; 
    }
    
	public Set<DSUser> getBackupUsers() {
		return backupUsers;
	}

	public void setBackupUsers(Set<DSUser> backupUsers) {
		this.backupUsers = backupUsers;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	

}
