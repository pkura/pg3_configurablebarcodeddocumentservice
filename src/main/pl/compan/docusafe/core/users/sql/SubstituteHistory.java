package pl.compan.docusafe.core.users.sql;

import java.util.*;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import org.apache.pdfbox.pdmodel.graphics.predictor.Sub;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Projections;
import static org.hibernate.criterion.Restrictions.*;

import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;

/**
 * Wpis do historii zast�pstw
 */
public class SubstituteHistory {
	
	private static final Logger log = LoggerFactory.getLogger(SubstituteHistory.class);
	
	private long id;

    /** login osoby zast�powanej */
	private String login;
    /** zast�puje od */
	private Date substitutedFrom;
    /** zast�puje do */
	private Date substitutedThrough;
    /** login osoby zast�puj�cej */
	private String substLogin;

	/** login osoby kt�ra stworzy�a zast�pstwo */
	private String substituteCreator;

	private Date cancelDate;
	
	/**
	 * status  w jakim jest zast�pstwo
	 * W bazie danych ustawiane jest tylko 1 i 2 status.
	 * Ustawienie 3 i 4 jest w metodzie getStatus();
	 */
	private Integer status;

	public static final int NEW = 1;
	public static final int DELETED = 2;
	public static final int UNDER = 3;
	public static final int END = 4;
	
	private static final Map<Integer, String> statusMap;
	static {
		Map<Integer, String> aMap = Maps.newHashMap();
		aMap.put(1, "Nowa");
		aMap.put(2, "Anulowana");
		aMap.put(3, "W trakcie");
		aMap.put(4, "Zako�czona");
		statusMap = Collections.unmodifiableMap(aMap);
	}
	
    @Deprecated
    /** Tylko dla Hibernate */
	public SubstituteHistory(){}
    
	public void delete() throws EdmException
	{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
	}
	
	public static @Nullable SubstituteHistory find(Long id) throws EdmException
	{
		return DSApi.getObject(SubstituteHistory.class, id);
	}
	
	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
	}

	/**
	 * Tworzy nowy obiekt SubstituteHistory. Od razy ustawiany jest status na NEW.
	 *
	 * @param login u�ytkownika kt�ry zostanie zast�piony
	 * @param substitutedFrom data od
	 * @param substitutedThrough data do
	 * @param substLogin u�ytkownik zast�puj�cy
	 * @param substituteCreator u�ytkownik tworz�cy zast�pstwo
	 */
	public SubstituteHistory(String login, Date substitutedFrom, Date substitutedThrough, String substLogin, String substituteCreator)
	{
		this.login = login;
		this.substitutedFrom = substitutedFrom;
		this.substitutedThrough = substitutedThrough;
		this.substLogin = substLogin;
		this.substituteCreator = substituteCreator;
		this.status = NEW;
	}
	
	public SubstituteHistory(String login, Date from, Date through, String substLogin){
		setLogin(login);
		setSubstitutedFrom(from);
		setSubstitutedThrough(through);
		setSubstLogin(substLogin);
	}

    public SubstituteHistory(SubstitutionDto dto) throws EdmException {
        setLogin(dto.getUserSubsituted().getUsername());
        setSubstLogin(dto.getUserSubstituting().getUsername());
        setSubstitutedFrom(DateUtils.startOfDay(dto.getStart().toDate()));
        setSubstitutedThrough(DateUtils.endOfDay(dto.getEnd().toDate()));
        setStatus(NEW);
        setSubstituteCreator(DSApi.context().getPrincipalName());
        DateUtils.validateFutureRange(dto.getStart().toDate(), dto.getEnd().toDate());
        if(validateSubstitution(login,substitutedFrom,substitutedThrough)){
            throw new EdmException("Nie mo�na przypisa� dw�ch zast�pstw przypadaj�cych na ten sam okres.");
        }

    }
	/**
	 * Zwraca liste zastepowanych uzytkownik�w przez uzytkownika cueUser
	 * Zwraca tylko te zast�pstwa kt�re nie zosta�y anulowane
	 * @param curUser
	 * @return
	 * @throws EdmException
	 */
    @SuppressWarnings("unchecked")
	public static List<SubstituteHistory> findActualSubstituted(String curUser) throws EdmException
	{
		Date now = new Date();
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(eq("substLogin", curUser).ignoreCase());
        criteria.add(ge("substitutedThrough", now));
        criteria.add(le("substitutedFrom", now));
        criteria.add(ne("status", DELETED));
        return criteria.list();
	}
   	
    @SuppressWarnings("unchecked")
    public static List<SubstituteHistory> getSubstitutionsAtTime(Date dateTime) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(ge("substitutedThrough", dateTime));
        criteria.add(le("substitutedFrom", dateTime));
        criteria.add(ne("status", DELETED));
        return criteria.list();
    }

	/**
	 * Zwraca liste zastepowanych uzytkownik�w przez uzytkownika cueUser bez uwzgledniania daty "Do"
	 * Zwraca tylko te zast�pstwa kt�re nie zosta�y anulowane
	 * @param curUser
	 * @return
	 * @throws EdmException
	 */
	public static List<SubstituteHistory> findActualSubstitutedWithoutThrough(String curUser) throws EdmException
	{
		Date now = new Date();
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(eq("substLogin", curUser).ignoreCase());
        criteria.add(le("substitutedFrom", now));
        criteria.add(ne("status", SubstituteHistory.END));
        return criteria.list();
	}
	
	/**
	 * Zwraca uzytkownika kt�ry zast�puj� bierz�cego u�ytkownika curUser
     * Zwraca list� aktualnych zast�pstw, zawi�raj�c� informacj� o zast�pstwach wykonywanych
     * przez danego u�ytkownika (dany u�ytkownik zast�puje kogo�)
     * @param login login u�ytkownika
     * @return lista, mo�e by� pusta
     * @throws EdmException
     */
    public static List<SubstituteHistory> getSubstitutedByUser(String login) throws EdmException {
        Date now = new Date();
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(eq("substLogin", login).ignoreCase());
        criteria.add(ge("substitutedThrough", now));
        criteria.add(le("substitutedFrom", now));
        criteria.add(ne("status", DELETED));
        return criteria.list();
    }
   	
	/**
	 * Zwraca uzytkownika kt�ry zast�puj� u�ytkownika curUser
	 * Zwraca tylko te zast�pstwa kt�re nie zosta�y anulowane
	 **/
	public static SubstituteHistory findActualSubstitute(String curUser) throws EdmException
	{
		Date now = new Date();
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(eq("login", curUser).ignoreCase());
        criteria.add(ge("substitutedThrough", now));
        criteria.add(le("substitutedFrom", now));
        criteria.add(ne("status", DELETED));
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (SubstituteHistory) list.get(0);
            }
            return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
	}
   	
	/**
	 * Zwraca uzytkownik�w kt�rzy zast�puja u�ytkownika wskazanego przez login
	 **/
	public static List<SubstituteHistory> listActualSubstitutes(String userLogin) throws EdmException
	{
		Date now = new Date();
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(eq("login", userLogin).ignoreCase());
       // criteria.add(ge("substitutedThrough", now));
        criteria.add(le("substitutedFrom", now));
        return criteria.list();
	}
	
	
	/**
	 * @param user - name uzytkownika
	 * @param fromDate - data poczatkowa
	 * @param toDate - data koncowa
	 * @return Zwraca true jezeli uzytkownik zast�puje kogo� w podanym przedziale czasu
	 * @throws EdmException
	 */
	public static Boolean checkSubstitiute(String user, Date fromDate, Date toDate) throws EdmException
	{
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(or(between("substitutedThrough", fromDate, toDate), between("substitutedFrom", fromDate, toDate)));
        criteria.add(eq("substLogin", user).ignoreCase());
        criteria.setProjection(Projections.count("substLogin")); 
        List list = criteria.list();
        if(list.get(0).equals(0l)){
        	return false;
        }else{
        	return true;
        }
	}

    /**
     * @param user - name uzytkownika
     * @param fromDate - data poczatkowa
     * @param toDate - data koncowa
     * @return Zwraca true jezeli uzytkownik jest zastepowany w podanym przedziale czasu
     * @throws EdmException
     */
    public static Boolean validateSubstitution(String user, Date fromDate, Date toDate) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(SubstituteHistory.class);
        criteria.add(or(between("substitutedThrough", fromDate, toDate), between("substitutedFrom", fromDate, toDate)));
        criteria.add(eq("login", user).ignoreCase());
        criteria.add(not(eq("status", DELETED)));
        List list = criteria.list();
        if(list.size()==0){
            return false;
        }else{
            return true;
        }
    }
	
    /**
     * Cancels this substitution
     */
    public void cancel(){
        this.cancelDate = new Date();
        this.status = DELETED;
    }
	
	public String getSubstituteCreator(){
		return substituteCreator;
	}

	public void setSubstituteCreator(String substituteCreator){
		this.substituteCreator = substituteCreator;
	}
	
	public void setId(long id){
		this.id=id;
	}
	
	public long getId(){
		return id;
	}
	
	public void setLogin(String login){
		this.login=login;
	}
	
	public String getLogin(){
		return login;
	}
	
	public void setSubstitutedFrom(Date date){
		this.substitutedFrom=date;
	}
	
	public Date getSubstitutedFrom(){
		return substitutedFrom;
	}
	
	public void setSubstitutedThrough(Date date){
		this.substitutedThrough=date;
	}

    public Date getSubstitutedThrough(){
		return substitutedThrough;
	}
	
	public void setSubstLogin(String login){
		this.substLogin=login;
	}
	
	public String getSubstLogin(){
		return substLogin;
	}
	
	public static Map<Integer, String> getStatusMap()
	{
		return statusMap;
	}

	/**
	 * Uwaga! status jest zmieniany dynamicznie
	 * Nie jest to tylko  getter. Do bazy danych s� wpisywane tylko statusy NEW i DELETED.
	 * Statusy END i UNDER ustawiane s� w tej metodzie na podstawie dat.
	 * @return
	 */
	public Integer getStatus()
	{
		if (status != null && status == DELETED)
			return DELETED;
		else
		{
			if (DateUtils.isGreaterDate(new Date(), this.getSubstitutedFrom()) && DateUtils.isGreaterDate(this.getSubstitutedThrough(), new Date()))
				return UNDER;
			else if (DateUtils.isGreaterDate(new Date(), this.getSubstitutedFrom()) && DateUtils.isGreaterDate(new Date(), this.getSubstitutedThrough()))
				return END;
			else
				return NEW;
		}
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}
	
	public Date getCancelDate()
	{
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate)
	{
		this.cancelDate = cancelDate;
	}
}
