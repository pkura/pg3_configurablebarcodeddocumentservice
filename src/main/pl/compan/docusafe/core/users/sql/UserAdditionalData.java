package pl.compan.docusafe.core.users.sql;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

public class UserAdditionalData {
	private Long id;
	private Long userId;
	private String mailFooter;
	
	public static UserAdditionalData find(Long id) throws EdmException{
		UserAdditionalData userAdditionalData = (UserAdditionalData)DSApi.getObject(UserAdditionalData.class, id);
		 
        if (userAdditionalData == null)
            throw new EntityNotFoundException(UserAdditionalData.class, id);

        return userAdditionalData;	
	}
	public static UserAdditionalData findByUserId(Long userId) throws HibernateException, EdmException{
		return (UserAdditionalData) DSApi.context().session().createCriteria(UserAdditionalData.class).add(Restrictions.eq("userId", userId)).uniqueResult();
	}
	public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public void delete() throws EdmException
    {
        Persister.delete(this);
    }
	public Long getId() {
		return id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getMailFooter() {
		return mailFooter;
	}
	public void setMailFooter(String mailFooter) {
		this.mailFooter = mailFooter;
	}
}
