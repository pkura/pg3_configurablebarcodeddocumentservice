package pl.compan.docusafe.core.users.sql;

public class UserAddress {

	private Long nrewid, id;
	private String kodpoczt, poczta, miejscowosc, ulica, nrdomu, nrmieszkania, rodzajadresu;
	
	public UserAddress(){
		
	}
	
	public UserAddress(Long nrewid, Long id, String kodpoczt, String poczta,
			String miejscowosc, String ulica, String nrdomu,
			String nrmieszkania, String rodzajadresu) {
		super();
		this.nrewid = nrewid;
		this.id = id;
		this.kodpoczt = kodpoczt;
		this.poczta = poczta;
		this.miejscowosc = miejscowosc;
		this.ulica = ulica;
		this.nrdomu = nrdomu;
		this.nrmieszkania = nrmieszkania;
		this.rodzajadresu = rodzajadresu;
	}
	public Long getNrewid() {
		return nrewid;
	}
	public void setNrewid(Long nrewid) {
		this.nrewid = nrewid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKodpoczt() {
		return kodpoczt;
	}
	public void setKodpoczt(String kodpoczt) {
		this.kodpoczt = kodpoczt;
	}
	public String getPoczta() {
		return poczta;
	}
	public void setPoczta(String poczta) {
		this.poczta = poczta;
	}
	public String getMiejscowosc() {
		return miejscowosc;
	}
	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getNrdomu() {
		return nrdomu;
	}
	public void setNrdomu(String nrdomu) {
		this.nrdomu = nrdomu;
	}
	public String getNrmieszkania() {
		return nrmieszkania;
	}
	public void setNrmieszkania(String nrmieszkania) {
		this.nrmieszkania = nrmieszkania;
	}
	public String getRodzajadresu() {
		return rodzajadresu;
	}
	public void setRodzajadresu(String rodzajadresu) {
		this.rodzajadresu = rodzajadresu;
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof UserAddress)) return false;
		else
		{
			UserAddress temp = (UserAddress) o;
			if(temp.getId().equals(getId())) return true;
		}
		return false;
		
	}
}
