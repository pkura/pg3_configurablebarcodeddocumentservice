package pl.compan.docusafe.core.users.sql;

import java.util.Date;

public class UserData {

	private Long id, nrewid, pesel, nip;
	private String imie, nazwisko, drugieimie, nazwiskorodowe, miejsceur, dowodosnr, umowastanowisko;
	private Date dataur, badanielekwaznosc, umowadatakoncowa;
	
	private static final long serialVersionUID = 1L;
	
	public UserData(){
		
	}
	
	public UserData(Long id, Long nrewid, Long pesel, Long nip, String imie,
			String nazwisko, String drugieimie, String nazwiskorodowe,
			String miejsceur, String dowodosnr, String umowastanowisko,
			Date dataur, Date badanielekwaznosc, Date umowadatakoncowa) {
		super();
		this.id = id;
		this.nrewid = nrewid;
		this.pesel = pesel;
		this.nip = nip;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.drugieimie = drugieimie;
		this.nazwiskorodowe = nazwiskorodowe;
		this.miejsceur = miejsceur;
		this.dowodosnr = dowodosnr;
		this.umowastanowisko = umowastanowisko;
		this.dataur = dataur;
		this.badanielekwaznosc = badanielekwaznosc;
		this.umowadatakoncowa = umowadatakoncowa;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNrewid() {
		return nrewid;
	}
	public void setNrewid(Long nrewid) {
		this.nrewid = nrewid;
	}
	public Long getPesel() {
		return pesel;
	}
	public void setPesel(Long pesel) {
		this.pesel = pesel;
	}
	public Long getNip() {
		return nip;
	}
	public void setNip(Long nip) {
		this.nip = nip;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getDrugieimie() {
		return drugieimie;
	}
	public void setDrugieimie(String drugieimie) {
		this.drugieimie = drugieimie;
	}
	public String getNazwiskorodowe() {
		return nazwiskorodowe;
	}
	public void setNazwiskorodowe(String nazwiskorodowe) {
		this.nazwiskorodowe = nazwiskorodowe;
	}
	public String getMiejsceur() {
		return miejsceur;
	}
	public void setMiejsceur(String miejsceur) {
		this.miejsceur = miejsceur;
	}
	public String getDowodosnr() {
		return dowodosnr;
	}
	public void setDowodosnr(String dowodosnr) {
		this.dowodosnr = dowodosnr;
	}
	public String getUmowastanowisko() {
		return umowastanowisko;
	}
	public void setUmowastanowisko(String umowastanowisko) {
		this.umowastanowisko = umowastanowisko;
	}
	public Date getDataur() {
		return dataur;
	}
	public void setDataur(Date dataur) {
		this.dataur = dataur;
	}
	public Date getBadanielekwaznosc() {
		return badanielekwaznosc;
	}
	public void setBadanielekwaznosc(Date badanielekwaznosc) {
		this.badanielekwaznosc = badanielekwaznosc;
	}
	public Date getUmowadatakoncowa() {
		return umowadatakoncowa;
	}
	public void setUmowadatakoncowa(Date umowadatakoncowa) {
		this.umowadatakoncowa = umowadatakoncowa;
	}
	
	@Override 
	public boolean equals(Object o){
		if(!(o instanceof UserData)) return false;
		else
		{
			UserData temp = (UserData) o;
			if(temp.equals(this));
		}
		return false;
	}

}
