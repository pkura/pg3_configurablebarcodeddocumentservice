package pl.compan.docusafe.core.users.sql;

import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Preconditions;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.crypto.DSA;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.UserSearchBean;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.function.Function;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SqlUserFactory.java,v 1.52 2010/08/17 08:05:01 pecet1 Exp $
 */
public class SqlUserFactory extends UserFactory
{
    private static final Logger log = LoggerFactory.getLogger(SqlUserFactory.class);

    public static final String NAME = "sql";

    public static void updatePasswordHistory(String username, String password)
        throws EdmException
    {
        if (username == null)
            throw new NullPointerException("username");
        if (password == null)
            throw new NullPointerException("password");

        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(
                "insert into ds_user_passwd_history (username, hashedpassword, ctime) values (?, ?, current_timestamp)");
            ps.setString(1, username);
            ps.setString(2, hashPassword(password));
            ps.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
    
    public static void reloadSubstitute() throws EdmException
    {
		List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
		for (DSUser dsUser : users) 
		{
			SubstituteHistory sh = SubstituteHistory.findActualSubstitute(dsUser.getName());
			dsUser.setSubstitutionFromHistory(sh);
		}
    }
    

    public static boolean passwordInHistory(String username, String password)
        throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement(
                "select count(*) from ds_user_passwd_history where username = ? and hashedpassword = ?");
            ps.setString(1, username);
            ps.setString(2, hashPassword(password));
            ResultSet rs = ps.executeQuery();
            rs.next();
            int count = rs.getInt(1);

            return count > 0;
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

    public int getUserCount() throws EdmException
    {
        try
        {
            List list = DSApi.context().classicSession().find(
                "select count(*) from u in class "+UserImpl.class.getName()+
                " where u.deleted = ? and u.system = ?",
                new Object[] { Boolean.FALSE, Boolean.FALSE },
                new Type[] { Hibernate.BOOLEAN, Hibernate.BOOLEAN });

            return ((Long) list.get(0)).intValue();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public DSUser findByExternalNameSpi(String username) throws EdmException, UserNotFoundException
    {
        String key = getClass().getName()+".userid."+username;

        try
        {
            Long id = (Long) DSApi.context().getAttribute(key);

            if (id != null)
            {
                return (DSUser) DSApi.context().session().load(UserImpl.class, id);
            }
            else
            {
                List list = DSApi.context().classicSession().find(
                    "from u in class "+UserImpl.class.getName()+
                    " where u.externalName = ?", username, Hibernate.STRING);

                if (list.size() > 0)
                {
                    UserImpl user = (UserImpl) list.get(0);
                    DSApi.context().setAttribute(key, user.getId());
                    return user;
                }
                throw new UserNotFoundException(username);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public DSUser findByUsernameSpi(String username) throws EdmException, UserNotFoundException
    {
        // w cache trzymane jest id u�ytkownika indeksowane jego
        // nazw�, poniewa� Hibernate za ka�dym razem wywo�uje
        // zapytanie SQL podczas szukania po nazwie u�ytkownika

        String key = getClass().getName()+".userid."+username;

        try
        {
            Long id = (Long) DSApi.context().getAttribute(key);

            if (id != null)
            {
                //log.debug("cache "+username+" => "+id);
                return (DSUser) DSApi.context().session().load(UserImpl.class, id);
            }
            else
            {
                List list = DSApi.context().classicSession().find(
                    "from u in class "+UserImpl.class.getName()+
                    " where u.name = ?", username, Hibernate.STRING);

                if (list.size() > 0)
                {
                    UserImpl user = (UserImpl) list.get(0);
                    DSApi.context().setAttribute(key, user.getId());
                    return user;
                }
                throw new UserNotFoundException(username);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public DSUser findByExtensionSpi(String extension) throws EdmException, UserNotFoundException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.eq("extension", extension).ignoreCase());
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSUser) list.get(0);
            }
            throw new UserNotFoundException(extension);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public DSUser findByIdentifierSpi(String identifier) throws EdmException, UserNotFoundException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.eq("identifier", identifier).ignoreCase());
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSUser) list.get(0);
            }
            throw new UserNotFoundException(identifier);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public DSUser findByEmailSpi(String email) throws EdmException, UserNotFoundException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Expression.eq("email", email).ignoreCase());
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSUser) list.get(0);
            }
            throw new UserNotFoundException(email);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public DSUser findByIdSpi(Long id) throws EdmException, UserNotFoundException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Expression.eq("id", id));
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSUser) list.get(0);
            }
            throw new UserNotFoundException("User id: " + id);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public DSUser findByCertificateSpi(X509Certificate certificate) throws EdmException, UserNotFoundException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("select ds_user.id from ds_user join ds_user_cert on ds_user.id = ds_user_cert.user_id where ds_user_cert.signature = ?");

            String sig = new String(Base64.encodeBase64(certificate.getSignature()));

            ps.setString(1, sig);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                throw new UserNotFoundException(certificate.getSubjectDN().getName());

            return (DSUser) DSApi.context().session().load(UserImpl.class, new Long(rs.getLong(1)));
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

    public DSDivision findDivisionSpi(String guid) throws DivisionNotFoundException, EdmException
    {
        Preconditions.checkNotNull(guid, "guid cannot be null");

        String key = getClass().getName()+".divisionid."+guid;
        try
        {
            Long id = (Long) DSApi.context().getAttribute(key);
            if (id != null)
            {
                if (log.isDebugEnabled())
                    log.debug("cache guid "+guid+" => "+id);
                return (DSDivision) DSApi.context().session().load(DivisionImpl.class, id);
            }
            else
            {
                Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
                criteria.add(Restrictions.eq("guid", guid).ignoreCase());
                List list = criteria.list();
                if (list.size() > 0)
                {
                    return (DSDivision) list.get(0);
                }else if(list.size() == 0){
                	DSDivision division = DivisionImpl.findByName(guid);
                	String nameToGuid = division.getGuid(); 
                	criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
                	criteria.add(Restrictions.eq("guid", nameToGuid).ignoreCase());
                	list = criteria.list();
	                	if(list.size() > 0){
	                		return (DSDivision) list.get(0);
	                	}
                }
                throw new DivisionNotFoundException(guid);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public List findDivisionsByDescriptionSpi(String desc) throws Exception 
    {
    	try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.eq("description", desc));
            return criteria.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public List findDivisionsByLikeNameSpi(String desc) throws Exception 
    {
    	try
        {
    		desc = "%"+desc+"%";
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.like("name", desc));
            return criteria.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public List findDivisionsByLikeCodeSpi(String desc) throws Exception 
    {
    	try
        {
    		desc = "%"+desc+"%";
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.like("code", desc));
            return criteria.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    
    
    public DSDivision findDivisionById(long id) throws DivisionNotFoundException, EdmException
    {
    	try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.eq("id", id));
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSDivision) list.get(0);
            }
            throw new DivisionNotFoundException(String.valueOf(id));
            
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public DSDivision findDivisionByNameSpi(String name) throws DivisionNotFoundException, EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.eq("name", name));
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSDivision) list.get(0);
            }
            throw new DivisionNotFoundException(name);
            
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
	public DSDivision findDivisionByCodeSpi(String code) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session()
					.createCriteria(DivisionImpl.class);
			criteria.add(Restrictions.eq("code", code));

			log.info("Zapytanie z criteria: " + criteria.toString());

			List list = criteria.list();
			if (list.size() > 0)
				return (DSDivision) list.get(0);
			throw new DivisionNotFoundException(code);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public List findDivisionListByCodeSpi(String code) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session()
					.createCriteria(DivisionImpl.class);
			criteria.add(Restrictions.eq("code", code));

			log.info("Zapytanie z criteria: " + criteria.toString());

				return criteria.list();
			
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
		
	}
    
    public DSDivision findDivisionByExternalIdSpi(String externalId) throws DivisionNotFoundException, EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.eq("externalId", externalId));
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSDivision) list.get(0);
            }
            throw new DivisionNotFoundException(externalId);
            
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
        
    public DSDivision findByNameAndParentIdSpi(String name, DSDivision division) throws DivisionNotFoundException, EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
            criteria.add(Restrictions.eq("name", name));
            criteria.add(Restrictions.eq("parent", division));
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSDivision) list.get(0);
            }
            throw new DivisionNotFoundException(name);
            
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    private String changeWildcards(String query) {
    	String sqlLikeQuery = query.replace('*', '%');
    	sqlLikeQuery = sqlLikeQuery.replace('?', '_');
    	return sqlLikeQuery;
    }
    public SearchResults<DSUser> searchUsers(int offset, int limit, int sortMode, boolean ascending, 
    		String query, UserSearchBean bean,boolean deleted) throws EdmException
	{
    	return searchUsers(offset, limit, sortMode, ascending, query, bean, deleted, false,null);
	}
    
    public SearchResults<DSUser> searchUsers(int offset, int limit, int sortMode, boolean ascending, 
    		String query, UserSearchBean bean,boolean deleted,boolean onlyDeleted,Boolean loginDisabled) throws EdmException
    {
    	FromClause from = new FromClause();
        TableAlias userTable = from.createTable(DSApi.getTableName(UserImpl.class));
        WhereClause where = new WhereClause();

        if(loginDisabled != null)
        {
        	where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ne(userTable.attribute("loginDisabled"), !loginDisabled));
        }
        if(onlyDeleted)
        {
        	where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(userTable.attribute("deleted"), Boolean.TRUE));
        }
        else
        {
        	if(!deleted)
        		where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(userTable.attribute("deleted"), Boolean.FALSE));
        }
        where.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(userTable.attribute("systemuser"), Boolean.FALSE));
        pl.compan.docusafe.util.querybuilder.expression.Junction AND = pl.compan.docusafe.util.querybuilder.expression.Expression.conjunction();
        
        
        if (query != null)
        {
        	query = changeWildcards(query);
            pl.compan.docusafe.util.querybuilder.expression.Junction OR =
                pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
            OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
                Function.upper(userTable.attribute("name")),
                StringUtils.substring(query.toUpperCase(), 0, 61)+"%"));
            OR.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
                Function.upper(userTable.attribute("lastname")),
                StringUtils.substring(query.toUpperCase(), 0, 61)+"%"));
            AND.add(OR);
        }
        if (bean != null) {
        	//TableAlias location = from.createJoinedTable(DSApi.getTableName(DicLocation.class), true, userTable, FromClause.LEFT_OUTER, "$l.location = $r.id");
        	TableAlias superior = from.createJoinedTable(DSApi.getTableName(UserImpl.class), true, userTable, FromClause.LEFT_OUTER, "$l.supervisor_id = $r.id");
        	pl.compan.docusafe.util.querybuilder.expression.Junction junc;
            
            if (bean.getDisjunction()) {
            	junc =  pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
            } else {
            	junc =  pl.compan.docusafe.util.querybuilder.expression.Expression.conjunction();
            }
            if (bean.getActive() != null) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.eq(
            			Function.upper(userTable.attribute("logindisabled")), !bean.getActive()));
            }
            if (StringUtils.isNotEmpty(bean.getCellPhone())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(userTable.attribute("mobile_phone_num")), changeWildcards(bean.getCellPhone())));
            }
            if (StringUtils.isNotEmpty(bean.getNomadicPhone())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(userTable.attribute("phone_num")), changeWildcards(bean.getNomadicPhone())));
            }
            if (StringUtils.isNotEmpty(bean.getEmail())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(userTable.attribute("email")), changeWildcards(bean.getEmail())));
            }
            if (StringUtils.isNotEmpty(bean.getKpx())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(userTable.attribute("kpx")), changeWildcards(bean.getKpx())));
            }
            if (StringUtils.isNotEmpty(bean.getPesel())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(userTable.attribute("pesel")), changeWildcards(bean.getPesel())));
            }
            if (StringUtils.isNotEmpty(bean.getSuperior())) {
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(superior.attribute("name")), changeWildcards(bean.getSuperior())));
            	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.like(
            			Function.upper(superior.attribute("lastname")), changeWildcards(bean.getSuperior())));
            	
            }
            if (bean.getCreatedFrom() != null) {
            	if (bean.getCreatedTo() == null) {
    	        	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(
    	        			Function.upper(userTable.attribute("ctime")), bean.getCreatedFrom()));
            	} else {
            		pl.compan.docusafe.util.querybuilder.expression.Junction dateJunc = 
            			pl.compan.docusafe.util.querybuilder.expression.Expression.conjunction();
            		dateJunc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(
    	        			Function.upper(userTable.attribute("ctime")), bean.getCreatedFrom()));
            		dateJunc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(
                			Function.upper(userTable.attribute("ctime")), bean.getCreatedTo()));
            		junc.add(dateJunc);
            	}
            } else {
    	        if (bean.getCreatedTo() != null) {
    	        	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(
    	        			Function.upper(userTable.attribute("ctime")), bean.getCreatedTo()));
    	        }
            }
            if (bean.getValidFrom() != null) {
            	if (bean.getValidTo() == null) {
    	        	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(
    	        			Function.upper(userTable.attribute("validity_date")), bean.getValidFrom()));
            	} else {
            		pl.compan.docusafe.util.querybuilder.expression.Junction dateJunc = 
            			pl.compan.docusafe.util.querybuilder.expression.Expression.conjunction();
            		dateJunc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.ge(
    	        			Function.upper(userTable.attribute("validity_date")), bean.getValidFrom()));
            		dateJunc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(
    	        			Function.upper(userTable.attribute("validity_date")), bean.getValidTo()));
            		junc.add(dateJunc);
            	}
            } else {
    	        if (bean.getValidTo() != null) {
    	        	junc.add(pl.compan.docusafe.util.querybuilder.expression.Expression.le(
    	        			Function.upper(userTable.attribute("validity_date")), bean.getValidTo()));
    	        }
            }
            if (!junc.isEmpty())
            	AND.add(junc);
        }
        where.add(AND);

        SelectClause select = new SelectClause(true);
        SelectColumn idCol = select.add(userTable, "id");

        OrderClause order = null;
        switch (sortMode)
        {
            case DSUser.SORT_NAME:
                order = new OrderClause();
                order.add(userTable.attribute("name"), Boolean.valueOf(ascending));
                select.add(userTable, "name");
                break;
            case DSUser.SORT_LASTNAME_FIRSTNAME:
                order = new OrderClause();
                order.add(userTable.attribute("lastname"), Boolean.valueOf(ascending));
                select.add(userTable, "lastname");
                break;
            case DSUser.SORT_FIRSTNAME_LASTNAME:
                order = new OrderClause();
                order.add(userTable.attribute("firstname"), Boolean.valueOf(ascending));
                select.add(userTable, "firstname");
                break;
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(*)");

        SelectQuery selectQuery;
        int totalCount;
        ResultSet rs = null;
        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            List params = new ArrayList(10);
            log.debug(selectQuery.toSqlString(params, new LinkedList<Object>(), false));
            
            for (Object param : params) {
            	log.debug("param: " + param.toString());
            }
            rs = selectQuery.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby u�ytkownik�w");

            totalCount = rs.getInt(countCol.getPosition());

            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            if(DSApi.isOracleServer())
            {
                selectQuery = new SelectQuery(select, from, where, order);
                if (limit > 0) {
                    selectQuery.setMaxResults(limit+offset);
                    selectQuery.setFirstResult(offset);
                }
            }
            else
            {
                selectQuery = new SelectQuery(select, from, where, order);
                if (limit > 0) {
                    selectQuery.setMaxResults(limit);
                    selectQuery.setFirstResult(offset);
                }
            }

            //rs = selectQuery.resultSet(DSApi.context().session().connection());
            
            rs = selectQuery.resultSet();
            
            
            List<DSUser> results = new ArrayList<DSUser>(limit > 0 ? limit : 64);

            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                //results.add((DSUser) DSApi.context().session().load(UserImpl.class, id));
                results.add(DSApi.context().load(UserImpl.class, id));
            }

            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<DSUser>(results, offset, totalCount, DSUser.class);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        } finally {
            DbUtils.closeQuietly(rs);
        }

    }
    public SearchResults<DSUser> searchUsersSpi(int offset, int limit, int sortMode, boolean ascending, String query,boolean deleted) throws EdmException
    {
    	return searchUsers(offset, limit, sortMode, ascending, query, null,deleted);
    }


    protected DSUser[] getUsersSpi() throws EdmException
    {
        try
        {
            List<DSUser> list = (List<DSUser>) DSApi.context().classicSession().find("from u in class "+UserImpl.class.getName());
            return list.toArray(new DSUser[list.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    protected DSDivision[] getDivisionsSpi() throws EdmException
    {
        try
        {
            List<DSDivision> list = (List<DSDivision>) DSApi.context().session().createCriteria(DivisionImpl.class).list();
            return list.toArray(new DSDivision[list.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    /**
     * Zwraca liste z sortowaniem malejacym
     * wykorzystywane w archiwum
     * @return
     * @throws EdmException
     */
    protected DSDivision[] getDivisionsDesc() throws EdmException
    {
        try
        {
            List<DSDivision> list = (List<DSDivision>) DSApi.context().classicSession().find("from u in class "+DivisionImpl.class.getName() +" order by id desc");
            return list.toArray(new DSDivision[list.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    protected DSDivision[] getDivisionsSpiAsList() throws EdmException
    {
        try
        {
            List<DSDivision> list = (List<DSDivision>) DSApi.context().classicSession().find("from u in class "+DivisionImpl.class.getName());
            return list.toArray(new DSDivision[list.size()]);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    /**
     * 
     * @return zwraca list� wszystkich dzia��w i grup, a w przypadku gdy jest to wersja dla impulsa takze cz�� grup
     * @throws EdmException
     */
    protected List<DSDivision> getDivisionsDivisionOrGroupType() throws EdmException
    {
        try
        {
        	if(DocumentKind.isAvailable(DocumentLogicLoader.DL_BINDER))
        	{
                return (List<DSDivision>) DSApi.context().classicSession().find("from u in class "+DivisionImpl.class.getName() +" where divisiontype like 'division' or guid like 'DL_BINDER%' order by name" );
        	}
        	else
        	{
        		return (List<DSDivision>) DSApi.context().classicSession().find("from u in class "+DivisionImpl.class.getName() +" where HIDDEN = false and divisiontype like 'division' order by name" );
        	}
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    protected List<DSDivision> getDivisionsDivisionType() throws EdmException
    {
        try
        {
            return (List<DSDivision>) DSApi.context().classicSession().find("from u in class "+DivisionImpl.class.getName() +" where divisiontype like 'division'" );
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    @SuppressWarnings("unchecked")
    protected DSDivision[] getOnlyDivisionsAndPositions() throws EdmException
    {
    	return getOnlyDivisionsAndPositions(true);
    }
    
    /**
     * @param hidden Czy pobrac z ukrytymi dzialami?
     */
    protected DSDivision[] getOnlyDivisionsAndPositions(boolean hidden) throws EdmException {
    	  Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
          criteria.add(Restrictions.or(Restrictions.eq("divisionType", DSDivision.TYPE_DIVISION),
        		  Restrictions.eq("divisionType", DSDivision.TYPE_POSITION)));
          //criteria.add(Restrictions.eq("divisionType", DSDivision.TYPE_DIVISION));
          if (!hidden) {
        	  criteria.add(Restrictions.eq("hidden", false));
          }
          criteria.addOrder(Order.asc("name")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
          try
          {
              List<DSDivision> list = (List<DSDivision>) criteria.list();
              return list.toArray(new DSDivision[list.size()]);
          }
          catch (HibernateException e)
          {
              throw new EdmHibernateException(e);
          }
    }
    
    /**
     * @param hidden Czy pobrac z ukrytymi dzialami?
     */
    protected DSDivision[] getOnlyDivisions(boolean hidden) throws EdmException {
    	  Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
          criteria.add(Restrictions.eq("divisionType", DSDivision.TYPE_DIVISION));
          //criteria.add(Restrictions.eq("divisionType", DSDivision.TYPE_DIVISION));
          if (!hidden) {
        	  criteria.add(Restrictions.eq("hidden", false));
          }
          criteria.addOrder(Order.asc("name")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
          try
          {
              List<DSDivision> list = (List<DSDivision>) criteria.list();
              return list.toArray(new DSDivision[list.size()]);
          }
          catch (HibernateException e)
          {
              throw new EdmHibernateException(e);
          }
    }
    
    protected DSUser createUserSpi(String username, String firstname, String lastname,
                                   boolean system) throws EdmException
    {
        UserImpl user = new UserImpl(username);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setSystem(system);
        Persister.create(user);
        return user;
    }

    protected void deleteUserSpi(String username) throws EdmException
    {
        ((UserImpl) findByUsername(username)).setDeleted(true);
    }
    
    protected void revertUserSpi(String username) throws EdmException
    {
        ((UserImpl) findByUsername(username)).setDeleted(false);
    }

    protected void deleteDivisionSpi(DSDivision dsDivision) throws EdmException
    {
        DivisionImpl division = (DivisionImpl) findDivision(dsDivision.getGuid());
        division.clearUsers();
        Persister.delete(division);
    }

    @SuppressWarnings("unchecked")
    protected DSDivision[] getGroupsSpi() throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(DivisionImpl.class);
        criteria.add(Restrictions.eq("divisionType", DSDivision.TYPE_GROUP));

        try {
            List<DSDivision> list = (List<DSDivision>) criteria.list();
            return list.toArray(new DSDivision[list.size()]);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    protected void removeUsers(DSDivision division)
        throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("delete from ds_user_to_division where division_id = ?");

            ps.setLong(1, ((DivisionImpl) division).getId());
            ps.executeUpdate();
            //ps.close w finally
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
    /**
     * XXX - WTF!!!!  - najpierw commit - potem begin???????????
     */
    public void hideDivisionSpi(final DSDivision division) throws EdmException
    {
        PreparedStatement ps = null;
        boolean wasOpened = DSApi.isContextOpen(); 
        if(!wasOpened)
        	DSApi.context().begin();
        try
        {
            ps = DSApi.context().prepareStatement("update ds_division set parent_id = ? where id = ?");
            Long id = ((DivisionImpl) division).getId();
            ps.setLong(1, id);
            ps.setLong(2, id);
            ps.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	
        	if(!wasOpened)
        		DSApi.context().commit();
        }
    }

    public boolean isLdap()
    {
        return false;
    }

	public DSDivision getDivisionListByCodeSpi(String code) throws EdmException {
		return findDivisionByCodeSpi(code);
	}
}
