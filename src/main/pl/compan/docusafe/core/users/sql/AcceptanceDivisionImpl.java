package pl.compan.docusafe.core.users.sql;

import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;


public class AcceptanceDivisionImpl 
{

	public static final Logger log = LoggerFactory.getLogger("acceptance_division");
	
	//public static final String ACCEPTANCE_ROOTDIVISION_DIVISION_TYPE = "ac.rootdivision";
	/**Grupa*/
	public static final String ACCEPTANCE_DIVISION_DIVISION_TYPE = "ac.division";
	/**Akceptacja*/
	public static final String ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE = "ac.acceptance";
	public static final String ACCEPTANCE_EXTERNAL_USER_DIVISION_TYPE = "ext.user";
	/**U�ytkownik*/
	public static final String ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE = "ds.user";
	/**Dzia�*/
	public static final String ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE = "ds.division";
	public static final String ACCEPTANCE_CENTRUM_KOSZTOW_TYPE = "ac.centrumKosztow";
	public static final String ACCEPTANCE_FIELD_VALUE_TYPE = "ac.fieldValue";
	
	private Long id;
	private String divisionType;
    private String guid;
    private String name;
    private AcceptanceDivisionImpl parent;
    private String code;
	private Set<AcceptanceDivisionImpl> children;
    private Integer ogolna;
    
	public AcceptanceDivisionImpl() {}
	
	public static AcceptanceDivisionImpl create(String guid, String name, String divisionType, String code, Long parentId, Integer ogolna) throws EdmException
	{
		AcceptanceDivisionImpl toCreate = new AcceptanceDivisionImpl();
		toCreate.setDivisionType(divisionType);
		toCreate.setGuid(guid);
		toCreate.setName(name);
		toCreate.setParent(findById(parentId));
		toCreate.setCode(code);
		toCreate.setOgolna(ogolna);
		pl.compan.docusafe.core.Persister.create(toCreate);
		return toCreate;
	}
	
	public static void remove(Long id) throws EdmException
	{
		AcceptanceDivisionImpl toRemove = findById(id);
		LoggerFactory.getLogger("tomekl").debug("toRemove {}",toRemove != null ? toRemove.getId() : "null");
		pl.compan.docusafe.core.Persister.delete(toRemove);
	}
	
	public static AcceptanceDivisionImpl findById(Long id)
	{
		try
		{
			return (AcceptanceDivisionImpl) DSApi.context().session().load(AcceptanceDivisionImpl.class, id);
		}
		catch (Exception e) 
		{
			return null;
		}
	}
	
	public static AcceptanceDivisionImpl findByCode(String code)
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(AcceptanceDivisionImpl.class);
			c.add(Restrictions.eq("code", code));
			return (AcceptanceDivisionImpl) c.list().get(0);
		}
		catch (Exception e) 
		{
			return null;
		}		
	}
	
	public String getPreetyDivisionType()
	{
		if (divisionType.equals(ACCEPTANCE_ACCEPTANCE_DIVISION_TYPE))
			return "Akceptacja";
		else if (divisionType.equals(ACCEPTANCE_DIVISION_DIVISION_TYPE))
			return "Grupa";
		else if (divisionType.equals(ACCEPTANCE_DOCUSAFE_DIVISION_DIVISION_TYPE))
			return "Dzia�";
		else if (divisionType.equals(ACCEPTANCE_DOCUSAFE_USER_DIVISION_TYPE))
			return "U�ytkownik";
		else if (divisionType.equals(ACCEPTANCE_CENTRUM_KOSZTOW_TYPE))
			return "Centrum kosztow";
		else 
			return "U�ytkownik zewnetrzny";
	}
	public String getDivisionType() {
		return divisionType;
	}
    
    public void setDivisionType(String divisionType) {
		this.divisionType = divisionType;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AcceptanceDivisionImpl getParent() {
		return parent;
	}

	public void setParent(AcceptanceDivisionImpl parent) {
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}

	public Set<AcceptanceDivisionImpl> getChildren() {
		return children;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getOgolna() {
		return ogolna;
	}

	public void setOgolna(Integer ogolna) {
		this.ogolna = ogolna;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcceptanceDivisionImpl other = (AcceptanceDivisionImpl) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	public AcceptanceDivisionImpl getAcceptance(String assignee) throws EdmException
	{
		AcceptanceDivisionImpl acc = null;
		for (AcceptanceDivisionImpl accept : getChildren())
			if (accept.getCode().equals(assignee))
				acc = accept;
			
		if (acc!= null)
			return acc;
		else throw new EdmException("Brak zdefiniowanych akcpetacji dla " + assignee);
	}

	public AcceptanceDivisionImpl getAcceptancesByGUID(String authorGuid) throws EdmException
	{
		AcceptanceDivisionImpl acc = null;
		for (AcceptanceDivisionImpl accept : getChildren())
			if (accept.getGuid().equals(authorGuid))
				acc = accept;
			
		if (acc!= null)
			return acc;
		else throw new EdmException("Brak zdefiniowanych akcpetacji dla dzialu: " + DSDivision.find(authorGuid).getName());
		
	}

}
