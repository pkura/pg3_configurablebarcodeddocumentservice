package pl.compan.docusafe.core.users.sql;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;

import pl.compan.docusafe.core.EdmException;


/* User: Administrator, Date: 2005-05-18 12:19:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserCertificate.java,v 1.9 2008/10/06 11:28:21 pecet4 Exp $
 */
public class UserCertificate
{
    private String certdata;
    private String signature;

    UserCertificate()
    {
    }

    public X509Certificate certificate() throws EdmException
    {
        return pl.compan.docusafe.core.certificates.UserCertificate.getX509Certificate(Base64.decodeBase64(certdata.getBytes()));
    }

    public UserCertificate(X509Certificate cert) throws EdmException
    {
        try
        {
            this.certdata = new String(Base64.encodeBase64(cert.getEncoded()));
            this.signature = new String(Base64.encodeBase64(cert.getSignature()));
        }
        catch (CertificateEncodingException e)
        {
            throw new EdmException(e.getMessage(), e);
        }
    }

    public String getCertdata()
    {
        return certdata;
    }

    public void setCertdata(String certdata)
    {
        this.certdata = certdata;
    }

    public String getSignature()
    {
        return signature;
    }

    public void setSignature(String signature)
    {
        this.signature = signature;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof UserCertificate)) return false;

        final UserCertificate userCertificate = (UserCertificate) o;

        if (certdata != null ? !certdata.equals(userCertificate.certdata) : userCertificate.certdata != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (certdata != null ? certdata.hashCode() : 0);
    }
}
