package pl.compan.docusafe.core.users.sql;

import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.prefs.Preferences;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PasswordPolicy;
import pl.compan.docusafe.core.address.UserLocation;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Image;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.form.UpdateUser;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserImpl.java,v 1.54 2010/07/05 11:50:54 mariuszk Exp $
 */
public class UserImpl extends DSUser
{
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(UserImpl.class);

    StringManager sm = GlobalPreferences.loadPropertiesFile(UserImpl.class.getPackage().getName(), null);
    private Timestamp ctime;
    private Long id;
    private String name;
    private String firstname;
    private String lastname;
    private String email;
    private String identifier;
    private boolean loginDisabled;
    private String userRoles;
    private Date lastSuccessfulLogin;
    private Date lastUnsuccessfulLogin;
    private Date substitutedFrom;
    private Date substitutedThrough;
    private boolean deleted;
    private boolean certificateLogin;
    private Date lastPasswordChange;
    private String hashedPassword;
    private String externalName;
    private String roomNum;
    private boolean changePasswordAtFirstLogon;
    private short authlevel;
    private String pracow_nrewid_erp;

    private String internalNumber;
    
//  zmieniane
    private String mobileNum;
    private Float limitMobile;
    private String phoneNum;
    private Float limitPhone;
    private String extension;
    private String communicatorNum;
    //do tego miejsca
    private String remarks;
    private String kpx;
    private String pesel;
    private Date validityDate;
    /**
     * Prze�o�ony u�ytkownika.
     */
    private UserImpl supervisor;
    /**
     * U�ytkownik zast�puj�cy tego u�ytkownika.
     */
    private UserImpl substitute;
    private Set<DSDivision> divisions;
    private Set<DSDivision> backupDivisions;
    private Set<DSUser> substitutedUsers;
    private Set<String> archiveRoles;
    private Set<Profile> profiles;
    private Map<Long,String> profilesSource;
   

	private Map<String,String> archiveRolesSource;
    
    /**
     * Adresy usera, kt�re b�d� widoczne w zak�adce kontakty/dane dodatkowe
     */
    private Set<UserAddress> userAddress;
    /**
     * Dodatkowe dane usera widoczne w zak�adce kontakty/dane dodatkowe
     */
    private Set<UserData> userData;
    /**
     * Lista obiekt�w UserCertificate.
     */
    private List<UserCertificate> certificates;
    private boolean system;
    
    /**
     * Prze�o�eni
     */
    private Set<UserImpl> supervisors;
    
    /**
     * Zdj�cie profilu
     */
    private Image image;
    
    /**
     * Opis stanowiska
     */
    private String postDescription;
    
    /**
     * Lokalizacja u�ytkownika
     */
    private UserLocation userLocation;
    
    /**
     * Opis o mnie
     */
    private String aboutMe;
    
    /**
     * Ulubione strony u�ytkownika
     */
    private String sites;
    /***
     * Zmienna pomocnicza nie zapisywana w bazie 
     * oznaczajaca czy uzytkonik nie jest na urlopie
     */
    private Boolean atWork;

    /**
     * Okre�la czy u�ytkownik pe�ni funkcj� kierownika
     */
    private boolean isSupervisor;
    
    public UserImpl()
    {
    }

    public UserImpl(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public String getEncodedPassword()
    {
        return hashedPassword;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public boolean isLoginDisabled()
    {
        return loginDisabled;
    }

    public void setLoginDisabled(boolean loginDisabled)
    {
        this.loginDisabled = loginDisabled;
    }
    
    

    public String[] getOfficeRoles() throws EdmException {
    	Set<String> result = new HashSet<String>();
    	List<Long> roleIds =  Role.listUserRoles(name);
    	for (Long roleId : roleIds) {
    		Role role = Role.find(roleId);
    		result.add(role.getName());
    	}
    	return result.toArray(new String[result.size()]);
    }
    
    public Set<String> getRoles()
    {
        return archiveRoles;
    }

    public Map<String, String[]> getRolesSources() {
    	Map<String, String[]> result = new HashMap<String, String[]>();
    	for (Entry<Object, ArrayList<String>> entry : UpdateUser.getSources(archiveRolesSource).entrySet()) {
    		result.put((String)entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()]));
    	}
    	return result;
    }

    public void addRole(String role, String source) throws HibernateException, EdmException
    {
    	if (archiveRoles == null)
            archiveRoles = new HashSet<String>();
        else
            DSApi.initializeProxy(archiveRoles);
    	if (archiveRolesSource == null) {
    		archiveRolesSource = new HashMap<String, String>();
    	}
    	UpdateUser.addObjectWithSource(role, source, role, archiveRoles, archiveRolesSource, this);
    }
    
    public void addRole(String role) throws HibernateException, EdmException {
    	addRole(role, "own");
    }
    
    public void removeRole(String role, String source) throws EdmException {
    	if (archiveRoles == null || !archiveRoles.contains(role)) {
        	return;
        } else {
        	DSApi.initializeProxy(archiveRoles);
        }
        if (UpdateUser.removeSource(archiveRolesSource, role, source))
        	archiveRoles.remove(role);
    }
    
    public void removeRole(String role) throws EdmException {
		removeRole(role, "own");
	}
    
    public void archiveRolesOfCollection(Collection<String> newRoles) throws EdmException {
		if (archiveRoles == null) {
			archiveRoles = new HashSet<String>();
		}
		Set<String> rolesToAdd = new HashSet<String>(newRoles);
		rolesToAdd.removeAll(archiveRoles);
		Set<String> rolesToRemove = new HashSet<String>(archiveRoles);
		rolesToRemove.removeAll(newRoles);
		for (String role: rolesToAdd) {
			addRole(role);
		}
		for (String role: rolesToRemove) {
			removeRole(role);
		}
	}
    
    public String[] getProfileNames() {
    	Set<String> names = new HashSet<String>();
    	for(Profile profile : profiles ) {
    		names.add(profile.getName());
    	}
    	return names.toArray(new String[profiles.size()]);
    }
    
    public Map<String,String> getProfilesWithKeys(){
    	Map<String,String> profilesKeys = new HashMap<String,String>();
    	for(Profile profile : profiles ) {
    		profilesKeys.put(profile.getName(),profilesSource.get(profile.getId()));
    	}
    	return profilesKeys;
    }
    
    public boolean hasKeysForProfiles(){
    	for(Map.Entry<Long, String> entry : this.profilesSource.entrySet()){
    		if(!entry.getValue().isEmpty()){
    			return true;
    		}
    	}
    	return false;
    }
    
    private String profileNameForId(Long id){
    	for(Profile profile: this.profiles){
    		if(profile.getId()==id){
    			return profile.getName();
    		}
    	}
    	return null;
    }
 
    public Map<String,String> getArchiveRolesSource() {
    	return this.archiveRolesSource;
	}
    

	public Map<Long,String> getProfilesSource() {
		return this.profilesSource;
	}
    
    public Date getLastSuccessfulLogin()
    {
        return lastSuccessfulLogin;
    }

    public Date getLastUnsuccessfulLogin()
    {
        return lastUnsuccessfulLogin;
    }

    public DSUser getSupervisor() throws UserNotFoundException, EdmException
    {
        return supervisor;
    }

    public void setSupervisor(DSUser supervisor)
    {
        this.supervisor = (UserImpl) supervisor;
    }

    public DSUser getSubstitute() throws UserNotFoundException, EdmException
    {
        return substitute;
    }

    protected void setSubstitute(DSUser substitute)
    {
        if(this.substitute != substitute && substitute != null
               /* && substitute.getName().equals(this.substitute.getName())*/){
            DSUserCache.onSubstitutionChange();
        }
        this.substitute = (UserImpl) substitute;
    }

    public Date getSubstitutedFrom()
    {
        return substitutedFrom;
    }

    protected void setSubstitutedFrom(Date substitutedFrom)
    {
        this.substitutedFrom = substitutedFrom;
    }

    public Date getSubstitutedThrough()
    {
        return substitutedThrough;
    }

    protected void setSubstitutedThrough(Date substitutedThrough)
    {
        this.substitutedThrough = substitutedThrough;
    }

    public DSUser[] getSubstitutedUsers() throws EdmException
    {
    	DSApi.initializeProxy(substitutedUsers);
    	return (DSUser[]) substitutedUsers.toArray(new DSUser[substitutedUsers.size()]);
    }
    
    public DSUser[] getSubstitutedUsersForNow(Date d) throws EdmException{
    	Criteria criteria = DSApi.context().session().createCriteria(DSUser.class);
		criteria.add(Restrictions.eq("substitute.id", getId()))
				.add(Restrictions.lt("substitutedFrom", d))
				.add(Restrictions.gt("substitutedThrough", d));
		List<UserImpl> subsUsers = (List<UserImpl>) criteria.list();
		
		//DSApi.context().session().close();
        return subsUsers.toArray(new DSUser[subsUsers.size()]);
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public Date getLastPasswordChange()
    {
        return lastPasswordChange;
    }

    public boolean verifyPassword(String password)
    {
        return hashedPassword != null && hashedPassword.equals(UserFactory.hashPassword(password));
    }

    public ArrayList<DSDivision> getDivisionsAsList() throws EdmException
    {
    	if (divisions != null)
        {
        	
            DSApi.initializeProxy(divisions);
            ArrayList<DSDivision> set = new ArrayList<DSDivision>();
            set.addAll(divisions);
            for (Iterator iter=divisions.iterator(); iter.hasNext(); )
            {
            	
                DSDivision d = (DSDivision) iter.next();
                if (d.isPosition() && (d.getParent() != null))
                	set.add(d.getParent());
            }
            return set;
        }
        return new ArrayList<DSDivision>();
    }

    public ArrayList<DSDivision> getDivisionsAsListSafe() {
        ArrayList<DSDivision> ret = new ArrayList<DSDivision>();

        try {
            return getDivisionsAsList();
        } catch(Exception ex) {
            log.error("[getDivisionsAsListSafe] error", ex);
            return ret;
        }
    }
    
    public DSDivision[] getDivisions() throws EdmException
    {
        if (divisions != null)
        {
        	
            DSApi.initializeProxy(divisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            set.addAll(divisions);
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
            	
                DSDivision d = iter.next();
                if (d.isPosition() && (d.getParent() != null))
                	set.add(d.getParent());
            }
            return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        }
        return new DSDivision[0];
    }
    
    public String[] getDivisionsGuid() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<String> set = new HashSet<String>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
            	DSDivision d = iter.next();
            	set.add(d.getGuid());                
                if (d.isPosition() && (d.getParent() != null))
                	set.add(d.getParent().getGuid());
            }
            return (String[]) set.toArray(new String[set.size()]);
        }
        return new String[0];
    }
    
    public String[] getDivisionsGuidWithoutGroup() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<String> set = new HashSet<String>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
            	
            	DSDivision d = iter.next();
            	if(d.isGroup())
            		continue;
            	set.add(d.getGuid());                
                if (d.isPosition() && (d.getParent() != null))
                	set.add(d.getParent().getGuid());
            }
            return (String[]) set.toArray(new String[set.size()]);
        }
        return new String[0];
    }
    
    @Override
    public DSDivision[] getDivisionsWithoutGroupPosition() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
                DSDivision d = iter.next();
                if(!d.isGroup() && !d.isPosition())
                {
                	set.add(d);
                }
                if(!d.isGroup())
                {
	                if (d.isPosition() && (d.getParent() != null))
	                	set.add(d.getParent());
                }
            }
            return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        }
        return new DSDivision[0];
    }
    
    public DSDivision[] getDivisionsWithoutGroup() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            LinkedHashSet<DSDivision> set = new LinkedHashSet<DSDivision>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
            	
                DSDivision d = iter.next();
                if(!d.isGroup())
                {
                	set.add(d);
	                if (d.isPosition() && (d.getParent() != null))
	                	set.add(d.getParent());
                }
            }
            return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        }
        return new DSDivision[0];
    }
    /**
     * Zwraca tablice dzia�� w kt�rych znajduje si� uzytkownik plus dzialy w kt�rych jest jako osoba backapowa
     * @return
     * @throws EdmException
     */
    public DSDivision[] getAllDivisions() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            set.addAll(divisions);
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
                DSDivision d = iter.next();
                if (d.isPosition() && (d.getParent() != null)) 
                	set.add(d.getParent());
            }
            return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        }
        return new DSDivision[0];
    }
    
    public DSDivision[] getBackupDivisions() throws EdmException
    {
        if (backupDivisions != null)
        {
            DSApi.initializeProxy(backupDivisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            set.addAll(backupDivisions);
            for (Iterator<DSDivision> iter=backupDivisions.iterator(); iter.hasNext(); )
            {
                DSDivision d = iter.next();
                if (d.isPosition() && (d.getParent() != null))
                	set.add(d.getParent());
            }
            return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        }
        return new DSDivision[0];
    }
    
    public DSDivision[] getSubordinateDivisionsWithoutGroup() throws EdmException
    {
    	if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
                DSDivision d = iter.next();
                if(d.isGroup())
                {
                	if(DocumentKind.isAvailable(DocumentLogicLoader.DL_BINDER))
                	{
                		if(d.getGuid().startsWith("DL_BINDER"))
                		{
                			set.add(d);
                		}
                		else { continue; }
                	}
                	else
                	{
                		continue;
                	}
                }
                else
                {
                	set.add(d);
                	if (d.isPosition() && (d.getParent() != null )&& !d.getParent().isGroup())
                	{
                		set.add(d.getParent());
                	}
                }
            }
            Set<DSDivision> returnSet = new HashSet<DSDivision>();
        	for (DSDivision division : set) 
        	{
        		addChildren(division, returnSet);
			}
        	return (DSDivision[]) returnSet.toArray(new DSDivision[set.size()]);
        		
        }
        return new DSDivision[0];
    }

    public DSDivision[] getOriginalDivisions() throws EdmException
    {
        if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            return (DSDivision[]) divisions.toArray(new DSDivision[divisions.size()]);
        }
        return new DSDivision[0];
    }
    
    public DSDivision[] getOriginalDivisionsWithoutGroup() throws EdmException
    {
    	if (divisions != null)
        {
            DSApi.initializeProxy(divisions);
            Set<DSDivision> set = new HashSet<DSDivision>();
            for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
            {
                DSDivision d = iter.next();
                if(d.isGroup())
                {
                	continue;
                }
                else
                {
                	set.add(d);
                }
            }
        	return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
        		
        }
        return new DSDivision[0];
    }
    
    public DSDivision[] getSubordinateDivisions() throws EdmException
    {
    	DSDivision[] temp = getDivisions();
    	Set<DSDivision> set = new HashSet<DSDivision>();
    	for(int i=0; i<temp.length;i++){
    		addChildren(temp[i], set);
    	}
    	return (DSDivision[]) set.toArray(new DSDivision[set.size()]);
    }
    
    private void addChildren(DSDivision root, Set<DSDivision> list) throws EdmException{
    	list.add(root);
    	DSDivision[] childs = root.getChildren();
    	if(childs.length < 1) return;
    	for(int i=0; i<childs.length;i++){
    		list.add(childs[i]);
    		addChildren(childs[i], list);
    	}
    }
    public boolean inDivision(DSDivision division) throws EdmException
    {
        DSApi.initializeProxy(divisions);
        boolean in = divisions != null && divisions.contains(division);
        if (in)
        	return true;
        if (divisions != null) {
	        for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
	        {
	        	DSDivision d = iter.next();
	        	if (d.isPosition() && (d.getParent() != null))
	        	{
	        		if (d.getParent().equals(division))
	        			return true;
	        	}
	        }
        }
        return false;
    }
    /**Sprawdza czy u�ytkownik znajduje si� w okre�lonym dziale, je�li stanowisko sprawdza dzia� nadrz�dny
     * Je�li guid = #system# zwraca true*/
    public boolean inDivisionByGuid(String guid) throws EdmException
    {
    	if("#system#".equals(guid))
    		return true;
        DSApi.initializeProxy(divisions);
        for (Iterator<DSDivision> iter=divisions.iterator(); iter.hasNext(); )
        {
        	DSDivision d = iter.next();
        	if(d.getGuid().equals(guid)) return true;
        	if (d.isPosition() && (d.getParent() != null))
        	{
        		if (d.getParent().getGuid().equals(guid))
        			return true;
        	}
        }
        return false;
    }

    public boolean inOriginalDivision(DSDivision division) throws EdmException
    {
        DSApi.initializeProxy(divisions);
        return divisions != null && divisions.contains(division);
    }
    
    public void update() throws EdmException
    {
    }
    
    public void setPassword(String password, boolean initial) throws EdmException
    {
        if (!initial)
        {
            if (SqlUserFactory.passwordInHistory(name, password))
            {
                throw new EdmException(sm.getString("ToHasloByloJuzUzyteWPrzeszlosci"));
            }
            this.lastPasswordChange = new Date();
            this.changePasswordAtFirstLogon = false;
        }
        else
        {
            this.lastPasswordChange = new Date(0);
            Preferences prefs = DSApi.context().systemPreferences().
	                node(PasswordPolicy.NODE_PASSWORD_POLICY);
            boolean mustChangePassword = prefs.getBoolean(PasswordPolicy.KEY_MUST_CHANGE_PASSWORD_AT_FIRST_LOGON, false);
            if(mustChangePassword){
            	this.changePasswordAtFirstLogon = true;
            }
            else{
            	this.changePasswordAtFirstLogon = false;
            }
        }
//        this.changePasswordAtFirstLogon = false;
        SqlUserFactory.updatePasswordHistory(name, password);
        this.hashedPassword = UserFactory.hashPassword(password);
    }

    public boolean addCertificate(X509Certificate cert) throws EdmException
    {
        if (cert == null)
            throw new NullPointerException("cert");
        UserCertificate userCert = new UserCertificate(cert);
        if (certificates == null)
        {
            certificates = new LinkedList<UserCertificate>();
        }
        else
        {
            if (certificates.contains(userCert))
                return false;
        }
        return certificates.add(userCert);
    }

    public boolean removeCertificate(X509Certificate cert) throws EdmException
    {
        if (certificates != null)
            return certificates.remove(new UserCertificate(cert));
        return false;
    }

    public X509Certificate[] getCertificates() throws EdmException
    {
        if (certificates == null || certificates.size() == 0)
            return new X509Certificate[0];
        X509Certificate[] certs = new X509Certificate[certificates.size()];
        for (int i=0; i < certificates.size(); i++)
        {
            certs[i] = ((UserCertificate) certificates.get(i)).certificate();
        }
        return certs;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof UserImpl)) return false;

        final UserImpl user = (UserImpl) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;

        return true;
    }
    
    public int hashCode()
    {
        return (id != null ? id.hashCode() : 0);
    }

    public Long getId()
    {
        return id;
    }

    public String getUserRoles()
    {
        return userRoles;
    }

    public String getHashedPassword()
    {
        return hashedPassword;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setUserRoles(String userRoles)
    {
        this.userRoles = userRoles;
    }

    public void setLastSuccessfulLogin(Date lastSuccessfulLogin)
    {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public void setLastUnsuccessfulLogin(Date lastUnsuccessfulLogin)
    {
        this.lastUnsuccessfulLogin = lastUnsuccessfulLogin;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public void setLastPasswordChange(Date lastPasswordChange)
    {
        this.lastPasswordChange = lastPasswordChange;
    }

    public void setHashedPassword(String hashedPassword)
    {
        this.hashedPassword = hashedPassword;
    }

    public void setSupervisor(UserImpl supervisor)
    {
        this.supervisor = supervisor;
    }

    public void setSubstitute(UserImpl substitute)
    {
        this.substitute = substitute;
    }

    public void setDivisions(Set divisions)
    {
        this.divisions = divisions;
    }

    public void setSubstitutedUsers(Set substitutedUsers)
    {
        this.substitutedUsers = substitutedUsers;
    }

    public void setRoles(Set roles)
    {
        this.archiveRoles = roles;
    }

    public boolean isCertificateLogin()
    {
        return certificateLogin;
    }

    public void setCertificateLogin(boolean certificateLogin)
    {
        this.certificateLogin = certificateLogin;
    }

    public boolean isSystem()
    {
        return system;
    }

    public void setSystem(boolean system)
    {
        this.system = system;
    }

	public String getCommunicatorNum() {
		return communicatorNum;
	}

	public void setCommunicatorNum(String communicatorNum) {
		this.communicatorNum = communicatorNum;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public void setBackupDivisions(Set<DSDivision> backupDivisions) {
		this.backupDivisions = backupDivisions;
	}    
	
	public Timestamp getCtime(){
		return ctime;
	}

	public void setCtime(Timestamp ctime) {
		this.ctime = ctime;
	}

	public String getExternalName() {
		return externalName;
	}

	public void setExternalName(String externalName) {
		this.externalName = externalName;
	}

	@Override
	public String getKpx() {
		return kpx;
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public Date getValidityDate() {
		return validityDate;
	}

	@Override
	public void setKpx(String kpx) {
		this.kpx = kpx;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getPesel() {
		return pesel;
	}
	@Override
	public String getRoomNum() {
		return this.roomNum;
	}

	@Override
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
		
	}

	public boolean isChangePasswordAtFirstLogon() {
		return changePasswordAtFirstLogon;
	}

	public void setChangePasswordAtFirstLogon(boolean changePasswordAtFirstLogon) {
		this.changePasswordAtFirstLogon = changePasswordAtFirstLogon;
	}
	
	public short getAuthlevel() {
		return authlevel;
	}

	public void setAuthlevel(short authlevel) {
		this.authlevel = authlevel;
	}

	/**
	 * Metoda zwracaj�ca sk�adow� image, pr�buje wczyta� j� z bazy danych je�li jest null
	 * 
	 * @return
	 */
	public Image getImage() throws EdmException
	{
		if (image == null && id != null)
		{
			String sql = "select usr.image from " + UserImpl.class.getName() + " usr where usr.id = ?";
			image = (Image) DSApi.context().session().createQuery(sql)
				.setLong(0, id)
				.uniqueResult();
		}
		return image;
	}

	/**
	 * Metoda ustawiaj�ca sk�adow� image
	 * 
	 * @param image
	 */
	public void setImage(Image image) 
	{
		this.image = image;
	}
	
	/**
	 * Metoda zwracaj�ca 
	 * 
	 * @return
	 */
	public String getPostDescription() 
	{
		return postDescription;
	}

	public void setPostDescription(String postDescription) 
	{
		this.postDescription = postDescription;
	}

	public UserLocation getUserLocation() 
	{
		return userLocation;
	}

	public void setUserLocation(UserLocation userLocation) 
	{
		this.userLocation = userLocation;
	}

	public String getAboutMe() 
	{
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) 
	{
		this.aboutMe = aboutMe;
	}

	public String getSites() 
	{
		return sites;
	}

	public void setSites(String sites) 
	{
		this.sites = sites;
	}

	public Set<UserImpl> getSupervisors() 
	{
		return supervisors;
	}

	public void setSupervisors(Set<UserImpl> supervisors) 
	{
		this.supervisors = supervisors;
	}
	
	
	
	public Set<UserAddress> getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(Set<UserAddress> userAddress) {
		this.userAddress = userAddress;
	}

	public Set<UserData> getUserData() {
		return userData;
	}

	public void setUserData(Set<UserData> userData) {
		this.userData = userData;
	}

	public List<UserImpl> getSupervisorsHierarchy() throws EdmException
	{
		if (supervisors == null || supervisors.size() == 0)
			return null;
		
		List<UserImpl> users = new ArrayList<UserImpl>();
		for (UserImpl supervisor : supervisors)
		{
			users.add(supervisor);
		}
		
		// ustalenie hierarchii przelozonych wg dzialow
		Collections.sort(users, new UserImpl.UserComparatorByPath());
		
		return users;
	}
	
    /**
     *  Sprawdza czy zalogowany u�ytkownik znajduje si� w grupie
     *  @param inGroup grupa
     */
    public static boolean inGroup(String inGroup) throws EdmException
    {
        DSDivision[] userGroup = DSApi.context().getDSUser().getDSGroups();
        
        for(DSDivision group : userGroup)
        {
            if(inGroup.equals(group.getGuid()))
                return true;
        }
        
        return false;
    }
    
    public static class UserComparatorByPath implements Comparator<UserImpl>
    {   	
        public int compare(UserImpl user1, UserImpl user2)
        {
        	try
			{
				DSDivision div1 = user1.getDivisionsWithoutGroup()[0];
				DSDivision div2 = user2.getDivisionsWithoutGroup()[0];

				int pathSize1 = div1.getPrettyPath("/").split("/").length;
				int pathSize2 = div2.getPrettyPath("/").split("/").length;

				if (pathSize1 > pathSize2)
					return -1;
				else if (pathSize1 < pathSize2)
					return 1;
				else
					return 0;
			}
			catch (EdmException ex)
			{
				ex.printStackTrace();
			}
			return 0;      	
        }
    }

	@Override
	public Float getLimitMobile() {
		return this.limitMobile;
	}

	@Override
	public Float getLimitPhone() {
		return this.limitPhone;
	}

	@Override
	public void setLimitMobile(Float limitMobile) {
		this.limitMobile = limitMobile;
	}

	@Override
	public void setLimitPhone(Float limitPhone) {
		this.limitPhone = limitPhone;
	}

	public Boolean getAtWork() {
		return atWork;
	}

	public void setAtWork(Boolean atWork) {
		this.atWork = atWork;
	}	
	
	public String getInternalNumber() {
		return internalNumber;
	}

	public void setInternalNumber(String internalNumber) {
		this.internalNumber = internalNumber;
	}

	/**
	 * Metoda zwraca obiekt UserImpl na podstawie zadanego identyfikatora
	 * @param id - identyfikator w bazie danych
	 * @return
	 * @throws EdmException
	 * @throws UserNotFoundException - wyrzucany kiedy nie znaleziono uzytkownika o podanym id
	 */
	  public static UserImpl find(Long id) throws EdmException,UserNotFoundException
	    {
		  UserImpl user = (UserImpl)DSApi.getObject(UserImpl.class, id);
		 
	        if (user  == null)
	            throw new EntityNotFoundException(UserImpl.class, id);

	        return user;
	    }

	public boolean getIsSupervisor() {
		return isSupervisor;
	}

	public void setIsSupervisor(boolean isSupervisor) {
		this.isSupervisor = isSupervisor;
	}

	@Override
	public boolean isSupervisor() {
		return this.isSupervisor;
	}

	public String getPracow_nrewid_erp() {
		return pracow_nrewid_erp;
	}

	public void setPracow_nrewid_erp(String pracow_nrewid_erp) {
		this.pracow_nrewid_erp = pracow_nrewid_erp;
	}

}
