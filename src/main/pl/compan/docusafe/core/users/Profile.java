package pl.compan.docusafe.core.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;

public class Profile {

	public final static Logger log = LoggerFactory.getLogger(Profile.class);

	private Long id;
	private String name;
	private Set<String> archiveRoles;
	private Set<DSDivision> groups;
	private Set<Role> officeRoles;
	private Set<DSUser> users;

	/**
	 * domyslny klucz dla usera, jezeli jest # to klucza nie ma
	 */
	public final static String emptyKey = "#";

	private Map<Long, String> profilesKeys;

	Profile(String name) {
		this.name = name;
	}

	Profile() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getArchiveRoles() {
		if (archiveRoles == null) {
			return Collections.EMPTY_SET;
		}
		return archiveRoles;
	}

	/**
	 * Przypisuje role do profilu, jednoczesnie przypisujac ja do uzytkownika
	 * 
	 * @param role
	 * @throws HibernateException
	 * @throws EdmException
	 */
	private void addArchiveRole(String role) throws HibernateException,
			EdmException {
		if (archiveRoles == null) {
			archiveRoles = new HashSet<String>();
		}
		archiveRoles.add(role);
		for (DSUser user : users) {
			user.addRole(role, this.name);
			clearCache(user);
		}
	}

	private void removeArchiveRole(String role) throws HibernateException,
			EdmException {
		for (DSUser user : users) {
			user.removeRole(role, this.name);
			clearCache(user);
		}
		archiveRoles.remove(role);
	}

	private void addOfficeRole(Role role) throws EdmException {
		if (officeRoles == null) {
			officeRoles = new HashSet<Role>();
		}
		officeRoles.add(role);
		for (DSUser user : users) {
			role.addUser(user.getName(), this.name);
			clearCache(user);
		}
	}

	public void removeOfficeRole(Role role) throws EdmException {
		for (DSUser user : users) {
			if (role.getUsernames().contains(user.getName())) {
				role.removeUser(user.getName(), this.name);
				clearCache(user);
			}
		}
		officeRoles.remove(role);
	}

	private void addGroup(DSDivision group) throws EdmException {
		for (DSUser user : users) {
			group.addUserSpi(user, true, this.name);
			clearCache(user);
		}
		groups.add(group);
	}

	public void removeGroup(DSDivision group) throws EdmException {
		for (DSUser user : users) {
			if (Arrays.asList(group.getUsers()).contains(user)) {
				group.removeUserSpi(user, this.name);
				clearCache(user);
			}
		}
		groups.remove(group);
	}

	public Set<DSDivision> getGroups() {
		if (groups == null) {
			return Collections.EMPTY_SET;
		}
		return groups;
	}

	public Set<Role> getOfficeRoles() {
		if (officeRoles == null) {
			return Collections.EMPTY_SET;
		}
		return officeRoles;
	}

	public Set<DSUser> getUsers() {
		if (users == null) {
			return Collections.EMPTY_SET;
		}
		return users;
	}

	public void update(String[] newArchiveRoles, Long[] newOfficeRoles,
			String[] newGroups) throws HibernateException, EdmException {

		List<String> newArchiveRolesList = Arrays.asList(newArchiveRoles);
		Set<String> archiveRolesToAdd = new HashSet<String>(newArchiveRolesList);
		archiveRolesToAdd.removeAll(archiveRoles);
		Set<String> archiveRolesToRemove = new HashSet<String>(archiveRoles);
		archiveRolesToRemove.removeAll(newArchiveRolesList);

		for (String newArchiveRole : archiveRolesToAdd) {
			addArchiveRole(newArchiveRole);
		}

		for (String newArchiveRole : archiveRolesToRemove) {
			removeArchiveRole(newArchiveRole);
		}

		List<DSDivision> newGroupsList = new ArrayList<DSDivision>();
		for (String groupName : newGroups) {
			try {
				DSDivision group = DSDivision.find(groupName);
				if (!group.getDivisionType().equals(DSDivision.TYPE_GROUP)) {
					continue;
				}
				newGroupsList.add(group);
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			}
		}
		Set<DSDivision> groupsToAdd = new HashSet<DSDivision>(newGroupsList);
		groupsToAdd.removeAll(groups);
		Set<DSDivision> groupsToRemove = new HashSet<DSDivision>(groups);
		groupsToRemove.removeAll(newGroupsList);

		for (DSDivision newGroup : groupsToAdd) {
			addGroup(newGroup);
		}

		for (DSDivision oldGroup : groupsToRemove) {
			removeGroup(oldGroup);
		}

		List<Role> newOfficeRolesList = new ArrayList<Role>();
		for (Long id : newOfficeRoles) {
			try {
				Role role = Role.find(id);
				newOfficeRolesList.add(role);
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				LogFactory.getLog("eprint").debug("", e);
			}
		}
		Set<Role> officeRolesToAdd = new HashSet<Role>(newOfficeRolesList);
		officeRolesToAdd.removeAll(officeRoles);
		Set<Role> officeRolesToRemove = new HashSet<Role>(officeRoles);
		officeRolesToRemove.removeAll(newOfficeRolesList);

		for (Role newRole : officeRolesToAdd) {
			addOfficeRole(newRole);
		}

		for (Role oldRole : officeRolesToRemove) {
			removeOfficeRole(oldRole);
		}

	}

	public void usersOfCollection(Collection<DSUser> newUsers)
			throws EdmException {

		if (users == null) {
			users = new HashSet<DSUser>();
		}
		Set<DSUser> usersToAdd = new HashSet<DSUser>(newUsers);
		usersToAdd.removeAll(users);
		Set<DSUser> usersToRemove = new HashSet<DSUser>(users);
		usersToRemove.removeAll(newUsers);
		for (DSUser user : usersToAdd) {
			addUser(user);
		}
		for (DSUser user : usersToRemove) {
			removeUser(user);
		}
	}

	/**
	 * Dodawanie uzytkownika do profilu
	 * 
	 * @param user
	 * @throws EdmException
	 */
	public void addUser(DSUser user) throws EdmException {
		this.addUser(user, emptyKey);
	}

	/**
	 * Dodawanie uzytkownika do profilu
	 * 
	 * @param user
	 * @param key
	 * @throws EdmException
	 */
	public void addUser(DSUser user, String key) throws EdmException {
		if (user == null) {
			log.trace("user==null");
			return;
		}
		log.debug("addUser user={} id={}", user.getName(), user.getId());
		for (String newRole : archiveRoles) {
			log.trace("dodajemy role {}", newRole);
			user.addRole(newRole, this.name);
		}
		for (DSDivision newGroup : groups) {
			log.trace("dodajemy grupe {}", newGroup.getGuid());
			// dlaczego on dodawal ludzi do grupy jako backup powinno byc false
			newGroup.addUserSpi(user, false, this.name);
		}
		for (Role newRole : officeRoles) {
			log.trace("dodajmy office role {}", newRole);
			newRole.addUser(user.getName(), this.name);
		}

		clearCache(user);
		users.add(user);

		this.profilesKeys.put(user.getId(), notEmptyStringKey(key) ? key : emptyKey);
	}

	public Map<Long, String> getProfilesKeys() {
		return profilesKeys;
	}

	public void updateKey(DSUser user, String key) {
		this.profilesKeys.put(user.getId(), notEmptyStringKey(key) ? key : emptyKey);
	}

	private boolean notEmptyStringKey(String key){
		return key != null && !key.equals("");
	}
	/**
	 * Usuniecie roli uzytkownika z profilu
	 * 
	 * @param user
	 * @throws EdmException
	 */
	private void removeUserRoles(DSUser user) throws EdmException {
		if (users == null || !users.contains(user)) {
			throw new EntityNotFoundException("UserNotFound");
		}
		log.trace("removeUserRoles");
		log.trace("user={} id={}", user.getName(), user.getId());
		for (String oldRole : archiveRoles) {
			user.removeRole(oldRole, this.name);
		}
		for (DSDivision oldGroup : groups) {
			oldGroup.removeUserSpi(user, this.name);
		}
		for (Role oldRole : officeRoles) {
			oldRole.removeUser(user.getName(), this.name);
		}

		clearCache(user);
	}

	/**
	 * Usuniecie uzytkownika z profilu
	 * 
	 * @param user
	 * @throws EdmException
	 */
	public void removeUser(DSUser user) throws EdmException {
		removeUserRoles(user);
		users.remove(user);
		this.profilesKeys.remove(user.getId());
	}

	public DSUser[] getUsersArray() throws EdmException {
		DSApi.initializeProxy(users);
		if (users == null || users.size() == 0) {
			return new DSUser[0];
		}
		return users.toArray(new DSUser[users.size()]);
	}

	public String[] getGuids() {
		if (groups == null) {
			return new String[0];
		}
		String[] result = new String[groups.size()];
		int i = 0;
		for (DSDivision group : groups) {
			result[i] = group.getGuid();
			i++;
		}
		return result;
	}

	public static String[] getProfilesNames() throws EdmException {
		Query q = DSApi.context().session()
				.createQuery("select p.name from Profile p order by p.name");
		try {
			List result = q.list();
			
			if (result.size() > 0) {
				Collections.sort(result);			//zapytanie czasami nie zwraca w�a�ciwie posortowanej listy (wielko�� liter) co generuje b��d 
				return (String[]) result.toArray(new String[result.size()]);
			} else
				return null;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static Profile[] getProfiles() throws EdmException {
		Criteria cr = DSApi.context().session().createCriteria(Profile.class);
		cr.addOrder(Order.asc("name"));
		try {
			List result = cr.list();
			if (result.size() > 0) {
				return (Profile[]) result.toArray(new Profile[result.size()]);
			}
			return new Profile[0];
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public static Profile findByProfilename(String name) throws EdmException {
		Criteria cr = DSApi.context().session().createCriteria(Profile.class);
		cr.add(Expression.eq("name", name));
		try {
			List result = cr.list();
			if (result.size() > 0) {
				return (Profile) result.get(0);
			}
			throw new ProfileNotFoundException(name);

		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    public static Profile findById(Long id) throws EdmException {
        return Finder.find(Profile.class, id);
    }

	public void remove() throws EdmException {
		for (DSUser user : users) {
			removeUserRoles(user);
		}
		users.clear();

		if (groups != null)
			groups.clear();
		if (officeRoles != null)
			officeRoles.clear();
		// if (users != null)
		// users.clear();
		try {
			Persister.delete(this);
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			LogFactory.getLog("eprint").debug("", e);
		}
		return;
	}

	public static Profile create(String name) throws EdmException {
		try {
			findByProfilename(name);
			throw new EdmException("profile already exists");
		} catch (ProfileNotFoundException e) {
			// ok
		}

		Profile profile = new Profile(name);
		try {
			Persister.create(profile);
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			LogFactory.getLog("eprint").debug("", e);
			return null;
		}
		return profile;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	private void clearCache(DSUser user) throws EdmException {
		PermissionCache permCache = (PermissionCache) ServiceManager
				.getService(PermissionCache.NAME);
		permCache.invalidate(user);
	}

}
