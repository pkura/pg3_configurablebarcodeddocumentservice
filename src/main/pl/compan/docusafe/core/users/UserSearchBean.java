package pl.compan.docusafe.core.users;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.DynaActionForm;

import pl.compan.docusafe.util.DateUtils;

public class UserSearchBean {
	String name; 
	String firstname;
	String lastname;
	String pesel;
	String kpx;
	String superior;
	String profile;
	Date createdFrom;
	Date createdTo;
	Date validFrom;
	Date validTo;
	Boolean active = null;
	String location;
	String email;
	String cellPhone;
	String nomadicPhone;
	
	public UserSearchBean() {
		// TODO Auto-generated constructor stub
	}
	
	public UserSearchBean(DynaActionForm form) {
		pesel = (String)form.get("pesel");
		location = (String) form.get("location");
		kpx = (String)form.get("kpx");
		superior = (String)form.get("superior");
		location = (String)form.get("location");
		email = (String)form.get("email");
		profile = (String)form.get("profile");
		cellPhone = (String)form.get("cellPhone");
		nomadicPhone = (String)form.get("nomadicPhone");
		Boolean tmp = (Boolean)form.get("inactive");
		active =  tmp == null ? null : !tmp;
		createdFrom = DateUtils.nullSafeParseJsDate((String) form.get("createdFrom"));
		createdTo = DateUtils.nullSafeParseJsDate((String) form.get("createdTo"));
		validFrom = DateUtils.nullSafeParseJsDate((String) form.get("validFrom"));
		validTo = DateUtils.nullSafeParseJsDate((String) form.get("validTo"));
	}
	
	private Boolean disjunction = true;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getKpx() {
		return kpx;
	}
	public void setKpx(String kpx) {
		this.kpx = kpx;
	}
	public String getSuperior() {
		return superior;
	}
	public void setSuperior(String superior) {
		this.superior = superior;
	}
	public Date getCreatedFrom() {
		return createdFrom;
	}
	public void setCreatedFrom(Date createdFrom) {
		this.createdFrom = createdFrom;
	}
	public Date getCreatedTo() {
		return createdTo;
	}
	public void setCreatedTo(Date createdTo) {
		this.createdTo = createdTo;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public String getNomadicPhone() {
		return nomadicPhone;
	}
	public void setNomadicPhone(String nomadicPhone) {
		this.nomadicPhone = nomadicPhone;
	}
	public void setDisjunction(Boolean disjunction) {
		this.disjunction = disjunction;
	}
	public Boolean getDisjunction() {
		return disjunction;
	}
	
	public Boolean isEmpty() {
		return !( validFrom != null || validTo != null || createdFrom != null || createdFrom != null || StringUtils.isNotEmpty(nomadicPhone)
				|| StringUtils.isNotEmpty(cellPhone) || StringUtils.isNotEmpty(email) || StringUtils.isNotEmpty(location)
				|| StringUtils.isNotEmpty(superior) || StringUtils.isNotEmpty(kpx) || StringUtils.isNotEmpty(pesel) || active != null);
	}
	
	public String getLinkParameters() {
		return "&validFrom="+validFrom+"&validTo="+validTo+"&createdFrom="+createdFrom+"&nomadicPhone="+nomadicPhone+"&cellPhone="+cellPhone+
		"&email="+email+"&location="+location+"&superior="+superior+"&kpx="+kpx+"&pesel="+pesel+"&inactive="+(active == null?null:!active);
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getProfile() {
		return profile;
	}
}