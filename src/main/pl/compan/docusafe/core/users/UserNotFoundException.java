package pl.compan.docusafe.core.users;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserNotFoundException.java,v 1.6 2006/02/20 15:42:26 lk Exp $
 */
public class UserNotFoundException extends EntityNotFoundException
{
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    public UserNotFoundException(String username)
    {
        super(sm.getString("userNotFoundException", username), true);
    }
}
