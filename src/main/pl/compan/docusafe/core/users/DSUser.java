package pl.compan.docusafe.core.users;

import com.google.common.base.Objects;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.annotations.Unchecked;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.TimeResource;
import pl.compan.docusafe.core.certificates.UserCertificate;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

/**
 * <h3>Certyfikaty</h3>
 * Spos�b przechowywania certyfikat�w nie jest optymalny ze wzgl�du
 * na to, �e niekiedy u�ytkownicy s� trzymani w LDAP.  Dlatego
 * z obiektem u�ytkownika bezpo�rednio zwi�zane s� tylko jego
 * certyfikaty w formacie X.509, natomiast CSR i certyfikaty
 * PKCS#7 (u�ywane do automatycznego instalowania w przegl�darce)
 * s� sk�adowane w oddzielnych obiektach UserCertificate - nie
 * w prostych beanach powi�zanych z klas� UserImpl.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSUser.java,v 1.90 2010/07/02 08:10:44 mariuszk Exp $
 */
public abstract class DSUser implements java.io.Serializable, TimeResource
{
    private static final Logger log = LoggerFactory.getLogger(DSUser.class);

    public static final String ADMIN_ROLE = "admin";
    public static final String DATA_ADMIN_ROLE="data_admin";
    public static final int SORT_NONE = 0;
    public static final int SORT_NAME = 1;
    public static final int SORT_FIRSTNAME_LASTNAME = 1 << 2;
    public static final int SORT_LASTNAME_FIRSTNAME = 1 << 3;
	public static final Comparator<DSUser> LASTNAME_COMPARATOR = new LastnameComparator();

    public abstract Long getId();
    public abstract String getName();
    public abstract void setName(String name);
    public abstract String getExternalName();
    public abstract void setExternalName(String externalName);
    public abstract String getEncodedPassword();
    public abstract String getFirstname();
    public abstract void setFirstname(String firstname);
    public abstract String getLastname();
    public abstract void setLastname(String lastname);
    public abstract String getEmail();
    public abstract void setEmail(String email);
    public abstract String getHashedPassword();
    public abstract String getIdentifier();
    public abstract void setIdentifier(String identifier);
    public abstract boolean isLoginDisabled();
    public abstract void setLoginDisabled(boolean loginDisabled);
    public abstract String getPhoneNum();
    public abstract void setPhoneNum(String phoneNum);
    public abstract String getMobileNum();
    public abstract void setMobileNum(String mobileNum);
    public abstract String getExtension();
    public abstract void setExtension(String extension);
    public abstract String getCommunicatorNum();
    public abstract String getRoomNum();
    public abstract void setRoomNum(String roomNum);
    public abstract void setCommunicatorNum(String communicatorNum);
    public abstract boolean isChangePasswordAtFirstLogon();
    public abstract void setChangePasswordAtFirstLogon(boolean changePasswordAtFirstLogon);
	public abstract short getAuthlevel();
	public abstract void setAuthlevel(short authlevel);
	public abstract String getPracow_nrewid_erp();
	public abstract void setPracow_nrewid_erp(String pracow_nrewid_erp);
    /**
     * Zwraca zbi�r nazw r�l, w kt�rych znajduje si� u�ytkownik.
     * Zwracany zbi�r mo�e by� pusty, nigdy nie jest r�wny null.
     */
    public abstract Set getRoles();
    public abstract void addRole(String role) throws HibernateException, EdmException;
    public abstract void addRole(String role, String source) throws HibernateException, EdmException;
    public abstract void removeRole(String role, String source) throws HibernateException, EdmException;
    public abstract void archiveRolesOfCollection(Collection<String> roles) throws EdmException;
    public abstract Date getLastSuccessfulLogin();
    public abstract void setLastSuccessfulLogin(Date date);
    public abstract Date getLastUnsuccessfulLogin();
    public abstract void setLastUnsuccessfulLogin(Date date);
    public abstract DSUser getSupervisor() throws UserNotFoundException, EdmException;
    public abstract void setSupervisor(DSUser supervisor);
    public abstract Date getValidityDate();
    public abstract void setValidityDate(Date date);
    public abstract String getKpx();
    public abstract void setKpx(String kpx);
    public abstract String getRemarks();
    public abstract void setRemarks(String remarks);
    public abstract String getPesel();
    public abstract void setPesel(String pesel);
	public abstract Float getLimitMobile();
	public abstract void setLimitMobile(Float limitMobile);
	public abstract Float getLimitPhone();
	public abstract void setLimitPhone(Float limitPhone);
    
    
    public abstract Timestamp getCtime();
    public abstract void setCtime(Timestamp c);
    
    /**
     * Czy uzytkownik jest autoryzowany przez AD
     */
    protected Boolean adUser = false;

    /**
     * Prefix domenty ActiveDirectory
     */
    private String domainPrefix = "";
    /**
     * Url dla ActiveDirectory
     */
    private String adUrl = null;

    private DSUser substitute;
    private boolean substituteRead;
    /**
     * Zwraca u�ytkownika, kt�ry zast�puje bie��cego u�ytkownika.
     * wywo�anie jest cache'owane, aby w ramach transkacji wywo�ania
	 * tej metody by�y sp�jne (getSubstitute sprawdza dat� zast�pstwa
	 * przed zwr�ceniem warto�ci)
     */
    public DSUser getSubstituteUser() throws UserNotFoundException, EdmException {
		log.trace("Wywolano funkcje getSubstituteUser");
		DSUser subst = getSubstitute();
		if (subst != null && !substituteRead) 
		{
			Date now = new Date();
			if ((getSubstitutedFrom() != null || getSubstitutedThrough() != null) &&  getSubstitutedThrough().before(now)) 
			{
				cancelSubstitution();
			} 
			else
			{
				substitute = subst;
				substituteRead = true;

			}
		}
		return substitute;
	}
    
    public void setSubstitutionFromHistory(SubstituteHistory sh) throws UserNotFoundException, EdmException
    {
    	if(sh != null)
    	{
	    	this.setSubstitutedFrom(sh.getSubstitutedFrom());
	    	this.setSubstitutedThrough(sh.getSubstitutedThrough());
	    	this.setSubstitute(DSUser.findByUsername(sh.getSubstLogin()));
    	}
    	else
    	{
	    	this.setSubstitutedFrom(null);
	    	this.setSubstitutedThrough(null);
	    	this.setSubstitute(null);
    	}
    }
    
/***
 * Nalezy uzywac getSubstituteUser() poniewaz odswieza zastepstwo
 * @return
 * @throws UserNotFoundException
 * @throws EdmException
 */
    @Deprecated 
    public abstract DSUser getSubstitute() throws UserNotFoundException, EdmException;

    public abstract Date getSubstitutedFrom();
    public abstract Date getSubstitutedThrough();
    protected abstract void setSubstitute(DSUser substitute);
    protected abstract void setSubstitutedFrom(Date substitutedFrom);
    protected abstract void setSubstitutedThrough(Date substitutedThrough);
    
    public abstract DSDivision[] getOriginalDivisionsWithoutGroup() throws EdmException;
    
    public boolean equals(DSUser user)
    {
    	return this.getName().equals(user.getName());
    }
    
    /**
     * Zwraca list� u�ytkownik�w zast�powanych przez danego u�ytkownika.
     * Zwracani s� tylko u�ytkownicy, dla kt�rych zdefiniowany zakres
     * czasowy zast�pstwa zawiera bie��cy czas.
     * <p>
     * @return Lista u�ytkownik�w, mo�e by� pusta.
     */
    public abstract DSUser[] getSubstitutedUsers() throws EdmException;
    public abstract DSUser[] getSubstitutedUsersForNow(Date d) throws EdmException;

	public DSUser[] getSubstituted() throws EdmException {
		return getSubstitutedUsers();
	}
    /**
     * Czy uzytkownik jest uzytkownikiem autoryzowanym w ActiveDirectory
     * @return
     */
    public boolean isAdUser()
    {
    	return (adUser!=null && adUser);
    }
    
    /**
     * Czy uzytkownik jest autoryzowany w zewnetrznym repozytorium
     * Obecnie jest to tozsame jest w isAdUser()
     * @return
     */
    public boolean isExternalAuthorizedUser()
    {
    	return isAdUser();
    }

    public abstract boolean isDeleted();
    public abstract void setDeleted(boolean isDeleted);
    public abstract boolean isSupervisor();
    public abstract void setIsSupervisor(boolean isSupervisor);
    public abstract boolean isCertificateLogin();
    public abstract void setCertificateLogin(boolean certificateLogin);
    public abstract boolean isSystem();
    public abstract void setSystem(boolean system);
    public abstract Date getLastPasswordChange();
    public abstract boolean addCertificate(java.security.cert.X509Certificate cert)
        throws EdmException;
    public abstract boolean removeCertificate(java.security.cert.X509Certificate cert)
        throws EdmException;
    /**
     * Zwraca list� certyfikat�w u�ytkownika.
     * @return Tablica certyfikat�w (pusta, je�eli u�ytkownik nie ma certyfikat�w).
     * @throws EdmException
     */
    public abstract java.security.cert.X509Certificate[] getCertificates()
        throws EdmException;
    public abstract boolean verifyPassword(String password);
    
    
    /**
     * Zwraca liste dzia��w, w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract ArrayList<DSDivision> getDivisionsAsList() throws EdmException;

    public abstract ArrayList<DSDivision> getDivisionsAsListSafe();
    
    /**
     * Zwraca tablic� dzia��w, w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract DSDivision[] getDivisions() throws EdmException;
    
    /**
     * Zwraca tablic� dzia��w i grup, w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract String[] getDivisionsGuid() throws EdmException;
    /**
     * Zwraca tablic� dzia��w bez grup, w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract String[] getDivisionsGuidWithoutGroup() throws EdmException;
    
    /**
     * Zwraca tablic� dzia��w (bez grup i stanowisk), w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract DSDivision[] getDivisionsWithoutGroupPosition() throws EdmException;
    
    /**
     * Zwraca tablic� dzia��w (bez grup), w kt�rych jest u�ytkownik. Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract DSDivision[] getDivisionsWithoutGroup() throws EdmException;
    /**
     * Zwraca tablic� dzia��w, w kt�rych jest u�ytkownik(oraz dzia�y gdzie jest jako backup). Gdy jest w 
     * jakim� dziale-stanowisku zwraca takze dzia� nadrz�dny do takiego.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract DSDivision[] getAllDivisions() throws EdmException;
    /**
     * Zwraca tablic� dzia��w, w kt�rych "fizycznie" jest u�ytkownik.
     * @return Tablica dzia��w, mo�e by� pusta.
     */
    public abstract DSDivision[] getOriginalDivisions() throws EdmException;
    /**
     * Zwraca tablic� dzia��w w kt�rych jest u�ytkownik, oraz tych podleg�ych 
     * dzia��w u�ytkownika.
     * @return Tablica dzia��w, mo�e by� pusta
     * @throws EdmException
     */
    public abstract DSDivision[] getSubordinateDivisions() throws EdmException;
    /**
     * Zwraca nazwy dzia��w w kt�rych jest u�ytkownik bez grup
     * @return
     * @throws EdmException
     */
    public String[] getOrdinaryDivisions() throws EdmException {
    	DSDivision[] allDivs = getOriginalDivisions();
    	Set<String> result = new HashSet<String>();
    	for (DSDivision div : allDivs) {
    		if (!div.getDivisionType().equals(DSDivision.TYPE_GROUP)) {
    			result.add(div.getPrettyPath());
    		}
    	}
    	return result.toArray(new String[result.size()]);
    }
    /**
     * Zwraca nazwy grup w kt�rych jest u�ytkownik
     * @return
     * @throws EdmException
     */
    public String[] getGroups() throws EdmException {
    	DSDivision[] allDivs = getOriginalDivisions();
    	Set<String> result = new HashSet<String>();
    	for (DSDivision div : allDivs) {
    		if (div.getDivisionType().equals(DSDivision.TYPE_GROUP)) {
    			result.add(div.getPrettyPath());
    		}
    	}
    	return result.toArray(new String[result.size()]);
    }
    /**
     * Zwraca guid grup w kt�rych jest u�ytkownik
     * @return
     * @throws EdmException
     */
    public String[] getGroupsGuid() throws EdmException{
        DSDivision[] allDivs = getOriginalDivisions();
    	Set<String> result = new HashSet<String>();
    	for (DSDivision div : allDivs) {
    		if (div.getDivisionType().equals(DSDivision.TYPE_GROUP)) {
    			result.add(div.getGuid());
    		}
    	}
    	return result.toArray(new String[result.size()]);
    }
    
    public DSDivision[] getDSGroups() throws EdmException {
    	DSDivision[] allDivs = getOriginalDivisions();
    	Set<DSDivision> result = new HashSet<DSDivision>();
    	for (DSDivision div : allDivs) {
    		if (div.getDivisionType().equals(DSDivision.TYPE_GROUP)) {
    			result.add(div);
    		}
    	}
    	return result.toArray(new DSDivision[result.size()]);
    }
    public abstract String[] getOfficeRoles() throws EdmException;
    public abstract boolean inDivision(DSDivision division) throws EdmException;
    /**Sprawdza czy u�ytkownik znajduje si� w okre�lonym dziale, je�li stanowisko sprawdza dzia� nadrz�dny
     * Je�li guid = #system# zwraca true*/
    public abstract boolean inDivisionByGuid(String guid) throws EdmException;
    public abstract boolean inOriginalDivision(DSDivision division) throws EdmException;
    /**Zwraca tablic� dzia��w w kt�rych jest u�ytkownik, oraz tych podleg�ych 
     * dzia��w u�ytkownika. Bez grup */
    public abstract DSDivision[] getSubordinateDivisionsWithoutGroup() throws EdmException;
    /**
     * Aktualizuje dane u�ytkownika z wyj�tkiem has�a.
     */
    public abstract void update() throws EdmException;
    /**
     * Aktualizuje has�o u�ytkownika.
     */
    public abstract void setPassword(String password, boolean initial) throws EdmException;

    public void setPassword(String password) throws EdmException
    {
        setPassword(password, false);
    }

    public abstract boolean equals(Object object);
    public abstract int hashCode();
    
    public abstract String[] getProfileNames();
    public abstract Map<String,String> getProfilesWithKeys();
    public abstract Map<String, String[]> getRolesSources();
    
    public static DSUser findByUsername(String username)
        throws EdmException, UserNotFoundException
    {
        //log.debug("findByUsername: '"+username+"'");
        return UserFactory.getInstance().findByUsername(username);
    }
    
    public static DSUser findByExternalName(String username)
    throws EdmException, UserNotFoundException
    {

    	return UserFactory.getInstance().findByExternalName(username);
    }


    public static DSUser findSystemUser(String username)
        throws EdmException, UserNotFoundException
    {
        return UserFactory.getInstance().findSystemUser(username);
    }
    
    public static DSUser findByExtension(String extension)
    throws EdmException, UserNotFoundException
	{
    	log.error("findByExtension ext=" + extension);
	    return UserFactory.getInstance().findByExtension(extension);
	}
    
    public static DSUser findAllByExtension(String extension)
    throws EdmException, UserNotFoundException
	{
	    return UserFactory.getInstance().findAllByExtension(extension);
	}
    
    public static DSUser findByIdentifier(String identifier)
    throws EdmException, UserNotFoundException
	{
	    return UserFactory.getInstance().findByIdentifier(identifier);
	}

    public static DSUser findByEmail(String email)
        throws EdmException, UserNotFoundException
    {
        return UserFactory.getInstance().findByEmail(email);
    }
    
    public static class UserComparatorAsLastFirstName implements Comparator<DSUser>
    {
        public int compare(DSUser e1, DSUser e2)
        {
            return (e1.asLastnameFirstname()).compareTo(e1.asLastnameFirstname());
        }
    }
    
    public static DSUser findById(Long id) throws EdmException, UserNotFoundException
    {
    	return UserFactory.getInstance().findById(id);
    }

    public static DSUser findByIdOrNameOrExternalName(String v) throws EdmException, UserNotFoundException
    {
        DSUser user = null;
        try{
            if(StringUtils.isNumeric(v))
                user = findById(Long.valueOf(v));
            else
                throw new UserNotFoundException("");
        }catch(UserNotFoundException e){
            try{
                user = findByUsername(v);
            } catch(UserNotFoundException ex){
                user = findByExternalName(v);
            }
        }

        return user;
    }

    public static DSUser findByCertificate(X509Certificate certificate)
        throws EdmException, UserNotFoundException
    {
        return UserFactory.getInstance().findByCertificate(certificate);
    }

    public static SearchResults<DSUser> getUsers(int offset, int limit, int sortMode, boolean ascending)
        throws EdmException
    {
        return UserFactory.getInstance().searchUsers(offset, limit, sortMode, ascending, null,false);
    }

    public static SearchResults<DSUser> search(int offset, int limit, int sortMode, boolean ascending,
                                       String query)
        throws EdmException
    {
        return UserFactory.getInstance().searchUsers(offset, limit, sortMode, ascending, query,false);
    }

    /**
     * Cache do listy zwracanej przez list(int sortMode)
     */
    private static final LoadingCache<Integer, List<DSUser>> cachedUserList = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build(new CacheLoader<Integer, List<DSUser>>() {
                @Override
                public List<DSUser> load(Integer integer) throws Exception {
                    return Collections.unmodifiableList(list(integer));
                }
            });
    
    /**
     * Cache do listy zwracanej przez list(int sortMode)
     */
    private static final LoadingCache<Integer, List<DSUser>> cachedUserListWithDeleted = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build(new CacheLoader<Integer, List<DSUser>>() {
                @Override
                public List<DSUser> load(Integer integer) throws Exception {
                    return Collections.unmodifiableList(listWithDeleted(integer));
                }
     });
    
    /**
     * Zwraca cacheowan� list� u�ytkownik�w wraz z usunietymi, u�ytkownicy s� modyfikowani w tak wielu miejscach,
     * �e metoda nie zawsze zwraca aktualne dane. Najlepiej stosowa� w miejscach gdzie trzeba tylko
     * wy�wietli� list� u�ytkownik�w.
     * @param sortMode
     * @return
     * @throws EdmException
     */
    public static List<DSUser> listWithDeletedCached(int sortMode) throws EdmException {
        try {
            return cachedUserListWithDeleted.get(sortMode);
        } catch (ExecutionException e) {
            throw new EdmException(e);
        }
    }
    

    /**
     * Zwraca cacheowan� list� u�ytkownik�w, u�ytkownicy s� modyfikowani w tak wielu miejscach,
     * �e metoda nie zawsze zwraca aktualne dane. Najlepiej stosowa� w miejscach gdzie trzeba tylko
     * wy�wietli� list� u�ytkownik�w.
     * @param sortMode
     * @return
     * @throws EdmException
     */
    public static List<DSUser> listCached(int sortMode) throws EdmException {
        try {
            return cachedUserList.get(sortMode);
        } catch (ExecutionException e) {
            throw new EdmException(e);
        }
    }

    public static List<DSUser> list(int sortMode) throws EdmException
    {
        long time = System.currentTimeMillis();
        List<DSUser> result = Arrays.asList(UserFactory.getInstance().searchUsers(0, 0, sortMode, true, null,false).results());
        if (log.isDebugEnabled()){
            log.debug("DSUser.list(int): "+(System.currentTimeMillis()-time));
        }
        return result;
    }
    
    /**
     * Zwraca list� podwladnych z drzewa akceptacji
     * @return
     * @throws EdmException 
     */
    public static List<DSUser> listAcceptanceSubordinates(String username) throws EdmException
    {
        Collection<String> usernames = new ArrayList<String>();
        List<DSUser> returnUsers = new ArrayList<DSUser>();
        
        List<AcceptanceCondition> conditions = null;
        
        if ((AvailabilityManager.isAvailable("aegon.docusafe.remote")) || 
           ( !AvailabilityManager.isAvailable("aegon.docusafe.remote")))
                conditions = AcceptanceCondition.findByUserName("szef_dzialu", username);
        else
                conditions = AcceptanceCondition.findByUserName("szef_dzialu", "ext:"+username);
        
        for (AcceptanceCondition condition : conditions)
        {
            if(!condition.getFieldValue().startsWith("ext:"))
            {
                if(!usernames.contains(condition.getFieldValue()))
                    usernames.add(condition.getFieldValue());
            }
            else
            {
//                pobiera list� uzytkownik�w zewn�trznych -- narazie nie da si� tego zrobi� DSUserEnumField - Krzyczy �e ma problem 
//                    DSUser tmpUser = AbstractOvertimeLogic.getRemoteUserInfo(condition.getFieldValue().replaceFirst("ext:", ""));
//                    returnUsers.add(tmpUser);
            }
        }
        boolean addAll = returnUsers.addAll(DSUser.findByUsernames(usernames));
        
        return returnUsers;
    }
    
    /**
     * Zwraca list� podwladnych z drzewa akceptacji
     * @see listAcceptanceSubordinates(String username)
     * @return
     * @throws EdmException 
     */
    public static List<DSUser> listAcceptanceSubordinates() throws EdmException
    {
        return listAcceptanceSubordinates(DSApi.context().getDSUser().getName());
    }
    
    public static List<DSUser> listWithDeleted(int sortMode) throws EdmException
    {
        List<DSUser> result = Arrays.asList(UserFactory.getInstance().searchUsers(0, 0, sortMode, true, null,true).results());
        return result;
    }
    
    /**
     * funkcja wywolywana przy kazdym udanym zalogowaniu.
     * Ustawia kolumne incorrectlogins (DS_USER) na zero.
     *
     * @param _username  username
     * @throws EdmException
     */
    @Unchecked
    public static void correctLogin(String username) throws EdmException
    {
    	try{
    		 DSApi.openAdmin();
			 DSApi.context().begin();
    		 Preferences prefs = DSApi.context().systemPreferences().node(PasswordPolicy.NODE_PASSWORD_POLICY); 
    		 if(!(prefs).getBoolean(PasswordPolicy.KEY_LOCKING_PASSWORDS,false))
    		 {
    			DSApi.context().setRollbackOnly();
    			DSApi.close();
    		 	return;
    		 }

    		 
    		PreparedStatement ps = DSApi.context().prepareStatement("update ds_user set incorrectlogins = 0 where name=?");
			
    		ps.setString(1, username);
			ps.executeUpdate();
			DSApi.context().closeStatement(ps);
					
			DSApi.context().commit();			
			DSApi.close();

    		 
    	}
    	catch(Exception e)
    	{
    		DSApi.context().setRollbackOnly();
    		DSApi.close();
    		throw new EdmException(e.getMessage(), e);
    	}
    	
    }
    
    /**
     * 
     * funkcja wywolywana przy kazdym nieudanym zalogowaniu.
     * Zwieksza wartosc kolumny incorrectlogins (DS_USER) o jeden.
     * jesli liczba ta osiagnie liczbe, przy ktorej konto powinno byc zablokowane
     * to konto jest blokowane, a nastepnie incorrectlogins jest ustawiane na zero.
     * 
     * @param _username  username
     * @throws EdmException
     * @return Czy konto zostalo zablokowane
     */
    public static boolean incorrectLogin(String username) throws EdmException
    {
    	boolean locked = false;
    	PreparedStatement ps = null;
    	try
    	{
    		 DSApi.openAdmin();
    		 
    		 Preferences prefs = DSApi.context().systemPreferences().node(PasswordPolicy.NODE_PASSWORD_POLICY);   
    		 if(!(prefs).getBoolean(PasswordPolicy.KEY_LOCKING_PASSWORDS,false))
    			 return locked;
    		 Integer maxIncorrectLogins = new Integer(prefs.getInt(PasswordPolicy.KEY_HOW_MANY_INCORRECT_LOGINS, 0));
    		 
    		 
    		 
   		 	 DSApi.context().begin(); 
   		 	 ResultSet rs;
	         ps = DSApi.context().prepareStatement("select logindisabled,incorrectlogins from ds_user where name=?");
	         ps.setString(1, username);
	         
	         rs = ps.executeQuery();	         
	         if(!rs.next())
	         {
	        	 DSApi.context().closeStatement(ps);
	        	 ps = null;
	        	 //DSApi.context().setRollbackOnly();
	        	 //DSApi.close();
	        	 return locked;
	         }
	         Boolean disabled = rs.getBoolean(1);
	         Integer incorrectlogins = rs.getInt(2);
	         
	         if(disabled)
	        	 return true;
	        	 
	         DSApi.context().closeStatement(ps);
	         ps = null;
	         if(maxIncorrectLogins<=incorrectlogins+1)
	         {
	         	ps = DSApi.context().prepareStatement("update ds_user set logindisabled = 1, incorrectlogins = 0 where name=?");
	         	ps.setString(1, username);
	         	ps.executeUpdate();
	         	locked = true;
	         	DSApi.context().closeStatement(ps);
	         }
	         else 
	         {
	        	 ps = DSApi.context().prepareStatement("update ds_user set incorrectlogins = ? where name=?");
	        	 ps.setInt(1, incorrectlogins+1);
	        	 ps.setString(2, username);
	        	 ps.executeUpdate();
	        	 DSApi.context().closeStatement(ps);
	         }
	         
	         DSApi.context().commit();
	         //DSApi.close();
	         return locked;
    	}
    	catch(Exception e)
    	{
    		DSApi.context().setRollbackOnly();
    		//DSApi.close();
    		throw new EdmException(e.getMessage(), e);
    	}
    	finally
    	{
    		DSApi.context().closeStatement(ps);
    		DSApi._close();
    	}

    }

   /**
    * Zwraca list� u�ytkownik�w, kt�rzy s� bezpo�rednimi 
    * prze�ozonymi bie��cego u�ytkownika, na podstawie danych w bazie
    * w kolumnie isSupervisor i dzia��w, w kt�rych jest uzytkownik
    * @return 
    * @throws EdmException
    */
	public DSUser[] getImmediateSupervisors() throws EdmException {
		LinkedList<DSDivision> ownDivisions = new LinkedList<DSDivision>(Arrays.asList(getDivisionsWithoutGroupPosition()));
		Set<DSUser> supervisors = new HashSet<DSUser>();

		try {
			do {
				List<DSDivision> toRemove = new ArrayList<DSDivision>();
				List<DSDivision> toAdd = new ArrayList<DSDivision>();
				for (DSDivision division : ownDivisions) {
					boolean repeat = true;
					for (DSUser neighbour : division.getUsers()) {
						if (neighbour.isSupervisor() && !this.equals(neighbour)) {
							supervisors.add(neighbour);
							repeat = false;
						}
					}
					if (!repeat) {
						toRemove.add(division);
					} else if (repeat && division.getParent() != null) {
						toAdd.add(division.getParent());
						toRemove.add(division);
					} else {
						toRemove.add(division);
					}
				}
				ownDivisions.removeAll(toRemove);
				ownDivisions.addAll(toAdd);
			} while (!ownDivisions.isEmpty());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		if (supervisors.isEmpty()) {
			supervisors.add(DSUser.findByUsername("admin"));
		}

		return supervisors.toArray(new DSUser[supervisors.size()]);
	}
    
    /**
     * Zwraca u�ytkownik�w, kt�rzy znajduj� si� w tych samych dzia�ach,
     * co bie��cy u�ytkownik.  U�ytkownicy przypisani do stanowiska
     * s� traktowani, jak znajduj�cy si� w dziale nadrz�dnym dla tego
     * stanowiska.
     * @param divisionTypes ..., mo�e by� null (nieu�ywane)
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public DSUser[] getNeighbours(String[] divisionTypes) throws EdmException
    {
        Set<DSUser> users = new HashSet<DSUser>();

        if (divisionTypes != null && divisionTypes.length > 0)
            Arrays.sort(divisionTypes);

        DSDivision[] divisions = getDivisions();
        for (int i=0; i < divisions.length; i++)
        {
            if (divisionTypes != null && divisionTypes.length > 0 &&
                Arrays.binarySearch(divisionTypes, divisions[i].getDivisionType()) < 0)
                continue;
            users.addAll(Arrays.asList(divisions[i].getUsers(true)));
            if (divisions[i].isPosition())
                users.addAll(Arrays.asList(divisions[i].getParent().getUsers(true)));
        }

        return (DSUser[]) users.toArray(new DSUser[users.size()]);
    }


    public boolean sharesDivision(DSUser user) throws EdmException
    {
        Set<DSUser> users = new HashSet<DSUser>();

        DSDivision[] divisions = getDivisions();
        for (int i=0; i < divisions.length; i++)
        {
            users.addAll(Arrays.asList(divisions[i].getUsers(true)));
        }

        return users.contains(user);
    }

    public boolean isAdmin()
    {
        Set roles = getRoles();
        return roles != null && roles.contains(ADMIN_ROLE);
    }

    /**
     * Zwraca imi� i nazwisko u�ytkownika rozdzielone spacj�. Je�eli
     * u�ytkownik nie ma zdefiniowanego ani imienia ani nazwiska,
     * zwracany jest pusty napis.
     */
    public String asFirstnameLastname()
    {
        return (getFirstname() != null ? getFirstname()+" " : "") +
            (getLastname() != null ? getLastname() : "");
    }
    
    

    /**
     * Zwraca imi� i nazwisko u�ytkownika rozdzielone spacj�, a po
     * nich nazw� (identyfikator) w nawiasach. Je�eli
     * u�ytkownik nie ma zdefiniowanego ani imienia ani nazwiska,
     * zwracany sam identyfikator bez nawias�w.
     */
    public String asFirstnameLastnameName()
    {
        String firstnameLastname = asFirstnameLastname();
        return firstnameLastname.length() > 0 ?
            firstnameLastname+" ("+getName()+")" : getName();
    }

    /**
     * Zwraca imi� i nazwisko u�ytkownika rozdzielone spacj�, a po
     * nich nazw� (identyfikator) w nawiasach. Je�eli
     * u�ytkownik nie ma zdefiniowanego ani imienia ani nazwiska,
     * zwracany sam identyfikator bez nawias�w.
     */
    public String getFirstnameLastnameName()
    {
        String firstnameLastname = asFirstnameLastname();
        return firstnameLastname.length() > 0 ?
            firstnameLastname+" ("+getName()+")" : getName();
    }

    public String asLastnameFirstname()
    {
        return (getLastname() != null ? getLastname()+" " : "") +
            (getFirstname() != null ? getFirstname() : "");
    }

    public String asLastnameFirstnameName()
    {
        String lastnameFirstname = asLastnameFirstname();
        return lastnameFirstname.length() > 0 ?
            lastnameFirstname+" ("+getName()+")" : getName();
    }
    
    public String getLastnameFirstnameName()
    {
        String lastnameFirstname = asLastnameFirstname();
        return lastnameFirstname.length() > 0 ?
            lastnameFirstname+" ("+getName()+")" : getName();
    }

    public String getLastnameFirstnameWithOptionalIdentifier()
    {
        String lastnameFirstname = asLastnameFirstname();
        String identifier = Objects.firstNonNull(getExternalName(), getName());
        return lastnameFirstname.length() > 0 ?
            lastnameFirstname + (isObligatoryIdentifier() ? " ("+ identifier +")" : "" ) : identifier;
    }

    private boolean isObligatoryIdentifier() {
        return AvailabilityManager.isAvailable("user.fullName.obligatoryIdentifier");
    }

    /**
     * Zwraca true, je�eli u�ytkownik znajduje si� w danym dziale
     * lub w jednym ze stanowisk przypisanych do dzia�u.
     */
    public final boolean inDivision(DSDivision division, boolean relaxed)
        throws EdmException
    {
        if (inDivision(division))
            return true;

        DSDivision[] positions = division.getChildren(DSDivision.TYPE_POSITION);
        for (int i=0; i < positions.length; i++)
        {
            if (inDivision(positions[i]))
                return true;
        }

        return false;
    }

    public final boolean addCertificate(byte[] certData) throws EdmException
    {
        return addCertificate(UserCertificate.getX509Certificate(certData));
    }

    public final boolean removeCertificate(byte[] certData) throws EdmException
    {
        return removeCertificate(UserCertificate.getX509Certificate(certData));
    }

    public void setSubstitution(DSUser substitute, Date from, Date through)
        throws EdmException
    {
        if (substitute == null)
            throw new NullPointerException("substitute");
        if (from == null)
            throw new NullPointerException("from");
        if (through == null)
            throw new NullPointerException("through");

        if (through.before(from))
            throw new EdmException("Data rozpocz�cia zast�pstwa jest p�niejsza ni� " +
                "data jego zako�czenia");

        Calendar calthrough = Calendar.getInstance();
    	calthrough.setTime(through);
    	calthrough.set(Calendar.HOUR_OF_DAY, 23);
    	calthrough.set(Calendar.MINUTE, 59);
    	
        setSubstitute(substitute);
        setSubstitutedFrom(from);
        setSubstitutedThrough(calthrough.getTime());
    }

    public void cancelSubstitution()
    {
        setSubstitute(null);
        setSubstitutedFrom(null);
        setSubstitutedThrough(null);
    }

    /**
     * Zwraca imi� i nazwisko u�ytkownika, napis "Nie znaleziono
     * u�ytkownika: NAZWA", je�eli u�ytkownik nie zostanie znaleziony
     * lub pusty napis, gdy parametr username jest r�wny null.
     */
    public static String safeToFirstnameLastname(String username) throws EdmException
    {
        if (username == null)
            return "";
        if("#system#".equals(username))
        {
        	StringManager sm =GlobalPreferences.loadPropertiesFile("",null);
        	return sm.getString("System");
        }
        	

        try
        {
            return findByUsername(username).asFirstnameLastname();
        }
        catch (UserNotFoundException e)
        {
            return "Nie znaleziono u�ytkownika: "+username;
        }
    }
    
    /**
     * Zwraca nazwisko imie u�ytkownika, napis "Nie znaleziono
     * u�ytkownika: NAZWA", je�eli u�ytkownik nie zostanie znaleziony
     * lub pusty napis, gdy parametr username jest r�wny null.
     */
    public static String safeToLastnameFirstname(String username) throws EdmException
    {
        if (username == null)
            return "";

        try
        {
            return findByUsername(username).asLastnameFirstname();
        }
        catch (UserNotFoundException e)
        {
            return "Nie znaleziono u�ytkownika: "+username;
        }
    }

    /**
     * Zwraca imi�, nazwisko i nazw� u�ytkownika, napis "Nie znaleziono
     * u�ytkownika: NAZWA", je�eli u�ytkownik nie zostanie znaleziony
     * lub pusty napis, gdy parametr username jest r�wny null.
     */
    public static String safeToFirstnameLastnameName(String username) throws EdmException
    {
        if (username == null)
            return "";

        try
        {
            return findByUsername(username).asFirstnameLastnameName();
        }
        catch (UserNotFoundException e)
        {
            return "Nie znaleziono u�ytkownika: "+username;
        }
    }

    public final String toString()
    {
        return getClass().getName()+"[name="+getName()+"]";
    }

    public static boolean enabled(String username) throws EdmException
    {
        try
        {
            if (DSUser.findByUsername(username).isDeleted())
                return false; 
            return true;
        }
        catch (UserNotFoundException e)
        {
            return false;
        }
    }
    
    public static DSUser findByFirstnameLastname(String firstname, String lastname) throws EdmException
    {
    	return findByFirstnameLastname(firstname, lastname,true);
    }
    
    /**
     * Wyszukuje uzykownika po imieniu i nazwisku, 
     * deleted = true -> uwzgldnia usunietych
     * */
    public static DSUser findByFirstnameLastname(String firstname, String lastname, boolean deleted) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.like("firstname", "%"+firstname+"%"));
        criteria.add(Restrictions.like("lastname", "%"+lastname+"%"));
        if(!deleted)
        	criteria.add(Restrictions.not(Restrictions.eq("deleted", true)));
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return (DSUser) list.get(0);
            }
           // throw new UserNotFoundException(firstname+" "+lastname);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
		return null;
    }
    
    /**
     * Wyszukuje u�ytkownik�w po imieniu i nazwisku, 
     * */
    public static List<DSUser> findUsersByFirstnameLastname(String firstname, String lastname) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.like("firstname", "%"+firstname+"%"));
        criteria.add(Restrictions.like("lastname", "%"+lastname+"%"));
       
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return list;
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
		return null;
    }
    
    /**
     * Zwraca podw�adnych u�ytkownika
     * @param supervisor
     * @return list
     * @throws EdmException
     */
    public static List<DSUser> getSupervisorUsers(DSUser supervisor) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
		criteria.add(Restrictions.eq("supervisor", supervisor));
		return criteria.list();
           
	}
    
    
    /** 
     * Zwraca List� uzytkownik�w o podanym username
     */
    public static List<DSUser> findByUsernames(Collection<String> usernames) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        if(usernames.isEmpty())
            return null;
        
        criteria.add(Restrictions.in("name", usernames));
        criteria.add((Restrictions.eq("deleted", false)));
        
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return list;
            }
           // throw new UserNotFoundException(firstname+" "+lastname);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
		return null;
    }
    /**
     * Wyszukuje uzykownik�w po nazwisku, zwraca null w przypadku braku pasuj�cych u�ytkownik�w
     * */
    public static List<DSUser> findByLastname(String lastname) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.like("lastname", "%"+lastname+"%"));
//        if(!deleted)
//        	criteria.add(Restrictions.not(Restrictions.eq("deleted", true)));
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return list;
            }
           // throw new UserNotFoundException(firstname+" "+lastname);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
		return null;
    }

    /**
     * Wyszukuje uzykownik�w po nazwisku(dokladnie po nim, a nie jak wczesniej like), zwraca null w przypadku braku pasuj�cych u�ytkownik�w
     * */
    public static List<DSUser> findByLastnameNotLike(String lastname) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.eq("lastname", lastname));
//        if(!deleted)
//        	criteria.add(Restrictions.not(Restrictions.eq("deleted", true)));
        try
        {
            List list = criteria.list();
            if (list.size() > 0)
            {
                return list;
            }
           // throw new UserNotFoundException(firstname+" "+lastname);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
		return null;
    }
	private static class LastnameComparator implements Comparator<DSUser>{
		public int compare(DSUser o1, DSUser o2) {
			return o1.getLastname().compareTo(o2.getLastname());
		}
	}

	/**
	 * Zwraca Tablic� usernam�w z przekazanych idk�w u�ytkownika
	 * @param userIds
	 * @throws EdmException 
	 */
	public static List<String> getUsernameArrayById(List<Long> userIds) throws EdmException
	{
		List<String> usernameArray = new ArrayList<String>();
		
		for(Long id : userIds)
		{
			try
			{
				usernameArray.add(DSUser.findById(id).getName());
			}catch (UserNotFoundException e) {
				log.error("nie ma user o przekazanym id", e);
			}
		}
		
		return usernameArray;
	}
	
	protected String getLdifDn(DSDivision parent) throws EdmException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("cn=").append(this.asFirstnameLastname()).append(",").append(parent.getLdifDn());
		return sb.toString();
	}

    /**
     * @param firstnameLastname
     * @param split Znak podzia�u
     * @return Imie, Naziwsko
     */
    public static String[] getUserByFirstnameLastnameBySplit(String firstnameLastname, String split) {
        String string = firstnameLastname;
        String[] user = string.split(split);
        String firstname = user[0];
        String lastname = user[1];
        return user;
    }
	
	protected String getLdifBlock(DSDivision parent) throws EdmException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("dn: ").append(this.getLdifDn(parent)).append("\n");
		sb.append("cn: ").append(this.asFirstnameLastname()).append("\n");
		sb.append("docusafe_id: ").append(this.getName()).append("\n");
		sb.append("sn: Organizational Person\n");
		sb.append("objectClass: Organizational Person\n");
		sb.append("objectClass: person\n");
		sb.append("objectClass: inetOrgPerson\n");
		sb.append("objectClass: top\n");
		return sb.toString();		
	}

	public static boolean isUserExists(String userName) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
		criteria.add(Restrictions.like("name", userName));
		try
		{
			List list = criteria.list();
			if (list.size() > 0)
			{
				return true;
			}
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
		return false;
	}
	
	public Boolean getAdUser() {
			return adUser;
	}
	public void setAdUser(Boolean adUser) {
		this.adUser = adUser;
	}
	public void setDomainPrefix(String domainPrefix) {
		this.domainPrefix = domainPrefix;
	}
	public String getDomainPrefix() {
		return domainPrefix;
	}
	
	public String getPartOneOfName()
	{
		return getFirstname();
	}
	
	public String getPartTwoOfName()
	{
		return getLastname();
	}
	
	public String getCn()
	{
		return getName();
	}
	
	public String getType()
	{
		return CalendarFactory.DSUSER_RESOURCE_TYPE;
	}
	
	public String getWholeName()
	{
		return getFirstname()+" "+getLastname();
	}

    public String getAdUrl() {
        return adUrl;
    }

    public void setAdUrl(String adUrl) {
        if(adUrl != null && adUrl.isEmpty()){
            this.adUrl = null;
        } else {
            this.adUrl = adUrl;
        }
    }

    public boolean getIsSubstituted() {
        try {
            return (this.getSubstituteUser()!=null);
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public abstract boolean hasKeysForProfiles();    
    
    public static DSUser getLoggedInUser() throws EdmException {
        DSContextOpener opener = DSContextOpener.openAsUserIfNeed();
        DSUser user = DSApi.context().getDSUser();
        opener.closeIfNeed();
//        DSUser user = null;
//        if (DSApi.isContextOpen()){
//            user = DSApi.context().getDSUser();
//        }
//        else{
//            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
//            user = DSApi.context().getDSUser();
//            if (DSApi.isContextOpen())
//                DSApi._close();
//        }
        return user;
    }

    /**
     * @return U�ytkownik. W przypadku wyj�tku zwracany jest null.
     */
    public static DSUser getLoggedInUserSafely() {
        try {
            return getLoggedInUser();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

}
