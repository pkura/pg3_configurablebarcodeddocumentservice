package pl.compan.docusafe.core.users;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *  Klasa zawieraj�ca r�ne cache dotycz�ce DSUSer
 */
public final class DSUserCache {
    private final static Logger LOG = LoggerFactory.getLogger(DSUserCache.class);

    /** Cache (loginy) u�ytkownik->u�ytkownik, kt�ry go zast�uje */
    public final static Cache<String, Optional<String>> SUBSTITUTE_USER_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build();

    /**
     * Zwraca login u�ytkownika, kt�ry zast�puje przekazanego u�ytkownika
     * @param username login u�ytkownika
     * @return login u�ytkownika, kt�ry zast�puje u�ytkownika o przekazanym loginie, mo�e by� null
     */
    public static String getSubstituteUser(final String username){
        try {
            return SUBSTITUTE_USER_CACHE.get(username, new Callable<Optional<String>>() {
                @Override
                public  Optional<String> call() throws Exception {
                    try {
                        DSUser substitute = DSUser.findByUsername(username).getSubstituteUser();
                        return substitute != null
                                ? Optional.of(substitute.getName())
                                : Optional.<String>absent();
                    } catch (UserNotFoundException ex) {
                        LOG.error("getSubstituteUser called and user not found ({})", username);
                        return Optional.absent();
                    }
                }
            }).orNull();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Metoda, kt�ra powinna by� wywo�ywana w momencie dowolnej zmiany dotycz�cej zast�pst
     */
    public static void onSubstitutionChange(){
        SUBSTITUTE_USER_CACHE.invalidateAll();
    }

    private DSUserCache(){}
}
