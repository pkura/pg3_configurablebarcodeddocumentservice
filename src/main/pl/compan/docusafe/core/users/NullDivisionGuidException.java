package pl.compan.docusafe.core.users;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NullDivisionGuidException.java,v 1.1 2004/05/16 20:10:35 administrator Exp $
 */
public class NullDivisionGuidException extends EdmException
{
    public NullDivisionGuidException(String message)
    {
        super(message);
    }

    public NullDivisionGuidException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullDivisionGuidException(Throwable cause)
    {
        super(cause);
    }
}
