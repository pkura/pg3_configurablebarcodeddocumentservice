package pl.compan.docusafe.core.users;

public class UserView {

    public final String name;
    public final String firstname;
    public final String lastname;
    public final Long id;

    public UserView(DSUser user) {
        this.name = user.getName();
        this.firstname = user.getFirstname();
        this.lastname = user.getLastname();
        this.id = user.getId();
    }

}
