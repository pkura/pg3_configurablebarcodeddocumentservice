package pl.compan.docusafe.core.users;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSDivisionFilter.java,v 1.1 2005/04/10 22:08:52 lk Exp $
 */
public interface DSDivisionFilter
{
    boolean accept(DSDivision division);
}
