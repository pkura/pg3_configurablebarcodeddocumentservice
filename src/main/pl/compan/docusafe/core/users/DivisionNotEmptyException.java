package pl.compan.docusafe.core.users;

import pl.compan.docusafe.core.EdmException;
/* User: Administrator, Date: 2006-03-07 12:40:59 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionNotEmptyException.java,v 1.1 2006/03/07 16:02:59 lk Exp $
 */
public class DivisionNotEmptyException extends EdmException
{
    public DivisionNotEmptyException(String message)
    {
        super(message, true);
    }
}
