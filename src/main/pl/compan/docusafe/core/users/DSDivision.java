package pl.compan.docusafe.core.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Optional;
import org.apache.axis.utils.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.DataMartEventBuilder;
import pl.compan.docusafe.core.datamart.DataMartEventProcessors.EventType;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import std.fun;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSDivision.java,v 1.47 2010/08/04 06:59:25 mariuszk Exp $
 */
public abstract class DSDivision
{
    /**
     * Sta�a u�ywana jako warto�� atrybutu docusafeDivisionType
     * dla zwyczajnych dzia��w. 
     */
    public static final String TYPE_DIVISION = DivisionType.DIVISION.getId();
    /**
     * Sta�a u�ywana jako warto�� atrybutu docusafeDivisionType
     * dla dzia��w b�d�cych stanowiskami.
     */
    public static final String TYPE_POSITION = DivisionType.POSITION.getId();
    /**
     * Sta�a u�ywana jako warto�� atrybutu docusafeDivisionType
     * dla dzia��w b�d�cych grupami u�ytkownik�w.
     */
    public static final String TYPE_GROUP = DivisionType.GROUP.getId();
    /**
     * Sta�a u�ywana jako warto�� atrybutu docusafeDivisionType
     * dla dzia��w b�d�cych swimlane'ami (rolami dla proces�w z zewn�trznego Workflow).
     */
    public static final String TYPE_SWIMLANE = DivisionType.SWIMPLANE.getId();
    
    public static final String GUID_PREFIX ="GUID_";
    /**
     * Guid dzia�u g��wnego.
     */
    public static final String ROOT_GUID = "rootdivision";
    public static final String UP = "Uprawnienia";
    
    protected static final String CODE_AS_NAME_PREFIX_AVAILABLE_NAME = "division.code_as_name_prefix";
    
    public abstract void setParent(DSDivision division);
    public abstract void setHidden(boolean hidden);

    public abstract boolean isHidden();
    public abstract String getGuid();
    public abstract void setGuid(String guid);
    public abstract String getName();
    public abstract Long getId();
    public abstract void setName(String name);
    public abstract String getCode();
    public abstract void setCode(String code);
    public abstract void setExternalId(String externalId);
    public abstract String getExternalId();
    public abstract void setDescription(String description);
    public abstract String getDescription();
    public abstract DSDivision getParent() throws DivisionNotFoundException,ReferenceToHimselfException, EdmException;
    public abstract Optional<DivisionImpl> getParentSafe();
    //public abstract void setParent(DSDivision division) throws EdmException;
    public abstract DSDivision[] getChildren() throws EdmException;
    public abstract DSDivision[] getAncestors() throws EdmException;
    public abstract Long[] getAncestorsIds() throws EdmException;
	public abstract  DSDivision[] getAncestorssWithoutPositionAndGroup() throws EdmException;

    public abstract boolean isRoot();
    public abstract String getDivisionType();
    public abstract void setDivisionType(String divisionType);
    /**
     * Dodaje u�ytkownika do dzia�u.
     * @return True, je�eli u�ytkownik zosta� dodany, false w przeciwnym wypadku
     *  (je�eli ju� znajdowa� si� w tej ga��zi).
     */
    protected abstract boolean addUserSpi(DSUser user) throws EdmException;
    protected abstract boolean addUserSpi(DSUser user,boolean backup) throws EdmException;
    protected abstract boolean addUserSpi(DSUser user,boolean backup, String source) throws EdmException;
    /**
     * Usuwa u�ytkownika z dzia�u.
     * @return True, je�eli u�ytkownik zosta� usuni�ty, false w przeciwnym wypadku
     *   (je�eli nie by�o go w tym dziale).
     */ 
    protected abstract boolean removeUserSpi(DSUser user) throws EdmException;
    protected abstract boolean removeUserSpi(DSUser user, String source) throws EdmException;
    public abstract DSDivision createDivision(String name, String code) throws EdmException;
    public abstract DSDivision createDivisionWithExternal(String name, String code, String externalId) throws EdmException;
    public abstract DSDivision createDivision(String name,String code, String guid) throws EdmException;
    public abstract DSDivision createGroup(String name) throws EdmException;
    public abstract DSDivision createGroup(String name, String guid) throws EdmException;
    public abstract DSDivision createPosition(String name) throws EdmException;
    public abstract DSDivision createSwimlane(String name) throws EdmException;
    /**
     * Zwraca list� nieusuni�tych u�ytkownik�w znajduj�cych si� w tym dziale oraz
     * dodatkowo u�ytkownik�w z dzia��w-dzieci, kt�re s� stanowiskami(isPosition() == true).
     * @return
     * @throws EdmException
     */
    public abstract DSUser[] getUsers() throws EdmException;
    public abstract DSUser[] getUsersWithDeleted(Boolean withDeleted) throws EdmException;
    /**
     * Zwraca list� nieusuni�tych u�ytkownik�w znajduj�cych si� w tym dziale.
     * @return
     * @throws EdmException
     */
    public abstract DSUser[] getOriginalUsers() throws EdmException;
    public abstract DSUser[] getOriginalBackupUsers() throws EdmException;
    public abstract DSUser[] getOriginalUsersWithoutBackup() throws EdmException;
    
    /**
     * Zwraca list� wszystkich u�ytkownik�w znajduj�cych si� w tym dziale, w��cznie
     * z usuni�tymi i systemowymi oraz dodatkowo 
     * u�ytkownik�w z dzia��w-dzieci, kt�re s� stanowiskami(isPosition() == true).
     * Zwracana lista mo�e by� pusta.
     * @throws EdmException
     */
    protected abstract DSUser[] getAllUsers() throws EdmException;
    /**
     * Zwraca list� wszystkich u�ytkownik�w znajduj�cych si� w tym dziale, w��cznie
     * z usuni�tymi i systemowymi.  Zwracana lista mo�e by� pusta.
     * @throws EdmException
     */
    protected abstract DSUser[] getAllOriginalUsers() throws EdmException;
    
    public abstract void update() throws EdmException;

    public abstract boolean equals(Object object);
    public abstract int hashCode();

    /**
     * sprawdza czy w strukturze istnieje jego parent i jeszcze dalej parent parenta,
     * czyli czy nie jest rootem lub bezposrednio synem roota
     * @return
     * @throws DivisionNotFoundException
     * @throws ReferenceToHimselfException
     * @throws EdmException
     */
    public boolean isGrandsonInTree() throws DivisionNotFoundException,ReferenceToHimselfException, EdmException
    {
    	return (this.getParent() != null && this.getParent().getParent() != null);
    }
    
    /**
     * Zwraca �cie�k� do dzia�u, napis "Brak dzia�u: GUID", je�eli
     * dzia� nie zostanie odnaleziony, pusty napis je�eli parametr
     * guid jest r�wny null.
     */
    public static String safeGetPrettyPath(String guid) throws EdmException
    {
        if (guid == null)
            return "";
        try
        {
            return DSDivision.find(guid).getPrettyPath();
        }
        catch (DivisionNotFoundException e)
        {
            return "Brak dzia�u: "+guid;
        }
    }

    public static String safeGetName(String guid) throws EdmException
    {
        if (guid == null)
            return "";
        try
        {
            return DSDivision.find(guid).getName();
        }
        catch (DivisionNotFoundException e)
        {
            return "Brak dzia�u: "+guid;
        }
    }

    public static DSDivision find(String guid) throws EdmException, DivisionNotFoundException
    {
        return UserFactory.getInstance().findDivision(guid);
    }

    public static DSDivision find(int id) throws EdmException, DivisionNotFoundException {
        return UserFactory.getInstance().findDivisionById(id);
    }

    public static DSDivision findById(long id) throws EdmException, DivisionNotFoundException {
        return UserFactory.getInstance().findDivisionById(id);
    }

    public static DSDivision findByName(String name) throws EdmException, DivisionNotFoundException
    {
        return UserFactory.getInstance().findDivisionByName(name);
    }
    
    public static List<DSDivision> findByDescription(String desc) throws Exception
    {
        return UserFactory.getInstance().findDivisionsByDescription(desc);
    }
    
    public static List<DSDivision> findByLikeName(String desc) throws Exception
    {
        return UserFactory.getInstance().findDivisionsByLikeName(desc);
    }
    
    public static List<DSDivision> findByLikeCode(String desc) throws Exception
    {
        return UserFactory.getInstance().findDivisionsByLikeCode(desc);
    }
    
    public static List<DSDivision> findDivisionsdByCode(String code) throws EdmException {
    	return UserFactory.getInstance().findDivisionListByCode(code);
    }
    
    public static DSDivision findByExternalId(String externalId) throws EdmException, DivisionNotFoundException
    {
        return UserFactory.getInstance().findDivisionByExternalId(externalId);
    }
    
    public static DSDivision findByNameAndParentId(String name, DSDivision division) throws EdmException, DivisionNotFoundException
    {
        return UserFactory.getInstance().findByNameAndParentId(name, division);
    }
    
    public static DSDivision findByCode(String code) throws EdmException {
    	return UserFactory.getInstance().findDivisionByCode(code);
    }

    

    /**
     * Zwraca tablic� wszystkich dzia��w.
     */
    public static DSDivision[] getAllDivisions() throws EdmException
    {
        return UserFactory.getInstance().getDivisionsSpi();
    }
    
    /**
     * Zwraca tablic� wszystkich dzia��w z sortowaniem malejacym.
     */
    public static DSDivision[] getAllDivisionsDesc() throws EdmException
    {
        return UserFactory.getInstance().getDivisionsDesc();
    }
    
    /**
     * Zwraca tablic� zawieraj�c� tyko dzia�y (TYPE_DIVISION) i stanowiska (TYPE_POSITION).
     */
    public static DSDivision[] getOnlyDivisionsAndPositions() throws EdmException
    {
        return UserFactory.getInstance().getOnlyDivisionsAndPositions();
    }
    
    /**
     * Zwraca tablic� zawieraj�c� tyko dzia�y (TYPE_DIVISION) i stanowiska (TYPE_POSITION).
	 * @param b - czy pobrac z ukrytymi
     */
	public static DSDivision[] getOnlyDivisionsAndPositions(boolean b) throws EdmException {
		return UserFactory.getInstance().getOnlyDivisionsAndPositions(b);
	}
	/**
     * Zwraca tablic� zawieraj�c� tyko dzia�y (TYPE_DIVISION).
	 * @param hidden - czy pobrac z ukrytymi
     */
	public static DSDivision[] getOnlyDivisions(boolean hidden) throws EdmException {
		return UserFactory.getInstance().getOnlyDivisions(hidden);
	}

    /**
     * Zwraca tablic� tylko tych dzia��w, kt�r� s� "grupami".
     */
    public static DSDivision[] getAllGroups() throws EdmException
    {
        return UserFactory.getInstance().getGroupsSpi();
    }
    
    public static List<DSUser> getUsersFromDivisionAsList(DSDivision[] dsd) throws EdmException
    {
    	List<DSUser> listaUsrPom = new ArrayList<DSUser>();

        for (int i = 0; i < dsd.length; i++) {
        	SearchResults<? extends DSUser> results = DSUser.search(0, 0, DSUser.SORT_FIRSTNAME_LASTNAME, true, null);
        	List<? extends DSUser> listaUsr = fun.list(results);

			if(dsd[i].isDivision() && !dsd[i].isRoot()){
				for (int k=0; k < listaUsr.size(); k++) {
					boolean znalazl = false;
					DSUser user = listaUsr.get(k);
					DSDivision[] pom = user.getDivisions();
					for (int j = 0; j < pom.length; j++) {
						if(dsd[i].equals(pom[j]))
							znalazl = true;
					}
					if(!znalazl)
					{
						listaUsr.remove(k);
						k--;
					}
				}
				for (Iterator iterator = listaUsr.iterator(); iterator.hasNext();) {
					DSUser user = (DSUser) iterator.next();
					if(!listaUsrPom.contains(user) && !user.equals(DSApi.context().getDSUser())){
						listaUsrPom.add(user);
					}
				}
			}
		}
        return listaUsrPom;
    }
    public DSUser[] getUsersFromDivision(DSDivision[] dsd) throws EdmException
    {
    	List<DSUser> listaUsrPom = new ArrayList<DSUser>();

        for (int i = 0; i < dsd.length; i++) {
        	SearchResults<? extends DSUser> results = DSUser.search(0, 0, DSUser.SORT_FIRSTNAME_LASTNAME, true, null);
        	List<? extends DSUser> listaUsr = fun.list(results);

			if(dsd[i].isDivision() && !dsd[i].isRoot()){
				for (int k=0; k < listaUsr.size(); k++) {
					boolean znalazl = false;
					DSUser user = listaUsr.get(k);
					DSDivision[] pom = user.getDivisions();
					for (int j = 0; j < pom.length; j++) {
						if(dsd[i].equals(pom[j]))
							znalazl = true;
					}
					if(!znalazl)
					{
						listaUsr.remove(k);
						k--;
					}
				}
				for (Iterator iterator = listaUsr.iterator(); iterator.hasNext();) {
					DSUser user = (DSUser) iterator.next();
					if(!listaUsrPom.contains(user) && !user.equals(DSApi.context().getDSUser())){
						listaUsrPom.add(user);
					}
				}
			}
		}
        return (DSUser[]) listaUsrPom.toArray(new DSUser[listaUsrPom.size()]);
    }

    /**
     * @see getUsers()
     * @return
     * @throws EdmException
     */
    public boolean hasUsers() throws EdmException
    {
        return getUsers() != null && getUsers().length > 0;
    }
    /**
     * Zwraca list� nieusuni�tych u�ytkownik�w znajduj�cych si� w tym dziale oraz
     * dodatkowo u�ytkownik�w z dzia��w-dzieci, kt�re s� stanowiskami(isPosition() == true). Je�li subdivisions=true 
     * zwraca r�wnie� user�w z dzia��w podrzednych.
     * @param subdivisions
     * @return
     * @throws EdmException
     */
    public DSUser[] getUsers(boolean subdivisions) throws EdmException
    {
        if (!subdivisions)
            return getUsers();

        if (isRoot())
        {
            List<DSUser> allUsers = DSUser.list(DSUser.SORT_NONE);
            return allUsers.toArray(new DSUser[allUsers.size()]);
        }
        else
        {
            List<DSUser> users = new LinkedList<DSUser>(Arrays.asList(getUsers()));
            List<DSDivision> divisions = new LinkedList<DSDivision>(Arrays.asList(getChildren()));
           
            int ptr = 0;
            while (ptr < divisions.size())
            {
                DSDivision division = ((DSDivision) divisions.get(ptr++));
                DSUser[] divisionUsers = division.getUsers();
                for (int i=0; i < divisionUsers.length; i++)
                {
                	
                    if (!users.contains(divisionUsers[i]))
                        users.add(divisionUsers[i]);
                }
                divisions.addAll(Arrays.asList(division.getChildren()));
            }

            return (DSUser[]) users.toArray(new DSUser[users.size()]);
        }
    }

    private DSDivision[] children_DIVISION;
    private DSDivision[] children_GROUP;
    private DSDivision[] children_POSITION;

    public DSDivision[] getChildren(String type) throws EdmException
    {
        DSDivision[] children = getChildren();

        if ((TYPE_DIVISION.equals(type) && children_DIVISION == null) ||
            (TYPE_GROUP.equals(type) && children_GROUP == null) ||
            (TYPE_POSITION.equals(type) && children_POSITION == null))
        {
            DSDivision[] tmp = new DSDivision[children.length];
            int j = 0;
            for (int i=0; i < children.length; i++)
            {
                if ((TYPE_DIVISION.equals(type) && children[i].isDivision()) ||
                    (TYPE_GROUP.equals(type) && children[i].isGroup()) ||
                    (TYPE_POSITION.equals(type) && children[i].isPosition()))
                {
                    tmp[j++] = children[i];
                }
            }

            DSDivision[] cache = new DSDivision[j];
            System.arraycopy(tmp, 0, cache, 0, j);

            if (TYPE_DIVISION.equals(type)) children_DIVISION = cache;
            else if (TYPE_GROUP.equals(type)) children_GROUP = cache;
            else if (TYPE_POSITION.equals(type)) children_POSITION = cache;
        }

        if (TYPE_DIVISION.equals(type)) return children_DIVISION;
        else if (TYPE_GROUP.equals(type)) return children_GROUP;
        else if (TYPE_POSITION.equals(type)) return children_POSITION;
        else throw new IllegalArgumentException("Nieznany rodzaj dzia�u: "+type);
    }

    public DSDivision[] getChildren(DSDivisionFilter filter) throws EdmException
    {
        DSDivision[] children = getChildren();
        List<DSDivision> filtered = new ArrayList<DSDivision>(children.length);
        for (int i=0; i < children.length; i++)
        {
            if (filter.accept(children[i]))
                filtered.add(children[i]);
        }
        return filtered.toArray(new DSDivision[filtered.size()]);
    }

    public String getPrettyPath() throws EdmException
    {
        return getPrettyPath(" / ");
    }

    public String getPrettyPath(String separator) throws EdmException
    {
        StringBuilder sb = new StringBuilder();
        List<DSDivision> lstPath = getPath();

        for (int i=0, n=lstPath.size(); i < n; i++)
        {
            sb.append(((DSDivision) lstPath.get(i)).getName());
            if (i < n-1)
                sb.append(separator);
        }

        return sb.toString();
    }

    private List<DSDivision> path;

    private List getPath() throws EdmException {

        if (path == null)
        {
            path = new LinkedList<DSDivision>();
            DSDivision division = this;
            do
            {
                path.add(division);
                if(!division.isHidden()){
                    division = division.getParent();
                } else {
                    break;
                }
                
            }
            while (division != null && (division.getParent() == null || !division.getGuid().equals(division.getParent().getGuid()))); // bo usuni�te dzia�y wskazuj� parent-em na siebie

            Collections.reverse(path);
        }

        return path;
    }

    public boolean isDivision()
    {
        return TYPE_DIVISION.equals(getDivisionType());
    }

    public boolean isGroup()
    {
        return TYPE_GROUP.equals(getDivisionType());
    }

    public boolean isPosition()
    {
        return TYPE_POSITION.equals(getDivisionType());
    }

    public boolean isSwimlane()
    {
        return TYPE_SWIMLANE.equals(getDivisionType());
    }    
    
    public static boolean isValidType(String type)
    {
        return TYPE_DIVISION.equals(type) || TYPE_GROUP.equals(type) || TYPE_POSITION.equals(type);
    }

    /**
     * Dodaje u�ytkownika do dzia�u.
     * @return True, je�eli u�ytkownik zosta� dodany, false w przeciwnym wypadku
     *  (je�eli ju� znajdowa� si� w tej ga��zi).
     */
    public final boolean addUser(DSUser user) throws EdmException
    {
        boolean isUserAdded = addUserSpi(user);
        if (isUserAdded){
            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
			DataMartEventBuilder.get()
				.event(EventType.DIVISION_USER_ADD)
				.objectId(this.getGuid())
				.valueChange(null, null, user.getName());
        }        
        if(Configuration.coreOfficeAvailable())
        	WorkflowFactory.addParticipantToUsernameMapping(null, null,"ds_guid_"+this.getGuid(), user.getName());

        return isUserAdded;
    }
    
    /**
     * Dodaje u�ytkownika do dzia�u.
     * @return True, je�eli u�ytkownik zosta� dodany, false w przeciwnym wypadku
     *  (je�eli ju� znajdowa� si� w tej ga��zi).
     */
    public final boolean addUser(DSUser user,String source) throws EdmException
    {
        boolean isUserAdded = addUserSpi(user,false,source);
        if (isUserAdded){
            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
			DataMartEventBuilder.get()
				.event(EventType.DIVISION_USER_ADD)
				.objectId(this.getGuid())
				.valueChange(null, null, user.getName());
        }        
        if(Configuration.coreOfficeAvailable())
        	WorkflowFactory.addParticipantToUsernameMapping(null, null,"ds_guid_"+this.getGuid(), user.getName());

        return isUserAdded;
    }
    
    public final boolean addUser(DSUser user, boolean backup) throws EdmException
    {
        boolean isUserAdded = addUserSpi(user,backup);
        if (isUserAdded)
        {
            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
			DataMartEventBuilder.get()
				.event(EventType.DIVISION_USER_ADD)
				.objectId(this.getGuid())
				.valueChange(null, null, user.getName());
        }
        if(!backup){
            if(Configuration.coreOfficeAvailable()){
                WorkflowFactory.addParticipantToUsernameMapping(null, null,"ds_guid_"+this.getGuid(), user.getName());
            }
        }
        return isUserAdded;
    }

    /**
     * Usuwa u�ytkownika z dzia�u.
     * @return True, je�eli u�ytkownik zosta� usuni�ty, false w przeciwnym wypadku
     *   (je�eli nie by�o go w tym dziale).
     */
    public final boolean removeUser(DSUser user) throws EdmException
    {
        boolean isUserRemoved = removeUserSpi(user);
        if (isUserRemoved)
        {
            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
			DataMartEventBuilder.get()
				.event(EventType.DIVISION_USER_REMOVE)
				.objectId(this.getGuid())
				.valueChange(null, user.getName(), null);
        }
        return isUserRemoved;
    }
    
    /**
     * Usuwa u�ytkownika z dzia�u.
     * @return True, je�eli u�ytkownik zosta� usuni�ty, false w przeciwnym wypadku
     *   (je�eli nie by�o go w tym dziale).
     */
    public final boolean removeUser(DSUser user,String source) throws EdmException
    {
        boolean isUserRemoved = removeUserSpi(user,source);
        if (isUserRemoved)
        {
            PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            cache.invalidate(user);
			DataMartEventBuilder.get()
				.event(EventType.DIVISION_USER_REMOVE)
				.objectId(this.getGuid())
				.valueChange(null, user.getName(), null);
        }
        return isUserRemoved;
    }

    /**Sprawdza czy zalogowany urzytkownik znajduje si� w dziale albo zast�puje uzytkownika z tego dzialu*/
    public static Boolean isInDivision(DSDivision division) throws EdmException
    {
    	return isInDivision(division, DSApi.context().getPrincipalName());
    }

	public static boolean isInDivision(String guid, String username) throws EdmException{
		DSDivision div = DSDivision.find(guid);
		return div != null && isInDivision(div, username);
	}

	public static boolean isInDivision(String guid) throws EdmException{
		return isInDivision(guid, DSApi.context().getPrincipalName());
	}
	 public static Boolean isInDivision(DSDivision division, String username) throws EdmException
	 {
		 return isInDivision(division, username, false);
	 }
    
    public static Boolean isInDivision(DSDivision division, String username, boolean subdivisions) throws EdmException
    {
    	DSUser[] users = division.getUsers(subdivisions);
    	DSUser user = DSUser.findByUsername(username);
    	String principal = user.getName();
    	for (int i = 0; i < users.length; i++) 
    	{
			if(users[i].getName().equals(principal))
			{
				return true;
			}
			else
			{
				DSUser[] us = user.getSubstituted();
				for (int j = 0; j < us.length; j++)
				{
					if(users[i].getName().equals(us[j].getName()))
					{
						return true;
					}
				}
			}
		}
    	return false;
    }
    
    public static Boolean isInDivisionAsBackup(DSDivision division,String user) throws EdmException
    {
    	DSUser[] users = division.getOriginalBackupUsers();
    	for (int i = 0; i < users.length; i++) 
    	{
			if(users[i].getName().equals(user))
			{
				return true;
			}
		}
    	return false;
    }
    
    /**
     * Sprawdza, czy u�ytkownik nale�y do dzia�u (lub poddzia�u, lub podpoddzia�u...),
     * przy tym, �e omijane s� dzia�y-grupy
     * @param user
     * @return true, je�li u�ytkownik znajduje si� w jakiejkolwiek ga��zi dzia�u
     * @throws EdmException
     */
    public Boolean isInAnyChildDivision(DSUser user) throws EdmException {
    	return isInAnyChildDivision(user, null);
    }
    
    public Boolean isInAnyChildDivision(DSDivision div) throws EdmException {
		return isInAnyChildDivision(null, new DSDivision[]{div});
    }
    
    public Boolean isInAnyChildDivision(DSUser user, DSDivision[] divs) throws EdmException {
    	if (user != null) {
    		divs = user.getDivisions();
    	}
    	
    	for (DSDivision div : divs) {
    		if (!div.isGroup() && this.equals(div)) {
    			return true;
    		}
    	}
    	
		for (DSDivision div : divs) {
			if (!div.isGroup()) {
				try {
					while ((div = div.getParent()) != null) {
						if (!div.isGroup() && this.equals(div)) {
							return true;
						}
					}
				} catch (ReferenceToHimselfException e) {
					// raczej nie mo�liwe, ewentualnie to b�dzie root
				}
			}
		}
    	
    	return false;
    }
    
    public static final Comparator<DSDivision> NAME_COMPARTOR = new Comparator() 
    {
    	public int compare(Object arg1, Object arg2) {
    		return ((DSDivision)arg1).getName().compareToIgnoreCase(((DSDivision)arg2).getName());
    	}
    };
    
    public abstract String[] getUserSources(DSUser user);
    
    public static class DivisionComparatorAsName implements Comparator<DSDivision>
    {
        public int compare(DSDivision e1, DSDivision e2)
        {
            return e1.getName().compareTo(e2.getName());
        }
    }
    
    public static class UserComparatorAsLastname implements Comparator<DSUser>
    {
        public int compare(DSUser e1, DSUser e2)
        {
            return e1.getLastname().compareTo(e2.getLastname());
        }
    }
    
    public String asNameParentName() throws DivisionNotFoundException, ReferenceToHimselfException, EdmException
    {
    	if(TYPE_POSITION.equals(this.getDivisionType()))
    	{
    		return this.getName()+"("+this.getParent().getName()+")";
    	}
    	else
    	{
    		return this.getName();
    	}
    }
    
    protected String getLdifDn() throws EdmException
    {
    	StringBuilder sb = new StringBuilder();
    	if(this.isRoot())
    	{
    		sb.append("dc=").append(this.getName()).append(",").append("dc=com");
    	}
    	else
    	{
    		sb.append(this.isDivision() ? "ou=" : "cn=").append(this.getName()).append(",").append(this.getParent().getLdifDn());
    	}
    	return sb.toString();
    }
    
    protected String getLdifBlock() throws EdmException
	{
    	StringBuilder sb = new StringBuilder();
    	sb.append("dn: ").append(this.getLdifDn()).append("\n");
    	
    	if(this.isRoot())
    	{
    		sb.append("dc: ").append(this.getName()).append("\n");
    		sb.append("docusafe_id: ").append(this.getName()).append("\n");
    		sb.append("o: ").append(this.getName()).append("\n");
    		sb.append("objectClass: organization\n");
    		sb.append("objectClass: dcObject\n");
    	}
    	else if(this.isDivision())
    	{
    		sb.append("docusafe_id: ").append(this.getName()).append("\n");
    		sb.append("ou: ").append(this.getName()).append("\n");
    		sb.append("objectClass: organizationalUnit\n");
    		sb.append("objectClass: top\n");
    	}
    	else 
    	{
    		sb.append("cn: ").append(this.getName()).append("\n");
    		sb.append("docusafe_id: ").append(this.getName()).append("\n");    		
    		sb.append("objectClass: groupOfNames\n");
    		sb.append("objectClass: top\n");
    	}
    	return sb.toString();
	}
    
    public String getLdif() throws EdmException
    {
    	
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(this.getLdifBlock());
    	
    	for(DSDivision d : this.getChildren())
    	{
    		if(!d.isHidden())
    		{
    			sb.append("\n");
    			sb.append(d.getLdif());
    		}
    	}
    	
    	for(DSUser u : this.getAllUsers())
    	{
    		sb.append("\n");
    		sb.append(u.getLdifBlock(this));
    	}
    	
    	return sb.toString();
    }
    
    public static boolean isCodeAsNamePrefix() {
    	return AvailabilityManager.isAvailable(CODE_AS_NAME_PREFIX_AVAILABLE_NAME) != null ? AvailabilityManager.isAvailable(CODE_AS_NAME_PREFIX_AVAILABLE_NAME) : false;
    }

    /*public String getLdifBlock(DSDivision parent)
    {
    	StringBuffer sb = new StringBuffer();
    	if(this.isRoot())
    	{
    		sb.append("dn: ");
    		sb.append("dc=").append(this.getName());
    		sb.append(",dc=com");
    		sb.append("\n");
    		sb.append("dc: ").append(this.getName());
    		sb.append("\n");
    		sb.append("docusafe_id: ").append(this.getGuid());
    		sb.append("\n");
    		sb.append("o: ").append(this.getName());
    		sb.append("\n");
    		sb.append("objectClass: organization");
    		sb.append("\n");
    		sb.append("objectClass: dcObject");
    		sb.append("\n");
    	}
    	else
    	{
    		
    	}
    	return sb.toString();
    }*/
   
    /**
	 * Metoda zwraca wszystkie DSDivision, kt�re maj� ustawiony externalId
	 * @return
	 * @throws DivisionNotFoundException
	 * @throws EdmException
	 */
	public static List<DSDivision> findDivisionWithExternalId()
			throws DivisionNotFoundException, EdmException {
		try {
			Criteria criteria = DSApi.context().session()
					.createCriteria(DivisionImpl.class);
			criteria.add(Restrictions.isNotNull("externalId"));
			List list = criteria.list();
			return list;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    /**
     * Sprawdza czy guidDivision zawiera si� w przekazanej Tablicy
     * @param guidDivision
     * @param divisions
     * @return
     */
    public static boolean containsDivisionGuid(String guidDivision, DSDivision[] divisions) {
        boolean ok = false;
        if(StringUtils.isEmpty(guidDivision)){
            return ok;
        }
        if (divisions.length > 0) {
            for (int i = 0; i < divisions.length; i++) {
                if (divisions[i].getGuid().equals(guidDivision)) {
                    ok = true;
                    break;
                }
            }
        }
        return ok;
    }
    
}
