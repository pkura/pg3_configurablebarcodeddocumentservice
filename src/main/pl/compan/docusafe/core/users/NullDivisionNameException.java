package pl.compan.docusafe.core.users;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NullDivisionNameException.java,v 1.1 2004/05/16 20:10:35 administrator Exp $
 */
public class NullDivisionNameException extends EdmException
{
    public NullDivisionNameException(String message)
    {
        super(message);
    }

    public NullDivisionNameException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NullDivisionNameException(Throwable cause)
    {
        super(cause);
    }
}
