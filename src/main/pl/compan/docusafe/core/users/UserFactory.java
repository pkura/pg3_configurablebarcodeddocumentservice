package pl.compan.docusafe.core.users;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.io.XMLWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.asprise.util.tiff.ab;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.AnyResource;
import pl.compan.docusafe.core.calendar.sql.UserCalendarBinder;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.jackrabbit.JackrabbitUsers;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.CollectiveAssignments;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.users.sql.SqlUserFactory;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.parametrization.utp.internalSignature.InternalSignature;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.util.ContextLocale;
import pl.compan.docusafe.util.SortUtils;
import pl.compan.docusafe.util.StringManager;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserFactory.java,v 1.57 2010/08/04 06:59:25 mariuszk Exp $
 */
public abstract class UserFactory
{
	static StringManager sm = GlobalPreferences.loadPropertiesFile(UserFactory.class.getPackage().getName(), null);
	
    private static final Log log = LogFactory.getLog(UserFactory.class);

    static UserFactory factory;

    public synchronized static void setFactory(String name)
    {
        if (SqlUserFactory.NAME.equals(name))
        {
            factory = new SqlUserFactory();
        }
        else
        {
            throw new IllegalArgumentException("Nieznana warto�� UserFactory: "+name);
        }
    }

    public static UserFactory getInstance()
    {
        return factory;
    }

    public abstract boolean isLdap();

    public abstract int getUserCount() throws EdmException;

    /**
     * Znajduje u�ytkownika na podstawie jego nazwy.
     * @throws UserNotFoundException Je�eli u�ytkownik o takiej nazwie
     *  nie istnieje.
     */
    public abstract DSUser findByUsernameSpi(String username)
        throws EdmException, UserNotFoundException;
        
    public abstract DSUser findByExtensionSpi(String username)
        throws EdmException, UserNotFoundException;
    
    public abstract DSUser findByIdentifierSpi(String identifier)
            throws EdmException, UserNotFoundException;
    
    public abstract DSUser findByExternalNameSpi(String username)
    throws EdmException, UserNotFoundException;

    /**
     * Znajduje u�ytkownika na podstawie jego adresu email.
     * @throws UserNotFoundException Je�eli u�ytkownik o takiej nazwie
     *  nie istnieje.
     */
    public abstract DSUser findByEmailSpi(String email)
        throws EdmException, UserNotFoundException;
    

    public abstract DSUser findByCertificateSpi(X509Certificate certificate)
        throws EdmException, UserNotFoundException;

    public abstract DSUser findByIdSpi(Long certificate)
    	throws EdmException, UserNotFoundException;
    
    /**
     * Znajduje dzia� o danym guid.  Implementacja powina rzuci� wyj�tek
     * DivisionNotFoundException, je�eli taki dzia� nie istnieje.
     */
    public abstract DSDivision findDivisionSpi(String guid)
        throws DivisionNotFoundException, EdmException;
    
    /**
     * Znajduje dzia� o danym guid.  Implementacja powina rzuci� wyj�tek
     * DivisionNotFoundException, je�eli taki dzia� nie istnieje.
     */
    public abstract DSDivision findDivisionByNameSpi(String name)
        throws DivisionNotFoundException, EdmException;
    
    public abstract List findDivisionsByDescriptionSpi(String desc) throws Exception;
    
    public abstract List findDivisionsByLikeNameSpi(String desc) throws Exception;
    public abstract List findDivisionsByLikeCodeSpi(String code) throws Exception;
    public abstract List findDivisionListByCodeSpi(String code) throws EdmException;
    
    public abstract DSDivision findDivisionByCodeSpi(String code) throws EdmException;


    public abstract DSDivision findDivisionByExternalIdSpi(String externalId)
    	throws DivisionNotFoundException, EdmException;
    
    public abstract DSDivision findByNameAndParentIdSpi(String externalId, DSDivision division)
	throws DivisionNotFoundException, EdmException;
    
    public abstract DSDivision findDivisionById(long id)
    	throws DivisionNotFoundException, EdmException;
    /**
     * Wyszukuje u�ytkownik�w.
     * @param offset Liczba pocz�tkowych wynik�w, kt�re nale�y pomin��.
     * @param limit Maksymalna liczba wynik�w, kt�re nale�y zwr�ci�. 0 oznacza
     *  wszystkie.
     * @param sortMode Jedna ze sta�ych DSUser.SORT_*
     * @param ascending True - sortowanie rosn�ce.
     * @param query Ci�g, od kt�rego powinna zaczyna� si� nazwa u�ytkownika lub
     *  jego nazwisko. Je�eli null, nie jest brany pod uwag�.
     * @param deleted 
     * @return
     * @throws EdmException
     */
    public abstract SearchResults<DSUser> searchUsersSpi(int offset, int limit, int sortMode,
                                                         boolean ascending, String query, boolean deleted)
        throws EdmException;

    /**
     * Tworzy u�ytkownika.  Implementacja mo�e za�o�y�, �e u�ytkownik o tej
     * nazwie nie istnieje, a wszystkie parametry b�d� r�ne od null.
     */
    protected abstract DSUser createUserSpi(String username, String firstname,
                                            String lastname, boolean system)
        throws EdmException;

    /**
     * Usuwa u�ytkownika.  Implementacja nie musi sprawdza�, czy jest to
     * bezpieczne.
     */
    protected abstract void deleteUserSpi(String username) throws EdmException;

    /**
     * Przywraca u�ytkownika.  Implementacja nie musi sprawdza�, czy jest to
     * bezpieczne.
     */
    protected abstract void revertUserSpi(String username) throws EdmException;
    
    /**
     * Usuwa dzia�.  Implementacja nie musi sprawdza�, czy usuni�cie dzia�u
     * jest bezpieczne i mo�e za�o�y�, �e dzia� nie ma dzia��w podrz�dnych.
     */
    protected abstract void deleteDivisionSpi(DSDivision division) throws EdmException;

    /**
     * Zwraca list� grup.  Zwracane s� wszystkie grupy niezale�nie od poziomu
     * zag��bienia.
     */
    protected abstract DSDivision[] getGroupsSpi() throws EdmException;

    protected abstract DSUser[] getUsersSpi() throws EdmException;

    protected abstract DSDivision[] getDivisionsSpi() throws EdmException;
    protected abstract DSDivision[] getDivisionsDesc() throws EdmException;
    protected abstract List<DSDivision> getDivisionsDivisionOrGroupType() throws EdmException;
    protected abstract List<DSDivision> getDivisionsDivisionType() throws EdmException;
    protected abstract DSDivision[] getOnlyDivisionsAndPositions() throws EdmException;
    protected abstract DSDivision[] getOnlyDivisionsAndPositions(boolean b) throws EdmException;
    protected abstract DSDivision[] getOnlyDivisions(boolean b) throws EdmException;

    public final SearchResults<DSUser> searchUsers(int offset, int limit, int sortMode,
                                                   boolean ascending, String query,boolean deleted)
        throws EdmException
    {
        if (offset < 0) offset = 0;
        if (limit < 0) limit = 0;
        if (sortMode != DSUser.SORT_FIRSTNAME_LASTNAME &&
            sortMode != DSUser.SORT_LASTNAME_FIRSTNAME &&
            sortMode != DSUser.SORT_NAME &&
            sortMode != DSUser.SORT_NONE)
            sortMode = DSUser.SORT_NONE;

        return searchUsersSpi(offset, limit, sortMode, ascending, query,deleted);
    }
    
    public abstract SearchResults<DSUser> searchUsers(int offset, int limit, int sortMode, boolean ascending, 
    		String query, UserSearchBean bean,boolean deleted,boolean onlyDeleted,Boolean loginDisabled) throws EdmException;
    
    
    public final DSDivision findDivision(String guid)
        throws DivisionNotFoundException, EdmException
    {
        if (guid == null)
            throw new NullPointerException("guid");

        return findDivisionSpi(guid.trim());
    }
    
    public final DSDivision findDivisionByName(String name)
    throws DivisionNotFoundException, EdmException
	{
	    if (name == null)
	        throw new NullPointerException("guid");

	    return findDivisionByNameSpi(name.trim());
	}
    
    public final List findDivisionsByDescription(String desc) throws Exception
    {
	    if (desc == null)
	        throw new NullPointerException("desc");

	    return findDivisionsByDescriptionSpi(desc.trim());
	}
    
    public final List findDivisionsByLikeName(String desc) throws Exception
    {
	    if (desc == null)
	        throw new NullPointerException("desc");

	    return findDivisionsByLikeNameSpi(desc.trim());
	}
    public final List findDivisionsByLikeCode(String desc) throws Exception
    {
	    if (desc == null)
	        throw new NullPointerException("desc");

	    return findDivisionsByLikeCodeSpi(desc.trim());
	}
    
    public final DSDivision findDivisionByExternalId(String externalId)
    throws DivisionNotFoundException, EdmException
	{
	    if (externalId == null)
	        throw new NullPointerException("externalId");

	    return findDivisionByExternalIdSpi(externalId.trim());
	}
    
    public final DSDivision findByNameAndParentId(String externalId, DSDivision division)
    throws DivisionNotFoundException, EdmException
	{
	    if (externalId == null)
	        throw new NullPointerException("externalId");
	    if (externalId == null)
	        throw new NullPointerException("division");

	    return findByNameAndParentIdSpi(externalId.trim(),division);
	}
    
    public final DSDivision findDivisionByCode(String code) throws EdmException {
    	if (code == null) throw new NullPointerException("guid");
    	
    	return findDivisionByCodeSpi(code.trim());
    }
    
    public final List findDivisionListByCode(String code) throws EdmException {
    	if (code == null) throw new NullPointerException("code");
    	
    	return findDivisionListByCodeSpi(code.trim());
    }

    public final DSUser findByUsername(String username)
        throws EdmException, UserNotFoundException
    {
        if (username == null)
            throw new NullPointerException("username");

//        if (user.isSystem())
//            throw new UserNotFoundException("Nie istnieje u�ytkownik "+username);
        return findByUsernameSpi(username.trim().toLowerCase());
    }
    
    public final DSUser findByExternalName(String username)
    throws EdmException, UserNotFoundException
	{
	    if (username == null)
	        throw new NullPointerException("username");

	    return findByExternalNameSpi(username.trim().toLowerCase());
	}
    

    public final DSUser findSystemUser(String username)
        throws EdmException, UserNotFoundException
    {
        if (username == null)
            throw new NullPointerException("username");

        DSUser user = findByUsernameSpi(username);
        if (!user.isSystem())
            throw new UserNotFoundException("Nie istnieje u�ytkownik systemowy o tej nazwie");

        return user;
    }
    
    public final DSUser findByExtension(String extension)
    throws EdmException, UserNotFoundException
	{
	    if (extension == null)
	        throw new NullPointerException("extension");
	
	    DSUser user = findByExtensionSpi(extension);
	    //bez sensu powinno byc na odwrot domyslnie zwraca nie systemowych 
	    if (!user.isSystem())
	        throw new UserNotFoundException("Nie istnieje u�ytkownik z podanym parametrem extension");
	
	    return user;
	}
    
    public final DSUser findAllByExtension(String extension)
    throws EdmException, UserNotFoundException
	{
	    if (extension == null)
	        throw new NullPointerException("extension");
	
	    DSUser user = findByExtensionSpi(extension);
	
	    return user;
	}
    
    public final DSUser findByIdentifier(String identifier)
    throws EdmException, UserNotFoundException
	{
	    if (identifier == null)
	        throw new NullPointerException("Identifier");
	
	    DSUser user = findByIdentifierSpi(identifier);
	
	    return user;
	}

    public final DSUser findByEmail(String email)
        throws EdmException, UserNotFoundException
    {
        if (email == null)
            throw new NullPointerException("email");

        return findByEmailSpi(email.trim().toLowerCase());
    }
    
    public final DSUser findById(Long id) throws EdmException, UserNotFoundException
    {
    	if (id == null)
    		throw new NullPointerException("id");
    	return findByIdSpi(id);
    }

    public final DSUser findByCertificate(X509Certificate certificate)
        throws EdmException, UserNotFoundException
    {
        if (certificate == null)
            throw new NullPointerException("certificate");

        return findByCertificateSpi(certificate);
    }

    /** ta metoda, wbrew nazwie, zwraca uzytkownikow ktorych zadania mozna podgladac (zalogowany uzytkownik)
     * i tym samym mozna ich zadania dekretowac na siebie (guzik assign me, z podsumowania i archiwizacji)
     */
    public final List<DSUser> getCanAssignUsers() throws UserNotFoundException, EdmException {
    	List<DSUser> result = new ArrayList<DSUser>(); 
    	Role rola = Role.findByName("archive_only");
    	if (DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
         {
             result =  DSUser.listCached(DSUser.SORT_LASTNAME_FIRSTNAME);
         }
         else if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD))
         {         	
         	ArrayList<DSDivision> divsList  = new ArrayList<DSDivision>();
         	List<DSDivision> divs = UserFactory.getInstance().getAllCanAssignDivisionsDivisionType();
         	List<DSUser> users = new ArrayList<DSUser>();
         	for (DSDivision div : divs) {
         		users.addAll(Arrays.asList(div.getUsers()));
			}
         
         	/*for(DSUser user:users)
         	{
         		if(!ret.contains(user) && !user.getRoles().contains("archive_only")){
         			ret.add(user);
         		}
         	}*/
        	//wywalamy powtarzaj�cych si� u�ytkownik�w
        	/*List<DSUser> ret = new ArrayList<DSUser>();
         	for (DSUser user : users)
			{ user.getRoles()
				List<Long> userRole = Role.listUserRoles(user.getName());
				if (!ret.contains(user) && rola != null && !userRole.contains(rola.getId()))
				{
					ret.add(user);
				}
			}*/
         	result =  users;            
         }
         else
         {
             List<DSUser> list = new ArrayList<DSUser>(1);
             list.add(DSApi.context().getDSUser());
             result = list;
         }
    	
		List<DSUser> result2 = new LinkedList<DSUser>();
		
		if (rola != null)
		{	
			for (DSUser user : result)
			{
				List<Long> userRole = Role.listUserRoles(user.getName());
				if (!userRole.contains(rola.getId()))
				{
					result2.add(user);
				}
			}

		} else
		{
			result2.addAll(result);
		}

    	
    	Collections.sort(result2, DSUser.LASTNAME_COMPARATOR);
    	return result2;
    }
    
    public final List<DSDivision> getCanAssignDivisions() throws EdmException {
   	 if (DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
        {
            return Arrays.asList(DSDivision.getAllDivisions());
        }
        else if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD))
        {
        	
        	DSDivision[] divs = DSApi.context().getDSUser().getSubordinateDivisions();
        	return Arrays.asList(divs);            
        } else {
        	return new LinkedList<DSDivision>();
        }
   }
    /**
     * 
     * @return Zwraca list� dzia��w oraz grup do kt�rych u�ytkownik posiada uprawnienia
     * @throws EdmException
     */
   public final List<DSDivision> getAllCanAssignDivisionsDivisionType() throws EdmException
   {
	   	 if (DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
	     {
	   		 return getDivisionsDivisionOrGroupType();   
	     }
	     else if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD))
	     {
             String userProfileKeys = Docusafe.getAdditionProperty("userProfileKeys");
	        	DSDivision[] divs = DSApi.context().getDSUser().getSubordinateDivisions();
	        	List<DSDivision> divisTemp = new LinkedList<DSDivision>();
	        	for(DSDivision a : divs)
	        	{
	            	if(AvailabilityManager.isAvailable("dl"))
	            	{
	            		if(a.isDivision() || (a.getGuid().startsWith("DL_BINDER")))
	            		{
	            			divisTemp.add(a);
	            		}
	            	}
	            	else
	            	{
	            		if(a.isDivision())
	            		{
                            if (userProfileKeys != null && Boolean.valueOf(userProfileKeys))
                            {
                                if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD, a.getGuid()))
                                    divisTemp.add(a);
                            }
                            else if (userProfileKeys == null || !Boolean.valueOf(userProfileKeys))
                                divisTemp.add(a);
	            		}
	            	}
	        	}
	        	divs = null;
	        	return divisTemp;    
	     }
	     else
	     {
	        	return new LinkedList<DSDivision>();
	     }
   }
   

   public final List<DSDivision> getAllDivisionsDivisionType() throws EdmException
   {
	   return getDivisionsDivisionType();
   }

    public final void deleteDivision(String guid, String substGuid) throws EdmException
    {
        if (guid == null)
            throw new NullPointerException("guid");
     
        try
        {
            DSDivision division = findDivision(guid); // DivisionNotFoundException
            DSDivision substDivision;
            if (substGuid!=null)
            	substDivision = findDivision(substGuid); // DivisionNotFoundException

           

            // lista dzia��w przeznaczonych do usuni�cia
            List<DSDivision> divisionStack = new LinkedList<DSDivision>();
            divisionStack.add(division);
            // pozycja na li�cie dzia�u, z kt�rego aktualnie pobierane
            // s� dzia�y potomne i umieszczane na li�cie, kiedy wska�nik
            // znajdzie si� za ko�cem listy, na li�cie s� wszystkie dzia�y
            int ptr = 0;
            while (ptr < divisionStack.size())
            {
                DSDivision d = (DSDivision) divisionStack.get(ptr);
                for (DSDivision sub : d.getChildren())
                {
                    divisionStack.add(sub);
                }
                ptr++;
            }

            
            // usuwanie dzia�u, od kt�rego rozpoczynane jest usuwanie
            divisionStack.remove(0);

            List<DSDivision> reversedDivisionStack = new ArrayList<DSDivision>(divisionStack);
            Collections.reverse(reversedDivisionStack);

            // najpierw usuni�cie dzia��w znajduj�cych si� w drzewie pod
            // usuwanym dzia�em; tutaj przenoszenie pism, dziennik�w etc.
            // gdzie indziej nie jest konieczne, poniewa� metoda checkCanDelete
            // nie pozwoli na usuwanie dzia�u, kt�rego dzia�y podrz�dne wymagaj�
            // tego rodzaju operacji


            // TODO: u�ytkownicy

            Class[] documentClasses = new Class[] { InOfficeDocument.class, OutOfficeDocument.class };

            PreparedStatement ps = null;
            try
            {
                // wszystkie dzia�y podrz�dne wzgl�dem usuwanego
                if (reversedDivisionStack.size() > 0)
                {
                    ps = DSApi.context().prepareStatement(
                        "delete from dso_role_divisions where division_guid = ?");
                    for (DSDivision div : reversedDivisionStack)
                    {
                        ps.setString(1, div.getGuid());
                        ps.executeUpdate();
                        removeUsers(div);
                        for (Profile prof : Profile.getProfiles()) {
                        	if (prof.getGroups().contains(div)) {
                        		prof.removeGroup(div);
                        	}
                        }
                        hideDivision(div);
                    }
                    DSApi.context().closeStatement(ps);
                }

                for (Class documentClass : documentClasses)
                {
                    ps = DSApi.context().prepareStatement(
                        "update "+DSApi.getTableName(documentClass)
                        +" set assigneddivision = ? where assigneddivision = ?");
                    ps.setString(1, substGuid);
                    ps.setString(2, guid);
                    ps.executeUpdate();
                    DSApi.context().closeStatement(ps);

                    ps = DSApi.context().prepareStatement(
                        "update "+DSApi.getTableName(documentClass)
                        +" set currentassignmentguid = ? where currentassignmentguid = ?");
                    ps.setString(1, substGuid);
                    ps.setString(2, guid);
                    ps.executeUpdate();
                    DSApi.context().closeStatement(ps);

                    ps = DSApi.context().prepareStatement(
                        "update "+DSApi.getTableName(documentClass)
                        +" set divisionguid = ? where divisionguid = ?");
                    ps.setString(1, substGuid);
                    ps.setString(2, guid);
                    ps.executeUpdate();
                    DSApi.context().closeStatement(ps);
                }

                ps = DSApi.context().prepareStatement(
                    "update "+DSApi.getTableName(Journal.class)+" set ownerguid = ? where ownerguid = ?");
                ps.setString(1, substGuid);
                ps.setString(2, guid);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                ps = DSApi.context().prepareStatement(
                    "update "+DSApi.getTableName(Container.class)
                    +" set divisionGuid = ? where divisionGuid = ?");
                ps.setString(1, substGuid);
                ps.setString(2, guid);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                ps = DSApi.context().prepareStatement(
                    "update "+DSApi.getTableName(Person.class)
                    +" set dictionaryGuid = ? where dictionaryGuid = ?");
                ps.setString(1, substGuid);
                ps.setString(2, guid);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                ps = DSApi.context().prepareStatement(
                    "update dso_role_divisions "+
                    " set division_guid = ? where division_guid = ?");
                ps.setString(1, substGuid);
                ps.setString(2, guid);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);
                checkCanDelete(division);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }

            hideDivision(division);
            removeUsers(division);
            for (Profile prof : Profile.getProfiles()) {
            	if (prof.getGroups().contains(division)) {
            		prof.removeGroup(division);
            	}
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public abstract void hideDivisionSpi(final DSDivision division) throws EdmException;

    private void hideDivision(final DSDivision division) throws EdmException
    {
        //division.setParent(DSDivision.find(DSDivision.ROOT_GUID));
        division.setHidden(true);
        division.update();
        hideDivisionSpi(division);
    }

    protected abstract void removeUsers(DSDivision division)
        throws EdmException;

    // czy mo�na usun�� dzia�?
    // musi by� pusty i wszystkie podrz�dne te� (checkDivisionEmpty)
    // lub
    //

/*
    public final boolean isDivisionEmpty(DSDivision division) throws EdmException
    {
        try
        {
            checkDivisionEmpty(division);
            return true;
        }
        catch (DivisionNotEmptyException e)
        {
            return false;
        }
        catch (DivisionIrremovableException e)
        {
            return false;
        }
    }
*/

    /**
     * Sprawdza, czy dzia� jest nieusuwalny. Dzia� mo�e by� nieusuwalny wtedy,
     * gdy wykorzystywany jest jako dzia� BOK lub instantAssignmentGroup
     * albo jest dzia�em g��wnym.
     * @param division
     * @throws DivisionIrremovableException
     */
    private final void checkDivisionIrremovable(final DSDivision division)
        throws DivisionIrremovableException
    {
        if (division.isRoot())
            throw new DivisionIrremovableException("Nie mo�na usun�� g��wnego dzia�u");

        String bokGuid = GlobalPreferences.getBokDivisionGuid();
        String instantAssignmentGroup =
            GlobalPreferences.getInstantAssignmentGroup();

        if (division.getGuid().equals(instantAssignmentGroup))
        {
            if (log.isDebugEnabled())
                log.debug("Nie mo�na usun�� grupy natychmiastowej "+
                "dekretacji: "+division.getName());
            throw new DivisionIrremovableException("Nie mo�na usun�� grupy natychmiastowej "+
                "dekretacji: "+division.getName());
        }

        if (division.getGuid().equals(bokGuid))
        {
            if (log.isDebugEnabled())
                log.debug("Nie mo�na usun�� grupy BOK: "+bokGuid);
            throw new DivisionIrremovableException("Nie mo�na usun�� grupy BOK: "+bokGuid);
        }
    }

    /**
     * Sprawdza, czy dzia� jest gdziekolwiek wykorzystywany (np. s� w nim pisma,
     * dzienniki etc.).  Nie bada dzia��w podrz�dnych; nie sprawdza, czy dzia�
     * jest nieusuwalny (bo jest dzia�em BOK, instantAssignmentGroup).
     *
     * @param division
     * @throws DivisionNotEmptyException Je�eli dzia� nie jest pusty.
     * @throws DivisionIrremovableException Je�eli dzia� nie mo�e by� usuni�ty
     *  bezwarunkowo (np. jest dzia�em BOK lub instantAssignmentGroup)
     * @throws EdmException Je�eli wyst�pi� inny b��d.
     */
    private final void checkDivisionEmpty(final DSDivision division)
        throws EdmException, DivisionNotEmptyException
    {
        try
        {
            if (Journal.findByDivisionGuid(division.getGuid()).size() > 0)
            {
                if (log.isDebugEnabled())
                    log.debug("W dziale "+division.getName()+" znajduj� si� dzienniki");
                throw new DivisionNotEmptyException("W dziale "+division.getName()+" znajduj� si� dzienniki");
            }

            // klasy dokument�w, dla kt�rych nale�y wykona� sprawdzenie
            Class[] documentClasses = new Class[] { InOfficeDocument.class, OutOfficeDocument.class };

            for (int i=0; i < documentClasses.length; i++)
            {            	
            	Criteria criteria = DSApi.context().session().createCriteria(documentClasses[i]); 
            	criteria.setProjection(Projections.rowCount());
            	Disjunction or = Restrictions.disjunction();
            	or.add(Restrictions.eq("assignedDivision" , division.getGuid()));
            	or.add(Restrictions.eq("currentAssignmentGuid" , division.getGuid()));
            	or.add(Restrictions.eq("divisionGuid" , division.getGuid()));
            	criteria.add(or);
            	
            	Long count  = (Long)(criteria.list().get(0)); 
                // ze wzgl�du na b��d w Hibernate nie mog� odczyta�
                // count(*) z OfficeDocument, poniewa� Hibernate wywo�uje
                // w�wczas dwa zapytania, dla ka�dej klasy dziedzicz�cej
                // po OfficeDocument i zwraca tylko wynik ostatniego
                // z tych zapyta�
//				Long count = (Long) DSApi.context().session().find(
//                    "select count(*) from d in class "+documentClasses[i]+
//                    " where d.assignedDivision = ? or d.currentAssignmentGuid = ?"+
//                    " or d.divisionGuid = ?",
//                    new Object[] { division.getGuid(), division.getGuid(), division.getGuid() },
//                    new Type[] { Hibernate.STRING, Hibernate.STRING, Hibernate.STRING }).get(0);
                if (count != null && count.intValue() > 0)
                {
                    if (log.isDebugEnabled())
                        log.debug("Nie mo�na usun�� dzia�u "+division.getName()+
                        ", poniewa� znajduj� si� w nim pisma");
                    throw new DivisionNotEmptyException("Nie mo�na usun�� dzia�u "+division.getName()+
                        ", poniewa� znajduj� si� w nim pisma");
                }
            }

            Long count = (Long) DSApi.context().classicSession().find(
                "select count(*) from d in class "+Journal.class.getName()+
                " where d.ownerGuid = ?",
                division.getGuid(), Hibernate.STRING).get(0);
            if (count != null && count.intValue() > 0)
            {
                if (log.isDebugEnabled())
                    log.debug("Nie mo�na usun�� dzia�u "+division.getName()+
                    ", poniewa� znajduj� si� w nim dzienniki (by� mo�e usuni�te)");
                throw new DivisionNotEmptyException("Nie mo�na usun�� dzia�u "+division.getName()+
                    ", poniewa� znajduj� si� w nim dzienniki (by� mo�e usuni�te)");
            }

            count = (Long) DSApi.context().classicSession().find(
                "select count(*) from d in class "+Container.class.getName()+
                " where d.divisionGuid = ?",
                division.getGuid(), Hibernate.STRING).get(0);
            if (count != null && count.intValue() > 0)
            {
                if (log.isDebugEnabled())
                    log.debug("Nie mo�na usun�� dzia�u "+division.getName()+
                    ", poniewa� znajduj� si� w nim teczki lub sprawy");
                throw new DivisionNotEmptyException("Nie mo�na usun�� dzia�u "+division.getName()+
                    ", poniewa� znajduj� si� w nim teczki lub sprawy");
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Zwraca true, je�eli dzia� mo�e zosta� usuni�ty.
     * @param division
     * @return
     * @throws EdmException
     */
    public final boolean canDelete(final DSDivision division) throws EdmException
    {
        try
        {
            checkCanDelete(division);
            return true;
        }
        catch (DivisionNotEmptyException e)
        {
            return false;
        }
        catch (DivisionIrremovableException e)
        {
            return false;
        }
    }

    /**
     * Sprawdza, czy dzia� mo�e zosta� usuni�ty przy pomocy metody
     * deleteDivision.  Je�eli ta metoda rzuci wyj�tek (co oznacza
     * brak zgody na usuwanie), deleteDivision r�wnie� rzuci wyj�tek
     * i odm�wi usuni�cia dzia�u.
     *
     * @param division
     * @throws EdmException
     * @throws DivisionNotEmptyException
     * @throws DivisionIrremovableException
     */
    public final void checkCanDelete(final DSDivision division)
        throws EdmException, DivisionNotEmptyException, DivisionIrremovableException
    {
        // usuni�cie dzia�u mo�liwe jest wtedy, gdy:
        // jest ostatnim dzia�em w swojej ga��zi i nie nale�y do BOK albo instantAssignment
        // TODO: czy instantAssignment jest jeszcze u�ywane?
        // nie jest ostatni, ale wszystkie ni�sze s� puste i nie s� w BOK lub instantAssignment
    	
        checkDivisionIrremovable(division);
        
        // lista dzia��w przeznaczonych do usuni�cia
        List<DSDivision> divisionStack = new LinkedList<DSDivision>();
        divisionStack.add(division);
        // pozycja na li�cie dzia�u, z kt�rego aktualnie pobierane
        // s� dzia�y potomne i umieszczane na li�cie, kiedy wska�nik
        // znajdzie si� za ko�cem listy, na li�cie s� wszystkie dzia�y
        // z tej ga��zi
        int ptr = 0;
        while (ptr < divisionStack.size())
        {
            DSDivision d = (DSDivision) divisionStack.get(ptr);
            for (DSDivision sub : d.getChildren())
            {
                divisionStack.add(sub);
            }
            ptr++;
        }

        // usuni�cie dzia�u pocz�tkowego - nie mo�e by� sprawdzany,
        // czy znajduj� si� w nim dzienniki etc.
        divisionStack.remove(0);

        // sprawdzanie, czy wszystkie dzia�y podrz�dne s� puste 
        for (final DSDivision div : divisionStack)
        {
            checkDivisionIrremovable(div);
            checkDivisionEmpty(div);
        }
    }

    public final DSUser createSystemUser(String username) throws EdmException
    {
        if (username == null)
            throw new NullPointerException("username");
        if (!username.startsWith("$"))
            throw new EdmException("Nazwa u�ytkownika systemowego musi zaczyna� si� od '$'.");

        // bez toLowerCase() !
        username = username.trim();

        try
        {
            findByUsername(username);
            throw new DuplicateNameException(sm.getString("istniejeUzytkownik", username));
            //throw new DuplicateNameException("Istnieje ju� u�ytownik o nazwie "+username+
            //    ", m�g� zosta� oznaczony jako usuni�ty");
        }
        catch (UserNotFoundException e)
        {
            DSUser user = createUserSpi(username, username, username, true);
            createWorkflowResource(user);
            return user;
        }
    }

    public final DSUser createUser(String username, String firstname, String lastname) throws EdmException
    {
        if (username == null)
            throw new NullPointerException("username");
        if (firstname == null)
            throw new NullPointerException("firstname");
        if (lastname == null)
            throw new NullPointerException("lastname");

        if (username.length() == 0 || username.trim().length() == 0)
            throw new EdmException("Podano pust� nazw� u�ytkownika");

        username = username.trim().toLowerCase();
        firstname = firstname.trim();
        lastname = lastname.trim();

        try
        {
            findByUsername(username);
            throw new DuplicateNameException(sm.getString("istniejeUzytkownik", username));
            //throw new DuplicateNameException("Istnieje ju� u�ytownik o nazwie "+username+
            //    ", m�g� zosta� oznaczony jako usuni�ty");
        }
        catch (UserNotFoundException e)
        {
            DSUser user = createUserSpi(username, firstname, lastname, false);
            createWorkflowResource(user);
            /**Udostepnieni wszystkich zasobow*/
            List<AnyResource> resources = AnyResource.list();
            if(resources != null)
            {
            	for (AnyResource resource : resources) 
            	{
    	        	UserCalendarBinder ucb = new UserCalendarBinder();
    	        	ucb.setPermissionType(CalendarFactory.PERMISSION_EDIT);
    	        	ucb.setCalendarId(resource.getCalendarId());
    	        	ucb.setShow(true);
    	        	ucb.setUsername(username);
    	        	ucb.create();
				}
            }
			if (AvailabilityManager.isAvailable("kancelaria.wlacz.podpisWewnetrzny")) {
				InternalSignature internalSignature = InternalSignature.getInstance();
				if (internalSignature.generateSignature(user.getId()) == null) {
					log.error("B��d podczas gnerowania sygnatury wewn�trznej", new NullPointerException());
					throw new NullPointerException();
				}
			}

            if(AvailabilityManager.isAvailable("jackrabbit.users.create")) {
                try {
                    // TODO: doda� propagacj� has�a
                    JackrabbitUsers.instance().createUser(username, "");
                } catch (Exception e1) {
                    log.error("[createUser] cannot propagate user to jackrabbit", e1);
                }
            }

            return user;
        }
    }

    private void createWorkflowResource(DSUser user) throws EdmException
    {
        if (Configuration.coreOfficeAvailable())
        {

            WorkflowFactory.createUser(user.getName(), "enhydra", "", user.getEmail(), "");
            WorkflowFactory.addParticipantToUsernameMapping(
                    null, null, user.getName(), user.getName());
        }
    }

    public void revertUser(String username) throws EdmException
    {
        revertUserSpi(username);
        DSUser user = findByUsernameSpi(username);
        createWorkflowResource(user);
    }
    
    public final void deleteUser(String username) throws EdmException
    {
        if (username == null)
            throw new NullPointerException("username");

        username = username.trim().toLowerCase();

        if (username.equals(DSApi.context().getPrincipalName()))
            throw new EdmException("Nie mo�na usun�� zalogowanego u�ytkownika");

        try
        {
            DSUser user = findByUsername(username);

            if (user.isSystem())
                throw new EdmException("Nie mo�na usun�� u�ytkownika systemowego "+username);

            deleteUserSpi(username);
            CollectiveAssignments.getInstance().removeUserFromCollectiveAssignments(username);
            
            // usuni�cie akceptacji, kt�re u�ytkownik mo�e nadawa�
            List<AcceptanceCondition> conditions = AcceptanceCondition.userAcceptances(username);
            for (AcceptanceCondition ac : conditions)
            	DSApi.context().session().delete(ac);
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public final DSDivision[] getGroups() throws EdmException
    {
        return getGroupsSpi();
/*
        return findDivision(DSDivision.ROOT_GUID).getChildren(new DSDivisionFilter()
        {
            public boolean accept(DSDivision division)
            {
                return division.isGroup();
            }
        });
*/
    }

    public void exportUsersAndDivisions(OutputStream os) throws EdmException
    {
        DSUser[] users = getUsersSpi();
        DSDivision[] divisions = getDivisionsSpi();

        try
        {
            XMLWriter writer = new XMLWriter(os);

            writer.startDocument();

            final Attributes EMPTY_ATTRS = new AttributesImpl();

            AttributesImpl orgAttrs = new AttributesImpl();
            orgAttrs.addAttribute("", "", "version", "CDATA", "1.0");

            writer.startElement("", "", "organization", orgAttrs);

            writer.startElement("", "", "users", EMPTY_ATTRS);

            // u�ytkownicy
            // <user name="admin" firstname="Administrator" ...>
            //    <role name="admin"/>
            //    <certificate encoded="BASE64=="/>
            // </user>
            for (int i=0; i < users.length; i++)
            {
                DSUser user = users[i];
                AttributesImpl userAttrs = new AttributesImpl();

                userAttrs.addAttribute("", "", "name", "CDATA", user.getName());
                if (user.getEncodedPassword() != null)
                    userAttrs.addAttribute("", "", "password", "CDATA", user.getEncodedPassword());
                if (user.getFirstname() != null)
                    userAttrs.addAttribute("", "", "firstname", "CDATA", user.getFirstname());
                if (user.getLastname() != null)
                    userAttrs.addAttribute("", "", "lastname", "CDATA", user.getLastname());
                if (user.getEmail() != null)
                    userAttrs.addAttribute("", "", "email", "CDATA", user.getEmail());
                if (user.getIdentifier() != null)
                    userAttrs.addAttribute("", "", "identifier", "CDATA", user.getIdentifier());
                userAttrs.addAttribute("", "", "login-disabled", "CDATA", String.valueOf(user.isLoginDisabled()));
                if (user.getLastSuccessfulLogin() != null)
                    userAttrs.addAttribute("", "", "last-successful-login", "CDATA", String.valueOf(user.getLastSuccessfulLogin().getTime()));
                if (user.getLastUnsuccessfulLogin() != null)
                    userAttrs.addAttribute("", "", "last-unsuccessful-login", "CDATA", String.valueOf(user.getLastUnsuccessfulLogin().getTime()));
                if (user.getSupervisor() != null)
                    userAttrs.addAttribute("", "", "supervisor", "CDATA", user.getSupervisor().getName());
                if (user.getSubstituteUser() != null)
                    userAttrs.addAttribute("", "", "substitute", "CDATA", user.getSubstituteUser().getName());
                if (user.getSubstitutedFrom() != null)
                    userAttrs.addAttribute("", "", "substitutes-from", "CDATA", String.valueOf(user.getSubstitutedFrom().getTime()));
                if (user.getSubstitutedThrough() != null)
                    userAttrs.addAttribute("", "", "substitutes-through", "CDATA", String.valueOf(user.getSubstitutedThrough().getTime()));
                userAttrs.addAttribute("", "", "deleted", "CDATA", String.valueOf(user.isDeleted()));
                userAttrs.addAttribute("", "", "certificate-login", "CDATA", String.valueOf(user.isCertificateLogin()));
                userAttrs.addAttribute("", "", "system", "CDATA", String.valueOf(user.isSystem()));
                if (user.getLastPasswordChange() != null)
                    userAttrs.addAttribute("", "", "last-password-change", "CDATA", String.valueOf(user.getLastPasswordChange().getTime()));

                writer.startElement("", "", "user", userAttrs);

                // role u�ytkownika
                for (Iterator iter=user.getRoles().iterator(); iter.hasNext(); )
                {
                    AttributesImpl roleAttrs = new AttributesImpl();

                    roleAttrs.addAttribute("", "", "name", "CDATA", (String) iter.next());

                    writer.startElement("", "", "role", roleAttrs);
                    writer.endElement("", "", "role");
                }

                // certyfikaty u�ytkownika
                X509Certificate[] certs = user.getCertificates();
                for (int c=0; c < certs.length; c++)
                {
                    try
                    {
                        AttributesImpl certAttrs = new AttributesImpl();

                        certAttrs.addAttribute("", "", "encoded", "CDATA", new String(Base64.encode(certs[c].getEncoded())));

                        writer.startElement("", "", "certificate", certAttrs);
                        writer.endElement("", "", "certificate");
                    }
                    catch (CertificateEncodingException e)
                    {
                        log.warn("Nie mo�na zdekodowa� certyfikatu "+certs[c].getSubjectDN()+" u�ytkownika "+user.getName()+", pomijanie");
                    }
                }

                writer.endElement("", "", "user");
            }

            writer.endElement("", "", "users");

            // dzia�y
            // <division guid="GUID" ... >
            //    <user name="admin"/>
            // </division>

            writer.startElement("", "", "divisions", EMPTY_ATTRS);

            for (int i=0; i < divisions.length; i++)
            {
                DSDivision division = divisions[i];
                AttributesImpl attrs = new AttributesImpl();

                attrs.addAttribute("", "", "guid", "CDATA", division.getGuid());
                attrs.addAttribute("", "", "type", "CDATA", division.getDivisionType());
                if (division.getName() != null)
                    attrs.addAttribute("", "", "name", "CDATA", division.getName());
                if (division.getCode() != null)
                    attrs.addAttribute("", "", "code", "CDATA", division.getCode());
                if (division.getParent() != null)
                    attrs.addAttribute("", "", "parent-guid", "CDATA", division.getParent().getGuid());

                writer.startElement("", "", "division", attrs);

                DSUser[] divisionUsers = division.getAllUsers();

                // u�ytkownicy w tym dziale
                for (int j=0; j < divisionUsers.length; j++)
                {
                    DSUser user = divisionUsers[j];

                    AttributesImpl userAttrs = new AttributesImpl();
                    userAttrs.addAttribute("", "", "name", "CDATA", user.getName());

                    writer.startElement("", "", "user", userAttrs);
                    writer.endElement("", "", "user");
                }

                writer.endElement("", "", "division");
            }

            writer.endElement("", "", "divisions");

            writer.endElement("", "", "organization");

            writer.close();
        }
        catch (SAXException e)
        {
            throw new EdmException("B��d tworzenia pliku eksportu danych", e);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d tworzenia pliku eksportu danych", e);
        }
    }


    public static void sortUsers(DSUser[] users, int sortMode, boolean ascending)
    {
        if (sortMode == DSUser.SORT_NONE)
            return;

        SortUtils.CollationKeyGenerator generator;

        // tworz� obiekt CollationKeyGenerator tworz�cy reprezentacj�
        // tekstow� obiekt�w User zgodnie z trybem sortowania
        switch (sortMode)
        {
            case DSUser.SORT_NAME:
                generator = new SortUtils.CollationKeyGenerator()
                {
                    public String getKey(Object object)
                    {
                        DSUser user = (DSUser) object;
                        return user.getName();
                    }
                };
                break;

            case DSUser.SORT_FIRSTNAME_LASTNAME:
                generator = new SortUtils.CollationKeyGenerator()
                {
                    public String getKey(Object object)
                    {
                        DSUser user = (DSUser) object;
                        StringBuilder key = new StringBuilder(32);
                        if (user.getFirstname() != null)
                        {
                            key.append(user.getFirstname());
                            key.append(' ');
                        }
                        if (user.getLastname() != null)
                            key.append(user.getLastname());
                        return key.toString();
                    }
                };
                break;

            case DSUser.SORT_LASTNAME_FIRSTNAME:
                generator = new SortUtils.CollationKeyGenerator()
                {
                    public String getKey(Object object)
                    {
                        DSUser user = (DSUser) object;
                        StringBuilder key = new StringBuilder(32);
                        if (user.getLastname() != null)
                        {
                            key.append(user.getLastname());
                            key.append(' ');
                        }
                        if (user.getFirstname() != null)
                            key.append(user.getFirstname());
                        return key.toString();
                    }
                };
                break;

            default:
                throw new IllegalArgumentException("Nieznany rodzaj sortowania u�ytkownik�w: "+sortMode);

        }

        SortUtils.sortObjects(users, generator, ContextLocale.getLocale(), ascending);
    }

    /**
     * Zwraca hash podanego has�a.
     * @param password Has�o w postaci jawnego tekstu.
     * @throws RuntimeException Je�eli biblioteka nie udost�pnia algorytmu
     *  SHA lub kodowania UTF-8.
     */
    public static String hashPassword(String password)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(password.getBytes("utf-8"));
            return "{SHA}"+new String(Base64.encode(md.digest()));
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

	public abstract SearchResults<DSUser> searchUsers(int offset, int limit,
			int sortMode, boolean ascending, String query, UserSearchBean bean,boolean deleted)
			throws EdmException;



}
