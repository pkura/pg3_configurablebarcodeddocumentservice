package pl.compan.docusafe.core.users;

import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.util.StringManager;

public class ProfileNotFoundException extends EntityNotFoundException {
	private static final StringManager sm = StringManager.getManager(Constants.Package);

	public ProfileNotFoundException(String profilename) {
		super(sm.getString("profileNotFoundException", profilename), true);
	}
	
}
