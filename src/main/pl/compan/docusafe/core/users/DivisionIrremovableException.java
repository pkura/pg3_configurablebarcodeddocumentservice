package pl.compan.docusafe.core.users;

import pl.compan.docusafe.core.EdmException;
/* User: Administrator, Date: 2006-03-07 12:43:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DivisionIrremovableException.java,v 1.1 2006/03/07 16:02:59 lk Exp $
 */
public class DivisionIrremovableException extends EdmException
{
    public DivisionIrremovableException(String message)
    {
        super(message, true);
    }
}
