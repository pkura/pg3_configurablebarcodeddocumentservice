package pl.compan.docusafe.core.users.ad;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Interface dla fabryki ActiveDirectory do budowania principali
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 * 
 */
public abstract class ActiveDirectoryFactory
{
	private final static Logger log = LoggerFactory
			.getLogger(ActiveDirectoryFactory.class);

	/**
	 * Zwraca instancj� fabryki podan�
	 */
	@SuppressWarnings("rawtypes")
	public static ActiveDirectoryFactory getInstance()
	{
		String factory = Docusafe.getAdditionProperty("active.directory.factory");
		ActiveDirectoryFactory instance = null;
		try
		{
			Class theClass = Class.forName(factory);
			instance = (ActiveDirectoryFactory) theClass.newInstance();
		} catch (NullPointerException ex)
		{
			log.error("null pointer w fabryce: " + factory, ex);
		} catch (ClassNotFoundException ex)
		{
			log.error("Nie znalazlem classy: " + factory, ex);
		} catch (Exception ex)
		{
			log.error("blad initializacji fabryki: " + factory, ex);
		}

		return instance;
	}
	
	/**
	 * Zwraca principal do logowania, uzywane w funkcji checkPassword
	 * @param ad
	 * @param domainPrefix
	 * @param username
	 * @return
	 */
	public abstract String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username);
}
