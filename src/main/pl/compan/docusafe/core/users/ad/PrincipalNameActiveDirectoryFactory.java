package pl.compan.docusafe.core.users.ad;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

/**
 * Fabryka ustawia principal w konwncji
 * username@domain, tyle �e ko�ysta ze wszystkich wcze�niej napisanych funkcjonalno�ci.
 * R�ni si� jeszcze tym �e domain jest brana wsp�lna z ustawie� a nie z warto�ci w bazie danych.
 * @author Mariusz Kilja�czyk </a>
 *
 */
public class PrincipalNameActiveDirectoryFactory extends ActiveDirectoryFactory
{
	public final static Logger log = LoggerFactory.getLogger(ActiveDirectoryManager.class);
	@Override
	public String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username)
	{
		String domainSiffix = TextUtils.trimmedStringOrNull(ad.getDomainSuffix());
	    log.trace("domainSiffix={}",domainSiffix);
        String mainDomainPrefix = TextUtils.trimmedStringOrNull(ad.getMainDomainPrefix());
        log.trace("mainDomainPrefix={}",mainDomainPrefix);
        String subDomainName = TextUtils.trimmedStringOrNull((ad.getSubdomain(TextUtils.trimmedStringOrNull(username))));
        log.trace("subDomainName={}",subDomainName);
        String user = TextUtils.trimmedStringOrNull(ad.getUsername(TextUtils.trimmedStringOrNull(username)));
        log.trace("user={}",user);
        String principal = null;
        principal = getPrincipalName(user, getFullDomainName(mainDomainPrefix, subDomainName, domainSiffix));
		return principal.toLowerCase();
	}


	/**
	 * Generuje principalName w konwencji user@fullDomainName
	 * @param user
	 * @param fullDomainName
	 * @return
	 */
	protected String getPrincipalName(String user, String fullDomainName)
	{
	    if (user==null)
	        return fullDomainName;
	    return user + "@" + fullDomainName;
	}
	
    /**
     * Sklada domene z subdomena
     * @param mainDomainName
     * @param subDomainName
     * @param subDomainName
     * @return
     */
    protected String getFullDomainName(String mainDomainName,String subDomainName,String domainSiffix)
    {
        if (subDomainName == null || subDomainName.trim().length()==0)
        {
            return  mainDomainName + "." + domainSiffix;
        }
        if (subDomainName.equals(mainDomainName))
        {
            return mainDomainName + "." + domainSiffix;
        }
        return mainDomainName + "." +subDomainName+ "." +domainSiffix;
    }

}
