package pl.compan.docusafe.core.users.ad;

import de.schlichtherle.io.File;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.DSLoginModule;
import pl.compan.docusafe.util.TextUtils;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.security.auth.callback.*;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.Hashtable;

/**
 * Klasa odpowiedzialna za autoryzacje uzytkownikow a AD
 * Przy naszym obecnym stanie wiedzy klasa jedynie weryfikuje haslo uzytkownika 
 * w jednej domenie 
 * @author wkutyla
 * @TODO - temat wymaga zbadania, nie wiadomo jeszcze jak zachowywac sie bedzie funkcja sprawdzajaca wygaszanie hasel
 * (dla uzytkownikow AD powinna byc nieaktywna, jak logowania awaryjne, etc)
 */
public class ActiveDirectoryManager 
{
	public final static Logger log = LoggerFactory.getLogger(ActiveDirectoryManager.class);
	
	static
	{
		if(Docusafe.getAdditionProperty("active.login.config") != null)
		{
			log.error("SET LOGIN CONF");
			System.setProperty("java.security.auth.login.config", Docusafe.getAdditionProperty("active.login.config"));
		}
		
		if(Docusafe.getAdditionProperty("active.directory.kerberos.realm") != null && Docusafe.getAdditionProperty("active.directory.kerberos.kdc") != null )
		{
			log.error("SET KDC REALM");
	        System.setProperty("java.security.krb5.realm", Docusafe.getAdditionProperty("active.directory.kerberos.realm"));  
	        System.setProperty("java.security.krb5.kdc", Docusafe.getAdditionProperty("active.directory.kerberos.kdc"));
		}
		if(Docusafe.getAdditionProperty("active.directory.kerberos.krb5.conf") != null)
		{
			System.setProperty("java.security.krb5.conf",  Docusafe.getAdditionProperty("active.directory.kerberos.krb5.conf"));
			File f = new File(Docusafe.getAdditionProperty("active.directory.kerberos.krb5.conf"));
			log.error("krb5 = "+f.getAbsolutePath());
			if(!f.exists())
				log.error("Brak pliku krb5.con");
        }
	}
	
    static final String SUB_DOMAIN_SEPARATOR = "\\";
    public static final String SECURITY_PRINCIPAL_KIND_USERPRINCIPALNAME = "userPrincipalName";
    public static final String SECURITY_PRINCIPAL_KIND_ONLY_USERNAME = "onlyUsername";

    public static String getDefaultAdUrl(){
        return Docusafe.getAdditionProperty(DSLoginModule.ACTIVE_DIRECTORY_URL);
    }
    
    /**
     * Fabryka wsparcia do połączeń AD
     */
    private ActiveDirectoryFactory factory = null;
    /**
     * Adres URL serwera Active Directory
     */
    private String defaultAdUrl = "ldap://127.0.0.1";
    /**
     * Nazwa domeny
     */
    private String domainSuffix = "";
    /**
     * Oznaczenie prefixu domeny glownej
     */
    private String mainDomainPrefix = "";
    
    private String userDomainPrefix = "";
	/***
     * Nazwa uzytkownika o uprawnieniach admina
     */
    private String adminUsername;
    
    /***
     * haslo admina
     */
    private String adminPass;
    /***
     * rodzaj principal name do logowania 
     */
    private String securityPrincipalKind;
    /***
     * 
     */
    private boolean addCnUsers;
    
    /**
     * tag dla głównej domeny domyslnie np. DC=domena.pl;
     * Dla OpenLDap jest np.: o=domena.pl
     */
    private String mainDomainTag;
    /**
     * tag dla głównej domeny domyslnie np. DC=Users, DC=domena.pl;
     * Dla OpenLDap jest np.: ou=People, o=domena.pl
     */
    private String subDomainTag;
    /**
     * tag dla głównej domeny domyslnie np. DC=Users, DC=domena.pl;
     * Dla OpenLDap jest np.: ou=People, o=domena.pl
     */
    private String siffixDomainTag;
    
    /**
     * Domyslnie jest CN np. : CN=Users
     * W lDAP jest np. ou= People
     */
	private String addCnUsersName;
	
	private String addCnUsersTag;
	/**
	 * Tag dla loginu usera
	 */
	private String addUserLoginTag;
	
	/**
	 * Naza katalogu dla grup
	 */
	private String addCnGroupsName;
	
	/**
	 * Nazwa katalogu dla użytkowników
	 */
	private String addCnGroupsTag;
	
    private boolean kerberosUse = false;
    private String kerberosRealm;
	
	/**
	 * Kontstruktor domyslny
	 */
	public ActiveDirectoryManager()
	{
		super();
		init();
	}
	
	/**
	 * Konstruktor w ktorym podajemy URL LDAPa
	 * @param ldapUrl
	 */
	public ActiveDirectoryManager(String ldapUrl)
	{
		this();
		defaultAdUrl = ldapUrl;
	}
	
	/**
	 * metoda inicjujaca AD
	 *
	 */
	private void init()
	{
		//ustawienie fabryki dla logowania AD
		factory = ActiveDirectoryFactory.getInstance();
		
		defaultAdUrl = Docusafe.getAdditionProperty("active.directory.url");
		domainSuffix = Docusafe.getAdditionProperty("active.directory.domainSuffix");
		mainDomainPrefix = Docusafe.getAdditionProperty("active.directory.mainDomainPrefix");
		userDomainPrefix = Docusafe.getAdditionProperty("active.directory.userDomainPrefix");
		adminUsername = Docusafe.getAdditionProperty("active.directory.adminUsername");
		adminPass = Docusafe.getAdditionProperty("active.directory.adminPass");
		addCnUsers = "true".equals(Docusafe.getAdditionProperty("active.directory.addCnUsers"));
		addCnUsersName = Docusafe.getAdditionProperty("active.directory.addCnUsersName");
		addCnUsersTag = Docusafe.getAdditionProperty("active.directory.addCnUsersTag");
		addCnGroupsName = Docusafe.getAdditionProperty("active.directory.addCnGroupsName");
		addCnGroupsTag = Docusafe.getAdditionProperty("active.directory.addCnGroupsTag");
		addUserLoginTag = Docusafe.getAdditionProperty("active.directory.addUserLoginTag");
		securityPrincipalKind = Docusafe.getAdditionProperty("active.directory.security.principal.kind");
		mainDomainTag=Docusafe.getAdditionProperty("active.directory.mainDomainTag");
		subDomainTag=Docusafe.getAdditionProperty("active.directory.subDomainTag");
		siffixDomainTag=Docusafe.getAdditionProperty("active.directory.siffixDomainTag");
		
		kerberosUse = "true".equals(Docusafe.getAdditionProperty("active.directory.kerberos.on"));
		kerberosRealm = Docusafe.getAdditionProperty("active.directory.kerberos.realm");
		
	}
	/**
	 * Weryfikacja hasla w AD
	 * @param user
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean checkPassword(DSUser user, String username, String password)
	{
		if(kerberosUse)
			return checkPasswordViaKerberos(user, username, password);
		else
			return checkPassword(user.getDomainPrefix(),user,password);
	}
	
	
	public boolean checkPasswordViaKerberos(DSUser user, String username, String password)
	{
		try
		{
			log.error("Login kerberos login = "+ username);
			LoginContext lc = new LoginContext("primaryLoginContext", new UserNamePasswordCallbackHandler(username, password.toCharArray()));
        	lc.login();
        	log.error("Zalogował = "+ username);
			return true;
		}
		catch (LoginException e) 
		{
					try
					{
						log.error("Login4 kerberos login = "+ username);
						log.error("BEDZIE = "+ username.toUpperCase());
						LoginContext lc = new LoginContext("primaryLoginContext", new UserNamePasswordCallbackHandler(username.toUpperCase(), password.toCharArray()));
			        	lc.login();
			        	log.error("Zalogował = "+ username.toUpperCase());
						return true;
					}
					catch (LoginException e4) 
					{
						log.error(e4.getMessage(),e4);
						return false;			
					}				
		}
	}
	
	public static class UserNamePasswordCallbackHandler implements CallbackHandler 
	{
		   private String _userName;
		   private char[] _password;

		   public UserNamePasswordCallbackHandler(String userName, char[] password) 
		   {
		     _userName = userName;
		     _password = password;
		   }

		   public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException 
		   {
			 for (Callback callback : callbacks) 
			 {
			   if (callback instanceof NameCallback && _userName != null) 
			   {
			     ((NameCallback) callback).setName(_userName);
			   }
			   else if (callback instanceof PasswordCallback && _password != null) 
			   {
			     ((PasswordCallback) callback).setPassword(_password);
			   }
			 }
		   }
	}
	
	/**
	 * Metoda sprawdza czy uzytkownik istnieje w domenie Windows
	 * Poprzez zalogowanie sie w ldapie
	 * Jesli istnieje zwracane jest true
	 * Jesli nie false
	 * W przypadku innego bledu tez false
	 * @param domainPrefix - prefix domeny
	 * @param user
	 * @param password
	 * @return
	 */
	public boolean checkPassword(String domainPrefix, DSUser user, String password)
	{
        String username = user.getExternalName() != null && !user.getExternalName().isEmpty()
                           ? user.getExternalName()
                           : user.getName();
		log.debug("sprawdzamu uzytkownika:{} name -> {} ",user.getName(), username);
		if (log.isTraceEnabled()) {
			log.trace("ldapUrl={}", user.getAdUrl() != null ? user.getAdUrl() : defaultAdUrl);
		}
		if (password==null || password.trim().length()==0) {
			log.debug("haslo puste");
			return false;
		}		
		DirContext context = null;
		try
		{
			String principal = factory.getPrincipalForPasswordCheck(this, domainPrefix, username);
			
			Hashtable<String,String> env = new Hashtable<String,String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.SECURITY_PRINCIPAL, principal);
            env.put(Context.SECURITY_CREDENTIALS, password);
            if(user.getAdUrl() != null){
                log.debug("principal : '{}' provider_url : '{}'", principal, user.getAdUrl());
                env.put(Context.PROVIDER_URL, user.getAdUrl());
            } else {
                log.debug("principal : '{}' provider_url : '{}'", principal, defaultAdUrl);
                env.put(Context.PROVIDER_URL, defaultAdUrl);
            }
            context = new InitialDirContext(env);
            log.debug("autoryzacja pozytywna");
            return true;
		}
        catch (Exception e)
        {
        	log.debug("Blad podczas dostepu do AD");
        	log.trace(e.getMessage(),e);
        	return false;
        }
        finally
        {
        	if (context!=null)
        	{
        		try
        		{
        			context.close();
        		}
        		catch (Exception e1)
        		{
        			log.debug("Blad w czasie zamykania",e1);
        		}
        	}
        }
	}
	
	public LdapContext LdapConnectAsAdminInLdapContext() throws Exception
	{
		return LdapConnectInLdapContext(adminUsername,adminPass);
	}
	
	public DirContext LdapConnectAsAdmin() throws Exception
	{
		return LdapConnect(adminUsername,adminPass);
	}
	
	public DirContext LdapConnectAnnonymous() throws Exception
	{
		return LdapConnect();
		
	}
	
	 /**
     * Logowanie przez
     * @param username
     * @param pass
     * @return
     * @throws java.lang.Exception
     */
    public LdapContext LdapConnectInLdapContext(String username, String pass) throws Exception
    {
        log.debug("logowanie ad username={}",username);
        InitialLdapContext context = null;
        try
        {
            context = new InitialLdapContext(getContextEnvironmentProp(username, pass),null);
            log.debug("autoryzacja pozytywna");
        }
        catch (Exception e)
        {
            log.error("Blad w czasie autoryzacji",e);
            return null;
        }
		return context;
    }
    
    /**
     * Logowanie przez
     * @param username
     * @param pass
     * @return
     * @throws java.lang.Exception
     */
    public DirContext LdapConnect(String username, String pass) throws Exception
    {
        log.debug("logowanie ad username={}",username);
        DirContext context = null;
        try
        {
            context = new InitialDirContext(getContextEnvironmentProp(username, pass));
            log.debug("autoryzacja pozytywna");
        }
        catch (Exception e)
        {
            log.error("Blad w czasie autoryzacji",e);
            return null;
        }
		return context;
    }
    
    public DirContext LdapConnect() throws Exception
    {
        DirContext context = null;
        try
        {
            context = new InitialDirContext(getContextEnvironmentProp());
            log.debug("autoryzacja pozytywna");
        }
        catch (Exception e)
        {
            log.error("Blad w czasie autoryzacji",e);
            return null;
        }
		return context;
    }
    
    private Hashtable<String, String> getContextEnvironmentProp(String username, String pass)
    {
    	if (username == null || username.trim().length()==0)
        {
            log.debug("user pusty");
            return null;
        }
        if (pass==null || pass.trim().length()==0)
        { 
            log.debug("haslo puste");
            return null;
        }
        String domainSiffix = TextUtils.trimmedStringOrNull(getDomainSuffix());
        log.trace("domainSiffix={}",domainSiffix);
        String mainDomainPrefix = TextUtils.trimmedStringOrNull(getMainDomainPrefix());
        log.trace("mainDomainPrefix={}",mainDomainPrefix);
        String subDomainName = TextUtils.trimmedStringOrNull((this.getSubdomain(TextUtils.trimmedStringOrNull(username))));
        log.trace("subDomainName={}",subDomainName);
        String user = TextUtils.trimmedStringOrNull(this.getUsername(TextUtils.trimmedStringOrNull(username)));
        log.trace("user={}",user);
        String principal = null;
        if(SECURITY_PRINCIPAL_KIND_USERPRINCIPALNAME.equals(securityPrincipalKind))
        {
        	principal = getPrincipalName(user, getFullDomainName(mainDomainPrefix, subDomainName, domainSiffix));
        }
        else if(SECURITY_PRINCIPAL_KIND_ONLY_USERNAME.equals(securityPrincipalKind))
        {
            principal = user;
        }else
        {
        	principal = getPrincipal(user, mainDomainPrefix, subDomainName, domainSiffix);
        }
        log.trace("principal={}",principal);
        Hashtable<String,String> env = new Hashtable<String,String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        if (username!=null && username.length()>0)
        {
        	env.put(Context.SECURITY_PRINCIPAL,principal);
        }
        env.put(Context.SECURITY_CREDENTIALS, pass);
        env.put(Context.PROVIDER_URL, getDefaultAdUrl());
        return env;
    }
    
    private Hashtable<String, String> getContextEnvironmentProp()
    {
    	Hashtable<String,String> env = new Hashtable<String,String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, getDefaultAdUrl());
        return env;
    }
    
    
    /**
     * Generuje SECURITY_PRINCIPAL w konwencji CN=Administrator,CN=Users,DC=docusafe,DC=pl
     * @param user
     * @return
     */
    protected String getPrincipal(String user,String mainDomainPrefix,String subDomainName,String domainSiffix)
    {
    	
    	return 	(StringUtils.isEmpty(addUserLoginTag) ? "" : addUserLoginTag+"=")+user+
    			(addCnUsers ? getTag(addCnUsersTag)+addCnUsersName:"")+
    			(!StringUtils.isEmpty(mainDomainPrefix)? getMainDomainTagWith()+mainDomainPrefix:"")+
    			(!StringUtils.isEmpty(subDomainName)? getSubDomainTagWith()+subDomainName:"")+
    			(!StringUtils.isEmpty(domainSiffix)? getSiffixDomainTagWith()+domainSiffix:"");
    }
    
    /**
     * Generuje principalName w konwencji user@fullDomainName
     * @param user
     * @param fullDomainName
     * @return
     */
    protected String getPrincipalName(String user, String fullDomainName)
    {
        if (user==null)
            return fullDomainName;
        return user + "@" + fullDomainName;
    }
    /**
     * Sklada domene z subdomena
     * @param mainDomainName
     * @param subDomainName
     * @param subDomainName
     * @return
     */
    protected String getFullDomainName(String mainDomainName,String subDomainName,String domainSiffix)
    {
        if (subDomainName == null || subDomainName.trim().length()==0)
        {
            return  mainDomainName + "." + domainSiffix;
        }
        if (subDomainName.equals(mainDomainName))
        {
            return mainDomainName + "." + domainSiffix;
        }
        return mainDomainName + "." +subDomainName+ "." +domainSiffix;
    }
    
    /**
     * Wyciaga subdomene z loginu
     * @param login
     * @return
     */
    protected String getSubdomain(String login)
    {
        if (login == null)
            return "";
        int position = login.indexOf(SUB_DOMAIN_SEPARATOR);
        if (position>=0)
        {
            return login.substring(0,position);
        }
        return "";
    }

    protected String getUsername(String login)
    {
        if (login == null)
            return "";
        int position = login.indexOf(SUB_DOMAIN_SEPARATOR);
        if (position>=0)
        {
            return login.substring(position+1,login.length());
        }
        return login;
    }

	public String getDomainSuffix() {
		return domainSuffix;
	}

	public String getMainDomainPrefix() {
		return mainDomainPrefix;
	}

	public String getAdminPass() {
		return adminPass;
	}

	public String getAdminUsername() {
		return adminUsername;
	}
	
	/**
	 * Zwraca np. ",DC="
	 * @return
	 */
	private String getMainDomainTagWith()
	{
		return getTag(mainDomainTag);
	}
	
	/**
	 * Zwraca np. ",DC="
	 * @return
	 */
	private String getSubDomainTagWith()
	{
		return getTag(subDomainTag);
	}
	
	/**
	 * Zwraca np. ",DC="
	 * @return
	 */
	private String getSiffixDomainTagWith()
	{
		return getTag(siffixDomainTag);
	}

	/**
	 * Zwraca Tag w formie: ",name="
	 * @param name
	 * @return
	 */
	public static String getTag(String name)
	{
		return "," +name + "=";
		
	}

	/**
	 * Zwraca principal dla uzytkowników,
	 * Uzywane do listowania uzytkowników w ldap
	 */
	public String getUsersPrincipal()
	{
		return  addCnUsersTag+"="+addCnUsersName+
    			(!StringUtils.isEmpty(mainDomainPrefix)? getMainDomainTagWith()+mainDomainPrefix:"");
	}
	
	/**
	 * Zwraca principal dla grupy aby wylistować istniejące grupy w strukturze
	 * @return
	 */
	public String getGroupsPrincipal()
	{
		return  addCnGroupsTag+"="+addCnGroupsName+
    			(!StringUtils.isEmpty(mainDomainPrefix)? getMainDomainTagWith()+mainDomainPrefix:"");
	}

	public boolean isAddCnUsers()
	{
		return addCnUsers;
	}

	public String getMainDomainTag()
	{
		return mainDomainTag;
	}
	
	public String getUserDomainPrefix() 
	{
		return userDomainPrefix;
	}

	 
	public String getSubDomainTag()
	{
		return subDomainTag;
	}

	public String getSiffixDomainTag()
	{
		return siffixDomainTag;
	}

	public String getAddCnUsersName()
	{
		return addCnUsersName;
	}

	public String getAddCnUsersTag()
	{
		return addCnUsersTag;
	}

	public String getAddUserLoginTag()
	{
		return addUserLoginTag;
	}

	public String getAddCnGroupsName()
	{
		return addCnGroupsName;
	}

	public String getAddCnGroupsTag()
	{
		return addCnGroupsTag;
	}
	
	
	
}


