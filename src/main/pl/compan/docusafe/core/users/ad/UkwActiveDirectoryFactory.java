package pl.compan.docusafe.core.users.ad;

public class UkwActiveDirectoryFactory extends ActiveDirectoryFactory {

	
	@Override
	/**
	 * Generuje principal dla UKW
	 * uid={username},ou=esod,DC=ukw,DC=edu,DC=pl
	 */
	public String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username) {
		return domainPrefix + "=" + username +
				ActiveDirectoryManager.getTag(ad.getAddCnUsersTag())+ad.getAddCnUsersName()+
				ActiveDirectoryManager.getTag(ad.getMainDomainTag())+ad.getUserDomainPrefix()+
				ActiveDirectoryManager.getTag(ad.getMainDomainTag())+ad.getSubDomainTag()+
				ActiveDirectoryManager.getTag(ad.getSiffixDomainTag())+ad.getDomainSuffix();
	}
}
