package pl.compan.docusafe.core.users.ad;

/**
 * Fabryka ustawia principal w konwncji
 * username@domain
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class PostDomainActiveDirectoryFactory extends ActiveDirectoryFactory
{

	@Override
	public String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username)
	{
		String principal = username;
		if (domainPrefix != null && domainPrefix.trim().length()>0) {
			principal += '@' + domainPrefix;
		}
		
		return principal.toLowerCase();
	}

}
