package pl.compan.docusafe.core.users.ad;

public class DefaultActiveDirectoryFactory extends ActiveDirectoryFactory
{

	@Override
	public String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username)
	{
		String principal = "";
		if (domainPrefix != null && domainPrefix.trim().length()>0) {
			principal = domainPrefix + "\\";
		}
		principal += username;
		
		return principal.toLowerCase();
	}
	
}
