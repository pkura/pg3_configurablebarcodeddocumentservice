package pl.compan.docusafe.core.users.auth;

import pl.compan.docusafe.util.StringManager;

import javax.security.auth.callback.*;
import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * Klasa s�u��ca do logowania u�ytkownik�w w �rodowisku aplikacji www.
 * W konstruktorze przekazywane s� dane u�ytkownika (nazwa, has�o),
 * a metoda {@link #handle(javax.security.auth.callback.Callback[])}
 * zwraca te warto�ci nie pytaj�c o nie u�ytkownika.
 * <p>
 * Obs�ugiwane s� nast�puj�ce obiekty implementuj�ce interfejs
 * {@link Callback}: {@link NameCallback}, {@link PasswordCallback},
 * {@link TextInputCallback}. TextInputCallback s�u�y do przekazania
 * nazwy sufiksu LDAP u�ywanego dla organizacji, do kt�rej nale�y
 * ten u�ytkownik.
 * <p>
 * Po napotkaniu obiektu innego ni� wymienione metoda handle rzuca
 * wyj�tek {@link UnsupportedCallbackException}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormCallbackHandler.java,v 1.4 2006/02/06 16:05:55 lk Exp $
 */
public class FormCallbackHandler implements CallbackHandler
{
    private X509Certificate certificate;
    private String username;
    private String password;
    private String clientAddr;

    private static StringManager sm = StringManager.getManager(Constants.Package);

    /**
     * Tworzy instancj� Callback z przygotowanymi danymi u�ytkownika.
     * Metoda handle nie pyta u�ytkownika o jego dane.
     * @param username
     * @param password
     * @throws NullPointerException Je�eli nazwa u�ytkownika,
     *  has�o lub ldapRoot maj� warto�� null.
     */
    public FormCallbackHandler(String username, String password, String clientAddr) //, String ldapRoot)
    {
        if (username == null)
            throw new NullPointerException(sm.getString("formCallback.nullParameter",
                "username"));
        if (password == null)
            throw new NullPointerException(sm.getString("formCallback.nullParameter",
                "password"));

        this.username = username;
        this.password = password;
        this.clientAddr = clientAddr;
    }

    public FormCallbackHandler(String username, String password, X509Certificate certificate, String clientAddr)
    {
        if (certificate == null)
            throw new NullPointerException("certificate");
        if (clientAddr == null)
            throw new NullPointerException("clientAddr");
        this.certificate = certificate;
        this.clientAddr = clientAddr;
        this.username = username;
        this.password = password;
    }

    /**
     * Uzupe�nia przekazane obiekty Callback danymi przekazanymi
     * w konstruktorze. Rozpoznaje tylko instancje Callback nale��ce
     * do klas NameCallback, PasswordCallback i TextInputCallback.
     *
     * @see CallbackHandler
     * @throws IOException
     * @throws UnsupportedCallbackException
     */
    public void handle(Callback[] callbacks)
        throws IOException, UnsupportedCallbackException
    {
        if (callbacks == null || callbacks.length == 0)
            return;

        // umieszczam w obiektach Callback informacje przekazane
        // w konstruktorze (w typowej implementacji nale�a�oby
        // w tym miejscu pyta� o te informacje u�ytkownika)
        for (int i=0; i < callbacks.length; i++)
        {
            Callback callback = callbacks[i];

            if (username != null && callback instanceof NameCallback)
            {
                ((NameCallback) callback).setName(username);
            }
            else if (password != null && callback instanceof PasswordCallback)
            {
                ((PasswordCallback) callback).setPassword(password.toCharArray());
            }
            else if (clientAddr != null && callback instanceof TextInputCallback)
            {
                TextInputCallback tic = (TextInputCallback) callback;
                if ("clientAddr".equals(tic.getPrompt()))
                    tic.setText(clientAddr);
            }
            else if (certificate != null && callback instanceof CertificateCallback)
            {
                ((CertificateCallback) callback).setCertificate(certificate);
            }
/*
            else
            {
                throw new UnsupportedCallbackException(callback,
                    sm.getString("formCallback.unsupportedCallback",
                        callback != null ? callback.getClass().getName() : "null"));
            }
*/
        }
    }
}
