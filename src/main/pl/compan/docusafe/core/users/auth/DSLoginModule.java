package pl.compan.docusafe.core.users.auth;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SignonLog;
import pl.compan.docusafe.core.news.NotificationsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListCounterManager;
import pl.compan.docusafe.core.office.workflow.snapshot.UserCounter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.parametrization.Parametrization;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

/**
 * Loguje u�ytkownika korzystaj�c z danych w serwerze LDAP.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DSLoginModule.java,v 1.23 2010/06/19 18:14:54 mariuszk Exp $
 */
public class DSLoginModule extends AbstractLoginModule
{
	
	static final Logger log = LoggerFactory.getLogger(DSLoginModule.class);
	
	public static final String ACTIVE_DIRECTORY_ON = "active.directory.on";
	public static final String ACTIVE_DIRECTORY_URL = "active.directory.url";
    
	public DSLoginModule()
    {
    }

    /**
     * Pobiera z LDAP dane u�ytkownika wskazanego w metodzie intialize
     * i por�wnuje has�a. Zwraca true, je�eli por�wnanie wypadnie pomy�lnie.
     * Dodaje do kolekcji {@link principals} obiekty UserPrincipal, RolePrincipal,
     * FullNamePrincipal i SubstitutedUserPrincipal.
     */
    protected boolean authenticate() throws EdmException
    {
        boolean validUser = false;
        boolean needCert = false;
        boolean openContextIfNeed = false;
        try
        {
            openContextIfNeed = DSApi.openContextIfNeeded();
            DSApi.context().begin();

            DSUser user=null;
            if (certificate != null)
            {
                user = DSUser.findByCertificate(certificate);
            	//System.out.println("jest certyfikat uzytkownika" + user.getName());
            	//System.out.println(user.getAuthlevel());
            	if(user.getAuthlevel()>1)
            	{
            		//System.out.println("auth wieksze od 1");
                    needCert = true;
            	}
            	else if(user.getAuthlevel()==1)
                {             
            		//System.out.println("poprawny user");
                	validUser = true;
                }
            }
            
            if(needCert)
            {
            	short lev = user.getAuthlevel();
            		
            	if(username != null)
            	{
            		if(lev==2)
            		{
            			if(user.getName().equals(username))
            			{
            				validUser = true;
            			}
            			else
            			{
                            throw new EdmException("Niepoprawny login");
            			}
            		}
            		else if(lev==3)
            		{
            			if (username != null && password != null)
            			{
            				if(user.getName().equals(username))
                			{
            					if(user.verifyPassword(password))
            					{
            						validUser = true;
            					}
            					else
            					{
                                    throw new EdmException("Niepoprawne has�o");
            					}
                			}
            			}
            			else
            			{
                            throw new EdmException("Aby zalogowa� u�ytkownika nale�y poda� login i has�o");
            			}
            		}
            	}
            	else
            	{
                     throw new EdmException("Aby zalogowa� u�ytkownika nale�y poda� login");
            	}
            }
            else if (username != null && password != null && validUser != true)
            {
            	try
            	{
            		user = DSUser.findByUsername(username);
                    if ( user.getSubstituteUser() != null) {
                        log.info("Uzytkownik jest zastepowany.");
                    }
            	}
            	catch (UserNotFoundException e) 
				{
            		try
            		{
            			user = DSUser.findByExternalName(username);
            		}
            		catch (UserNotFoundException e2) 
            		{
            			user = Parametrization.getInstance().addUserIsNotExist(username);
            		}
				}        
            	// walidowanie przez AD
            	if (user.isAdUser() && "true".equalsIgnoreCase(Docusafe.getAdditionProperty(ACTIVE_DIRECTORY_ON)))
            	{
            		log.trace("autoryzacja uzytkownika przez AD");
            		ActiveDirectoryManager ad = new ActiveDirectoryManager(Docusafe.getAdditionProperty(ACTIVE_DIRECTORY_URL));
            		validUser = ad.checkPassword(user,username, password);
            	}
            	else
            	{
            		validUser = Parametrization.getInstance().verifyPassword(user,password);
            	}
            }
            else
            {
            	if(validUser != true)
            	{
            		throw new EdmException("Nie przekazano certyfikatu ani nazwy u�ytkownika");            
            	}
            }

            if ((user.isCertificateLogin() && certificate == null) || (user.getAuthlevel()!=0 && validUser != true))
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik mo�e si� logowa� tylko przy pomocy certyfikatu");
                throw new EdmException("U�ytkownik "+username+" mo�e si� logowa� tylko " +
                    "przy pomocy certyfikatu");
            }

            if (user.isLoginDisabled())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik nieaktywny");
                throw new EdmException("U�ytkownik nieaktywny");
            }

            if (user.isDeleted())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik usuni�ty");
                throw new EdmException("U�ytkownik usuni�ty");
            }

            UserPrincipal userPrincipal = new UserPrincipal(user.getName());
            if (validUser)
            {
                principals.add(userPrincipal);

                if (certificate != null)
                    principals.add(new CertificatePrincipal(certificate));

                for (Iterator iter=user.getRoles().iterator(); iter.hasNext(); )
                {
                    principals.add(new RolePrincipal((String) iter.next()));
                }

                principals.add(new FullNamePrincipal(user.asFirstnameLastname()));

                if (user.getLastSuccessfulLogin() != null)
                {
                    userPrincipal.setLastSuccessfulLogin(user.getLastSuccessfulLogin());
                }
                
                
                if(user.getAdUser() != null)
                {
                	userPrincipal.setActiveDirectoryUser(user.getAdUser());
                }
                else
                {
                	userPrincipal.setActiveDirectoryUser(Boolean.FALSE);
                }

                if (user.getLastUnsuccessfulLogin() != null)
                {
                    userPrincipal.setLastUnsuccessfulLogin(user.getLastUnsuccessfulLogin());
                }	       		 
                user.setLastSuccessfulLogin(new Date());
                
                SubstituteHistory substitute = SubstituteHistory.findActualSubstitute(user.getName());
                user.setSubstitutionFromHistory(substitute);
                
                List<SubstituteHistory> substitutedInPast = SubstituteHistory.findActualSubstitutedWithoutThrough(user.getName());
                
                for(SubstituteHistory substituteHistory: substitutedInPast){
                	if(substituteHistory.getStatus()==SubstituteHistory.END){
                		substituteHistory.setStatus(SubstituteHistory.END);
                		
                		try{
                			DSUser su = DSUser.findByUsername(substituteHistory.getLogin());
                			su.setSubstitutionFromHistory(null);
                		}
                		catch(Exception e){
                			log.error(e.getMessage(),e);
                		}
                	}
                }
                
                List<SubstituteHistory> substitutedUsers =SubstituteHistory.findActualSubstituted(user.getName());
                // lista zast�powanych u�ytkownik�w
//                DSUser[] substitutedUsers = user.getSubstituted();

                for (SubstituteHistory substituteHistory : substitutedUsers) {
                	DSUser su = DSUser.findByUsername(substituteHistory.getLogin());
                	su.setSubstitutionFromHistory(substituteHistory);
                    principals.add(new SubstitutedUserPrincipal(su.getName(), su.asFirstnameLastname()));
                    
                    if(AvailabilityManager.isAvailable("Zastepstwa.dodajWpisdoDokumentowZastepowanego")){
                    	//wpisy do historii pism zastepowanego, o ile jeszcze ich nie ma
                    	StringManager sm = GlobalPreferences.loadPropertiesFile(DSLoginModule.class.getPackage().getName(),null);
                    	TaskList taskList = (TaskList) ServiceManager.getService(TaskList.NAME);
                    	List<Task> listaZadanUzytkownika=taskList.getTasks(su);
                    	for(Task task:listaZadanUzytkownika){
                    		boolean jestJuzWHistorii=false;
                    		DateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                    		for(Audit audi:OfficeDocument.find(task.getDocumentId()).getWorkHistory()){
                    			try{
                    				if(audi.getProperty()!=null&&audi.getDescription()!=null&&audi.getLparam()!=null&&audi.getWparam()!=null&&audi.getUsername()!=null){
                    					//nie ma powtarzajacego sie wpisu, lub jest, ale z data zakonczenia zastepstwa wieksza niz data poczatku nowego zastepstwa
                    					if(("substitute").equals(audi.getProperty())&&DSApi.context().getPrincipalName().equals(audi.getUsername())&&
                    							sm.getString("DokumentPrzejalWZastepstwie",user.asLastnameFirstname()).equals(audi.getDescription())&&task.getDocumentId().equals(audi.getWparam())
                    							&&DateUtils.parseDateAnyFormat(sdf.format(substituteHistory.getSubstitutedFrom())).compareTo(DateUtils.parseDateAnyFormat(audi.getLparam()))<=0)
                    						jestJuzWHistorii=true;
                    				}
                    			}catch (ParseException e) {
                    				//jak nie moze sparsowac lparam do daty, to znaczy, ze nie jest wpis z zastepstwa
                    			}
                    		}

                    		if(!jestJuzWHistorii)OfficeDocument.find(task.getDocumentId()).addWorkHistoryEntry(Audit.create("substitute", DSApi.context().getPrincipalName(), sm.getString("DokumentPrzejalWZastepstwie",user.asLastnameFirstname()),sdf.format(substituteHistory.getSubstitutedThrough()),task.getDocumentId()));
                    	}
                    }
				}

                SignonLog.logSignon(user.getName());
            }
            else
            {
                SignonLog.logFailedSignon(user.getName(), "Niepoprawne has�o");
                user.setLastUnsuccessfulLogin(new Date());
            }

            DSApi.context().commit();
        }
        catch (UserNotFoundException e)
        {
            SignonLog.logFailedSignon(username);
            DSApi.context().setRollbackOnly();
            throw e;
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            log.debug("", e);
            throw e;
        }
        catch (Exception e)
        {
            DSApi.context().setRollbackOnly();
            throw new EdmException("B��d logowania", e);
        }
        finally
        {
            DSApi.closeContextIfNeeded(openContextIfNeed);
        }

        return validUser;
    }



}
