package pl.compan.docusafe.core.users.auth;

import pl.compan.docusafe.util.StringManager;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.Principal;
import java.util.Date;
import java.util.Set;

/**
 * Obiekt reprezentuj�cy nazw� u�ytkownika w obiekcie Subject.
 * Obiekt Subject mo�e posiada� tylko jeden obiekt tej klasy
 * w kolekcji principals.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserPrincipal.java,v 1.5 2009/03/26 13:16:40 mariuszk Exp $
 */
public final class UserPrincipal implements Principal, Serializable
{
    private String name;
    private Date lastSuccessfulLogin;
    private Date lastUnsuccessfulLogin;
    private Boolean activeDirectoryUser;

    /* Jak to bylo tu umieszczone to sie zdrowo kaszanilo
     * private static final StringManager sm =
        StringManager.getManager(Constants.Package);*/

    public UserPrincipal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public Date getLastSuccessfulLogin()
    {
        return lastSuccessfulLogin;
    }

    public void setLastSuccessfulLogin(Date lastSuccessfulLogin)
    {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public Date getLastUnsuccessfulLogin()
    {
        return lastUnsuccessfulLogin;
    }

    public void setLastUnsuccessfulLogin(Date lastUnsuccessfulLogin)
    {
        this.lastUnsuccessfulLogin = lastUnsuccessfulLogin;
    }

    public String toString()
    {
        return getClass().getName()+"["+name+"]";
    }

    /**
     * Pobiera z przekazanej sesji obiekt Subject, a z niego
     * obiekt UserPrincipal. Je�eli w obiekcie Subject nie
     * ma obiektu UserPrincipal, rzucany jest wyj�tek
     * MissingUserPrincipalException. Je�eli w sesji nie ma
     * obiektu Subject, zwracana jest warto�� null.
     */
    public static UserPrincipal getUserPrincipal(HttpSession session)
    throws MissingUserPrincipalException
    {
        Subject subject = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

        if (subject != null)
        {
            Set users = subject.getPrincipals(UserPrincipal.class);
            if (users == null || users.size() == 0)
                throw new MissingUserPrincipalException("Brakuje danych uzytkownika");

            return (UserPrincipal) users.iterator().next();
        }
        else
        {
            return null;
        }
    }

	public void setActiveDirectoryUser(Boolean activeDirectoryUser)
	{
		this.activeDirectoryUser = activeDirectoryUser;
	}

	public Boolean getActiveDirectoryUser()
	{
		return activeDirectoryUser;
	}
}
