package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SubstitutedUserPrincipal.java,v 1.2 2006/02/20 15:42:27 lk Exp $
 */
public class SubstitutedUserPrincipal implements Principal, Serializable
{
    private String name;
    private String fullname;

    public SubstitutedUserPrincipal(String name, String fullname)
    {
        this.name = name;
        this.fullname = fullname;
    }

    public String getName()
    {
        return name;
    }

    public String getFullname()
    {
        return fullname;
    }

    public String toString()
    {
        return fullname;
    }
}
