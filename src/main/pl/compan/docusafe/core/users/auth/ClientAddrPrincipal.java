package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * Reprezentuje adres komputera, z kt�rego zalogowa� si� u�ytkownik.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ClientAddrPrincipal.java,v 1.2 2006/02/20 15:42:27 lk Exp $
 */
public class ClientAddrPrincipal implements Principal, Serializable
{
    private String name;

    public ClientAddrPrincipal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
