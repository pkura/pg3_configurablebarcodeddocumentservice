package pl.compan.docusafe.core.users.auth;

import com.google.common.collect.Lists;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * User: Tomasz
 * Date: 17.03.14
 * Time: 14:05
 */
public class SsoTokenStore {
    private static Logger LOG = LoggerFactory.getLogger(SsoTokenStore.class);
    private static SsoTokenStore INSTANCE = new SsoTokenStore();

    private ConcurrentMap<String, Token> tokens = new ConcurrentHashMap<String, Token>();
    private SecureRandom random = new SecureRandom();
    private SsoTokenStore() {};

    public static SsoTokenStore instance() { return INSTANCE; };

    public synchronized String generateTokenForCurrentUser() {
        if(tokens.size() > 100) {
            cleanUp();
        }

        String tokenKey =  Math.abs(random.nextLong()) + ":" + Math.abs(random.nextLong()) + ":" + Math.abs(random.nextLong());
        long currentTime = System.currentTimeMillis();
        long validTime = currentTime + (30 * 1000);

        Token token = new Token();
        token.login = DSApi.context().getPrincipalName();
        token.validUntilTime = validTime;

        tokens.put(tokenKey, token);

        LOG.info("token map = {}",tokens);
        return tokenKey;
    }

    public synchronized String getLoginForToken(String tokenKey) {
        Token token = tokenKey != null ? tokens.get(tokenKey) : null;
        if(token != null) {
            tokens.remove(token);
            if(token.validUntilTime > System.currentTimeMillis()) {
                return token.login;
            }
        }
        return null;
    }

    private static class Token {
        String login;
        long validUntilTime;
    }

    private void cleanUp() {
        long currentTime = System.currentTimeMillis();
        List<String> removeList = Lists.newArrayList();
        for(Map.Entry<String, Token> entry : tokens.entrySet()) {
            if(currentTime > entry.getValue().validUntilTime) {
                removeList.add(entry.getKey());
            }
        }
        for(String key : removeList) {
            tokens.remove(key);
        }
    }

}
