package pl.compan.docusafe.core.users.auth;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.util.Base64;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Klasa bazowa dla modu��w realizuj�cych autentykacj� u�ytkownika.
 * Nazwa u�ytkownika przekazana w obiekcie Callback jest konwertowana
 * do ma�ych liter.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AbstractLoginModule.java,v 1.4 2006/02/06 16:05:55 lk Exp $
 */
public abstract class AbstractLoginModule implements LoginModule
{
    private static final Log log = LogFactory.getLog(AbstractLoginModule.class);

    // nazwy parametr�w przekazywanych w konfiguracji modu�u
    public static final String PARAM_DEBUG = "debug";
    public static final String PARAM_DIGEST = "digest";

    private static final String DEFAULT_DIGEST = "SHA";

    protected Subject subject;
    protected Set<Principal> principals = new HashSet<Principal>();
    protected MessageDigest md;

    protected X509Certificate certificate;
    protected String username;
    protected String password;
    protected String clientAddr;

    protected boolean debug;

    protected boolean loginSucceeded;

    public void initialize(Subject subject, CallbackHandler callbackHandler,
                           Map sharedState, Map options)
    {
        if ("true".equalsIgnoreCase((String) options.get(PARAM_DEBUG)))
            debug = true;

        String digest = (String) options.get(PARAM_DIGEST);
        try
        {
            md = MessageDigest.getInstance(digest != null ? digest : DEFAULT_DIGEST);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalArgumentException("Nie istnieje wymagany algorytm szyfrowania hase�: "+digest);
        }

        this.subject = subject;
        try
        {
            Callback[] callbacks = new Callback[4];
            // napisy przekazywane obiektom Callback jako parametr "prompt"
            // nie s� nigdzie wy�wietlane, s� tu tylko w celu informacyjnym
            callbacks[0] = new NameCallback("Nazwa u�ytkownika");
            callbacks[1] = new PasswordCallback("Has�o", false);
            callbacks[2] = new TextInputCallback("clientAddr");
            callbacks[3] = new CertificateCallback();
            callbackHandler.handle(callbacks);

            NameCallback cbUsername = (NameCallback) callbacks[0];
            PasswordCallback cbPassword = (PasswordCallback) callbacks[1];
            TextInputCallback cbClientAddr = (TextInputCallback) callbacks[2];
            CertificateCallback cbCertificate = (CertificateCallback) callbacks[3];

            username = cbUsername.getName() != null ? cbUsername.getName().toLowerCase() : null;
            password = cbPassword.getPassword() != null ? new String(cbPassword.getPassword()) : null;
            clientAddr = cbClientAddr.getText();
            certificate = cbCertificate.getCertificate();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new RuntimeException("B��d autentykacji", e);
        }
    }

    /**
     * Przeprowadza autentykacj� u�ytkownika, ale nie umieszcza klas
     * Principal w obiekcie Subject.
     * @return True, je�eli autentykacja si� powiod�a.
     * @throws javax.security.auth.login.FailedLoginException Je�eli has�o by�o nieprawid�owe.
     */
    public final boolean login() throws LoginException
    {
        loginSucceeded = false;
        
        try
        {
            if (authenticate())
            {
                loginSucceeded = true;
            }
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            throw new FailedLoginException("B��d podczas autentykacji u�ytkownika "+ username + " z adresu "+ clientAddr );
        }

        if (!loginSucceeded)
            throw new FailedLoginException("Niepoprawne has�o lub nazwa u�ytkownika "+ username + " z adresu "+ clientAddr);

        // zawsze zwracam true, je�eli autentykacja si� nie powiod�a, nale�y
        // rzuci� wyj�tek FailedLoginException (wy�ej)
        return true;
    }

    public final boolean commit() throws LoginException
    {
        if (loginSucceeded)
        {
            principals.add(new ClientAddrPrincipal(clientAddr));
            subject.getPrincipals().addAll(principals);
            subject.setReadOnly();
            principals.clear();
            return true;
                    }
        else
        {
            principals.clear();
            return false;
        }

    }

    public final boolean abort() throws LoginException
    {
        principals.clear();

        if (loginSucceeded)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean logout() throws LoginException
    {
        if (subject.isReadOnly())
        {
            // miejsce na usuni�cie subject.credentials
        }
        else
        {
            // usuwane s� tylko obiekty umieszczane w Subject przez t�
            // klas�
            subject.getPrincipals().removeAll(subject.getPrincipals(UserPrincipal.class));
            subject.getPrincipals().removeAll(subject.getPrincipals(RolePrincipal.class));
            subject.getPrincipals().removeAll(subject.getPrincipals(FullNamePrincipal.class));
            subject.getPrincipals().removeAll(subject.getPrincipals(SubstitutedUserPrincipal.class));
        }

        return true;
            }

    /**
     * Zwraca zaszyfrowane has�o. Hasz zwr�cony przez funkcj� szyfruj�c�
     * jest kodowany przy pomocy base64, a na pocz�tku do��czana jest nazwa
     * szyfrowania w nawiasach klamrowych, np. "{SHA}".
     * <p>
     * Funkcja utworzona na bazie org.apache.catalina.realm.RealmBase.digest(String).
     * @param credentials
     */
    protected String digest(String credentials)
    {
        md.reset();
        md.update(credentials.getBytes());
        //return "{"+digest+"}"+pl.compan.docusafe.util.HexUtils.convert(md.digest());
        return "{"+md.getAlgorithm()+"}"+new String(Base64.encode(md.digest()));
    }

    /**
     * Metoda, kt�ra sprawdza, czy warto�ci p�l {@link #username} i {@link #password}
     * okre�laj� istniej�cego u�ytkownika i dodaje do kolekcji {@link #principals}
     * obiekty {@link UserPrincipal} oraz {@link RolePrincipal}.
     * @return True, je�eli autentykacja u�ytkownika si� powiod�a.
     * @throws EdmException
     */
    protected abstract boolean authenticate() throws EdmException;
}
