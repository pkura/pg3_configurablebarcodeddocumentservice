package pl.compan.docusafe.core.users.auth;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SignonLog;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.NetUtil;

public class IISLogin  extends AbstractLoginModule
{
	static final Logger log = LoggerFactory.getLogger(IISLogin.class);
	
	public IISLogin()
	{
	}



	public static void login(HttpServletRequest request, HttpServletResponse response, String username) throws LoginException, EdmException, IOException
	{
		LoginContext lc;

		/*
		 * com.sun.security.auth.module.NTSystem NTSystem = new
		 * com.sun.security.auth.module.NTSystem();
		 * System.out.println(NTSystem.getName());
		 */

		// String username = assertion.getPrincipal().getName();
		lc = new LoginContext("DocuSafe", new FormCallbackHandler(username, "",
				NetUtil.getClientAddr(request).getIp()));

		lc.login();

		request.getSession().setAttribute(
				pl.compan.docusafe.web.filter.AuthFilter.LANGUAGE_KEY,
				GlobalPreferences.getUserLocal(lc.getSubject()));

		if ((GlobalPreferences.EMERGENCY == true)
				&& (GlobalPreferences.getFreeUser(username) == null)) {
			request.setAttribute("emergency", Boolean.TRUE);
		} else {
			DSUser.correctLogin(username);
			request.getSession(true).setAttribute(
					pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY,
					lc.getSubject());

		/*	String sessionTable = Docusafe
					.getAdditionProperty("phpbb.sessionTable");
			if (sessionTable != null)
				AuthUtil.createPhpbbSession(request, sessionTable);
*/
		}

		response.sendRedirect(request.getContextPath()
				+ "/office/tasklist/user-task-list.action");

	}
	
	/**
	 * zmodyfikowana funkcja logowania u�ytkownika - nie jest wymagane has�o, jedynie username
	 */
	protected boolean authenticate() throws EdmException
    {
		boolean validUser = false;
        boolean needCert = false;
        try
        {
            DSApi.openAdmin();
            DSApi.context().begin();

            DSUser user=null;
           
            if (username != null && password != null && validUser != true)
            {
            	try
            	{
            		//System.out.println(username +  " nazwa loginu");
            		user = DSUser.findByUsername(username);
            	}
            	catch (UserNotFoundException e) 
				{
            		 user = DSUser.findByExternalName(username);
				}        
            	
            	{
            		//validUser = user.verifyPassword(password);
            	}
            	validUser= true;
            }
            else
            {
            	if(validUser != true)
            	{
            		throw new EdmException("Nie przekazano certyfikatu ani nazwy u�ytkownika");            
            	}
            }

            if ((user.isCertificateLogin() && certificate == null) || (user.getAuthlevel()!=0 && validUser != true))
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik mo�e si� logowa� tylko przy pomocy certyfikatu");
                throw new EdmException("U�ytkownik "+username+" mo�e si� logowa� tylko " +
                    "przy pomocy certyfikatu");
            }

            if (user.isLoginDisabled())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik nieaktywny");
                throw new EdmException("U�ytkownik nieaktywny");
            }

            if (user.isDeleted())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik usuni�ty");
                throw new EdmException("U�ytkownik usuni�ty");
            }

            UserPrincipal userPrincipal = new UserPrincipal(user.getName());
            if (validUser)
            {
                principals.add(userPrincipal);

                if (certificate != null)
                    principals.add(new CertificatePrincipal(certificate));

                for (Iterator iter=user.getRoles().iterator(); iter.hasNext(); )
                {
                    principals.add(new RolePrincipal((String) iter.next()));
                }

                principals.add(new FullNamePrincipal(user.asFirstnameLastname()));

                if (user.getLastSuccessfulLogin() != null)
                {
                    userPrincipal.setLastSuccessfulLogin(user.getLastSuccessfulLogin());
                }
                
                
                if(user.getAdUser() != null)
                {
                	userPrincipal.setActiveDirectoryUser(user.getAdUser());
                }
                else
                {
                	userPrincipal.setActiveDirectoryUser(Boolean.FALSE);
                }

                if (user.getLastUnsuccessfulLogin() != null)
                {
                    userPrincipal.setLastUnsuccessfulLogin(user.getLastUnsuccessfulLogin());
                }	       		 
                user.setLastSuccessfulLogin(new Date());

                // lista zast�powanych u�ytkownik�w
                DSUser[] substitutedUsers = user.getSubstituted();

                for (int i=0; i < substitutedUsers.length; i++)
                {
                    DSUser su = substitutedUsers[i];
                    principals.add(new SubstitutedUserPrincipal(su.getName(), su.asFirstnameLastname()));
                }

                SignonLog.logSignon(user.getName());
            }
            else
            {
                SignonLog.logFailedSignon(user.getName(), "Niepoprawne has�o");
                user.setLastUnsuccessfulLogin(new Date());
            }

            DSApi.context().commit();
        }
        catch (UserNotFoundException e)
        {
            SignonLog.logFailedSignon(username);
            DSApi.context().setRollbackOnly();
            throw e;
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            log.debug("", e);
            throw e;
        }
        catch (Exception e)
        {
            DSApi.context().setRollbackOnly();
            throw new EdmException("B��d logowania", e);
        }
        finally
        {
            DSApi._close();
        }

        return validUser;
    }

}
