package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * Anonimowy u�ytkownik. Klasa pomocnicza u�ywana w sytuacji, gdy
 * u�ytkownik nie jest zalogowany, ale wymagany jest jego dost�p
 * do jakiej� cz�ci serwisu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AnonymousUserPrincipal.java,v 1.2 2006/02/20 15:42:26 lk Exp $
 */
public final class AnonymousUserPrincipal implements Principal, Serializable
{
    public static final AnonymousUserPrincipal PRINCIPAL = new AnonymousUserPrincipal("");

    private String name;

    private AnonymousUserPrincipal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
