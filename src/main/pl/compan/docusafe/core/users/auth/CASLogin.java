package pl.compan.docusafe.core.users.auth;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;


import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.opensymphony.webwork.ServletActionContext;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;

import pl.compan.docusafe.core.xes.XesLog;
import pl.compan.docusafe.core.xes.XesLoggedActionsTypes;
import pl.compan.docusafe.parametrization.pg.CASApi;
import pl.compan.docusafe.parametrization.pg.integration.IntegrationFactory;
import pl.compan.docusafe.util.NetUtil;
import pl.docusafe.pg.model.Person;

/**
 * Loguje u�ytkownika o ile poprawnie zalogowa� si� na serwerze CAS. Do logowania u�ywa zwr�con� warto�� username
 *
 * aby w��czy� logowanie CAS, nale�y:
 *  -ustawi� w "login.config" jako pierwsz� klas� "pl.compan.docusafe.core.users.auth.CASLogin sufficient"
 *  -ustawi� odpowiednie parametry w adds.properties
 *  	cas.enabled= true
 *  	cas.server.name= localhost:8080
 *  	cas.server.url_prefix= https://opzz-dev.demo.eo.pl/cas-web/
 *  	cas.server.login_url = https://opzz-dev.demo.eo.pl/cas-web/login
 *  	cas.server.logout_url = https://opzz-dev.demo.eo.pl/cas-web/logout
 * @author Bartosz Kaliszewski
 */
public class CASLogin extends AbstractLoginModule
{
	static final Logger log = LoggerFactory.getLogger(CASLogin.class);
	
	public CASLogin()
	{
	}
	
	public static void loginCAS(HttpServletRequest request, HttpServletResponse response, Assertion assertion) throws ServletException, IOException
	{
		LoginContext lc;
        boolean contextOpen = false;
		try {

            contextOpen = DSApi.openContextIfNeeded();
            DSApi.beginTransacionSafely();
			
			String username = assertion.getPrincipal().getName();
            log.error("cas username = {}", username);
            if(AvailabilityManager.isAvailable("cas.import.user"))
            {
                try
                {
                	
                    DSUser user = DSUser.findByExternalName(username);
                    username=user.getName();
                    user.setLastSuccessfulLogin(new Date());
                }
                catch(UserNotFoundException une)
                {

                	log.error("username= "+username);
                	System.out.print("username= "+username);

                    IntegrationFactory factory = new IntegrationFactory();
                    DSUser user = factory.createDSUser(username);
                    username = user.getName();
                    DSApi.context().session().save(user);
                    DSApi.context().session().flush();
                    
                }
            }

			lc = new LoginContext("DocuSafe", new FormCallbackHandler(username,
					"", request != null ? request.getRemoteAddr() : ""));

			lc.login();

			request.getSession().setAttribute(
					pl.compan.docusafe.web.filter.AuthFilter.LANGUAGE_KEY,
					GlobalPreferences.getUserLocal(lc.getSubject()));
			
			if ((GlobalPreferences.EMERGENCY == true)
					&& (GlobalPreferences.getFreeUser(username) == null)) {
				request.setAttribute("emergency", Boolean.TRUE);
			} else {
				DSUser.correctLogin(username);
				request.getSession(true).setAttribute(
						pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY,
						lc.getSubject());

				String sessionTable = Docusafe
						.getAdditionProperty("phpbb.sessionTable");
				if (sessionTable != null)
					AuthUtil.createPhpbbSession(request, sessionTable);

			}

            String uri = request.getRequestURI();
            if(! uri.matches("login\\.jsp")) {
                response.sendRedirect(uri.replaceFirst(";jsessionid(.*)$", ""));
            } else {
                response.sendRedirect(request.getContextPath()
                        + "/office/tasklist/user-task-list.action");
            }

		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServletException(e.getMessage(), e);
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServletException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServletException(e.getMessage(), e);
		}
        finally
        {
            DSApi.closeContextIfNeeded(contextOpen);
        }
	}
	
	
	/**
	 * zmodyfikowana funkcja logowania u�ytkownika - nie jest wymagane has�o, jedynie username
	 */
	protected boolean authenticate() throws EdmException
    {
		boolean validUser = false, contextOpen = false;
        boolean needCert = false;
        try
        {
            contextOpen = DSApi.openContextIfNeeded();
            DSApi.beginTransacionSafely();

            DSUser user=null;

            if (username != null && password != null && validUser != true)
            {
            	try
            	{
            		user = DSUser.findByUsername(username);
            	}
            	catch (UserNotFoundException e) 
				{
                    user = DSUser.findByExternalName(username);
                }

            	validUser= true;
            }
            else
            {
            	if(validUser != true)
            	{
            		throw new EdmException("Nie przekazano certyfikatu ani nazwy u�ytkownika");            
            	}
            }

            if ((user.isCertificateLogin() && certificate == null) || (user.getAuthlevel()!=0 && validUser != true))
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik mo�e si� logowa� tylko przy pomocy certyfikatu");
                throw new EdmException("U�ytkownik "+username+" mo�e si� logowa� tylko " +
                    "przy pomocy certyfikatu");
            }

            if (user.isLoginDisabled())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik nieaktywny");
                throw new EdmException("U�ytkownik nieaktywny");
            }

            if (user.isDeleted())
            {
                SignonLog.logFailedSignon(user.getName(), "U�ytkownik usuni�ty");
                throw new EdmException("U�ytkownik usuni�ty");
            }

            UserPrincipal userPrincipal = new UserPrincipal(user.getName());
            if (validUser)
            {
                principals.add(userPrincipal);

                if (certificate != null)
                    principals.add(new CertificatePrincipal(certificate));

                for (Iterator iter=user.getRoles().iterator(); iter.hasNext(); )
                {
                    principals.add(new RolePrincipal((String) iter.next()));
                }

                principals.add(new FullNamePrincipal(user.asFirstnameLastname()));

                if (user.getLastSuccessfulLogin() != null)
                {
                    userPrincipal.setLastSuccessfulLogin(user.getLastSuccessfulLogin());
                }
                
                
                if(user.getAdUser() != null)
                {
                	userPrincipal.setActiveDirectoryUser(user.getAdUser());
                }
                else
                {
                	userPrincipal.setActiveDirectoryUser(Boolean.FALSE);
                }

                if (user.getLastUnsuccessfulLogin() != null)
                {
                    userPrincipal.setLastUnsuccessfulLogin(user.getLastUnsuccessfulLogin());
                }	       		 
                user.setLastSuccessfulLogin(new Date());

                // lista zast�powanych u�ytkownik�w
                DSUser[] substitutedUsers = user.getSubstituted();

                for (int i=0; i < substitutedUsers.length; i++)
                { 
                    DSUser su = substitutedUsers[i];
                    
                    principals.add(new SubstitutedUserPrincipal(su.getName(), su.asFirstnameLastname()));
                }

                SignonLog.logSignon(user.getName());
            }
            else
            {
                SignonLog.logFailedSignon(user.getName(), "Niepoprawne has�o");
                user.setLastUnsuccessfulLogin(new Date());
            }

            DSApi.context().session().flush();
            if(XesLoggedActionsTypes.isActionLoggingEnableByUriAndAction("login", null)){
            	String uri="";
            	if (ServletActionContext.getRequest()!=null){
            		uri =ServletActionContext.getRequest().getRequestURI();
            	}else{
            		uri = "/docusafe/login.jsp";
            	}
            	XesLog.createLoginLogoutEvent(user ,"login", this.toString() ,uri,true);
            }
        }
        catch (UserNotFoundException e)
        {
            SignonLog.logFailedSignon(username);
            DSApi.context().setRollbackOnly();
            throw e;
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            log.debug("", e);
            throw e;
        }
        catch (Exception e)
        {
            DSApi.context().setRollbackOnly();
            throw new EdmException("B��d logowania", e);
        }
        finally
        {
            DSApi.closeContextIfNeeded(contextOpen);
        }

        return validUser;
    }

}
