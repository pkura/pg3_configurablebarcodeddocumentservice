package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * Obiekt reprezentuj�cy rol� u�ytkownika w obiekcie Subject.
 * Obiekt Subject mo�e posiada� zero lub wi�cej obiekt�w
 * RolePrincipal.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RolePrincipal.java,v 1.2 2006/02/20 15:42:27 lk Exp $
 */
public final class RolePrincipal implements Principal, Serializable
{
    private String name;

    public RolePrincipal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public String toString()
    {
        return getClass().getName()+"["+name+"]";
    }
}
