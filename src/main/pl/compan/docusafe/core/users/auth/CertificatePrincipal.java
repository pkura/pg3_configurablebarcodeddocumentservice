package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;
import java.security.cert.X509Certificate;

/* User: Administrator, Date: 2005-05-19 09:40:23 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CertificatePrincipal.java,v 1.2 2006/02/20 15:42:26 lk Exp $
 */
public class CertificatePrincipal implements Principal, Serializable
{
    private X509Certificate certificate;

    public CertificatePrincipal(X509Certificate certificate)
    {
        this.certificate = certificate;
    }

    public String getName()
    {
        try
        {
            return certificate.getSubjectDN().getName();
        }
        catch (Exception e)
        {
            return "[CertificatePrincipal: "+e.toString()+"]";  
        }
    }

    public X509Certificate getCertificate()
    {
        return certificate;
    }
}
