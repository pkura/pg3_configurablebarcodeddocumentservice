package pl.compan.docusafe.core.users.auth;

import javax.security.auth.callback.Callback;
import java.security.cert.X509Certificate;

/* User: Administrator, Date: 2005-05-19 11:23:01 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: CertificateCallback.java,v 1.1 2005/05/19 14:51:51 lk Exp $
 */
public class CertificateCallback implements Callback
{
    private X509Certificate certificate;

    public X509Certificate getCertificate()
    {
        return certificate;
    }

    public void setCertificate(X509Certificate certificate)
    {
        this.certificate = certificate;
    }
}
