package pl.compan.docusafe.core.users.auth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.security.cert.CertificateEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import de.schlichtherle.io.File;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.NetUtil;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AuthUtil.java,v 1.10 2006/02/20 15:42:26 lk Exp $
 */
public class AuthUtil
{
    private static final Random random = new Random(System.currentTimeMillis());
    private final static Logger log = LoggerFactory.getLogger(AuthUtil.class);

    private static final CertificateFactory cf;
    static
    {
        try
        {
            cf = CertificateFactory.getInstance("X509");
        }
        catch (CertificateException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }

    private AuthUtil()
    {
    }

    public java.security.cert.X509Certificate getCertificate(
        javax.security.cert.X509Certificate cert) throws EdmException
    {
        synchronized (cf)
        {
            try
            {
                return (java.security.cert.X509Certificate) cf.generateCertificate(new ByteArrayInputStream(cert.getEncoded()));
            }
            catch (CertificateException e)
            {
                throw new EdmException(e.getMessage(), e);
            }
            catch (CertificateEncodingException e)
            {
                throw new EdmException(e.getMessage(), e);
            }
        }
    }

/*
    public static Subject getAdminSubject()
    {
        Set principals = new HashSet();
        principals.add(new UserPrincipal("admin"));
        principals.add(new RolePrincipal("admin"));
        Subject subject = new Subject(true, principals, Collections.EMPTY_SET,
            Collections.EMPTY_SET);
        return subject;
    }
*/

    /**
     * Zwraca obiekt Subject umieszczony w sesji u�ytkownika
     * lub null, je�eli u�ytkownik nie jest zalogowany.
     */
    public static Subject getSubject(HttpServletRequest request)
    {
        Subject subject = null;
        if(request != null)
        {
            HttpSession session = request.getSession();
            if (session != null)
                subject = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
        }
        if(subject == null)
        {
            subject = AbstractAuthenticator.getThreadSubject();
        }
        return subject;
    }

    public static boolean hasRole(Subject subject, String role)
    {
        if (subject == null)
            throw new NullPointerException("subject");

        Set rolePrincipals = subject.getPrincipals(RolePrincipal.class);
        if (rolePrincipals != null && rolePrincipals.size() > 0)
        {
            for (Iterator iter=rolePrincipals.iterator(); iter.hasNext(); )
            {
                if (((RolePrincipal) iter.next()).getName().equals(role))
                    return true;
            }
        }

        return false;
    }

    public static String[] getRoles(Subject subject)
    {
        if (subject == null)
            throw new NullPointerException("subject");

        Set rolePrincipals = subject.getPrincipals(RolePrincipal.class);
        if (rolePrincipals != null && rolePrincipals.size() > 0)
        {
            String[] roles = new String[rolePrincipals.size()];
            Iterator iter=rolePrincipals.iterator();
            int i=0;
            while (iter.hasNext()) { roles[i++] = ((RolePrincipal) iter.next()).getName(); }
            return roles;
        }

        return new String[0];
    }

    public static UserPrincipal getUserPrincipal(HttpServletRequest request)
    {
        return getUserPrincipal(getSubject(request));
    }

    /**
     * Zwraca obiekt UserPrincipal zawarty w przekazanym obiekcie Subject.
     * Je�eli przekazany obiekt jest r�wny null, zwracana jest warto�� null.
     * @throws IllegalArgumentException Je�eli w przekazanym obiekcie
     * Subject brakuje obiektu UserPrincipal.
     */
    public static UserPrincipal getUserPrincipal(Subject subject)
    {
        if (subject == null) return null;
        Set users = subject.getPrincipals(UserPrincipal.class);
        if (users != null && users.size() > 0)
            return (UserPrincipal) users.iterator().next();
        else
            throw new IllegalArgumentException("Brak obiektu UserPrincipal "+
                "w przekazanym obiekcie Subject");
    }

    public static String generatePassword(int length)
    {
        if (length < 1)
            throw new IllegalArgumentException("Has�o musi mie� przynajmniej jeden znak.");

        char[] password = new char[length];

        for (int i=0; i < length; i++)
        {
            int selector = random.nextInt(3);
            switch (selector)
            {
                case 0: password[i] = (char) (random.nextInt('z'-'a'+1) + 'a'); break;
                case 1: password[i] = (char) (random.nextInt('Z'-'A'+1) + 'A'); break;
                case 2: password[i] = (char) (random.nextInt('9'-'1'+1) + '1'); break;
            }
        }

        for (int i=0; i < password.length; i++)
        {
            if (password[i] == 'l') password[i] = 'f';
            else if (password[i] == '1') password[i] = 'g';
            else if (password[i] == '0') password[i] = '8';
            else if (password[i] == 'O') password[i] = '7';
            else if (password[i] == 'I') password[i] = 'W';
        }

        return new String(password);
    }
    
    public static void createPhpbbSession(HttpServletRequest request, String sessionTable) throws Exception {
    	if(sessionTable != null) {
			DSApi.openAdmin();
			DSApi.context().begin();
			String sessionId = request.getSession().getId();
			PreparedStatement php;
			// sprawdzamy czy istnieje ju� sesja w tabeli sessionTable
			PreparedStatement phpCheckSession = DSApi.context().prepareStatement("SELECT session_id FROM " + sessionTable + " WHERE session_id=?");
			phpCheckSession.setString(1, sessionId);
			if(!phpCheckSession.executeQuery().isBeforeFirst()) { // == false-> nie ma takiej sesji w tabeli
				php = DSApi.context().prepareStatement("INSERT INTO " + sessionTable + 
						" VALUES(?,(select user_id FROM phpbb_users WHERE username_clean = ?), 0, 0, ?, ?, ?, ?, '', 'index.php', 0, 0, ?)");
			} else {
				StringBuilder query = new StringBuilder("update ");
				query.append(sessionTable);
				query.append(" set session_id=?, session_user_id=(select user_id FROM phpbb_users WHERE username_clean = ?), ");
				query.append("session_start=?, session_time=?, session_ip=?, session_browser=?, session_admin=?");
				php = DSApi.context().prepareStatement(query);
			}
			
			
			php.setString(1, sessionId);
			php.setString(2, getUserPrincipal(request).getName());
			php.setLong(3, (new java.util.Date()).getTime() / 1000);
			php.setLong(4, (new java.util.Date()).getTime() / 1000 + 86400); // next day
			php.setString(5, request.getRemoteAddr());
			php.setString(6, request.getHeader("User-Agent"));
			php.setBoolean(7, DSApi.context().getDSUser().isAdmin());
			
			php.executeUpdate();
			DSApi.context().closeStatement(php);
			DSApi.context().commit();			
			DSApi.close();
		}
    }
    
    public static void destroyPhpbbSession(HttpSession session, String sessionTable) {
         if(sessionTable != null) {
         	try {
					DSApi.openAdmin();
					DSApi.context().begin();
					PreparedStatement ps = DSApi.context().prepareStatement("DELETE FROM " + sessionTable + " WHERE session_id=?");
					ps.setString(1, session.getId());
					ps.executeUpdate();
					DSApi.context().closeStatement(ps);
					DSApi.context().commit();
				} catch (Exception e) {
					//
				}
         	
         }
    }

    /**
     * Zwraca login context "BasicAuthentication" z pliku login.config w HOME, kt�ry powinnien byc bazowym kontextem logowania
     *
     *
     * @param username
     * @param password
     * @param request
     * @return
     * @throws LoginException
     */
    public static LoginContext getBasicLoginContext(String username, String password, HttpServletRequest request) throws LoginException {
        return new LoginContext(AdditionManager.getPropertyOrDefault("authutil.basicLoginContext", "DocuSafe"), new FormCallbackHandler(username, password,
                request != null ? request.getRemoteAddr() : ""));
    }

    /**
     * Zwraca login context z pliku login.config w HOME.
     * @param username
     * @param password
     * @param request
     * @return
     * @throws LoginException
     */
    public static LoginContext getLoginContext(String username, String password, HttpServletRequest request) throws LoginException {
        return new LoginContext(AdditionManager.getPropertyOrDefault("authutil.loginContext", "DocuSafe"), new FormCallbackHandler(username, password,
                        request != null ? request.getRemoteAddr() : ""));
    }

    /**
     * Loguje u�ytkownika
     * @param context
     * @param request
     * @return
     * @throws javax.security.auth.login.LoginException
     */
    public static Subject login(String username, String password,HttpServletRequest request) throws LoginException {
        LoginContext lc = AuthUtil.getBasicLoginContext(username, password, request);
        lc.login();
        return lc.getSubject();
    }

    /**
     * Loguje u�ytkownika za pomoc� przekazanego certyfiaktu
     * @param login
     * @param certificate Bas64 certyfikatu
     * @param request
     * @return
     */
    public static LoginContext loginByCertificate(String certificate, HttpServletRequest request) throws LoginException, EdmException, IOException {
        X509Certificate cert = parseCertificateFromBase64(certificate);

        return  new LoginContext(AdditionManager.getPropertyOrDefault("authutil.basicLoginContext", "DocuSafe"),
                new FormCallbackHandler(null, null, cert,
                request != null ? request.getRemoteAddr() : ""));
    }

    protected static X509Certificate parseCertificateFromBase64(String certificate) throws IOException, EdmException {
        X509Certificate cert = null;
        byte[] encodedDocument = certificate.getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);
        java.io.File file = File.createTempFile("cert", "pfx");
        FileUtils.writeByteArrayToFile(file, decodedDocument);

        try
        {
            cert = (X509Certificate) cf.generateCertificate(FileUtils.openInputStream(file));
        }
        catch (CertificateException e)
        {
            log.error("", e);
            throw new EdmException("B��d certyfikatu");
        }
        return cert;
    }
}
