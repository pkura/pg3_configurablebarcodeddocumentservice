package pl.compan.docusafe.core.users.auth;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MissingUserPrincipalException.java,v 1.2 2006/02/06 16:05:55 lk Exp $
 */
public class MissingUserPrincipalException extends EdmException
{
    public MissingUserPrincipalException(String message)
    {
        super(message);
    }

    public MissingUserPrincipalException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MissingUserPrincipalException(Throwable cause)
    {
        super(cause);
    }
}
