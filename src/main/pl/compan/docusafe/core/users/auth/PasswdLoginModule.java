package pl.compan.docusafe.core.users.auth;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SignonLog;
import pl.compan.docusafe.core.base.DSLog;
import pl.compan.docusafe.util.StringManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Autentykacja u�ytkownika na podstawie pliku <code>passwd</code>
 * lub <code>passwd.txt</code> znajduj�cego si� w katalogu domowym aplikacji.
 * <p>
 * Najpierw szukany jest plik o nazwie <code>passwd</code>, a je�eli nie
 * istnieje, plik o nazwie <code>passwd.txt</code>.
 * <p>
 * Plik <code>passwd</code> jest plikiem tekstowym, w kt�rym ka�da linia
 * powinna mie� posta�:
 * <code>{nazwa u�ytkownika} ":" {has�o} [ ":" {role rozdzielone przecinkami} ]</code>,
 * przyk�adowo: <code>admin:{SHA}0DPiKuNIrrVmD8IUCuw1hQxNqZc=:admin</code>
 * <p>
 * Kolejne linie pliku odczytywane s� do momentu, gdy napotkana zostanie
 * linia odpowiadaj�ca u�ytkownikowi, kt�ry chce zalogowa� si� do systemu.
 * Nast�pnym krokiem jest weryfikacja has�a. Je�eli has�o w pliku rozpoczyna
 * si� od znaku <code>{</code>, przyjmuje si�, �e jest ono zaszyfrowane.
 * W przeciwnym razie has�o traktowane jest jako zapisane otwartym tekstem.
 * Je�eli weryfikacja has�a powiedzie si�, u�ytkownik zostaje zalogowany
 * i nadawane mu s� role okre�lone jako trzeci parametr w wierszu.  Je�eli
 * has�o jest niew�a�ciwe, u�ytkownik nie jest logowany i metoda {@link #authenticate()}\
 * zwraca warto�� <code>false</code>. 
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PasswdLoginModule.java,v 1.9 2008/10/06 10:53:51 pecet4 Exp $
 */
public class PasswdLoginModule extends AbstractLoginModule
{
    StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
    protected boolean authenticate() throws EdmException
    {
        // autentykacja na podstawie pliku passwd
        BufferedReader reader = null;
        File passwd = new File(Docusafe.getHome(), "passwd");
        if (!passwd.exists())
            passwd = new File(Docusafe.getHome(), "passwd.txt");

        try
        {
            if (passwd.exists() && passwd.isFile() && passwd.canRead())
            {
                reader = new BufferedReader(new FileReader(passwd));
                String line;
                while ((line = reader.readLine()) != null)
                {
                    StringTokenizer st = new StringTokenizer(line, ":");
                    String _username = st.hasMoreTokens() ? st.nextToken() : null;
                    String _password = st.hasMoreTokens() ? st.nextToken() : null;
                    String _roles = st.hasMoreTokens() ? st.nextToken() : null;

                    if (_username != null && _password != null &&
                        _password.length() > 0 && _username.equals(username))
                    {
                        boolean passwordMatches = false;

                        if (_password.length() > 0 && _password.charAt(0) == '{')
                            passwordMatches = _password.equals(digest(password));
                        else
                            passwordMatches = _password.equals(password);

                        if (passwordMatches)
                        {
                            UserPrincipal userPrincipal = new UserPrincipal(username);
                            principals.add(userPrincipal);

                            if (_roles != null)
                            {
                                StringTokenizer roleSt = new StringTokenizer(_roles, ", ");
                                while (roleSt.hasMoreTokens())
                                {
                                    principals.add(new RolePrincipal(roleSt.nextToken()));
                                }
                            }

                            principals.add(new FullNamePrincipal(username+" [" + sm.getString("ZalogowanyAwaryjnie")+ "]"));
                            
                            GlobalPreferences.addLoginFile(username);
                            
                            SignonLog.logEmergencySignon(username);
                            
                            DSApi.openAdmin();
                            DSApi.context().begin();
	            	     	   	DSLog l = new DSLog();
	            	     	   	l.setCode(DSLog.LOG_PASSWD);
	            	     	   	l.setCtime(new Date());
	            	     	   	l.setParam(password);
	            	     	   	l.setUsername(username);
	            	     	   	l.setReason(_roles);
	            	     	   	l.create();
	            	     	DSApi.context().commit();
	            	     	DSApi.close();

                            return true;
                        }

                        break;
                    }
                }
            }
        }
        catch (IOException e)
        {
        }
        finally
        {
            if (reader != null) try { reader.close(); } catch (Exception e) { }
        }

        return false;
    }
}
