package pl.compan.docusafe.core.users.auth;

import java.io.Serializable;
import java.security.Principal;

/**
 * Obiekt reprezentujący imię i nazwisko użytkownika pobrane
 * z serwera LDAP.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FullNamePrincipal.java,v 1.2 2006/02/20 15:42:27 lk Exp $
 */
public final class FullNamePrincipal implements Principal, Serializable
{
    private String name;

    public FullNamePrincipal(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public String toString()
    {
        return getClass().getName()+"["+name+"]";
    }
}
