package pl.compan.docusafe.core.users.auth;

import pl.compan.docusafe.util.StringManager;

import java.security.Permission;

/**
 * Klasa reprezentuj�ca prawo dost�pu do urla. Argumentem konstruktora
 * mo�e by� konkretny url lub jego cz�� zako�czona znakiem '*'.
 * W drugim przypadku uprawnienie dotyczy wszystkich urli pasuj�cych
 * do tego wzorca. Znak '*' mo�e wyst�pi� tylko raz i tylko na ko�cu
 * urla.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: URLPermission.java,v 1.2 2006/02/06 16:05:55 lk Exp $
 */
public class URLPermission extends Permission
{
    private String prefix;
    private boolean wildcard;

    private static StringManager sm = StringManager.getManager(Constants.Package);

    public URLPermission(String name)
    {
        super(name);

        if (name == null)
            throw new NullPointerException(sm.getString("urlPermission.nullParameter",
                "name"));

        if (name.indexOf('*') >= 0)
        {
            prefix = name.substring(0, name.indexOf('*'));
            wildcard = true;
        }
        else
        {
            prefix = name;
        }
    }

    /**
     * Zwraca true, je�eli prawo reprezentowane przez przekazany
     * obiekt Permission jest reprezentowane (by� mo�e niejawnie)
     * r�wnie� przez bie��cy obiekt.
     * @throws IllegalArgumentException Je�eli przekazany obiekt
     *  nie jest instancj� URLPermission.
     */
    public boolean implies(Permission permission)
    {
        if (!(permission instanceof URLPermission))
            throw new IllegalArgumentException(sm.getString("urlPermission.badClass",
                URLPermission.class.getName()));

        if (wildcard)
        {
            // XXX: sprawdza�, czy permission.getName() zawiera '*'
            return permission.getName().startsWith(prefix) &&
                permission.getName().substring(prefix.length()).indexOf('/') < 0;
        }
        else
        {
            return permission.getName().equals(getName());
        }
    }

    /**
     * Zawsze zwraca null.
     */
    public String getActions()
    {
        return null;
    }

    /**
     * Dwa obiekty URLPermission s� r�wne, je�eli maj� te same nazwy
     * ({@link #getName()}.
     */
    public boolean equals(Object obj)
    {
        if (obj == this) return true;

        if (!(obj instanceof URLPermission))
            return false;

        return getName().equals(((URLPermission) obj).getName());
    }

    /**
     * Zwraca getName().hashCode().
     */
    public int hashCode()
    {
        return getName().hashCode();
    }

    public String toString()
    {
        return getClass().getName()+"["+getName()+"]";
    }
}
