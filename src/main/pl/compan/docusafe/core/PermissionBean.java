package pl.compan.docusafe.core;

import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa opakowujaca informacje o uprawnieniu 
 * @author wkutyla
 *
 */
public class PermissionBean 
{	
	
	private final static Logger log = LoggerFactory.getLogger(PermissionBean .class);
	private String permission;
    private String subject;
    private String subjectType;
    /**
     * Czy nalezy tworzyc grupe o guid = subject and name = groupName
     */
    private boolean createGroup = false;
    private String groupName = null;
    public PermissionBean()
    {
    	super();
    }
    /**
     * Konstruktor 
     * @param permission
     * @param subject
     * @param subjectType
     */
    public PermissionBean (String permission, String subject, String subjectType)
    {
    	this();
    	this.permission = permission;
    	this.subject = subject;
    	this.subjectType = subjectType;
    }
    /**
     * Konstruktor z nazwa grupy
     * @param permission
     * @param subject
     * @param subjectType
     * @param groupName
     */
    public PermissionBean (String permission, String subject, String subjectType,String groupName)
    {
    	this(permission,subject,subjectType);
    	if (subjectType.equals(ObjectPermission.GROUP))
    	{
    		createGroup = true;
    	}
    	this.groupName =groupName; 
    }
    /**
     * Zwraca klucz potrzebny do porownan w mapie
     * @return
     */
    public String getKey()
    {
    	return permission + ":" + subject + ":" + subjectType;
    }

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}
	public boolean isCreateGroup() {
		return createGroup;
	}
	public void setCreateGroup(boolean createGroup) {
		this.createGroup = createGroup;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
