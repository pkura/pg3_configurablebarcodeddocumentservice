package pl.compan.docusafe.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Wyj�tek u�ywany przez {@link PasswordPolicy}
 * do informowania o nieprawid�owo�ci has�a. Metoda {@link #getMessages()}
 * zwraca list� komunikat�w o przyczynach, kt�re spowodowa�y rzucenie wyj�tku.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: IllegalPasswordException.java,v 1.6 2006/02/20 15:42:16 lk Exp $
 */
public class IllegalPasswordException extends EdmException
{
    private List<String> messages = new ArrayList<String>(3);

    public IllegalPasswordException(String message)
    {
        super(message);
    }

    public IllegalPasswordException(String message, List<String> messages)
    {
        super(message);
        if (messages != null) this.messages.addAll(messages);
    }

    public List getMessages()
    {
        return Collections.unmodifiableList(messages);
    }
}
