package pl.compan.docusafe.core;

import com.google.common.base.Function;

/**
 * Klasa u�atwiaj�ca dzia�ania w procesie kancelaryjnym dla danego procesu zwi�zanego z danym dokumentem
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl
 */
public interface OfficeFacade {

    /**
     * Zwraca activityId dla danego dokumentu
     * @return
     */
    String getActivityId();

    /**
     * Przegotowuje dekretacj� na danego u�ytkownika
     * @param username nie mo�e by� null
     * @param guid je�eli == null to zostanie wzi�ty pierwszy dzia� do kt�rego nale�y u�ytkownik
     * @return
     */
    AssignmentBuilder prepareAssignment(String username, String guid) throws EdmException;

    /**
     * Przygorowuje dekretacje na dany dzia�
     * @param guid nie mo�e by� null
     * @return
     */
    AssignmentBuilder prepareAssignment(String guid) throws EdmException;

    /**
     * Przegotowuje dekretacj� do wiadomo�ci na danego u�ytkownika
     * @param username nie mo�e by� null
     * @param guid je�eli == null to zostanie wzi�ty pierwszy dzia� do kt�rego nale�y u�ytkownik
     * @return
     */
    AssignmentBuilder prepareCC(String username, String guid) throws EdmException;

    /**
     * Przygorowuje dekretacje do wiadomo�ci na dany dzia�
     * @param guid nie mo�e by� null
     * @return
     */
    AssignmentBuilder prepareCC(String guid) throws EdmException;

    /**
     * Podstawa implementacji OfficeFacade
     * - klasy bazuj�ce musz� udost�pnia� activityId i obiekty umo�liwiaj�ce dekretacje
     */
    public abstract static class AbstractOfficeFacade implements OfficeFacade{

        final protected String activityId;

        public AbstractOfficeFacade(String activityId) {
            this.activityId = activityId;
        }

        public String activityId() {
            return activityId;
        }

        public AssignmentBuilder prepareAssignment(String username, String guid) throws EdmException {
            return new AssignmentBuilder(activityId, username, guid, getAssigner());
        }

        public AssignmentBuilder prepareAssignment(String guid) throws EdmException {
            return new AssignmentBuilder(activityId, null, guid, getAssigner());
        }

        public AssignmentBuilder prepareCC(String username, String guid) throws EdmException {
            return new AssignmentBuilder(activityId, username, guid, getCCAssigner());
        }

        public AssignmentBuilder prepareCC(String guid) throws EdmException {
            return new AssignmentBuilder(activityId, null, guid, getCCAssigner());
        }

        protected abstract Function<AssignmentBuilder, Boolean> getAssigner();
        protected abstract Function<AssignmentBuilder, Boolean> getCCAssigner();
    }
}
