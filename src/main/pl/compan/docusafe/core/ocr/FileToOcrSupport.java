package pl.compan.docusafe.core.ocr;

import com.google.common.collect.Sets;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;
import java.io.File;

/**
 * @author Mariusz Kilja�czyk
 */
public class FileToOcrSupport {
    private final static OcrSupport OCR_SUPPORT = OcrUtils.getFileOcrSupport(
    		new File(Docusafe.getHome(), "ocr-put"),
            new File(Docusafe.getHome(), "ocr-get"),
            Sets.newHashSet("tiff","tif","gif","pdf","jpg")
    );

    public static OcrSupport get(){
        return OCR_SUPPORT;
    }
}
