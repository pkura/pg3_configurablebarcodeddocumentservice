package pl.compan.docusafe.core.ocr;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;

/**
 * Obiekt umo�liwiaj�cy ocrowanie dokument�w
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface OcrSupport {

    /**
     * Udost�pnia dany za��cznik do sprawdzenia czy mo�liwy/potrzebny jest mechanizm ocr
     * implementacja tej metody powinna sprawdza� czy ocr jest potrzebny i je�eli jest to podejmowa� odpowiednie
     * czynno�ci
     *
     * @param doc Dokument, do kt�rego do��czony jest za��cznik - nie mo�e by� null
     * @param ar Za��cznik, kt�ry jest sprawdzany czy jest do ocrowania
     */
    void checkForOcr(Document doc, AttachmentRevision ar) throws EdmException;
    
    
    /**Wysy�a pliki do z ORC-owania ale z innym warunkiem czy powinien. */
    void putToOcr(AttachmentRevision ar) throws EdmException ;

}
