package pl.compan.docusafe.core.ocr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Set;

import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.service.ocr.OcrFileService;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class OcrUtils {
    static final Logger LOG = LoggerFactory.getLogger(OcrUtils.class);

    public static OcrSupport getFileOcrSupport(File destDir, File ocrDir, Set<String> suportedExtensions){
        checkNotNull(destDir, "destDir cannot be null");
        checkNotNull(ocrDir, "ocrDir cannot be null");
        checkNotNull(suportedExtensions, "suportedExtensions cannot be null");
        checkArgument(destDir.isDirectory(), "destDir must be existing directory");
        checkArgument(ocrDir.isDirectory(), "ocrDir must be existing directory");
        LOG.info("creating OcrSupport : {} {} {} {}", destDir, ocrDir, suportedExtensions);
        FileOcrSupport ret = new FileOcrSupport(destDir, ocrDir, suportedExtensions);
        try {
            OcrFileService.getService().addWatchedDirectory(ocrDir);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
        return ret;
    }

    /**
     * Zwraca rozszerzenie pliku (fragment nazwy po ostatniej kropce w nazwie), "" w przypadku
     * braku kropki, lub gdy kropka jest ostatnim znakiem
     * @param file
     * @return
     */
    public static String fileExtension(File file){
        String filename = file.getName();
        return fileExtension(filename);
    }

    /**
     * Zwraca rozszerzenie pliku (fragment nazwy po ostatniej kropce w nazwie), "" w przypadku
     * braku kropki, lub gdy kropka jest ostatnim znakiem
     * @param file
     * @return
     */
    public static String fileExtension(String filename){
        int dotIndex = filename.lastIndexOf('.');
        if(dotIndex == -1 || dotIndex == filename.length()){
            return "";
        } else {
            return filename.substring(dotIndex + 1);
        }
    }
    
    private OcrUtils(){}
}

/**
 * Klasa zapisuj�ca za��czniki do wybranego katalogu.
 *
 * OcrFileService s�u�y do dodawania za��cznik�w z powrotem
 **/
class FileOcrSupport implements OcrSupport{

    private final File destDir;
    private final File ocrDir;
    /** rozszerzenia (lowercase) */
    private final Set<String> supportedExtensions;

    FileOcrSupport(File destDir, File ocrDir, Set<String> supportedExtensions) {
        this.destDir = destDir;
        this.ocrDir = ocrDir;
        this.supportedExtensions = Sets.newHashSet(Iterables.transform(supportedExtensions, new Function<String, String>(){
            @Override
            public String apply(String s) {
                return s.toLowerCase();
            }
        }));
    }

    public synchronized void checkForOcr(Document doc, AttachmentRevision ar) throws EdmException {
        File target = new File(destDir, ar.getId() + "_" + ar.getOriginalFilename());
        if(target.isFile()){ //ju� ocrowane
            OcrUtils.LOG.info("file '{}' already added", target.getName());
            return;
        }
        if(!supportedExtensions.contains(OcrUtils.fileExtension(target).toLowerCase())){
            OcrUtils.LOG.info("extension of file '{}' is not supported", target.getName());
            return;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(target);
            IOUtils.copy(ar.getAttachmentStream(),fos);
        } catch (Exception ex) {
            OcrUtils.LOG.error(ex.getMessage(), ex);
        } finally {
            IOUtils.closeQuietly(fos);
        }
        OcrUtils.LOG.info("trying to ocr attachment rev : {} - target file '{}'", ar.getId(), target.getName());
    }
   
    public synchronized void putToOcr(AttachmentRevision ar) throws EdmException 
    {    
    	if(ar.getIndexStatus() == IndexStatus.NOT_INDEXED)
    	{
	    	if(!supportedExtensions.contains(OcrUtils.fileExtension(ar.getOriginalFilename()).toLowerCase())){
	            OcrUtils.LOG.info("extension of file '{}' is not supported", ar.getMime());
	            ar.setIndexStatus(IndexStatus.INDEXING_NOT_SUPPORTED);
	            return;
	        }
	    	File target = new File(destDir, ar.getId() + "_"+ ar.getOriginalFilename());
	        FileOutputStream fos = null;
	        try {
	            fos = new FileOutputStream(target);
	            IOUtils.copy(ar.getAttachmentStream(),fos);
	            ar.setIndexStatus(IndexStatus.SEND_TO_OCR);
	        } catch (Exception ex) {
	            OcrUtils.LOG.error(ex.getMessage(), ex);
	        } finally {
	            IOUtils.closeQuietly(fos);
	        }
    	}
        else
            return;
    }
    
}
