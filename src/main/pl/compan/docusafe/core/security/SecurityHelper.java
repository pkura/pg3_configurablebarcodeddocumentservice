package pl.compan.docusafe.core.security;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SecurityHelper.java,v 1.8 2006/03/09 16:02:36 lk Exp $
 */
public class SecurityHelper {
    private static final Logger log = LoggerFactory.getLogger(SecurityHelper.class);

    public SecurityHelper() {
    }

    /**
     * Zwraca true, je�eli bie��cy u�ytkownik mo�e zmienia� uprawnienia
     * przekazanego obiektu.
     * <p/>
     * U�ytkownik mo�e zmienia� uprawnienia obiektu je�eli jest administratorem
     * lub autorem obiektu.
     */
    public boolean canSetPermissions(Document document) throws EdmException {
        return DSApi.context().isAdmin() ||
                (document.getAuthor() != null && document.getAuthor().equalsIgnoreCase(DSApi.context().getPrincipalName()));
    }

    /**
     * Zwraca true, je�eli bie��cy u�ytkownik mo�e zmienia� uprawnienia
     * przekazanego obiektu.
     * <p/>
     * U�ytkownik mo�e zmienia� uprawnienia obiektu je�eli jest administratorem
     * lub autorem obiektu.
     */
    public boolean canSetPermissions(Folder folder) throws EdmException {
        return DSApi.context().isAdmin() ||
                (folder.getAuthor() != null && folder.getAuthor().equalsIgnoreCase(DSApi.context().getPrincipalName()));
    }

    /**
     * Dodaje uprawnienia obiektu dla danego obiektu
     *
     * @param objectId
     * @param permissionsString
     * @param clazz
     * @return
     * @throws EdmException
     */
    public static boolean applyObjectPermissions(Long objectId, String permissionsString, Class clazz) throws EdmException {
        Map<String, List<String>> subjectPermissions = getPermissionMaps(permissionsString);
        return applyObjectPermissions(objectId, subjectPermissions, clazz);
    }

    /**
     * Dodaje uprawnienia obiektu dla danego obiektu
     *
     * @param objectId
     * @param clazz
     * @return
     * @throws EdmException
     */
    public static boolean applyObjectPermissions(Long objectId, Map<String, List<String>> subjectPermissions, Class clazz) throws EdmException {
        List<ObjectPermission> permissions = ObjectPermission.find(clazz, objectId);

        removeExistedPermissions(subjectPermissions, permissions);

        // flush i clear
        DSApi.context().session().flush();
        DSApi.context().session().clear();

        createPermission(objectId, clazz, subjectPermissions);

        if (AvailabilityManager.isAvailable("jackrabbit.metadata") && clazz == Document.class) {
            JackrabbitXmlSynchronizer.createEvent(objectId);
        }

        return false;
    }


    public static void createPermission(Long objectId, Class clazz, Map<String, List<String>> subjectPermissions) throws EdmException {
        for (String subject : subjectPermissions.keySet() ) {
            // nazwa aktora sk�adaj�ca si� z dw�ch cz�ci: nazwy i typu
            // rozdzielonych uko�nikiem
            if (subject.indexOf('/') < 0) {
                log.warn("W nazwie aktora brakuje uko�nika: {}", subject);
                continue;
            }
            String subjectName = subject.substring(0, subject.indexOf('/'));
            String subjectType = subject.substring(subject.indexOf('/') + 1);
            List<String> permNames = subjectPermissions.get(subject);

            for (String name : permNames ) {
                ObjectPermission.create(clazz, objectId, subjectName, subjectType, name);
            }
        }
    }

    /**
     * je�eli jest w tablicy przekazanej w formularzu, usuwam z tej tablicy, je�eli nie ma, usuwam z bazy
     * @param subjectPermissions
     * @param permissions
     * @throws EdmException
     */
    private static void removeExistedPermissions(Map<String, List<String>> subjectPermissions, List<ObjectPermission> permissions) throws EdmException {
        for (ObjectPermission permission : permissions ) {
            List<String> subjectPerms = subjectPermissions.get(permission.getSubject());
            boolean containsPerms = subjectPerms != null && subjectPerms.contains(permission.getName());
            if (containsPerms) {
                subjectPerms.remove(permission.getName());
                if (CollectionUtils.isEmpty(subjectPerms))
                    subjectPermissions.remove(permission.getSubject());
            } else {
                permission.revoke();
            }
        }
    }

    /**
     * Usuwa uprawnienia z bazy je�li istniej� w mapie
     * @param subjectPermissions
     * @param permissions
     * @throws EdmException
     */
    public static void removePermissions(Map<String, List<String>> subjectPermissions, List<ObjectPermission> permissions) throws EdmException {
        for (ObjectPermission permission : permissions ) {
            List<String> subjectPerms = subjectPermissions.get(permission.getSubjectAndSubjectType());
            boolean containsPerms = subjectPerms != null && subjectPerms.contains(permission.getName());
            if (containsPerms) {
                permission.revoke();
            }
        }
    }

    /**
     * mapowanie nazw u�ytkownik�w na tablice nazw uprawnie� (List)
     * przekazany parametr ma posta�:
     * lk { read write create }; * { delete read }; wk { read create }
     **/
    private static Map<String, List<String>> getPermissionMaps(String permissionsString) {
        Map<String, List<String>> subjectPermissions = new HashMap();
        if (permissionsString != null && permissionsString.indexOf(';') > 0) {
            String[] defs = StringUtils.split(permissionsString, ";");
            for (int i = 0, n = defs.length; i < n; i++) {
                if (defs[i] == null || defs.length == 0 ||
                        defs[i].indexOf('{') < 0 ||
                        defs[i].indexOf('}') < defs[i].indexOf('{'))
                    continue;

                String def = defs[i].trim();
                String subject = def.substring(0, def.indexOf('{')).trim();
                String[] perms = StringUtils.split(def.substring(def.indexOf('{') + 1, def.indexOf('}')), " ");
                if (perms == null || perms.length == 0)
                    continue;

                for (int j = 0, m = perms.length; j < m; j++) {
                    perms[j] = perms[j].trim();
                }
                Arrays.sort(perms);
                subjectPermissions.put(subject, new ArrayList(Arrays.asList(perms)));
            }
        }

        if (log.isDebugEnabled())
            log.debug("subjectPermissions={}", subjectPermissions);
        return subjectPermissions;
    }
}
