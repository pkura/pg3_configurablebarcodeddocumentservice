package pl.compan.docusafe.core.security;

import pl.compan.docusafe.core.EdmException;
/* User: Administrator, Date: 2005-09-01 17:02:42 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrivilegedProcedure.java,v 1.2 2006/02/08 14:54:19 lk Exp $
 */
public interface PrivilegedProcedure<T>
{
    T run() throws EdmException;
}
