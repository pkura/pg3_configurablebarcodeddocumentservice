package pl.compan.docusafe.core.security;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: AccessDeniedException.java,v 1.3 2008/11/08 13:59:37 wkuty Exp $
 */
public class AccessDeniedException extends EdmException
{
	static final long serialVersionUID = 23223; 
    public AccessDeniedException(String message)
    {
        super(message);
    }

    public AccessDeniedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public AccessDeniedException(Throwable cause)
    {
        super(cause);
    }
}
