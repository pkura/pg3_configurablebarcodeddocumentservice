package pl.compan.docusafe.core;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * Ka�da instancja tej klasy opisuje wydarzenie zwi�zane z obiektem.
 * Wydarzenie jest opisane nazw� w�asno�ci, kt�rej dotyczy wydarzenie
 * (pole nieobowi�zkowe), nazw� u�ytkownika, kt�ry wywo�uje wydarzenie,
 * oraz opisem wydarzenia i dwoma parametrami typu String i Long.
 * <p>
 * Dodatkowe parametry (lparam i wparam) s�u�� do opisania wydarzenia
 * w formie zrozumia�ej dla aplikacji. Proponowane s� nast�puj�ce
 * warto�ci tych parametr�w dla rozmaitych wydarze�:
 * <h3>Historia zmian dokumentu</h3>
 * <ul>
 * <li>Dodanie za��cznika: property="attachments", lparam="ADD",
 *  wparam=identyfikator za��cznika
 * <li>Usuni�cie za��cznika: property="attachments", lparam="DELETE",
 *  wparam=identyfikator za��cznika
 * <li>Trwa�e usuni�cie za��cznika: property="attachments", lparam="PERMDELETE",
 *  wparam=identyfikator za��cznika
 * <li>Podpisanie za��cznika: property="attachments", lparam="SIGN",
 *  wparam=identyfikator za��cznika
 * <li>Trwa�e usuni�cie wszystkich za��cznik�w: property="attachments",
 *     lparam="ALLPERMDELETE", wparam=null
 * <li>Nowa wersja za��cznika: property="attachments", lparam="REVISION",
 *  wparam=identyfikator za��cznika
 * <li>Oznaczenie za��cznika (np. jako widzianego): property="attachments",
 *  lparam="LPARAM", wparam=identyfikator za��cznika
 * <li>Zmiana warto�ci atrybutu location: property="location",
 *  lparam=nowa warto�� location, wparam=null
 * <li>Zmiana warto�ci atrybutu status: property="status",
 *  lparam=status.getName(), wparam=status.getId()
 * <li>Zmiana referenta pisma: property="clerk",
 *  lparam=nowa warto�� clerk
 * <li>Zmiana znaku pisma: property="referenceId",
 *  lparam=nowa warto�� referenceid
 * <li>Przyj�cie pisma w dziale: property="assignedDivision",
 *  lparam=nowa warto�� identyfikatora dzia�u
 * <li>Umieszczenie pisma w sprawie: property="containingCase",
 *  lparam=warto�� caseId, wparam=identyfikator sprawy (Case.id)
 * <li>Udzielenie odpowiedzi na pismo przychodz�ce (w momencie
 *  nadania numeru kancelaryjnego pismu wychodz�cemu): property="::reply",
 *  lparam=null, wparam=identyfikator pisma wychodz�cego (OutOfficeDocument.id)
 * <li>Zmiana jednej z dat: incomingDate, documentDate, stampDate:
 *  property="incomingDate", lparam=data w formacie
 *  {@link pl.compan.docusafe.util.DateUtils#portableDateTimeFormat}
 * <li>Anulowanie pisma: property="cancelled", lparam=Pow�d anulowania
 *  pisma wpisany przez u�ytkownika.
 * <li>Utworzenie pisma: property="::create"
 * <li>Zmiana statusu pisma wychodz�cego na brudnopis/czystopis:
 *  property="prepState", lparam=Nowa warto�� prepState.
 * <li>Zmiana sposobu dostarczenia pisma: property="delivery",
 *  lparam=Nazwa sposobu dostarczenia, wparam=Identyfikator sposobu dostarczenia
 * <li>Zmiana sposobu odbioru pisma: property="outgoingDelivery"
 *  lparam=Nazwa sposobu odbioru pisma, wparam=Identyfikator sposobu odbioru
 *  (obiekty InOfficeDocument)
 * <li>Zmiana opisu pisma: property="summary"
 * <li>Zmiana liczby za��cznik�w: property="numAttachments",
 *  wparam=nowa liczba za��cznik�w
 * <li>Zmiana kodu kreskowego: property="barcode"
 *  lparam=nowy kod kreskowy lub null, je�eli nowy kod jest pusty
 * <li>Zmiana numeru R (poczta): property="postalRegNumber",
 *  lparam=nowy numer R.
 * <li>Zmiana powodu zwrotu pisma: property="returnReason",
 *  wparam=identyfikator nowego powodu
 * <li>Nadanie numeru kancelaryjnego (tylko pismo wychodz�ce):
 *  property="officeNumber", wparam=nowy numer kancelaryjny
 * <li>Umieszczenie dokumentu w pudle archiwalnym:
 *  property="box" wparam=null, lparam=ID pud�a 
 * <li>Usuni�cie dokumentu z pud�a.
 *  property="boxremove" wparam=null, lparam=ID pud�a
 * <li>Utworzenie dokumentu DRS (Nationwide)
 *  property="nw_create_drs", wparam=null, lparam=ID dokumentu RS 
 * <li>Podpisanie dokumentu
 *  property="signatures", wparam="SIGN", lparam=ID otrzymanego podpisu
 * <li>Weryfikacja dokumentu
 *  property="signatures", wparam="VERIFY", lparam=ID otrzymanego podpisu
 * <li>Przypisanie dokumentu do agenta/agencji
 * property="daa", lparam=ID agenta, wparam=ID agencji (lparam i wparam moga byc null)
 * <li>Zmiana doctype-u dokumentu
 * property="changeDoctype", wparam=nowy doctype
 * <li> Dokonanie akceptacji dokumentu
 * property="giveAccept", lparam=cn akceptacji
 * <li> Wycofanie akceptacji dokumentu
 * property="withdrawAccept", lparam=cn akceptacji
 * <li> Utworzenie nowego dokumentu z za��cznik�w: property="newDocument", 
 * lparam=ID nowego dokumentu/ID starego dokumentu, wparam=CREATE_INTO/CREATE_FROM
 * <li> Przypisanie do dokumentu nowego centrum kosztowego
 * property="addCentrum", lparam=kod nowego centrum
 * <li> Usuni�cie z dokumentu centrum kosztowego
 * property="removeCentrum", lparam=kod usuwanegi centrum
 * </ul>
 *
 * <h3>Historia zmian sprawy</h3>
 * <ul>
 * <li>Utworzenie sprawy: property="::create"
 * <li>Zmiana referenta sprawy: property="clerk",
 *  lparam=nowa warto�� clerk
 * <li>Przypisanie sprawy do teczki: property="portfolio",
 *  wparam=Portfolio.portfolioId, lparam=Portfolio.id
 * <li>Zmiana priorytetu sprawy: property="priority"
 *  wparam=nazwa priorytetu, lparam=id priorytetu
 * <li>Zmiana numeru sprawy: property="changeNumber"
 *  lparam=nowy numer
 * </ul>
 *
 * <h3>Historia zmian teczki</h3>
 * <ul>
 * <li>Utworzenie teczki: property="::create"
 * <li>Dodanie sprawy do teczki: property="children",
 *  lparam=OfficeCase.officeId, wparam=OfficeCase.id
 * <li>Zmiana referenta sprawy: property="clerk",
 *  lparam=nowa warto�� clerk
 * <li>Zmiana opisu (nazwy) teczki: property="name",
 *  lparam=nowa nazwa
 * <li>Zmiana numeru pocz�tkowego sprawy: property="caseSeqNum",
 *  wparam=nowy numer pocz�tkowy sprawy.
 * <li>Zmiana numeru teczki: property="changeNumber"
 *  lparam=nowy numer
 * <li>Dodanie wpisu w rejestrze pozwole� na budowe: property="addConstruction"
 *  lparam=numer wniosku
 *  <li>Zmiana atrybutu rejestru pozwolen na budowe: property"changeAtr"
 *  lparam = nowy atrubut
 *  <li>Wycofanie wniosku pozwolenia na budowe: property="withdraw" 
 *  lparam = numer wniosku
 *  <li>Przywr�cenie wniosku pozwolenia na budowe: property="unwithdraw"
 *  lparam = numer wniosku
 *  <li>Usuni�cie wniosku pozwolenia na budowe: property="deleteConstruction" 
 *  lparam = numer wniosku
 * </ul>
 *
 * <h3>Historia zmian wpisu do rejestru</h3>
 * <ul>
 * <li>Utworzenie wpisu: property="::create"
 * <li>Zmiana pola wpisu: property="changeFields",
 * <li>Przypisanie do sprawy: property="setCase",
 *  lparam=warto�� caseId, wparam=identyfikator sprawy (Case.id)
 *  <li>Akcja dotycz�ca za��cznika: property="attachments",
 * </ul>
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Audit.java,v 1.39 2008/11/20 18:57:36 wkuty Exp $
 */
public final class Audit
{
    private Date ctime;
    private String property;
    private String username;
    private String description;
    private String lparam;
    private Long wparam;
    private Integer priority;
    
    public static final int HIGHEST_PRIORITY = 1;
    public static final int LOWEST_PRIORITY = 2;

    public static Audit create(String property, String username, String description, String lparam, Long wparam)
    {
        return new Audit(new Date(), property, username, description, lparam, wparam, HIGHEST_PRIORITY);
    }

    public static Audit create(String property, String username, String description, String lparam)
    {
        return new Audit(new Date(), property, username, description, lparam, null, HIGHEST_PRIORITY);
    }

    public static Audit create(String property, String username, String description)
    {
        return new Audit(new Date(), property, username, description, null, null, HIGHEST_PRIORITY);
    }

    public static Audit createWithPriority(String property, String username, String description, String lparam, Integer priority)
    {
        return new Audit(new Date(), property, username, description, lparam, null, priority);
    }

    public Audit()
    {
    }

    private Audit(Date ctime, String property, String username, String description, String lparam, Long wparam, Integer priority)
    {
        this.ctime = ctime;
        this.property = property;
        this.username = username;
        this.description = StringUtils.defaultIfEmpty(StringUtils.left(description, 1000), " ");
        this.lparam = StringUtils.left(lparam, 200);
        this.wparam = wparam;
        this.priority = priority;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLparam()
    {
        return lparam;
    }

    public void setLparam(String lparam)
    {
        this.lparam = lparam;
    }

    public Long getWparam()
    {
        return wparam;
    }

    public void setWparam(Long wparam)
    {
        this.wparam = wparam;
    }

    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public Integer getPriority()
    {
        return priority;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Audit)) return false;

        final Audit audit = (Audit) o;

        if (ctime != null ? !ctime.equals(audit.ctime) : audit.ctime != null) return false;
        if (description != null ? !description.equals(audit.description) : audit.description != null) return false;
        if (lparam != null ? !lparam.equals(audit.lparam) : audit.lparam != null) return false;
        if (property != null ? !property.equals(audit.property) : audit.property != null) return false;
        if (username != null ? !username.equals(audit.username) : audit.username != null) return false;
        if (wparam != null ? !wparam.equals(audit.wparam) : audit.wparam != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (ctime != null ? ctime.hashCode() : 0);
        result = 29 * result + (property != null ? property.hashCode() : 0);
        result = 29 * result + (username != null ? username.hashCode() : 0);
        result = 29 * result + (description != null ? description.hashCode() : 0);
        result = 29 * result + (lparam != null ? lparam.hashCode() : 0);
        result = 29 * result + (wparam != null ? wparam.hashCode() : 0);
        return result;
    }

}
