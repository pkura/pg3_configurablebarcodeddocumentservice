package pl.compan.docusafe.core;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

/**
 * Klasa trzymaj�ca struktury DocFacade i OfficeDocFacade,
 * trzyma je w ThreadContext, kt�ry powinien by� czyszczony po ka�dym obs�u�onym wywo�aniu
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DocumentsCache {
    private final static Logger LOG = LoggerFactory.getLogger(DocumentsCache.class);

    private static DocumentsCache INSTANCE = new DocumentsCache();

    static DocumentsCache getInstance(){
        return INSTANCE;
    }

    ThreadLocal<Map<Long, DocFacade>> docFacades = new ThreadLocal<Map<Long, DocFacade>>(){
        @Override
        protected Map<Long, DocFacade> initialValue() {
            return Maps.newHashMap();
        }
    };
    ThreadLocal<Map<Long, OfficeDocFacade>> officeDocFacades= new ThreadLocal<Map<Long, OfficeDocFacade>>(){
        @Override
        protected Map<Long, OfficeDocFacade> initialValue() {
            return Maps.newHashMap();
        }
    };

    DocFacade getDocFacade(long id){
        DocFacade doc = docFacades.get().get(id);
        if(doc == null){
            doc = new DocFacadeImpl(id);
        }
        docFacades.get().put(id, doc);
        return doc;
    }

    OfficeDocFacade getOfficeDocFacade(long id, String activityId){
        OfficeDocFacade doc = officeDocFacades.get().get(id);
        if(doc == null){
            doc = new OfficeDocFacadeImpl(id, activityId);
            docFacades.get().put(id, doc);
            officeDocFacades.get().put(id, doc);
        }
        return doc;
    }

    /**
     * zapisuje zmiany w sesji
     */
    void saveChanges() throws EdmException{
        for(DocFacade df: Lists.newArrayList(docFacades.get().values())){
            df.saveChanges();
        }

        for(OfficeDocFacade odf: Lists.newArrayList(officeDocFacades.get().values())){
            odf.saveChanges();
        }
    }


    void flushDocument(long id) throws EdmException {
        DocFacade df = officeDocFacades.get().get(id);
        if(df != null){
            //saveChanges wywo�uje setOnly, a te wywo�uje flushDocument -> dlatego nale�y najpier usun��, a
            //potem zapisa�
            officeDocFacades.get().remove(id);
            docFacades.get().remove(id);
            df.saveChanges();
        } else {
            //saveChanges wywo�uje setOnly, a te wywo�uje flushDocument -> dlatego nale�y najpier usun��, a
            //potem zapisa�
            df = docFacades.get().get(id);
            if(df != null){
                docFacades.get().remove(id);
                df.saveChanges();
            }
        }
    }

    /**
     * czy�ci cache dla danego w�tku, brak wywo�ywania tej metody mo�e spowodowa� wycieki pami�ci
     */
    void clear(){
        //mo�na te� korzysta� z ThreadLocal#remove ale tak jest chyba szybciej
        docFacades.get().clear();
        officeDocFacades.get().clear();
    }

    private abstract static class DocFacadeBaseImpl implements DocFacade {
        final long id;
        Fields fields = null;
        FieldsManager fm = null;

        DocFacadeBaseImpl(long id){
            this.id = id;
        }

        public long getId() {
            return id;
        }

        public Fields getFields() throws EdmException {
            if(fields == null){
                fields = new FieldAccessHashCacheImpl(getFieldsManager());
            }
            return fields;
        }

        public FieldsManager getFieldsManager() throws EdmException {
            if(fm == null){
                fm = getDocument().getFieldsManager();
            }
            return fm;
        }

        public void saveChanges() throws EdmException {
            if(fields != null){
                fields.commitChanges();
            }
        }
    }

    private static class DocFacadeImpl extends DocFacadeBaseImpl implements DocFacade {
        Document document = null;

        private DocFacadeImpl(long id) {
            super(id);
        }

        public Document getDocument() throws EdmException {
            if(document == null){
                document = Document.find(id);
            }
            return document;
        }
        public Document getDocument(boolean checkPermission) throws EdmException {
             if(document == null){
                 document = Document.find(id, checkPermission);
             }
             return document;
         }
    }

    private static class OfficeDocFacadeImpl extends DocFacadeBaseImpl implements OfficeDocFacade {
        OfficeDocument document = null;
        String activityId = null;

        private OfficeDocFacadeImpl(long id, String activityId) {
            super(id);
            this.activityId = activityId;
        }

        public OfficeDocument getDocument() throws EdmException {
            if(document == null){
                document = OfficeDocument.find(id);
            }
            return document;
        }
        
        public OfficeDocument getDocument(boolean checkPermission) throws EdmException {
             if(document == null){
                 document = (OfficeDocument) OfficeDocument.find(id, checkPermission);
             }
             return document;
         }

        public OfficeFacade getOffice() {
            return null;
        }
    }
}