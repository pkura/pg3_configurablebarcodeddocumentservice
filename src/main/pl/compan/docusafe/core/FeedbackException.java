package pl.compan.docusafe.core;

/**
 * Wyjatek przeznaczony do raportowania Exceptionow zwiazanych z odpowiedziami innych uslug
 * Webservicow, URL, etc, etc
 * @author wkutyla
 *
 */
public class FeedbackException extends EdmException 
{
	final static long serialVersionUID = 1; 
    public FeedbackException (String message)
    {
        super(message);
    }

    public FeedbackException (String message, Throwable cause)
    {
        super(message, cause);
    }

    public FeedbackException (Throwable cause)
    {
        super(cause);
    }
}
