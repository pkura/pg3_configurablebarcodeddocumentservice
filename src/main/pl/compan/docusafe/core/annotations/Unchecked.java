package pl.compan.docusafe.core.annotations;

import java.lang.annotation.Documented;

@Documented
public @interface Unchecked {

}
