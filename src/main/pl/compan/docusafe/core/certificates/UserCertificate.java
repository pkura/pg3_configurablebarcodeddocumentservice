package pl.compan.docusafe.core.certificates;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.type.Type;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.PKCS7SignedData;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.*;
import pl.compan.docusafe.util.DateUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UserCertificate.java,v 1.11 2008/10/30 09:33:23 wkuty Exp $
 */
public class UserCertificate
{
    private Long id;
    private String username;
    private String encodedCsr;
    /**
     * Certyfikat w postaci X.509 zakodowany w base64.
     */
    private String encodedCrt;
    /**
     * Certyfkat w postaci PKCS#7 zakodowany w base64.
     */
    private String encodedPkcs7;
    private String alias;
    private Date ctime;
    private Date signingTime;
    /**
     * Warto�� true oznacza, �e certyfikat zosta� podpisany i pola
     * encodedCrt oraz encodedPkcs7 maj� prawid�owe warto�ci.
     */
    private boolean signed;

    private Date expirationTime;
    private Integer serialNumber;
    private String subjectDn;
    private String encodedSignature;


    private static final CertificateFactory x509certificateFactory;

    static
    {
        try
        {
            x509certificateFactory = CertificateFactory.getInstance("X509");
        }
        catch (CertificateException e)
        {
            throw new Error(e.getMessage(), e);
        }
    }

    UserCertificate()
    {
    }

    public static SearchResults search(CertificateQuery query) throws EdmException
    {
        FromClause from = new FromClause();
        final TableAlias certTable = from.createTable(DSApi.getTableName(UserCertificate.class));
        final WhereClause where = new WhereClause();

        query.visitQuery(new FormQuery.QueryVisitorAdapter()
        {
            public void visitEq(FormQuery.Eq expr) throws EdmException
            {
                if (expr.getAttribute().equals("username"))
                {
                    where.add(Expression.eq(certTable.attribute("username"), expr.getValue()));
                }
            }

            public void visitGe(FormQuery.Ge expr) throws EdmException
            {
                if (expr.getAttribute().equals("signingTime"))
                {
                    where.add(Expression.ge(certTable.attribute("signingtime"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("expirationTime"))
                {
                    where.add(Expression.ge(certTable.attribute("expirationTime"), expr.getValue()));
                }
            }

            public void visitLe(FormQuery.Le expr) throws EdmException
            {
                if (expr.getAttribute().equals("signingTime"))
                {
                    where.add(Expression.le(certTable.attribute("signingtime"), expr.getValue()));
                }
                else if (expr.getAttribute().equals("expirationTime"))
                {
                    where.add(Expression.le(certTable.attribute("expirationTime"), expr.getValue()));
                }
            }
        }
        );

        SelectClause selectId = new SelectClause(true);
        SelectColumn idCol = selectId.add(certTable, "id");

        OrderClause order = new OrderClause();
        FormQuery.OrderBy[] orderBy = query.getOrder();
        for (int i=0; i < orderBy.length; i++)
        {
            order.add(certTable.attribute(orderBy[i].getAttribute()), Boolean.valueOf(orderBy[i].isAscending()));
            selectId.add(certTable, orderBy[i].getAttribute());
/*
            if ("expirationTime".equals(orderBy[i].getAttribute()))
            {
                order.add(certTable.attribute("expirationtime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(certTable, "expirationtime");
            }
            else if ("signingTime".equals(orderBy[i].getAttribute()))
            {
                order.add(certTable.attribute("signingtime"), Boolean.valueOf(orderBy[i].isAscending()));
                selectId.add(certTable, "signingtime");
            }
*/
        }

        SelectClause selectCount = new SelectClause();
        SelectColumn countCol = selectCount.addSql("count(distinct "+certTable.getAlias()+".id)");

        int totalCount;
        SelectQuery selectQuery;

        try
        {
            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby certyfikat�w.");
            totalCount = rs.getInt(countCol.getPosition());
            rs.close();

            selectQuery = new SelectQuery(selectId, from, where, order);
            selectQuery.setMaxResults(query.getLimit());
            selectQuery.setFirstResult(query.getOffset());
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<UserCertificate> results = new ArrayList<UserCertificate>(query.getLimit());
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                results.add(DSApi.context().load(UserCertificate.class, id));
            }
            rs.close();
            return new SearchResultsAdapter<UserCertificate>(results, query.getOffset(), totalCount,
                UserCertificate.class);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static class CertificateQuery extends FormQuery
    {
        private static final String[] SORT_ATTRIBUTES =
            new String[] { "signingTime", "expirationTime",
                           "subjectDn" };

        private CertificateQuery()
        {
        }

        public CertificateQuery(int offset, int limit)
        {
            super(offset, limit);
        }

        protected String[] sortAttributes()
        {
            return SORT_ATTRIBUTES;
        }

        public void signingTime(Date from, Date to)
        {
            if (from != null)
                addGe("signingTime", from);
            if (to != null)
                addLe("signingTime", to);
        }

        public void expirationTime(Date from, Date to)
        {
            if (from != null)
                addGe("expirationTime", from);
            if (to != null)
                addLe("expirationTime", to);
        }

        public void username(String username)
        {
            addEq("username", username);
        }
    }

    /**
     * Konwertuje ci�g bajt�w do postaci obiektu X509Certificate.
     */
    public static X509Certificate getX509Certificate(byte[] certData) throws EdmException
    {
        try
        {
            synchronized (x509certificateFactory)
            {
                return (X509Certificate) x509certificateFactory.
                    generateCertificate(new ByteArrayInputStream(certData));
            }
        }
        catch (CertificateException e)
        {
            throw new EdmException("B��d analizy certyfikatu", e);
        }
    }

    private static final String BEGIN = "-----BEGIN CERTIFICATE REQUEST-----";
    //private static final String END = "-----END CERTIFICATE REQUEST-----";

/*
    public static byte[] decodePEM(byte[] data) throws EdmException
    {
        if (data.length < BEGIN.length())
            throw new EdmException("Spodziewano si� danych w formacie PEM");

        if (new String(data, 0, BEGIN.length()).equals(BEGIN))
        {
            byte[] data64 = new byte[data.length];

            byte last = 0;
            int i=BEGIN.length();
            for (; i < data.length; i++)
            {
                if (data[i] != '-' && last == '-')
                    break;
                last = data[i];
            }

            int j=0;
            for (; i < data.length && j < data64.length; i++)
            {
                if (data[i] == '-') // pocz�tek "-----END CERTIFICATE REQUEST-----"
                    break;
                if ((data[i] >= 'A' && data[i] <= 'Z') || (data[i] >= 'a' && data[i] <= 'z') ||
                    (data[i] >= '0' && data[i] <= '9') || data[i] == '+' || data[i] == '/' || data[i] == '=')
                {
                    data64[j++] = data[i];
                }
            }
            if (j == 0)
                throw new EdmException("Nie znaleziono danych w formacie PEM");
            byte[] tmp = new byte[j];
            System.arraycopy(data64, 0, tmp, 0, tmp.length);
            return Base64.decodeBase64(tmp);
        }

        throw new EdmException("Nie znaleziono danych w formacie PEM");
    }
*/

    /**
     * Zwraca obiekt {@link PKCS10CertificationRequest} utworzony
     * na podstawie jego reprezentacji binarnej.  Dane mog� mie�
     * format DER lub PEM (OpenSSL).
     */
    public static PKCS10CertificationRequest getPKCS10CertificationRequest(byte[] data) throws EdmException
    {
        if (data == null)
            throw new NullPointerException("data");

        try
        {
            return new PKCS10CertificationRequest(data);
        }
        catch (IllegalArgumentException e)
        {
            try
            {
                Object req = new PEMReader(new InputStreamReader(
                    new ByteArrayInputStream(data), "iso-8859-1")).readObject();
                if (!(req instanceof PKCS10CertificationRequest))
                {
                    throw new EdmException("Odczytano nieznany obiekt: "+
                        (req != null ? req.getClass().getName() : "null"));
                }

                return (PKCS10CertificationRequest) req;
            }
            catch (IOException e1)
            {
                throw new EdmException("Nieznany format pliku CSR", e1);
            }
        }
    }

    /**
     * Zwraca list� obiekt�w UserCertificate.
     */
    public static List<UserCertificate> findByUsername(String username, boolean signed) throws EdmException
    {
        if (username == null)
            throw new NullPointerException("username");

        try
        {
            List<UserCertificate> certs = (List<UserCertificate>)
                DSApi.context().classicSession().find("from c in class "+UserCertificate.class.getName()+
                " where c.signed = ? and c.username = ?", new Object[] { Boolean.valueOf(signed), username },
                new Type[] { Hibernate.BOOLEAN, Hibernate.STRING } );

            return certs;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static UserCertificate findById(Long id) throws EdmException
    {
        return (UserCertificate) Finder.find(UserCertificate.class, id);
    }

    public static List<UserCertificate> list(boolean signed) throws EdmException
    {
        try
        {
            List<UserCertificate> certs = (List<UserCertificate>)
                DSApi.context().classicSession().find("from c in class "+UserCertificate.class.getName()+
                " where c.signed = ?", new Object[] { Boolean.valueOf(signed) },
                new Type[] { Hibernate.BOOLEAN } );

            return certs;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    /**
     * Tworzy certyfikat (CSR) oczekuj�cy na podpisanie.  Podpisywanie
     * certyfikatu odbywa si� przy pomocy metody {@link #sign(java.security.cert.X509Certificate, java.security.PrivateKey, int, int)}.
     */
    public static UserCertificate create(PKCS10CertificationRequest csr, String username, String alias) throws EdmException
    {
        if (csr == null)
            throw new NullPointerException("csr");
        if (username == null)
            throw new NullPointerException("username");

        UserCertificate cert = new UserCertificate();
        cert.setAlias(alias);
        cert.setCtime(new Date());
        cert.setEncodedCsr(new String(Base64.encodeBase64(csr.getEncoded())));
        cert.setUsername(username);

        Persister.create(cert);

        return cert;
    }

    private X509Certificate x509Certificate;

    /**
     * Pobiera podpisany certyfikat jako obiekt {@link X509Certificate}.
     * Je�eli certyfikat nie zosta� jeszcze podpisany, zwraca null.
     * @return X509Certificate lub null.
     */
    public X509Certificate getX509Certificate() throws EdmException
    {
        if (x509Certificate == null)
        {
            if (encodedCrt == null)
                return null;
            x509Certificate = getX509Certificate(Base64.decodeBase64(encodedCrt.getBytes()));
        }
        return x509Certificate;
    }

    private PKCS10CertificationRequest pkcs10CertificationRequest;

    public PKCS10CertificationRequest getPKCS10CertificationRequest() throws EdmException
    {
        if (pkcs10CertificationRequest == null)
        {
            if (encodedCsr == null)
                return null;
            pkcs10CertificationRequest = new PKCS10CertificationRequest(Base64.decodeBase64(encodedCsr.getBytes()));
        }
        return pkcs10CertificationRequest;
    }

    private X509V3CertificateGenerator generator = new X509V3CertificateGenerator();

    public X509Certificate sign(X509Certificate caCertificate, PrivateKey caPrivateKey, //PublicKey userPublicKey,
                                int days, final int serial)
        throws EdmException
    {
        Date notAfter = new Date(System.currentTimeMillis() + days * DateUtils.DAY);
        //BigInteger serial = new BigInteger(id.toString());
        synchronized(generator)
        {
            generator.reset();
            generator.setSerialNumber(new BigInteger(String.valueOf(serial)));
            generator.setIssuerDN(new X509Name(caCertificate.getSubjectDN().toString()));
            generator.setNotBefore(new Date());
            generator.setNotAfter(notAfter);
            generator.setSubjectDN(new X509Name(getPKCS10CertificationRequest().getCertificationRequestInfo().getSubject().toString()));
            //generator.setPublicKey(userPublicKey);
            generator.setSignatureAlgorithm("SHA1WITHRSA");

            try
            {
                generator.setPublicKey(getPKCS10CertificationRequest().getPublicKey());
                generator.addExtension(X509Extensions.SubjectKeyIdentifier, false, createSubjectKeyId(getPKCS10CertificationRequest().getPublicKey()));
            }
            catch (Exception e)
            {
                throw new EdmException("B��d podczas podpisywania CSR", e);
            }

            generator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, createAuthorityKeyId(caCertificate.getPublicKey()));

            generator.addExtension(X509Extensions.BasicConstraints, false, new BasicConstraints(false));

            generator.addExtension(X509Extensions.KeyUsage, false,
                new KeyUsage(KeyUsage.keyAgreement | KeyUsage.keyEncipherment |
                KeyUsage.dataEncipherment | KeyUsage.digitalSignature));

            try
            {
                X509Certificate cert = generator.generateX509Certificate(caPrivateKey, "BC", new java.security.SecureRandom());
                cert.checkValidity(new Date());
                cert.verify(caCertificate.getPublicKey());

                PKCS7SignedData pkcs7 = new PKCS7SignedData(
                    caPrivateKey,
                    new Certificate[] { cert },
                    "SHA1" );

                encodedCrt = new String(Base64.encodeBase64(cert.getEncoded()), "iso-8859-1");
                encodedPkcs7 = new String(Base64.encodeBase64(pkcs7.getEncoded()), "iso-8859-1");
                expirationTime = notAfter;
                serialNumber = new Integer(serial);
                subjectDn = getPKCS10CertificationRequest().getCertificationRequestInfo().getSubject().toString();
                signed = true;
                signingTime = new Date();
                encodedSignature = new String(Base64.encodeBase64(cert.getSignature()), "iso-8859-1");

                return cert;
            }
            catch (Exception e)
            {
                throw new EdmException("B��d podczas podpisywania CSR", e);
            }
        }
    }

    private static SubjectKeyIdentifier createSubjectKeyId(PublicKey pubKey) throws EdmException
    {
        try
        {
            ByteArrayInputStream bIn = new ByteArrayInputStream(pubKey.getEncoded());
            //SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new DERInputStream(bIn).readObject());
            SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(bIn).readObject());
            return new SubjectKeyIdentifier(info);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu klucza publicznego", e);
        }
    }

    private static AuthorityKeyIdentifier createAuthorityKeyId(PublicKey pubKey) throws EdmException
    {
        try
        {
            ByteArrayInputStream bIn = new ByteArrayInputStream(pubKey.getEncoded());
            //SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new DERInputStream(bIn).readObject());
            SubjectPublicKeyInfo info = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(bIn).readObject());
            return new AuthorityKeyIdentifier(info);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu klucza publicznego", e);
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEncodedCsr()
    {
        return encodedCsr;
    }

    public void setEncodedCsr(String encodedCsr)
    {
        this.encodedCsr = encodedCsr;
    }

    public String getEncodedCrt()
    {
        return encodedCrt;
    }

    public void setEncodedCrt(String encodedCrt)
    {
        this.encodedCrt = encodedCrt;
    }

    public String getAlias()
    {
        return alias;
    }

    void setAlias(String alias)
    {
        this.alias = alias;
    }

    public Date getCtime()
    {
        return ctime;
    }

    void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public Date getSigningTime()
    {
        return signingTime;
    }

    void setSigningTime(Date signingTime)
    {
        this.signingTime = signingTime;
    }

    public boolean isSigned()
    {
        return signed;
    }

    void setSigned(boolean signed)
    {
        this.signed = signed;
    }

    public String getEncodedPkcs7()
    {
        return encodedPkcs7;
    }

    void setEncodedPkcs7(String encodedPkcs7)
    {
        this.encodedPkcs7 = encodedPkcs7;
    }

    public Date getExpirationTime()
    {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Integer getSerialNumber()
    {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber)
    {
        this.serialNumber = serialNumber;
    }

    public String getSubjectDn()
    {
        return subjectDn;
    }

    public void setSubjectDn(String subjectDn)
    {
        this.subjectDn = subjectDn;
    }

    public String getEncodedSignature()
    {
        return encodedSignature;
    }

    public void setEncodedSignature(String encodedSignature)
    {
        this.encodedSignature = encodedSignature;
    }
}
