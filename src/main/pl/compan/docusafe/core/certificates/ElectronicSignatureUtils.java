package pl.compan.docusafe.core.certificates;

import pl.compan.docusafe.core.dockinds.DocumentKind;

/**
 * Klasa narz�dziowa do podpis�w elektronicznych
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class ElectronicSignatureUtils {
    public final static String ELECTRONIC_SIGNATURE_AVAILABLE_PROPERTY = "electronic-signature";

    public static boolean isAvailable(DocumentKind kind){
        return Boolean.valueOf(
                kind.getProperties().get(ELECTRONIC_SIGNATURE_AVAILABLE_PROPERTY));
    }

    private ElectronicSignatureUtils(){}
}
