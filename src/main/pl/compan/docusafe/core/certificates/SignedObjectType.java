package pl.compan.docusafe.core.certificates;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.IdEntity;
import pl.compan.docusafe.util.StringManager;

/**
 * Enum określający rodzaj podpisanego obiektu
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
enum SignedObjectType implements IdEntity<Integer> {
    /** Podpis dotyczy pewnej wersji załącznika - AttachmentRevision */
    ATTACHMENT_REVISION(1),
    /** Podpis dotyczy dokumentu - Document */
    DOCUMENT(2),
    /** Podpis dotyczy pewnej czynności użytkownika - ProcessActionEntry */
    ACTION(3);

    static StringManager sm = GlobalPreferences.loadPropertiesFile(
            SignedObjectType.class.getPackage().getName(), null);

    final int id;
    /** klucz do zasobu opisującego dany podpis */
    final String descriptionKey = "SignedObjectType." + this.name();

    SignedObjectType(int id) {
        this.id = id;
    }
    public Integer getId(){
        return id;
    }
    public String getDescription(){
        return sm.getString(descriptionKey);
    }
}
