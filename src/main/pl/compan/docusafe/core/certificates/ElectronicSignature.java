package pl.compan.docusafe.core.certificates;

import pl.compan.docusafe.util.IdEntity;
import pl.compan.docusafe.util.IdEnumSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Obiekt s�u��cy do przechowywania wpis�w z podpisami electronicznymi
 *
 * Nie powinno si� korzysta� z obiekt�w tego typu w kodzie, tylko wykorzystywa� metody ElectronicSignatureStore
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
@Entity
@Table(name = "ds_electronic_signature")
public class ElectronicSignature implements ElectronicSignatureBean{

    @Id
    @SequenceGenerator(name = "ds_electronic_signature_seq", sequenceName = "ds_electronic_signature_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ds_electronic_signature_seq")
    Long id;

    /** Id zapisywanego obiektu, wraz z informacj� o typie obiektu */
    @Embedded
    Key key;

    /** Informacja jak dany wpis jest zapisany */
    @Column(name="persistence_type_id", nullable = false)
    int persistenceTypeId;

    @Column(name="date_", nullable = false)
    Date date;

    /** Zapisany w bazie danych plik xml, kt�ry zosta� podpisany */
    @Lob
    @Column(name = "signed_file", nullable = true)
    String signedFile;

    /** Plik zawieraj�cy informacje o podpisie */
    @Lob
    @Column(name = "signature_file", nullable = true)
    String signatureFile;

    /** Uri wskazuj�ce na miejsce zapisu - je�li signedFile i signatureFile nie s� zapisane w bazie danych */
    @Column(name="uri", nullable = true)
    String uri;

    /** ID dokumentu - wykorzystywane do szybkiego wyszukiwania */
    @Column(name="document_id", nullable = true)
    Long documentId;

    @Column(name="is_timestamp_sign", nullable = true)
    Boolean isTimestampSign = Boolean.FALSE;
    
    @Column(name="verifyResult", nullable = true)
    Boolean verifyResult = Boolean.FALSE;

    @Column(name="verifyDate", nullable = false)
    Date verifyDate;
    
    @Column(name="baseSignatureId", nullable = false)
    Long baseSignatureId;

	/**
     * Klucz pod��czaj�cy podpis do za��cznika, wspiera tylko klucze long
     */
    @Embeddable
    public static class Key implements Serializable {
        @Column(name = "fk_id", nullable = false)
        private long foreignKeyId;
        @Column(name = "fk_type_id", nullable = false)
        private int foreignKeyTypeId;

        long getId(){
            return foreignKeyId;
        }

        SignedObjectType getFkType(){
            return signedObjectSupport.get(foreignKeyTypeId);
        }
    }

    public static Key attachmentRevisionKey(long revId){
        Key ret = new Key();
        ret.foreignKeyId = revId;
        ret.foreignKeyTypeId = SignedObjectType.ATTACHMENT_REVISION.id;
        return ret;
    }

    public static Key documentIdKey(long documentId){
        Key ret = new Key();
        ret.foreignKeyId = documentId;
        ret.foreignKeyTypeId = SignedObjectType.DOCUMENT.id;
        return ret;
    }

    public static Key processActionIdKey(long processActionEntryId){
        Key ret = new Key();
        ret.foreignKeyId = processActionEntryId;
        ret.foreignKeyTypeId = SignedObjectType.ACTION.id;
        return ret;
    }

    private final static IdEnumSupport<Integer, PersistenceType> persistenceTypeSupport
            = IdEnumSupport.create(PersistenceType.class);
    static enum PersistenceType implements IdEntity<Integer>{
        DATABASE(1);
        private final int id;
        PersistenceType(int id) {
            this.id = id;
        }
        public Integer getId(){
            return id;
        }
    }

	private final static IdEnumSupport<Integer, SignedObjectType> signedObjectSupport
            = IdEnumSupport.create(SignedObjectType.class);


    PersistenceType getPersistenceType(){
        return persistenceTypeSupport.get(persistenceTypeId);
    }

    void setPersistenceType(PersistenceType pt){
        persistenceTypeId = pt.id;
    }

    public Date getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public SignedObjectType getSignedObjectType() {
        return key.getFkType();
    }

    public String getSignatureFile() {
        return signatureFile;
    }

    public void setSignatureFile(String signatureFile) {
        this.signatureFile = signatureFile;
    }

    public String getSignedFile() {
        return signedFile;
    }

    public Boolean isTimestampSign() {
        return isTimestampSign;
    }

    public void setIsTimestampSign(Boolean timestampSign) {
        isTimestampSign = timestampSign;
    }
    
    public Boolean getVerifyResult() {
		return verifyResult;
	}

	public void setVerifyResult(Boolean verifyResult) {
		this.verifyResult = verifyResult;
	}

	public Date getVerifyDate() {
		return verifyDate;
	}

	public void setVerifyDate(Date verifyDate) {
		this.verifyDate = verifyDate;
	}
	
	public Long getBaseSignatureId() {
		return baseSignatureId;
	}

	public void setBaseSignatureId(Long baseSignatureId) {
		this.baseSignatureId = baseSignatureId;
	}
	public Long getDocumentId(){
		return key.getId();
	}
}
