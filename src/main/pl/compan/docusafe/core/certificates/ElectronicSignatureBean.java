package pl.compan.docusafe.core.certificates;


import java.util.Date;

/**
 * Klasa zawieraj�ca informacje o podpisie elektronicznym
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface ElectronicSignatureBean {

    /**
     * Zwraca identyfikator zapisanego podpisu
     * @return
     */
    long getId();

    /**
     * Zwraca czas podpisu
     * @return
     */
    Date getDate();

    /**
     * Zwraca rodzaj podpisanego obiektu
     * @return
     */
    SignedObjectType getSignedObjectType();
}
