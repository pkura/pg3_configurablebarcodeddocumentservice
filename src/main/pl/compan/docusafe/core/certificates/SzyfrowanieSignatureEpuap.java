    
package pl.compan.docusafe.core.certificates;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;


public class SzyfrowanieSignatureEpuap {

	public static final String DEFAULT_ENCODING = "UTF-8";
	private String doZakodowania;
	private String doOdkodowania;
	private String key = "ab1$%(uhf1c76&C*(ju423o";


	
	private static String base64encode(String text) {
		try {
			String rez = String.valueOf(new Base64().encode(text.getBytes(DEFAULT_ENCODING)));
			return rez;
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}// base64encode

	private static String base64decode(String text) {

		try {
			return String.valueOf(new Base64().decode(text.getBytes(DEFAULT_ENCODING)));
		} catch (IOException e) {
			return null;
		}

	}// base64decode

	private static String xorMessage(String message, String key) {
		try {
			if (message == null || key == null)
				return null;

			char[] keys = key.toCharArray();
			char[] mesg = message.toCharArray();

			int ml = mesg.length;
			int kl = keys.length;
			char[] newmsg = new char[ml];

			for (int i = 0; i < ml; i++) {
				newmsg[i] = (char) (mesg[i] ^ keys[i % kl]);
			}// for i
			mesg = null;
			keys = null;
			return new String(newmsg);
		} catch (Exception e) {
			return null;
		}
	}// xorMessage

	public String Szyfruj(String doZakodowania) {
		
		String txt = xorMessage(doZakodowania, key);
		String res = base64encode(txt);
		
		return res;
	}
	public String Odszyfruj(String doOdkodowania) {
		String txt = base64decode(doOdkodowania);
		String res = xorMessage(txt, key);
	
		
		return res;
	}

	
//	{
//		String txt = "some text to be encrypted grzesiek ";
//		String key = "ab1$%(uhf1c76&C*(ju423o";
//		System.out.println(txt + " XOR-ed to: " + (txt = xorMessage(txt, key)));
//		String encoded = base64encode(txt);
//		System.out.println(" is encoded to: " + encoded
//				+ " and that is decoding to: " + (txt = base64decode(encoded)));
//		System.out.print("XOR-ing back to original: " + xorMessage(txt, key));
//
//	}

}// class