package pl.compan.docusafe.core.certificates.timestamp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jfree.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.compan.docusafe.core.certificates.ElectronicSignature;

import com.google.common.base.Charsets;

/**
 * Klasa s�u�y do znakowania znacznikiem czasu podpis�w
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class ElectronicSignatureTimestampStore
{
    private final static ElectronicSignatureTimestampStore instance = new ElectronicSignatureTimestampStore();
    private static String timestampCanonicalizationMethodAlgorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";

    public static ElectronicSignatureTimestampStore getInstance() {
        return instance;
    }

    /**
     * Metoda zwraca podpis z dodanym znacznikiem czasu.
     * @param es
     * @throws Exception
     */
    public String signTimestamp(ElectronicSignature es) throws Exception
    {
        InputStream is = null;
        try
        {
            is = new ByteArrayInputStream(es.getSignatureFile().getBytes(Charsets.UTF_8));
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);

            if(appendToUnsignedSignatureProperties(doc))
            	Log.info("Dodano znacznik czasu.");
            else
            	Log.info("B��dne dodanie znacznika czasu.");
            	

            return getDocumentAsString(doc);
        }finally{
            if(is != null)
                is.close();
        }

    }

    /**
     * Konwertuje dokument dom
     * @param doc
     * @throws TransformerException
     */
    private String getDocumentAsString(Document doc) throws TransformerException, IOException
    {
        StringWriter sw = null;
        try
        {
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //create string from xml tree
            sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            String xml = sw.toString();

            return xml;
        }finally{
            if(sw != null)
                sw.close();
        }

    }

    private boolean appendToUnsignedSignatureProperties(Document doc) throws Exception {
        NodeList qpnl = doc.getElementsByTagName("xades:QualifyingProperties");
        NodeList upelementl = doc.getElementsByTagName("xades:UnsignedProperties");
        NodeList uspropertiesl = doc.getElementsByTagName("xades:UnsignedSignatureProperties");

        if (qpnl.getLength() > 0) {
            Element elem = getElementSignatureTimeStamp(doc);
            if (upelementl.getLength() == 0) {
                Element UPElement = doc.createElement("xades:UnsignedProperties");
                Element USProperties = doc.createElement("xades:UnsignedSignatureProperties");
                USProperties.appendChild(elem);
                UPElement.appendChild(USProperties);
                qpnl.item(0).appendChild(UPElement);
                return true;
            } else {
                Node UPElement = upelementl.item(0);
                if (uspropertiesl.getLength() == 0) {
                    Element USProperties = doc.createElement("xades:UnsignedSignatureProperties");
                    USProperties.appendChild(elem);
                    UPElement.appendChild(USProperties);
                    qpnl.item(0).appendChild(UPElement);
                    return true;
                } else {
                    Node USProperties = uspropertiesl.item(0);
                    USProperties.appendChild(elem);
                    return true;
                }
            }
        }
        return false;
    }

    private Element getElementSignatureTimeStamp(Document doc) throws Exception {
        System.out.println("Znakowanie czasem...");
        Element signatureTimeStamp = doc.createElement("ds:SignatureTimeStamp");
        Element canonicalizationMethod = doc.createElement("ds:CanonicalizationMethod");
        canonicalizationMethod.setAttributeNS("", "Algorithm", timestampCanonicalizationMethodAlgorithm);
        Element encapsulatedTimeStamp = doc.createElement("ds:EncapsulatedTimeStamp");
        encapsulatedTimeStamp.setTextContent(RemoteTimestampFactory.getEncapsulatedTimeStamp(doc));
        signatureTimeStamp.appendChild(canonicalizationMethod);
        signatureTimeStamp.appendChild(encapsulatedTimeStamp);

        return signatureTimeStamp;
    }

    private Element getElementArchiveTimeStamp(Document doc) throws Exception {
        System.out.println("Znakowanie czasem (archiwalny znacznik)...");
        Element signatureTimeStamp = doc.createElement("ArchiveTimeStamp");
        Element canonicalizationMethod = doc.createElement("CanonicalizationMethod");
        canonicalizationMethod.setAttributeNS("", "Algorithm", timestampCanonicalizationMethodAlgorithm);
        Element encapsulatedTimeStamp = doc.createElement("EncapsulatedTimeStamp");
        encapsulatedTimeStamp.setTextContent(RemoteTimestampFactory.getEncapsulatedTimeStamp(doc));
        signatureTimeStamp.appendChild(canonicalizationMethod);
        signatureTimeStamp.appendChild(encapsulatedTimeStamp);

        return signatureTimeStamp;
    }
}
