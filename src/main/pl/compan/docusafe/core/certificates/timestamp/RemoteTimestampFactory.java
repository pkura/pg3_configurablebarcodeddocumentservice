package pl.compan.docusafe.core.certificates.timestamp;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.bouncycastle.tsp.*;
import org.bouncycastle.util.encoders.Base64;
import org.w3c.dom.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.SortedMap;

/**
 * Klasa odpowiedzialna za wygenerowanie znacznika czasu
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RemoteTimestampFactory {

    private final static Logger log = LoggerFactory.getLogger(RemoteTimestampFactory.class);

    //serwer udost�pniaj�cy czas zgodny z TSA
    private static String timestampServerURL = "http://timestamping.edelweb.fr/service/tsp";

    /**
     * Pobiera timestamp do podpisu z ustawionego urla
     * @param doc
     * @return
     * @throws Exception
     */
    public static String getEncapsulatedTimeStamp(Document doc)throws Exception {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(timestampServerURL);
        TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();

        reqGen.setCertReq(false);

        byte[] hash = getSignatureValueBytes(doc);

        TimeStampRequest request = reqGen.generate(TSPAlgorithms.SHA1, hash);
        byte[] enc_req = request.getEncoded();
        ByteArrayInputStream bais = new ByteArrayInputStream(enc_req);


        method.setRequestEntity(new InputStreamRequestEntity(bais,"application/timestamp-query"));
        //method.setRequestBody(bais);
        //method.setRequestContentLength(enc_req.length);
        //method.setRequestHeader("Content-type", "application/timestamp-query");
        client.executeMethod(method);

        TimeStampResponse resp = validateResponse(method, request);

        byte[] responseBody = resp.getEncoded();
        String responseStringB64 = new String(Base64.encode(responseBody));

        log.trace("Czas: " + new String((responseBody)));
        log.trace(responseStringB64);

        return responseStringB64;
    }

    private static TimeStampResponse validateResponse(PostMethod method, TimeStampRequest request) throws IOException, TSPException {
        InputStream in = method.getResponseBodyAsStream();
        TimeStampResponse resp = new TimeStampResponse(in);

        resp.validate(request);
        System.out.println("Timestamp validated");
        return resp;
    }

    private static byte[] getSignatureValueBytes(Document doc) throws NoSuchAlgorithmException {
        String signatureValue = doc.getElementsByTagName("ds:SignatureValue").item(0).getTextContent();
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        SortedMap<String, Charset> charsets = Charset.availableCharsets();
        Charset utf8 = charsets.get("UTF-8");
        //byte[] hash = md.digest(new String(Base64.decode(signatureValue)).getBytes(utf8));
        log.trace(signatureValue);
        return md.digest(signatureValue.getBytes(utf8));
    }
}
