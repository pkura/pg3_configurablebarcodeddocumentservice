package pl.compan.docusafe.core.certificates.timestamp;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * W�tek sprawdzaj�cy istniej�ce podpisy w ds_electronic_signature i znakuje je znacznikiem czasu.
 * @see pl.compan.docusafe.core.certificates.timestamp pakiet definiuje zarzadzanie znacznikami czasu
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class SignatureTimestampService extends ServiceDriver implements Service {

    private final static Logger log = LoggerFactory.getLogger(SignatureTimestampService.class);
    private Timer timer;
    private ElectronicSignatureStore signatureManager = ElectronicSignatureStore.getInstance();
    private ElectronicSignatureTimestampStore timestampManager = ElectronicSignatureTimestampStore.getInstance();

    Integer time = 5;
    @Override
    protected void start() throws ServiceException {
        System.out.println("SignatureTimestamp START");

        timer = new Timer(true);
        timer.schedule(new SignatureTimestamp() , 1000, time * DateUtils.SECOND);
        System.out.println("SignatureTimestamp wystartowa�");
    }

    public class SignatureTimestamp extends TimerTask {

        @Override
        public void run()
        {
            try
            {
                DSApi.openAdmin();
                List<ElectronicSignature> signatures = signatureManager.list(false);
                List<ElectronicSignature> signaturesToUpdates = new ArrayList<ElectronicSignature>();

                for(ElectronicSignature es : signatures)
                {
                     timestampSignature(es, signaturesToUpdates);
                }

                updateTimestampSignatures(signaturesToUpdates);

            }catch(Exception e){
                console(Console.ERROR, " Nieoczekiwany b��d [" + e.getMessage() + "]", e);
                log.error("", e);
            } finally {
                DSApi._close();
            }
        }

        /**
         * Otwiera kontext i aktualizuje podpisy w bazie danych. Oznaczone czasem.
         * @param signaturesToUpdates
         */
        private void updateTimestampSignatures(List<ElectronicSignature> signaturesToUpdates)
        {
            try
            {
                DSApi.context().begin();

                for(ElectronicSignature es : signaturesToUpdates)
                {
                     signatureManager.update(es);
                }

                DSApi.context().commit();
            }catch(Exception e){
                DSApi.context()._rollback();
                console(Console.ERROR, "B��d zapisu danych do bazy [" +e.getMessage()+ "]", e);
                log.error("", e);
            }
        }

        /**
         * Znakuje znacznikiem czasu podpis
         * @param es
         * @return
         * @throws Exception
         */
        private void timestampSignature(ElectronicSignature es,List<ElectronicSignature> signToUpdate) throws Exception
        {
            String signatureTimestampable = null;
            boolean isValidAfterDays = validateX509Certificate(es);
            //czy certyfiakt b�dzie nie wa�ny niedlugo
            if(!isValidAfterDays)
            {
                signatureTimestampable = timestampManager.signTimestamp(es);
            }

            if(signatureTimestampable != null)
            {
                es.setSignatureFile(signatureTimestampable);
                es.setIsTimestampSign(Boolean.TRUE);
                signToUpdate.add(es);
            }
        }

        /**
         * Sprawdza czy certyfikat jest wa�ny dla okre�lonej daty.
         * @TODO co zrobi� z pozosta�ymi exceptionami
         * @param es
         * @return
         * @throws Exception
         */
        private boolean validateX509Certificate(ElectronicSignature es) throws Exception
        {
            try
            {
                X509Certificate cert = signatureManager.extractX509Certificate(es);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, 7);
                cert.checkValidity(cal.getTime());
                return true;
            }catch(CertificateExpiredException e){
                log.error("Certyfikat wygas�");
                return false;
            }
        }
    }

    @Override
    protected void stop() throws ServiceException {
    }

    @Override
    protected boolean canStop() {
        return false;
    }
}
