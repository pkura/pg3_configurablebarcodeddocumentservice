package pl.compan.docusafe.core.certificates.auth;

import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;

import javax.persistence.*;

/**
 * certyfikat u�ytkownika
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Entity
@DiscriminatorValue("USER")
@NamedQueries(
        @NamedQuery(name = "DSUserPublicKey.findByUserId",
                    query = "SELECT d FROM DSUserPublicKey d WHERE d.user.id = :userId"
        )
)
public class DSUserPublicKey extends DSPublicKey{

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserImpl user;


    public UserImpl getUser() {
        return user;
    }

    public void setUser(UserImpl user) {
        this.user = user;
    }
}
