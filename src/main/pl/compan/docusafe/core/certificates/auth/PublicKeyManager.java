package pl.compan.docusafe.core.certificates.auth;

import com.google.common.collect.Lists;

import org.apache.tika.io.IOUtils;
import org.apache.ws.commons.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

/**
 * Klasa s�u��ca do zapisywania kluczy publicznych
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class PublicKeyManager {

    private static final Logger log = LoggerFactory.getLogger(PublicKeyManager.class);

    public static void create(DSUser user, InputStream is) throws EdmException {
       try {
            byte[] keyBytes = IOUtils.toByteArray(is);

            String temp = new String(keyBytes);
            String publicKeyPEM = temp.replace("-----BEGIN PUBLIC KEY-----\r\n", "");
            publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");

           byte[] decoded = Base64.decode(publicKeyPEM);

           validatePublicKey(decoded);

           create(user, publicKeyPEM);
       }catch(Exception e){
           log.error("", e);
           throw new EdmException(e);
       }
    }

    protected static void validatePublicKey(byte[] decoded) throws NoSuchAlgorithmException, InvalidKeySpecException {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(decoded);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey publicKey = kf.generatePublic(spec);
    }

    protected static void create(DSUser user, String publicKeyPEM) throws EdmException {
        DSUserPublicKey userPublicKey = findByUserId(user);
        //userPublicKey.setUser((UserImpl) user);
        userPublicKey.setPublicKey(publicKeyPEM);

        DSApi.context().session().saveOrUpdate(userPublicKey);
    }

    /**
     * Waliduje host oraz dost�py po uri
     * @param hostName
     * @param uri
     * @param base64Cert
     * @return
     */
    public static boolean validateByHostName(String hostName, String uri, String base64Cert){
        boolean result = false;

        List<DSPublicKey> publicKeys = findByHostName(hostName);

        if(!CollectionUtils.isEmpty(publicKeys)){ // nie jest puste veryfikujemy
            for(DSPublicKey pk : publicKeys){
                if(pk.containsUri(uri) && pk.getKeyType().validate(pk.getPublicKey(), base64Cert))
                    result = true;
            }
        }

        return result;
    }

    public static List<DSPublicKey> findByHostName(String hostName){
        List<DSPublicKey> results = Lists.newArrayList();
        try {
            results = DSApi.context().session().getNamedQuery("DSPublicKey.findByHostName")
                    .setParameter("hostName", hostName)
                    .list();
        } catch (Exception e) {
            log.error("", e);
        }
        log.trace("{}", results);
        return results;
    }

    public static DSPublicKey findById(Long id) throws EdmException {
        return (DSPublicKey) DSApi.context().session().get(DSPublicKey.class, id);
    }

    public static List<DSHostPublicKey> list(){
        List<DSHostPublicKey> results = Lists.newArrayList();
        try {
            results = DSApi.context().session().getNamedQuery("DSHostPublicKey.list")
                    .list();
        } catch (Exception e) {
            log.error("", e);
        }
        log.trace("size {}", results.size());
        return results;
    }

    protected static DSUserPublicKey findByUserId(DSUser user) throws EdmException {
        try
        {
            DSUserPublicKey publicKey = (DSUserPublicKey) DSApi.context().session().getNamedQuery("DSUserPublicKey.findByUserId")
                .setParameter("userId", user.getId()).uniqueResult();

            return publicKey;
        }catch(Exception e){
            log.error("", e);
        }

        return new DSUserPublicKey();
    }
    //DSHostPublicKey.findOneByHostName
    public static List<DSHostPublicKey> findOneByHostName(String hostName){
        List<DSHostPublicKey> results = null;
        try {
            results = (List<DSHostPublicKey>) DSApi.context().session().getNamedQuery("DSHostPublicKey.findOneByHostName")
            		.setParameter("hostName", hostName).list();
        } catch (Exception e) {
            log.error("", e);
        }
        log.trace("size {}", results);
        return results;
    }

}
