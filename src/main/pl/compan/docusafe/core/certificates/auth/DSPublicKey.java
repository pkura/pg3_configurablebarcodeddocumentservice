package pl.compan.docusafe.core.certificates.auth;


import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Obiekt s�u��cy do przechowywania kluczy publicznych, w celu weryfikacji
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Entity
@Table(name = "ds_public_key")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DISCRIMINATOR", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({
        @NamedQuery(name = "DSPublicKey.list", query = "SELECT d FROM DSPublicKey d"),
        @NamedQuery(name = "DSPublicKey.findByHostName", query = "SELECT d FROM DSPublicKey d WHERE d.hostName = :hostName OR d.hostName = '*'")
})
public abstract class DSPublicKey implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "key_type", nullable = false)
    private PublicKeyType keyType;

    @Column(name = "host_name")
    private String hostName;

    // zakodowany w base64 klucz publiczny
    @Column(name = "public_key", length = 4000)
    private String publicKey;

    @Column(name = "enable_uri")
    private String enableUris = "/api,/services,/atom10,/atom11,/browser,/services11,/services10";

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public PublicKeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(PublicKeyType keyType) {
        this.keyType = keyType;
    }
    public void setKeyType(String keyType) {
        this.keyType = PublicKeyType.valueOf(keyType);
    }

    public String getEnableUris() {
        return enableUris;
    }

    public void setEnableUris(String enableUris) {
        this.enableUris = enableUris;
    }

    public boolean containsUri(String uri) {
        //narazie zwraca true, w razie gdyby dost�p mia�byc rozszerzony globalnie na DS
        return true;
//        boolean ok = false;
//        if(StringUtils.isBlank(enableUris))
//            return ok;
//
//        if(enableUris.equals("*")){
//            ok = true;
//        } else {
//            for(String enableUri : StringUtils.split(enableUris, ",")){
//                if(uri.startsWith(enableUri))
//                    ok = true;
//            }
//        }
//
//        return ok;
    }

    @Override
    public String toString() {
        return "DSPublicKey{" +
                "id=" + id +
                ", keyType=" + keyType +
                ", hostName='" + hostName + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", enableUris='" + enableUris + '\'' +
                '}';
    }
}
