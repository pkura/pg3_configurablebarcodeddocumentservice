package pl.compan.docusafe.core.certificates.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.util.Base64;
import system.NotImplementedException;

import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * Rodzaj klucza jakim b�dzie weryfikacja dost�pu do API
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public enum PublicKeyType {

    ACCESS {
        @Override
        public boolean validate(String serverBase64, String base64) {
            return true;
        }
    },
    FORBIDDEN {
        public boolean validate(String serverBase64, String base64) {
            return false;
        }
    },
    X509 {
        @Override
        public boolean validate(String serverBase64, String base64) {
            try {
                Certificate serverCertificate = getCertificate(serverBase64);
                Certificate requestCertificate = getCertificate(base64);

                if(serverCertificate.equals(requestCertificate))
                    return true;
            } catch (CertificateException e) {
                log.error("", e);
            }
            return false;

        }

        protected Certificate getCertificate(String serverBase64) throws CertificateException {
            byte[] decode = Base64.decode(serverBase64.getBytes());
            return CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(decode));
        }
    }

    ;
    private static final Logger log = LoggerFactory.getLogger(PublicKeyType.class);
    /**
     * Metoda ma zadanie walidowa� klucz/certyfikat jaki zosta� przes�any
     * zgodnie z metod�
     * @param serverBase64
     * @param base64
     * @return
     */
    public abstract boolean validate(String serverBase64, String base64);

}
