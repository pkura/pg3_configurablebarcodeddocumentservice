package pl.compan.docusafe.core.certificates.auth;

import javax.persistence.*;

/**
 * certyfikat dla danego hostu
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@Entity
@DiscriminatorValue("HOST")
@NamedQueries({
        @NamedQuery(name = "DSHostPublicKey.list", query = "SELECT d FROM DSHostPublicKey d"),
        @NamedQuery(name = "DSHostPublicKey.findOneByHostName", query = "SELECT d FROM DSHostPublicKey d WHERE d.hostName = :hostName")
})
public class DSHostPublicKey extends DSPublicKey {

}
