package pl.compan.docusafe.core.certificates;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.XPathException;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.dockinds.process.ProcessActionEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Preconditions;


/**
 * Klasa do zapisu/odczytu podpis�w elektronicznych
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ElectronicSignatureStore {
    private final static Logger LOG = LoggerFactory.getLogger(ElectronicSignatureStore.class);

    //do rozbudowania (istnieje wiele mo�liwo�ci zapisu)
    private final static ElectronicSignatureStore INSTANCE = new ElectronicSignatureStore();
    private ElectronicSignatureStore() {}

    static class ESBean implements ElectronicSignatureBean {
        final ElectronicSignature.Key key;
        final Date date;
        final long id;

        public ESBean(Long id, ElectronicSignature.Key key, Date date) {
            this.key = key;
            this.id = id;
            this.date = date;
        }

        public Date getDate() {
            return date;
        }

        public long getId() {
            return id;
        }

        public SignedObjectType getSignedObjectType() {
            return key.getFkType();
        }
    }

    public static ElectronicSignatureStore getInstance(){
        return INSTANCE;
    }

    /**
     * Zapisuje podpis za��cznika
     * @param attachmentRevision - id wersji za��cznika
     * @param signedFile tre�� podpisywanego pliku
     * @param signatureFile tre�� podpisanego pliku lub tre�� podpisu
     * @throws EdmException
     * @throws FileNotFoundException 
     */
    public void storeAttachmentSignature(long attachmentRevision, String signedFile, String signatureFile) throws EdmException, FileNotFoundException {
        LOG.info("storing in database...");
        ElectronicSignature es = prepareElectronicSignature(ElectronicSignature.attachmentRevisionKey(attachmentRevision),
                signedFile,
                signatureFile);
        es.documentId = AttachmentRevision.find(attachmentRevision).getAttachment().getDocument().getId();
        DSApi.context().session().save(es);
    }

    /**
     * Wyciaga podpis
     * @param es
     * @return
     * @throws Exception
     */
    public X509Certificate extractX509Certificate(ElectronicSignature es) throws Exception {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Object result = getCertificateElement(es);
        LOG.debug("xpath result = {}", result);
        if(result instanceof Element) {
            Element el = (Element) result;
            StringBuilder sb = new StringBuilder()
            		.append("-----BEGIN CERTIFICATE-----\n")
            		.append(el.getText().replace(" ",""))
            		.append("\n-----END CERTIFICATE-----");
            LOG.debug("der encoding:\n{}", sb);
            return (X509Certificate) cf.generateCertificate(IOUtils.toInputStream(sb.toString()));
        } else if(result instanceof ArrayList){
        	ListIterator iter = ((ArrayList) result).listIterator();
        	while(iter.hasNext()){
        		Object o = iter.next();
        		if(o instanceof Element){
        			Element el = (Element) o;
                    StringBuilder sb = new StringBuilder()
                    		.append("-----BEGIN CERTIFICATE-----\n")
                    		.append(el.getText().replace(" ",""))
                    		.append("\n-----END CERTIFICATE-----");
                    LOG.debug("der encoding:\n{}", sb);
                    return (X509Certificate) cf.generateCertificate(IOUtils.toInputStream(sb.toString()));
        		} else{
                    throw new EdmException("Unsuported signature for ID = " + es.getId());
        		}
        	}
            throw new EdmException("Unsuported signature for ID = " + es.getId());
        } else{
            throw new EdmException("Unsuported signature for ID = " + es.getId());
        }
    }
    
    public int caluclateNumberOfSignatures(ElectronicSignature es) throws DocumentException, IOException{
    	Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ds","http://www.w3.org/2000/09/xmldsig#");
		
		Document xml = new SAXReader().read(IOUtils.toInputStream(es.getSignatureFile(),"utf-8"));
	    XPath xpath = xml.createXPath("//ds:X509Certificate");
	
	    xpath.setNamespaceURIs(namespaces);
	    Object result = xpath.evaluate(xml.getRootElement());			
		
	    if(result instanceof Element)
	    	return 1;
	    else if(result instanceof ArrayList)
	    	return ((ArrayList)result).size();
	    else
	    	return 0;
    }

    /**
     * Zwraca element certyfikatu z podpisu na podstawie
     * @param es {@link ElectronicSignature} getSignatureFile()
     * @return
     * @throws DocumentException
     * @throws IOException
     */
	private Object getCertificateElement(ElectronicSignature es) throws DocumentException, IOException {
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ds","http://www.w3.org/2000/09/xmldsig#");
		
		Document xml = new SAXReader().read(IOUtils.toInputStream(es.getSignatureFile(),"utf-8"));
	    XPath xpath = xml.createXPath("//ds:X509Certificate");
	
	    xpath.setNamespaceURIs(namespaces);
	    Object result = xpath.evaluate(xml.getRootElement());			
		
		return result;
	}

    /**
     * Zapisuje podpis dokumentu
     * @param documentId
     * @param signedFile
     * @param signatureFile
     * @throws EdmException
     * @throws SQLException 
     * @throws IOException 
     */
   
	public void storeDocumentSignature(long documentId, String signedFile, String signatureFile) throws EdmException, SQLException, IOException {
        LOG.info("storing in database...");
        ElectronicSignature es = prepareElectronicSignature(ElectronicSignature.documentIdKey(documentId), signedFile, signatureFile);
        es.documentId = documentId;
        
        
        // jako ze firebird nie wspiera Clob  i przy zaisywaniu pliku do bazy jako blob
        // ucina� w ru�nych miejscach syganture w XML-u co uniemozliwialo jej poprawne pobranie oraz wys�anie
        // sygantura jest szyfrowana i zapisywana do katalogu ze �cie�k� w adsach "firebird.pathToEpuapDocuments" 
        
       if (DSApi.isFirebirdServer()){
       
    	   PreparedStatement pst = null;
    	   
    	   long liczbaWpisow = 0;
           pst = DSApi.context().prepareStatement("select count(*) from ds_electronic_signature");
		  
		   ResultSet rs = pst.executeQuery();
		 
		   if (rs.next()){
		    liczbaWpisow  = rs.getLong(1);
		    }
		   pst.close();
           try {
   			
   			String idDokumentu = es.documentId.toString();
   			String signatureFileAsBytes = es.getSignatureFile();
   			
   			//przygotowanie sciezki do zapisu sygnatury 
   			StringBuilder path = new StringBuilder();
   			try{
   			if (Docusafe.getAdditionProperty("firebird.pathToEpuapDocuments") == null ){
   				throw new EdmException("W ustawieniach aplikacji nie skonfigurowano sciezki dla dokument�w ePUAP" +
   						"\n W celu usuniecia b��du, skontaktuj si� z wsparciem technicznym COM-PAN SYSTEM tel. 534 101 002 ");
   			}
   			}catch (EdmException e) {
   				LOG.error("�cie�ka w firebird.pathToEpuapDocuments nie zosta�a skonfigurowana poprawnie");
			} 
   				
   		 
            path.append(Docusafe.getAdditionProperty("firebird.pathToEpuapDocuments"));
        
            path.append("/");
           
            path.append(idDokumentu);
            new File(path.toString()).mkdirs();
            path.append("/");
            String filename = Long.toHexString(es.documentId);
            int len = filename.length();
            filename = ("00000000".concat(filename)).substring(len);
         
            path.append(filename.toUpperCase());
            path.append(liczbaWpisow+1);
            path.append(".DAT");

            String lokalizacja =path.toString();
           
               FileOutputStream fos = null;
               try {
            	   //szyfrowanie sygnatury
            	   SzyfrowanieSignatureEpuap  sse = new SzyfrowanieSignatureEpuap();
            	   String signature = sse.Szyfruj(signatureFileAsBytes);
            	   
            	   fos = new FileOutputStream(lokalizacja); //Otwieranie pliku 
            	    for(int i = 0; i < signature.length(); i++){
            	      fos.write((int)signature.charAt(i)); //Zapis bajt po bajcie ka�dego znaku...
            	    }
            	 } catch(IOException ex){
            	    System.out.println("B��d operacji na pliku: "+ex);
            	 }
               try {
            	   fos.close(); //Zamykanie pliku 
            	  } catch (IOException e2) {
            	    e2.printStackTrace();
            	  } 
        es.uri = lokalizacja;
        es.signatureFile = ""; 
       
           } catch (Exception e) {
			// TODO: handle exception
		}
       }
        
       DSApi.context().session().save(es);

        OfficeDocument document = OfficeDocument.find(documentId);
        
		if (AvailabilityManager.isAvailable("createAttachmentFromSingedEpuapDocument"))
		{ 
			/**Tworze Za�acznik z podpisanego dokuemntu*/
			Attachment attachment = new Attachment("PodpisanyDokument");
			document.createAttachment(attachment); 
			StringBuilder sb = new StringBuilder();
			sb.append(System.getProperty("catalina.base"));
			sb.append("/temp/");
			  String filename = Long.toHexString(es.documentId);
			   int len = filename.length();
	            filename = ("00000000".concat(filename)).substring(len);
	         
	            sb.append(filename);
	            sb.append(".xml");
			File file= new File(sb.toString());
			FileOutputStream fos =  new FileOutputStream(file);
			for(int i = 0; i < es.getSignatureFile().length(); i++){
      	      fos.write((int)es.getSignatureFile().charAt(i)); //Zapis bajt po bajcie ka�dego znaku...
      	    }
			  fos.close();
			  
			 // es.uri = sb.toString();
			attachment.createRevision(file);
			
		}
		if (AvailabilityManager.isAvailable("blockDocumentAfterSinging"))
		
			document.setBlocked(true);

		
    	DSUser user = DSApi.context().getDSUser();
    	DateUtils date = new DateUtils();
    	String data = date.formatCommonDateTime(es.date);
    	
       document.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),user.getName(),
                "Pismo zosta�o podpisane przez : "+user.asLastnameFirstname() + " w dniu : " +data));

         TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
	}
	
	  public static Document stringToXml(String xmlSource) throws SAXException, ParserConfigurationException, IOException {
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        return (Document) builder.parse(new InputSource(new StringReader(xmlSource)));
	    }
	public long storeDocumentSignatureFromFile(long documentId, String signedFile, String signatureFile, Date date) throws EdmException, FileNotFoundException, SQLException {
		return storeDocumentSignatureFromFile(documentId, signedFile, signatureFile, date, null);
	}
	
	public long storeDocumentSignatureFromFile(long documentId, String signedFile, String signatureFile, Date date, Long baseSignatureId) throws EdmException, FileNotFoundException, SQLException {
		LOG.info("storing in database...");
		ElectronicSignature es = prepareElectronicSignature(ElectronicSignature.documentIdKey(documentId), signedFile, signatureFile, date);
		es.documentId = documentId;


		// jako ze firebird nie wspiera Clob  i przy zaisywaniu pliku do bazy jako blob
		// ucina� w ru�nych miejscach syganture w XML-u co uniemozliwialo jej poprawne pobranie oraz wys�anie
		// sygantura jest szyfrowana i zapisywana do katalogu ze �cie�k� w adsach "firebird.pathToEpuapDocuments" 

		if (DSApi.isFirebirdServer()){

			PreparedStatement pst = null;

			long liczbaWpisow = 0;
			pst = DSApi.context().prepareStatement("select count(*) from ds_electronic_signature");

			ResultSet rs = pst.executeQuery();

			if (rs.next()){
				liczbaWpisow  = rs.getLong(1);
			}
			pst.close();
			try {

				String idDokumentu = es.documentId.toString();
				String signatureFileAsBytes = es.getSignatureFile();

				//przygotowanie sciezki do zapisu sygnatury 
				StringBuilder path = new StringBuilder();
				try{
					if (Docusafe.getAdditionProperty("firebird.pathToEpuapDocuments") == null ){
						throw new EdmException("W ustawieniach aplikacji nie skonfigurowano sciezki dla dokument�w ePUAP" +
						"\n W celu usuniecia b��du, skontaktuj si� z wsparciem technicznym COM-PAN SYSTEM tel. 534 101 002 ");
					}
				}catch (EdmException e) {
					LOG.error("�cie�ka w firebird.pathToEpuapDocuments nie zosta�a skonfigurowana poprawnie");
				} 


				path.append(Docusafe.getAdditionProperty("firebird.pathToEpuapDocuments"));

				path.append("/");

				path.append(idDokumentu);
				new File(path.toString()).mkdirs();
				path.append("/");
				String filename = Long.toHexString(es.documentId);
				int len = filename.length();
				filename = ("00000000".concat(filename)).substring(len);

				path.append(filename.toUpperCase());
				path.append(liczbaWpisow+1);
				path.append(".DAT");

				String lokalizacja =path.toString();

				FileOutputStream fos = null;
				try {
					//szyfrowanie sygnatury
					SzyfrowanieSignatureEpuap  sse = new SzyfrowanieSignatureEpuap();
					String signature = sse.Szyfruj(signatureFileAsBytes);

					fos = new FileOutputStream(lokalizacja); //Otwieranie pliku 
					for(int i = 0; i < signature.length(); i++){
						fos.write((int)signature.charAt(i)); //Zapis bajt po bajcie ka�dego znaku...
					}
				} catch(IOException ex){
					System.out.println("B��d operacji na pliku: "+ex);
				}
				try {
					fos.close(); //Zamykanie pliku 
				} catch (IOException e2) {
					e2.printStackTrace();
				} 
				es.uri = lokalizacja;
				es.signatureFile = ""; 

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if(baseSignatureId!=null && baseSignatureId.longValue()!=0)
			es.setBaseSignatureId(baseSignatureId);
		
		DSApi.context().session().save(es);

		OfficeDocument document = OfficeDocument.find(documentId);
		DSUser user = DSApi.context().getDSUser();

		TaskSnapshot.updateAllTasksByDocumentId(document.getId(),document.getStringType());
		return es.getId();
	}

    /**
     * Zwraca pe�ny podpis - wraz z podpisami
      * @param signatureId - id podpisu
     * @return Obiekt podpisu
     */
	public ElectronicSignature get(long signatureId) throws EdmException
	{

		if (DSApi.isPostgresServer())
		{
			ElectronicSignature es = null;
			try
			{// przy pobieraniu /zapisywaniu/ updatowaniu  Large objects z postgresa  np typ text  konieczne jest wylaczenie auto comita na tym obiekcie
				DSApi.context().session().connection().setAutoCommit(false);
				es = (ElectronicSignature) DSApi.context().session().get(ElectronicSignature.class, signatureId);
				DSApi.context().session().connection().setAutoCommit(true);

			} catch (HibernateException e)
			{
				LOG.error("blad przy wczytywaniu electonic signature", es);
			} catch (SQLException e)
			{
				LOG.error("blad przy wczytywaniu electonic signature", es);
			}
			return es;
		} else
			return (ElectronicSignature) DSApi.context().session().get(ElectronicSignature.class, signatureId);
	}

    /**
     * Zapisuje podpis czynno�ci w procesie
     * @param pae czynno�� podpisywana
     * @param signedFile plik podpisywany
     * @param signatureFile plik podpisu
     * @throws EdmException
     */
    public void storeActionSignature(ProcessActionEntry pae, String signedFile, String signatureFile) throws EdmException, FileNotFoundException {
        LOG.info("storing actionSignature in database...");
        ElectronicSignature es = prepareElectronicSignature(ElectronicSignature.processActionIdKey(pae.getId()), signedFile, signatureFile);
        es.documentId = pae.getDocumentId();
        DSApi.context().session().save(es);
    }

    private ElectronicSignature prepareElectronicSignature(ElectronicSignature.Key key, String signedFile, String signatureFile) throws FileNotFoundException {
        Preconditions.checkArgument(!StringUtils.isBlank(signatureFile));
        Preconditions.checkArgument(!StringUtils.isBlank(signedFile));
        if (AvailabilityManager.isAvailable("zamienSzlaczkiZSygantury"))
		{
			 String[] znaki = Docusafe.getAdditionProperty("SzlaczkiZSygantury").split(",");
			 String[] znakiNa = Docusafe.getAdditionProperty("SzlaczkiZSyganturyNa").split(",");
			if (AvailabilityManager.isAvailable("zamienSzlaczkiZSyganturyZapiszXmlDoHome"))
			{
				String patch = Docusafe.getHome().getAbsolutePath() + "/" + (String.valueOf(key.getId()))+String.valueOf((new Date().getMinutes()))+ "PrzedZmiana.xml";
				PrintWriter zapiss = new PrintWriter(patch);
				zapiss.println(signatureFile);
				zapiss.close();
			}
			int i = 0;
			for (String znak : znaki){
			signatureFile.replaceAll(znak ,znakiNa[i]);
			i++;
			}
			if (AvailabilityManager.isAvailable("zamienSzlaczkiZSyganturyZapiszXmlDoHome"))
			{
				String patch = Docusafe.getHome().getAbsolutePath() + "/" + (String.valueOf(key.getId()))+String.valueOf((new Date().getMinutes()))+ "poZmianie.xml";
				PrintWriter zapiss = new PrintWriter(patch);
				zapiss.println(signatureFile);
				zapiss.close();
			}
		}
		if (AvailabilityManager.isAvailable("zamienSzlaczkiZSyganturyZapiszXmlDoHome"))
		{
			String patch = Docusafe.getHome().getAbsolutePath() + "/" + (String.valueOf(key.getId()))+String.valueOf((new Date().getMinutes()))+ "BezZmian.xml";
			PrintWriter zapiss = new PrintWriter(patch);
			zapiss.println(signatureFile);
			zapiss.close();
		}
		
        ElectronicSignature es = new ElectronicSignature();
        es.key = key;
        es.signatureFile = signatureFile;
        es.signedFile = signedFile;
        es.date = new Date();
        es.setPersistenceType(ElectronicSignature.PersistenceType.DATABASE);
       
	      return es;
    }

    private ElectronicSignature prepareElectronicSignature(ElectronicSignature.Key key, String signedFile, String signatureFile, Date date) throws FileNotFoundException {
        Preconditions.checkArgument(!StringUtils.isBlank(signatureFile));
        Preconditions.checkArgument(!StringUtils.isBlank(signedFile));

        ElectronicSignature es = new ElectronicSignature();
        es.key = key;
        es.signatureFile = signatureFile;
        es.signedFile = signedFile;
        es.date = date;
        es.setPersistenceType(ElectronicSignature.PersistenceType.DATABASE);
       
	      return es;
    }
    /**
     * Zwraca list� podpis�w dla danego dokumentu
     * @param documentId id dokumentu
     * @return lista podpis�w, mo�e by� pusta, nigdy null
     */
    public List<ElectronicSignatureBean> findSignaturesForDocument(long documentId) throws EdmException {
    	List<ElectronicSignatureBean> list = DSApi.context().session()
    	.createQuery(MessageFormat.format("SELECT new {0}(id,key,date) FROM {1} WHERE documentId = " + documentId + 
    			" AND baseSignatureId IS NOT NULL",
    			ESBean.class.getName(), ElectronicSignature.class.getName()))
    			.list();
    	if(list.size()>0)
    		return list;

    	list = DSApi.context().session()
    	.createQuery(MessageFormat.format("SELECT new {0}(id,key,date) FROM {1} WHERE documentId = " + documentId,
    			ESBean.class.getName(), ElectronicSignature.class.getName()))
    			.list();
    	return list;
    }

    /**
     *
     * @return
     * @throws EdmException
     */
    public List<ElectronicSignature> list(Boolean isTimestampSign) throws EdmException
    {
        Criteria criteria = DSApi.context().session().createCriteria(ElectronicSignature.class);

        if(isTimestampSign != null)
           criteria.add(Restrictions.eq("isTimestampSign", isTimestampSign));

        return criteria.list();
    }

    /**
     * Aktualizuje podpis w Bazie
     * @param es
     * @throws EdmException
     */
    public void update(ElectronicSignature es) throws EdmException
    {
        DSApi.context().session().update(es);
    }
}
