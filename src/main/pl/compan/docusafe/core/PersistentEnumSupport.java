package pl.compan.docusafe.core;

/**
 * Klasa wspomagaj�ca tworzenie klas enumeracyjnych typu PersistentEnum
 * obs�ugiwanych przez Hibernate.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PersistentEnumSupport.java,v 1.3 2006/12/07 13:49:42 mmanski Exp $
 */
public abstract class PersistentEnumSupport /*implements PersistentEnum*/
{
    /**
     * Sprawdza, czy ka�de pole typu implementuj�cego PersistentEnum zadeklarowane
     * w przekazanej klasie ma unikalny identyfikator. Przeszukiwane s� pola
     * o modyfikatorach "public static final" implementuj�ce interfejs
     * PersistentEnum.
     * <p>
     * Metoda powinna by� wywo�ywana w sekcji <code>static</code> klasy
     * dziedzicz�cej.
     * @throws IllegalStateException Je�eli znaleziony zostanie konflikt
     * identyfikator�w p�l.
     * @see PersistentEnum#toInt()
     */
  /*  protected static void checkUniqueness(Class clazz)
    {
        Set identifiers = new HashSet();
        Field[] fields = clazz.getDeclaredFields();
        for (int i=0; i < fields.length; i++)
        {
            Field field = fields[i];
            if (PersistentEnum.class.isAssignableFrom(field.getType()) &&
                Modifier.isFinal(field.getModifiers()) &&
                Modifier.isPublic(field.getModifiers()) &&
                Modifier.isStatic(field.getModifiers()))
            {
                try
                {
                    Integer value = new Integer(((PersistentEnum) field.get(null)).toInt());
                    if (identifiers.contains(value))
                        throw new IllegalStateException("Nieunikalna warto�� identyfikatora " +
                            "pola "+field.getName()+": "+value);
                    identifiers.add(value);
                }
                catch (IllegalAccessException e)
                {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
    }

    private static final Map cache = new HashMap();
*/
    /**
     * Szuka we wskazanej klasie pola typu PersistentEnum o wskazanej
     * warto�ci.
     * @see PersistentEnum#toInt()
     * @throws IllegalArgumentException Je�eli przekazana warto�� pola
     * value nie odpowiada �adnemu z p�l zadeklarowanych w klasie.
     */
/*    protected static PersistentEnum fromInt(Class clazz, int value)
    {
        Field[] valueFields = (Field[]) cache.get(clazz);

        if (valueFields == null)
        {
            // tworzenie listy p�l, kt�re b�d� traktowane jako warto�ci
            // klasy enumeracyjnej
            Field[] fields = clazz.getDeclaredFields();
            Field[] tmp = new Field[fields.length];
            int valueIndex = 0;
            for (int i=0; i < fields.length; i++)
            {
                Field field = fields[i];
                if (PersistentEnum.class.isAssignableFrom(field.getType()) &&
                    Modifier.isFinal(field.getModifiers()) &&
                    Modifier.isPublic(field.getModifiers()) &&
                    Modifier.isStatic(field.getModifiers()))
                {
                    tmp[valueIndex++] = field;
                }
            }

            valueFields = tmp;
            cache.put(clazz, valueFields);
        }

        // szukanie przekazanej warto�ci w�r�d warto�ci klasy enumeracyjnej
        for (int i=0; i < valueFields.length; i++)
        {
            Field field = valueFields[i];
            try
            {
                PersistentEnum fieldValue = (PersistentEnum) field.get(null);
                if (fieldValue.toInt() == value)
                    return fieldValue;
            }
            catch (IllegalAccessException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        throw new IllegalArgumentException("Nieznana warto�� klasy enumeracyjnej " +
            clazz.getName()+": "+value);
    }
*/
    /**
     * Zwraca tablic� wszystkich p�l typu PersistentEnum o modyfikatorach
     * "public static final" zadeklarowanych w przekazanej klasie. Metoda
     * powinna by� u�yta jednorazowo w sekcji static klasy dziedzicz�cej.
     * @param clazz Typ elementu zwracanej tablicy, musi implementowa� lub
     * dziedziczy� po PersistentEnum.
     */
/*    protected static PersistentEnum[] getValues(Class clazz)
    {
        if (!PersistentEnum.class.isAssignableFrom(clazz))
            throw new IllegalArgumentException("Spodziewano si� klasy implementuj�cej " +
                "lub dziedzicz�cej po "+PersistentEnum.class.getName());

        Field[] fields = clazz.getDeclaredFields();
        Field[] valueFields = new Field[fields.length];
        int valueIndex = 0;

        for (int i=0; i < fields.length; i++)
        {
            Field field = fields[i];
            if (PersistentEnum.class.isAssignableFrom(field.getType()) &&
                Modifier.isFinal(field.getModifiers()) &&
                Modifier.isPublic(field.getModifiers()) &&
                Modifier.isStatic(field.getModifiers()))
            {
                valueFields[valueIndex++] = field;
            }
        }

        Object array = Array.newInstance(clazz, valueIndex);
        try
        {
            for (int i=0; i < valueIndex; i++)
            {
                Array.set(array, i, valueFields[i].get(null));
            }
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }

        return (PersistentEnum[]) array;
    }
    */
}
