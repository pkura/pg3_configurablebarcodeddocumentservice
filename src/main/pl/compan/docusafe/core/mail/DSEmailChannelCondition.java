package pl.compan.docusafe.core.mail;

import java.io.Serializable;

public class DSEmailChannelCondition implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Operator OR
	 */
	public static final String OR = "or";
	
	/**
	 * Operator AND
	 */
	public static final String AND = "and";
	
	/**
	 * Operator NOT
	 */
	public static final String NOT = "not";
	
	public static enum Terms {
		BodyTerm, 
		FromTerm,
		SentDateTerm,
		SubjectTerm,
	};
	
	/**
	 * Rodzja operatora: or lub and
	 */
	private String operator;
	
	/**
	 * Rodzaj pola: tytul, od
	 */
	private String field;
	
	/**
	 * Warto�� pola
	 */
	private String value;
	
	/**
	 * Zwr�cenie nazwy operatora.
	 * 
	 * @return
	 */
	public String getOperator() 
	{
		return operator;
	}
	
	/**
	 * Ustawienie operatora
	 * 
	 * @param operator
	 */
	public void setOperator(String operator) 
	{
		if(!OR.equalsIgnoreCase(operator) && !AND.equalsIgnoreCase(operator))
			this.operator = OR;
		else
			this.operator = operator;
	}
	
	/**
	 * Zwr�cenie rodzaju pola: or, and, not
	 * 
	 * @return
	 */
	public String getField() 
	{
		return field;
	}
	
	public void setField(String field) 
	{
		this.field = field;
	}
	
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value) 
	{
		if(value==null) this.value=null;
		else
		{
			value = value.replace(":", "");
			value = value.replace(";", "");
			this.value = value;
		}
	}
	
	public String getChannelConditionAsString()
	{
		return operator+":"+field+":"+value;
	}
		
}
