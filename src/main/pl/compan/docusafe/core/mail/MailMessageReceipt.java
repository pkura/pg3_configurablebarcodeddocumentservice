package pl.compan.docusafe.core.mail;

import pl.compan.docusafe.core.crm.Contractor;

/**
 * Klasa informuj�ca o szczeg�ach zwi�zanych z odbiorc� danej wiadomo�ci e-mail
 */
public class MailMessageReceipt {

    private Long id;
    private MailMessage mailMessage;
    private Contractor contractor;
    private String emailAddress;
    private MailMessageReceiptType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public MailMessageReceiptType getType() {
        return type;
    }

    public void setType(MailMessageReceiptType type) {
        this.type = type;
    }

    public static MailMessage getEmailCorrespondence(String emailAddress){
        /*try
        {
            Criteria c = DSApi.context().session().createCriteria(Contractor.class);
            if(nip != null)
                c.add(Restrictions.eq("nip", nip));
            else
                c.add(Restrictions.isNull("nip"));

            return (List<Contractor>) c.list();
        }
        catch (Exception e)
        {
            log.error("B��d wyszukania kontrahenta po numerze contractorNo " + e);
        }*/
        return null;
    }

    public MailMessage getMailMessage() {
        return mailMessage;
    }

    public void setMailMessage(MailMessage mailMessage) {
        this.mailMessage = mailMessage;
    }
}