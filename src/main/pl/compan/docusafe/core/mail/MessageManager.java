package pl.compan.docusafe.core.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.URLName;
import javax.mail.internet.MimeUtility;
import com.google.common.base.Preconditions;
import org.apache.commons.codec.digest.DigestUtils;
import org.h2.command.Prepared;
import org.hibernate.Query;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPMessage;
import com.sun.mail.pop3.POP3Folder;
import com.sun.mail.pop3.POP3Message;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.ilpoldwr.CokUtils;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
/* User: Administrator, Date: 2006-11-22 14:22:31 */

/**
 * Klasa pobieraj�ca i zapisuj�ca do bazy danych wiadomo�ci
 * z podanego kana�u mailowego.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 * @version $Id$
 */
public class MessageManager
{
	private final static Logger LOG = LoggerFactory.getLogger(MessageManager.class);
	/**
	 * Ustalenie klasy faktory dla po��cze� SSL stosowanego w kanale mailowym
	 */
	private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	
	/**
	 * Limit pobranych w jednym cyklu wiadomo�ci
	 */
	private static int LIMIT = 10;
	
	public static Map<String, String> contentTypes;
	
	
	static{
		//	wykaz mozliwych typow mime
		//	https://github.com/unfiltered/unfiltered/blob/master/library/src/main/scala/request/types.scala
		//	http://en.wikipedia.org/wiki/Internet_media_type
		contentTypes = new HashMap<String, String>();
		contentTypes.put("text/html", ".html");
		contentTypes.put("text/plain", ".txt");
		contentTypes.put("application/xml", ".xml");
	}
	
	/**
	 * Pobiera wszystkie kana�y mailowe.
	 * 
	 * @return
	 * @throws EdmException
	 */
	public static List<DSEmailChannel> getChannels() 
		throws EdmException
	{
		String sql = "select d from d in class " + DSEmailChannel.class.getName()+ " where d.deleted = ?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setBoolean(0, false);
		List<DSEmailChannel> ranges = query.list();
		return ranges;
	}
	
	/**
	 * Pobiera wszystkie w��czone kana�y mailowe.
	 * 
	 * @return
	 * @throws EdmException
	 */
	public static List<DSEmailChannel> getEnabledChannels() 
		throws EdmException
	{
		String sql = "select d from d in class " + DSEmailChannel.class.getName() +
				" where d.enabled = ?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setBoolean(0, true);
		List<DSEmailChannel> ranges = query.list();
		return ranges;
	}
	
	
	/**
	 * Zwraca kana� mailowy o podanym identyfikatorze.
	 * 
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static DSEmailChannel getChannel(Long id) throws EdmException
	{
		String sql = "select d from d in class "+ DSEmailChannel.class.getName() + " where d.id=?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setLong(0, id);
		List<DSEmailChannel> ranges = query.list();
		if(ranges.size()>0)
			return ranges.get(0);
		else throw new EdmException("NieZnalezionoKanalu");
	}
	
	/**
	 * Parsuje warunki dla kana�u ze stringa.
	 * 
	 * @param str
	 * @return
	 */
	public static Vector<DSEmailChannelCondition> getConditionsFromString(String str)
	{
		if(str==null) return null;
		Vector<DSEmailChannelCondition> vect = new Vector<DSEmailChannelCondition>();
		DSEmailChannelCondition ecc;
		String[] conds = str.split(";");
		String[] vars;
		for(String s:conds)
		{
			vars = s.split(":");
			if(vars.length>=3)
			{
				ecc = new DSEmailChannelCondition();
				ecc.setOperator(vars[0]);
				ecc.setField(vars[1]);
				ecc.setValue(vars[2]);
				vect.add(ecc);
			}
		}
		return vect;
	}
	
	
	/**
	 * Zwraca string reprezentujacego warunki kanalu.
	 * 
	 * @param conds
	 * @return
	 */
	public static String getStringFromConditions(Vector<DSEmailChannelCondition> conds)
	{
		if(conds==null) return null;
		String ret = "";
		int counter = 0;
		for(DSEmailChannelCondition ecc:conds)
		{
			ret += ecc.getChannelConditionAsString();
			counter++;
			if(counter!=conds.size()) ret += ";";
		}
		return ret;
	}
	
    /**
     * Pobranie i zapisanie wiadomo�ci z podanego kana�u mailowego.
     * 
     * @param emailChannel
     * @return Liczba znalezionych wiadomo�ci lub <code>-1</code> gdy b��d
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    public static int downloadMessagesFromEmailChannel(DSEmailChannel emailChannel)
    	throws EdmException
    {
    	// ustawienie w�a�ciwo�ci
    	Properties properties = loadEmailChannelProperties(emailChannel);
    	Session session = Session.getDefaultInstance(properties, null);
    	URLName urlName = new URLName(emailChannel.getProtocol(), emailChannel.getHost(), 
    								  emailChannel.getPort(), "", 
    								  emailChannel.getUser(), emailChannel.getPassword());
    	
    	int totalMessages = -1;
    	
    	try 
    	{
    		// utworzenie obiektu Store i nawi�zanie po��czenia
        	Store store = session.getStore(urlName);
        	store.connect();
        	// otworzenie folderu
        	Folder folder = openMailBoxFolder(store, emailChannel.getMbox());
    		try
    		{	
            	totalMessages = folder.getMessageCount(); 
            	if (totalMessages == 0)
            	{
            		// brak wiadomo�ci w folderze
            		throw new EdmException(
            				StringManager.getManager(Constants.Package)
            				.getString("BrakWiadomosciWFolderze"));
            	}
            	
                // pobranie wiadomo�ci z folderu danego kana�u mailowego
                // KM: argument emailChannel nie jest do niczego uzyty
                Message[] messages = getFolderMessages(emailChannel, folder);
                // zapisanie wiadomo�ci wraz z za��cznikami w bazie danych
                try{
                    saveMassages(emailChannel, folder, messages);
                }catch(EdmException e){
                    LOG.error(e.getMessage(), e);
                }
            }
            finally
            {
                folder.close(true);
                // Wywo�anie metody expunge() jest niepotrzebne
                //folder.expunge();
                store.close();
            }
        }
        catch (Exception ex)
        {
            throw new EdmException(ex);
        }

    	return totalMessages;
    }
    
    /**
     * Za�adowanie w�a�ciwo�ci po��czenia z danym kana�em mailowym.
     * 
     * @param emailChannel
     * @return obiekt w�a�ciwo�ci
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private static Properties loadEmailChannelProperties(DSEmailChannel emailChannel)
    {
    	Properties properties = new Properties();
    	if (emailChannel.isSSL()) 
    	{
    		// je�li u�uwa SSL ustawiam odpowiednie w�a�ciow�ci
    		properties.put(
    				"mail." + emailChannel.getProtocol() + ".socketFactory.class", SSL_FACTORY);
    		properties.put(
    				"mail." + emailChannel.getProtocol() + ".socketFactory.falback", "false");
    		properties.put(
    				"mail." + emailChannel.getProtocol() + ".socketFactory.port", "" + emailChannel.getPort());
    		properties.put(
    				"mail." + emailChannel.getProtocol() + ".port", "" + emailChannel.getPort());
    	}
    	return properties;
    }
    
    /**
     * Otworzenie folderu z podanego obiektu Store i nazwy folderu.
     * 
     * @param store Obiekt Store.
     * @param mailbox Nazwa folderu np. INBOX
     * @return Otwarty folder o nazwie podanej w parametrze mailbox.
     * @throws MessagingException
     * @throws EdmException
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private static Folder openMailBoxFolder(Store store, String mailbox)
    	throws MessagingException, EdmException
    {
    	//if (mailbox == null || mailbox.equals(""))
    		mailbox = "INBOX";
    	Folder[] fs = store.getPersonalNamespaces();
    	
//    	for (Folder folder : fs) {
//    		folder.open(Folder.READ_ONLY);
//			System.out.println(folder.getFullName());
//			System.out.println(folder.getName());
//		}
    	Folder folder = store.getFolder(mailbox);
    	if (folder == null)
    		throw new EdmException(StringManager
    				.getManager(Constants.Package)
    				.getString("BrakFolderu"));
    	try
    	{
    		// pr�ba otawrcia folderu do odczytu i zapisu
    		folder.open(Folder.READ_WRITE);
    	}
    	catch (MessagingException ex)
    	{
    		// otwarcie folderu tylko do odczytu
    		folder.open(Folder.READ_ONLY);
    	}
    	
    	return folder;
    }
    
    /**
     * Pobranie wiadomo�ci z folderu danego kana�u mailowego. Je�li w kanale mailowym
     * jest ustawiona warto�� lastMessageDate, zostan� pobrane wiadomo�ci p�niejsze od 
     * tej daty.
     * 
     * @param emailChannel Kana� mailowy.
     * @param folder Obiekt folderu.
     * @return Tablica wiadomo�ci pobranych z folderu.
     * @throws MessagingException
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private static Message[] getFolderMessages(final DSEmailChannel emailChannel, Folder folder)
    	throws MessagingException
    {
    	Message[] messages = null;
    	
		// pobranie wiadomo�ci
    	messages = folder.getMessages();
    	// ustwienie profilu pobierania wiadomo�ci
    	FetchProfile fetchProfile = new FetchProfile();
    	fetchProfile.add(UIDFolder.FetchProfileItem.UID);
    	fetchProfile.add(FetchProfile.Item.FLAGS);
    	folder.fetch(messages, fetchProfile);
    	
    	return messages;
    }
    
    /**
     * Zapisanie wiadomo�ci do bazy danych.
     * 
     * @param emailChannel
     * @param messages
     * @throws EdmException
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    private static void saveMassages(DSEmailChannel emailChannel, Folder folder, Message[] messages)
    	throws EdmException
    {
    	if (messages == null)
    		return;
    	
    	// otworzenie transakcji
		DSApi.context().begin();
		
		// pobranie wszystkich UID'�w wiadomo�ci z danego kana�u mailowego
		List uidsWithProcessed = MailMessage.getUidsWithProcessed(emailChannel);
		if (uidsWithProcessed == null)
			uidsWithProcessed = new ArrayList();
		
		// za�adowanie wynik�w do mapy
		LinkedHashMap<String, Boolean> uidsMap = new LinkedHashMap<String, Boolean>();
		for (int i = 0; i < uidsWithProcessed.size(); i++)
		{
			Object[] row = (Object[]) uidsWithProcessed.get(i);
			uidsMap.put((String) row[0], (Boolean) row[1]);
		}
		
		int idx = 0;
		
		if(Docusafe.getAdditionProperty("emailLIMITnumber") != null){
			try{
				LIMIT = Integer.parseInt(Docusafe.getAdditionProperty("emailLIMITnumber"));
			}catch(Exception e){
				//LOG.error(e.getMessage());
			}
		}
    	for (Message message : messages)
    	{	
    		try
    		{
    			// je�li idx r�wny limitowi pobranych wiadomo�ci, przerwanie p�tli
    			if (LIMIT == idx) 
    				break;
    			

    			// ustalenie numeru UID wiadomo�ci
    			String uid = null;
    			
    			if(Docusafe.getAdditionProperty("uidGenerationMethod") != null && Docusafe.getAdditionProperty("uidGenerationMethod").contains("new")){
    				// tiger: olewamy uidy maila bo sie powtarzaja
    				uid = generateUid(message);
    			}else{
    				if (folder instanceof POP3Folder)
	    				// poczta POP3, zwracany UID jest typu String
	    				uid = ((POP3Folder) folder).getUID(message);
	    			else if (folder instanceof IMAPFolder)
	    				// poczta IMAP, zwracany UID jest typu long
	    				uid = "" + ((IMAPFolder) folder).getUID(message);
	    			else
	    				throw new EdmException("NieprawidlowyTypFolderu");
    			}
    			
    			// sprawdzenie czy nie ma ju� takiej wiadomo�ci
    			if (uidsMap.containsKey(uid))
    			{
    				
    				/*
    				// jest taka wiadomo��, sprawdzenie czy wiadomo�� jest do usuni�cia
    				Boolean deleteMessage = AvailabilityManager.isAvailable("delete.processed.message");
    				if (uidsMap.get(uid).booleanValue() == true && deleteMessage != null && deleteMessage == Boolean.TRUE)
    				{
    					// oznaczenie flagi wiadomo�ci jako usni�ta
    					message.setFlag(Flags.Flag.DELETED, true);
    				}
    				*/
    				
    			} 
    			else
    			{
    				//	zapisanie wiadomo�ci do bazy
    				//	System.out.println("saving message...");
        			saveMessage(emailChannel, message, uid);
                    InOfficeDocument doc = new InOfficeDocument();
                    
                    // aby uruchomic nale�y dodac createTaskFromEmail = true w .properties
                    if(Docusafe.getAdditionProperty("createTaskFromEmail") != null && Docusafe.getAdditionProperty("createTaskFromEmail").equals("true")){
                        CokUtils.createDocumentFromEmail(doc, DocumentKind.findByCn(IlpolCokLogic.DOCKIND_NAME), DSApi.context().getDSUser(), null, null, "Dokument utworzony z maila", "Zg�oszenie ICS", "Dokument utworzony z maila", uid, emailChannel);
                    }
        			
        			//	zwi�kszenie idx dla limitu w momencie znalezienia i zapisania nowej wiadomosci
        			idx++;
        			
        			//	oznaczenie wiadomosci jako przeczytanej, tylko folder imap obsluguje flagi (?)
        			//	message.setFlag(Flags.Flag.SEEN, true);
    			}
    			
    			// usuni�cie wiadomo�ci z pami�ci, przy du�ej ilo�ci wiadomo�ci mo�liwy jest heap space
    			// metoda invalidate (invalidateHeaders) czy�ci pami�� z danej wiadomo�ci
    			if (message instanceof POP3Message){
    				((POP3Message) message).invalidate(true);
    			}else if (message instanceof IMAPMessage){
    				((IMAPMessage) message).invalidateHeaders();
    			}else
    				throw new EdmException("NieprawidlowyTypWiadomosci");
    		}
    		catch (Exception ex)
    		{
    			// wycofanie transkacji je�li zosta� rzucony wyj�tek
                LOG.error(ex.getMessage(), ex);
    			DSApi.context().setRollbackOnly();
    		}
    	}
    	// commit transakcji
		DSApi.context().commit();
    }
    
    /**
     * Zapisanie podanej wiadomo�ci w bazie danych wraz z informacj� o typie wiadomo�ci (NEW/REPLY/FORWARD) oraz jej odbiorcach
     *
     * @param emailChannel
     * @param message
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    public static MailMessage saveMessage(DSEmailChannel emailChannel, Message message, MailMessageType mailMessageType, String toEmail, Collection<String> toCc, Collection<String> toBcc, Long contractorId) throws MessagingException, UnsupportedEncodingException, IOException, EdmException
    {
        MailMessage mailMessage = saveMessage(emailChannel, message, generateUid(message));
        if(mailMessageType == null){
            //ustawienie typu wiadomosci
            mailMessageType = MailMessageType.getType(message.getSubject());
        }
        mailMessage.setType(mailMessageType);

        /*
            Tworzenie informacji zwi�zanych z odbiorem wiadomo�ci email
         */
        Collection<String> mainReceipt = new HashSet<String>();
        mainReceipt.add(toEmail);
        MessageManager.prepareMailMessageReceipt(mailMessage, mainReceipt, contractorId, MailMessageReceiptType.MAIN);
        MessageManager.prepareMailMessageReceipt(mailMessage, toCc, contractorId, MailMessageReceiptType.CC);
        MessageManager.prepareMailMessageReceipt(mailMessage, toBcc, contractorId, MailMessageReceiptType.BCC);
        return mailMessage;
    }

    /**
     * Zapisanie podanej wiadomo�ci w bazie danych.
     * 
     * @param emailChannel
     * @param message
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
     */
    public static MailMessage saveMessage(DSEmailChannel emailChannel, Message message, String uid) throws MessagingException, UnsupportedEncodingException, IOException, EdmException
    {
        if (message == null)
            return null;

        MailMessage mailMessage = new MailMessage();
        mailMessage.setEmailChannel(emailChannel);

        // ustawienie messageId
        mailMessage.setMessageId(uid);

		// ustawienie tematu wiadomo�ci
		String subject = message.getSubject(); 
        if (subject != null && !subject.equals(""))
        	mailMessage.setSubject(MimeUtility.decodeText(subject));
        else 
        	mailMessage.setSubject(StringManager.getManager(Constants.Package).getString("BrakTematu"));
        
        // ustawienie daty przys�ania wiadomo�ci
        mailMessage.setSentDate(message.getSentDate());
        
        // ustawienie nadawcy wiadomo�ci
        String from = "";
        Address address[] = message.getFrom();
        for (int i=0; i < address.length; i++)
        {
            from += (i > 0 ? ", " : "") + address[i].toString();
        }
        mailMessage.setFrom(MimeUtility.decodeText(from));
        
        // zpisanie wiadomo�ci
        mailMessage.create();
        
        // zapisanie za��cznik�w
        saveAttachments(message, mailMessage);
        
        // utworzenie pliku tymczasowego dla contentu wiadomo��i
        File file = File.createTempFile("mail",".tmp");
        OutputStream output = new  FileOutputStream(file);
        message.writeTo(output);
        output.close();
        // update tre�ci wiadomo�ci
        InputStream input = new FileInputStream(file);
        mailMessage.updateBinaryStream(input, input.available());
        input.close();
        file.delete();
        return mailMessage;
    }
    
    /**
     * Zapisuje do bazy danych za��czniki podanej wiadomo�ci.
     * 
     * @param message
     * @param mailMessage
     * @throws IOException
     * @throws MessagingException
     * @throws EdmException
     */
    private static void saveAttachments(Message message, MailMessage mailMessage) 
    	throws IOException, MessagingException, EdmException
    {
        if (message.getContent() instanceof Multipart)
        {
            Multipart mp = (Multipart) message.getContent();

            for (int j = 0, m = mp.getCount(); j < m; j++) 
            {
                Part part = mp.getBodyPart(j);

                String disposition = part.getDisposition();
                if ((disposition != null) &&
                    ((disposition.equals(Part.ATTACHMENT) ||
                    (disposition.equals(Part.INLINE)))))
                {
                    MessageAttachment attachment = new MessageAttachment();
                    attachment.setAttachmentNo(j);
                    //	w przypadku poczty wp zdarza sie, ze zalacznik reprezentujacy zawartosc maila (plik list.html) nie posiada nazwy
                    //  wtedy w przypadku przypisania zalacznika do dokumentu nastepuje blad z powodu wart. null dla nazwy pliku
                    String fileName = part.getFileName();
                    if(fileName == null){
                    	if(MessageManager.contentTypes.keySet().contains(part.getContentType().split(";")[0])){
                    		fileName = "brak nazwy" + MessageManager.contentTypes.get(part.getContentType().split(";")[0]);
                    	}
                    	attachment.setFilename(fileName);
                    }
                    else 
                    	//	nazwy zalacznikow sa kodowane w ASCII, stad jesli nazwa zawiera znaki spoza tablicy ASCII,
                    	//	to jest ona kodowana jako rozszerzone slowo MIME, ktore nastepnie dekodujemy do oryginalnej formy
                    	attachment.setFilename(MimeUtility.decodeText(fileName));

                    attachment.setSize(part.getSize());
                    attachment.setContentType(part.getContentType());
                    attachment.setMailMessage(mailMessage);

                    File file = File.createTempFile("att",".tmp");
                    OutputStream output = new FileOutputStream(file);
                    part.getDataHandler().writeTo(output);
                    output.close();

                    if(attachment.getSize() == -1){
                        attachment.setSize((int)file.length());
                    }
                    attachment.create();
                    mailMessage.getAttachments().add(attachment);

                    InputStream input = new FileInputStream(file);
                    //InputStream input = part.getInputStream();
                    attachment.updateBinaryStream(input, input.available());//part.getInputStream(), part.getSize());
                    input.close();
                    file.delete();
                }
            }
        }
    }
    
    private synchronized static String generateUid(Message message) throws MessagingException{
        Message mimeMessage = message;
        StringBuilder param = new StringBuilder();
        param.append(mimeMessage.getSubject());
        param.append("/");
        param.append(mimeMessage.getSize());
        param.append("/");
        param.append(mimeMessage.getSentDate());
        return DigestUtils.shaHex(param.toString());
    }

    public synchronized static void prepareMailMessageReceipt(MailMessage mailMessage, Collection<String> emailAddress, Long contractorId, MailMessageReceiptType mailMessageReceiptType) throws EdmException {
        MailMessageReceipt mailMessageReceipt = null;
        Set<MailMessageReceipt> receipts = null;
        if(emailAddress != null || emailAddress.size() > 0){
            receipts = new HashSet<MailMessageReceipt>();
            for(String mailAddress : emailAddress){
                mailMessageReceipt = createMailMessageReceipt(contractorId, mailAddress, mailMessageReceiptType);
                mailMessageReceipt.setMailMessage(mailMessage);
                receipts.add(mailMessageReceipt);
                DSApi.context().session().saveOrUpdate(mailMessageReceipt);
            }
            Set<MailMessageReceipt> mailMessageReceipts = mailMessage.getMailMessageReceipts();
            if(mailMessage.getMailMessageReceipts() == null){
                mailMessage.setMailMessageReceipts(new HashSet<MailMessageReceipt>());
            }
            mailMessage.getMailMessageReceipts().addAll(receipts);
            DSApi.context().session().saveOrUpdate(mailMessage);
        }
    }

    private synchronized static MailMessageReceipt createMailMessageReceipt(Long contractorId, String emailAddress, MailMessageReceiptType mailMessageReceiptType) throws EdmException {
        Preconditions.checkNotNull(emailAddress);
        Preconditions.checkNotNull(mailMessageReceiptType);

        MailMessageReceipt mailMessageReceipt = new MailMessageReceipt();
        mailMessageReceipt.setContractor(Contractor.findById(contractorId));
        mailMessageReceipt.setEmailAddress(emailAddress);
        //g��wny odbiorca / do wiadomo�ci / ukryte do wiadomo�ci
        mailMessageReceipt.setType(mailMessageReceiptType);

        return mailMessageReceipt;
    }
}
