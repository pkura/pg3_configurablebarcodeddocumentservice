package pl.compan.docusafe.core.mail;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeManager;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeRange;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class DSEmailChannel 
{
	static final Logger log = LoggerFactory.getLogger(DSEmailChannel.class);
	
	private String pop3Host;
	private String pop3Username;
	private String pop3Password;
	private String folderName;
	private String kolejka;
	private String warunki;
	private Date ctime;
	private String contentType;
	Vector<DSEmailChannelCondition> conditions;
	private BarcodeRange assignedRange;
	
	private Long id;
	private String protocol;
	private String host;
	private String user;
	private String password;
	private int port;
	private String mbox;
	private boolean SSL;
	private boolean enabled;
	private boolean deleted;
	private Set<MailMessage> mailMessages;
	
	private String smtpHost;
	private Integer smtpPort;
	private String smtpUsername;
	private String smtpPassword;
	private boolean smtpTLS;
	private boolean smtpSSL;
	private boolean smtpEnabled;
	
	private String userLogin;

    private Long documentKindId;

	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Vector<DSEmailChannelCondition> getConditions()
	{
		conditions = MessageManager.getConditionsFromString(warunki);
		return conditions;
	}
	
	
	
	public Boolean fitsConditions(MailMessage mm)
	{
		getConditions();
		Boolean result = null;
		Boolean temp;
		if(conditions==null || conditions.size() == 0) return true;
		for(DSEmailChannelCondition ecc:conditions)
		{
			temp = false;
			if(ecc.getField().equalsIgnoreCase("title"))
			{
				if(mm.getSubject().contains(ecc.getValue()))
					temp = true;
			}
			else if(ecc.getField().equalsIgnoreCase("from"))
			{
				if(mm.getFrom().contains(ecc.getValue()))
					temp = true;
			}
			
			if(result==null)
			{
				result = temp;
			}
			else
			{
				if(ecc.getOperator().equalsIgnoreCase("or"))
					result = result || temp;
				else if(ecc.getOperator().equalsIgnoreCase("or"))
					result = result && temp;
			}
		}
		return result;
	}
	
	
	
	/**
	 * Przydzielenie nastepnego barkodu z puli
	 * @return
	 * @throws EdmException
	 */
	
	public String getNextBarcode() throws EdmException
	{
		String code;
		
		if(this.assignedRange==null)
		{
			log.debug("przydzielam nowy zakres");
			if(!DSApi.isContextOpen())
				DSApi.openAdmin();
			List<BarcodeRange> ranges = BarcodeManager.getRangeForClient(this.kolejka, BarcodeManager.CLIENT_TYPE_MAILER, BarcodeManager.RANGE_STATUS_INUSE);
			if(ranges.size()>0)
			{
				assignedRange = ranges.get(0);
			}
			else 
			{
				accuireBarcodeRange(100);				
			}
		}
		if(assignedRange == null) return null;
		code = assignedRange.getNextBarcodeAsStringOrNullOwnTransaction();
		return code;
		
	}
	
	private void accuireBarcodeRange(Integer number) throws EdmException
	{
		BarcodeRange br = BarcodeManager.assignBarcodeRange(this.kolejka, BarcodeManager.CLIENT_TYPE_MAILER, number);
		br = BarcodeManager.acceptBarcodeRange(br.getId(), BarcodeManager.RANGE_STATUS_INUSE);
		this.assignedRange = br;
	}


    public static DSEmailChannel findById(long id) throws EdmException
    {
        try
        {
            String sql = "select d from d in class " + DSEmailChannel.class.getName() + " where d.id like ?";
            Query q = DSApi.context().session().createQuery(sql);
            q.setLong(0, id);
            List<DSEmailChannel> list = q.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static DSEmailChannel findByUsername(String username) throws EdmException
    {
        //Criteria c = DSApi.context().session().createCriteria(MailMessage.class);

        try
        {
            String sql = "select d from d in class " + DSEmailChannel.class.getName() + " where d.user like ?";
            Query q = DSApi.context().session().createQuery(sql);
            q.setString(0, String.valueOf(username));
            List<DSEmailChannel> list = q.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

	public static DSEmailChannel findByMessageId(Long messageId) throws EdmException
    {
        //Criteria c = DSApi.context().session().createCriteria(MailMessage.class);

        try
        {
        	String sql = "select d from d in class " + DSEmailChannel.class.getName() + 
        		" where d.messageId like ?";
        	Query q = DSApi.context().session().createQuery(sql);
        	q.setString(0, String.valueOf(messageId));
            List<DSEmailChannel> list = q.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	public static List<DSEmailChannel> list()
	        throws EdmException
    {
        //Criteria c = DSApi.context().session().createCriteria(MailMessage.class);

        try
        {
        	String sql = "select d from d in class " + DSEmailChannel.class.getName();
        	Query q = DSApi.context().session().createQuery(sql);
            List<DSEmailChannel> list = q.list();
            if (list.size() > 0)
                return list;
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public String getPop3Host() {
		return pop3Host;
	}
	public void setPop3Host(String pop3Host) {
		this.pop3Host = pop3Host;
	}
	public String getPop3Username() {
		return pop3Username;
	}
	public void setPop3Username(String pop3Username) {
		this.pop3Username = pop3Username;
	}
	public String getPop3Password() {
		return pop3Password;
	}
	public void setPop3Password(String pop3Password) {
		this.pop3Password = pop3Password;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public String getKolejka() {
		return kolejka;
	}
	public void setKolejka(String kolejka) {
		this.kolejka = kolejka;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public String getWarunki() {
		return warunki;
	}
	public void setWarunki(String warunki) {
		this.warunki = warunki;
		if(this.warunki==null) this.warunki="";
	}
	/**
	 * Zwraca nazw� protoko�u, np.: imap, pop3.
	 * 
	 * @return Nazwa protoko�u.
	 */
	public String getProtocol() 
	{
		return protocol;
	}
	/**
	 * Ustawia nazw� protoko�u, przez kt�ry b�dzie nawi�zywane po��czenie
	 * z danym kana�em mailowym. 
	 * 
	 * @param protocol
	 */
	public void setProtocol(String protocol) 
	{
		this.protocol = protocol;
	}
	/**
	 * Zwraca nazw� hosta dla danego kana�u mailowego, np.: pop3.wp.pl, imap.gmail.com.
	 * 
	 * @return Nazwa hosta.
	 */
	public String getHost() 
	{
		return host;
	}
	/**
	 * Ustawia nazw� hosta, przez kt�r� b�dzie nawi�zywane po��czenie z danym kana�em
	 * mailowym.
	 * 
	 * @param host
	 */
	public void setHost(String host) 
	{
		this.host = host;
	}
	/**
	 * Zwraca nazw� u�ytkownika danego protoko�u mailowego np.: user@company.pl.
	 * @return
	 */
	public String getUser() 
	{
		return user;
	}
	/**
	 * Ustawia nazw� u�ytkownika jaka b�dzie u�ywana podczas nawi�zywania
	 * po��czenia przez dany kana� mailowy.
	 * 
	 * @param user
	 */
	public void setUser(String user) 
	{
		this.user = user;
	}
	/**
	 * Zwraca has�o do danego kana�u emailowego.
	 * 
	 * @return
	 */
	public String getPassword() 
	{
		return password;
	}
	/**
	 * Ustawia has�o do danego kana�u mailowego.
	 * 
	 * @param password Has�o.
	 */
	public void setPassword(String password) 
	{
		this.password = password;
	}
	/**
	 * Zwraca numer portu. Domy�lny port ma warto�� -1.
	 * 
	 * @return Numer portu.
	 */
	public int getPort() 
	{
		return port;
	}
	/**
	 * Ustawia port, przez kt�ry b�dzie si� ��czy� dany kana� mailowy.
	 * U�ycie domy�lnego portu jest realizowane przez ustawienie warto�ci -1.
	 * 
	 * @param port Numer portu.
	 */
	public void setPort(int port) 
	{
		this.port = port;
	}
	/**
	 * Zwraca nazw� folderu, z kt�rego zostan� pobrane wiadomo�ci.
	 * 
	 * @return Nazwa folderu.
	 */
	public String getMbox() 
	{
		return mbox;
	}
	/**
	 * Ustawia nazw� folderu, z kt�rego b�d� pobierane wiadomo�ci.
	 * 
	 * @param mbox Nazwa folderu.
	 */
	public void setMbox(String mbox) 
	{
		this.mbox = mbox;
	}
	
	/**
	 * Sprawdza czy dany kana� mailowy ��czy si� przez SSL.
	 * 
	 * @return Czy po��czenie jest szyfrowane przez SSL.
	 */
	public boolean isSSL() 
	{
		return SSL;
	}
	/**
	 * Ustawienie tej warto�ci na true powoduje u�ycie szyfrowania SSL
	 * przy po��czeniu w danym kanale mailowym.
	 * 
	 * @param sSL Je�li warto�� true, po��czenie b�dzie szyfrowane.
	 */
	public void setSSL(boolean sSL) 
	{
		SSL = sSL;
	}
	/**
	 * Dodaje zbi�r wiadomo�ci do danego kana�u mailowego.
	 * 
	 * @param mailMessages
	 */
	public void setMailMessages(Set<MailMessage> mailMessages) 
	{
		this.mailMessages = mailMessages;
	}
	/**
	 * Zwraca wiadomo�ci z danego kana�u mailowego.
	 * 
	 * @return Zbi�r wiadomo�ci. 
	 */
	public Set<MailMessage> getMailMessages() 
	{
		if (mailMessages == null) {
			mailMessages = new LinkedHashSet<MailMessage>();
		}
		return mailMessages;
	}
	/**
	 * Okre�la czy dany kana� mailowy jest w��czony.
	 * 
	 * @return
	 */
	public boolean isEnabled() 
	{
		return enabled;
	}

	/**
	 * Ustawienie warto�� na true powoduje w��czenie kana�u mailowego.
	 * 
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) 
	{
		this.enabled = enabled;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public Integer getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getSmtpUsername() {
		return smtpUsername;
	}
	public void setSmtpUsername(String smtpUsername) {
		this.smtpUsername = smtpUsername;
	}
	public String getSmtpPassword() {
		return smtpPassword;
	}
	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}
	public boolean isSmtpTLS() {
		return smtpTLS;
	}
	public void setSmtpTLS(boolean smtpTLS) {
		this.smtpTLS = smtpTLS;
	}
	public boolean isSmtpSSL() {
		return smtpSSL;
	}
	public void setSmtpSSL(boolean smtpSSL) {
		this.smtpSSL = smtpSSL;
	}
	public boolean isSmtpEnabled() {
		return smtpEnabled;
	}
	public void setSmtpEnabled(boolean smtpEnabled) {
		this.smtpEnabled = smtpEnabled;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin=userLogin;
	}
    public Long getDocumentKindId() {
        return documentKindId;
    }
    public void setDocumentKindId(Long documentKindId) {
        this.documentKindId = documentKindId;
    }
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
