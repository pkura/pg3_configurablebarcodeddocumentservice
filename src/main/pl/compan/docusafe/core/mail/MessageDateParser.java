package pl.compan.docusafe.core.mail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.slf4j.LoggerFactory;

/**
 * Statyczna klasa wyci�gaj�ca z nag��wka wiadomo�ci e-mail dat� odbioru lub wys�ania wiadomo�ci.
 * Klasa wykorzystywana jest w przypadku protoko�u POP3, kt�ry nie udost�pnia
 * bezpo�redniego odczytu daty odbioru wiadomo�ci (msg.getReceivedDate() zawsze zwraca null).
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 * @version 
 */
public class MessageDateParser 
{
	/**
	 * Zwraca dat� z nag��wka wiadomo�ci. Metoda wykorzystywana jest w przypadku
	 * wiadomo�ci pochodz�cych z serwer�w POP3, poniewa� protok� POP3 nie wspiera
	 * w�a�ciwo�ci received date (msg.getReceivedDate()).
	 * 
	 * @param msg
	 * @return data otrzymania lub null je�li nie uda si� okre�li� daty.
	 * @throws MessagingException
	 */
	public static Date getDateFromMessageHeader(Message msg) 
		throws MessagingException 
	{
		Date today = new Date();
		// pobranie nag��wka received
		String[] received = msg.getHeader("received");
		if(received != null)
		{
			for (int i = 0; i < received.length; i++) 
			{
				String dateStr = null;
				try 
				{
					// pobranie ci�gu daty
					dateStr = getDateString(received[i]);
					if(dateStr != null) 
					{
						// sparsowanie ci�gu daty
						Date msgDate = parseDate(dateStr);
						if(msgDate.before(today))
							return msgDate;
					}
				} 
				catch(ParseException ex) 
				{ } 
			}
		}
		
		// pobranie nag��wka date, je�li nie powiod�o si� z received
		String[] dateHeader = msg.getHeader("date");
		if(dateHeader != null) 
		{
			String dateStr = dateHeader[0];
			try 
			{
				Date msgDate = parseDate(dateStr);
				if(msgDate.before(today)) 
					return msgDate;
			} 
			catch(ParseException ex) 
			{}
		}
		
		LoggerFactory.getLogger("kamilj").debug("MSG: " + msg.getSubject() + "=> PUSTA DATA!!!");
		// je�li nie uda si� okre�li� daty zwraca null
		return null;
	}
	
	/**
	 * Je�li data rozpoczyna si� od nazwy dnia tygodnia, przycina ci�g znak�w i
	 * zwraca sam� dat�
	 * 
	 * @param text
	 * @return
	 */
	private static String getDateString(String text) 
	{
		String[] daysInDate = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
		int startIndex = -1;
		for (int i = 0; i < daysInDate.length; i++) 
		{
			startIndex = text.lastIndexOf(daysInDate[i]);
			if(startIndex != -1)
				break;
		}
		if(startIndex == -1) 
		{
			return null;
		}
		
		text = text.substring(startIndex);
		return text.replaceAll("\\(|\\)", "");
	}	
	
	/**
	 * Parsuje dat� otrzyman� z ci�gu znak�w.
	 * 
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 */
	private static Date parseDate(String dateStr) 
		throws ParseException 
	{
		SimpleDateFormat dateFormat ; 
		try 
		{
			// Parsuje dat� np. Mon, 21 Jun 99 13:23:33 +0100
			// Locale.UK jest ustawione poniewa� u�ywane s� angielskie nazwy dni, miesi�cy
			dateFormat = new SimpleDateFormat("EEE, d MMM yy HH:mm:ss Z", Locale.UK) ;
			return dateFormat.parse(dateStr);
		} 
		catch(ParseException e) 
		{
			try 
			{
				dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.UK) ;
				return dateFormat.parse(dateStr);
			} 
			catch(ParseException ex) 
			{
				dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss Z", Locale.UK) ;
				return dateFormat.parse(dateStr);
			}
		}
	}
}
