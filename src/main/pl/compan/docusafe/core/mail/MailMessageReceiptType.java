package pl.compan.docusafe.core.mail;

/**
 * Okre�lenie typu odbioru wiadomo��i email:
 * <ul>
 * <li>MAIN - odbiorca jest g��wnym adresatem wiadomo�ci</li>
 * <li>CC (carbon copy) - odbiorca otrzyma� email w formie do wiadomo�ci / kopii</li>
 * <li>BCC (blind cc) - odbiorca otrzyma� email w formie ukrytej do wiadomo�ci / ukrytej kopii</li>
 * </ul>
 */
public enum MailMessageReceiptType {
    MAIN,
    CC,
    BCC;
}
