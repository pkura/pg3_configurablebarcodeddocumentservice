package pl.compan.docusafe.core.mail;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.common.base.Preconditions;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
/* User: Administrator, Date: 2006-11-22 15:22:00 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class MailMessage
{
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    public static final String MAIL_ATTACHMENT_CN = "mail";

    private Long id;
    private String messageId;
    private String subject;
    private Date sentDate;
    private String from;
    private Boolean processed;
    private Set<MessageAttachment> attachments;
    private DSEmailChannel emailChannel;
    private MailMessageType type;
    private Set<MailMessageReceipt> mailMessageReceipts;

    public MailMessage()
    {
    }

    public void create() throws EdmException
    {
        try
        {
            processed = Boolean.FALSE;
            
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public static MailMessage findByMessageId(String messageId, DSEmailChannel emailChannel)
        throws EdmException
    {
        //Criteria c = DSApi.context().session().createCriteria(MailMessage.class);
        try
        {
        	String sql = "select d from d in class " + MailMessage.class.getName() + 
        		" where d.messageId like ? and  d.emailChannel = ?";
        	Query q = DSApi.context().session().createQuery(sql);
        	q.setString(0, messageId);
        	q.setParameter(1, emailChannel);
            //c.add(org.hibernate.criterion.Expression.eq("messageId",messageId));
            List<MailMessage> list = q.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static MailMessage findByMessageId(String messageId) throws EdmException
    {
        try
        {
            String sql = "select d from d in class " + MailMessage.class.getName() + " where d.messageId like ?";
            Query q = DSApi.context().session().createQuery(sql);
            q.setString(0, messageId);
            List<MailMessage> list = q.list();
            if (list.size() > 0)
                return list.get(0);
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Pobranie wszystkich UID'� i w�a�ciwo�ci processed wiadomo�ci z danego kana�u mailowego
     * 
     * @param emailChannel
     * @return
     * @throws EdmException
     */
    public static List getUidsWithProcessed(DSEmailChannel emailChannel)
    	throws EdmException
    {
    	try
    	{
    		String sql = "select m.messageId, m.processed from m in class " + MailMessage.class.getName() +
    			" where m.emailChannel = ?";
    		Query query = DSApi.context().session().createQuery(sql);
    		query.setParameter(0, emailChannel);
    		List list = query.list();
    		return list;
    	}
    	catch (HibernateException ex)
    	{
    		throw new EdmHibernateException(ex);
    	}
    }

    public static MailMessage find(Long id)
        throws EdmException
    {
        return (MailMessage) Finder.find(MailMessage.class, id);
    }

    public static List<MailMessage> getUnprocessed() throws EdmException
    {
    	Criteria c = DSApi.context().session().createCriteria(MailMessage.class);
        c.add(Expression.eq("processed", new Boolean(false)));
        c.addOrder(Order.desc("id"));
        try
        {
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static List<MailMessage> getUnprocessedMessagesFromEmailChannel(DSEmailChannel emailChannel)
    	throws EdmException
    {
    	String sql = "from m in class " + MailMessage.class.getName() + 
		" where m.emailChannel = ? and m.processed = ? order by m.sentDate desc";
    	Query query = DSApi.context().session().createQuery(sql);
    	query.setParameter(0, emailChannel);
    	query.setBoolean(1, false);
    	return query.list();
    }
    
    public static void markAsProcessed(Long id) throws EdmException
    {
    	PreparedStatement ps = null;
    	try
        {
    		DSApi.context().begin();
	    	String sql = "update ds_message set processed = 1 where id = ?";
	    	ps = DSApi.context().prepareStatement(sql);
	    	ps.setLong(1, id);
	    	ps.execute();	
	    	//psa zamykamy w finally
	    	DSApi.context().commit();
        }
        catch (SQLException e)
        {
        	DSApi.context().setRollbackOnly();
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }
    
    public static List<MailMessage> search(Map<String, Object> parameters) throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(MailMessage.class);

        if (parameters != null)
        {
            if(parameters.get("processed") != null) c.add(Expression.eq("processed", parameters.get("processed")));
            if(parameters.get("subject") != null) c.add(Expression.like("subject", parameters.get("subject")));
            if(parameters.get("sender") != null) c.add(Expression.like("from", parameters.get("sender")));
            if(parameters.get("sentDate") != null) c.add(Expression.eq("sentDate", parameters.get("sentDate")));
        }

        c.addOrder(Order.desc("id"));

        try
        {
            return c.list();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public BlobInputStream getBinaryStream()
        throws EdmException
    {
    	boolean closeStm = true;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select contentdata from ds_message where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", id));

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }


    public void updateBinaryStream(InputStream stream, int size) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            int rows;
            ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(MailMessage.class)+
                        " set contentdata = ? where id = ?");
            ps.setBinaryStream(1, stream, size);
            ps.setLong(2, this.getId().longValue());
            rows = ps.executeUpdate();

            if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }

    public void createAttachments(Document doc) throws AccessDeniedException, EdmException, IOException {
        for (MessageAttachment messageAttachment : this.getAttachments())
        {
            Attachment attachment = new Attachment(messageAttachment.getFilename());
            attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
            attachment.setWparam(this.getId());
            doc.createAttachment(attachment);
            attachment.createRevision(messageAttachment.getBinaryStream(), messageAttachment.getSize(), messageAttachment.getFilename());
            System.out.println("createAttachments: " + messageAttachment.getFilename());
        }

        Attachment attachment = new Attachment("Mail");
        attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
        attachment.setWparam(this.getId());
        doc.createAttachment(attachment);
        int streamSize = getStreamSize(this.getBinaryStream());
        attachment.createRevision(this.getBinaryStream(), streamSize, this.getSubject() + ".eml");
        this.setProcessed(Boolean.TRUE);
        //	wiazemy dokument z emailem (message_id)
        if(doc instanceof InOfficeDocument){
            ((InOfficeDocument)doc).setMessageId(this.getId());
        }
    }

    private int getStreamSize(InputStream inputStream) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream);
        char[] content = new char[2048];
        int size = 0;
        while(reader.read(content) != -1){
            size += content.length;
        }
        reader.close();
        return size;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = TextUtils.trimmedStringOrNull(subject, 190);
    }

    public Date getSentDate()
    {
        return sentDate;
    }

    public void setSentDate(Date sentDate)
    {
    	if (sentDate == null) {
    		sentDate = new Date();
    	}
        this.sentDate = sentDate;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public Boolean getProcessed()
    {
        return processed;
    }

    public void setProcessed(Boolean processed)
    {
        this.processed = processed;
    }

    public Set<MessageAttachment> getAttachments()
    {
         if (attachments == null)
            attachments = new LinkedHashSet<MessageAttachment>();
        return attachments;
    }

    public void setAttachments(Set<MessageAttachment> attachments)
    {
        this.attachments = attachments;
    }

	public void setEmailChannel(DSEmailChannel emailChannel) 
	{
		this.emailChannel = emailChannel;
	}

	public DSEmailChannel getEmailChannel() 
	{
		return emailChannel;
	}

	public void setMessageId(String messageId) 
	{
		this.messageId = messageId;
	}

	public String getMessageId() 
	{
		return messageId;
	}

    public MailMessageType getType() {
        return type;
    }

    public void setType(MailMessageType type) {
        this.type = type;
    }

    public Set<MailMessageReceipt> getMailMessageReceipts() {
        return mailMessageReceipts;
    }

    public void setMailMessageReceipts(Set<MailMessageReceipt> mailMessageReceipts) {
        this.mailMessageReceipts = mailMessageReceipts;
    }
}
