package pl.compan.docusafe.core.mail;

/**
 * Okre�la rodzaj wiadomo�ci e-mail:
 * <ul>
 * <li>NEW - nowa wiadomo��</li>
 * <li>REPLY - odpowied� na otrzyman� wiadomo��</li>
 * <li>FORWARD - wiadomo�� przes�ana dalej</li>
 * </ul>
 */
public enum MailMessageType {
    NEW,
    REPLY{
        @Override
        public String getPrefix(){
            return "RE: ";
        }
    },
    FORWARD{
        @Override
        public String getPrefix(){
            return "FWD: ";
        }
    };

    /**
     *
     * Metoda zwraca prefiks odpowiedni dla typu wiadomo�ci tj.:
     * <ul>
     * <li>NEW - ""</li>
     * <li>REPLY - "RE: "</li>
     * <li>FORWARD = "FWD: " </li>
     * </ul>
     * @return
     */
    public String getPrefix(){
        return "";
    }

    /**
     * Metoda zwracaj�ca typ wiadomo�ci email na podstawie tre�ci jej tematu.
     * Nie istnieje obligatoryjny standard pozwalaj�cy zweryfikowa� jakiego typu jest dana wiadomo�� email.
     * Serwery poczty mog� (np. gmail.com), ale nie musz� (np. wp.pl) dostarcza� s�u��cych ku temu informacji w nag��wkach (References oraz In-Reply-To) wiadomo�ci.
     * @return
     */
    public synchronized static MailMessageType getType(String mailMessageSubject){
        MailMessageType messageType = null;
        int reIdx;
        int odpIdx;
        int fwdIdx;
        int pdIdx;
        int replayIdx;
        int forwardIdx;

        reIdx = mailMessageSubject.toUpperCase().indexOf("RE:");
        odpIdx = mailMessageSubject.toUpperCase().indexOf("ODP:");
        fwdIdx = mailMessageSubject.toUpperCase().indexOf("FWD:");
        pdIdx = mailMessageSubject.toUpperCase().indexOf("PD:");

        /**
         * Ustalenie najwcze�niejszego wyst�pienia prefiksu, oznaczaj�cego �e wiadomo�� jest odpowiedzi� na innego maila.
         */
        if(reIdx == -1){
            replayIdx = odpIdx;
        }else{
            if(reIdx > odpIdx && odpIdx != -1){
                replayIdx = odpIdx;
            }else{
                replayIdx = reIdx;
            }
        }

        /**
         * Ustalenie najwcze�niejszego wyst�pienia prefiksu, oznaczaj�cego �e wiadomo�� jest przekazywana dalej.
         */
        if(fwdIdx == -1){
            forwardIdx = pdIdx;
        }else{
            if(fwdIdx > pdIdx && pdIdx != -1){
                forwardIdx = pdIdx;
            }else{
                forwardIdx = fwdIdx;
            }
        }

        /**
         * Finalne ustalenie typu wiadomo�ci
         */
        if(replayIdx == -1 || replayIdx > 2){
            if(forwardIdx == -1 || forwardIdx > 2){
                messageType = NEW;
            }else{
                messageType = FORWARD;
            }
        }else{
            messageType = REPLY;
        }

        return messageType;
    }
}