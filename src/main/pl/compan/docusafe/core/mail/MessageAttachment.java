package pl.compan.docusafe.core.mail;

import pl.compan.docusafe.core.base.BlobInputStream;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.util.StringManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.io.InputStream;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Criteria;
/* User: Administrator, Date: 2006-11-29 11:42:32 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id: MessageAttachment.java,v 1.5 2010/03/30 08:42:31 kamilj Exp $
 */
public class MessageAttachment
{
    static final StringManager sm =
        StringManager.getManager(Constants.Package);

    private Long id;
    private MailMessage mailMessage;
    private String filename;
    private Integer size;
    private Integer attachmentNo;
    private String contentType;

    public MessageAttachment()
    {
    }

    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public void delete() throws EdmException
    {
        Persister.delete(this);
    }

    public static MessageAttachment find(Long messageId, Integer attachmentNo)
        throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(MessageAttachment.class);

        try
        {
            c.add(org.hibernate.criterion.Expression.eq("mailMessage.id", messageId));
            c.add(org.hibernate.criterion.Expression.eq("attachmentNo", attachmentNo));
            List list = c.list();
            if (list.size() > 0)
                return (MessageAttachment)c.list().get(0);
            else
                return null;

        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static MessageAttachment find(Integer id)
            throws EdmException
        {
            Criteria c = DSApi.context().session().createCriteria(MessageAttachment.class);

            try
            {
                c.add(org.hibernate.criterion.Expression.eq("id", id));
                List list = c.list();
                if (list.size() > 0)
                    return (MessageAttachment)c.list().get(0);
                else
                    return null;

            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
    
    
    public BlobInputStream getBinaryStream()
        throws EdmException
    {
    	boolean closeStm = true;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select contentdata from ds_message_attachment where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException(sm.getString("documentHelper.blobRetrieveFailed", id));

            InputStream is = rs.getBinaryStream(1);
            // sterownik dla SQLServera nie wspiera ResultSet.getBlob
            //Blob blob = rs.getBlob(1);
            if (is != null)
            {
            	closeStm = false;
                return new BlobInputStream(is, pst);
            }
            else
                return null;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	if (closeStm)
        	{
        		DSApi.context().closeStatement(pst);
        	}
        }
        // Nie mo�na zamkn�� obiektu Statement od razu po zwr�ceniu strumienia,
        // poniewa� w�wczas nie b�dzie mo�liwe odczytanie danych ze strumienia.
        // Statement jest zamykane w metodzie BlobInputStream.closeStream()
    }


    public void updateBinaryStream(InputStream stream, int size) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {

            int rows;
            ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(MessageAttachment.class)+
                        " set contentdata = ? where id = ?");
            ps.setBinaryStream(1, stream, size);
            ps.setLong(2, this.getId().longValue());
            rows = ps.executeUpdate();

            if (rows != 1)
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
            DSApi.context().closeStatement(ps);
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public MailMessage getMailMessage()
    {
        return mailMessage;
    }

    public void setMailMessage(MailMessage mailMessage)
    {
        this.mailMessage = mailMessage;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public Integer getSize()
    {
        return size;
    }

    public void setSize(Integer size)
    {
        this.size = size;
    }

    public Integer getAttachmentNo()
    {
        return attachmentNo;
    }

    public void setAttachmentNo(Integer attachmentNo)
    {
        this.attachmentNo = attachmentNo;
    }

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
