package pl.compan.docusafe.core.utils;

import org.krysalis.barcode4j.impl.code39.Code39LogicImpl;
import org.krysalis.barcode4j.impl.int2of5.Interleaved2Of5LogicImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.parametrization.pg.BarcodeHelper;

/**
 * Przemek Nowak <przemek.nowak.pl@gmail.com>
 * Date: 14.10.13 21:05
 */
public enum ChecksumAlgorithm 
{
	NO_VALIDATION
			{
				@Override
				public boolean isValid(String barcode)
				{
					return true;
				}
			},

	/**
	 * Interleaved2Of5LogicImpl
	 * @return
	 */
	INTERLEAVED_2_OF_5
			{
				@Override
				public boolean isValid(String barcode)
				{
					try
					{
						return Interleaved2Of5LogicImpl.validateChecksum(barcode);
					}
					catch (Exception e)
					{
						return false;
					}
				}
			},
	/**
	 * CheckSuma specyficzna dla TP
	 * @return
	 */
	CHECKSUM_39
			{
				@Override
				public boolean isValid(String barcode)
				{
                    return Code39LogicImpl.validateChecksum(barcode);
				}
			},
    PG
            {
                public boolean isValid(String barcode){
                    return BarcodeHelper.validateCheckCharacter(barcode);
                }
            }
    ;

	private static final Logger log = LoggerFactory.getLogger(ChecksumAlgorithm.class);

	public abstract boolean isValid(String barcode);

	/**
	 * Prawdopodobnie code39 uzywany jest.
	 * Uzywac tylko wyliczania Sumy kontrolnej do validacji
	 *
	 * @param s string
	 * @return integer
	 */
	private static Integer calculateControlDigitForCode39(String s)
	{
		int currentpos = 0, sum1 = 0;
		int ret = 0;
		try
		{
			while (currentpos < s.length())
			{
				sum1 += (int) s.charAt(currentpos);
				currentpos++;
			}
			ret = sum1 % 10;
		}
		catch (NumberFormatException e)
		{
			log.debug(e.getMessage(), e);
		}
		return ret;
	}


}
