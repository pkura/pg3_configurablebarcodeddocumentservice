package pl.compan.docusafe.core;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: PermissionsHelper.java,v 1.2 2004/08/27 03:32:18 lk Exp $
 */
public class PermissionsHelper
{
    PermissionsHelper()
    {
    }

    public boolean canRead(Document document)
    {
        if (document == null)
            throw new NullPointerException("document");

        if (document.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }

    public boolean canModify(Document document)
    {
        if (document == null)
            throw new NullPointerException("document");

        if (document.getId() == null || !DSApi.isContextOpen())
            return true;

        if (document instanceof OfficeDocument)
            return true;

        return true;
    }

    public boolean canDelete(Document document)
    {
        if (document == null)
            throw new NullPointerException("document");

        if (document.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }

    public boolean canCreate(Folder folder)
    {
        if (folder == null)
            throw new NullPointerException("folder");

        if (folder.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }

    public boolean canModify(Folder folder)
    {
        if (folder == null)
            throw new NullPointerException("folder");

        if (folder.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }

    public boolean canDelete(Folder folder)
    {
        if (folder == null)
            throw new NullPointerException("folder");

        if (folder.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }

    public boolean canRead(Folder folder)
    {
        if (folder == null)
            throw new NullPointerException("folder");

        if (folder.getId() == null || !DSApi.isContextOpen())
            return true;

        return true;
    }
}
