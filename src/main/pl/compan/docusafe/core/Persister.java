package pl.compan.docusafe.core;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Persister.java,v 1.3 2006/12/07 13:49:42 mmanski Exp $
 */
public class Persister
{
    public static void create(Object object) throws EdmException
    {
        create(object, false);
    }

    public static void create(Object object, boolean flush) throws EdmException
    {
        try
        {
            DSApi.context().session().save(object);
            if (flush)
            {
                DSApi.context().session().flush();
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static void delete(Object object) throws EdmException
    {
        try
        {
            DSApi.context().session().delete(object);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    public static void update(Object object) throws EdmException
    {
        try
        {
            DSApi.context().session().update(object);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
}
