package pl.compan.docusafe.core.dockinds;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;

import java.util.List;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DataBaseEnumChannelProvider implements ChannelProvider {
    private final static Logger LOG = LoggerFactory.getLogger(DataBaseEnumChannelProvider.class);

    private final String fieldCn;
    private final int firstId;
    private final String prefix;
    private final String dockindCn;

    /**
     * Tworzy nowy obiekt DataBaseEnumChannelProvider
     * @param fieldCn pole dataBaseEnumField, nie mo�e by� null
     * @param firstId pierwsze uwzgl�dniane id, mo�e by� ujemne
     */
    public DataBaseEnumChannelProvider(String fieldCn, String dockindCn, String prefix, int firstId) {
        this.fieldCn = Preconditions.checkNotNull(fieldCn, "field cannot be null");
        this.prefix = Preconditions.checkNotNull(prefix, "prefix cannot be null");
        this.dockindCn = Preconditions.checkNotNull(dockindCn, "dockindCn cannot be null");
        this.firstId = firstId;

        LOG.info("created provider : {}", this.toString());
    }

    public String apply(DocFacade documentFacade) {
        try {
            Fields fields = documentFacade.getFields();
            EnumItem ei = (EnumItem)fields.get(fieldCn);
            if(ei == null){
                LOG.info("field null ... returning");
                return null;
            }
            LOG.info("field id = {}, firstId = {}", ei.getId(), firstId);
            if(ei.getId() >= firstId){
                LOG.info("returning {}{}", prefix, ei.getId());
                return prefix + ei.getId();
            } else {
                return null;
            }
        } catch (EdmException e) {
            throw new RuntimeException(e);
        }
    }

    private DataBaseEnumField getField() throws EdmException {
       return (DataBaseEnumField)DocumentKind.findByCn(dockindCn).getFieldByCn(fieldCn);
    }

    public List<ProcessChannel> getEditableChannels() throws EdmException {
        List<ProcessChannel> ret = Lists.newArrayList();
        for(DockindDictionaries dd : DockindDictionaries.list(getField().getTableName(), firstId)){
            if(dd.isAvailable()){
                ProcessChannel pc = new ProcessChannel();
                pc.setName(dd.getTitle());
                pc.setCn(prefix + dd.getId());
                ret.add(pc);
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        return "DataBaseEnumChannelProvider{" +
                "dockindCn='" + dockindCn + '\'' +
                ", fieldCn=" + fieldCn +
                ", firstId=" + firstId +
                ", prefix='" + prefix + '\'' +
                '}';
    }
}
