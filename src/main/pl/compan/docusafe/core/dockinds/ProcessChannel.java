package pl.compan.docusafe.core.dockinds;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariusz Kilja�czyk
 */
public class ProcessChannel 
{
	public static final String CONDITION_MIXING_AND = "and";
	public static final String CONDITION_MIXING_OR = "or";
	
	private String cn;
	private String name;
	private String conditionMixing;
	private List<Condition> conditions = new ArrayList<Condition>();
	private String username;
	
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getConditionMixing() {
		return conditionMixing;
	}
	public void setConditionMixing(String conditionMixing) {
		this.conditionMixing = conditionMixing;
	}
	public List<Condition> getConditions() {
		return conditions;
	}
	public void setConditions(List<Condition> list) {
		this.conditions = list;
	}
	public void addCondition(Condition c)
	{
		this.conditions.add(c);
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(""+ cn+"::"+name+"::"+conditionMixing+"\n");
		
		for(Condition con :conditions)
		{
			sb.append(con.toString());
		}
		
		return sb.toString();
	}

}
