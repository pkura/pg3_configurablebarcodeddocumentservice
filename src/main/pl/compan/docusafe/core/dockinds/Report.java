package pl.compan.docusafe.core.dockinds;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;

/**
 * Definiuje jeden raport dla danego rodzaju dokumentu.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class Report
{
    private String cn;
    private String type = DEFAULT_TYPE;
    private Map<String,String> names;
    private Map<String,String> descriptions;
    private String reportId;
    private Map<Integer,Criterion> criteria;
    private Map<String,String> properties;
    
    // nazwy w�a�ciwo�ci
    public static final String COLUMN_PARTITION = "column-partition";
    public static final String ROW_PARTITION = "row-partition";
    public static final String ROW_SUMMARY = "row-summary";
    public static final String FIRST_COLUMN_STYLE = "column1-style";
    public static final String SECOND_COLUMN_STYLE = "column2-style";
    public static final String PDF_ORIENTATION = "pdf-orientation";
    /** podtyp raportu - jego znaczenie definije ju� klasa generuj�ca dany raport */
    public static final String TYPE = "type";
    /** 
     * okre�la kolumny na szczeg�owym widoku "jednej linijki raportu" - gdy null tzn. �e nie mo�na 
     * podejrze� �adnych szczeg��w raportu 
     */
    public static final String DETAILS_VIEW = "details-view";
    /**
     * okre�la kolumny w raporcie (w postaci identyfikatora obiektu Criterion z ROW_PARTITION - musi by�
     * to podzbi�r listy warto�ci z w�a�ciwo�ci ROW_PARTITION), z kt�rych prowadz�
     * linki do szczeg�owego widoku
     */
    public static final String DETAILS_COLS = "details-cols";
    /**
     * okre�la kolumny specyficzne w raporcie (cz�ciowo znaczenie tego pola zale�y od rodzaju raportu),
     * np. w raportach liczbowych to pole jest ignorowane, a w zestawieniach dokument�w s� to kolumny
     * opisuj�ce wypisywane dokumenty
     */
    public static final String SPECIFIC_COLUMNS = "specific-columns";
    /** 
     * okre�la numer specyficznej kolumny (SPECIFIC_COLUMNS), kt�ra oznacza identyfiktor dokumentu - 
     * gdy ustawione to w tej kolumnie b�d� tworzone na raporcie linki do ekranu edycji dokumentu.
     */  
    public static final String DOCUMENT_LINK_COL = "document-link-col";
    
    // style wy�wietlania kolumn
    public static final String STYLE_SPECIAL_ROW = "special-row";
    public static final String STYLE_ONLY_FIRST = "only-first";
    
    // orientacje wydruku
    public static final String VERTICAL_ORIENTATION = "vertical";   // pionowa
    public static final String HORIZONTAL_ORIENTATION = "horizontal"; // pozioma
    
    // typy raportow, raport moze byc normalny (default), wtedy wyswietla sie na formatce,
    // albo csv_only wtedy nie mozna go wyswieltic w formatce, jest tylko do pobrania w pliku .csv 
    public static final String DEFAULT_TYPE = "default";
    public static final String CSV_ONLY = "csv";
    
    
    
    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public Map<String, String> getProperties()
    {
        if (properties == null)
            properties = new LinkedHashMap<String, String>();
        return properties;
    }
    
    public String getProperty(String key)
    {
        return properties != null ? properties.get(key) : null;
    }
    
    public boolean propertyOn(String key)
    {
        return properties != null ? "true".equals(properties.get(key)) : false;
    }
    
    public String[] getPropertyArray(String key)
    {
        if (key == null || properties == null || properties.get(key) == null)
            return new String[]{};
        return properties.get(key).split("\\s*;\\s*");      
    }
    
    public Map<Integer,Criterion> getCriteria()
    {
        if (criteria == null)
            criteria = new LinkedHashMap<Integer, Criterion>();
        return criteria;
    }

    public void setCriteria(Map<Integer,Criterion> criteria)
    {
        this.criteria = criteria;
    }
    
    public Criterion getCriterion(String id)
    {        
        return criteria.get(Integer.valueOf(id));
    }
    
    public Criterion getCriterion(String type, String fieldCn)
    {
        for (Criterion criterion : criteria.values())
            if (criterion.getType().equals(type) && criterion.getFieldCn().equals(fieldCn))
                return criterion;
        return null;
    }
    
    public String getName()
    {
        return Docusafe.getLanguageValue(names);
    }

    public void setNames(Map<String,String> names)
    {
        this.names = names;
    }

    public String getDescription()
    {
        return Docusafe.getLanguageValue(descriptions);
    }

    public void setDescriptions(Map<String,String> descriptions)
    {
        this.descriptions = descriptions;
    }

    public String getReportId()
    {
        return reportId;
    }

    public void setReportId(String reportId)
    {
        this.reportId = reportId;
    }
    
    public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public static class Criterion
    {
        private String type;        
        private String fieldCn;
        private Map<String,String> names;
        private boolean required;

        public Criterion(String type, String fieldCn, Map<String,String> names, boolean required)
        {            
            this.type = type;
            this.fieldCn = fieldCn;
            this.names = names;
            this.required = required;
        }
        
        public String getFieldCn()
        {
            return fieldCn;
        }

        public String getType()
        {
            return type;
        }

        public String getName()
        {
            return Docusafe.getLanguageValue(names);
        }

        public boolean isRequired()
        {
            return required;
        }                 
    }
	
	
	public static File getCSVreport(long id) {
		File file = new File(Docusafe.getHome() + "/reports/report_"+id+".csv");
		if (file.exists()) {
			return file;
		} else {
			return null;
		}
	}
}
