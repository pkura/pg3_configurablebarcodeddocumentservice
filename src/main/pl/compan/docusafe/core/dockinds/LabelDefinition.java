package pl.compan.docusafe.core.dockinds;

import java.util.ArrayList;
import java.util.List;

public class LabelDefinition {

	
	public static final String CONDITION_MIXING_AND = "and";
	public static final String CONDITION_MIXING_OR = "or";
	
	private String name;
	private String conditionMixing;
	private Boolean hidding;
	private Boolean deletesAfter;
	private String ancestorName;
	private String description;
	private String fieldCn;
	private Integer inactionTime;
	private List<Condition> conditions = new ArrayList<Condition>();
	private Boolean strong;
	
	
	public Boolean getHidding() {
		return hidding;
	}
	public void setHidding(Boolean hidding) {
		this.hidding = hidding;
	}
	public Boolean getDeletesAfter() {
		return deletesAfter;
	}
	public void setDeletesAfter(Boolean deletesAfter) {
		this.deletesAfter = deletesAfter;
	}
	
	public String getAncestorName() {
		return ancestorName;
	}
	public void setAncestorName(String ancestorName) {
		this.ancestorName = ancestorName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFieldCn() {
		return fieldCn;
	}
	public void setFieldCn(String fieldCn) {
		this.fieldCn = fieldCn;
	}
	public Integer getInactionTime() {
		return inactionTime;
	}
	public void setInactionTime(Integer inactionTime) {
		this.inactionTime = inactionTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getConditionMixing() {
		return conditionMixing;
	}
	public void setConditionMixing(String conditionMixing) {
		this.conditionMixing = conditionMixing;
	}
	public List<Condition> getConditions() {
		return conditions;
	}
	public void setConditions(List<Condition> list) {
		this.conditions = list;
	}
	public void addCondition(Condition c)
	{
		this.conditions.add(c);
	}
	public boolean verify()
	{
		if(hidding==null || deletesAfter == null || inactionTime == null ||
				fieldCn==null||fieldCn.equals("")) return false;
		else return true;
	}
	public Boolean getStrong()
	{
		return strong;
	}
	public void setStrong(Boolean strong)
	{
		this.strong = strong;
	}
	
	
}
