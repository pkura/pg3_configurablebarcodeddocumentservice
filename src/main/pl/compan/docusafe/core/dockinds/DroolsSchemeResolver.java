package pl.compan.docusafe.core.dockinds;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import pl.compan.docusafe.core.jbpm4.Jbpm4ActivityInfo;
import pl.compan.docusafe.api.SchemeFields;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.FieldAccessHashCacheImpl;
import pl.compan.docusafe.core.drools.DroolsUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DroolsSchemeResolver implements SchemeResolver{
    private final static Logger LOG = LoggerFactory.getLogger(DroolsSchemeResolver.class);
    private final KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

    private DroolsSchemeResolver(){
    }

    public DockindScheme provideSheme(FieldsManager fm) throws EdmException {
        LOG.info("provideScheme");
        DockindScheme ds = new DockindScheme();
        //if(fm.getDocumentId() == null) {
        	//return ds;
       // }
        StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
        ksession.execute(Arrays.asList(
                SchemeFields.fromScheme(ds),
                new FieldAccessHashCacheImpl(fm)));
                /*Documents.document(fm.getDocumentId()).getFields())*/
        return ds;
    }

    public DockindScheme provideSheme(FieldsManager fm, String activity) throws EdmException {
        LOG.info("provideScheme");
        DockindScheme ds = new DockindScheme();
        //if(fm.getDocumentId() == null) {
        	//return ds;
       // }
        StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
        ksession.execute(Arrays.asList(
                SchemeFields.fromScheme(ds),
                new FieldAccessHashCacheImpl(fm),
                new Jbpm4ActivityInfo(activity)));
        return ds;
    }
    public static SchemeResolver fromSource(String source){
        LOG.info("schemas source : {}", source);
        DroolsSchemeResolver ret = new DroolsSchemeResolver();
        KnowledgeBuilder kb = DroolsUtils.createKnowledgeBuilder();
        try {
            kb.add(ResourceFactory.newByteArrayResource(source.getBytes("UTF-8")),
                   ResourceType.DRL);
        } catch (UnsupportedEncodingException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
        if(kb.hasErrors()){
            DroolsUtils.reportErrors(kb);
        } else {
            ret.kbase.addKnowledgePackages(kb.getKnowledgePackages());
            LOG.info("drools compilation successful : " + ret.kbase.getKnowledgePackages().size() + " packages");
        }
        return ret;
    }
}
