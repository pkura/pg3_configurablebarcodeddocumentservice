package pl.compan.docusafe.core.dockinds.measure;

public class BasicCheckPoint implements CheckPoint
{
	private String name;
	private Integer score;
	private boolean checkpointFinal;
	private boolean fulfilled;
	private boolean currentCheckpoint;
	
	public BasicCheckPoint(String name, Integer score, boolean checkpointFinal, boolean fulfilled,boolean currentCheckpoint)
	{
		super();
		this.name = name;
		this.score = score;
		this.checkpointFinal = checkpointFinal;
		this.fulfilled = fulfilled;
		this.currentCheckpoint = currentCheckpoint;
	}

	public String getCheckPointName()
	{
		return name;
	}

	public Integer getCheckpointScore()
	{
		return score;
	}

	public boolean isCheckpointFinal()
	{
		return checkpointFinal;
	}

	public boolean isFulfilled()
	{
		return fulfilled;
	}

	public boolean isCurrentCheckpoint()
	{
		return currentCheckpoint;
	}

}
