package pl.compan.docusafe.core.dockinds.measure;

import java.util.ArrayList;
import java.util.List;


/**
 * Klasa zawierająca informacje o procesie zadania 
 */
public class ProcessParameterBean
{
	private String processName;
	private List<Alert> alerts;
	private List<CheckPoint> checkpoints;

	public void addCheckpoint(CheckPoint checkPoint)
	{
		if(checkpoints == null)
			checkpoints = new ArrayList<CheckPoint>();
		checkpoints.add(checkPoint);
	}

	public void addAlert(Alert alert)
	{
		if(alerts == null)
			alerts = new ArrayList<Alert>();
		alerts.add(alert);
	}

	public List<Alert> getAlerts()
	{
		return alerts;
	}

	public List<CheckPoint> getCheckpoints()
	{
		return checkpoints;
	}

	public void setProcessName(String processName)
	{
		this.processName = processName;
	}

	public String getProcessName()
	{
		return processName;
	}
}
