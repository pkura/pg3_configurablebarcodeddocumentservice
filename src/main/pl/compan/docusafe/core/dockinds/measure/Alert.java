package pl.compan.docusafe.core.dockinds.measure;

/**
 * Interfejs reprezentujacy alert dla procesu
 * @author wkutyla
 */
public interface Alert
{
	/**
	 * Nazwa alertu
	 * @return
	 */
	public String getAlertName();
	
	/**
	 * Kod alertu
	 * @return
	 */
	public String getAlertCode();
	
	/**
	 * Czy alert jest wlaczony
	 * @return
	 */	
	public boolean isAlertPresent();
	/**
	 * Ocena alertu od 0..100
	 * @return
	 */
	public Integer getAlertScore();
	
	/**
	 * Typ alertu - zgodnie z wykazem kodow
	 * @return
	 */
	public String getAlertType();
	
	public final static String SOFT_ALERT_TYPE = "soft";
	public final static String HARD_ALERT_TYPE = "hard";
	public final static String CRITICAL_ALERT_TYPE = "critical";
}
