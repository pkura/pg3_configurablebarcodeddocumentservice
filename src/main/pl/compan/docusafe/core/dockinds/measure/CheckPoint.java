package pl.compan.docusafe.core.dockinds.measure;

/**
 * Punkt kontrolny procesu 
 * @author wkutyla
 *
 */
public interface CheckPoint 
{
	/**
	 * Nazwa checkpointu
	 * @return
	 */
	public String getCheckPointName();
	
	/**
	 * Czy checkpoint jest zaliczony
	 * @return
	 */
	public boolean isFulfilled();
	
	/**
	 * Ocena checkpointu w skali 0..100
	 * @return
	 */
	public Integer getCheckpointScore();
	
	/**
	 * Czy wypelnienie checkpointu oznacza wypelnienie procesu
	 * @return
	 */
	public boolean isCheckpointFinal();
	
	/**
	 * True jesli jest to aktualne miejsce procesu
	 * @return
	 */
	public boolean isCurrentCheckpoint();
}
