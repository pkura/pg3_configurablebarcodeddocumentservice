package pl.compan.docusafe.core.dockinds.dwr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DwrTest
{
	private List<Field> fields;
	private Map<String, Object> values;
	private Logger log = LoggerFactory.getLogger("basia");

	public DwrTest()
	{
		fields = new ArrayList<Field>();
		fields.add(new Field("kind", "Rodzaj", Field.Type.ENUM)); // obiekt jako lista warto�ci, warto��  zaznaczona

		fields.get(0).setSubmit(true);

		Field name = new Field("name", "Nazwa", Field.Type.STRING);
		name.setSubmit(true);
		name.setSubmitTimeout(2000);
		name.setValidatorRegExp("^firma");

		fields.add(name);

		Field delivery = new Field("delivery", "Spos�b dostarczenia", Field.Type.ENUM_AUTOCOMPLETE);
		delivery.setSubmit(true);
		fields.add(delivery);

		fields.add(new Field("count", "Liczba", Field.Type.INTEGER));
		fields.get(fields.size() - 1).setRequired(true);

		Field money = new Field("money", "Kwota", Field.Type.MONEY);
		money.setRequired(true);
		fields.add(money);

		fields.add(new Field("date", "Data", Field.Type.DATE));
		fields.add(new Field("paid", "Op�acona", Field.Type.BOOLEAN));
		fields.add(new Field("timer", "Czas", Field.Type.TIMESTAMP));
		fields.add(new Field("multi", "MultiTest", Field.Type.ENUM_MULTIPLE));

		Field desc = new Field("desc", "Opis", Field.Type.STRING);
		desc.setHidden(true);
		fields.add(desc);

		Field dic = new Field("supplier", "Dostawca", Field.Type.DICTIONARY);
		dic.setDictionary(new DwrDictionaryBase());
		fields.add(dic);
	}

	public List<Field> getFieldsList()
	{
		return fields;
	}

	public Map<String, Object> getFieldsValues()
	{
		/*
		 * try { Thread.currentThread().sleep(4000); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		values = new HashMap<String, Object>();
		HashMap<String, String> enumValues = new HashMap<String, String>() {
			{
				this.put("1", "uno");
				this.put("2", "dos");
				this.put("3", "tres");
				this.put("4", "quatro");
				this.put("6", "seis");
			}
		};
		List<String> enumSelected = new ArrayList<String>() {
			{
				this.add("dos");
				this.add("seis");
			}
		};

		Map<String, String> delivery = new HashMap<String, String>() {
			{
				this.put("1", "poczta");
				this.put("2", "kurier");
				this.put("3", "pocztex");
				this.put("4", "kurier ups");
				this.put("5", "kurier ups - powy�ej 20kg");
				this.put("6", "kurier si�demka");
				this.put("7", "kurier si�demka - ekspres");
				this.put("8", "list polecony");
				this.put("9", "list zwyk�y");
			}
		};
		List<String> deliverySelected = new ArrayList<String>() {
			{
				this.add("pocztex");
			}
		};

		Map<String, String> multiKeys = new HashMap<String, String>() {
			{
				this.put("1", "lunes");
				this.put("2", "martes");
				this.put("3", "miercoles");
				this.put("4", "jueves");
				this.put("5", "viernes");
				this.put("6", "sabado");
				this.put("7", "domingo");
			}
		};

		List<String> multiSelected = new ArrayList<String>() {
			{
				this.add("sabado");
				this.add("domingo");
			}
		};

		values.put("kind", new EnumValues(enumValues, enumSelected));
		values.put("delivery", new EnumValues(delivery, deliverySelected));
		values.put("name", "Firma trans");
		values.put("count", 5000);
		values.put("date", new Date());
		values.put("paid", true);
		values.put("timer", new Date());
		values.put("multi", new EnumValues(multiKeys, multiSelected));
		values.put("desc", "Dzialalnosc osobista");
		values.put("supplier", new DwrDictionaryBase());
		return values;
	}

	public List<Field> setFieldValue(String cn, Integer value)
	{ // wszystkie pola
		for (Field field : fields)
			field.setStatus(Field.Status.NOCHANGE);

		Field f = fields.get(fields.size() - 1);
		// f.setStatus(Field.Status.REMOVE);

		Field client = new Field("client", "Klient", Field.Type.STRING);
		if (!fields.contains(client))
			fields.add(client);

		return fields;
	}

	public List<Field> setFieldsValues(Map<String, FieldData> values)
	{
		Field msg = new Field("messageField", "Schemat zosta� zmieniony", Field.Type.BOOLEAN);
		msg.setKind(Field.Kind.MESSAGE);

		for (Field field : fields)
			field.setStatus(Field.Status.NOCHANGE);

		Field f = fields.get(fields.size() - 1);
		// f.setStatus(Field.Status.REMOVE);

		Field client = new Field("client", "Klient", Field.Type.STRING);
		if (!fields.contains(client))
			fields.add(client);

		fields.add(msg);
		return fields;
	}
}
