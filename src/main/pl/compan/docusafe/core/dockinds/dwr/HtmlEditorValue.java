package pl.compan.docusafe.core.dockinds.dwr;

/**
 * Warto�� pola typu {@link Field.Type#HTML_EDITOR}
 * @author Kamil
 *
 */
public class HtmlEditorValue {
	private String html;
	
	public HtmlEditorValue() {
		
	}
	
	public HtmlEditorValue(String html) {
		this.html = html;
	}
	
	public String getHtml() {
		return html;
	}
	
	public void setHtml(String html) {
		this.html = html;
	}

	@Override
	public String toString() { 
		return (html != null) ? html : "";
	}
	
	
}
