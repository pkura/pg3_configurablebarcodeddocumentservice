package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class EmptyValueHandler implements ValueHandler
{

	@Override
	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception
	{
		
	}

}
