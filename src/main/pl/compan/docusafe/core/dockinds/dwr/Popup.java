package pl.compan.docusafe.core.dockinds.dwr;

import org.apache.commons.lang.StringUtils;

public class Popup {
    String showpopupcn;
    String closepopupcn;
    String popupcn;
    String caption;
    boolean closeable;
    String closerefcn;

    public boolean isPopupPresent() {
        return StringUtils.isNotEmpty(showpopupcn) || StringUtils.isNotEmpty(closepopupcn) || StringUtils.isNotEmpty(popupcn);
    }

    public String getClosepopupcn() {
        return closepopupcn;
    }

    public void setClosepopupcn(String closepopupcn) {
        this.closepopupcn = closepopupcn;
    }

    public String getPopupcn() {
        return popupcn;
    }

    public void setPopupcn(String popupcn) {
        this.popupcn = popupcn;
    }

    public String getShowpopupcn() {
        return showpopupcn;
    }

    public void setShowpopupcn(String showpopupcn) {
        this.showpopupcn = showpopupcn;
    }

    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCloserefcn() {
        return closerefcn;
    }

    public void setCloserefcn(String closerefcn) {
        this.closerefcn = closerefcn;
    }
}
