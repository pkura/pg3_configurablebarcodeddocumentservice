package pl.compan.docusafe.core.dockinds.dwr;

public class LinkValue {

	private String label;
	private String link;
	
	public LinkValue(String label, String link) {
		this.label = label;
		this.link = link;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	public String toString()
	{
		return link;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		LinkValue other = (LinkValue) obj;
		if (link == null)
		{
			if (other.link != null)
				return false;
		}
		
		else if (!link.equals(other.link))
			return false;
		
		return true;
	}


	
	
	
}
