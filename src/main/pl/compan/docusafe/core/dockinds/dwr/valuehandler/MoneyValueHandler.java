package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class MoneyValueHandler implements ValueHandler
{

	@Override
	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception
	{	
		Object newValue = null;
		Object oldValue = dwrFacade.getDwrFieldsValues().get(field.getCn());
		if (values.get(field.getCn()).getData() != null)
			newValue = values.get(field.getCn()).getMoneyData();
		if ((oldValue != null && !oldValue.equals(newValue)) || (oldValue == null && newValue != null))
			dwrFacade.getFieldValues().put(oldCn, newValue);
	
	}

}
