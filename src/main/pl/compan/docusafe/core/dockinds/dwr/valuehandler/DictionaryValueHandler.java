package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DictionaryValueHandler implements ValueHandler
{

	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values)
	{
		Object newValue = null;
		if (containIds(field, values))
		{
			newValue = values.get(field.getCn()).getStringData();
			String [] ids = newValue.toString().split(",");
			if (ids.length == 1)
			dwrFacade.getFieldValues().put(oldCn, ids[0]);
			else
				dwrFacade.getFieldValues().put(oldCn, ids);
		}
		else
		{
			FieldData newField = values.get(field.getCn());
			if(newField == null)	
			{
				dwrFacade.getFieldValues().put(oldCn, null);
				return;
			}
			newValue = values.get(field.getCn()).getData();
			
			if (newValue instanceof Map)
			{
				List<String> ids = new ArrayList<String>();
				String dicCn = field.getCn().replaceFirst(DwrFacade.DWR_PREFIX, "");
				for (String cn : ((Map<String, FieldData>) newValue).keySet())
				{
					if (isIdField(newValue, dicCn, cn))
                        //todo - sprawdzenie, czy id wyst�puje w sesji umo�liwi usuwanie wierszy ze s�ownik�w
						ids.add(((Map<String, FieldData>) newValue).get(cn).getStringData());
				}
				if (ids.size() > 0)
                {
                    Collections.sort(ids);
                    dwrFacade.getFieldValues().put(oldCn, ids);
                }
				else
					dwrFacade.getFieldValues().put(oldCn, null);
			}
			else if (newValue instanceof List)
			{
				dwrFacade.getFieldValues().put(oldCn, (List)newValue);
			}
			else
				dwrFacade.getFieldValues().put(oldCn, newValue);
		}
		
	}

    /**
     * Zwraca true je�li pole istnieje w <code>values</code>, warto�� pola jest typu String lub Long
     * @param field
     * @param values
     * @return
     */
	boolean containIds(Field field, Map<String, FieldData> values)
	{
		return values.get(field.getCn()) != null && (values.get(field.getCn()).getType().equals(Field.Type.STRING) || values.get(field.getCn()).getType().equals(Field.Type.LONG));
	}
	
	boolean isIdField(Object newValue, String dicCn, String cn)
	{
		return (cn.contains(dicCn + "_ID") || cn.equals("id")) && ((Map<String, FieldData>) newValue).get(cn).getData() != null && !((Map<String, FieldData>) newValue).get(cn).getStringData().equals("");
	}
}
