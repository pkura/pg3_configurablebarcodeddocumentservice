package pl.compan.docusafe.core.dockinds.dwr;


public class ButtonValue {
	private String label;
	private String value;
	private boolean clicked = false;
	
	public ButtonValue() {
		
	}
	
	public ButtonValue(String label, String value, boolean clicked) {
		this.label = label;
		this.value = value;
		this.clicked = clicked;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public boolean isClicked() {
		return clicked;
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(label).append(", ");
		sb.append(value).append(", ");
		sb.append(clicked);
		
		return sb.toString();
	}
}
