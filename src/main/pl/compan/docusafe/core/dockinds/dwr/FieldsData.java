package pl.compan.docusafe.core.dockinds.dwr;

import java.util.Date;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FieldsData {
	private Object data;
	private String dataType;
	
	private Logger log = LoggerFactory.getLogger("basia"); 
	
	//typ danych trzymanych w data
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getStringData() {
		return data.toString();
	}

	public void setStringData(String data) {
		this.data = data;
	}
	
	public void setIntegerData(Integer data) {
		this.data = data;
	}
	
	public Integer getIntegerData() {
		return (Integer) data;

	}
	
	public Date getDateData() {
		return (Date) this.data;
	}
	
	public void setDateData(Date data) {
		this.data = data;
	}
	
	public void setEnumValuesData(EnumValues data) {
		this.data = data;
	}
	
	public EnumValues getEnumValuesData() {
		return (EnumValues) data;
	}

	public Boolean getBooleanData() {
		return (Boolean) data;
	}
	
	public void setBooleanData(Boolean data) {
		this.data = data;
	}
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public FieldsData() {}
}
