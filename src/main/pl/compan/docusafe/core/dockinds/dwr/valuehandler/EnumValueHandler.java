package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class EnumValueHandler implements ValueHandler
{

	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception 
	{
		String oldSelectedCn = null;
		EnumValues enumValues = values.get(field.getCn()).getEnumValuesData();
		if (((EnumValues) dwrFacade.getDwrFieldsValues().get(field.getCn())).getSelectedOptions().size() != 0)
			oldSelectedCn = ((EnumValues) dwrFacade.getDwrFieldsValues().get(field.getCn())).getSelectedOptions().get(0);
		String newSelectedCn = enumValues.getSelectedOptions().get(0);
		if ((oldSelectedCn != null && !oldSelectedCn.equals(newSelectedCn)) || (oldSelectedCn == null && newSelectedCn != null))
		{
			dwrFacade.getFieldValues().put(oldCn, newSelectedCn);
			dwrFacade.reloadValuesSaveSession();
		}
	}

}
