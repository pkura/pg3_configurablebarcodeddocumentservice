package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExp;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.viewserver.ViewServer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Logik� mo�na nadpisywa� w dockindach.
 * 
 * Elementy dodawane przez popup w mapie ( metoda add()) jako klucze maj�:- SLOWNIK_POLE
 * Elementy zapisywane poprzez przycisk zapisz w mapie (metoda add()) maj�: - POLE
 * 
 */
public class DwrDictionaryBase implements DwrSearchable, DwrDictionaryFilter
{
	private static final Logger log = LoggerFactory.getLogger(DwrDictionaryBase.class);
    public static final String ID = "id";

    private DwrDictionaryFilter dictionaryFilter = new DwrDictionaryDefaultFilter();

	private pl.compan.docusafe.core.dockinds.field.Field oldField;
	/**
	 * Nazwa s�ownika w mapie {@link #dictionaries}
	 */
	private String name;
	/**
	 * Wszystkie pola w s�owniku
	 */
	private List<Field> fields;
	/**
	 * Pola s�ownika widoczne na formatce
	 */
	private List<String> visibleFieldsCns;
	/**
	 * Pola s�ownika, z autouzupe�nianiem (wyszukiwaniem)
	 */
	private List<String> searchableFieldsCns;
	/**
	 * Pola s�ownika widoczne na li�cie podpowiedzi. <br />
	 * Etykieta ({@link Field#label} jest nag�owkiem kolumny.
	 */
	private List<String> promptFieldsCns;
	/**
	 * Pola s�ownika widoczne na formatece popUp
	 */
	private List<Field> popUpFields;
	/**
	 * Wartosci fildow slownika.
	 */
	private Map<String, Object> values;
	/**
	 * Czy wy�wietla� przycisk pokazuj�cy okienko ze s�ownikiem?
	 */
	private boolean showDictionaryPopup = true;
	/**
	 * Czy s�owonik jest wielo wartosciowy
	 */
	private boolean multiple = false;
	/**
	 * List buttonow popUpa doAdd,doEdit,doRemove,doSelect
	 */
	private List<String> popUpButtons;
	/**
	 * List buttonow s�ownika showPopup,doAdd,doClear,doRemove
	 */
	private List<String> dicButtons;
	/**
	 * Mo�liwo�c modyfikacji istniej�ych wpisow przez uzytkownika.
	 */
	private boolean cantUpdate = false;
	/**
	 * W��czenie submita ca�egj strony przy zmianie wartosci pola s�ownikowego
	 */
	private boolean disabledSubmit = false;
	/**
	 * CZy dodajemy nowy wpis s�ownikowy tylko z popupa
	 */
	private boolean onlyPopUpCreate = false;
	/**
	 * Czy s�ownik ma wy�wietla� si� w postaci tabeli: [Etykieta_pola_1]
	 * [Etykieta_pola_2] [Warto��_1] [Warto��_2]
	 */
	private boolean tableView = true;
	/**
	 * Flaga okre�la od�wie�anie ca�ego s�ownika wielowarto�ciowego. <br />
	 * Ma zastosowanie tylko dla s�ownika wielowarto�ciowego ({@link DwrDictionaryBase#multiple} ustawione na <code>true</code>).
	 * <ul>
	 * <li><code>true</code> - po dodaniu nowego elementu do s�ownika od�wie�one zostaj� wszystkie warto�ci
	 * dla s�ownika - nie tylko te nowo dodane przez u�ytkownika
	 * <li><code>false</code> - po dodaniu nowego elementu nie od�wie�amy warto�ci
	 * </ul>
	 */
	private boolean refreshMultipleDictionary = false;
	
	private String jsPopupFunction = null;
	private boolean autohideMultipleDictionary = false;
	
	/**
	 * czy poprzednie wyszukanie dla tego s�ownika by�o puste
	 */
	private boolean lastSearchResultEmpty = false;
	/**
	 * liczba znak�w zawartych w parametrach poprzedniego wyszukiwania
	 */
	private Integer lastSearchValuesSize;
    private boolean autocleanEmptyItems = false;

    public DwrDictionaryBase()
	{
	}

	private void init(pl.compan.docusafe.core.dockinds.field.Field dictionaryField, Map<String, SchemeField> schemesField) throws EdmException
	{
		this.fields = new ArrayList<Field>();
		this.visibleFieldsCns = new ArrayList<String>();
		this.searchableFieldsCns = new ArrayList<String>();
		this.promptFieldsCns = new ArrayList<String>();
		this.popUpFields = new ArrayList<Field>();
		this.popUpButtons = new ArrayList<String>();
		this.dicButtons = new ArrayList<String>();
		
		this.oldField = dictionaryField;
		this.disabledSubmit = oldField.isDisableSubmitDictionary();
		this.tableView = oldField.isAsTable();
		this.multiple = oldField.isMultiple();
		this.showDictionaryPopup = oldField.getShowDictionaryPopUp();
		this.cantUpdate = oldField.isCantUpdate();
		this.onlyPopUpCreate = oldField.isOnlyPopUpCreate();
		this.popUpButtons = oldField.getPopUpButtons();
		this.dicButtons = oldField.getDicButtons();
		this.refreshMultipleDictionary = oldField.isRefreshMultipleDictionary();
		this.jsPopupFunction = oldField.getJsPopupFunction();
		this.autohideMultipleDictionary = oldField.isAutohideMultipleDictionary();
        this.autocleanEmptyItems = oldField.isAutocleanEmptyItems();
		
		List<String> pop = oldField.getPopUpFields();
		List<String> prompt = oldField.getPromptFields();
		List<String> search = oldField.getSearchFields();
		List<String> visible = oldField.getVisibleFields();

		for (pl.compan.docusafe.core.dockinds.field.Field f : oldField.getFields())
		{
			Field field = null;
			SchemeField scheme = null;

			if (schemesField != null)
			{
				if (schemesField.containsKey(oldField.getCn() + "_" +f.getCn()))
					scheme = schemesField.get(oldField.getCn() + "_" +f.getCn());
				else
					scheme = schemesField.get(f.getCn());
			}

			field = f.getFieldForDwr(scheme);
			
			if (f instanceof NonColumnField)
				field.setCn(oldField.getCn().toUpperCase() + "_" + f.getCn().toUpperCase());
			else
				field.setCn(oldField.getCn().toUpperCase() + "_" + f.getColumn().toUpperCase());
			
			if (pop.contains(f.getCn()) || pop.get(0).equals("ALL"))
					popUpFields.add(field);
			
			if (prompt != null && prompt.contains(f.getCn()))
				promptFieldsCns.add(field.getCn());
			if (search != null && search.contains(f.getCn()))
				searchableFieldsCns.add(field.getCn());
			if (visible != null && visible.contains(f.getCn()))
				visibleFieldsCns.add(field.getCn());

			field.setKind(Field.Kind.DICTIONARY_FIELD);
			// TODO - pole schowane musi by� wysy�ane do s�ownika
			// �eby JS m�g� je ukry� i usun�� nag��wek tabelki
//			if (!field.getHidden())
				fields.add(field);
		}
		
		Field idField = null;
		if (isMultiple())
			idField = new Field(oldField.getCn().toUpperCase() + "_ID_1", oldField.getCn().toUpperCase() + "_ID_1", Field.Type.STRING);
		else
			idField = new Field("id", "id", Field.Type.STRING);
		
		idField.setHidden(true);
		idField.setSubmit(true);
		idField.setStatus(Field.Status.ADD);
		
		if (!fields.contains(idField))
		{
			fields.add(0,idField);
		
		}
		else
		{
			int indexOfIdField = fields.indexOf(idField);
			idField.setHidden(true);
			idField.setType(fields.get(indexOfIdField).getType());
		
		}

		if (!popUpFields.contains(idField))
			popUpFields.add(idField);
		//if (visible!=null && visible.contains(idField.getCn()))
			visibleFieldsCns.add(idField.getCn());
		this.name = oldField.getCn();
	}

	public DwrDictionaryBase(pl.compan.docusafe.core.dockinds.field.Field dictionaryField, Map<String, SchemeField> schemesField) throws EdmException
	{
		if (dictionaryField.getLogic() != null)
		{
			String dicClassName = dictionaryField.getLogic();
			try
			{
				Class c = Class.forName(dicClassName);
				DwrDictionaryBase obj = (DwrDictionaryBase) c.getConstructor().newInstance();
				obj.init(dictionaryField, schemesField);
				if (dictionaryField.getType().equals("document-person"))
				{
					((PersonDictionary) obj).setSender(((DocumentPersonField) dictionaryField).isSenderField());
				}
				DwrDictionaryFacade.dictionarieObjects.put(dictionaryField.getCn(), (DwrDictionaryBase) obj);
				DwrDictionaryFacade.addDwrDictionaryBase(dictionaryField.getDockindCn(), dictionaryField.getCn(), (DwrDictionaryBase) obj);
			}
			catch (Exception ex)
			{
				log.error(ex.getMessage(), ex);
			}
		}
		else
		{
			init(dictionaryField, schemesField);
			DwrDictionaryFacade.dictionarieObjects.put(dictionaryField.getCn(), this);
			DwrDictionaryFacade.addDwrDictionaryBase(dictionaryField.getDockindCn(), dictionaryField.getCn(), this);
		}
	}

	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		return null;
	}
	
	private int valuesSize(Map<String, FieldData> values){
		int size = 0;
		for(Map.Entry<String, FieldData> value : values.entrySet()){
			size+=value.getValue().getStringData().length();
		}
		return size;
	}
	
	/**
	 * @param dockindFields - nullable
	 */
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
			Map<String, FieldData> dockindFields) throws EdmException
	{
//        HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
//        if(req != null && (Map<String, String>)req.getSession().getAttribute("dwrSession") != null) {
//            Map<String, String> valu = ((Map<String, String>) req.getSession().getAttribute("dwrSession"));
//            String dockindCn = ((Map<String, String>)req.getSession().getAttribute("dwrSession")).get("dockindKind");
//        }


		// sprawdzanie czy poprzednie wyszukiwanie da�o rezultaty. Je�li nie, a kryteria si� zaw�zi�y zwraca pusty wynik bez wyszukiwania.
		if(lastSearchResultEmpty && lastSearchValuesSize <= valuesSize(values)){
			lastSearchValuesSize = valuesSize(values);
			return new ArrayList<Map<String, Object>>();
		}
		//zapisane d�ugo�ci parametr�w wyszukiwania
		lastSearchValuesSize = valuesSize(values);
		
		
		List<Map<String, Object>> result;
		if ((result = search(values,limit,offset))!=null) {
			return result;
		}
		
		String filteringDictionary = ((AbstractDictionaryField) getOldField()).getFilteringDictionaryCn();
		Map<String, String> filteredColumns = new HashMap<String, String>();
		Map<String, FieldData> filteringValues = null;
		//	jesli slownik jest slownikiem filtrowanym
		if(filteringDictionary != null){
			//	mapa filtrowanych kolumn wraz z odpowiadajacymi im kolumnami filtrujacymi
			filteredColumns = ((AbstractDictionaryField) getOldField()).getFieldsAttributeValue().get("filteringColumn");
			//	wartosci kolumn filtrujacych
			filteringValues = (Map<String, FieldData>) WebContextFactory.get().getHttpServletRequest().getSession().getAttribute("DIC_FILTER_"+filteringDictionary);
		}
		Map<String, String> whereExpsToValue = new HashMap<String, String>();
		List<Map<String, Object>> ret = Lists.newArrayList();
		String tableName;
		String resourceName = null;
		String id = "id";
		FieldData fieldData = null;
		boolean isDocumentField = getOldField().getType().equals("document-field");
		if (isDocumentField)
		{
			tableName = ((DocumentField) getOldField()).getTable();
			id = "document_id";
			((DocumentField) getOldField()).getDocumentKind().logic().addSpecialSearchDictionaryValues(getName(), values, dockindFields);
		}
		else
		{
			tableName = ((AbstractDictionaryField) getOldField()).getTable();
			resourceName = ((AbstractDictionaryField) getOldField()).getResource(); 
			((AbstractDictionaryField) getOldField()).getDocumentKind().logic().addSpecialSearchDictionaryValues(getName(), values, dockindFields);
		}
		
		NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
		NativeProjection np = NativeExps.projection();
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields()) // dodaje do projekcji pola posiadaj±ce swe kolumny
		{
			if ((!(f instanceof NonColumnField) && !f.getCn().equals("ID") && !f.getCn().equals("ID_1")))
			{
				String column = f.getColumn();
				if (isMultiple() && !(f instanceof NonColumnField))
					column = column.substring(0, column.lastIndexOf("_"));
				np.addProjection("d." + column, column);
			}
		}
		np.addProjection("d." + id, "id");
		nc.setProjection(np);
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
		{
			String column = f.getColumn();
			String fCn = f.getCn().toUpperCase();
			if (isMultiple() && !(f instanceof NonColumnField))
			{
				column = column.substring(0, column.lastIndexOf("_"));
				fCn = fCn.substring(0, fCn.lastIndexOf("_")).toUpperCase();
			}

			//nazwa pola
			String nameCn = new StringBuilder(getName()).append("_").append(fCn).toString();

			if (values.containsKey(nameCn) && !(f instanceof NonColumnField) && values.get(nameCn).getData() != null)
			{
				if (values.get(nameCn).getType().equals(Field.Type.DATE))
				{
					if (values.get(nameCn).getData() instanceof Date)
					{
						java.sql.Date sqlDate = new java.sql.Date(values.get(nameCn).getDateData().getTime());
						NativeExp exp = NativeExps.gte(column, sqlDate);
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						Calendar c = Calendar.getInstance();
						c.setTime(sqlDate); 
						c.add(Calendar.DATE, 1);
						exp = NativeExps.lte(column, c.getTime());
						nc.add(exp);
						sqlDate = new java.sql.Date(c.getTime().getTime());
						whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						
						if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
							sqlDate = new java.sql.Date(fieldData.getDateData().getTime());
							exp = NativeExps.gte(column, sqlDate);
							nc.add(exp);
							whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
							c = Calendar.getInstance();
							c.setTime(sqlDate); 
							c.add(Calendar.DATE, 1);
							exp = NativeExps.lte(column, c.getTime());
							nc.add(exp);
							sqlDate = new java.sql.Date(c.getTime().getTime());
							whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						}
					}
				}
				else if (values.get(nameCn).getType().equals(Field.Type.MONEY))
				{
					NativeExp exp = NativeExps.like(column, "%" + values.get(getName() + "_" + fCn).getMoneyData() +"%" );
					nc.add(exp);
					whereExpsToValue.put(exp.toSQL(), "'%" + values.get(getName() + "_" + fCn).getMoneyData() +"%'"); 
					if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
						fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
						exp = NativeExps.like(column, "%" +fieldData.getMoneyData() +"%" );
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'%" + fieldData.getMoneyData() +"%'"); 
					}
				}
				else if (values.get(nameCn).getType().equals(Field.Type.ENUM) 
						|| values.get(nameCn).getType().equals(Field.Type.ENUM_AUTOCOMPLETE)
						|| values.get(nameCn).getType().equals(Field.Type.INTEGER)
						|| values.get(nameCn).getType().equals(Field.Type.LONG))
						{
							if(!values.get(nameCn).getData().equals(NULLABLE)){
								NativeExp exp = NativeExps.eq(column, values.get(nameCn).getData());
								nc.add(exp);
								whereExpsToValue.put(exp.toSQL(), values.get(nameCn).getData().toString()); 
								if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
									fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
									exp = NativeExps.eq(column, fieldData.getData());
									nc.add(exp);
									whereExpsToValue.put(exp.toSQL(), fieldData.getData().toString()); 
								}
							}else{
								NativeExp exp = NativeExps.isNull(column);
								nc.add(exp);
								whereExpsToValue.put(exp.toSQL(), values.get(nameCn).getData().toString());
								if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
									fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
									exp = NativeExps.isNull(column);
									nc.add(exp);
									whereExpsToValue.put(exp.toSQL(), fieldData.getData().toString());
								}
							}
						}
				//wartosc dla pola boolean typu smallint null OR wartosc
				else if(values.get(nameCn).getType().equals(Field.Type.BOOLEAN))
				{
					NativeExp nativeValue = NativeExps.eq(column, values.get(nameCn).getData());
					NativeExp nativeNull = NativeExps.isNull(column);
					
					List<NativeExp> exps = new ArrayList<NativeExp>();
					if (values.get(nameCn).getData().equals(Boolean.FALSE)) {
						exps.add(nativeNull);
					}
					exps.add(nativeValue);
					NativeExp exp = NativeExps.disjunction(exps);
					nc.add(exp);
					if(values.get(nameCn).getData().toString().toLowerCase().equals("true")){
						whereExpsToValue.put(exp.toSQL(), 1+")"); 	
					}else{
						whereExpsToValue.put(exp.toSQL(), 0+")"); 
					}
					if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
						fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
						nativeValue = NativeExps.eq(column, fieldData.getData());
						nativeNull = NativeExps.isNull(column);
						exps = new ArrayList<NativeExp>();
						if (values.get(nameCn).getData().equals(Boolean.FALSE)) {
							exps.add(nativeNull);
						}
						exps.add(nativeValue);
						exp = NativeExps.disjunction(exps);
						nc.add(exp);
						if(fieldData.toString().toLowerCase().equals("true")){
							whereExpsToValue.put(exp.toSQL(), 1+")"); 	
						}else{
							whereExpsToValue.put(exp.toSQL(), 0+")"); 
						}
					}
				}
				else if (!values.get(nameCn).getStringData().equals("") && !(column.equals("ID") && isDocumentField))
				{
					try {
						String valueData = (String) values.get(nameCn).getData();
						if (column.equalsIgnoreCase("nip"))
							valueData = valueData.replace("-", "").replace(" ", "");
						NativeExp exp = null;
						if (f.isFullTextSearch())
							exp = NativeExps.contains(column, valueData );
						else
							exp = NativeExps.likeCaseInsensitive(column, "%" + valueData + "%");
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'%" + valueData + "%'");
						if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
							fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
							valueData = (String) fieldData.getData();
							if (column.equalsIgnoreCase("nip"))
								valueData = valueData.replace("-", "").replace(" ", "");
							if (f.isFullTextSearch())
								exp = NativeExps.contains(column, valueData);
							else
								exp = NativeExps.like(column, "%" + valueData + "%");
							nc.add(exp);
							whereExpsToValue.put(exp.toSQL(), "'%" + valueData +"%'");
						}
					}
					catch (Exception e)
					{
						log.debug(e);
					}
				}
			}
		}
		// ograniczenie za pomocą atrybutu not-nullable-fields
		if (this.getOldField() instanceof AbstractDictionaryField && ((AbstractDictionaryField)this.getOldField()).getNotNullableFields() != null) {
			for (String notNullField : ((AbstractDictionaryField)this.getOldField()).getNotNullableFields()){
				if (StringUtils.isNotEmpty(notNullField)) {
					NativeExp exp = NativeExps.isNotNull(notNullField);
					nc.add(exp);
					whereExpsToValue.put(exp.toSQL(), notNullField.toString()); 
				}
			}
		}
		
		if (values.containsKey("id") && !(values.get("id").getData().toString()).equals("")){
			NativeExp exp = NativeExps.eq("d." + id, values.get("id").getData());
			nc.add(exp);
			whereExpsToValue.put(exp.toSQL(), values.get("id").getData().toString()); 
		}else
			nc.setLimit(limit).setOffset(offset);
		try
		{
			//	gdy zrodlem pol slownika jest zewnetrzna baza danych
			if(resourceName != null){
				String sqlQuery = getQuery(nc.buildCriteriaQuery().getQueryString(), filteredColumns, filteringValues, whereExpsToValue, limit, offset);
				DictionaryField dic = (DictionaryField) getOldField();
				Map<String, Object> row = Maps.newLinkedHashMap();
				Context initCtx = new InitialContext();
				DataSource dataSource = (DataSource) initCtx.lookup("java:comp/env/jdbc/" + resourceName);
				Connection connection = dataSource.getConnection();
				ResultSet resultSet = connection.prepareStatement(sqlQuery).executeQuery();
            	String colVal = "";
            	while(resultSet.next()){
                	row = Maps.newLinkedHashMap();
                	row.put("id", resultSet.getObject("ID"));
                	row.put(getName().toUpperCase() + "_ID", resultSet.getObject("ID"));
            		for(String columnName : dic.getSearchFields()){
            			Object oVal = resultSet.getObject(columnName);
            			if(oVal != null){
            				colVal += columnName + " = " + resultSet.getObject(columnName) + " (" + resultSet.getObject(columnName).getClass() + ")\n";
            				row.put(getName().toUpperCase() + "_" + columnName, resultSet.getObject(columnName));
            			}else{
            				colVal += columnName + " = " + "" + " (" + "" + ")\n";
            				row.put(getName().toUpperCase() + "_" + columnName, "");
            			}
            		}
                	ret.add(row);
            	}
            	connection.close();
			}else{ 
				CriteriaResult cr = nc.criteriaResult();
				while (cr.next())
				{
					Map<String, Object> row = Maps.newLinkedHashMap();
					String entryId =  cr.getString("id", null);
					/* Jezeli jest polem dokumentu (document-field) to bierzemy tylko te, do ktorych uzytkownik ma uprawienia.
					Trzeba to bedzie przerobic, zeby sprawdzac w momencie wyszukania, a nie dopiero po*/
					if (isDocumentField)
					{
						boolean checkPermission = fromDwrSearch;
						try {
							Document.find(Long.valueOf(entryId), checkPermission);
						}
						catch(DocumentNotFoundException e1)
						{
							continue;
						}
						catch(AccessDeniedException e2)
						{
							continue;
						}
					}
					row.put("id", cr.getString("id", null));
					for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
					{
						String fCn = f.getCn();
						String column = f.getColumn();
						// dla multiple ucina _(id wpisu) z CN
						if (isMultiple())
						{
							fCn = fCn.substring(0, fCn.lastIndexOf("_"));
							if (!(f instanceof NonColumnField))
								column = column.substring(0, column.lastIndexOf("_"));
						}
	
						if (!(f instanceof NonColumnField) && cr.getValue(column, null) != null)
						{
							if (f.getType().equals(f.DATE))
							{
								if (cr.getValue(column, null) instanceof String)
								{
									DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
									try
									{
										Date date = df.parse(cr.getString(column, null));
										row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), date);
									}
									catch (ParseException e)
									{
										log.error(e.getMessage());
									}
								}
								else
									row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), cr.getDate(column, null));
							}
							else
								row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), cr.getValue(column, null));
						}
						if (f.getType().equals(pl.compan.docusafe.core.dockinds.field.Field.LINK))
						{
							String fieldLabel = ((LinkField) f).getLinkLabel();
							Document doc =  null;

							String label = null;
							if (fieldLabel.equals("document-title"))
							{
								doc = Document.find(Long.parseLong((String) row.get("id")), false);
								label = doc.getTitle();
							}
							else if (fieldLabel.equals("document-description"))
							{
								doc = Document.find(Long.parseLong((String) row.get("id")), false);
								label = doc.getDescription();
							}
							else if (fieldLabel.equals("link-value"))
								label =  ((LinkField)f).getLogicField().getLinkLabel();
							else if (fieldLabel.equals("column"))
								label =  cr.getString(((LinkField) f).getLabelColumn(), "-");
							else
							{
								FieldsManager fm = null;
								try {
									doc = Document.find(Long.parseLong((String) row.get("id")), false);
									fm = doc.getFieldsManager();
									label = (String)fm.getValue(fieldLabel);
								} catch (Exception e){
									doc = doc == null ? Document.find(Long.parseLong((String) row.get("id")), false) : doc;
									fm =  fm == null ? doc.getFieldsManager() : fm;
									label = fm.getValue(fieldLabel).toString();
								}
							}
							row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), new LinkValue(label, ((LinkField)f).getLogicField().createLink((String) row.get("id"))));
						}
					}
					ret.add(row);
				}
			}
		}
		catch (Exception a)
		{
			log.error(a.getMessage(), a);
			throw new EdmException(a);
		}
		// zapisywanie, czy wynik wyszukiwania by� pusty
		lastSearchResultEmpty = ret.isEmpty();
		return ret;
	}

	
	/**
	 * Metoda zast�puje sparametryzowane warto�ci z zapytania where odpowiadaj�cym im wartosciom przekazanym przez u�ytkownika
	 * UWAGA: W wolnym czasie warto zoptymalizowa� dzia�anie tej metody
	 * @param sqlParameterizedQuery sparametryzowana kwerenda
	 * @param whereExpsCompType mapa poszczeg�lnych zapyta� z klauzuli where wraz z warto�ciami dla znajduj�cych si� w nich parametr�w
	 * @param recordsToDisplay ilo�� rekord�w do wy�wietlenia
	 * @return
	 */
	private String getQuery(String sqlParameterizedQuery, Map<String, String> filtredColumns, Map<String, FieldData> filteringValues, Map<String, String> whereExpsCompType, Integer limit, Integer offset){
		String selectedColumns = sqlParameterizedQuery.substring(sqlParameterizedQuery.indexOf("SELECT")+6,sqlParameterizedQuery.indexOf("FROM")-1);
		String tableName = sqlParameterizedQuery.substring(sqlParameterizedQuery.indexOf("FROM")+4, sqlParameterizedQuery.indexOf("WHERE"));
		StringBuilder sqlQuery = new StringBuilder("SELECT " + selectedColumns + " FROM (SELECT ROW_NUMBER() OVER (ORDER BY id) as rowNum, " + selectedColumns + " FROM " + tableName + " WHERE ");
		String whereExpWithoutParam = "";
		int checkedExprs = 0;
		for(String whereExpr : whereExpsCompType.keySet()){
			++checkedExprs;
			whereExpWithoutParam += whereExpr.substring(0, whereExpr.indexOf(":"));
			whereExpWithoutParam += whereExpsCompType.get(whereExpr);
			if(checkedExprs < whereExpsCompType.keySet().size())
				whereExpWithoutParam += " AND ";
		}
		FieldData data = null;
		String WHERE = "";
		for(String filteredColumn : filtredColumns.keySet()){
			String filteringColumnCn = filtredColumns.get(filteredColumn);
			if(filteringValues.keySet().contains(filteringColumnCn)){
				data = filteringValues.get(filteringColumnCn);
				WHERE += filteredColumn + " : " + data.getStringData();
			}
		}
		sqlQuery.append(whereExpWithoutParam+") AS d WHERE d.rowNum > " + offset + " AND d.rowNum <= " + (limit+offset+1) + " AND " + whereExpWithoutParam);
		return sqlQuery.toString();
	}

	/**
	 * Zwraca map� p�l, kt�re maj� ustawion� flag� required
	 * @return
	 * @throws EdmException
	 */
	public Map<String,Field> getRequiredField() throws EdmException{
		Map<String, Field> results = com.beust.jcommander.internal.Maps.newHashMap();
		for(Field field : getFields()){
			if(field.getRequired()){
				results.put(field.getCn(), field);
			}
		}

		return results;
	}

	/**
	 * Waliduje pola pod k�tem wymagalno�ci
	 * @param maps
	 */
	public void validateField(Map<String, FieldData> values) throws EdmException {
		boolean newDictionaryValueIsNotEmpty = isDictionaryEmpty(values);
		List<String> requiredFieldInfo = Lists.newArrayList();
		for(Map.Entry<String, Field> required : getRequiredField().entrySet()){
			if(!values.containsKey(isMultiple() ? required.getKey().replace("_1","") : required.getKey())) {
				requiredFieldInfo.add(required.getKey().replaceFirst(getName().toUpperCase(Locale.ENGLISH) + "_", ""));
			}
		}

		if(!requiredFieldInfo.isEmpty()) {
			throw new EdmException("Pola + " + requiredFieldInfo.toString() + " s� wymagane!");
		}
	}


    /**
     * @param values
     * @return  -1 jesli @param values nie zawiera zadnych wartosci slownikowych skojarzonych z kluczem badz @param values jest puste, 0 jesli zapytanie do bazy nie by�o typu DML, b�d� id pierwszego dodanego do bazy rekordu
     * @throws EdmException
     */
	@Override
	public long add(Map<String, FieldData> values) throws EdmException
	{
		boolean newDictionaryValueIsNotEmpty = isDictionaryEmpty(values);

		long ret = -1;

		if (!newDictionaryValueIsNotEmpty)
			return ret;
		
		PreparedStatement ps = null;
		String tableName = "";
		pl.compan.docusafe.core.dockinds.field.Field f = getOldField();
//		(AbstractDictionaryField)field).getDockindInfo().getDockindFields()
		
		if (f instanceof DictionaryField)
		{
			tableName = ((DictionaryField) f).getTable();
			((DictionaryField) f).getDocumentKind().logic().addSpecialInsertDictionaryValues(getName(), values);
		}
		else if (f instanceof DocumentField)
		{
			tableName = ((DocumentField) f).getTable();
		}
		StringBuilder query = new StringBuilder("insert into ").append(tableName).append(" (");
		StringBuilder queryVals = new StringBuilder("values (");

		int size = values.size();
		if (size == 0)
			return ret;
		for (String cn : values.keySet())
		{
			// w s�owniku wielowarto�ciowym mamy field'a 'ID_1' i musimy
			// pomin�� go przy budowania
			// zapytania. Dla jednego przypadku musimy usunac ostatnie
			// przecinki
			if (!cn.equals("ID"))
			{
				query.append(cn.replaceFirst(getName().toUpperCase(Locale.ENGLISH) + "_", "")).append(",");
				queryVals.append("?,");
			}
		}
		query.deleteCharAt(query.lastIndexOf(","));
		queryVals.deleteCharAt(queryVals.lastIndexOf(","));
		if (DSApi.isOracleServer())
		{
			query.append(",id ");
			queryVals.append(", " + tableName + "_id.nextval ");
		}
		queryVals.append(")");
		query = query.append(") ");
		query = query.append(queryVals.toString());
		log.debug(query.toString());
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement(query);
			int psi = 1;
			for (String cn : values.keySet())
			{
				// w s�owniku wielowarto�ciowym mamy field'a 'ID_1' i musimy
				// pomin�� go przy budowaniu zapytania
				if (!cn.equals("ID"))
				{
					FieldData fd = values.get(cn);
					fd.getData(ps, psi);
					psi++;
				}
			}
			ret = ps.executeUpdate();

			if (ret != 0)
			{
				DSApi.context().closeStatement(ps);
				query = new StringBuilder("select id from ").append(tableName).append(" order by id desc");
				ps = DSApi.context().prepareStatement(query);
				rs = ps.executeQuery();
				rs.next();
				ret = rs.getLong(1);

			}
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
		}

		return ret;
	}

	/**
	 * Sprawdza czy mapa jest pusta
	 * @param values
	 * @return
	 */
	private boolean isDictionaryEmpty(Map<String, FieldData> values) {
		boolean newDictionaryValueIsNotEmpty = false;
		// Na poczatku sprawdzamy, czy nie jest to przypadek, w ktorym slownik
		// jest caly pusty (nie wpisano zadnej wartosci)
		for (FieldData fieldData : values.values())
		{
			if ((fieldData.getData() != null) && (!fieldData.getData().equals("")))
			{
				newDictionaryValueIsNotEmpty = true;
				break;
			}
		}
		return newDictionaryValueIsNotEmpty;
	}

	@Override
	public int remove(String id) throws EdmException
	{
		String tableName = ((DictionaryField) getOldField()).getTable();
		String multipleTableName = ((DictionaryField) getOldField()).getDocumentKind().getMultipleTableName();
		PreparedStatement ps = null;
		int ret = -1;

		StringBuilder query = new StringBuilder("delete from ").append(tableName).append(" where id=?");
		try
		{
			if (isMultiple())
			{
				String del = "delete from " + multipleTableName + " where field_cn like '" + getName() + "' and field_val like '" + id + "'";
				ps = DSApi.context().prepareStatement(del);
				ps.execute();
			}
			ps = DSApi.context().prepareStatement(query);
			ps.setLong(1, Long.valueOf(id));
			ret = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}

		return ret;
	}

	@Override
	public int update(Map<String, FieldData> values) throws EdmException
	{

        boolean isDocField = false;
		PreparedStatement ps = null;

		String tableName = "";

		if (getOldField() instanceof DictionaryField)
		{
			tableName = ((DictionaryField) getOldField()).getTable();
		}
		else if (getOldField() instanceof DocumentField)
		{
            isDocField = true;
			tableName = ((DocumentField) getOldField()).getTable();
		}

		if (cantUpdate)
			return -1;

		StringBuilder query = new StringBuilder("update ").append(tableName).append(" set ");

		int i = 1;
		if (!values.containsKey("id") && !values.containsKey("ID"))
			throw new NullPointerException("No id key found in values map");
		String sId = null;
		
		if (values.containsKey("id"))
			sId = "id";
		else
			sId = "ID";
		
		FieldData fieldId = values.get(sId);
		Object id = values.get(sId).getData();
		values.remove(sId);
		int size = values.size();
		for (String cn : values.keySet())
		{
			if (values.get(cn).getData() == null)
			{
				if (i == size)
				{
					int indexOf = query.toString().lastIndexOf(",");
					if ((indexOf + 2) == (query.length()))
						query = new StringBuilder(query.toString().substring(0, indexOf));
				}
				i++;
				continue;
			}
			query.append(cn.replace(name + "_", "")).append("=").append("?");
			if (i++ != size)
				query.append(", ");
		}
		
		 if (getOldField() instanceof DocumentField)
			query.append(" where DOCUMENT_ID=").append(id);
		else 
			query.append(" where id=").append(id);
		
		log.debug(query.toString());
		try
		{
			ps = DSApi.context().prepareStatement(query);
			int psi = 1;
			for (String cn : values.keySet())
			{
				if (values.get(cn).getData() == null)
				{
					i++;
					continue;
				}
				FieldData fd = values.get(cn);
				if (fd.getData() != null)
				{
					/*TODO TODO Proces ZADANIE - bardzo bardzo brzydki HARD fix 
					ale na szybko niema innej opcji do zrobienia dla s�ownik�w multi i w nich
					 zawartych USERS ENUMS �eby API NIE ZEPSUC bo musial bym przerabia� beny i logik� na  stringa
					  a niema czasu na to pozniej musze to przeniesc na logik� dokuemntu*/
					if (AvailabilityManager.isAvailable("PG.parametrization") && cn.equals("DELEGATE_USER_ID") && tableName.equals("DS_PG_ZADANIE_ODBIORCA")
							&& !fd.getStringData().isEmpty())
					{
						ps.setLong(psi, fd.getLongData());

					} else
					{
						fd.getData(ps, psi);
					}
				}
				psi++;
			}
			values.put(sId, fieldId);

            if (isDocField) {
                Long idL = null;
                if (id instanceof String)
                    idL = Long.parseLong((String) id);
                else if (id instanceof Long)
                    idL = (Long) id;
                else if (id instanceof Integer)
                    idL = (long) (Integer) id;

                if (idL != null) {
                    //prepare values
                    Map<String, Object> valuesDK = new HashMap<String, Object>();

                    for (Map.Entry<String, FieldData> entry : values.entrySet()) {
                        FieldData fd = entry.getValue();

                        if (fd != null && fd.getData() != null){
                            final String stringData = fd.getStringData();
                            if (StringUtils.isNotEmpty(stringData) && !stringData.equalsIgnoreCase("null"))
                                valuesDK.put(entry.getKey(), fd.getData());
                            else
                                valuesDK.put(entry.getKey(), null);
                        }
                    }

                    if (valuesDK.size() > 0) {
                        Document.find(idL).getDocumentKind().setWithHistory(idL, valuesDK, false);
                        return 1;
                    }
                }
            }

            return ps.executeUpdate();
		}
		catch (SQLException e)
		{
			log.error(this.name + ": " + e.getMessage(), e);
			return -1;
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}

	public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException
	{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		Map<String, Object> fieldsValues = new HashMap<String, Object>();
		if (fieldValues != null)
			fieldsValues.putAll(fieldValues);
		try
		{
			Map<String, FieldData> values = new HashMap<String, FieldData>();
			if (id != null && !"".equals(id))
			{
				FieldData idData = new FieldData();
				if (id.contains(";u:") || id.contains(";d:")){
					idData.setType(Field.Type.STRING);
					idData.setStringData(id);
				} else {
					idData.setType(Field.Type.INTEGER);
					idData.setData(Integer.parseInt(id));  
				}
				values.put("id", idData);	
					
				result = search(values, 1, 2, false, null); // szukanie w DB
			}

			for (Field field : getFields())
			{
				String fCn = field.getCn();
				// dla multiple ucina _(id wpisu) z CN
				if (isMultiple())
				{
					fCn = fCn.substring(0, fCn.lastIndexOf("_"));
				}
				if (field.getType().equals(Field.Type.ENUM_AUTOCOMPLETE) || field.getType().equals(Field.Type.ENUM) || field.getType().equals(Field.Type.ENUM_MULTIPLE))
				{
					for (pl.compan.docusafe.core.dockinds.field.Field f : (getOldField().getFields()))
					{

						if (fCn.toUpperCase().replaceFirst(getName().toUpperCase() + "_", "").toUpperCase().equals(f.getColumn().contains("_1") ? f.getColumn().replaceFirst("_1", "").toUpperCase() : f.getColumn().toUpperCase()))
						{
							Map<String, Object> enumVal = new HashMap<String, Object>();
							if (result.size() > 0 && result.get(0).containsKey(fCn))
							{
								fieldsValues.put(f.getCn(), result.get(0).get(fCn));
							}
							if (result.size() == 0)
							{
								if (f instanceof DataBaseEnumField) {
									enumVal.put(fCn, ((DataBaseEnumField)f).getEnumItemsForDwr(fieldsValues,id));
								} else {
									enumVal.put(fCn, f.getEnumItemsForDwr(fieldsValues));
								}
								result.add(enumVal);
							}
							else
							{
								if (f instanceof DataBaseEnumField) {
									result.get(0).put(fCn, ((DataBaseEnumField)f).getEnumItemsForDwr(fieldsValues,id));
								} else {
									result.get(0).put(fCn, f.getEnumItemsForDwr(fieldsValues));
								}
							}
						}
					}
				}
				else if (field.getType().equals(Field.Type.LINK) && values.containsKey("id") && !result.isEmpty() && result.get(0) == null)
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();
					String a = field.getCn().replace(getName() + "_", "");
					pl.compan.docusafe.core.dockinds.field.Field linkField = null;
					for (pl.compan.docusafe.core.dockinds.field.Field f : (oldField.getFields()))
					{
						if (f.getCn().equals(a))
						{
							linkField = f;
							break;
						}
					}

					Document doc = Document.find(Long.parseLong(id), false);
					String fieldLabel = ((LinkField) linkField).getLinkLabel();
					String label = null;
					if (fieldLabel.equals("document-title"))
						label = doc.getTitle();
					else if (fieldLabel.equals("document-description"))
						label = doc.getDescription();
					else if (fieldLabel.equals("link-value"))
						label =  ((LinkField) linkField).getLogicField().getLinkLabel();
					else 
						label = (String) doc.getFieldsManager().getValue(fieldLabel);

					if (result.size() == 0)
					{
						enumVal.put(fCn, new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
						result.add(enumVal);
					}
					//else
					//	result.get(0).put(fCn, new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));

				}
				else if(field.getType().equals(Field.Type.HTML)) {
					
					Map<String, Object> htmlVal = new HashMap<String, Object>();
					String cnWithoutPrefix = field.getCn().replace(getName() + "_", "");
					HTMLField htmlField = null;
					
					for (pl.compan.docusafe.core.dockinds.field.Field f : (oldField.getFields()))
					{
						if (f.getCn().equals(cnWithoutPrefix)) {
							htmlField = (HTMLField)f;
							break;
						}
					}
					
					if (result.size() == 0)
					{
						if (id != null)
							htmlVal.put(fCn, htmlField.getValue(id));
						else
							htmlVal.put(fCn, htmlField.getDefaultValue());
						result.add(htmlVal);
					}
					else
					{
						if (id != null)
							result.get(0).put(fCn, htmlField.getValue(id));
						else
							result.get(0).put(fCn, htmlField.getDefaultValue());
					}
				}
				else if(field.getType().equals(Field.Type.ATTACHMENT)){
					String cnWithoutPrefix = field.getCn().replace(getName() + "_", "");
					AttachmentField attachmentField = null;
					String attCn = null;
					for (pl.compan.docusafe.core.dockinds.field.Field f : (oldField.getFields()))
					{
						if (f.getColumn().toUpperCase().equals(cnWithoutPrefix)) {
							attachmentField = (AttachmentField)f;
							attCn = f.getCn().substring(0,f.getCn().lastIndexOf("_"));
							break;
						}
					}
					
					
					if (result.size() > 0)
					{
						Long attId = null;
						for(String fieldCn : result.get(0).keySet()){
							if(fieldCn.replace(getName() + "_", "").equals(attCn)){ 
								attId = ((BigDecimal) result.get(0).get(fieldCn)).longValue();
							}
						}
						if(attId != null){
							Attachment attachment = Attachment.find(attId);
							AttachmentRevision mostRecentRevision = attachment.getMostRecentRevision();
							if(attachment != null && mostRecentRevision != null) {
								AttachmentValue attVal = new AttachmentValue();
								attVal.setAttachmentId(attachment.getId());
								attVal.setTitle(attachment.getTitle());
								attVal.setUserSummary(DSUser.safeToFirstnameLastname(attachment.getAuthor()));
								attVal.setCtime(attachment.getCtime());
								mostRecentRevision = attachment.getMostRecentRevision();
								attVal.setRevisionId(mostRecentRevision.getId());
								attVal.setRevisionMime(mostRecentRevision.getMime());
								attVal.setMimeAcceptable(ViewServer.mimeAcceptable(attVal.getRevisionMime()));
								attVal.setMimeXml(ViewServer.mimeXml(attVal.getRevisionMime()));
								result.get(0).put(fCn, attVal);
							
							}else{
								
							}
						}
					}else{
						
					}
				}
				else if(field.getType().equals(Field.Type.BUTTON)) {
					String cnWithoutPrefix = field.getCn().replace(getName() + "_", "");
					ButtonField buttonField;
					ButtonValue buttonValue;
					
					for (pl.compan.docusafe.core.dockinds.field.Field f : (oldField.getFields()))
					{
						if (f.getCn().equals(cnWithoutPrefix)) {
							buttonField = (ButtonField)f;
							buttonValue = buttonField.getButtonForDWR();
							if(result.size() > 0) {
								result.get(0).put(fCn, buttonValue);
							} else {
								Map<String, Object> vals = new HashMap<String, Object>();
								vals.put(fCn, buttonValue);
								result.add(vals);
							}
							break;
						}
					}
					
					
				}
			}
			if (result.size() == 0)
				return new HashMap<String, Object>();
			return result.get(0);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return Collections.EMPTY_MAP;
		}
		
	}
	@Override
	public Map<String, Object> getValues(String id) throws EdmException
	{
		return getValues(id, null);
	}

	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	public List<Field> getFields()
	{
		return fields;
	}

	public void setVisibleFieldsCns(List<String> visibleFieldsCns)
	{
		this.visibleFieldsCns = visibleFieldsCns;
	}

	public List<String> getVisibleFieldsCns()
	{
		return visibleFieldsCns;
	}

	public void setSearchableFieldsCns(List<String> searchableFieldsCns)
	{
		this.searchableFieldsCns = searchableFieldsCns;
	}

	public List<String> getSearchableFieldsCns()
	{
		return searchableFieldsCns;
	}

	public void setPromptFieldsCns(List<String> promptFieldsCns)
	{
		this.promptFieldsCns = promptFieldsCns;
	}

	public List<String> getPromptFieldsCns()
	{
		return promptFieldsCns;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public List<Field> getPopUpFields()
	{
		return popUpFields;
	}

	public void setPopUpFields(List<Field> popUpFields)
	{
		this.popUpFields = popUpFields;
	}

	public void setShowDictionaryPopup(boolean showDictionaryPopup)
	{
		this.showDictionaryPopup = showDictionaryPopup;
	}

	public Boolean getShowDictionaryPopup()
	{
		return showDictionaryPopup;
	}

	public boolean isMultiple()
	{
		return multiple;
	}

	public void setMultiple(boolean multiple)
	{
		this.multiple = multiple;
	}

	public pl.compan.docusafe.core.dockinds.field.Field getOldField()
	{
		return oldField;
	}

	public void setOldField(pl.compan.docusafe.core.dockinds.field.Field oldField)
	{
		this.oldField = oldField;
	}

	public List<String> getPopUpButtons()
	{
		return popUpButtons;
	}

	public void setPopUpButtons(List<String> popUpButtons)
	{
		this.popUpButtons = popUpButtons;
	}

	public List<String> getDicButtons() {
		return dicButtons;
	}

	public void setDicButtons(List<String> dicButtons) {
        if(dicButtons!=null && !dicButtons.contains("showPopup")) this.showDictionaryPopup=false;
		this.dicButtons = dicButtons;
	}

	public void setTableView(boolean tableView)
	{
		this.tableView = tableView;
	}

	public boolean isTableView()
	{
		return tableView;
	}

	public boolean isDisabledSubmit()
	{
		return disabledSubmit;
	}

	public void setDisabledSubmit(boolean disabledSubmit)
	{
		this.disabledSubmit = disabledSubmit;
	}

	public boolean isCantUpdate()
	{
		return cantUpdate;
	}

	public void setCantUpdate(boolean cantUpdate)
	{
		this.cantUpdate = cantUpdate;
	}

	public boolean isOnlyPopUpCreate()
	{
		return onlyPopUpCreate;
	}

	public void setOnlyPopUpCreate(boolean onlyPopUpCreate)
	{
		this.onlyPopUpCreate = onlyPopUpCreate;
	}

	public boolean isRefreshMultipleDictionary() {
		return refreshMultipleDictionary;
	}

	public void setRefreshMultipleDictionary(boolean refreshMultipleDictionary) {
		this.refreshMultipleDictionary = refreshMultipleDictionary;
	}

	public void setJsPopupFunction(String jsPopupFunction) {
		this.jsPopupFunction = jsPopupFunction;
	}

	public String getJsPopupFunction() {
		return jsPopupFunction;
	}

	public void setAutohideMultipleDictionary(boolean autohideMultipleDictionary) {
		this.autohideMultipleDictionary = autohideMultipleDictionary;
	}

	public boolean isAutohideMultipleDictionary() {
		return autohideMultipleDictionary;
	}

    public boolean isAutocleanEmptyItems() {
        return autocleanEmptyItems;
    }

    public void setAutocleanEmptyItems(boolean autocleanEmptyItems) {
        this.autocleanEmptyItems = autocleanEmptyItems;
    }

    @Override
    public boolean filterBeforeGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dictionaryEntryId, Map<String, Object> documentValues) {
        return dictionaryFilter.filterBeforeGetValues(dictionaryFilteredFieldsValues, dictionaryEntryId, documentValues);
    }

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        dictionaryFilter.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId, fieldsValues, documentValues);
    }

    @Override
    public void filterBeforeUpdateValues(Map<String, FieldData> dictionaryFilteredFieldData) {
        dictionaryFilter.filterBeforeUpdateValues(dictionaryFilteredFieldData);
    }


}
