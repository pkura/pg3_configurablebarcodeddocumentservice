package pl.compan.docusafe.core.dockinds.dwr;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.directwebremoting.AjaxFilter;
import org.directwebremoting.AjaxFilterChain;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.WatchDog;

public class DwrAjaxFilter implements AjaxFilter {
	private static long alertTime = 3000;
	
	static {
		try {
			alertTime = Long.parseLong(Docusafe.getAdditionProperty("dwr_alert_time"));
		} catch(NumberFormatException e) {}
	}

	@Override
	public Object doFilter(Object obj, Method method, Object[] params,
			AjaxFilterChain chain) throws Exception {
		StringBuilder sb = new StringBuilder("DWR ");
		sb.append(method.getName());
		sb.append("(").append(Arrays.toString(params)).append(")");
		
		WatchDog watchDog = new WatchDog(sb.toString(), alertTime);
		Object flt = chain.doFilter(obj, method, params);
		watchDog.tick();
		
		return flt;
	}

}
