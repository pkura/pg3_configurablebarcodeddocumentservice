package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import org.jfree.util.Log;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindScheme;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.dockinds.field.DocumentUserDivisionSecondField;
import pl.compan.docusafe.core.dockinds.field.EmailMessageField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.dockinds.field.SchemeField;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.webwork.FormFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DockindFieldsProvider implements DwrFieldsProvider
{
	private final static Logger LOG = LoggerFactory.getLogger(DockindFieldsProvider.class);

	public Map<String, Object> fromRequest(HttpServletRequest request) throws EdmException
	{
		Map<String, Object> ret = Maps.newHashMap();

		String dockindCn = request.getParameter("documentKindCn");
		LOG.debug("dockindCn = {}", dockindCn);
		if (dockindCn == null)
			dockindCn = "invoice_ic";
		DocumentKind kind = DocumentKind.findByCn(dockindCn);
		List<Field> fields = kind.getFields();
		Map<String, Object> dictionariesValues = new HashMap<String, Object>();
		List<Object> recipientsUserDivisionSecondValues = new ArrayList<Object>();
		
		Long docId = null;
		try {
			if (request.getParameter("documentId") != null) {
				docId = Long.valueOf(request.getParameter("documentId"));
			}
		} catch (NumberFormatException e1) {
			LOG.error("B�ad parsowania docId: " +e1.getMessage(),e1);
		}
		
		Map<String, SchemeField> schemeFields = null;
		if (docId != null) {
			FieldsManager fieldsManager = Document.find(docId).getFieldsManager();
			DockindScheme dockindScheme = fieldsManager.getDocumentKind().getSheme(fieldsManager, (request.getParameter("activity") != null && !request.getParameter("activity").equals("")) ?  (String) request.getParameter("activity") : null);
			
			if (dockindScheme != null) {
				schemeFields = dockindScheme.getSchemeFields();
			}
		}
		
		for (Field field : fields)
		{
			String cn = field.getCn();
			
			if (schemeFields != null && schemeFields.get(cn) != null && schemeFields.get(cn).getDisabled()) {
				continue;
			}
			
			LOG.trace("trying field {}", cn);
			if (field.isMultiple() && request.getParameterValues("DWR_" + cn) != null && !field.isHidden())
			{
				if (field.getType().equals(Field.DICTIONARY) || field.getType().equals(Field.DOCUMENT_FIELD) || field.getType().equals(Field.DOCUMENT_USER_DIVISION_SECOND))
				{
					HashMap<String, String> fieldNamesTypes = new HashMap<String, String>();
					for (Field dicCn : field.getFields())
					{
						if (!(dicCn instanceof NonColumnField) && (!dicCn.isHidden() || dicCn.getCn().startsWith("ID_")))
						{
							// dodajemy nazwy fieldow s�ownika z ucietym
							// _(id) oraz typy fieldow
							fieldNamesTypes.put(dicCn.getColumn().substring(0, dicCn.getColumn().lastIndexOf("_")).toUpperCase(), dicCn.getType());
						}
						else if(dicCn instanceof NonColumnField && !dicCn.isHidden()){
							if(dicCn instanceof EmailMessageField && ((EmailMessageField)dicCn).getType().equals(EmailMessageField.ContentType.EMAIL_ATTACHMENT)){
								fieldNamesTypes.put(dicCn.getColumn().substring(0, dicCn.getColumn().lastIndexOf("_")).toUpperCase(), ((EmailMessageField)dicCn).getContentType().toString());
							}
						}
					}
					if (field.getType().equals(Field.DOCUMENT_FIELD))
						fieldNamesTypes.put("ID", Field.STRING);
					// wyciagamy wartosci fieldow i zapisujemy do
					// dictionaryAttributes dop�ki istnieja wpisy
					//pierwszy numer na li�cie slownika kt�ry moze istnie�
					Set<Long> dictionariesKeys = new HashSet<Long>();
					
					//iteruje po polach s�ownika, i sprawdzam o jakich _Liczba s� przekazane
					//I dodaje do powyzego dictionariesKeys
					for(String fieldName : fieldNamesTypes.keySet())
					{
						//Prefix pola do sprawdzenia, 
						String fieldPrefix= new StringBuffer().append(cn).append("_").append(fieldName.toUpperCase()).append("_").toString();
	
					    //iteruj� po elementach requesta
						for(String key : request.getParameterMap().keySet())
						{
							//znalazl pole s�ownikowe z rozszerzon� liczb�
							if(key.substring(0, key.lastIndexOf("_") + 1).equalsIgnoreCase(fieldPrefix))
							{
								String number = key.substring(key.lastIndexOf("_")+1, key.length());
								try{
									dictionariesKeys.add(Long.parseLong(number));
								}catch (NumberFormatException e) {
									LOG.error("b��d mapowania elementu s�ownika: {}", key );
								}
							}
						}
					}
					
					//iteruje po numerach konc�wek s�ownika przekazanych w requescie
					for(Long number : dictionariesKeys)
					{
						Map<String, FieldData> dictionaryAttributes = new HashMap<String, FieldData>();
						for (String a : fieldNamesTypes.keySet())
						{
							//sklejanie do cn pola s�ownika
							String fieldRequestName = new StringBuffer().append(cn).append("_").append(a).append("_").append(number).toString().toUpperCase();
							boolean isRequestParam = (Object) request.getParameter(fieldRequestName) != null;
							
							//LOG.debug("name: {},  request: {} ", fieldRequestName, (Object) request.getParameter(fieldRequestName));
							
							if (fieldNamesTypes.get(a).equals(Field.DATE) && isRequestParam)
							{
								Date date = DateUtils.parseJsDate(request.getParameter(fieldRequestName));
								dictionaryAttributes.put(a, new FieldData(FieldData.getFieldType(fieldNamesTypes.get(a)), date));
							}
							else if (fieldNamesTypes.get(a).equals(Field.TIMESTAMP) && isRequestParam)
							{
								Date date = DateUtils.parseJsDateTimeWithSeconds(request.getParameter(fieldRequestName));
								dictionaryAttributes.put(a, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.TIMESTAMP, date));
							}  
							else if (fieldNamesTypes.get(a).equals(Field.ATTACHMENT))
							{
								String value = request.getParameter(fieldRequestName); 
								MultiPartRequestWrapper multiReq = (MultiPartRequestWrapper) request;
								File file = multiReq.getFile(fieldRequestName);
								
								if(file != null) {
									AttachmentValue attValue = new AttachmentValue(); 
									FormFile formFile = new FormFile(file, file.getName(), MimetypesFileTypeMap.getInstance().getContentType(file));
									attValue.setFormFile(formFile);
									
									// je�li mamy value, to znaczy �e wgrywamy now� wersj� za��cznika
									if(value != null && !value.trim().isEmpty()) {
										attValue.setAttachmentId((Long) field.simpleCoerce(value));
									}
//									ret.put(cn, attValue);
									dictionaryAttributes.put(a, new FieldData(FieldData.getFieldType(fieldNamesTypes.get(a)), attValue));
									// ustawiamy na null, bo dostali�my plik a value oznacza�oby attachmentId ustawiony na null,
									// czyli usuni�cie za��cznika
									value = null;
								}
							}
							else if(fieldNamesTypes.get(a).equals(EmailMessageField.ContentType.EMAIL_ATTACHMENT)){
								String value = request.getParameter(fieldRequestName); 
								MultiPartRequestWrapper multiReq = (MultiPartRequestWrapper) request;
								File file = multiReq.getFile(fieldRequestName);
								
								if(file != null) {
									AttachmentValue attValue = new AttachmentValue(); 
									FormFile formFile = new FormFile(file, file.getName(), MimetypesFileTypeMap.getInstance().getContentType(file));
									attValue.setFormFile(formFile);
									
									// je�li mamy value, to znaczy �e wgrywamy now� wersj� za��cznika
									if(value != null && !value.trim().isEmpty()) {
										attValue.setAttachmentId((Long) field.simpleCoerce(value));
									}
//									ret.put(cn, attValue);
									dictionaryAttributes.put(a, new FieldData(FieldData.getFieldType(fieldNamesTypes.get(a)), attValue));
									// ustawiamy na null, bo dostali�my plik a value oznacza�oby attachmentId ustawiony na null,
									// czyli usuni�cie za��cznika
									value = null;
								}
							} else if (field.getType().equals(Field.DOCUMENT_USER_DIVISION_SECOND)) {
								StringBuilder name = new StringBuilder();
								name.append(field.getCn()).append("_ID");
								if (fieldRequestName.startsWith(name.toString())) {
									recipientsUserDivisionSecondValues.add(request.getParameter(fieldRequestName));
									ret.put(field.getCn(), recipientsUserDivisionSecondValues);
								}
							}
							else
								dictionaryAttributes.put(a, new FieldData(FieldData.getFieldType(fieldNamesTypes.get(a)), (Object) request.getParameter(fieldRequestName)));			
						}
						LOG.debug("DICTIONARY MULTI attributes size: " + dictionaryAttributes.size() + " o cn " + cn);

						//TODO: dictionaryAttributes moze by� nullem jesli usuniem element ze �rodka listy
						if(dictionaryAttributes != null && !dictionaryAttributes.isEmpty())
							dictionariesValues.put(field.getCn() + "_" + number, dictionaryAttributes);
					}
				}
				else
				{
					String[] par = request.getParameterValues("DWR_" + cn);
					if (par.length == 1)
						par = par[0].split(",");
					List<Object> values = new ArrayList<Object>();
					for (String str : Arrays.asList(par))
					{
						values.add(field.simpleCoerce(str));
					}
					ret.put(cn, values);
				}
			}
			else
			{
				String value = request.getParameter("DWR_" + cn);
				// to bedzie pozniej dla filda DICTIONARY
				if (field.getType().equals(Field.DOCUMENT_PERSON))
				{
					Map<String, String> personAttributes = new HashMap<String, String>();
					for (Field dicCn : field.getFields())
					{
						if (request.getParameter(cn.toUpperCase() + "_" + dicCn.getColumn().toUpperCase()) != null)
						{
							Log.info("wartosc ustawiona dla sender i pola: " + dicCn.getColumn());
							personAttributes.put(dicCn.getColumn().toUpperCase(), request.getParameter(cn.toUpperCase() + "_" + dicCn.getColumn().toUpperCase()));
						}
					}
					if (!personAttributes.isEmpty() && allIsNotEmptyStrings(personAttributes))
					{
						ret.put(cn.toUpperCase() + "_PARAM", personAttributes);
					}
				}
				else if (field.getType().equals(Field.DICTIONARY) && (!field.isHidden() || field.isSaveHidden()))
				{
					Map<String, FieldData> dictionaryAttributes = new HashMap<String, FieldData>();
					boolean onlyOne = false;
					for (Field dicCn : field.getFields())
					{
						String param = request.getParameter(cn.toUpperCase() + "_" + dicCn.getColumn().toUpperCase());
						if (param != null && param.length() > 0)
						{
							onlyOne = true;
							dictionaryAttributes.put(dicCn.getColumn().toUpperCase(), new FieldData(FieldData.getFieldType(dicCn.getType()), (Object) param));
						}
					}
					if (onlyOne)
						ret.put(field.getCn().toUpperCase() + "_DICTIONARY", dictionaryAttributes);
				}
				else if (field.getType().equals(Field.BOOL))
				{
                    if (isNotHiddenField(schemeFields, field)) {
						ret.put(cn, field.simpleCoerce(value));
                    }
				}
				else if (field.getType().equals(Field.ATTACHMENT))
				{
					if(field.isMultiple()) {
						// multiple dla za��cznika oznacza �e ju� zosta� wys�any na server
						DwrAttachmentStorage storage = DwrAttachmentStorage.getInstance();
						List<DwrFileItem> attItems = storage.getFiles(request.getSession(false), DwrUtils.getCnWithoutPrefix(field.getCn()));
						if(attItems != null) {
							ret.put(cn, attItems);
						} else {
							ret.put(cn, null);
						}
						
					} else {
						MultiPartRequestWrapper multiReq = (MultiPartRequestWrapper) request;
						File file = multiReq.getFile("DWR_" + cn);
						
						if(file != null) {
							AttachmentValue attValue = new AttachmentValue();
							attValue.setFormFile((FormFile) field.simpleCoerce(file));
							
							// je�li mamy value, to znaczy �e wgrywamy now� wersj� za��cznika
							if(value != null && !value.trim().isEmpty()) {
								attValue.setAttachmentId((Long) field.simpleCoerce(value));
							}
							ret.put(cn, attValue);
							
							// ustawiamy na null, bo dostali�my plik a value oznacza�oby attachmentId ustawiony na null,
							// czyli usuni�cie za��cznika
							value = null;
						}
					}
				}
				else if(field.getType().equals(Field.EMAIL_MESSAGE)){
					if(((EmailMessageField)field).getType().equals(EmailMessageField.ContentType.EMAIL_ATTACHMENT)){
						String sqlQuery = "SELECT message_id FROM " + kind.getTablename() + " WHERE DOCUMENT_ID = " + docId;
					}
				}
				if (value != null )//&& !field.getType().equals(Field.DOCUMENT_PERSON))
					ret.put(cn, field.simpleCoerce(value));
			}
		}
		if (!dictionariesValues.isEmpty())
		{
			ret.put("M_DICT_VALUES", dictionariesValues);
		}
		//LOG.debug("=== RESULTS === \n {}", ret);

		return ret;
	}

    private boolean isNotHiddenField(Map<String, SchemeField> schemeFields, Field field) {
        return (schemeFields != null && schemeFields.get(field.getCn()) != null && !Boolean.TRUE.equals(schemeFields.get(field.getCn()).isHidden()))
                || (!field.isHidden() && (schemeFields == null || schemeFields.get(field.getCn()) == null));
    }

    private boolean allIsNotEmptyStrings(Map<String, String> personAttributes)
	{
		Map<String, String> personAttributesTmp = new HashMap(personAttributes);
		String tmp = "";
		for (String key : personAttributes.keySet())
		{
			if (personAttributes.get(key).equals(""))
				personAttributesTmp.remove(key);
		}
		if (personAttributesTmp.size() > 0)
			return true;
		return false;
	}
}
