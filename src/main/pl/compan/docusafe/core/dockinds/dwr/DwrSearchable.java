package pl.compan.docusafe.core.dockinds.dwr;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;

public interface DwrSearchable {

	String NULLABLE = "is_null";
	/**
	 * Wyszukuje po wszystkich polach {@link Dictionary#searchableFieldsCns}
	 * 
	 * @param values
	 *            Mapa <code>Cn pola -> warto�� pola</code>
	 * @return
	 */
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException;

	public long add(Map<String, FieldData> values) throws EdmException;

	public int remove(String id) throws EdmException;
	
	public int update(Map<String, FieldData> values) throws EdmException;
	
	public Map<String, Object> getValues(String id) throws EdmException;
	
	public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException;
	
}