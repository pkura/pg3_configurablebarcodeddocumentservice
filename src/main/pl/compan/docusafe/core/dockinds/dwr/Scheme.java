package pl.compan.docusafe.core.dockinds.dwr;

import java.util.List;

public class Scheme {
	private String cn;
	private List<Field> fields;
	
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getCn() {
		return cn;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public List<Field> getFields() {
		return fields;
	}
}
