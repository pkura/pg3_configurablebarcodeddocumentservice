package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class EnumMulipleValueHandler implements ValueHandler
{

	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception
	{
		List<String> oldSelectedCns = new ArrayList<String>();
		if (((EnumValues) dwrFacade.getDwrFieldsValues().get(field.getCn())).getSelectedOptions().size() != 0)
			for (String sel : ((EnumValues) dwrFacade.getDwrFieldsValues().get(field.getCn())).getSelectedOptions())
				oldSelectedCns.add(sel);
		List<String> newSelectedCns = new ArrayList<String>();
		for (String sel : values.get(field.getCn()).getEnumValuesData().getSelectedOptions())
			newSelectedCns.add(sel);
		if (oldSelectedCns.size() == newSelectedCns.size() && oldSelectedCns.containsAll(newSelectedCns))
		{
			dwrFacade.getFieldValues().put(oldCn, oldSelectedCns);
		}
		else
		{
			dwrFacade.getFieldValues().put(oldCn, newSelectedCns);
			dwrFacade.reloadValuesSaveSession();
		}
	}

}
