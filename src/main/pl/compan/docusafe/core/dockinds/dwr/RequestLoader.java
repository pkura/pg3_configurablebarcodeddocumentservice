package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class RequestLoader {
    private final static Logger LOG = LoggerFactory.getLogger(DockindFieldsProvider.class);

    private final static Map<String, DwrFieldsProvider> providers;

    static {
        Map<String, DwrFieldsProvider> providerMap = Maps.newHashMap();
        providerMap.put("DOCKIND", new DockindFieldsProvider());
        providerMap.put("DOCKIND_FROM_JSON", new DockindFieldsJsonProvider());
        providers = Collections.unmodifiableMap(providerMap);
    }

    public static Map<String, Object> loadKeysFromWebWorkRequest() throws EdmException {
        return loadKeysFromRequest(ServletActionContext.getRequest());
    }

    public static Map<String, Object> loadKeysFromRequest(HttpServletRequest request) throws EdmException {
        Preconditions.checkNotNull(request, "request cannot be null");
        //TODO ewentualne mechanizmy do wczytywania z wielu �r�de�
        String context = request.getParameter("DWR_CONTEXT");
        LOG.info("dwr_context = {}",context);
        if(context == null) context = "DOCKIND";
        return providers.get(context).fromRequest(request);
    }
}
