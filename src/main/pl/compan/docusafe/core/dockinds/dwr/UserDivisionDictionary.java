package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class UserDivisionDictionary extends DwrDictionaryBase
{
	private static final Logger log = LoggerFactory.getLogger(UserDivisionDictionary.class);

	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();

		StringBuilder query = new StringBuilder();
		FieldData idFieldData = values.get("id");
        DocumentUserDivisionSecondField oldField = (DocumentUserDivisionSecondField)getOldField();
		boolean sen = oldField.isSenderField();
        String index = "";
        if (oldField.isMultiple())
        {
              for (String lp : values.keySet())
              {
                 index = lp.substring(lp.lastIndexOf("_")+1);
                 if (index.length() > 0)
                 {
                     try{
                        Integer.parseInt(index);
                     }
                     catch (Exception e)
                     {
                         index = "";
                     }
                 }
              }
        }
		String pref = getOldField().getCn();
		if (idFieldData != null && !"".equals(idFieldData.getStringData())) // zwracamy jeden wpis
		{
			Map<String, Object> row = Maps.newLinkedHashMap();
			String id = values.get("id").getStringData();
			String userName = null, userDivisionGUID = null;
			DSUser user =null;
			DSDivision userDivision = null;
            String _firstname = (oldField.isMultiple() && index.length() > 0) ? "_firstname_"+index : "_firstname";
            String _lastname = (oldField.isMultiple() && index.length() > 0) ? "_lastname_"+index : "_lastname";
            String _division = (oldField.isMultiple() && index.length() > 0) ? "_division_"+index : "_division";
            String _email = (oldField.isMultiple() && index.length() > 0) ? "_email_"+index : "_email";
			if (id.contains("u:") || id.contains("d:"))
			{
				String[] ids = id.split(";");
				if (ids[0].contains("u:"))
				{
					userName = ids[0].split(":")[1];
					user = DSUser.findByUsername(userName);
				}
				if (ids[1].contains("d:"))
				{
					if(ids[1].length() > 2) {
						userDivisionGUID = ids[1].split(":")[1];
						userDivision = DSDivision.find(userDivisionGUID);
					}
				}

				row.put("id", id);

				if (user!=null && user.getFirstname() != null)
					row.put( pref + _firstname.toUpperCase(), user.getFirstname());
				if (user!=null && user.getLastname() != null)
					row.put(pref + _lastname.toUpperCase(), user.getLastname());
				if (userDivision != null)
					row.put(pref + _division.toUpperCase(), userDivision.getName());
				if (user!=null && user.getEmail() != null)
					row.put(pref + _email.toUpperCase(), user.getEmail());
				
			}
			else
			{
				
				Long recipientId = Long.valueOf(id);
				Person recipient = Person.find(recipientId.longValue());
				String divisionName = "";
				
				row.put("id", recipient.getId());
				if (recipient.getFirstname() != null)
					row.put(pref + _firstname.toUpperCase(), recipient.getFirstname());
				if (recipient.getLastname() != null)
					row.put(pref + _lastname.toUpperCase(), recipient.getLastname());
				if (recipient.getOrganizationDivision() != null)
					divisionName = recipient.getOrganizationDivision();

				row.put(pref + _division.toUpperCase(), divisionName);
				
				if (recipient.getEmail() != null)
					row.put(pref + _email.toUpperCase(), recipient.getEmail());
			}
			ret.add(row);
		}
		else
		{
			String firstName = "", lastName = "", division = "",email ="";

			if (sen)
			{
				firstName = "SENDER_HERE_FIRSTNAME";
				lastName = "SENDER_HERE_LASTNAME";
				division = "SENDER_HERE_DIVISION";
				email	= "SENDER_HERE_EMAIL";
			}
			else
			{
				firstName = (oldField.isMultiple() && index.length() > 0) ? oldField.getCn()+"_FIRSTNAME_"+index : oldField.getCn()+"_FIRSTNAME";
				lastName = (oldField.isMultiple() && index.length() > 0) ? oldField.getCn()+"_LASTNAME_"+index : oldField.getCn()+"_LASTNAME";
				division = (oldField.isMultiple() && index.length() > 0) ? oldField.getCn()+"_DIVISION_"+index : oldField.getCn()+"_DIVISION";
				email = 	(oldField.isMultiple() && index.length() > 0) ? oldField.getCn()+"_EMAIL_"+index : oldField.getCn()+"_EMAIL";
			}
			boolean hasTwoParameters = false;
			if (values.get(firstName) != null || values.get(lastName) != null) {
				query.append("select u.*, di.name divisionName, di.GUID divisionGUID, di.CODE divisionCode from DS_USER u, DS_DIVISION di, DS_USER_TO_DIVISION userdi where u.ID = userdi.USER_ID and di.ID = userdi.DIVISION_ID and u.deleted = ? and");
				hasTwoParameters = true;
			}
			else
				query.append("select di.name divisionName, di.GUID divisionGUID, di.CODE divisionCode from DS_DIVISION di where");

			query.append(" (di.DIVISIONTYPE = 'division' or di.DIVISIONTYPE = 'position') AND di.HIDDEN = ? and di.GUID <> '").append(DSDivision.ROOT_GUID).append("'");

			for (String name : values.keySet())
			{
				if (name.equals(firstName) && values.get(name) != null)
					query.append(" and upper(u.FIRSTNAME) like '%").append(values.get(name).getStringData().toUpperCase()).append("%'");
				if (name.equals(lastName) && values.get(name) != null)
					query.append(" and upper(u.LASTNAME) like '%").append(values.get(name).getStringData().toUpperCase()).append("%'");
				if (name.equals(email) && values.get(name) != null)
						query.append(" and upper(u.EMAIL) like '%").append(values.get(name).getStringData().toUpperCase()).append("%'");
				if (name.equals(division) && values.get(name) != null)
					query.append(" and ( upper(di.NAME) like '%").append(values.get(name).getStringData().toUpperCase()).append("%'").
					append(" or upper(di.code) like '%").append(values.get(name).getStringData().toUpperCase()).append("%' )");
			}

			ResultSet rs = null;
			PreparedStatement ps = null;

			try
			{
				ps = DSApi.context().prepareStatement(query);
				ps.setBoolean(1, false);
				if (hasTwoParameters) {
					ps.setBoolean(2, false);
				}
				log.debug("Query from UserDivisionDictionary: " + query);
				rs = ps.executeQuery();
				while (rs.next())
				{
					Map<String, Object> row = Maps.newLinkedHashMap();
					String id = "";
					if ((values.get(firstName) != null || values.get(lastName) != null) && rs.getString("name") != null)
					{
						id += "u:";
						id += rs.getString("name");
					}
					id += ";";
					if (rs.getString("divisionGUID") != null)
					{
						id += "d:";
						id += rs.getString("divisionGUID");
					}

					row.put("id", id);
					if (values.get(firstName) != null || values.get(lastName) != null)
					{
						row.put(firstName, rs.getString("firstname"));
						row.put(lastName, rs.getString("lastname")); 
						
					}
					if (values.get(email) != null && rs.getString("email")!=null)
						row.put(email, rs.getString("email"));
					
					if(DSDivision.isCodeAsNamePrefix() && rs.getString("divisionCode") != null)
						row.put(division,rs.getString("divisionCode")+" "+ rs.getString("divisionName"));
					else
						row.put(division, rs.getString("divisionName"));
					
					if (AvailabilityManager.isAvailable("userDivisionDictionary.search.showUniqueUsers")) {
						if (!isUserInRet(ret, row)) {
							ret.add(row);
						}
					} else {
						ret.add(row);
					}
				}
			}
			catch (SQLException e)
			{
				log.debug(e.getMessage(), e);
			}

			DSApi.context().closeStatement(ps);
			List<Map<String, Object>> retOut = Lists.newArrayList();
			int maxRe = offset + limit < ret.size() ? offset + limit : ret.size();
			for (int i = offset; i < maxRe; i++)
			{
				retOut.add(ret.get(i));
			}
			return retOut;
			
		}
		return ret;
	}
	
	private boolean isUserInRet(List<Map<String, Object>> ret, Map<String, Object> row) {
		String userName;
		String userNameFromRet;
		userName = getUserNameFromRow(row);
		if (StringUtils.isNotBlank(userName)) {
			for (Map<String, Object> rowInRet : ret) {
				userNameFromRet = getUserNameFromRow(rowInRet);
				if (userName.equals(userNameFromRet)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private String getUserNameFromRow(Map<String, Object> row) {
		int beginIndex = 2;
		String userName = null;
		if (row.containsKey("id") && StringUtils.isNotBlank((String) row.get("id"))) {
			String id = (String) row.get("id");
			int endIndex = id.lastIndexOf(";");
			userName = id.substring(beginIndex, endIndex);
		}
		return userName;
	}
	
    @Override
    public boolean filterBeforeGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dictionaryEntryId, Map<String, Object> documentValues) {
        boolean ret = super.filterBeforeGetValues(dictionaryFilteredFieldsValues, dictionaryEntryId, documentValues);
        // resetujemy
        if("-1".equals(dictionaryEntryId)) {
            Field oldField = getOldField();
            String oldFieldCn = oldField.getCn();
            dictionaryFilteredFieldsValues.put(oldFieldCn + "_FIRSTNAME", "");
            dictionaryFilteredFieldsValues.put(oldFieldCn + "_LASTNAME", "");
            dictionaryFilteredFieldsValues.put(oldFieldCn + "_DIVISION", "");
            dictionaryFilteredFieldsValues.put(oldFieldCn + "_EMAIL", "");
            return false;
        }

        return ret;
    }

    public long add(Map<String, FieldData> values)
	{
		return -1;
	}

	public int remove(String id)
	{
		return -1;
	}

	public int update(Map<String, FieldData> values)
	{
		return -1;
	}

}
