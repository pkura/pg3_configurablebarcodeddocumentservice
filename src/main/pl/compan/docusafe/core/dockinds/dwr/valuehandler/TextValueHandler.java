package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class TextValueHandler implements ValueHandler
{

	@Override
	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception
	{
		Object oldValue = dwrFacade.getDwrFieldsValues().get(field.getCn());
		//wczesniej bylo:
		//if ((oldValue != null || oldValue == null) && values.get(field.getCn()).getData() == null) {
		if ( values.get(field.getCn()) != null &&  values.get(field.getCn()).getData() == null ) { 
			// TODO bylo: if (oldValue != null || oldValue == null)
			dwrFacade.getFieldValues().put(oldCn, null);
			return;
		}
		String newValue = values.get(field.getCn()) != null ? values.get(field.getCn()).getStringData().trim() : null;
		if ((oldValue != null && !oldValue.equals(newValue)) || (oldValue == null && newValue != null))
			dwrFacade.getFieldValues().put(oldCn, newValue);
	}

}
