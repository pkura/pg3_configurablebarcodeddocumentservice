package pl.compan.docusafe.core.dockinds.dwr;

import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class DwrSelectedValues {

    public static final String SELECTED_FIELDS_VALUES = "SELECTED_FIELDS_VALUES";

    /**
     * @param dictionaryName Nazwa s�ownika
     * @param dictionaryId Id dokumentu znajduj�cego si� w s�owniku
     * @param selectedValues Wszystkie wybrane warto�ci dla wszystkich p�l i ze wszystkich s�ownik�w
     * @return Warto�ci ze s�ownika dla dokumnetu
     */
    public static Map<String,Object> getValuesForDictionaryDocument(String dictionaryName, String dictionaryId, Map<String, FieldData> selectedValues){
        if (selectedValues!=null){

            FieldData dwrFieldData = getDictionaryFieldData(dictionaryName, selectedValues);
            if (dwrFieldData!=null && dwrFieldData.getType() == Field.Type.DICTIONARY){
                Map<String, FieldData> dsgValues = dwrFieldData.getDictionaryData();

                String docNumSuffix = getDictionaryElementNum(dsgValues, dictionaryId);
                if (docNumSuffix!=null)
                    return getValuesForDsg(dsgValues, docNumSuffix);
            }
        }
        return new HashMap<String, Object>();
    }

    /**
     * @param dictionaryName Nazwa s�ownika
     * @param selectedValues Wszystkie wybrane warto�ci dla wszystkich p�l i ze wszystkich s�ownik�w
     * @return S�ownik o szukanej nazwie <br></br> lub null
     */
    private static FieldData getDictionaryFieldData(String dictionaryName, Map<String, FieldData> selectedValues){
        return selectedValues.get("DWR_"+dictionaryName);
//        Set<Map.Entry<String,FieldData>> dwrGroupsEntries = selectedValues.entrySet();
//        for(Map.Entry<String, FieldData> dwrGroup : dwrGroupsEntries){
//            String dwrGroupKey = dwrGroup.getKey();
//            if (dwrGroupKey.startsWith("DWR_"+dictionaryName)){
//                FieldData dwrGroupFieldData = dwrGroup.getValue();
//                return dwrGroupFieldData;
//            }
//        }
//        return null;
    }

    /**
     * @param values Zbi�t wszystkich warto�ci w s�owniku / Zbi�r warto�ci wszystkich p�l dla wszystkich dokument�w w jakim� s�owniku
     * @param dictionaryId Id dokumentu znajduj�cego si� w s�owniku
     * @return Numer dokumnetu w s�owniku <br></br> lub null
     */
    private static String getDictionaryElementNum(Map<String, FieldData> values, String dictionaryId){
        Long num = DwrUtils.extractNumFromDictionary(values,dictionaryId);
        return num != null ? Long.toString(num) : null;
    }

    //wybranie wartosci dla danego dokumentu/wpisu/wiersza

    /**
     * @param dsgValues Zbi�t wszystkich warto�ci w s�owniku / Zbi�r warto�ci wszystkich p�l dla wszystkich dokument�w w jakim� s�owniku
     * @param docNumSuffix Numer okre�laj�cy dokument w s�owniku
     * @return Warto�ci ze s�ownika dla dokumnetu
     */
    private static Map<String, Object> getValuesForDsg(Map<String, FieldData> dsgValues, String docNumSuffix) {
        Set<Map.Entry<String,FieldData>> dsgValuesSet = dsgValues.entrySet();
        Map<String, Object> fieldsValues = new HashMap<String, Object>();
        for (Map.Entry<String, FieldData> dsg : dsgValuesSet){
            String dsgKey = dsg.getKey();

            if (dsgKey.endsWith("_"+docNumSuffix)){
                Object dsgData = dsg.getValue().getData();
                fieldsValues.put(dsgKey.substring(0,dsgKey.lastIndexOf('_')),dsgData);
            }
        }
        return fieldsValues;
    }

    public static Map<String, FieldData> copyRefValues(Map<String, FieldData> values) {
        Map<String, FieldData> copy = new HashMap<String, FieldData>();
        for (Map.Entry<String,FieldData> entry : values.entrySet())
            copy.put(entry.getKey(), entry.getValue());
        return copy;
    }

    /**
     * @param dictionaryName Nazwa s�ownika
     * @param selectedValues Wszystkie wybrane warto�ci dla wszystkich p�l i ze wszystkich s�ownik�w
     * @return Wszystkie id-ki dokument�w w szukanym s�owniku
     */
    public static List<String> getDictionaryIds(String dictionaryName, Map<String, FieldData> selectedValues){
        List<String> ids = new ArrayList<String> ();
        if (selectedValues!=null){

            FieldData dwrFieldData = getDictionaryFieldData(dictionaryName, selectedValues);
            if (dwrFieldData!=null && dwrFieldData.getType() == Field.Type.DICTIONARY){
                Map<String, FieldData> dsgValues = dwrFieldData.getDictionaryData();

                for (Map.Entry<String, FieldData> dsg : dsgValues.entrySet()){
                    String dsgKey = dsg.getKey();
                    FieldData dsgFieldData = dsg.getValue();

                    if (dsgKey.contains("_ID_") && dsgFieldData.getData() != null) {
                        String dsgId = null;
                        switch (dsgFieldData.getType()) {
                            case LONG:
                                dsgId = Long.toString(dsgFieldData.getLongData());
                                break;
                            case INTEGER:
                                dsgId = Integer.toString(dsgFieldData.getIntegerData());
                                break;
                            case STRING:
                                dsgId = dsgFieldData.getStringData();
                                break;
                        }
                        if (dsgId!=null)
                            ids.add(dsgId);
                    }
                }
            }
        }
        return ids;
    }

}
