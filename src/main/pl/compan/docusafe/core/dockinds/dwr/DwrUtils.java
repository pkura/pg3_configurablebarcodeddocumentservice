package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import system.NotImplementedException;

import javax.validation.UnexpectedTypeException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

public class DwrUtils {

	public static final String DWR_PREFIX = "DWR_";

    public enum CalcOperation {
		ADD,SUBTRACT,MULTIPLY,DIVIDE
	}
	
	protected static Logger log = LoggerFactory.getLogger(DwrUtils.class);
	
	private DwrUtils() {
	}
	
	/**
	 * Zwraca pole zmienione przez u�ytkownika, tj.: takie ktore ma flag� <code>sender</code> - ta flaga jest ustawiana na formatce, je�li u�ytkownik
	 * zmieni� warto�� pola (albo klikn�� na przycisk DWR)
	 * @param values
	 * @return Pole <b>tylko do odczytu</b>, kt�re zosta�o zmienione przez u�ytkownika lub <code>null</code> gdy formatka zosta�a wyslana bez zmiany pola.
	 */
	public static Map.Entry<String, FieldData> getSenderField(Map<String, FieldData> values) {
		for(Map.Entry<String, FieldData> entry : values.entrySet()) {
			FieldData fieldData = entry.getValue();
			if(fieldData != null && fieldData.isSender()) {
				return new SimpleImmutableEntry<String, FieldData>(entry);
			}
		}
		
		return null;
	}

    /**
     * sprawdza, czy kt�rekolwiek z podanych p�l s� oznaczone jako sender
     * @param values
     * @param fieldCns
     * @return true, je�li kt�rekolwiek z podanych p�l zosta�o zmienione przez u�ytkownika
     */
	public static boolean isSenderField(Map<String, FieldData> values, String... fieldCns) {
        return isSenderField(false, values,fieldCns);
	}
	public static boolean isSenderField(boolean addPrefixToFieldCn, Map<String, FieldData> values, String... fieldCns) {
		for(String fieldCn : fieldCns) {
            String fieldCnWithDwrPrefix = addPrefixToFieldCn ? dwr(fieldCn) : fieldCn;
            if (values.get(fieldCnWithDwrPrefix) != null && values.get(fieldCnWithDwrPrefix).isSender()) {
                return true;
            }
		}

		return false;
	}

    /**
     * Zwraca pole z prefiksem dwr
     * @param fieldCn
     * @return
     */
    public static String getCnWithPrefix(String fieldCn) {
        checkNotNull(fieldCn);
        if(fieldCn.startsWith(DWR_PREFIX)) {
            return fieldCn;
        } else {
            return DWR_PREFIX + fieldCn;
        }
    }

    /**
     * Usuwa przedrostek DWR_ z nazwy pola
     * @param fieldCn Nazwa pola z przedrostkiem (nie mo�e by� <code>null</code>)
     * @return Nazwa pola bez przedrostka DWR_
     */
    public static String getCnWithoutPrefix(String fieldCn) {
        checkNotNull(fieldCn);
        return fieldCn.replaceFirst("^DWR_", "");
    }

    /**
	 * Zwraca warto�� pola jako string. Dla p�l enum zwraca zaznaczon� opcj�
	 * @param values
	 * @param fieldCn
	 * @return <ul>
	 * <li>Warto�� jako String</li>
	 * <li>Pusty string, gdy pole puste lub <code>null</code></li>
	 * <li><code>null</code> gdy nie ma pola w <code>values</code></li>
	 * </ul>
	 */
	public static String getStringValue(Map<String, FieldData> values, String fieldCn) {
		if(!values.containsKey(fieldCn)) {
			return null;
		}
		FieldData fieldData = values.get(fieldCn);
		switch(fieldData.getType()) {
		case HTML:
			return fieldData.getHtmlData();
		case HTML_EDITOR:
			HtmlEditorValue val = fieldData.getHtmlEditorData();
			if(val == null)
				return "";
			return val.getHtml();
		case STRING:
        case TEXTAREA:
			return fieldData.getStringData();
		case ENUM:
			EnumValues enumVals = fieldData.getEnumValuesData();
			if(enumVals == null) {
				return "";
			}
			return enumVals.getSelectedId();
		default:
			throw new UnsupportedOperationException(fieldData.getType().toString());
		}
	}

    /**
     * Zwraca liczbow� warto�� pola
     * @param values
     * @param fieldCn
     * @return Warto�� skonwertowan� do <code>Long</code> lub <code>null</code> gdy nie da si� skonwertowa� warto�ci
     * na liczb� lub gdy nie ma pola w <code>values</code>
     */
    public static Long getLongValue(Map<String, FieldData> values, String fieldCn) {
        String stringVal = getStringValue(values, fieldCn);
        if(StringUtils.isEmpty(stringVal))
            return null;

        Long longVal = null;
        try {
            longVal = Long.parseLong(stringVal);
        } catch(NumberFormatException e) {

        }

        return longVal;
    }

    /**
	 * Metoda zwraca map� pola s�ownikowego, je�li istnieje takie pole i jest polem s�ownikowym.<br/>
	 * W przeciwnym wypadku zwraca null'a.
	 * @param values
	 * @param fieldCn - cn pola s�ownikowego, np. CONTRACTOR, DSG_CENTRUM_KOSZTOW_FAKTURY
	 * @return map� pola s�ownikowego Map<String, FieldData> lub null
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, FieldData> getDictionaryValue(Map<String, FieldData> values, String fieldCn) {
		return (Map<String, FieldData>)getDictionaryValue(values, fieldCn, new String[0]);
	}
	
	/**
	 * Metoda zwraca pole lub map� z wybranymi polami s�ownika, je�li istnieje s�ownik o podanym cn.<br/>
	 * W przeciwnym wypadku zwraca null'a.
	 * @param values
	 * @param fieldCn - cn pola s�ownikowego, np. CONTRACTOR, DSG_CENTRUM_KOSZTOW_FAKTURY
	 * @param dicFieldCn - cn lub cn'y p�l w s�owniku
	 * @return pole FieldData lub mapa p�l Map<String, FieldData>
	 */
    public static Object getDictionaryValue(Map<String, FieldData> values, String fieldCn, String... dicFieldCn) {
        if (values == null || fieldCn == null || fieldCn.equals("")) {
            return null;
        }

        FieldData dictionaryField = null;

        boolean dwrPrefixInFieldCn = false;
        if (values.containsKey(DWR_PREFIX+fieldCn)) {
            dictionaryField = values.get(DWR_PREFIX+fieldCn);
        } else {
            dwrPrefixInFieldCn = true;
            dictionaryField = values.get(fieldCn);
        }

        if (dictionaryField != null && dictionaryField.getType() == Field.Type.DICTIONARY) {
            Map<String, FieldData> dictionary = dictionaryField.getDictionaryData();

            if (dicFieldCn.length < 1) {
                return dictionary;
            } else if (dicFieldCn.length == 1) {
                if (dicFieldCn[0] != null) {
                    return dictionary.get(dwrPrefixInFieldCn ? fieldCn.substring(4) : fieldCn + '_' + dicFieldCn[0]);
                } else {
                    return null;
                }
            } else {
                Map<String, FieldData> returnedDictionary = new HashMap<String, FieldData>();
                for (String field : dicFieldCn) {
                    if (field != null && dictionary.containsKey(field)) {
                        returnedDictionary.put(dwrPrefixInFieldCn ? fieldCn.substring(4) : fieldCn  + '_' + field, dictionary.get(field));
                    }
                }
                return returnedDictionary;
            }
        }

        return null;
    }
	
	/**
	 * Metoda zwraca list� id'k�w wpis�w wyst�puj�cych w polu s�ownikowym
	 * @param values
	 * @param dicName - cn pola s�ownikowego, np. CONTRACTOR, DSG_CENTRUM_KOSZTOW_FAKTURY
	 * @return lista z id'kami
	 */
	public static List<Long> extractIdFromDictonaryField(Map<String,FieldData> values, String dicName) {
		return extractIdFromDictonaryField(values.containsKey(DWR_PREFIX+dicName)?values.get(DWR_PREFIX+dicName):values.get(dicName), dicName);
	}
	
	/**
	 * Metoda zwraca list� id'k�w wpis�w wyst�puj�cych w polu s�ownikowym
	 * @param field - pole s�ownikowe
	 * @param dicName - cn pola s�ownikowego, np. CONTRACTOR, DSG_CENTRUM_KOSZTOW_FAKTURY
	 * @return lista z id'kami (pusta gdy nie znaleziono ID, nigdy <code>null</code>
	 */
	public static List<Long> extractIdFromDictonaryField(FieldData field, String dicName) {
		List<Long> ids = new ArrayList<Long>();
		
		if (field != null && field.getType() == Field.Type.DICTIONARY) {
			Map<String,FieldData> dic = field.getDictionaryData();
			
			//s�ownik jednowarto�ciowy 
			if (dic.containsKey("id")) {
				if (dic.get("id").getData() != null && StringUtils.isNotEmpty(dic.get("id").getData().toString())) {
					try {
						ids.add(Long.parseLong(dic.get("id").getData().toString()));
					} catch (NumberFormatException e) {
						log.error("ID nie jest liczb�");
					}
				}
				return ids;
			}
			
			Pattern p = Pattern.compile(dicName + "_ID_\\d+");
			
			for (String fName : dic.keySet())
			{
				Matcher m = p.matcher(fName);
				
				if (m.find() && dic.get(fName) != null && dic.get(fName).getData() != null && StringUtils.isNotEmpty(dic.get(fName).getData().toString())) {
					try {
						ids.add(Long.parseLong(dic.get(fName).getData().toString()));
					} catch (NumberFormatException e) {
						log.error("ID nie jest liczb�");
					}
				}
			}
			return ids;
		
		} else
			return ids;
	}

    /** Wybiera pola, kt�rych nazwa jest zgodna ze wzorcem: {@code .*} endOfNameBeforeNum{@code _\d+$}, np. DEKRETACJA_KSIEG_DEKRETACJA_KSIEG_AMOUNT_1
     * @param dicField - s�ownik wielowarto�ciowy
     * @param endOfNameBeforeNum poprzedzaj�ca numer cze�� nazwy wybieranych p�l
     * @return
     */
    public static Map<String,FieldData> extractFieldsFromDictionaryField(FieldData dicField, String endOfNameBeforeNum){
        Map<String,FieldData> fields = new HashMap<String, FieldData>();

        if (StringUtils.isNotEmpty(endOfNameBeforeNum) && dicField != null && dicField.getType() == Field.Type.DICTIONARY) {
            Map<String,FieldData> dic = dicField.getDictionaryData();

            Pattern p = Pattern.compile("^.*" + endOfNameBeforeNum + "_\\d+$");
            for (String fName : dic.keySet())
            {
                Matcher m = p.matcher(fName);
                if (m.find())
                    fields.put(fName,dic.get(fName));
            }
        }

        return fields;
    }


    /**Metoda zwraca numer pozycji w s�owniku wielowarto�ciowym w oparciu o klucz dla pola z id.
     * @param valueKey wzorzec argumentu, z kt�rego pobrany b�dzie numer {@code^.+_ID_\d+$}, np DWR_MPK_ID_23 -> 23
     * @return numer lub null.
     */
    public static Long extractNumFromDictionaryIdFieldKey(String valueKey){
        if (StringUtils.isEmpty(valueKey)) return null;

        Pattern p = Pattern.compile("^.+_ID_\\d+$");
        Matcher m = p.matcher(valueKey);

        if (!m.find()) return null;

        try {
            String num = valueKey.substring(valueKey.lastIndexOf('_')+1);
            return Long.parseLong(num);
        } catch (NumberFormatException e) {
            log.error("ID nie jest liczb�");
            return null;
        }
    }

    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getData(FieldData field, Class<T> cls) {
        Object o = field.getData();
        if (o != null)
            if (o.getClass().equals(cls))
                return cls.cast(o);
        return null;
    }

    /**Metoda zwraca numer pozycji w s�owniku wielowarto�ciowym odpowiadaj�cy podanej warto�ci pola, kt�rego klucz jest
     * zbudowany wed�ug wzorca {@code^.+_ID_\d+$}.
     *
     * @param dictionaryFieldIdValue Warto��-id, dla kt�rej wyszukiwany jest przypisany jej numer w s�owniku wielowarto�ciowym
     * @param values Zbi�t wszystkich warto�ci w s�owniku wielowarto�ciowym
     * @return numer lub null. Przyk�ad:<br>
     * <br>
     * values=<br>
     * MPK_ID_1     : 310<br>
     * MPK_NAPIS_1  : napis dla 310<br>
     * MPK_ID_2     : 546<br>
     * MPK_NAPIS_2  : napis dla 546<br>
     * <br>
     * dictionaryFieldIdValue=546<br>
     * <br>
     * zwr�cony zostanie 2
     */
    public static Long extractNumFromDictionary (Map<String, FieldData> values, String dictionaryFieldIdValue){
        // nie ma sensu szuka�, bo przebiegamy po ca�ej mapie por�wnuj�c z null
        if(StringUtils.isEmpty(dictionaryFieldIdValue))
            return null;

        Set<Map.Entry<String,FieldData>> valuesSet = values.entrySet();
        for (Map.Entry<String, FieldData> val : valuesSet){
            String key = val.getKey();
            FieldData fieldData = val.getValue();

            Long num = DwrUtils.extractNumFromDictionaryIdFieldKey(key);
            if (num != null && fieldData != null && fieldData.getData() != null){
                if (dictionaryFieldIdValue.equals(fieldData.getStringData()))
                    return num;
            }
        }

        return null;
	}
	
	/**
	 * sprawdza czy wartosci podanych pol nie sa r�wne null
	 * @param values - mapa z wartosciami pol, z klucze z przedrostekim DWR czyli klucz = DWR_CNPOLA
	 * @param cns - cn pol do sprawdzania
	 * @return true jezeli nie sa puste
	 */
	public static boolean isNotNull(Map<String, FieldData> values, String... cns)
	{
		for (String cn : cns)
		{
			if (!values.containsKey(cn))
				return false;
			else if (values.get(cn).getData() == null)
				return false;
		}
		return true;
	}

	public static String longListToComaString(List<Long> mpkIds)
	{
		StringBuilder itemIds = new StringBuilder("");
		for (Long item : mpkIds)
		{
			if (itemIds.length()>0)
			{
				itemIds.append(",");
			}
			itemIds.append(item);
		}
		return itemIds.toString();
	}
	
	public static String getNotEmptyValue(Map<String,FieldData> values, String fieldName) {
		if (values.get(fieldName)!= null && values.get(fieldName).getData()!=null && StringUtils.isNotEmpty(values.get(fieldName).getData().toString())) {
			return values.get(fieldName).getData().toString();
		} else {
			return null;
		}
	}
	
    public static BigDecimal calcMoneyFieldValues(CalcOperation operation, Integer scale, boolean allowNull, Map<String, FieldData> values,  String... fieldCns) {
        List<FieldData> fields = Lists.newLinkedList();
        for (String fieldCn : fieldCns) {
            fields.add(values.get(fieldCn));
        }

        return calcMoneyFieldValues(operation, scale, allowNull, fields.toArray(new FieldData[]{}));
    }

	public static BigDecimal calcMoneyFieldValues(CalcOperation operation, Integer scale, boolean allowNull, FieldData... fields) {
		BigDecimal calcAmount = BigDecimal.ZERO;
		if (operation != null) {
            if (fields.length > 0 && fields[0] != null && fields[0].getData() != null && fields[0].getType() == Field.Type.MONEY) {
               calcAmount = fields[0].getMoneyData();
            } else if (allowNull) {
                if (operation == CalcOperation.ADD || operation == CalcOperation.SUBTRACT) {
                    calcAmount = BigDecimal.ZERO;
                } else if (operation == CalcOperation.MULTIPLY) {
                    calcAmount = BigDecimal.ONE;
                }
                return null;
            } else {
                return null;
            }

			for (int i = 1; i < fields.length; i++) {
                FieldData field = fields[i];
                    if (field != null) {
                        if (field.getData() != null && field.getType() == Field.Type.MONEY) {
                            if (operation == CalcOperation.ADD) {
                                calcAmount = calcAmount.add(field.getMoneyData());
                            } else if (operation == CalcOperation.SUBTRACT) {
                                calcAmount = calcAmount.subtract(field.getMoneyData());
                            } else if (operation == CalcOperation.MULTIPLY) {
                                calcAmount = calcAmount.multiply(field.getMoneyData());
                            } else if (operation == CalcOperation.DIVIDE) {
                                calcAmount = calcAmount.divide(field.getMoneyData(), 10, RoundingMode.HALF_UP);
                            }
                        } else {
                            if (!allowNull) {
                                return null;
                            }
                        }
                    } else {
                        if (!allowNull) {
                            return null;
                        }
                    }
			}
		}
        return scale != null ? calcAmount.setScale(scale, RoundingMode.HALF_UP) : calcAmount;
    }
	
	/**
	 * Ustawia na s�owniku enumy z referencjami. U�yteczne w setMultipledictionaryValue z {@link pl.compan.docusafe.core.dockinds.logic.DocumentLogic}
	 * @param values
	 * @param results
	 * @param refFieldName - np. "CENTRUMID"
	 * @param dictionaryNamePrefix - np. "MPK_"
	 * @param fieldName - np. "ACCEPTINGCENTRUMID"
	 * @param fieldTableName - np. "dsg_ul_project_budget_positions"
	 */
	public static void setRefValueEnumOptions(Map<String, FieldData> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix,
			String fieldName, String fieldTableName) {
		setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, null);
	}
	
	public static void setRefValueEnumOptions(Map<String, FieldData> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix,
			String fieldName, String fieldTableName, String refFieldSelectedId) {
		setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, refFieldSelectedId, null, "_1");
	}

	public static void setRefValueEnumOptions(Map<String, FieldData> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix, String fieldName,
			String fieldTableName, String refFieldSelectedId, EnumValues emptyEnumValue) {
		setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, refFieldSelectedId, emptyEnumValue, "_1");
	}

	public static void setRefValueEnumOptions(Map<String, ?> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix, String fieldName,
			String fieldTableName, String refFieldSelectedId, EnumValues emptyEnumValue, String fieldSuffix) {
        setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, refFieldSelectedId, emptyEnumValue, fieldSuffix, null);
	}

    public static void setRefValueEnumOptions(Map<String, ?> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix, String fieldName,
                                              String fieldTableName, String refFieldSelectedId, EnumValues emptyEnumValue, String fieldSuffix, String refFieldSuffix) {
        setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, refFieldSelectedId, emptyEnumValue, fieldSuffix, null, "");
    }

    /**
     * ustawia warto�ci dla p�l z referencjami refField
     * @param values
     * @param results
     * @param refFieldName cn pola refField
     * @param dictionaryNamePrefix cn s�ownika zako�czony znakiem _
     * @param fieldName cn pola
     * @param fieldTableName nazwa tabeli
     * @param refFieldSelectedId wymuszone id wpisu pola refField, je�li null to pobiera values lub results
     * @param emptyEnumValue obiekt EnumValue jaki zostanie wys�any do przegl�drki, je�li nie znaleziono �adnych pozycji
     * @param fieldSuffix przyrostek dla pola, "" lub "_1" dla wielowarto�ciowych
     * @param refFieldSuffix przyrostek dla pola refField, "" lub "_1" dla wielowarto�ciowych
     * @param fieldDefaultId domy�lne id wpisu pola
     */
    public static void setRefValueEnumOptions(Map<String, ?> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix, String fieldName,
                                              String fieldTableName, String refFieldSelectedId, EnumValues emptyEnumValue, String fieldSuffix, String refFieldSuffix, String fieldDefaultId) {
        setRefValueEnumOptions(values, results, refFieldName, dictionaryNamePrefix, fieldName, fieldTableName, refFieldSelectedId, emptyEnumValue, fieldSuffix, null, "", null);
    }

    public static void setRefValueEnumOptions(Map<String, ?> values, Map<String, Object> results, String refFieldName, String dictionaryNamePrefix, String fieldName,
			String fieldTableName, String refFieldSelectedId, EnumValues emptyEnumValue, String fieldSuffix, String refFieldSuffix, String fieldDefaultId, String dockindCn) {
        if (refFieldSuffix == null) {
            refFieldSuffix = fieldSuffix;
        }
        try {
			Map<String, Object> fieldsValuesTemp = new HashMap<String, Object>();
			if (refFieldSelectedId == null) {
				refFieldSelectedId = "";

                if (results.containsKey(dictionaryNamePrefix + refFieldName)) {
                    try {
                        refFieldSelectedId = ((EnumValues)results.get(dictionaryNamePrefix + refFieldName)).getSelectedId();
                    } catch (Exception e) {
                        log.error("CAUGHT "+e.getMessage(),e);
                    }
                } else {
                    try {
                        EnumValues enumValue = getEnumValues(values, dictionaryNamePrefix + refFieldName);
                        if (enumValue != null) {
                            refFieldSelectedId = enumValue.getSelectedOptions().get(0);
                        }
                    } catch (Exception e) {
                        log.error("CAUGHT " + e.getMessage(), e);
                    }
                }

				fieldsValuesTemp.put(refFieldName + refFieldSuffix, refFieldSelectedId);
			} else {
				fieldsValuesTemp.put(refFieldName + refFieldSuffix, refFieldSelectedId);
			}

            String fieldSelectedId = "";
            EnumValues enumValue = getEnumValues(values, dictionaryNamePrefix + fieldName);
            if (enumValue != null) {
                if (!enumValue.getSelectedOptions().isEmpty()) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            }

            if (fieldSelectedId == null || fieldSelectedId.equals("")) {
                fieldSelectedId = fieldDefaultId;
            }

			boolean containsSelectedFieldId = false;
			boolean containsDefaultSelectedFieldId = false;

			if (DataBaseEnumField.tableToField.get(fieldTableName) != null) {
				if (DataBaseEnumField.tableToField.get(fieldTableName).iterator().next() != null) {
                    String fieldWithSuffix = fieldName + fieldSuffix;
                    DataBaseEnumField base = null;
                    // tableToField mo�e zawiera� np.: NAZWISKO, NAZWISKO_1, wtedy musimy bra� pole z sufiksem
                    for (DataBaseEnumField iterField : DataBaseEnumField.tableToField.get(fieldTableName)) {
                        if(iterField.getCn().equals(fieldWithSuffix) && (dockindCn == null || dockindCn.equals(iterField.getDockindCn()))) {
                            base = iterField;
                            break;
                        }
                    }
                    checkNotNull(base);

                    EnumValues val = base.getEnumItemsForDwr(fieldsValuesTemp);

                    if (val != null && val.getAllOptions() != null && !val.getAllOptions().isEmpty()) {
                        results.put(dictionaryNamePrefix + fieldName, val);

                        for (Map<String, String> map : val.getAllOptions()) {
                            if (map.containsKey(fieldSelectedId)) {
                                containsSelectedFieldId = true;
                                break;
                            }
                            if (map.containsKey(fieldDefaultId)) {
                                containsDefaultSelectedFieldId = true;
                            }
                        }
                        if (containsSelectedFieldId) {
                            List<String> selectedIdTempList = new ArrayList<String>();
                            selectedIdTempList.add(fieldSelectedId);
                            ((EnumValues) results.get(dictionaryNamePrefix + fieldName)).setSelectedOptions(selectedIdTempList);
                        } else {
                            if (containsDefaultSelectedFieldId) {
                                List<String> selectedIdTempList = new ArrayList<String>();
                                selectedIdTempList.add(fieldDefaultId);
                                ((EnumValues) results.get(dictionaryNamePrefix + fieldName)).setSelectedOptions(selectedIdTempList);
                            }
                        }
                    } else if (emptyEnumValue != null) {
                        results.put(dictionaryNamePrefix + fieldName, emptyEnumValue);
                    }
                }
            }
		} catch (Exception e) {
			log.error("DwrUtils - caught execption: " + e.getMessage(), e);
		}
	}

    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }

    /**
     *
     * @param values
     * @param dictionaryCn
     * @return ilo�� pozycji na formatce s�ownika wielowarto�ciowego
     */
    public static int countNoOfItemsInDictionary(Map<String, FieldData> values, String dictionaryCn) {
        Set<String> ids = Sets.newHashSet();
        FieldData field = values.get(DWR_PREFIX+dictionaryCn);
        if (field != null && field.getType() == pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY) {
            Map<String, FieldData> dictionary = field.getDictionaryData();
            Pattern pattern = Pattern.compile("(" + dictionaryCn + "_ID_)" + "(\\d)+");
            Matcher matcher = null;
            for (String key : dictionary.keySet()) {
                matcher = pattern.matcher(key);
                if (matcher.find()) {
                    if (!ids.add(matcher.group(2))) {
                        throw new IllegalArgumentException();
                    }
                }
            }
        }

        return ids.size();
    }

    /**
     *
     * @param values
     * @param dictionaryCn
     * @param moneyFieldCn
     * @return sum� warto�ci p�l typu money z wszystkich wpis�w s�ownika wielowarto�ciowego
     */
    public static BigDecimal sumDictionaryMoneyFields(Map<String, FieldData> values, String dictionaryCn, String moneyFieldCn) {
        BigDecimal amountSum = new BigDecimal(0);
        FieldData dictionaryField = values.get(DWR_PREFIX + dictionaryCn);
        if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
            Map<String,FieldData> dictionary = dictionaryField.getDictionaryData();

            for (int i=1; true; i++) {
                String processedField = dictionaryCn+"_"+moneyFieldCn+"_"+i;
                if (!dictionary.containsKey(processedField)) {
                    break;
                }

                if (dictionary.get(processedField) != null
                        && dictionary.get(processedField).getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY)
                        && dictionary.get(processedField).getMoneyData() != null) {
                    amountSum = amountSum.add(dictionary.get(processedField).getMoneyData());
                }
            }
        }

        return amountSum;
    }

    /**
     * Return list of multiple dictionary rows.
     * Fields in rows are identified by cn without dictionaryCn and without id row.
     * Id of row is added with "ID" key.
     * @param values
     * @param dictionaryCn
     * @return
     * @throws EdmException
     */
    public static Collection<Map<String,FieldData>> getDictionaryData(Map<String, FieldData> values, String dictionaryCn) throws EdmException {

        Map<Long, Map<String,FieldData>> itemsByCellId = new HashMap<Long, Map<String, FieldData>>();

        FieldData dictionaryField = values.get(dictionaryCn.startsWith(DWR_PREFIX) ? dictionaryCn : dwr(dictionaryCn));

        if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
            Map<String,FieldData> dictionaryData = dictionaryField.getDictionaryData();
            Set<Long> ids = getMultipleDictionaryIds(values, dictionaryCn);

            String cellPatternValue = dictionaryCn + "_.+_\\d+";
            Pattern cellPattern = Pattern.compile(cellPatternValue);

            for (String dictCellColumn : dictionaryData.keySet()){
                if (cellPattern.matcher(dictCellColumn).find()){
                    String column = dictCellColumn.substring(dictionaryCn.length()+1,dictCellColumn.lastIndexOf('_'));
                    Long cellId = Long.parseLong(dictCellColumn.substring(dictCellColumn.lastIndexOf('_') + 1));
                    FieldData cellData = dictionaryData.get(dictCellColumn);

                    Map<String,FieldData> items = itemsByCellId.get(cellId);
                    if (items == null){
                        items = new HashMap<String, FieldData>();
                        itemsByCellId.put(cellId, items);
                    }
                    items.put(column,cellData);
                }
            }

//            boolean stop = false;
//            for (int id = 1; !stop; ++id){
//                String cellPatternValue = dictionaryCn + "_.+_"+id;
//                Pattern cellPattern = Pattern.compile(cellPatternValue);
//
//                Map<String,FieldData> row = new HashMap<String, FieldData>();
//                //row.put("ID",new FieldData(Field.Type.LONG,id));
//
//                for (String dictCellColumn : dictionaryData.keySet()){
//                    if (cellPattern.matcher(dictCellColumn).find()){
//                        String column = dictCellColumn.substring(dictionaryCn.length(),dictCellColumn.lastIndexOf('_'));
//                        FieldData cellData = dictionaryData.get(dictCellColumn);
//
//                        row.put(column,cellData);
//                    }
//                }
//
//                if (row.isEmpty())
//                    stop = true;
//                else
//                    items.add(row);
//            }
        } else throw new EdmException(dictionaryCn + "is not field of dictionary type");

        return itemsByCellId.values();
    }

    public static abstract class SumDictionaryNumbersCondition {

        public double sumDictionaryNumbers(Map<String, FieldData> values, String[] dictionaryCns, String numberFieldCn) {
            return sumDictionaryNumbers(values, dictionaryCns, numberFieldCn, this);
        }

        public static double sumDictionaryNumbers(Map<String, FieldData> values, String[] dictionaryCns, String numberFieldCn, SumDictionaryNumbersCondition conditionHandler) {
            double sum = 0;
            for (String dictCn : dictionaryCns) {
                Double dictSum = sumDictionaryNumbers(values, dictCn, numberFieldCn, conditionHandler);
                if (dictSum != null)
                    sum += dictSum;
            }

            return sum;
        }

        public double sumDictionaryNumbers(Map<String, FieldData> values, String dictionaryCn, String numberFieldCn) {
            return sumDictionaryNumbers(values, dictionaryCn, numberFieldCn, this);
        }

        public static double sumDictionaryNumbers(Map<String, FieldData> values, String dictionaryCn, String numberFieldCn, SumDictionaryNumbersCondition conditionHandler) {
            double sum = 0;

            FieldData dictionaryField = values.get(DWR_PREFIX + dictionaryCn);
            if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
                Map<String, FieldData> dictionary = dictionaryField.getDictionaryData();

                for (int i = 1; true; i++) {

                    String processedField = dictionaryCn + "_" + numberFieldCn + "_" + i;
                    if (!dictionary.containsKey(processedField)) {
                        //brak wiersza -> koniec danych
                        break;
                    }

                    Map<String, Object> conditionValues = conditionHandler.getConditionValues(dictionary, dictionaryCn, i);

                    if (conditionHandler.checkCondition(dictionaryCn, conditionValues)) {
                        FieldData fieldData = dictionary.get(processedField);
                        Double value = getDoubleValueSafely(fieldData);
                        if (value != null)
                            sum += value;
                    }
                }
            }

            return sum;
        }

        protected List<FieldsManagerUtils.FieldProperty> conditionsFields;

        public SumDictionaryNumbersCondition(List<FieldsManagerUtils.FieldProperty> conditionsFields) {
            this.conditionsFields = conditionsFields;
        }

        public Map<String, Object> getConditionValues(Map<String, FieldData> dictionaryData, String dictionaryCn, int dictRow) {
            //znalezienie wartosci do spelnienia warunkow
            Map<String, Object> conditionValues = new HashMap<String, Object>();
            for (FieldsManagerUtils.FieldProperty fieldProperty : conditionsFields) {
                String condFieldCn = dictionaryCn + "_" + fieldProperty.cn + "_" + dictRow;

                if (dictionaryData.containsKey(condFieldCn))
                    conditionValues.put(fieldProperty.cn, dictionaryData.get(condFieldCn));
            }
            return conditionValues;
        }

        public abstract boolean checkCondition(String dictionaryCn, Map<String, Object> conditionValues);
    }

    public static class SumDictionaryNumbersEnumCondition extends SumDictionaryNumbersCondition {

        private List<String> validSelectedIds = new ArrayList<String>();
        private String enumDictFieldCn;

        public SumDictionaryNumbersEnumCondition(String enumDictFieldCn, int[] validIds) {
            super(Collections.singletonList(new FieldsManagerUtils.FieldProperty(enumDictFieldCn, EnumValues.class)));
            this.enumDictFieldCn = enumDictFieldCn;
            for (int id : validIds)
                this.validSelectedIds.add(Integer.toString(id));
        }

        @Override
        public boolean checkCondition(String dictionaryCn, Map<String, Object> conditionValues) {
            FieldData enumFieldData = ((FieldData)conditionValues.get(enumDictFieldCn));
            if (enumFieldData == null)
                return false;
            String selectedId = enumFieldData.getEnumValuesData().getSelectedId();
            return validSelectedIds.contains(selectedId);
        }
    }

    /**
     * Ustawia aktualn� dat� dla przekazanego pola
     * @param values
     * @param fieldCn cn pola, dla kt�rego chcemy ustawi� aktualn� dat�
     */
    public static void setCurrentDate(Map<String, FieldData> values, String fieldCn){
        FieldData fieldDate = values.get(fieldCn);
        Date currentDateTime = DateUtils.getCurrentTime();
        fieldDate.setDateData(currentDateTime);
    }


    public static Object getValueFromDictionary(Map<String, FieldData> values, String dictionaryCn, String fieldCn) {
        List<Object> valueList = getValueListFromDictionary(values, dictionaryCn, false, fieldCn);
        return valueList.isEmpty() ? null : valueList.get(0);
    }

    /**
     * Zwraca list� warto�ci p�l z s�ownika.
     * @param values
     * @param dictionaryCn
     * @param isMultiple
     * @param fieldCn
     * @return
     */
    public static <E> List<E> getValueListFromDictionary(Map<String, FieldData> values, String dictionaryCn, boolean isMultiple, String fieldCn) {
        List<E> fieldValues = Lists.newLinkedList();

        FieldData dictionaryField = values.get(DWR_PREFIX+dictionaryCn);
        if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
            Map<String,FieldData> dictionary = dictionaryField.getDictionaryData();

            String dictFieldCn = dictionaryCn + "_" + fieldCn;
            int i=1;
            do {
                String processedField = isMultiple ? dictFieldCn + "_" + i : dictFieldCn;
                if (!dictionary.containsKey(processedField)) {
                    break;
                }

                FieldData fieldData = dictionary.get(processedField);
                if (fieldData != null && fieldData.getData() != null) {
                    fieldValues.add((E) fieldData.getData());
                }
                i++;
            } while (isMultiple);
        }

        return fieldValues;
    }

    /**
     * Zwraca cn wybranej pozycji pola typu dataBase
     * @param values
     * @param fieldCn
     * @param tableName
     * @return
     * @throws EdmException
     */
    public static String getSelectedEnumCn(Map<String, FieldData> values, String fieldCn, String tableName) throws EdmException {
        return getSelectedEnumCn(values, null, fieldCn, tableName);
    }

    public static String getSelectedEnumCn(Map<String, FieldData> values, Map<String, Object> results, String fieldCn, String tableName) throws EdmException {
        String rawSelectedId = "";
        if (results != null && results.containsKey(fieldCn)) {
            try {
                rawSelectedId = ((EnumValues) results.get(fieldCn)).getSelectedId();
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, fieldCn);
                if (enumValue != null) {
                    rawSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        }

        Integer itemId = Ints.tryParse(rawSelectedId);
        if (itemId != null) {
            //FIXME getEnumItemForTable
            EnumItem cost = DataBaseEnumField.getEnumItemForTable(tableName, itemId);
            return cost.getCn();
        }
        return null;
    }

    public static void setEnumNoSelected(FieldData fieldData) {
        setEnumNoSelected(fieldData.getEnumValuesData());
    }
    public static void setEnumNoSelected(EnumValues enumValues) {
        List<String> sel1 = new ArrayList<String>();
        sel1.add("");
        enumValues.setSelectedOptions(sel1);
    }

    public static void setEnumNoSelected(Map<String, FieldData> values, String dwrFieldCn) {
        List<String> sel1 = new ArrayList<String>();
        sel1.add("");
        setEnumNoSelected(values.get(dwrFieldCn));
    }

    /**
     * Resets all fields from given list<br/>
     * if Field is Multiple dictionary it is all reseted (haven't got any entry)
     * - multiple dictionary should have <code>autoCleanEmptyItems = true</code>
     * @param values
     * @param fm
     * @param fieldsCns List of fields cns without DWR_ prefix
     * @throws EdmException
     */
    public static void resetFields(Map<String, FieldData> values, FieldsManager fm, Collection<String> fieldsCns) throws EdmException {
        for(String cn : fieldsCns){
            resetField(values, fm, cn);
        }
    }

    /**
     * Resets field with given CN<br/>
     * if Field is Multiple dictionary it is all reseted (haven't got any entry)
     * - multiple dictionary should have <code>autoCleanEmptyItems = true</code>
     * @param values
     * @param fm
     * @param fieldCn CN of field to reset without DWR_ prefix
     * @throws EdmException
     */
    public static void resetField(Map<String, FieldData> values, FieldsManager fm, String fieldCn) throws EdmException {
        pl.compan.docusafe.core.dockinds.field.Field field = fm.getField(fieldCn);
        if(field == null){
            log.error("No field with fieldCn="+ fieldCn);
            return;
        }
        if("dictionary".equals(field.getType())){
            if(field.isMultiple() && !field.isAutocleanEmptyItems()){
                log.info("Czyszczenie mo�e nie dzia�a�. Aby wyczy�ci� s�ownik wielowarto�ciowy nale�y ustawi� na nim autoCleanEmptyItems = true");
            }
            if(field.isMultiple()){
                cleanMultipleDictionary(values, fieldCn);
            }else{
                cleanNonMultipleDictionary(values,fieldCn);
            }
        }else{
            DwrUtils.resetField(values, fieldCn, true);
        }
    }


    /**
     * Required autocleanEmptyItems="true"
     * @param values
     * @param dictionaryCn
     * @throws java.lang.NullPointerException if dictionaryCn is null
     */
    public static void cleanMultipleDictionary(Map<String, FieldData> values, String dictionaryCn) {
        List<Long> fakeList = new ArrayList<Long>();
        fakeList.add(-777L);
        FieldData fd = new FieldData(Field.Type.DICTIONARY, fakeList);
        if(!dictionaryCn.startsWith("DWR_")){
            dictionaryCn = dwr(dictionaryCn);
        }
        values.put(dictionaryCn, fd);
    }

    /**
     * !!!FIXME!!! NIE DZIA�A
     * @param values
     * @param dictionaryCn
     */
    public static void cleanNonMultipleDictionary(Map<String, FieldData> values, String dictionaryCn){
        log.error("Czyszczenie s�ownika jednowarto�ciowego nie jest jeszcze zaimplementowane");
        if(!dictionaryCn.startsWith("DWR_")){
            dictionaryCn = dwr(dictionaryCn);
        }
        FieldData dicData = values.get(dictionaryCn);
        if (dicData != null) {
            Map<String, FieldData> dicValues = dicData.getDictionaryData();
            resetFields(dicValues, dicValues.keySet(), false);
            dicData.setDictionaryData(dicValues);
        }
    }


    public static void resetFields(Map<String, FieldData> values, Collection<String> fieldsCns, boolean addDwrPrefix){
        for(String cn : fieldsCns){
            resetField(values,cn,addDwrPrefix);
        }
    }

    public static void resetField(Map<String, FieldData> values, String cn, boolean addDwrPrefix){
        FieldData fd = values.get(addDwrPrefix ? DWR_PREFIX + cn : cn);
        if(fd != null){
            try {
                values.put(addDwrPrefix ? DWR_PREFIX + cn : cn, resetField(fd, fd.getType()));
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
            }
        }
    }

    public static FieldData resetField(FieldData fieldData,Field.Type fieldType) throws EdmException {
        if(fieldType == null){
                throw new EdmException("Cannot clear field of type null");
        }
        if(fieldData == null){
            fieldData = new FieldData(fieldType, null);
        }

        switch (fieldType){
            case TEXTAREA:
            case STRING:{
                    fieldData.setStringData("");
                    break;
            }
            case ENUM:{
                EnumValues enumValues = fieldData.getEnumValuesData();
                if (enumValues == null) {
                    enumValues = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
                }
                setEnumNoSelected(enumValues);
                fieldData.setEnumValuesData(enumValues);
                break;
            }
            case DATE:
            case INTEGER:
            case LONG:
            case BOOLEAN:
            case MONEY:{
               fieldData.setData(null);
               break;
            }
            default:
                throw new EdmException(new NotImplementedException("Brak implementacji czyszczenia pola typu " + fieldType.name()));
        }

        return fieldData;
    }

    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getFieldData(FieldData fieldData, Class<T> cls) {
        if (fieldData.getType()== Field.Type.ENUM && cls.isAssignableFrom(EnumValues.class))
            return cls.cast(fieldData.getEnumValuesData());
        Object data = fieldData.getData();
        if (data != null && cls.isAssignableFrom(data.getClass()))
            //if (o.getClass().equals(cls))
            return cls.cast(data);
        return null;
    }
    /**
     * @return data if found object is instance of cls, or null
     */
    public static <T> T getFieldData(Map<String,FieldData> values, String fieldCn, Class<T> cls) {
        FieldData fieldData = values.get(fieldCn);
        return fieldData != null ? getFieldData(fieldData,cls) : null;
    }


    public static String dwr(String fieldCn) {
        return "DWR_" + fieldCn;
    }
    public static String dwrAdd(String fieldCn) {
        return fieldCn.startsWith(DWR_PREFIX) ? fieldCn : DWR_PREFIX + fieldCn;
    }
    public static String dwrRemove(String fieldCn) {
        return fieldCn.startsWith(DWR_PREFIX) ? fieldCn.replace(DWR_PREFIX,"") : fieldCn;
    }

    public static void calculateRelatedField(FieldData value, FieldData percentOfSum, FieldData sum) {
        calculateRelatedField(value, true, percentOfSum, true, sum, true);
    }

    /**
     * Zwraca brakuj�cy cz�on wyra�enia: sum * percentOfSum / 100 = value
     * dok�adnie 1 argument musi by� r�wny NULL
     * Umo�liwia okre�lenie, kt�re pola mog� by� modyfikowane
     * Warto�� wynikowa jest ma najwy�szy priorytet i jest zmieniana, gdy jej warto�� == null
     * @param values
     * @param sum
     * @return obliczona warto�� brakuj�cego cz�onu wyra�enia lub null, gdy pr�ba dzielenia przez 0 lub gdy liczba cz�on�w wyra�enia r�wnych NULL jest r�na od 1
     */
    public static void calculateSum(FieldData sum, FieldData ... values) {
        double sumValue = 0d;
        for (FieldData fd : values){
            Double val = getDoubleValueSafely(fd);
            if (val != null)
                sumValue += val;
        }
        setFieldData(sum, sumValue);
    }
    /**
     * Zwraca brakuj�cy cz�on wyra�enia: sum * percentOfSum / 100 = value
     * dok�adnie 1 argument musi by� r�wny NULL
     * Umo�liwia okre�lenie, kt�re pola mog� by� modyfikowane
     * Warto�� wynikowa jest ma najwy�szy priorytet i jest zmieniana, gdy jej warto�� == null
     * @param value
     * @param percentOfSum
     * @param sum
     * @return obliczona warto�� brakuj�cego cz�onu wyra�enia lub null, gdy pr�ba dzielenia przez 0 lub gdy liczba cz�on�w wyra�enia r�wnych NULL jest r�na od 1
     */
    public static void calculateRelatedField(FieldData value, boolean valueModify, FieldData percentOfSum, boolean percentOfSumModify, FieldData sum, boolean sumModify) {
        if (value==null || percentOfSum==null || sum==null)
            return;

        if (value.isSender()) {
            if (percentOfSumModify && getDoubleValueSafely(sum) != null) // to co wa�niejsze w przypadku wype�nionych wszystkich p�l
                setFieldData(percentOfSum, calculateRelatedFieldOneNull(value, null, sum));
            else if (sumModify && getDoubleValueSafely(percentOfSum) != null)
                setFieldData(sum, calculateRelatedFieldOneNull(value, percentOfSum, null));
        } else if (percentOfSum.isSender()) {
            if (valueModify && getDoubleValueSafely(sum) != null) // to co wa�niejsze w przypadku wype�nionych wszystkich p�l
                setFieldData(value, calculateRelatedFieldOneNull(null, percentOfSum, sum));
            else if (sumModify && getDoubleValueSafely(value) != null)
                setFieldData(sum, calculateRelatedFieldOneNull(value, percentOfSum, null));
        } else if (sum.isSender()) {
            if (percentOfSumModify && getDoubleValueSafely(value) != null) // to co wa�niejsze w przypadku wype�nionych wszystkich p�l
                setFieldData(percentOfSum, calculateRelatedFieldOneNull(value, null, sum));
            else if (valueModify && getDoubleValueSafely(percentOfSum) != null)
                setFieldData(value, calculateRelatedFieldOneNull(null, percentOfSum, sum));
        }
    }

    public static BigDecimal getBigDecimal(Double value) {
        return value == null ? null : new BigDecimal(value);
    }

    /**
     * Zwraca brakuj�cy cz�on wyra�enia: sum * percentOfSum / 100 = value
     * dok�adnie 1 argument musi by� r�wny NULL
     * @param value
     * @param percentOfSum
     * @param sum
     * @return obliczona warto�� brakuj�cego cz�onu wyra�enia lub null, gdy pr�ba dzielenia przez 0 lub gdy liczba cz�on�w wyra�enia r�wnych NULL jest r�na od 1
     */
    private static Double calculateRelatedFieldOneNull(FieldData value, FieldData percentOfSum, FieldData sum) {
        return calculateRelatedFieldOneNull(
                getDoubleValueSafely(value),
                getDoubleValueSafely(percentOfSum),
                getDoubleValueSafely(sum));
    }

    public static Double getDoubleValueSafely (FieldData fd){
        return fd == null ? null : getDoubleValue(fd);
    }

    public static Double getDoubleValue (FieldData fd){
        Object data = fd.getData();
        if (data != null){
            if (data instanceof Double)
                return (Double)data;
            if (data instanceof Integer)
                return (double)(((Integer) data).intValue());
            if (data instanceof BigDecimal)
                return ((BigDecimal)data).doubleValue();
            throw new UnexpectedTypeException("Pole jest niew�a�ciwego typu: " + fd.getType().name()+" - nie mo�na dokona� operacji arytmetycznych");
        }
        return null;
    }

    public static void setFieldData(FieldData fd, Double value) {
        if (value == null){
            fd.setData(null);
            return;
        }

        switch (fd.getType()) {
            case INTEGER:
                log.warn("Tracona jest dok�adno�� liczby (double -> integer): "+value.toString());
                fd.setIntegerData(value.intValue());
                break;
            case LONG:
                log.warn("Tracona jest dok�adno�� liczby (double -> long): "+value.toString());
                fd.setLongData(value.longValue());
                break;
            case MONEY:
                BigDecimal moneyValue = new BigDecimal(value);
                fd.setMoneyData(moneyValue);
                break;
            default:
                break;
        }
    }

    /**
     * Zwraca brakuj�cy cz�on wyra�enia: sum * percentOfSum / 100 = value
     * dok�adnie 1 argument musi by� r�wny NULL
     * @param value
     * @param percentOfSum
     * @param sum
     * @return obliczona warto�� brakuj�cego cz�onu wyra�enia lub null, gdy pr�ba dzielenia przez 0 lub gdy liczba cz�on�w wyra�enia r�wnych NULL jest r�na od 1
     */
    private static Double calculateRelatedFieldOneNull(Double value, Double percentOfSum, Double sum) {
        int counter = (value != null ? 1 : 0) + (percentOfSum != null ? 1 : 0) + (sum != null ? 1 : 0);
        if (counter != 2) return null;

        // wartosc * proc / 100 = koszty
        if (sum == null)
            return percentOfSum.doubleValue() == 0 ? null : value * 100d / percentOfSum;
        if (percentOfSum == null)
            return sum == 0 ? null : value * 100d / sum;
        if (value == null)
            return sum * percentOfSum / 100d;

        throw new IllegalArgumentException("calculateRelatedFieldOneNull - Sytuacja niemo�liwa");
    }



    public static Map<String, FieldData> getDictionaryFields(Map<String, FieldData> values, String dictCn, String dictFieldCn) {
        FieldData dict = values.get(dwr(dictCn));
        if (!isDictionary(dict))
            return new HashMap<String, FieldData>();
        return getDictionaryFields(dict,dictCn,dictFieldCn);
    }
    public static boolean isDictionary(FieldData dict){
        return dict != null && dict.getType() == Field.Type.DICTIONARY;
    }
    public static Map<String, FieldData> getDictionaryFields(FieldData dict, String dictCn, String dictFieldCn) {
        Map<String,FieldData> fields = new HashMap<String, FieldData>();
        Map<String, FieldData> dictData = dict.getDictionaryData();

        Pattern pattern = Pattern.compile(dictCn + "_" + dictFieldCn + "_\\d+");
        for (Map.Entry<String,FieldData> data : dictData.entrySet()){
            if (pattern.matcher(data.getKey()).find())
                fields.put(data.getKey(),data.getValue());
        }

        return fields;
    }


    /**
     * Update multiple dictionary, adds and removes given ids
     * @param values
     * @param dictionaryName cn of dictionary (with or without DWR_ prefix)
     * @param idToAdd id of dictionary entry to add (ommited if null)
     * @param idToRemove id of dictionary entry to remove (ommited if null)
     * @return collection of all ids after update in dictionary
     */
    public static Collection<Long> updateMultipleDictionary(Map<String, FieldData> values,String dictionaryName, Long idToAdd, Long idToRemove) {
        if(!dictionaryName.startsWith(DWR_PREFIX)){
            dictionaryName = dwr(dictionaryName);
        }
        Set<Long> ids =getMultipleDictionaryIds(values, dictionaryName);

        if(idToAdd != null){
            ids.add(idToAdd);
        }
        if(idToRemove != null){
            ids.remove(idToRemove);
        }

        FieldData fd = new FieldData(Field.Type.DICTIONARY, ids);
        values.put(dictionaryName, fd);

        return ids;
    }

    /**
     * Update multiple dictionary, adds and removes given ids
     * @param values
     * @param dictionaryName name without DWR_ prefix
     * @param idsToAdd  collection of ids of dictionary entry to add (ommited if null)
     * @param idsToRemove collection of ids id of dictionary entry to remove (ommited if null)
     * @return collection of all ids after update in dictionary
     */
    public static Collection<Long> updateMultipleDictionary(Map<String, FieldData> values, String dictionaryName, Collection<Long> idsToAdd, Collection<Long> idsToRemove) {
        if(!dictionaryName.startsWith(DWR_PREFIX)){
            dictionaryName = dwr(dictionaryName);
        }
        Set<Long> ids =getMultipleDictionaryIds(values, dictionaryName);
        if(idsToAdd != null){
            ids.addAll(idsToAdd);
        }
        if(idsToRemove != null){
            ids.removeAll(idsToRemove);
        }

        FieldData fd = new FieldData(Field.Type.DICTIONARY, ids);
        values.put(dictionaryName, fd);

        return ids;
    }

//    public static Set<Long> getMultipleDictionaryIds(Map<String, FieldData> values,String dictionaryName){
//        Set<Long> ids = new HashSet<Long>();
//
//        Map<String, FieldData> idFields = DwrUtils.getDictionaryFields(values, dictionaryName.startsWith(DWR_PREFIX) ? dictionaryName.replace(DWR_PREFIX,"") : dictionaryName, "ID");
//        for (FieldData value : idFields.values()) {
//            ids.add(value.getLongData());
//        }
//
//        return ids;
//    }
    public static Set<Long> getMultipleDictionaryIds(Map<String, FieldData> values, String dictionaryName){
        FieldData dict = values.get(dwrAdd(dictionaryName));
        if (!isDictionary(dict))
            return new HashSet<Long>();

        //jezeli dla slownika ustawiono id-ki w bazie
        Object data = dict.getData();
        if (data instanceof Set)
            return (Set<Long>) data;

        //std metoda
        Set<Long> ids = new HashSet<Long>();
        Map<String,FieldData> fields = getDictionaryFields(dict,dictionaryName,"ID");
        for (FieldData value : fields.values()) {
            ids.add(value.getLongData());
        }

        return ids;
    }

    public static String senderButtonFromDictionary(Map<String, FieldData> values, String dictionaryName, String... buttonsNames) throws EdmException {
        String name;
        for(String buttonName : buttonsNames){
            name = senderButtonFromDictionary(values, dictionaryName, buttonName);
            if(name != null){
                return name;
            }
        }
        return null;
    }

    /**
     * Checks if button of given name in given multipleDictionary was clicked
     * and reset this button property clicked = false
     *
     * @param dictionaryName without DWR_  prefix
     * @param buttonName without dictionary prefix
     * @return Name of clicked button, or null if no button was clicked
     * */
    public static String senderButtonFromDictionary(Map<String, FieldData> values, String dictionaryName, String buttonName) throws EdmException {
        Map<String, FieldData> buttons = DwrUtils.getDictionaryFields(values, dictionaryName, buttonName);

        for(Map.Entry<String, FieldData> button : buttons.entrySet()){
            if(button.getValue().getType().equals(Field.Type.BUTTON) && button.getValue().getButtonData().isClicked()){
                button.getValue().getButtonData().setClicked(false);
                return button.getKey();
            }
        }

        return null;
    }

    public static DwrSetter setter(Map<String, FieldData> values) {
        return new DwrSetter(values);
    }

    public static class DwrSetter {
        private Object data;
        private Map<String, FieldData> values;

        private DwrSetter(Map<String, FieldData> values) {
            this.values = values;
        }

        private DwrSetter(DwrSetter dwrSetter) {
            this.values = dwrSetter.values;
            this.data = dwrSetter.data;
        }

        public DwrSetter set(Object data) {
            this.data = data;
            return this;
        }

        public final void to(String... keys) {
            for (String key : keys) {
                FieldData field = getFieldData(key);
                field.setData(getData(data));
            }
        }

        FieldData getFieldData(String key) {
            FieldData fieldData = values.get(key);
            return Optional.fromNullable(fieldData).or(new FieldData());
        }

        Object getData(Object data) {
            return data;
        }

        public DwrSetter useForNull(final Object nullObject) {
            return new DwrSetter(this) {
                @Override
                Object getData(Object data) {
                    return data != null ? data : nullObject;
                }
            };
        }
    }
}
