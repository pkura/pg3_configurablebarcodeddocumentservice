package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.ButtonValue;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;

public class ButtonValueHandler implements ValueHandler {

	@Override
	public void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception {
		if(values.get(field.getCn()) != null)
		{
			Object newValue = values.get(field.getCn()).getButtonData();
			Object oldValue = dwrFacade.getDwrFieldsValues().get(field.getCn());
			if ((oldValue != null && !oldValue.equals(newValue)) || (oldValue == null && newValue != null))
				dwrFacade.getFieldValues().put(oldCn, newValue);
		}
	}
}
