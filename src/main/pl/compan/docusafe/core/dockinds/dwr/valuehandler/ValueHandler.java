package pl.compan.docusafe.core.dockinds.dwr.valuehandler;

import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

/** Obsluguje zmiane zawartosci pola dokumentu poprzez fasade */
public interface ValueHandler
{	
	/** wpisuje zawartosc pola Field w miejsce pola o cn=oldCn.
	 * Wpisanie moze sie sprowadzic takze do usuniecia pola.
	 * Modyfikacja odbywa sie poprzez fasade dokumentu.
	 * @param dwrFacade - dokument, ktory ma byc zmodyfikowany
	 * @param field - pole, ktore ma byc wpisane/zaktualizowane
	 * @param oldCn - nazwa cn pola bez przedrostka DWR_
	 * @param values - mapa nowych wartosci innych pol */
	void handle(DwrFacade dwrFacade, Field field, String oldCn, Map<String, FieldData> values) throws Exception;
}
