package pl.compan.docusafe.core.dockinds.dwr;

import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.FormFile;

/**
 * Opisuje za��cznik na potrzeby DWR (nowych dockind�w). <br />
 * Ikonki wy�wietlane przy za��czniku s� takie same jak w document-archive.
 * @see #setMimeAcceptable(boolean)
 * @see #setMimeXml(boolean)
 * @author Kamil
 *
 */
public class AttachmentValue {
	private String title;
	private String userSummary;
	private Date ctime;
	private String revisionMime;
	private Long attachmentId;
	private Long revisionId;
	private boolean mimeAcceptable = false;
	private boolean mimeXml = false;
	
	private FormFile formFile = null;

    public AttachmentValue() {

    }

    public AttachmentValue(Attachment att) {
        this.attachmentId = att.getId();
        this.title = att.getTitle();
        try {
            this.userSummary = DSUser.safeToFirstnameLastname(att.getAuthor());
        } catch (EdmException e) {
            this.userSummary = e.getLocalizedMessage();
        }
        this.ctime = att.getCtime();
        AttachmentRevision rev = att.getMostRecentRevision();
        this.revisionId = rev.getId();
        this.revisionMime = rev.getMime();
        this.mimeAcceptable = ViewServer.mimeAcceptable(this.revisionMime);
        this.mimeXml = ViewServer.mimeXml(this.revisionMime);
    }
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * Informacja o u�ytkowniku, kt�ry utworzy� za��cznik.
	 * @see AttachmentValue#setUserSummary(String)
	 * @return informacje o u�ytkowniku
	 */
	public String getUserSummary() {
		return userSummary;
	}
	/**
	 * Informacja o u�ytkowniku, kt�ry utworzy� za��cznik.<br/>
	 * W document-archive jest to imi� i nazwisko u�ytkownika - {@link DSUser#safeToFirstnameLastname(String)}
	 * @param userSummary Informacja wy�wietlana na formatce
	 */
	public void setUserSummary(String userSummary) {
		this.userSummary = userSummary;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public String getRevisionMime() {
		return revisionMime;
	}
	public void setRevisionMime(String revisionMime) {
		this.revisionMime = revisionMime;
	}
	public Long getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}
	public Long getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(Long revisionId) {
		this.revisionId = revisionId;
	}
	/**
	 * Okre�la czy za��cznik mo�e by� wy�wietlony w viewserverze i pobrany jako PDF.
	 * @see AttachmentValue#setMimeAcceptable(boolean)
	 * @return Czy typ mime jest akceptowany przez vieserver
	 */
	public boolean isMimeAcceptable() {
		return mimeAcceptable;
	}
	/**
	 * Okre�la czy za��cznik mo�e by� wy�wietlony w viewserverze i pobrany jako PDF,
	 * czyli tworzy dwie ikonki: "wy�wietl za��cznik" (lupka) i "pobierz jako PDF".<br/>
	 * Do ustawienia nale�y u�y� metody {@link ViewServer#mimeAcceptable(String)}
	 * @param mimeAcceptable Czy typ mime jest akceptowany przez vieserver
	 */
	public void setMimeAcceptable(boolean mimeAcceptable) {
		this.mimeAcceptable = mimeAcceptable;
	}
	/**
	 * Okre�la czy za��cznik ma by� wy�wietlony w viewserverze jako XML
	 * @return
	 */
	public boolean isMimeXml() {
		return mimeXml;
	}
	/**
	 * Pozwala na wy�wietlenie pliku XML w viewserverze. <br/> 
	 * Do ustawienia nale�y u�yc metody {@link ViewServer#mimeXml(String)}<br/>
	 * <strong>UWAGA - to ustawienie nadpisuje {@link #setMimeAcceptable(boolean)}</strong>
	 * @see #setMimeAcceptable(boolean)
	 * @param mimeXml Czy za��cznik mo�e zosta� wy�wietlony jako XML w viewserverze
	 */
	public void setMimeXml(boolean mimeXml) {
		this.mimeXml = mimeXml;
	}
	public FormFile getFormFile() {
		return formFile;
	}
	/**
	 * Wys�any plik
	 * @param formFile Ustawia plik do wys�ania
	 */
	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}	
}
