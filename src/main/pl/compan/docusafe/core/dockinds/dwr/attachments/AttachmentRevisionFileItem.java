package pl.compan.docusafe.core.dockinds.dwr.attachments;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemHeaders;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.AttachmentRevision;

import java.io.*;
import java.text.Normalizer;

public class AttachmentRevisionFileItem implements FileItem {
    private AttachmentRevision rev;

    public AttachmentRevisionFileItem(AttachmentRevision rev) {
        this.rev = rev;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        try {
            return rev.getAttachmentStream();
        } catch (EdmException e) {
            throw new IOException(e.getLocalizedMessage(), e);
        }
    }

    @Override
    public String getContentType() {
        return rev.getMime();
    }

    @Override
    public String getName() {
        if(rev.getOriginalFilename() == null)
            return "";
        return Normalizer.normalize(rev.getOriginalFilename(), Normalizer.Form.NFKD).replaceAll("[^\\p{Alnum}\\.]+", " ");
    }

    @Override
    public boolean isInMemory() {
        return false;
    }

    @Override
    public long getSize() {
        return rev.getSize();
    }

    @Override
    public byte[] get() {
        try {
            return IOUtils.toByteArray(rev.getBinaryStream());
        } catch (IOException e) {
            return null;
        } catch (EdmException e) {
            return null;
        }
    }

    @Override
    public String getString(String s) throws UnsupportedEncodingException {
        return new String(this.get(), s);
    }

    @Override
    public String getString() {
        return new String(this.get());
    }

    @Override
    public void write(File file) throws Exception {
        rev.saveToFile(file);
    }

    @Override
    public void delete() {
        // nop
    }

    @Override
    public String getFieldName() {
        return "";
    }

    @Override
    public void setFieldName(String s) {
        // nop
    }

    @Override
    public boolean isFormField() {
        return false;
    }

    @Override
    public void setFormField(boolean b) {
        // nop
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new IllegalStateException("Unsupported");
    }

}
