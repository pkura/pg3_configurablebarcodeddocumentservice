package pl.compan.docusafe.core.dockinds.dwr.attachments;

import com.google.common.collect.*;
import org.apache.commons.fileupload.FileItem;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Przechowuje za��czniki wys�ane ajaksowe przez DWR w strukturze:
 * Sesja u�ytkownika -> cn pola -> lista za��cznik�w
 *
 */
public class DwrAttachmentStorage {
	private final static Logger log = LoggerFactory.getLogger(DwrAttachmentStorage.class);
	private static volatile DwrAttachmentStorage instance = null;
	private ConcurrentMap<HttpSession, ListMultimap<String, DwrFileItem>> sessionFiles = null;
	
	private DwrAttachmentStorage() {
		sessionFiles = Maps.newConcurrentMap();
	}

	public static DwrAttachmentStorage getInstance() {
		if(instance == null) {
			synchronized(DwrAttachmentStorage.class) {
				if(instance == null) {
					instance = new DwrAttachmentStorage();
				}
			}
		}
		
		return instance;
	}

    /**
     * Dodaje za��czniki do przechowywania
     * @param session  Sesja u�ytkownika
     * @param fieldCn  Cn pola
     * @param fileItems  Lista plik�w
     */
	public void addFiles(HttpSession session, String fieldCn, List<DwrFileItem> fileItems) {
		checkNotNull(session);
		checkNotNull(fieldCn);
		checkNotNull(fileItems);

        ListMultimap<String, DwrFileItem> items = Multimaps.synchronizedListMultimap(ArrayListMultimap.<String, DwrFileItem>create());
        ListMultimap<String, DwrFileItem> oldItems = sessionFiles.putIfAbsent(session, items);
        if(oldItems != null) {
            items = oldItems;
        }
        items.putAll(fieldCn, fileItems);

		if(log.isDebugEnabled()) {
			Subject subject = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);
			subject.getPrincipals(UserPrincipal.class);
		}
	}

    /**
     * Zwraca list� plik�w dla danej sesji i pola
     * @param session  Sesja u�ytkownika
     * @param fieldCns  Cn p�l
     * @return Kopia listy plik�w dla sesji i pola (nigdy <code>null</code>)
     */
	public List<DwrFileItem> getFiles(HttpSession session, String... fieldCns) {
        checkNotNull(session);
        checkNotNull(fieldCns);
        if (sessionFiles.containsKey(session)) {
            ListMultimap<String, DwrFileItem> items = sessionFiles.get(session);
            synchronized (items) {
                List<DwrFileItem> ret = Lists.newArrayList();
                for(String fieldCn : fieldCns) {
                    ret.addAll(items.get(fieldCn));
                }

                return ImmutableList.copyOf(ret);
            }
        }

        return Collections.emptyList();
    }

    /**
     * Zwraca wszystkie pliki w sesji u�ytkownika
     * @param session  Sesja u�ytkownika
     * @return Wszystkie pliki w sesji
     */
    public ListMultimap<String, DwrFileItem> getAllFiles(HttpSession session) {
        checkNotNull(session);
        if(sessionFiles.containsKey(session)) {
            ListMultimap<String, DwrFileItem> items = sessionFiles.get(session);
            return items;
        }

        return ArrayListMultimap.create();
    }

    /**
     * Usuwa wszystkie pliki z sesji
     * @param session  Sesja u�ytkownika
     * @param fieldCn  Nazwa pola, z kt�rego nale�y usun�� pliki lub <code>null</code>, aby usun��
     * wszystkie pliki w sesji
     */
	public void removeAll(HttpSession session, String fieldCn) {
        checkNotNull(session);
        if (fieldCn == null) {
            sessionFiles.remove(session);
        } else if (sessionFiles.containsKey(session)) {
            List<DwrFileItem> removedItems = sessionFiles.get(session).removeAll(fieldCn);
            if (log.isDebugEnabled()) {
                StringBuilder sb = new StringBuilder("Files ");
                for (FileItem fileItem : removedItems) {
                    sb.append(fileItem.getName()).append(",").append(fileItem.getFieldName()).append("; ");
                }
                sb.append(" were removed from user ").append(session.getId());
                log.debug(sb.toString());
            }
        }
    }

    /**
     * Usuwa pojedynczy plik z sesji
     * @param session  Sesha u�ytkownika
     * @param fieldCn  Nazwa pola za��cznika
     * @param id  Id pliku
     */
    public void remove(HttpSession session, String fieldCn, String id) {
        checkNotNull(session);
        checkNotNull(fieldCn);
        checkNotNull(id);
        if(sessionFiles.containsKey(session)) {
            ListMultimap<String, DwrFileItem> items = sessionFiles.get(session);
            if(items.containsKey(fieldCn)) {
                List<DwrFileItem> files = items.get(fieldCn);
                synchronized (items) {
                    for(Iterator<DwrFileItem> iter = files.iterator(); iter.hasNext();) {
                        DwrFileItem fileItem = iter.next();
                        if(fileItem.id.equals(id)) {
                            log.debug("Removing file {}", fileItem.getName());
                            iter.remove();
                            break;
                        }
                    }
                }
            }
        }
    }
}
