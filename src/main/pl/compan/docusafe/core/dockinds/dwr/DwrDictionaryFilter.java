package pl.compan.docusafe.core.dockinds.dwr;

import java.util.Map;

/**
 * Filtrowanie warto�ci s�ownika przed/po wykonaniu operacji
 */
public interface DwrDictionaryFilter {
    /**
     * Umo�liwia zmian� zwracanych warto�ci s�ownika przed wywo�aniem {@link DwrDictionaryBase#getValues(String, java.util.Map)} )}. <br />
     * UWAGA - je�li zwr�ci <code>false</code>, to {@link DwrDictionaryBase#getValues(String, java.util.Map)} NIE jest wywo�ywane
     * @param dictionaryFilteredFieldsValues
     * @param dictionaryEntryId
     * @param documentValues Mo�e by� <code>null</code>
     * @return
     */
    boolean filterBeforeGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dictionaryEntryId, Map<String, Object> documentValues);
    /**
     * Umo�liwia zmian� zwracanych warto�ci s�ownika w trakcie wywo�ania {@link DwrDictionaryFacade#getFieldsValues(String, String)}. <br />
     * Dok�adniej: tu� przed zwr�ceniem warto�ci do JavaScriptu.
     * @param dictionaryFilteredFieldsValues
     * @param dicitonaryEntryId
     * @param fieldsValues
     * @param documentValues
     */
    void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues);

    /**
     * Umo�liwia zmian� warto�ci s�ownika tu� przed zapisem, czyli przed {@link DwrDictionaryBase#update(java.util.Map)}
     * @param dictionaryFilteredFieldData
     */
    void filterBeforeUpdateValues(Map<String, FieldData> dictionaryFilteredFieldData);
}