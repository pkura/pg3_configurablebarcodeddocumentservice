package pl.compan.docusafe.core.dockinds.dwr;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.Bag;
import org.apache.commons.collections.bag.HashBag;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DockindScheme;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field.Status;
import pl.compan.docusafe.core.dockinds.dwr.valuehandler.*;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.wssk.LogicExtension_Reopen;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.*;

public class DwrFacade
{
	private static final Logger log = LoggerFactory.getLogger(DwrFacade.class);
	
	/*
	 * DwrParams
	 */
	public static final String DWR_DOCUMENTID = "documentId";
	private static final String DWR_DOCUMENTTYPE = "documentType";
	private static final String DWR_DOCKIND = "dockind";
	private static final String DWR_DOCIDS = "docIds";
	private static final String ACTIVITY = "activity";

	public static final String DWR_SESSION_NAME = "dwrSession";
	private static final String DWR_MESSAGE_FIELD = "messageField";
	public static final String DWR_PREFIX = "DWR_";

	private FieldsManager fieldsManager;
	private HttpSession session;
	private Map<String, Object> dwrSessionValues;
	private Map<String, Object> fieldValues;
	/** pola i ich wartosc dla DWR */
	private List<Field> dwrFields;
	private Map<String, Object> dwrFieldsValues;
	static final Map<Field.Type, ValueHandler> valueHandlers = new HashMap<Field.Type, ValueHandler> ();
	static 
	{
		valueHandlers.put(Field.Type.ENUM, new EnumValueHandler());
		valueHandlers.put(Field.Type.ENUM_MULTIPLE, new EnumMulipleValueHandler());
		valueHandlers.put(Field.Type.DICTIONARY, new DictionaryValueHandler());
		valueHandlers.put(Field.Type.DATE, new BasicValueHandler());
		valueHandlers.put(Field.Type.TIMESTAMP, new BasicValueHandler());
		valueHandlers.put(Field.Type.INTEGER, new BasicValueHandler());
		valueHandlers.put(Field.Type.LONG, new BasicValueHandler());
		valueHandlers.put(Field.Type.HTML, new TextValueHandler());
		valueHandlers.put(Field.Type.HTML_EDITOR, new HtmlEditorValueHandler());
		valueHandlers.put(Field.Type.STRING, new TextValueHandler());
		valueHandlers.put(Field.Type.TEXTAREA, new TextValueHandler());
		valueHandlers.put(Field.Type.BOOLEAN, new BooleanValueHandler());
		valueHandlers.put(Field.Type.MONEY, new MoneyValueHandler());
		valueHandlers.put(Field.Type.LINK, new EmptyValueHandler());
		valueHandlers.put(Field.Type.ATTACHMENT, new AttachmentValueHandler());
		valueHandlers.put(Field.Type.BUTTON, new ButtonValueHandler());
	}
			
	public DwrFacade()
	{	
		//log.error("DWR_FACADE CREATE");
	}
	
	public List<Field> getFieldsList(Map<String, String> dwrParams)
	{
		initialDwrParams(dwrParams);
		initialize();
		return this.dwrFields;
	}

	public Map<String, Object> getFieldsValues(Map<String, String> dwrParams)
	{
		initialDwrParams(dwrParams);
		initialize();
		return this.getDwrFieldsValues();
	}

	public List<Field> setFieldsValues(Map<String, FieldData> values, Map<String, String> dwrParams) throws Exception {
		initialDwrParams(dwrParams);
		initialize(values.keySet());
        Map<String, FieldData> selectedValues = DwrSelectedValues.copyRefValues(values);
        addToDwrSession(selectedValues, DwrSelectedValues.SELECTED_FIELDS_VALUES);
		try
		{	
			Field msg = fieldsManager.getDocumentKind().logic().validateDwr(values, fieldsManager);
			if(msg != null)
			{				
				msg.setKind(Field.Kind.MESSAGE);
				msg.setCn(DWR_MESSAGE_FIELD);
				this.dwrFields.add(msg);
			}
			else
				this.dwrFields.remove(DWR_MESSAGE_FIELD);
			
			for (Field field : this.dwrFields)
			{ 
				// odciecie przedrostka DWR_ - DWR_PREFIX
				String oldCn = field.getCn().replaceFirst(DWR_PREFIX, "");
				valueHandlers.get(field.getType()).handle(this, field, oldCn, values);
			}
			reloadValuesSaveSession();
			reloadSearchFilter(values);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
            // dzi�ki temu wy�wietli si� na formatce
            throw e;
		}
		return initialize();
	}

    /**
     * Wymaga by pole session by�o zainicjowane
     */
    private void addToDwrSession(Map<String, FieldData> values, String key) {
        //this.session.setAttribute(key, values);
        dwrSessionValues.put(key, values);
        this.session.setAttribute(DWR_SESSION_NAME, dwrSessionValues);
    }

	/**
	 * Metoda aktualizuje warto�ci filtruj�cych s�ownik�w zainicjalizowanych w trakcie wywo�ania metody {@link pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade#dwrSearch(String, Map, int, int) DwrDictionaryFacade.dwrSearch(String, Map, int, int)}
	 * @param values
	 */
	private void reloadSearchFilter(Map<String, FieldData> values){
		for(String docFieldName : values.keySet()){
			try
			{
				if(values.get(docFieldName).getType() == Field.Type.DICTIONARY && values.get(docFieldName).getData() instanceof Map){
					Map<String, FieldData> fieldsValues = (Map<String, FieldData>)values.get(docFieldName).getData();
					docFieldName = docFieldName.replaceFirst(DWR_PREFIX, "");
					if(this.session.getAttribute(DwrDictionaryFacade.FILTER_PREFIX+docFieldName) != null){
						this.session.setAttribute(DwrDictionaryFacade.FILTER_PREFIX+docFieldName, fieldsValues);
					}
				}
			}catch(Exception e){
				//TODO rzuca nullpoiterem w linii 155
				log.error(e.getMessage(), e);
			}
		}
	}
	
	private void populateWithEmailContent(Map<String, Object> values, Object messageId) throws EdmException, SQLException{
		if(messageId != null && messageId instanceof String){
			Long msgId = Long.parseLong(String.valueOf(messageId));
			for(pl.compan.docusafe.core.dockinds.field.Field field : this.fieldsManager.getFields()){
				if(field instanceof EmailMessageField){
					EmailMessageField emailField = (EmailMessageField)field;
					if(emailField.getContentType().equals(EmailMessageField.ContentType.EMAIL_SUBJECT)){
						values.put(emailField.getDwrField().getCn().replaceFirst("DWR_", ""), emailField.provideFieldValue(msgId).getValue());
					}
					else if(emailField.getContentType().equals(EmailMessageField.ContentType.EMAIL_SENDER)){
						values.put(emailField.getDwrField().getCn().replaceFirst("DWR_", ""), emailField.provideFieldValue(msgId).getValue());
					}
					else if(emailField.getContentType().equals(EmailMessageField.ContentType.EMAIL_SENTDATE)){
						values.put(emailField.getDwrField().getCn().replaceFirst("DWR_", ""), emailField.provideFieldValue(msgId).getValue());								
					}
					else if(emailField.getContentType().equals(EmailMessageField.ContentType.EMAIL_BODY)){
						values.put(emailField.getDwrField().getCn().replaceFirst("DWR_", ""), emailField.provideFieldValue(msgId).getValue());
					}
					else if(emailField.getContentType().equals(EmailMessageField.ContentType.EMAIL_ATTACHMENT)){
						values.put(emailField.getDwrField().getCn().replaceFirst("DWR_", ""), emailField.provideFieldValue(msgId).getValue());
					}
				}
			}
		}
	}
	
	private void costsCentersInOrder()
	{
		HashMap<String, Object> mapa = (HashMap<String, Object>) dwrSessionValues.get("FIELDS_VALUES");
        List<Long> listaKosztow = (List<Long>)mapa.get("DSG_CENTRUM_KOSZTOW_FAKTURY");
        List<Long> kopiaKosztowParz = new ArrayList<Long>();
        List<Long> kopiaKosztowNieParz = new ArrayList<Long>();
	    Long lastOne = null;
	    
		if(listaKosztow != null && !listaKosztow.isEmpty())
		{
			int size = listaKosztow.size();
			if(size%2 == 0)
			{
				for(int i = 0; i < listaKosztow.size(); i++)
				{
					if(i%2 == 0)
						kopiaKosztowParz.add(listaKosztow.get(i));
					else
						kopiaKosztowNieParz.add(listaKosztow.get(i));
				}
			}
			else
			{
				for(int i = 0; i < listaKosztow.size() - 1; i++)
				{
					if(i%2 == 0)
						kopiaKosztowParz.add(listaKosztow.get(i));
					else
						kopiaKosztowNieParz.add(listaKosztow.get(i));
				}
			}
			lastOne = listaKosztow.get(listaKosztow.size() - 1);
			for(int i = 0; i < kopiaKosztowParz.size(); i++)
			{
				listaKosztow.set(2*i + 1, kopiaKosztowParz.get(i));
			}
			for(int i = 0; i < kopiaKosztowNieParz.size(); i++)
			{
				listaKosztow.set(2*i, kopiaKosztowNieParz.get(i));
			}
			((HashMap<String, Object>) dwrSessionValues.get("FIELDS_VALUES")).remove("DSG_CENTRUM_KOSZTOW_FAKTURY");
			((HashMap<String, Object>) dwrSessionValues.get("FIELDS_VALUES")).put("DSG_CENTRUM_KOSZTOW_FAKTURY", listaKosztow);	
		}			
	}

	private void initialDwrParams(Map<String, String> dwrParams)
	{
		log.debug("initialDwrParams: {}", dwrParams);
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		this.session = WebContextFactory.get().getSession();

		try
		{
			DSApi.open(AuthUtil.getSubject(req));
			String type = dwrParams.get(DWR_DOCUMENTTYPE);
			String dockindCn = dwrParams.get(DWR_DOCKIND);
			String docIds = dwrParams.get(DWR_DOCIDS);
			
			Long documentId = null;

			DocumentKind documentKind;

			if (!String.valueOf("").equalsIgnoreCase(dwrParams.get(DWR_DOCUMENTID)))// jest documentId
			{
				documentId = Long.valueOf(dwrParams.get(DWR_DOCUMENTID));
				documentKind = Document.find(documentId).getDocumentKind();
				if (!documentKind.getCn().equals(dockindCn))
					documentKind = DocumentKind.findByCn(dockindCn);
			}
			else
			{
				documentKind = DocumentKind.findByCn(dockindCn);
			}
			
			if (session.getAttribute(DWR_SESSION_NAME) != null)
			{
				
				this.dwrSessionValues = (Map<String, Object>) session.getAttribute(DWR_SESSION_NAME);
                if(log.isDebugEnabled()){
                    log.debug(dwrSessionValues.get("documentId") != null ? dwrSessionValues.get("documentId").toString() : "BRAK documentId");
                    log.debug(dwrSessionValues.get("dockindKind") != null ? dwrSessionValues.get("dockindKind").toString() : "BRAK dockindKind");
                    log.debug(dwrSessionValues.get("FIELDS_VALUES") != null ? dwrSessionValues.get("FIELDS_VALUES").toString() : "BRAK FIELDS_VALUES");
                }
				
				this.fieldsManager = documentKind.getFieldsManager(documentId).withActivityId(dwrParams.get(ACTIVITY));
				if (!dwrSessionValues.get("dockindKind").equals(documentKind.getCn()) || (documentId != null && !dwrSessionValues.get("documentId").equals(documentId)))
				{
					session.removeAttribute(DWR_SESSION_NAME);
				}
				if(AvailabilityManager.isAvailable("ifpan.fakturaZapotrzebowanie.centraKosztow"))
					costsCentersInOrder();
			}

			// jezli inny niz poprzednio powinino byc null, wcze�niej bo session.removeAttribute(DWR_SESSION_NAME);
			if (session.getAttribute(DWR_SESSION_NAME) == null)
			{
				this.fieldsManager = documentKind.getFieldsManager(documentId).withActivityId(dwrParams.get(ACTIVITY));
				if (!String.valueOf("").equals(docIds))
				{
					Gson gson = new Gson();
					Map<String, Object> vals = gson.fromJson(URLDecoder.decode(docIds, "UTF-8"), new TypeToken<Map<String, String>>() {}.getType());	
					//	jesli tworzymy dokument na podstawie emaila message_id -> ds_message.message_id
					if(vals.containsKey(EmailMessageField.MESSAGE_ID_COLUMN)){
						populateWithEmailContent(vals, vals.get(EmailMessageField.MESSAGE_ID_COLUMN));
					}
					this.fieldsManager.reloadValues(vals); 
				}			
				
				if (documentId == null)
                {
					documentKind.logic().setInitialValues(fieldsManager, Integer.valueOf(type));
                }
				else
					documentKind.logic().setAdditionalValues(fieldsManager, Integer.valueOf(type), dwrParams.get(ACTIVITY));
				
				this.fieldsManager.resetValues();
				
				this.dwrSessionValues = new HashMap<String, Object>();
				this.dwrSessionValues.put("documentId", documentId);
				this.dwrSessionValues.put("dockindKind", documentKind.getCn());
				this.dwrSessionValues.put("activity", dwrParams.get(ACTIVITY));
				this.dwrSessionValues.put("FIELDS_VALUES", this.fieldsManager.getFieldValues());
				this.session.setAttribute(DWR_SESSION_NAME, this.dwrSessionValues);
			}
			
			/* Rozszerzenie dla WSSK: o ile dany dokument tego wymaga,
			 * wykonuje w odpowiedni spos�b setFieldValues(...) na TEJ fasadzie.
			 * Mo�e skorzysta� z nast�puj�cych wywo�a�:
			 * dwrFacade.getFieldValues()
			 * dwrSessionValues.get("activity")
			 * dwrSessionValues.get("dockindKind")
			 * dwrSessionValues.get("FIELDS_VALUES")
			 * request.getAttribute("org.directwebremoting.dwrp.batch").getPage() */
			if ( documentKind.logic() instanceof LogicExtension_Reopen )
				((LogicExtension_Reopen) documentKind.logic()).modifyDwrFacade(req, dwrSessionValues, this.fieldsManager, this);

			this.setFieldValues((Map<String, Object>) dwrSessionValues.get("FIELDS_VALUES")); 
			this.fieldsManager.reloadValues(this.getFieldValues());							  
			//----------------------------------------------------------------------------
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi._close();
		}
	}

	
	private List<Field> initialize()
	{
		return initialize(null);
	}
	
	/** Inicjalizuje ta fasade.
	 * @param fieldsKey - mozna podac NULL
	 * @return lista pol dwrFields */
	private List<Field> initialize(Set<String> fieldsKey)
	{
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		
		Map<String, SchemeField> schemeFields = null;
		// TODO zmieni� !!!!
		if (this.dwrFields == null)
			this.dwrFields = new ArrayList<Field>();
		if (this.getDwrFieldsValues() == null)
			this.setDwrFieldsValues(new TreeMap<String, Object>());
		try
		{
			DSApi.open(AuthUtil.getSubject(req));
			DockindScheme dockindScheme = fieldsManager.getDocumentKind().getSheme(fieldsManager, (this.dwrSessionValues.get("activity") != null && !this.dwrSessionValues.get("activity").equals("")) ?  (String) this.dwrSessionValues.get("activity") : null);
			if (dockindScheme != null)
				schemeFields = dockindScheme.getSchemeFields();
            boolean isActivity = (this.dwrSessionValues.get("activity") != null && !this.dwrSessionValues.get("activity").equals(""));
			DSUser loggedUser = DSApi.context().getDSUser();
			boolean unlockFields = false;
			if(Docusafe.getAdditionProperty("search_doc_and_edit").equalsIgnoreCase("true") && WebContextFactory.get().getCurrentPage().endsWith("edit-dockind-document.action") && loggedUser.isAdmin())
				unlockFields = true;
			
			boolean lockFields = false;
			
            if(AvailabilityManager.isAvailable("document.modifyPermission.IsOnTasklist") && !DSApi.context().isAdmin()){
            	if(fieldsManager.getDocumentId() != null && !PermissionManager.isDocumentInTaskList(fieldsManager.getDocumentId())){
            		lockFields = true;
            	}
            }
            
			Field newField = null;
			int i = 0;
			for (pl.compan.docusafe.core.dockinds.field.Field field : fieldsManager.getFields())
			{
				newField = field.getFieldForDwr(schemeFields);
				if(unlockFields)
				{
					newField.setHidden(false);
					newField.setDisabled(false);
					newField.setRequired(false);
					newField.setReadonly(false);
				}
                if(AvailabilityManager.isAvailable("dwrFacade.disable.field.withoutActitivty") && !isActivity && fieldsManager.getDocumentId() != null )
                {
                    boolean canEdit = false;
                    SchemeField  a = schemeFields.get(field.getCn());

                    if (field.getEditPermissions() != null && field.getEditPermissions().size() > 0)
                    {
                        //DSDivision[] divisions = DSApi.context().getDSUser().getDivisions();
                        DSDivision[] divisions = DSUser.findByUsername(DSApi.context().getPrincipalName()).getDivisions();

                        for (DSDivision division : divisions)
                        {
                            if (field.getEditPermissions().contains(division.getGuid()))
                            {
                                canEdit = true;
                                break;
                            }
                        }
                    }

                    if (!canEdit)
                    {
                        newField.setDisabled(true);
                        newField.setReadonly(true);
                    }
                }

                if(lockFields){
                	newField.setDisabled(true);
                	newField.setReadonly(true);
                }
                
				if (newField.getHiddenOnDisabledEmpty() && !newField.getHidden() && newField.getDisabled()) {
					newField.setHidden(isEmptyField(field.getCn()));
				}
				
				boolean exist = addToFields(fieldsKey, newField, i);
				if (exist)
				{
					i++;
					if (isEnumField(field))
					{
						if (field instanceof DataBaseEnumField && field.getCorrection())
							this.getDwrFieldsValues().put(newField.getCn(), ((DataBaseEnumField) field).getEnumItemsForDwr(fieldsManager.getDocumentId(), getFieldValues()));
						else
							this.getDwrFieldsValues().put(newField.getCn(), field.getEnumItemsForDwrByScheme(getFieldValues(), schemeFields));
					}
					else if (getFieldValues() != null)
					{
//						if (getFieldValues().get(field.getCn()) == null && !(getFieldValues().get(field.getCn()) instanceof FieldData && ((FieldData)getFieldValues().get(field.getCn())).isAllowNull()))
//							continue;

                        Object fieldValue = getFieldValues().get(field.getCn());
                        if (fieldValue == null && !field.isAllowNull())
							continue;
						
//						if (getFieldValues().get(field.getCn()) == null)
//							continue;
						/**
						 * TODO - interfejs dla warto�ci FieldData DWR
						 */
                        if (field instanceof AttachmentField) {
                            if (fieldValue != null && fieldValue instanceof Long)
                                this.dwrFieldsValues.put(newField.getCn(), ((AttachmentField) field).getAttachmentForDwr((Long) fieldValue));
                            else
                                this.dwrFieldsValues.put(newField.getCn(), ((AttachmentField) field).getAttachmentForDwr(null));
                        } else if (field instanceof ButtonField)
                            this.dwrFieldsValues.put(newField.getCn(), ((ButtonField) field).getButtonForDWR());
                        else
                            this.getDwrFieldsValues().put(newField.getCn(), fieldValue);
                    }
				}
			}
            /**
             * Opcjonalna modyfikacja w�a�no�ci p�l dwr poprzez implementacj� metody modifyDwrFields(...) klasy AbstractDocumentLogic przez logik� dockinda.
             * U�ywa� z umiarem i rozs�dkiem, gdy� "z wielk� moc� wi��e si� wielka odpowiedzialno��"...
             */
            DocumentLogic documentLogic = fieldsManager.getDocumentKind().logic();
            if(documentLogic != null && documentLogic instanceof AbstractDocumentLogic){
                ((AbstractDocumentLogic)documentLogic).modifyDwrFields(this.dwrFields, this.dwrFieldsValues, fieldsManager);
            }

		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		finally
		{
			DSApi._close();
		} 
		return this.dwrFields;
	}

	/** Zapewnia, ze: <br/>
	 * pole jest zawarte wsrod this.dwrFields <==> pole nie jest ukryte 
	 * <p>
	 * Jesli newField nie jest ukryte ( getHidden()==false ) to:
	 * <ul> dodaje pole do this.dwrFields, o ile go tam nie bylo,</ul>
	 * <ul> uaktualnia status pola (CHANGED / NOCHANGE),</ul>
	 * <ul> zwraca TRUE.</ul></p>
	 * <p>
	 * Jesli newField jest ukryte ( getHidden()==true ) to:
	 * <ul> usuwa pole z this.dwrFields, o ile tam bylo,</ul>
	 * <ul> zwraca FALSE.</ul></p>
	 * 
	 * @param fieldsKey - moze byc celowo jako NULL.
	 * @param newField
	 * @param i - indeks do dwrFields */
	private boolean addToFields(Set<String> fieldsKey, Field newField, int i)
	{
		boolean exist = false;
		if (!this.dwrFields.contains(newField) && !newField.getHidden())
		{
			this.dwrFields.add(i, newField);
			if (fieldsKey != null && fieldsKey.contains(newField.getCn()))
				this.dwrFields.get(i).setStatus(Status.NOCHANGE);
			exist = true;
		}
		else if (this.dwrFields.contains(newField))
		{
			if (newField.getHidden())
				this.dwrFields.remove(i);
			else
			{
				if (checkChanges(this.dwrFields.get(i), newField))
					this.dwrFields.get(i).setStatus(Status.CHANGED);
				else
					this.dwrFields.get(i).setStatus(Status.NOCHANGE);
				exist = true;
			}
		}
		return exist;
	}
	
	/** Porownuje oldField i newField pod wzgledem: <br/>
	 * disabled, readOnly, required, hidden.
	 * <p>
	 * Jesli pole jest slownikiem, to porownuje odrebne pola slownika.
	 * Jesli wystepuja roznice, oldField jest zmieniane wg newField
	 * @return true jesli wystapila choc jedna roznica */
	private boolean checkChanges(Field oldField, Field newField)
	{
		boolean changed = false;
		if (!oldField.getDisabled().equals(newField.getDisabled()))
		{
			oldField.setDisabled(newField.getDisabled());
			changed = true;
		}
		if (!oldField.getReadonly().equals(newField.getReadonly()))
		{
			oldField.setReadonly(newField.getReadonly());
			changed = true;
		}
		if (!oldField.getRequired().equals(newField.getRequired()))
		{
			oldField.setRequired(newField.getRequired());
			changed = true;
		}
		if (!oldField.getHidden().equals(newField.getHidden()))
		{
			oldField.setHidden(newField.getHidden());
			changed = true;
		}
		if (oldField.getType().equals(Field.Type.DICTIONARY))
		{
			changed = checkChangesForDictionaryFields(oldField, newField);
			Bag oldBag = new HashBag(oldField.getDictionary().getDicButtons() != null ? oldField.getDictionary().getDicButtons() : Collections.EMPTY_LIST );
			Bag newBag = new HashBag(newField.getDictionary().getDicButtons() != null ? newField.getDictionary().getDicButtons() : Collections.EMPTY_LIST);
			if(!oldBag.equals(newBag)) {
				oldField.getDictionary().setDicButtons(newField.getDictionary().getDicButtons());
				changed = true;
			}
			
		}
		return changed;
	}

	/** Porownuje skladowe pola slownikow oldField i newField pod wzgledem: <br/>
	 * disabled, readOnly, required, hidden.
	 * <p>
	 * Kazde skladowe pole z oldField ma zmieniany status na NOCHANGE,
	 * chyba ze wystepuja roznice z newField - wtedy status CHANGED
	 * @return true jesli wystapila choc jedna roznica wsrod pol skladowych */
	private boolean checkChangesForDictionaryFields(Field oldDic, Field newDic)
	{
		boolean changed = false;
		for (Field oldDicField : oldDic.getDictionary().getFields())
		{
			int index = oldDic.getDictionary().getFields().indexOf(oldDicField);
			int newIndex = newDic.getDictionary().getFields().indexOf(oldDicField);
			if (newDic.getDictionary().getFields().contains(oldDicField))
			{
                if (checkChanges(oldDic.getDictionary().getFields().get(index), newDic.getDictionary().getFields().get(newIndex)))
				{
					oldDic.getDictionary().getFields().get(index).setStatus(Status.CHANGED);
                    changed = true;
				}
				else if(oldDic.getDictionary().getFields().get(index).getStatus() != Status.CHANGED)
					oldDic.getDictionary().getFields().get(index).setStatus(Status.NOCHANGE);
			}
		}
		return changed;
	}

	/** Przeladowuje wartosci z tej fasady do fieldsManagera, <br/>
	 * z niego do dwrFieldsValues jako "FIELDS_VALUES", <br/>
	 * a dwrFieldsValues zapisuje do sesji.
	 * @throws Exception */
	public void reloadValuesSaveSession() throws Exception
	{
		this.fieldsManager.reloadValues(this.getFieldValues());
		dwrSessionValues.put("FIELDS_VALUES", this.fieldsManager.getFieldValues());
		this.session.setAttribute(DWR_SESSION_NAME, dwrSessionValues);
	}

	public Map<String, Object> getDwrFieldsValues()
	{
		return dwrFieldsValues;
	}

	public void setDwrFieldsValues(Map<String, Object> dwrFieldsValues)
	{
		this.dwrFieldsValues = dwrFieldsValues;
	}

	public Map<String, Object> getFieldValues()
	{
		return fieldValues;
	}
	
	public boolean isEmptyField(String cn) {
		Object value = getFieldValues().get(cn);
		
		if (value == null) {
			return true;
		}
		if ("".equals(value.toString())) {
			return true;
		}
		
		return false;
	}

	public void setFieldValues(Map<String, Object> fieldValues)
	{
		this.fieldValues = fieldValues;
	}
	
	/** @param field - pole do sprawdzenia
	 * @return - TRUE jesli field jest instancja:
	 *  AbstractEnumColumnField, EnumerationField, EnumNonColumnField. */
	private boolean isEnumField(pl.compan.docusafe.core.dockinds.field.Field field)
	{
		return field instanceof AbstractEnumColumnField
							|| field instanceof EnumerationField 
							|| field instanceof EnumNonColumnField;
	}
}

