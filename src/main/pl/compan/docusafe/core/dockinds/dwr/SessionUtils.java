package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.base.Objects;
import org.directwebremoting.WebContextFactory;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by kk on 24.03.14.
 */
public class SessionUtils {

    private SessionUtils(){};

    public static Object getFromSession(String key) {
        return getFromSession(null, key, true);
    }

    public static Object getFromSession(Object token, String key) {
        return getFromSession(token, key, null);
    }

    public static <T> T getFromSession(Object token, String key, T defaultValue) {
        Object value = null;
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            token = Objects.firstNonNull(token, Objects.firstNonNull(getDocumentIdFromDwrSession(session), ""));
            value = session.getAttribute(key + "#" + token);
        }

        return value != null ? (T) value : defaultValue;
    }

    public static void putToSession(Object token, String key, Object value) {
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            token = Objects.firstNonNull(token, Objects.firstNonNull(getDocumentIdFromDwrSession(session), ""));
            session.setAttribute(key + "#" + token, value);
        }
    }

    public static void clearSession(Object token, String singleKey, String... keys) {
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            token = Objects.firstNonNull(token, Objects.firstNonNull(getDocumentIdFromDwrSession(session), ""));
            session.removeAttribute(singleKey + "#" + token);
            for (String key : keys) {
                session.removeAttribute(key + "#" + token);
            }
        }
    }

    public static Object getDocumentIdFromDwrSession(HttpSession session) {
        return ((Map<String, Object>) session.getAttribute(DwrFacade.DWR_SESSION_NAME)) != null ? ((Map<String, Object>) session.getAttribute(DwrFacade.DWR_SESSION_NAME)).get(DwrFacade.DWR_DOCUMENTID) : null;
    }
}
