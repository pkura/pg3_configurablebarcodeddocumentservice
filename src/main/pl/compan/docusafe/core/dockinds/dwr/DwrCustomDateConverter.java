package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import org.directwebremoting.ConversionException;
import org.directwebremoting.convert.DateConverter;
import org.directwebremoting.extend.InboundVariable;
import org.directwebremoting.extend.MarshallException;
import org.directwebremoting.extend.NonNestedOutboundVariable;
import org.directwebremoting.extend.OutboundContext;
import org.directwebremoting.extend.OutboundVariable;
import org.directwebremoting.extend.ProtocolConstants;
import pl.compan.docusafe.util.DateUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Konwerter unifikuj�cy strefy czasowe po stronie przegl�darki.
 * Data wysy�ana z dwr jest konwertowana do strefy czasowej serwera,
 * a tak�e odwrotnie. Data wysy�ana do klienta jest ustawiana na jego stref� czasow�.
 * Rozwi�zanie s�uszne, dop�ki DocuSafe nie b�dzie rzeczywi�cie u�ywany w r�nych strefach czasowych.
 *
 * User: kk
 * Date: 25.09.13
 * Time: 09:52
 */
public class DwrCustomDateConverter extends DateConverter {

    //zgodnie z DWR_DATE_FORMAT_ID w dwr-document-base.jsp
    private static final Map<String,DateFormatPattern> DATE_FORMAT_PATTERNS = ImmutableMap.<String, DateFormatPattern>builder()
                                              .put("1", new DateFormatPattern("dd-MM-yyyy", " 00:00:00"))
                                              .put("2", new DateFormatPattern("dd-MM-yyyy HH:mm:ss", ""))
                                              .put("3", new DateFormatPattern("dd-MM-yyyy HH", ":00:00"))
                                              .put("4", new DateFormatPattern("dd-MM-yyyy HH:mm", ":00"))
                                              .build();

    private static final ThreadLocal<DateFormat> dateFormatThreadLocal = new ThreadLocal<DateFormat>();
    private static final ThreadLocal<Calendar> callendarThreadLocal = new ThreadLocal<Calendar>();

    public OutboundVariable convertOutbound(Object data, OutboundContext outctx) throws MarshallException {
        Calendar cal;
        if (data instanceof Calendar) {
            cal = (Calendar) data;
        } else if (data instanceof Date) {
            Date date = (Date) data;
            cal = getCalendarInstance();
            cal.setTime(date);
        } else {
            throw new MarshallException(data.getClass());
        }

        //new Date(year, month, day, hours, minutes, seconds, milliseconds)
        String jsCode = new StringBuilder("new Date(")
                .append(cal.get(Calendar.YEAR)).append(",")
                .append(cal.get(Calendar.MONTH)).append(",")
                .append(cal.get(Calendar.DAY_OF_MONTH)).append(",")
                .append(cal.get(Calendar.HOUR_OF_DAY)).append(",")
                .append(cal.get(Calendar.MINUTE)).append(",")
                .append(cal.get(Calendar.SECOND)).append(",")
                .append(cal.get(Calendar.MILLISECOND)).append(")")
                .toString();

        return new NonNestedOutboundVariable(jsCode);
    }

    @Override
    public Object convertInbound(Class<?> paramType, InboundVariable data) throws ConversionException {
        String value = stringDecode(data.getValue());

        //Date date = parseDate(value, getDateFormatInstance());
        Date date = parseDate(value);

        if (value.trim().equals(ProtocolConstants.INBOUND_NULL) || date == null) {
            return null;
        }

        try {

            if (paramType == Date.class) {
                return date;
            } else if (paramType == java.sql.Date.class) {
                return new java.sql.Date(date.getTime());
            } else if (paramType == Time.class) {
                return new Time(date.getTime());
            } else if (paramType == Timestamp.class) {
                return new Timestamp(date.getTime());
            } else if (paramType == Calendar.class) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                return cal;
            } else {
                throw new MarshallException(paramType);
            }
        } catch (MarshallException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new MarshallException(paramType, ex);
        }
    }

    public static String stringDecode(String value) {
        try {
            return URLDecoder.decode(value, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return value;
        }
    }

    private Calendar getCalendarInstance() {
        Calendar cal = callendarThreadLocal.get();
        if (cal == null) {
            cal = Calendar.getInstance();
            callendarThreadLocal.set(cal);
        }
        return cal;
    }

    private Date parseDate(String value) {
        try {
            return org.apache.commons.lang.time.DateUtils.parseDate(value, DateUtils.JS_DATE_WITH_OPTIONAL_TIME);
        } catch (ParseException e) {
            return null;
        }
    }

    private Date parseDate(String value, DateFormat parser) {
        Date date = null;
        if (!Strings.isNullOrEmpty(value)) {
            String[] dateRaw = value.split("#");
            if (dateRaw.length == 2) {
                DateFormatPattern datePattern = DATE_FORMAT_PATTERNS.get(dateRaw[0]);

                if (datePattern != null) {
                    try {
                        date = parser.parse(dateRaw[1]+datePattern.getSuffixTofullDate());
                    } catch (ParseException e) {
                    }
                }

            }
        }

        return date;
    }

    private DateFormat getDateFormatInstance() {
        DateFormat parser = dateFormatThreadLocal.get();
        if (parser == null) {
            parser = (DateFormat) DateUtils.jsDateTimeWithSecondsFormat.clone();
            dateFormatThreadLocal.set(parser);
        }
        return parser;
    }

    private static class DateFormatPattern {
        private String pattern;
        private String suffixTofullDate;

        DateFormatPattern(String pattern, String suffixTofullDate) {
            this.pattern = pattern;
            this.suffixTofullDate = suffixTofullDate;
        }

        public String getPattern() {
            return pattern;
        }

        public String getSuffixTofullDate() {
            return suffixTofullDate;
        }
    }
}