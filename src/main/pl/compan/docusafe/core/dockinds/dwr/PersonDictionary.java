package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.UserToBriefcase;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

/**
 * 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class PersonDictionary extends DwrDictionaryBase {
	private boolean isSender;
	private static final Logger log = LoggerFactory.getLogger(PersonDictionary.class);
	private boolean uniqePersonNipRegonPesel = AvailabilityManager.isAvailable("uniqePersonNipRegonPesel");

	Map<String, Object> toMap(Person person, String type) {
		Map<String, Object> ret = Maps.newHashMap();
		ret.put("id", person.getId());
		if (person.getTitle() != null)
			ret.put(type + "_title".toUpperCase(), person.getTitle());
		if (person.getFirstname() != null)
			ret.put(type + "_firstname".toUpperCase(), person.getFirstname());
		if (person.getLastname() != null)
			ret.put(type + "_lastname".toUpperCase(), person.getLastname());
		if (person.getAddress() != null)
			ret.put(type + "_address".toUpperCase(), person.getAddress());
		if (person.getCountry() != null)
			ret.put(type + "_country".toUpperCase(), person.getCountry());
		if (person.getStreet() != null)
			ret.put(type + "_street".toUpperCase(), person.getStreet());
		if (person.getEmail() != null)
			ret.put(type + "_email".toUpperCase(), person.getEmail());
		if (person.getFax() != null)
			ret.put(type + "_fax".toUpperCase(), person.getFax());
		if (person.getPesel() != null)
			ret.put(type + "_pesel".toUpperCase(), person.getPesel());
		if (person.getNip() != null)
			ret.put(type + "_nip".toUpperCase(), person.getNip().replace("-", "").replace(" ", ""));
		if (person.getRegon() != null)
			ret.put(type + "_regon".toUpperCase(), person.getRegon());
		if (person.getOrganizationDivision() != null)
			ret.put(type + "_organizationDivision".toUpperCase(), person.getOrganizationDivision());
		if (person.getOrganization() != null)
			ret.put(type + "_organization".toUpperCase(), person.getOrganization());
		if (person.getLocation() != null)
			ret.put(type + "_location".toUpperCase(), person.getLocation());
		if (person.getZip() != null)
			ret.put(type + "_zip".toUpperCase(), person.getZip());
		if (person.getLparam() != null)
			ret.put(type + "_lparam".toUpperCase(), person.getLparam());
		if (person.getWparam() != null)
			ret.put(type + "_wparam".toUpperCase(), person.getWparam());

		if (person.getAdresSkrytkiEpuap() != null)
			ret.put(type + "_adresskrytkiepuap".toUpperCase(), person.getAdresSkrytkiEpuap());
		if (person.getIdentifikatorEpuap() != null)
			ret.put(type + "_identifikatorepuap".toUpperCase(), person.getIdentifikatorEpuap());
		if (person.getPhoneNumber() != null)
			ret.put(type + "_phoneNumber".toUpperCase(), person.getPhoneNumber());

		return ret;
	}

	/**
	 * @param values
	 *            Je�li zawiera klucz <code>id</code> o warto�ci r�nej od
	 *            <code>null</code>, to zwracamy jeden wynik, je�li nie ma
	 *            klucza <code>id</code> - normalne wyszukiwanie
	 */
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
		List<Map<String, Object>> ret = Lists.newArrayList();

		FieldData idFieldData = values.get(ID);

		String type = null;
		if (isSender())
			type = "SENDER";
		else
			type = "RECIPIENT";

		if (idFieldData != null && !"".equals(idFieldData.getStringData())) // zwracamy
																			// jeden
																			// wpis
		{
			Long personId = Long.valueOf(idFieldData.getStringData());
			Person p = Person.find(personId.longValue());
			String t = p.getDictionaryType();
			if (t != null)
				type = t.toUpperCase();
			ret.add(toMap(p, type));
		} else// wyszukiwanie
		{
			for (String cn : values.keySet()) {
				log.info(cn + " = " + values.get(cn).getData());
			}
			log.info("limit: " + limit);
			log.info("offset: " + offset);

			QueryForm qf = new QueryForm(offset, limit);

			for (Map.Entry<String, FieldData> entry : values.entrySet()) {
				if (!"".equals(entry.getValue().getData())) {
					int index = entry.getKey().indexOf("_");
					Object dataValue = entry.getValue().getData();
					if (entry.getKey().substring(index + 1).toLowerCase().equalsIgnoreCase("nip"))
						dataValue = ((String) dataValue).replace("-", "").replace(" ", "");
					qf.addProperty(entry.getKey().substring(index + 1).toLowerCase(), dataValue);
				}
			}
			qf.addOrderAsc("organization");
			qf.addProperty("DISCRIMINATOR", "PERSON"); 
			if (this.getOldField() != null && this.getOldField() instanceof DocumentPersonField && ((DocumentPersonField) this.getOldField()).getNotNullableFields() != null) {
				qf.addProperty(Person.NOT_NULLABLE_FIELDS_PARAM, ((DocumentPersonField) this.getOldField()).getNotNullableFields());
			}
		
			for (Person p : Person.search(qf).results()) {
				ret.add(toMap(p, getName()));
			}
			log.info("PersonDictionary.search={}", ret);
		}
			
		
		return ret;
	}

	private List<Person> validateUniqePersonData(Map<String, FieldData> values) throws EdmException
	{
		
		List<Person> critList = new ArrayList<Person>();
		QueryForm qf = new QueryForm(0, 0);
		for (Map.Entry<String, FieldData> entry : values.entrySet())
		{
			if (!"".equals(entry.getValue().getData()))
			{
				int index = entry.getKey().indexOf("_");
				Object dataValue = entry.getValue().getData();
				if (entry.getKey().substring(index + 1).toLowerCase().equalsIgnoreCase("nip"))
					dataValue = ((String) dataValue).replace("-", "").replace(" ", "");
				qf.addProperty(entry.getKey().substring(index + 1).toLowerCase(), dataValue);
			}
		}
		if (qf.hasProperty("pesel") || qf.hasProperty("nip") || qf.hasProperty("regon"))
		{
			Criteria criteria = DSApi.context().session().createCriteria(Person.class);
			Disjunction or = Restrictions.disjunction();

			if (qf.hasProperty("pesel"))
				or.add(Restrictions.eq("pesel", qf.getProperty("pesel").toString().trim()));
		
			if (qf.hasProperty("nip"))
				or.add(Restrictions.eq("nip", qf.getProperty("nip").toString().trim()));
		
			if (qf.hasProperty("regon"))
				or.add(Restrictions.eq("regon", qf.getProperty("regon").toString().trim()));
			
			criteria.add(or);
			critList.addAll((List<Person>)criteria.list());
			
		
		}
		return critList;

	}

	public long add(Map<String, FieldData> values) {
		
		log.info("Start ADD {}", values);
		Person person = new Person();
	
		String type = null;
		if (isSender())
			type = "SENDER";
		else
			type = "RECIPIENT";

		setPersonValue(getName(), person, values);

		try {
			
			List<Map<String, Object>> list = (List<Map<String, Object>>) search(values, 0, 0);
			if (uniqePersonNipRegonPesel && list.isEmpty() &&  !validateUniqePersonData(values).isEmpty())
				return -1;//return -10;
			else if (list.isEmpty()) {
				try {
					boolean inTransaction = DSApi.context().inTransaction();
					if(!inTransaction) {
						DSApi.context().begin();
					}
					person.create();
					if(!inTransaction) {
						DSApi.context().commit();
					}
				} catch (EdmException e1) {
					log.error(e1.getMessage());
					return -1;
				}
			} else {
				return -1;
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return -1;
		}
		return person.getId();
	}

	public static void setPersonValue(String type, Person person, Map<String, FieldData> values) {
		if (StringUtils.isEmpty(type)) {
			type = "";
		} else {
			type += "_";
		}

		for (String cn : values.keySet()) {
			if (!StringUtils.isEmpty(values.get(cn).getStringData())) {
				if (cn.equalsIgnoreCase(type + "title"))
					person.setTitle(values.get(type + "title".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "firstname"))
					person.setFirstname(values.get(type + "firstname".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "lastname"))
					person.setLastname(values.get(type + "lastname".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "organization"))
					person.setOrganization(values.get(type + "organization".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "street"))
					person.setStreet(values.get(type + "street".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "location"))
					person.setLocation(values.get(type + "location".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "zip"))
					person.setZip(values.get(type + "zip".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "country"))
					person.setCountry(values.get(type + "country".toUpperCase()).getData().toString());
				if (cn.equalsIgnoreCase(type + "email"))
					person.setEmail(values.get(type + "email".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "fax"))
					person.setFax(values.get(type + "fax".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "nip"))
					person.setNip(values.get(type + "nip".toUpperCase()).getStringData().replace("-", "").replace(" ", ""));
				if (cn.equalsIgnoreCase(type + "pesel"))
					person.setPesel(values.get(type + "pesel".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "regon"))
					person.setRegon(values.get(type + "regon".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "lparam"))
					person.setLparam(values.get(type + "lparam".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "organizationDivision"))
					person.setOrganizationDivision(values.get(type + "organizationDivision".toUpperCase()).getStringData());

				if (cn.equalsIgnoreCase(type + "adresskrytkiepuap"))
					person.setAdresSkrytkiEpuap(values.get(type + "adresskrytkiepuap".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "identifikatorepuap"))
					person.setIdentifikatorEpuap(values.get(type + "identifikatorepuap".toUpperCase()).getStringData());
				if (cn.equalsIgnoreCase(type + "phoneNumber")) {
					person.setPhoneNumber(values.get(type + "phoneNumber".toUpperCase()).getStringData());
				}
			}
			person.setDictionaryGuid("rootdivision");
		}
	}

	public static void prepareFieldNamesForBasePerson(String prefix, Map<String, FieldData> values) {
		Map<String, FieldData> entryValues = new HashMap<String, FieldData>();

		entryValues.putAll(values);
		values.clear();
		for (String cn : entryValues.keySet()) {
			if (cn.length() < prefix.length())
				values.put(prefix + "_" + cn, entryValues.get(cn));
			else if (cn.length() >= prefix.length() && !cn.substring(0, prefix.length()).equals(prefix))
				values.put(prefix + "_" + cn, entryValues.get(cn));
			else
				values.put(cn, entryValues.get(cn));
		}
	}

	public static void prepareFieldNamesForSenderPerson(Long personId, String prefix, Map<String, FieldData> values) {
		if (values.put(prefix + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "SENDER")) == null)
			values.remove("DISCRIMINATOR");
		if (values.put(prefix + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0")) == null)
			values.remove("ANONYMOUS");
		if (values.put(prefix + "_BASEPERSONID", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, personId)) == null)
			values.remove("BASEPERSONID");
	}

	public int remove(String id) {
		try {
			DSApi.context().begin();
			Person s = Person.find(Long.parseLong(id));
			if (s.getDocumentId() != null) {
				try {
					OfficeDocument.find(s.getDocumentId()).setSenderId(null);
				} catch (EdmException e) {
					log.error(e.toString());
				}
			}
			s.delete();
			DSApi.context().commit();
			return 1;
		} catch (EdmException e) {
			log.error(e.getMessage());
			return -1;
		}
	}

	public int update(Map<String, FieldData> values) {

		try {
			DSApi.context().begin();
			Long id = Long.valueOf(values.get("id").getStringData());
			Person temp = Person.find(id.longValue());
			setPersonValue(getName(), temp, values);
			DSApi.context().commit();
			return temp.getId().intValue();
		} catch (EdmException ed) {
			log.error(ed.getMessage(), ed);
			return -1;
		}
	}

	public boolean isSender() {
		return isSender;
	}

	public void setSender(boolean isSender) {
		this.isSender = isSender;
	}
}
