package pl.compan.docusafe.core.dockinds.dwr;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * Slownik dla u�ytkownik�w w systemie. Usuwa do wyboru obecnie pracuj�cego na dokumencie.
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class UserDictionary extends DwrDictionaryBase
{

	
	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDWRSearch,
			Map<String, FieldData> dockindFields) throws EdmException
	{
		List<Map<String, Object>> results = super.search(values, limit, offset,fromDWRSearch, dockindFields);

		Long loggedId = DSApi.context().getDSUser().getId();
		if(results == null)
			return null;
		ListIterator<Map<String, Object>> it = results.listIterator();
		
		//usuwa zalogowanego z listy
		while(it.hasNext())
		{
			Map<String, Object> columns = it.next();
			if(columns.containsKey("id") && columns.get("id") != null && columns.get("id").toString().equals(loggedId.toString()))
			{
				it.remove();
			}
		}
		
		return results;
	}
	
	
	public long add(Map<String, FieldData> values)
	{
		return -1;
	}

	public int remove(String id)
	{
		return -1;
	}

	public int update(Map<String, FieldData> values)
	{
		return -1;
	}
}
