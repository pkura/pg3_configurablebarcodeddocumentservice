package pl.compan.docusafe.core.dockinds.dwr;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

public class FieldData {
    /** @var warto�c pola */
	private Object data;
	private Field.Type type;
	private boolean sender = false;
	
	public FieldData() {}
	
	public FieldData(Field.Type type, Object data)
	{
		this.data = data;
		this.type = type;
	}

    public FieldData(String type, Object data)
    {
        this.data = data;
        this.type = getFieldType(type);
    }
	
	private Logger log = LoggerFactory.getLogger("basia"); 
	
	/** typ danych trzymanych w data */
	public Field.Type getType() {
		return type;
	}

	public void setType(Field.Type type) {
		this.type = type;
	}

	public String getStringData() {
		return data != null ? data.toString() : null;
	}

	public void setStringData(String data) {
		this.data = data;
	}
	
	public void setIntegerData(Integer data) {
		this.data = data;
	}
	
	public Integer getIntegerData() {
		return (Integer) data;

	}
	
	public void setLinkData(LinkValue data) {
		this.data = data;
	}
	
	public LinkValue getLinkData() {
		return (LinkValue) data;

	}
	
	public void setLongData(Long data) {
		this.data = data;
	}
	
	public Long getLongData() {
		if(data instanceof String)
			return Long.parseLong((String) data);
        else if (data instanceof Integer)
            return ((Integer)data).longValue();
		return (Long) data;
	}
	
	/** Dodane zabezpieczenie w razie gdyby przypadkiem obiekt by� stringiem*/
	public Date getDateData() {
		if(data instanceof String)
		{
			return DateUtils.parseJsDate(data.toString());
		}
		else
			return (Date) this.data;
	}
	
	public Date getTimestampData() {
		if(data instanceof String) {
			return DateUtils.parseJsDateWithOptionalTime(data.toString());
		} else {
			return (Date)this.data;
		}
	}
	
	public void setDateData(Date data) {
		this.data = data;
	}
	
	public void setEnumValuesData(EnumValues data) {
		this.data = data;
	}
	
	public EnumValues getEnumValuesData() {
		return (EnumValues) data;
	}

	public Boolean getBooleanData() {
		if (data == null)
			return null;
		if (data.toString().equalsIgnoreCase("on")) {
		    return true;
		}
		return Boolean.parseBoolean(data.toString());
	}
	
	public void setBooleanData(Boolean data) {
		this.data = data;
	}
	
	public void setMoneyData(BigDecimal data) {
		this.data = data;
	}
	
	public void setHtmlData(String data) {
		this.data = data;
	}
	
	public String getHtmlData() {
		return (String)this.data;
	}
	
	public void setHtmlEditorData(HtmlEditorValue data) {
		this.data = data;
	}
	
	public HtmlEditorValue getHtmlEditorData() {
		return (HtmlEditorValue)this.data;
	}
	
	public BigDecimal getMoneyData() {
		if(data == null || data.equals(""))
		{
			return null;
		}
		else if(data instanceof String)		{
			return new BigDecimal((String)data);
		}
		else {
			return (BigDecimal)data;
		}
	}
	
	public Object getData() {

		if(data != null && data instanceof EnumValues){
			if (((EnumValues)data).getSelectedOptions().size()>0 && StringUtils.isNotEmpty(((EnumValues)data).getSelectedOptions().get(0)))
				return ((EnumValues)data).getSelectedOptions().get(0);
			else
				return null;
		}
		else {
			return data;
		}
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public void setTextareaData(String data) {
		this.data = data;
	}
	
	public String getTextareaData() {
		return (String)this.data;
	}
	
	public void setDictionaryData(Map<String, FieldData> data) {
		this.data = data;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, FieldData> getDictionaryData() {
		return (Map<String, FieldData>)this.data;
	}
	
	public void setAttachmentValueData(AttachmentValue data) {
		this.data = data;
	}
	
	public AttachmentValue getAttachmentValueData() {
		return (AttachmentValue) this.data;
	}

	public void setButtonData(ButtonValue data) {
		this.data = data;
	}

	public ButtonValue getButtonData() {
		return (ButtonValue) this.data;
	}

	public static Field.Type getFieldType(String name)
	{
		if(name.equals("dataBase") || name.equals("enum"))
		{
			return Field.Type.ENUM;
		}
		if(name.equals("attachment"))
		{
			return Field.Type.ATTACHMENT;
		}
		if(name.equals("dictionary"))
		{
			return Field.Type.DICTIONARY;
		}
		else if(name.equals("string"))
		{
			return Field.Type.STRING;
		}
		else if(name.equals("money"))
		{
			return Field.Type.MONEY;
		}
		else if(name.equals("bool"))
		{
			return Field.Type.BOOLEAN;
		}
		else if(name.equals("long"))
		{
			return Field.Type.LONG;
		}
		else if(name.equals("integer"))
		{
			return Field.Type.INTEGER;
		}
		else if(name.equals("timestamp"))
		{
			return Field.Type.TIMESTAMP;
		}
		else if(name.equals("date"))
		{
			return Field.Type.DATE;
		}
		else if(name.equals("document-date"))
		{
			return Field.Type.DATE;
		}
		else if(name.equals("link"))
		{
			return Field.Type.LINK;
		}
		else if(name.equals("double"))
		{
			return Field.Type.MONEY;
		}
		else if(name.equals("HTML"))
		{
			return Field.Type.HTML;
		}
		else if(name.equals("HTML-EDITOR"))
		{
			return Field.Type.HTML_EDITOR;
		}
		else if(name.equals("document-out-zpo-date"))
		{
			return Field.Type.DATE;
		}
		else if(name.equals(pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_OUT_ZPO))
		{
			return Field.Type.BOOLEAN;
		}
        else if(name.equals(pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_OUT_ADDITIONAL_ZPO))
        {
            return Field.Type.BOOLEAN;
        }
		else if(name.equals("dsuser"))
		{
			return Field.Type.ENUM;
		}
        else if(name.equals("dsdivision"))
        {
            return Field.Type.ENUM;
        }
		else if(name.equals(pl.compan.docusafe.core.dockinds.field.Field.DOCUMENT_OUT_GAUGE))
		{
			return Field.Type.ENUM;
		}
        else if (name.equals(pl.compan.docusafe.core.dockinds.field.Field.MAIL_WEIGHT))
        {
            return Field.Type.ENUM;
        }
        else if (name.equals(pl.compan.docusafe.core.dockinds.field.Field.CACHEABLE_SOURCE_ENUM))
        {
            return Field.Type.ENUM;
        }
        else {
            return null;
        }
    }
	
	public void getData(PreparedStatement ps, int index) throws SQLException
	{
		if (data == null || data.toString().isEmpty())
		{
			ps.setObject(index, null);
			return;
		}
		switch (getType()) {
			case LONG:
				ps.setLong(index, getLongData());
				break;
			case STRING: 
				ps.setString(index, getStringData().trim());
				break;
			case TEXTAREA:
				ps.setString(index, getTextareaData().trim());
				break;
			case BOOLEAN:
				ps.setBoolean(index, getBooleanData());
				break;
			case ENUM:
				ps.setString(index, (String)getData());
				break;
			case MONEY:
				ps.setBigDecimal(index, getMoneyData());
				break;
			case DATE:
				java.sql.Date sqlDate = new java.sql.Date(getDateData().getTime());
				ps.setDate(index, sqlDate);
				break;
			case TIMESTAMP:
				log.debug("TIMESTAMP: {}", getData());
				java.sql.Timestamp sqlTime = new java.sql.Timestamp(getTimestampData().getTime());
				ps.setTimestamp(index, sqlTime);
				break;
			case INTEGER:
				ps.setInt(index, Integer.valueOf(getStringData()));
				break;
			case ATTACHMENT:
				ps.setLong(index, getAttachmentValueData().getAttachmentId());
				break;
			default:
				ps.setObject(index, getData());
				break;
				
		}
	}
	
	@Override
	public String toString() {
		if (data == null)
			return "null";
		switch (getType()) {
			case STRING:
			case MONEY:
			case INTEGER:
			case DATE:
			case TEXTAREA:
			case HTML_EDITOR:
            case HTML:
				return data.toString();
			case ENUM:
			case ENUM_MULTIPLE:
				return data != null ? data.toString() : "NULL"; 
			default:
				if (type == null)
				return "no definition toString for type: null" ;
				else return "no definition toString for type: " + type.toString() ;
		}
	}

	/**
	 * ��danie DWR zosta�o wykonane po zmianie warto�ci przez u�ytkownika
	 * @return
	 */
	public boolean isSender() {
		return sender;
	}

	public void setSender(boolean sender) {
		this.sender = sender;
	}	
	
	/**
	 * Puste dla enuma to znaczy �e �adna opcja nie jest zaznaczona
	 * @return
	 */
	public boolean isEmpty() {
		switch(type) {
		case ENUM:
		case ENUM_AUTOCOMPLETE:
		case ENUM_MULTIPLE:
			if(data != null) {
				EnumValues enumValues = getEnumValuesData();
				if(enumValues != null && !enumValues.getSelectedId().equals("")) {
					return false;
				}
			} 
			return true;
		default:
			throw new UnsupportedOperationException();
		}
	}
}

