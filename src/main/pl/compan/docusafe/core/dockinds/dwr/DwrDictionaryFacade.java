package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.SchemeField;
import pl.compan.docusafe.core.regtype.EnumField;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

public class DwrDictionaryFacade
{
	private static final Logger log = LoggerFactory.getLogger(DwrDictionaryFacade.class);
	public static final String FILTER_PREFIX = "DIC_FILTER_";
	/**
	 * Mapa wszystkich s�ownik�w: <br/>
	 * nazwa s�ownika (name) --> s�ownik implementuj�cy
	 * {@link pl.compan.docusafe.core.dockinds.dwr.DwrSearchable DwrSearchable}
	 */
	public static Map<String, DwrDictionaryBase> dictionarieObjects = Maps.newHashMap();
	public static Map<String, Map<String, DwrDictionaryBase>> documentsDictionarieObjects = Maps.newHashMap();
	
	public int dwrSave(String dictionaryName, Map<String, FieldData> values)
	{
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		try
		{
			DSApi.open(AuthUtil.getSubject(req));
            DwrDictionaryBase dwrDictionaryBase = getDwrDictionaryBase(req, dictionaryName);
            dwrDictionaryBase.filterBeforeUpdateValues(values);
            return dwrDictionaryBase.update(values);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return -1;
		}
		finally
		{
			DSApi._close();
		}
	}

	public long dwrAdd(String dictionaryName, Map<String, FieldData> values) throws EdmException
	{
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		long id = -1;
		try
		{
            DSApi.open(AuthUtil.getSubject(req));
			id = getDwrDictionaryBase(req, dictionaryName).add(values);
		}
		catch (EdmException e)
		{
			id = -1;
			log.error(e.getMessage(), e);
			throw e;
			//return -1;
		}
		finally
		{
			DSApi._close();
		}
		
		return id;
	}

	public List<Map<String, Object>> dwrSearch(String dictionaryName, Map<String, FieldData> values, int limit, int offset)
	{
		boolean open = false;
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.open(AuthUtil.getSubject(req));
				open = true;
			}
			for (Field field : getDwrDictionaryBase(req, dictionaryName).getFields())
			{
				if (field.getType().equals(Field.Type.ENUM) && values.containsKey(field.getCn()) && values.get(field.getCn()).getData()!=null)
				{
					String id = values.get(field.getCn()).getEnumValuesData().getSelectedOptions().get(0);
					FieldData idData = new FieldData();
					idData.setData(id);
					idData.setType(Field.Type.STRING);
					values.put(field.getCn(), idData);
				}
			}

			//	pobieramy nazwe aktualnie przetwarzanego dockinda
			//	uwaga: wystepuje rzadki przypadek, w ktorym atrybut dwrSession nie istnieje	
			String dockindCn = "";
			if((Map<String, String>)req.getSession().getAttribute("dwrSession") != null)
				dockindCn = ((Map<String, String>)req.getSession().getAttribute("dwrSession")).get("dockindKind");
			
			Object recentlyProcessedDockind = req.getSession().getAttribute("RECENTLY_PROCESSED_DOCKIND");

			if(dockindCn != null){
				// jesli jest to pierwsze wyszukanie rekordow dla pol aktualnie przetwarzanego dockinda
				if(recentlyProcessedDockind == null){
					populateFilteringDictionaries(req, dictionaryName, values);
				// jesli uzytkownik zmienil typ przetwarzanego dokumentu	
				}else if(!((String)recentlyProcessedDockind).equals(dockindCn)){
					// czyscimy sesje z filtrujacych slownikow powiazanych z poprzednio przetwarzanym dockindem
					removeFilteringDictionaries(req);
					populateFilteringDictionaries(req, dictionaryName, values);
				// jesli uzytkownik ponownie wyszukuje rekordy nie zmieniajac typu przetwarzanego dokumentu
				}else{
					// jesli slownik jest filtrujacym slownikiem
					if(req.getSession().getAttribute(FILTER_PREFIX+dictionaryName) != null){
						req.getSession().setAttribute(FILTER_PREFIX+dictionaryName, values);
					}
				}
				req.getSession().setAttribute("RECENTLY_PROCESSED_DOCKIND", dockindCn);
			}
			return getDwrDictionaryBase(req, dictionaryName).search(removeNullData(values), limit, offset, true,
					(Map<String, FieldData>) ((Map<String, Object>) req.getSession().getAttribute("dwrSession")).get("FIELDS_VALUES"));
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return new ArrayList<Map<String, Object>>();
		}
		finally
		{
			if (open)
			{
				DSApi._close();
			}
		}
	}
	
	/**
	 * Metoda zapisuje w sesji nazwy filtruj�cych s�ownik�w i zapisuje warto�ci p�l aktualnego s�ownika, je�li jest on s�ownikiem filtruj�cym 
	 * @param request
	 * @param dictionaryName nazwa aktualnie przetwarzanego s�ownika
	 * @param values warto�ci p�l aktualnie przetwarzanego s�ownika
	 */
	private void populateFilteringDictionaries(HttpServletRequest request, String dictionaryName, Map<String, FieldData> values){
		pl.compan.docusafe.core.dockinds.field.Field dicField = null;
		String filteringDictionary = "";
		for(DwrDictionaryBase dictionary: dictionarieObjects.values()){
			dicField = dictionary.getOldField();
			if(dicField instanceof DictionaryField && (filteringDictionary = ((DictionaryField)dicField).getFilteringDictionaryCn()) != null){
				// jesli aktualnie przetwarzany slownik jest slownikiem filtrujacym to przechowujemy aktualne wartosci jego pol
				// w przeciwnym razie zapisujemy w sesji informacje o wystepujacych w dockindzie filtrujacych slownikach
				if(dictionaryName.equals(filteringDictionary))
					request.getSession().setAttribute(FILTER_PREFIX+filteringDictionary, values);
				else
					request.getSession().setAttribute(FILTER_PREFIX+filteringDictionary, new HashMap<String, FieldData>(1));
			}
		}
	}

	private void removeFilteringDictionaries(HttpServletRequest request) throws EdmException{
		Enumeration attributesNames = request.getSession().getAttributeNames();
		String name;
		while(attributesNames.hasMoreElements()){
			name = (String)attributesNames.nextElement();
			if(name.contains(FILTER_PREFIX)){
				request.getSession().removeAttribute(name);
			}
		}
	}
	
	private Map<String, FieldData> removeNullData(Map<String, FieldData> values)
	{
		Map<String, FieldData> tmp = new HashMap<String, FieldData>();
		for (String cn : values.keySet())
			if (values.get(cn).getData() != null)
				tmp.put(cn, values.get(cn));
		return tmp;
	}

	public int dwrRemove(String dictionaryName, String id)
	{
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		try
		{
			DSApi.open(AuthUtil.getSubject(req));
			return getDwrDictionaryBase(req, dictionaryName).remove(id);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return -1;
		}
		finally
		{
			DSApi._close();
		}
	}

	/**
	 * Rozwi�zuje zale�no�ci mi�dzy warto�ciamy p�l dla wpis�w w s�ownikach
	 * wielowarto�ciowych. To znaczy: na podstawie podanych warto�ci p�l
	 * jednego wpisu s�ownika wielowarto�ciowego (<code>values</code>) zwraca
	 * wyliczone warto�ci dla tych samych p�l.
	 * 
	 * @param dwrParams
	 *             Parametry DWR - te same co w
	 *             {@link DwrBase#getFieldsList(Map)}
	 * @param dictionaryName
	 *             Nazwa s�ownika
	 * @param values
	 *             Warto�ci p�l przes�ane z DWR (z formatki) w postaci mapy:
	 *             cn_pola -> warto�� pola (bez indeks�w p�l)
	 * @return Wyliczone warto�ci p�l w postaci mapy cn_pola -> warto�� pola
	 *         (bez indeks�w p�l)
	 */
	public Map<String, Object> getMultipleFieldsValues(Map<String, String> dwrParams, String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> result;
		String dockindCn = dwrParams.get("dockind");
		Long documentId = null;
		if(dwrParams.get("documentId") != null && dwrParams.get("documentId").length() > 0)
			documentId = Long.parseLong(dwrParams.get("documentId"));
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		try
		{
			DSApi.open(AuthUtil.getSubject(req));
			DSApi.context().begin();
			//nie wiem czemu to tutaj jest tak zrobione, na wszelki wypadek zostawiam dla PCZ w elsie jak nie ma docId to staram sie pobrac z sesji. powinna zostac wywalona metoda z mniejsza ilo�ci� atrybut�w.
			if(WebContextFactory.get().getSession().getAttribute("dwrSession") != null && dwrParams.containsKey("dockind") && dwrParams.get("dockind").equals("pcz_costinvoice")){
				Map<String, Object> mapaVal =  ((Map<String, Object>) WebContextFactory.get().getSession().getAttribute("dwrSession"));
				Object xxx = mapaVal.get("documentId");
				result = DocumentKind.findByCn(dockindCn).logic().setMultipledictionaryValue(dictionaryName, values, getFieldsValues(dictionaryName, String.valueOf(xxx)),Long.parseLong(String.valueOf(xxx)));
			}
			else
			{
                HttpSession session = WebContextFactory.get().getSession();
                Map<String,Object> fieldsValues = null;
                if (session != null) {
                    Map<String,Object> dwrSession = (Map<String, Object>) session.getAttribute("dwrSession");
                    if (dwrSession != null) {
                        fieldsValues = (Map<String, Object>)dwrSession.get("FIELDS_VALUES");
                    }
                }
				if(documentId == null)
				{
					Map<String, Object> mapaVal =  ((Map<String, Object>) WebContextFactory.get().getSession().getAttribute("dwrSession"));
					if(mapaVal != null && mapaVal.get("documentId") != null)
						documentId = Long.parseLong(String.valueOf(mapaVal.get("documentId")));
				}
                //to nie dzia�a prawid�owo (to 10 linii wy�ej, te�), getFieldsValues(cn s�ownika oraz id wpisu, nie id dokumentu)
				result = DocumentKind.findByCn(dockindCn).logic().setMultipledictionaryValue(dictionaryName, values, fieldsValues, documentId);
			}
			DSApi.context().commit();
		}
		catch (EdmException e)
		{
			DSApi.context()._rollback();
			log.error(e.getMessage(), e);
			result = null;
		}
		finally
		{
			DSApi._close();
		}

		return result;
	}

    /**
	 * @param dictionaryName
	 * @param id
	 *  , 
	 * @return 
	 */
    public Map<String, Object> getFieldsValues(String dictionaryName, String id) {
    	boolean open = false;
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.open(AuthUtil.getSubject(req));
				open = true;
			}

            DwrDictionaryBase dwrDictionaryBase = getDwrDictionaryBase(req, dictionaryName);
            Map<String,Object> dictionaryFieldsValues = Maps.newHashMap();
            Map<String, Object> fieldsValues = null;
            Map<String, Object> documentValues = null;

			HttpSession session = WebContextFactory.get().getSession();

            if (session.getAttribute("dwrSession") != null) {
                documentValues = (Map<String, Object>) ((Map<String, Object>) session.getAttribute("dwrSession")).get("FIELDS_VALUES");
                if(dwrDictionaryBase.filterBeforeGetValues(dictionaryFieldsValues, id, documentValues)) {
                    dictionaryFieldsValues = dwrDictionaryBase.getValues(id, documentValues);
                }
            } else {
                if(dwrDictionaryBase.filterBeforeGetValues(dictionaryFieldsValues, id, null)) {
                    dictionaryFieldsValues = dwrDictionaryBase.getValues(id);
                }
            }

            if (session.getAttribute("dwrSession") != null){
                Map<String, FieldData> selectedValues = (Map<String, FieldData>) ((Map<String, Object>) session.getAttribute("dwrSession")).get(DwrSelectedValues.SELECTED_FIELDS_VALUES);
                fieldsValues = DwrSelectedValues.getValuesForDictionaryDocument(dictionaryName, id, selectedValues);
            }
            if (fieldsValues==null || fieldsValues.isEmpty())
                fieldsValues = documentValues;


            dwrDictionaryBase.filterAfterGetValues(dictionaryFieldsValues, id, fieldsValues, documentValues);

            return dictionaryFieldsValues;
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return Collections.EMPTY_MAP;//retrn null;
		}
		finally
		{
			if (open)
			{
				DSApi._close();
			}
		}
	}

    private Map<String, FieldData> popValuesFromSession(HttpSession session, String key){
        Map<String, FieldData> values = (Map<String, FieldData>) session.getAttribute(key);
        if (values != null)
            session.removeAttribute(key);
        return values;
    }

    @Beta
	public List<Map<String, Object>> getFieldsValuesList(String dictionaryName, String[] id) {
        for(String str : id) {
            LoggerFactory.getLogger("tomekl").debug("getFieldsValuesList -> {} {}", dictionaryName, str);
        }

        HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
        DwrDictionaryBase base = getDwrDictionaryBase(req,dictionaryName);
        Set<String> lazyCns = new HashSet<String>();
        for(Field f : base.getFields()) {
            if(f.getLazyEnumLoad()) {
                lazyCns.add(f.getCn().replaceAll("_1",""));
            }
        }

		List<Map<String, Object>> res = new LinkedList<Map<String, Object>>();

        for(int i = 0; i < id.length; i ++) {
            Map<String, Object> fieldValues = this.getFieldsValues(dictionaryName, id[i]);
            for(String lcn : lazyCns) {
                EnumValues enumValues = (EnumValues) fieldValues.get(lcn);
                List<Map<String,String>> tmpList = new ArrayList<Map<String, String>>();
                Map<String,String> tmpMap = null;

                for(Map<String,String> m : enumValues.getAllOptions()) {
                    for(String k : m.keySet()) {
                        if(k.equals("") || k.equals(enumValues.getSelectedId())) {
                            tmpMap = new HashMap<String, String>();
                            tmpMap.put(k,m.get(k));
                            tmpList.add(tmpMap);
                        }
                    }
                }
                enumValues.setAllOptions(tmpList);
            }
            res.add(fieldValues);
		}
		
		return res;
	}

    /** **/
    public Object getFieldValue(String dictionaryName, String fieldCn) {
        return this.getFieldsValues(dictionaryName, null).get(fieldCn);
    }

	public static DwrSearchable getDictionary(pl.compan.docusafe.core.dockinds.field.Field dictionaryField, Map<String, SchemeField> fieldsScheme) throws EdmException
	{
		DwrDictionaryBase dwrDic= new DwrDictionaryBase(dictionaryField, fieldsScheme);
		return getDwrDictionary(dictionaryField.getDockindCn(), dictionaryField.getCn());
	}
	
	public static DwrDictionaryBase getDwrDictionaryBase(HttpServletRequest req, String dictionaryName) {
		if(req != null && (Map<String, String>)req.getSession().getAttribute("dwrSession") != null) {
			String dockindCn = ((Map<String, String>)req.getSession().getAttribute("dwrSession")).get("dockindKind");
			if (dockindCn != null) {
				return getDwrDictionary(dockindCn, dictionaryName);
			}
		}
		
		return getDwrDictionary(null, dictionaryName);
	}

	public static DwrDictionaryBase getDwrDictionary(String dockindCn, String dictionaryName) {
		if (dockindCn != null && documentsDictionarieObjects.get(dockindCn) != null) {
			return documentsDictionarieObjects.get(dockindCn).get(dictionaryName);
		} else {
			return dictionarieObjects.get(dictionaryName);
		}
	}

    public static Map<String, DwrDictionaryBase> getDwrDockindDictionaries(String dockindCn){
        if(StringUtils.isNotBlank(dockindCn)){
            return documentsDictionarieObjects.get(dockindCn);
        }

        return Maps.newHashMap();
    }

	static void addDwrDictionaryBase(String dockindCn, String dictionaryCn, DwrDictionaryBase dict) {
		if (DwrDictionaryFacade.documentsDictionarieObjects.get(dockindCn) == null) {
			DwrDictionaryFacade.documentsDictionarieObjects.put(dockindCn, new HashMap<String, DwrDictionaryBase>());
		}
		DwrDictionaryFacade.documentsDictionarieObjects.get(dockindCn).put(dictionaryCn, dict);
	}
}