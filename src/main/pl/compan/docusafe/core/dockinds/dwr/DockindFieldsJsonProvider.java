package pl.compan.docusafe.core.dockinds.dwr;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.EdmException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class DockindFieldsJsonProvider extends DockindFieldsProvider {
    @Override
    public Map<String, Object> fromRequest(HttpServletRequest request) throws EdmException {
        Map<String, String[]> parameters = null;
        try {
            parameters = serializeParameters(request);
        } catch (IOException e) {
            throw new EdmException("B��d przy odczycie p�l formularza", e);
        }
        MyHttpServletRequest myRequest = new MyHttpServletRequest(request, parameters);
        return super.fromRequest(myRequest);
    }

    /**
     * <p>
     *     Serialize json to map from request
     * </p>
     *
     * @param request
     * @return
     * @throws IOException
     */
    Map<String, String[]> serializeParameters(HttpServletRequest request) throws IOException {
        Map<String, String[]> parameters = new HashMap<String, String[]>();

        StringWriter writer = new StringWriter();
        IOUtils.copy(request.getInputStream(), writer, "UTF-8");
        String rawInput = writer.toString();

        Gson gson = new Gson();
        Type mapType = new TypeToken<Map<String,String>>() {}.getType();
        Map<String, String> map = gson.fromJson(rawInput, mapType);

        for(Map.Entry<String, String> entry : map.entrySet()) {
            String[] values = new String[]{entry.getValue()};
            parameters.put(entry.getKey(), values);
        }

        return parameters;
    }

    private static class MyHttpServletRequest extends HttpServletRequestWrapper {

        private final Map<String, String[]> parameters;

        public MyHttpServletRequest(HttpServletRequest request, Map<String, String[]> parameters) {
            super(request);

            parameters.putAll(request.getParameterMap());

            this.parameters = parameters;
        }

        public String getParameter(String name) {
            String[] strings = parameters.get(name);
            if(strings != null && strings.length > 0) {
                return strings[0];
            } else {
                return null;
            }
        }

        public String[] getParameterValues(String name) {
            return parameters.get(name);
        }

        public Map<String, String[]> getParameterMap() {
            return parameters;
        }

        private Map<String, String> getRequestParameters(HttpServletRequest request) {
            Map<String, String> parameters = new HashMap<String, String>();

            for(String key: request.getParameterMap().keySet()) {
                parameters.put(key, request.getParameter(key));
            }

            return parameters;
        }

    }
}
