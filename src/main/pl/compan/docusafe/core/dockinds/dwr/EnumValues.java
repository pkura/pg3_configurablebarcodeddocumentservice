package pl.compan.docusafe.core.dockinds.dwr;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnumValues implements Serializable {
    /** Mapa klucz -> nazwa */
	private List<Map<String, String>> allOptions;
    /** Lista kluczy */
	private List<String> selectedOptions;
	
	public EnumValues() {}
	
	public EnumValues(Map<String, String> values, List<String> selected) {
		this.allOptions = new ArrayList<Map<String,String>>();
		for(Map.Entry<String, String> item : values.entrySet()) {
			Map<String, String> option = new HashMap<String, String>();
			option.put(item.getKey(), item.getValue());
			this.allOptions.add(option);
		}
		this.selectedOptions = selected;
	}

	public List<Map<String, String>> getAllOptions() {
		return allOptions;
	}

	public void setAllOptions(List<Map<String, String>> values) {
		this.allOptions = values;
	}

	/**
	 * Lista wybranych opcji (dok�adniej ich kluczy)
	 * @return Klucze wybranych opcji
	 * <p>
	 * <b>UWAGA</b> mo�e zwr�ci� pust� list� lub <code>null</code>
	 */
	public List<String> getSelectedOptions() {
		return selectedOptions;
	}

    public Object provideDescription(Object value)
    {
    	return getSelectedValue();
    }
	
	public String getSelectedValue()
	{
		if(selectedOptions != null && selectedOptions.size() > 0 && allOptions != null) {
			String selectedId = selectedOptions.get(0);
			if (!selectedId.equals(""))
			{
				for (int i = 0; i < getAllOptions().size(); i++)
				{
					if (getAllOptions().get(i).get(selectedId) != null)
						return getAllOptions().get(i).get(selectedId);
				}
			}
		}
		return "";
	}
	
	public String getSelectedId()
	{
		if(selectedOptions != null && selectedOptions.size() > 0 && allOptions != null) {
			String selectedId = selectedOptions.get(0);
			return selectedId;
		}
		return "";
	}

    public void setSelectedId(String id) {
        this.setSelectedOptions(Lists.newArrayList(id));
    }
	
	public void setSelectedOptions(List<String> selected) {
		this.selectedOptions = selected;
	}
	
	public String toString() {
		String string = "allOptions: " + allOptions != null ? allOptions.toString() : "";
		string += "selectedOptions: " + selectedOptions != null ? selectedOptions.toString() : "";
		return string;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((selectedOptions == null) ? 0 : selectedOptions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnumValues other = (EnumValues) obj;
		if (selectedOptions == null)
		{
			if (other.selectedOptions != null)
				return false;
		}
		else if (!selectedOptions.equals(other.selectedOptions))
			return false;
		return true;
	}
}
