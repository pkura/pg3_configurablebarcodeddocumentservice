package pl.compan.docusafe.core.dockinds.dwr;

import pl.compan.docusafe.core.EdmException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface DwrFieldsProvider {

    Map<String, Object> fromRequest(HttpServletRequest request) throws EdmException;

}
