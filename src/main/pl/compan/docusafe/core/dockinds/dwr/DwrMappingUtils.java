package pl.compan.docusafe.core.dockinds.dwr;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.dictionary.erp.simple.utils.ReflectionUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import system.NotImplementedException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Damian on 25.04.14.
 */
public class DwrMappingUtils {

    private final static Logger log = LoggerFactory.getLogger(DwrMappingUtils.class);


    private static String dwr(String cn) {
        return "DWR_" + cn;
    }

    /**
     * Returns list of entities of given class filled with values from dockind fields
     *
     * @param values
     * @param fm
     * @param entityToDwr map: entity field name -> dockind field Name (without dictionary prefix and index suffix)
     * @param entityClass
     * @param dictionaryName name of dictionary (doesn't matter if multiple) on dockind, if null fields aren't part of dictionary
     * @return list of entities of given class filled with values from dockind fields </br>
     * if entity isn't map to multiple dictionary it returns list with one element
     * @throws EdmException
     */
    public static <T> List<T> getEntityFromDwr(Map<String, FieldData> values, FieldsManager fm, Map<String, String> entityToDwr, Class<T> entityClass, String dictionaryName) throws EdmException {
        boolean isMultiple = false;
        if(fm.getField(dictionaryName) != null){
            isMultiple = fm.getField(dictionaryName).isMultiple();
        }
        return getEntityFromDwr(values,entityToDwr,entityClass,dictionaryName,isMultiple);
    }

    /**
     * Returns list of entities of given class filled with values from dockind fields
     * @param values
     * @param entityToDwr  map: entity field name -> dockind field Name (without dictionary prefix and index suffix)
     * @param entityClass
     * @param dictionaryName name of dictionary on dockind, if null fields aren't part of dictionary
     * @param isMultiple is dictionary multiple
     * @return
     * @throws EdmException
     */
    public static <T> List<T> getEntityFromDwr(Map<String, FieldData> values, Map<String, String> entityToDwr, Class<T> entityClass, String dictionaryName, boolean isMultiple) throws EdmException {
        boolean wasOpened = false;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            List<T> entities = new ArrayList<T>();
            if(dictionaryName != null){
                return getEntityFromDictionary(values,entityToDwr,entityClass,dictionaryName, isMultiple);
            }else{
                T entity = getEntityFromFields(values, entityToDwr, entityClass);
                if(entity != null){
                    entities.add(entity);
                }
                return entities;
            }
        }catch (EdmException e){
            throw e;
        }catch(Exception e){
            throw new EdmException(e);
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }

    private static <T> List<T> getEntityFromDictionary(Map<String, FieldData> values, Map<String, String> entityToDwr, Class<T> entityClass, String dictionaryName, boolean isMultiple) throws Exception {

        if(!dictionaryName.startsWith("DWR_")){
            dictionaryName =dwr(dictionaryName);
        }
        Map<String, FieldData> dicValues = values.get(dictionaryName).getDictionaryData();

        List<T> entities = new ArrayList<T>();

        if(!isMultiple){
            T entity = getEntityFromFields(values,entityToDwr,entityClass,dictionaryName);
            if(entityToDwr.containsValue("id")){
                setIdFromDictionary(entity, dicValues,entityToDwr);
            }
            if(entity != null){
                entities.add(entity);
            }
        }else{
            Pattern pattern = Pattern.compile("(DWR_)?(.*)");
            Matcher matcher = pattern.matcher(dictionaryName);
            Map<String, FieldData> ids = null;
            if(matcher.find()){
                ids = DwrUtils.getDictionaryFields(values, matcher.group(2), "ID");
            }


            Map<String, T> rowEntities = new HashMap<String, T>();
            for(Map.Entry<String, FieldData> field : ids.entrySet()){
                String key = field.getKey().substring(field.getKey().lastIndexOf("_"));

                T entity = null;
                Long id = field.getValue().getLongData();
                if(id != null){
                    Criteria criteria = DSApi.context().session().createCriteria(entityClass);
                    criteria.add(Restrictions.idEq(id));
                    entity = (T) criteria.uniqueResult();
                }else{
                    entity = entityClass.newInstance();
                }
                rowEntities.put(key,entity);

            }
            updateEntityFromDictionaryValues(matcher.group(2), dicValues, rowEntities, entityToDwr);

            entities.addAll(rowEntities.values());
        }
        return entities;
    }

    private static <T> void updateEntityFromDictionaryValues(String dictionaryName, Map<String, FieldData> dicValues, Map<String, T> entitiesForRow, Map<String, String> entityToDwr) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        for(Map.Entry<String,T> rowEntity : entitiesForRow.entrySet()){
            for(Map.Entry<String,String> fieldToDwr : entityToDwr.entrySet()){
                Object value = getFieldValue(dicValues,dictionaryName + "_" + fieldToDwr.getValue() + rowEntity.getKey());
                setValueWithSetter(rowEntity.getValue(),fieldToDwr.getKey(), value);
            }
        }
    }

    private static <T> void setIdFromDictionary(T entity, Map<String, FieldData> dicValues, Map<String, String> entityToDwr) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String id = DwrUtils.getFieldData(dicValues, "id", String.class);
        Object value = null;
        if(id != null && id.matches("\\d+")){
            value = Long.valueOf(id);
        }
        if(value != null){
            for(Map.Entry<String,String> fieldToDwr : entityToDwr.entrySet()){
                if("id".equals(fieldToDwr.getValue())){
                    setValueWithSetter(entity,fieldToDwr.getKey(),value);
                }
            }
        }
    }

    private static <T> T getEntityFromFields(Map<String, FieldData> values, Map<String, String> entityToDwr, Class<T> entityClass) throws Exception {
            return getEntityFromFields(values, entityToDwr, entityClass,"DWR");
    }

    private static <T> T getEntityFromFields(Map<String, FieldData> values, Map<String, String> entityToDwr, Class<T> entityClass, String fieldPrefix) throws Exception {
        Object entity = entityClass.newInstance();
        if(!fieldPrefix.endsWith("_")){
            fieldPrefix = fieldPrefix + "_";
        }

        for(Map.Entry<String,String> fieldToDwr : entityToDwr.entrySet()){
            Object value = getFieldValue(values,fieldPrefix + fieldToDwr.getValue());
            setValueWithSetter(entity,fieldToDwr.getKey(), value);
        }

        //fm.getField("CZY_MAGAZYNOWA").getEnumItem(Integer.valueOf(DwrUtils.getStringValue(values, "DWR_CZY_MAGAZYNOWA"))).getCn();
        //FieldsManagerUtils.getData(fm,"WS_KWOTA", Object.class);
        return (T) entity;
    }


    private static Object getFieldValue(Map<String, FieldData> values, String docFieldName) {
        FieldData fieldData = values.get(docFieldName);
        return getFieldValue(fieldData);
    }

    private static Object getFieldValue(FieldData fieldData){
        if(fieldData == null){
            return null;
        }
        Class dataClass = resolveDataClass(fieldData.getType());
        Object value = DwrUtils.getFieldData(fieldData, dataClass);

        if(value != null && fieldData.getType().equals(Field.Type.ENUM) && value.toString().matches("\\d+")){
            value = Long.valueOf(value.toString());
        }
        return value;
    }


    private static void setValueWithSetter(Object entity, String entityFieldName, Object value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class entityClass = entity.getClass();
        Method setter = ReflectionUtils.getSetter(entityClass, entityFieldName, false);
        if(setter == null){
            throw new NoSuchMethodException("Class " + entityClass.getCanonicalName() + " hasn't got setter method for field " + entityFieldName);
        }
        setter.invoke(entity,value);
    }



    private static Class resolveDataClass(Field.Type type) {
        if (type != null) {
            switch (type){
                case DATE: return Date.class;
                case STRING:
                case ENUM:
                case TEXTAREA: return String.class;
                case INTEGER: return Integer.class;
                case LONG: return Long.class;
                case BOOLEAN: return Boolean.class;
                case MONEY: return BigDecimal.class;
                case DICTIONARY: return Map.class;
                case BUTTON: return ButtonValue.class;
                default:
                    return null;
            }
        }else{
            return null;
        }
    }


    /**
     * Returns description from given entity mapped to fields by entityToDwr<br/>
     * Structure of description:<br/>
     *
     * for each field:  <code> |newLine| fieldTitle: fieldValue </code>(if enum it is enum title)
     * @param entityToDwr map: entity field name -> dockind field Name (without dictionary prefix and index suffix)
     * @param entity
     * @param fm
     * @param dictionaryName name of dictionary (doesn't matter if multiple) on dockind, if null fields aren't part of dictionary
     * @return Returns description from given entity maped to fields by entityToDwr if entity == null returns empty string
     * @throws Exception
     */
    public static String prepareDescription(Map<String,String> entityToDwr, Object entity, FieldsManager fm, String dictionaryName) throws EdmException {
        return prepareDescription(entityToDwr, entity, fm, dictionaryName,"\n");
    }

    /**
     * Returns description from given entity mapped to fields by entityToDwr<br/>
     * Structure of description:<br/>
     *
     * for each field:  <code> |fieldSeparator| fieldTitle: fieldValue </code>(if enum it is enum title)
     * @param entityToDwr map: entity field name -> dockind field Name (without dictionary prefix and index suffix)
     * @param entity
     * @param fm
     * @param dictionaryName name of dictionary (doesn't matter if multiple) on dockind, if null fields aren't part of dictionary
     * @param fieldSeparator string that will separate descriptions of each field
     * @return Returns description from given entity maped to fields by entityToDwr if entity == null returns empty string
     * @throws Exception
     */
    public static String prepareDescription(Map<String,String> entityToDwr, Object entity, FieldsManager fm, String dictionaryName,String fieldSeparator) throws EdmException {

        StringBuilder description = new StringBuilder();
        if(entity != null){
            for(Map.Entry<String,String> entry : entityToDwr.entrySet()){
                Object value = null;
                try {
                    value = getValueWithGetter(entity, entry.getKey());
                } catch (Exception e) {
                    throw new EdmException(e);
                }
                if(value == null){
                    continue;
                }
                description.append(decriptionFromField(fm,entry.getValue(),value,dictionaryName,fieldSeparator));
            }
        }
        int firstSeparatorIndex = description.indexOf(fieldSeparator);
        if(firstSeparatorIndex == 0){
            description.delete(firstSeparatorIndex, fieldSeparator.length());
        }
        return description.toString();

    }

    private static StringBuilder decriptionFromField(FieldsManager fm, String fieldCn, Object value,String dictionaryName, String fieldSeparator) throws EdmException {
        StringBuilder description = new StringBuilder();

        pl.compan.docusafe.core.dockinds.field.Field field = extractField(fm,dictionaryName,fieldCn);

        if (field == null) {
            throw new EdmException("Can't find field with CN = " + fieldCn);
        }
        description.append(fieldSeparator).append(field.getName()).append(": ");

        Field.Type fieldType = field.getDwrField().getType();
        switch (fieldType){
            case DATE:
            case STRING:
            case INTEGER:
            case LONG:
            case BOOLEAN:
            case MONEY:
            case TEXTAREA: {
                description.append(value.toString().trim());
                break;
            }
            case ENUM:{
                if(value.toString().matches("\\d+")){
                    EnumItem enumItem = field.getEnumItem(Integer.valueOf(value.toString()));
                    description.append(enumItem.getTitle().trim());
                }else{
                    throw new EdmException("Can't resolve title for id = " +value.toString() + " from enum field " + fieldCn );
                }
                break;
            }
            case DICTIONARY:
            default: throw new EdmException(new NotImplementedException("No implementation for Field type " + fieldType.name()));
        }

        return description;
    }

    private static pl.compan.docusafe.core.dockinds.field.Field extractField(FieldsManager fm, String dictionaryCn, String fieldCn) throws EdmException {
        if(dictionaryCn != null){
            pl.compan.docusafe.core.dockinds.field.Field field = fm.getField(dictionaryCn);
            if(field != null && field.isMultiple()){
                return FieldsManagerUtils.getDictionaryField(fm, dictionaryCn, fieldCn,  pl.compan.docusafe.core.dockinds.field.Field.class);
            }else if(field != null && !field.isMultiple()){
                return FieldsManagerUtils.getFieldFromMultiple(fm,dictionaryCn,fieldCn,true);
            }else {
                return null;
            }
        }else {
            return fm.getField(fieldCn);
        }
    }

    private static Object getValueWithGetter(Object entity, String entityFieldName) throws Exception {
        Class entityClass = entity.getClass();
        Method getter = ReflectionUtils.getGetter(entityClass,entityFieldName, false);
        if(getter == null){
            throw new NoSuchMethodException("Class " + entityClass.getCanonicalName() + " hasn't got setter method for field " + entityFieldName);
        }
        return getter.invoke(entity);
    }


    public static void setFieldsFromEntity(Map<String, FieldData> values, FieldsManager fm, Map<String, String> entityToDwr, Object entity) throws EdmException {

        for(Map.Entry<String,String> entry : entityToDwr.entrySet()){
            Object value = null;
            try {
                value = getValueWithGetter(entity, entry.getKey());
            } catch (Exception e) {
                throw new EdmException(e);
            }
            if (value == null) {
                continue;
            }
            setDwrFieldValue(values,fm,entry.getValue(),value);
        }
    }

    private static void setDwrFieldValue(Map<String, FieldData> values, FieldsManager fm, String fieldCn, Object value) throws EdmException {
        pl.compan.docusafe.core.dockinds.field.Field field = extractField(fm,null,fieldCn);

        if (field == null) {
            throw new EdmException("Can't find field with CN = " + fieldCn);
        }

        Field.Type fieldType = field.getDwrField().getType();

        switch (fieldType){
            case DATE:
            case STRING:
            case INTEGER:
            case LONG:
            case BOOLEAN:
            case MONEY:
            case TEXTAREA: {
                break;
            }
            case ENUM:{
                EnumValues data = new EnumValues();
                data.setSelectedId(value.toString());
                value = data;
                break;
            }
            default: throw new EdmException(new NotImplementedException("No implementation for Field type " + fieldType.name()));
        }

        FieldData fieldData = new FieldData(fieldType, value);
        if(!fieldCn.startsWith("DWR_")){
            fieldCn = dwr(fieldCn);
        }
        values.put(fieldCn,fieldData);
    }

    /**
     * Sets dictionary data from given entity
     * <B>UWAGA ZAIMPLEMENTOWANE TYLKO DLA WIELOWARTOŚCIOWEGO SŁOWNIKA</B>
     * @param values
     * @param fm
     * @param idFieldName name of entity id field
     * @param entities  list of entities from which dictionary should be filled
     * @param dictionaryName name of dictionary (doesn't matter if multiple) without DWR prefix
     * @throws EdmException
     */
    public static void setDictionaryFromEntity(Map<String, FieldData> values, FieldsManager fm, String idFieldName, Collection entities, String dictionaryName) throws EdmException {

        List<Long> ids = new ArrayList<Long>();

        for(Object entity : entities){
            Object value = null;
            try {
                value = getValueWithGetter(entity, idFieldName);
            } catch (Exception e) {
                throw new EdmException(e);
            }

            if(value != null && value.toString().matches("\\d+")){
                Long id = Long.valueOf(value.toString());
                ids.add(id);
            }
        }

        setDictionaryFromEntity(values, fm, ids, dictionaryName);
    }


    private static void setDictionaryFromEntity(Map<String, FieldData> values, FieldsManager fm, Collection entitiesIds, String dictionaryName) throws EdmException {
        boolean isMultiple = false;
        if(fm.getField(dictionaryName) != null){
            isMultiple = fm.getField(dictionaryName).isMultiple();
        }
        if(isMultiple){
            FieldData fd = new FieldData(Field.Type.DICTIONARY, entitiesIds);
            values.put(dwr(dictionaryName), fd);
        }else{
            throw new EdmException(new NotImplementedException("Uzupełnianie słownika jednowarotściowego jeszcze nie zaimplementowane"));
        }
    }

}

