package pl.compan.docusafe.core.dockinds.dwr.attachments;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemHeaders;
import pl.compan.docusafe.core.DSApi;

import java.io.*;

public class DwrFileItem implements FileItem {
    String id;
    private FileItem fileItem;

    public DwrFileItem(FileItem fileItem, String id) {
        this.fileItem = fileItem;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public void delete() {
        fileItem.delete();
    }

    @Override
    public byte[] get() {
        return fileItem.get();
    }

    @Override
    public String getContentType() {
        return fileItem.getContentType();
    }

    @Override
    public String getFieldName() {
        return fileItem.getFieldName();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return fileItem.getInputStream();
    }

    @Override
    public String getName() {
        return fileItem.getName();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return fileItem.getOutputStream();
    }

    @Override
    public long getSize() {
        return fileItem.getSize();
    }

    @Override
    public String getString() {
        return fileItem.getString();
    }

    @Override
    public String getString(String s) throws UnsupportedEncodingException {
        return fileItem.getString(s);
    }

    @Override
    public boolean isFormField() {
        return fileItem.isFormField();
    }

    @Override
    public boolean isInMemory() {
        return fileItem.isInMemory();
    }

    @Override
    public void setFieldName(String s) {
        fileItem.setFieldName(s);
    }

    @Override
    public void setFormField(boolean b) {
        fileItem.setFormField(b);
    }

    @Override
    public void write(File file) throws Exception {
        DSApi.openAdmin();
        fileItem.write(file);
        DSApi._close();
    }
}
