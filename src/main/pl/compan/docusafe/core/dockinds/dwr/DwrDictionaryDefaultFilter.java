package pl.compan.docusafe.core.dockinds.dwr;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 06.06.13
 * Time: 17:29
 * To change this template use File | Settings | File Templates.
 */
public class DwrDictionaryDefaultFilter implements DwrDictionaryFilter {

    private static final String NO_EMPTY_ENUM_KEY = "NO_EMPTY_ENUM_KEY";

    @Override
    public boolean filterBeforeGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dictionaryEntryId, Map<String, Object> documentValues) {
        return true;
    }

    /**
     * Zast�puje warto�ci w dictionaryFilteredFieldsValues znalezionymi w fieldsValues na podstawie kluczy z dictionaryFilteredFieldsValues w nast�puj�cych sytuacjach:
     * <ul>
     * <li> typy obiekt�w w dictionaryFilteredFieldsValues i fieldsValues s� takie same </li>
     * <li> obiekt w dictionaryFilteredFieldsValues jest null i obiekt w fieldsValues jest typu String </li>
     * <li> w przypakdu gdy obiektu w dictionaryFilteredFieldsValues jest typu EnumValues ustawiana jest warto�� selectedId pobran� z filedsValues pod warunkiem, �e jest to String. </li>
     * </ul>
     * @param dictionaryFilteredFieldsValues
     * @param dicitonaryEntryId
     * @param fieldsValues
     * @param documentValues
     */
    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        if (fieldsValues==null)
            return;
        for (String key : dictionaryFilteredFieldsValues.keySet()) {
            if (fieldsValues.containsKey(key)) {

                Object selectedValue = fieldsValues.get(key);
                Object dictionaryValue = dictionaryFilteredFieldsValues.get(key);

                if (dictionaryValue instanceof EnumValues && selectedValue!=null && selectedValue instanceof String)
                    ((EnumValues) dictionaryValue).setSelectedId((String) selectedValue);

                else if (dictionaryValue instanceof EnumValues && selectedValue==null){
                    //wybrana warto�� null ~ "wybierz" -> ustaw pust� warto�� dla s�ownika
                    String emptyKey = getEmptyOption((EnumValues)dictionaryValue);
                    if(!emptyKey.equals(NO_EMPTY_ENUM_KEY))
                        ((EnumValues) dictionaryValue).setSelectedId(emptyKey);
                }

                else if (selectedValue!=null)
                    if ((dictionaryValue == null && selectedValue instanceof String)
                        || (dictionaryValue != null && dictionaryValue.getClass().equals(selectedValue.getClass())))
                        dictionaryFilteredFieldsValues.put(key, selectedValue);
            }
        }
    }

    private String getEmptyOption(EnumValues enumValues) {
        for (Map<String, String> optSet : enumValues.getAllOptions()) {
            for (Map.Entry<String, String> opt : optSet.entrySet())
                if (StringUtils.isEmpty(opt.getKey()))
                    return opt.getKey();
        }
        return NO_EMPTY_ENUM_KEY;
    }

    @Override
    public void filterBeforeUpdateValues(Map<String, FieldData> dictionaryFilteredFieldData) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
