package pl.compan.docusafe.core.dockinds.dwr;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Field {
	
	private Type type;
	private String cn;
	/**
	 * Etykieta pola. <br />
	 * Dla pola {@link Field.Kind#MESSAGE} zawarto�� wy�wietlanego komunikatu.
	 */
	private String label;
	private String cssClass = "";
	/**
	 * Status pola - zobacz {@link Field.Status}
	 */
	private Status status = Status.ADD;
	/**
	 * Czy wysy�a� formularz po zmanie pola, tj. po javascriptowych eventach: <br />
	 * onKeyUp, onChange, lub po okre�lonym czasie nie zmieniania warto�ci pola <br />
	 * przez u�ytkownika (na przyk�ad dla input text)
	 */
	private Boolean submit = false;
	/**
	 * Czas w milisekundach po kt�rym, zostanie przes�any formularz je�li u�ytkownik <br />
	 * nie wprowadza zmian w polu. Ma sens tylko wtedy, gdy ustawimy {@link Field#submit}
	 */
	private Integer submitTimeout = 0;
	/**
	 * Numer kolumny (od <code>0</code> do <code>2</code>)
	 */
	private Integer column = 0;
	/**
	 * Nazwa grupy (id w html), w kt�rej umieszczamy pole. <br />
	 * Grupa znajduje si� w kolumnie
	 */
	private String fieldset = "default";
	/**
	 * Rodzaj pola - etykiety, uwagi. <br />
	 * W szczeg�lno�ci {@link Kind#MESSAGE} - wy�wietlanie wiadomo�ci dla u�ytkownika
	 */
	private Kind kind = Kind.NORMAL;
	/**
	 * Wyra�enie regularne do walidacji pola przez javascript
	 */
	private String validatorRegExp = null;
	/**
     * TODO, bo nie dzia�a.
	 * Komunikat wy�wietlany dla pola, kt�rego nie przesz�o walidacji,
	 * tj. nie odpowiada wzorcowi {@link #validatorRegExp}.
	 * Je�li <code>NULL</code> lub pusty - wy�wietlany jest komunikat domy�lny.
	 */
	private String validatorMessage = null;
	/**
	 * Format prezentacji p�l liczbowych
	 */
	private String format = null;
	/**
	 * Pole wymagane - (tzn. niepuste)
	 */
	private Boolean required = false;
	/**
	 * Pole tylko do odczytu (to nie to samo co pole {@link Field#disabled}). <br />
	 * Dla tego pola nie ma sensu przypisywa� walidator�w, czy warto�ci <code>submit</code>.
	 */
	private Boolean readonly = false;
	/**
	 * Pole wy��czone (to nie to samo co pole {@link Field#readonly}. <br />
	 * Podobnie jak dla <code>required</code> nie przypisujemy walidator�w i warto�ci <code>submit</code>
	 */
	private Boolean disabled = false;
	
	/**
	 * Pole okre�laj�ce funkcj� wywo�ywan� na onchange
	 */
	private String onchange = null;
	/**
	 * Pomoc dotycz�ca pola
	 */
	private String info;
	/**
	 * Dodatkowe parametry dla niekt�rych typ�w p�l.<br />
	 * Obecnie nie u�ywane.
	 */
	private Map<String, Object> params = null;
	/**
	 * Pole schowane (niewidoczne)
	 */
	private Boolean hidden = false;
	
	/**
	 * wymu� w dwr, hidden na true, je�li pole jest puste oraz disabled=true
	 */
	private Boolean hiddenOnDisabledEmpty = false;

    /**
     * ladowanie enumow dla pola w slowniku odbywa sie po kliknieciu na select
     */
    private Boolean lazyEnumLoad = false;

    /**
     * wyswietlenie slownika wielowartosciowego jako text
     */
    private Boolean asText = false;
	/**
	 * Pole z podpowiadaniem (NIE-ajaxowym). Nale�y u�ywa� zamiast {@link Type#ENUM_AUTOCOMPLETE}
	 * dla typ�w {@link Type#ENUM} oraz {@link Type#ENUM_MULTIPLE}. <br />
	 * W przysz�o�ci mo�liwo�ci dodania obs�ugi dla innych typ�w p�l.
	 */
	private Boolean autocomplete = false;
	/**
	 * Dla p�l z {@link Field#autocomplete} ustawionym na <code>true</code> jest to wyra�enie regularne
	 * okre�laj�ce spos�b dopasowywania podpowiedzi. Znak <code>%</code> jest zamieniany na tekst wpisany przez u�ytkownika.
	 * Tryb wyszukiwania jest ustawiony na ignorowanie wielko�ci znak�w. <br />
	 * Przyk�ady:
	 * <ul>
	 * <li><code>^%</code> - dopasowuje tylko wyst�pienia zaczynaj�ce si� od tekstu wpisanego przez u�ytkownika, 
	 * tzn.: dla opcji <code>"DocuSafe"</code> dopasuje tekst <code>"docu"</code>, ale tekst <code>"safe"</code> ju� nie
	 * <li><code>%</code> - dopasowuje r�wnie� w �rodku wyra�enie, tz.: dla opcji <code>"DocuSafe"</code> dopasuje
	 * zar�wno <code>"docu"</code> jak i <code>"safe"</code> 
	 * </ul>
	 */
	private String autocompleteRegExp = null;
	/**
	 * S�ownik - tylko dla pola {@link Type#DICTIONARY}
	 */
	private DwrDictionaryBase dictionary;
	/**
	 * Etykieta wy�wietlana, je�li u�ytkownik dodaje nowy wpis do s�ownika.<br/>
	 * Domy�lnie jest to "Nowy wpis"
	 */
	private String newItemMessage;
	
	/**
	 * Dla p�l typu LinkField, parametryzuje atrybut target.
	 */
	private String linkTarget; 
	
	/**
	 * Dla za��cznika okre�la czy mo�na wgrywa� wiele za��cznik�w na raz
	 */
	private boolean multiple;
	
	/**
	 * Wsp�rz�dne pola w uk�adzie szachownicy opcjonalnie oddzielone dwukropkiem:<br />
	 * <ul>
	 * <li><code>"b3"</code> - pole b�dzie rysowane w kolumnie b (drugiej od lewej) i wierszu trzecim (od g�ry)
	 * <li><code>"b3:d4</code> - pole b�dzie rysowane od kolumny b (drugiej od lewej) do kolumny d (czwartej od lewej)
	 * i od wiersza trzeciego do czwartego
	 * <li><b>WSZYSTKIE</b> pola w danym dockindzie musz� mie� podane wsp�rz�dne - tylko na tej podstawie jeste�my w stanie
	 * narysowa� szachownic�. Je�li <b>�ADNE</b> pole w dockindzie nie ma podanych wsp�rz�dnych rysujemy pola wed�ug kolumn
	 * </ul>
	 * <b>Uwagi:</b>
	 * <ol>
	 * <li>Sk�adnia <code>"b3:"</code> jest nieprawid�owa
	 * <li>Pola rysujemy od lewej do prawej i od g�ry do do�u, wi�c sk�adnia <code>"d4:b3"</code> jest nieprawid�owa
	 * i nie nale�y jest stosowa� - nigdzie nie jest to sprawdzane i mo�e spowodowa� trudne do wykrycia b��dy
	 * <li>Sk�adnia <code>"b3:b3"</code> jest prawid�owa i r�wnowa�na <code>"b3"</code>
	 * <li>Nale�y u�ywa� ma�ych liter - jest to jedynie zalecenie - javascript powinien sobie poradzi� r�wnie� z du�ymi literami
	 * </ol>
	 */
	private String coords;
	
	/** właściwość wskazująca czy pole powinno wywoływać listenery wskazane w atrybucie listenersList */
	private Boolean submitForm;
	/** lista z nazwami listenerów, które powinny zostać wywołane w przypadku ustawienia parametru submitForm na true */
	private List<String> listenersList;
    /** Okre�la czy dane pole ma by� automatycznie 'zafokusowane'. Tylko jedno pola na dokumencie mo�e miec ustawiony ten atrybut na true*/
    private boolean autofocus;
    /**
     * Popup pola
     */
    private Map<String, Popup> popups = new HashMap<String, Popup>();
    /**
     * Uruchamianie walidacji
     */
    private boolean validate;
	/*rodzaj: etykiety, uwagi
	getmessage - wiadomo�� dla u�ytkownika - specjalny rodzaj fielda*/

    public static String DWR_PREFIX = "DWR_";

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cn == null) ? 0 : cn.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Field other = (Field) obj;
		if (cn == null)
		{
			if (other.cn != null)
				return false;
		}
		else if (!cn.equalsIgnoreCase(other.cn))
			return false;
		return true;
	}
	
	public Field(String cn, String label, Type type) {
		this.cn = cn;
		this.label = label;
		this.type = type;
	}

    /**
     * Tworzy pole type {@link Kind#MESSAGE}
     * @param message  Komunikat do wy�wietlenia
     * @param messageCn  Cn wiadomo�ci
     */
    public Field(String message, String messageCn) {
        this.label = message;
        this.cn = messageCn;
        this.kind = Kind.MESSAGE;
        this.type = Type.STRING;
    }
	
	public Field(Field field)
	{
		this.cn = field.getCn();
		this.label = field.getLabel();
		this.type = field.getType();
	}
	
	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getCn() {
		return cn;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Status getStatus() {
		return status;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setSubmit(Boolean submit) {
		this.submit = submit;
	}

	public Boolean getSubmit() {
		return submit;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public Integer getColumn() {
		return column;
	}

	public void setKind(Kind kind) {
		this.kind = kind;
	}

	public Kind getKind() {
		return kind;
	}

	public void setValidatorRegExp(String validatorRegExp) {
		this.validatorRegExp = validatorRegExp;
	}

	public String getValidatorRegExp() {
		return validatorRegExp;
	}

	public void setSubmitTimeout(Integer submitTimeout) {
		this.submitTimeout = submitTimeout;
	}

	public Integer getSubmitTimeout() {
		return submitTimeout;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}

	public Boolean getReadonly() {
		return readonly;
	}

	public void setFieldset(String fieldset) {
		this.fieldset = fieldset;
	}

	public String getFieldset() {
		return fieldset;
	}

	public void setDictionary(DwrDictionaryBase dictionary) {
		this.dictionary = dictionary;
	}

	public DwrDictionaryBase getDictionary() {
		return dictionary;
	}
	
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getDisabled() {
		return disabled;
	}
	
	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public Boolean getHiddenOnDisabledEmpty() {
		return hiddenOnDisabledEmpty;
	}

	public void setHiddenOnDisabledEmpty(Boolean hiddenOnDisabledEmpty) {
		this.hiddenOnDisabledEmpty = hiddenOnDisabledEmpty;
	}

    public Boolean getLazyEnumLoad() {
        return lazyEnumLoad;
    }

    public void setLazyEnumLoad(Boolean lazyEnumLoad) {
        this.lazyEnumLoad = lazyEnumLoad;
    }

    public Boolean getAsText() {
        return asText;
    }

    public void setAsText(Boolean asText) {
        this.asText = asText;
    }

    public void setInfo(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public Map<String, Object> getParams()
	{
		return params;
	}

	public void setParams(Map<String, Object> params)
	{
		this.params = params;
	}
	
	public void addParam(String key, Object obj)
	{
		if (this.params == null)
			this.params = new HashMap<String, Object>();
		this.params.put(key, obj);
	}
	
	public void setFormat(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}

	public String getValidatorMessage() {
		return validatorMessage;
	}

	public void setValidatorMessage(String validatorMessage) {
		this.validatorMessage = validatorMessage;
	}

    public void setAutofocus(boolean autofocus)
    {
        this.autofocus = autofocus;
    }

    public boolean isAutofocus()
    {
        return autofocus;
    }

    /**
	 * Typ pola (niekt�re maj� domy�lny walidator w JavaScripcie - symbol [W]):
	 * <ul>
	 *	<li><code>Type.DATE</code> - data (wy�wietlona w formacie DD-MM-YYY - tak jak w ca�ym docusafe) [W]
	 *	<li><code>Type.STRING</code> - tekst
	 *	<li><code>Type.INTEGER</code> - liczba [W]
	 *	<li><code>Type.BOOLEAN</code> - checkbox
	 *	<li><code>Type.TIMESTAMP</code> - godzina [W]
	 *	<li><code>Type.ENUM</code> - lista rozwijana (tylko jedna opcja mo�liwa do zaznaczenia)
	 *	<li><code>Type.ENUM_MULTIPLE</code> - lista (kilka opcji mo�liwych do zaznaczenia)
	 *	<li><code></code> - lista rozwijana (tylko jedna opcja mo�liwa do zaznaczenia) z podpowiadaniem
	 *	<li><code>Type.DICTIONARY</code> - s�ownik
	 *  <li><code>Type.HTML_EDITOR</code> - edytowalne pole HTML (jak w cms)
	 *  <li><code>Type.BUTTON</code> - przycisk
	 * </ul>
	 */

	public static enum Type { LINK, DATE, STRING, INTEGER, LONG, BOOLEAN, ENUM, TIMESTAMP, LIST_VALUE, @Deprecated ENUM_AUTOCOMPLETE, 
		MONEY, ENUM_MULTIPLE, DICTIONARY, TEXTAREA, ATTACHMENT, HTML, HTML_EDITOR, BUTTON }

	/**
	 * Rodzaj pola:
	 * <ul>
	 * 	<li><code>Kind.NORMAL</code> - standardowe
	 *  <li><code>Kind.MESSAGE</code> - wy�wietla tekst {@link Field#label label} w postaci komunikatu u g�ry ekranu.
	 *  <li><code>Kind.DICTIONARY_FIELD</code> - musi by� ustawione, gdy pole jest zawarte w s�owniku 
	 *  <li><code>Kind.DICTIONARY_ID_FIELD</code> - specjalny typ dla id s�ownika
	 * </ul>
	 */
	public static enum Kind { NORMAL, MESSAGE, DICTIONARY_FIELD, DICTIONARY_ID_FIELD }
	/**
	 * Okre�la, czy pole ma by�:
	 * <ul>
	 * 	<li><code>Status.ADD</code> - dodane (narysowane od nowa na formatce)
	 * 	<li><code>Status.REMOVE</code> - usuni�te
	 * 	<li><code>Status.NOCHANGE</code> - pozostawione bez zmian
	 * </ul>
	 */
	public static enum Status { ADD, NOCHANGE, CHANGED }
	
	public void setNewItemMessage(String newItemMessage)
	{
		this.newItemMessage = newItemMessage;
	}

	public String getNewItemMessage()
	{
		return newItemMessage;
	}

	public void setAutocomplete(Boolean autocomplete) {
		this.autocomplete = autocomplete;
	}

	public Boolean getAutocomplete() {
		return autocomplete;
	}


	public void setCoords(String coords) {
		this.coords = coords;
	}

	public String getCoords() {
		return coords;
	}

	public void setAutocompleteRegExp(String autocompleteRegExp) {
		this.autocompleteRegExp = autocompleteRegExp;
	}

	public String getAutocompleteRegExp() {
		return autocompleteRegExp;
	}

	public String getOnchange() {
		return onchange;
	}

	public void setOnchange(String onchange) {
        if(StringUtils.isNotEmpty(onchange)){
		    this.onchange = onchange.replaceAll("servlet_context_path", Docusafe.getBaseUrl());
        }
        else
            this.onchange = onchange;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	public void setLinkTarget(String linkTarget) {
		this.linkTarget = linkTarget;
	}

	public Boolean getSubmitForm() {
		return submitForm;
	}

	public void setSubmitForm(Boolean submitForm) {
		this.submitForm = submitForm;
	}

	public List<String> getListenersList() {
		return listenersList;
	}

	public void setListenersList(List<String> listenersList) {
		this.listenersList = listenersList;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

    public Map<String, Popup> getPopups() {
        return popups;
    }

    public void setPopups(Map<String, Popup> popups) {
        this.popups = popups;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}

