package pl.compan.docusafe.core.dockinds;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.DOMWriter;
import org.dom4j.io.SAXReader;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;


public class DockindDBUtils 
{
	private static Configuration configure() throws Exception
	{
		SAXReader reader = new SAXReader();
		
		//
        reader.setEntityResolver(new EntityResolver()
        {
            final static String BASE = "http://hibernate.sourceforge.net/";
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
            {
                if (systemId != null && systemId.startsWith(BASE))
                {
                    return new InputSource(getClass().getClassLoader().getResourceAsStream(
                        "org/hibernate/"+systemId.substring(BASE.length())));
                }
                return null;
            }
        });
        reader.setIncludeExternalDTDDeclarations(false);
        reader.setIncludeInternalDTDDeclarations(false);
        reader.setValidation(false);
        //
        
		Document doc = reader.read(DSApi.class.getClassLoader().getResourceAsStream("hibernate.cfg.xml"));
		
		Document xml = reader.read(new File(Docusafe.getHome(), "jbpm4.hibernate.cfg.xml"));
		
		Element sf = (Element) doc.selectSingleNode("/hibernate-configuration/session-factory");
		
		sf.add((Element) ((Element) xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.dialect']")).clone());

        if (xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.datasource']") != null) {
            sf.add((Element) ((Element) xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.datasource']")).clone());
        } else {
            sf.add((Element) ((Element) xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.url']")).clone());
            sf.add((Element) ((Element) xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.username']")).clone());
            sf.add((Element) ((Element) xml.selectSingleNode("/hibernate-configuration/session-factory/property[@name='hibernate.connection.password']")).clone());
        }

		Configuration c = new Configuration();
		c.configure(new DOMWriter().write(doc));
		return c;
	}
	
	public static void runUpdates(List<Document> mappings) throws EdmException
	{
		try
		{
			Configuration c = configure();

			for(Document d : mappings)
				c.addDocument(new DOMWriter().write(d));
			
			org.hibernate.tool.hbm2ddl.SchemaUpdate u = new SchemaUpdate(c);
			u.execute(true, true);
			
			for(Object o : u.getExceptions())
				throw new EdmException(o.toString());
			
			
		} catch(Exception e) { throw new EdmException(e.getMessage(), e); }
	}
	
	public static void runUpdate(Document mapping) throws EdmException
	{
		List<Document> mappings = new ArrayList<Document>();
		mappings.add(mapping);
		runUpdates(mappings);
	}	
}
