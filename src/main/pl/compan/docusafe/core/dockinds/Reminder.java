package pl.compan.docusafe.core.dockinds;

public class Reminder {
	private String name;
	private String searchQuery;
	private String recipient;
	private RecipientType recipientType;
	private String recipientSplitSeq;
	private String period;
	private String templateFileName;
	
	public enum RecipientType {
		USERNAME,USERNAMES,GUID,TASK_LIST,USERNAME_FIELD_FROM_DICTIONARY
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public RecipientType getRecipientType() {
		return recipientType;
	}

	public void setRecipientType(RecipientType recipientType) {
		this.recipientType = recipientType;
	}

	public String getRecipientSplitSeq() {
		return recipientSplitSeq;
	}

	public void setRecipientSplitSeq(String recipientSplitSeq) {
		this.recipientSplitSeq = recipientSplitSeq;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getTemplateFileName() {
		return templateFileName;
	}

	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}
}
