package pl.compan.docusafe.core.dockinds;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.crypto.DockindCryptoDigester;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.Reminder.RecipientType;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.FinalAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.Field.ValidationRule;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinitionXMLParser;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.JackrabbitXmlSynchronizer;
import pl.compan.docusafe.lucene.IndexStatus;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.XmlUtils;
import pl.compan.docusafe.util.filter.UserFilters;
import pl.compan.docusafe.webwork.FormFile;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Klasa opisuj�ca rodzaj dokumentu. Rodzaje mog� by� identyfikowane albo przez 'id' albo przez 'cn'.
 * Rodzaj o 'cn' r�wnym {@link NORMAL_KIND} opisuje zwyk�y dokument (nie posiadaj�cy �adnych atrybut�w biznesowych),
 * zatem jego zbi�r p�l jest pusty. Do pobierania p�l biznesowych i ich warto�ci dla danego dokumentu
 * powinno si� zawsze u�ywa� obiektu klasy {@link FieldsManager}.
 * Aby zapisa� warto�ci tych p�l w bazie nale�y wykona�:
 * <ul>
 * <li> metod� {@link #setWithHistory(Long, Map, boolean)}
 * (gdy chcemy zapisa� informacje o zmianach warto�ci p�l do historii)
 * <li> metod� {@link #set(Long, Map)} lub {@link #setOnly(Long, Map)} (w przypadku gdy nie chcemy zapisywa�
 * informacji o zmianach do historii).
 * </ul>
 * We wszystkich tych funkcjach parametr typu 'Map' to mapa warto�ci indeksowana po 'cn' p�l tego rodzaju.
 * Mo�e by� to mapa pobierana z formularzy HTML, kt�re uzupe�nia u�ytkownik, albo by� utworzona r�cznie.
 *
 * Pola rodzaju dokumentu (tak jak i warto�ci tych p�l w {@link FieldsManager})
 * s� inicjowane przy pierwszym odwo�aniu do nich. Zatem chc�c ich u�ywa� (bez uprzedniego odwo�ania do nich)
 * bez po��czenia z baz� (np. na stronie JSP) trzeba wykona� wcze�niej metod� 'initialize()'.
 * Przyk�ady u�ycia s� np. w {@link pl.compan.docusafe.web.office.common.DocumentMainTabAction} i
 * {@link pl.compan.docusafe.web.archive.repository.EditDockindDocumentAction}.
 *
 * Ka�dy rodzaj dokumentu posiada obiekt implementuj�cy interfejs {@link DocumentLogic}, kt�ry odpowiada
 * za funkcjonalno�ci zale�ne od logiki rodzaju dokumentu jak np. uprawnienia czy nazewnictwo folder�w.
 *
 * UWAGA: dodanie do systemu nowego rodzaju dokumentu wymaga wykonania conajmniej poni�szych operacji:
 * <ul>
 * <li> dodanie sta�ej w tej klasie oznaczaj�cej 'cn' tworzonego rodzaju (tak jak NORMAL_KIND, DAA_KIND, itp)
 * <li> dodanie do listy zwracanej przez funkcj� {@link DocumentKind#getAvailableDocumentKindsCn()} powy�ej
 * wspomnianej sta�ej
 * <li> utworzenie klasy implementuj�cej interfejs {@link pl.compan.docusafe.core.dockinds.logic.DocumentLogic}
 * (tak jak NormalLogic, DaaLogic, itp)
 * <li> dodanie w metodzie {@link DocumentKind#logic()} odpowiedniego "mapowania" z 'cn' na obiekt reprezentuj�cy
 * logik� dokumentu (wspomnian� w punkcie powy�ej)
 * </ul>
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DocumentKind implements Serializable
{
    protected static Map<String,DockindInfoBean> dockindInformation = new ConcurrentHashMap<String,DockindInfoBean>();
	/**
	 * @TODO - trzeba wydzielic z tej klasy definicje, typu pola, etc, etc i umiescic w jakiejs klasie, ktora to przechowuje
	 */

	/**
	 * Mapa zawierajaca definicje pol
	 */
	//protected static Map<String, List<Field>> dockindFields = new TreeMap<String, List<Field>>();
    private static final Logger log = LoggerFactory.getLogger(pl.compan.docusafe.core.dockinds.DocumentKind.class);
    static final StringManager sm = GlobalPreferences.loadPropertiesFile(DocumentKind.class.getPackage().getName(),null);

    /**
     * id dockindu
     */
    private Long id;
    /**
     * Nazwa
     */
    private String name;
    /**
     * CN
     */
    private String cn;
    /**
     * Nazwa tabeli
     */
    private String tablename;
    /**
     * Data dodania
     */
    private Date addDate;

    /**
     * Pole pobierane z tabeli
     */
    private boolean enabled;
    /** "logika" tego rodzaju dokumentu */
    private DocumentLogic logic;
    /**
     * Wersja
     */
    private Integer hversion;

    private Date availableToDate;

    /** rodzaj dokumentu okre�laj�cy zwyk�� dokumenty nie-biznesowe */
    public static final String NORMAL_KIND = "normal";

    //klucze dla mapy properties
    public static final String BLOCK_TOINFORMATION_DECRETATION = "block-toinformation-decretation";
    public static final String SHOW_FOLDER_IN_SUMMARY_KEY = "show-folder-in-summary";
    public static final String SHOW_DESCRIPTION_AMOUNT_KEY = "show-description-amount";
    public static final String BOX_LINE_KEY = "box-line";
    public static final String BLOCK_DECRETATION = "blockDecretation";
    public static final String RUN_DSI_AFTER_IMPORT = "runDsiAfterImport";

    /**
     * Okre�la pole, kt�rego pewne warto�ci mog� wymusza� dodanie uwagi.
     * Musi by� to pole typu ENUM.
     */
    public static final String REMARK_FIELD_CN = "remark-field";
    /** true <=> w momencie dodanie uwagi (zwi�zanej z w�asno�ci� REMARK_FIELD_CN) usuwane jest zadanie z listy zada� */
    public static final String FINISH_WITH_REMARK_ADD = "finish-with-remark-add";
    public static final String BOX_NOT_REQUIRED_TO_FINISH = "box-not-required-to-finish";
    public static final String ACCEPTANCES_IN_SUMMARY = "acceptances-in-summary";
    public static final String CAN_CHANGE_STYLE = "can-change-style";
    public static final String SEARCH_STYLE = "search-style";
    public static final String CENTRA_DIVISION = "centra-division";
    public static final String OPEN_TAB_IN_OFFICE = "open-tab-in-office";
    public static final String SORT_BY_FIELD = "sort-by-field";

    /**
     * Pig
     */
    public static final String ZAMOWIENIE_PIG = "zamowieniePIG";

    /**Dokumentacja Workflow */
    public static final String WORKFLOW_WF = "wf";
    public static final String OVERTIME_APP = "overtime_app";
    public static final String OVERTIME_RECEIVE = "overtime_receive";

    public static final String MULTIPLE_DICTIONARY_VALUES = "M_DICT_VALUES";


    private final static Cache<Long, String> DOCKIND_NAME_CACHE = CacheBuilder
                                                        .newBuilder()
                                                        .expireAfterWrite(10, TimeUnit.MINUTES)
                                                        .build();
    /**
     * Zwraca nazw� (czyteln� dla u�ytkownika) dla dockinda o podanym id, warto�ci s�
     * cache'owane metod� mo�na cz�sto wywo�ywa�
     * @param id id dockinda
     * @return nazwa nigdy null
     */
    public static String getDockindNameForId(final long id){
        try {
            return DOCKIND_NAME_CACHE.get(id, new Callable<String>() {
                @Override
                public String call() throws Exception {
                    DocumentKind dk = DocumentKind.find(id);
                    return dk.getName();
                }
            });
        } catch (ExecutionException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Pobiera dane informacyjne o dockindzie
     * @return
     */
    public DockindInfoBean getDockindInfo()
    {
    	DockindInfoBean bean = dockindInformation.get(cn);
    	if (bean==null)
    	{
    		bean = new DockindInfoBean(cn);
    		dockindInformation.put(cn,bean);
    	}
    	return bean;
    }

    /**
     * Pobiera dane informacyjne o dockindzie o danym cn
     * @return
     */
    public static DockindInfoBean getDockindInfo(String cn)
    {
    	DockindInfoBean bean = dockindInformation.get(cn);
    	if (bean==null)
    	{
    		bean = new DockindInfoBean(cn);
    		dockindInformation.put(cn,bean);
    	}
    	return bean;
    }

    /**
     * Wymusza ponowny init
     * @param cn
     */
    public static void resetDockindInfo(String cn) throws EdmException
	{
		dockindInformation.remove(cn);
		try
		{
			DocumentKind.findByCn(cn).loadXmlData();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
	}

    /**
     * Wymusza ponowny init
     */
    public static void resetAllDockindInfo()
    {
    	dockindInformation.clear();
    	try
    	{
    		for(DocumentKind kind : DocumentKind.list())
    		{
    			kind.loadXmlData();
    		}
    	}
    	catch (Exception e)
    	{
			log.error(e.getMessage(),e);
		}
    }

    /**
     * Wymusza ponowny init
     */
    public void resetDockindInfo() throws EdmException
    {
    	resetDockindInfo(cn);
    }
    /**
     * Zwraca domy�lny rodzaj dokumentu w systemie.
     */
    public static String getDefaultKind() throws EdmException
    {
        String defaultCn = Docusafe.getAdditionProperty("dockind.default");
        if (defaultCn == null || findByCn(defaultCn) == null)
            defaultCn = NORMAL_KIND;
        return defaultCn;
    }
    
    /**
     * Zwraca domy�lny rodzaj dokumentu w systemie.
     * adds z domy�lnym rodzajem dokumentu dla danej grupy u�ytkownika 
     * i typu pisma powinien by� okre�lony wed�ug schematu "dockind.default.groupGUID.docType"
     */
    public static String getDefaultKind(Integer type) throws EdmException
    {
    	String defaultCn = Docusafe.getAdditionProperty("dockind.default");

    	if(type != null)
    	{
    		if(type.equals(DocumentLogic.TYPE_IN_OFFICE))
    		{
    			if (getDefaultCnForGroup("in") != null) 
    				defaultCn = getDefaultCnForGroup("in");
    			else defaultCn = Docusafe.getAdditionProperty("dockind.default.in");
    		}
    		else if(type.equals(DocumentLogic.TYPE_INTERNAL_OFFICE))
    		{
    			if (getDefaultCnForGroup("int") != null) 
    				defaultCn = getDefaultCnForGroup("int");
    			else defaultCn = Docusafe.getAdditionProperty("dockind.default.int");
    		}
    		else if(type.equals(DocumentLogic.TYPE_OUT_OFFICE))
    		{
    			if (getDefaultCnForGroup("out") != null) 
    				defaultCn = getDefaultCnForGroup("out");
    			else defaultCn = Docusafe.getAdditionProperty("dockind.default.out");
    		}
    		else if(type.equals(DocumentLogic.TYPE_ARCHIVE))
    		{
    			if (getDefaultCnForGroup("archive") != null) 
    				defaultCn = getDefaultCnForGroup("archive");
    			else defaultCn = Docusafe.getAdditionProperty("dockind.default.archive");
    		}
    	}
        if (defaultCn == null || findByCn(defaultCn) == null)
            defaultCn = NORMAL_KIND;
        return defaultCn;
    }

	/**
	 * Sprawdza czy istnieje domy�lny dockind dla grup, do kt�rych nale�y u�ytkownik
	 * @return zwraca domy�lny dockind
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	private static String getDefaultCnForGroup(String docType) throws EdmException, UserNotFoundException {
		DSUser user = DSApi.context().getDSUser();
		String[] groups = user.getGroupsGuid();
		String dockindCn = null;
		for (String group : groups) {
			String temp = Docusafe.getAdditionProperty("dockind.default."+group.toLowerCase()+"."+docType);
			if (temp != null && !temp.isEmpty())
				dockindCn = temp;
		}
		return dockindCn;
	}

    public DocumentKind()
    {
        super();
    }

    public boolean isNormalKind()
    {
        return NORMAL_KIND.equals(cn);
    }

    public static List<DocumentKind> list() throws EdmException
    {
        return Finder.list(DocumentKind.class, "name");
    }

    /**
     * Zwraca list� nazw kodowych wszystkich dost�pnych (wgranych) rodzaj�w dokument�w w tej instancji
     * Docusafe.
     */
    public static String[] listCns() throws EdmException
    {
        List<DocumentKind> list = Finder.list(DocumentKind.class, "name");
        String[] cns = new String[list.size()];
        for (int i=0; i < list.size(); i++)
            cns[i] = list.get(i).getCn();
        return cns;
    }


    @SuppressWarnings("unchecked")
    public static List<DocumentKind> list(boolean onlyEnabled) throws EdmException
    {
        if (onlyEnabled)
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentKind.class);
            criteria.add(Restrictions.eq("enabled", Boolean.TRUE));
            try
            {
                return (List<DocumentKind>) criteria.list();
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
        }
        else
        {
            return list();
        }
    }
    //TODO: DOKONCZYC ten mechanizm 
    /**
     * Zwraca list� tych rodzaj�w dokument�w, kt�re mo�e utworzy� aktualnie zalogowany u�ytkownik.
     * Najpierw pobieram wszystkie "w��czone" i wyrzucam, kt�rych "r�cznie z formatki" dany u�ytkownik
     * nie mo�e utworzy�.
     *
     * @return
     * @throws EdmException
     */
    public static List<DocumentKind> listForCreate() throws EdmException
    {
    	return listForCreate(1);
    }

    /**
     * Zwraca list� tych rodzaj�w dokument�w, kt�re mo�e utworzy� aktualnie zalogowany u�ytkownik.
     * Najpierw pobieram wszystkie "w��czone" i wyrzucam, kt�rych "r�cznie z formatki" dany u�ytkownik
     * nie mo�e utworzy�. type oznacza rodzaj formatki kancelaria archiwum
     *
     * @return
     * @throws EdmException
     */
    public static List<DocumentKind> listForCreate(Integer type) throws EdmException
    {
        List<DocumentKind> list = list(true);
        List<DocumentKind> results = new ArrayList<DocumentKind>();
        for (DocumentKind kind : list)
        {
        	//TODO:na szybko przerobic 
        	if(AvailabilityManager.isAvailable("disabledNormalInOffice")&& DocumentLogic.TYPE_INTERNAL_OFFICE == 2 && NORMAL_KIND.equals(kind.getCn()))
        	{
        		
        	}
        	else if (kind.logic().canCreateManually()
					&& kind.logic().canReadDocumentDictionaries()
					&& kind.getDockindInfo().getVisibilityFilter().acceptCurrentUser()
                    && kind.getDockindInfo().getCreatingFilter().acceptCurrentUser()
					)
                results.add(kind);
        }
        return results;
    }

    
    
    /**
     * Zwraca list� tych rodzaj�w dokument�w, kt�re mog� si� pojawi� w "select-ie" w
     * edycji/archiwizacji dokumentu.
     *
     * @return
     * @throws EdmException
     */
    public static List<DocumentKind> listForEdit() throws EdmException
    {
        List<DocumentKind> list = list(true);
        List<DocumentKind> results = new ArrayList<DocumentKind>();
        for (DocumentKind kind : list){
            if (kind.logic().canCreateManually() && kind.getDockindInfo().getVisibilityFilter().acceptCurrentUser()){
                results.add(kind);
            }
        }
        return results;
    }

    public static List<DocumentKind> filterByCreatingPermission(List<DocumentKind> list, String currentDocumentKindCn) throws EdmException
    {
        List<DocumentKind> results = new ArrayList<DocumentKind>();
        for (DocumentKind kind : list){
            if (kind.getDockindInfo().getCreatingFilter().acceptCurrentUser()
                    || currentDocumentKindCn.equalsIgnoreCase(kind.getCn())){
                results.add(kind);
            }
        }
        return results;
    }

    public static DocumentKind find(Long id) throws EdmException
    {
        return Finder.find(DocumentKind.class, id);
    }

	public static boolean isAvailable(String cn) throws EdmHibernateException, EdmException{
		try {

			//sprawdzanie licencji
			if(!DockindCryptoDigester.getInstance().isDockindAvailable(cn))	return false;

			return DSApi.context()
				.session()
				.createCriteria(DocumentKind.class)
				.add(Restrictions.eq("cn", cn))
				.setMaxResults(1)
				.list()
				.size() > 0;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    @SuppressWarnings("unchecked")
    public static DocumentKind findByCn(String cn) throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(DocumentKind.class);
            criteria.add(Restrictions.eq("cn", cn));
            List<DocumentKind> list = criteria.list();

            if (list.size() > 0)
            {
                return (DocumentKind) list.get(0);
            }

            return null;//throw new EntityNotFoundException("Nie znaleziono typu dokumentu o cn = "+cn);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public static Optional<DocumentKind> findByCnOptional(String cn) throws EdmException
    {
        return Optional.fromNullable(findByCn(cn));
    }

    private final static List<String> DOCKIND_LIST = Arrays.asList(	
		NORMAL_KIND,
        DocumentLogicLoader.NATIONWIDE_KIND,
        DocumentLogicLoader.NATIONWIDE_DRS_KIND,
        DocumentLogicLoader.DAA_KIND,
        DocumentLogicLoader.ALD_KIND,
        DocumentLogicLoader.ALD_UMOWY_KIND,
        DocumentLogicLoader.DP_KIND,
		DocumentLogicLoader.DP_PTE_KIND,
        DocumentLogicLoader.DF_KIND,
        DocumentLogicLoader.BACKOFFICE_KIND,
        DocumentLogicLoader.ROCKWELL_KIND,
        DocumentLogicLoader.INVOICE_KIND,
        DocumentLogicLoader.DL_KIND,
        DocumentLogicLoader.DC_KIND,
        DocumentLogicLoader.CRMAPP_KIND,
        DocumentLogicLoader.CRMTASK_KIND,
        DocumentLogicLoader.CRMBUSINESS_TASK_KIND,
        DocumentLogicLoader.CRM_VINDICATION_KIND,
        DocumentLogicLoader.COMPLAIN_IRON_KIND,
        DocumentLogicLoader.JYSK_KIND,
        DocumentLogicLoader.INVOICE_IC_KIND,
        DocumentLogicLoader.CONTRACT_IC_KIND,
        DocumentLogicLoader.HARM_KIND,
        DocumentLogicLoader.BANK_KIND,
        DocumentLogicLoader.ZAMOWIENIE,
        DocumentLogicLoader.CRM_MARKETING_TASK_KIND,
        DocumentLogicLoader.PROSIKA,
        DocumentLogicLoader.PROSIKA_BLANK,
        DocumentLogicLoader.DL_BINDER,
        DocumentLogicLoader.SAD_KIND,
        DocumentLogicLoader.BLANK_DOCUMENT_KIND,
        DocumentLogicLoader.AKT_NOTARIALNY,
        DocumentLogicLoader.EMPLOYEE_KIND,
        DocumentLogicLoader.ALTMASTER,
        DocumentLogicLoader.INVOICE_PRESENTATION,
        DocumentLogicLoader.UMOWA_PRESENTATION,
        DocumentLogicLoader.P4_KIND,
        DocumentLogicLoader.EUROPOL_INV,
        DocumentLogicLoader.RADWAR_PRESENTATION,
        DocumentLogicLoader.HOLIDAY_REQUEST,
        DocumentLogicLoader.COMPLAINT,
        DocumentLogicLoader.ABSENCE_REQUEST,
        DocumentLogicLoader.ABSENCE_WITHOUT_PROCCES,
        DocumentLogicLoader.INVOICE_PTE,
        "eservice",
        "plexiprocedura",
        "plexikarta",
        "plexiatest",
        "plexireklamacja",
        "polisowy",
        "agenta",        
        "dpd",
        "zarzadzenie",
        "legislacyjny",
        "komunikat",
        "normal_int",
        "normal_out",
        "normal_arch",
        "oferta",
        "zamowieniePIG",
        "zapotrzebowanie",
        DocumentLogicLoader.DOCSPR_PRESENTATION,
        DocumentLogicLoader.BISK_REK_PRESENTATION,
        "docincase",
        "wf",
        "overtime_app",
        "overtime_receive",
        "doccase",
        "mediator",
        "mediator_show",
        "questionnaire",
        "doctemplate",
        // IFPAN
        "fakt_koszt_ifpan",
        "zapotrzeb_ifpan",
        "contract_ifpan",
        "fakt_zap_ifpan",
        "delegacja_ifpan",
        // P4
        "parcel_registry_in",
        // POLITECHNIKA CZESTOCHOWSKA
        "wniosek_urlop",
        "invoice_cost_pcz",
        "contract_pcz",
        // BECK
        	// potrzeby prezentacji
        "zamowien_beck",
        "fakt_kosz_beck",
        	// end potrzeby prezentacji
        "eco_faktura",
        "task",
        "zadanie",
        // WSSK
        DocumentLogicLoader.WSSK_UTWORZENIE_KONT_SYSTEMOWYCH,
        DocumentLogicLoader.WSSK_WYKORZYSTANIE_SPRZETU_PRYW,
        DocumentLogicLoader.WSSK_WYDANIE_PIECZATKI,
        DocumentLogicLoader.WSSK_ZAPOTRZEBOWANIE_WEW,
        DocumentLogicLoader.WSSK_KARTA_UNIEWAZNIENIE,
        DocumentLogicLoader.WSSK_KARTA_ZAPOTRZEBOWANIE,
        DocumentLogicLoader.WSSK_ZWOLNIONY_PRACOWNIK,
        DocumentLogicLoader.WSSK_UDO,
        DocumentLogicLoader.WSSK_NOWY_PRACOWNIK,
		DocumentLogicLoader.WSSK_POWIADOMIENIE_WYPADEK,
        DocumentLogicLoader.WSSK_UDOSTEP_DOK_ZEWN,
        DocumentLogicLoader.WSSK_UDOSTEP_DOK_ODP,
        DocumentLogicLoader.WSSK_UDOSTEP_DOK_INTERN,
        DocumentLogicLoader.WSSK_PACJENT_NIEUBEZP,
        DocumentLogicLoader.WSSK_REOPEN,
        DocumentLogicLoader.WSSK_FAKTURA,
        DocumentLogicLoader.WSSK_FAKTURA2,
        // end WSSK
        DocumentLogicLoader.OPZZ_CASES,
        DocumentLogicLoader.OPZZ_DOC,
        DocumentLogicLoader.OPZZ_DOC_OPINION,
        DocumentLogicLoader.OPZZ_DOC_QUEST,
        DocumentLogicLoader.OPZZ_PROBLEM,
        DocumentLogicLoader.OPZZ_ODZNAKA,
        DocumentLogicLoader.OPZZ_CZLONKOSTWO,
        DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY,
        DocumentLogicLoader.PROTOKOL_BRAKOWANIA,
        DocumentLogicLoader.PIKA_DOCKIND,
        DocumentLogicLoader.PLG_DOCKIND,
        DocumentLogicLoader.PLG_OUT_DOCKIND,
        //paa
        DocumentLogicLoader.PAA_DOC_IN,
        DocumentLogicLoader.PAA_DOC_OUT,
        DocumentLogicLoader.PAA_DOC_INT,
        DocumentLogicLoader.PAA_PLAN_PRACY,
        DocumentLogicLoader.PAA_WNIOSEK_URLOPOWY,
        DocumentLogicLoader.PAA_PLAN_PRACY_ZAD,
        DocumentLogicLoader.PAA_PLAN_PRACY_PODZAD,
        //adm
        DocumentLogicLoader.ADM_DOC_IN,
        DocumentLogicLoader.ADM_DOC_OUT,
        DocumentLogicLoader.ADM_DOC_INT,
        DocumentLogicLoader.ADM_DOC_BOK,
        
    	
        //nfos
        DocumentLogicLoader.NFOS_EMAIL,
        
        //archiwum zakadowe
        DocumentLogicLoader.ARCHIVES,
        DocumentLogicLoader.ARCHIVES_UDOSTEPNIENIE,
        DocumentLogicLoader.ARCHIVES_WYCOFANIE,
        DocumentLogicLoader.ARCHIVES_PRZEKAZANIE_AP,
        
        //Utp modul pocztowy
        DocumentLogicLoader.EMAIL,
        
        // Archiwum Zak�adowe
        DocumentLogicLoader.AR_PROT_BRAK,

        //INS
        DocumentLogicLoader.INS_COST_INVOICE,
        DocumentLogicLoader.WNIOSEK_ZAP,
        DocumentLogicLoader.INVOICE_GENERAL,
        
        //UKW
        DocumentLogicLoader.UKW_WEW_AKTY_PRAW,
        DocumentLogicLoader.UKW_WNIOSEK_O_UDZIELENIE_PO,
		DocumentLogicLoader.UKW_WNIOSEK_O_ZAPOMOGE,
        DocumentLogicLoader.UKW_WNIOSEK_O_DELEGACJE,
        DocumentLogicLoader.UKW_UMOWA_CYWILNO_PRAWNA,
        DocumentLogicLoader.UKW_WNIOSEK_O_URLOP,
 		DocumentLogicLoader.UKW_PLAN_STUDIOW,
		DocumentLogicLoader.UKW_PODZIAL_STUDIOW_STACJ
	);
    private static ArrayList<String> DOCKIND_LIST_CREATED_BY_USER = new ArrayList<String>(DOCKIND_LIST);
    /**
     * Zwraca list� nazw kodowych dost�pnych (czyli zaimplementowanych) w systemie rodzaj�w dokument�w.
     * Ta funkcja te� zwraca NORMAL_KIND, cho� zapewne dobrze gdyby ten rodzaj by� dodawany podczas tworzenia
     * nowej instancji Docusafe (ewentualnie jego edycja mo�e by� potrzebna, jak si� oka�e, �e XML opisuj�cy
     * 'Dokument zwyk�y' powinien nie by� pusty.
     */
    public static List<String> getAvailableDocumentKindsCn() {
	    if (AvailabilityManager.isAvailable("createDockindByUser"))
	    {
		    return DOCKIND_LIST_CREATED_BY_USER;
	    }
	    return DOCKIND_LIST;
    }

    static DocumentLogicLoader logicLoader = new DocumentLogicLoader();

    /**
     * Metoda pobiera logike dockundu
     * @return
     */
    public DocumentLogic logic()
    {
    	//sprawdzanie licencji
		if(!DockindCryptoDigester.getInstance().isDockindAvailable(cn))
			throw new IllegalStateException("Licencja na rodzaj dokumentow '"+cn+"' wygasla.");

        if (logic == null){
        	logic = logicLoader.getDocumentLogic(cn,getDockindInfo().getProperties().get("logic-class"));
        }
        return logic;
    }

    public FieldsManager getFieldsManager(Long documentId)
    {
        return new FieldsManager(documentId, this);
    }

    /**
     * Metoda inicjujaca dockind, wedlug mnie niepotrzebna
     * @throws EdmException
     */
    public void initialize() throws EdmException
    {
    	try {
			if (getDockindInfo().getDockindFields() == null) {
				if (!DockindCryptoDigester.getInstance().isDockindAvailable(cn)) {
					throw new IllegalStateException("Licencja na rodzaj dokument�w '" + cn + "' wygas�a.");
				}
				loadXmlData();
			}
		}
    	catch (Throwable e)
    	{
    		log.error(e.getMessage(),e);
		}
    }

    /**
     * Metoda zwracaj�ca zainicjalizowane pola dockinda
     * @return 
     * @throws EdmException
     */
    public List<Field> getFields() throws EdmException
    {
    	initialize();
        return getDockindInfo().getDockindFields();
    }

    /**
     * Zwraca map� p�l, kt�re maj� ustawion� flag� required
     * @return
     * @throws EdmException
     */
    public Map<String, Field> getRequiredField() throws EdmException{
        Map<String, Field> results = Maps.newHashMap();
        for(Field field : getFields()){
            if(field.isRequired()){
                results.put(field.getCn(), field);
            }
        }

        return results;
    }

    public Map<String,Field> getFieldsMap() throws EdmException
    {
    	initialize();
        return getDockindInfo().getDockindFieldsMap();
    }

    public List<Field> getFieldsByAspect(String cn) throws EdmException
    {
    	initialize();
    	if(getDockindInfo().getAspectsMap() == null || cn == null || !getDockindInfo().getAspectsMap().containsKey(cn))
    		return getDockindInfo().getDockindFields();
    	else
    		return getDockindInfo().getAspectsMap().get(cn).getFields();
    }

    public Field getFieldByCn(String cn) throws EdmException
    {
        return getFieldsMap().get(cn);
    }

    /**
     * Zwraca pole s�ownika na dockindzie
     * @param dictionaryCn
     * @param subField
     * @return
     */
    public Field getDictionaryField(String dictionaryCn, String subField) throws EdmException {
        Field dicField = getFieldByCn(dictionaryCn);
        for(Field f : dicField.getFields()){
            //jesli slownik jest wielowartosciowy por�wnujemy bez ostatniej warto�ci _1
            String fCn = f.getCnWithoutLastUnderscore(dicField.isMultiple());
            if(fCn.equals(subField))
                return f;
        }

        return null;
    }

    public List<Aspect> getAspectsForUser(String username)  throws EdmException
    {
    	if(this.getDockindInfo().getAspects() == null)
    		return null;

    	List<Aspect> retList = new ArrayList<Aspect>();
    	for(Aspect aspect : this.getDockindInfo().getAspects())
    	{
    		if(aspect.canUse(username))
    		{
    			retList.add(aspect);
    		}
    	}

    	if(retList.isEmpty())
    		retList.add(this.getDockindInfo().getAspectsMap().get("default"));

    	return retList;
    }

    /**
     * Wczytuje z XML zapisanego w bazie informacje o polach biznesowych tego rodzaju dokumentu.
     * Jest ono wykonywane tylko przy pierwszym odwo�aniu.
     *
     * @throws EdmException
     */
    public final synchronized void loadXmlData() throws EdmException {
        if (getDockindInfo().getDockindFields() != null){
            return;
        }
        getDockindInfo().setDockindId(getId());
        org.dom4j.Document doc = getXML();

        readTypes(doc);
		FieldsXmlLoader.readFields(doc.getRootElement().element("fields"), getDockindInfo(), getCn());
        FieldsXmlLoader.readPopups(doc.getRootElement().element("popups"), getDockindInfo());
        readOther(doc.getRootElement().element("other"));
        readChannels(doc.getRootElement().element("channels"));
		readVisible(doc.getRootElement().element("visible-when"));
        readCreatingPermission(doc.getRootElement().element("creating-permission"));
        readLabels(doc.getRootElement().element("labels"));
        readAspects(doc.getRootElement().element("aspects"));
        readBoxLines(doc.getRootElement().element("box-lines"));
        readFolderProvider(doc.getRootElement().element("folder"));
        readArchiveProviders(doc.getRootElement().element("archive"));
		if(doc.getRootElement().element("processes")!= null){
			readProcessesDeclarations(doc.getRootElement().element("processes").asXML());
		}
        readSchemas(doc);

        AvailabilityManager.readAvailable(doc.getRootElement().element("availableProperties"), cn);
        Docusafe.initAddsForDockind(doc.getRootElement().element("addsProperties"), cn);
    }

    private void readArchiveProviders(Element archiveEl) {
        if(archiveEl == null) return;
        getDockindInfo().setTitleProvider(parseProvider(archiveEl.element("title")));
        getDockindInfo().setDescriptionProvider(parseProvider(archiveEl.element("description")));
    }

    private static Function<DocFacade, String> parseProvider(Element el){
        if(el == null) return null;
        if(el.attributeValue("template") != null){
            final String template = el.attributeValue("template");
            return new Function<DocFacade, String> () {
                public String apply(DocFacade document) {
                    StringTemplate st = new StringTemplate(template);
                    try {
                        st.setAttribute("fields", document.getFields());
                    } catch (Exception ex) {
                        throw new RuntimeException(ex.getMessage());
                    }
                    return st.toString();
                }
            };
        } else {
            throw new IllegalArgumentException("nieznany provider : " + el.asXML());
        }
    }

    private void readFolderProvider(Element folderElement) {
        if(folderElement == null) return;
        String splitter = folderElement.attributeValue("split-sequence");
        if(splitter == null){
            splitter = "/";
        }
        String pattern = folderElement.getTextTrim();
        log.info("readFolderProvider splitter {} pattern {}", splitter, pattern);
        getDockindInfo().setFolderResolver(new StringPatternResolver(pattern,  splitter));
    }

    private void readTypes(org.dom4j.Document doc) {
        String stringTypes = doc.getRootElement().attributeValue("types");
        if(stringTypes != null){
            Set<DocumentType> types = EnumSet.noneOf(DocumentType.class);
            for(String type: stringTypes.split(",")){
                types.add(DocumentType.forName(type));
            }
            getDockindInfo().setDocumentTypes(types);
        }
        log.info("dockind {} available types: {}", getCn(), getDockindInfo().getDocumentTypes());
    }

    private void readSchemas(org.dom4j.Document doc) throws EdmException {
        log.info("loading schemas");
        Element schemasEl = doc.getRootElement().element("schemas");

        if(schemasEl != null && "drools".equals(schemasEl.attributeValue("resolver"))){
            log.info("drools resolver");
            getDockindInfo().setSchemeResolver(DroolsSchemeResolver.fromSource(schemasEl.getText()));
        } else { //domy�lnie wykorzystywany jest najstarszy resolver
            log.info("old xml resolver");
            LinkedList<DockindScheme> list = new LinkedList<DockindScheme>();
            DockindScheme.readSchemas(schemasEl, list);
            if(list.isEmpty()) {
                getDockindInfo().setSchemeResolver(SchemeResolver.NULL);
            } else {
                getDockindInfo().setSchemeResolver(new OldSchemeResolver(list));
            }
        }
    }

    public void readVisible(Element xml) throws EdmException{
		if(xml != null){
			log.info("ograniczenie widoczno�ci rodzaju dokumentu");
			getDockindInfo().setVisibilityFilter(UserFilters.fromXml(xml));
			log.info("rodzaj dokumentu widoczny gdy:" + getDockindInfo().getVisibilityFilter());
		} else {
			log.info("brak restrykcji widoczno�ci");
		}
	}

    public void readCreatingPermission(Element xml) throws EdmException{
        if(xml != null){
            log.info("ograniczenie mo�liwo?ci rejestracji rodzaju dokumentu");
            getDockindInfo().setCreatingFilter(UserFilters.fromXml(xml));
            log.info("rejestrowanie rodzaju dokumentu mo�liwe gdy:" + getDockindInfo().getCreatingFilter());
        } else {
            log.info("brak zdefiniowanych wymaga� do rejestracji rodzaju dokumentu");
        }
    }

	public void readProcessesDeclarations(String xml) throws EdmException{
		getDockindInfo().setProcessesDeclarations(ProcessDefinitionXMLParser.parse(xml));
	}

	//TODO mo�naby generowa� ten string(albo od razu prepared statement) tylko raz
	private String getInsertStatement() throws EdmException {
		final List<Field> fields = getFields();
		// wstawiamy nowy wiersz
		boolean first = true;
		StringBuilder builder = new StringBuilder("insert into ");
		builder.append(getTablename()).append(" (");
		for (Field field : fields) {
			// wielowartosciowe trzymane w innej tabeli; p�l ukrytych i tylko do odczytu nie zmieniamy
			if (!field.isMultiple()
                    && /*!field.isReadOnly()*/ (field.isSaveHidden() || !field.isHidden()) 
					&& field.isPersistedInColumn()){
				if (!first) {
					builder.append(", ");
				} else {
					first = false;
				}
				builder.append(field.getColumn());
			}
		}
		if (!first) {
			builder.append(", ");
		}
		builder.append("document_id) values (");
		first = true;
		for (int i = 0; i < fields.size() + 1; i++) {
			// wielowartosciowe trzymane w innej tabeli; p�l ukrytych i tylko do odczytu nie zmieniamy
			if ((i == fields.size()
                    || (fields.get(i).isPersistedInColumn()
                        && !fields.get(i).isMultiple()
					/*  && !fields.get(i).isReadOnly() */
                        && (fields.get(i).isSaveHidden() || !fields.get(i).isHidden())    ))) {
				if (!first) {
					builder.append(", ");
				} else {
					first = false;
				}
				builder.append("?");
			}
		}
		builder.append(")");
		return builder.toString();
	}

	//TODO mo�naby generowa� ten string(albo od razu prepared statement) tylko raz
	private String getUpdateStatement() throws EdmException {
		String statement;
		StringBuilder builder;
		final List<Field> fields = getFields();
		// aktualizujemy istniejacy wiersz
		if (fields.size() == 0) {
			// jak nie ma zadnych pol w typie to nie ma czego aktualizowac
			return null;
		}
		boolean first = true;
		builder = new StringBuilder("update ");
		builder.append(getTablename());
		builder.append(DSApi.rowlockString());
		builder.append(" set ");
		for (Field field : fields)
		{
			// wielowartosciowe trzymane w innej tabeli; p�l ukrytych i tylko do odczytu nie zmieniamy
			if (!field.isMultiple()
                    && /*!field.isReadOnly() &&*/ (field.isSaveHidden() || !field.isHidden()) 
					&& field.isPersistedInColumn()){
				if (!first) {
					builder.append(", ");
				} else {
					first = false;
				}
				builder.append(field.getColumn());
				builder.append(" = ?");
			}
		}
		builder.append(" where document_id = ?");
		statement = builder.toString();
		return statement;
	}

    /**
     * Metoda odczytuje etykiety zdefiniowane na dokumencie
     * @param elOther
     * @throws EdmException
     */
    private void readLabels(Element elOther) throws EdmException
    {
    	if (elOther != null)
        {
    		List<org.dom4j.Element> labels = elOther.elements("label"); 

    		LabelDefinition ld;
            final List<LabelDefinition> labelsDefs = Lists.newArrayList();
            for(org.dom4j.Element label : labels){
	    		ld= new LabelDefinition();
	    		if(label.attributeValue("hidden")!=null&&!label.attributeValue("hidden").equals("")) ld.setHidding(Boolean.parseBoolean(label.attributeValue("hidden")));
	    		if(label.attributeValue("inactionTime")!=null&&!label.attributeValue("inactionTime").equals("")) ld.setInactionTime(Integer.parseInt(label.attributeValue("inactionTime")));
	    		if(label.attributeValue("deletesAfter")!=null&&!label.attributeValue("deletesAfter").equals("")) ld.setDeletesAfter(Boolean.parseBoolean(label.attributeValue("deletesAfter")));
	    		if(label.attributeValue("ancestorName")!=null&&!label.attributeValue("ancestorName").equals("")) ld.setAncestorName(label.attributeValue("ancestorName"));
	    		ld.setName(label.attributeValue("name"));
                ld.setConditionMixing(label.attributeValue("condition-mixing"));
                ld.setDescription(label.element("description").getText());
                if(label.attributeValue("strong")!=null && label.attributeValue("strong").equals("false"))
                {
                	ld.setStrong(false);
                }
                else 
                	ld.setStrong(true);
	    		List elConditionList = label.elements("condition");
                if (elConditionList != null)
                {
                    for (Iterator conIter=elConditionList.iterator(); conIter.hasNext(); )
                    {
                     	Element elCon = (Element) conIter.next();
                     	Condition con = new Condition();
                     	con.setField(elCon.attributeValue("field"));
                     	con.setOperator(elCon.attributeValue("operator"));
                     	con.setValue(elCon.attributeValue("value"));
                     	ld.addCondition(con);
                    }
                }
	    		labelsDefs.add(ld);
    		}
    		for(LabelDefinition l: labelsDefs)
    		{
    			Label label;
				if(!LabelsManager.existsLabel(l.getName(), Label.LABEL_TYPE))
				{
	        		label = new Label();
	        		if(l!=null && LabelsManager.findSystemLabelByName(l.getAncestorName(), Label.LABEL_TYPE)!=null)
	        			label.setAncestorId(LabelsManager.findSystemLabelByName(l.getAncestorName(), Label.LABEL_TYPE).getId());
	        		label.setDescription(l.getDescription());
	        		label.setHidden(l.getHidding());
	        		if(l.getInactionTime()==null) label.setInactionTime(null);
	        		else label.setInactionTime(l.getInactionTime()*24);
	        		label.setName(l.getName());
	        		label.setParentId(0L);
	        		label.setModifiable(false);
	        		label.setDeletesAfter(l.getDeletesAfter());
	        		label.setType(Label.LABEL_TYPE);
	        		label.setReadRights(Label.SYSTEM_LABEL_OWNER);
	        		label.setWriteRights(Label.NONE_OWNER);
	        		label.setModifyRights(Label.NONE_OWNER);
	        		Persister.create(label);
	        		DSApi.context().session().flush();
				}
    		}
            setLabelsDefs(labelsDefs);
        }
    }

    /**
     * wczytuje z elementu z XMLa pozosta�� cz�� parametryzacji rodzaju dokumentu (znajduj�c�
     * si� wewn�trz tag-u "other")
     * @throws IOException
     */
    private void readOther(Element elOther) throws EdmException
    {
        getDockindInfo().setAttachmentRequired(false);
        if (elOther != null)
        {
            // dane o tabeli trzymaj�cej warto�ci atrybut�w wielowarto�ciowych
            org.dom4j.Element elMultipleTable = elOther.element("multiple-table");
            if (elMultipleTable != null)
                getDockindInfo().setMultipleTableName(elMultipleTable.attributeValue("name"));
            // "ustawienia" za��cznik�w
            org.dom4j.Element elAttachment = elOther.element("attachment");
            if (elAttachment != null)
                getDockindInfo().setAttachmentRequired("true".equals(elAttachment.attributeValue("required")));

            readValidationRules(elOther.element("validation"));
            readAcceptances(elOther.element("acceptances"));
            readIcrCutter(elOther.element("IcrCutter"));
            readReports(elOther.element("reports"));
            readProperties(elOther.element("properties"));
            readReminders(elOther.element("reminders"));
            readTemplates(elOther.element("templates"));
        }
    }

    private void readReminders(Element elProperties) {
		if (elProperties != null) {
			
			List<Element> propTags = elProperties.elements("reminder");
			if (propTags != null) {
				for (Element elProp : propTags) {
					Reminder reminder = new Reminder();
					
					reminder.setName(elProp.attributeValue("name"));
					reminder.setPeriod(elProp.attributeValue("period"));
					
					Element queryElement = elProp.element("sql");
					if (queryElement != null) {
						reminder.setSearchQuery(queryElement.getText());
					}
					
					Element recipientElement = elProp.element("recipient");
					if (recipientElement != null) {
						String recipientType = recipientElement.attributeValue("type");
						if ("username".equals(recipientType))
							reminder.setRecipientType(RecipientType.USERNAME);
						if ("usernames".equals(recipientType))
							reminder.setRecipientType(RecipientType.USERNAMES);
						if ("guid".equals(recipientType))
							reminder.setRecipientType(RecipientType.GUID);
						if ("task_list_users".equals(recipientType))
							reminder.setRecipientType(RecipientType.TASK_LIST);
						if ("username_field_from_dictionary".equals(recipientType))
							reminder.setRecipientType(RecipientType.USERNAME_FIELD_FROM_DICTIONARY);
						reminder.setRecipientSplitSeq(recipientElement.attributeValue("split-sequence"));
						reminder.setRecipient(recipientElement.getText());
					}
					
					Element templateElement = elProp.element("template");
					if (templateElement != null) {
						reminder.setTemplateFileName(templateElement.attributeValue("file-name"));
					}
					
					getDockindInfo().addReminder(reminder);
				}
			}
		}
    }
    
    private void readTemplates(Element elTemplates) {
    	getDockindInfo().setTemplates(new LinkedHashMap<String, String>());
    	
    	if (elTemplates != null) {
    		List templateTags = elTemplates.elements("template");
    		if (templateTags != null) {
    			for (Iterator iter=templateTags.iterator(); iter.hasNext(); ) {
    				Element elTemplate = (Element) iter.next();
    				if (elTemplate.attributeValue("file-name") != null && elTemplate.attributeValue("label") != null) {
    					getDockindInfo().getTemplates().put(elTemplate.attributeValue("file-name"), elTemplate.attributeValue("label"));
    				}
    			}
    		}
    	}
    }
    
    private void readProperties(Element elProperties) {
        getDockindInfo().setProperties(new LinkedHashMap<String, String>());
        setDefaultPropertiesValues(getDockindInfo().getProperties());

        if (elProperties != null)
        {
            List propTags = elProperties.elements("property");
            if (propTags != null)
            {
                //to nie jest tu potrzebne - przeciez domyslnie wartosci zostana nadpisane
                //properties = new LinkedHashMap<String, String>();
                for (Iterator iter=propTags.iterator(); iter.hasNext(); )
                {
                    Element elProp = (Element) iter.next();
                    if (elProp.attributeValue("name") != null && elProp.attributeValue("value") != null)
                        getDockindInfo().getProperties().put(elProp.attributeValue("name"), elProp.attributeValue("value"));
                }
            }
        }
    }

    private void readReports(Element elReports) throws EdmException {
        if (elReports != null)
        {
            getDockindInfo().setReports(new LinkedHashMap<String, Report>());
            List elRepList = elReports.elements("report");
            for (Iterator itemIter=elRepList.iterator(); itemIter.hasNext();)
            {
                Element elRep = (Element) itemIter.next();
                Report report = new Report();
                report.setCn(elRep.attributeValue("cn"));
                report.setNames(XmlUtils.readLanguageValues(elRep, "name"));
                report.setDescriptions(XmlUtils.readLanguageValues(elRep,"description"));
                report.setReportId(elRep.attributeValue("id"));
                String type = elRep.attributeValue("type");
                if (Report.CSV_ONLY.equals(type)) {
                    LogFactory.getLog("kuba").debug("raport csv");
                    report.setType(type);
                } else {
                    LogFactory.getLog("kuba").debug("raport nie csv");
                }


                // kryteria w raporcie
                Element elCriteria = elRep.element("criteria");
                if (elCriteria != null)
                {
                    List elCritList = elCriteria.elements("criterion");
                    for (Iterator critIter=elCritList.iterator(); critIter.hasNext();)
                    {
                        Element elCrit = (Element) critIter.next();
                        try
                        {
                            Integer id = Integer.valueOf(elCrit.attributeValue("id"));
                            report.getCriteria().put(id ,new Report.Criterion(elCrit.attributeValue("type"), elCrit.attributeValue("fieldCn"), XmlUtils.readLanguageValues(elCrit,"name"), "true".equals(elCrit.attributeValue("required"))));
                        }
                        catch (NumberFormatException e)
                        {
                            throw new EdmException("Znaleziono niepoprawne id dla pewnego kryterium w raporcie");
                        }
                    }
                }
                // ustawienia raportu
                Element elProperties = elRep.element("properties");
                if (elProperties != null)
                {
                    List elPropList = elProperties.elements("property");
                    for (Iterator propIter=elPropList.iterator(); propIter.hasNext(); )
                    {
                        Element elProp = (Element) propIter.next();
                        if (elProp.attributeValue("name") != null && elProp.attributeValue("value") != null)
                            report.getProperties().put(elProp.attributeValue("name"), elProp.attributeValue("value"));
                    }
                }
                getDockindInfo().getReports().put(report.getCn(), report);
            }
        }
    }

    private void readIcrCutter(Element elIcrCutter) {
        if(elIcrCutter != null)
        {
            Element power = elIcrCutter.element("power");
            if(power != null)
            {
                if(power.attributeValue("value").equalsIgnoreCase("on"))
                    getDockindInfo().setIsIcrCutterOn(new Boolean(true));
                else
                    getDockindInfo().setIsIcrCutterOn(new Boolean(false));
            }

            Element hotIcrFolder = elIcrCutter.element("hotIcrFolder");
            if(hotIcrFolder != null)
            {
                getDockindInfo().setIcrCutterFolder(hotIcrFolder.attributeValue("value"));
            }

            Element cut = elIcrCutter.element("cut");
            if(cut != null)
            {
                getDockindInfo().setIcrCutterCut(new ArrayList<String>());
                List<Element> cutList = cut.elements("when");
                for(Element cutWhen : cutList)
                {
                    getDockindInfo().getIcrCutterCut().add(cutWhen.attributeValue("cn")+":"+cutWhen.attributeValue("value"));
                }
            }
        }
    }

    private void readAcceptances(Element elAcceptances) {
        if (elAcceptances != null && "true".equals(elAcceptances.attributeValue("enabled")))
        {
            getDockindInfo().setAcceptancesDefinition(new AcceptancesDefinition());
            List elAccList = elAcceptances.elements("acceptance");
            for (Iterator itemIter=elAccList.iterator(); itemIter.hasNext(); )
            {
                Element elAcc = (Element) itemIter.next();
                getDockindInfo().getAcceptancesDefinition().addAcceptance(elAcc.attributeValue("cn"),
                        elAcc.attributeValue("name"),
                        elAcc.attributeValue("fieldCn"),elAcc.attributeValue("level"), elAcc.attributeValue("transitionName"));
            }
            getDockindInfo().getAcceptancesDefinition().setCentrumKosztFieldCn(elAcceptances.attributeValue("centrum-koszt"));
            getDockindInfo().getAcceptancesDefinition().setGivingDifferentAcceptancesEnabled("true".equals(elAcceptances.attributeValue("give-diff-accepts-enabled")));

            Element elFinal = elAcceptances.element("finalAcceptance");
            if (elFinal != null)
            {
                FinalAcceptance finalAcceptance = new FinalAcceptance();
                finalAcceptance.setFieldCn(elFinal.attributeValue("fieldCn"));
                finalAcceptance.setExactAmountFieldCn(elFinal.attributeValue("exact-amount"));
                String string = elFinal.attributeValue("min-diff-accepts");
                if (string != null)
                    finalAcceptance.setMinDifferentAcceptances(Integer.parseInt(string));
                string = elFinal.attributeValue("required-accepts");
                if (string != null)
                    finalAcceptance.setRequiredAcceptances(string.split("\\s*;\\s*"));
                getDockindInfo().getAcceptancesDefinition().setFinalAcceptance(finalAcceptance);
            }
        }
    }

    private void readValidationRules(Element elValidation) {
        if (elValidation != null)
        {
            List ruleTags = elValidation.elements("rule");
            if (ruleTags != null)
            {
                getDockindInfo().setValidationRules(new ArrayList<ValidationRule>(ruleTags.size()));
                for (Object ruleTag : ruleTags) {
                    Element elRule = (Element) ruleTag;
                    ValidationRule rule = new ValidationRule();
                    if (elRule.attributeValue("cn") != null && elRule.attributeValue("type") != null) {
                        rule.setFieldCn(elRule.attributeValue("cn"));
                        rule.setType(elRule.attributeValue("type"));
                        rule.setValue(elRule.attributeValue("value"));
                        getDockindInfo().getValidationRules().add(rule);
                    }
                }
            }
        }
    }

    private void setDefaultPropertiesValues(Map<String, String> prop)
    {
    	try
    	{
    		Properties propTmp = new Properties();
    		propTmp.load(Docusafe.class.getClassLoader().getResourceAsStream("default.properties"));
    		Set<Object> keys = propTmp.keySet();
    		for(Object obj : keys)
    		{
    			prop.put(obj.toString(), (String)propTmp.get(obj));
    		}
    	}
    	catch (IOException e)
    	{
			log.error("ERR-010: B��d odczytu pliku default properties",e);
		}

	}

    /**
     * Metoda odczytuje aspekty zdefiniowane na dokumencie
     * @throws EdmException
     */
    private void readAspects(Element elAspects) throws EdmException
    {
    	if(elAspects != null)
    	{
    		//this.getDockindInfo().setAspects(new ArrayList<Aspect>());
    		this.getDockindInfo().setAspectsMap(new HashMap<String, Aspect>());

    		List elAspectList = elAspects.elements("aspect");
    		for(Iterator elementIterator = elAspectList.iterator(); elementIterator.hasNext(); )
    		{
    			Element elAspect = (Element) elementIterator.next();
    			Aspect aspect = new Aspect(elAspect.attributeValue("cn"), elAspect.attributeValue("name"), elAspect.attributeValue("guid"), this.getCn());

    			List elFieldList = elAspect.elements("field");
    			for(Iterator fieldIterator = elFieldList.iterator(); fieldIterator.hasNext(); )
    			{
    				Element elField = (Element) fieldIterator.next();
    				aspect.addField(this.getFieldByCn(elField.attributeValue("cn")));

    				List elEnumList = elField.elements("enum");
    				for(Iterator enumIterator = elEnumList.iterator(); enumIterator.hasNext(); )
    				{
    					Element elEnum = (Element) enumIterator.next();
    					aspect.addEnumItem(elField.attributeValue("cn"), this.getFieldByCn(elField.attributeValue("cn")).getEnumItemByCn(elEnum.attributeValue("cn")));
    				}

    			}
    			//this.getDockindInfo().getAspects().add(aspect);
    			this.getDockindInfo().getAspectsMap().put(aspect.getCn(), aspect);
    		}
    	}
    }

	/**
     * wczytuje z elementu z XMLa dotyczaca lini pudel (znajduj�c�
     * si� wewn�trz tag-u "box-lines")
     */
    private void readBoxLines(Element elLines) throws EdmException
    {
        if (elLines != null)
        {
        	List elLineList = elLines.elements("box-line");
            if (elLineList != null)
            {
            	List<BoxLine> boxesLines = new ArrayList<BoxLine>();
            	getDockindInfo().setBoxesLines(boxesLines);
                for (Iterator itemIter=elLineList.iterator(); itemIter.hasNext(); )
                {
                    Element elChn = (Element) itemIter.next();
                    BoxLine boxLine = new BoxLine();
                    boxLine.setCn(elChn.attributeValue("cn"));
                    boxLine.setName(elChn.attributeValue("name"));
                    boxLine.setConditionMixing(elChn.attributeValue("condition-mixing"));

                    // warunki w kanale
                    List elConditionList = elChn.elements("condition");
                    if (elConditionList != null)
                    {
                        for (Iterator conIter=elConditionList.iterator(); conIter.hasNext(); )
                        {
                        	Element elCon = (Element) conIter.next();
                        	Condition con = new Condition();
                        	con.setField(elCon.attributeValue("field"));
                        	con.setOperator(elCon.attributeValue("operator"));
                        	con.setValue(elCon.attributeValue("value"));
                        	boxLine.addCondition(con);
                        }
                    }
                    boxesLines.add(boxLine);
                }
            }
        }
    }

	/**
     * wczytuje z elementu z XMLa dotyczaca kanalow koordynatorow (znajduj�c�
     * si� wewn�trz tag-u "chanells")
     */
    private void readChannels(Element elChanells) throws EdmException {
        if(elChanells == null){
            return;
        }
        List<Element> elProviders = elChanells.elements("coordinator-provider");
        List<ChannelProvider> providers = Lists.newArrayList();
        for(Element elProvider: elProviders){
            ChannelProvider provider;
            String type = elProvider.attributeValue("type");
            if(type.equals("database-enum")){
                provider = new DataBaseEnumChannelProvider(
                        (elProvider.element("field").getText()),
                        cn,
                        elProvider.element("prefix").getText(),
                        Integer.valueOf(elProvider.element("first-id").getText()));
            } else {
                throw new EdmException("unkown provider type : " + type);
            }
            providers.add(provider);
        }
        getDockindInfo().setChannelProviders(providers);

        List elChannelList = elChanells.elements("channel");
        List<ProcessChannel> processChannels = new ArrayList<ProcessChannel>();
        for (Object anElChannelList : elChannelList) {
            Element elChn = (Element) anElChannelList;
            ProcessChannel pChannel = new ProcessChannel();
            pChannel.setCn(elChn.attributeValue("cn"));
            pChannel.setName(elChn.attributeValue("name"));
            pChannel.setConditionMixing(elChn.attributeValue("condition-mixing"));

            // warunki w kanale
            List elConditionList = elChn.elements("condition");
            if (elConditionList != null) {
                for (Object anElConditionList : elConditionList) {
                    Element elCon = (Element) anElConditionList;
                    Condition con = new Condition();
                    con.setField(elCon.attributeValue("field"));
                    con.setOperator(elCon.attributeValue("operator"));
                    con.setValue(elCon.attributeValue("value"));
                    pChannel.addCondition(con);
                }
            }
            processChannels.add(pChannel);
        }
        getDockindInfo().setStaticProcessChannels(processChannels);
    }


    public BoxLine getBoxLine(Document document) throws EdmException
    {
        if(getDockindInfo().getBoxesLines() != null && document != null)
        {
	    	FieldsManager fm = document.getFieldsManager();
	    	if(fm != null)
	    	{
		    	for(BoxLine boxline : getDockindInfo().getBoxesLines())
		    	{
					boolean andMixing = boxline.getConditionMixing().equals(DockindScheme.CONDITION_MIXING_AND);
					boolean equal = true;

					log.debug("boxline : " + boxline.getCn());
		    		for(Condition con: boxline.getConditions())
		    		{
		    			if(con.getOperator().equals(Condition.OPERATOR_IS_NULL))
		    			{
		    				if (fm.getKey(con.getField()) == null) {
								if (andMixing) {
									continue;
								} else {
									return boxline;
								}
							} else {
								if (andMixing) {
									equal = false;
									break;
								}
							}
		    			}
		    			else if(con.getOperator().equals(Condition.OPERATOR_NOT_NULL))
		    			{
		    				if(fm.getKey(con.getField()) != null)
		    					if(andMixing) continue;
				    			else
				    			{
				    				return boxline;
				    			}
		    				else
			    			{
			    				if(andMixing)
			    				{
			    					equal = false; break;
			    				}
			    			}
		    			}
		    			else
		    			{
			    			if(con.getField() == null || fm.getKey(con.getField()) == null)
			    			{
								log.debug("boxline : null value" + con.getField());
			    				equal = false; break;
			    			}
			    			String value = fm.getKey(con.getField()).toString();
							log.debug("boxline con ={}, value={}", con.getValue(), value);
			    			Integer result = value.compareTo(con.getValue());
			    			if(con.getOperatorAsInteger()== 0 && result == 0) {
				    			if(andMixing) continue;
				    			else return boxline;
							} else if(con.getOperatorAsInteger()>0 && result >0) {
				    			if(andMixing) continue;
				    			else return boxline;
							} else if(con.getOperatorAsInteger()<0 && result <0) {
				    			if(andMixing) continue;
				    			else return boxline;
							} else {
			    				if(andMixing)
			    				{
			    					equal = false; break;
			    				}
			    			}
			    		}
		    		}
		    		if(equal && andMixing)
		    		{
		    			return boxline;
		    		}
		    	}
	    	}
	    	else
	    	{
	    		log.error("DOCKIND SHEME ERROR : FM is null");
	    	}
        }
    	return null;
    }

    /**
     * Zwraca nazw� kana�u bazuj�c na zdefiniowanych regu�ach i dynamicznych providerach
     * @param documentId
     * @return
     * @throws EdmException
     */
    public String getChannel(Long documentId) throws EdmException {
        log.info("checking providers for document {}", documentId);
        DocFacade df = Documents.document(documentId);
        for(ChannelProvider cp : getDockindInfo().getChannelProviders()){
            log.info("checking provider '{}'", cp.getClass());
            String ret = cp.apply(df);
            if(ret != null) {
                log.info("returning channel '{}' from provider '{}'", ret, cp.getClass());
                return ret;
            }
        }

//    	org.dom4j.Document doc = getXML();
//        readChannels(doc.getRootElement().element("channels"));
        return findStaticChannel(df);
    }

    /**
     * Szuka kana�u w�r�d statycznych definicji z xml'a
     * @param df
     * @return nazw� kana�u lub null w przypadku nie znalezienia �adnego kana�u
     * @throws EdmException
     */
    private String findStaticChannel(DocFacade df) throws EdmException {
        if(!getDockindInfo().getStaticProcessChannels().isEmpty()) {
	    	FieldsManager fm = df.getFieldsManager();
            for(ProcessChannel pc : getDockindInfo().getStaticProcessChannels())
            {
                boolean conditionMixing = ProcessChannel.CONDITION_MIXING_AND.equals(pc.getConditionMixing());
                boolean equal = true;
                for(Condition con: pc.getConditions())
                {
                    if (fm.getKey(con.getField()) == null ) {
                        //zak�adam, �e null nie mo�e by� equals i nie spe�nia �adnego warunku (ale te� nie powoduje �adnych wyj�tk�w)
                        equal = false;
                        break;
                    } else if(con.getOperator().equals("not-null")){
                        continue;
                    }
                    String value = fm.getKey(con.getField()) + ""; //+ "" bo mo�e by� null
                    Integer result = value.compareTo(con.getValue());
                    if(con.getOperatorAsInteger()== 0 && result == 0){
                        if(conditionMixing) continue;
                        else return pc.getCn();
                    } else if(con.getOperatorAsInteger()>0 && result >0) {
                        if(conditionMixing) continue;
                        else return pc.getCn();
                    } else if(con.getOperatorAsInteger()<0 && result <0) {
                        if(conditionMixing) continue;
                        else return pc.getCn();
                    } else {
                        if(conditionMixing)
                        {
                            equal = false; break;
                        }
                    }
                }
                if(equal)
                {
                    return pc.getCn();
                }
            }
        }
        return null;
    }

    public DockindScheme getSheme(FieldsManager fm, String activity) throws EdmException {
    	if (activity == null)
    	{
    		if (fm.getDocumentId() != null)
    		{
    			List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentId(fm.getDocumentId());
    			for (JBPMTaskSnapshot task : tasks)
    			{
    				activity = task.getActivityKey();
    				activity += "proc,"+activity;
    				break;
    			}
    		}
    		
    		if (activity == null)
    			return getSheme(fm);
    	}
        DockindScheme ret = getDockindInfo().getSchemeResolver().provideSheme(fm, activity);
        log.debug("getScheme for '{}' = '{}'", fm == null ? null : fm.getDocumentId(), ret == null ? null : ret.getCn());
        return ret;
    }
    
    public DockindScheme getSheme(FieldsManager fm) throws EdmException {
        DockindScheme ret = getDockindInfo().getSchemeResolver().provideSheme(fm);
        log.debug("getScheme for '{}' = '{}'", fm == null ? null : fm.getDocumentId(), ret == null ? null : ret.getCn());
        return ret;
    }

    /**
     * Zwraca XML opisuj�cy typ dokumentu.  Ponowne wywo�ania
     * nie odwo�uj� si� do bazy, tylko zwracaj� dokument odczytany
     * za pierwszym razem.
     * <p>
     * Je�eli w bazie nie ma XML opisuj�cego typ dokumentu, zwracany jest
     * utworzony ad hoc dokument zawieraj�cy tylko element g��wny.
     */
    public org.dom4j.Document getXML() throws EdmException
    {
    	org.dom4j.Document xmlDocument = getDockindInfo().getXml();
        if (xmlDocument != null)
            return xmlDocument;
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("select content from " +
                    DSApi.getTableName(DocumentKind.class)+" where id = ?");
            pst.setLong(1, getId());
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
                throw new EdmException("Nie mo�na odczyta� obiektu DocumentKind#"+getId());
            Blob blob = null;
            InputStream is =null;
            if(DSApi.isPostgresServer())
            	is = rs.getBinaryStream(1);
            else {
            	blob =  rs.getBlob(1);
            is = blob.getBinaryStream();
            }
            // je�eli w bazie by� XML, odczytuj� go, w przeciwnym razie
            // tworz� pusty dokument (tylko g��wny element)
            if (blob != null ||is!=null ){
            
                SAXReader reader = new SAXReader();
				try
				{
					xmlDocument = reader.read(is);
				}
				catch(Exception e)
				{
					//e.printStackTrace();
					LoggerFactory.getLogger("mariusz").error("Nie udalo si� odczyta� XML proba przeczytania przez plik");
					File file = File.createTempFile("docusafe_report_", ".xml");
					OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
					byte[] buffer = new byte[1024];
					int count = 0;
					while ((count = is.read(buffer)) > 0)
					{
						os.write(buffer, 0, count);
					}
					org.apache.commons.io.IOUtils.closeQuietly(is);
					org.apache.commons.io.IOUtils.closeQuietly(os);
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					String line = null;
					StringBuilder sb = new StringBuilder();
					sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					while((line = br.readLine())!=null)
					{
						sb.append(line);
						sb.append("\n");
					}
					br.close();
					log.error(sb.toString());
					xmlDocument = DocumentHelper.parseText(sb.toString());
				}
                getDockindInfo().setXml(xmlDocument);
                org.apache.commons.io.IOUtils.closeQuietly(is);
            }
            else
            {
                org.dom4j.DocumentFactory factory = new org.dom4j.DocumentFactory();
                xmlDocument = factory.createDocument(factory.createElement("doctype"));
            }
            return xmlDocument;
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e, "id="+id);
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (Exception e)
        {
            throw new EdmException("B��d odczytu XML, cn = "+cn, e);
        }
        finally
        {
        	DbUtils.closeQuietly(pst);
        }
    }

    /**
     * Zapisuje do bazy zawartosc pliku XML opisuj�cego dany rodzaj dokumentu.
     *
     * @param file plik opisuj�cy rejestr
     * @throws EdmException 
     */
  public void saveContent(File file) throws EdmException {
        FileInputStream stream = null;
        try
        {
            stream = new FileInputStream(file);
            int fileSize = (int) file.length();
            PreparedStatement ps = null;
            try
            {
                DSApi.context().session().save(this);
                DSApi.context().session().flush();

                int rows;
                ps = DSApi.context().prepareStatement("update "+DSApi.getTableName(DocumentKind.class)+
                    " set content = ? where id = ?");
                ps.setBinaryStream(1, stream, fileSize);
               // ps.setBlob(1, stream,fileSize);
                //ps.setB
                ps.setLong(2, this.getId().longValue());
                rows = ps.executeUpdate();
                if (rows != 1){
                    throw new EdmException(sm.getString("documentHelper.blobInsertFailed", new Integer(rows)));
                }
                this.getDockindInfo().setXml(null);
            }
            catch (HibernateException e)
            {
                throw new EdmHibernateException(e);
            }
            catch (SQLException e)
            {
                throw new EdmSQLException(e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu pliku", e);
        }
        finally
        {
            if (stream != null) try { stream.close(); } catch (Exception e) { }
        }
    }

    /**
     * Zapisuje do tabeli dotyczacej danej dockind-a wartosci dokumentu o id 'documentId'. Gdy juz taki
     * wiersz istnia� wykonywany jest update, wpp wstawiany nowy wiersz.
     * <b>Gdy nie ma jakiego� pola w 'fieldValues' to w�wczas jest mu przypisywany null! Patrz: {@link #setOnly(Long, Map)}</b>
     *
     * UWAGA: Ze wzgl�d�w bezpiecze�stwa pola 'readonly' i 'hidden' nie s� tu zapisywane/zmieniane (nawet je�li w
     *        fieldValues znajduje si� warto�� dla tego pola)!
     *        W celu zmiany warto�ci pola 'readonly' lub 'hidden'  nale�y u�y� metody {@link #setOnly(Long, Map)}.
     *
     * @see this.setOnly(Long, Map)
     * @param documentId
     * @param fieldValues
     * @throws EdmException
     */
    public void set(Long documentId, Map<String, ?> fieldValues) throws EdmHibernateException, EdmSQLException, EdmException
    {
        Documents.flushChangesForDocument(documentId);
        PreparedStatement ps = null;
        PreparedStatement psd = null;
        String statement  = null;
        try
        {
            psd = DSApi.context().prepareStatement("select document_id from " + getTablename() + " where document_id = ?");
            psd.setLong(1, documentId);
            ResultSet rs = psd.executeQuery();

			if (!rs.next()) {
				statement = getInsertStatement();
			} else {
				statement = getUpdateStatement();
			}
			if(statement == null) {
				//brak zmian
				return;
			}
			
            ps = DSApi.context().prepareStatement(statement);
            // ustalamy parametry zapytania
			final List<Field> fields = getFields();
            int count = 0;
            for (Field field : fields) {
                if (field.isMultiple()) {
                    //centrum/centra kosztowe NIE jest/s� zapisywane z formatki posiadaj�cej dockind-fields
                    //do edycji centr�w przypisanych do dokumentu (MPK�w) s�u�� do tego oddzielne
                    // akcje (CentrumKosztowDlaFaktury...)
                    if (!(field instanceof CostCenterField)){
                        saveMultipleData(documentId, field, (List<Object>) field.coerce(fieldValues.get(field.getCn())), getMultipleTableName());
                    } //jak jest to nic nie r�b
                }
                else if(Field.DOCLIST.equals(field.getType()))
                {
                	updateDocList(fieldValues,(DocListField) field,false);
                }
                else if ((field.isSaveHidden() || !field.isHidden()) && field.isPersistedInColumn())
                {
                    count++;
                    field.set(ps, count, fieldValues.get(field.getCn()));
                    log.debug("field: {}, val: {}", field.getCn(), fieldValues.get(field.getCn()));
                }
            }
            
            ps.setLong(count+1, documentId);
            ps.executeUpdate();

            final List<CustomPersistence> customPersistedFields = getDockindInfo().getCustomPersistedFields();
            if(!customPersistedFields.isEmpty()){
            	log.info("documentId id = {}", documentId);
            	Document doc = Document.find(documentId);
            	for(CustomPersistence cp : customPersistedFields){
            		String fieldCn = ((Field)cp).getCn();
            		if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu") && !AvailabilityManager.isAvailable("klonowanie.wlacz.zapisDoDziennika")) {
            			if(fieldValues.containsKey(fieldCn) && !fieldCn.equals("JOURNAL")){     
            				cp.persist(doc, fieldValues.get(fieldCn));
            			}
            		} else {
            			if(fieldValues.containsKey(fieldCn)){     
            				cp.persist(doc, fieldValues.get(fieldCn));
            			}
            		}
            	}
            }

            //ps zamykany w finnally
            // dodatkowe czynno�ci zale�ne od rodzaju dokumentu
            onModifyDocument(documentId, fieldValues);
        }
        catch (SQLException e)
        {
        	log.error("DocumentKind cn {}", this.cn);
        	log.error(statement,e);
            EdmSQLException tralala =  new EdmSQLException(e);
            throw (EdmSQLException)tralala.fillInStackTrace();
        }
        catch (HibernateException e)
        {
        	log.error("",e);
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	DSApi.context().closeStatement(psd);
        }
    }

    private void onModifyDocument(long documentId, Map<String, ?> fieldValues, boolean checkPermission) throws EdmException, SQLException {
	        log.info("onModifyDocument");
	        Document doc = Documents.document(documentId).getDocument(checkPermission);
	        doc.setIndexStatus(IndexStatus.DIRTY_INDEX);
	        doc.updateMtime();

            if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
                JackrabbitXmlSynchronizer.createEvent(documentId);
            }

	        if(logic != null) {
                logic().onSaveActions(this, documentId, fieldValues);
            }
	    }
    
    private void onModifyDocument(long documentId, Map<String, ?> fieldValues) throws EdmException, SQLException {
        log.info("onModifyDocument");
        Document doc = Documents.document(documentId).getDocument();
        doc.setIndexStatus(IndexStatus.DIRTY_INDEX);
        doc.updateMtime();
        if(AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            JackrabbitXmlSynchronizer.createEvent(documentId);
        }

        if(logic != null) {
            logic().onSaveActions(this, documentId, fieldValues);
        }
    }

    /**
     * Update wartosci w dokumentach powiazanych
     * @param fieldValues
     * @param field
     * @param setOnly 
     * @throws EdmException
     * @throws SQLException
     */
    private static void updateDocList(Map<String,?> fieldValues,DocListField field, Boolean setOnly) throws EdmException, SQLException
    {
		if (field.getEditFields().size() < 1)
        {
            return;
        }
		String[] ids = null;
		try
		{
			ids = (String[]) fieldValues.get(field.getCn());
		}
		catch (ClassCastException e)
		{
			ids = new String[]{(String) fieldValues.get(field.getCn())};
		}
    	if(ids == null)
    		return;
        DocumentKind docKind = DocumentKind.findByCn(field.getDockind());
    	Map<String,Field> fMap = docKind.getDockindInfo().getDockindFieldsMap();
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	String update = createUpdateStatement(field, fMap, docKind,setOnly,fieldValues);
    	String select = createSelectStatement(field, fMap, docKind);
    	Map<String,Object> oldValues = new HashMap<String, Object>();

 	    try
 	    {
 	    	for (int i = 0; i < ids.length; i++)
	    	{
 	    		ps = DSApi.context().prepareStatement(select);
 	    		ps.setLong(1, Long.parseLong(ids[i]));
 	    		rs = ps.executeQuery();
 	    		if(rs.next())
 	    		{
 	    			for (String fieldCn : field.getEditFields())
 		 	       	{
 	    				oldValues.put(field.getCn()+"_EF_"+fieldCn+ids[i],rs.getObject(fMap.get(fieldCn).getColumn()));
					}
 	    		}
 	    		DSApi.context().closeStatement(ps);
 	    		ps = null;
	    	}
	    	for(int i = 0; i < ids.length; i++)
	    	{
	    		StringBuilder historyBuilder = null;
	    		for (String fieldCn : field.getEditFields())
    			{
    				Object oldDesc = oldValues.get(field.getCn()+"_EF_"+fieldCn+ids[i]);
    				Object newDesc = null;
    				if(setOnly && !fieldValues.containsKey(field.getCn()+"_EF_"+fieldCn+ids[i]))
    					newDesc = oldDesc;
    				else
    					newDesc = fieldValues.get(field.getCn()+"_EF_"+fieldCn+ids[i]);
                    if(notEqualToString(oldDesc,newDesc))
                    {
                        if(AvailabilityManager.isAvailable(docKind.getCn()+".datamart.document.field.change"))
                        {
                        	new DataMartEvent(true,  new Long(ids[i]),null,DataMartDefs.DOCUMENT_FIELD_CHANGE,fieldCn,oldDesc!=null?oldDesc.toString():"", newDesc!=null?newDesc.toString():"", null);
                        }
                    	int count = 0;
        	 	       	ps = DSApi.context().prepareStatement(update);
        	 	       	for (String fCn : field.getEditFields())
        	 	       	{
        	 	       		Field fd = fMap.get(fCn);    
        	 	       		Object efValues =null;
        	 	       	//	if(setOnly && fieldValues.get(field.getCn()+"_EF_"+fd.getCn()+ids[i]) == null)
        	 	       	//		efValues = oldValues.get(field.getCn()+"_EF_"+fd.getCn()+ids[i]);
        	 	       //		else
        	 	       		efValues = fieldValues.get(field.getCn()+"_EF_"+fd.getCn()+ids[i]);        	 	       		    	 	       		
        	 	       		count ++;
        	 	       		fd.set(ps, count, efValues);
        	 	       	}
        	 	       	ps.setLong(count+1, new Long(ids[i]));
        	 	       	ps.execute();
        	 	       	DSApi.context().closeStatement(ps);
        	 	       	ps = null;

        	 	       	Field changeField = fMap.get(fieldCn);
        	 	       	if(changeField instanceof EnumerationField) {
        	 	       		if(newDesc != null) {
        	 	       			newDesc = changeField.getEnumItem(new Integer(newDesc.toString())).getTitle();
        	 	       		}
        	 	       		if(oldDesc != null) {
        	 	       			oldDesc = changeField.getEnumItem(new Integer(oldDesc.toString())).getTitle();
        	 	       		}
        	 	       	}
	    				String change = null;
	    				if (oldDesc == null)
	                       change = changeField.getName() + " " + sm.getString("ZwartosciPustejNa",newDesc);
	    				else if (newDesc == null)
	                       change = changeField.getName() + " " + sm.getString("ZNaWartoscPusta",oldDesc);
	    				else
	                       change = changeField.getName() + " " + sm.getString("Zna",oldDesc,newDesc);
	    				if (historyBuilder == null)
	    					historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(change);
	    				else
	                       historyBuilder.append(", ").append(change);
                    }
				}
	    		if(historyBuilder != null)
	    		{
	    			Audit audit = Audit.createWithPriority("changeFields", DSApi.context().getPrincipalName(),
	                        historyBuilder.toString(), null, Audit.LOWEST_PRIORITY);
					try
					{
						DataMartManager.addHistoryEntry(Long.parseLong(ids[i]), audit);
					}
					catch (Exception e)
					{
						log.error("",e);
						throw new EdmException(e);
					}
	    		}
	    	}
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

    private static String createUpdateStatement(DocListField field,Map<String,Field> fMap,DocumentKind docKind, Boolean setOnly, Map<String, ?> fieldValues)
    {
    	StringBuilder builder = null;
        boolean first = true;
        builder = new StringBuilder("update ");
        builder.append(docKind.getTablename()).append(" set ");

        for (String fieldCn : field.getEditFields())
        {
            Field fd = fMap.get(fieldCn);
            if (fd != null && !fd.isMultiple())
            {
                if (!first)
                    builder.append(", ");
                else
                    first = false;
                builder.append(fd.getColumn());
                builder.append(" = ?");
            }
        }
        builder.append(" where document_id = ?");
        return builder.toString();
    }

    private static String createSelectStatement(DocListField field,Map<String,Field> fMap,DocumentKind docKind)
    {
    	StringBuilder builder = null;
        boolean first = true;
        builder = new StringBuilder("select ");

        for (String fieldCn : field.getEditFields())
        {
            Field fd = fMap.get(fieldCn);
            if (fd != null && !fd.isMultiple())
            {
                if (!first)
                    builder.append(", ");
                else
                    first = false;
                builder.append(fd.getColumn());
            }
        }
        builder.append(" from ").append(docKind.getTablename());
        builder.append(" where document_id = ?");
        return builder.toString();
    }

    /**
     * Zapisuje do tabeli dotyczacej danej dockind-a wartosci dokumentu o id 'documentId'. Gdy juz taki
     * wiersz istnia� wykonywany jest update, wpp wstawiany nowy wiersz.
     * <b>Tylko uaktualniane te pola, kt�re podane w 'fieldValues', zatem je�li kt�rego� pola
     * tam nie ma to jego warto�� w bazie nie ulegnie zmianie.</b>
     *
     * @param documentId
     * @param fieldValues
     * @throws EdmException
     */
    public void setOnly(Long documentId, Map<String,?> fieldValues) throws EdmException
    {
	    setOnly(documentId, fieldValues, true);
    }
    public void setOnly(Long documentId, Map<String,?> fieldValues, boolean checkPermission) throws EdmException
    {
        Documents.flushChangesForDocument(documentId);
        PreparedStatement ps = null;
        PreparedStatement pst = null;
        try
        {
        	Boolean setAnyField = false;
            Set<String> fieldCns = fieldValues.keySet();
            final StringBuilder builder = new StringBuilder();
            pst = DSApi.context().prepareStatement("select document_id from "+getTablename()+" where document_id = ?");
            pst.setLong(1, documentId);
            ResultSet rs = pst.executeQuery();
            if (!rs.next())
            {
                setAnyField = prepareSetOnlyCreate(fieldValues, fieldCns, builder);
            }
            else // aktualizujemy istniejacy wiersz
            {

                if (fieldValues.keySet().size() == 0)
                {
                    return;
                }

                setAnyField = prepareUpdateSetOnly(fieldValues, fieldCns, builder);
            }

            DSApi.context().closeStatement(pst);
            pst = null;
            //log.info("setOnly statement = " + builder.toString());
            ps = DSApi.context().prepareStatement(builder.toString());
            // ustalamy parametry zapytania
            int count = 0;
            for (String fieldCn : fieldCns) {
                Field field = getFieldByCn(fieldCn);
                if (field != null  && !Field.DOCLIST.equals(field.getType())) {
	                if (field.isMultiple()) { //patrz opis w set
	                    if (!(field instanceof CostCenterField)){
	                        saveMultipleData(documentId, field, (List<Object>) field.coerce(fieldValues.get(field.getCn())), getMultipleTableName());
                        }
	                } else if(field.isPersistedInColumn()) {
	                    count++;
	                    field.set(ps, count, fieldValues.get(field.getCn()));
                        //log.info("set " + count + " -> " + fieldValues.get(field.getCn()));
	                }
                }
            }
            ps.setLong(count+1, documentId);
            //log.info("set " + (count+1) + " -> " + documentId);

            if(setAnyField){
            	int num = ps.executeUpdate();
                //log.info(num + " rows updated");
            }

            final List<CustomPersistence> customPersistedFields = getDockindInfo().getCustomPersistedFields();
            if(!customPersistedFields.isEmpty()){
                log.info("documentId id = {}", documentId);
                Document doc = Document.find(documentId, checkPermission);
                for(CustomPersistence cp : customPersistedFields){
                    String fieldCn = ((Field)cp).getCn();
                    if(fieldValues.containsKey(fieldCn)){   
                        cp.persist(doc, fieldValues.get(fieldCn));
                    }
                }
            }
            
            DSApi.context().closeStatement(ps);

            // dodatkowe czynno�ci zale�ne od rodzaju dokumentu
            onModifyDocument(documentId, fieldValues, checkPermission);
        }
        catch (SQLException e)
        {
			log.error("{}\nfieldValues: {}\ndocId: {}\nusername:{}",
					e.getClass() + "." + "setOnly",
					fieldValues,
					documentId,
					DSApi.context().getPrincipalName());
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	DSApi.context().closeStatement(pst);
        }
    }

    private Boolean prepareUpdateSetOnly(Map<String, ?> fieldValues, Set<String> fieldCns, StringBuilder builder) throws EdmException, SQLException {
        Boolean setAnyField = false;
        boolean first = true;
        builder.append("update ");
        builder.append(getTablename());
        builder.append(DSApi.rowlockString());
        builder.append(" set ");

            for (String fieldCn : fieldCns)
        {
            Field field = getFieldByCn(fieldCn);
            // wielowartosciowe trzymane w innej tabeli
            if(field != null && Field.DOCLIST.equals(field.getType())){
                updateDocList(fieldValues,(DocListField) field,true);
            } else if (field != null && !field.isMultiple() && field.isPersistedInColumn()) {
                setAnyField = true;
                if (!first)
                    builder.append(", ");
                else
                    first = false;
                builder.append(field.getColumn());
                builder.append(" = ?");
             
            }
        }
              builder.append(" where document_id = ?");
        return setAnyField;
    }

    private Boolean prepareSetOnlyCreate(Map<String, ?> fieldValues, Set<String> fieldCns, StringBuilder builder) throws EdmException, SQLException {
        Boolean setAnyField;
        setAnyField = true;
        // wstawiamy nowy wiersz
        builder.append("insert into ");
        builder.append(getTablename());
        builder.append(" (");

        prepareColumnNames(fieldValues, fieldCns, builder);
        prepareQuestionMarksWTF(fieldValues, fieldCns, builder);

        return setAnyField;
    }

    private void prepareQuestionMarksWTF(Map<String, ?> fieldValues, Set<String> fieldCns, StringBuilder builder) throws EdmException, SQLException {
        boolean first = true;
        for (String fieldCn : fieldCns){
            Field field = getFieldByCn(fieldCn);
            // wielowartosciowe trzymane w innej tabeli
            if(field != null && Field.DOCLIST.equals(field.getType())){
                updateDocList(fieldValues,(DocListField) field,true);
            } else if (field != null && !field.isMultiple() && field.isPersistedInColumn()){
                if (!first)
                    builder.append(", ");
                else
                    first = false;
                builder.append("?");
            }
        }
        if (!first)
            builder.append(", ");
        builder.append("?)");
    }

    private void prepareColumnNames(Map<String, ?> fieldValues, Set<String> fieldCns, StringBuilder builder) throws EdmException, SQLException {
        boolean first = true;
        for (String fieldCn : fieldCns) {
            Field field = getFieldByCn(fieldCn);
            if(field == null) {
                continue;
            }

            // wielowartosciowe trzymane w innej tabeli
            if(Field.DOCLIST.equals(field.getType())) {
                updateDocList(fieldValues,(DocListField) field,true);
            } else if (!field.isMultiple() && field.isPersistedInColumn()){
                if (!first)
                    builder.append(", ");
                else
                    first = false;
                builder.append(field.getColumn());
            }
        }
        if (!first) {
            builder.append(", ");
        }
        builder.append("document_id) values (");
    }

    /**
     * Zapisuje do tabeli dotyczacej danej typu dokumentu wartosci wpisu o id 'documentId', dodatkowo
     * sprawdzaj�c czy dane nie uleg�y zmianie od ostaniego zapisu - je�li uleg�y to dodawany jest
     * odpowiedni wpis do historii danego obiektu Document.
     *
     * @param documentId  id dokumentu, kt�remu nale�y zmieni� warto�ci p�l
     * @param fieldValues  mapa warto�ci p�l
     * @param changeAll  true => warto�ci wszystkich p�l zostan� zmienione (je�li nie podano warto�ci
     *        danego pola, w to miejse wpisany zostanie null);
     *        false => zmienione zostan� tylko te pola, dla kt�rych wskazano warto�� w 'fieldValues'
     */
    public void setWithHistory(Long documentId, Map<String,Object> fieldValues, boolean changeAll) throws EdmException
    {
        checkNotNull(fieldValues, "fieldValues cannot be null");
    	setWithHistory(documentId, fieldValues, changeAll, null, DSApi.context().getPrincipalName());
    }
    
    /**
     * Zapisuje do tabeli dotyczacej danej typu dokumentu wartosci wpisu o id 'documentId', dodatkowo
     * sprawdzaj�c czy dane nie uleg�y zmianie od ostaniego zapisu - je�li uleg�y to dodawany jest
     * odpowiedni wpis do historii danego obiektu Document.
     *
     * @param documentId  id dokumentu, kt�remu nale�y zmieni� warto�ci p�l
     * @param fieldValues  mapa warto�ci p�l
     * @param changeAll  true => warto�ci wszystkich p�l zostan� zmienione (je�li nie podano warto�ci
     *        danego pola, w to miejse wpisany zostanie null);
     *        false => zmienione zostan� tylko te pola, dla kt�rych wskazano warto�� w 'fieldValues'
     */
    public void setWithHistory(Long documentId, Map<String,Object> fieldValues, boolean changeAll,String username) throws EdmException
    {
        checkNotNull(fieldValues, "fieldValues cannot be null");
    	setWithHistory(documentId, fieldValues, changeAll, null,username);
    }

    public void setWithHistory(Long documentId, Map<String,Object> fieldValues, boolean changeAll, String sessionIdForDatamart,String username) throws EdmException
    {
        // pobieramy stare warto�ci
        FieldsManager oldFm = Documents.document(documentId).getFieldsManager();
        oldFm.initialize();
        
        Document doc = Document.find(documentId);
   		saveMultiDictionaryValues(doc, fieldValues);
   		saveDictionaryValues(doc, fieldValues);
        /**Pole odpowiedzialen za uruchamianie zdarzen nie potrzebne przy zapisie*/
        fieldValues.remove("dockindEvent");

        // zapisujemy zmiany
        if (changeAll)
            set(documentId, fieldValues);
        else
            setOnly(documentId, fieldValues);

        // pobieramy nowe warto�ci
        //FieldsManager newFm = this.getFieldsManager(documentId);
        //newFm.initialize();
        FieldsManager newFm = Documents.document(documentId).getFieldsManager();

        // por�wnujemy stare warto�ci z nowymi przechodz�c kolejno po wszystkich polach
        StringBuilder historyBuilder = null;
        for (Field field : getFields()) {
        	// jesli wartosc pola nie jest zapisywana w kolumnie tabeli dockinda badz pole nie posiada wlasnej logiki
        	if(!(field instanceof NonColumnField) || field.getLogic() != null){
	            boolean hasChanged = false;
	            if (field.isMultiple())
	            {
	                // por�wnujemy listy (ale troche "niedok�adnie", w zwi�zku z tym zgadzamy si� z tym,
	                // �e czasem b�dzie informacja o zmianie mimo, �e takiej zmiany nie by�o)
	                 List<Object> oldValue = (List<Object>) oldFm.getKey(field.getCn());
	                 List<Object> newValue = (List<Object>) newFm.getKey(field.getCn());

                if ((oldValue == null && newValue != null) || (oldValue != null && newValue == null))
                    hasChanged = true;
                else if (oldValue != null && newValue != null)
                {
              	  	if (field instanceof DictionaryField)
              	  	{
              	  		List<Long> oldValuea = (List<Long>) oldFm.getKey(field.getCn());
              	  		List<Long> newValuea = (List<Long>) newFm.getKey(field.getCn());
              	  		Collections.sort(oldValuea, new Comparator<Long>() {
              	  			public int compare(Long o1, Long o2) {
              	  				return o2.compareTo(o1);
              	  			}
              	  		});
              	  		Collections.sort(newValuea, new Comparator<Long>() {
              	  			public int compare(Long o1, Long o2) {
              	  				return o2.compareTo(o1);
              	  			}
              	  		});
              	  		
              	  		Collections.sort(oldValue, new LongColumnField.LongComparator());
              	  		Collections.sort(newValue, new LongColumnField.LongComparator());
              	  	}
                    if (oldValue.size() != newValue.size())
                        hasChanged = true;
                    else
                    {
                        boolean listsEqual = true;
                        for (int i=0; i < oldValue.size(); i++)
                        {
                            if (notEqual(oldValue.get(i), newValue.get(i)))
                                listsEqual = false;
                        }
                        hasChanged = !listsEqual;
                    }
                }
                
                if (!hasChanged && AvailabilityManager.isAvailable("dwr") && field instanceof DictionaryField && (oldValue != null && newValue != null))
                {
               	 	oldValue = (List<Object>) oldFm.getValue(field.getCn());
                     newValue = (List<Object>) newFm.getValue(field.getCn());
                     
                     Collections.sort(oldValue, new MapIdComparator());
                     Collections.sort(newValue, new MapIdComparator());
                 
	               	 Map<String,Object> dicOldValue, dicNewValue;
	               	 
					 for (int i = 0; i < oldValue.size(); i++)
					 {
						 dicOldValue = (Map<String, Object>)oldValue.get(i);
						 dicNewValue = (Map<String, Object>)newValue.get(i);
						 StringBuilder dicString = checkDictionaryField(dicOldValue, dicNewValue, field);
						 if (historyBuilder == null && dicString != null)
				                    historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(dicString);
						  else if (historyBuilder != null && dicString != null)
				                    historyBuilder.append(", ").append(dicString);
					 }
                }
            }
            else if(Field.DOCLIST.equals(field.getType()))
            {
            	hasChanged = false;
            }
            else if (AvailabilityManager.isAvailable("dwr") && field instanceof DictionaryField)
            {
	          	  Map<String,Object> oldValue = (Map<String,Object>) oldFm.getValue(field.getCn());
	          	  Map<String,Object> newValue = (Map<String,Object>) newFm.getValue(field.getCn());
	          	  StringBuilder dicString = checkDictionaryField(oldValue, newValue, field);
	          	  if (historyBuilder == null && dicString != null)
		                    historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(dicString);
				  else if (historyBuilder != null && dicString != null)
		                    historyBuilder.append(", ").append(dicString);
            }
            else
            {
                hasChanged = notEqual(oldFm.getKey(field.getCn()), newFm.getKey(field.getCn()));
            }

            if (hasChanged)
            {
          	  if (field instanceof DictionaryField && (oldFm.getValue(field.getCn()) != null && newFm.getValue(field.getCn()) != null))
          	  {
          		  Map<String,Object> dicOldValue, dicNewValue;
          		  List<Object> oldValue = (List<Object>) oldFm.getValue(field.getCn());
     			  List<Object> newValue = (List<Object>) newFm.getValue(field.getCn());
     			  StringBuilder dicString = null;
     			  
     			  Collections.sort(oldValue, new MapIdComparator());
     			  Collections.sort(newValue, new MapIdComparator());
     			  
          		  if (field.isMultiple())
          		  {
          			  int size = oldValue.size() >= newValue.size() ? newValue.size() : oldValue.size();
          			  for (int i = 0; i < size; i++)
          			  {
          				  dicOldValue = (Map<String, Object>)oldValue.get(i);
          				  dicNewValue = (Map<String, Object>)newValue.get(i);
          				  dicString = checkDictionaryField(dicOldValue, dicNewValue, field);
          				  if (historyBuilder == null && dicString != null)
       		                    historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(dicString);
          				  else if (historyBuilder != null && dicString != null)
       		                    historyBuilder.append(", ").append(dicString);
      				  }
          		  }
          		  else
          		  {
          			  dicOldValue= (Map<String,Object>) oldFm.getValue(field.getCn());
          			  dicNewValue = (Map<String,Object>) newFm.getValue(field.getCn());
          			  dicString = checkDictionaryField(dicOldValue, dicNewValue, field);
     				  if (historyBuilder == null && dicString != null)
     		                    historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(dicString);
     				  else if (historyBuilder != null && dicString != null)
     		                    historyBuilder.append(", ").append(dicString);
     		          	  
          		  }
          	  }
          	  else {
                // warto�� danego pola uleg�a zmianie
                Object oldDesc = oldFm.getDescription(field.getCn());
                Object oldKey = oldFm.getKey(field.getCn());
                Object newDesc = newFm.getDescription(field.getCn());
                Object newKey = newFm.getKey(field.getCn());
                if(AvailabilityManager.isAvailable(newFm.getDocumentKind().getCn()+".datamart.document.field.change"))
                {
                	new DataMartEvent(true, documentId,null,DataMartDefs.DOCUMENT_FIELD_CHANGE,field.getCn(),oldKey!=null?oldKey.toString():"", newKey!=null?newKey.toString():"", sessionIdForDatamart);
                }
	                
                FieldLogic fieldLogic = null;
                String logic = field.getLogic();
                String change = null;
                if(logic != null)
                	fieldLogic = field.prepareFieldLogic();
                //	jesli pole posiada indywidualna logike
            	if(fieldLogic != null){
            		change = Strings.nullToEmpty(fieldLogic.prepareCustomFieldHistoryEntry(oldDesc, oldKey, newDesc, newKey));
            		if(historyBuilder == null)
            			historyBuilder = new StringBuilder(change);
            		else
            			historyBuilder.append(", ").append(change);
            	}else{
	                if (oldDesc == null || "".equals(oldDesc))
	                    change = field.getName() + " " + sm.getString("ZwartosciPustejNa",newDesc);
	                else if (newDesc == null || "".equals(newDesc))
	                    change = field.getName() + " " + sm.getString("ZNaWartoscPusta",oldDesc);
	                else
	                    change = field.getName() + " " + sm.getString("Zna",oldDesc,newDesc);
	                if (historyBuilder == null)
	                    historyBuilder = new StringBuilder(sm.getString("ZmienionoWartosciPol")+ " ").append(change);
	                else
	                    historyBuilder.append(", ").append(change);
            	}
	          }
          	  }
            }
        }
        if(doc.getDocumentKind().logic() != null)
        	doc.getDocumentKind().logic().specialCompareData(newFm, oldFm);
        
        if (historyBuilder != null)
        {
            try
			{
				DataMartManager.addHistoryEntry(documentId,
				Audit.createWithPriority("changeFields", username,
				       historyBuilder.toString(), null, Audit.LOWEST_PRIORITY));
				Document.find(documentId).fireDocumentModified();
			}
			catch (Exception e)
			{
				throw new  EdmException(e);
			}
        }
    }

    private StringBuilder checkDictionaryField(Map<String, Object> dicOldValue, Map<String, Object>  dicNewValue, Field dicField)
    {
		StringBuilder historyBuilder = null;
		if (dicOldValue != null)
		{
			String dictionaryCn = dicField.getCn();
			for (Field tmpField : dicField.getFields())
			{
				if(!(tmpField instanceof NonColumnField)){
					String tmpFieldCn = tmpField.getCn().replace("_1", "");
					Object oldDicFieldValue = dicOldValue.get(dictionaryCn + "_" + tmpFieldCn);
					Object newDicFieldValue = dicNewValue.get(dictionaryCn + "_" + tmpFieldCn);
					String change = null;
	
					if (oldDicFieldValue != null && oldDicFieldValue instanceof EnumValues)
					{
						oldDicFieldValue = ((EnumValues) oldDicFieldValue).getSelectedValue();
						if (oldDicFieldValue.equals("")) oldDicFieldValue = null;
					}
					if (newDicFieldValue instanceof EnumValues)
					{
						newDicFieldValue = ((EnumValues) newDicFieldValue).getSelectedValue();
						if (newDicFieldValue.equals("")) newDicFieldValue = null;
					}
					
					if (newDicFieldValue != null && (oldDicFieldValue == null || oldDicFieldValue.equals("")))
						change = "Pole " + tmpField.getName() + " " + sm.getString("ZwartosciPustejNa", newDicFieldValue);
					else if (oldDicFieldValue != null && (newDicFieldValue == null || newDicFieldValue.equals("")))
						change = "Pole " + tmpField.getName() + " " + sm.getString("ZNaWartoscPusta", oldDicFieldValue);
					else if (newDicFieldValue != null && oldDicFieldValue != null && !newDicFieldValue.equals(oldDicFieldValue))
						change = "Pole " + tmpField.getName() + " " + sm.getString("Zna", oldDicFieldValue, newDicFieldValue);
					
					if (historyBuilder == null && change != null)
						historyBuilder = new StringBuilder("S�ownik ").append(dicField.getName()).append(": ").append(change);
					else if (change != null)
						historyBuilder.append(", ").append(change);
				}
			}
		}
		return historyBuilder;
    }
    
    /**
     * @TODO - nie moze byc tak, ze wszystko kasujemy i zapisujemy ponownie
     * Trzeba weryfikowac i nanosic tylko roznice
     * Zapisuje do bazy warto�ci atrybutu wielokrotnego.
     *
     * values - lista obiekt�w b�d�cych kolejnymi wpisami w s�owniku
     * field_cn - cn pola typu multiple (na jego podstawie mozna uzyskac informacje o tabeli slownika przechowujacego dane powiazany z tym obiektem)
     * field_val - id wpisu w tabeli przechowujacej informacje o danym obiekcie umieszczonym w slowniku
     */
    private static void saveMultipleData(Long documentId, Field field, List<Object> values, String multipleTableName) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {        

            String del = "delete from "+multipleTableName+" where document_id=? and field_cn=?";
            ps = DSApi.context().prepareStatement(del);
            ps.setLong(1, documentId);
            ps.setString(2, field.getCn());
            ps.execute();
            DSApi.context().closeStatement(ps);

            if (values != null)
            {
                String ins = "insert into "+multipleTableName+" (document_id,field_cn,field_val) values (?,?,?)";
                for (Object object : values)
                {
                    if (object != null)
                    {
                        ps = DSApi.context().prepareStatement(ins);
                        ps.setLong(1, documentId);
                        ps.setString(2, field.getCn());
                        ps.setString(3, object.toString());
                        ps.execute();
                        DSApi.context().closeStatement(ps);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        }
    }

    /**
     * usuwa z tabeli dotyczacej tego dockind-a wiersz z wartosciami dla dokumentu o id 'documentId'
     */
    public void remove(Long documentId) throws EdmException
    {
        if (documentId == null)
            throw new NullPointerException("documentId");

        try
        {
        	Document doc = Document.find(documentId);
        	FieldsManager fm = doc.getDocumentKind().getFieldsManager(documentId);
        	fm.initialize();
        	for(Field f:fm.getFields())
        	{
        		DataMartEvent dme = new DataMartEvent(true, documentId, null, DataMartDefs.DOCUMENT_DOCKIND_REMOVE, f.getCn(), fm.getDescription(f.getCn()).toString(), null, null);
     	        //DataMartManager.storeEvent(dme);
        	}
        }
        catch(Exception e)
        {

        }
        PreparedStatement pst = null;
        try
        {
            pst = DSApi.context().prepareStatement("delete from "+getTablename()+" where document_id=?");
            pst.setLong(1, documentId);
            pst.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
        finally
        {
            DSApi.context().closeStatement(pst);
        }
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DocumentKind)) return false;

        final DocumentKind documentKind = (DocumentKind) o;

        if (id != null ? !id.equals(documentKind.getId()) : documentKind.getId() != null) return false;

        return true;
    }

    public SearchResults<Document> checkDuplicate(Map<String, Object> values) throws EdmException
    {
    	
    	DockindQuery dockindQuery;
        dockindQuery = new DockindQuery(0, 0);
        SearchResults<Document> searchResults = null;
        dockindQuery.setDocumentKind(this);
        dockindQuery.setCheckPermissions(false);
        dockindQuery.orderDesc("id");
        

        List<Field> dFields = getFieldsToCheck();
        if(dFields == null || dFields.size()== 0)
        {
            return null;
        }
        for(Field field : dFields)
        {
            dockindQuery.addForceSearchEq(field.getCn());
        }

        try
        {
            for(Field field : dFields)
            {
                if(values.get(field.getCn()) != null)
                {
                    if (Field.ENUM.equals(field.getType()) ||  Field.ENUM_REF.equals(field.getType()))
                    {
                        if (values.get(field.getCn()) == null)
                            continue;
                        Integer[] enumIds = TextUtils.parseIntoIntegerArray(values.get(field.getCn()));
                        dockindQuery.enumField(field, enumIds);
                    }
                    else if (field instanceof DateField)// || field.isSearchByRange())
                    {
                        String from = (String) values.get(field.getCn());//+"_from");
                       // String to = (String) values.get(field.getCn()+"_to");
                       // Date dateFrom = (Date) field.simpleCoerce(from);
                       // Date dateTo = (Date) field.simpleCoerce(to);
                      //  dateTo.setHours(24);
                        dockindQuery.field(field, from);
                    }
                    else if (Field.STRING.equals(field.getType()))
                    {
                        String value = (String) field.simpleCoerce(values.get(field.getCn()));
                        dockindQuery.stringField(field, value);
                    }
                    else if (Field.BOOL.equals(field.getType()))
                    {
                        String value = (String) values.get(field.getCn());
                        Boolean bool = (Boolean) field.simpleCoerce(value);
                        dockindQuery.boolField(field, bool);
                    }
                    else if (field instanceof FloatField)
                    {
                        String value = (String) values.get(field.getCn());
                        Float fl = (Float) field.simpleCoerce(value);
                        dockindQuery.field(field, fl);
                    }
                    else if (Field.CLASS.equals(field.getType()))
                    {
                        String value = (String) values.get(field.getCn());
                        dockindQuery.field(field, value);
                        for (String property : field.getDictionaryAttributes().keySet())
                        {
                            value = (String) values.get(field.getCn()+"_"+property);
                            dockindQuery.otherClassField(field, property, value);
                        }
                    }
                    else
                    {
                        Object value = field.simpleCoerce(values.get(field.getCn()));
                        if (value instanceof Object[])
                        {
                            Object[] array = (Object[]) value;
                            if (array.length == 0)
                                value = null;
                            else
                                value = array[0];
                        }
                        if (value != null)
                        {
                            dockindQuery.field(field, values.get(field.getCn()));
                        }
                    }
                }
                else
                {
                }
            }
            searchResults = DocumentKindsManager.search(dockindQuery);
            return searchResults;
        }
        catch (EdmException e)
        {
        	log.debug("", e);
        }

        return searchResults;
    }

    public List<Field> getFieldsToCheck()
    {
        List<Field> returnFiled = new ArrayList<Field>();
        try
        {
            for (Field field : this.getFields())
            {
                   if(field.isKeyMember())
                   {
                       returnFiled.add(field);
                    }
            }
        }
        catch (EdmException e)
        {
        	log.debug("", e);
        }
        return returnFiled;

    }

    public static void resaveDocument(Document doc, int type) throws EdmException
    {
    	doc.getDocumentKind().logic().archiveActions(doc, type);
    	doc.getDocumentKind().logic().documentPermissions(doc);
    }

	/**
	 * Uaktualnia etykiety dla danego dokumentu wg xml'a. Je�li jakis ciag warunkow zostanie spelniony usuwa wszystkie etykiety zdefiniowane w xmlu i dodaje t� etykiet�.
	 * @param doc
	 * @throws pl.compan.docusafe.core.EdmException
	 */
    public void handleLabels(Document doc, FieldsManager fm) throws EdmException
    {

    	List<LabelDefinition> labs = this.getLabelsDefs();
    	//FieldsManager fm = this.getFieldsManager(doc.getId());
    	List<Condition> conds;
    	String condMix;

		labelDefinitionsForEach:
    	for(LabelDefinition ld:labs)
    	{
    		conds = ld.getConditions();
    		condMix = ld.getConditionMixing();
    		boolean equals = false;
    		boolean strong = ld.getStrong();
    		boolean conditionMixing = false;
    		if(condMix.equals("or"))  conditionMixing = false;
    		else if(condMix.equals("and")) conditionMixing = true;
			for(Condition c:conds)
			{
				
				if(fm.getKey(c.getField()) == null)
    				break;
    			String value = fm.getKey(c.getField()).toString();
    			Integer result = value.compareTo(c.getValue());
    			if(c.getOperatorAsInteger()== 0 && result == 0) {
    				equals = true;
	    			if(conditionMixing) continue;
	    			else break;
				} else if(c.getOperatorAsInteger()>0 && result >0) {
    				equals = true;
	    			if(conditionMixing) continue;
	    			else break;
				} else if(c.getOperatorAsInteger()<0 && result <0) {
    				equals = true;
	    			if(conditionMixing) continue;
	    			else break;
				} else if (conditionMixing) {
					equals = false;
					break;
				}
			} //foreach conds

			
			if(equals)
			{
				Label label;
				label = LabelsManager.findSystemLabelByName(ld.getName(), Label.LABEL_TYPE);
				/*for(LabelDefinition temp:labs)
				{
					label = LabelsManager.findSystemLabelByName(temp.getName(), Label.LABEL_TYPE);
					if(label == null)
					{
						log.error("Brak etykiety: \"{}\"", temp.getName());
					}
					else
					{
						LabelsManager.removeLabel(doc.getId(), label.getId());
					}
				}*/
				
				if (label == null)
				{
					log.error("Brak etykiety: \"{}\"", ld.getName());
				}
				else
				{
					LabelsManager.addLabel(doc.getId(), label.getId(), Label.SYSTEM_LABEL_OWNER);
					break labelDefinitionsForEach; //zako�cz dzialanie je�li znalaz�e� ju� jedn� z etykiet
				}
			}
			if(strong && !equals)
			{
				Label label;
				label = LabelsManager.findSystemLabelByName(ld.getName(), Label.LABEL_TYPE);
				if(label == null)
				{
					log.error("Brak etykiety: \"{}\"",ld.getName());
				}
				else
				{
					LabelsManager.removeLabel(doc.getId(), label.getId());
				}
			}
    	}
    }

    /*
	 * author Kamil Skrzypek
	 */
	public void saveMultiDictionaryValues(Document doc, Map<String, Object> dockindKeys) throws EdmException
	{
        checkNotNull(dockindKeys, "dockindKeys cannot be null");
		if (dockindKeys.containsKey(MULTIPLE_DICTIONARY_VALUES))
		{
			// dziala dla jednego slownika wielowartosciowego w dokumencie
			Map<String, Map<String, FieldData>> dValues = (Map<String, Map<String, FieldData>>) dockindKeys.get(MULTIPLE_DICTIONARY_VALUES);
			try
			{
				String cutCn = null;
				Set<String> cnSet = dValues.keySet();
				String[] cnArray = cnSet.toArray(new String[cnSet.size()]);
				// normalnie zapisane mamy np. wartosc DS_AR_INVENTORY_ITEM_10,
				// kiedy CN slownika wynosi DS_AR_INVENTORY_ITEM, czyli bez ostatnich znakow od ostatniej kreski
				String dictionaryCn= cnArray[0];
				dictionaryCn= dictionaryCn.substring(0, dictionaryCn.lastIndexOf('_'));
				// ponizsza flaga oznacza, ze chcemy zapisac wszystkie rekordy slownika
				// w takiej kolejnosci, w jakiej wprowadzone sa na formatce
				// bez tej flagi kolejnosc staje sie losowa (poniewaz Set nie jest posortowany w zaden sposob)
				boolean saveInTheOrder= doc.getDocumentKind().getFieldByCn(dictionaryCn).isSaveInTheOrder();
				if(saveInTheOrder){
					Arrays.sort(cnArray,new Comparator<String>() {
						@Override
						public int compare(String s1, String s2)
						{
							if( s1.length() < s2.length())
								return -1;
							if( s1.length() > s2.length())
								return 1;
							return s1.compareTo(s2);
						}});
				}
				for (String cn : cnArray)
				{
					ArrayList<Long> ids = new ArrayList<Long>();
					cutCn = cn.substring(0, cn.lastIndexOf("_"));
					
					if (dValues.size() > 0 && (!dValues.containsKey("ID") && (dValues.get(cn).get("ID").getData() == null || dValues.get(cn).get("ID").getData().toString().equals(""))))
					{
						log.debug("dodajemy nowy wpis slownikowi");
						for(String fName : dValues.get(cn).keySet()){
							FieldData fd = dValues.get(cn).get(fName);
							//	w przypadku zalacznikow musimy uprzednio utworzyc ich obiekty w bazie 
							//	a nastepnie przekazujemy id utworzonego zalacznika do tabeli slownika wielowartosciowego
							if(fd.getType() == pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT){
								AttachmentValue attVal;
								if(fd.getData() instanceof AttachmentValue && (attVal = (AttachmentValue) fd.getData()).getAttachmentId() == null){
									FormFile formFile;
									if((formFile = attVal.getFormFile()) != null){
										Attachment att = new Attachment(formFile.getNameWithoutExtension());
										doc.createAttachment(att);
										AttachmentRevision rev = att.createRevision(formFile.getFile());
										rev.setOriginalFilename(formFile.getName());	
										attVal.setAttachmentId(att.getId());
									}
								}
							}
						}
						long idDict = DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), cutCn).add(dValues.get(cn));

						if(idDict != -1L)
							ids.add(idDict);
					}
					else if (dValues.get(cn) == null)
					{

					}
					else
					{
						log.info("Wybrany s�ownik juz istnieje -> update istniejacego");
						//	aktualizujemy wartosci wszystkich napotkanych zalacznikow
						for(String fName : dValues.get(cn).keySet()){
							FieldData fd = dValues.get(cn).get(fName);
							if(fd.getType() == pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT){
								AttachmentValue attVal;
								if(fd.getData() instanceof AttachmentValue && (attVal = (AttachmentValue) fd.getData()).getAttachmentId() != null){
									FormFile formFile;
									if((formFile = attVal.getFormFile()) != null){
										doc.getAttachment(attVal.getAttachmentId()).createRevision(attVal.getFormFile().getFile());
									}
								}
							}
						}
						Map<String, FieldData> properDictionaryValues = (Map<String, FieldData>) dValues.get(cn);
						FieldData o = dValues.get(cn).get("ID");
						if (o != null)
						{
							Object a = o.getData();
							if (a != null)
								log.debug("a: {}", a.getClass());
						}
						// Aktualizacja wartosci slownika
                        DwrDictionaryBase dwrDictionary = DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), cutCn);
                        dwrDictionary.filterBeforeUpdateValues(properDictionaryValues);
                        dwrDictionary.update(properDictionaryValues);

						/*
						 * ID jest pobierany z pola dockindu umieszczonego
						 * w xml'u (slownik wielowartosciowy) Kazdy
						 * slownik posiada pole ID <field id="300"
						 * cn="ID_1" column="ID_1"> <type name="integer"/>
						 * </field>
						 */

						FieldData fd = (FieldData) properDictionaryValues.get("ID");
						if (fd != null && fd.getData().toString().length() > 0)
						{
							String idString = fd.getData().toString();
							Long id = new Long(idString);
							
							if(id != -1L)
								ids.add(id);
						}
						else
						{
							log.error("Slownik dictionary multiple=true nie posiada zdefiniowanego pola  <field id='' cn='ID_1' column='ID_1'>, badz id jest pustym stringiem");
						}
					}
					if (dockindKeys.containsKey(cutCn))
					{
						if(ids != null && !ids.isEmpty() && ids.get(0) != null)
						{
							ArrayList<Long> lista = (ArrayList<Long>) dockindKeys.get(cutCn);
							lista.add(ids.get(0));
							dockindKeys.put(cutCn, lista);
						}
					}
					else if (ids.size() > 0)
					{
						dockindKeys.put(cutCn, ids);
					}
					else if(ids.size() == 0)
					{
						dockindKeys.put(cutCn, new ArrayList<Long>());
					}
				}
			}
			catch (EdmException ed)
			{
				log.error(ed + ed.getMessage());
				throw ed;
			}
		}
	}

	public void saveDictionaryValues(Document doc, Map<String, Object> dockindKeys) throws EdmException
	{
		if(doc.getDocumentKind().logic() != null) {
			doc.getDocumentKind().logic().specialSetDictionaryValues(dockindKeys);
		}

		Set<String> ke = new HashSet<String>(dockindKeys.keySet());
		for (String dKey : ke)
		{
			log.debug("dictionaryKEY VALUE: " + dKey);
			//log.debug("dictionary val: {}", dockindKeys.get(dKey));
			if (dKey.contains("_DICTIONARY") && 
					( (!dockindKeys.containsKey(dKey.replace("_DICTIONARY", ""))) || (dockindKeys.get(dKey.replace("_DICTIONARY", "")) == null)))
			{
				log.debug("DockindCN: " + dKey);
				// jesli slownik multiwartosciowy
				if (dockindKeys.get(dKey) instanceof ArrayList)
				{
					continue;
				}
				Map<String, FieldData> m = (Map<String, FieldData>) dockindKeys.get(dKey);
				log.info("rozmiar parametrow slownika: " + m.size());
				boolean onlyPopupCreate = false;
				try
				{
					dKey = dKey.replace("_DICTIONARY", "");
					log.debug("DictionaryCN: " + dKey);
					if (DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey) != null && DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey).isOnlyPopUpCreate()) {
						onlyPopupCreate = true;
						throw new EdmException(sm.getString("OnlyPopupCreate"));
					}
					long idDict = DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey).add(m);
					doc.getDocumentKind().setOnly(doc.getId(), Collections.singletonMap(dKey, idDict));
					dockindKeys.put(dKey, idDict);
				}
				catch (EdmException ed)
				{
					log.error(ed + ed.getMessage());
					if (onlyPopupCreate) {
						throw ed;
					}
				}
			}
            // daje to na adds bo nie wie, ze skoro tyle czasu tutaj nie bylo mozliwosci aktualizacji wpisu w slowniku jednowartosciwoy, to dodanie tego fragmetnu czegos nie zwali
            else  if (AvailabilityManager.isAvailable("updateDictionaryInSetOnly") && dKey.contains("_DICTIONARY") &&
                    ( (dockindKeys.containsKey(dKey.replace("_DICTIONARY", ""))) && (dockindKeys.get(dKey.replace("_DICTIONARY", "")) != null)) && !dKey.contains("KLIENT"))
            {
                if (dockindKeys.get(dKey) instanceof ArrayList)
                {
                    continue;
                }

                Map<String, FieldData> m = (Map<String, FieldData>) dockindKeys.get(dKey);
                log.info("rozmiar parametrow slownika: " + m.size());
                boolean onlyPopupCreate = false;
                try
                {
                    dKey = dKey.replace("_DICTIONARY", "");
                    log.debug("DictionaryCN: " + dKey);
                    if (DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey) != null && DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey).isOnlyPopUpCreate()) {
                        onlyPopupCreate = true;
                        throw new EdmException(sm.getString("OnlyPopupCreate"));
                    }
                    DwrDictionaryFacade.getDwrDictionary(doc.getDocumentKind().getCn(), dKey).update(m);
                }
                catch (EdmException ed)
                {
                    log.error(ed + ed.getMessage());
                    if (onlyPopupCreate) {
                        throw ed;
                    }
                }

            }
		}
	}
	
	public void createOrUpdateTable() throws EdmException
	{
		try
		{
			List<org.dom4j.Document> docs = new ArrayList<org.dom4j.Document>();
			DockindToHibernateMappingConverter conv = new DockindToHibernateMappingConverter();
			org.dom4j.Document doc = conv.convertToHBMThrowsException(this.getXML(), this.getTablename());
			docs.add(doc);
			
    		String xPathDictionaryType = "/*[name()='doctype']/*[name()='fields']/*[name()='field'][@column!='']/*[name()='type'][@name='dictionary']";
			
    		int i = 0;
			for(Object o : this.getXML().selectNodes(xPathDictionaryType))
    		{
    			org.dom4j.Element type = (org.dom4j.Element) o;
    			conv = new DockindToHibernateMappingConverter();
    			conv.convertToHBMThrowsException(this.getXML(), type.attribute("tableName").getText(), ((Node) type).getPath()+"/*[name()='field']", i);
    			org.dom4j.Document d = conv.getHbmDocument();
    			docs.add(d);
    			i++;
    		}
			
			//DockindDBUtils.runUpdate(doc);
			DockindDBUtils.runUpdates(docs);
		}
		catch(Exception e) { throw new EdmException(e.getMessage(),e); }
	}

    /**
     * Zwraca true <=> obiekt o1 jest r�ny od obiektu o2.
     */
    private static boolean notEqual(Object o1, Object o2)
    {
        return ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null));
    }

    /**
     * Zwraca true <=> obiekt o1 jest r�ny od obiektu o2.
     */
    private static boolean notEqualToString(Object o1, Object o2)
    {
    	if(o1 != null)
    		o1 = o1.toString();
    	if(o2 != null)
    		o2 = o2.toString();
        return ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null));
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    /** Zwraca nazw� dokumnetu dla u�ytkownika*/
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /** Zwraca nazw� kodow� dokumentu*/
    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public String getTablename()
    {
        return tablename;
    }

    public void setTablename(String tablename)
    {
        this.tablename = tablename;
    }

    public String getMultipleTableName()
    {
        return getDockindInfo().getMultipleTableName();
    }

    public void setMultipleTableName(String multipleTableName)
    {
        getDockindInfo().setMultipleTableName(multipleTableName);
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public Integer getHversion()
    {
        return hversion;
    }

    public void setHversion(Integer hversion)
    {
        this.hversion = hversion;
    }

    public String getMainFieldCn()
    {
        return getDockindInfo().getMainFieldCn();
    }

    public void setMainFieldCn(String mainFieldCn)
    {
        getDockindInfo().setMainFieldCn(mainFieldCn);
    }

    public boolean isAttachmentRequired()
    {
        return getDockindInfo().isAttachmentRequired();
    }

    public void setAttachmentRequired(boolean attachmentRequired)
    {
        getDockindInfo().setAttachmentRequired(attachmentRequired);
    }

    public List<ValidationRule> getValidationRules()
    {
        return getDockindInfo().getValidationRules();
    }

    public AcceptancesDefinition getAcceptancesDefinition()
    {
        return getDockindInfo().getAcceptancesDefinition();
    }

    public Map<String, String> getProperties()
    {
    	Map<String,String> properties = getDockindInfo().getProperties();
        if (properties == null)
            properties = new LinkedHashMap<String, String>();
        return properties;
    }

    public Map<String, Report> getReports()
    {
        return getDockindInfo().getReports();
    }

    public Report getReport(String reportCn)
    {
        return (getDockindInfo().getReports() != null ? getDockindInfo().getReports().get(reportCn) : null);
    }

	public List<ProcessChannel> getProcessChannels() throws EdmException {
        List<ProcessChannel> ret = Lists.newArrayList();
        for(ChannelProvider cp : getDockindInfo().getChannelProviders()){
            ret.addAll(cp.getEditableChannels());
        }
        ret.addAll(getDockindInfo().getStaticProcessChannels());
		return ret;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public String getForrmatedAddDate()
	{
		if(addDate != null)
			return DateUtils.formatCommonDateTime(addDate);
		else
			return "";
	}

	public List<LabelDefinition> getLabelsDefs() {
		return getDockindInfo().getLabelsDefs();
	}

	public void setLabelsDefs(List<LabelDefinition> labelsDefs) {
		getDockindInfo().setLabelsDefs(labelsDefs);
	}

	public Date getAvailableToDate() {
		return availableToDate;
	}

	public void setAvailableToDate(Date availableToDate) {
		this.availableToDate = availableToDate;
	}

    /**
     * Metoda ma mie� zadanie walidacji danych przekazanych przed zapisywaniem dokument�w.
     * Czyli ma posiada� walidacj� tak� jak jest na formatka jako AJAX. Tutaj b�dzie walidacja od strony kodu.
     * Wymagane do DS API oraz do Importera.
     * @param values
     * @param kind
     * @throws EdmException
     */
    public void validateFields(String dockindCn, Map<String, Object> values) throws EdmException
    {
        Map<String, Field> requiredField = getRequiredField();
        Set<String> requriedKeys = requiredField.keySet();
        Set<String> valuesKeys = values.keySet();

        Sets.SetView<String> difference = Sets.difference(requriedKeys, valuesKeys);
        if(!difference.isEmpty()){
            throw new EdmException("Brak wymaganych p�l na dokumencie:  " + difference.toString()+ "!");
        }

        for(Map.Entry<String , Object> entry : values.entrySet()){
            String cn = entry.getKey();
            Field dicField = getFieldByCn(cn);
            if(dicField instanceof DictionarableField){
                checkDictionaryRequiredFieldsValue(dockindCn, entry, cn, dicField);
            }
        }
    }

    /**
     * Sprawdza czy obiekt s�ownika ma uzupe�nione wymagane pola
     * @param dockindCn
     * @param entry
     * @param cn
     * @param dicField
     * @throws EdmException
     */
    protected void checkDictionaryRequiredFieldsValue(String dockindCn, Map.Entry<String, Object> entry, String cn, Field dicField) throws EdmException {
        log.info("Pole s�ownikowe {}, {}", dicField.getClass(), entry.getKey());
        DwrSearchable dictionary = FieldsManagerUtils.getDictionaryFromDockind(dockindCn, entry.getKey());
        String object = entry.getValue().toString();
        checkDictionarFields(cn, (DictionarableField) dicField, dictionary, object);
    }

    /**
     * Sprawdza czy obiekt s�ownika ma uzupe�nione wymagane pola, uwzgl�dnia slwoniki mulit
     * @param dockindCn
     * @param entry
     * @param cn
     * @param dicField
     * @throws EdmException
     */
    private void checkDictionarFields(String cn, DictionarableField dicField, DwrSearchable dictionary, String object) throws EdmException {
        String[] keys = StringUtils.split(object,",");
        if(keys != null && keys.length > 0){
            for(String key : keys){
                //obiekt s�ownika
                Map<String, Object> dicFields = dictionary.getValues(key);
                if(!dicFields.containsKey("id") || dicFields.get("id") == null)
                    throw new EdmException("Nie ma wpisu s�ownika " + cn + " o warto�ci " + key);
                //wymagane pola s�ownika
                Map<String, Field> requiredDicFields = dicField.getRequiredFields();
                log.info("wartosci wymagane s�ownika {}", requiredDicFields);
                for(String k : requiredDicFields.keySet()){
                    Object o = dicFields.get(cn + "_" + cutOffUnderline(k));
                    if(o == null || "".equals(o) ){
                        throw new EdmException("S�ownik: " + cn + " posiada nie uzupe�nione wymagane pole " + k + "!");
                    }
                }
            }
        }
    }

    private String cutOffUnderline(String key) {

    	if ( key.contains("_") ) {
	    	int lastIndexOfUnderline = key.lastIndexOf("_");
	    	boolean isAlphaNumTailOfKey = checkIfAlphaNum(key.substring(lastIndexOfUnderline+1));
	    	if (isAlphaNumTailOfKey) {
	    		return key.substring(0, lastIndexOfUnderline);
	    	} else {
	    		return key;
	    	}
    	} else {
    		return key;
    	}
	}

	private boolean checkIfAlphaNum(String substring) {
		return StringUtils.isNumeric(substring);
	}

	/**
	 * Komparator dla  mapy wartosci wykorzystywany w setWithHistory funkcji podczas por�wnywania obiekt�w
	 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
	 */
	public static class MapIdComparator implements Comparator<Object>
	{
			  public int compare(Object o1, Object o2) {
				  if(((Map<String,Object>)o2).get("id") != null && ((Map<String,Object>)o1).get("id") != null)
					  return ((Map<String,Object>)o2).get("id").toString().compareTo(((Map<String,Object>)o1).get("id").toString());
				  else
					  return 1;
			  }
	}
	
	public static class DocumentKindComparatorByName implements Comparator<DocumentKind>
	    {
	        public int compare(DocumentKind e1, DocumentKind e2)
	        {
	            return e1.getName().compareTo(e2.getName());
	        }
	    }
}
