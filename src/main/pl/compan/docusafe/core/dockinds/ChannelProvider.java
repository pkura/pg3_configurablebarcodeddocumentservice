package pl.compan.docusafe.core.dockinds;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;

import java.util.List;

/**
 * Obiekt zwracaj�cy nazw� kana�u dla danego dokumentu
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface ChannelProvider extends Function<DocFacade, String> {

    /**
     * Zwraca kana� dla danego dokumentu, wymaga otwartego kontekstu
     * @return nazwa kana�u - mo�e by� null (dokument nie jest obs�ugiwany przez tego providera)
     */
    String apply(DocFacade documentFacade);

    /**
     * Zwraca list� kana��w kt�re mo�na edytowa� w ProcessCoordinatorsAction
     * @return lista mo�e by� pusta, nie mo�e by� null
     */
    List<ProcessChannel> getEditableChannels() throws EdmException;
}
