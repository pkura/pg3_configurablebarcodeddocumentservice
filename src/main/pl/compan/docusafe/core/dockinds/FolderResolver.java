package pl.compan.docusafe.core.dockinds;

import pl.compan.docusafe.core.EdmException;

import java.util.Map;

/**
 * Interfesj obiekt�w s�u��cych do dodawania dokumentu do odpowiedniego katalogu,
 * Implementacje musz� by� thread-safe
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface FolderResolver {

    /**
     * Dodaje przekazany dokument do odpowiedniego katalogu
     * @param documentId id dokumentu
     */
    public void insert(long documentId, Map<String, ?> additionalParams) throws EdmException;

}
