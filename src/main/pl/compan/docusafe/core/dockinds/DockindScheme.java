package pl.compan.docusafe.core.dockinds;

import java.io.Serializable;
import java.util.*;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.SchemeField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DockindScheme implements Serializable
{
	private final static Logger LOG = LoggerFactory.getLogger(DockindScheme.class);
	public static final String CONDITION_MIXING_AND = "and";
	public static final String CONDITION_MIXING_OR = "or";
	
	private String cn;
	private String conditionMixing;
	private List<Condition> conditions = new ArrayList<Condition>();
	private final Map<String,SchemeField> schemeFields = new HashMap<String, SchemeField>();
	
	/**
     * wczytuje z elementu z XMLa dotyczaca schemat�w (znajduj�c�
     * si� wewn�trz tag-u "schemas")
     */
    public static void readSchemas(Element elSchemas,List<DockindScheme> dockindScheme) throws EdmException
    {       
        if (elSchemas != null)        	
        {        
            // kanaly
        	List elSchemelList = elSchemas.elements("scheme");  
            if (elSchemelList != null)
            {
            	//dockindScheme = new ArrayList<DockindScheme>();
                for (Iterator itemIter=elSchemelList.iterator(); itemIter.hasNext(); )
                {
                	
                    Element elSheme = (Element) itemIter.next();
                    DockindScheme dSch = new DockindScheme();
                    dSch.setCn(elSheme.attributeValue("cn"));
                    dSch.setConditionMixing(elSheme.attributeValue("condition-mixing"));
                    
                    // warunki w kanale
                    Element conditions  = elSheme.element("conditions");
                    List elConditionList = conditions.elements("condition");
                    if (elConditionList != null)
                    {
                        for (Iterator conIter=elConditionList.iterator(); conIter.hasNext(); )
                        {
                        	Element elCon = (Element) conIter.next();
                        	Condition con = new Condition();
                        	con.setField(elCon.attributeValue("field"));
                        	con.setOperator(elCon.attributeValue("operator"));
                        	con.setValue(elCon.attributeValue("value"));
                        	dSch.addCondition(con);
                        }                        
                    }
                    
                    //Pola schematu
                    Element schemeFields  = elSheme.element("schemeFields");
                    List elschemeFieldList = schemeFields.elements("schemeField");
                    if (elschemeFieldList != null)
                    {
                        for (Iterator fieldIter=elschemeFieldList.iterator(); fieldIter.hasNext(); )
                        {
                        	Element elFiled = (Element) fieldIter.next();
                        	SchemeField field = new SchemeField(elFiled.attributeValue("cn"));
                        	field.setColor(elFiled.attributeValue("color"));
                        	field.setVisible("true".equals(elFiled.attributeValue("visible")));
                        	
                        	if(elFiled.attributeValue("disabled") != null)
                        	{
                        		field.setDisabled( "true".equals(elFiled.attributeValue("disabled")) );
                        	}
                        	else
                        	{
                        		field.setDisabled(new Boolean(false)); 
                        	}

                        	if(elFiled.attributeValue("showAs") != null)
                        	{                        		
                        		field.setShowAs(elFiled.attributeValue("showAs"));
                        	}
                        	else
                        	{
                        		field.setShowAs("normal");
                        	}

                        	dSch.addSchemeField(field);
                        }                        
                    }
                    dockindScheme.add(dSch);
                }
            }
        }        
    }

	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getConditionMixing() {
		return conditionMixing;
	}
	public void setConditionMixing(String conditionMixing) {
		this.conditionMixing = conditionMixing;
	}
	public List<Condition> getConditions() {
		return conditions;
	}
	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}
	public void addCondition(Condition c)
	{
		this.conditions.add(c);
	}
	public Map<String, SchemeField> getSchemeFields() {
		return schemeFields;
	}
//	public void setSchemeFields(Map<String, SchemeField> schemeFields) {
//		this.schemeFields = schemeFields;
//	}
	public void addSchemeField(SchemeField c)
	{
		this.schemeFields.put(c.getCn(), c);
	}
	
	@Override
	public String toString()
	{
		return " :"+cn+":"+conditionMixing+":"+conditions.size()+":"+schemeFields.size()+":";
	}
}
