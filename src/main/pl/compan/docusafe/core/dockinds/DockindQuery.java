package pl.compan.docusafe.core.dockinds;

import com.google.common.collect.Lists;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.FormQuery;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.Database.Column;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.ListValueDbEnumField;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.web.archive.repository.search.SearchCriteria;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Klasa s�u��ca do budowania zapyta� dla wyszukiwania dokument�w z okre�lonym
 * rodzajem dokumentu. 
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class DockindQuery extends FormQuery
{
    private static final Logger LOG = LoggerFactory.getLogger(DockindQuery.class);
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(SearchDockindDocumentsAction.class.getPackage().getName(), null);
    public static String ATTRIBUTE_SEPARATOR = ",";

    public static final String DATAMART_DOCUMENT_VIEW_TABLE = "ds_datamart_document_view_log";

    private final static JoinInfo accepted = new JoinInfo("document_id", "ds_document", "id");
    private final static JoinInfo datamart = new JoinInfo("document_id", "ds_document", "id", true);
    private final static JoinInfo changelog = new JoinInfo("document_id", "ds_document", "id");
    private final static JoinInfo flags = new JoinInfo("document_id", "ds_document", "id");
    private final static JoinInfo userflags = new JoinInfo("document_id", "ds_document", "id");
    private final static JoinInfo inOfficeDocument = new JoinInfo("id", "ds_document", "id");
    private final static JoinInfo outOfficeDocument = new JoinInfo("id", "ds_document", "id");
    private final static JoinInfo person = new JoinInfo("document_id", "ds_document", "id");
    private final static JoinInfo journal = new JoinInfo("documentid", "ds_document", "id");

    private DocumentKind documentKind;
    private Map<String,JoinInfo> tables = new HashMap<String,JoinInfo>();
    /**
     * okre�la czy maj� by� sprawdzane uprawnienia do dokument�w podczas wyszukiwania
     */
    private boolean checkPermissions = true;
    /**
     * Zbi�r 'cn' p�l napisowych, kt�re chcemy �eby by�y wyszukiwane dok�adnie (czyli przez eq, a nie like)
     * - czyli zignorujemy MATCH_EXACT z danego pola.
     * Te pola musz� by� dodane tutaj ZANIM zostan� wywo�ane odpowiednie metody stringField, bo wpp
     * forceSearchEq nie b�dzie mia� wp�ywu na operatory wyszukiwania.
     */
    private Set<String> forceSearchEq = new HashSet<String>();

    /**
     * Czy nalezy wylaczyc konstrukcje UPPER (param) w szukaniu
     * Stosowac tylko wtedy gdy mamy pewnosc ze baza ma CaseInsensitive search
     */
    private boolean ommitUpperInSearch = false;

    /**
     * Normalny spos�b budowania zapytania - nie poprzez modyikacj� o�miu klas dla jednego przypadku (patrz addEq)
     * Lista zawiera wyra�enia z WHERE
     */
    private List<Expression> expressionList = Lists.newArrayList();

    /**
     * Dockind query wspiera tylko pojedyncz� osob�, nie mo�na szuka� sendera i recipienta jednocze�nie
     */
    private boolean personChoosen = false;
    
    private DockindQuery(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
        if ("true".equalsIgnoreCase(Docusafe.getAdditionProperty("dockidQuery.ommitUpperInSearch")))
        {
            ommitUpperInSearch = true;
        }
    }

    public DockindQuery(int offset, int limit)
    {
        super(offset, limit);
    }

    private void notifyTable(String tableName, JoinInfo join)
    {
        if (tableName == null)
            throw new NullPointerException("tableName");
        if (tables.get(tableName) == null)
        {
            tables.put(tableName, join);
        }
    }

    public DocumentKind getDocumentKind()
    {
        return documentKind;
    }

    public void setDocumentKind(DocumentKind documentKind)
    {
        this.documentKind = documentKind;
    }

    public Map<String, JoinInfo> getTables()
    {
        return tables;
    }

    public void accessedBy(String username)
    {
        if (username == null)
            throw new NullPointerException("username");
        addEq("ds_document_changelog"+ATTRIBUTE_SEPARATOR+"username", username);
        notifyTable("ds_document_changelog", changelog);
    }

    private void accessedAs(String action)
    {
        addEq("ds_document_changelog"+ATTRIBUTE_SEPARATOR+"what", action);
    }

    public void enumAccessedAs(String[] actions)
    {
        if (actions == null)
            throw new NullPointerException("actions");

        if (actions.length == 0)
            return;

        // TODO: dla AND dodawa� warunki do g��wnego obiektu

        int added = 0;
        DockindQuery or = new DockindQuery(documentKind);
        for (int i=0; i < actions.length; i++)
        {
            if (actions[i] != null && actions[i].length() > 0)
            {
                or.accessedAs(actions[i]);
                added++;
            }
        }

        if (added > 0)
        {
            addDisjunction(or);
            notifyTable("ds_document_changelog", changelog);
        }
    }

    public void acceptedBy(String username){
        addEq("ds_document_acceptance"+ATTRIBUTE_SEPARATOR+"username", username);
        notifyTable("ds_document_acceptance", accepted);
    }

    public void accessedFromTo(Date accessedFrom, Date accessedTo)
    {
        if (accessedFrom != null)
            addGe("ds_document_changelog"+ATTRIBUTE_SEPARATOR+"ctime", accessedFrom);
        if (accessedTo != null)
            addLe("ds_document_changelog"+ATTRIBUTE_SEPARATOR+"ctime", accessedTo);
    }

    public void viewed(boolean notViewed, String[] usernames, Date viewFrom, Date viewTo) {
        notifyTable(DATAMART_DOCUMENT_VIEW_TABLE, datamart);

        if (notViewed) {
            FormQuery or = new FormQuery();
            or.addEq(DATAMART_DOCUMENT_VIEW_TABLE + ATTRIBUTE_SEPARATOR + "username", null);
            if (usernames != null) {
                or.addNotIn(DATAMART_DOCUMENT_VIEW_TABLE + ATTRIBUTE_SEPARATOR + "username", usernames);
            }
            addDisjunction(or);
        } else {
            if (usernames != null) {
                addIn(DATAMART_DOCUMENT_VIEW_TABLE + ATTRIBUTE_SEPARATOR + "username", usernames);
            }
            if (viewFrom != null) {
                Date fromMidnight = DateUtils.nullSafeMidnight(viewFrom, 0);
                addGe(DATAMART_DOCUMENT_VIEW_TABLE + ATTRIBUTE_SEPARATOR + "event_date", fromMidnight);
            }
            if (viewTo != null) {
                Date toMidnight = DateUtils.nullSafeMidnight(viewTo != null ? viewTo : new Date(), 1);
                addLe(DATAMART_DOCUMENT_VIEW_TABLE + ATTRIBUTE_SEPARATOR + "event_date", toMidnight);
            }
        }
    }

    public void boxId(Long value)
    {
        if (value == null)
            throw new NullPointerException("value");
        addEq("ds_document"+ATTRIBUTE_SEPARATOR+"box_id", value);
    }
    
    public void setFlags(List<Long> flags, Date from, Date to)
    {
    	Long[] flagIds = flags.toArray(new Long[flags.size()]);

    	DockindQuery or = new DockindQuery(documentKind);
    	//DockindQuery flagi = new DockindQuery(documentKind);
    	for (int i=0; i < flagIds.length; i++)
        {
    		//or = new DockindQuery(documentKind);
    		or.addEq("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"label_id",flagIds[i]);
    		if (from != null)
    			or.addGe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(from, 0));
            if (to != null)
            	or.addLe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(to, 1));
            //flagi.addDisjunction(or);
        }
        addDisjunction(or);
        notifyTable("dso_document_to_label", DockindQuery.flags);
    }

    public void setUserFlags(List<Long> flags, Date from, Date to)
    {
    	Long[] flagIds = flags.toArray(new Long[flags.size()]);

    	DockindQuery or = new DockindQuery(documentKind);
    	//DockindQuery flagi = new DockindQuery(documentKind);
    	for (int i=0; i < flagIds.length; i++)
        {
    		//or = new DockindQuery(documentKind);
    		or.addEq("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"label_id",flagIds[i]);
    		if (from != null)
    			or.addGe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(from, 0));
            if (to != null)
            	or.addLe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(to, 1));
            //flagi.addDisjunction(or);
        }
        addDisjunction(or);
        notifyTable("dso_document_to_label", DockindQuery.userflags);
    }
    
    // TODO: do przerobienia, trzeba zagniezdzic warunki
    public void setAllFlags(Long[] flags, Date from, Date to, Long[] u_flags, Date u_from, Date u_to) {
    	DockindQuery or = new DockindQuery(documentKind);
    	if (flags != null) {
	    	for (int i=0; i < flags.length; i++) {
	    		or.addEq("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"label_id",flags[i]);
	        }
	   		if (from != null)
				or.addGe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(from, 0));
	        if (to != null)
	        	or.addLe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(to, 1));
    	}
    	if (u_flags != null) {
	    	for (int i=0; i < u_flags.length; i++) {
	    		or.addEq("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"label_id",u_flags[i]);
	        }
	   		if (u_from != null)
				or.addGe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(u_from, 0));
	        if (u_to != null)
	        	or.addLe("dso_document_to_label"+ATTRIBUTE_SEPARATOR+"ctime", DateUtils.midnight(u_to, 1));
    	}
       
        if((flags != null && flags.length > 0 ) || ( u_flags != null && u_flags.length > 0 ))
        {
        	 addDisjunction(or);
        	 notifyTable("dso_document_to_label", DockindQuery.userflags);
        }
      
    }

    public void senderAttributes(String firstname, String lastname, String organization, String organizationDivision, String senderLocation, String senderZip, String senderNip) throws EdmException{
        if(personChoosen){
            throw new EdmException(sm.getString("WyszukiwaniePoNadawcyIOdboircyNieJestMozliwe"));
        }
        if(firstname != null){
            addLike("dso_person"+ATTRIBUTE_SEPARATOR+"firstname", "%"+firstname+"%");
        }
        if(lastname != null){
        	addLike("dso_person"+ATTRIBUTE_SEPARATOR+"lastname", "%"+lastname+"%");
        }
        if(organization != null){
        	addLike("dso_person"+ATTRIBUTE_SEPARATOR+"organization", "%"+organization+"%");
        }
        if(organizationDivision != null){
        	addLike("dso_person"+ATTRIBUTE_SEPARATOR+"organizationDivision", "%"+organizationDivision+"%");
        }
        if(senderNip != null){
             addLike("dso_person"+ATTRIBUTE_SEPARATOR+"nip", "%"+senderNip+"%");
         }
        if(senderLocation != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"location", "%"+senderLocation+"%");
         }
        if(senderZip != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"zip", "%"+senderZip+"%");
         }
        addEq("dso_person"+ATTRIBUTE_SEPARATOR+"discriminator", "SENDER");
        notifyTable("dso_person",person);
        personChoosen = true;
    }

	public void personAttributes(String tableName, Object tableValue, Set<Map.Entry<Database.Column, String>> entries, SearchCriteria recipientSC)
			throws EdmException
	{
		if (personChoosen)
			throw new EdmException(sm.getString("WyszukiwaniePoNadawcyIOdboircyNieJestMozliwe"));
		if (!entries.isEmpty())
		{
			for (Map.Entry<Database.Column, String> entry : entries)
				addLike(tableName + ATTRIBUTE_SEPARATOR + entry.getKey(), "%" + entry.getValue() + "%");

		} else if (!recipientSC.getColumnValue().isEmpty())
		{

			for (Entry<Column, String> entry : recipientSC.getColumnValue())
			{
				addLike(tableName + ATTRIBUTE_SEPARATOR + entry.getKey(), "%" + entry.getValue() + "%");
			}

		}
		addEq(tableName + ATTRIBUTE_SEPARATOR + "discriminator", tableValue);
		notifyTable(tableName, person);
		personChoosen = true;
	}

    public void recipientAttributes(String firstname, String lastname, String organization, String organizationDivision, String recipientLocation, String recipientNip, String recipientZip) throws EdmException {
        if(personChoosen){
            throw new EdmException(sm.getString("WyszukiwaniePoNadawcyIOdboircyNieJestMozliwe"));
        }
        if(firstname != null){
        	addLike("dso_person"+ATTRIBUTE_SEPARATOR+"firstname", "%"+firstname+"%");
        }
        if(lastname != null){
        	addLike("dso_person"+ATTRIBUTE_SEPARATOR+"lastname", "%"+lastname+"%");
        }
        if(organization != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"organization", "%"+organization+"%");
        }
        if(organizationDivision != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"organizationDivision", "%"+organizationDivision+"%");
        }
        if(recipientNip != null){
             addLike("dso_person"+ATTRIBUTE_SEPARATOR+"nip", "%"+recipientNip+"%");
         }
        if(recipientLocation != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"location", "%"+recipientLocation+"%");
         }
        if(recipientZip != null){
     	   addLike("dso_person"+ATTRIBUTE_SEPARATOR+"zip", "%"+recipientZip+"%");
         }
        addEq("dso_person"+ATTRIBUTE_SEPARATOR+"discriminator", "RECIPIENT");
        notifyTable("dso_person", person);
        personChoosen = true;
    }

    public void outOfficePostalRegNumber(String number) {
        addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"postalregnumber", number);
        notifyTable("dso_out_document",outOfficeDocument);
    }

    public void inOfficePostalRegNumber(String number) {
        if(AvailabilityManager.isAvailable("seachDockindDocument.LikePostalRegnumber")){
            addLike("dso_in_document"+ATTRIBUTE_SEPARATOR+"postalregnumber", "%"+number+"%", true);
        }
        else
        {
            addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"postalregnumber", number);
        }
        notifyTable("dso_in_document",inOfficeDocument);
    }

    public void outOfficeDocumentDate(Date from, Date to) {
        if (from != null){
            addGe("dso_out_document"+ATTRIBUTE_SEPARATOR+"documentdate", from);
        }
        if (to != null){
            addLe("dso_out_document"+ATTRIBUTE_SEPARATOR+"documentdate", to);
        }
        notifyTable("dso_out_document",outOfficeDocument);
    }
    
    public void officeDelivery(String type, Object id)
    {
    	if("out".equals(type) && id != null)
    	{
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"delivery", id);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    	else if("in".equals(type) && id != null)
    	{
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"delivery", id);
    		notifyTable("dso_in_document",inOfficeDocument);
    	}    	
    }
    public void officeDocumentClerk(String type, Object id)
    {
    	if("out".equals(type) && id != null)
    	{
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"clerk", id);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    	else if("in".equals(type) && id != null)
    	{
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"clerk", id);
    		notifyTable("dso_in_document",inOfficeDocument);
    	}    	
    }
    
    public void inOfficeKind(Object id)
	{
		if (id != null)
		{
			addEq("dso_in_document" + ATTRIBUTE_SEPARATOR + "kind", id);
			notifyTable("dso_in_document", inOfficeDocument);
		}
	}
    
    public void inOfficeInvoiceKind(Object id)
    {
    	if (id != null)
		{
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"invoiceKind", id);
    		notifyTable("dso_in_document",inOfficeDocument); 
		}
    }
    public void inOfficeStatus(Object id)
    {	if (id != null)
		{
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"status", id);
    		notifyTable("dso_in_document",inOfficeDocument); 
		}
    }
 
    public void likeOfficeCaseSymbol(String type, String symbol)
    {
    	if("out".equals(type) && symbol != null)
    	{
    		addLike("dso_out_document"+ATTRIBUTE_SEPARATOR+"casedocumentid", "%"+symbol+"%",true);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    	else if("in".equals(type) && symbol != null)
    	{
    		addLike("dso_in_document"+ATTRIBUTE_SEPARATOR+"casedocumentid", "%"+symbol+"%",true);
    		notifyTable("dso_in_document",inOfficeDocument);
    	}    	
    }
    
    public void likedocumentReferenceId(String type, String reference)
    {
    	if("out".equals(type) && reference != null)
    	{
    		addLike("dso_out_document"+ATTRIBUTE_SEPARATOR+"referenceid", "%"+reference+"%",true);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    	else if("in".equals(type) && reference != null)
    	{
    		addLike("dso_in_document"+ATTRIBUTE_SEPARATOR+"referenceid",  "%"+reference+"%",true);
    		notifyTable("dso_in_document",inOfficeDocument);
    	}    	
    }
    
    public void likeInOfficeLocation(String location) {
        addLike("dso_in_document"+ATTRIBUTE_SEPARATOR+"location", location);
        notifyTable("dso_in_document",inOfficeDocument);
    }
    
    
    public void documentBarcode(String type, String barcode)
    {
    	if("out".equals(type) && barcode != null)
    	{
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"barcode", barcode);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    	else if("in".equals(type) && barcode != null)
    	{
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"barcode", barcode);
    		notifyTable("dso_in_document",inOfficeDocument);
    	}    	
    }
    public void outOfficeZpoDate(Date from, Date to) {
        if (from != null){
            addGe("dso_out_document"+ATTRIBUTE_SEPARATOR+"zpodate", from);
        }
        if (to != null){
            addLe("dso_out_document"+ATTRIBUTE_SEPARATOR+"zpodate", to);
        }
        notifyTable("dso_out_document",outOfficeDocument);
    }
    
    public void isZpo(Boolean zpo) {
        if (zpo != null){
            addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"zpo", zpo);
        }
        notifyTable("dso_out_document",outOfficeDocument);
    }

    public void isAdditionalZpo(Boolean additionalZpo) {
        if (additionalZpo != null){
            addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"additionalZpo", additionalZpo);
        }
        notifyTable("dso_out_document",outOfficeDocument);
    }

	public void czyAktualny() {
		addEq("ds_document" + ATTRIBUTE_SEPARATOR + "CZY_AKTUALNY", Boolean.TRUE);
		 
	}
    
    public void outOfficeGauge(Integer id) {
    	if (id != null) {
    		addEq("dso_out_document" + ATTRIBUTE_SEPARATOR + "gauge", id);
    	}
    	notifyTable("dso_out_document", outOfficeDocument);
    }
    public void inSubmitToBip(Boolean bip) {
        if (bip != null){
            addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"submittobip", bip);
        }
        notifyTable("dso_in_document",inOfficeDocument);
    }

    public void isPriority(Boolean priority)
	{
    	if (priority != null){
            addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"priority", priority);
        }
        notifyTable("dso_out_document",outOfficeDocument);
		
	}
    public void isOriginal(Boolean original) {
        if (original != null){
            addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"original", original);
        }
        notifyTable("dso_in_document",inOfficeDocument);
    }
    
    public void inOfficeDocumentDate(Date from, Date to) {
        if (from != null){
            addGe("dso_in_document"+ATTRIBUTE_SEPARATOR+"documentdate", from);
        }
        if (to != null){
            addLe("dso_in_document"+ATTRIBUTE_SEPARATOR+"documentdate", to);
        }
        notifyTable("dso_in_document",inOfficeDocument);
    }
    
    public void inDocumentStampDate(Date date)
	{
    	addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"stampdate", date);
    	 notifyTable("dso_in_document",inOfficeDocument);
		
	}
    public void likeDocumentDescription(String desc){
    	addLike("ds_document"+ATTRIBUTE_SEPARATOR+"description", desc);
    }
    public void documentDescription(String desc){
        addEq("ds_document"+ATTRIBUTE_SEPARATOR+"description", desc);
    }
    public void likeDocumentAbstractDescription(String desc){
        addLike("ds_document"+ATTRIBUTE_SEPARATOR+"abstractdescription", desc);
    }
    public void officeNumber(Integer ko, boolean isInOfficeDocument){
    	if (isInOfficeDocument) {
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"officenumber", ko);
    		notifyTable("dso_in_document",inOfficeDocument);
    	} else {
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"officenumber", ko);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    }
    public void officeNumber(Long ko, boolean isInOfficeDocument){
    	if (isInOfficeDocument) {
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"officenumber", ko);
    		notifyTable("dso_in_document",inOfficeDocument);
    	} else {
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"officenumber", ko);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    }
    public void officeNumberFromTo(Integer from,Integer to, boolean isInOfficeDocument){
    	if (isInOfficeDocument) {
    		if (from!=null){
    		addGe("dso_in_document"+ATTRIBUTE_SEPARATOR+"officenumber", from);
    		notifyTable("dso_in_document",inOfficeDocument);
    		}
    		if (to!=null){
    			addLe("dso_in_document"+ATTRIBUTE_SEPARATOR+"officenumber", to);
        		notifyTable("dso_in_document",inOfficeDocument);
    		}
    	} else {
    		if (from!=null){
    		addGe("dso_out_document"+ATTRIBUTE_SEPARATOR+"officenumber", from);
    		notifyTable("dso_out_document",outOfficeDocument);
    		}
    		if (to!=null){
    			addLe("dso_out_document"+ATTRIBUTE_SEPARATOR+"officenumber", to);
        		notifyTable("dso_out_document",outOfficeDocument);
    		}
    	}
    }
    public void officeNumberYear(Long rok, boolean isInOfficeDocument){
    	if (isInOfficeDocument) {
    		addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"officeNumberYear", rok);
    		notifyTable("dso_in_document",inOfficeDocument);
    	} else {
    		addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"officeNumberYear", rok);
    		notifyTable("dso_out_document",outOfficeDocument);
    	}
    }

    public void journalId(Long journalId) {
        addEq("DSO_JOURNAL_ENTRY"+ATTRIBUTE_SEPARATOR+"journal_id", journalId);
        notifyTable("DSO_JOURNAL_ENTRY",journal);
    }

    public void barcode(String barcode, boolean isInOfficeDocument){
        if (isInOfficeDocument) {
            addEq("dso_in_document"+ATTRIBUTE_SEPARATOR+"barcode", barcode);
            notifyTable("dso_in_document",inOfficeDocument);
        } else {
            addEq("dso_out_document"+ATTRIBUTE_SEPARATOR+"barcode", barcode);
            notifyTable("dso_out_document",outOfficeDocument);
        }
    }

    public void dictionary(DictionaryField field, Map<String, ?> values){
	    dictionary(field, values, "id");
    }
    private void dictionary(AbstractDictionaryField field, Map<String, ?> values, String id)
    {
        LOG.info("field = {}, values = {}", field, values);
        JoinInfo ji = new JoinInfo("document_id", "ds_document", "id");
        if(field.isMultiple()){ // je�eli jest multiple to jest dodatkowa tabela po drodze
            addEq(documentKind.getMultipleTableName() + ATTRIBUTE_SEPARATOR + "field_cn", field.getCn());
            notifyTable(documentKind.getMultipleTableName(), ji);
            notifyTable(field.getTable(), new JoinInfo(id, documentKind.getMultipleTableName(),"field_val"));
        } else { //je�li nie to field column wskazuje na kolumn� gdzie jest zapisywane pole
            notifyTable(field.getTable(), new JoinInfo(id, documentKind.getTablename(), field.getColumn()));
        }
        for(Field f: field.getDockindSearchFields())
        {
            if(values.containsKey(f.getCn()))
            {
	            if(f.getType().equals(Field.DATE))
	            {
	            	List<String> list = (List<String>) values.get(f.getCn());
	            	System.out.println("from "+list.get(0));
	            	System.out.println("to "+list.get(1));
	            	addGe(field.getTable() + ATTRIBUTE_SEPARATOR + f.getColumn().replaceAll("_1",""), DateUtils.parseJsDate(list.get(0)));
	                addLe(field.getTable() + ATTRIBUTE_SEPARATOR + f.getColumn().replaceAll("_1",""), DateUtils.parseJsDate(list.get(1)));
	            }
	            else if (Field.MATCH_CONTEXT.equals(f.getMatch()))
	            {
	          		  addLike(field.getTable() + ATTRIBUTE_SEPARATOR + f.getColumn().replaceAll("_1",""), "%"+values.get(f.getCn())+"%",!ommitUpperInSearch);
	            }
	          	else
	          	{	     
	          		System.out.println("tu nie");
	          		  addEq(field.getTable() + ATTRIBUTE_SEPARATOR + f.getColumn().replaceAll("_1",""), values.get(f.getCn()));
	          	}
	         }
        }
    }
    
    public void documentField(AbstractDictionaryField field, Map<String, ?> values)
    {
	    dictionary(field, values, "document_id");
    }

    public void ctime(Date ctimeFrom, Date ctimeTo)
    {
        if (ctimeFrom != null)
            addGe("ds_document"+ATTRIBUTE_SEPARATOR+"ctime", ctimeFrom);
        if (ctimeTo != null)
            addLe("ds_document"+ATTRIBUTE_SEPARATOR+"ctime", ctimeTo);                
    }
    public void stampDateIn(Date stampFrom, Date stampTo)
    {
        if (stampFrom != null)
            addGe("dso_in_document"+ATTRIBUTE_SEPARATOR+"stampDate", stampFrom);
        if (stampTo != null)
            addLe("dso_in_document"+ATTRIBUTE_SEPARATOR+"stampDate", stampTo);                
    }
    public void mtime(Date mtimeFrom, Date mtimeTo)
    {
        if (mtimeFrom != null)
            addGe("ds_document"+ATTRIBUTE_SEPARATOR+"mtime", mtimeFrom);
        if (mtimeTo != null)
            addLe("ds_document"+ATTRIBUTE_SEPARATOR+"mtime", mtimeTo);                
    }
    
    public void author(String author)
    {
        if (author != null)
            addEq("ds_document"+ATTRIBUTE_SEPARATOR+"author", author);
    }
    
    /**
     * Dodaje warunek Eq dla podanego pola i wskazanej warto�ci.
     */
    public void field(Field field, Object value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value != null)
        {
            if (field.isMultiple())
            {
                String attribute = documentKind.getMultipleTableName()+"#"+field.getCn()+ATTRIBUTE_SEPARATOR+"field_val";
                notifyTable(documentKind.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));
                addEq(attribute, value);
            }
            else
                addEq(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), value);
        }
    }
        
    /**
     * Dodaje warunek In dla podanego pola i wskazanej tablicy warto�ci
     */
    public void InField(Field field, Object[] value)
    {
    	InField(field, value, false);
    }
    
    /**
     * Dodaje warunek In dla podanego pola i wskazanej tablicy warto�ci
     * like = true tworzy in like 
     */
    public void InField(Field field, Object[] value,boolean like)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value != null)
        {
            if (field.isMultiple())
            {
                String attribute = documentKind.getMultipleTableName()+"#"+field.getCn()+ATTRIBUTE_SEPARATOR+"field_val";
                notifyTable(documentKind.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));

                if(like)
                {
	                DockindQuery or = new DockindQuery(documentKind);
	                for (Object object : value) {
	                	 or.addLike(attribute, object+"%");
					}
	                addDisjunction(or);
                }
                else
                	addIn(attribute, value);
                
            }
            else
            {
            	if(like)
            	{
	                DockindQuery or = new DockindQuery(documentKind);
	                for (Object object : value) {
	                	 or.addLike(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), object+"%");
					}
	                addDisjunction(or);
            	}
            	else
            	{
            		addIn(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), value);
            	}
            }
               

        }
    }

    /**
     * Dodaje warunek dla podanego pola (zak�adaj�c si� jest typu bool) i wskazanej warto�ci.
     */
    public void boolField(Field field, Boolean value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;
        if (value.booleanValue())
        {
            addEq(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), Boolean.TRUE);
        }
        else
        {
            DockindQuery or = new DockindQuery(documentKind);
            or.addEq(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), Boolean.FALSE);
            or.addEq(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), null);
            addDisjunction(or);
        }
    }    
    public void stringFieldRange(Field field, String valueMin, String valueMax){
    	 if (field == null)
             throw new NullPointerException("field");
         if (valueMin == null || valueMax==null)
             return;
         String attribute = documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn();
         addGe(attribute, valueMin);
         addLe(attribute, valueMax);
    }
    public void stringFieldWithout(Field field, String value){
    	 if (field == null)
             throw new NullPointerException("field");
         if (value == null)
             return;
         String attribute = documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn();
         
         
    }
    public void stringField(Field field, String value)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;

        String attribute;
        if (field.isMultiple())
        {
            attribute = documentKind.getMultipleTableName()+"#"+field.getCn()+ATTRIBUTE_SEPARATOR+"field_val";
            notifyTable(documentKind.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));
        }
        else
            attribute = documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn();
        
        if (Field.MATCH_EXACT.equals(field.getMatch()) || forceSearchEq.contains(field.getCn()))
            addEq(attribute, value);
        else if (Field.MATCH_STAR_CONTEXT.equals(field.getMatch())) {
//            LOG.info("star search !! " + field.getCn());
            if(value.indexOf('*') < 0 && value.indexOf('%') < 0){ //brak gwiazdki
                addEq(attribute, value);
            } else {
                addLike(attribute, value.replace("*","%"),!ommitUpperInSearch);
            }
        } else if (Field.MATCH_CONTEXT.equals(field.getMatch()))
            addLike(attribute, "%"+value+"%",!ommitUpperInSearch);
        else if (Field.MATCH_CONTEXT_CASE_SENSITIVE.equals(field.getMatch()))
            addLike(attribute, value+"%", false);
        else if (field.getMatch() != null)
	        {        		
        		try 
        		{
	        		int dlugosc = Integer.parseInt(field.getMatch());
	        		if(value.length() < dlugosc)
	        			addLike(attribute, "%"+value+"%");
	        		else
	        			addEq(attribute, value);
        		}	 
        		catch (Exception e)
        		{
        		}        		
	        }
        else
        {
            //jesli wlaczylismy ommitUpperInSearch to szukanie jest bez nieszczesnej kostrukcji UPPER(param)
            addLike(attribute, value+"%",!ommitUpperInSearch);
        }
    }
    
    public void InStringField(Field field, String[] value)
    {
    	String attribute = documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn();
    	addIn(attribute, value);
    }

    public void centrumKosztowField(Field field, Integer value)
    {  
		addEq("DSG_CENTRUM_KOSZTOW_FAKTURY"+ATTRIBUTE_SEPARATOR+"centrumId",value);        
        notifyTable("DSG_CENTRUM_KOSZTOW_FAKTURY",  new JoinInfo("documentId", "ds_document", "id"));        
    }
    
    public void kontoKosztoweField(String value)
    {
    	addEq("DSG_CENTRUM_KOSZTOW_FAKTURY"+ATTRIBUTE_SEPARATOR+"accountNumber",value);        
        notifyTable("DSG_CENTRUM_KOSZTOW_FAKTURY",  new JoinInfo("documentId", "ds_document", "id")); 
    }
    
    public void lokalizacjaField(String value)
    {
    	addEq("DSG_CENTRUM_KOSZTOW_FAKTURY"+ATTRIBUTE_SEPARATOR+"lokalizacja",value);        
        notifyTable("DSG_CENTRUM_KOSZTOW_FAKTURY",  new JoinInfo("documentId", "ds_document", "id")); 
    }
    
    public void rangeField(Field field, Object from, Object to)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (from != null)
            addGe(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), from);
        if (to != null)
            addLe(documentKind.getTablename()+ATTRIBUTE_SEPARATOR+field.getColumn(), to);
    }
    
    public void enumField(Field field, Integer value)
    {
    	enumField(field, TextUtils.integerToIntegerArray(value));
    }

    public void enumField(Field field, Integer[] values)
    {
        if (field == null)
            throw new NullPointerException("field");
        if (values == null)
            throw new NullPointerException("values");
        if (values.length == 0)
            return;

        DockindQuery or = new DockindQuery(documentKind);
        for (int i=0; i < values.length; i++)
        {
            if (values[i] != null)
                or.field(field, values[i]);
        }

        addDisjunction(or);
    }

    public void otherClassField(Field field, String property, String value) throws EdmException
    {
        if (field == null)
            throw new NullPointerException("field");
        if (value == null)
            return;
        try
        {
            Class c = Class.forName(field.getClassname());  
            String tableName = DSApi.getTableName(c);
            if (field.isMultiple())
            {
                notifyTable(documentKind.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));
                notifyTable(tableName, new JoinInfo("id", documentKind.getMultipleTableName()+"#"+field.getCn(), "field_val"));                  
            }            
            else
            {
                notifyTable(tableName, new JoinInfo("id", documentKind.getTablename(), field.getColumn()));            
            }
            if(field.isOverloadQuery(property))
            {
            	field.addOverloadQuery(property,value,this);
            }
            else
            {

            	String columnName = DSApi.getColumnName(c, property);
            if (Field.MATCH_EXACT.equals(field.getMatch()) || forceSearchEq.contains(field.getCn())) {
            	System.out.println(tableName + ATTRIBUTE_SEPARATOR + columnName + " " + value);
                addEq(tableName + ATTRIBUTE_SEPARATOR + columnName, value);
            } else if (Field.MATCH_STAR_CONTEXT.equals(field.getMatch())) {
//                LOG.info("star search !! " + field.getCn());
                if (value.indexOf('*') < 0 && value.indexOf('%') < 0) { //brak gwiazdki lub brak procenta
                    addEq(tableName + ATTRIBUTE_SEPARATOR + columnName, value);
                } else {
                    addLike(tableName + ATTRIBUTE_SEPARATOR + columnName, value.replace("*", "%"), !ommitUpperInSearch);
                }
            } else if (Field.MATCH_CONTEXT.equals(field.getMatch())) {
                addLike(tableName + ATTRIBUTE_SEPARATOR + columnName, "%"+value+"%", !ommitUpperInSearch);
            } else {
                addLike(tableName + ATTRIBUTE_SEPARATOR + columnName, value+"%", false);
            }
            }

        }
        catch (ClassNotFoundException e)
        {
            throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
        }
    }
    
    public void classField(Field field, String[] values,String column) throws EdmException, ClassNotFoundException
    {  
    	Class c = Class.forName(field.getClassname());  
        String tableName = DSApi.getTableName(c);
        String columnName = DSApi.getColumnName(c, column);
        
        if (field.isMultiple())
        {
            notifyTable(documentKind.getMultipleTableName()+"#"+field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));
            notifyTable(tableName, new JoinInfo("id", documentKind.getMultipleTableName()+"#"+field.getCn(), "field_val"));                  
        }            
        else
        {
            notifyTable(tableName, new JoinInfo("id", documentKind.getTablename(), field.getColumn()));            
        }
    	
        DockindQuery or = new DockindQuery(documentKind);
        for (int i=0; i < values.length; i++)
        {
            or.addLike(tableName+ATTRIBUTE_SEPARATOR+columnName,values[i]+"%");
        }
        addDisjunction(or);
    }
    
    public void listValueField(ListValueDbEnumField field, String[] values) throws EdmException, ClassNotFoundException
    {    
        String tableName = field.getTableName();
        String columnName = field.getIdColumnName();
        
        if (field.isMultiple())
        {
            notifyTable(documentKind.getMultipleTableName() + "#" + field.getCn(), new JoinInfo(documentKind.getTablename(), field.getColumn()));
            notifyTable(tableName, new JoinInfo("id", documentKind.getMultipleTableName()+"#"+field.getCn(), "field_val"));                  
        }            
        else
        {
            notifyTable(tableName, new JoinInfo("id", documentKind.getTablename(), field.getColumn()));            
        }
    	
        DockindQuery or = new DockindQuery(documentKind);
        for (int i=0; i < values.length; i++)
        {
            or.addEq(tableName+ATTRIBUTE_SEPARATOR+columnName,values[i]);
        }
        addDisjunction(or);
    }
    
    public boolean isCheckPermissions()
    {
        return checkPermissions;
    }

    public void setCheckPermissions(boolean checkPermissions)
    {
        this.checkPermissions = checkPermissions;
    }

    public void addForceSearchEq(String fieldCn)
    {
        forceSearchEq.add(fieldCn);
    }

    @Override
    public void orderAsc(String attribute) throws EdmException
    {
        super.orderAsc(attribute);
        checkSortField(attribute);
    }

    @Override
    public void orderDesc(String attribute) throws EdmException
    {
        super.orderDesc(attribute);
        checkSortField(attribute);
    }      
    
    /**
     * Sprawdza czy konieczne jest dodania tabeli s�ownikowej do zapytania ze wzgl�du na sortowanie po danym polu.
     */
    private void checkSortField(String attribute) throws EdmException
    {
        if (attribute.startsWith("dockind_"))
        {
            String fieldCn = attribute.substring("dockind_".length());
            Field field = documentKind.getFieldByCn(fieldCn);
            if (Field.CLASS.equals(field.getType()))
            {
                try
                {
                    Class c = Class.forName(field.getClassname());       
                    String tableName = DSApi.getTableName(c);                                       
                    
                    notifyTable(tableName, new JoinInfo("id", documentKind.getTablename(), field.getColumn()));
                }
                catch (ClassNotFoundException e)
                {
                    throw new EdmException("Nie znaleziono klasy: "+field.getClassname());
                }               
            }
        }
    }



    /**
     * Przechowuje informacje o z��czaniu danej tabeli
     */
    public static class JoinInfo implements Serializable
    {
        /**
         * nazwa g��wnej kolumny w tabeli, kt�rej ten obiekt dotyczy
         */
        private String tableKeyColumn;
        /**
         * nazwa tabeli, z kt�r� "ta tabela" ma zosta� z��czona
         */
        private String joinTableName;
        /**
         * nazwa g��wnej kolumny w tabeli, z kt�r� ten obiekt ma by� po��czony
         */
        private String joinTableColumn;
        /**
         * true <=> oznacza tabel� z warto�ciami wielokrotnymi (zamiast tableKeyColumn ��czenie po document_id,field_cn)
         */
        private boolean multiple;

        /**
         * po��czenie typu outer (left outer) na tabeli joinTableName
         */
        private boolean outerJoin;

        /**
         * Z��czenie ze zwyk�� tabel�.
         */
        public JoinInfo(String tableKeyColumn, String joinTableName, String joinTableColumn)
        {
            this.tableKeyColumn = tableKeyColumn;
            this.joinTableName = joinTableName;
            this.joinTableColumn = joinTableColumn;
        }

        public JoinInfo(String tableKeyColumn, String joinTableName, String joinTableColumn, boolean outerJoin)
        {
            this.tableKeyColumn = tableKeyColumn;
            this.joinTableName = joinTableName;
            this.joinTableColumn = joinTableColumn;
            this.outerJoin = outerJoin;
        }

        /**
         * Z��czenie z tabel� trzymaj�ca warto�ci wielokrotne dla dockina-a.
         */
        public JoinInfo(String joinTableName, String joinTableColumn)
        {
            this.multiple = true;
            this.joinTableName = joinTableName;
            this.joinTableColumn = joinTableColumn;
        }
        
        public String getTableKeyColumn()
        {
            return tableKeyColumn;
        }

        public void setTableKeyColumn(String tableKeyColumn)
        {
            this.tableKeyColumn = tableKeyColumn;
        }

        public String getJoinTableName()
        {
            return joinTableName;
        }

        public void setJoinTableName(String joinTableName)
        {
            this.joinTableName = joinTableName;
        }

        public String getJoinTableColumn()
        {
            return joinTableColumn;
        }

        public void setJoinTableColumn(String joinTableColumn)
        {
            this.joinTableColumn = joinTableColumn;
        }

        public boolean isMultiple()
        {
            return multiple;
        }

        public void setMultiple(boolean multiple)
        {
            this.multiple = multiple;
        }

        public boolean isOuterJoin() {
            return outerJoin;
        }

        public void setOuterJoin(boolean outerJoin) {
            this.outerJoin = outerJoin;
        }
    }
    
    public void addExpression(Expression ex) {
        expressionList.add(ex);
    }

    public List<Expression> getExpressionList() {
        return expressionList;
    }

    public void setTables(Map<String, JoinInfo> tables) {
		this.tables = tables;
	}
    
    public FormQuery addMSSQLContains(String column, String textQuery){
        checkNotNull(column, "column cannot be null");
        checkNotNull(textQuery, "textQuery cannot be null");
        LOG.info("oracle contains '{}' form column '{}'", textQuery, column);
        addExpression(Expression.mssqlContains(column, textQuery));
        return this;
    }

    public FormQuery addOracleContains(String column, String textQuery){
        checkNotNull(column, "column cannot be null");
        checkNotNull(textQuery, "textQuery cannot be null");
        LOG.info("oracle contains '{}' form column '{}'", textQuery, column);
        addExpression(Expression.oracleContains(column, textQuery));
        return this;
    }

	

	
}
