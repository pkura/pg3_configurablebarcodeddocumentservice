package pl.compan.docusafe.core.dockinds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

public class Aspect 
{
	
	private String cn;
	private String name;
	private String guid;
	private DSDivision division;
	private DocumentKind documentKind;
	
	private List<Field> fields;
	private Map<String, List<EnumItem>> enumItems;
	
	public Aspect(String cn, String name, String guid, String documentKindCn) throws EdmException
	{
		this.cn = cn;
		this.name = name;
		this.documentKind = DocumentKind.findByCn(documentKindCn);
		if(!cn.equalsIgnoreCase("default"))
		{
			this.guid = guid;
			this.division = DSDivision.find(this.guid);
		}
		else
		{
			this.guid = null;
			this.division = null;
		}
		
		
	}
	
	public void addEnumItem(String fieldCn, EnumItem enumItem) throws EdmException
	{
		if(enumItems == null)
			enumItems = new HashMap<String, List<EnumItem>>();
		
		if(enumItems.containsKey(fieldCn))
		{
			enumItems.get(fieldCn).add(enumItem);
		}
		else
		{
			ArrayList<EnumItem> enums = new ArrayList<EnumItem>();
			enums.add(enumItem);
			enumItems.put(fieldCn, enums);
		}	
	}
	
	
	public void addField(Field field)
	{
		if(fields == null)
			fields = new ArrayList<Field>();
		fields.add(field);
	}

	public Boolean canUse(String username) throws EdmException
	{
		if(this.cn.equalsIgnoreCase("default")) return false;
		
		DSUser user = DSUser.findByUsername(username);
		return user.inDivision(this.division);
	}
	
	public List<Field> getFields() 
	{
		return fields;
	}
	
	public List<String> getFiListCns()
	{
		List<String> fieldsCns = new ArrayList<String>();
		
		for(Field field : fields)
		{
			fieldsCns.add(field.getCn());
		}
		
		return fieldsCns;
	}

	public String getCn() 
	{
		return cn;
	}

	public String getName() 
	{
		return name;
	}

	public String getGuid() 
	{
		return guid;
	}

	public DSDivision getDivision() 
	{
		return division;
	}



	public Map<String, List<EnumItem>> getEnumItems() {
		return enumItems;
	}

	public DocumentKind getDocumentKind() {
		return documentKind;
	}
	
}
